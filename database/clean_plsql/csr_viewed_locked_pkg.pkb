CREATE OR REPLACE
PACKAGE BODY clean.CSR_VIEWED_LOCKED_PKG
AS

PROCEDURE UPDATE_CSR_VIEWED
(
IN_CSR_ID                       IN CSR_VIEWED_ENTITIES.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_VIEWED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_VIEWED_ENTITIES.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating the
        entities viewed by the given csr.  If a given entity is already
        viewed, the timestamp will be updated on the record.  If not and
        the number of viewed records is at the limit, the oldest viewed
        record will be updated with the new information.  If the number of
        records is not at the limit, the information will be inserted.

Input:
        csr_id                          varchar2
        entity_type                     varchar2
        entity_id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_max_entities          number := NVL (TO_NUMBER (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE ('CSR_VIEWED_ENTITIES', 'MAX_ENTRIES')), 100);
v_total_viewed          number;
v_count                 number;
v_exists                char(1);

v_sql                   varchar2(32000);
v_sql_start             varchar2(20000);
v_sql_orders            varchar2(20000);
v_sql_orddet            varchar2(20000);
v_sql_add               varchar2(20000);

sql_cur                 types.ref_cursor;


BEGIN


-- get the number of viewed records for the csr
	SELECT  count(*)
	INTO    v_count
	FROM    csr_viewed_entities
	WHERE   csr_id = in_csr_id
	AND     entity_type = in_entity_type
	AND     entity_id = in_entity_id;

if v_count > 0 then
   v_exists := 'Y';
end if;

-- build sql based on entity_type passed
v_sql_start :=  ' select count(1) from ' ;

v_sql_orders :=	'(SELECT  distinct o.order_guid ';
v_sql_orders := v_sql_orders || ' FROM    clean.csr_viewed_entities v, clean.orders o ';
v_sql_orders := v_sql_orders ||	' WHERE   v.csr_id = '''||in_csr_id||''' ';
v_sql_orders := v_sql_orders ||	' AND     v.entity_type = ''ORDERS'' ';
v_sql_orders := v_sql_orders ||	' AND     o.order_guid = v.entity_id ' ;

v_sql_orddet :=	' UNION ';
v_sql_orddet := v_sql_orddet ||	' SELECT  distinct od.order_guid ';
v_sql_orddet := v_sql_orddet || ' FROM    clean.csr_viewed_entities v, clean.order_details od ';
v_sql_orddet := v_sql_orddet ||	' WHERE   v.csr_id = '''||in_csr_id||''' ';
v_sql_orddet := v_sql_orddet ||	' AND     v.entity_type = ''ORDER_DETAILS'' ';
v_sql_orddet := v_sql_orddet ||	' AND     to_char(od.order_detail_id) = v.entity_id ' ;

IF in_entity_type = 'ORDERS' THEN
	v_sql_add :=   ' UNION '||
		     ' SELECT  o.order_guid '||
		     ' FROM    clean.orders o '||
		     ' WHERE   o.order_guid = '''||in_entity_id||''' )';

ELSE
	IF in_entity_type = 'ORDER_DETAILS' THEN
		v_sql_add := ' UNION '||
			   ' SELECT  od.order_guid '||
			   ' FROM    clean.order_details od ' ||
			   ' WHERE   to_char(od.order_detail_id) = '''||in_entity_id||''' )';
	ELSE
		v_sql_add := ' ) ';
	END IF;
END IF;

v_sql := v_sql_start || v_sql_orders || v_sql_orddet || v_sql_add;

OPEN sql_cur for v_sql;
FETCH sql_cur INTO v_total_viewed;
CLOSE sql_cur;

IF v_exists = 'Y' THEN
        -- update the timestamp if the csr has previously viewed the record
        UPDATE  csr_viewed_entities
        SET     updated_on = sysdate
        WHERE   csr_id = in_csr_id
        AND     entity_type = in_entity_type
        AND     entity_id = in_entity_id;

ELSE
        IF v_total_viewed > v_max_entities THEN
                -- update the oldest viewed record with the new viewed information
                UPDATE  csr_viewed_entities
                set     entity_type = in_entity_type,
                        entity_id = in_entity_id,
                        updated_on = sysdate
                WHERE   csr_id = in_csr_id
                AND     updated_on =
                        (
                                SELECT  min (updated_on)
                                FROM    csr_viewed_entities
                                WHERE   csr_id = in_csr_id
                        )
                AND     rownum = 1;

        ELSE
                -- add the new viewed information
                INSERT INTO csr_viewed_entities
                (
                        csr_id,
                        entity_type,
                        entity_id,
                        updated_on
                )
                VALUES
                (
                        in_csr_id,
                        in_entity_type,
                        in_entity_id,
                        sysdate
                );
        END IF;
END IF;

commit;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in UPDATE_CSR_VIEWED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;

END UPDATE_CSR_VIEWED;


PROCEDURE UPDATE_CSR_VIEWING
(
IN_SESSION_ID                   IN VARCHAR2,
IN_CSR_ID                       IN VARCHAR2,
IN_ENTITY_TYPE                  IN VARCHAR2,
IN_ENTITY_ID                    IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieve the csrs currently
        viewing an entity

Input:
        session_id                      varchar2
        csr_id                          varchar2
        entity_type                     varchar2
        entity_id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_customer_id           csr_viewing_entities.customer_id%type;
v_order_guid            csr_viewing_entities.order_guid%type;
v_order_detail_id       csr_viewing_entities.order_detail_id%type;
v_check                 number;

-- get the customer of the shopping cart
CURSOR cust_cur IS
        SELECT  customer_id
        FROM    orders
        WHERE   order_guid = in_entity_id;

-- get the cart and customer of the order
CURSOR order_cur IS
        SELECT  o.customer_id,
                o.order_guid
        FROM    orders o
        JOIN    order_details od
        ON      o.order_guid = od.order_guid
        WHERE   od.order_detail_id = to_number (in_entity_id);

-- check if the session record exists
CURSOR check_cur IS
        SELECT  count (1)
        FROM    csr_viewing_entities
        WHERE   session_id = in_session_id;
BEGIN

out_status := 'Y';

CASE in_entity_type
WHEN 'CUSTOMER' THEN
        --
        v_customer_id := to_number (in_entity_id);
        v_order_guid := null;
        v_order_detail_id := null;

WHEN 'ORDERS' THEN
        OPEN cust_cur;
        FETCH cust_cur INTO v_customer_id;

        IF cust_cur%notfound THEN
                out_status := 'N';
                out_message := 'Order not found';
        END IF;

        CLOSE cust_cur;

        v_order_guid := in_entity_id;
        v_order_detail_id := null;

WHEN 'ORDER_DETAILS' THEN
        OPEN order_cur;
        FETCH order_cur INTO v_customer_id, v_order_guid;

        IF order_cur%notfound THEN
                out_status := 'N';
                out_message := 'Order not found';
        END IF;

        CLOSE order_cur;

        v_order_detail_id := to_number (in_entity_id);
ELSE
        out_status := 'N';
        out_message := 'Invalid type';

END CASE;

IF out_status = 'Y' THEN
        OPEN check_cur;
        FETCH check_cur INTO v_check;
        CLOSE check_cur;

        IF v_check > 0 THEN
                -- update the session info if it exists
                UPDATE  csr_viewing_entities
                SET     csr_id = in_csr_id,
                        customer_id = v_customer_id,
                        order_guid = v_order_guid,
                        order_detail_id = v_order_detail_id,
                        updated_on = sysdate
                WHERE   session_id = in_session_id;
        ELSE

                -- add the session info
                INSERT INTO csr_viewing_entities
                (
                        session_id,
                        csr_id,
                        customer_id,
                        order_guid,
                        order_detail_id,
                        updated_on
                )
                VALUES
                (
                        in_session_id,
                        in_csr_id,
                        v_customer_id,
                        v_order_guid,
                        v_order_detail_id,
                        sysdate
                );
        END IF;

        commit;
ELSE
        rollback;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CSR_VIEWING [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;

END UPDATE_CSR_VIEWING;


PROCEDURE DELETE_CSR_VIEWING
(
IN_SESSION_ID                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the csrs currently
        viewing record

Input:
        session_id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

DELETE FROM csr_viewing_entities
WHERE   session_id = in_session_id;

COMMIT;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CSR_VIEWING [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_CSR_VIEWING;


PROCEDURE DELETE_CSR_VIEWING_EXP_SESSION
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the csrs currently
        viewing record for any expired session

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM csr_viewing_entities v
WHERE   EXISTS
(
        SELECT  1
        FROM    aas.sessions s
        WHERE   v.session_id = s.session_id
        AND     s.timeout_date < sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_CSR_VIEWING_EXP_SESSION [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CSR_VIEWING_EXP_SESSION;



PROCEDURE GET_CSR_VIEWING
(
IN_SESSION_ID                   IN VARCHAR2,
IN_CSR_ID                       IN VARCHAR2,
IN_ENTITY_TYPE                  IN VARCHAR2,
IN_ENTITY_ID                    IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the viewing entities.

Input:
        session_d                       varchar2
        csr_id                          varchar2
        entity_type                     varchar2
        entity_id                       varchar2

Output:
        cursor containing csr_id ids that are currently viewing an entity

-----------------------------------------------------------------------------*/

v_out_status            char(1);
v_out_message           varchar2(300);

BEGIN

-- update the csr viewing data with the current entity info
update_csr_viewing
(
        in_session_id,
        in_csr_id,
        in_entity_type,
        in_entity_id,
        v_out_status,
        v_out_message
);

IF v_out_status = 'Y' THEN
        -- update the csr viewed data with the current entity info
        update_csr_viewed
        (
                in_csr_id,
                in_entity_type,
                in_entity_id,
                v_out_status,
                v_out_message
        );
END IF;

IF v_out_status = 'Y' THEN

        CASE in_entity_type
        WHEN 'CUSTOMER' THEN
                -- return all viewing the customer
                OPEN OUT_CUR FOR
                        SELECT  distinct
                                csr_id
                        FROM    csr_viewing_entities
                        WHERE   customer_id = to_number (in_entity_id)
                        AND     order_guid is null
                        AND     csr_id <> in_csr_id;

        WHEN 'ORDERS' THEN
                -- return all viewing the shopping cart and all orders within the cart
                OPEN OUT_CUR FOR
                        SELECT  distinct
                                csr_id
                        FROM    csr_viewing_entities
                        WHERE   order_guid = in_entity_id
                        AND     csr_id <> in_csr_id;

        WHEN 'ORDER_DETAILS' THEN
                -- return all viewing the shopping cart of the order and the order itself
                OPEN OUT_CUR FOR
                        SELECT  distinct
                                csr_id
                        FROM    csr_viewing_entities
                        WHERE   order_guid =
                                (
                                        select  min (order_guid)
                                        from    csr_viewing_entities
                                        where   order_detail_id = to_number (in_entity_id)
                                )
                        AND     (order_detail_id is null OR order_detail_id = to_number (in_entity_id))
                        AND     csr_id <> in_csr_id;


        ELSE
                -- return error message for an invalid entity type
                OPEN OUT_CUR FOR
                        SELECT  'Invalid type' error_mesg
                        FROM    dual;
        END CASE;
ELSE

        -- return error message for a failed update
        OPEN OUT_CUR FOR
                SELECT  v_out_message error_mesg
                FROM    dual;
END IF;

END GET_CSR_VIEWING;



PROCEDURE GET_CSR_VIEWED
(
IN_CSR_ID                       IN VARCHAR2,
IN_REC_COUNT                    IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the viewed entities.

Input:
        csr_id                          varchar2
        record count                    number - number of records to return

Output:
        cursor containing all records from csr_viewed_entities

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  a.*
        FROM
        (
                SELECT  c.last_name,
                        c.first_name,
                        null order_number,
                        to_char ((select max (o1.order_date) from orders o1 where o1.customer_id = c.customer_id), 'mm/dd/yyyy') last_order_date,
                        null scrub_status,
                        max (v.updated_on) updated_on,
                        c.customer_id,
                        null order_guid,
                        null order_detail_id,
                        customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
                	customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
                	customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
                	customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
                	customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
                	customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer
                FROM    csr_viewed_entities v
                JOIN    orders o
                ON      (v.entity_type = 'ORDERS' AND o.order_guid = v.entity_id)
                OR      (v.entity_type = 'ORDER_DETAILS' AND o.order_guid = (select od.order_guid from order_details od where to_char (od.order_detail_id) = v.entity_id))
                JOIN    customer c
                ON      o.customer_id = c.customer_id
                WHERE   v.csr_id = IN_CSR_ID
                GROUP BY c.last_name, c.first_name, c.customer_id
                ORDER BY updated_on DESC
        ) a
        WHERE   rownum <= in_rec_count;


END GET_CSR_VIEWED;



PROCEDURE CHECK_CSR_LOCKED_ENTITIES
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_LOCKED_IND                  OUT VARCHAR2,
OUT_LOCKED_CSR_ID               OUT CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_UPDATE_IND                  OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking the lock on the given
        record for the csr.  The entity timeout is retrieved from
        frp.global_parms.  If the entity timeout is not defined, the default
        entity timeout is 15 minutes.

Input:
        entity_type                     varchar2
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2

Output:
        cursor containing all records from csr_locked_entities

-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT
                l.session_id,
                l.csr_id,
                l.locked_on,
                l.locked_on + coalesce (to_number (frp.misc_pkg.get_global_parm_value ('Entity Lock Timeout', in_entity_type)), 15)/1440 entity_timeout,
                decode (l.csr_id, 'SYS', sysdate+1, s.timeout_date) timeout_date     -- if the locked csr is SYS (background process), there will be no valid session in AAS, so set the session timeout til tomorrow
        FROM    csr_locked_entities l
        LEFT OUTER JOIN aas.sessions s
        ON      l.session_id = s.session_id
        WHERE   l.entity_type = in_entity_type
        AND     l.entity_id = in_entity_id;

v_session_id    CSR_LOCKED_ENTITIES.SESSION_ID%TYPE;
v_csr_id        CSR_LOCKED_ENTITIES.CSR_ID%TYPE;
v_locked_on     CSR_LOCKED_ENTITIES.LOCKED_ON%TYPE;
v_timeout_date  AAS.SESSIONS.TIMEOUT_DATE%TYPE;

v_today         date := sysdate;
v_entity_timeout  date;


BEGIN

OPEN check_cur;
FETCH check_cur INTO
        v_session_id,
        v_csr_id,
        v_locked_on,
        v_entity_timeout,
        v_timeout_date;
CLOSE check_cur;

CASE
-- no locked record found is not locked
WHEN    v_session_id is null THEN
        -- not locked
        out_locked_ind := 'N';
        out_update_ind := 'N';

-- different user with a valid session and valid entity lock time is locked cannot update
WHEN    v_csr_id <> in_csr_id AND
        v_timeout_date > v_today AND
        v_entity_timeout > v_today THEN
        -- locked
        out_locked_ind := 'Y';
        out_locked_csr_id := v_csr_id;
        out_update_ind := 'N';

-- different user with an invalid session or invalid entity lock time is not locked
WHEN    v_csr_id <> in_csr_id AND
        ((v_timeout_date <= v_today OR
          v_timeout_date is null) OR
         v_entity_timeout <= v_today) THEN
        -- not locked
        out_locked_ind := 'N';
        out_update_ind := 'Y';

-- same user with a different valid session is locked but can be updated
-- 4/11/2005 - per discussion with Munter, Lazuk and Layne
-- same user with different valid session will be considered locked;
-- if a user closes a browser with a current lock, the record will
-- need to be unlocked by the lock maintenance page or wait for the
-- page lock to timeout.  If there is a system error, there will be
-- a button on the error page that will allow the user to keep the
-- same session.
WHEN    v_csr_id = in_csr_id AND
        v_session_id <> in_session_id AND
        v_timeout_date > v_today AND
        v_entity_timeout > v_today THEN
        -- locked;  previously lock can be overriden
        out_locked_ind := 'Y';
        out_locked_csr_id := v_csr_id;
        out_update_ind := 'N';  -- previously 'Y'

-- same user with a different invalid session or invalid lock time is not locked
WHEN    v_csr_id = in_csr_id AND
        v_session_id <> in_session_id AND
        ((v_timeout_date <= v_today OR
          v_timeout_date is null) OR
         v_entity_timeout <= v_today) THEN
        -- not locked
        out_locked_ind := 'N';
        out_update_ind := 'Y';

WHEN    v_csr_id = in_csr_id AND
        v_session_id = in_session_id THEN
        out_locked_ind := 'N';
        out_update_ind := 'Y';


END CASE;

END CHECK_CSR_LOCKED_ENTITIES;


PROCEDURE CHECK_CSR_LOCKED_ENTITIES
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
IN_ORDER_LEVEL                  IN VARCHAR2,
OUT_LOCKED_IND                  OUT VARCHAR2,
OUT_LOCKED_CSR_ID               OUT CSR_LOCKED_ENTITIES.CSR_ID%TYPE
)

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking the lock on the given
        record for the csr

Input:
        entity_type                     varchar2
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2
        order level                     varchar2 (ORDERS or ORDER_DETAILS for PAYMENT entity type)

Output:
        locked_ind                      varchar2 (Y - locked; N - not locked)
        locked_csr                      varchar2 (csr id who has the current lock)

Note: This procedure overload the one above.  The update indicator is not
returned (this indicator is used to flag if a record exists to be updated,
otherwise a record is inserted when the lock is created)
-----------------------------------------------------------------------------*/

-- table to hold any distinct locking csr ids
TYPE var_typ IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
csr_tab         var_typ;
v_csr_id        varchar2(100);

CURSOR order_guid_cur (p_order_guid varchar2) IS
        SELECT  order_detail_id
        FROM    order_details
        WHERE   order_guid = p_order_guid;

CURSOR order_detail_cur (p_order_detail_id number) IS
        SELECT  order_guid,
                recipient_id
        FROM    order_details
        WHERE   order_detail_id = p_order_detail_id;

v_order_guid            order_details.order_guid%type;
v_order_detail_id       order_details.order_detail_id%type;
v_recipient_id          order_details.recipient_id%type;

v_customer_entity       varchar2(35) := 'CUSTOMER';
v_order_detail_entity   varchar2(35) := 'ORDER_DETAILS';
v_payment_entity        varchar2(35) := 'PAYMENTS';
v_comm_entity           varchar2(35) := 'COMMUNICATION';

v_locked_ind            char(1);
v_locked_csr_id         varchar2(1000);
v_update_ind            char(1);

BEGIN

out_locked_ind := 'N';

CASE in_entity_type
WHEN 'MODIFY_ORDER' THEN
        OPEN order_detail_cur (to_number (in_entity_id));
        FETCH order_detail_cur INTO v_order_guid, v_recipient_id;
        CLOSE order_detail_cur;

        -- check if the order is locked
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_order_detail_entity,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- check if the recipient of the order is locked
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_customer_entity,
                IN_ENTITY_ID=>to_char (v_recipient_id),
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- check if any payment locks exist for the item
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_payment_entity,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- check if any payment locks exist for the cart
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_payment_entity,
                IN_ENTITY_ID=>v_order_guid,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );


        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- check if any communcation locks exist for the order
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_comm_entity,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- get the locking csr ids and create the output message
        v_csr_id := csr_tab.FIRST;
        WHILE v_csr_id IS NOT NULL LOOP
                out_locked_csr_id := out_locked_csr_id || csr_tab (v_csr_id) || ', ';
                v_csr_id := csr_tab.next (v_csr_id);
        END LOOP;

        IF out_locked_csr_id IS NOT NULL THEN
                out_locked_ind := 'Y';
                out_locked_csr_id := substr (out_locked_csr_id, 1, length (out_locked_csr_id) - 2);
        END IF;

WHEN 'PAYMENTS' THEN
        -- check if the given entity type and id is locked
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>in_entity_id,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>v_locked_ind,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

        IF v_locked_ind = 'Y' THEN
                csr_tab (v_locked_csr_id) := v_locked_csr_id;
        END IF;

        -- check for cart level lock or order leve locks depending on the given order level
        CASE in_order_level
        WHEN 'ORDERS' THEN

                FOR rec IN order_guid_cur (in_entity_id) LOOP
                        -- check if there are any item level payment locks
                        CHECK_CSR_LOCKED_ENTITIES
                        (
                                IN_ENTITY_TYPE=>v_payment_entity,
                                IN_ENTITY_ID=>rec.order_detail_id,
                                IN_SESSION_ID=>in_session_id,
                                IN_CSR_ID=>in_csr_id,
                                OUT_LOCKED_IND=>v_locked_ind,
                                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                                OUT_UPDATE_IND=>v_update_ind
                        );

                        IF v_locked_ind = 'Y' THEN
                                csr_tab (v_locked_csr_id) := v_locked_csr_id;
                        END IF;

                        -- check if any locks exists for the order
                        CHECK_CSR_LOCKED_ENTITIES
                        (
                                IN_ENTITY_TYPE=>v_order_detail_entity,
                                IN_ENTITY_ID=>rec.order_detail_id,
                                IN_SESSION_ID=>in_session_id,
                                IN_CSR_ID=>in_csr_id,
                                OUT_LOCKED_IND=>v_locked_ind,
                                OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                                OUT_UPDATE_IND=>v_update_ind
                        );

                        IF v_locked_ind = 'Y' THEN
                                csr_tab (v_locked_csr_id) := v_locked_csr_id;
                        END IF;
                END LOOP;

        WHEN 'ORDER_DETAILS' THEN
                OPEN order_detail_cur (to_number (in_entity_id));
                FETCH order_detail_cur INTO v_order_guid, v_recipient_id;
                CLOSE order_detail_cur;

                -- check if there are any cart level payment locks
                CHECK_CSR_LOCKED_ENTITIES
                (
                        IN_ENTITY_TYPE=>v_payment_entity,
                        IN_ENTITY_ID=>v_order_guid,
                        IN_SESSION_ID=>in_session_id,
                        IN_CSR_ID=>in_csr_id,
                        OUT_LOCKED_IND=>v_locked_ind,
                        OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                        OUT_UPDATE_IND=>v_update_ind
                );

                IF v_locked_ind = 'Y' THEN
                        csr_tab (v_locked_csr_id) := v_locked_csr_id;
                END IF;

                -- check if any locks exists for the order
                CHECK_CSR_LOCKED_ENTITIES
                (
                        IN_ENTITY_TYPE=>v_order_detail_entity,
                        IN_ENTITY_ID=>in_entity_id,
                        IN_SESSION_ID=>in_session_id,
                        IN_CSR_ID=>in_csr_id,
                        OUT_LOCKED_IND=>v_locked_ind,
                        OUT_LOCKED_CSR_ID=>v_locked_csr_id,
                        OUT_UPDATE_IND=>v_update_ind
                );

                IF v_locked_ind = 'Y' THEN
                        csr_tab (v_locked_csr_id) := v_locked_csr_id;
                END IF;
        END CASE;

        -- get the locking csr ids and create the output message
        v_csr_id := csr_tab.FIRST;
        WHILE v_csr_id IS NOT NULL LOOP
                out_locked_csr_id := out_locked_csr_id || csr_tab (v_csr_id) || ', ';
                v_csr_id := csr_tab.next (v_csr_id);
        END LOOP;

        IF out_locked_csr_id IS NOT NULL THEN
                out_locked_ind := 'Y';
                out_locked_csr_id := substr (out_locked_csr_id, 1, length (out_locked_csr_id) - 2);
        END IF;


WHEN 'COMMUNICATION' THEN
        -- only check if any locks exists for the order,
        -- there is no locking when 2 csrs are in the communcation screen for the same order
        -- locking is in place between modify order and communcation screen for the order
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>v_order_detail_entity,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>out_locked_ind,
                OUT_LOCKED_CSR_ID=>out_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

ELSE
        -- check if the given entity type and id is locked
        CHECK_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>in_entity_type,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_LOCKED_IND=>out_locked_ind,
                OUT_LOCKED_CSR_ID=>out_locked_csr_id,
                OUT_UPDATE_IND=>v_update_ind
        );

END CASE;

END CHECK_CSR_LOCKED_ENTITIES;


PROCEDURE UPDATE_CSR_LOCKED_ENTITIES
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for setting or updating the lock
        on the given entity.

Input:
        entity_type                     varchar2
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_locked_ind            char(1);
v_locked_csr_id         CSR_LOCKED_ENTITIES.CSR_ID%TYPE;
v_update_ind            char(1);
v_today                 date := sysdate;

BEGIN

CHECK_CSR_LOCKED_ENTITIES
(
        IN_ENTITY_TYPE=>in_entity_type,
        IN_ENTITY_ID=>in_entity_id,
        IN_SESSION_ID=>in_session_id,
        IN_CSR_ID=>in_csr_id,
        OUT_LOCKED_IND=>v_locked_ind,
        OUT_LOCKED_CSR_ID=>v_locked_csr_id,
        OUT_UPDATE_IND=>v_update_ind
);

out_status := 'Y';

IF (v_locked_ind = 'N' AND v_update_ind = 'Y') OR
   (v_locked_ind = 'Y' AND v_update_ind = 'Y') THEN
        UPDATE  csr_locked_entities
        SET
                session_id = in_session_id,
                csr_id = in_csr_id,
                locked_on = v_today
        WHERE   entity_type = in_entity_type
        AND     entity_id = in_entity_id;

ELSIF v_locked_ind = 'N' AND v_update_ind = 'N' THEN
        INSERT INTO csr_locked_entities
        (
                entity_type,
                entity_id,
                session_id,
                csr_id,
                locked_on
        )
        VALUES
        (
                in_entity_type,
                in_entity_id,
                in_session_id,
                in_csr_id,
                v_today
        );

ELSIF v_locked_ind = 'Y' AND v_update_ind = 'N' THEN
        out_status := 'N';
        out_message := v_locked_csr_id;
END IF;

IF out_status = 'Y' THEN
        commit;
ELSE
        rollback;
END IF;

EXCEPTION 
     WHEN DUP_VAL_ON_INDEX THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUP_VAL_ON_INDEX OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;

     WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;

END UPDATE_CSR_LOCKED_ENTITIES;


PROCEDURE CHECK_AND_DELETE_LOCK
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing the lock on the given
        entity

Input:
        entity_type                     varchar2
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_locked_ind            char(1);
v_locked_csr_id         CSR_LOCKED_ENTITIES.CSR_ID%TYPE;
v_update_ind            char(1);
v_today                 date := sysdate;

BEGIN

CHECK_CSR_LOCKED_ENTITIES
(
        IN_ENTITY_TYPE=>in_entity_type,
        IN_ENTITY_ID=>in_entity_id,
        IN_SESSION_ID=>in_session_id,
        IN_CSR_ID=>in_csr_id,
        OUT_LOCKED_IND=>v_locked_ind,
        OUT_LOCKED_CSR_ID=>v_locked_csr_id,
        OUT_UPDATE_IND=>v_update_ind
);

out_status := 'Y';

IF (v_locked_ind = 'N' AND v_update_ind = 'Y') OR
   (v_locked_ind = 'Y' AND v_update_ind = 'Y') THEN
        DELETE FROM csr_locked_entities
        WHERE   entity_type = in_entity_type
        AND     entity_id = in_entity_id;

ELSIF v_locked_ind = 'Y' AND v_update_ind = 'N' THEN
        out_status := 'N';
        out_message := 'Record locked by ' || v_locked_csr_id;
END IF;

IF out_status = 'Y' THEN
        commit;
ELSE
        rollback;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;

END CHECK_AND_DELETE_LOCK;


PROCEDURE DELETE_CSR_LOCKED_ENTITIES
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for handling the logic for removing
        the entity locks. Business logic is handled for Modify Order and
        Communications.

Input:
        entity_type                     varchar2
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_detail_cur (p_order_detail_id number) IS
        SELECT  recipient_id
        FROM    order_details
        WHERE   order_detail_id = p_order_detail_id;

v_recipient_id          order_details.recipient_id%type;

v_customer_entity       varchar2(35) := 'CUSTOMER';
v_order_detail_entity   varchar2(35) := 'ORDER_DETAILS';

BEGIN

CASE in_entity_type
WHEN 'MODIFY_ORDER' THEN
        OPEN order_detail_cur (to_number (in_entity_id));
        FETCH order_detail_cur INTO v_recipient_id;
        CLOSE order_detail_cur;

        -- unlock the recipient
        CHECK_AND_DELETE_LOCK
        (
                IN_ENTITY_TYPE=>v_customer_entity,
                IN_ENTITY_ID=>v_recipient_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        IF out_status = 'Y' THEN
                -- unlock the order
                CHECK_AND_DELETE_LOCK
                (
                        IN_ENTITY_TYPE=>v_order_detail_entity,
                        IN_ENTITY_ID=>in_entity_id,
                        IN_SESSION_ID=>in_session_id,
                        IN_CSR_ID=>in_csr_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END IF;

WHEN 'COMMUNICATION' THEN
        -- unlock the order
        CHECK_AND_DELETE_LOCK
        (
                IN_ENTITY_TYPE=>in_entity_type,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        -- if the lock cannot be deleted because another csr has the lock, then
        -- ingore the lock error and return successful results
        -- there is no locking when 2 csrs are in the communcation screen for the same order
        -- locking is in place between modify order and communcation screen for the order
        IF out_status = 'N' and substr (out_message, 1, 13) = 'Record locked' THEN
                out_status := 'Y';
                out_message := null;
        END IF;

ELSE
        CHECK_AND_DELETE_LOCK
        (
                IN_ENTITY_TYPE=>in_entity_type,
                IN_ENTITY_ID=>in_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
END CASE;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CSR_LOCKED_ENTITIES;


FUNCTION GET_ORDER_LAST_TOUCHED
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN DATE
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning when an order was last touched

Input:
        order_detail_id                 number

Output:
        last touched date               date

-----------------------------------------------------------------------------*/

v_last_touched  date;

CURSOR last_cur IS
        SELECT  max (updated_on)
        FROM    csr_viewed_entities
        WHERE   entity_type = 'ORDER_DETAILS'
        AND     entity_id = to_char (in_order_detail_id);

BEGIN

OPEN last_cur;
FETCH last_cur INTO v_last_touched;
CLOSE last_cur;

RETURN v_last_touched;

END GET_ORDER_LAST_TOUCHED;


PROCEDURE UPDATE_CSR_LOCKED_ENTITIES
(
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
IN_ORDER_LEVEL                  IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2,
OUT_LOCK_OBTAINED               OUT VARCHAR2,
OUT_LOCKED_CSR_ID               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is is the overall locking procedure which is responsible
        locking logic for Modify Order, Payments and Communication pages.

Input:
        entity_type                     varchar2 (MODIFY_ORDER, PAYMENT, COMMUNICATION, etal.)
        entity_id                       varchar2
        session_id                      varchar2
        csr_id                          varchar2
        order level                     varchar2 (ORDERS or ORDER_DETAILS for PAYMENT entity type)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (db error message)
        locked obtained                 varchar2 (Y or N - if the lock was obtained)
        locked csr id                   varchar2 (comma delimited list of csr who are holding requested lock)

Notes:
entity type: MODIFY_ORDER
entity id: order detail id value
logic:
        1) check communication locks using the given order detail id
        2) check for cart level payment locks using the order guid of the order detail id
        3) check for item level payment of the given order detail id
        4) lock the recipient of the order detail id (type CUSTOMER)
        5) lock the order detail id (type ORDER_DETAILS)


entity type: PAYMENTS
entity id: order guid value (cart level payment lock) or order detail id value (item level payment lock)
order level: ORDERS (cart level payment lock) or ORDER_DETAILS (item leve payment lock)
logic:
        1) if cart level payment lock requested:
                a) check for item level payment locks for all order details ids of the given order guid
                b) check for order locks for all order detail ids of the given order guid
        2) if item level payment lock requested:
                a) check for a cart level payment lock for the order guid of the given order detail id
                b) check for order lock of the given order detail id
        3) lock the payment entity (type PAYMENTS)

entity type: COMMUNICATION
entity id: order detail id value
logic:
        1) check for order locks for the given order detail ids
        2) lock the communcation entity (type COMMUNCATION)

-----------------------------------------------------------------------------*/

CURSOR order_guid_cur (p_order_guid varchar2) IS
        SELECT  order_detail_id
        FROM    order_details
        WHERE   order_guid = p_order_guid;

CURSOR order_detail_cur (p_order_detail_id number) IS
        SELECT  recipient_id
        FROM    order_details
        WHERE   order_detail_id = p_order_detail_id;

v_order_guid            order_details.order_guid%type;
v_order_detail_id       order_details.order_detail_id%type;
v_recipient_id          order_details.recipient_id%type;

v_customer_entity       varchar2(35) := 'CUSTOMER';
v_order_detail_entity   varchar2(35) := 'ORDER_DETAILS';
v_payment_entity        varchar2(35) := 'PAYMENTS';
v_comm_entity           varchar2(35) := 'COMMUNICATION';

v_locked_ind            char(1) := 'N';
v_locked_csr_id         varchar2(100);
v_update_ind            char(1);

BEGIN

out_lock_obtained := 'N';

-- check if any modify order locks exist for the order
CHECK_CSR_LOCKED_ENTITIES
(
        IN_ENTITY_TYPE=>in_entity_type,
        IN_ENTITY_ID=>in_entity_id,
        IN_SESSION_ID=>in_session_id,
        IN_CSR_ID=>in_csr_id,
        IN_ORDER_LEVEL=>in_order_level,
        OUT_LOCKED_IND=>v_locked_ind,
        OUT_LOCKED_CSR_ID=>out_locked_csr_id
);

IF v_locked_ind = 'N' THEN
        CASE in_entity_type
        WHEN 'MODIFY_ORDER' THEN
                OPEN order_detail_cur (to_number (in_entity_id));
                FETCH order_detail_cur INTO v_recipient_id;
                CLOSE order_detail_cur;

                -- lock the recipient of the order
                UPDATE_CSR_LOCKED_ENTITIES
                (
                        IN_ENTITY_TYPE=>v_customer_entity,
                        IN_ENTITY_ID=>to_char (v_recipient_id),
                        IN_SESSION_ID=>in_session_id,
                        IN_CSR_ID=>in_csr_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );

                IF out_status = 'Y' THEN
                        -- lock the order
                        UPDATE_CSR_LOCKED_ENTITIES
                        (
                                IN_ENTITY_TYPE=>v_order_detail_entity,
                                IN_ENTITY_ID=>in_entity_id,
                                IN_SESSION_ID=>in_session_id,
                                IN_CSR_ID=>in_csr_id,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                END IF;

        ELSE
                UPDATE_CSR_LOCKED_ENTITIES
                (
                        IN_ENTITY_TYPE=>in_entity_type,
                        IN_ENTITY_ID=>in_entity_id,
                        IN_SESSION_ID=>in_session_id,
                        IN_CSR_ID=>in_csr_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );

        END CASE;

        IF out_status = 'Y' THEN
                out_lock_obtained := 'Y';
        ELSE
                -- if the out_status is N and the message does not contain a database error
                -- set the out parameters to state update was successful but the lock was not
                -- obtained
                IF substr (out_message, 1, 5) <> 'ERROR' THEN
                        out_lock_obtained := 'N';
                        out_locked_csr_id := out_message;
                        out_status := 'Y';
                        out_message := null;
                END IF;
        END IF;

ELSE
        out_status := 'Y';

END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CSR_LOCKED_ENTITIES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CSR_LOCKED_ENTITIES;

PROCEDURE GET_CSR_LOCKED_ENTITIES
(
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the locked entities.

Input:
        csr_id                          varchar2

Output:
        cursor containing locks obtained by the csr_id

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  l.entity_type,
                l.entity_id,
                case
                        when l.entity_type = 'CUSTOMER'
                                then (select c.first_name || ' ' || c.last_name from customer c where to_char (c.customer_id) = l.entity_id)
                        when l.entity_type = 'ORDERS'
                                then (select o.master_order_number from orders o where o.order_guid = l.entity_id)
                        when l.entity_type = 'ORDER_DETAILS'
                                then (select od.external_order_number from order_details od where to_char (od.order_detail_id) = l.entity_id)
                end entity_description,
                l.locked_on
        FROM    csr_locked_entities l
        WHERE   l.csr_id = in_csr_id
        AND     l.entity_type IN ('CUSTOMER', 'ORDERS', 'ORDER_DETAILS')
        ORDER BY l.locked_on desc, l.entity_type desc;

END GET_CSR_LOCKED_ENTITIES;


PROCEDURE RELEASE_LOCKS_FOR_CSR
(
IN_CSR_ID                       IN CSR_LOCKED_ENTITIES.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure deletes all records from the lock table acquired by the
        given csr.

Input:
        csr_id                          varchar

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (db error message)


-----------------------------------------------------------------------------*/
BEGIN

DELETE FROM csr_locked_entities
WHERE   csr_id = in_csr_id
AND     ((entity_type = in_entity_type AND entity_id = in_entity_id)
OR       (in_entity_type is null and in_entity_id is null));

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CSR_LOCKED_ENTITIES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END RELEASE_LOCKS_FOR_CSR;

PROCEDURE DELETE_CSR_VIEWED
(
IN_CSR_ID                       IN VARCHAR2,
IN_ORDER_GUID					IN ORDERS.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CUSTOMER_ID		            IN ORDERS.CUSTOMER_ID%TYPE,
IN_SESSION_ID					IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the csrs current
        viewed record

Input:
        csr_id                          varchar2
        order_guid						varchar2
        order_detail_id					number
        customer_id						number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

-- variables
v_order_guid_count			int := 0;
v_order_detail_count		int := 0;
v_customer_id_count			int := 0;

BEGIN

IF in_order_guid is not null THEN 
	--delete csr viewed associated to this order guid
	select count(*) into v_order_guid_count
	  FROM    clean.csr_viewed_entities
      WHERE   entity_type = 'ORDERS'
      AND     entity_id = in_order_guid;
      
	IF  v_order_guid_count > 0 THEN

		DELETE FROM csr_viewed_entities
		WHERE   csr_id = in_csr_id
		AND     entity_id = in_order_guid
		AND     entity_type = 'ORDERS';
		
		COMMIT;
	END IF;
	
	--delete csr viewing associated to this order guid
	v_order_guid_count := 0;
	select count(*) into v_order_guid_count
	  FROM    clean.csr_viewing_entities
      WHERE   csr_id = in_csr_id
      AND     order_guid = in_order_guid
      AND     session_id = in_session_id;
      
	IF  v_order_guid_count > 0 THEN

		DELETE FROM csr_viewing_entities
		WHERE   csr_id = in_csr_id
		AND     order_guid = in_order_guid
		AND     session_id = in_session_id;
		
		COMMIT;
	END IF;

END IF;

IF in_order_detail_id is not null THEN 
	--delete csr viewed for associated order detail id
	select count(*) into v_order_detail_count
	  FROM    clean.csr_viewed_entities
      WHERE   entity_type = 'ORDER_DETAILS'
      AND     entity_id = in_order_detail_id;
      
	IF  v_order_detail_count > 0 THEN

		DELETE FROM csr_viewed_entities
		WHERE   csr_id = in_csr_id
		AND     entity_id = in_order_detail_id
		AND     entity_type = 'ORDER_DETAILS';
		
		COMMIT;
	END IF;
	
	--delete csr viewing associated to this order guid
	v_order_detail_count := 0;
	
	select count(*) into v_order_detail_count
	  FROM    clean.csr_viewing_entities
      WHERE   csr_id = in_csr_id
      AND     order_detail_id = in_order_detail_id
      AND     session_id = in_session_id;
      
	IF  v_order_detail_count > 0 THEN

		DELETE FROM csr_viewing_entities
		WHERE   csr_id = in_csr_id
        AND     order_detail_id = in_order_detail_id
        AND     session_id = in_session_id;
		
		COMMIT;
	END IF;

END IF;

IF in_customer_id is not null THEN 
	--delete csr viewed for associated customer id
	select count(*) into v_customer_id_count
	  FROM    clean.csr_viewed_entities
      WHERE   entity_type = 'CUSTOMER'
      AND     entity_id = in_customer_id;
      
	IF  v_customer_id_count > 0 THEN

		DELETE FROM csr_viewed_entities
		WHERE   csr_id = in_csr_id
		AND     entity_id = in_customer_id
		AND     entity_type = 'CUSTOMER';
		
		COMMIT;
	END IF;
	
	--delete csr viewing associated to this customer id
	v_customer_id_count := 0;
	
	select count(*) into v_customer_id_count
	  FROM    clean.csr_viewing_entities
      WHERE   csr_id = in_csr_id
      AND     customer_id = in_customer_id
      AND     session_id = in_session_id;
      
	IF  v_customer_id_count > 0 THEN

		DELETE FROM csr_viewing_entities
		WHERE   csr_id = in_csr_id
        AND     customer_id = in_customer_id
        AND     session_id = in_session_id;
		
		COMMIT;
	END IF;
	

END IF;



OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_CSR_VIEWED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_CSR_VIEWED;

END;
.
/
