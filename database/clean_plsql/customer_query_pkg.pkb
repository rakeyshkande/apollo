CREATE OR REPLACE
PACKAGE BODY clean.CUSTOMER_QUERY_PKG

AS

PROCEDURE SEARCH_CUSTOMER
(
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_PHONE_NUMBER                 IN CUSTOMER_PHONES.PHONE_NUMBER%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS.CC_NUMBER%TYPE,
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_NUMBER            IN MEMBERSHIPS.MEMBERSHIP_NUMBER%TYPE,
IN_ORDER_NUMBER                 IN VARCHAR2,
IN_TRACKING_NUMBER              IN ORDER_TRACKING.TRACKING_NUMBER%TYPE,
IN_AP_ACCOUNT                   IN AP_ACCOUNTS.AP_ACCOUNT_TXT%TYPE,
IN_BUYER_INDICATOR              IN VARCHAR2,
IN_RECIPIENT_INDICATOR          IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_PC_MEMBERSHIP                IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the results of the
        customer search.

Input:
        last_name
        first_name
        phone_number
        zip_code
        email_address
        cc_number
        customer_id
        membership_number
        order_number                    varchar - any order number
        tracking_number
        ap_account
        buyer_indicator                 varchar - search for buyers
        recipient_indicator             varchar - search for recipients
        start_position                  number - index of the start item in the id array
        max_number_returned             number - number of items in the result set

Output:
        cursor containing search results

Notes:
1. Customer and or recipient information searches (last  name, phone number, zip code)
2. Customer Number only
3. Email Address only or with any customer information
4. Credit Card Number only or with any customer information
5. Membership Number only or with any customer information
6. Order Number only or with recipient information
7. Tracking Number only
8. Aleternate Payment email address only

-----------------------------------------------------------------------------*/

v_debug                 boolean := false; -- debug mode will display the sql string
v_sql                   varchar2 (32000);

-- select clause variables
v_cust_sql              varchar2 (10000);
v_orphan_sql            varchar2 (10000);

v_cust_select           varchar2 (500) := 'select distinct c.customer_id, 1, UPPER (c.last_name), UPPER (c.first_name), decode (c.buyer_indicator, ''Y'', to_char ((select max (o.order_date) from orders o where o.customer_id = c.customer_id), ''mm/dd/yyyy''), ''Recipient'') last_order_date ';
v_orphan_select         varchar2 (500) := 'select distinct c.customer_id, 3, UPPER (c.last_name), UPPER (c.first_name), decode (c.recipient_indicator, ''Y'', ''Recipient'', ''No Orders'') last_order_date ';

-- from clause
v_from                  varchar2 (500) := 'from customer c ';

-- join clause variables
v_cust_join             varchar2 (4000);
v_orphan_join           varchar2 (4000);

-- the joins to email, credit card, and memberships will only apply to buyers
v_email_join            varchar2 (500) := 'join customer_email_ref cer on c.customer_id = cer.customer_id join email e on e.email_id = cer.email_id ';
v_email_address         varchar2 (500) := 'and lower (e.email_address) = ''' || lower (in_email_address) || ''' ';

v_credit_card_join      varchar2 (500) := 'join credit_cards cc on c.customer_id = cc.customer_id ';
v_cc_number             varchar2 (500) := 'and cc.cc_number = global.encryption.encrypt_it(''' || in_cc_number || ''',frp.misc_pkg.get_global_parm_value(''Ingrian'',''Current Key'')) ';

v_membership_join       varchar2 (500) := 'join memberships m on c.customer_id = m.customer_id ';
v_membership_number     varchar2 (500) := 'and lower (m.membership_number) = ''' || lower (in_membership_number) || ''' ';

v_ap_account_join       varchar2 (500) := 'join ap_accounts aa on c.customer_id = aa.customer_id ';
v_ap_account_number     varchar2 (500) := 'and aa.ap_account_txt = global.encryption.encrypt_it(''' || in_ap_account || ''',frp.misc_pkg.get_global_parm_value(''Ingrian'',''Current Key'')) ';

-- the join to phones can apply to buyer, recipient or orphan recipient sql
v_phone_join            varchar2 (500) := 'join customer_phones p on c.customer_id = p.customer_id ';
v_phone_number          varchar2 (500) := 'and p.phone_number = ''' || in_phone_number || ''' ';

-- the customer/order join
v_cust_order_join       varchar2 (500) := 'join orders o on c.customer_id = o.customer_id ';

-- the order/order_detail join will apply to recipient sql when a master order number is searched
-- with recipient information
v_recip_join            varchar2 (4000) := 'join order_details od on c.customer_id = od.recipient_id ' ;
v_order_od_join         varchar2 (500) := 'join orders o on o.order_guid = od.order_guid ';
--this will apply for premier circle search
v_pc_order_od_join      varchar2 (500) := 'join order_details od on o.order_guid = od.order_guid ';



-- the order tracking join will apply to the recipient sql
v_order_tracking_join   varchar2 (500) := 'join order_tracking ot on od.order_detail_id = ot.order_detail_id ';
v_tracking_number       varchar2 (500) := 'and lower (ot.tracking_number) = ''' || lower (in_tracking_number) || ''' ';

-- where clause variables
v_cust_where            varchar2 (4000) := 'where 1=1 ';
v_buyer_indicator       varchar2 (500) := 'and EXISTS (select 1 from orders o where o.customer_id = c.customer_id) ';
v_recip_indicator       varchar2 (500) := 'and EXISTS (select 1 from order_details od where od.recipient_id = c.customer_id) ';
v_both_indicator        varchar2 (500) := 'and (EXISTS (select 1 from orders o where o.customer_id = c.customer_id) or EXISTS (select 1 from order_details od where od.recipient_id = c.customer_id)) ';
v_orphan_where          varchar2 (4000) := 'where (NOT EXISTS (select 1 from orders o where o.customer_id = c.customer_id) AND NOT EXISTS (select 1 from order_details od where od.recipient_id = c.customer_id)) ';

v_customer_id           varchar2 (500) := 'and c.customer_id = ' || to_char (in_customer_id) || ' ';
v_last_name             varchar2 (500) := 'and upper (c.last_name) = ''' || upper (in_last_name) || ''' ';
v_first_name            varchar2 (500) := 'and upper (c.first_name) like ''' || upper (in_first_name) || '%'' ';
v_zip_code              varchar2 (500) := 'and c.zip_code LIKE ''' || in_zip_code || '%'' ';
v_pc_membership         varchar2 (500) := 'and od.pc_membership_id = ''' || IN_PC_MEMBERSHIP || ''' ';

-- order by variables (order by last_name, firs_name, last_order_date)
v_order_by              varchar2 (4000) := 'order by 3, 4, 5 ';

-- array for the results of the dynamic sql statement
id_tab                  id_list_pkg.number_tab_typ;
group_tab               id_list_pkg.number_tab_typ;
last_name_tab           id_list_pkg.varchar_tab_typ;
first_name_tab          id_list_pkg.varchar_tab_typ;
last_order_tab          id_list_pkg.varchar_tab_typ;

-- cursor for the dynamic sql statement
sql_cur                 types.ref_cursor;
count_cur               types.ref_cursor;

-- cursor to get the count of orders within a cart when searching only for a cart
cursor cart_order_count_cur (p_order_guid varchar2) is
        select  count (1)
        from    order_details
        where   order_guid = p_order_guid;

v_cart_order_count      number := 0;

-- order variables when finding the actual order ids
v_order_detail_id       order_details.order_detail_id%type;
v_order_guid            orders.order_guid%type;
v_external_order_number order_details.external_order_number%type;
v_master_order_number   orders.master_order_number%type;
v_order_cust_id         orders.customer_id%type;
v_order_disp_code       order_details.order_disp_code%type;
v_order_indicator       varchar2 (35);
v_order_where           varchar2 (5000);

-- flags to indicate which sql to include in the unioned result set
v_cust_sql_flag         boolean := FALSE;
v_orphan_sql_flag       boolean := FALSE;

-- variables to get the string of ids
-- could not use the id_list_pkg code because of the ordering within the unioned sql
v_begin                 number := in_start_position;
v_end                   number := in_start_position + in_max_number_returned - 1;

-- string to store the list of ids
v_cust_str_id           varchar2(32767);
v_orphan_str_id         varchar2(32767);

BEGIN

-- initialize the row count returned
out_id_count := 0;

-- build the join and where clauses based on the given search parameters
CASE

-- if the order number is specified
WHEN in_order_number is not null THEN
        v_cust_sql_flag := TRUE;

        -- find the order detail id from the given order number
        order_query_pkg.FIND_ORDER_NUMBER_INCLU_MERC (in_order_number, v_order_detail_id, v_order_guid, v_external_order_number, v_master_order_number, v_order_cust_id, v_order_disp_code, v_order_indicator);


        IF v_order_indicator = 'master_order_number' or v_order_indicator = 'az_order_number' AND
           coalesce (in_last_name, in_first_name, in_zip_code, in_phone_number) IS NULL THEN

                -- searching for a cart
                -- get the number of orders in the cart, to be used later to
                -- determine if the scrub status needs to be returned in the search result set
                open cart_order_count_cur (v_order_guid);
                fetch cart_order_count_cur into v_cart_order_count;
                close cart_order_count_cur;

                -- if the master order number is given with no recipient information
                -- get the customer who placed the order
                v_cust_join := v_cust_join || v_cust_order_join;
                v_cust_join := v_cust_join || 'and o.order_guid = ''' || v_order_guid || ''' ';

        ELSE

                -- join customer with order details to search for the recipients
                v_cust_join := v_cust_join || v_recip_join;

                -- if the order specified was at the detail level or the master order number was given
                -- with recipient information, then get the recipient id of the order
                IF v_order_indicator = 'master_order_number' or v_order_indicator = 'az_order_number' THEN
                        -- if the master order number order number was given
                        -- join the order table to join with the found order guid
                        v_cust_join := v_cust_join || v_order_od_join;
                        v_cust_join := v_cust_join || 'and o.order_guid = ''' || v_order_guid || ''' ';

                ELSE
                        -- order was not found, so the search should not return any rows
                        if v_order_detail_id is null then
                                v_order_detail_id := -1;
                        end if;

                        -- join order details with the found order detail id
                        v_cust_join := v_cust_join || 'and od.order_detail_id = ' || to_char (v_order_detail_id) || ' ';
                END IF;

        END IF;

-- if the tracking number is specified, get the recipient of the order for the tracking number
WHEN in_tracking_number is not null THEN
        v_cust_sql_flag := TRUE;

        v_order_detail_id := order_query_pkg.get_order_from_tracking (in_tracking_number);

        -- order was not found, so the search should not return any rows
        if v_order_detail_id is null then
                v_order_detail_id := -1;
        end if;

        -- join order details with the found order detail id
        v_cust_join := v_cust_join || v_recip_join || 'and od.order_detail_id = ' || to_char (v_order_detail_id) || ' ';

-- if the customer id is specified, no other criteria is required
WHEN in_customer_id is not null THEN
        v_cust_sql_flag := TRUE;
        v_cust_where := v_cust_where || v_customer_id;
        v_orphan_where := v_orphan_where || v_customer_id;

-- if the membership number is specified, only search buyers
WHEN in_membership_number is not null then
        v_cust_sql_flag := TRUE;
        v_cust_join := v_cust_join || v_membership_join || v_membership_number;
        
WHEN in_pc_membership is not null then
        v_cust_sql_flag := TRUE;
        v_cust_join := v_cust_join || v_cust_order_join || v_pc_order_od_join || v_pc_membership;
        

-- if email address is specified, only search buyers
WHEN in_email_address is not null then
        v_cust_sql_flag := TRUE;
        v_cust_join := v_cust_join || v_email_join || v_email_address;

-- if credit card number is specified, only search buyers
WHEN in_cc_number is not null then
        v_cust_sql_flag := TRUE;
        v_cust_join := v_cust_join || v_credit_card_join || v_cc_number;

-- if alternate payment email is specified, only search buyers
WHEN in_ap_account is not null then
        v_cust_sql_flag := TRUE;
        v_cust_join := v_cust_join || v_ap_account_join || v_ap_account_number;

-- if only customer or recipient info was specified, check the indicators
-- if the search should only include buyers or recipients
-- if both, no criteria is required
ELSE
        IF in_buyer_indicator = 'Y' AND in_recipient_indicator = 'Y' THEN
                v_cust_sql_flag := TRUE;
                v_cust_where := v_cust_where || v_both_indicator;

                v_orphan_sql_flag := TRUE;

                -- 10/4/05 always search for orphans
                -- v_orphan_where := v_orphan_where || v_recip_indicator;

        ELSIF in_buyer_indicator = 'Y' THEN
                v_cust_sql_flag := TRUE;
                v_cust_where := v_cust_where || v_buyer_indicator;

                -- 10/4/05 always search for orphans
                v_orphan_sql_flag := TRUE;

        ELSIF in_recipient_indicator = 'Y' THEN
                v_cust_sql_flag := TRUE;
                v_cust_where := v_cust_where || v_recip_indicator;

                -- 10/4/05 always search for orphans
                v_orphan_sql_flag := TRUE;
                -- v_orphan_where := v_orphan_where || v_recip_indicator;

        ELSE
                v_orphan_sql_flag := TRUE;
        END IF;


END CASE;

-- include customer or recipient info only if needed
-- customer id and tracking number searches do not require customer or recipient info
IF coalesce (to_char (in_customer_id), in_tracking_number) is null THEN
        -- set the phone number join if specified
        if in_phone_number is not null then
                v_cust_join := v_cust_join || v_phone_join || v_phone_number;
                v_orphan_join := v_orphan_join || v_phone_join || v_phone_number;
        end if;

        -- set the customer last name if specified
        if in_last_name is not null then
                v_cust_where := v_cust_where || v_last_name;
                v_orphan_where := v_orphan_where || v_last_name;
        end if;

        -- set the customer first name if specified
        if in_first_name is not null then
                v_cust_where := v_cust_where || v_first_name;
                v_orphan_where := v_orphan_where || v_first_name;
        end if;

        -- set the customer zip code if specified
        if in_zip_code is not null then
                v_cust_where := v_cust_where || v_zip_code;
                v_orphan_where := v_orphan_where || v_zip_code;
        end if;
END IF;

-- 08/24/06 - added code to check if all parameters not including the buyer and recipient indicators
-- are null, then force the search to bypass the query and return an empty result set.
IF coalesce (in_last_name, in_first_name, in_phone_number, in_zip_code, in_email_address, in_cc_number,
    to_char (in_customer_id), in_membership_number, in_pc_membership, in_order_number, in_tracking_number, in_ap_account) is null THEN
    	v_sql := v_orphan_select || v_from || 'where 1=0 ';

ELSE

	-- build the entire sql statement for the initial query to get the rowids
	v_cust_sql := v_cust_select || v_from || v_cust_join || v_cust_where;
	v_orphan_sql := v_orphan_select || v_from || v_orphan_join || v_orphan_where;

	-- build the union
	if v_cust_sql_flag then
		v_sql := v_cust_sql;
	end if;

	if v_orphan_sql_flag then
		if v_sql is not null then
			v_sql := v_sql || ' UNION ';
		end if;
		v_sql := v_sql || v_orphan_sql;
	end if;

	-- include order by clause
	v_sql := v_sql || v_order_by;
END IF;

-- execute the sql statement
if v_debug then
        open out_cur for select v_sql as debug_mode from dual;

else
        -- get the customer ids returned by the search and create the xml to limit the result set
        -- using the given starting index and number of records
        -- 10/30/2005 NOTE -- LIMIT WAS HARD CODED FOR PRODUCTION FIX -- THIS SHOULD BE CHANGE TO BE
        -- A GLOBAL PARM
        open sql_cur for v_sql;
        fetch sql_cur bulk collect into id_tab, group_tab, last_name_tab, first_name_tab, last_order_tab limit 200;
        close sql_cur;

        -- get the ids in a string
        -- could not use the id_list_pkg code because of the ordering within the unioned sql

        -- check if the ending index is more than what is in the array
        -- if so, set the end index to be the end of the array to prevent out of bounds exception
        if v_end >= id_tab.count or in_max_number_returned is null then
                v_end := id_tab.count;
        end if;

        -- loop through the given index range creating the string of ids
        -- check if the start index is more than what is in the array
        if id_tab.count > 0 and v_begin <= id_tab.count then
                for x in v_begin..v_end loop
                        if group_tab (x) = 1 then
                                v_cust_str_id := v_cust_str_id || id_tab(x) || ',';
                        elsif group_tab(x) = 3 then
                                v_orphan_str_id := v_orphan_str_id || id_tab(x) || ',';
                        end if;
                end loop;
        end if;

        --  remove the last comma from the id strings
        v_cust_str_id := coalesce (substr(v_cust_str_id, 1, length(v_cust_str_id) - 1), '-1');
        v_orphan_str_id := coalesce (substr(v_orphan_str_id, 1, length(v_orphan_str_id) - 1), '-1');
end if;

if not v_debug then

        if id_tab.count = 1 and (in_order_number is not null or in_tracking_number is not null) then
                out_id_count := id_tab.count;

                -- Search for order number and order was found

                -- Master order number was used for search without any recipient info
                -- so return the cart info.
                IF v_order_indicator = 'master_order_number' or v_order_indicator = 'az_order_number' AND
                   coalesce (in_last_name, in_first_name, in_zip_code, in_phone_number) IS NULL THEN

                        IF v_cart_order_count = 1 THEN
                                -- Return with scrub status and order number for a
                                -- cart with only 1 order.  If the 1 order is in scrub
                                -- the application will display the in scrub pop up.
                                open out_cur for
                                       'SELECT  c.customer_id,
                                                c.last_name,
                                                c.first_name,
                                                c.address_1 || '' '' || c.address_2 address,
                                                c.city,
                                                c.state,
                                                o.order_date last_order_date,
                                                order_query_pkg.get_scrub_status_ind (od.order_disp_code) scrub_status,
                                                od.external_order_number order_number,
                                                upper (c.last_name) upper_last_name,
                                                upper (c.first_name) upper_first_name,
                                                customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
						                		customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
						                		customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
						                		customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
						                		customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
						                		customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
						                		customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
						                		customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer,
						                		customer_query_pkg.is_usaa_recipient (od.recipient_id) usaa_recipient
                                        FROM    customer c
                                        JOIN    orders o
                                        ON      c.customer_id = o.customer_id
                                        JOIN    order_details od
                                        ON      o.order_guid = od.order_guid
                                        WHERE    o.order_guid = ''' || v_order_guid || ''' ';
                        ELSE
                                -- Multiple orders with in the cart, so
                                -- don't return the scrub status and order number.
                                -- Any orders in scrub will be displayed with the scrub icon
                                -- in the shopping cart page.
                                open out_cur for
                                       'SELECT  c.customer_id,
                                                c.last_name,
                                                c.first_name,
                                                c.address_1 || '' '' || c.address_2 address,
                                                c.city,
                                                c.state,
                                                o.order_date last_order_date,
                                                null scrub_status,
                                                null order_number,
                                                upper (c.last_name) upper_last_name,
                                                upper (c.first_name) upper_first_name,
                                                customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
						                		customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
						                		customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
						                		customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
						                		customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
						                		customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
						                		customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
						                		customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer,
						                		customer_query_pkg.is_usaa_recipient (c.customer_id) usaa_recipient
                                        FROM    customer c
                                        JOIN    orders o
                                        ON      c.customer_id = o.customer_id
                                        WHERE   o.order_guid = ''' || v_order_guid || ''' ';
                        END IF;

                ELSE
                        IF v_order_indicator = 'master_order_number' or v_order_indicator = 'az_order_number' THEN
                                -- Cart search with recipient info returns the specific order info
                                -- within the given cart.  The application will retrieve the shopping cart
                                -- and highlight the tab of the searched recipient.
                                open out_cur for
                                       'SELECT  o.customer_id,
                                                c.last_name,
                                                c.first_name,
                                                c.address_1 || '' '' || c.address_2 address,
                                                c.city,
                                                c.state,
                                                ''Recipient'' last_order_date,
                                                order_query_pkg.get_scrub_status_ind (od.order_disp_code) scrub_status,
                                                od.external_order_number order_number,
                                                upper (c.last_name) upper_last_name,
                                                upper (c.first_name) upper_first_name,
                                                customer_query_pkg.is_preferred_customer (o.customer_id) preferred_customer,
						                		customer_query_pkg.is_ftd_customer (o.customer_id) ftd_customer,
						                		customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
						                		customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
						                		customer_query_pkg.GET_PREFERRED_MASKED_NAME (o.customer_id) preferred_masked_name,
						                		customer_query_pkg.GET_PREFERRED_WARNING_MSG (o.customer_id) preferred_warning_message,
						                		customer_query_pkg.GET_PREFERRED_RESOURCE (o.customer_id) preferred_resource,
						                		customer_query_pkg.is_usaa_customer (o.customer_id) usaa_customer,
						                		customer_query_pkg.is_usaa_recipient (od.recipient_id) usaa_recipient
                                        FROM    customer c
                                        JOIN    order_details od
                                        ON      c.customer_id = od.recipient_id
                                        JOIN    orders o
                                        ON      o.order_guid = od.order_guid
                                        WHERE   o.order_guid = ''' || v_order_guid || '''
                                        AND     c.customer_id in (' || v_cust_str_id || ') ';
                        ELSE
                                -- Searched order was at the detail level
                                open out_cur for
                                       'SELECT  o.customer_id,
                                                c.last_name,
                                                c.first_name,
                                                c.address_1 || '' '' || c.address_2 address,
                                                c.city,
                                                c.state,
                                                ''Recipient'' last_order_date,
                                                order_query_pkg.get_scrub_status_ind (od.order_disp_code) scrub_status,
                                                od.external_order_number order_number,
                                                upper (c.last_name) upper_last_name,
                                                upper (c.first_name) upper_first_name,
                                                customer_query_pkg.is_preferred_customer (o.customer_id) preferred_customer,
						                		customer_query_pkg.is_ftd_customer (o.customer_id) ftd_customer,
						                		customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
						                		customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
						                		customer_query_pkg.GET_PREFERRED_MASKED_NAME (o.customer_id) preferred_masked_name,
						                		customer_query_pkg.GET_PREFERRED_WARNING_MSG (o.customer_id) preferred_warning_message,
						                		customer_query_pkg.GET_PREFERRED_RESOURCE (o.customer_id) preferred_resource,
						                		customer_query_pkg.is_usaa_customer (o.customer_id) usaa_customer,
						                		customer_query_pkg.is_usaa_recipient (od.recipient_id) usaa_recipient
                                        FROM    customer c
                                        JOIN    order_details od
                                        ON      c.customer_id = od.recipient_id
                                        JOIN    orders o
                                        ON      o.order_guid = od.order_guid
                                        WHERE   od.order_detail_id = ' || v_order_detail_id || ' ';
                        END IF;
                END IF;

        elsif id_tab.count > 0 then
                out_id_count := id_tab.count;

                -- WITH clause -
                --      Gets the number of orders for the customer id (# of carts as a buyer + # of order as a recipient)
                --      This will be used to determine if the buyer or recipient id needs to be returned
                --      as well as the order status and order number.
                -- 1st SELECT clause -
                --      If the searched customer is only associated to 1 order, then the customer id for the cart is returned, otherwise the searched customer id is returned
                --              This will allow the application to go directly to the shopping cart page
                --      If the searched customer is a buyer, the last order date is returned, otherwise 'Recipient' is returned for the last order date
                --      If the searched customer is only associated to 1 order, then the scrub status and external order number is returned for that order
                --              This will allow the application to go to the specific order if the search customer is a recipient
                -- 2nd SELECT clause -
                --      Gets any customer that is not associated to a cart or order.  This could happen in 2 cases: 1) recipient information was updated
                --              that caused a new recipient record to be created.  2) Marketing requirement of adding a customer who requested
                --              a newletter or mailling subscription.
                open out_cur for
                       'WITH oc AS (
                                select  coalesce (o.customer_id, od.customer_id) customer_id,
                                        sum (case when o.order_guid is not null and od.order_guid is null then 1
                                                  when od.order_guid is not null and o.order_guid is null then 1
                                                  when o.order_guid = od.order_guid then 1
                                                  when o.order_guid is not null and od.order_guid is not null and o.order_guid <> od.order_guid then 2
                                                  else 0
                                             end) order_count
                                from
                                        (select o.customer_id, o.order_guid from clean.orders o where o.customer_id in (' || v_cust_str_id || ')) o
                                full outer join
                                        (select od.recipient_id customer_id, od.order_guid from clean.order_details od where od.recipient_id in (' || v_cust_str_id || ')) od
                                on o.customer_id = od.customer_id
                                group by coalesce (o.customer_id, od.customer_id)
                                ) ' ||
                       'SELECT  coalesce (decode (oc.order_count, 1, (select o.customer_id from orders o join order_details od on o.order_guid = od.order_guid where od.recipient_id = c.customer_id), null), c.customer_id) customer_id,
                                c.last_name,
                                c.first_name,
                                c.address_1 || '' '' || c.address_2 address,
                                c.city,
                                c.state,
                                decode (c.buyer_indicator, ''Y'', to_char ((select max (order_date) from orders o where o.customer_id = c.customer_id), ''mm/dd/yyyy''), ''Recipient'') last_order_date,
                                decode (oc.order_count, 1, (select order_query_pkg.get_scrub_status_ind (od.order_disp_code) from order_details od where od.recipient_id = c.customer_id), null) scrub_status,
                                decode (oc.order_count, 1, (select od.external_order_number from order_details od where od.recipient_id = c.customer_id), null) order_number,
                                upper (c.last_name) upper_last_name,
                                upper (c.first_name) upper_first_name,
                                customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
                				customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
                				customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
                				customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
                				customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
                				customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
                				customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
						        customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer,
						        customer_query_pkg.is_usaa_recipient (c.customer_id) usaa_recipient						        
                        FROM    customer c
                        LEFT OUTER JOIN oc
                        ON      c.customer_id = oc.customer_id
                        WHERE   c.customer_id in (' || v_cust_str_id || ') ' ||
                       'UNION
                        SELECT  c.customer_id,
                                c.last_name,
                                c.first_name,
                                c.address_1 || '' '' || c.address_2 address,
                                c.city,
                                c.state,
                                decode (recipient_indicator, ''Y'', ''Recipient'', ''No Order'') last_order_date,
                                null scrub_status,
                                null order_number,
                                upper (c.last_name) upper_last_name,
                                upper (c.first_name) upper_first_name,
                                customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
                				customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
                				customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
                				customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
                				customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
                				customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
                				customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
						        customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer,
						        customer_query_pkg.is_usaa_recipient (c.customer_id) usaa_recipient	
                        FROM    customer c
                        WHERE   c.customer_id in (' || v_orphan_str_id || ') ' ||
                       'ORDER BY 10, 11, 7';

        else
                -- If there is no records returned in the customer search cursor, then it's possible that
                -- the csr is searching for an order in scrub by order number.
                -- See if the given order number was found.
                -- This code would happen when an order is still in scrub and the order ids
                -- were dispatched to clean.  The order detail record would not have a recipient id
                -- so the search query would return 0 records.  In this case return
                -- the scrub status and the external order number

                v_order_disp_code := order_query_pkg.get_scrub_status_ind (v_order_disp_code);
                if v_order_disp_code is not null and v_order_detail_id <> -1 and v_order_detail_id is not null then
                        out_id_count := 1;

                        open out_cur for
                                SELECT  v_order_cust_id customer_id,
                                        null last_name,
                                        null first_name,
                                        null address,
                                        null city,
                                        null state,
                                        null last_order_date,
                                        v_order_disp_code scrub_status,
                                        v_external_order_number external_order_number,
                                        null upper_last_name,
                                        null upper_first_name
                                FROM    dual;
                else
                        -- open an empty cursor so the application does not abnormally terminate
                        open out_cur for
                                SELECT  c.customer_id,
                                        c.last_name,
                                        c.first_name,
                                        c.address_1 || ' ' || c.address_2 address,
                                        c.city,
                                        c.state,
                                        null last_order_date,
                                        null scrub_status,
                                        null order_number,
                                        null upper_last_name,
                                        null upper_first_name
                                FROM    customer c
                                WHERE   c.customer_id = -1;
                end if;

        end if;

end if;

END SEARCH_CUSTOMER;



PROCEDURE GET_CUSTOMER
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer record for
        the given customer id

Input:
        customer_id                     number

Output:
        cursor containing customer info

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                c.concat_id,
                c.first_name customer_first_name,
                c.last_name customer_last_name,
                c.business_name,
                c.address_1 customer_address_1,
                c.address_2 customer_address_2,
                c.city customer_city,
                c.state customer_state,
                c.zip_code customer_zip_code,
                c.country customer_country,
                (select UPPER(cm.name) from ftd_apps.country_master cm where c.country = cm.country_id) as country_name,
                c.address_type,
                c.created_on customer_created_on,
                c.buyer_indicator,
                c.recipient_indicator,
                c.vip_customer,
                clean.comment_history_pkg.customer_has_comments (c.customer_id) customer_comment_indicator,
                c.county customer_county,
                customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
                customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
                customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
                customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
                customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
                (select description from frp.address_types at where at.address_type = c.address_type) as address_type_description,
				customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer
       	FROM    customer c
        WHERE   c.customer_id = in_customer_id;

END GET_CUSTOMER;


PROCEDURE GET_CUSTOMER_PHONES
(
IN_CUSTOMER_ID                  IN CUSTOMER_PHONES.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the phone numbers
        for the given customer id

Input:
        customer_id                     number

Output:
        cursor containing customer phone information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                phone_id customer_phone_id,
                phone_type customer_phone_type,
                phone_number customer_phone_number,
                extension customer_extension,
                phone_number_type cus_phone_number_type,
                sms_opt_in customer_sms_opt_in
        FROM    customer_phones
        WHERE   customer_id = in_customer_id
        ORDER BY phone_type;

END GET_CUSTOMER_PHONES;


PROCEDURE GET_EMAIL
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_MORE_EMAIL_IND              OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving email
        addresses for the given customer

Input:
        company_id                      varchar2

Output:
        cursor containing subscribed email addresses

-----------------------------------------------------------------------------*/
CURSOR more_cur IS
        SELECT  decode (count (1), 0, 'N', 'Y') more_email
        FROM    customer_email_ref
        WHERE   customer_id = in_customer_id
        AND     most_recent_by_company = 'N';

BEGIN

OPEN more_cur;
FETCH more_cur INTO out_more_email_ind;
CLOSE more_cur;

OPEN OUT_CUR FOR
        SELECT
                e.email_id,
                c.company_name email_company_name,
                e.email_address,
                null email_active_indicator,
                e.subscribe_status email_subscribe_status
        FROM    email e
        JOIN    customer_email_ref cer
        ON      e.email_id = cer.email_id
        JOIN    ftd_apps.company_master c
        ON      e.company_id = c.company_id
        WHERE   cer.customer_id = in_customer_id
        AND     cer.most_recent_by_company = 'Y'
        AND     c.email_direct_mail_flag = 'Y'
        ORDER BY e.email_address;

END GET_EMAIL;


PROCEDURE GET_MEMBERSHIPS
(
IN_CUSTOMER_ID                  IN MEMBERSHIPS.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving membership infomration
        for the given customer.  Only the latest membership info will
        be displayed for one given membership type.

Input:
        customer_id                     number
a
Output:
        cursor containing membership information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                membership_id,
                membership_number,
                membership_type,
                first_name membership_first_name,
                last_name membership_last_name
        FROM    memberships
        WHERE   customer_id = in_customer_id
        AND     most_recent_by_type = 'Y'
        ORDER BY membership_type;

END GET_MEMBERSHIPS;


PROCEDURE GET_DIRECT_MAIL
(
IN_CUSTOMER_ID                  IN MEMBERSHIPS.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving direct mail info
        for the given customer

Input:
        customer_id                     number

Output:
        cursor containing direct mail information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                c.company_name direct_mail_company,
                d.subscribe_status direct_mail_subscribe_status,
                c.company_id direct_mail_company_id
        FROM    ftd_apps.company_master c
        LEFT OUTER JOIN direct_mail d
        ON      d.company_id = c.company_id
        AND     d.customer_id = in_customer_id
        WHERE   c.company_id in (
            SELECT DISTINCT cm.newsletter_company_id
            FROM FTD_APPS.COMPANY_MASTER CM
            WHERE cm.email_direct_mail_flag = 'Y')
        ORDER BY d.company_id;

END GET_DIRECT_MAIL;

PROCEDURE GET_CUSTOMER_HOLD
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all hold reasons
        for the given customer

Input:
        customer_id                     number

Output:
        cursor containing hold reasons

-----------------------------------------------------------------------------*/

v_cust_cur      types.ref_cursor;
v_cc_cur        types.ref_cursor;
v_phone_cur     types.ref_cursor;
v_member_cur    types.ref_cursor;
v_email_cur     types.ref_cursor;
v_ap_cur        types.ref_cursor;

-- index by table to store cursor result sets
TYPE varchar_tab_typ IS TABLE OF VARCHAR2(200) INDEX BY PLS_INTEGER;
TYPE number_tab_typ IS TABLE OF NUMBER INDEX BY PLS_INTEGER;

-- generic variable tables to hold cursor result sets
v_num_tab1      number_tab_typ;
v_num_tab2      number_tab_typ;
v_num_tab3      number_tab_typ;
v_num_tab4      number_tab_typ;

v_char_tab1     varchar_tab_typ;
v_char_tab2     varchar_tab_typ;
v_char_tab3     varchar_tab_typ;
v_char_tab4     varchar_tab_typ;
v_char_tab5     varchar_tab_typ;
v_char_tab6     varchar_tab_typ;
v_char_tab7     varchar_tab_typ;
v_char_tab8     varchar_tab_typ;

v_reason_tab1   varchar_tab_typ;
v_reason_tab2   varchar_tab_typ;
v_reason_tab3   varchar_tab_typ;

BEGIN

EXECUTE IMMEDIATE ('TRUNCATE TABLE CUSTOMER_HOLD_TEMP');

-- get the customer hold information
GET_CUSTOMER_HOLD_CUST_INFO
(
        IN_CUSTOMER_ID=>in_customer_id,
        OUT_CURSOR_CUSTOMER=>v_cust_cur,
        OUT_CURSOR_CREDIT_CARDS=>v_cc_cur,
        OUT_CURSOR_PHONES=>v_phone_cur,
        OUT_CURSOR_MEMBERSHIPS=>v_member_cur,
        OUT_CURSOR_EMAILS=>v_email_cur,
        OUT_CURSOR_AP_ACCOUNTS=>v_ap_cur
);

FETCH v_cust_cur BULK COLLECT INTO
        v_num_tab1,
        v_reason_tab1,
        v_num_tab2,
        v_char_tab1,
        v_char_tab2,
        v_reason_tab2,
        v_num_tab3,
        v_char_tab3,
        v_char_tab4,
        v_char_tab5,
        v_char_tab6,
        v_char_tab7,
        v_char_tab8,
        v_reason_tab3,
        v_num_tab4;
CLOSE v_cust_cur;

-- store customer id hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));
-- store customer name hold reasons
FORALL x IN 1..v_reason_tab2.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab2(x));
-- store customer address hold reasons
FORALL x IN 1..v_reason_tab3.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab3(x));


FETCH v_cc_cur BULK COLLECT INTO
        v_char_tab1,
        v_reason_tab1,
        v_num_tab1,
        v_char_tab2;
CLOSE v_cc_cur;

-- store the credit card hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));

FETCH v_phone_cur BULK COLLECT INTO
        v_char_tab1,
        v_reason_tab1,
        v_num_tab1;
CLOSE v_phone_cur;

-- store the phone number hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));

FETCH v_member_cur BULK COLLECT INTO
        v_char_tab1,
        v_reason_tab1,
        v_num_tab1;
CLOSE v_member_cur;

-- store the membership hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));

FETCH v_email_cur BULK COLLECT INTO
        v_char_tab1,
        v_reason_tab1,
        v_num_tab1;
CLOSE v_email_cur;

-- store the email hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));

FETCH v_ap_cur BULK COLLECT INTO
        v_num_tab1,
        v_char_tab2,
        v_char_tab3,
        v_reason_tab1,
        v_num_tab1;
CLOSE v_ap_cur;

-- store the alternate payment hold reasons
FORALL x IN 1..v_reason_tab1.count
        INSERT INTO customer_hold_temp VALUES (in_customer_id, v_reason_tab1(x));

OPEN OUT_CUR FOR
        SELECT  distinct
                hv.description customer_hold_description
        FROM    customer_hold_temp cht
        JOIN    customer_hold_reason_val hv
        ON      cht.hold_reason_code = hv.hold_reason_code
        WHERE   cht.customer_id = in_customer_id
        ORDER BY hv.description;

END GET_CUSTOMER_HOLD;

PROCEDURE GET_CUSTOMER_ORDERS
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all order detail ids
        for the given customer

Input:
        customer_id                     number

Output:
        cursor containing order detail id

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  order_detail_id
        FROM
        (
                SELECT
                        od.order_detail_id,
                        o.order_date,
                        o.master_order_number,
                        od.external_order_number,
			(SELECT  distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                        ) partner_name,
                        (SELECT  distinct pm.PREFERRED_PROCESSING_RESOURCE FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                        ) partner_resource
                FROM    customer c
                JOIN    orders o
                ON      c.customer_id = o.customer_id
                JOIN    order_details od
                ON      o.order_guid = od.order_guid
                WHERE   c.customer_id = in_customer_id
                UNION
                SELECT
                        od.order_detail_id,
                        o.order_date,
                        o.master_order_number,
                        od.external_order_number,
			(SELECT  distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                        ) partner_name,
                        (SELECT  distinct pm.PREFERRED_PROCESSING_RESOURCE FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                        ) partner_resource
                FROM    customer c
                JOIN    order_details od
                ON      c.customer_id = od.recipient_id
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                WHERE   c.customer_id = in_customer_id
                ORDER BY order_date desc, master_order_number, external_order_number
        );

END GET_CUSTOMER_ORDERS;

PROCEDURE GET_CUSTOMER_CARTS
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
OUT_NUMBER_OF_CARTS             OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving a count of customer
        shoppping carts
Input:
        customer_id                     number

Output:
        number of shopping carts

-----------------------------------------------------------------------------*/
BEGIN

SELECT  count (1)
INTO    out_number_of_carts
FROM
(
        SELECT  o.order_guid
        FROM    orders o
        WHERE   o.customer_id = in_customer_id
        UNION
        SELECT  distinct (od.order_guid)
        FROM    order_details od
        WHERE   od.recipient_id = in_customer_id
);

END GET_CUSTOMER_CARTS;


PROCEDURE GET_CUSTOMER_ACCT
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_ORDER_NUMBER                 IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_SESSION_ID                   IN CSR_VIEWING_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_VIEWING_ENTITIES.CSR_ID%TYPE,
OUT_CUSTOMER_CUR                OUT TYPES.REF_CURSOR,
OUT_PHONE_CUR                   OUT TYPES.REF_CURSOR,
OUT_EMAIL_CUR                   OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDER_CUR                   OUT TYPES.REF_CURSOR,
OUT_NUMBER_OF_ORDERS            OUT NUMBER,
OUT_HOLD_CUR                    OUT TYPES.REF_CURSOR,
OUT_VIEWING_CUR                 OUT TYPES.REF_CURSOR,
OUT_LOCKED_INDICATOR            OUT VARCHAR2,
OUT_NUMBER_OF_CARTS             OUT NUMBER,
OUT_MORE_EMAIL_INDICATOR        OUT VARCHAR2,
OUT_DIRECT_MAIL_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer information
        to be displayed in the customer account page

Input:
        customer_id                     number
        order_number                    varchar2 - original searched order
        start_position                  number - index of the start item in the id array
        max_number_returned             number - number of items in the result set
        session_id                      varchar2 - session id for viewing info
        csr_id                          varchar2 - viewing csr
Output:
        cursor containing customer information
        cursor containing customer phone information
        cursor containing customer email information
        cursor containing customer membership information
        cursor containing customer order information
        number of orders for the customer
        cursor containing customer hold information
        cursor containing csr viewing information
        customer locked indicator
        number of distinct shopping carts for the customer
        indicator if there are more than one email per company
        cursor containing the direct mail information

-----------------------------------------------------------------------------*/
-- cursor to hold the customer order detail ids
v_order_cur             types.ref_cursor;

-- array for of the order detail ids for the customer
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                varchar2(32767);

-- locked indicators
v_locked_ind            char(1);
v_locked_csr_id         varchar2(100);
v_update_ind            char(1);

BEGIN

-- get customer info
GET_CUSTOMER (IN_CUSTOMER_ID, OUT_CUSTOMER_CUR);
GET_CUSTOMER_PHONES (IN_CUSTOMER_ID, OUT_PHONE_CUR);
GET_EMAIL (IN_CUSTOMER_ID, OUT_EMAIL_CUR, OUT_MORE_EMAIL_INDICATOR);
GET_MEMBERSHIPS (IN_CUSTOMER_ID, OUT_MEMBERSHIP_CUR);
GET_DIRECT_MAIL (IN_CUSTOMER_ID, OUT_DIRECT_MAIL_CUR);
GET_CUSTOMER_HOLD (IN_CUSTOMER_ID, OUT_HOLD_CUR);

-- get the customer orders
GET_CUSTOMER_ORDERS (IN_CUSTOMER_ID, v_order_cur);
FETCH v_order_cur bulk collect INTO id_tab;
CLOSE v_order_cur;

-- create the string from the array of order detail ids
id_list_pkg.get_str_id (id_tab, in_start_position, in_max_number_returned, v_str_id);

order_query_pkg.GET_ORDER_LIST_BY_ID (v_str_id, in_order_number, in_customer_id, OUT_ORDER_CUR);

OUT_NUMBER_OF_ORDERS := id_tab.count;

-- get the list of csrs viewing the customer
CSR_VIEWED_LOCKED_PKG.GET_CSR_VIEWING (IN_SESSION_ID, IN_CSR_ID, 'CUSTOMER', to_char (IN_CUSTOMER_ID), OUT_VIEWING_CUR);

-- check if a csr has the record locked
CSR_VIEWED_LOCKED_PKG.CHECK_CSR_LOCKED_ENTITIES ('CUSTOMER', to_char (IN_CUSTOMER_ID), IN_SESSION_ID, IN_CSR_ID, null, v_locked_ind, v_locked_csr_id);
OUT_LOCKED_INDICATOR := v_locked_ind;

GET_CUSTOMER_CARTS (in_customer_id, out_number_of_carts);

END GET_CUSTOMER_ACCT;


PROCEDURE GET_ALL_CUSTOMER_EMAIL
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving email
        addresses for the given customer

Input:
        customer_id                     number

Output:
        cursor containing email addresses fields

-----------------------------------------------------------------------------*/
CURSOR email_cur IS
        SELECT  e.email_id
        FROM    email e
        JOIN    customer_email_ref cer
        ON      e.email_id = cer.email_id
        JOIN    ftd_apps.company_master c
        ON      e.company_id = c.company_id
        WHERE   cer.customer_id = in_customer_id
        AND     c.email_direct_mail_flag = 'Y'
        ORDER BY c.sort_order, e.updated_on desc;

-- array for the results of the dynamic sql statement
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                varchar2(32767);
v_max_cnt               NUMBER;

BEGIN

OPEN email_cur;
FETCH email_cur BULK COLLECT INTO id_tab;
CLOSE email_cur;

v_max_cnt := in_max_number_returned;
if v_max_cnt = 0 then
v_max_cnt := id_tab.count;
end if;

-- get the ids in a string
id_list_pkg.get_str_id (id_tab, in_start_position, v_max_cnt, v_str_id);

OUT_ID_COUNT := id_tab.count;

OPEN OUT_CUR FOR
       'SELECT
                e.email_id,
                c.company_name company_name,
                e.email_address,
                e.subscribe_status,
                to_char (e.updated_on, ''mm/dd/yyyy'') updated_on
        FROM    email e
        JOIN    ftd_apps.company_master c
        ON      e.company_id = c.company_id
        WHERE   e.email_id in (' || v_str_id || ') ' ||
       'AND     c.email_direct_mail_flag = ''Y''
        ORDER BY c.sort_order, e.updated_on desc';

END GET_ALL_CUSTOMER_EMAIL;


PROCEDURE GET_ALL_CUSTOMER_EMAIL_UPDATE
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving email
        addresses for the given customer

Input:
        customer_id                     number

Output:
        cursor containing email addresses fields

-----------------------------------------------------------------------------*/
CURSOR email_cur IS
        SELECT  count (distinct e.email_address)
        FROM    email e
        JOIN    customer_email_ref cer
        ON      e.email_id = cer.email_id
        WHERE   cer.customer_id = in_customer_id
        ORDER BY lower (e.email_address);

BEGIN

OPEN email_cur;
FETCH email_cur INTO out_id_count;
CLOSE email_cur;

OPEN OUT_CUR FOR
        SELECT  *
        FROM
        (
                SELECT  a.*,
                        rownum row_number
                FROM
                (
                        SELECT
                                e.email_address,
                                to_char (max (e.updated_on), 'mm/dd/yyyy') updated_on
                        FROM    email e
                        JOIN    customer_email_ref cer
                        ON      e.email_id = cer.email_id
                        WHERE   cer.customer_id = in_customer_id
                        GROUP BY e.email_address
                        ORDER BY lower (e.email_address)
                ) a
                WHERE   rownum <= (in_start_position+in_max_number_returned-1)
        )
        WHERE   row_number >= in_start_position;

END GET_ALL_CUSTOMER_EMAIL_UPDATE;



FUNCTION GET_CUSTOMER_BY_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer of the
        given order.

Input:
        order_detail_id                         number

Output:
        customer id who placed the given order

-----------------------------------------------------------------------------*/

v_customer_id           customer.customer_id%type;

BEGIN

SELECT  o.customer_id
INTO    v_customer_id
FROM    orders o
JOIN    order_details od
ON      o.order_guid = od.order_guid
WHERE   od.order_detail_id = in_order_detail_id;

RETURN v_customer_id;

END GET_CUSTOMER_BY_ORDER;


PROCEDURE GET_MEMBERSHIP_BY_ID
(
IN_MEMBERSHIP_ID                IN MEMBERSHIPS.MEMBERSHIP_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving membership information
        for the given membership id.

Input:
        membership_id                     number

Output:
        cursor containing membership information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                membership_id,
                membership_number,
                membership_type,
                first_name membership_first_name,
                last_name membership_last_name,
                null date_miles_posted
        FROM    memberships
        WHERE   membership_id = in_membership_id;

END GET_MEMBERSHIP_BY_ID;


FUNCTION IS_CUSTOMER_ON_ORDER_HOLD
(
 IN_CUSTOMER_ID               IN NUMBER,
 IN_LOSS_PREVENTION_INDICATOR IN VARCHAR2
)
 RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a Y/N flag if the customer_id passed in
        has a hold on it.

Input:
        customer_id - NUMBER

Output:
        flag - CHAR

-----------------------------------------------------------------------------*/

v_flag VARCHAR2(5);
v_fraud_count NUMBER := 0;
v_prefer_count NUMBER := 0;


BEGIN

  BEGIN
    SELECT v_prefer_count + 1
      INTO v_prefer_count
      FROM DUAL
     WHERE EXISTS (SELECT 1
                     FROM customer_hold_entities
                    WHERE entity_type = 'CUSTOMER_ID'
                      AND hold_value_1 = TO_CHAR(in_customer_id)
                      AND hold_reason_code = 'Prefer');

    v_flag := 'CUST';

    EXCEPTION WHEN NO_DATA_FOUND THEN v_prefer_count := 0;
  END;

  BEGIN
    SELECT v_fraud_count + 1
      INTO v_fraud_count
      FROM DUAL
     WHERE EXISTS (SELECT 1
                     FROM customer_hold_entities
                    WHERE entity_type = 'CUSTOMER_ID'
                      AND hold_value_1 = TO_CHAR(in_customer_id)
                      AND hold_reason_code = 'Fraud');

    v_flag := 'CUST';

    EXCEPTION WHEN NO_DATA_FOUND THEN v_fraud_count := 0;
  END;


  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            credit_cards cc,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cc.customer_id = c.customer_id
                        AND che.entity_type = 'CC_NUMBER'
                        AND cc.cc_number = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'CC';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;

    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            credit_cards cc,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cc.customer_id = c.customer_id
                        AND che.entity_type = 'CC_NUMBER'
                        AND cc.cc_number = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'CC';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;


  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            ap_accounts apa,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND apa.customer_id = c.customer_id
                        AND che.entity_type = 'AP_ACCOUNT'
                        AND apa.ap_account_id = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'AP';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;

    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            ap_accounts apa,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND apa.customer_id = c.customer_id
                        AND che.entity_type = 'AP_ACCOUNT'
                        AND apa.ap_account_id = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'AP';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;


  IF (v_fraud_count = 0) AND (in_loss_prevention_indicator IS NOT NULL) THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities
                      WHERE entity_type = 'LOSS_PREVENTION_INDICATOR'
                        AND hold_value_1 = in_loss_prevention_indicator
                        AND hold_reason_code = 'Prefer');

      v_flag := 'LPI';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities
                      WHERE entity_type = 'LOSS_PREVENTION_INDICATOR'
                        AND hold_value_1 = in_loss_prevention_indicator
                        AND hold_reason_code = 'Fraud');

      v_flag := 'LPI';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer_phones cp,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cp.customer_id = c.customer_id
                        AND che.entity_type = 'PHONE_NUMBER'
                        AND cp.phone_number = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'PN';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer_phones cp,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cp.customer_id = c.customer_id
                        AND che.entity_type = 'PHONE_NUMBER'
                        AND cp.phone_number = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'PN';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            email e,
                            customer_email_ref cer,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cer.customer_id = c.customer_id
                        AND cer.email_id = e.email_id
                        AND che.entity_type = 'EMAIL_ADDRESS'
                        AND LOWER (e.email_address) = LOWER (che.hold_value_1)
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'EA';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            email e,
                            customer_email_ref cer,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND cer.customer_id = c.customer_id
                        AND cer.email_id = e.email_id
                        AND che.entity_type = 'EMAIL_ADDRESS'
                        AND LOWER (e.email_address) = LOWER (che.hold_value_1)
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'EA';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            memberships m,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND m.customer_id = c.customer_id
                        AND che.entity_type = 'MEMBERSHIP_ID'
                        AND TO_CHAR(m.membership_id) = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'MEM';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            memberships m,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND m.customer_id = c.customer_id
                        AND che.entity_type = 'MEMBERSHIP_ID'
                        AND TO_CHAR(m.membership_id) = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'MEM';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND che.entity_type = 'ADDRESS'
                        AND (c.address_1 || '|' || SUBSTR(c.zip_code,1,5)) = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'ADD';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND che.entity_type = 'ADDRESS'
                        AND (c.address_1 || '|' || SUBSTR(c.zip_code,1,5)) = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'ADD';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count = 0 THEN
    BEGIN
      SELECT v_prefer_count + 1
        INTO v_prefer_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND che.entity_type = 'NAME'
                        AND (c.first_name || '|' || c.last_name) = che.hold_value_1
                        AND che.hold_reason_code = 'Prefer');

      v_flag := 'NAME';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
    BEGIN
      SELECT v_fraud_count + 1
        INTO v_fraud_count
        FROM DUAL
       WHERE EXISTS (SELECT 1
                       FROM customer_hold_entities che,
                            customer c
                      WHERE c.customer_id = in_customer_id
                        AND che.entity_type = 'NAME'
                        AND (c.first_name || '|' || c.last_name) = che.hold_value_1
                        AND che.hold_reason_code = 'Fraud');

      v_flag := 'NAME';

      EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_fraud_count > 0 THEN
    v_flag := 'F' || v_flag;
  ELSIF v_prefer_count > 0 THEN
    v_flag := 'P' || v_flag;
  ELSE
    v_flag := 'N';
  END IF;

  RETURN v_flag;

END IS_CUSTOMER_ON_ORDER_HOLD;


FUNCTION GET_CUSTOMER_CONCAT_ID
(
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for making the concatentate id of the
        customer.  last name + first name (up to the first space) +
        first 5 characters of the zip code + first 4 characters of address 1.

Input:
        first_name                      varchar2
        last_name                       varchar2
        address_1                       varchar2
        zip_code                        varchar2

Output:
        concat_id                       varchar2


-----------------------------------------------------------------------------*/

v_concat_id             customer.concat_id%type;

BEGIN

v_concat_id := upper (rtrim (ltrim (in_last_name))) || upper (substr (rtrim (ltrim (in_first_name)), 1, instr (rtrim (ltrim (in_first_name)) || ' ', ' ') - 1)) ||
               upper (substr (rtrim (ltrim (in_zip_code)), 1, 5)) || upper (substr (rtrim (ltrim (in_address_1)), 1, 4));

RETURN v_concat_id;

END GET_CUSTOMER_CONCAT_ID;


FUNCTION GET_CUSTOMER_ID_BY_CONCAT_ID
(
IN_CONCAT_ID                    IN CUSTOMER.CONCAT_ID%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for finding the customer with the
        given concatenated id.  If muliple customer records are found with
        the same concat id, the record from the most recently created will be
        used, then the customer id.

Input:
        concat_id                       varchar2

Output:
        customer_id                     number


-----------------------------------------------------------------------------*/

CURSOR cust_cur IS
        SELECT  c.customer_id
        FROM    customer c
        WHERE   concat_id = in_concat_id
        ORDER BY c.created_on DESC, c.customer_id DESC;

v_customer_id           customer.customer_id%type;

BEGIN

OPEN cust_cur;
FETCH cust_cur INTO v_customer_id;
CLOSE cust_cur;

RETURN v_customer_id;

END GET_CUSTOMER_ID_BY_CONCAT_ID;

FUNCTION GET_CUSTOMER_EMAIL_ID
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for finding the customer email id
        for the given customer and email address.

Input:
        customer_id                     number
        email_address                   varchar2

Output:
        email_id                        number


-----------------------------------------------------------------------------*/

CURSOR email_cur IS
        SELECT  e.email_id
        FROM    email e
        JOIN    customer_email_ref cer
        ON      e.email_id = cer.email_id
        WHERE   cer.customer_id = in_customer_id
        AND     LOWER (e.email_address) = LOWER (in_email_address);

v_email_id              email.email_id%type;

BEGIN

OPEN email_cur;
FETCH email_cur INTO v_email_id;
CLOSE email_cur;

RETURN v_email_id;

END GET_CUSTOMER_EMAIL_ID;


FUNCTION GET_MEMBERHIP_ID
(
IN_CUSTOMER_ID                  IN MEMBERSHIPS.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_MEMBERSHIP_NUMBER            IN MEMBERSHIPS.MEMBERSHIP_NUMBER%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for finding the membership id
        for the given customer, membership type and number

Input:
        customer_id                     number
        membership_type                 varchar2
        membership_number               varchar2

Output:
        membership_id                   number


-----------------------------------------------------------------------------*/

CURSOR membership_cur IS
        SELECT  membership_id
        FROM    memberships
        WHERE   customer_id = in_customer_id
        AND     membership_type = in_membership_type
        AND     membership_number = in_membership_number;

v_membership_id         membershipS.membership_id%type;

BEGIN

OPEN membership_cur;
FETCH membership_cur INTO v_membership_id;
CLOSE membership_cur;

RETURN v_membership_id;

END GET_MEMBERHIP_ID;


PROCEDURE GET_CUSTOMER_EMAIL_INFO
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_COMPANY_ID                   IN EMAIL.COMPANY_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer first,
        and last name and email address

Input:
        customer_id                     number
        company_id                      varchar2

Output:
        cursor containing customer info

-----------------------------------------------------------------------------*/

CURSOR email_cur (in_v_company_id varchar2) IS
        select  e.email_address,e.subscribe_status
        from    email e
        join    customer_email_ref cer
        on      e.email_id = cer.email_id
        where   cer.customer_id = in_customer_id
        and     e.company_id = in_v_company_id
        and     cer.most_recent_by_company = 'Y';

v_email_address           email.email_address%type;
v_email_subscribe_status  email.subscribe_status%type;
v_company_id              varchar2(12);

BEGIN

v_company_id := GET_NEWSLETTER_COMPANY_ID(in_company_id);

OPEN email_cur (v_company_id);
FETCH email_cur INTO v_email_address, v_email_subscribe_status;
CLOSE email_cur;


OPEN OUT_CUR FOR
        SELECT
                c.first_name,
                c.last_name,
                v_email_address email_address,
                v_email_subscribe_status email_subscribe_status
        FROM    customer c
        WHERE   c.customer_id = in_customer_id;

END GET_CUSTOMER_EMAIL_INFO;



PROCEDURE GET_DIRECT_MAIL_EMAIL
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_MORE_EMAIL_IND              OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving direct mail and email
        addresses statuses for the given customer

Input:
        customer_id                     number

Output:
        cursor containing direct mail and the most recent email addresses
        for all companies

-----------------------------------------------------------------------------*/
CURSOR more_cur IS
        SELECT  decode (count (1), 0, 'N', 'Y') more_email
        FROM    customer_email_ref
        WHERE   customer_id = in_customer_id
        AND     most_recent_by_company = 'N';

BEGIN

OPEN more_cur;
FETCH more_cur INTO out_more_email_ind;
CLOSE more_cur;

OPEN OUT_CUR FOR
        SELECT
                c.company_name,
                c.company_id,
                d.subscribe_status direct_mail_subscribe_status,
                e.email_id,
                e.email_address,
                e.active_indicator email_active_indicator,
                e.subscribe_status email_subscribe_status,
                e.updated_on email_updated_on
        FROM    ftd_apps.company_master c
        LEFT OUTER JOIN    direct_mail d
        ON      d.company_id = c.company_id
        AND     d.customer_id = in_customer_id
        LEFT OUTER JOIN
        (
                select  e1.email_id,
                        e1.email_address,
                        null active_indicator,
                        e1.subscribe_status,
                        e1.updated_on,
                        e1.company_id
                from    email e1
                join    customer_email_ref cer
                on      e1.email_id = cer.email_id
                where   cer.customer_id = in_customer_id
                and     cer.most_recent_by_company = 'Y'
        ) e
        ON      e.company_id = c.company_id
        WHERE   c.company_id in (
            SELECT DISTINCT cm.newsletter_company_id
            FROM FTD_APPS.COMPANY_MASTER CM
            WHERE cm.email_direct_mail_flag = 'Y')
        ORDER BY c.sort_order;

END GET_DIRECT_MAIL_EMAIL;


PROCEDURE GET_CUSTOMER_ACCT_ONLY
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_SESSION_ID                   IN CSR_VIEWING_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_VIEWING_ENTITIES.CSR_ID%TYPE,
OUT_CUSTOMER_CUR                OUT TYPES.REF_CURSOR,
OUT_PHONE_CUR                   OUT TYPES.REF_CURSOR,
OUT_DIRECT_MAIL_EMAIL_CUR       OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR              OUT TYPES.REF_CURSOR,
OUT_HOLD_CUR                    OUT TYPES.REF_CURSOR,
OUT_VIEWING_CUR                 OUT TYPES.REF_CURSOR,
OUT_LOCKED_INDICATOR            OUT VARCHAR2,
OUT_MORE_EMAIL_INDICATOR        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer information
        to be displayed in the customer account page

Input:
        customer_id                     number
        order_number                    varchar2 - original searched order
        start_position                  number - index of the start item in the id array
        max_number_returned             number - number of items in the result set
        session_id                      varchar2 - session id for viewing info
        csr_id                          varchar2 - viewing csr
Output:
        cursor containing customer information
        cursor containing customer phone information
        cursor containing customer email information
        cursor containing customer membership information
        cursor containing customer hold information
        cursor containing csr viewing information
        customer locked indicator
        indicator if there are more than one email per company
        cursor containing the direct mail information

-----------------------------------------------------------------------------*/

-- locked indicators
v_locked_ind            char(1);
v_locked_csr_id         varchar2(100);
v_update_ind            char(1);

BEGIN

-- get customer info
GET_CUSTOMER (IN_CUSTOMER_ID, OUT_CUSTOMER_CUR);
GET_CUSTOMER_PHONES (IN_CUSTOMER_ID, OUT_PHONE_CUR);
GET_DIRECT_MAIL_EMAIL (IN_CUSTOMER_ID, OUT_DIRECT_MAIL_EMAIL_CUR, OUT_MORE_EMAIL_INDICATOR);
GET_MEMBERSHIPS (IN_CUSTOMER_ID, OUT_MEMBERSHIP_CUR);
GET_CUSTOMER_HOLD (IN_CUSTOMER_ID, OUT_HOLD_CUR);

-- get the list of csrs viewing the customer
CSR_VIEWED_LOCKED_PKG.GET_CSR_VIEWING (IN_SESSION_ID, IN_CSR_ID, 'CUSTOMER', to_char (IN_CUSTOMER_ID), OUT_VIEWING_CUR);

-- check if a csr has the record locked
CSR_VIEWED_LOCKED_PKG.CHECK_CSR_LOCKED_ENTITIES ('CUSTOMER', to_char (IN_CUSTOMER_ID), IN_SESSION_ID, IN_CSR_ID, null, v_locked_ind, v_locked_csr_id);
OUT_LOCKED_INDICATOR := v_locked_ind;

END GET_CUSTOMER_ACCT_ONLY;


PROCEDURE GET_EMAIL_BY_EMAIL_ADDRESS
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer's email
        records for the given email address

Input:
        customer_id                     number
        email_address                   varchar2

Output:
        cursor containing customer emails

-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
        SELECT
                e.email_address,
                c.company_id,
                e.subscribe_status,
                (select max(e1.updated_on) from email e1 join customer_email_ref cer1 on e1.email_id = cer1.email_id where lower (e1.email_address) = lower (in_email_address) and cer1.customer_id = in_customer_id) updated_on,
                cer.email_id,
                c.company_name
        FROM    ftd_apps.company_master c
        LEFT OUTER JOIN email e
        ON      c.company_id = e.company_id
        AND     lower (e.email_address) = lower (in_email_address)
        LEFT OUTER JOIN customer_email_ref cer
        ON      e.email_id = cer.email_id
        AND     cer.customer_id = in_customer_id
        WHERE   c.company_id in (
            SELECT DISTINCT cm.newsletter_company_id
            FROM FTD_APPS.COMPANY_MASTER CM
            WHERE cm.email_direct_mail_flag = 'Y')
        ORDER BY c.sort_order;

END GET_EMAIL_BY_EMAIL_ADDRESS;


PROCEDURE SEARCH_CUSTOMER_BY_PHONE
(
IN_PHONE_NUMBER                 IN CUSTOMER_PHONES.PHONE_NUMBER%TYPE,
IN_BUYER_INDICATOR              IN VARCHAR2,
IN_RECIPIENT_INDICATOR          IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer info
        for the given phone number

Input:
        phone_number            varchar2
        buyer_indicator         varchar2
        recipient_indicator     varchar2

Output:
        cursor containing customer customer info

-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
        SELECT
                c.customer_id,
                c.first_name,
                c.last_name,
                c.address_1,
                c.address_2,
                c.city,
                c.state,
                c.country,
                c.zip_code
        FROM    customer c
        WHERE   EXISTS
        (
                select  1
                from    customer_phones cp
                where   cp.phone_number = in_phone_number
                and     cp.customer_id = c.customer_id
        )
        AND     ((c.buyer_indicator = 'Y' and in_buyer_indicator = 'Y')
        OR       (c.recipient_indicator = 'Y' and in_recipient_indicator = 'Y'))
        ORDER BY c.customer_id;

END SEARCH_CUSTOMER_BY_PHONE;


PROCEDURE GET_CUST_HOLD_REASON_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table customer_hold_reason_val.

Input:
        N/A

Output:
        cursor containing customer_hold_reason_val info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT hold_reason_code,
            description
       FROM customer_hold_reason_val
      WHERE status = 'Active';

END GET_CUST_HOLD_REASON_VAL_LIST;


PROCEDURE GET_CUSTOMER_HOLD_CUST_INFO
(
 IN_CUSTOMER_ID           IN VARCHAR2,
 OUT_CURSOR_CUSTOMER     OUT TYPES.REF_CURSOR,
 OUT_CURSOR_CREDIT_CARDS OUT TYPES.REF_CURSOR,
 OUT_CURSOR_PHONES       OUT TYPES.REF_CURSOR,
 OUT_CURSOR_MEMBERSHIPS  OUT TYPES.REF_CURSOR,
 OUT_CURSOR_EMAILS       OUT TYPES.REF_CURSOR,
 OUT_CURSOR_AP_ACCOUNTS  OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        This stored procedure retrieves a customer information from tables
        customer, credit_card, customer_phones, mememberships, and emails
        for a given customer id.

Input:
        customer_id VARCHAR2

Output:
        cursor containing customer records
        cursor containing customer credit cards
        cursor containing customer phones
        cursor containing memberships
        cursor containing email addresses
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor_customer FOR
     SELECT DISTINCT c.customer_id,
            chrv1.hold_reason_code id_reason,
            che1.customer_hold_entity_id entity_id_customer,
            c.first_name,
            c.last_name,
            chrv2.hold_reason_code name_reason,
            che2.customer_hold_entity_id entity_id_name,
            c.address_1,
            c.address_2,
            c.city,
            c.zip_code,
            c.state,
            c.country,
            chrv3.hold_reason_code address_reason,
            che3.customer_hold_entity_id entity_id_address
       FROM customer_hold_reason_val chrv3,
            customer_hold_entities che3,
            customer_hold_reason_val chrv2,
            customer_hold_entities che2,
            customer_hold_reason_val chrv1,
            customer_hold_entities che1,
            customer c
      WHERE c.customer_id = in_customer_id
        AND (che1.entity_type (+) = 'CUSTOMER_ID'
            AND che1.hold_value_1 (+) = to_char (c.customer_id)
            AND chrv1.hold_reason_code (+) = che1.hold_reason_code)
        AND (che2.entity_type (+) = 'NAME'
            AND che2.hold_value_1 (+) = (c.first_name || '|' || c.last_name)
            AND chrv2.hold_reason_code (+) = che2.hold_reason_code)
        AND (che3.entity_type (+) = 'ADDRESS'
            AND che3.hold_value_1 (+) = (c.address_1 || '|' || SUBSTR(c.zip_code,1,5))
            AND chrv3.hold_reason_code (+) = che3.hold_reason_code);


   OPEN out_cursor_credit_cards FOR
     SELECT DISTINCT global.encryption.decrypt_it(cc.cc_number,cc.key_name) cc_number,
            chrv.hold_reason_code cc_reason,
            che.customer_hold_entity_id,
            p.avs_code || ' ' ||
            NVL((select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id = p.payment_type),
                (select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id IS NULL)
            ) avs_response
       FROM credit_cards cc,
            customer_hold_reason_val chrv,
            customer_hold_entities che,
            payments p
      WHERE cc.customer_id = in_customer_id
        AND (che.entity_type (+) = 'CC_NUMBER'
            AND che.hold_value_1 (+) = cc.cc_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code)
        AND p.cc_id = cc.cc_id
        AND p.created_on = (
            SELECT max(created_on)
            FROM payments p1
            WHERE p1.cc_id = cc.cc_id
            AND p1.additional_bill_id is null
            AND p1.payment_indicator = 'P')
            AND global.encryption.decrypt_it(cc.cc_number,cc.key_name) != '1611015378284387';

   OPEN out_cursor_phones FOR
     SELECT DISTINCT cp.phone_number,
            chrv.hold_reason_code phone_reason,
            che.customer_hold_entity_id
       FROM customer_phones cp,
            customer_hold_reason_val chrv,
            customer_hold_entities che
      WHERE cp.customer_id = in_customer_id
        AND cp.phone_number IS NOT NULL
        AND (che.entity_type (+) = 'PHONE_NUMBER'
            AND che.hold_value_1 (+) = cp.phone_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_memberships FOR
     SELECT DISTINCT m.membership_number,
            chrv.hold_reason_code membership_reason,
            che.customer_hold_entity_id
       FROM memberships m,
            customer_hold_reason_val chrv,
            customer_hold_entities che
      WHERE m.customer_id = in_customer_id
        AND (che.entity_type (+) = 'MEMBERSHIP_ID'
            AND che.hold_value_1 (+) = m.membership_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_emails FOR
     SELECT DISTINCT e.email_address,
            chrv.hold_reason_code email_reason,
            che.customer_hold_entity_id
       FROM email e,
            customer_email_ref cer,
            customer_hold_reason_val chrv,
            customer_hold_entities che
      WHERE cer.customer_id = in_customer_id
        AND cer.email_id = e.email_id
        AND (che.entity_type (+) = 'EMAIL_ADDRESS'
            AND lower (che.hold_value_1 (+)) = lower (e.email_address)
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_ap_accounts FOR
     SELECT DISTINCT aa.ap_account_id ap_account,
            global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt,
            pm.description ap_account_type_desc,
            chrv.hold_reason_code ap_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            ftd_apps.payment_methods pm,
            ap_accounts aa
      WHERE aa.customer_id = in_customer_id
        AND pm.payment_method_id = aa.payment_method_id
        AND (che.entity_type (+) = 'AP_ACCOUNT'
            AND che.hold_value_1 (+) = aa.ap_account_id
            AND chrv.hold_reason_code (+) = che.hold_reason_code);


END GET_CUSTOMER_HOLD_CUST_INFO;



PROCEDURE GET_EMAIL_N_DIRECT_MAIL_HIST
(
 IN_CUSTOMER_ID           IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
 IN_SORT_BY               IN VARCHAR2,
 IN_START_POSITION        IN NUMBER,
 IN_END_POSITION          IN NUMBER,
 OUT_MAX_ROWS            OUT NUMBER,
 OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all records in tables direct_mail_history and email_history for
the
   for the customer id passed in.

Input:
        customer_id               VARCHAR2
        in_sort_by                VARCHAR2
        in_start_position         NUMBER
        in_end_position           NUMBER

Output:
        out_max_rows              NUMBER
        cursor containing source_master info
------------------------------------------------------------------------------*/

v_main_sql VARCHAR2(3000);
v_rowcount_sql VARCHAR2(3000);
v_sql VARCHAR2(1000);
v_params_exist BOOLEAN := FALSE;


BEGIN

   v_main_sql := 'SELECT ''catalog'' target,
                         company_id,
                         LOWER(subscribe_status) subscribe_status,
                         updated_on,
                         LOWER(updated_by) updated_by
                     FROM direct_mail_history
                      WHERE customer_id = ' || in_customer_id || '
                     UNION
                      SELECT LOWER(e.email_address) target,
                             e.company_id,
                             LOWER(e.subscribe_status) subscribe_status,
                             e.updated_on,
                             LOWER(e.updated_by) updated_by
                        FROM email_history e
                        JOIN customer_email_ref cer
                        ON   e.email_id = cer.email_id
                       WHERE cer.customer_id = ' || in_customer_id;

   v_rowcount_sql := 'SELECT (SELECT COUNT(1)
                                  FROM direct_mail_history
                                 WHERE customer_id = ' || in_customer_id ||')
                                +
                                (SELECT COUNT(1)
                                  FROM email_history e
                                  JOIN customer_email_ref cer
                                  ON   e.email_id = cer.email_id
                                 WHERE customer_id = ' || in_customer_id ||')
                           FROM dual';


   --concatinate the sql string with the ordering parameter that may have been passed in
   IF in_sort_by IS NOT NULL THEN
     v_main_sql := v_main_sql || ' ORDER BY ' || in_sort_by;
   END IF;

   --the follwing code will only return the rows asked for
   IF in_start_position IS NOT NULL AND in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                  ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position || ')
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_start_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                 ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt)
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position;
   END IF;
   EXECUTE IMMEDIATE v_rowcount_sql INTO out_max_rows;

   OPEN out_cursor FOR v_main_sql;
---open out_cursor for select v_main_sql from dual;

END GET_EMAIL_N_DIRECT_MAIL_HIST;


PROCEDURE GET_CUST_INFO_BY_ORDER
(
IN_ORDER_GUID           IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer information
        for the given order guid to authorize payments

Input:
        customer_id                     number

Output:
        cursor containing customer information

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  c.customer_id,
                c.address_1,
                c.zip_code,
                c.state,
                o.company_id
        FROM    customer c
        JOIN    orders o
        ON      c.customer_id = o.customer_id
        WHERE   o.order_guid = in_order_guid;

END GET_CUST_INFO_BY_ORDER;


PROCEDURE GET_EMAIL_BY_EMAIL_ADDRESS
(
 IN_EMAIL_ADDRESS   IN EMAIL.EMAIL_ADDRESS%TYPE,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer's email
        records for the given email address

Input:
        email_address                   varchar2

Output:
        cursor containing customer emails

-----------------------------------------------------------------------------*/


BEGIN

OPEN out_cursor FOR
        SELECT e.company_id,
               e.subscribe_status,
               e.subscribe_date,
               cer.customer_id,
               cer.most_recent_by_company
          FROM email e,
               customer_email_ref cer
         WHERE lower (e.email_address) = lower (in_email_address)
           AND e.email_id = cer.email_id;


END GET_EMAIL_BY_EMAIL_ADDRESS;


PROCEDURE GET_EMAIL
(
 IN_EMAIL_ADDRESS   IN EMAIL.EMAIL_ADDRESS%TYPE,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the customer's email
        records for the given email address

Input:
        email_address    varchar2

Output:
        cursor containing customer emails

-----------------------------------------------------------------------------*/


BEGIN

OPEN out_cursor FOR
        SELECT company_id,
               subscribe_status,
               subscribe_date
          FROM email
         WHERE lower (email_address) = lower (in_email_address);


END GET_EMAIL;


PROCEDURE GET_EMAIL_FOR_CART_UPDATE
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving email
        addresses for the given customer

Input:
        customer_id                     number

Output:
        cursor containing email addresses fields

-----------------------------------------------------------------------------*/
CURSOR order_cur IS
        SELECT  o.customer_id,
                e.email_address
        FROM    orders o
        LEFT OUTER JOIN    email e
        ON      e.email_id = o.email_id
        WHERE   o.order_guid = in_order_guid;

CURSOR email_cur (p_customer_id varchar2) IS
        select  count (distinct e.email_address)
        from    email e
        join    customer_email_ref cer
        on      e.email_id = cer.email_id
        where   cer.customer_id = p_customer_id;

v_customer_id   customer.customer_id%type;
v_email_address email.email_address%type;

BEGIN

OPEN order_cur;
FETCH order_cur INTO v_customer_id, v_email_address;
CLOSE order_cur;

OPEN email_cur (v_customer_id);
FETCH email_cur INTO out_id_count;
CLOSE email_cur;

OPEN OUT_CUR FOR
        SELECT  *
        FROM
        (
                SELECT  a.*,
                        rownum row_number
                FROM
                (
                        SELECT  e.email_address,
                                e.updated_on,
                                decode (e.email_address, v_email_address, 'Y', 'N') current_order_email
                        FROM
                        (
                                select  e.email_address,
                                        max (e.updated_on) updated_on
                                from    email e
                                join    customer_email_ref cer
                                on      e.email_id = cer.email_id
                                where   cer.customer_id = v_customer_id
                                group by e.email_address
                        ) e
                        ORDER BY e.updated_on desc
                ) a
                WHERE   rownum <= (in_start_position+in_max_number_returned-1)
        )
        WHERE   row_number >= in_start_position;

END GET_EMAIL_FOR_CART_UPDATE;



PROCEDURE SEARCH_CUSTOMER_WEBOE
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN CUSTOMER_PHONES.PHONE_NUMBER%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the results of the
        customer search in web oe.  This procedure is called by
        ftd_apps.oe_get_customer.

Input:
        customer_id                     number
        address_1                       varchar
        city                            varchar
        state                           varchar
        zip_code                        varchar
        phone_number                    varchar


Output:
        cursor containing search results

-----------------------------------------------------------------------------*/

v_sql                   varchar2 (32000);

-- select clause variables
v_cust_select           varchar2 (500) := 'select distinct c.customer_id ';

-- from clause
v_from                  varchar2 (500) := 'from customer c ';

-- where clause variables
v_where                 varchar2 (4000) := 'where 1=1 ';
v_customer_id           varchar2 (500) := 'and c.customer_id = ' || in_customer_id || ' ';
v_address_1             varchar2 (500) := 'and c.address_1 like ''' || upper (in_address_1) || '%'' ';
v_city                  varchar2 (500) := 'and c.city like ''' || upper (in_city) || '%'' ';
v_state                 varchar2 (500) := 'and c.state = ''' || in_state || ''' ';
v_zip_code              varchar2 (500) := 'and c.zip_code like ''' || in_zip_code || '%'' ';
v_phone_number          varchar2 (500) := 'and exists (select 1 from customer_phones p where p.customer_id = c.customer_id and p.phone_number = ''' || in_phone_number || ''') ';

-- order by variables
v_order_by              varchar2 (500) := 'order by c.customer_id ';

-- array for the results of the dynamic sql statement
id_tab                  id_list_pkg.number_tab_typ;

-- cursor for the dynamic sql statement
sql_cur                 types.ref_cursor;

-- string to store the list of ids
v_str_id           varchar2(32767);

BEGIN

IF in_phone_number = '0000000000' OR in_phone_number = '9999999999' THEN
        -- these are test phone numbers so return an empty result set for the search
        v_str_id := '-999';
ELSE
        IF in_phone_number is not null THEN
                -- search for customers that have the given phone number
                v_where := v_where || v_phone_number;
        ELSE
                -- search for the remaining given criteria
                IF in_customer_id is not null THEN
                        v_where := v_where || v_customer_id;
                END IF;
                IF in_address_1 is not null THEN
                        v_where := v_where || v_address_1;
                END IF;
                IF in_city is not null THEN
                        v_where := v_where || v_city;
                END IF;
                IF in_state is not null THEN
                        v_where := v_where || v_state;
                END IF;
                IF in_zip_code is not null THEN
                        v_where := v_where || v_zip_code;
                END IF;
        END IF;

        -- build the entire sql statement for the initial query to get the ids
        v_sql := v_cust_select || v_from || v_where || v_order_by;

        -- get the customer ids
        open sql_cur for v_sql;
        fetch sql_cur bulk collect into id_tab limit 100;
        close sql_cur;

        IF id_tab.count = 0 THEN
                -- set the string value to return no rows
                v_str_id := '-999';
        ELSE
                -- get the ids in a string
                id_list_pkg.get_str_id (id_tab, 1, 100, v_str_id);
        END IF;
END IF;

-- return the result of the customer search
OPEN out_cur FOR
       'SELECT  c.customer_id "customerId",
                nls_initcap (c.first_name) "firstName",
                nls_initcap (c.last_name) "lastName",
                nls_initcap (c.address_1) "address1",
                nls_initcap (c.address_2) "address2",
                nls_initcap (c.city) "city",
                c.state "state",
                c.zip_code "zipCode",
                phh.phone_number "homePhone",
                phw.phone_number "workPhone",
                null "faxNumber",
                cer.email_address "email",
                phw.extension "workPhoneExt",
                c.country "countryId",
                c.address_type "addressType",
                c.business_name "bfhName",
                c.business_info "bfhInfo",
                nls_initcap (c.county) "county",
                cer.promo_flag "promoFlag"
        FROM    clean.customer c,
                clean.customer_phones phh,
                clean.customer_phones phw,
               (select  cer.customer_id,
                        e.email_address,
                        decode (e.subscribe_status, ''Subscribe'', ''Y'', ''N'') promo_flag
                from    clean.customer_email_ref cer,
                        clean.email e
                where   cer.customer_id IN ('|| v_str_id || ')
                and     cer.email_id = e.email_id
                and     cer.most_recent_by_company = ''Y''
                and     cer.updated_on = (select max (cer2.updated_on)
                                          from  clean.customer_email_ref cer2
                                          where cer2.customer_id = cer.customer_id
                                          and   cer2.most_recent_by_company = ''Y'')
                ) cer
        WHERE   c.customer_id IN ('|| v_str_id || ')
        and     c.customer_id = phh.customer_id (+)
        and     phh.phone_type (+) = ''Evening''
        and     c.customer_id = phw.customer_id (+)
        and     phw.phone_type (+) = ''Day''
        and     c.customer_id = cer.customer_id (+)
        ORDER BY c.customer_id';



END SEARCH_CUSTOMER_WEBOE;


FUNCTION GET_CUSTOMER_PHONE_BY_ID_TYPE (
	IN_CUSTOMER_ID    IN customer_phones.customer_id%TYPE,
	IN_PHONE_TYPE     IN customer_phones.phone_type%TYPE )
RETURN customer_phones.phone_number%TYPE
IS

v_phone customer_phones.phone_number%TYPE;
BEGIN

BEGIN
SELECT phone_number
INTO v_phone
FROM customer_phones
WHERE phone_type = in_phone_type
AND customer_id = in_customer_id;

EXCEPTION
   WHEN NO_DATA_FOUND THEN v_phone := NULL;
END;

RETURN v_phone;

END GET_CUSTOMER_PHONE_BY_ID_TYPE;




FUNCTION GET_CUSTOMER_EXT_BY_ID_TYPE (
	IN_CUSTOMER_ID    IN customer_phones.customer_id%TYPE,
	IN_PHONE_TYPE     IN customer_phones.phone_type%TYPE )
RETURN customer_phones.extension%TYPE
IS

v_ext customer_phones.extension%TYPE;

BEGIN

BEGIN
SELECT extension
INTO v_ext
FROM customer_phones
WHERE phone_type = in_phone_type
AND customer_id = in_customer_id;

EXCEPTION
   WHEN NO_DATA_FOUND THEN v_ext := NULL;
END;

RETURN v_ext;

END GET_CUSTOMER_EXT_BY_ID_TYPE;


PROCEDURE GET_CUST_HOLD_BY_ENTITY_VALUE 
(
IN_ENTITY_TYPE                  IN  CLEAN.CUSTOMER_HOLD_ENTITIES.ENTITY_TYPE%TYPE,
IN_VALUE                        IN  CLEAN.CUSTOMER_HOLD_ENTITIES.HOLD_VALUE_1%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_CUR FOR
  SELECT e.customer_hold_entity_id, e.entity_type, e.hold_value_1, e.hold_reason_code, r.description, r.status
    FROM CLEAN.CUSTOMER_HOLD_REASON_VAL r, CLEAN.CUSTOMER_HOLD_ENTITIES e
   WHERE e.entity_type = IN_ENTITY_TYPE
     AND e.hold_value_1 = IN_VALUE
     AND r.hold_reason_code = e.hold_reason_code;

END GET_CUST_HOLD_BY_ENTITY_VALUE;


FUNCTION         IS_EMAIL_ADDRESS_ASSOCIATED
(
IN_ORDER_DETAIL_ID IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_EMAIL_ADDRESS IN CLEAN.EMAIL.EMAIL_ADDRESS%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns true if the email address is associated with the customer in the order.

Input:
        order detail id                      number
        email address			string

Output:
        Y/N

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
SELECT COUNT(DISTINCT email_address) FROM CLEAN.EMAIL em
INNER JOIN CLEAN.CUSTOMER_EMAIL_REF  cer ON cer.EMAIL_ID = em.EMAIL_ID
INNER JOIN CLEAN.ORDERS ord ON ord.CUSTOMER_ID = cer.CUSTOMER_ID  or ord.EMAIL_ID = em.EMAIL_ID
INNER JOIN CLEAN.ORDER_DETAILS orddtl ON orddtl.ORDER_GUID = ord.ORDER_GUID
WHERE orddtl.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
AND lower(em.EMAIL_ADDRESS) = lower(trim(IN_EMAIL_ADDRESS));


v_exists        char(1) := 'N';
v_count			int := 0;
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_count;
CLOSE exists_cur;

IF v_count > 0 THEN
   v_exists := 'Y';
END IF;

RETURN v_exists;

END IS_EMAIL_ADDRESS_ASSOCIATED;

PROCEDURE GET_EMAIL_UNSUBSCRIBES
(
IN_START_DATE           IN DATE,
IN_END_DATE             IN DATE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all emails unsubscribed between the
        given dates

Input:
        start_date              date
        end_date                date

Output:
        cursor containing email info

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cursor FOR
        SELECT  email_address, TO_CHAR(MIN(updated_on), 'MM/DD/YYYY HH24:MI')
        FROM    email_history
        WHERE   subscribe_status = 'Unsubscribe'
        AND     updated_on >= in_start_date
        AND     updated_on < in_end_date
        GROUP BY email_address;

END GET_EMAIL_UNSUBSCRIBES;

/******************************************************************************
 IS_PREFERRED_CUSTOMER 

Description:  Check to see customer has any preferred orders associated to them.  

******************************************************************************/
FUNCTION IS_PREFERRED_CUSTOMER 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count			int := 0;
v_IS_PREFERRED_CUSTOMER  varchar2 (1) := 'N';

BEGIN

  select count(*) into v_count
  FROM    clean.customer c
                JOIN    clean.orders o
                ON      c.customer_id = o.customer_id
                JOIN    clean.order_details od
                ON      o.order_guid = od.order_guid
                WHERE   c.customer_id = in_customer_id
   AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
               );

IF v_count = 0 THEN
  	select count(*) into v_count 
  	FROM    scrub.buyers b
                JOIN    scrub.orders o
                ON      b.buyer_id = o.buyer_id
                JOIN    scrub.order_details od
                ON      o.order_guid = od.order_guid
                WHERE   b.clean_customer_id = in_customer_id
  	AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
               );  
END IF;

IF v_count > 0 THEN
  v_IS_PREFERRED_CUSTOMER  := 'Y';
END IF;

RETURN v_IS_PREFERRED_CUSTOMER;

END IS_PREFERRED_CUSTOMER;

/******************************************************************************
 IS_FTD_CUSTOMER 

Description:  Check to see customer has any non-preferred orders associated to them.  

NOTE - AUG 2017
This function is used by COM to allow viewing list of orders for preferred customers 
if they had any non-preferred orders.  The business has decided to not allow this for 
USAA only - i.e., if it's a USAA customer, not even non-USAA orders should be listed. 

******************************************************************************/
FUNCTION IS_FTD_CUSTOMER 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_total_ftd_order_count  int := 0;
v_usaa_count             int := 0;
v_IS_FTD_CUSTOMER  varchar2 (1) := 'N';

BEGIN

  -- First check if there are any USAA orders for the customer.  If so, we always want to return 'N'
  -- to prevent CSR's from viewing any order lists for USAA customers (unless they have USAA role of course).
  --
  select count(*) into v_usaa_count
  FROM    clean.customer c
                JOIN    clean.orders o
                ON      c.customer_id = o.customer_id
                JOIN    clean.order_details od
                ON      o.order_guid = od.order_guid
                WHERE   c.customer_id = in_customer_id
  AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr 
                 JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name 
                 JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name 
                 WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                 AND pm.partner_name = 'USAA'
               );
               
  IF v_usaa_count = 0 THEN
    
     -- Not USAA customer, so proceed with check...
     --
     select count(*) into v_total_ftd_order_count
     FROM    clean.order_details cod, clean.orders co, clean.customer cc
     WHERE   cod.order_guid = co.order_guid
     AND     co.customer_id = cc.customer_id
     AND     co.customer_id = IN_CUSTOMER_ID
     AND NOT EXISTS 
      ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name 
        JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = cod.source_code
      )      
     AND NOT EXISTS 
      ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name 
        JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = (select sod.source_code
   			                       from scrub.order_details sod
   			                       where sod.order_detail_id = cod.order_detail_id )
      );
      
     IF v_total_ftd_order_count > 0 THEN
       v_IS_FTD_CUSTOMER  := 'Y';
     END IF;
  END IF;

  RETURN v_IS_FTD_CUSTOMER;

END IS_FTD_CUSTOMER;



/******************************************************************************
 IS_PREFERRED_RECIPIENT 

Description:  Check to see if recipient is associated to any preferred orders.  

******************************************************************************/
FUNCTION IS_PREFERRED_RECIPIENT 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count			int := 0;
v_IS_PREFERRED_RECIPIENT  varchar2 (1) := 'N';

BEGIN

  select count(*) into v_count
  FROM    clean.customer c
                JOIN    clean.order_details od
                ON      c.customer_id = od.RECIPIENT_ID
                WHERE   c.customer_id = in_customer_id
  AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
               );

IF v_count > 0 THEN
  v_IS_PREFERRED_RECIPIENT  := 'Y';
END IF;

RETURN v_IS_PREFERRED_RECIPIENT;

END IS_PREFERRED_RECIPIENT;

/******************************************************************************
 IS_FTD_RECIPIENT 

Description:  Check to see if the recipient is associated to any non-preferred orders.  

******************************************************************************/
FUNCTION IS_FTD_RECIPIENT 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_total_ftd_order_count         int := 0;
v_IS_FTD_RECIPIENT  varchar2 (1) := 'N';

BEGIN

  select count(*) into v_total_ftd_order_count
  FROM    clean.order_details cod, clean.customer cc
  WHERE   cod.RECIPIENT_ID = cc.customer_id
  AND     cod.RECIPIENT_ID = in_customer_id
  AND NOT EXISTS 
   ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = cod.source_code
   )      
  AND NOT EXISTS 
   ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = (select sod.source_code
			                       from scrub.order_details sod
			                       where sod.order_detail_id = cod.order_detail_id ) 
   );    

IF v_total_ftd_order_count > 0 THEN
  v_IS_FTD_RECIPIENT  := 'Y';
END IF;

RETURN v_IS_FTD_RECIPIENT;

END IS_FTD_RECIPIENT;


/*------------------------------------------------------------------------------
                        FUNCTION CART_HAS_PREF_PARTNER_ORDER 

Description:
    Check to see if the cart contains any orders where the source code
    is for a preferred partner  

Input:
    IN_ORDER_GUID   varchar2 - order guid

Output:
                    varchar2 - Y/N 

------------------------------------------------------------------------------*/

FUNCTION CART_HAS_PREF_PARTNER_ORDER 
(
IN_ORDER_GUID       IN   CLEAN.ORDER_DETAILS.ORDER_GUID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count     int := 0;
v_cart_has_pref_partner_order  varchar2 (1) := 'N';

BEGIN

  select count(*) 
  into   v_count
  from   clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
  where  od.order_guid = IN_ORDER_GUID
  and    pm.preferred_partner_flag = 'Y' 
  and    spr.source_code = od.source_code
  and    spr.program_name = pp.program_name
  and    pm.partner_name = pp.partner_name;


  IF v_count > 0 THEN
    v_cart_has_pref_partner_order := 'Y';
  END IF;

  RETURN v_cart_has_pref_partner_order;

END CART_HAS_PREF_PARTNER_ORDER;



/*------------------------------------------------------------------------------
                        PROCEDURE GET_VALIDATED_PRIVACY_POLICY

Description:
    For a given call log id, retrieve all the order detail ids and customer ids 
    in cursors.  Also, for a given call log id, if there exist one validation 
    record where bypass_priv_policy_verif_flag = Y, return a Y OUT_BYPASS_PPV_FLAG

Input:
    IN_CALL_LOG_ID                  varchar2 - call log id

Output:
    OUT_VALIDATED_CUSTOMERS         cursor
    OUT_VALIDATED_ORDER_DETAILS     cursor
    OUT_BYPASS_PPV_FLAG             varchar2 - Y/N

------------------------------------------------------------------------------*/
PROCEDURE GET_VALIDATED_PRIVACY_POLICY
(
IN_CALL_LOG_ID                CLEAN.CALL_LOG.CALL_LOG_ID%TYPE,
OUT_VALIDATED_CUSTOMERS       OUT TYPES.REF_CURSOR,
OUT_VALIDATED_ORDER_DETAILS   OUT TYPES.REF_CURSOR,
OUT_BYPASS_PPV_FLAG           OUT VARCHAR2
)
AS

CURSOR bypass_ppv_flag_cur IS
  SELECT  decode (count (1), 0, 'N', 'Y') bypass_ppv_flag_count
  FROM    clean.privacy_policy_verification ppv, clean.privacy_policy_option ppo
  WHERE   ppv.privacy_policy_option_code = ppo.privacy_policy_option_code
  AND     ppv.call_log_id = IN_CALL_LOG_ID
  AND     ppo.bypass_priv_policy_verif_flag = 'Y';

BEGIN

OPEN bypass_ppv_flag_cur;
FETCH bypass_ppv_flag_cur INTO OUT_BYPASS_PPV_FLAG;
CLOSE bypass_ppv_flag_cur;


OPEN OUT_VALIDATED_CUSTOMERS FOR
  SELECT  distinct customer_id
  FROM    clean.privacy_policy_verification
  WHERE   call_log_id = IN_CALL_LOG_ID
  AND     customer_id is not null;   
  
OPEN OUT_VALIDATED_ORDER_DETAILS FOR
  SELECT  distinct order_detail_id
  FROM    clean.privacy_policy_verification
  WHERE   call_log_id = IN_CALL_LOG_ID
  AND     order_detail_id is not null;


END GET_VALIDATED_PRIVACY_POLICY;


/*------------------------------------------------------------------------------
                        PROCEDURE GET_PRIVACY_POLICY

Description:
    This stored proc will be invoked when loading the privacy policy page. 
    Since the PPV popup can be triggered for either (customer validation) or 
    (customer validation and order validation), it takes both the customer id and 
    the order detail id.  However, there is a check to ensure that if the order 
    detail id is not passed, it will not retrieve order info. 
    Also, this proc will return all the options in the PRIVACY_POLICY_OPTION table. 
    The first 2 characters of the PRIVACY_POLICY_OPTION.PRIVACY_POLICY_OPTION_CODE
    will always be CV, OV, LD, VE with the initial implementation.  The stored
    proc will return it in separate cursors so that the GUI can be build dynamically.

Input:
    IN_CUSTOMER_ID                customer id
    IN_ORDER_DETAIL_ID            order detail id

Output:
    OUT_CUSTOMER_INFO             cursor - customer info only
    OUT_CUSTOMER_ORDERS_INFO      cursor - last 5 external order numbers for the customer
    OUT_ORDER_INFO                cursor - order info
    OUT_CC_INFO                   cursor - last 4 digits of the CC
    OUT_PRIVACY_POLICY_OPTION_CV  cursor - customer validation options
    OUT_PRIVACY_POLICY_OPTION_OV  cursor - order validation options
    OUT_PRIVACY_POLICY_OPTION_LD  cursor - limited disclosure options
    OUT_PRIVACY_POLICY_OPTION_VE  cursor - validation exception options

------------------------------------------------------------------------------*/
PROCEDURE GET_PRIVACY_POLICY
(
IN_CUSTOMER_ID                IN  CLEAN.CUSTOMER.CUSTOMER_ID%TYPE,
IN_ORDER_DETAIL_ID            IN  CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUSTOMER_INFO             OUT TYPES.REF_CURSOR,
OUT_CUSTOMER_ORDERS_INFO      OUT TYPES.REF_CURSOR,
OUT_ORDER_INFO                OUT TYPES.REF_CURSOR,
OUT_PHONE_INFO                OUT TYPES.REF_CURSOR,
OUT_CC_INFO                   OUT TYPES.REF_CURSOR,
OUT_PRIVACY_POLICY_OPTION_CV  OUT TYPES.REF_CURSOR,
OUT_PRIVACY_POLICY_OPTION_OV  OUT TYPES.REF_CURSOR,
OUT_PRIVACY_POLICY_OPTION_LD  OUT TYPES.REF_CURSOR,
OUT_PRIVACY_POLICY_OPTION_VE  OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_CUSTOMER_INFO FOR
    SELECT    c.customer_id, c.first_name, c.last_name, c.address_1, c.city, c.state, c.zip_code
    FROM      clean.customer c
    WHERE     c.customer_id = IN_CUSTOMER_ID;


  OPEN OUT_CUSTOMER_ORDERS_INFO FOR
    SELECT  * 
    FROM 
    (
      SELECT    od.external_order_number
      FROM      clean.customer c, clean.order_details od, clean.orders o
      WHERE     o.order_guid = od.order_guid
      AND       o.customer_id = c.customer_id
      AND       c.customer_id = IN_CUSTOMER_ID
      ORDER BY  od.created_on desc, od.external_order_number
    ) temp
    WHERE   rownum <= 5
    ORDER BY temp.external_order_number;

  IF IN_ORDER_DETAIL_ID IS NOT NULL THEN
    CLEAN.ORDER_QUERY_PKG.GET_ORDER_DETAILS (IN_ORDER_DETAIL_ID, OUT_ORDER_INFO);

    OPEN OUT_PHONE_INFO FOR
      SELECT    od.order_detail_id, od.recipient_id,
                CUSTOMER_QUERY_PKG.GET_CUSTOMER_PHONE_BY_ID_TYPE(od.recipient_id,'Day') recipient_day_number,
                CUSTOMER_QUERY_PKG.GET_CUSTOMER_PHONE_BY_ID_TYPE(od.recipient_id,'Evening') recipient_evening_number
      FROM      clean.order_details od
      WHERE     od.order_detail_id = IN_ORDER_DETAIL_ID;
    
    OPEN OUT_CC_INFO FOR
      SELECT    cc.cc_number_masked 
      FROM      clean.payments p, clean.credit_cards cc, clean.order_details od
      WHERE     p.order_guid = od.order_guid
      AND       p.cc_id = cc.cc_id
      AND       od.order_detail_id = IN_ORDER_DETAIL_ID
      AND       p.additional_bill_id IS NULL
      AND       p.payment_indicator = 'P';
  ELSE 
    OPEN OUT_ORDER_INFO FOR
      SELECT  null
      FROM    dual; 

    OPEN OUT_PHONE_INFO FOR
      SELECT  null
      FROM    dual; 

    OPEN OUT_CC_INFO FOR
      SELECT  null
      FROM    dual; 
  END IF; 

  OPEN OUT_PRIVACY_POLICY_OPTION_CV FOR
    SELECT    * 
    FROM      clean.privacy_policy_option
    WHERE     substr(PRIVACY_POLICY_OPTION_CODE,1,2) = 'CV'
    ORDER BY  display_order_seq_num;


  OPEN OUT_PRIVACY_POLICY_OPTION_OV FOR
    SELECT    * 
    FROM      clean.privacy_policy_option
    WHERE     substr(PRIVACY_POLICY_OPTION_CODE,1,2) = 'OV'
    ORDER BY  display_order_seq_num;


  OPEN OUT_PRIVACY_POLICY_OPTION_LD FOR
    SELECT    * 
    FROM      clean.privacy_policy_option
    WHERE     substr(PRIVACY_POLICY_OPTION_CODE,1,2) = 'LD'
    ORDER BY  display_order_seq_num;


  OPEN OUT_PRIVACY_POLICY_OPTION_VE FOR
    SELECT    * 
    FROM      clean.privacy_policy_option
    WHERE     substr(PRIVACY_POLICY_OPTION_CODE,1,2) = 'VE'
    ORDER BY  display_order_seq_num;


END GET_PRIVACY_POLICY;


/*------------------------------------------------------------------------------
                        PROCEDURE GET_VALIDATED_PRIVACY_POLICY

Description:
    This proc will return all the options in the PRIVACY_POLICY_OPTION table. 

Input:

Output:
    OUT_PRIVACY_POLICY_OPTION     cursor - all validation options

------------------------------------------------------------------------------*/
PROCEDURE GET_ALL_PRIVACY_POLICY_OPTION
(
OUT_PRIVACY_POLICY_OPTION     OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_PRIVACY_POLICY_OPTION FOR
    SELECT    * 
    FROM      clean.privacy_policy_option
    ORDER BY  privacy_policy_option_code, display_order_seq_num;


END GET_ALL_PRIVACY_POLICY_OPTION; 

PROCEDURE GET_PREFERRED_PARTNERS
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
OUT_PREFERRED_PARTNER		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all preferred partner names the 
        customer is associated with
        
Input:
        customer_id                     number
        
Output:
        cursor containing preferred partner names associated to the customer

-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_PREFERRED_PARTNER FOR

	SELECT DISTINCT pm.PREFERRED_PROCESSING_RESOURCE, pm.DISPLAY_NAME
	FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE c.customer_id = IN_CUSTOMER_ID
	AND c.customer_id = o.customer_id
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name = pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y';
	
END GET_PREFERRED_PARTNERS;


/******************************************************************************
 GET_PREFERRED_PARTNER_MASKED_NAME

Description:  Return preferred partner masked name to be displayed
              to reps who do not have access to preferred partner customer
              information

******************************************************************************/
FUNCTION GET_PREFERRED_MASKED_NAME
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_preferred_partner_count         int := 0;
v_PREFERRED_MASKED_NAME		  varchar2 (100);

BEGIN

	select count(*) into v_preferred_partner_count
	from
	(
	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE c.customer_id = IN_CUSTOMER_ID
	AND c.customer_id = o.customer_id
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'

	UNION

	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
	AND b.BUYER_ID = o.BUYER_ID
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'
	);

	IF v_preferred_partner_count = 0 THEN
	  select count(distinct pm.partner_name) into v_preferred_partner_count
	  from ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm, clean.order_details od, clean.customer c
	  where pp.program_name = spr.program_name
 	  and pm.partner_name = pp.partner_name 
	  and spr.source_code = od.source_code
	  and c.customer_id = od.RECIPIENT_ID
	  and c.customer_id = in_customer_id;
	END IF;

IF v_preferred_partner_count > 1 THEN
  v_PREFERRED_MASKED_NAME  := 'USAA';
ELSIF v_preferred_partner_count = 1 THEN

    BEGIN 
      select preferred_masked_name into v_PREFERRED_MASKED_NAME
      from
      (
      select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_masked_name
      FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
      WHERE c.customer_id = IN_CUSTOMER_ID
      AND c.customer_id = o.customer_id
      AND o.order_guid = od.order_guid
      AND spr.source_code = od.source_code
      AND pp.program_name = spr.program_name
      AND pm.partner_name =   pp.partner_name
      AND pm.PREFERRED_PARTNER_FLAG = 'Y'
  
      UNION
  
      select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_masked_name
      FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
      WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
      AND b.BUYER_ID = o.BUYER_ID
      AND o.order_guid = od.order_guid
      AND spr.source_code = od.source_code
      AND pp.program_name = spr.program_name
      AND pm.partner_name =   pp.partner_name
      AND pm.PREFERRED_PARTNER_FLAG = 'Y'
      );

      EXCEPTION WHEN NO_DATA_FOUND THEN
        select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  into v_PREFERRED_MASKED_NAME
        FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
        WHERE c.customer_id = IN_CUSTOMER_ID
        AND o.order_guid = od.order_guid
        AND c.customer_id = od.RECIPIENT_ID
        AND spr.source_code = od.source_code
        AND pp.program_name = spr.program_name
        AND pm.partner_name =   pp.partner_name
        AND pm.PREFERRED_PARTNER_FLAG = 'Y';
      END;

END IF;


RETURN v_PREFERRED_MASKED_NAME;

END GET_PREFERRED_MASKED_NAME;

/******************************************************************************
 GET_PREFERRED_WARNING_MSG

Description:  Return order access restriction message associated to
              preferred partner

******************************************************************************/
FUNCTION GET_PREFERRED_WARNING_MSG
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_preferred_partner_count         int := 0;
v_PREFERRED_PARTNER		  varchar2 (100);
v_PREFERRED_WARNING_MSG		  varchar2 (10000);

BEGIN

        select count(*) into v_preferred_partner_count
	from
	(
	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE c.customer_id = IN_CUSTOMER_ID
	AND c.customer_id = o.customer_id
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'

	UNION

	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
	AND b.BUYER_ID = o.BUYER_ID
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'
	);
	
	IF v_preferred_partner_count = 0 THEN
	  select count(distinct pm.partner_name) into v_preferred_partner_count
	  from ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm, clean.order_details od, clean.customer c
	  where pp.program_name = spr.program_name
 	  and pm.partner_name = pp.partner_name 
	  and spr.source_code = od.source_code
	  and c.customer_id = od.RECIPIENT_ID
	  and c.customer_id = in_customer_id;
	END IF;

IF v_preferred_partner_count > 1 THEN
        FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER ('PREFERRED_PARTNER', 'ORDER_ACCESS_RESTRICTION', 'DEFAULT', null, v_PREFERRED_WARNING_MSG);
ELSIF v_preferred_partner_count = 1 THEN

	BEGIN
	  select preferred_resource into v_PREFERRED_PARTNER
	  from
	  (
	  select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_resource
	  FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	  WHERE c.customer_id = IN_CUSTOMER_ID
	  AND c.customer_id = o.customer_id
	  AND o.order_guid = od.order_guid
	  AND spr.source_code = od.source_code
	  AND pp.program_name = spr.program_name
	  AND pm.partner_name =   pp.partner_name
	  AND pm.PREFERRED_PARTNER_FLAG = 'Y'

	  UNION

	  select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_resource
	  FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	  WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
	  AND b.BUYER_ID = o.BUYER_ID
	  AND o.order_guid = od.order_guid
	  AND spr.source_code = od.source_code
	  AND pp.program_name = spr.program_name
	  AND pm.partner_name =   pp.partner_name
	  AND pm.PREFERRED_PARTNER_FLAG = 'Y'
	  );
	
	  EXCEPTION WHEN NO_DATA_FOUND THEN
            select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  into v_PREFERRED_PARTNER
      	    FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
      	    WHERE c.customer_id = IN_CUSTOMER_ID
            AND o.order_guid = od.order_guid
            AND c.customer_id = od.RECIPIENT_ID
            AND spr.source_code = od.source_code
            AND pp.program_name = spr.program_name
            AND pm.partner_name =   pp.partner_name
            AND pm.PREFERRED_PARTNER_FLAG = 'Y';
          END;

	  FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER ('PREFERRED_PARTNER', 'ORDER_ACCESS_RESTRICTION', v_PREFERRED_PARTNER, null, v_PREFERRED_WARNING_MSG);
END IF;

RETURN v_PREFERRED_WARNING_MSG;

END GET_PREFERRED_WARNING_MSG;

/******************************************************************************
 GET_PREFERRED_RESOURCE

Description:  Return preferred partner resource name rep needs to access order

******************************************************************************/
FUNCTION GET_PREFERRED_RESOURCE
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_preferred_partner_count         int := 0;
v_PREFERRED_RESOURCE		  varchar2 (100);

BEGIN

        select count(*) into v_preferred_partner_count
	from
	(
	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE c.customer_id = IN_CUSTOMER_ID
	AND c.customer_id = o.customer_id
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'

	UNION

	select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE
	FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
	AND b.BUYER_ID = o.BUYER_ID
	AND o.order_guid = od.order_guid
	AND spr.source_code = od.source_code
	AND pp.program_name = spr.program_name
	AND pm.partner_name =   pp.partner_name
	AND pm.PREFERRED_PARTNER_FLAG = 'Y'
	);

	IF v_preferred_partner_count = 0 THEN
	  select count(distinct pm.partner_name) into v_preferred_partner_count
	  from ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm, clean.order_details od, clean.customer c
	  where pp.program_name = spr.program_name
 	  and pm.partner_name = pp.partner_name 
	  and spr.source_code = od.source_code
	  and c.customer_id = od.RECIPIENT_ID
	  and c.customer_id = in_customer_id;
	END IF;

IF v_preferred_partner_count > 1 THEN
  v_PREFERRED_RESOURCE  := 'USAA';
ELSIF v_preferred_partner_count = 1 THEN

        BEGIN
          select preferred_resource into v_PREFERRED_RESOURCE
	  from
	  (
	  select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_resource
	  FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	  WHERE c.customer_id = IN_CUSTOMER_ID
	  AND c.customer_id = o.customer_id
	  AND o.order_guid = od.order_guid
	  AND spr.source_code = od.source_code
	  AND pp.program_name = spr.program_name
	  AND pm.partner_name =   pp.partner_name
	  AND pm.PREFERRED_PARTNER_FLAG = 'Y'

	  UNION

	  select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  preferred_resource
	  FROM scrub.buyers  b, scrub.orders o, scrub.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
	  WHERE b.CLEAN_CUSTOMER_ID = IN_CUSTOMER_ID
	  AND b.BUYER_ID = o.BUYER_ID
	  AND o.order_guid = od.order_guid
	  AND spr.source_code = od.source_code
	  AND pp.program_name = spr.program_name
	  AND pm.partner_name =   pp.partner_name
	  AND pm.PREFERRED_PARTNER_FLAG = 'Y'
	  );
	  
	  EXCEPTION WHEN NO_DATA_FOUND THEN
            select DISTINCT pm.PREFERRED_PROCESSING_RESOURCE  into v_PREFERRED_RESOURCE
      	    FROM clean.customer c, clean.orders o, clean.order_details od, ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
      	    WHERE c.customer_id = IN_CUSTOMER_ID
            AND o.order_guid = od.order_guid
            AND c.customer_id = od.RECIPIENT_ID
            AND spr.source_code = od.source_code
            AND pp.program_name = spr.program_name
            AND pm.partner_name =   pp.partner_name
            AND pm.PREFERRED_PARTNER_FLAG = 'Y';
        END;

END IF;

RETURN v_PREFERRED_RESOURCE;

END GET_PREFERRED_RESOURCE;


PROCEDURE GET_EMAIL_BY_CUSTOMERID
(
IN_CUSTOMER_ID                IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving email information
        for the given customer id.

Input:
        customer_id                     number

Output:
        cursor containing email information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        select distinct e.email_address from
                    clean.customer_email_ref cef,
                    clean.email e
        where
        cef.customer_id = in_customer_id
        and cef.email_id = e.email_id;       

END GET_EMAIL_BY_CUSTOMERID; 

PROCEDURE GET_PRO_ORDERS
(
  IN_PRO_ORDER_NUMBER           IN 		VARCHAR2,
  OUT_CUR                    	OUT 	TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving confirmation numbers
        for the given pro web order number 

Input:
        IN_PRO_ORDER_NUMBER                     varchar2

Output:
        cursor containing confirmation numbers/ external order numbers

-----------------------------------------------------------------------------*/
 v_query_str varchar(400);
 
BEGIN
    
    v_query_str := 'SELECT  distinct confirmation_number FROM ptn_pi.partner_order po join ptn_pi.partner_order_detail pod
    on pod.partner_order_id = po.partner_order_id and po.partner_id = ''PRO'' 
    join scrub.order_details od on od.external_order_number = pod.confirmation_number 
    where  po.partner_order_number like ''' || IN_PRO_ORDER_NUMBER || '%''';
	open OUT_CUR for v_query_str;
	
END GET_PRO_ORDERS;


PROCEDURE SEARCH_PRO_CUSTOMER
(
IN_PRO_ORDER_NUMBER             IN VARCHAR2, 
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the results of the
        customer search.
-----------------------------------------------------------------------------*/
 
v_sql                   varchar2 (2000);
v_cust_str_id           varchar2(32767); 
id_tab                  id_list_pkg.number_tab_typ; 
sql_cur                 types.ref_cursor;
count_cur               types.ref_cursor;
v_begin                 number := in_start_position;
v_end                   number := in_start_position + in_max_number_returned - 1;


BEGIN

	out_id_count := 0;

	v_sql := '	SELECT  distinct c.customer_id from clean.customer c 
			  join clean.orders o on c.customer_id = o.customer_id  
				join ptn_pi.partner_order po on po.master_order_number = o.master_order_number
				and po.partner_id = ''PRO'' and po.partner_order_number like ''' || IN_PRO_ORDER_NUMBER || '%'''; 

    open sql_cur for v_sql;
        fetch sql_cur bulk collect into id_tab limit 200;
    close sql_cur;
     

    if v_end >= id_tab.count or in_max_number_returned is null then
        v_end := id_tab.count;
    end if;
    
	if id_tab.count > 0 and v_begin <= id_tab.count then  
		for x in v_begin..v_end loop
			v_cust_str_id := v_cust_str_id || id_tab(x) || ',';
		end loop;
	end if;

    v_cust_str_id := coalesce (substr(v_cust_str_id, 1, length(v_cust_str_id) - 1), '-1');
    
  	
	if id_tab.count > 0 then
        out_id_count := id_tab.count;
		
		open out_cur for
	   'WITH oc AS (
				select  coalesce (o.customer_id, od.customer_id) customer_id,
						sum (case when o.order_guid is not null and od.order_guid is null then 1
								  when od.order_guid is not null and o.order_guid is null then 1
								  when o.order_guid = od.order_guid then 1
								  when o.order_guid is not null and od.order_guid is not null and o.order_guid <> od.order_guid then 2
								  else 0
							 end) order_count
				from
						(select o.customer_id, o.order_guid from clean.orders o where o.customer_id in (' || v_cust_str_id || ')) o
				full outer join
						(select od.recipient_id customer_id, od.order_guid from clean.order_details od where od.recipient_id in (' || v_cust_str_id || ')) od
				on o.customer_id = od.customer_id
				group by coalesce (o.customer_id, od.customer_id)
				) ' ||
	   'SELECT  coalesce (decode (oc.order_count, 1, (select o.customer_id from orders o join order_details od on o.order_guid = od.order_guid where od.recipient_id = c.customer_id), null), c.customer_id) customer_id,
				c.last_name,
				c.first_name,
				c.address_1 || '' '' || c.address_2 address,
				c.city,
				c.state,
				decode (c.buyer_indicator, ''Y'', to_char ((select max (order_date) from orders o where o.customer_id = c.customer_id), ''mm/dd/yyyy''), ''Recipient'') last_order_date,
				decode (oc.order_count, 1, (select order_query_pkg.get_scrub_status_ind (od.order_disp_code) from order_details od where od.recipient_id = c.customer_id), null) scrub_status,
				decode (oc.order_count, 1, (select od.external_order_number from order_details od where od.recipient_id = c.customer_id), null) order_number,
				upper (c.last_name) upper_last_name,
				upper (c.first_name) upper_first_name,
				customer_query_pkg.is_preferred_customer (c.customer_id) preferred_customer,
		customer_query_pkg.is_ftd_customer (c.customer_id) ftd_customer,
		customer_query_pkg.is_preferred_recipient (c.customer_id) preferred_recipient,
		customer_query_pkg.is_ftd_recipient (c.customer_id) ftd_recipient,
		customer_query_pkg.GET_PREFERRED_MASKED_NAME (c.customer_id) preferred_masked_name,
		customer_query_pkg.GET_PREFERRED_WARNING_MSG (c.customer_id) preferred_warning_message,
		customer_query_pkg.GET_PREFERRED_RESOURCE (c.customer_id) preferred_resource,
		customer_query_pkg.is_usaa_customer (c.customer_id) usaa_customer
		FROM    customer c
		LEFT OUTER JOIN oc
		ON      c.customer_id = oc.customer_id
		WHERE   c.customer_id in (' || v_cust_str_id || ') ';
  
  else
        -- open an empty cursor so the application does not abnormally terminate
       
        open out_cur for
                SELECT  c.customer_id,
                        c.last_name,
                        c.first_name,
                        c.address_1 || ' ' || c.address_2 address,
                        c.city,
                        c.state,
                        null last_order_date,
                        null scrub_status,
                        null order_number,
                        null upper_last_name,
                        null upper_first_name
                FROM    customer c
                WHERE   c.customer_id = -1;
   end if;
   
   Exception when others then
   
    open out_cur for
                SELECT  c.customer_id,
                        c.last_name,
                        c.first_name,
                        c.address_1 || ' ' || c.address_2 address,
                        c.city,
                        c.state,
                        null last_order_date,
                        null scrub_status,
                        null order_number,
                        null upper_last_name,
                        null upper_first_name
                FROM    customer c
                WHERE   c.customer_id = -1;

END SEARCH_PRO_CUSTOMER;

/******************************************************************************
 IS_USAA_CUSTOMER 

Description:  Check to see if customer has any USAA orders associated to them.  

******************************************************************************/
FUNCTION IS_USAA_CUSTOMER 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count			int := 0;
v_IS_USAA_CUSTOMER  varchar2 (1) := 'N';

BEGIN

  select count(*) INTO v_count
  FROM    clean.customer c
                JOIN    clean.orders o
                ON      c.customer_id = o.customer_id
                JOIN    clean.order_details od
                ON      o.order_guid = od.order_guid
                WHERE   c.customer_id = IN_CUSTOMER_ID
   AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                 and pm.PREFERRED_PROCESSING_RESOURCE = 'USAA'
                );

IF v_count = 0 THEN
  	select count(*) into v_count 
  	FROM    scrub.buyers b
                JOIN    scrub.orders o
                ON      b.buyer_id = o.buyer_id
                JOIN    scrub.order_details od
                ON      o.order_guid = od.order_guid
                WHERE   b.clean_customer_id = in_customer_id
  	AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
               	 and pm.PREFERRED_PROCESSING_RESOURCE = 'USAA'
               );  
END IF;

IF v_count > 0 THEN
  v_IS_USAA_CUSTOMER  := 'Y';
END IF;

RETURN v_IS_USAA_CUSTOMER;

END IS_USAA_CUSTOMER;

/******************************************************************************
 IS_USAA_RECIPIENT

Description:  Check to see if recipient has any USAA orders associated to them.  

******************************************************************************/
FUNCTION IS_USAA_RECIPIENT 
(
IN_CUSTOMER_ID       IN   CUSTOMER.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count			int := 0;
v_IS_USAA_RECIPIENT varchar2 (1) := 'N';

BEGIN

  select count(*) into v_count
  FROM    clean.customer c
                JOIN    clean.order_details od
                ON      c.customer_id = od.RECIPIENT_ID
                WHERE   c.customer_id = in_customer_id
  AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
                 and pm.PREFERRED_PROCESSING_RESOURCE = 'USAA'
               );
               
IF v_count = 0 THEN
  	select count(*) into v_count 
  	FROM    scrub.recipients r
                JOIN    scrub.order_details od
                ON      r.recipient_id = od.recipient_id
                WHERE   r.recipient_id = in_customer_id
  	AND EXISTS 
               ( SELECT 1 FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = od.source_code
               	 and pm.PREFERRED_PROCESSING_RESOURCE = 'USAA'
               );  
END IF;

IF v_count > 0 THEN
  v_IS_USAA_RECIPIENT  := 'Y';
END IF;

RETURN v_IS_USAA_RECIPIENT;

END IS_USAA_RECIPIENT;

END;

.
/
