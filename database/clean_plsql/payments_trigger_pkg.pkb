CREATE OR REPLACE PACKAGE BODY clean.payments_trigger_pkg AS 
   TYPE row_tbl IS TABLE OF PAYMENTS.PAYMENT_ID%TYPE; 
   modified_rows row_tbl; 
 
   PROCEDURE before_stmt_trigger IS 
   BEGIN 
      modified_rows := row_tbl(); 
   END before_stmt_trigger; 
 
   PROCEDURE for_each_row_trigger(in_payment_id IN PAYMENTS.PAYMENT_ID%TYPE) IS 
   BEGIN 
      modified_rows.extend;
      modified_rows(modified_rows.last) := in_payment_id; 
   END for_each_row_trigger; 
 
   PROCEDURE after_stmt_trigger IS  
      V_COUNT NUMBER;
      V_ORDER_GUID PAYMENTS.ORDER_GUID%TYPE;
      V_ADDITIONAL_BILL_ID PAYMENTS.ADDITIONAL_BILL_ID%TYPE;
      V_BILL_DATE PAYMENTS.BILL_DATE%TYPE;
   
   BEGIN 
      
      if modified_rows.count > 0 then
      FOR i IN modified_rows.first .. modified_rows.last LOOP 
         SELECT order_guid, additional_bill_id, bill_date 
         INTO v_order_guid, v_additional_bill_id, v_bill_date 
         FROM payments 
         WHERE payment_id = modified_rows(i); 
         
	-- USECASE 23033 - UPDATE ORDER_BILLS BILL_STATUS
	select count(*) 
	into v_count 
	from payments p
	where p.payment_indicator = 'P' 
	and p.order_guid = v_order_guid 
	and ((v_additional_bill_id is null and p.additional_bill_id is null)
	      or
	     (v_additional_bill_id is not null and v_additional_bill_id = p.additional_bill_id)
	    )
	and (p.bill_status not in ('Billed','Settled') or p.bill_status is null);

	IF v_count = 0 THEN			
		update order_bills
		SET bill_status = 'Billed', bill_date = v_bill_date, updated_on = SYSDATE, updated_by = USER
		where order_bill_id in
		(
		select order_bill_id 
		from order_bills ob 
		join order_details od on ob.order_detail_id = od.order_detail_id
		where  od.order_guid = v_order_guid 
		and ob.bill_status = 'Unbilled'
		and ((v_additional_bill_id is null and ob.additional_bill_indicator = 'N')
		     or (v_additional_bill_id = ob.order_bill_id and ob.additional_bill_indicator = 'Y')
		    )
		)
		and bill_status = 'Unbilled'
		;

	END IF;
      END LOOP; 
      modified_rows.delete;
      end if;
   END after_stmt_trigger; 
END payments_trigger_pkg; 
/