CREATE OR REPLACE
TRIGGER clean.email_trg
AFTER UPDATE OF subscribe_status OR INSERT ON clean.email
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
   IF (:new.subscribe_status IS NOT NULL and :old.subscribe_status IS NULL) OR
      (:new.subscribe_status <> :old.subscribe_status) THEN
           INSERT INTO clean.email_history (email_id, company_id, email_address, updated_on, updated_by, subscribe_status)
           VALUES (:new.email_id, :new.company_id, :new.email_address, sysdate, :new.updated_by, :new.subscribe_status);
   END IF;
END email_trg;
.
/
