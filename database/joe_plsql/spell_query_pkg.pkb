CREATE OR REPLACE PACKAGE BODY JOE.SPELL_QUERY_PKG as

FUNCTION GET_DICTIONARY
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_DICTIONARY
-- Type:    Function
-- Syntax:  GET_DICTIONARY
-- Returns: all records in the JOE.SPELL_CHECK_WORDS table
--
--==============================================================================
AS
  out_cur types.ref_cursor;
BEGIN
  OPEN out_cur FOR
    SELECT s.WORD FROM JOE.SPELL_CHECK_WORDS s ORDER BY s.WORD;

  return out_cur;
END GET_DICTIONARY;

END SPELL_QUERY_PKG;
/
