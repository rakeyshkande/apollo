CREATE OR REPLACE PACKAGE BODY JOE.SPELL_MAINT_PKG as

PROCEDURE SPELL_MAINT_TEST
(
   OUT_STATUS                      OUT VARCHAR2,
   OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is is a stub procedure in order to get the
             initial package definition to compile.
             
Input:  None

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN
    out_status := 'Y';
    out_message := null;

END SPELL_MAINT_TEST;


END SPELL_MAINT_PKG;
/
