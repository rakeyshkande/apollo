CREATE OR REPLACE TRIGGER joe.cross_sell_master_$
AFTER INSERT OR UPDATE OR DELETE ON joe.cross_sell_master 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into joe.cross_sell_master$ 
           (cross_sell_master_id,
			      product_id,
			      upsell_master_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.cross_sell_master_id,                        
         :NEW.product_id,          
         :NEW.upsell_master_id,              
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',v_current_timestamp);

   ELSIF UPDATING  THEN
      insert into joe.cross_sell_master$ 
           (cross_sell_master_id,
			      product_id,
			      upsell_master_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.cross_sell_master_id,                        
         :OLD.product_id,          
         :OLD.upsell_master_id,              
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',v_current_timestamp);

      insert into joe.cross_sell_master$ 
           (cross_sell_master_id,
			      product_id,
			      upsell_master_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.cross_sell_master_id,                        
         :NEW.product_id,          
         :NEW.upsell_master_id,              
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',v_current_timestamp);


   ELSIF DELETING  THEN

      insert into joe.cross_sell_master$ 
           (cross_sell_master_id,
			      product_id,
			      upsell_master_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.cross_sell_master_id,                        
         :OLD.product_id,          
         :OLD.upsell_master_id,              
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',v_current_timestamp);


   END IF;

END;
/
