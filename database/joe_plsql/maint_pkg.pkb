create or replace package body joe.MAINT_PKG is

PROCEDURE DELETE_ALL_CARD_MESSAGES (
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: delete all records in the JOE.CARD_MESSAGE table

Input:  product_id

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN
    DELETE FROM JOE.CARD_MESSAGE;
    
    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END DELETE_ALL_CARD_MESSAGES;

PROCEDURE INSERT_CARD_MESSAGE (
    IN_DISPLAY_SEQUENCE              IN NUMBER,
    IN_MESSAGE_TXT                   IN VARCHAR2,
    IN_ACTIVE_FLAG                   IN VARCHAR2,
    IN_USER_ID                       IN VARCHAR2,
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: insert a record into the JOE.CARD_MESSAGE table

Input:  product_id

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN
    INSERT INTO JOE.CARD_MESSAGE m (
        m.DISPLAY_SEQ, 
        m.MESSAGE_TXT, 
        m.ACTIVE_FLAG, 
        m.CREATED_BY, 
        m.CREATED_ON, 
        m.UPDATED_BY, 
        m.UPDATED_ON
    ) VALUES (
        IN_DISPLAY_SEQUENCE, 
        IN_MESSAGE_TXT, 
        IN_ACTIVE_FLAG, 
        IN_USER_ID, 
        SYSDATE, 
        IN_USER_ID, 
        SYSDATE
    );
    
    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_CARD_MESSAGE;

end MAINT_PKG;
/
