CREATE OR REPLACE TRIGGER joe.cross_sell_detail_$
AFTER INSERT OR UPDATE OR DELETE ON joe.cross_sell_detail 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into joe.cross_sell_detail$ 
           (cross_sell_master_id,
            display_order_seq_num,
			      cross_sell_product_id,
			      cross_sell_upsell_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.cross_sell_master_id,
         :NEW.display_order_seq_num,
         :NEW.cross_sell_product_id,          
         :NEW.cross_sell_upsell_id,              
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',v_current_timestamp);

   ELSIF UPDATING  THEN
      insert into joe.cross_sell_detail$ 
           (cross_sell_master_id,
            display_order_seq_num,
			      cross_sell_product_id,
			      cross_sell_upsell_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.cross_sell_master_id,
         :OLD.display_order_seq_num,
         :OLD.cross_sell_product_id,          
         :OLD.cross_sell_upsell_id,              
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',v_current_timestamp);

      insert into joe.cross_sell_detail$ 
           (cross_sell_master_id,
            display_order_seq_num,
			      cross_sell_product_id,
			      cross_sell_upsell_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.cross_sell_master_id,
         :NEW.display_order_seq_num,
         :NEW.cross_sell_product_id,          
         :NEW.cross_sell_upsell_id,              
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',v_current_timestamp);


   ELSIF DELETING  THEN

      insert into joe.cross_sell_detail$ 
           (cross_sell_master_id,
            display_order_seq_num,
			      cross_sell_product_id,
			      cross_sell_upsell_id,
			      created_by,
			      created_on,
			      updated_by,
			      updated_on,
			      OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.cross_sell_master_id,
         :OLD.display_order_seq_num,
         :OLD.cross_sell_product_id,          
         :OLD.cross_sell_upsell_id,              
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',v_current_timestamp);

   END IF;

END;
/
