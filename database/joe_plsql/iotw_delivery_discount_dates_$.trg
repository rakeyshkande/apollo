CREATE OR REPLACE
TRIGGER joe.iotw_delivery_discount_dates_$
AFTER INSERT OR UPDATE OR DELETE ON joe.iotw_delivery_discount_dates 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO joe.iotw_delivery_discount_dates$ (
         iotw_id,          
         discount_date,    
         created_by,       
         created_on,       
         updated_by,       
         updated_on,       
         OPERATION$, TIMESTAMP$       
      ) VALUES (
         :NEW.IOTW_ID,                        
         :NEW.DISCOUNT_DATE,                    
         :NEW.CREATED_BY,                     
         :NEW.CREATED_ON,                     
         :NEW.UPDATED_BY,                     
         :NEW.UPDATED_ON,                     
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO joe.iotw_delivery_discount_dates$ (
         iotw_id,          
         discount_date,    
         created_by,       
         created_on,       
         updated_by,       
         updated_on,       
         OPERATION$, TIMESTAMP$       
      ) VALUES (
         :OLD.IOTW_ID,                        
         :OLD.DISCOUNT_DATE,                    
         :OLD.CREATED_BY,                     
         :OLD.CREATED_ON,                     
         :OLD.UPDATED_BY,                     
         :OLD.UPDATED_ON,                     
         'UPD_OLD',SYSDATE);
   
      INSERT INTO joe.iotw_delivery_discount_dates$ (
         iotw_id,          
         discount_date,    
         created_by,       
         created_on,       
         updated_by,       
         updated_on,       
         OPERATION$, TIMESTAMP$       
      ) VALUES (
         :NEW.IOTW_ID,                        
         :NEW.DISCOUNT_DATE,                    
         :NEW.CREATED_BY,                     
         :NEW.CREATED_ON,                     
         :NEW.UPDATED_BY,                     
         :NEW.UPDATED_ON,                     
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO joe.iotw_delivery_discount_dates$ (
         iotw_id,          
         discount_date,    
         created_by,       
         created_on,       
         updated_by,       
         updated_on,       
         OPERATION$, TIMESTAMP$       
      ) VALUES (
         :OLD.IOTW_ID,                        
         :OLD.DISCOUNT_DATE,                    
         :OLD.CREATED_BY,                     
         :OLD.CREATED_ON,                     
         :OLD.UPDATED_BY,                     
         :OLD.UPDATED_ON,                     
         'DEL',SYSDATE);

   END IF;

END;
/
