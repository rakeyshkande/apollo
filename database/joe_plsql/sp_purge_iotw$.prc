CREATE OR REPLACE PROCEDURE JOE.SP_PURGE_IOTW$ 
   (IN_KEEP_DAYS           IN    NUMBER      DEFAULT 90,
    OUT_STATUS             OUT   VARCHAR2,
    OUT_MESSAGE            OUT   VARCHAR2) 
AS
/*-----------------------------------------------------------------------------

 Name:    SP_PURGE_IOTW$
 Type:    Procedure
 Syntax:  SP_PURGE_IOTW$(num_days, status, err_msg)

Description:
        This stored procedure will purge the iotw shadow/audit
        table so that only X days are kept (as specified in the input parameter).
        
Input:
        in_keep_days        number      Number of days of data to retain
        
Output:
        out_status          varchar2    Y if Successful, N if Error
        out_message         varchar2    loaded with oracle error if status=N

-----------------------------------------------------------------------------*/

--
-- Get rowids of the rows to purge
--
cursor purge_cursor is
select rowid
from joe.IOTW$
where timestamp$ < (sysdate - in_keep_days);

v_commit_cnt   number := 0;

BEGIN

  OUT_STATUS := 'Y';

   for purge_rec in purge_cursor LOOP
     delete from joe.IOTW$
     where rowid = purge_rec.rowid;

     v_commit_cnt := v_commit_cnt + 1;
     
     if v_commit_cnt > 50000 then
        commit;
        v_commit_cnt := 0;
     end if;
   END LOOP;

   -- everything went good!
   commit;   


/* Trouble? */
EXCEPTION
   when others then
      ROLLBACK;
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'UNEXPECTED ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   

END SP_PURGE_IOTW$;
/