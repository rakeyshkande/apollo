CREATE OR REPLACE PACKAGE BODY JOE."OE_QUERY_PKG" as

PROCEDURE AJAX_GET_COUNTRIES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the country_master records

Input:  None

Output: cursor containing country info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

	"GLOBAL"."GLOBAL_PKG".GET_COUNTRY_LIST_OE(OUT_CUR);
	
    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_COUNTRIES;

PROCEDURE AJAX_GET_ORDER_LANGUAGES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the language_id records

Input:  None

Output: cursor containing language info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

	"GLOBAL"."GLOBAL_PKG".GET_ORDER_LANGUAGES(OUT_CUR);
	
    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_ORDER_LANGUAGES;

PROCEDURE AJAX_GET_STATES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the state_master records

Input:  None

Output: cursor containing state info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    "GLOBAL"."GLOBAL_PKG".GET_STATE_LIST_OE(OUT_CUR);

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_STATES;

PROCEDURE AJAX_GET_DNIS
(
IN_DNIS_ID                       IN VARCHAR2,
OUT_CUR1                        OUT TYPES.REF_CURSOR,
OUT_CUR2                        OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves the DNIS row for the given dnis_id

Input:  dnis_id

Output: cursor containing dnis info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

out_dnis_id                FTD_APPS.DNIS.DNIS_ID%TYPE := NULL;
out_description            FTD_APPS.DNIS.DESCRIPTION%TYPE := NULL;
out_origin                 FTD_APPS.DNIS.ORIGIN%TYPE := NULL;
out_status_flag            FTD_APPS.DNIS.STATUS_FLAG%TYPE := NULL;
out_oe_flag                FTD_APPS.DNIS.OE_FLAG%TYPE := NULL;
out_yellow_pages_flag      FTD_APPS.DNIS.YELLOW_PAGES_FLAG%TYPE := NULL;
out_default_source_code    FTD_APPS.DNIS.DEFAULT_SOURCE_CODE%TYPE := NULL;
out_script_code            FTD_APPS.DNIS.SCRIPT_CODE%TYPE := NULL;
out_company_id             FTD_APPS.DNIS.COMPANY_ID%TYPE := NULL;
out_company_name           FTD_APPS.COMPANY_MASTER.COMPANY_NAME%TYPE := NULL;

BEGIN

    SELECT DNIS.DNIS_ID,
        DNIS.DESCRIPTION,
        DNIS.ORIGIN,
        DNIS.STATUS_FLAG,
        DNIS.OE_FLAG,
        DNIS.YELLOW_PAGES_FLAG,
        DNIS.SCRIPT_CODE,
        DNIS.DEFAULT_SOURCE_CODE,
        DNIS.COMPANY_ID,
        CM.COMPANY_NAME
    INTO
        OUT_DNIS_ID,
        OUT_DESCRIPTION,
        OUT_ORIGIN,
        OUT_STATUS_FLAG,
        OUT_OE_FLAG,
        OUT_YELLOW_PAGES_FLAG,
        OUT_SCRIPT_CODE,
        OUT_DEFAULT_SOURCE_CODE,
        OUT_COMPANY_ID,
        OUT_COMPANY_NAME
    FROM FTD_APPS.DNIS DNIS, FTD_APPS.COMPANY_MASTER CM
    WHERE DNIS.DNIS_ID = IN_DNIS_ID
        AND DNIS.STATUS_FLAG = 'A'
        AND DNIS.COMPANY_ID = CM.COMPANY_ID;

    OPEN OUT_CUR1 FOR
        SELECT OUT_DNIS_ID          AS "DNIS_ID",
        OUT_DESCRIPTION             AS "DESCRIPTION",
        OUT_ORIGIN                  AS "ORIGIN",
        OUT_STATUS_FLAG             AS "STATUS_FLAG",
        OUT_OE_FLAG                 AS "OE_FLAG",
        OUT_YELLOW_PAGES_FLAG       AS "YELLOW_PAGES_FLAG",
        OUT_SCRIPT_CODE             AS "SCRIPT_CODE",
        OUT_DEFAULT_SOURCE_CODE     AS "DEFAULT_SOURCE_CODE",
        OUT_COMPANY_ID              AS "COMPANY_ID",
        OUT_COMPANY_NAME            AS "COMPANY_NAME"
    FROM DUAL;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    IF OUT_DEFAULT_SOURCE_CODE IS NOT NULL THEN
        AJAX_GET_SOURCE_CODE (OUT_DEFAULT_SOURCE_CODE, OUT_DNIS_ID, OUT_CUR2, OUT_STATUS, OUT_MESSAGE);
    ELSE
        OPEN out_cur2 for select * from dual where dummy = 'Y';
    END IF;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        OPEN out_cur1 for select * from dual where dummy = 'Y';
        OPEN out_cur2 for select * from dual where dummy = 'Y';
        OUT_STATUS := 'Y';
        OUT_MESSAGE := '';
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END AJAX_GET_DNIS;

PROCEDURE AJAX_GET_INTRO_DATA
(
OUT_CUR1                        OUT TYPES.REF_CURSOR,
OUT_CUR2                        OUT TYPES.REF_CURSOR,
OUT_CUR3                        OUT TYPES.REF_CURSOR,
OUT_CUR4                        OUT TYPES.REF_CURSOR,
OUT_CUR5                        OUT TYPES.REF_CURSOR,
OUT_CUR6                        OUT TYPES.REF_CURSOR,
OUT_CUR7                        OUT TYPES.REF_CURSOR,
OUT_CUR8                        OUT TYPES.REF_CURSOR,
OUT_CUR9                        OUT TYPES.REF_CURSOR,
OUT_CUR10                       OUT TYPES.REF_CURSOR,
OUT_CUR11                       OUT TYPES.REF_CURSOR,
OUT_CUR12                       OUT TYPES.REF_CURSOR,
OUT_CUR13						OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    AJAX_GET_COUNTRIES ( OUT_CUR1, OUT_STATUS, OUT_MESSAGE);
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_STATES (OUT_CUR2, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_OCCASIONS (OUT_CUR3, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_ADDONS (OUT_CUR4, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_OCCASION_ADDONS (OUT_CUR5, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_PAYMENT_METHODS (OUT_CUR6, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_ADDRESS_TYPES (OUT_CUR7, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_CARD_MESSAGES (OUT_CUR8, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_PRODUCT_FAVORITES (OUT_CUR9, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_GLOBAL_PARMS (OUT_CUR10, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        AJAX_GET_OE_SCRIPT_MASTER ('Default', OUT_CUR11, OUT_STATUS, OUT_MESSAGE);
    END IF;
    IF OUT_STATUS = 'Y' THEN
        OPEN OUT_CUR12 FOR
            select price_header_id, description
            from ftd_apps.price_header
            order by price_header_id;
    END IF;
	IF OUT_STATUS = 'Y' THEN
		AJAX_GET_ORDER_LANGUAGES (OUT_CUR13, OUT_STATUS, OUT_MESSAGE);
	END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_INTRO_DATA;

PROCEDURE AJAX_GET_SOURCE_CODE
(
IN_SOURCE_CODE                   IN VARCHAR2,
IN_DNIS_ID                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves the SOURCE_MASTER row for the given source code

Input:  source_code

Output: cursor containing source_master info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        SELECT SM.SOURCE_CODE,
            SM.DESCRIPTION,
            SM.SNH_ID,
            SM.PRICE_HEADER_ID,
            SM.START_DATE,
            SM.END_DATE,
            SM.PAYMENT_METHOD_ID,
            SM.DISCOUNT_ALLOWED_FLAG,
            SM.COMPANY_ID,
            SPR.PROGRAM_NAME PARTNER_ID,
            PP.MEMBERSHIP_DATA_REQUIRED,
            SM.IOTW_FLAG,
            SM.INVOICE_PASSWORD,
            PM.PREFERRED_PROCESSING_RESOURCE RESOURCE_ID,
            SM.ADD_ON_FREE_ID,
            A.ADDON_TEXT,
            PM.PARTNER_NAME,
            SM.RECPT_LOCATION_TYPE,
            SM.RECPT_BUSINESS_NAME,
            SM.RECPT_LOCATION_DETAIL,
            SM.RECPT_ADDRESS,
            SM.RECPT_ZIP_CODE,
            SM.RECPT_CITY,
            SM.RECPT_STATE_ID,
            SM.RECPT_COUNTRY_ID,
            SM.RECPT_PHONE,
            SM.RECPT_PHONE_EXT,
            SM.CUST_FIRST_NAME,
            SM.CUST_LAST_NAME,
            SM.CUST_DAYTIME_PHONE,
            SM.CUST_DAYTIME_PHONE_EXT,
            SM.CUST_EVENING_PHONE,
            SM.CUST_EVENING_PHONE_EXT,
            SM.CUST_ADDRESS,
            SM.CUST_ZIP_CODE,
            SM.CUST_CITY,
            SM.CUST_STATE_ID,
            SM.CUST_COUNTRY_ID,
            SM.CUST_EMAIL_ADDRESS,
            SM.ALLOW_FREE_SHIPPING_FLAG,
            SM.SOURCE_TYPE
        FROM FTD_APPS.DNIS,
            FTD_APPS.SOURCE_MASTER SM
        LEFT OUTER JOIN
        (
            select  sp.source_code, sp.program_name
            from    ftd_apps.source_program_ref sp
            where   sp.start_date =
                (
                    select  max(b.start_date)
                    from    ftd_apps.source_program_ref b
                    where   b.source_code = sp.source_code
                    and     b.start_date <= sysdate
                )
        ) spr
        ON sm.source_code = spr.source_code
        LEFT OUTER JOIN    ftd_apps.partner_program pp
        ON spr.program_name = pp.program_name
        LEFT OUTER JOIN    ftd_apps.partner_master pm
        ON pp.partner_name = pm.partner_name
        LEFT OUTER JOIN    ftd_apps.addon a
        ON sm.add_on_free_id = a.addon_id
        WHERE SM.SOURCE_CODE = IN_SOURCE_CODE
            AND TRUNC(SM.START_DATE) <= TRUNC(SYSDATE)
            AND TRUNC(DECODE(SM.END_DATE, NULL, SYSDATE, SM.END_DATE)) >= TRUNC(SYSDATE)
            AND DNIS.DNIS_ID = IN_DNIS_ID
            AND SM.COMPANY_ID = DNIS.SCRIPT_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_SOURCE_CODE;

PROCEDURE AJAX_GET_ZIP_CODE
(
IN_ZIP_CODE                      IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all zip_code rows for the given zip_code

Input:  zip_code

Output: cursor containing zip_code info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        SELECT zip_code_id,
            city,
            state_id
        FROM FTD_APPS.ZIP_CODE
        WHERE ZIP_CODE_id = IN_ZIP_CODE
        ORDER BY CITY;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_ZIP_CODE;

PROCEDURE AJAX_GET_OCCASIONS
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the occasion records

Input:  None

Output: cursor containing occasion info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        OCCASION_ID,
        DESCRIPTION,
        INDEX_ID,
        DISPLAY_ORDER
      FROM FTD_APPS.OCCASION
      WHERE ACTIVE = 'Y'
      ORDER BY DISPLAY_ORDER;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_OCCASIONS;

PROCEDURE AJAX_GET_ADDONS
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the addon records

Input:  None

Output: cursor containing add-on info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

cursor image_url IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'IMAGE_SERVER_URL';

v_image_url                VARCHAR2(100) := '';

BEGIN

    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    OPEN OUT_CUR FOR
      SELECT
        ADDON_ID,
        ADDON_TYPE,
        DESCRIPTION,
        PRICE,
        ADDON_TEXT,
        DECODE(ADDON_TYPE,'4', v_image_url || '/' || ADDON_ID || '.jpg', null) "small_image_url",
        DECODE(ADDON_TYPE,'4', v_image_url || '/' || ADDON_ID || '_1.jpg', null) "large_image_url"
      FROM FTD_APPS.ADDON
      ORDER BY ADDON_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_ADDONS;

PROCEDURE AJAX_GET_OCCASION_ADDONS
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the occasion_addon records

Input:  None

Output: cursor containing occasion_addon info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        OCCASION_ID,
        ADDON_ID
      FROM FTD_APPS.OCCASION_ADDON
      ORDER BY OCCASION_ID, ADDON_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_OCCASION_ADDONS;

PROCEDURE AJAX_GET_PAYMENT_METHODS
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the payment_method records

Input:  None

Output: cursor containing payment_method info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        PM.PAYMENT_METHOD_ID,
        PM.DESCRIPTION,
        PM.PAYMENT_TYPE,
        PM.CARD_ID,
        PM.HAS_EXPIRATION_DATE,
        PM.OVER_AUTH_ALLOWED_PCT,
        PM.ACTIVE_FLAG,
        PM.JOE_FLAG,
        PM. MOD10_FLAG,
        PM.REGEX_PATTERN,
        PMT.TEXT_TXT,
        PM.CSC_REQUIRED_FLAG
      FROM FTD_APPS.PAYMENT_METHODS PM,
        FTD_APPS.PAYMENT_METHODS_TEXT PMT
      WHERE PMT.PAYMENT_METHOD_ID = PM.PAYMENT_METHOD_ID
      ORDER BY PAYMENT_TYPE, PAYMENT_METHOD_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_PAYMENT_METHODS;

PROCEDURE AJAX_GET_ADDRESS_TYPES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the address_type records

Input:  None

Output: cursor containing address_type info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        ADT.ADDRESS_TYPE as ADDRESS_TYPE_CODE,
        ADT.DESCRIPTION,
        ADT.PROMPT_FOR_BUSINESS_FLAG,
        ADT.BUSINESS_LABEL_TXT,
        ADT.PROMPT_FOR_LOOKUP_FLAG,
        ADT.LOOKUP_LABEL_TXT,
        ADT.PROMPT_FOR_ROOM_FLAG,
        ADT.ROOM_LABEL_TXT,
        ADT.PROMPT_FOR_HOURS_FLAG,
        ADT.HOURS_LABEL_TXT,
        ADT.DEFAULT_FLAG
      FROM FRP.ADDRESS_TYPES ADT
      ORDER BY ADT.DEFAULT_FLAG ASC, ADT.SORT_ORDER, ADT.ADDRESS_TYPE;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_ADDRESS_TYPES;

PROCEDURE AJAX_GET_CARD_MESSAGES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the card_message records

Input:  None

Output: cursor containing card_message info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        CM.DISPLAY_SEQ,
        CM.MESSAGE_TXT
      FROM JOE.CARD_MESSAGE CM
      WHERE CM.ACTIVE_FLAG = 'Y'
      ORDER BY DISPLAY_SEQ;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_CARD_MESSAGES;

PROCEDURE AJAX_GET_PRODUCT_FAVORITES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the product_favorite records

Input:  None

Output: cursor containing product_favorite info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        PF.DISPLAY_SEQ,
        PF.PRODUCT_ID
      FROM JOE.PRODUCT_FAVORITE PF
      ORDER BY DISPLAY_SEQ;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_PRODUCT_FAVORITES;

PROCEDURE AJAX_GET_GLOBAL_PARMS
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the product_favorite records

Input:  None

Output: cursor containing product_favorite info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      SELECT
        GP.NAME,
        GP.VALUE
      FROM FRP.GLOBAL_PARMS GP
      WHERE GP.CONTEXT = 'OE_CONFIG'
        OR (GP.CONTEXT = 'FTDAPPS_PARMS'
            AND GP.NAME LIKE 'FLORAL_LABEL_%');

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_GLOBAL_PARMS;

PROCEDURE AJAX_GET_OE_SCRIPT_MASTER
(
IN_SCRIPT_TYPE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            SCRIPT_ID,
            SCRIPT_TXT
        FROM JOE.OE_SCRIPT_MASTER
        WHERE SCRIPT_TYPE_CODE = IN_SCRIPT_TYPE;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_OE_SCRIPT_MASTER;



PROCEDURE AJAX_GET_PRODUCT_DETAILS
(
IN_PRODUCT_ID                    IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR_PRODUCT                 OUT TYPES.REF_CURSOR,
OUT_CUR_SUBCODE                 OUT TYPES.REF_CURSOR,
OUT_CUR_UPSELL                  OUT TYPES.REF_CURSOR,
OUT_CUR_COLOR                   OUT TYPES.REF_CURSOR,
OUT_CUR_ADDON                   OUT TYPES.REF_CURSOR,
OUT_CUR_VASE                    OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

  AJAX_GET_PRODUCT_DETAILS
  (
    IN_PRODUCT_ID,
    IN_SOURCE_CODE,
    null,
    OUT_CUR_PRODUCT,
    OUT_CUR_SUBCODE,
    OUT_CUR_UPSELL,
    OUT_CUR_COLOR,
    OUT_CUR_ADDON,
    OUT_CUR_VASE,
    OUT_STATUS,
    OUT_MESSAGE     
  );


END AJAX_GET_PRODUCT_DETAILS;


PROCEDURE AJAX_GET_PRODUCT_DETAILS
(
IN_PRODUCT_ID                    IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
IN_VALID_UPSELL_IDS              IN VARCHAR2,
OUT_CUR_PRODUCT                 OUT TYPES.REF_CURSOR,
OUT_CUR_SUBCODE                 OUT TYPES.REF_CURSOR,
OUT_CUR_UPSELL                  OUT TYPES.REF_CURSOR,
OUT_CUR_COLOR                   OUT TYPES.REF_CURSOR,
OUT_CUR_ADDON                   OUT TYPES.REF_CURSOR,
OUT_CUR_VASE                    OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

cursor get_global_parm(in_context varchar2, in_name varchar2) IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = in_context
             and name = in_name;

cursor get_snh_by_id(in_snh_id varchar2) IS
    SELECT   first_order_domestic
    FROM     FTD_APPS.SNH
    WHERE    SNH_ID = in_snh_id;

cursor image_url IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'IMAGE_SERVER_URL';

cursor get_preferred_partner_flag IS
    SELECT   distinct pm.preferred_partner_flag
    FROM     ftd_apps.source_master sm,
             ftd_apps.source_program_ref spr,
             ftd_apps.partner_program pp,
             ftd_apps.partner_master pm
    WHERE    sm.source_code = IN_SOURCE_CODE
             AND spr.source_code = sm.source_code
             AND pp.program_name = spr.program_name
             AND pm.partner_name = pp.partner_name;

cursor get_add_on_free_id is
    SELECT   add_on_free_id
    FROM     ftd_apps.source_master
    WHERE    source_code = IN_SOURCE_CODE;

v_pricingCode              FTD_APPS.SOURCE_MASTER.PRICE_HEADER_ID%TYPE := NULL;
v_snh_id                   FTD_APPS.SOURCE_MASTER.SNH_ID%TYPE := NULL;
v_subCode                  FTD_APPS.PRODUCT_SUBCODES.PRODUCT_SUBCODE_ID%TYPE := NULL;
v_upsell_master_id         VARCHAR2(100) := UPPER(IN_PRODUCT_ID);
out_productID              VARCHAR2(100) := UPPER(IN_PRODUCT_ID);
v_global_chocolate_flag    VARCHAR2(10) := 'Y';
v_image_url                VARCHAR2(100) := '';
v_shipping_key             VARCHAR2(100);
v_price                    NUMBER;
v_ship_method_carrier      VARCHAR2(100) := 'N';
v_shipping_key_detail_id   VARCHAR2(100) := 'XX';
v_product_type             varchar2(100) := '';
v_global_freshcut_flag     VARCHAR2(100) := 'N';
v_freshcut_value           VARCHAR2(100) := '0.00';
v_freshcut_saturday_value  VARCHAR2(100) := '0.00';
v_preferred_partner_flag   VARCHAR2(100) := 'N';
v_add_on_free_id           VARCHAR2(100) := '';

BEGIN

    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_SUBCODE_ID, PRODUCT_ID
        INTO v_subCode, out_productID
        FROM FTD_APPS.PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = out_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO out_productID
          FROM FTD_APPS.PRODUCT_MASTER
          WHERE NOVATOR_ID = out_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO out_productID
            FROM FTD_APPS.PRODUCT_MASTER
            WHERE PRODUCT_ID = out_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END; 
    END;

     -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICE_HEADER_ID, SNH_ID
        INTO v_pricingCode, v_snh_id
        FROM FTD_APPS.SOURCE_MASTER
        WHERE SOURCE_CODE = in_source_code;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    BEGIN
        SELECT UPSELL_MASTER_ID
        INTO v_upsell_master_id
        FROM FTD_APPS.UPSELL_DETAIL
        WHERE UPSELL_DETAIL_ID = out_productID;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    begin

        select shipping_key, standard_price, ship_method_carrier, product_type
        into v_shipping_key, v_price, v_ship_method_carrier, v_product_type
        from ftd_apps.product_master
        where product_id = out_productID;

        if (v_ship_method_carrier = 'Y') then
            select shipping_detail_id into v_shipping_key_detail_id
            from ftd_apps.shipping_key_details
            where shipping_key_id = v_shipping_key
                and min_price <= v_price
                and max_price >= v_price;
        end if;

    exception when no_data_found then
        v_ship_method_carrier := 'N';
    end;

    -- Get the global chocolate flag from frp.global_parms
    open get_global_parm('FTDAPPS_PARMS', 'MOD_ORDER_CHOCOLATES_AVAILABLE');
    fetch get_global_parm into v_global_chocolate_flag;
    close get_global_parm;

    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    -- Get the preferred_partner_flag
    open get_preferred_partner_flag;
    fetch get_preferred_partner_flag into v_preferred_partner_flag;
    close get_preferred_partner_flag;

    -- Get the add_on_free_id
    open get_add_on_free_id;
    fetch get_add_on_free_id into v_add_on_free_id;
    close get_add_on_free_id;

    OPEN OUT_CUR_PRODUCT FOR
      SELECT
        PM.PRODUCT_ID,
        PM.NOVATOR_ID,
        PM.PRODUCT_NAME,
        PM.NOVATOR_NAME,
        PM.STATUS,
        PM.DELIVERY_TYPE,
        PM.PRODUCT_TYPE,
        PM.COLOR_SIZE_FLAG,
        PM.STANDARD_PRICE,
        PM.DELUXE_PRICE,
        PM.PREMIUM_PRICE,
        PM.VARIABLE_PRICE_MAX,
        PM.SHORT_DESCRIPTION,
        PM.LONG_DESCRIPTION,
        PM.ADD_ON_CARDS_FLAG,
        PM.ADD_ON_FUNERAL_FLAG,
        PM.EXCEPTION_CODE,
        TO_CHAR(PM.EXCEPTION_START_DATE, 'mm/dd/yyyy') EXCEPTION_START_DATE,
        TO_CHAR(PM.EXCEPTION_END_DATE, 'mm/dd/yyyy') EXCEPTION_END_DATE,
        PM.EXCEPTION_MESSAGE,
        PM.SHIP_METHOD_CARRIER,
        PM.SHIP_METHOD_FLORIST,
        PM.SHIPPING_KEY,
        PM.SHIPPING_SYSTEM,
        PM.WEBOE_BLOCKED,
        PM.CUSTOM_FLAG,
        PM.NO_TAX_FLAG,
        PM.DISCOUNT_ALLOWED_FLAG,
        PM.POPULARITY_ORDER_CNT,
        decode(substr(lower(psc.oe_description1), 1,4), 'none', null, PSC.OE_DESCRIPTION1 || PSC.OE_DESCRIPTION2) "second_choice",
        v_image_url || '/' || PM.NOVATOR_ID || '_a.jpg' "smallImage",
        v_image_url || '/' || PM.NOVATOR_ID || '_c.jpg' "largeImage",
        PM.OVER_21,
        PM.PERSONAL_GREETING_FLAG,
        PM.PREMIER_COLLECTION_FLAG,
        PM.ADD_ON_FREE_ID,
        PM.ALLOW_FREE_SHIPPING_FLAG
      FROM FTD_APPS.PRODUCT_MASTER PM,
        FTD_APPS.SOURCE_MASTER SM,
        FTD_APPS.PRODUCT_COMPANY_XREF PCX,
        FTD_APPS.PRODUCT_SECOND_CHOICE PSC
      WHERE PM.PRODUCT_ID = out_productID
        AND SM.SOURCE_CODE = in_source_code
        AND PCX.PRODUCT_ID = PM.PRODUCT_ID
        AND SM.COMPANY_ID = PCX.COMPANY_ID
        AND PSC.PRODUCT_SECOND_CHOICE_ID = PM.SECOND_CHOICE_CODE (+);

    OPEN OUT_CUR_SUBCODE FOR
      SELECT
        PRODUCT_SUBCODE_ID,
        PRODUCT_ID,
        SUBCODE_DESCRIPTION,
        SUBCODE_PRICE,
        DISPLAY_ORDER
      FROM FTD_APPS.PRODUCT_SUBCODES
      WHERE PRODUCT_ID = out_productID
        AND ACTIVE_FLAG = 'Y';

    if (IN_VALID_UPSELL_IDS is null) then
      OPEN OUT_CUR_UPSELL FOR
         SELECT
           UD.UPSELL_DETAIL_ID,
           UD.UPSELL_MASTER_ID,
           UD.UPSELL_DETAIL_NAME,
           UD.UPSELL_DETAIL_SEQUENCE,
           PM.STANDARD_PRICE "UPSELL_PRICE"
         FROM FTD_APPS.UPSELL_DETAIL UD,
           FTD_APPS.UPSELL_MASTER UM,
           FTD_APPS.SOURCE_MASTER SM,
           FTD_APPS.PRODUCT_COMPANY_XREF PCX,
           FTD_APPS.PRODUCT_MASTER PM
         WHERE UD.UPSELL_MASTER_ID = v_upsell_master_id
           AND UM.UPSELL_MASTER_ID = UD.UPSELL_MASTER_ID
           AND UM.UPSELL_STATUS = 'Y'
           AND PM.PRODUCT_ID = UD.UPSELL_DETAIL_ID
           AND PM.STATUS = 'A'
           AND PM.WEBOE_BLOCKED <> 'Y'
           AND PCX.PRODUCT_ID = PM.PRODUCT_ID
           AND SM.SOURCE_CODE = in_source_code
           AND SM.COMPANY_ID = PCX.COMPANY_ID
         ORDER BY UD.UPSELL_DETAIL_SEQUENCE;
    else
      OPEN OUT_CUR_UPSELL FOR
         'SELECT
           UD.UPSELL_DETAIL_ID,
           UD.UPSELL_MASTER_ID,
           UD.UPSELL_DETAIL_NAME,
           UD.UPSELL_DETAIL_SEQUENCE,
           PM.STANDARD_PRICE as UPSELL_PRICE
         FROM FTD_APPS.UPSELL_DETAIL UD,
           FTD_APPS.UPSELL_MASTER UM,
           FTD_APPS.SOURCE_MASTER SM,
           FTD_APPS.PRODUCT_COMPANY_XREF PCX,
           FTD_APPS.PRODUCT_MASTER PM
         WHERE UD.UPSELL_MASTER_ID = ''' || v_upsell_master_id || '''
           AND UM.UPSELL_MASTER_ID = UD.UPSELL_MASTER_ID
           AND UM.UPSELL_STATUS = ''Y''
           AND PM.PRODUCT_ID = UD.UPSELL_DETAIL_ID
           AND PM.STATUS = ''A''
           AND PM.WEBOE_BLOCKED <> ''Y''
           AND UD.UPSELL_DETAIL_ID in (' || IN_VALID_UPSELL_IDS || ')
           AND PCX.PRODUCT_ID = PM.PRODUCT_ID
           AND SM.SOURCE_CODE = ''' || in_source_code || '''
           AND SM.COMPANY_ID = PCX.COMPANY_ID
         ORDER BY UD.UPSELL_DETAIL_SEQUENCE';

    end if; 

    OPEN OUT_CUR_COLOR FOR
      SELECT
        PC.PRODUCT_COLOR,
        CM.DESCRIPTION
      FROM FTD_APPS.PRODUCT_COLORS PC,
        FTD_APPS.COLOR_MASTER CM
      WHERE PC.PRODUCT_ID = out_productID
        AND CM.COLOR_MASTER_ID = PC.PRODUCT_COLOR
      ORDER BY PC.ORDER_BY;

    if (v_ship_method_carrier = 'Y') then
        if (v_product_type = 'FRECUT' OR v_product_type = 'SDFC') then

            -- Get the global fresh cut flag from frp.global_parms
            open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SVC_CHARGE_TRIGGER');
            fetch get_global_parm into v_global_freshcut_flag;
            close get_global_parm;

            if (v_global_freshcut_flag = 'Y') then

                -- Get the global fresh cut value from frp.global_parms
                open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SVC_CHARGE');
                fetch get_global_parm into v_freshcut_value;
                close get_global_parm;

            else

                -- Get the fresh cut value from the first_order_domestic column of
                -- ftd_apps.snh
                open get_snh_by_id(v_snh_id);
                fetch get_snh_by_id into v_freshcut_value;
                close get_snh_by_id;

            end if;

            -- Get the saturday fresh cut fee from frp.global_parms
            open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SAT_CHARGE');
            fetch get_global_parm into v_freshcut_saturday_value;
            close get_global_parm;

            v_freshcut_saturday_value := v_freshcut_saturday_value + v_freshcut_value;

        end if;
    end if;

    OPEN OUT_CUR_ADDON FOR
        select a.addon_id, a.description, a.price, pa.max_qty, a.addon_type, florist_addon_flag, vendor_addon_flag
        from ftd_apps.addon a, ftd_apps.product_addon pa, ftd_apps.product_master pm
        where pa.product_id = out_productID
        and a.addon_id = pa.addon_id
        and a.active_flag = 'Y'
        and pa.active_flag = 'Y'
        and a.addon_type not in ('4', '7',
            decode(v_global_chocolate_flag, 'Y', decode(v_preferred_partner_flag, 'Y', decode(v_add_on_free_id, 'FC', '5', 'N/A'), 'N/A'), '5'),
            decode(v_add_on_free_id, a.addon_id, 'N/A', '6'))
        and pm.product_id = pa.product_id
        and ((pm.ship_method_florist = 'Y' and a.florist_addon_flag = 'Y') 
          or (pm.ship_method_carrier = 'Y' and a.vendor_addon_flag = 'Y' and exists (
            select 'Y' from ftd_apps.vendor_addon va, ftd_apps.vendor_product vp
            where va.addon_id = a.addon_id
            and va.active_flag = 'Y'
            and vp.vendor_id = va.vendor_id
            and vp.product_subcode_id = pa.product_id
            and vp.available = 'Y')))
        order by pa.display_seq_num;

    OPEN OUT_CUR_VASE FOR
        select a.addon_id, a.description, a.price, pa.max_qty, florist_addon_flag, vendor_addon_flag
        from ftd_apps.addon a, ftd_apps.product_addon pa, ftd_apps.product_master pm
        where pa.product_id = out_productID
        and a.addon_id = pa.addon_id
        and a.active_flag = 'Y'
        and pa.active_flag = 'Y'
        and a.addon_type = '7'
        and pm.product_id = pa.product_id
        and ((pm.ship_method_florist = 'Y' and a.florist_addon_flag = 'Y') 
          or (pm.ship_method_carrier = 'Y' and a.vendor_addon_flag = 'Y' and exists (
            select 'Y' from ftd_apps.vendor_addon va, ftd_apps.vendor_product vp
            where va.addon_id = a.addon_id
            and va.active_flag = 'Y'
            and vp.vendor_id = va.vendor_id
            and vp.product_subcode_id = pa.product_id
            and vp.available = 'Y')))
        order by pa.display_seq_num;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_PRODUCT_DETAILS;

PROCEDURE GET_DELIVERY_DATE_RANGES
(
OUT_CUR       OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Return all active delivery date ranges.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_DELIVERY_DATE_RANGES;
/*
   OPEN OUT_CUR FOR
       SELECT START_DATE,
           END_DATE,
           INSTRUCTION_TEXT
       FROM FTD_APPS.DELIVERY_DATE_RANGE
       WHERE ACTIVE_FLAG = 'Y'
           AND END_DATE > SYSDATE
       ORDER BY START_DATE;
*/

END GET_DELIVERY_DATE_RANGES;

PROCEDURE GET_SOURCE_CODE_BY_ID
(
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves the SOURCE_MASTER row for the given source code

Input:  source_code

Output: cursor containing source_master info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        SELECT SM.SOURCE_CODE,
            SM.DESCRIPTION,
            SM.SNH_ID,
            SM.PRICE_HEADER_ID,
            SM.START_DATE,
            SM.END_DATE,
            SM.PAYMENT_METHOD_ID,
            SM.DISCOUNT_ALLOWED_FLAG,
            SM.COMPANY_ID,
            SPR.PROGRAM_NAME PARTNER_ID,
            PP.MEMBERSHIP_DATA_REQUIRED,
            SM.IOTW_FLAG,
            SM.INVOICE_PASSWORD,
            SM.PRICE_HEADER_ID,
            SM.ADD_ON_FREE_ID,
            A.ADDON_TEXT,
            SM.ORDER_SOURCE,
            PP.PARTNER_NAME,
            SM.FUNERAL_CEMETERY_LOC_CHK,
            SM.HOSPITAL_LOC_CHCK,
            SM.FUNERAL_CEMETERY_LEAD_TIME_CHK,
            SM.FUNERAL_CEMETERY_LEAD_TIME,
            SM.BO_HRS_MON_FRI_START,
            SM.BO_HRS_MON_FRI_END,
            SM.BO_HRS_SAT_START,
            SM.BO_HRS_SAT_END,
            SM.BO_HRS_SUN_START,
            SM.BO_HRS_SUN_END
        FROM FTD_APPS.SOURCE_MASTER SM
        LEFT OUTER JOIN
        (
            select  sp.source_code, sp.program_name
            from    ftd_apps.source_program_ref sp
            where   sp.start_date =
                (
                    select  max(b.start_date)
                    from    ftd_apps.source_program_ref b
                    where   b.source_code = sp.source_code
                    and     b.start_date <= sysdate
                )
        ) spr
        ON sm.source_code = spr.source_code
        LEFT OUTER JOIN    ftd_apps.partner_program pp
        ON spr.program_name = pp.program_name
        LEFT OUTER JOIN    ftd_apps.addon a
        ON sm.add_on_free_id = a.addon_id
        WHERE SM.SOURCE_CODE = IN_SOURCE_CODE;

END GET_SOURCE_CODE_BY_ID;

PROCEDURE AJAX_GET_CUSTOMER_BY_PHONE
(
IN_PHONE_NUMBER                  IN VARCHAR2,
IN_COMPANY_ID                    IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all customer records for the given phone number

Input:  phone number

Output: cursor containing customer info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

CURSOR partner_cur(sourceCode varchar2) IS
        SELECT PP.PARTNER_NAME
        FROM FTD_APPS.SOURCE_MASTER SM
        LEFT OUTER JOIN
        (
            select  sp.source_code, sp.program_name
            from    ftd_apps.source_program_ref sp
            where   sp.start_date =
                (
                    select  max(b.start_date)
                    from    ftd_apps.source_program_ref b
                    where   b.source_code = sp.source_code
                    and     b.start_date <= sysdate
                )
        ) spr
        ON sm.source_code = spr.source_code
        LEFT OUTER JOIN    ftd_apps.partner_program pp
        ON spr.program_name = pp.program_name
        WHERE SM.SOURCE_CODE = sourceCode;


CURSOR max_records IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'MAX_PHONE_RECORDS';

v_partner_name     varchar2(100) := '';
v_max_records      number;
v_company_id       varchar2(12);

BEGIN

    v_company_id := CLEAN.GET_NEWSLETTER_COMPANY_ID(in_company_id);

    -- Get the partner name from ftd_apps.partner_program
    open partner_cur(IN_SOURCE_CODE);
    fetch partner_cur into v_partner_name;
    close partner_cur;

    -- Get the max records to display from frp.global_parms
    open max_records;
    fetch max_records into v_max_records;
    close max_records;


    OPEN OUT_CUR FOR
        SELECT CP.customer_id,
       (select phone_number
           from clean.customer_phones
           where customer_id = CP.customer_id
             and phone_type = 'Day' and rownum = 1) Day_Phone,
       (select extension
           from clean.customer_phones
           where customer_id = CP.customer_id
             and phone_type = 'Day' and rownum = 1) Day_Extension,
       (select phone_number
           from clean.customer_phones
           where customer_id = CP.customer_id
             and phone_type = 'Evening' and rownum = 1) Evening_Phone,
       (select extension
           from clean.customer_phones
           where customer_id = CP.customer_id
             and phone_type = 'Evening' and rownum = 1) Evening_Extension,
        DECODE(C.ADDRESS_TYPE, 'HOSPITAL', NULL, 'FUNERAL HOME', NULL,
            'NURSING HOME', NULL, 'CEMETERY', NULL, C.FIRST_NAME) FIRST_NAME,
        DECODE(C.ADDRESS_TYPE, 'HOSPITAL', NULL, 'FUNERAL HOME', NULL,
            'NURSING HOME', NULL, 'CEMETERY', NULL, C.LAST_NAME) LAST_NAME,
        C.ADDRESS_TYPE,
        C.BUSINESS_NAME,
        C.ADDRESS_1,
        C.ADDRESS_2,
        C.CITY,
        C.STATE,
        C.ZIP_CODE,
        C.COUNTRY,
        EM.EMAIL_ADDRESS,
        EM.SUBSCRIBE_STATUS,
        CM.MEMBERSHIP_TYPE PARTNER_ID,
        CM.MEMBERSHIP_NUMBER MEMBERSHIP_ID,
        CM.FIRST_NAME MEMBERSHIP_FIRST_NAME,
        CM.LAST_NAME MEMBERSHIP_LAST_NAME
        FROM (select * from 
                (select distinct customer_id, phone_number, updated_on
                   from   clean.CUSTOMER_PHONES
                  where  phone_number = IN_PHONE_NUMBER
                  order by updated_on desc
                ) where rownum <= v_max_records
             ) CP
        JOIN CLEAN.CUSTOMER C
            ON CP.customer_id = C.customer_id
        LEFT OUTER JOIN (select cer.customer_id,
            cer.email_id,
            e.email_address,
            e.subscribe_status,
            e.company_id,
            e.created_on
            from clean.customer_email_ref cer,
                clean.email e
            where e.email_id = cer.email_id
                and e.company_id = V_COMPANY_ID
            ) em
        ON em.customer_id = cp.customer_id
        LEFT OUTER JOIN
        (
            select m.customer_id,
                m.membership_type,
                m.membership_number,
                m.first_name,
                m.last_name
            from clean.memberships m
            where m.created_on =
                (
                    select max(mm.created_on)
                    from clean.memberships mm
                    where mm.customer_id = m.customer_id
                    and mm.membership_type = v_partner_name
                    and most_recent_by_type = 'Y'
                    and mm.customer_id in (select distinct customer_id from clean.CUSTOMER_PHONES where  phone_number = IN_PHONE_NUMBER)
                )
            and m.customer_id in (select distinct customer_id from clean.CUSTOMER_PHONES where  phone_number = IN_PHONE_NUMBER)
        ) cm
        ON cp.customer_id = cm.customer_id
        WHERE CP.PHONE_NUMBER = IN_PHONE_NUMBER
            and (em.created_on is NULL
                or em.created_on =
                    (select max(e2.created_on)
                    from clean.email e2,
                        clean.customer_email_ref cer2
                    where e2.email_id = cer2.email_id
                        and cer2.customer_id = CP.customer_id
                        and e2.company_id = V_COMPANY_ID));


    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_CUSTOMER_BY_PHONE;

PROCEDURE AJAX_GET_ZIPS_BY_CITY_STATE
(
IN_CITY                          IN VARCHAR2,
IN_STATE                         IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all zip_code records that match the given city/state

Input:  city, phone number

Output: cursor containing zip code info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        SELECT ZIP_CODE_ID, CITY
        FROM FTD_APPS.ZIP_CODE
        WHERE CITY LIKE UPPER(IN_CITY) || '%'
            AND STATE_ID = UPPER(IN_STATE)
        ORDER BY CITY, ZIP_CODE_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_ZIPS_BY_CITY_STATE;

PROCEDURE GET_GIFT_CERTIFICATE_DETAILS
(
IN_GIFT_CERTIFICATE              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
--==============================================================================
-- Returns: ref_cursor for
--          giftCertificateId       VARCHAR2(10)
--          certificateAmount       NUMBER(7.2)
--          expirationDate          VARCHAR2(10)
--          issueDate               VARCHAR2(10)
--          redemptionFlag          VARCHAR2(1)
--          companyId               VARCHAR2(12)
--
-- Description:   Queries the GIFT_CERTIFICATE_MASTER table by GIFT_CERTIFICATE_ID
--
--==============================================================================
BEGIN

    OPEN out_cur FOR
        SELECT  gcc.gc_coupon_number,
                gcc.issue_amount,
                gcc.expiration_date,
                gcc.issue_date,
                gcc.gc_coupon_status
        FROM    clean.gc_coupons gcc
        WHERE   upper(gcc.gc_coupon_number) = upper(IN_GIFT_CERTIFICATE)
        AND     gcc.gc_coupon_status IN ('Reinstate', 'Redeemed', 'Issued Active',
                    'Issued Expired');

END GET_GIFT_CERTIFICATE_DETAILS;

PROCEDURE GET_FLORIST_VALIDATION_INFO
(
IN_FLORIST_ID                    IN VARCHAR2,
IN_PRODUCT_ID                    IN VARCHAR2,
IN_ZIP_CODE                      IN VARCHAR2,
IN_DELIVERY_DATE                 IN DATE,
OUT_CUR_FM                      OUT TYPES.REF_CURSOR,
OUT_CUR_FZ                      OUT TYPES.REF_CURSOR,
OUT_CUR_FC                      OUT TYPES.REF_CURSOR,
OUT_CUR_FB                      OUT TYPES.REF_CURSOR,
OUT_CUR_FCS			OUT TYPES.REF_CURSOR,
OUT_CUR_FDS			OUT TYPES.REF_CURSOR
)
AS

CURSOR codified_cur IS
    select fc.block_start_date, fc.block_end_date
        from ftd_apps.florist_codifications fc,
            ftd_apps.codified_products cp
        where cp.product_id = in_product_id
            and fc.codification_id = cp.codification_id
            and fc.florist_id = in_florist_id;

v_codified_flag          varchar2(1) := '';
v_florist_codified_flag  varchar2(1) := '';
v_block_start_date       date := null;
v_block_end_date         date := null;
v_codification_id        varchar2(100) := '';

BEGIN

    OPEN OUT_CUR_FM FOR
        select status, super_florist_flag
        from ftd_apps.florist_master
        where florist_id = in_florist_id;

    OPEN OUT_CUR_FZ FOR
        select block_start_date, block_end_date
        from ftd_apps.florist_zips
        where florist_id = in_florist_id
            and zip_code = in_zip_code;
        
    OPEN OUT_CUR_FB FOR
        select block_type,block_start_date, block_end_date
        from ftd_apps.florist_blocks
        where florist_id = in_florist_id; 

    begin
        select 'Y', codification_id
        into v_codified_flag, v_codification_id
        from ftd_apps.codified_products
        where product_id = in_product_id;

        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_codified_flag := 'N';
    end;

    if (v_codified_flag = 'Y') then
    begin
        select 'Y' into v_florist_codified_flag
        from ftd_apps.florist_codifications
        where florist_id = in_florist_id
        and codification_id = v_codification_id;

        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_florist_codified_flag := 'N';
    end;
    end if;

    OPEN CODIFIED_CUR;
    FETCH CODIFIED_CUR INTO v_block_start_date, v_block_end_date;
    CLOSE CODIFIED_CUR;

    OPEN OUT_CUR_FC FOR
        SELECT v_codified_flag codified_flag,
               v_florist_codified_flag florist_codified,
               v_block_start_date block_start_date,
               v_block_end_date block_end_date
        FROM   dual;
  
    
        OPEN OUT_CUR_FCS FOR
     	select ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) as current_closure_status,
     	( (select listagg('Florist is currently '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || '.  Florist schedule is ') 
               within group (order by fh1.florist_id) 
               from ftd_apps.florist_hours fh1
               where fh1.florist_id = fm.florist_id
               and UPPER(trim(to_char(SYSDATE, 'Day'))) = UPPER(fh1.day_of_week)
            ) 
            || 
            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
              within group (ORDER BY 
              CASE
                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
              END ASC) || '.' as florist_schedule
             from ftd_apps.florist_hours fh1
             WHERE fh1.florist_id = fm.florist_id) ) as florist_currently_closed_msg
	from ftd_apps.florist_master fm
	where fm.florist_id = in_florist_id;    
    
    
    OPEN OUT_CUR_FDS FOR
     	select ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, in_delivery_date) as open_on_delivery,
       	  ( (select listagg('Florist is '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || ' on selected delivery date.  Florist schedule is ') 
               within group (order by fh1.florist_id) 
               from ftd_apps.florist_hours fh1
               where fh1.florist_id = fm.florist_id
               and UPPER(trim(to_char(in_delivery_date, 'Day'))) = UPPER(fh1.day_of_week)
            ) 
            || 
            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
              within group (ORDER BY 
              CASE
                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
              END ASC) || '.' as florist_schedule
             from ftd_apps.florist_hours fh1
             WHERE fh1.florist_id = fm.florist_id) ) as florist_closed_on_dd_msg
	from ftd_apps.florist_master fm
	where fm.florist_id = in_florist_id;   
	


END GET_FLORIST_VALIDATION_INFO;

FUNCTION GET_ELEMENTS
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_ELEMENTS
-- Type:    Function
-- Syntax:  GET_ELEMENTS
-- Returns: all records in the JOE.ELEMENTS_CONFIG table
--
--==============================================================================
AS
  out_cur types.ref_cursor;
BEGIN
  OPEN out_cur FOR
    SELECT
        e.APP_CODE,
        e.ELEMENT_ID,
        e.LOCATION_DESC,
        e.LABEL_ELEMENT_ID,
        e.COUNT_ELEMENT_ID,
        e.REQUIRED_FLAG,
        e.REQUIRED_COND_DESC,
        e.REQUIRED_COND_TYPE_NAME,
        e.VALIDATE_FLAG,
        e.VALIDATE_EXP,
        e.VALIDATE_EXP_TYPE_NAME,
        e.ERROR_TXT,
        e.TITLE_TXT,
        e.VALUE_MAX_BYTES_QTY,
        e.TAB_SEQ,
        e.TAB_INIT_SEQ,
        e.TAB_COND_DESC,
        e.TAB_COND_TYPE_NAME,
        e.ACCORDION_INDEX_NUM,
        e.TAB_CTRL_INDEX_NUM,
        e.RECALC_FLAG,
        e.PRODUCT_VALIDATE_FLAG,
        e.ACCESS_KEY_NAME,
        e.XML_NODE_NAME,
        e.CALC_XML_FLAG
    FROM JOE.ELEMENT_CONFIG e
    ORDER BY e.APP_CODE, e.ELEMENT_ID;

    return out_cur;
END GET_ELEMENTS;

PROCEDURE GET_EXCLUDED_STATES_BY_ZIP
(
IN_PRODUCT_ID                    IN VARCHAR2,
IN_ZIP_CODE                      IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select
            pes.product_id,
            pes.excluded_state,
            pes.sun,
            pes.mon,
            pes.tue,
            pes.wed,
            pes.thu,
            pes.fri,
            pes.sat
        from ftd_apps.product_excluded_states pes,
             ftd_apps.zip_code zc
        where zc.zip_code_id = in_zip_code
            and pes.excluded_state = zc.state_id
            and pes.product_id = in_product_id;

END GET_EXCLUDED_STATES_BY_ZIP;

PROCEDURE GET_BILLING_INFO
(
IN_SOURCE_CODE_ID                IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select
            billing_info_sequence,
            info_description,
            info_format,
            prompt_type,
            info_display,
            validation_regex
        from ftd_apps.billing_info
        where source_code_id = in_source_code_id
        order by billing_info_sequence;

END GET_BILLING_INFO;

PROCEDURE GET_BILLING_INFO_OPTIONS
(
IN_SOURCE_CODE_ID                IN VARCHAR2,
IN_INFO_DESCRIPTION              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select
            option_value,
            option_name,
            option_sequence
        from ftd_apps.billing_info_options
        where source_code_id = in_source_code_id
            and billing_info_desc = in_info_description
        order by option_sequence;

END GET_BILLING_INFO_OPTIONS;

PROCEDURE GET_FLORIST_INFO
(
IN_FLORIST_ID_LIST              IN VARCHAR2,
IN_SOURCE_CODE_ID               IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

v_Sql           VARCHAR2(4000);

BEGIN

  v_Sql := 'select
            fafm.florist_id,
            fafm.florist_name,
            fafm.address,
            fafm.phone_number,
            fafm.mercury_flag,
            fafm.florist_weight,
            fafm.super_florist_flag,
            DECODE(ftd_apps.florist_query_pkg.is_florist_open_sunday(fafm.florist_id), ''N'', '''', ''Y'', ''Y'') sunday_delivery_flag,
            (select fasfp.priority
                from FTD_APPS.SOURCE_FLORIST_PRIORITY fasfp
                   where fasfp.SOURCE_CODE = ' || IN_SOURCE_CODE_ID || '
                   and fasfp.florist_id = fafm.florist_id
            ) as priority
          from ftd_apps.florist_master fafm
          where fafm.florist_id in (' || IN_FLORIST_ID_LIST || ')
          order by priority, florist_weight DESC';

  -- execute the sql statement

  OPEN OUT_CUR FOR v_Sql;

END GET_FLORIST_INFO;

PROCEDURE GET_SHIPPING_COST
(
IN_SHIPPING_KEY                  IN VARCHAR2,
IN_SHIP_METHOD                   IN VARCHAR2,
IN_PRODUCT_PRICE                 IN FTD_APPS.SHIPPING_KEY_DETAILS.MIN_PRICE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select skc.shipping_key_detail_id,
            skc.shipping_cost
        from ftd_apps.shipping_key_costs skc,
            ftd_apps.shipping_key_details skd
        where skd.shipping_key_id = in_shipping_key
            and skd.min_price <= in_product_price
            and skd.max_price >= in_product_price
            and skc.shipping_key_detail_id = skd.shipping_detail_id
            and skc.shipping_method_id = in_ship_method;

END GET_SHIPPING_COST;

PROCEDURE AJAX_GET_PRODUCT_CROSS_SELL
(
IN_PRODUCT_ID                    IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select csd.display_order_seq_num,
            nvl(csd.cross_sell_product_id,(select upsell_detail_id from ftd_apps.upsell_detail
                where upsell_master_id = csd.cross_sell_upsell_id and upsell_detail_sequence = '1')) cross_sell_product_id,
            pm.over_21
        from joe.cross_sell_master csm, joe.cross_sell_detail csd
        join ftd_apps.product_master pm on
            nvl(csd.cross_sell_product_id,(select upsell_detail_id from ftd_apps.upsell_detail
                where upsell_master_id = csd.cross_sell_upsell_id and upsell_detail_sequence = '1')) = pm.product_id
        where (csm.product_id = in_product_id or csm.upsell_master_id = in_product_id)
            and csd.cross_sell_master_id = csm.cross_sell_master_id
        order by csd.display_order_seq_num;

    EXCEPTION WHEN NO_DATA_FOUND THEN
        OPEN OUT_CUR for select * from dual where dummy = 'Y';

END AJAX_GET_PRODUCT_CROSS_SELL;

PROCEDURE GET_VALID_QMS_RESPONSE
(
IN_QMS_RESPONSE_CODE             IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT QMS_RESPONSE_DESC
        FROM JOE.QMS_RESPONSE
        WHERE QMS_RESPONSE_CODE = IN_QMS_RESPONSE_CODE;

END GET_VALID_QMS_RESPONSE;

PROCEDURE GET_PAYMENT_METHOD_BY_ID
(
IN_PAYMENT_METHOD_ID             IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            DESCRIPTION,
            PAYMENT_TYPE,
            CARD_ID,
            HAS_EXPIRATION_DATE,
            OVER_AUTH_ALLOWED_PCT,
            ACTIVE_FLAG,
            JOE_FLAG,
            MOD10_FLAG,
            REGEX_PATTERN,
            MIN_AUTH_AMT,
            CSC_REQUIRED_FLAG
        FROM FTD_APPS.PAYMENT_METHODS
        WHERE PAYMENT_METHOD_ID = IN_PAYMENT_METHOD_ID;

END GET_PAYMENT_METHOD_BY_ID;

PROCEDURE AUTHENTICATE_IDENTITY
(
IN_IDENTITY_ID                   IN VARCHAR2,
IN_CREDENTIALS                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

TODAY           DATE := SYSDATE;

BEGIN

    OPEN OUT_CUR FOR
        SELECT  IDENTITY_ID,
                USER_ID,
                aas.authenticate_pkg.is_csr_in_resource(IN_IDENTITY_ID, 'NoCharge', 'Yes') STATUS
        FROM    AAS.IDENTITY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID
        AND     CREDENTIALS = GLOBAL.MD5_HASH (IN_CREDENTIALS)
        AND     IDENTITY_EXPIRE_DATE > TODAY
        AND     CREDENTIALS_EXPIRE_DATE > TODAY
        AND     ATTEMPT_TRY_NUMBER < 3
        AND     LOCKED_FLAG = 'N';

END AUTHENTICATE_IDENTITY;

PROCEDURE GET_JOE_ORDERS
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            O.MASTER_ORDER_NUMBER,
            O.BUYER_ID,
            O.DNIS_ID,
            O.SOURCE_CODE,
            O.ORIGIN_ID,
            to_char(O.ORDER_DATE, 'MM/DD/YYYY HH24:MI:SS') ORDER_DATE,
            O.ORDER_TAKEN_BY_IDENTITY_ID,
            O.ITEM_CNT,
            O.ORDER_AMT,
            O.FRAUD_ID,
            O.FRAUD_CMNT,
            D.COMPANY_ID,
			O.LANGUAGE_ID,
			O.BUYER_HAS_FREE_SHIPPING
        FROM JOE.ORDERS O, FTD_APPS.DNIS D
        WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER
            AND D.DNIS_ID = O.DNIS_ID;

END GET_JOE_ORDERS;

PROCEDURE GET_JOE_ORDER_PAYMENTS
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
PAYMENT_EXT_CUR                         OUT TYPES.REF_CURSOR
)
AS

joe_payment_id number;

CURSOR get_payment_id IS
  SELECT PAYMENT_ID
  FROM JOE.ORDER_PAYMENTS
  WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER AND ROWNUM=1;

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            PAYMENT_ID,
            MASTER_ORDER_NUMBER,
            PAYMENT_METHOD_ID,
            decode(cc_number, null, null, global.encryption.decrypt_it (cc_number,key_name)) CC_NUMBER,
            KEY_NAME,
            CC_EXPIRATION,
            AUTH_AMT,
            APPROVAL_CODE,
            APPROVAL_VERBIAGE,
            APPROVAL_ACTION_CODE,
            ACQ_REFERENCE_NUMBER,
            AVS_RESULT_CODE,
            GC_COUPON_NUMBER,
            GC_AMT,
            AAFES_TICKET_NUMBER,
            NC_APPROVAL_IDENTITY_ID,
            NC_TYPE_CODE,
            NC_REASON_CODE,
            NC_ORDER_DETAIL_ID,
            NC_AMT,
            CSC_RESPONSE_CODE,
            CSC_VALIDATED_FLAG,
            CSC_FAILURE_CNT,
            cc_auth_provider,
            wallet_indicator
        FROM JOE.ORDER_PAYMENTS
        WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

      open get_payment_id;
      fetch get_payment_id into joe_payment_id;
      close get_payment_id;
        
      OPEN PAYMENT_EXT_CUR FOR 
     	SELECT AUTH_PROPERTY_NAME, AUTH_PROPERTY_VALUE
     	FROM JOE.PAYMENTS_EXT
     	WHERE PAYMENT_ID = joe_payment_id;

END GET_JOE_ORDER_PAYMENTS;

PROCEDURE GET_JOE_ORDER_BILLING_INFO
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            BILLING_INFO_ID,
            MASTER_ORDER_NUMBER,
            BILLING_INFO_NAME,
            BILLING_INFO_DATA
        FROM JOE.ORDER_BILLING_INFO
        WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

END GET_JOE_ORDER_BILLING_INFO;

PROCEDURE GET_JOE_ORDER_DETAILS
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            EXTERNAL_ORDER_NUMBER,
            MASTER_ORDER_NUMBER,
            SOURCE_CODE,
            DELIVERY_DATE,
            RECIPIENT_ID,
            PRODUCT_ID,
            PRODUCT_SUBCODE_ID,
            FIRST_COLOR_CHOICE,
            SECOND_COLOR_CHOICE,
            IOTW_FLAG,
            OCCASION_ID,
            CARD_MESSAGE_SIGNATURE,
            ORDER_CMNTS,
            FLORIST_CMNTS,
            FLORIST_ID,
            SHIP_METHOD_ID,
            DELIVERY_DATE_RANGE_END,
            SIZE_IND,
            PRODUCT_AMT,
            ADD_ON_AMT,
            SHIPPING_FEE_AMT,
            SERVICE_FEE_AMT,
            DISCOUNT_AMT,
            TAX_AMT,
            ITEM_TOTAL_AMT,
            MILES_POINTS_QTY,
            PERSONAL_GREETING_ID,
            BIN_SOURCE_CHANGED_FLAG,
            tax1_name,
            tax1_description,
            tax1_rate,
            tax1_amount,
            tax2_name,
            tax2_description,
            tax2_rate,
            tax2_amount,
            tax3_name,
            tax3_description,
            tax3_rate,
            tax3_amount,
            tax4_name,
            tax4_description,
            tax4_rate,
            tax4_amount,
            tax5_name,
            tax5_description,
            tax5_rate,
            tax5_amount,
            tax_service_performed,
            tax_total_description,
            total_tax_rate,
            original_order_has_sdu,
            time_of_service,
            late_cutoff_fee,
            vendor_mon_upcharge,
            vendor_sun_upcharge
        FROM JOE.ORDER_DETAILS
        WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

END GET_JOE_ORDER_DETAILS;

PROCEDURE GET_JOE_ORDER_ADDONS
(
IN_EXTERNAL_ORDER_NUMBER         IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            ORDER_ADD_ON_ID,
            EXTERNAL_ORDER_NUMBER,
            ADD_ON_CODE,
            ADD_ON_QTY,
            FUNERAL_BANNER_TXT
        FROM JOE.ORDER_ADD_ONS
        WHERE EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

END GET_JOE_ORDER_ADDONS;

PROCEDURE GET_JOE_ORDER_CUSTOMER
(
IN_CUSTOMER_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            CUSTOMER_ID,
            FIRST_NAME,
            LAST_NAME,
            ADDRESS,
            CITY,
            STATE,
            ZIP_CODE,
            COUNTRY,
            BUYER_RECIPIENT_IND,
            DAYTIME_PHONE,
            DAYTIME_PHONE_EXT,
            EVENING_PHONE,
            EVENING_PHONE_EXT,
            EMAIL_ADDRESS,
            NEWSLETTER_FLAG,
            QMS_RESPONSE_CODE,
            ADDRESS_TYPE,
            BUSINESS_NAME,
            BUSINESS_INFO,
            MEMBERSHIP_ID,
            MEMBERSHIP_FIRST_NAME,
            MEMBERSHIP_LAST_NAME,
            PROGRAM_NAME
        FROM JOE.ORDER_CUSTOMER
        WHERE CUSTOMER_ID = IN_CUSTOMER_ID;

END GET_JOE_ORDER_CUSTOMER;

PROCEDURE GET_IOTW
(
IN_SOURCE_CODE                   IN VARCHAR2,
IN_PRODUCT_ID                    IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            I.IOTW_ID IOTW_ID,
            I.IOTW_SOURCE_CODE IOTW_SOURCE_CODE,
            I.START_DATE START_DATE,
            I.END_DATE END_DATE
        FROM JOE.IOTW I, FTD_APPS.SOURCE_MASTER SM
        WHERE I.SOURCE_CODE = IN_SOURCE_CODE
          AND I.PRODUCT_ID = IN_PRODUCT_ID
          AND TRUNC(I.START_DATE) <= TRUNC(SYSDATE)
          AND (I.END_DATE IS NULL OR TRUNC(I.END_DATE) >= TRUNC(SYSDATE))
          AND SM.SOURCE_CODE = I.IOTW_SOURCE_cODE
          AND SM.IOTW_FLAG = 'Y';

END GET_IOTW;

PROCEDURE GET_IOTW_DEL_DISCOUNT_DATES
(
IN_IOTW_ID                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            IOTW_ID,
            DISCOUNT_DATE
        FROM JOE.IOTW_DELIVERY_DISCOUNT_DATES
        WHERE IOTW_ID = IN_IOTW_ID;

END GET_IOTW_DEL_DISCOUNT_DATES;

PROCEDURE GET_IOTW_PRODUCTS
(
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT
            IOTW_ID,
            PRODUCT_ID,
            IOTW_SOURCE_CODE,
            PRODUCT_PAGE_MESSAGING_TXT,
            SM.PRICE_HEADER_ID PRICE_HEADER_ID,
            SM.DISCOUNT_ALLOWED_FLAG DISCOUNT_ALLOWED_FLAG,
            (SELECT COUNT(*) 
                FROM JOE.IOTW_DELIVERY_DISCOUNT_DATES IDDD
                WHERE IDDD.IOTW_ID = IOTW.IOTW_ID) IDDD_CNT,
            IOTW.CREATED_BY
        FROM JOE.IOTW, FTD_APPS.SOURCE_MASTER SM
        WHERE IOTW.SOURCE_CODE = IN_SOURCE_CODE
            AND SM.SOURCE_CODE = IOTW_SOURCE_CODE
            AND SM.IOTW_FLAG = 'Y'
            AND TRUNC(IOTW.START_DATE) <= TRUNC(SYSDATE)
            AND (IOTW.END_DATE IS NULL OR TRUNC(IOTW.END_DATE) >= TRUNC(SYSDATE));

END GET_IOTW_PRODUCTS;

PROCEDURE VALIDATE_EXTERNAL_ORDER_NUMBER
(
IN_EXTERNAL_ORDER_NUMBER         IN VARCHAR2,
OUT_ORDER_DETAIL_ID             OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    SELECT ORDER_DETAIL_ID INTO OUT_ORDER_DETAIL_ID
    FROM CLEAN.ORDER_DETAILS
    WHERE EXTERNAL_ORDER_NUMBER = UPPER(IN_EXTERNAL_ORDER_NUMBER);

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VALIDATE_EXTERNAL_ORDER_NUMBER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VALIDATE_EXTERNAL_ORDER_NUMBER;

PROCEDURE AJAX_GET_COMPANIES
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the companies
             associated with products

Input:  None

Output: cursor containing company info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
    SELECT
      cm.COMPANY_ID
    FROM FTD_APPS.COMPANY_MASTER cm
    WHERE cm.order_flag = 'Y';

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_COMPANIES;

PROCEDURE GET_CROSS_SELL_BY_COMPANY
(
IN_COMPANY_ID                   IN  VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the cross
             sell products for a company.

Input:  company_id

Output: cursor containing company info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

cursor get_max_items IS
    SELECT   max(display_order_seq_num)
    FROM     joe.cross_sell_detail;

cursor get_max_global_parms IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'NUM_CROSS_SELL_SKUS';

v_max_items         number;
v_max_global_parms  number;

BEGIN

    open get_max_items;
    fetch get_max_items into v_max_items;
    close get_max_items;

    open get_max_global_parms;
    fetch get_max_global_parms into v_max_global_parms;
    close get_max_global_parms;

    if (v_max_global_parms > v_max_items) then
        v_max_items := v_max_global_parms;
    end if;

    OPEN OUT_CUR FOR
      select csm.cross_sell_master_id, 
             csm.product_id,
             csm.upsell_master_id,
             nvl(pm.novator_id,csm.upsell_master_id) novator_id,
             decode(pm.status,'A','YES','U','NO',decode(um.upsell_status,'Y','YES','NO')) status,
             nvl(pm.over_21,'N') over_21,
             pm.product_type,
             nvl(pm.novator_name,um.upsell_name) novator_name,
             v_max_items max_items
      from joe.cross_sell_master csm,
           ftd_apps.product_master pm,
           ftd_apps.upsell_master  um
      where 1=1
      and pm.product_id (+)= csm.product_id
      and um.upsell_master_id (+)= csm.upsell_master_id
      and ((IN_COMPANY_ID is null) OR
     (IN_COMPANY_ID in (
               select company_id from ftd_apps.product_company_xref where product_id = csm.product_id
               UNION
               select company_id from ftd_apps.upsell_company_xref where product_id = csm.upsell_master_id)
      ));

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_CROSS_SELL_BY_COMPANY;

PROCEDURE AJAX_GET_CROSS_SELL_PRODUCT
(
IN_PRODUCT_ID                   IN  VARCHAR2,
PRODUCT_CUR                     OUT TYPES.REF_CURSOR,
CROSS_SELL_CUR                  OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving the product and it's
             cross sell products.

Input:  product_id Can be product_id, novator_id or upsell

Output: cursor containing product info
        cursor containing cross sell info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

cursor image_url IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'IMAGE_SERVER_HTTP_URL';

cursor get_max_items IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'NUM_CROSS_SELL_SKUS';

v_image_url                VARCHAR2(100) := '';
v_product_id               VARCHAR2(100) := '';
v_upsell_id                VARCHAR2(100) := '';
v_max_items                VARCHAR2(100) := '';

BEGIN

    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    open get_max_items;
    fetch get_max_items into v_max_items;
    close get_max_items;

    BEGIN

        SELECT PRODUCT_ID
        INTO v_product_id
        FROM FTD_APPS.PRODUCT_MASTER
        WHERE NOVATOR_ID = IN_PRODUCT_ID
           OR PRODUCT_ID = IN_PRODUCT_ID;

    EXCEPTION WHEN NO_DATA_FOUND THEN

        SELECT UPSELL_MASTER_ID
  INTO v_upsell_id
        FROM FTD_APPS.UPSELL_MASTER
  WHERE UPSELL_MASTER_ID = IN_PRODUCT_ID;

    END;

    IF v_product_id IS NOT NULL THEN
      BEGIN
        OPEN PRODUCT_CUR FOR
          select pm.product_id,
                 pm.novator_id,
                 '' upsell_master_id,
                 decode(pm.status,'A','YES','NO') status ,
                 pm.over_21,
                 pm.product_type,
                 pm.novator_name,
           pm.standard_price,
           pm.long_description,
                 to_char(pm.exception_start_date,'MM/dd/yyyy') exception_start_date,
                 to_char(pm.exception_end_date,'MM/dd/yyyy') exception_end_date,
                 v_image_url || '/' || pm.NOVATOR_ID || '_a.jpg' smallImage,
                 v_image_url || '/' || pm.NOVATOR_ID || '_c.jpg' largeImage,
                 v_max_items max_items
          from ftd_apps.product_master pm
          where 1=1
          and (pm.product_id = IN_PRODUCT_ID OR pm.novator_id = IN_PRODUCT_ID);
      END;
    ELSE
      BEGIN
        OPEN PRODUCT_CUR FOR
          SELECT um.upsell_master_id product_id,
                 um.upsell_master_id novator_id,
                 um.upsell_master_id upsell_master_id,
                 DECODE(um.upsell_status,'Y','YES','NO') status ,
                 '' over_21,
                 '' product_type,
                 um.upsell_name novator_name,
           '' standard_price,
           um.upsell_description long_description,
                 NULL exception_start_date,
                 NULL exception_end_date,
                 'images/npi_a.jpg' smallImage,
                 'images/npi_c.jpg' largeImage,
                 v_max_items max_items
          FROM ftd_apps.upsell_master um
          WHERE 1=1
          and (um.upsell_master_id = IN_PRODUCT_ID);
      END;
    END IF;


    OPEN CROSS_SELL_CUR FOR
        select csd.cross_sell_master_id,
            --nvl(csd.cross_sell_product_id,csd.cross_sell_upsell_id) product_id,
            csd.cross_sell_product_id product_id,
            csd.cross_sell_upsell_id upsell_id,
            decode(pm.status,'A','AVAILABLE','N','NOT AVAILABLE',decode(um.upsell_status,'Y','AVAILABLE','NOT AVAILABLE')) status,
            nvl(pm.novator_name,um.upsell_name) novator_name,
            pm.standard_price,
            pm.long_description,
            pm.over_21,
            pm.product_type,
            to_char(pm.exception_start_date,'MM/dd/yyyy') exception_start_date,
            to_char(pm.exception_end_date,'MM/dd/yyyy') exception_end_date,
            decode(pm.novator_id,NULL,'images/npi_a.jpg',v_image_url || '/' || pm.NOVATOR_ID || '_a.jpg') small_image,
            decode(pm.novator_id,NULL,'images/npi_c.jpg',v_image_url || '/' || pm.NOVATOR_ID || '_c.jpg') large_image
        from joe.cross_sell_detail csd,
            joe.cross_sell_master csm,
            ftd_apps.product_master pm,
            ftd_apps.upsell_master um
        where 1=1
        and (csm.product_id = v_product_id
            OR csm.upsell_master_id = v_upsell_id)
        and csd.cross_sell_master_id = csm.cross_sell_master_id
        and pm.product_id (+) = csd.cross_sell_product_id
        and um.upsell_master_id (+) = csd.cross_sell_upsell_id
        order by csd.display_order_seq_num;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        OPEN PRODUCT_CUR for select * from dual where dummy = 'Y';
        OPEN CROSS_SELL_CUR for select * from dual where dummy = 'Y';
        OUT_STATUS := 'Y';
        OUT_MESSAGE := '';
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END AJAX_GET_CROSS_SELL_PRODUCT;



PROCEDURE AJAX_GET_CROSS_SELL_MASTER
(
IN_PRODUCT_ID                   IN  VARCHAR2,
PRODUCT_CUR                     OUT TYPES.REF_CURSOR,
CROSS_SELL_CUR                  OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving the product and all the
             cross sell products the product is on.

Input:  product_id

Output: cursor containing product info
        cursor containing cross sell info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
cursor image_url IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'IMAGE_SERVER_HTTP_URL';
v_image_url                VARCHAR2(100) := '';
v_product_id               VARCHAR2(100) := '';
v_upsell_id                VARCHAR2(100) := '';

BEGIN
    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    OPEN CROSS_SELL_CUR FOR
      select csm.cross_sell_master_id, 
             nvl(csm.product_id,csm.upsell_master_id) product_id,
             csm.upsell_master_id,
             nvl(pm.novator_id,csm.upsell_master_id) novator_id,
             decode(pm.status,'A','YES','U','NO',decode(um.upsell_status,'Y','YES','NO')) status,
             nvl(pm.over_21,'N') over_21,
             pm.product_type,
             NVL(pm.long_description,um.UPSELL_DESCRIPTION) long_description
      from joe.cross_sell_master csm,
           ftd_apps.product_master pm,
           ftd_apps.upsell_master  um
      where 1=1
      and pm.product_id (+)= csm.product_id
      and um.upsell_master_id (+)= csm.upsell_master_id
      and csm.cross_sell_master_id in (
             select cross_sell_master_id
             from joe.cross_sell_detail
             where cross_sell_product_id = IN_PRODUCT_ID
                or cross_sell_upsell_id = IN_PRODUCT_ID);

    BEGIN

        SELECT PRODUCT_ID
        INTO v_product_id
        FROM FTD_APPS.PRODUCT_MASTER
        WHERE NOVATOR_ID = IN_PRODUCT_ID
           OR PRODUCT_ID = IN_PRODUCT_ID;

    EXCEPTION WHEN NO_DATA_FOUND THEN

        SELECT UPSELL_MASTER_ID
  INTO v_upsell_id
        FROM FTD_APPS.UPSELL_MASTER
  WHERE UPSELL_MASTER_ID = IN_PRODUCT_ID;

    END;

    IF v_product_id IS NOT NULL THEN
      BEGIN
        OPEN PRODUCT_CUR FOR
          select pm.product_id,
                 pm.novator_id,
                 decode(pm.status,'A','YES','NO') status ,
                 pm.over_21,
                 pm.product_type,
                 pm.novator_name,
           pm.standard_price,
           pm.long_description,
                 to_char(pm.exception_start_date,'MM/dd/yyyy') exception_start_date,
                 to_char(pm.exception_end_date,'MM/dd/yyyy') exception_end_date,
                 v_image_url || '/' || pm.NOVATOR_ID || '_a.jpg' smallImage,
                 v_image_url || '/' || pm.NOVATOR_ID || '_c.jpg' largeImage
          from ftd_apps.product_master pm
          where 1=1
          and (pm.product_id = IN_PRODUCT_ID OR pm.novator_id = IN_PRODUCT_ID);
      END;
    ELSE
      BEGIN
        OPEN PRODUCT_CUR FOR
          SELECT um.upsell_master_id product_id,
                 um.upsell_master_id novator_id,
                 DECODE(um.upsell_status,'Y','YES','NO') status ,
                 '' over_21,
                 '' product_type,
                 um.upsell_name novator_name,
           '' standard_price,
           um.upsell_description long_description,
                 NULL exception_start_date,
                 NULL exception_end_date,
                 'images/npi_a.jpg' smallImage,
                 'images/npi_c.jpg' largeImage
          FROM ftd_apps.upsell_master um
          WHERE 1=1
          and (um.upsell_master_id = IN_PRODUCT_ID);
      END;
    END IF;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        OPEN PRODUCT_CUR for select * from dual where dummy = 'Y';
        OPEN CROSS_SELL_CUR for select * from dual where dummy = 'Y';
        OUT_STATUS := 'Y';
        OUT_MESSAGE := '';
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END AJAX_GET_CROSS_SELL_MASTER;

PROCEDURE GET_CROSS_SELL_DETAIL
(
IN_CROSS_SELL_MASTER_ID          IN JOE.CROSS_SELL_DETAIL.CROSS_SELL_MASTER_ID%TYPE,
CROSS_SELL_CUR                  OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

cursor image_url IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'IMAGE_SERVER_HTTP_URL';

v_image_url                VARCHAR2(100) := '';

BEGIN

    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    OPEN CROSS_SELL_CUR FOR
        select display_order_seq_num,
            csd.cross_sell_product_id,
            csd.cross_sell_upsell_id,
            nvl(csd.cross_sell_product_id,csd.cross_sell_upsell_id) cross_sell_display_id,
            pm.novator_id,
            decode(pm.status,'A','YES','NO') status,
            pm.over_21,
            pm.product_type,
            pm.novator_name,
            pm.standard_price,
      pm.long_description,
            to_char(pm.exception_start_date,'MM/dd/yyyy') exception_start_date,
            to_char(pm.exception_end_date,'MM/dd/yyyy') exception_end_date,
            v_image_url || '/' || pm.NOVATOR_ID || '_a.jpg' smallImage,
            v_image_url || '/' || pm.NOVATOR_ID || '_c.jpg' largeImage
        from joe.cross_sell_detail csd
        join ftd_apps.product_master pm on
            nvl(csd.cross_sell_product_id,(select upsell_detail_id from ftd_apps.upsell_detail
                where upsell_master_id = csd.cross_sell_upsell_id and upsell_detail_sequence = '1')) = pm.product_id
        where csd.cross_sell_master_id = in_cross_sell_master_id
        order by csd.cross_sell_master_id, csd.display_order_seq_num;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        OPEN CROSS_SELL_CUR for select * from dual where dummy = 'Y';
        OUT_STATUS := 'Y';
        OUT_MESSAGE := '';
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GET_CROSS_SELL_DETAIL;

PROCEDURE GET_PRODUCT_ID
(
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_ALLOW_SUBCODES               IN  VARCHAR2,
IN_ALLOW_UPSELLS                IN  VARCHAR2,
OUT_PRODUCT_ID                  OUT VARCHAR2,
OUT_NOVATOR_ID                  OUT VARCHAR2,
OUT_UPSELL_MASTER_ID            OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure obtains the product_id and novator_id for a 
             product_id which can be a novator id or product_id

Input:  product_id         Can be novator_id or product_id
        in_allow_subcodes  If a product_id can be a subcode.  product_id returned 
                           would be the master product id, if a subcode is passed in.

Output: Product_id from product_master
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN

    IF IN_ALLOW_SUBCODES is NOT NULL AND IN_ALLOW_SUBCODES = 'Y' THEN
      BEGIN
          SELECT ps.PRODUCT_ID, pm.NOVATOR_ID
          INTO OUT_PRODUCT_ID, OUT_NOVATOR_ID
          FROM FTD_APPS.PRODUCT_SUBCODES ps
          JOIN FTD_APPS.PRODUCT_MASTER pm
          ON ps.PRODUCT_ID = pm.PRODUCT_ID
          WHERE ps.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID;
      EXCEPTION WHEN OTHERS THEN
          BEGIN
            SELECT PRODUCT_ID, NOVATOR_ID
            INTO OUT_PRODUCT_ID, OUT_NOVATOR_ID
            FROM FTD_APPS.PRODUCT_MASTER
            WHERE NOVATOR_ID = IN_PRODUCT_ID
               OR PRODUCT_ID = IN_PRODUCT_ID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
      END;
    ELSE
        BEGIN
          SELECT PRODUCT_ID, NOVATOR_ID
          INTO OUT_PRODUCT_ID, OUT_NOVATOR_ID
          FROM FTD_APPS.PRODUCT_MASTER
          WHERE NOVATOR_ID = IN_PRODUCT_ID
             OR PRODUCT_ID = IN_PRODUCT_ID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
    END IF;

    IF IN_ALLOW_UPSELLS is NOT NULL AND IN_ALLOW_UPSELLS = 'Y' AND OUT_PRODUCT_ID IS NULL THEN
      BEGIN
          SELECT UPSELL_MASTER_ID 
          INTO OUT_UPSELL_MASTER_ID
          FROM FTD_APPS.UPSELL_MASTER
          WHERE UPSELL_MASTER_ID = IN_PRODUCT_ID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    END IF;


    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_PRODUCT_ID;


PROCEDURE GET_IOTW
(
IN_IOTW_ID                      IN  VARCHAR2,
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_SOURCE_CODE                  IN  VARCHAR2,
IN_IOTW_SOURCE_CODE             IN  VARCHAR2,
IN_EXPIRED_ONLY                 IN  VARCHAR2,
IOTW_CUR                        OUT TYPES.REF_CURSOR,
DEL_DISC_DATE_CUR               OUT TYPES.REF_CURSOR,
PRICE_HEADER_CUR                OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving IOTW programs.
The procedure will return all programs if no IN_IOTW_ID exists otherwise
the results are limited to the IN_IOTW_ID.

Input:  iotw_id

Output: cursor containing IOTW info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN
      OPEN IOTW_CUR FOR
      SELECT
            i.iotw_id,
            i.source_code,
            DECODE(i.product_id, NULL, i.upsell_master_id, i.product_id) product_id,
            i.start_date,
            i.end_date,
            to_char(i.start_date,'mm/dd/yyyy') start_date_fmt,
            to_char(i.end_date,'mm/dd/yyyy') end_date_fmt,
            i.product_page_messaging_txt,
            i.calendar_messaging_txt,
            i.iotw_source_code,
            i.special_offer_flag,
            i.wording_color_txt,
            i.created_by,
            DECODE(i.product_id, NULL, DECODE((select um.upsell_status from ftd_apps.upsell_master um where um.upsell_master_id = i.upsell_master_id), 'Y', 'true', 'false'), DECODE((select pm.status from ftd_apps.product_master pm where pm.product_id = i.product_id), 'A', 'true', 'false')) product_available,
            (

                SELECT (pp.PARTNER_NAME || ' ' || CASE WHEN pr.points > 1 THEN pr.points || '' ELSE pr.points*100 || '%' END || ' ' || pr.reward_type || ' ' || cbv.description)
                FROM ftd_apps.source_program_ref spr
                JOIN ftd_apps.program_reward pr
                ON spr.program_name = pr.program_name
                JOIN ftd_apps.partner_program pp
                ON spr.program_name = pp.program_name
                JOIN ftd_apps.calculation_basis_val cbv
                ON pr.calculation_basis = cbv.calculation_basis
                WHERE i.iotw_source_code = spr.source_code
                AND spr.start_date =
                  (
                      select  max(spr2.start_date)
                      from    ftd_apps.source_program_ref spr2
                      where   spr2.source_code = i.iotw_source_code
                      and     spr2.start_date <= sysdate
                  )
            ) promotion_discount
      FROM joe.iotw i
      WHERE (IN_IOTW_ID IS NULL OR i.iotw_id = IN_IOTW_ID)
      AND (in_product_id is null or i.product_id = in_product_id or i.upsell_master_id = in_product_id)
      AND (in_source_code is null or i.source_code = in_source_code)
      AND (in_iotw_source_code is null or i.iotw_source_code = in_iotw_source_code)
      AND (in_expired_only is null or i.end_date < trunc(sysdate));

      OPEN DEL_DISC_DATE_CUR FOR
      SELECT
            ddd.iotw_id,
            ddd.discount_date,
            to_char(ddd.discount_date,'mm/dd/yyyy') discount_date_fmt
      FROM joe.iotw_delivery_discount_dates ddd
      WHERE (IN_IOTW_ID IS NULL OR ddd.iotw_id = IN_IOTW_ID);

      OPEN PRICE_HEADER_CUR FOR
      SELECT i.iotw_source_code,
          ph.description,
          phd.min_dollar_amt,
          phd.max_dollar_amt,
          phd.discount_type,
          phd.discount_amt
      FROM (select distinct(iotw_source_code) iotw_source_code from joe.iotw) i
      join ftd_apps.source_master sm
      on sm.source_code = i.iotw_source_code
      join ftd_apps.price_header ph
      on ph.price_header_id = sm.price_header_id
      join ftd_apps.price_header_details phd
      on phd.price_header_id = ph.price_header_id;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_IOTW;

PROCEDURE GET_IOTW_BY_PROD_SRC_CDE
(
IN_SOURCE_CODE                  IN  VARCHAR2,
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_UPSELL_MASTER_ID             IN  VARCHAR2,
IOTW_CUR                        OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving IOTW programs based
on product id or upsell master id and source code.

Input:  source_code
        product_id
        upsell_master_id

Output: cursor containing IOTW info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN
      OPEN IOTW_CUR FOR
      SELECT
            i.iotw_id,
            i.source_code,
            i.product_id,
            i.upsell_master_id,
            i.start_date,
            i.end_date,
            i.product_page_messaging_txt,
            i.calendar_messaging_txt,
            i.iotw_source_code,
            i.special_offer_flag,
            i.wording_color_txt
      FROM joe.iotw i
      WHERE i.source_code = IN_SOURCE_CODE
      AND (i.product_id = IN_PRODUCT_ID OR i.upsell_master_id = IN_UPSELL_MASTER_ID);

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_IOTW_BY_PROD_SRC_CDE;

PROCEDURE GET_ADDRESS_TYPE_BY_CODE
(
IN_ADDRESS_TYPE_CODE             IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT
        prompt_for_business_flag,
        business_label_txt,
        prompt_for_room_flag,
        room_label_txt,
        prompt_for_hours_flag,
        hours_label_txt
    FROM frp.address_types
    WHERE address_type = in_address_type_code;

END GET_ADDRESS_TYPE_BY_CODE;

PROCEDURE GET_COST_CENTERS
(
IN_COST_CENTER_ID                IN VARCHAR2,
IN_PARTNER_ID                    IN VARCHAR2,
IN_SOURCE_CODE_ID                IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT
        COST_CENTER_ID,
        FILLER,
        PARTNER_ID,
        SOURCE_CODE_ID
    FROM FTD_APPS.COST_CENTERS
    WHERE COST_CENTER_ID = IN_COST_CENTER_ID
        AND PARTNER_ID = IN_PARTNER_ID
        AND SOURCE_CODE_ID = IN_SOURCE_CODE_ID;

END GET_COST_CENTERS;

PROCEDURE GET_AAA_MEMBERS
(
IN_AAA_MEMBER_ID                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT MEMBER_NAME
    FROM FTD_APPS.AAA_MEMBERS
    WHERE AAA_MEMBER_ID = IN_AAA_MEMBER_ID;

END GET_AAA_MEMBERS;

PROCEDURE GET_COUNTRY_MASTER_BY_ID
(
IN_COUNTRY_ID   IN FTD_APPS.COUNTRY_MASTER.COUNTRY_ID%TYPE,
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves country_master data for the country_id passed in.

Input:
country_id        VARCHAR2

Output:
cursor containing country_master info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT country_id,
         name,
         oe_country_type,
         oe_display_order,
         add_on_days,
         cutoff_time,
         monday_closed,
         tuesday_closed,
         wednesday_closed,
         thursday_closed,
         friday_closed,
         saturday_closed,
         sunday_closed,
         three_character_id,
         status
    FROM ftd_apps.country_master
   WHERE country_id = in_country_id
     AND status = 'Active';

END GET_COUNTRY_MASTER_BY_ID;

PROCEDURE GET_FRAUD_ORDER_DISPOSITION
(
IN_FRAUD_ID                      IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT FRAUD_DESCRIPTION
        FROM SCRUB.FRAUD_ORDER_DISPOSITIONS
        WHERE FRAUD_ID = IN_FRAUD_ID;

END GET_FRAUD_ORDER_DISPOSITION;


PROCEDURE GET_PREFERRED_PART_BY_SOURCE
(
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves the Preferred Partner name (e.g., USAA),
              if any, for the source code passed in.
-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CUR FOR
    SELECT   distinct pm.partner_name 
    FROM     ftd_apps.source_master sm,
             ftd_apps.source_program_ref spr,
             ftd_apps.partner_program pp,
             ftd_apps.partner_master pm
    WHERE    sm.source_code = IN_SOURCE_CODE
             AND spr.source_code = sm.source_code
             AND pp.program_name = spr.program_name
             AND pm.partner_name = pp.partner_name
             AND pm.preferred_partner_flag = 'Y';

END GET_PREFERRED_PART_BY_SOURCE;

PROCEDURE GET_AVS_ADDRESS
(
IN_EXTERNAL_ORDER_NUMBER            IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning AVS address information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in AVS address
        avs_address_id                  number
        address                         varchar2
        city                            varchar2
        state			                      varchar2
        zip			                        varchar2
        country                         varchar2
        latitude                        varchar2
        longitude                       varchar2
        entity_type                     varchar2
        override_flag                   varchar2
        avs_result                      varchar2
        avs_performed                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
			SELECT  AA.AVS_ADDRESS_ID,
					AA.RECIPIENT_ID,
					AA.ADDRESS,
					AA.CITY,
					AA.STATE,
					AA.ZIP,
					AA.COUNTRY,
					AA.LATITUDE,
					AA.LONGITUDE,
					AA.ENTITY_TYPE,
					AA.OVERRIDE_FLAG,
					AA.AVS_RESULT,
					AA.AVS_PERFORMED
			FROM    AVS_ADDRESS AA
			JOIN    ORDER_DETAILS OD
			ON      AA.RECIPIENT_ID = OD.RECIPIENT_ID
			WHERE   OD.EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

END GET_AVS_ADDRESS;


PROCEDURE GET_AVS_ADDRESS_SCORE
(
IN_EXTERNAL_ORDER_NUMBER            IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning AVS address score information
        for the given order or the specific recipient.

Input:
        avs_address_id                  number

Output:
        cursor result set containing all fields in AVS address score
        avs_address_score_id            number
        avs_address_id                  number
        reason                          varchar2
        score                           varchar2

-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
			SELECT  SC.AVS_SCORE_ID,
					SC.AVS_ADDRESS_ID,
					SC.SCORE_REASON,
					SC.SCORE
			FROM    AVS_ADDRESS_SCORE SC, AVS_ADDRESS AA
			JOIN    ORDER_DETAILS OD
			ON      AA.RECIPIENT_ID = OD.RECIPIENT_ID
			WHERE   OD.EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER AND AA.AVS_ADDRESS_ID = SC.AVS_ADDRESS_ID;

END GET_AVS_ADDRESS_SCORE;

PROCEDURE GET_UPDATE_ORDER_INFO
(
IN_EXTERNAL_ORDER_NUMBER            IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_CUR_ORDER                       OUT TYPES.REF_CURSOR,
OUT_CUR_ADDON                       OUT TYPES.REF_CURSOR)
AS

BEGIN

    OPEN OUT_CUR_ORDER FOR
        select o.language_id,
            o.order_guid,
            pm.product_name,
            od.order_detail_id,
            od.source_code,
            od.product_id,
            od.ship_method,
            to_char(od.delivery_date, 'yyyymmdd') delivery_date,
            to_char(od.delivery_date, 'mm/dd/yyyy') display_delivery_date,
            to_char(od.delivery_date_range_end, 'mm/dd/yyyy') delivery_date_range_end,
            od.occasion,
            od.size_indicator,
            od.card_message,
            od.miles_points,
            od.special_instructions,
            od.recipient_id,
            ship.description ship_method_description,
            c1.description color1_description,
            c2.description color2_description,
            decode (pr.program_name, null, phd.discount_type, pr.reward_type) discount_reward_type,
            recip.first_name recip_first_name,
            recip.last_name recip_last_name,
            recip.address_type recip_address_type,
            recip.address_1 recip_address_1,
            recip.address_2 recip_address_2,
            recip.business_name recip_business_name,
            recip.business_info recip_business_info,
            recip.city recip_city,
            recip.state recip_state,
            recip.zip_code recip_zip_code,
            recip.country recip_country,
            nvl(nvl(rdphone.phone_number,rephone.phone_number),'') recip_phone_number,
            nvl(nvl(rdphone.extension, rephone.extension),'') recip_phone_ext,
            buyer.first_name buyer_first_name,
            buyer.last_name buyer_last_name,
            buyer.address_type buyer_address_type,
            buyer.address_1 buyer_address_1,
            buyer.address_2 buyer_address_2,
            buyer.city buyer_city,
            buyer.state buyer_state,
            buyer.zip_code buyer_zip_code,
            buyer.country buyer_country,
            bephone.phone_number buyer_secondary_phone,
            nvl(bephone.extension, '') buyer_secondary_phone_ext,
            bdphone.phone_number buyer_primary_phone,
            nvl(bdphone.extension, '') buyer_primary_phone_ext,
            e.email_address buyer_email_address,
            m.membership_number,
            m.first_name membership_first_name,
            m.last_name membership_last_name,
			ob.product_amount - nvl (r.refund_product_amount, 0) product_amount,
            ob.discount_product_price  - nvl (r.refund_product_amount, 0 ) + nvl (r.refund_discount_amount, 0) discount_product_price,
			ob.tax - nvl (r.refund_tax, 0) tax,
            ob.add_on_amount + nvl(ob.add_on_discount_amount, 0) - nvl (r.refund_add_on_amount, 0) add_on_amount,
            ob.add_on_amount - nvl (r.refund_add_on_amount, 0) add_on_discount_amount,
			cc.cc_type,
            decode(cc.cc_number, null, null, global.encryption.decrypt_it (cc.cc_number, cc.key_name)) cc_number,
            cc.cc_expiration,
            pay.payment_type,
            od.original_order_has_sdu,
            p.wallet_indicator,
            od.time_of_service,
            nvl(pm1.update_order_flag, 'N') pi_modify_order_flag,
            pod.partner_id pi_partner_id,
            gp.value pi_modify_order_threshold,
            (ob1.pdb_price + nvl((select sum(oao.add_on_quantity * aoh.price)
                from clean.order_add_ons oao
                join ftd_apps.addon_history aoh
                on aoh.addon_history_id = oao.add_on_history_id
                where oao.order_detail_id = od.order_detail_id), 0)) pi_merc_total,
            ob1.pdb_price pi_pdb_price,
            od.florist_id
        from clean.orders o
        join clean.order_details od
        on od.order_guid = o.order_guid
        join clean.customer recip
        on recip.customer_id = od.recipient_id
        join clean.customer buyer
        on buyer.customer_id = o.customer_id
        left outer join clean.customer_phones rephone
        on rephone.customer_id = od.recipient_id
        and rephone.phone_type =  'Evening'
		left outer join clean.customer_phones rdphone
        on rdphone.customer_id = od.recipient_id
        and rdphone.phone_type =  'Day'
        left outer join clean.customer_phones bephone
        on bephone.customer_id = o.customer_id
        and bephone.phone_type = 'Evening'
        left outer join clean.customer_phones bdphone
        on bdphone.customer_id = o.customer_id
        and bdphone.phone_type = 'Day'
		join 
        (  select  ob.order_detail_id,
                 sum (ob.product_amount) product_amount,
                 sum (ob.discount_product_price) discount_product_price,
                 sum (ob.shipping_tax) + sum (ob.tax) tax,
                 sum (ob.add_on_amount) add_on_amount,
                 sum (ob.add_on_discount_amount) add_on_discount_amount
                from    clean.order_bills ob
                where   ob.order_detail_id = (select od.order_detail_id from clean.order_details od where od.external_order_number = IN_EXTERNAL_ORDER_NUMBER)
                group by ob.order_detail_id
        ) ob
        on ob.order_detail_id = od.order_detail_id
		left outer join
        ( select        r.order_detail_id,
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_tax) refund_tax,
                        sum (r.refund_addon_amount) refund_add_on_amount
                from    clean.refund r
                where   r.order_detail_id = (select od.order_detail_id from clean.order_details od where od.external_order_number = IN_EXTERNAL_ORDER_NUMBER)
                group by r.order_detail_id
        ) r
        on      r.order_detail_id = od.order_detail_id 
	    join clean.payments p
        on p.order_guid = o.order_guid
        left outer join clean.credit_cards cc
        on cc.cc_id = p.cc_id
        left outer join ftd_apps.payment_methods pay
        on pay.payment_method_id = cc.cc_type
        join ftd_apps.product_master pm
        on od.product_id = pm.product_id
        left outer join ftd_apps.color_master c1
        on od.color_1 = c1.color_master_id
        left outer join ftd_apps.color_master c2
        on od.color_2 = c2.color_master_id
        left outer join ftd_apps.ship_methods ship
        on ship.ship_method_id = od.ship_method
        left outer join clean.email e
        on e.email_id = o.email_id
        left outer join clean.memberships m
        on m.membership_id = o.membership_id
        join ftd_apps.source_master sm
        on sm.source_code = od.source_code
        join ftd_apps.price_header_details phd
        on sm.price_header_id = phd.price_header_id
        left outer join ftd_apps.source_program_ref spr
        on spr.source_code = od.source_code
        LEFT OUTER JOIN ftd_apps.program_reward pr
        ON     pr.program_name = spr.program_name
        left outer join ptn_pi.partner_order_detail pod
        on pod.confirmation_number = od.external_order_number
        left outer join ptn_pi.partner_mapping pm1
        on pm1.partner_id = pod.partner_id
        left outer join frp.global_parms gp
        on gp.context = 'OE_CONFIG'
        and gp.name = 'MODIFY_ORDER_THRESHOLD_' || pm1.partner_id
        join clean.order_bills ob1
        on ob1.order_detail_id = od.order_detail_id
        and ob1.additional_bill_indicator = 'N'
        where od.external_order_number = IN_EXTERNAL_ORDER_NUMBER;

    OPEN OUT_CUR_ADDON FOR
        select oao.add_on_code,
            oao.add_on_quantity,
            nvl (fa.description, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'DESCRIPTION')) add_on_description
        from clean.order_add_ons oao
        join clean.order_details od
        on od.order_detail_id = oao.order_detail_id
        left outer join ftd_apps.addon_history fa
        on oao.add_on_history_id = fa.addon_history_id
        where od.external_order_number = IN_EXTERNAL_ORDER_NUMBER;

END GET_UPDATE_ORDER_INFO;

PROCEDURE AJAX_GET_MEMBERSHIP_ID
(
IN_ZIP_CODE                      IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all zip_code rows for the given zip_code

Input:  zip_code

Output: cursor containing member ship ID info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        select distinct(am.aaa_member_id) ,am.member_name 
      	from FTD_APPS.aaa_members am,FTD_APPS.aaa_zipcode_clubcode azc
    	where azc.zipcode=IN_ZIP_CODE and 
		am.aaa_member_id in (select am2.aaa_member_id from FTD_APPS.aaa_members am2 where azc.clubcode =  substr(am2.aaa_member_id,4,6) and rownum=1);

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END AJAX_GET_MEMBERSHIP_ID;

FUNCTION CHECK_FEMOE
(
IN_ORDER_DETAIL_ID                  IN PARTNER_PRODUCT_UPDATE.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

OUT_STATUS         VARCHAR(10);

BEGIN
  
  OUT_STATUS := 'N';
	
  SELECT 'Y' INTO OUT_STATUS FROM PARTNER_PRODUCT_UPDATE WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

  RETURN OUT_STATUS;  
  
  EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        RETURN OUT_STATUS;
    END;

END CHECK_FEMOE;


PROCEDURE AJAX_GROUPON_SOURCE_CODES
(
IN_PROD_ID         IN VARCHAR2,
OUT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all source codes for the given 
             Groupon product.  This is only intended to be used with Groupon 
             source codes (that are defined as 'limit_product' indexex).
-----------------------------------------------------------------------------*/

cursor groupon_cur IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'OE_CONFIG'
             and name = 'GROUPON_SOURCE_CODES';

v_groupon_sources VARCHAR2(200) := '';

BEGIN

    -- Get list of all Groupon source codes from global_parms
    open groupon_cur;
    fetch groupon_cur into v_groupon_sources;
    close groupon_cur;

    OPEN OUT_CUR FOR
      SELECT distinct str.SOURCE_CODE, sm.PRICE_HEADER_ID, phd.DISCOUNT_TYPE, phd.DISCOUNT_AMT, pm.STANDARD_PRICE
      FROM
      ftd_apps.si_template_index_prod tip,
      ftd_apps.si_source_template_ref str,
      ftd_apps.source_master sm, ftd_apps.price_header_details phd,
      ftd_apps.product_master pm
      WHERE tip.si_template_id=str.si_template_id
      AND tip.index_name='limit_products' 
      AND INSTR(v_groupon_sources, '''' || str.SOURCE_CODE || '''') > 0
      AND sm.source_code = str.source_code
      AND phd.PRICE_HEADER_ID = sm.PRICE_HEADER_ID
      AND (sm.end_date is null OR sm.end_date >= trunc(sysdate))
      AND pm.product_id = IN_PROD_ID
      AND pm.standard_price between phd.min_dollar_amt and phd.max_dollar_amt
      AND tip.PRODUCT_ID = pm.product_id;

END AJAX_GROUPON_SOURCE_CODES;



PROCEDURE AJAX_PROMOTIONS_INFO
(
IN_SOURCE_CODE     IN VARCHAR2,
OUT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves source code info.
             It is intended for microservice consumption and should be moved
             elsewhere in the long term.
-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      --
      -- Active sourcecode, promotion, and program data (if any)
      --
      select sm.source_code, sm.description, sm.SOURCE_TYPE, sm.iotw_flag, ph.description as discount_description, 
             phd.discount_type, phd.discount_amt, sm.start_date, sm.end_date, 
             spr.PROGRAM_NAME, pm.preferred_partner_flag, pp.partner_name, pp.membership_data_required,
             'A' as available
      from ftd_apps.source_master sm  
      join ftd_apps.price_header_details phd on phd.price_header_id = sm.price_header_id
      join ftd_apps.price_header ph on ph.price_header_id = sm.price_header_id
      left outer join ftd_apps.source_program_ref spr on spr.source_code = sm.source_code
      left outer join ftd_apps.partner_program pp on pp.program_name = spr.program_name
      left outer join ftd_apps.partner_master pm on pm.partner_name = pp.partner_name
      WHERE 
      (sm.end_date is null or sm.end_date >= trunc(sysdate))
      and sm.start_date <= trunc(sysdate)
      and sm.source_code = IN_SOURCE_CODE
      --
      -- Inactive sourcecode
      --
      UNION
      select sm.source_code, sm.description, sm.SOURCE_TYPE, sm.iotw_flag, ph.description as discount_description, 
             phd.discount_type, phd.discount_amt, sm.start_date, sm.end_date, 
             null, null, null, null,
             'U' as available
      from ftd_apps.source_master sm  
      join ftd_apps.price_header_details phd on phd.price_header_id = sm.price_header_id
      join ftd_apps.price_header ph on ph.price_header_id = sm.price_header_id
      WHERE 
      sm.end_date < trunc(sysdate)
      and sm.source_code = IN_SOURCE_CODE;

END AJAX_PROMOTIONS_INFO;


PROCEDURE AJAX_PROMOTIONS_IOTW
(
IN_SOURCE_CODE     IN VARCHAR2,
OUT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves source code IOTW information.
             It is intended for microservice consumption and should be moved
             elsewhere in the long term.
-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      -- IOTW data for particular sourcecode
      -- 
      select iotw.source_code, iotw.product_id, ph.description as discount_description,
             NVL(
               NVL((select pm.novator_id from ftd_apps.product_master pm where pm.product_id = iotw.product_id), iotw.upsell_master_id), 
               iotw.product_id) as novator_id, 
             sm.description as iotw_description, iotw.iotw_source_code,
             phd.discount_type, phd.discount_amt,
             iotw.upsell_master_id, iotw.start_date, iotw.end_date,
             (select listagg(to_char(iddd.discount_date,'YYYY-MM-DD'),', ') 
              within group (order by iddd.iotw_id) discount_dates
              from joe.iotw_delivery_discount_dates iddd 
              where iddd.iotw_id = iotw.iotw_id
              group by iddd.iotw_id
             ) as discount_dates    
      from joe.iotw 
      join ftd_apps.source_master sm on sm.source_code = iotw.iotw_source_code
      join ftd_apps.price_header_details phd on phd.price_header_id = sm.price_header_id
      join ftd_apps.price_header ph on ph.price_header_id = sm.price_header_id
      WHERE 
      (iotw.end_date is null or iotw.end_date >= trunc(sysdate)) 
      and iotw.start_date <= trunc(sysdate)
      and iotw.SOURCE_CODE = IN_SOURCE_CODE;      

END AJAX_PROMOTIONS_IOTW;



PROCEDURE FRESH_GET_PRODUCT_CATEGORIES
(
IN_PRODUCT_ID     IN VARCHAR2,
IN_COMPANY_ID     IN VARCHAR2,
OUT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves categories for a product.
             It is intended for microservice consumption and should be moved
             elsewhere in the long term.
             Note that in_company_id is passed as null for FTD.
-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
      select distinct pi.company_id,
                      pi.index_file_name  as index_file_1, pi.index_name  as index_name_1,  
                      pi2.index_file_name as index_file_2, pi2.index_name as index_name_2,  
                      pi3.index_file_name as index_file_3, pi3.index_name as index_name_3, 
                      pi4.index_file_name as index_file_4, pi4.index_name as index_name_4, 
                      pi5.index_file_name as index_file_5, pi5.index_name as index_name_5, 
                      pi6.index_file_name as index_file_6, pi6.index_name as index_name_6 
      from ftd_apps.product_index pi 
      left outer join ftd_apps.product_index pi2 on pi2.index_id = pi.parent_index_id
      left outer join ftd_apps.product_index pi3 on pi3.index_id = pi2.parent_index_id
      left outer join ftd_apps.product_index pi4 on pi4.index_id = pi3.parent_index_id
      left outer join ftd_apps.product_index pi5 on pi5.index_id = pi4.parent_index_id
      left outer join ftd_apps.product_index pi6 on pi6.index_id = pi5.parent_index_id
      where 
      decode(pi.company_id, null, 'ISNULL', pi.company_id) = decode(IN_COMPANY_ID, null, 'ISNULL', IN_COMPANY_ID)
      and pi.index_id in (select product_index_id from ftd_apps.product_index_xref where product_id = IN_PRODUCT_ID);

END FRESH_GET_PRODUCT_CATEGORIES;



PROCEDURE FRESH_GET_PRODUCT_DETAILS
(
IN_PRODUCT_ID                IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
OUT_CUR_PRODUCT                 OUT TYPES.REF_CURSOR,
OUT_CUR_UPSELL                  OUT TYPES.REF_CURSOR,
OUT_CUR_COLOR                   OUT TYPES.REF_CURSOR,
OUT_CUR_ADDON                   OUT TYPES.REF_CURSOR,
OUT_CUR_VASE                    OUT TYPES.REF_CURSOR,
OUT_CUR_RESTRICT                OUT TYPES.REF_CURSOR,
OUT_CUR_UMRESTRICT              OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

  FRESH_GET_PRODUCT_DETAILS
  (
    IN_PRODUCT_ID,
    IN_SOURCE_CODE,
    null,
    OUT_CUR_PRODUCT,
    OUT_CUR_UPSELL,
    OUT_CUR_COLOR,
    OUT_CUR_ADDON,
    OUT_CUR_VASE,
    OUT_CUR_RESTRICT,
    OUT_CUR_UMRESTRICT,
    OUT_STATUS,
    OUT_MESSAGE     
  );


END FRESH_GET_PRODUCT_DETAILS;


PROCEDURE FRESH_GET_PRODUCT_DETAILS
(
IN_PRODUCT_ID                IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
IN_VALID_UPSELL_IDS              IN VARCHAR2,
OUT_CUR_PRODUCT                 OUT TYPES.REF_CURSOR,
OUT_CUR_UPSELL                  OUT TYPES.REF_CURSOR,
OUT_CUR_COLOR                   OUT TYPES.REF_CURSOR,
OUT_CUR_ADDON                   OUT TYPES.REF_CURSOR,
OUT_CUR_VASE                    OUT TYPES.REF_CURSOR,
OUT_CUR_RESTRICT                OUT TYPES.REF_CURSOR,
OUT_CUR_UMRESTRICT              OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

cursor get_global_parm(in_context varchar2, in_name varchar2) IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = in_context
             and name = in_name;

cursor get_snh_by_id(in_snh_id varchar2) IS
    SELECT   first_order_domestic
    FROM     FTD_APPS.SNH
    WHERE    SNH_ID = in_snh_id;

cursor image_url IS
    SELECT   value
    FROM     frp.global_parms 
    WHERE    context = 'SERVICE'
             and name = 'FRESH_IMAGE_BASE_URL';

cursor get_preferred_partner_flag IS
    SELECT   distinct pm.preferred_partner_flag
    FROM     ftd_apps.source_master sm,
             ftd_apps.source_program_ref spr,
             ftd_apps.partner_program pp,
             ftd_apps.partner_master pm
    WHERE    sm.source_code = IN_SOURCE_CODE
             AND spr.source_code = sm.source_code
             AND pp.program_name = spr.program_name
             AND pm.partner_name = pp.partner_name;

cursor get_add_on_free_id is
    SELECT   add_on_free_id
    FROM     ftd_apps.source_master
    WHERE    source_code = IN_SOURCE_CODE;

v_pricingCode              FTD_APPS.SOURCE_MASTER.PRICE_HEADER_ID%TYPE := NULL;
v_snh_id                   FTD_APPS.SOURCE_MASTER.SNH_ID%TYPE := NULL;
v_upsell_master_id         VARCHAR2(100) := UPPER(IN_PRODUCT_ID);
out_productID              VARCHAR2(100) := UPPER(IN_PRODUCT_ID);
v_prod_is_upsell_master    VARCHAR2(100) := NULL;
v_prod_upsell_default      VARCHAR2(100) := NULL;
v_prod_upsell_master_name  VARCHAR2(100) := NULL;
v_global_chocolate_flag    VARCHAR2(10) := 'Y';
v_image_url                VARCHAR2(100) := '';
v_shipping_key             VARCHAR2(100);
v_price                    NUMBER;
v_ship_method_carrier      VARCHAR2(100) := 'N';
v_shipping_key_detail_id   VARCHAR2(100) := 'XX';
v_product_type             varchar2(100) := '';
v_global_freshcut_flag     VARCHAR2(100) := 'N';
v_freshcut_value           VARCHAR2(100) := '0.00';
v_freshcut_saturday_value  VARCHAR2(100) := '0.00';
v_preferred_partner_flag   VARCHAR2(100) := 'N';
v_add_on_free_id           VARCHAR2(100) := '';
v_is_addon_and_prod        VARCHAR2(10)  := 'N';
v_is_addon_only            VARCHAR2(10)  := 'N';
v_upsell_master_companies  VARCHAR2(100) := NULL;

BEGIN

    -- See if this is an upsell master
    BEGIN
        SELECT um.upsell_master_id, ud.upsell_detail_id, um.upsell_name,
               (select listagg(ucx.company_id,',') within group (order by ucx.product_id) 
                from ftd_apps.upsell_company_xref ucx where ucx.product_id = um.upsell_master_id group by ucx.product_id
               )        
        INTO  v_prod_is_upsell_master, v_prod_upsell_default, v_prod_upsell_master_name, v_upsell_master_companies
        FROM  FTD_APPS.UPSELL_MASTER um
        JOIN  FTD_APPS.UPSELL_DETAIL ud ON ud.upsell_master_id = um.upsell_master_id 
        WHERE um.UPSELL_MASTER_ID = out_productID
        AND   ud.default_sku_flag = 'Y';
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- See if this is an addon and is also a product (i.e., vendor addons)
    BEGIN
      SELECT DISTINCT PRODUCT_ID, 'Y', 'N'
      INTO   out_productID, v_is_addon_and_prod, v_is_addon_only
      FROM   FTD_APPS.ADDON
      WHERE  (ADDON_ID = out_productID AND PRODUCT_ID IS NOT NULL)
         OR  (PRODUCT_ID = out_productID);
    EXCEPTION WHEN NO_DATA_FOUND THEN
       NULL;
    END;

    -- Get Apollo product_id from passed in ftd_product_id (i.e. Novator ID).
    -- Otherwise try product_id or addon_id
    BEGIN
      IF v_is_addon_and_prod = 'N' THEN    
          BEGIN 
             -- Novator ID was passed in
             SELECT PRODUCT_ID
             INTO out_productID
             FROM FTD_APPS.PRODUCT_MASTER
             WHERE NOVATOR_ID = out_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
             BEGIN 
               -- PDB product ID was passed in
               SELECT PRODUCT_ID
               INTO out_productID
               FROM FTD_APPS.PRODUCT_MASTER
               WHERE PRODUCT_ID = out_productID;
             EXCEPTION WHEN NO_DATA_FOUND THEN
              BEGIN 
                 -- Addon ID was passed in and is NOT a product (i.e., floral addon)
                 SELECT 'N', 'Y'
                 INTO v_is_addon_and_prod, v_is_addon_only
                 FROM FTD_APPS.ADDON
                 WHERE ADDON_ID = out_productID
                 AND PRODUCT_ID IS NULL;
               EXCEPTION WHEN NO_DATA_FOUND THEN
                 NULL;
               END; 
             END;        
          END; 
      END IF;
    END; 

    -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICE_HEADER_ID, SNH_ID
        INTO v_pricingCode, v_snh_id
        FROM FTD_APPS.SOURCE_MASTER
        WHERE SOURCE_CODE = in_source_code;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- Get upsell master associated with product (if any)
    BEGIN
        SELECT UPSELL_MASTER_ID
        INTO v_upsell_master_id
        FROM FTD_APPS.UPSELL_DETAIL
        WHERE UPSELL_DETAIL_ID = out_productID;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    begin
        -- If an upsell master was passed as the product id, then use the default upsell detail as product
        if (v_prod_is_upsell_master is not null) then
           out_productID := v_prod_upsell_default;
        end if; 

        select shipping_key, standard_price, ship_method_carrier, product_type
        into v_shipping_key, v_price, v_ship_method_carrier, v_product_type
        from ftd_apps.product_master
        where product_id = out_productID;

        if (v_ship_method_carrier = 'Y') then
            select shipping_detail_id into v_shipping_key_detail_id
            from ftd_apps.shipping_key_details
            where shipping_key_id = v_shipping_key
                and min_price <= v_price
                and max_price >= v_price;
        end if;

    exception when no_data_found then
        v_ship_method_carrier := 'N';
    end;

    -- Get the global chocolate flag from frp.global_parms
    open get_global_parm('FTDAPPS_PARMS', 'MOD_ORDER_CHOCOLATES_AVAILABLE');
    fetch get_global_parm into v_global_chocolate_flag;
    close get_global_parm;

    -- Get the image url from frp.global_parms
    open image_url;
    fetch image_url into v_image_url;
    close image_url;

    -- Get the preferred_partner_flag
    open get_preferred_partner_flag;
    fetch get_preferred_partner_flag into v_preferred_partner_flag;
    close get_preferred_partner_flag;

    -- Get the add_on_free_id
    open get_add_on_free_id;
    fetch get_add_on_free_id into v_add_on_free_id;
    close get_add_on_free_id;

    IF (v_is_addon_only = 'N' AND v_is_addon_and_prod = 'N') THEN

       -- Normal product
       --              
       OPEN OUT_CUR_PRODUCT FOR
         SELECT
           PM.PRODUCT_ID AS APOLLO_PRODUCT_ID,
           PM.MONDAY_DELIVERY_FRESHCUT AS MONDAY_DELIVERY_FRESHCUT,
           PM.MORNING_DELIVERY_FLAG AS MORNING_DELIVERY_FLAG,
           PM.EXPRESS_SHIPPING_ONLY AS EXPRESS_SHIPPING_ONLY,
           PM.NEXT_DAY_UPGRADE_FLAG AS NEXT_DAY_UPGRADE_FLAG,
           PM.NOVATOR_ID AS FTD_PRODUCT_ID,
           PM.PRODUCT_NAME AS APOLLO_PRODUCT_NAME,
           PM.NOVATOR_NAME AS FTD_PRODUCT_NAME,
           v_prod_is_upsell_master AS UPSELL_MASTER_PROD_ID,
           v_prod_upsell_master_name AS UPSELL_MASTER_NAME,
           PM.STATUS,
           PM.DELIVERY_TYPE,
           PM.PRODUCT_TYPE,
           PM.COLOR_SIZE_FLAG,
           PM.STANDARD_PRICE,
           PM.DELUXE_PRICE,
           PM.PREMIUM_PRICE,
           PM.VARIABLE_PRICE_MAX,
           PM.SHORT_DESCRIPTION,
           PM.LONG_DESCRIPTION,
           PM.ADD_ON_CARDS_FLAG,
           PM.ADD_ON_FUNERAL_FLAG,
           PM.EXCEPTION_CODE,
           TO_CHAR(PM.EXCEPTION_START_DATE, 'mm/dd/yyyy') EXCEPTION_START_DATE,
           TO_CHAR(PM.EXCEPTION_END_DATE, 'mm/dd/yyyy') EXCEPTION_END_DATE,
           PM.EXCEPTION_MESSAGE,
           PM.SHIP_METHOD_CARRIER,
           PM.SHIP_METHOD_FLORIST,
           PM.SHIPPING_KEY,
           PM.SHIPPING_SYSTEM,
           PM.WEBOE_BLOCKED,
           PM.CUSTOM_FLAG,
           PM.NO_TAX_FLAG,
           PM.DISCOUNT_ALLOWED_FLAG,
           PM.POPULARITY_ORDER_CNT,
           decode(substr(lower(psc.oe_description1), 1,4), 'none', null, PSC.OE_DESCRIPTION1 || PSC.OE_DESCRIPTION2) "second_choice",
           v_image_url ||  PM.NOVATOR_ID || '_120x120.jpg' "smallImage",
           v_image_url || 'zoom/' || PM.NOVATOR_ID || '_600x600.jpg' "largeImage",
           PM.OVER_21,
           PM.PERSONAL_GREETING_FLAG,
           PM.PREMIER_COLLECTION_FLAG,
           PM.ADD_ON_FREE_ID,
           PM.ALLOW_FREE_SHIPPING_FLAG,
           PM.PQUAD_PRODUCT_ID,
           PM.BULLET_DESCRIPTION, 
           PM.PRODUCT_SUB_TYPE, 
           PM.CATEGORY AS PDB_CATEGORY, 
           v_is_addon_and_prod as IS_ADDON_PRODUCT, 
           (select listagg(psm.ship_method_id,',') within group (order by psm.product_id) 
            from ftd_apps.product_ship_methods psm where psm.product_id = pm.product_id group by psm.product_id
           ) as SHIPPING_METHODS, 
           (select listagg(pcx.company_id,',') within group (order by pcx.product_id) 
            from ftd_apps.product_company_xref pcx where pcx.product_id = pm.product_id group by pcx.product_id
           ) as COMPANIES,
           v_upsell_master_companies as UPSELL_MASTER_COMPANIES
         FROM FTD_APPS.PRODUCT_MASTER PM,
           FTD_APPS.PRODUCT_SECOND_CHOICE PSC
         WHERE PM.PRODUCT_ID = out_productID
           AND PSC.PRODUCT_SECOND_CHOICE_ID (+) = PM.SECOND_CHOICE_CODE;

    ELSIF (v_is_addon_and_prod = 'Y') THEN

       -- Addon that is also a product (i.e., vendor addon)
       --
       OPEN OUT_CUR_PRODUCT FOR
         SELECT
           PM.PRODUCT_ID AS APOLLO_PRODUCT_ID,
           PM.MONDAY_DELIVERY_FRESHCUT AS MONDAY_DELIVERY_FRESHCUT,
           PM.MORNING_DELIVERY_FLAG AS MORNING_DELIVERY_FLAG,
           PM.EXPRESS_SHIPPING_ONLY AS EXPRESS_SHIPPING_ONLY,
           PM.NEXT_DAY_UPGRADE_FLAG AS NEXT_DAY_UPGRADE_FLAG,
           PM.NOVATOR_ID AS FTD_PRODUCT_ID,
           PM.PRODUCT_NAME AS APOLLO_PRODUCT_NAME,
           A.DESCRIPTION AS FTD_PRODUCT_NAME,
           DECODE(A.ACTIVE_FLAG, 'Y','A','U') AS STATUS,
           PM.DELIVERY_TYPE,
           PM.PRODUCT_TYPE,
           PM.COLOR_SIZE_FLAG,
           A.PRICE AS STANDARD_PRICE,
           PM.VARIABLE_PRICE_MAX,
           PM.SHORT_DESCRIPTION,
           A.DESCRIPTION AS LONG_DESCRIPTION,
           PM.ADD_ON_CARDS_FLAG,
           PM.ADD_ON_FUNERAL_FLAG,
           A.FLORIST_ADDON_FLAG AS SHIP_METHOD_FLORIST,
           A.VENDOR_ADDON_FLAG AS SHIP_METHOD_CARRIER,
           PM.SHIPPING_KEY,
           PM.SHIPPING_SYSTEM,
           PM.CUSTOM_FLAG,
           PM.NO_TAX_FLAG,
           PM.DISCOUNT_ALLOWED_FLAG,
           v_image_url || 'addons/' || A.addon_id || '_99x99.jpg' "smallImage",
           PM.OVER_21,
           PM.PERSONAL_GREETING_FLAG,
           PM.PREMIER_COLLECTION_FLAG,
           PM.ADD_ON_FREE_ID,
           PM.ALLOW_FREE_SHIPPING_FLAG,
           A.PQUAD_ACCESSORY_ID AS PQUAD_PRODUCT_ID, 
           PM.BULLET_DESCRIPTION, 
           PM.PRODUCT_SUB_TYPE, 
           PM.CATEGORY AS PDB_CATEGORY, 
           A.ADDON_TEXT,
           A.ADDON_ID AS ADDON_ID,
           'Y' as IS_ADDON_PRODUCT, 
           (select listagg(pcx.company_id,',') within group (order by pcx.product_id) 
            from ftd_apps.product_company_xref pcx where pcx.product_id = pm.product_id group by pcx.product_id
           ) as COMPANIES
         FROM FTD_APPS.PRODUCT_MASTER PM,
           FTD_APPS.ADDON A
         WHERE PM.PRODUCT_ID = out_productID
           AND A.PRODUCT_ID = PM.PRODUCT_ID;

    ELSIF (v_is_addon_only = 'Y') THEN

       -- Addon only (i.e., floral addon)
       --
       OPEN OUT_CUR_PRODUCT FOR
         SELECT
           A.ADDON_ID AS APOLLO_PRODUCT_ID,
           A.ADDON_ID AS ADDON_ID,
           A.DESCRIPTION AS APOLLO_PRODUCT_NAME,
           DECODE(A.ACTIVE_FLAG, 'Y','A','U') AS STATUS,
           'ADDON_ONLY' AS PRODUCT_TYPE,
           A.PRICE AS STANDARD_PRICE,
           A.DESCRIPTION AS LONG_DESCRIPTION,
           A.FLORIST_ADDON_FLAG AS SHIP_METHOD_FLORIST,
           A.VENDOR_ADDON_FLAG AS SHIP_METHOD_CARRIER,
           v_image_url || 'addons/' || 
                DECODE(A.ADDON_TYPE,'1','A','2','B','4','RC999','5','C',A.ADDON_ID) || '_99x99.jpg' "smallImage",
           A.PQUAD_ACCESSORY_ID AS PQUAD_PRODUCT_ID, 
           A.ADDON_TEXT,
           'Y' AS IS_ADDON_PRODUCT 
         FROM FTD_APPS.ADDON A
         WHERE A.ADDON_ID = out_productID;
         
    END IF;
        

    if (IN_VALID_UPSELL_IDS is null) then
      OPEN OUT_CUR_UPSELL FOR
         SELECT
           UD.UPSELL_DETAIL_ID,
           UD.UPSELL_MASTER_ID,
           UD.UPSELL_DETAIL_NAME,
           UD.UPSELL_DETAIL_SEQUENCE,
           UD.DEFAULT_SKU_FLAG, 
           PM.STANDARD_PRICE "UPSELL_PRICE",
           PM.NOVATOR_ID as FTD_PRODUCT_ID,
           v_image_url || PM.NOVATOR_ID || '_120x120.jpg' as smallImage
         FROM FTD_APPS.UPSELL_DETAIL UD,
           FTD_APPS.UPSELL_MASTER UM,
           FTD_APPS.PRODUCT_MASTER PM
         WHERE UD.UPSELL_MASTER_ID = v_upsell_master_id
           AND UM.UPSELL_MASTER_ID = UD.UPSELL_MASTER_ID
           AND UM.UPSELL_STATUS = 'Y'
           AND PM.PRODUCT_ID = UD.UPSELL_DETAIL_ID
           AND PM.STATUS = 'A'
           AND PM.WEBOE_BLOCKED <> 'Y'
         ORDER BY UD.UPSELL_DETAIL_SEQUENCE;
    else
      OPEN OUT_CUR_UPSELL FOR
         'SELECT
           UD.UPSELL_DETAIL_ID,
           UD.UPSELL_MASTER_ID,
           UD.UPSELL_DETAIL_NAME,
           UD.UPSELL_DETAIL_SEQUENCE,
           UD.DEFAULT_SKU_FLAG,
           PM.STANDARD_PRICE as UPSELL_PRICE,
           PM.NOVATOR_ID as FTD_PRODUCT_ID, 
           ''' || v_image_url || ''' || PM.NOVATOR_ID || ''_120x120.jpg'' as smallImage
         FROM FTD_APPS.UPSELL_DETAIL UD,
           FTD_APPS.UPSELL_MASTER UM,
           FTD_APPS.PRODUCT_MASTER PM
         WHERE UD.UPSELL_MASTER_ID = ''' || v_upsell_master_id || '''
           AND UM.UPSELL_MASTER_ID = UD.UPSELL_MASTER_ID
           AND UM.UPSELL_STATUS = ''Y''
           AND PM.PRODUCT_ID = UD.UPSELL_DETAIL_ID
           AND PM.STATUS = ''A''
           AND PM.WEBOE_BLOCKED <> ''Y''
           AND UD.UPSELL_DETAIL_ID in (' || IN_VALID_UPSELL_IDS || ')
         ORDER BY UD.UPSELL_DETAIL_SEQUENCE';

    end if; 

    OPEN OUT_CUR_COLOR FOR
      SELECT
        PC.PRODUCT_COLOR,
        CM.DESCRIPTION,
        CM.MARKER_TYPE,
        PC.ORDER_BY
      FROM FTD_APPS.PRODUCT_COLORS PC,
        FTD_APPS.COLOR_MASTER CM
      WHERE PC.PRODUCT_ID = out_productID
        AND CM.COLOR_MASTER_ID = PC.PRODUCT_COLOR
      ORDER BY PC.ORDER_BY;

    if (v_ship_method_carrier = 'Y') then
        if (v_product_type = 'FRECUT' OR v_product_type = 'SDFC') then

            -- Get the global fresh cut flag from frp.global_parms
            open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SVC_CHARGE_TRIGGER');
            fetch get_global_parm into v_global_freshcut_flag;
            close get_global_parm;

            if (v_global_freshcut_flag = 'Y') then

                -- Get the global fresh cut value from frp.global_parms
                open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SVC_CHARGE');
                fetch get_global_parm into v_freshcut_value;
                close get_global_parm;

            else

                -- Get the fresh cut value from the first_order_domestic column of
                -- ftd_apps.snh
                open get_snh_by_id(v_snh_id);
                fetch get_snh_by_id into v_freshcut_value;
                close get_snh_by_id;

            end if;

            -- Get the saturday fresh cut fee from frp.global_parms
            open get_global_parm('FTDAPPS_PARMS', 'FRESHCUTS_SAT_CHARGE');
            fetch get_global_parm into v_freshcut_saturday_value;
            close get_global_parm;

            v_freshcut_saturday_value := v_freshcut_saturday_value + v_freshcut_value;

        end if;
    end if;

    OPEN OUT_CUR_ADDON FOR
        select a.addon_id, a.description, a.price, pa.max_qty, a.addon_type, florist_addon_flag, vendor_addon_flag,
               a.product_id AS APOLLO_PRODUCT_ID, pa.display_seq_num, a.addon_text, a.pquad_accessory_id, 
               (SELECT NOVATOR_ID FROM FTD_APPS.PRODUCT_MASTER WHERE PRODUCT_ID = a.product_id) AS FTD_PRODUCT_ID,
               v_image_url || 'addons/' || a.addon_id || '_99x99.jpg' as image            
        from ftd_apps.addon a, ftd_apps.product_addon pa, ftd_apps.product_master pm
        where pa.product_id = out_productID
        and a.addon_id = pa.addon_id
        and a.active_flag = 'Y'
        and pa.active_flag = 'Y'
        and a.addon_type not in ('4', '7',
            decode(v_global_chocolate_flag, 'Y', decode(v_preferred_partner_flag, 'Y', decode(v_add_on_free_id, 'FC', '5', 'N/A'), 'N/A'), '5'),
            decode(v_add_on_free_id, a.addon_id, 'N/A', '6'))
        and pm.product_id = pa.product_id
        and ((pm.ship_method_florist = 'Y' and a.florist_addon_flag = 'Y') 
          or (pm.ship_method_carrier = 'Y' and a.vendor_addon_flag = 'Y' and exists (
            select 'Y' from ftd_apps.vendor_addon va, ftd_apps.vendor_product vp
            where va.addon_id = a.addon_id
            and va.active_flag = 'Y'
            and vp.vendor_id = va.vendor_id
            and vp.product_subcode_id = pa.product_id
            and vp.available = 'Y')))
        order by pa.display_seq_num;

    OPEN OUT_CUR_VASE FOR
        select a.addon_id, a.description, a.price, pa.max_qty, florist_addon_flag, vendor_addon_flag, 
               a.product_id AS APOLLO_PRODUCT_ID, pa.display_seq_num, a.addon_text, a.pquad_accessory_id,  
               (SELECT NOVATOR_ID FROM FTD_APPS.PRODUCT_MASTER WHERE PRODUCT_ID = a.product_id) AS FTD_PRODUCT_ID,  
               v_image_url || 'addons/' || a.addon_id || '_99x99.jpg' as image 
        from ftd_apps.addon a, ftd_apps.product_addon pa, ftd_apps.product_master pm
        where pa.product_id = out_productID
        and a.addon_id = pa.addon_id
        and a.active_flag = 'Y'
        and pa.active_flag = 'Y'
        and a.addon_type = '7'
        and pm.product_id = pa.product_id
        and ((pm.ship_method_florist = 'Y' and a.florist_addon_flag = 'Y') 
          or (pm.ship_method_carrier = 'Y' and a.vendor_addon_flag = 'Y' and exists (
            select 'Y' from ftd_apps.vendor_addon va, ftd_apps.vendor_product vp
            where va.addon_id = a.addon_id
            and va.active_flag = 'Y'
            and vp.vendor_id = va.vendor_id
            and vp.product_subcode_id = pa.product_id
            and vp.available = 'Y')))
        order by pa.display_seq_num;

    OPEN OUT_CUR_RESTRICT FOR
        select source_code from 
        ftd_apps.product_source
        where product_id = out_productID
        order by source_code;

    OPEN OUT_CUR_UMRESTRICT FOR
        select source_code from 
        ftd_apps.upsell_source
        where upsell_master_id = v_prod_is_upsell_master
        order by source_code;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END FRESH_GET_PRODUCT_DETAILS;



end OE_QUERY_PKG;
/
