create or replace package body joe.QUERY_PKG is

PROCEDURE AJAX_GET_ALL_CARD_MESSAGES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure retrieves all of the card_message records 

Input:  None

Output: cursor containing card_message info

-----------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
      SELECT
        CM.DISPLAY_SEQ,
        CM.MESSAGE_TXT,
        CM.ACTIVE_FLAG
      FROM JOE.CARD_MESSAGE CM
      ORDER BY DISPLAY_SEQ;
END AJAX_GET_ALL_CARD_MESSAGES;

end QUERY_PKG;
/
