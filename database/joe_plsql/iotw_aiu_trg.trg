CREATE OR REPLACE
TRIGGER JOE.IOTW_AIU_TRG
BEFORE INSERT OR UPDATE OR DELETE ON JOE.IOTW
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

    v_count                 number := 0;
    v_default_source_code   FRP.GLOBAL_PARMS.VALUE%TYPE;
    v_source_code           JOE.IOTW.SOURCE_CODE% TYPE;
    v_product_id            JOE.IOTW.PRODUCT_ID% TYPE;
    v_amz_product_id		number := 0;
	
BEGIN

    select value into v_default_source_code 
    from FRP.GLOBAL_PARMS 
    where CONTEXT='AMAZON_CONFIG' and NAME = 'default_source_code';
    
    IF DELETING THEN
      v_product_id := :old.product_id;
      v_source_code := :old.source_code;
    ELSE 
      v_product_id := :new.product_id;
      v_source_code := :new.source_code;
    END IF;
    
    select count(*) into v_count
            from PTN_AMAZON.az_price_feed
            where product_id = v_product_id
            and feed_status = 'NEW';
     
	select count(*) into v_amz_product_id
            from ptn_amazon.az_product_master where product_id = v_product_id;
            
    if v_source_code = v_default_source_code and v_count = 0 and v_amz_product_id > 0 then
        
       insert into PTN_AMAZON.az_price_feed (
                    AZ_PRICE_FEED_ID,
                    PRODUCT_ID,
                    FEED_STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_amazon.az_price_feed_id_sq.nextval,
                    v_product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');

    end if;

END IOTW_AIU_TRG;
.
/