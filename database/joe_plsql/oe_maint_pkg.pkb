create or replace package body JOE.OE_MAINT_PKG as

PROCEDURE PERSIST_CUSTOMER
(
IN_FIRST_NAME                    IN VARCHAR2,
IN_LAST_NAME                     IN VARCHAR2,
IN_ADDRESS                       IN VARCHAR2,
IN_CITY                          IN VARCHAR2,
IN_STATE                         IN VARCHAR2,
IN_ZIP_CODE                      IN VARCHAR2,
IN_COUNTRY                       IN VARCHAR2,
IN_BUYER_RECIP_IND               IN VARCHAR2,
IN_DAYTIME_PHONE                 IN VARCHAR2,
IN_DAYTIME_PHONE_EXT             IN VARCHAR2,
IN_EVENING_PHONE                 IN VARCHAR2,
IN_EVENING_PHONE_EXT             IN VARCHAR2,
IN_EMAIL_ADDRESS                 IN VARCHAR2,
IN_NEWSLETTER_FLAG               IN VARCHAR2,
IN_QMS_RESPONSE_CODE             IN VARCHAR2,
IN_ADDRESS_TYPE                  IN VARCHAR2,
IN_BUSINESS_NAME                 IN VARCHAR2,
IN_BUSINESS_INFO                 IN VARCHAR2,
IN_MEMBERSHIP_ID                 IN VARCHAR2,
IN_MEMBERSHIP_FIRST_NAME         IN VARCHAR2,
IN_MEMBERSHIP_LAST_NAME          IN VARCHAR2,
IN_PROGRAM_NAME                  IN VARCHAR2,
OUT_CUSTOMER_ID                 OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDER_CUSTOMER
    (
        customer_id,
        first_name,
        last_name,
        address,
        city,
        state,
        zip_code,
        country,
        buyer_recipient_ind,
        daytime_phone,
        daytime_phone_ext,
        evening_phone,
        evening_phone_ext,
        email_address,
        newsletter_flag,
        qms_response_code,
        address_type,
        business_name,
        business_info,
        membership_id,
        membership_first_name,
        membership_last_name,
        program_name,
        created_by,
        created_on,
        updated_by,
        updated_on
    )
    VALUES
    (
        customer_id_sq.nextval,
        in_first_name,
        in_last_name,
        in_address,
        in_city,
        in_state,
        in_zip_code,
        in_country,
        in_buyer_recip_ind,
        in_daytime_phone,
        in_daytime_phone_ext,
        in_evening_phone,
        in_evening_phone_ext,
        in_email_address,
        in_newsletter_flag,
        in_qms_response_code,
        in_address_type,
        in_business_name,
        in_business_info,
        in_membership_id,
        in_membership_first_name,
        in_membership_last_name,
        in_program_name,
        'JOE',
        sysdate,
        'JOE',
        sysdate
    ) RETURNING customer_id INTO out_customer_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_CUSTOMER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_CUSTOMER;

PROCEDURE PERSIST_PAYMENTS
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
IN_PAYMENT_METHOD_ID             IN VARCHAR2,
IN_CC_NUMBER                     IN VARCHAR2,
IN_KEY_NAME                      IN VARCHAR2,
IN_CC_EXPIRATION                 IN VARCHAR2,
IN_AUTH_AMT                      IN VARCHAR2,
IN_APPROVAL_CODE                 IN VARCHAR2,
IN_APPROVAL_VERBIAGE             IN VARCHAR2,
IN_APPROVAL_ACTION_CODE          IN VARCHAR2,
IN_ACQ_REFERENCE_NUMBER          IN VARCHAR2,
IN_AVS_RESULT_CODE               IN VARCHAR2,
IN_GC_COUPON_NUMBER              IN VARCHAR2,
IN_GC_AMT                        IN VARCHAR2,
IN_AAFES_TICKET_NUMBER           IN VARCHAR2,
IN_NC_APPROVAL_IDENTITY_ID       IN VARCHAR2,
IN_NC_TYPE_CODE                  IN VARCHAR2,
IN_NC_REASON_CODE                IN VARCHAR2,
IN_NC_ORDER_DETAIL_ID            IN VARCHAR2,
IN_NC_AMT                        IN VARCHAR2,
IN_CSC_RESPONSE_CODE             IN VARCHAR2,
IN_CSC_VALIDATED_FLAG            IN VARCHAR2,
IN_CSC_FAILURE_CNT               IN NUMBER,
IN_CC_AUTH_PROVIDER				 IN VARCHAR2,
IN_PAYMENT_EXT_INFO				 IN LONG,
IN_WALLET_INDICATOR              IN NUMBER,
OUT_PAYMENT_ID                  OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDER_PAYMENTS
    (
        payment_id,
        master_order_number,
        payment_method_id,
        cc_number,
        key_name,
        cc_expiration,
        auth_amt,
        approval_code,
        approval_verbiage,
        approval_action_code,
        acq_reference_number,
        avs_result_code,
        gc_coupon_number,
        gc_amt,
        aafes_ticket_number,
        nc_approval_identity_id,
        nc_type_code,
        nc_reason_code,
        nc_order_detail_id,
        nc_amt,
	csc_response_code,
	csc_validated_flag,
	csc_failure_cnt,
		cc_auth_provider,
        created_by,
        created_on,
        updated_by,
        updated_on,
        wallet_indicator
    )
    VALUES
    (
        payment_id_sq.nextval,
        in_master_order_number,
        in_payment_method_id,
        decode(in_cc_number, null, null, global.encryption.encrypt_it(in_cc_number, in_key_name)),
        in_key_name,
        in_cc_expiration,
        in_auth_amt,
        in_approval_code,
        in_approval_verbiage,
        in_approval_action_code,
        in_acq_reference_number,
        in_avs_result_code,
        in_gc_coupon_number,
        in_gc_amt,
        in_aafes_ticket_number,
        in_nc_approval_identity_id,
        in_nc_type_code,
        in_nc_reason_code,
        in_nc_order_detail_id,
        in_nc_amt,
	in_csc_response_code,
  NVL(in_csc_validated_flag,'N'),
	in_csc_failure_cnt,
		in_cc_auth_provider,
        'JOE',
        sysdate,
        'JOE',
        sysdate,
        in_wallet_indicator
    ) RETURNING payment_id INTO out_payment_id;
    
IF IN_PAYMENT_EXT_INFO IS NOT NULL THEN
	EXECUTE IMMEDIATE GLOBAL.GLOBAL_PKG.INSERT_PAYMENT_EXT_SCRIPT(OUT_PAYMENT_ID,IN_PAYMENT_EXT_INFO,'###','&&&','JOE');
END IF;   

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_PAYMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_PAYMENTS;

PROCEDURE PERSIST_ORDER_DETAILS
(
IN_EXTERNAL_ORDER_NUMBER         IN VARCHAR2,
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
IN_DELIVERY_DATE                 IN DATE,
IN_RECIPIENT_ID                  IN VARCHAR2,
IN_PRODUCT_ID                    IN VARCHAR2,
IN_PRODUCT_SUBCODE_ID            IN VARCHAR2,
IN_FIRST_COLOR_CHOICE            IN VARCHAR2,
IN_SECOND_COLOR_CHOICE           IN VARCHAR2,
IN_IOTW_FLAG                     IN VARCHAR2,
IN_OCCASION_ID                   IN VARCHAR2,
IN_CARD_MESSAGE_SIGNATURE        IN VARCHAR2,
IN_ORDER_CMNTS                   IN VARCHAR2,
IN_FLORIST_CMNTS                 IN VARCHAR2,
IN_FLORIST_ID                    IN VARCHAR2,
IN_SHIP_METHOD_ID                IN VARCHAR2,
IN_DELIVERY_DATE_RANGE_END       IN DATE,
IN_SIZE_IND                      IN VARCHAR2,
IN_PRODUCT_AMT                   IN NUMBER,
IN_ADD_ON_AMT                    IN NUMBER,
IN_SHIPPING_FEE_AMT              IN NUMBER,
IN_SERVICE_FEE_AMT               IN NUMBER,
IN_DISCOUNT_AMT                  IN NUMBER,
IN_TAX_AMT                       IN NUMBER,
IN_ITEM_TOTAL_AMT                IN NUMBER,
IN_MILES_POINTS_QTY              IN NUMBER,
IN_PERSONAL_GREETING_ID          IN VARCHAR2,
IN_BIN_SOURCE_CHANGED_FLAG       IN VARCHAR2,
IN_TAX1_NAME                     IN VARCHAR2,
IN_TAX1_DESCRIPTION              IN VARCHAR2,
IN_TAX1_RATE                     IN NUMBER,
IN_TAX1_AMOUNT                   IN NUMBER,
IN_TAX2_NAME                     IN VARCHAR2,
IN_TAX2_DESCRIPTION              IN VARCHAR2,
IN_TAX2_RATE                     IN NUMBER,
IN_TAX2_AMOUNT                   IN NUMBER,
IN_TAX3_NAME                     IN VARCHAR2,
IN_TAX3_DESCRIPTION              IN VARCHAR2,
IN_TAX3_RATE                     IN NUMBER,
IN_TAX3_AMOUNT                   IN NUMBER,
IN_TAX4_NAME                     IN VARCHAR2,
IN_TAX4_DESCRIPTION              IN VARCHAR2,
IN_TAX4_RATE                     IN NUMBER,
IN_TAX4_AMOUNT                   IN NUMBER,
IN_TAX5_NAME                     IN VARCHAR2,
IN_TAX5_DESCRIPTION              IN VARCHAR2,
IN_TAX5_RATE                     IN NUMBER,
IN_TAX5_AMOUNT                   IN NUMBER,
IN_TOTAL_TAX_DESCRIPTION		 IN VARCHAR2,
IN_TOTAL_TAX_RATE		 		 IN NUMBER,
IN_TAX_SERVICE_PERFORMED_FLAG    IN VARCHAR2,
IN_ORIGINAL_ORDER_HAS_SDU        IN VARCHAR2,
IN_TIME_OF_SERVICE		        IN VARCHAR2,
IN_LATE_CUTOFF_FEE            IN NUMBER,
IN_VENDOR_MON_UPCHARGE        IN NUMBER,
IN_VENDOR_SUN_UPCHARGE        IN NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDER_DETAILS
    (
        external_order_number,
        master_order_number,
        source_code,
        delivery_date,
        recipient_id,
        product_id,
        product_subcode_id,
        first_color_choice,
        second_color_choice,
        iotw_flag,
        occasion_id,
        card_message_signature,
        order_cmnts,
        florist_cmnts,
        florist_id,
        ship_method_id,
        delivery_date_range_end,
        size_ind,
        product_amt,
        add_on_amt,
        shipping_fee_amt,
        service_fee_amt,
        discount_amt,
        tax_amt,
        item_total_amt,
        miles_points_qty,
        created_by,
        created_on,
        updated_by,
        updated_on,
        personal_greeting_id,
        bin_source_changed_flag,
        tax1_name,
        tax1_description,
        tax1_rate,
        tax1_amount,
        tax2_name,
        tax2_description,
        tax2_rate,
        tax2_amount,
        tax3_name,
        tax3_description,
        tax3_rate,
        tax3_amount,
        tax4_name,
        tax4_description,
        tax4_rate,
        tax4_amount,
        tax5_name,
        tax5_description,
        tax5_rate,
        tax5_amount,
        tax_total_description,
        total_tax_rate,
        tax_service_performed,
        original_order_has_sdu,
        TIME_OF_SERVICE,
        late_cutoff_fee,
        vendor_mon_upcharge,
        vendor_sun_upcharge
    )
    VALUES
    (
        in_external_order_number,
        in_master_order_number,
        in_source_code,
        in_delivery_date,
        in_recipient_id,
        in_product_id,
        in_product_subcode_id,
        in_first_color_choice,
        in_second_color_choice,
        in_iotw_flag,
        in_occasion_id,
        in_card_message_signature,
        in_order_cmnts,
        in_florist_cmnts,
        in_florist_id,
        in_ship_method_id,
        in_delivery_date_range_end,
        in_size_ind,
        in_product_amt,
        in_add_on_amt,
        in_shipping_fee_amt,
        in_service_fee_amt,
        in_discount_amt,
        in_tax_amt,
        in_item_total_amt,
        in_miles_points_qty,
        'JOE',
        sysdate,
        'JOE',
        sysdate,
        in_personal_greeting_id,
        nvl(in_bin_source_changed_flag, 'N'),
        in_tax1_name,
        in_tax1_description,
        in_tax1_rate,
        in_tax1_amount,
        in_tax2_name,
        in_tax2_description,
        in_tax2_rate,
        in_tax2_amount,
        in_tax3_name,
        in_tax3_description,
        in_tax3_rate,
        in_tax3_amount,
        in_tax4_name,
        in_tax4_description,
        in_tax4_rate,
        in_tax4_amount,
        in_tax5_name,
        in_tax5_description,
        in_tax5_rate,
        in_tax5_amount,
        in_total_tax_description,
        in_total_tax_rate,
        in_tax_service_performed_flag,
        in_original_order_has_sdu,
        IN_TIME_OF_SERVICE,
        IN_LATE_CUTOFF_FEE,
        IN_VENDOR_MON_UPCHARGE,
        IN_VENDOR_SUN_UPCHARGE
    );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_ORDER_DETAILS;

PROCEDURE PERSIST_ORDERS
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
IN_BUYER_ID                      IN VARCHAR2,
IN_DNIS_ID                       IN VARCHAR2,
IN_SOURCE_CODE                   IN VARCHAR2,
IN_ORIGIN_ID                     IN VARCHAR2,
IN_ORDER_DATE                    IN DATE,
IN_ORDER_TAKEN_BY_IDENTITY_ID    IN VARCHAR2,
IN_ITEM_CNT                      IN NUMBER,
IN_ORDER_AMT                     IN NUMBER,
IN_FRAUD_ID                      IN VARCHAR2,
IN_FRAUD_CMNT                    IN VARCHAR2,
IN_LANGUAGE_ID       			 IN VARCHAR2,
IN_BUYER_HAS_FREE_SHIPPING		 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDERS
    (
        master_order_number,
        buyer_id,
        dnis_id,
        source_code,
        origin_id,
        order_date,
        order_taken_by_identity_id,
        item_cnt,
        order_amt,
        fraud_id,
        fraud_cmnt,
        sent_to_apollo_date,
        sent_to_apollo_retry_cnt,
        created_by,
        created_on,
        updated_by,
        updated_on,
		language_id,
		buyer_has_free_shipping
    )
    VALUES
    (
        in_master_order_number,
        in_buyer_id,
        in_dnis_id,
        in_source_code,
        in_origin_id,
        in_order_date,
        in_order_taken_by_identity_id,
        in_item_cnt,
        in_order_amt,
        in_fraud_id,
        in_fraud_cmnt,
        null,
        0,
        'JOE',
        sysdate,
        'JOE',
        sysdate,
		in_language_id,
		in_buyer_has_free_shipping
    );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_ORDERS;

PROCEDURE PERSIST_ADD_ONS
(
IN_EXTERNAL_ORDER_NUMBER         IN VARCHAR2,
IN_ADD_ON_CODE                   IN VARCHAR2,
IN_ADD_ON_QTY                    IN NUMBER,
IN_FUNERAL_BANNER_TXT            IN VARCHAR2,
OUT_ADD_ON_ID                   OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDER_ADD_ONS
    (
        order_add_on_id,
        external_order_number,
        add_on_code,
        add_on_qty,
        funeral_banner_txt,
        created_by,
        created_on,
        updated_by,
        updated_on
    )
    VALUES
    (
        order_add_on_id_sq.nextval,
        in_external_order_number,
        in_add_on_code,
        in_add_on_qty,
        in_funeral_banner_txt,
        'JOE',
        sysdate,
        'JOE',
        sysdate
    ) RETURNING order_add_on_id INTO out_add_on_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_ADD_ONS;

PROCEDURE PERSIST_BILLING_INFO
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
IN_BILLING_INFO_NAME             IN VARCHAR2,
IN_BILLING_INFO_DATA             IN VARCHAR2,
OUT_BILLING_INFO_ID             OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO JOE.ORDER_BILLING_INFO
    (
        billing_info_id,
        master_order_number,
        billing_info_name,
        billing_info_data,
        created_by,
        created_on,
        updated_by,
        updated_on
    )
    VALUES
    (
        billing_info_id_sq.nextval,
        in_master_order_number,
        in_billing_info_name,
        in_billing_info_data,
        'JOE',
        sysdate,
        'JOE',
        sysdate
    ) RETURNING billing_info_id INTO out_billing_info_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_BILLING_INFO;

PROCEDURE GET_MASTER_SEQUENCE
(
OUT_MASTER_SEQUENCE     OUT VARCHAR2
)
AS

v_master_sequence     varchar2(100);

BEGIN

    SELECT ftd_apps.big_sequence.nextval into v_master_sequence
    FROM dual;

    out_master_sequence := v_master_sequence;

END GET_MASTER_SEQUENCE;

PROCEDURE GET_ORDER_DETAIL_SEQUENCE
(
OUT_ORDER_DETAIL_SEQUENCE     OUT VARCHAR2
)
AS

v_order_detail_sequence     varchar2(100);

BEGIN

    SELECT ftd_apps.oe_order_id.nextval into v_order_detail_sequence
    FROM dual;

    out_order_detail_sequence := v_order_detail_sequence;

END GET_ORDER_DETAIL_SEQUENCE;

PROCEDURE UPDATE_SENT_TO_APOLLO_DATE
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    UPDATE JOE.ORDERS
        SET SENT_TO_APOLLO_DATE = sysdate
        WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_SENT_TO_APOLLO_DATE;

PROCEDURE UPDATE_APOLLO_RETRY_CNT
(
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
OUT_RETRY_COUNT                 OUT JOE.ORDERS.SENT_TO_APOLLO_RETRY_CNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    update joe.orders
        set sent_to_apollo_retry_cnt = sent_to_apollo_retry_cnt + 1
        where master_order_number = in_master_order_number
        RETURNING sent_to_apollo_retry_cnt INTO out_retry_count;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_APOLLO_RETRY_CNT;

PROCEDURE PERSIST_CALL_TIME_HISTORY
(
IN_DNIS_ID                       IN VARCHAR2,
IN_CSR_IDENTITY_ID               IN VARCHAR2,
IN_MASTER_ORDER_NUMBER           IN VARCHAR2,
IN_START_DATETIME                IN DATE,
IN_END_DATETIME                  IN DATE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR exists_cur IS
    select count(1)
    from   joe.call_time_history
    WHERE MASTER_ORDER_NUMBER IS NOT NULL
        AND MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

exists_check  number;

BEGIN

    OPEN exists_cur;
    FETCH exists_cur into exists_check;

    IF exists_check > 0 THEN

        UPDATE JOE.CALL_TIME_HISTORY
            SET END_DATETIME = IN_END_DATETIME,
            UPDATED_ON = SYSDATE
        WHERE MASTER_ORDER_NUMBER IS NOT NULL
            AND MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

    ELSE

        INSERT INTO JOE.CALL_TIME_HISTORY
        (
            call_time_id,
            dnis_id,
            csr_identity_id,
            master_order_number,
            start_datetime,
            end_datetime,
            created_by,
            created_on,
            updated_by,
            updated_on
        )
        VALUES
        (
            call_time_id_sq.nextval,
            in_dnis_id,
            in_csr_identity_id,
            in_master_order_number,
            in_start_datetime,
            in_end_datetime,
            'JOE',
            sysdate,
            'JOE',
            sysdate
        );

    END IF;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PERSIST_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PERSIST_CALL_TIME_HISTORY;


PROCEDURE INSERT_FAVORITE
(
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_DISPLAY_SEQUENCE             IN  NUMBER,
IN_UPDATED_BY                   IN  VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Insert or update a favorite product

Input:  product_id
        display_sequence
        updated_by

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
    select count(1)
    from   product_favorite
    where  display_seq = IN_DISPLAY_SEQUENCE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE product_favorite
    SET
      product_id = IN_PRODUCT_ID,
      updated_by = IN_UPDATED_BY,
      updated_on = sysdate
    WHERE  display_seq = IN_DISPLAY_SEQUENCE;

  ELSE

    INSERT INTO product_favorite (
      product_id,
      display_seq,
      updated_by,
      updated_on,
      created_by,
      created_on
    ) VALUES (
      IN_PRODUCT_ID,
      IN_DISPLAY_SEQUENCE,
      IN_UPDATED_BY,
      sysdate,
      IN_UPDATED_BY,
      sysdate
    );

  END IF;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_FAVORITE;

PROCEDURE DELETE_FAVORITE
(
IN_DISPLAY_SEQUENCE             IN  NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Delte a favorite product

Input:
        display_sequence

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
    select count(1)
    from   product_favorite
    where  display_seq = IN_DISPLAY_SEQUENCE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    DELETE from product_favorite
    WHERE  display_seq = IN_DISPLAY_SEQUENCE;

  END IF;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END DELETE_FAVORITE;

PROCEDURE INSERT_IOTW
(
IN_IOTW_ID                      IN  VARCHAR2,
IN_SOURCE_CODE                  IN  VARCHAR2,
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_UPSELL_MASTER_ID             IN  VARCHAR2,
IN_START_DATE                   IN  DATE,
IN_END_DATE                     IN  DATE,
IN_PRODUCT_PAGE_MESSAGING_TXT   IN  VARCHAR2,
IN_CALENDAR_MESSAGING_TXT       IN  VARCHAR2,
IN_IOTW_SOURCE_CODE             IN  VARCHAR2,
IN_SPECIAL_OFFER_FLAG           IN  VARCHAR2,
IN_WORDING_COLOR_TXT            IN  VARCHAR2,
IN_UPDATED_BY                   IN  VARCHAR2,
OUT_IOTW_ID                     OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Insert or update an IOTW program

Input:  iotw_id
        source_code
        product_id
        upsell_master_id
        start_date
        end_date
        product_page_messaging_txt
        calendar_messaging_txt
        iotw_source_code
        special_offer_flag
        wording_color_txt
        updated_by

Output: iotw_id
        status
        message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN

    IF IN_IOTW_ID IS NULL THEN
        SELECT joe.iotw_sq.nextval into OUT_IOTW_ID
        FROM dual;

        INSERT INTO joe.iotw (
          iotw_id,
          source_code,
          product_id,
          upsell_master_id,
          start_date,
          end_date,
          product_page_messaging_txt,
          calendar_messaging_txt,
          iotw_source_code,
          special_offer_flag,
          wording_color_txt,
          updated_by,
          updated_on,
          created_by,
          created_on
        ) VALUES (
          OUT_IOTW_ID,
          IN_SOURCE_CODE,
          IN_PRODUCT_ID,
          IN_UPSELL_MASTER_ID,
          IN_START_DATE,
          IN_END_DATE,
          IN_PRODUCT_PAGE_MESSAGING_TXT,
          IN_CALENDAR_MESSAGING_TXT,
          IN_IOTW_SOURCE_CODE,
          IN_SPECIAL_OFFER_FLAG,
          IN_WORDING_COLOR_TXT,
          IN_UPDATED_BY,
          sysdate,
          IN_UPDATED_BY,
          sysdate
        );

    ELSE
        OUT_IOTW_ID := IN_IOTW_ID;

        UPDATE joe.iotw i
          SET source_code = IN_SOURCE_CODE,
          product_id = IN_PRODUCT_ID,
          upsell_master_id = IN_UPSELL_MASTER_ID,
          start_date = IN_START_DATE,
          end_date = IN_END_DATE,
          product_page_messaging_txt = IN_PRODUCT_PAGE_MESSAGING_TXT,
          calendar_messaging_txt = IN_CALENDAR_MESSAGING_TXT,
          iotw_source_code = IN_IOTW_SOURCE_CODE,
          special_offer_flag = IN_SPECIAL_OFFER_FLAG,
          wording_color_txt = IN_WORDING_COLOR_TXT,
          updated_by = IN_UPDATED_BY,
          updated_on = SYSDATE
          WHERE i.iotw_id = IN_IOTW_ID;
    END IF;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_IOTW;


PROCEDURE INSERT_IOTW_DEL_DISC_DATE
(
IN_IOTW_ID                      IN  VARCHAR2,
IN_DISCOUNT_DATE                IN  VARCHAR2,
IN_UPDATED_BY                   IN  VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Insert an IOTW program delivery discount date

Input:  iotw_id
        delivery_discount_date
        updated_by

Output: status
        message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN

    INSERT INTO joe.iotw_delivery_discount_dates (
      iotw_id,
      discount_date,
      updated_by,
      updated_on,
      created_by,
      created_on
    ) VALUES (
      IN_IOTW_ID,
      IN_DISCOUNT_DATE,
      IN_UPDATED_BY,
      sysdate,
      IN_UPDATED_BY,
      sysdate
    );

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_IOTW_DEL_DISC_DATE;


PROCEDURE REMOVE_IOTW
(
IN_IOTW_ID                      IN  VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Remove an IOTW program

Input:  iotw_id

Output: status
        message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN

    DELETE FROM joe.iotw_delivery_discount_dates i
    WHERE i.iotw_id = IN_IOTW_ID;

    DELETE FROM joe.iotw i
    WHERE i.iotw_id = IN_IOTW_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END REMOVE_IOTW;

PROCEDURE REMOVE_IOTW_DEL_DISC_DATES
(
IN_IOTW_ID                      IN  VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Remove IOTW delivery discount dates

Input:  iotw_id

Output: status
        message if status is not 'Y'

-----------------------------------------------------------------------------*/

BEGIN

    DELETE FROM joe.iotw_delivery_discount_dates i
    WHERE i.iotw_id = IN_IOTW_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END REMOVE_IOTW_DEL_DISC_DATES;

PROCEDURE INSERT_CROSS_SELL_MASTER
(
IN_PRODUCT_ID                   IN  VARCHAR2,
IN_UPSELL_MASTER_ID             IN  VARCHAR2,
IN_UPDATED_BY                   IN  VARCHAR2,
OUT_CROSS_SELL_MASTER_ID        OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: Insert or update a cross sell master record for a product

Input:  product_id
        upsell_master_id

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

CURSOR cross_sell_seq_cur IS
    select cross_sell_master_sq.nextval
    from dual;

cross_sell_master_id_next number;

BEGIN

    OPEN cross_sell_seq_cur;
    FETCH cross_sell_seq_cur into cross_sell_master_id_next;

    INSERT INTO cross_sell_master (
      cross_sell_master_id,
      product_id,
      upsell_master_id,
      updated_by,
      updated_on,
      created_by,
      created_on
    ) VALUES (
      cross_sell_master_id_next,
      IN_PRODUCT_ID,
      IN_UPSELL_MASTER_ID,
      IN_UPDATED_BY,
      sysdate,
      IN_UPDATED_BY,
      sysdate
    ) RETURNING cross_sell_master_id INTO out_cross_sell_master_id ;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_CROSS_SELL_MASTER;

PROCEDURE INSERT_CROSS_SELL_DETAIL
(
IN_CROSS_SELL_MASTER_ID         IN  NUMBER,
IN_CROSS_SELL_PRODUCT_ID        IN  VARCHAR2,
IN_CROSS_SELL_UPSELL_ID         IN  VARCHAR2,
IN_DISPLAY_ORDER_SEQ            IN  NUMBER,
IN_UPDATED_BY                   IN  VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO cross_sell_detail (
      cross_sell_master_id,
      cross_sell_product_id,
      cross_sell_upsell_id,
      display_order_seq_num,
      updated_by,
      updated_on,
      created_by,
      created_on
    ) VALUES (
      IN_CROSS_SELL_MASTER_ID,
      IN_CROSS_SELL_PRODUCT_ID,
      IN_CROSS_SELL_UPSELL_ID,
      IN_DISPLAY_ORDER_SEQ,
      IN_UPDATED_BY,
      sysdate,
      IN_UPDATED_BY,
      sysdate
    );

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_CROSS_SELL_DETAIL;

PROCEDURE DELETE_CROSS_SELL_MASTER
(
IN_PRODUCT_ID        IN  VARCHAR2,
OUT_STATUS           OUT VARCHAR2,
OUT_MESSAGE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description: delete a cross sell record for a product

Input:  product_cross_sell_id

Output: Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select cross_sell_master_id
    from   cross_sell_master
    where  product_id = IN_PRODUCT_ID
       or  upsell_master_id = IN_PRODUCT_ID;

v_cross_sell_master_id  number;

BEGIN

    OPEN exists_cur;
    FETCH exists_cur into v_cross_sell_master_id;

    IF v_cross_sell_master_id > 0 THEN

        DELETE from cross_sell_detail
        WHERE cross_sell_master_id = v_cross_sell_master_id;

        DELETE from cross_sell_master
        WHERE cross_sell_master_id = v_cross_sell_master_id;

    END IF;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END DELETE_CROSS_SELL_MASTER;

PROCEDURE DELETE_CROSS_SELL_DETAIL
(
IN_CROSS_SELL_MASTER_ID         IN  NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    DELETE FROM CROSS_SELL_DETAIL
    WHERE CROSS_SELL_MASTER_ID = IN_CROSS_SELL_MASTER_ID;

    OUT_STATUS := 'Y';
    OUT_MESSAGE := '';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END DELETE_CROSS_SELL_DETAIL;


PROCEDURE PERSIST_AVS_ADDRESS

(
IN_RECIPIENT_ID                 IN AVS_ADDRESS.RECIPIENT_ID%TYPE,
IN_ADDRESS                      IN AVS_ADDRESS.ADDRESS%TYPE,
IN_CITY                         IN AVS_ADDRESS.CITY%TYPE,
IN_STATE                        IN AVS_ADDRESS.STATE%TYPE,
IN_ZIP                          IN AVS_ADDRESS.ZIP%TYPE,
IN_COUNTRY                      IN AVS_ADDRESS.COUNTRY%TYPE,
IN_LATITUDE                     IN AVS_ADDRESS.LATITUDE%TYPE,
IN_LONGITUDE                    IN AVS_ADDRESS.LONGITUDE%TYPE,
IN_ENTITY_TYPE                  IN AVS_ADDRESS.ENTITY_TYPE%TYPE,
IN_OVERRIDE_FLAG                IN AVS_ADDRESS.OVERRIDE_FLAG%TYPE,
IN_RESULT                       IN AVS_ADDRESS.AVS_RESULT%TYPE,
IN_AVS_PERFORMED                IN AVS_ADDRESS.AVS_PERFORMED%TYPE,
OUT_AVS_ADDRESS_ID              OUT AVS_ADDRESS.AVS_ADDRESS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting AVS address information.

Input:
        recipient_id                    number
        address                         varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        country                         varchar2
        latitude                        varchar2
        longitude                       varchar2
        entity_type                     varchar2
        override_flag                   varchar2
        avs_result                      varchar2
        avs_performed                   varchar2

Output:
        avs_address_id                          number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
CURSOR address_sequence_cur IS
    select avs_address_sq.nextval
    from dual;

BEGIN

OPEN address_sequence_cur;
FETCH address_sequence_cur into OUT_AVS_ADDRESS_ID;

INSERT INTO AVS_ADDRESS
(
        AVS_ADDRESS_ID,
        RECIPIENT_ID,
        ADDRESS,
        CITY,
        STATE,
        ZIP,
        COUNTRY,
        LATITUDE,
        LONGITUDE,
        ENTITY_TYPE,
        OVERRIDE_FLAG,
        AVS_RESULT,
        AVS_PERFORMED,
        CREATED_ON
)
VALUES
(
        OUT_AVS_ADDRESS_ID,
        IN_RECIPIENT_ID,
        IN_ADDRESS,
        IN_CITY,
        IN_STATE,
        IN_ZIP,
        IN_COUNTRY,
        IN_LATITUDE,
        IN_LONGITUDE,
        IN_ENTITY_TYPE,
        IN_OVERRIDE_FLAG,
        IN_RESULT,
        IN_AVS_PERFORMED,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END PERSIST_AVS_ADDRESS;

PROCEDURE PERSIST_AVS_ADDRESS_SCORE
(
IN_AVS_ADDRESS_ID               IN AVS_ADDRESS_SCORE.AVS_ADDRESS_ID%TYPE,
IN_REASON                       IN AVS_ADDRESS_SCORE.SCORE_REASON%TYPE,
IN_SCORE                        IN AVS_ADDRESS_SCORE.SCORE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting QMS address information.

Input:
        avs_address_id                  number
        reason                          varchar2
        score                           varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR score_sequence_cur IS
    select avs_address_score_sq.nextval
    from   dual;

w_score_id    number;

BEGIN

OPEN score_sequence_cur;
FETCH score_sequence_cur into w_score_id;

INSERT INTO AVS_ADDRESS_SCORE
(
        AVS_SCORE_ID,
        AVS_ADDRESS_ID,
        SCORE_REASON,
        SCORE,
        CREATED_ON
)
VALUES
(
        w_score_id,
        IN_AVS_ADDRESS_ID,
        IN_REASON,
        IN_SCORE,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END PERSIST_AVS_ADDRESS_SCORE;

PROCEDURE INSERT_FS_AUTORENEW_ORDER
(
	IN_EXTERNAL_ORDER_NUMBER 	IN CLEAN.ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
	IN_EMAIL_ADDRESS		    IN CLEAN.EMAIL.EMAIL_ADDRESS%TYPE,
	IN_PROD_ID                 	IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
	IN_PROD_PRICE 				IN NUMBER,
	IN_CC_ID                   	IN NUMBER,
	IN_CC_TYPE                 	IN VARCHAR2,
	IN_CC_NUMBER               	IN VARCHAR2,
	IN_CC_EXPIRATION           	IN VARCHAR2,
	IN_KEY_NAME                	IN VARCHAR2,
	IN_APPROVAL_CODE 			IN VARCHAR2,
	IN_APPROVAL_VERBIAGE		IN VARCHAR2,
	IN_APPROVAL_ACTION_CODE		IN VARCHAR2,
	IN_CC_AUTH_PROVIDER			IN VARCHAR2,
	IN_PAYMENT_EXT_INFO		    IN LONG,
	IN_AVS_RESULT_CODE			IN VARCHAR2,

	OUT_STATUS              	OUT VARCHAR2,
	OUT_EXTERNAL_ORDER_NUMBER 	OUT VARCHAR2,
	OUT_MASTER_ORDER_NUMBER		OUT VARCHAR2
)
AS
  v_prod_id                 clean.order_details.product_id%type;
  v_src_cd                  clean.order_details.source_code%type;
  v_ext_ord                 varchar2(100);
  v_mas_ord                 varchar2(100);
  v_prod_price              NUMBER;
  v_active_src_cd_cnt       number;
  v_src_res_cnt             number;
  v_recipient_id            clean.order_details.recipient_id%type;
  v_buyer_id                NUMBER;
  cust_rec                  clean.customer%ROWTYPE;
  pay_cur                   TYPES.REF_CURSOR;
  ext_ord_cur               TYPES.REF_CURSOR;
  mas_ord_cur               TYPES.REF_CURSOR;
  ord_det_rec               clean.order_details%ROWTYPE;
  ord_rec                   clean.orders%ROWTYPE;

  v_pay_id                  NUMBER;
  cc_number                 varchar2(20);
  v_system_message_id       number;
  v_sql_condition           varchar2(200);
  v_stat                    VARCHAR2(1);
  v_mess                    VARCHAR2(256);
  v_user_id                 varchar2(1000);
  v_payment_id              varchar2(1000);  

  INVALID_SKU EXCEPTION;
  INVALID_SOURCECODE EXCEPTION;

  CURSOR ap_cur IS
    select
	  cod.external_order_number, cod.occasion, cod.product_id,
	  concat('SOURCE_CODE_',co.company_id) as source_code, cod.order_guid,
	  co.master_order_number, cod.order_detail_id, co.origin_id, co.language_id,
	  cod.recipient_id, fac.company_id,
	  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(cod.recipient_id,'Day') as recipient_day_phone,
	  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(cod.recipient_id,'Evening') as recipient_evening_phone,
	  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(cod.recipient_id,'Day') as recipient_day_ext,
	  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(cod.recipient_id,'Evening') as recipient_evening_ext,
	  co.customer_id,
	  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(co.customer_id,'Day') as customer_day_phone,
	  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(co.customer_id,'Evening') as customer_evening_phone,
	  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(co.customer_id,'Day') as customer_day_ext,
	  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(co.customer_id,'Evening') as customer_evening_ext,
	  IN_EMAIL_ADDRESS as customer_email,
	  fac.default_dnis_id as dnis_id
  from
	clean.order_details cod,
	clean.orders co, FTD_APPS.company fac
  where
	  cod.external_order_number = in_external_order_number
	  and cod.order_guid = co.order_guid
	  and fac.company_id = co.company_id||'P'
	  and cod.product_id is not null;

  ap_rec ap_cur%ROWTYPE;

  BEGIN

	v_prod_id := IN_PROD_ID;
	v_prod_price := IN_PROD_PRICE;

	BEGIN
    select value into v_user_id from frp.global_parms where context = 'SERVICES_FREESHIPPING' and name = 'UPDATED_BY';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        v_user_id := NULL;
  END;
	if v_user_id is null then
		  v_user_id := 'FTDGoldAutoRenew';
    end if;

  OPEN ap_cur;
  FETCH ap_cur into ap_rec;

  IF ap_cur%found THEN

      -- Check if the auto renew source code is valid otherwise send system message ans quit the program
      select count(*) INTO  v_src_res_cnt
	  from  FTD_APPS.PRODUCT_ATTR_RESTR_SOURCE_EXCL fpa, frp.global_parms fgp
	  where fpa.source_code = FGP.VALUE AND fgp.context='SERVICES_FREESHIPPING'
			AND FGP.NAME=ap_rec.source_code and fpa.product_attr_restr_id = 10;

      select COUNT(*) INTO  v_active_src_cd_cnt
	  from ftd_apps.source_master fsm, frp.global_parms fgp
	  where FSM.SOURCE_CODE=FGP.VALUE AND fgp.context='SERVICES_FREESHIPPING'
			AND FGP.NAME=ap_rec.source_code AND (FSM.END_DATE IS NULL OR FSM.END_DATE >= SYSDATE) ;

      if v_active_src_cd_cnt = 0  or v_src_res_cnt > 0 then
         frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'FS AUTO-RENEWAL FAILED DUE TO INVALID SOURCE CODE, CHECK THE SETUP FOR THE SOURCE CODE IN GLOBAL_PARMS -> '||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number||' SOURCE CODE : '||ap_rec.source_code,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
            RAISE INVALID_SOURCECODE;
      end if;

      select value into v_src_cd from frp.global_parms where name=ap_rec.source_code and context = 'SERVICES_FREESHIPPING';


        --Create recipient
        select customer_id, first_name, last_name, address_1, city, state, zip_code, country, address_type, business_name
        into cust_rec.customer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
        cust_rec.zip_code, cust_rec.country, cust_rec.address_type, cust_rec.business_name
        from clean.customer cc where customer_id = ap_rec.recipient_id;

        v_recipient_id := joe.customer_id_sq.nextval;

        INSERT INTO JOE.ORDER_CUSTOMER
        (
            customer_id, first_name, last_name, address, city, state, zip_code, country, buyer_recipient_ind,
            daytime_phone, daytime_phone_ext, evening_phone, evening_phone_ext, email_address, newsletter_flag,
            qms_response_code, address_type, business_name, business_info, membership_id, membership_first_name,
            membership_last_name, program_name, created_by, created_on, updated_by, updated_on
        )
        VALUES
        (
            v_recipient_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
            cust_rec.zip_code, cust_rec.country, 'R', ap_rec.recipient_day_phone, ap_rec.recipient_day_ext, ap_rec.recipient_evening_phone,
            ap_rec.recipient_evening_ext, null, 'N', null, cust_rec.address_type, cust_rec.business_name, null, null, null, null, null,
            v_user_id, sysdate, v_user_id, sysdate
        );

        --Create buyer
        select customer_id, first_name, last_name, address_1, city, state, zip_code, country, address_type, business_name
        into cust_rec.customer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
        cust_rec.zip_code, cust_rec.country, cust_rec.address_type, cust_rec.business_name
        from clean.customer where customer_id = ap_rec.customer_id;

        v_buyer_id := joe.customer_id_sq.nextval;
        INSERT INTO JOE.ORDER_CUSTOMER
        (
            customer_id, first_name, last_name, address, city, state, zip_code, country, buyer_recipient_ind,
            daytime_phone, daytime_phone_ext, evening_phone, evening_phone_ext, email_address, newsletter_flag,
            qms_response_code, address_type, business_name, business_info, membership_id, membership_first_name,
            membership_last_name, program_name, created_by, created_on, updated_by, updated_on
        )
        VALUES
        (
            v_buyer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
            cust_rec.zip_code, cust_rec.country, 'B', ap_rec.customer_day_phone, ap_rec.customer_day_ext, ap_rec.customer_evening_phone,
            ap_rec.customer_evening_ext, ap_rec.customer_email, 'N', null, cust_rec.address_type, cust_rec.business_name, null, null, null, null, null,
            v_user_id, sysdate, v_user_id, sysdate
        );

        JOE.OE_MAINT_PKG.GET_ORDER_DETAIL_SEQUENCE(v_ext_ord);
        JOE.OE_MAINT_PKG.GET_MASTER_SEQUENCE(v_mas_ord);

        v_ext_ord := 'C'|| v_ext_ord;
        v_mas_ord := 'M'|| v_mas_ord;
        --DBMS_OUTPUT.PUT_LINE('Got new Master_Order_Number : '||v_mas_ord);
        --DBMS_OUTPUT.PUT_LINE('Got new External_Order_Number : '||v_ext_ord);

		OUT_EXTERNAL_ORDER_NUMBER := v_ext_ord;
		OUT_MASTER_ORDER_NUMBER := v_mas_ord;

        INSERT INTO JOE.ORDERS
        (
            master_order_number, buyer_id, dnis_id, source_code, origin_id, order_date,
            order_taken_by_identity_id, item_cnt, order_amt, fraud_id, fraud_cmnt, sent_to_apollo_date,
            sent_to_apollo_retry_cnt, created_by, created_on, updated_by, updated_on, language_id
        )
        VALUES
        (
            v_mas_ord, v_buyer_id, ap_rec.dnis_id, v_src_cd, ap_rec.company_id,
            sysdate, v_user_id, 1, v_prod_price, null, null, null, 0,
            v_user_id, sysdate, v_user_id, sysdate, ap_rec.language_id
        );
        INSERT INTO JOE.ORDER_DETAILS
        (
            external_order_number, master_order_number, source_code, delivery_date, recipient_id,
            product_id, product_subcode_id, first_color_choice, second_color_choice, iotw_flag, occasion_id,
            card_message_signature, order_cmnts, florist_cmnts, florist_id, ship_method_id, delivery_date_range_end,
            size_ind, product_amt, add_on_amt, shipping_fee_amt, service_fee_amt, discount_amt, tax_amt, item_total_amt,
            miles_points_qty, created_by, created_on, updated_by, updated_on, personal_greeting_id, bin_source_changed_flag,
            tax1_name, tax1_description, tax1_rate, tax1_amount, tax2_name, tax2_description, tax2_rate, tax2_amount,
            tax3_name, tax3_description, tax3_rate, tax3_amount, tax4_name, tax4_description, tax4_rate, tax4_amount,
            tax5_name, tax5_description, tax5_rate, tax5_amount
        )
        VALUES
        (
            v_ext_ord, v_mas_ord, v_src_cd, sysdate, v_buyer_id, v_prod_id,
            null, null, null, 'N', ap_rec.occasion, null, null, null, null, null,
            null, null, v_prod_price, 0, 0, 0, 0, 0, v_prod_price,
            null, v_user_id, sysdate, v_user_id, sysdate, null,'N',
            null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null
        );
      v_payment_id := joe.payment_id_sq.nextval;
      INSERT INTO JOE.ORDER_PAYMENTS
      (
          payment_id, master_order_number, payment_method_id, cc_number, key_name,
          cc_expiration, auth_amt, approval_code, approval_verbiage, approval_action_code,
          acq_reference_number, avs_result_code, gc_coupon_number, gc_amt,  aafes_ticket_number,
          nc_approval_identity_id, nc_type_code, nc_reason_code, nc_order_detail_id, nc_amt,
          csc_response_code, csc_validated_flag, csc_failure_cnt, created_by, created_on,
          updated_by, updated_on, cc_auth_provider
      )
      VALUES
      (
          v_payment_id, v_mas_ord, in_cc_type,
          GLOBAL.encryption.ENCRYPT_IT(in_cc_number,in_key_name),in_key_name,  in_cc_expiration, v_prod_price,
          in_approval_code, in_approval_verbiage, in_approval_action_code, null, IN_AVS_RESULT_CODE, null, null, null, null,  null, null, null, null,
          null, 'N', null, v_user_id, sysdate, v_user_id, sysdate, IN_CC_AUTH_PROVIDER
      );
      
IF IN_PAYMENT_EXT_INFO IS NOT NULL THEN
	EXECUTE IMMEDIATE GLOBAL.GLOBAL_PKG.INSERT_PAYMENT_EXT_SCRIPT(v_payment_id,IN_PAYMENT_EXT_INFO,'###','&&&','FTDGoldAutoRenew');
END IF;

	end IF;
  CLOSE ap_cur;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN INVALID_SKU THEN
        BEGIN
            OUT_STATUS := 'N';
		END;
	WHEN INVALID_SOURCECODE	THEN
        BEGIN
            OUT_STATUS := 'N';
		END;
	WHEN OTHERS THEN
        rollback;
		OUT_STATUS := 'N';
        frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256)||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);

END INSERT_FS_AUTORENEW_ORDER;



PROCEDURE GET_CC_AUTH_PARAMS_BY_EXTORDNO
(
IN_EXTERNAL_ORDER_NUMBER 	IN CLEAN.ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_STATUS              	OUT VARCHAR2,
OUT_PROD_ID                 OUT CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
OUT_PROD_PRICE 				OUT NUMBER,
OUT_ORDER_GUID				OUT CLEAN.ORDER_DETAILS.ORDER_GUID%TYPE
)
AS

  v_prod_id                 clean.order_details.product_id%type;
  v_product_id              clean.order_details.product_id%type;
  v_order_guid				clean.order_details.order_guid%type;
  v_default_prod_id         clean.order_details.product_id%type;
  v_prod_price              NUMBER;
  v_default_prod_price      NUMBER;
  v_active_prod_cnt         number;
  v_active_default_prod_cnt number;
  v_system_message_id       number;
  v_stat                    VARCHAR2(1);
  v_mess                    VARCHAR2(256);
  
  INVALID_SKU EXCEPTION;
  
BEGIN

select product_id, order_guid  into v_product_id, v_order_guid
from clean.order_details 
where external_order_number = IN_EXTERNAL_ORDER_NUMBER;

select count(*) into v_active_default_prod_cnt from FRP.global_parms fgp, FTD_APPS.product_master fpm where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and fgp.name = 'DEFAULT' AND FPM.STATUS='A' AND fpm.product_type='SERVICES' 
		  and fpm.product_sub_type='FREESHIP';
		  
if v_active_default_prod_cnt > 0 then
    select NVL(fgp.value,''), NVL(fpm.standard_price,0) into v_default_prod_id, v_default_prod_price 
	from FRP.global_parms fgp, FTD_APPS.product_master fpm 
	where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and fgp.name = 'DEFAULT' 
		  AND fpm.product_type='SERVICES' and fpm.product_sub_type='FREESHIP';
end if;

select count(*) into v_active_prod_cnt 
		from FRP.global_parms fgp, ftd_apps.product_master fpm 
		where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and upper(fgp.name) = upper(v_product_id) 
		  AND FPM.STATUS='A' AND fpm.product_type='SERVICES' 
		  and fpm.product_sub_type='FREESHIP';
  /*
  *Check if the auto-renewal sku or default sku is valid. if both are invalid send system message and quit the program
  */
  if v_active_prod_cnt = 0 then
	  if v_active_default_prod_cnt = 0 then
		--DBMS_OUTPUT.PUT_LINE('Send System Message');
		frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
		'FS AUTO-RENEWAL FAILED DUE TO INVALID SKU, CHECK THE SETUP FOR THE SKU IN GLOBAL_PARMS -> '||'EXTERNAL_ORDR_NUMBER : '
		||in_external_order_number||' SKU : '||v_product_id,
		sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
		RAISE INVALID_SKU;
	  else
		v_prod_id := v_default_prod_id;
		v_prod_price := v_default_prod_price;
	  end if;
  else
	
	select value into v_prod_id from frp.global_parms where context = 'SERVICES_FREESHIPPING' and name = v_product_id;
	select standard_price into v_prod_price from FTD_APPS.product_master where product_id = v_prod_id;
  end if;
  
	OUT_STATUS 		:= 'Y';
	OUT_PROD_PRICE 	:= v_prod_price;
	OUT_ORDER_GUID	:= v_order_guid;
	OUT_PROD_ID		:= v_prod_id;
	
  EXCEPTION WHEN INVALID_SKU THEN
        BEGIN
            OUT_STATUS := 'N';
		END;	
	WHEN OTHERS THEN
        
		OUT_STATUS := 'N';
        frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256)||'EXTERNAL_ORDR_NUMBER : '||in_external_order_number,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
			
END GET_CC_AUTH_PARAMS_BY_EXTORDNO;

PROCEDURE UPDATE_ORDER_DETAILS
(
IN_ORDER_DETAIL_ID          IN VARCHAR2,
IN_PRODUCT_ID               IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE            IN CLEAN.ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_CSR_ID                   IN CLEAN.ORDER_DETAILS.UPDATED_BY%TYPE,
IN_FLORIST_ID               IN CLEAN.ORDER_DETAILS.FLORIST_ID%TYPE,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
) AS

BEGIN

    dbms_application_info.set_client_info('Update order_details' || '::' || in_order_detail_id); 

    update clean.order_details
    set product_id = in_product_id,
    delivery_date = in_delivery_date,
    florist_id = in_florist_id,
    updated_on = sysdate,
    updated_by = in_csr_id
    where order_detail_id = to_number(in_order_detail_id);

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block
    
END UPDATE_ORDER_DETAILS;

PROCEDURE INSERT_PARTNER_PRODUCT_UPDATE
(
IN_ORDER_DETAIL_ID          IN VARCHAR2,
IN_DELIVERY_DATE            IN DATE,
IN_ORIGINAL_PRODUCT_ID      IN VARCHAR2,
IN_NEW_PRODUCT_ID           IN VARCHAR2,
IN_ORIGINAL_MERCH_AMOUNT    IN NUMBER,
IN_NEW_MERCH_AMOUNT         IN NUMBER,
IN_CSR_ID                   IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
) AS

BEGIN

    INSERT INTO JOE.PARTNER_PRODUCT_UPDATE (
        PARTNER_PRODUCT_UPDATE_ID,
        ORDER_DETAIL_ID,
        DELIVERY_DATE,
        ORIGINAL_PRODUCT_ID,
        NEW_PRODUCT_ID,
        ORIGINAL_MERCH_AMOUNT,
        NEW_MERCH_AMOUNT,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY
    ) VALUES (
        JOE.PARTNER_PRODUCT_UPDATE_ID_SQ.NEXTVAL,
        IN_ORDER_DETAIL_ID,
        IN_DELIVERY_DATE,
        IN_ORIGINAL_PRODUCT_ID,
        IN_NEW_PRODUCT_ID,
        IN_ORIGINAL_MERCH_AMOUNT,
        IN_NEW_MERCH_AMOUNT,
        SYSDATE,
        IN_CSR_ID,
        SYSDATE,
        IN_CSR_ID
    );
    
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block
    
END INSERT_PARTNER_PRODUCT_UPDATE;

end OE_MAINT_PKG;
/
