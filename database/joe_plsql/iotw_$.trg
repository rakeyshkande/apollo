CREATE OR REPLACE
TRIGGER joe.iotw_$
AFTER INSERT OR UPDATE OR DELETE ON joe.iotw 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_payload varchar2(2000);
v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_trigger_enabled varchar2(10) := 'N';

BEGIN

   IF INSERTING THEN

      INSERT INTO joe.iotw$ (
         IOTW_ID,                        
         SOURCE_CODE,                    
         PRODUCT_ID,                     
         UPSELL_MASTER_ID,
         START_DATE,                     
         END_DATE,                       
         PRODUCT_PAGE_MESSAGING_TXT,
         CALENDAR_MESSAGING_TXT,
         IOTW_SOURCE_CODE,               
         SPECIAL_OFFER_FLAG,             
         WORDING_COLOR_TXT,              
         CREATED_BY,                     
         CREATED_ON,                     
         UPDATED_BY,                     
         UPDATED_ON,                     
         operation$, timestamp$
      ) VALUES (
         :NEW.IOTW_ID,                        
         :NEW.SOURCE_CODE,                    
         :NEW.PRODUCT_ID,                
         :NEW.UPSELL_MASTER_ID,
         :NEW.START_DATE,                     
         :NEW.END_DATE,                       
         :NEW.PRODUCT_PAGE_MESSAGING_TXT,
         :NEW.CALENDAR_MESSAGING_TXT,
         :NEW.IOTW_SOURCE_CODE,               
         :NEW.SPECIAL_OFFER_FLAG,             
         :NEW.WORDING_COLOR_TXT,              
         :NEW.CREATED_BY,                     
         :NEW.CREATED_ON,                     
         :NEW.UPDATED_BY,                     
         :NEW.UPDATED_ON,                     
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO joe.iotw$ (
         IOTW_ID,                        
         SOURCE_CODE,                    
         PRODUCT_ID,                     
         UPSELL_MASTER_ID,
         START_DATE,                     
         END_DATE,                       
         PRODUCT_PAGE_MESSAGING_TXT,
         CALENDAR_MESSAGING_TXT,
         IOTW_SOURCE_CODE,               
         SPECIAL_OFFER_FLAG,             
         WORDING_COLOR_TXT,              
         CREATED_BY,                     
         CREATED_ON,                     
         UPDATED_BY,                     
         UPDATED_ON,                     
         operation$, timestamp$
      ) VALUES (
         :OLD.IOTW_ID,                        
         :OLD.SOURCE_CODE,                    
         :OLD.PRODUCT_ID,                     
         :OLD.UPSELL_MASTER_ID,
         :OLD.START_DATE,                     
         :OLD.END_DATE,                       
         :OLD.PRODUCT_PAGE_MESSAGING_TXT,
         :OLD.CALENDAR_MESSAGING_TXT,
         :OLD.IOTW_SOURCE_CODE,               
         :OLD.SPECIAL_OFFER_FLAG,             
         :OLD.WORDING_COLOR_TXT,              
         :OLD.CREATED_BY,                     
         :OLD.CREATED_ON,                     
         :OLD.UPDATED_BY,                     
         :OLD.UPDATED_ON,                     
         'UPD_OLD',SYSDATE);

      INSERT INTO joe.iotw$ (
         IOTW_ID,                        
         SOURCE_CODE,                    
         PRODUCT_ID,                     
         UPSELL_MASTER_ID,
         START_DATE,                     
         END_DATE,                       
         PRODUCT_PAGE_MESSAGING_TXT,
         CALENDAR_MESSAGING_TXT,
         IOTW_SOURCE_CODE,               
         SPECIAL_OFFER_FLAG,             
         WORDING_COLOR_TXT,              
         CREATED_BY,                     
         CREATED_ON,                     
         UPDATED_BY,                     
         UPDATED_ON,                     
         operation$, timestamp$
      ) VALUES (
         :NEW.IOTW_ID,                        
         :NEW.SOURCE_CODE,                    
         :NEW.PRODUCT_ID,                     
         :NEW.UPSELL_MASTER_ID,
         :NEW.START_DATE,                     
         :NEW.END_DATE,                       
         :NEW.PRODUCT_PAGE_MESSAGING_TXT,
         :NEW.CALENDAR_MESSAGING_TXT,
         :NEW.IOTW_SOURCE_CODE,               
         :NEW.SPECIAL_OFFER_FLAG,             
         :NEW.WORDING_COLOR_TXT,              
         :NEW.CREATED_BY,                     
         :NEW.CREATED_ON,                     
         :NEW.UPDATED_BY,                     
         :NEW.UPDATED_ON,                     
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO joe.iotw$ (
         IOTW_ID,                        
         SOURCE_CODE,                    
         PRODUCT_ID,                     
         UPSELL_MASTER_ID,
         START_DATE,                     
         END_DATE,                       
         PRODUCT_PAGE_MESSAGING_TXT,
         CALENDAR_MESSAGING_TXT,
         IOTW_SOURCE_CODE,               
         SPECIAL_OFFER_FLAG,             
         WORDING_COLOR_TXT,              
         CREATED_BY,                     
         CREATED_ON,                     
         UPDATED_BY,                     
         UPDATED_ON,                     
         operation$, timestamp$
      ) VALUES (
         :OLD.IOTW_ID,                        
         :OLD.SOURCE_CODE,                    
         :OLD.PRODUCT_ID,                     
         :OLD.UPSELL_MASTER_ID,
         :OLD.START_DATE,                     
         :OLD.END_DATE,                       
         :OLD.PRODUCT_PAGE_MESSAGING_TXT,
         :OLD.CALENDAR_MESSAGING_TXT,
         :OLD.IOTW_SOURCE_CODE,               
         :OLD.SPECIAL_OFFER_FLAG,             
         :OLD.WORDING_COLOR_TXT,              
         :OLD.CREATED_BY,                     
         :OLD.CREATED_ON,                     
         :OLD.UPDATED_BY,                     
         :OLD.UPDATED_ON,                     
         'DEL',SYSDATE);

   END IF;
      
   -- For Project Fresh, post a message with 20 sec delay
   SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
   IF (v_trigger_enabled = 'Y') THEN 
      v_payload := 'FRESH_MSG_PRODUCER|promo|iotw|' || NVL(:OLD.SOURCE_CODE, :NEW.SOURCE_CODE);
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 20, v_stat, v_mess);      
      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message for Fresh IOTW = ' || :OLD.SOURCE_CODE || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;        
   END IF;

END;
/
