CREATE OR REPLACE PROCEDURE sitescope.sequence_count  (p_min_age NUMBER) AS
/* Send out pages for sequence checker problems.
   Sequencechecker is a Java module that watches for missing sequence numbers.
   It uses the frp.order_sequence table, and purges it when it's finished.
   If sequencechecker isn't running, this table will start to grow, which is
   what this program checks for. */
/* To add, delete or modify recipients, look at the table sitescope.pagers. */
/* Parameter list:
      Minimum age in minutes
         messages more recent than this many minutes will not be counted
*/

   c utl_smtp.connection;

   num_orders   NUMBER := 0;

   CURSOR pagers_c IS
      SELECT pager_number
      FROM   pagers
      WHERE  project = 'SCRUB';
   pagers_r pagers_c%ROWTYPE;

   PROCEDURE send_header(name IN VARCHAR2, header IN VARCHAR2) AS
   BEGIN
      utl_smtp.write_data(c, name || ': ' || header || utl_tcp.CRLF);
   END;

BEGIN

   SELECT COUNT(*)
   INTO   num_orders
   FROM   frp.order_sequence
   WHERE  timestamp < SYSDATE-(p_min_age/1440);

   IF num_orders > 0 THEN

      c := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server', 'NAME'));
      utl_smtp.helo(c, 'ftdi.com');
      utl_smtp.mail(c, frp.use_this_from_address);
      FOR pagers_r IN pagers_c LOOP
         utl_smtp.rcpt(c, pagers_r.pager_number);
      END LOOP;
      utl_smtp.open_data(c);
      send_header('Subject', 'Sequencechecker Failure');
      utl_smtp.write_data(c, 'There are '||TO_CHAR(num_orders)||' orders older than '||TO_CHAR(p_min_age)||' minutes in order_sequence');
      utl_smtp.close_data(c);
      utl_smtp.quit(c);

   END IF;

EXCEPTION

   WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

      BEGIN

         utl_smtp.quit(c);

      EXCEPTION

         WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

            NULL; -- When the SMTP server is down or unavailable, we don't have
            -- a connection to the server. The quit call will raise an
            -- exception that we can ignore.

      END;

      raise_application_error(-20000, 'Failed to send mail due to the following error: ' || sqlerrm);

END;
/
