REM This metascript creates a second script, plsql_flat_files.  plsql_flat_files has SPOOL statements and queries
REM to create a set of flat files in a plsql subdirectory that can be used to recreate all the stored procedures
REM in a schema.
SET PAGESIZE 0 LINESIZE 1000 TRIMSPOOL ON FEEDBACK OFF VERIFY OFF
ACCEPT schema_owner PROMPT 'Enter name of schema owner, CASE-SENSITIVE >'
SPOOL plsql_flat_files.sql
SELECT 'SET PAGESIZE 0 LINESIZE 1000 TRIMSPOOL ON FEEDBACK OFF RECSEP OFF'
FROM   dual
/
SELECT 'SPOOL '||lower('&&schema_owner')||'_plsql/'||LOWER(object_name)||'.'||
       DECODE(object_type, 'PACKAGE',     'pkg',
                           'PACKAGE BODY','pkb',
                           'FUNCTION',    'fnc',
                           'PROCEDURE',   'prc',
                           'TRIGGER',     'trg')||CHR(10)||
       'SELECT ''CREATE OR REPLACE '' FROM dual;'||CHR(10)||
       'SELECT text FROM dba_source WHERE owner = ''&&schema_owner'' '||
       'AND type = '''||object_type||''' AND name = '''||object_name||''' ORDER BY line;'||CHR(10)||
       'SELECT ''.''||CHR(10)||''/'' FROM dual;'||CHR(10)||
       'SPOOL OFF'
FROM   dba_objects
WHERE  owner = '&&schema_owner'
AND    object_type IN ('PACKAGE','PACKAGE BODY','FUNCTION','PROCEDURE','TRIGGER')
/
SELECT 'EXIT'
FROM   dual
/
SPOOL OFF
EXIT
