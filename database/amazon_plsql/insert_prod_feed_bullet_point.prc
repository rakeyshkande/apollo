CREATE OR REPLACE
PROCEDURE amazon.insert_prod_feed_bullet_point (
   in_prod_feed_id         IN prod_feed_bullet_point.prod_feed_id%TYPE,
   in_bullet_point         IN prod_feed_bullet_point.bullet_point%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into prod_feed_bullet_point.

Input:
   in_prod_feed_id             number
   in_bullet_point             varchar2

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO prod_feed_bullet_point (
      prod_feed_bullet_point_id,
      prod_feed_id,
      bullet_point)
   VALUES (
      keygen('PROD_FEED_BULLET_POINT'),
      in_prod_feed_id,
      in_bullet_point);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_prod_feed_bullet_point;
.
/
