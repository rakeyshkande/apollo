CREATE OR REPLACE
PROCEDURE amazon.insert_price_feed (
   in_sku_id               IN prod_related_feed.sku_id%TYPE,
   in_company_id           IN prod_related_feed.company_id%TYPE,
   in_product_id           IN prod_related_feed.product_id%TYPE,
   in_prod_feed_id         IN prod_related_feed.prod_feed_id%TYPE,
   in_standard_price       IN prod_price_feed.standard_price%TYPE,
   in_start_date           IN prod_price_feed.start_date%TYPE,
   in_end_date             IN prod_price_feed.end_date%TYPE,
   out_related_id         OUT prod_related_feed.prod_related_feed_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
  This procedure will insert a record into the PROD_RELATED_FEED table and a
  corresponding entry into the PROD_PRICE_FEED table.  The applicable source code
  will be determined and the discounted price will be calculated.

   Because this insert can be part of a larger transaction, rollbacks will be
   done in the app tier.

Input:
   in_sku_id                   number
   in_company_id               varchar2
   in_product_id               varchar2
   in_prod_feed_id             number
   in_standard_price           number
   in_start_date               date
   in_end_date                 date
Output:
   out_related_id              number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

v_current_time date := SYSDATE;
v_sale_price number(12,2) := 0;
v_start_date date;
v_end_date date;
v_source_code az_source.SOURCE_CODE%TYPE;
v_discount FTD_APPS.PRICE_HEADER_DETAILS.DISCOUNT_AMT%TYPE := 0;
v_discount_type FTD_APPS.PRICE_HEADER_DETAILS.DISCOUNT_TYPE%TYPE;
v_status varchar2(1);
v_message varchar2(512);

BEGIN
  AMAZON.API_PKG.GET_SOURCE_CODE(
    IN_COMPANY_ID => in_company_id,
    IN_TIME_RECEIVED => in_start_date,
    OUT_SOURCE => v_source_code,
    OUT_DISCOUNT => v_discount,
    OUT_DISCOUNT_TYPE => v_discount_type,
    OUT_STATUS => v_status,
    OUT_MESSAGE => v_message
  );

  IF v_status='Y' THEN
    out_related_id := keygen('PROD_RELATED_FEED');
    INSERT INTO prod_related_feed (
        prod_related_feed_id,
        prod_related_feed_type,
        prod_feed_id,
        sku_id,
        company_id,
        product_id,
        created_date,
        status_date,
        prod_related_feed_status)
    VALUES (
        out_related_id,
        'PRICE',
        in_prod_feed_id,
        in_sku_id,
        in_company_id,
        in_product_id,
        v_current_time,
        v_current_time,
        'NOT SENT');

    IF v_discount > 0 THEN
      if v_discount_type='P' THEN
        v_discount := (100-v_discount)/100;
        v_sale_price :=  ROUND(v_discount*in_standard_price,2);
        v_start_date := in_start_date;
        v_end_date := in_end_date;
      ELSE
        IF v_discount_type='D' THEN
          v_sale_price := in_standard_price - v_discount;
          v_start_date := in_start_date;
          v_end_date := in_end_date;
        ELSE
          v_start_date := null;
          v_end_date := null;
          v_sale_price := null;
        END IF;
      END IF;
    ELSE
      v_start_date := null;
      v_end_date := null;
      v_sale_price := null;
    END IF;

    INSERT INTO prod_price_feed (
      PROD_RELATED_FEED_ID,
      STANDARD_PRICE,
      START_DATE,
      END_DATE,
      SALE_PRICE)
    VALUES (
      out_related_id,
      in_standard_price,
      v_start_date,
      v_end_date,
      v_sale_price
    );

    OUT_STATUS := 'Y';
  ELSE
    OUT_STATUS := v_status;
    OUT_MESSAGE := v_message;
  END IF;


EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_price_feed;
.
/
