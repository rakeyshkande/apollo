CREATE OR REPLACE
PROCEDURE amazon.insert_prod_feed_item_type     (
   in_prod_feed_id         IN prod_feed_item_type.prod_feed_id%TYPE,
   in_cat_marketing_idx_id IN prod_feed_item_type.cat_marketing_idx_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into prod_feed_item_type.

Input:
   in_prod_feed_id             number
   in_cat_marketing_idx_id     number

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO prod_feed_item_type (
      prod_feed_item_type_id,
      prod_feed_id,
      cat_marketing_idx_id)
   VALUES (
      keygen('PROD_FEED_ITEM_TYPE'),
      in_prod_feed_id,
      in_cat_marketing_idx_id);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_prod_feed_item_type;
.
/
