CREATE OR REPLACE
PROCEDURE amazon.GET_PROD_FEED_BULLET_POINTS (
  IN_PROD_FEED_ID               IN AMAZON.PROD_RELATED_FEED.PROD_RELATED_FEED_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will retrieve all AMAZON.PRODUCT_FEED_BULLET_POINT
    records for the passed prod_feed_id.

Input:
        prod_feed_id                           varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN
OPEN OUT_CUR FOR
	SELECT BULLET_POINT
	FROM AMAZON.PROD_FEED_BULLET_POINT
	WHERE PROD_FEED_ID=IN_PROD_FEED_ID
	ORDER BY PROD_FEED_BULLET_POINT_ID;
END GET_PROD_FEED_BULLET_POINTS;
.
/
