CREATE OR REPLACE
PROCEDURE amazon.get_sku_item_type         (
   in_sku_id     IN sku_item_type.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the item types for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref cursor containing
      sku_item_type_id         number
      cat_marketing_idx_id     number

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         sku_item_type_id,
         cat_marketing_idx_id
      FROM amazon.sku_item_type
      WHERE sku_id = in_sku_id;

END get_sku_item_type;
.
/
