CREATE OR REPLACE
PROCEDURE amazon.GET_REMITTANCE_DETAIL
(
 IN_AZ_SETTLEMENT_DATA_ID      IN SETTLEMENT_DETAIL.AZ_SETTLEMENT_DATA_ID%TYPE,
 OUT_SETTLE_ORDER_ITEM_CURSOR  OUT TYPES.REF_CURSOR,
 OUT_SETTLE_ADJUST_ITEM_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves all the records from tables
        settlement_adjustment_item and settlement_order_item that are related
        to the az_settlement_data_id passed in.

Input:
        az_settlement_data_id NUMBER

Output:
        cursor containing settlement_order_item info
        cursor containing settlement_adjustment_item info

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_settle_order_item_cursor FOR
       SELECT soi.settlement_detail_id,
              soi.item_ordinal,
              soi.amazon_order_item_code,
              aod.confirmation_number,
              soi.merchants_at_order,
              soi.sku,
              soi.quantity,
              soi.item_price,
              soi.tax,
              soi.shipping,
              soi.shipping_tax
         FROM settlement_order_item soi,
              az_order_detail aod,
              settlement_detail sd
        WHERE sd.az_settlement_data_id = in_az_settlement_data_id
          AND soi.settlement_detail_id = sd.settlement_detail_id
          AND aod.az_order_item_number = soi.amazon_order_item_code;

  OPEN out_settle_adjust_item_cursor FOR
       SELECT sai.settlement_detail_id,
              sai.item_ordinal,
              sai.amazon_order_item_code,
              aod.confirmation_number,
              sai.merchant_adjustment_item_id,
              sai.sku,
              sai.item_price,
              sai.tax,
              sai.shipping,
              sai.shipping_tax
         FROM settlement_adjustment_item sai,
              az_order_detail aod,
              settlement_detail sd
        WHERE sd.az_settlement_data_id = in_az_settlement_data_id
          AND sai.settlement_detail_id = sd.settlement_detail_id
          AND aod.az_order_item_number = sai.amazon_order_item_code;


END GET_REMITTANCE_DETAIL;
.
/
