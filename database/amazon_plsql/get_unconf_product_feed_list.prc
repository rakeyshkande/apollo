CREATE OR REPLACE
PROCEDURE amazon.get_unconf_product_feed_list (
   in_company_id           IN sku.company_id%TYPE,
   out_cur                OUT types.ref_cursor) IS

BEGIN

  OPEN out_cur FOR
  select product_id, prod_related_feed_type, prod_related_feed_status, status_date
  from amazon.prod_related_feed
  where company_id = IN_COMPANY_ID AND
        prod_related_feed_status!='CONFIRMED' AND
        prod_related_feed_status!='HOLD' AND
        prod_feed_id IS NULL
  order by status_date desc, product_id;

END get_unconf_product_feed_list;
.
/
