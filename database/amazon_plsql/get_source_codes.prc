CREATE OR REPLACE
PROCEDURE amazon.GET_SOURCE_CODES (
 IN_COMPANY_ID    IN AZ_SOURCE.COMPANY_ID%TYPE,
 OUT_CUR         OUT types.ref_cursor
)
as

/*------------------------------------------------------------------------------
   Returns all the source codes for the passed company id

Input:
   in_company_id               varchar2

Output:
   out_cur                     ref cursor
------------------------------------------------------------------------------*/
begin
 OPEN OUT_CUR FOR
  SELECT s.SOURCE_CODE, f.DESCRIPTION
  FROM AZ_SOURCE s
  LEFT OUTER JOIN FTD_APPS.SOURCE f ON f.source_code = s.source_code
  WHERE s.COMPANY_ID=IN_COMPANY_ID
  ORDER BY s.SOURCE_CODE;
end GET_SOURCE_CODES;
.
/
