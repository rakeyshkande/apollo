CREATE OR REPLACE
PROCEDURE amazon.UPD_PROD_RELATED_TO_CONFIRMED (
  IN_TRANSACTION_ID               IN PROD_RELATED_FEED.AZ_TRANSACTION_ID%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update PROD_RELATED_ID record with
    the passed transaction id and ordinal to the status of "CONFIRMED"
    for all records with a status of 'SENT'.  If a confirmed record is of type
    PROD_INVENTORY_FEED and the action was to delete, then create a product feed
    for that sku.  If a confirmed record is of type PROD_OVERRIDE_FEED and the
    action was not to delete, then create an inventory feed for the sku making
    it available.

Input:
        transaction id                  varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_sku_Id                   amazon.sku.sku_id%TYPE;
v_tax_code                 amazon.sku.tax_code%TYPE;
v_title                    amazon.sku.title%TYPE;
v_company_id               amazon.sku.company_id%TYPE;
v_product_id               amazon.sku.product_id%TYPE;
v_cat_sub_category_id      amazon.sku.cat_sub_category_id%TYPE;
v_max_order_qty            amazon.sku.max_order_qty%TYPE;
v_gift_message             amazon.sku.gift_message%TYPE;
v_brand                    amazon.sku.brand%TYPE;
v_description              amazon.sku.description%TYPE;
v_manufacturer             amazon.sku.manufacturer%TYPE;
v_prod_feed_id             amazon.prod_related_feed.prod_feed_id%TYPE;
v_dummy                    number;

BEGIN
OUT_STATUS := 'Y';
v_dummy := null;

/*Now check to see if this was an inventory delete action*/
DECLARE CURSOR inventory_cur IS
  SELECT
    s.SKU_ID,
    s.TAX_CODE,
    s.TITLE,
    s.company_id,
    s.product_id,
    s.cat_sub_category_id,
    s.max_order_qty,
    s.gift_message,
    s.brand,
    s.description,
    s.manufacturer
  FROM AMAZON.PROD_RELATED_FEED r
  JOIN AMAZON.PROD_INVENTORY_FEED i ON i.prod_related_feed_id = r.prod_related_feed_id
  JOIN AMAZON.SKU s ON s.SKU_ID = r.SKU_ID
  WHERE r.AZ_TRANSACTION_ID=IN_TRANSACTION_ID
    AND (r.PROD_RELATED_FEED_STATUS='CONFIRMED' OR r.PROD_RELATED_FEED_STATUS='SENT')
    AND r.PROD_RELATED_FEED_TYPE='INVENTORY'
    AND i.available='N';

BEGIN
OPEN inventory_cur;
LOOP
  FETCH inventory_cur
    INTO
      v_sku_Id,
      v_tax_code,
      v_title,
      v_company_id,
      v_product_id,
      v_cat_sub_category_id,
      v_max_order_qty,
      v_gift_message,
      v_brand,
      v_description,
      v_manufacturer;

  EXIT WHEN inventory_cur%NOTFOUND;

  INSERT_PRODUCT_FEED(
    v_sku_id,
    v_company_id,
    v_product_id,
    'N',
    v_tax_code,
    null,
    null,
    v_title,
    v_brand,
    v_description,
    v_manufacturer,
    v_max_order_qty,
    v_gift_message,
    v_cat_sub_category_id,
    'SYSTEM',
    v_dummy,
    OUT_STATUS,
    OUT_MESSAGE);
  EXIT WHEN OUT_STATUS='N';
END LOOP;
CLOSE inventory_cur;
END;

/*Now check to see if this was an override on a live product*/
/*If so, we will insert an inventory feed*/
DECLARE CURSOR override_cur IS
  SELECT
    s.SKU_ID,
    s.TAX_CODE,
    s.TITLE,
    s.company_id,
    s.product_id,
    r.prod_feed_id
  FROM AMAZON.PROD_RELATED_FEED r
  JOIN AMAZON.PROD_FEED p ON p.prod_related_feed_id = r.prod_feed_id
  JOIN AMAZON.SKU s ON s.SKU_ID = r.SKU_ID
  WHERE r.AZ_TRANSACTION_ID=IN_TRANSACTION_ID
    AND (r.PROD_RELATED_FEED_STATUS='CONFIRMED' OR r.PROD_RELATED_FEED_STATUS='SENT')
    AND r.PROD_RELATED_FEED_TYPE='OVERRIDE'
    AND p.live='Y';

BEGIN
OPEN override_cur;
LOOP
  FETCH override_cur
    INTO
      v_sku_Id,
      v_tax_code,
      v_title,
      v_company_id,
      v_product_id,
      v_prod_feed_id;

  EXIT WHEN override_cur%NOTFOUND;

  INSERT_INVENTORY_FEED(
    v_sku_id,
    v_company_id,
    v_product_id,
    v_prod_feed_id,
    'Y',
    v_dummy,
    OUT_STATUS,
    OUT_MESSAGE);
  EXIT WHEN OUT_STATUS='N';
END LOOP;
CLOSE override_cur;
END;

IF OUT_STATUS='Y' THEN
BEGIN
UPDATE PROD_RELATED_FEED
  SET
    PROD_RELATED_FEED_STATUS='CONFIRMED',
    STATUS_DATE=SYSDATE,
    RESULT_CODE=null,
    RESULT_DESCRIPTION=null,
    RESULT_MESSAGE_CODE=null
  WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID
    AND PROD_RELATED_FEED_STATUS='SENT';
END;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPD_PROD_RELATED_TO_CONFIRMED;
.
/
