CREATE OR REPLACE
PROCEDURE amazon.insert_prod_feed_search_term  (
   in_prod_feed_id         IN prod_feed_search_term.prod_feed_id%TYPE,
   in_search_term          IN prod_feed_search_term.search_term%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into prod_feed_search_term.
Input:
   in_prod_feed_id             number
   in_search_term              varchar2

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO prod_feed_search_term (
      prod_feed_search_term_id,
      prod_feed_id,
      search_term)
   VALUES (
      keygen('PROD_FEED_SEARCH_TERM'),
      in_prod_feed_id,
      in_search_term);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_prod_feed_search_term;
.
/
