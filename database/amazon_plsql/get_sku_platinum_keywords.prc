CREATE OR REPLACE
PROCEDURE amazon.get_sku_platinum_keywords (
   in_sku_id     IN sku_platinum_keyword.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the "platinum keywords" for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref cursor containing
      sku_platinum_keyword_id  number
      keyword                  varchar2

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         sku_platinum_keyword_id,
         keyword
      FROM amazon.sku_platinum_keyword
      WHERE sku_id = in_sku_id;

END get_sku_platinum_keywords;
.
/
