CREATE OR REPLACE
PROCEDURE amazon.get_sku_eligibility (
   in_company_id          IN sku.company_id%TYPE,
   in_product_id          IN sku.product_id%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*-----------------------------------------------------------------------------
    This procedure will determine if a product from FTD_APPS.PRODUCT_MASTER is
    eligible for inclusion on Amazon.

Input:
        transaction id                  varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_product_id varchar(10);
v_continue boolean;
v_status varchar(1);
v_product_type varchar(10);
v_count number;
v_exception_start_date date;
v_exception_end_date date;
v_exception_code varchar(1);
v_dummy_id varchar(10);

BEGIN
v_continue := true;

-- Is this product id or a novator id
DECLARE CURSOR productId_cur IS
  SELECT PRODUCT_ID, STATUS FROM FTD_APPS.PRODUCT_MASTER
    WHERE PRODUCT_ID = IN_PRODUCT_ID or NOVATOR_ID = IN_PRODUCT_ID;

BEGIN
  OPEN productId_cur;
    FETCH productId_cur INTO v_product_id, v_status;
    IF productId_cur%NOTFOUND THEN
      v_product_id := IN_PRODUCT_ID;
    END IF;
  CLOSE productId_cur;
END;

-- Is this a current SKU on Amazon
IF v_continue THEN
  DECLARE CURSOR sku_cur IS
    SELECT PRODUCT_ID FROM SKU
      WHERE PRODUCT_ID=v_product_id AND COMPANY_ID=IN_COMPANY_ID
        AND LIVE='Y';

  BEGIN
    OPEN sku_cur;
      FETCH sku_cur INTO v_dummy_id;
      IF sku_cur%NOTFOUND THEN
        v_continue := true;
      ELSE
        v_continue := false;
        OUT_STATUS := 'Y';
      END IF;
    CLOSE sku_cur;
  END;
END IF;

--Is the product a gift certificate?
IF v_continue AND instr(v_product_id,'GC')=1 THEN
  v_continue := false;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'sku.eligibility.006';
END IF;

--Is this a dummy sku?
IF v_continue THEN
  SELECT COUNT(*)
    INTO v_count
  FROM FTD_APPS.PRODUCT_SUBCODES
  WHERE PRODUCT_ID=v_product_id;

  IF v_count>0 THEN
    v_continue := false;
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'sku.eligibility.005';
  END IF;
END IF;


--Is the product a master sku
IF v_continue THEN
  DECLARE CURSOR upsell_cur IS
    SELECT UPSELL_MASTER_ID FROM FTD_APPS.UPSELL_MASTER
      WHERE UPSELL_MASTER_ID=v_product_id;

    BEGIN
      OPEN upsell_cur;
        FETCH upsell_cur INTO v_dummy_id;
        IF upsell_cur%FOUND THEN
          v_continue := false;
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'sku.eligibility.005';
        ELSE
          v_continue := true;
        END IF;
      CLOSE upsell_cur;
    END;
END IF;

--Is the product in PDB
IF v_continue THEN
  DECLARE CURSOR pdb_cur IS
    SELECT m.STATUS, m.PRODUCT_TYPE, m.EXCEPTION_CODE, m.EXCEPTION_START_DATE, m.EXCEPTION_END_DATE
    FROM FTD_APPS.PRODUCT_MASTER m
    JOIN FTD_APPS.PRODUCT_COMPANY_XREF x ON x.PRODUCT_ID=m.PRODUCT_ID
    WHERE m.PRODUCT_ID=v_product_id
      AND x.COMPANY_ID=IN_COMPANY_ID;

    BEGIN
      OPEN pdb_cur;
        FETCH pdb_cur INTO v_status, v_product_type, v_exception_code, v_exception_start_date, v_exception_end_date;
        IF pdb_cur%NOTFOUND THEN
          v_continue := false;
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'sku.eligibility.001';
        ELSE
          v_continue := true;
        END IF;
      CLOSE pdb_cur;
    END;
END IF;

--PDB staus set to available
if v_continue AND v_status<>'A' THEN
  v_continue := false;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'sku.eligibility.003';
END IF;

--Past the exception end date
IF v_continue AND v_exception_code='A' AND TRUNC(SYSDATE)>v_exception_end_date THEN
  v_continue := false;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'sku.eligibility.003';
END IF;

--In the unavailable exception zone
IF v_continue AND v_exception_code='U' AND TRUNC(SYSDATE)>=v_exception_start_date AND TRUNC(SYSDATE)<=v_exception_end_date THEN
  v_continue := false;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'sku.eligibility.003';
END IF;

--Same Day Gift?
IF v_continue AND v_product_type='SDG' THEN
  v_continue := false;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'sku.eligibility.002';
END IF;

--Second day shipping available on specialty gifts?
--need to do a select on product_ship_method
IF v_continue AND v_product_type='SPEGFT' THEN
  DECLARE CURSOR gift_cur IS
    SELECT PRODUCT_ID FROM FTD_APPS.PRODUCT_SHIP_METHODS
      WHERE PRODUCT_ID=IN_PRODUCT_ID AND SHIP_METHOD_ID='2F';

    BEGIN
      OPEN gift_cur;
        FETCH gift_cur INTO v_dummy_id;
        IF gift_cur%NOTFOUND THEN
          v_continue := false;
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'sku.eligibility.004';
        ELSE
          v_continue := true;
        END IF;
      CLOSE gift_cur;
    END;
END IF;

--In the Excluded products table
IF v_continue THEN
  DECLARE CURSOR ex_cur IS
    SELECT PRODUCT_ID FROM AMAZON.EXCLUDED_PRODUCT
      WHERE PRODUCT_ID=IN_PRODUCT_ID;

  BEGIN
    OPEN ex_cur;
      FETCH ex_cur INTO v_dummy_id;
      IF ex_cur%FOUND THEN
        v_continue := false;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'sku.eligibility.001';
      ELSE
        v_continue := true;
      END IF;
    CLOSE ex_cur;
  END;
END IF;

IF v_continue=true THEN
  OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END get_sku_eligibility;
.
/
