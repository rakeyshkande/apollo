CREATE OR REPLACE
PROCEDURE amazon.ins_prod_feed_target_audience  (
   in_prod_feed_id         IN prod_feed_target_audience.prod_feed_id%TYPE,
   in_cat_marketing_idx_id IN prod_feed_target_audience.cat_marketing_idx_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into prod_feed_target_audience.

Input:
   in_prod_feed_id             number
   in_cat_marketing_idx_id     number

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO prod_feed_target_audience (
      prod_feed_target_audience_id,
      prod_feed_id,
      cat_marketing_idx_id)
   VALUES (
      keygen('PROD_FEED_TARGET_AUDIENCE'),
      in_prod_feed_id,
      in_cat_marketing_idx_id);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END ins_prod_feed_target_audience;
.
/
