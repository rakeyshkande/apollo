CREATE OR REPLACE
PROCEDURE amazon.GET_PROD_FEED_USED_FORS (
 IN_PROD_FEED_ID               IN AMAZON.PROD_RELATED_FEED.PROD_RELATED_FEED_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
as
 /*-----------------------------------------------------------------------------
    This procedure will return records from the AMAZON. PROD_FEED_USED_FOR
    table for the passed product feed id.

Input:
        prod_feed_id                           varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/
begin

OPEN OUT_CUR FOR
	SELECT
	 c.IDX_NAME,
	 p.CAT_MARKETING_IDX_ID
	FROM PROD_FEED_USED_FOR p
	JOIN AMAZON.CAT_MARKETING_INDEX c ON c.CAT_MARKETING_IDX_ID=p.CAT_MARKETING_IDX_ID

	WHERE p.PROD_FEED_ID=IN_PROD_FEED_ID
	ORDER BY PROD_FEED_USED_FOR_ID;
end;
.
/
