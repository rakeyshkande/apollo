CREATE OR REPLACE
PROCEDURE amazon.INSERT_CAT_SUB_CATEGORY (
 IN_SUB_CATEGORY_NAME IN CAT_SUB_CATEGORY.SUB_CATEGORY_NAME%TYPE,
 IN_CAT_CATETORY_CODE IN CAT_SUB_CATEGORY.CAT_CATEGORY_CODE%TYPE,
 OUT_ID              OUT NUMBER,
 OUT_STATUS          OUT VARCHAR2,
 OUT_MESSAGE         OUT VARCHAR2
)
as

/*------------------------------------------------------------------------------
   Insert a row into CAT_SUB_CATEGORY.

Input:
   IN_SUB_CATEGORY_NAME        varchar2
   IN_CAT_CATETORY_CODE        varchar2

Output:
   out_id                      numeric
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

begin
    /*First check to see if the category exists*/
  DECLARE CURSOR CAT_CUR IS
    SELECT  CAT_SUB_CATEGORY_ID
      FROM    CAT_SUB_CATEGORY
      WHERE   SUB_CATEGORY_NAME = IN_SUB_CATEGORY_NAME
        and   CAT_CATEGORY_CODE = IN_CAT_CATETORY_CODE;

    BEGIN
      OPEN CAT_CUR ;
      FETCH CAT_CUR
            INTO OUT_ID;
      IF CAT_CUR%NOTFOUND THEN
        OUT_ID := keygen('CAT_SUB_CATEGORY');

        INSERT INTO CAT_SUB_CATEGORY (
          CAT_SUB_CATEGORY_ID,
          SUB_CATEGORY_NAME,
          CAT_CATEGORY_CODE)
        VALUES (
          OUT_ID,
          IN_SUB_CATEGORY_NAME,
          IN_CAT_CATETORY_CODE);
      END IF;
      CLOSE CAT_CUR;
    END;

    out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
end INSERT_CAT_SUB_CATEGORY;
.
/
