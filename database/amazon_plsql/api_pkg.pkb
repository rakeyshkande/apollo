CREATE OR REPLACE
PACKAGE BODY amazon.API_PKG AS

FUNCTION KEYGEN
(
  IN_P_TABLE  IN VARCHAR2
) RETURN NUMBER AS
/* This function returns key values for the AMAZON schema.
   It assumes that each table needing keys has a corresponding sequence,
   that has the same name as the table, followed by _SQ.  If this is
   true, it will return the NEXTVAL of that sequence. */
   v_key number;
   v_string varchar(400);
BEGIN
   v_string := 'select '||upper(IN_P_TABLE)||'_sq.nextval from dual';
   execute immediate v_string into v_key;
   return v_key;
END KEYGEN;


PROCEDURE INSERT_ORDER_ADJUSTMENT
(
  IN_CONFIRMATION_NUMBER        IN AMAZON.AZ_ORDER_RELATED.CONFIRMATION_NUMBER%TYPE,
  IN_ADJUSTMENT_REASON          IN AZ_ORDER_ADJUSTMENT.REASON%TYPE,
  IN_PRINCIPAL                  IN AZ_ORDER_ADJUSTMENT.PRINCIPAL%TYPE,
  IN_SHIPPING                   IN AZ_ORDER_ADJUSTMENT.SHIPPING%TYPE,
  IN_TAX                        IN AZ_ORDER_ADJUSTMENT.TAX%TYPE,
  IN_SHIPPING_TAX               IN AZ_ORDER_ADJUSTMENT.SHIPPING_TAX%TYPE,
  IN_SOURCE                     IN AZ_ORDER_ADJUSTMENT.SOURCE%TYPE,
  IN_CSR_USER_NAME              IN AZ_ORDER_ADJUSTMENT.CSR_USER_NAME%TYPE,
  OUT_ORDER_RELATED_ID          OUT AZ_ORDER_RELATED.AZ_ORDER_RELATED_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    The purpose of this procedure is to insert an adjustment into the tables.
    This procedure will insert a record into both the AZ_ORDER_RELATED and
    AZ_ORDER_ADJUSTMENT TABLES.

Input:
        confirmation_number             varchar2
        adjustment_reason               varchar2
        principal                       number
        shipping                        number
        tax                             number
        shipping_tax                    number
        adjustment_reason               varchar2
        csr_user_name                   varchar2

Output:
        order_related_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;
v_az_order_number varchar(32);
last_type_count number;
msg_type varchar(32);

BEGIN
msg_type := 'OrderAdjustment';

--Look up the AZ_ORDER_NUMBER
DECLARE CURSOR detail_cur IS
  SELECT  AZ_ORDER_NUMBER
    FROM    AZ_ORDER_DETAIL
    WHERE   CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur
        INTO v_az_order_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

BEGIN
--Get the next type_count
SELECT MAX(TYPE_COUNT)
INTO last_type_count
FROM AZ_ORDER_RELATED
WHERE CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER AND TYPE_FEED=msg_type;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  BEGIN
    last_type_count := 0;
  END; -- end NO_DATA_FOUND exception
END; -- select type count

IF last_type_count IS NULL THEN
  last_type_count :=0;
END IF;

OUT_ORDER_RELATED_ID := AMAZON.API_PKG.KEYGEN('AZ_ORDER_RELATED');
current_time := SYSDATE;

INSERT INTO AMAZON.AZ_ORDER_RELATED (
  AZ_ORDER_RELATED_ID,
  TYPE_FEED,
  AZ_ORDER_NUMBER,
  CONFIRMATION_NUMBER,
  TYPE_COUNT,
  CREATED_DATE,
  STATUS_DATE,
  STATUS
)
VALUES (
  OUT_ORDER_RELATED_ID,
  msg_type,
  v_az_order_number,
  IN_CONFIRMATION_NUMBER,
  last_type_count+1,
  current_time,
  current_time,
  'NOT SENT'
);

INSERT INTO AMAZON.AZ_ORDER_ADJUSTMENT (
  AZ_ORDER_RELATED_ID,
  REASON,
  PRINCIPAL,
  SHIPPING,
  TAX,
  SHIPPING_TAX,
  SOURCE,
  CSR_USER_NAME
) VALUES (
  OUT_ORDER_RELATED_ID,
  IN_ADJUSTMENT_REASON,
  IN_PRINCIPAL,
  IN_SHIPPING,
  IN_TAX,
  IN_SHIPPING_TAX,
  IN_SOURCE,
  IN_CSR_USER_NAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Order for confirmation number ' || IN_CONFIRMATION_NUMBER || ' not found.';
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_ADJUSTMENT;

PROCEDURE INSERT_ORDER_ACKNOWLEDGEMENT
(
  IN_MASTER_ORDER_NUMBER        IN AZ_ORDER.MASTER_ORDER_NUMBER%TYPE,
  IN_SUCCESSFUL                 IN AZ_ORDER_ACK.ORDER_IN_SYSTEM%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    The purpose of this procedure is to insert an acknowledgement into the tables.
    This procedure will insert a record into both the AZ_ORDER_RELATED and
    AZ_ORDER_ACKNOWLEDGEMENT TABLES.

Input:
        in_master_order_number          varchar2
        in_successful                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;
v_az_order_number varchar(32);
last_type_count number;
msg_type varchar(32);
order_related_id number;

BEGIN
msg_type := 'OrderAcknowledgement';

--Look up the AZ_ORDER_NUMBER
DECLARE CURSOR detail_cur IS
  SELECT AZ_ORDER_NUMBER
    FROM AZ_ORDER
    WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur
        INTO v_az_order_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

BEGIN
--Get the next type_count
SELECT MAX(TYPE_COUNT)
INTO last_type_count
FROM AZ_ORDER_RELATED
WHERE AZ_ORDER_NUMBER = v_az_order_number AND TYPE_FEED=msg_type;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  BEGIN
    last_type_count := 0;
  END; -- end NO_DATA_FOUND exception
END; -- select type count

IF last_type_count IS NULL THEN
  last_type_count :=0;
END IF;

current_time := SYSDATE;

DECLARE CURSOR related_cur IS
  SELECT CONFIRMATION_NUMBER FROM AZ_ORDER_DETAIL
    WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;

BEGIN
  FOR related_rec IN related_cur
    LOOP
      last_type_count := last_type_count+1;
      order_related_id := AMAZON.API_PKG.KEYGEN('AZ_ORDER_RELATED');
      INSERT INTO AMAZON.AZ_ORDER_RELATED (
        AZ_ORDER_RELATED_ID,
        TYPE_FEED,
        AZ_ORDER_NUMBER,
        CONFIRMATION_NUMBER,
        TYPE_COUNT,
        CREATED_DATE,
        STATUS_DATE,
        STATUS
      )
      VALUES (
        order_related_id,
        msg_type,
        v_az_order_number,
        related_rec.CONFIRMATION_NUMBER,
        last_type_count,
        current_time,
        current_time,
        'NOT SENT'
      );

      INSERT INTO AMAZON.AZ_ORDER_ACK (
        AZ_ORDER_RELATED_ID,
        ORDER_IN_SYSTEM
      ) VALUES (
        order_related_id,
        SUBSTR (IN_SUCCESSFUL,1,1)
      );
    END LOOP;
END;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Order for master order number ' || IN_MASTER_ORDER_NUMBER || ' not found.';
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_ACKNOWLEDGEMENT;


PROCEDURE INSERT_ORDER_FULFILLMENT
(
  IN_CONFIRMATION_NUMBER        IN AMAZON.AZ_ORDER_RELATED.CONFIRMATION_NUMBER%TYPE,
  IN_CARRIER                    IN AZ_ORDER_FULFILLMENT.CARRIER%TYPE,
  IN_TRACKING_NUMBER            IN AZ_ORDER_FULFILLMENT.TRACKING_NUMBER%TYPE,
  IN_FULFILLMENT_DATE           IN AZ_ORDER_FULFILLMENT.FULFILLMENT_DATE%TYPE,
  IN_SHIPPING_METHOD            IN AZ_ORDER_FULFILLMENT.SHIPPING_METHOD%TYPE,
  OUT_ORDER_RELATED_ID          OUT NUMBER,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    The purpose of this procedure is to insert an adjustment into the tables.
    This procedure will insert a record into both the AZ_ORDER_RELATED and
    AZ_ORDER_ADJUSTMENT TABLES.

Input:
        confirmation_number             varchar2
        carrier                         varchar2
        tracking_number                 varchar2
        fulfillment_date                date
Output:
        order_related_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;
v_az_order_number varchar(32);
last_type_count number;
msg_type varchar(32);

BEGIN
msg_type := 'OrderFulfillment';

--Look up the AZ_ORDER_NUMBER
DECLARE CURSOR detail_cur IS
  SELECT AZ_ORDER_NUMBER
    FROM AZ_ORDER_DETAIL
    WHERE CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur
        INTO v_az_order_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

BEGIN
--Get the next type_count
SELECT MAX(TYPE_COUNT)
  INTO last_type_count
  FROM AZ_ORDER_RELATED
  WHERE CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER AND TYPE_FEED=msg_type;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  BEGIN
    last_type_count := 0;
  END; -- end NO_DATA_FOUND exception
END; -- select type count

IF last_type_count IS NULL THEN
  last_type_count :=0;
END IF;

OUT_ORDER_RELATED_ID := AMAZON.API_PKG.KEYGEN('AZ_ORDER_RELATED');
current_time := SYSDATE;

INSERT INTO AMAZON.AZ_ORDER_RELATED (
  AZ_ORDER_RELATED_ID,
  TYPE_FEED,
  AZ_ORDER_NUMBER,
  CONFIRMATION_NUMBER,
  TYPE_COUNT,
  CREATED_DATE,
  STATUS_DATE,
  STATUS
)
VALUES (
  OUT_ORDER_RELATED_ID,
  msg_type,
  v_az_order_number,
  IN_CONFIRMATION_NUMBER,
  last_type_count+1,
  current_time,
  current_time,
  'NOT SENT'
);

INSERT INTO AMAZON.AZ_ORDER_FULFILLMENT (
  AZ_ORDER_RELATED_ID,
  CARRIER,
  TRACKING_NUMBER,
  FULFILLMENT_DATE,
  SHIPPING_METHOD
) VALUES (
  OUT_ORDER_RELATED_ID,
  IN_CARRIER,
  IN_TRACKING_NUMBER,
  TRUNC(IN_FULFILLMENT_DATE),
  IN_SHIPPING_METHOD
);

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Order for confirmation number ' || IN_CONFIRMATION_NUMBER || ' not found.';
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_FULFILLMENT;


PROCEDURE INSERT_TRAFFIC
(
  IN_INCOMING                   IN AZ_TRAFFIC_LOG.INCOMING%TYPE,
  IN_XML                        IN AZ_TRAFFIC_LOG.XML%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    The purpose of theis procedure is to inser a new record into the
    AZ_TRAFFIC_LOG table.

Input:
        in_incoming                     char
        in_xml                          clob

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT INTO AZ_TRAFFIC_LOG (
  AZ_TRAFFIC_LOG_ID,
  EVENT_DATE,
  INCOMING,
  XML
) VALUES (
  AMAZON.API_PKG.KEYGEN('AZ_TRAFFIC_LOG'),
  SYSTIMESTAMP,
  IN_INCOMING,
  IN_XML
);

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block
END INSERT_TRAFFIC;

PROCEDURE GET_ORDER_BY_AZ_ORDER_NUM
(
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
  IN_RETURN_XML                 IN BOOLEAN,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will query the AZ_ORDER table for the order record for
    the passed am_order_number.  If return_xml is true, it will return the
    amazon and ftd xml clobs.

Input:
        in_az_order_number                     varchar2
        in_return_xml                          boolean

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN

  IF IN_RETURN_XML = TRUE THEN
    OPEN OUT_CUR FOR
      SELECT  AZ_ORDER_NUMBER,
              MASTER_ORDER_NUMBER,
              COMPANY_ID,
              AZ_XML,
              FTD_XML,
              ORDER_STATUS
      FROM    AZ_ORDER
      WHERE   AZ_ORDER_NUMBER = IN_AZ_ORDER_NUMBER;
  ELSE
    OPEN OUT_CUR FOR
      SELECT  AZ_ORDER_NUMBER,
              MASTER_ORDER_NUMBER,
              NULL,
              NULL
      FROM    AZ_ORDER
      WHERE   AZ_ORDER_NUMBER = IN_AZ_ORDER_NUMBER;
  END IF;
END GET_ORDER_BY_AZ_ORDER_NUM;


PROCEDURE INSERT_ORDER_FEED_MONITOR
(
  IN_RELATED_ID                 IN AZ_ORDER_RELATED.AZ_ORDER_RELATED_ID%TYPE,
  IN_TRANSACTION_ID             IN AZ_ORDER_FEED_MONITOR.AZ_TRANSACTION_ID%TYPE,
  IN_ORDINAL                    IN AZ_ORDER_RELATED.ORDINAL_SENT%TYPE,
  IN_COMPANY_ID                 IN AZ_ORDER_FEED_MONITOR.COMPANY_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will insert a record into the AZ_ORDER_FEED_MONITOR table and
    updates the status in the AZ_ORDER_RELATED table.

Input:
        in_related_id                     number
        in_transaction_id                 number
        in_ordinal                        number
        in_company_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;

BEGIN

BEGIN
--is the record in the database?
SELECT LAST_UPDATE INTO current_time
  FROM AZ_ORDER_FEED_MONITOR
  WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID;

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
  current_time := SYSDATE;
  INSERT INTO AZ_ORDER_FEED_MONITOR (
    AZ_TRANSACTION_ID,
    LAST_STATUS,
    LAST_UPDATE,
    NEXT_UPDATE,
    COMPANY_ID
  ) VALUES (
    IN_TRANSACTION_ID,
    'NOT CONFIRMED',
    current_time,
    current_time,
    IN_COMPANY_ID
  );
END;
END;

BEGIN
UPDATE AZ_ORDER_RELATED
  SET
    AZ_TRANSACTION_ID = IN_TRANSACTION_ID,
    ORDINAL_SENT = IN_ORDINAL,
    STATUS = 'SENT',
    STATUS_DATE = SYSDATE,
    SENT_DATE = SYSDATE
  WHERE
    AZ_ORDER_RELATED_ID = IN_RELATED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END;
END INSERT_ORDER_FEED_MONITOR;


PROCEDURE INSERT_ERROR
(
  IN_CONTEXT                    IN AZ_ERROR.CONTEXT%TYPE,
  IN_ERROR                      IN AZ_ERROR.ERROR%TYPE,
  IN_XML                        IN AZ_ERROR.XML%TYPE,
  IN_EVENT_NAME                 IN AZ_ERROR.EVENT_NAME%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will insert a record into the AZ_ERROR table.

Input:
        in_context                      varchar2
        in_message                      varchar2
        in_error                        varchar2
        in_time_stamp                   datetime
        in_xml                          clob
        in_event_id                     number
        in_event_name                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

current_time date;

BEGIN
current_time := SYSDATE;

INSERT INTO AZ_ERROR
(
  AZ_ERROR_ID,
  CONTEXT,
  ERROR,
  TIME_STAMP,
  XML,
  EVENT_NAME
) VALUES (
  AMAZON.API_PKG.KEYGEN('AZ_ERROR'),
  IN_CONTEXT,
  IN_ERROR,
  current_time,
  IN_XML,
  IN_EVENT_NAME
);

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END INSERT_ERROR;

PROCEDURE GET_UNSENT_ACKNOWLEDGEMENTS
(
  IN_COMPANY                    IN AZ_ORDER.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will query the AZ_ORDER_RELATED, AZ_ORDER_DETAIL,
    AZ_ORDER, and AZ_ORDER_ACK tables for all messages with the type_feed of
    "OrderAcknowledgement", status of "NOT SENT", and for the specified company.
    Records will be returned in AZ_ORDER_NUMBER / AZ_ORDER_ITEM_NUMBER,
    order in system sequence.

Input:
        in_company                             varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  SELECT REL.AZ_ORDER_RELATED_ID,
         OD.AZ_ORDER_NUMBER,
         OD.MASTER_ORDER_NUMBER,
         OD.AZ_ORDER_ITEM_NUMBER,
         OD.CONFIRMATION_NUMBER,
         OA.ORDER_IN_SYSTEM
  FROM AZ_ORDER_ACK OA
  JOIN AZ_ORDER_RELATED REL ON OA.AZ_ORDER_RELATED_ID = REL.AZ_ORDER_RELATED_ID
  JOIN AZ_ORDER_DETAIL OD ON OD.CONFIRMATION_NUMBER = REL.CONFIRMATION_NUMBER
  JOIN AZ_ORDER O ON O.AZ_ORDER_NUMBER = OD.AZ_ORDER_NUMBER
  WHERE O.COMPANY_ID = IN_COMPANY
    AND REL.STATUS='NOT SENT'
    AND REL.TYPE_FEED='OrderAcknowledgement'
  ORDER BY OD.AZ_ORDER_NUMBER, OD.AZ_ORDER_ITEM_NUMBER, OA.ORDER_IN_SYSTEM;
END GET_UNSENT_ACKNOWLEDGEMENTS;

PROCEDURE GET_UNSENT_ADJUSTMENTS
(
  IN_COMPANY                    IN AZ_ORDER.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will query the AZ_ORDER_RELATED, AZ_ORDER_DETAIL,
    AZ_ORDER, and AZ_ORDER_ADJUSTMENT tables for all messages with the type_feed of
    "OrderAdjustment", status of "NOT SENT", and for the specified company.
    Records will be returned in AZ_ORDER_NUMBER / AZ_ORDER_ITEM_NUMBER sequence.

Input:
        in_company                             varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  SELECT REL.AZ_ORDER_RELATED_ID,
         OD.AZ_ORDER_NUMBER,
         OD.MASTER_ORDER_NUMBER,
         OD.AZ_ORDER_ITEM_NUMBER,
         OD.CONFIRMATION_NUMBER,
         REL.TYPE_COUNT,
         OJ.REASON,
         OJ.PRINCIPAL,
         OJ.SHIPPING,
         OJ.TAX,
         OJ.SHIPPING_TAX
  FROM AZ_ORDER_ADJUSTMENT OJ
  JOIN AZ_ORDER_RELATED REL ON OJ.AZ_ORDER_RELATED_ID = REL.AZ_ORDER_RELATED_ID
  JOIN AZ_ORDER_DETAIL OD ON OD.CONFIRMATION_NUMBER = REL.CONFIRMATION_NUMBER
  JOIN AZ_ORDER O ON O.MASTER_ORDER_NUMBER = OD.MASTER_ORDER_NUMBER
  WHERE O.COMPANY_ID = IN_COMPANY
    AND REL.STATUS='NOT SENT'
    AND REL.TYPE_FEED='OrderAdjustment'
  ORDER BY OD.MASTER_ORDER_NUMBER, OD.CONFIRMATION_NUMBER;
END GET_UNSENT_ADJUSTMENTS;

PROCEDURE GET_UNSENT_FULFILLMENTS
(
  IN_COMPANY                    IN AZ_ORDER.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will query the AZ_ORDER_RELATED, AZ_ORDER_DETAIL,
    AZ_ORDER and AZ_ORDER_FULFILLMENT tables for all messages with the type_feed of
    "OrderFulfillment", status of "NOT SENT", and for the specified company.
    Records will be returned in AZ_ORDER_NUMBER / AZ_ORDER_ITEM_NUMBER sequence.

Input:
        in_company                             varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  SELECT REL.AZ_ORDER_RELATED_ID,
         OD.AZ_ORDER_NUMBER,
         OD.MASTER_ORDER_NUMBER,
         OD.AZ_ORDER_ITEM_NUMBER,
         OD.CONFIRMATION_NUMBER,
         REL.TYPE_COUNT,
         OA.CARRIER,
         OA.TRACKING_NUMBER,
         OA.FULFILLMENT_DATE,
         OA.SHIPPING_METHOD
  FROM AZ_ORDER_FULFILLMENT OA
  JOIN AZ_ORDER_RELATED REL ON OA.AZ_ORDER_RELATED_ID = REL.AZ_ORDER_RELATED_ID
  JOIN AZ_ORDER_DETAIL OD ON OD.CONFIRMATION_NUMBER = REL.CONFIRMATION_NUMBER
  JOIN AZ_ORDER O ON O.MASTER_ORDER_NUMBER = OD.MASTER_ORDER_NUMBER
  WHERE O.COMPANY_ID = IN_COMPANY
    AND REL.STATUS='NOT SENT'
    AND REL.TYPE_FEED='OrderFulfillment'
  ORDER BY OD.MASTER_ORDER_NUMBER, OD.CONFIRMATION_NUMBER;
END GET_UNSENT_FULFILLMENTS;


PROCEDURE GET_ASSIGNED_CONF_NUMBER
(
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
  IN_AZ_ORDER_ITEM_NUMBER       IN AZ_ORDER_DETAIL.AZ_ORDER_ITEM_NUMBER%TYPE,
  OUT_CONFIRMATION_NUMBER       OUT AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will lookup or create the confirmation number for
    the passed AZ_ORDER_NUMBER/AZ_ORDER_ITEM_NUMBER pair.  If the
    order cannot be found in the system, then a new confirmation number will be
    created.  The confirmation number is a unique sequence number with a Z
    prepended.  The sequence number will be right justified and zero filled to
    a length of 10 characters for a total confirmation number with a length of
    eleven characters.

Input:
        in_az_order_number              varchar2
        in_az_order_item_number         varchar2

Output:
        confirmation_number             varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT CONFIRMATION_NUMBER
  INTO OUT_CONFIRMATION_NUMBER
  FROM AZ_ORDER_DETAIL
  WHERE AZ_ORDER_NUMBER=IN_AZ_ORDER_NUMBER
    AND AZ_ORDER_ITEM_NUMBER=IN_AZ_ORDER_ITEM_NUMBER;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
  -- no record was found
  OUT_CONFIRMATION_NUMBER := AMAZON.API_PKG.GET_NEXT_CONFIRMATION_NUMBER();
END; -- end NO_DATA_FOUND exception

OUT_STATUS := 'Y';

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_ASSIGNED_CONF_NUMBER;

PROCEDURE UPDATE_ORDER_STATUS
(
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
  IN_ORDER_STATUS               IN AZ_ORDER.ORDER_STATUS%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update the status to the passed status in
    the AZ_ORDER table for the record with the passed AZ_ORDER_NUMBER.

Input:
        in_AZ_order_number          varchar2
        in_order_status                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE AZ_ORDER
  SET ORDER_STATUS=IN_ORDER_STATUS
  WHERE AZ_ORDER_NUMBER=IN_AZ_ORDER_NUMBER;

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block
END UPDATE_ORDER_STATUS;

PROCEDURE INSERT_ORDER
(
  IN_COMPANY                    IN AZ_ORDER.COMPANY_ID%TYPE,
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
  IN_MASTER_ORDER_NUMBER        IN AZ_ORDER.MASTER_ORDER_NUMBER%TYPE,
  IN_AZ_ORDER                   IN AZ_ORDER.AZ_XML%TYPE,
  IN_FTD_ORDER                  IN AZ_ORDER.FTD_XML%TYPE,
  IN_STATUS                     IN AZ_ORDER.ORDER_STATUS%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_ORDER table.

Input:
        IN_COMPANY                      varchar2
        IN_AZ_ORDER_NUMBER              varchar2
        IN_MASTER_ORDER_NUMBER          varchar2
        IN_AZ_ORDER                     clob
        IN_FTD_ORDER                    clob
        IN_STATUS                       varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  BEGIN
    INSERT INTO AZ_ORDER (
      AZ_ORDER_NUMBER,
      MASTER_ORDER_NUMBER,
      COMPANY_ID,
      AZ_XML,
      FTD_XML,
      ORDER_STATUS
    ) VALUES (
      IN_AZ_ORDER_NUMBER,
      IN_MASTER_ORDER_NUMBER,
      IN_COMPANY,
      IN_AZ_ORDER,
      IN_FTD_ORDER,
      IN_STATUS
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
      UPDATE AZ_ORDER
        SET
          MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER,
          COMPANY_ID = IN_COMPANY,
          AZ_XML = IN_AZ_ORDER,
          FTD_XML = IN_FTD_ORDER,
          ORDER_STATUS = IN_STATUS
        WHERE
          AZ_ORDER_NUMBER = IN_AZ_ORDER_NUMBER;

        OUT_STATUS := 'Y';
  END;    -- end duplicate error
END;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER;

PROCEDURE INSERT_ORDER_DETAIL
(
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER_DETAIL.AZ_ORDER_NUMBER%TYPE,
  IN_MASTER_ORDER_NUMBER        IN AZ_ORDER_DETAIL.MASTER_ORDER_NUMBER%TYPE,
  IN_CONFIRMATION_NUMBER        IN AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  IN_AZ_ORDER_ITEM_NUMBER       IN AZ_ORDER_DETAIL.AZ_ORDER_ITEM_NUMBER%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_ORDER_DETAIL
    table.

Input:
        IN_AZ_ORDER_NUMBER              varchar2
        IN_MASTER_ORDER_NUMBER          varchar2
        IN_CONFIRMATION_NUMBER          varchar2
        IN_AZ_ORDER_ITEM_NUMBER         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  BEGIN
    INSERT INTO AZ_ORDER_DETAIL (
      AZ_ORDER_NUMBER,
      MASTER_ORDER_NUMBER,
      CONFIRMATION_NUMBER,
      AZ_ORDER_ITEM_NUMBER
    ) VALUES (
      IN_AZ_ORDER_NUMBER,
      IN_MASTER_ORDER_NUMBER,
      IN_CONFIRMATION_NUMBER,
      IN_AZ_ORDER_ITEM_NUMBER
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    BEGIN
      UPDATE AZ_ORDER_DETAIL
        SET
          AZ_ORDER_NUMBER = IN_AZ_ORDER_NUMBER,
          MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER,
          CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER
        WHERE
          AZ_ORDER_ITEM_NUMBER = IN_AZ_ORDER_ITEM_NUMBER;

        OUT_STATUS := 'Y';
  END;    -- end duplicate error
END;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_DETAIL;

FUNCTION GET_NEXT_CONFIRMATION_NUMBER
RETURN VARCHAR2 AS

/*-----------------------------------------------------------------------------
Description:
        Return the next available confirmation sequence number.
-----------------------------------------------------------------------------*/
confirmation_number varchar(32);

BEGIN

confirmation_number := 'Z' || LPAD ( TO_CHAR(AMAZON.API_PKG.KEYGEN('CONFIRMATION_NUMBER')), 10, '0');

RETURN confirmation_number;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_NEXT_CONFIRMATION_NUMBER;

PROCEDURE GET_SOURCE_CODE
(
  IN_COMPANY_ID                 IN AZ_SALE.COMPANY_ID%TYPE,
  IN_TIME_RECEIVED              IN AZ_SALE.START_DATE%TYPE,
  OUT_SOURCE                    OUT AZ_SALE.DEFAULT_SOURCE_CODE%TYPE,
  OUT_DISCOUNT                  OUT FTD_APPS.PRICE_HEADER_DETAILS.DISCOUNT_AMT%TYPE,
  OUT_DISCOUNT_TYPE             OUT FTD_APPS.PRICE_HEADER_DETAILS.DISCOUNT_TYPE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
)  AS

/*-----------------------------------------------------------------------------
    Returns the source code that was in effect when the order was received.

Input:
        company_id                      varchar2
        time_received                   varchar2

Output:
        source_code                     number
        discount                        number
        discount_type                   varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
default_code varchar(10);
sale_code varchar(10);
start_date date;
end_date date;
v_message varchar(512);

BEGIN

DECLARE CURSOR sale_cur IS
  SELECT  DEFAULT_SOURCE_CODE,
          SALE_SOURCE_CODE,
          TRUNC(START_DATE), --Truncate for price feed calculations
          END_DATE
  FROM AZ_SALE
    WHERE COMPANY_ID = IN_COMPANY_ID;

BEGIN
  OPEN sale_cur;
  FETCH sale_cur
        INTO    default_code,
                sale_code,
                start_date,
                end_date;
  IF sale_cur%NOTFOUND THEN
    v_message := 'Sale record not found.';
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE sale_cur;

  IF (IN_TIME_RECEIVED-start_date>=0) AND (end_date-IN_TIME_RECEIVED>=0) THEN
    OUT_SOURCE := sale_code;
  ELSE
    OUT_SOURCE := default_code;
  END IF;
END;

DECLARE CURSOR disc_cur IS
  SELECT D.DISCOUNT_AMT, D.DISCOUNT_TYPE
  FROM  FTD_APPS.PRICE_HEADER_DETAILS D
  JOIN FTD_APPS.SOURCE S ON S.PRICING_CODE = D.PRICE_HEADER_ID
  WHERE S.SOURCE_CODE = OUT_SOURCE;

  BEGIN
    OPEN disc_cur;
    FETCH disc_cur INTO OUT_DISCOUNT, OUT_DISCOUNT_TYPE;
    IF disc_cur%NOTFOUND THEN
      v_message := 'Discount detail record not found for source code ' || OUT_SOURCE || '.';
      RAISE NO_DATA_FOUND;
    END IF;
    CLOSE disc_cur;
  END;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := v_message;
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END GET_SOURCE_CODE;


PROCEDURE GET_PDB_DATA
(
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  OUT_PRICE                     OUT FTD_APPS.PRODUCT_MASTER.STANDARD_PRICE%TYPE,
  OUT_NOVATOR_ID                OUT FTD_APPS.PRODUCT_MASTER.NOVATOR_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    Gets the current price and novator id in product database for the passed
    product id.

Input:
        novator_id                      varchar2

Output:
        price                           number
        novator id                      varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DECLARE CURSOR price_cur IS
  SELECT  STANDARD_PRICE, NOVATOR_ID
  FROM FTD_APPS.PRODUCT_MASTER
  WHERE PRODUCT_ID=IN_PRODUCT_ID;

BEGIN
  OPEN price_cur;
  FETCH price_cur
        INTO    OUT_PRICE, OUT_NOVATOR_ID;
  IF price_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE price_cur;
END;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Product id ''' || IN_PRODUCT_ID || ''' not found in product database.';
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_PDB_DATA;

PROCEDURE UPDATE_ORDER_TRANS_ITEM_STATUS
(
  IN_TRANSACTION_ID             IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
  IN_ORDINAL                    IN AZ_ORDER_RELATED.ORDINAL_SENT%TYPE,
  IN_STATUS                     IN AZ_ORDER_RELATED.STATUS%TYPE,
  IN_RESULT_CODE                IN AZ_ORDER_RELATED.RESULT_CODE%TYPE,
  IN_RESULT_DESCRIPTION         IN AZ_ORDER_RELATED.RESULT_DESCRIPTION%TYPE,
  IN_RESULT_MESSAGE_CODE        IN AZ_ORDER_RELATED.RESULT_MESSAGE_CODE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update the result fields in the
    AZ_ORDER_RELATED table with the passed values.

Input:
        transaction_id                  varchar2
        ordinal                         number
        status                          varchar2
        result_code                     varchar2
        result_description              varchar2
        result_message_code             number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;

BEGIN

current_time := SYSDATE;

IF IN_ORDINAL=0 THEN
  UPDATE AZ_ORDER_RELATED
      SET
        STATUS = IN_STATUS,
        STATUS_DATE = current_time,
        RESULT_CODE = IN_RESULT_CODE,
        RESULT_DESCRIPTION = IN_RESULT_DESCRIPTION,
        RESULT_MESSAGE_CODE = IN_RESULT_MESSAGE_CODE
      WHERE
        AZ_TRANSACTION_ID=IN_TRANSACTION_ID;
  ELSE
  UPDATE AZ_ORDER_RELATED
      SET
        STATUS = IN_STATUS,
        STATUS_DATE = current_time,
        RESULT_CODE = IN_RESULT_CODE,
        RESULT_DESCRIPTION = IN_RESULT_DESCRIPTION,
        RESULT_MESSAGE_CODE = IN_RESULT_MESSAGE_CODE
      WHERE
        AZ_TRANSACTION_ID=IN_TRANSACTION_ID AND ORDINAL_SENT=IN_ORDINAL;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_ORDER_TRANS_ITEM_STATUS;


PROCEDURE INSERT_PRICE_VARIANCE
(
  IN_CONFIRMATION_NUMBER        IN AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  IN_PRODUCT_ID                 IN AZ_PRICE_VARIANCE.PRODUCT_ID%TYPE,
  IN_FTD_PRICE                  IN AZ_PRICE_VARIANCE.FTD_PRICE%TYPE,
  IN_FTD_SHIPPING               IN AZ_PRICE_VARIANCE.FTD_SHIPPING%TYPE,
  IN_FTD_TAXES                  IN AZ_PRICE_VARIANCE.FTD_TAXES%TYPE,
  IN_FTD_SHIPPING_TAXES         IN AZ_PRICE_VARIANCE.FTD_SHIPPING_TAXES%TYPE,
  IN_FTD_SERVICE_FEE            IN AZ_PRICE_VARIANCE.FTD_SERVICE_FEE%TYPE,
  IN_AMAZON_PRICE               IN AZ_PRICE_VARIANCE.AMAZON_PRICE%TYPE,
  IN_AMAZON_SHIPPING            IN AZ_PRICE_VARIANCE.AMAZON_SHIPPING%TYPE,
  IN_AMAZON_TAXES               IN AZ_PRICE_VARIANCE.AMAZON_TAXES%TYPE,
  IN_AMAZON_SHIPPING_TAXES      IN AZ_PRICE_VARIANCE.AMAZON_SHIPPING_TAXES%TYPE,
  IN_AMAZON_SERVICE_FEE         IN AZ_PRICE_VARIANCE.AMAZON_SERVICE_FEE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_PRICE_VARIANCE
    and AZ_HP TABLES.

Input:
        confirmation number             varchar2
        product_id                      number
        ftd_price                       number
        ftd_shipping                    number
        ftd_taxes                       number
        ftd_shipping_taxes              number
        ftd_service_fee                 number
        amazon_price                    number
        amazon_shipping                 number
        amazon_taxes                    number
        amazon_shipping_taxes           number
        amazon_service_fee              number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

az_order_item_number varchar2(32);

BEGIN


DECLARE CURSOR detail_cur IS
  SELECT  AZ_ORDER_ITEM_NUMBER
    FROM    AZ_ORDER_DETAIL
    WHERE   CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur
        INTO az_order_item_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

INSERT INTO AMAZON.AZ_PRICE_VARIANCE
(
  ORDER_ITEM_NUMBER,
  ORDER_TIMESTAMP,
  PRODUCT_ID,
  FTD_PRICE,
  FTD_SHIPPING,
  FTD_TAXES,
  FTD_SHIPPING_TAXES,
  FTD_SERVICE_FEE,
  AMAZON_PRICE,
  AMAZON_SHIPPING,
  AMAZON_TAXES,
  AMAZON_SHIPPING_TAXES,
  AMAZON_SERVICE_FEE,
  CONFIRMATION_NUMBER
) VALUES (
  az_order_item_number,
  SYSDATE,
  IN_PRODUCT_ID,
  IN_FTD_PRICE,
  IN_FTD_SHIPPING,
  IN_FTD_TAXES,
  IN_FTD_SHIPPING_TAXES,
  IN_FTD_SERVICE_FEE,
  IN_AMAZON_PRICE,
  IN_AMAZON_SHIPPING,
  IN_AMAZON_TAXES,
  IN_AMAZON_SHIPPING_TAXES,
  IN_AMAZON_SERVICE_FEE,
  IN_CONFIRMATION_NUMBER
);

INSERT INTO AMAZON.AZ_HP (
  AZ_HP_ID,
  TABLE_NAME,
  KEY_FIELD_1,
  KEY_TYPE_1,
  KEY_VALUE_1
) VALUES (
  AMAZON.API_PKG.KEYGEN('AZ_HP'),
  'AZ_PRICE_VARIANCE',
  'ORDER_ITEM_NUMBER',
  'VARCHAR2',
  az_order_item_number
);

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Unable to locate order detail for confirmation number ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'Y';
        OUT_MESSAGE := 'Variance already in database for ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END INSERT_PRICE_VARIANCE;

PROCEDURE VIEW_AZ_ERROR
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all amazon errors.

Input:
        none

Output:
        cursor result set containing all fields in order exceptions
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  AZ_ERROR_ID,
                CONTEXT,
                ERROR,
                TIME_STAMP,
                XML,
                EVENT_NAME
        FROM    AZ_ERROR
        ORDER BY AZ_ERROR_ID;

END VIEW_AZ_ERROR;

PROCEDURE DELETE_AZ_ERROR
(
  IN_ERROR_ID                     IN AZ_ERROR.AZ_ERROR_ID%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the amazon error
        messages.

Input:
        error_id                        number
        message                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

DELETE FROM AZ_ERROR
  WHERE AZ_ERROR_ID = IN_ERROR_ID;

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block


END DELETE_AZ_ERROR;

PROCEDURE GET_UNCONFIRMED_ORDER_TRANS
(
  IN_COMPANY                    IN AZ_ORDER_FEED_MONITOR.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will for records in the AZ_ORDER_FEED_MONITOR
    table with a NEXT_UPDATE date/time earlier then the current time and a
    status that is not one of the following:
    'Completed'
    'Rejected'
    'HOLD'
    'CANCELLED'

Input:
        company id                      varchar2

Output:
        out                             ref cursor
-----------------------------------------------------------------------------*/

BEGIN
OPEN OUT_CUR FOR
  SELECT AZ_TRANSACTION_ID
  FROM AZ_ORDER_FEED_MONITOR
  WHERE COMPANY_ID = IN_COMPANY
    AND NEXT_UPDATE < SYSDATE
    AND LAST_STATUS <> 'Rejected'
    AND LAST_STATUS <> 'Complete'
    AND LAST_STATUS <> 'HOLD'
    AND LAST_STATUS <> 'CANCELLED'
  ORDER BY AZ_TRANSACTION_ID;
END GET_UNCONFIRMED_ORDER_TRANS;

PROCEDURE GET_UNCONFIRMED_PRODUCT_TRANS
(
  IN_COMPANY                    IN AZ_PRODUCT_FEED_MONITOR.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will for records in the AZ_PRODUCT_FEED_MONITOR
    table with a NEXT_UPDATE date/time earlier then the current time and a
    status that is not one of the following:
    'Complete'
    'Rejected'
    'HOLD'
    'CANCELLED'

Input:
        company id                      varchar2

Output:
        out                             ref cursor
-----------------------------------------------------------------------------*/

BEGIN
OPEN OUT_CUR FOR
  SELECT AZ_TRANSACTION_ID
  FROM AZ_PRODUCT_FEED_MONITOR
  WHERE COMPANY_ID = IN_COMPANY
    AND NEXT_UPDATE < SYSDATE
    AND LAST_STATUS <> 'Rejected'
    AND LAST_STATUS <> 'Complete'
    AND LAST_STATUS <> 'HOLD'
    AND LAST_STATUS <> 'CANCELLED'
  ORDER BY AZ_TRANSACTION_ID;
END GET_UNCONFIRMED_PRODUCT_TRANS;

PROCEDURE UPDATE_ORDER_TRANS_STATUS
(
  IN_TRANSACTION_ID               IN AZ_ORDER_FEED_MONITOR.AZ_TRANSACTION_ID%TYPE,
  IN_STATUS                       IN AZ_ORDER_FEED_MONITOR.LAST_STATUS%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update AZ_ORDER_FEED_MONITOR records with
    the passed transaction id to the passed status

Input:
        transaction id                  number
        status                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

oldAttempts NUMBER;

BEGIN

SELECT ATTEMPTS INTO oldAttempts
  FROM AZ_ORDER_FEED_MONITOR
  WHERE AZ_TRANSACTION_ID = IN_TRANSACTION_ID;

UPDATE AZ_ORDER_FEED_MONITOR
  SET
    LAST_STATUS=IN_STATUS,
    LAST_UPDATE=SYSDATE,
    ATTEMPTS=oldAttempts+1
  WHERE AZ_TRANSACTION_ID = IN_TRANSACTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Status update failed for transacion id  ' || IN_TRANSACTION_ID || '.  Transaction not found.';
END; -- end NO_DATA_FOUND exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_ORDER_TRANS_STATUS;

PROCEDURE UPDATE_ORDER_REL_TRANS_STATUS
(
  IN_TRANSACTION_ID               IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
  IN_ORDINAL                      IN AZ_ORDER_RELATED.ORDINAL_SENT%TYPE,
  IN_STATUS                       IN AZ_ORDER_RELATED.STATUS%TYPE,
  IN_RESULT_CODE                  IN AZ_ORDER_RELATED.RESULT_CODE%TYPE,
  IN_RESULT_DESCRIPTION           IN AZ_ORDER_RELATED.RESULT_DESCRIPTION%TYPE,
  IN_RESULT_MESSAGE_CODE          IN AZ_ORDER_RELATED.RESULT_MESSAGE_CODE%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update AZ_ORDER_RELATED record with
    the passed transaction id and ordinal to the passed status

Input:
        transaction id                  varchar2
        ordinal                         number
        status                          varchar2
        result code                     varchar2
        result description              varchar2
        result message code             number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE AZ_ORDER_RELATED
  SET
    STATUS=IN_STATUS,
    STATUS_DATE=SYSDATE,
    RESULT_CODE = IN_RESULT_CODE,
    RESULT_DESCRIPTION = IN_RESULT_DESCRIPTION,
    RESULT_MESSAGE_CODE = IN_RESULT_MESSAGE_CODE
  WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID
    AND ORDINAL_SENT=IN_ORDINAL;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_ORDER_REL_TRANS_STATUS;


PROCEDURE UPDATE_ORDER_REL_2_CONFIRMED
(
  IN_TRANSACTION_ID               IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will update AZ_ORDER_RELATED record with
    the passed transaction id and ordinal to the status of "CONFIRMED"
    for all records with a status of 'SENT'

Input:
        transaction id                  varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE AZ_ORDER_RELATED
  SET
    STATUS='CONFIRMED',
    STATUS_DATE=SYSDATE
  WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID
    AND STATUS='SENT';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_ORDER_REL_2_CONFIRMED;


PROCEDURE INSERT_SETTLEMENT_DATA
(
  IN_SETTLEMENT_ID               IN SETTLEMENT_DATA.AZ_SETTLEMENT_DATA_ID%TYPE,
  IN_TOTAL_AMOUNT                IN SETTLEMENT_DATA.TOTAL_AMOUNT%TYPE,
  IN_START_DATE                  IN SETTLEMENT_DATA.START_DATE%TYPE,
  IN_END_DATE                    IN SETTLEMENT_DATA.END_DATE%TYPE,
  IN_DEPOSIT_DATE                IN SETTLEMENT_DATA.DEPOSIT_DATE%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
     This procedure will insert a record into the SETTLEMENT_DATA table and the
     AZ_HP TABLE.
Input:
            settlement id               number
            total amount                number
            start date                  date
            end date                    date
            deposit date                date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_testId number;

BEGIN

OUT_STATUS := 'Y';

--Look up the amazon settlement id
DECLARE CURSOR settlement_cur IS
  SELECT AZ_SETTLEMENT_DATA_ID
  	FROM SETTLEMENT_DATA
  	WHERE AZ_SETTLEMENT_DATA_ID=IN_SETTLEMENT_ID;

BEGIN
  OPEN settlement_cur;
  FETCH settlement_cur
        INTO v_testId;
  IF settlement_cur%FOUND THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'Duplicate settlement';
  END IF;
  CLOSE settlement_cur;
END;

IF OUT_STATUS = 'Y' THEN
	INSERT INTO SETTLEMENT_DATA (
	  AZ_SETTLEMENT_DATA_ID,
	  DOWNLOAD_DATE,
	  TOTAL_AMOUNT,
	  START_DATE,
	  END_DATE,
	  DEPOSIT_DATE
	) VALUES (
	  IN_SETTLEMENT_ID,
	  SYSDATE,
	  IN_TOTAL_AMOUNT,
	  IN_START_DATE,
	  IN_END_DATE,
	  IN_DEPOSIT_DATE
	);

	INSERT INTO AMAZON.AZ_HP (
	  AZ_HP_ID,
	  TABLE_NAME,
	  KEY_FIELD_1,
	  KEY_TYPE_1,
	  KEY_VALUE_1
	) VALUES (
	  AMAZON.API_PKG.KEYGEN('AZ_HP'),
	  'SETTLEMENT_DATA',
	  'AZ_SETTLEMENT_DATA_ID',
	  'NUMBER',
	  IN_SETTLEMENT_ID
	);
END IF;

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SETTLEMENT_DATA;

PROCEDURE INSERT_SETTLEMENT_DETAIL
(
  IN_SETTLEMENT_ID                IN SETTLEMENT_DETAIL.AZ_SETTLEMENT_DATA_ID%TYPE,
  IN_DETAIL_ORDINAL               IN SETTLEMENT_DETAIL.DETAIL_ORDINAL%TYPE,
  IN_DETAIL_TYPE                  IN SETTLEMENT_DETAIL.DETAIL_TYPE%TYPE,
  IN_AZ_ORDER_ID                  IN SETTLEMENT_ORDER.AZ_ORDER_ID%TYPE,
  IN_MASTER_ORDER_NUMBER          IN SETTLEMENT_ORDER.MASTER_ORDER_NUMBER%TYPE,
  IN_POSTED_DATE                  IN SETTLEMENT_DETAIL.POSTED_DATE%TYPE,
  IN_AMOUNT                       IN SETTLEMENT_MISC_EVENT.AMOUNT%TYPE,
  OUT_SETTLEMENT_DETAIL_ID        OUT NUMBER,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
     This procedure will insert a record into the SETTLEMENT_detail table and a
     corresponding record into the SETTLEMENT_ORDER, SETTLEMENT_ADJUSTMENT, or
     SETTLEMENT_MISC_EVENT table depending on the type parameter passed.
Input:
        settlement id                   number
        detail ordinal                  number
        type                            varchar2
        az order id                     number
        master order number             varchar2
        posted date                     date
        amount                          number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_SETTLEMENT_DETAIL_ID := AMAZON.API_PKG.KEYGEN('SETTLEMENT_DETAIL');

INSERT INTO SETTLEMENT_DETAIL (
  SETTLEMENT_DETAIL_ID,
  AZ_SETTLEMENT_DATA_ID,
  DETAIL_TYPE,
  DETAIL_ORDINAL,
  POSTED_DATE
) VALUES (
  OUT_SETTLEMENT_DETAIL_ID,
  IN_SETTLEMENT_ID,
  IN_DETAIL_TYPE,
  IN_DETAIL_ORDINAL,
  IN_POSTED_DATE
);

IF IN_DETAIL_TYPE = 'Order' THEN
  INSERT INTO SETTLEMENT_ORDER (
    SETTLEMENT_DETAIL_ID,
    AZ_ORDER_ID,
    MASTER_ORDER_NUMBER
  ) VALUES (
    OUT_SETTLEMENT_DETAIL_ID,
    IN_AZ_ORDER_ID,
    IN_MASTER_ORDER_NUMBER
  );
ELSE
  IF IN_DETAIL_TYPE = 'Adjustment' THEN
    INSERT INTO SETTLEMENT_ADJUSTMENT (
      SETTLEMENT_DETAIL_ID,
      AZ_ORDER_ID,
      MASTER_ORDER_NUMBER
    ) VALUES (
      OUT_SETTLEMENT_DETAIL_ID,
      IN_AZ_ORDER_ID,
      IN_MASTER_ORDER_NUMBER
    );
  ELSE --MiscEvent (Check constraint validates so no if required
    INSERT INTO SETTLEMENT_MISC_EVENT (
      SETTLEMENT_DETAIL_ID,
      AMOUNT
    ) VALUES (
      OUT_SETTLEMENT_DETAIL_ID,
      IN_AMOUNT
    );
  END IF;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SETTLEMENT_DETAIL;

PROCEDURE INSERT_SETTLEMENT_ORDER_ITEM
(
  IN_SETTLEMENT_DETAIL_ID         IN SETTLEMENT_ORDER_ITEM.SETTLEMENT_DETAIL_ID%TYPE,
  IN_ITEM_ORDINAL                 IN SETTLEMENT_ORDER_ITEM.ITEM_ORDINAL%TYPE,
  IN_AZ_ORDER_ITEM_CODE           IN SETTLEMENT_ORDER_ITEM.AMAZON_ORDER_ITEM_CODE%TYPE,
  IN_CONFIRMATION_NUMBER          IN SETTLEMENT_ORDER_ITEM.CONFIRMATION_NUMBER%TYPE,
  IN_MERCHANTS_AT_ORDER           IN SETTLEMENT_ORDER_ITEM.MERCHANTS_AT_ORDER%TYPE,
  IN_SKU                          IN SETTLEMENT_ORDER_ITEM.SKU%TYPE,
  IN_QUANTITY                     IN SETTLEMENT_ORDER_ITEM.QUANTITY%TYPE,
  IN_ITEM_PRICE                   IN SETTLEMENT_ORDER_ITEM.ITEM_PRICE%TYPE,
  IN_TAX                          IN SETTLEMENT_ORDER_ITEM.TAX%TYPE,
  IN_SHIPPING                     IN SETTLEMENT_ORDER_ITEM.SHIPPING%TYPE,
  IN_SHIPPING_TAX                 IN SETTLEMENT_ORDER_ITEM.SHIPPING_TAX%TYPE,
  IN_COMMISSION                   IN SETTLEMENT_ORDER_ITEM.COMMISSION%TYPE,
  IN_PROMOTION                    IN SETTLEMENT_ORDER_ITEM.PROMOTION%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
     This procedure will insert a record into the SETTLEMENT_ORDER_ITEM table
Input:
        settlement detail id            number
        item ordinal                    number
        az order item code              varchar
        confirmation number             number
        merchants at order              varchar
        sku                             varchar
        quantity                        number
        item price                      number
        tax                             number
        shipping                        number
        shipping tax                    number
        commission                      number
        promotion                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO SETTLEMENT_ORDER_ITEM (
  SETTLEMENT_DETAIL_ID,
  ITEM_ORDINAL,
  AMAZON_ORDER_ITEM_CODE,
  CONFIRMATION_NUMBER,
  MERCHANTS_AT_ORDER,
  SKU,
  QUANTITY,
  ITEM_PRICE,
  TAX,
  SHIPPING,
  SHIPPING_TAX,
  COMMISSION,
  PROMOTION
) VALUES (
  IN_SETTLEMENT_DETAIL_ID,
  IN_ITEM_ORDINAL,
  IN_AZ_ORDER_ITEM_CODE,
  IN_CONFIRMATION_NUMBER,
  IN_MERCHANTS_AT_ORDER,
  IN_SKU,
  IN_QUANTITY,
  IN_ITEM_PRICE,
  IN_TAX,
  IN_SHIPPING,
  IN_SHIPPING_TAX,
  IN_COMMISSION,
  IN_PROMOTION
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SETTLEMENT_ORDER_ITEM;

PROCEDURE INSERT_SETTLEMENT_ADJ_ITEM
(
  IN_SETTLEMENT_DETAIL_ID         IN SETTLEMENT_ADJUSTMENT_ITEM.SETTLEMENT_DETAIL_ID%TYPE,
  IN_ITEM_ORDINAL                 IN SETTLEMENT_ADJUSTMENT_ITEM.ITEM_ORDINAL%TYPE,
  IN_AZ_ORDER_ITEM_CODE           IN SETTLEMENT_ADJUSTMENT_ITEM.AMAZON_ORDER_ITEM_CODE%TYPE,
  IN_CONFIRMATION_NUMBER          IN SETTLEMENT_ADJUSTMENT_ITEM.CONFIRMATION_NUMBER%TYPE,
  IN_MERCHANT_ADJUSTMENT_ITEM_ID  IN SETTLEMENT_ADJUSTMENT_ITEM.MERCHANT_ADJUSTMENT_ITEM_ID%TYPE,
  IN_SKU                          IN SETTLEMENT_ADJUSTMENT_ITEM.SKU%TYPE,
  IN_ITEM_PRICE                   IN SETTLEMENT_ADJUSTMENT_ITEM.ITEM_PRICE%TYPE,
  IN_TAX                          IN SETTLEMENT_ADJUSTMENT_ITEM.TAX%TYPE,
  IN_SHIPPING                     IN SETTLEMENT_ADJUSTMENT_ITEM.SHIPPING%TYPE,
  IN_SHIPPING_TAX                 IN SETTLEMENT_ADJUSTMENT_ITEM.SHIPPING_TAX%TYPE,
  IN_COMMISSION                   IN SETTLEMENT_ADJUSTMENT_ITEM.COMMISSION%TYPE,
  IN_PROMOTION                    IN SETTLEMENT_ADJUSTMENT_ITEM.PROMOTION%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
     This procedure will insert a record into the SETTLEMENT_ADJUSTMENT_ITEM
     table.
Input:
        settlement detail id            number
        item ordinal                    number
        az order item code              varchar
        confirmation number             number
        merchants adjustment item id    varchar
        sku                             varchar
        item price                      number
        tax                             number
        shipping                        number
        shipping tax                    number
        commission                      number
        promotion                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO SETTLEMENT_ADJUSTMENT_ITEM (
  SETTLEMENT_DETAIL_ID,
  ITEM_ORDINAL,
  AMAZON_ORDER_ITEM_CODE,
  CONFIRMATION_NUMBER,
  MERCHANT_ADJUSTMENT_ITEM_ID,
  SKU,
  ITEM_PRICE,
  TAX,
  SHIPPING,
  SHIPPING_TAX,
  COMMISSION,
  PROMOTION
) VALUES (
  IN_SETTLEMENT_DETAIL_ID,
  IN_ITEM_ORDINAL,
  IN_AZ_ORDER_ITEM_CODE,
  IN_CONFIRMATION_NUMBER,
  IN_MERCHANT_ADJUSTMENT_ITEM_ID,
  IN_SKU,
  IN_ITEM_PRICE,
  IN_TAX,
  IN_SHIPPING,
  IN_SHIPPING_TAX,
  IN_COMMISSION,
  IN_PROMOTION
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SETTLEMENT_ADJ_ITEM;

PROCEDURE INSERT_ORDER_ADJUSTMENT_HP
(
  IN_CONFIRMATION_NUMBER        IN AMAZON.AZ_ORDER_RELATED.CONFIRMATION_NUMBER%TYPE,
  IN_ADJUSTMENT_REASON          IN AZ_ORDER_ADJUSTMENT.REASON%TYPE,
  IN_PRINCIPAL                  IN AZ_ORDER_ADJUSTMENT.PRINCIPAL%TYPE,
  IN_SHIPPING                   IN AZ_ORDER_ADJUSTMENT.SHIPPING%TYPE,
  IN_TAX                        IN AZ_ORDER_ADJUSTMENT.TAX%TYPE,
  IN_SHIPPING_TAX               IN AZ_ORDER_ADJUSTMENT.SHIPPING_TAX%TYPE,
  IN_SOURCE                     IN AZ_ORDER_ADJUSTMENT.SOURCE%TYPE,
  IN_CSR_USER_NAME              IN AZ_ORDER_ADJUSTMENT.CSR_USER_NAME%TYPE,
  OUT_ORDER_RELATED_ID          OUT AZ_ORDER_RELATED.AZ_ORDER_RELATED_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure is a tranactionalized wrapper to procedure
    INSERT_ORDER_ADJUSTMENT.

Input:
        confirmation_number             varchar2
        adjustment_reason               varchar2
        principal                       number
        shipping                        number
        tax                             number
        shipping_tax                    number
        adjustment_reason               varchar2
        csr_user_name                   varchar2

Output:
        order_related_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT_ORDER_ADJUSTMENT(
	IN_CONFIRMATION_NUMBER,
	IN_ADJUSTMENT_REASON,
	IN_PRINCIPAL,
	IN_SHIPPING,
	IN_TAX,
	IN_SHIPPING_TAX,
	IN_SOURCE,
	IN_CSR_USER_NAME,
	OUT_ORDER_RELATED_ID,
	OUT_STATUS,
	OUT_MESSAGE);

IF OUT_STATUS = 'Y' THEN
	COMMIT;
ELSE
	ROLLBACK;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END INSERT_ORDER_ADJUSTMENT_HP;

PROCEDURE INSERT_ORDER_FULFILLMENT_HP
(
  IN_CONFIRMATION_NUMBER        IN AMAZON.AZ_ORDER_RELATED.CONFIRMATION_NUMBER%TYPE,
  IN_CARRIER                    IN AZ_ORDER_FULFILLMENT.CARRIER%TYPE,
  IN_TRACKING_NUMBER            IN AZ_ORDER_FULFILLMENT.TRACKING_NUMBER%TYPE,
  IN_FULFILLMENT_DATE           IN AZ_ORDER_FULFILLMENT.FULFILLMENT_DATE%TYPE,
  IN_SHIPPING_METHOD            IN AZ_ORDER_FULFILLMENT.SHIPPING_METHOD%TYPE,
  OUT_ORDER_RELATED_ID          OUT NUMBER,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure is a tranactionalized wrapper to procedure
    INSERT_ORDER_ADJUSTMENT.

Input:
        confirmation_number             varchar2
        carrier                         varchar2
        tracking_number                 varchar2
        fulfillment_date                date
Output:
        order_related_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT_ORDER_FULFILLMENT(
  IN_CONFIRMATION_NUMBER,
  IN_CARRIER,
  IN_TRACKING_NUMBER,
  IN_FULFILLMENT_DATE,
  IN_SHIPPING_METHOD,
  OUT_ORDER_RELATED_ID,
  OUT_STATUS,
  OUT_MESSAGE);

IF OUT_STATUS = 'Y' THEN
	COMMIT;
ELSE
	ROLLBACK;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END INSERT_ORDER_FULFILLMENT_HP;


PROCEDURE INSERT_SKIP_DOCUMENT_ID
(
  IN_DOCUMENT_ID                  IN SKIP_DOWNLOAD_IDS.DOCUMENT_ID%TYPE,
  IN_SOURCE                       IN SKIP_DOWNLOAD_IDS.SOURCE%TYPE,
  IN_REASON                       IN SKIP_DOWNLOAD_IDS.REASON%TYPE,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a record into the SKIP_DOWNLOAD_IDS
    table.  If the record already exists, then no error is returned.

Input:
        document id                     number
        source                          varchar2
        reason                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO SKIP_DOWNLOAD_IDS
(
  DOCUMENT_ID,
  SOURCE,
  REASON
) VALUES (
  IN_DOCUMENT_ID,
  IN_SOURCE,
  IN_REASON
);

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'Y';
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END INSERT_SKIP_DOCUMENT_ID;


PROCEDURE IS_DOCUMENT_DOWNLOAD_BLOCKED
(
  IN_DOCUMENT_ID                  IN SKIP_DOWNLOAD_IDS.DOCUMENT_ID%TYPE,
  IN_SOURCE                       IN SKIP_DOWNLOAD_IDS.SOURCE%TYPE,
  OUT_BLOCKED                     OUT VARCHAR2,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will return a 'Y' in OUT_BLOCKED if the document id
    is in the table for the passed source.

Input:
        document id                     number
        source                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
document_id number;

BEGIN

--Look up the document id
DECLARE CURSOR download_cur IS
  SELECT  DOCUMENT_ID
    FROM  SKIP_DOWNLOAD_IDS
    WHERE DOCUMENT_ID=IN_DOCUMENT_ID AND SOURCE=IN_SOURCE;

BEGIN
  OPEN download_cur;
  FETCH download_cur
        INTO document_id;
  IF download_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE download_cur;
END;

OUT_STATUS := 'Y';
OUT_BLOCKED := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'Y';
        OUT_BLOCKED:= 'N';
        OUT_MESSAGE := 'Download blocked for document id ' || IN_DOCUMENT_ID;
END; -- end NO_DATA_FOUND exception
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END IS_DOCUMENT_DOWNLOAD_BLOCKED;

PROCEDURE GET_AMAZON_PARMS
(
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will return all records from the amazon parms table.

Input:
        in_az_order_number                     varchar2
        in_return_xml                          boolean

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT  AMAZON_PARMS_ID,
        VARIANCE_MAX_ORDERS,
        VARIANCE_TIME_PERIOD,
        VARIANCE_CEILING,
        VARIANCE_ORDER_LIMIT
FROM    AMAZON_PARMS;

END GET_AMAZON_PARMS;

PROCEDURE GET_DEFAULT_SOURCE_CODE
(
  IN_COMPANY_ID                   IN AZ_SALE.COMPANY_ID%TYPE,
  OUT_SOURCE_CODE                 OUT VARCHAR2,
  OUT_STATUS                      OUT VARCHAR2,
  OUT_MESSAGE                     OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    Returns the default source code for Amazon.

Input:
        document id                     number
-----------------------------------------------------------------------------*/

BEGIN

SELECT DEFAULT_SOURCE_CODE INTO OUT_SOURCE_CODE
  FROM AZ_SALE
  WHERE COMPANY_ID = IN_COMPANY_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END GET_DEFAULT_SOURCE_CODE;

PROCEDURE INSERT_ORDER_REPORT
(
  IN_ORDER_REPORT               IN AZ_ORDER_REPORT.REPORT%TYPE,
  IN_COMPANY_ID                 IN AZ_ORDER_REPORT.COMPANY_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_ORDER_REPORT
    table.

Input:
        order report                    varchar2
        company id                      varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  INSERT INTO AZ_ORDER_REPORT (
    AZ_ORDER_REPORT_ID,
    REPORT,
    COMPANY_ID
  ) VALUES (
    AMAZON.API_PKG.KEYGEN('AZ_ORDER_REPORT'),
    IN_ORDER_REPORT,
    IN_COMPANY_ID
  );

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_REPORT;

PROCEDURE GET_ORDER_REPORTS
(
  IN_COMPANY_ID                 IN AZ_ORDER_REPORT.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will query the AZ_ORDER_REPORT and return all records for
    the passed company id

Input:
        in_company_id                          varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CUR FOR
    SELECT AZ_ORDER_REPORT_ID,
            REPORT
    FROM    AZ_ORDER_REPORT
--    WHERE   COMPANY_ID = IN_COMPANY_ID AND PROCESSED='N' FOR UPDATE;
    WHERE COMPANY_ID = IN_COMPANY_ID AND PROCESSED='N';
END GET_ORDER_REPORTS;

PROCEDURE DELETE_ORDER_REPORT
(
  IN_ORDER_REPORT_ID            IN AZ_ORDER_REPORT.AZ_ORDER_REPORT_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will delete the record in the AZ_ORDER_REPORT_TABLE for
    the passed id.

Input:
        in_company_id                          varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  DELETE
    FROM    AZ_ORDER_REPORT
    WHERE   AZ_ORDER_REPORT_ID = IN_ORDER_REPORT_ID;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END DELETE_ORDER_REPORT;

PROCEDURE UPDATE_ORDER_REPORT_PROCESSING
(
  IN_ORDER_REPORT_ID            IN AZ_ORDER_REPORT.AZ_ORDER_REPORT_ID%TYPE,
  IN_PROCESSED                  IN AZ_ORDER_REPORT.PROCESSED%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will change the status on the record in the
    AZ_ORDER_REPORT_TABLE for the passed id to the passed status.

Input:
        in_company_id                   varchar2
        in_processed                    varchar2 (Y or N)
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  UPDATE AZ_ORDER_REPORT
    SET PROCESSED = IN_PROCESSED
    WHERE AZ_ORDER_REPORT_ID = IN_ORDER_REPORT_ID;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_ORDER_REPORT_PROCESSING;

PROCEDURE INSERT_SETTLEMENT_REPORT
(
  IN_SETTLEMENT_REPORT          IN AZ_SETTLEMENT_REPORT.REPORT%TYPE,
  IN_COMPANY_ID                 IN AZ_SETTLEMENT_REPORT.COMPANY_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_SETTLEMENT_REPORT
    table.

Input:
        order report                    varchar2
        company id                      varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  INSERT INTO AZ_SETTLEMENT_REPORT (
    AZ_SETTLEMENT_REPORT_ID,
    REPORT,
    COMPANY_ID
  ) VALUES (
    AMAZON.API_PKG.KEYGEN('AZ_SETTLEMENT_REPORT'),
    IN_SETTLEMENT_REPORT,
    IN_COMPANY_ID
  );

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_SETTLEMENT_REPORT;

PROCEDURE GET_SETTLEMENT_REPORTS
(
  IN_COMPANY_ID                 IN AZ_SETTLEMENT_REPORT.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will query the AZ_SETTLEMENT_REPORT and return all records for
    the passed company id

Input:
        in_company_id                          varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CUR FOR
    SELECT AZ_SETTLEMENT_REPORT_ID,
            REPORT
    FROM    AZ_SETTLEMENT_REPORT
--    WHERE   COMPANY_ID = IN_COMPANY_ID AND PROCESSED='N' FOR UPDATE;
    WHERE COMPANY_ID = IN_COMPANY_ID AND PROCESSED='N';
END GET_SETTLEMENT_REPORTS;

PROCEDURE UPDATE_SETTLEMENT_REPORT
(
  IN_SETTLEMENT_REPORT_ID            IN AZ_SETTLEMENT_REPORT.AZ_SETTLEMENT_REPORT_ID%TYPE,
  IN_PROCESSED                  IN AZ_SETTLEMENT_REPORT.PROCESSED%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS


/*-----------------------------------------------------------------------------
    This procedure will change the status on the record in the
    AZ_SETTLEMENT_REPORT_TABLE for the passed id to the passed status.

Input:
        in_company_id                   varchar2
        in_processed                    varchar2 (Y or N)
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  UPDATE AZ_SETTLEMENT_REPORT
    SET PROCESSED = IN_PROCESSED
    WHERE AZ_SETTLEMENT_REPORT_ID = IN_SETTLEMENT_REPORT_ID;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_SETTLEMENT_REPORT;


PROCEDURE INSERT_PRODUCT_FEED_MONITOR
(
  IN_RELATED_ID                 IN PROD_RELATED_FEED.PROD_RELATED_FEED_ID%TYPE,
  IN_TRANSACTION_ID             IN AZ_PRODUCT_FEED_MONITOR.AZ_TRANSACTION_ID%TYPE,
  IN_ORDINAL                    IN PROD_RELATED_FEED.ORDINAL_SENT%TYPE,
  IN_COMPANY_ID                 IN AZ_PRODUCT_FEED_MONITOR.COMPANY_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will insert a record into the AZ_ORDER_FEED_MONITOR table and
    updates the status in the AZ_ORDER_RELATED table.

Input:
        in_related_id                     number
        in_transaction_id                 number
        in_ordinal                        number
        in_company_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

current_time date;

BEGIN

BEGIN
--is the record in the database?
SELECT LAST_UPDATE INTO current_time
  FROM AZ_PRODUCT_FEED_MONITOR
  WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID;

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
  current_time := SYSDATE;
  INSERT INTO AZ_PRODUCT_FEED_MONITOR (
    AZ_TRANSACTION_ID,
    LAST_STATUS,
    LAST_UPDATE,
    NEXT_UPDATE,
    COMPANY_ID
  ) VALUES (
    IN_TRANSACTION_ID,
    'NOT CONFIRMED',
    current_time,
    current_time,
    IN_COMPANY_ID
  );
END;
END;

BEGIN
UPDATE PROD_RELATED_FEED
  SET
    AZ_TRANSACTION_ID = IN_TRANSACTION_ID,
    ORDINAL_SENT = IN_ORDINAL,
    PROD_RELATED_FEED_STATUS = 'SENT',
    STATUS_DATE = SYSDATE,
    SENT_DATE = SYSDATE
  WHERE
    PROD_RELATED_FEED_ID = IN_RELATED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END;
END INSERT_PRODUCT_FEED_MONITOR;


PROCEDURE GET_ORDER_FROM_MASTER_ORDER
(
  IN_MASTER_ORDER_NUMBER IN AZ_ORDER.MASTER_ORDER_NUMBER%TYPE,
  OUT_CUR               OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will retrieve an amazon order number, given the master order
    number.

Input:
        in_master_order_number            varchar2

Output:
        out                               ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT az_order_number
FROM   az_order
WHERE  master_order_number = in_master_order_number;

END get_order_from_master_order;

END API_PKG;
.
/
