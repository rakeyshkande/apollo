CREATE OR REPLACE
PROCEDURE amazon.insert_image_feed (
   in_sku_id               IN prod_related_feed.sku_id%TYPE,
   in_company_id           IN prod_related_feed.company_id%TYPE,
   in_product_id           IN prod_related_feed.product_id%TYPE,
   in_prod_feed_id         IN prod_related_feed.prod_feed_id%TYPE,
   in_image_location       IN prod_image_feed.image_location%TYPE,
   out_related_id         OUT prod_related_feed.prod_related_feed_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert into prod_related_feed and prod_image_feed.

   Because this insert can be part of a larger transaction, rollbacks will be
   done in the app tier.

Input:
   in_sku_id                   number
   in_company_id               varchar2
   in_product_id               varchar2
   in_prod_feed_id             number
   in_image_location           varchar2

Output:
   out_related_id              number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   out_related_id := keygen('PROD_RELATED_FEED');

   INSERT INTO prod_related_feed (
      prod_related_feed_id,
      prod_related_feed_type,
      prod_feed_id,
      sku_id,
      company_id,
      product_id,
      created_date,
      status_date,
      prod_related_feed_status)
   VALUES (
      out_related_id,
      'IMAGE',
      in_prod_feed_id,
      in_sku_id,
      in_company_id,
      in_product_id,
      SYSDATE,
      SYSDATE,
      'NOT SENT');

   INSERT INTO prod_image_feed (
      prod_related_feed_id,
      image_type,
      image_location)
   VALUES (
      out_related_id,
      'Main',
      in_image_location);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_image_feed;
.
/
