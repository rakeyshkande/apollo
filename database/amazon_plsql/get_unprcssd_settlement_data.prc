CREATE OR REPLACE
PROCEDURE amazon.GET_UNPRCSSD_SETTLEMENT_DATA
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves all the records from table settlement_data
        that do not have not been processed.

Input:
        N/A

Output:
        cursor containing settlement_data info

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
       SELECT az_settlement_data_id,
	      download_date,
	      total_amount,
	      start_date,
	      end_date,
	      deposit_date,
	      processed_date
         FROM settlement_data
        WHERE processed_date IS NULL;

END GET_UNPRCSSD_SETTLEMENT_DATA;
.
/
