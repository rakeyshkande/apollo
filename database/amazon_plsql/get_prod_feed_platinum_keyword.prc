CREATE OR REPLACE
PROCEDURE amazon.GET_PROD_FEED_PLATINUM_KEYWORD  (
 IN_PROD_FEED_ID               IN AMAZON.PROD_RELATED_FEED.PROD_RELATED_FEED_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
as
 /*-----------------------------------------------------------------------------
    This procedure will return records from the AMAZON. PROD_FEED_PLATINUM_KEYWORDS
    table for the passed product feed id.

Input:
        prod_feed_id                           varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/
begin

OPEN OUT_CUR FOR
	SELECT PLATINUM_KEYWORD
	FROM AMAZON.PROD_FEED_PLATINUM_KEYWORD
	WHERE PROD_FEED_ID=IN_PROD_FEED_ID
	ORDER BY PROD_FEED_PLATINUM_KEYWORD_ID;
end;
.
/
