CREATE OR REPLACE
FUNCTION amazon.KEYGEN (
  IN_P_TABLE  IN VARCHAR2
) RETURN NUMBER AS
/* This function returns key values for the AMAZON schema.
   It assumes that each table needing keys has a corresponding sequence,
   that has the same name as the table, followed by _SQ.  If this is
   true, it will return the NEXTVAL of that sequence. */
   v_key number;
   v_string varchar(400);
BEGIN
   v_string := 'select '||upper(IN_P_TABLE)||'_sq.nextval from dual';
   execute immediate v_string into v_key;
   return v_key;
END KEYGEN;
.
/
