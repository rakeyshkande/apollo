CREATE OR REPLACE PROCEDURE AMAZON.AZ_INSERT_REINSTATE_ORDER
(
IN_MASTER_ORDER_NUMBER					IN	AZ_REINSTATE_ORDER_LOG.MASTER_ORDER_NUMBER%TYPE,
IN_CONTEXT									IN AZ_REINSTATE_ORDER_LOG.CONTEXT%TYPE,
IN_XML										IN AZ_REINSTATE_ORDER_LOG.XML%TYPE,
IN_CREATED_BY                  		IN AZ_REINSTATE_ORDER_LOG.CREATED_BY%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record in AZ_REINSTATE_ORDER_LOG 

Input:
        in_master_order_number             varchar2
        in_context								 varchar2
        in_xml										 clob
        in_created_by							 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	INSERT INTO az_reinstate_order_log
	(
			  master_order_number,
			  context,
			  xml,
			  created_by,
			  created_on
	)
	VALUES
	(
			  in_master_order_number,
			  in_context,
			  in_xml,
			  UPPER(in_created_by),
			  SYSDATE
	);

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END AZ_INSERT_REINSTATE_ORDER;
/