CREATE OR REPLACE
PROCEDURE amazon.ins_prod_feed_platinum_keyword (
   in_prod_feed_id         IN prod_feed_platinum_keyword.prod_feed_id%TYPE,
   in_platinum_keyword     IN prod_feed_platinum_keyword.platinum_keyword%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into prod_feed_platinum_keyword.

Input:
   in_prod_feed_id             number
   in_platinum_keyword         varchar2

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO prod_feed_platinum_keyword (
      prod_feed_platinum_keyword_id,
      prod_feed_id,
      platinum_keyword)
   VALUES (
      keygen('PROD_FEED_PLATINUM_KEYWORD'),
      in_prod_feed_id,
      in_platinum_keyword);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END ins_prod_feed_platinum_keyword;
.
/
