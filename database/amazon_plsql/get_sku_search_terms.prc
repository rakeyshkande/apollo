CREATE OR REPLACE
PROCEDURE amazon.get_sku_search_terms  (
   in_sku_id     IN sku_search_term.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the search terms for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref_cursor containing
      search_term_id           number
      search_term              varchar2

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         search_term_id,
         search_term
      FROM amazon.sku_search_term
      WHERE sku_id = in_sku_id;

END get_sku_search_terms;
.
/
