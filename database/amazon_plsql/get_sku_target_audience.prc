CREATE OR REPLACE
PROCEDURE amazon.get_sku_target_audience   (
   in_sku_id     IN sku_target_audience.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the target audiences for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref cursor containing
      sku_target_audience_id   number
      cat_marketing_idx_id     number

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         sku_target_audience_id,
         cat_marketing_idx_id
      FROM sku_target_audience
      WHERE sku_id = in_sku_id;

END get_sku_target_audience;
.
/
