CREATE OR REPLACE
PROCEDURE amazon.insert_sku_platinum_keyword (
   in_sku_id              IN sku_platinum_keyword.sku_id%TYPE,
   in_keyword             IN sku_platinum_keyword.keyword%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into sku_platinum_keyword.

Input:
   in_sku_id   number
   in_keyword  varchar2

Output:
   out_status  varchar2
   out_message varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO sku_platinum_keyword (
      sku_platinum_keyword_id,
      sku_id,
      keyword)
   VALUES (
      keygen('SKU_PLATINUM_KEYWORD'),
      in_sku_id,
      in_keyword);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_sku_platinum_keyword;
.
/
