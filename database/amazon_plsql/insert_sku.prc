CREATE OR REPLACE
PROCEDURE amazon.insert_sku (
   in_company_id          IN sku.company_id%TYPE,
   in_product_id          IN sku.product_id%TYPE,
   in_live                IN sku.live%TYPE,
   in_tax_code            IN sku.tax_code%TYPE,
   in_launch_date         IN sku.launch_date%TYPE,
   in_discontinue_date    IN sku.discontinue_date%TYPE,
   in_title               IN sku.title%TYPE,
   in_brand               IN sku.brand%TYPE,
   in_description         IN sku.description%TYPE,
   in_max_order_qty       IN sku.max_order_qty%TYPE,
   in_manufacturer        IN sku.manufacturer%TYPE,
   in_gift_message        IN sku.gift_message%TYPE,
   in_cat_sub_category_id IN sku.cat_sub_category_id%TYPE,
   in_novator_id          IN sku.novator_id%TYPE,
   in_image_url           IN sku.image_url%TYPE,
   in_user_id             IN sku.LAST_UPDATE_USER_ID%TYPE,
   out_sku_id            OUT sku.sku_id%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a new SKU.

Input:
   in_company_id               varchar2
   in_product_id               varchar2
   in_live                     char
   in_tax_code                 varchar2
   in_launch_date              date
   in_discontinue_date         date
   in_title                    varchar2
   in_brand                    varchar2
   in_description              varchar2
   in_max_order_qty            number
   in_manufacturer             varchar2
   in_gift_message             varchar2
   in_cat_sub_category_id      number
   in_novator_id               varchar2
   in_image_url                varchar2
   in_user_id                  varchar2

Output:
   out_sku_id                  number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN
   out_sku_id := keygen('SKU');

   INSERT INTO sku (
      sku_id,
      company_id,
      product_id,
      live,
      tax_code,
      launch_date,
      discontinue_date,
      title,
      brand,
      description,
      max_order_qty,
      manufacturer,
      gift_message,
      cat_sub_category_id,
      novator_id,
      image_url,
      last_update_user_id,
      last_update)
   VALUES (
      out_sku_id,
      in_company_id,
      in_product_id,
      in_live,
      in_tax_code,
      in_launch_date,
      in_discontinue_date,
      in_title,
      in_brand,
      in_description,
      in_max_order_qty,
      in_manufacturer,
      in_gift_message,
      in_cat_sub_category_id,
      in_novator_id,
      in_image_url,
      in_user_id,
      SYSDATE);

   OUT_STATUS := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_sku;
.
/
