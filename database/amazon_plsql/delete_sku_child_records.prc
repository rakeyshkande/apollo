CREATE OR REPLACE
PROCEDURE amazon.delete_sku_child_records  (
   in_sku_id              IN sku.sku_id%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   For the input sku, delete from the following tables:
      sku_bullet_point
      sku_item_type
      sku_platinum_keyword
      sku_search_term
      sku_target_audience
      sku_used_for
      prod_feed_bullet_point
      prod_feed_item_type
      prod_feed_platinum_keyword
      prod_feed_search_term
      prod_feed_target_audience
      prod_feed_used_for

Input:
   in_sku_id                   number

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

   CURSOR c_prod_related_feed IS
      SELECT prod_feed_id
      FROM   prod_related_feed
      WHERE  sku_id = in_sku_id;
   r_prod_related_feed c_prod_related_feed%ROWTYPE;

BEGIN

   DELETE FROM sku_bullet_point WHERE sku_id = in_sku_id;

   DELETE FROM sku_item_type WHERE sku_id = in_sku_id;

   DELETE FROM sku_platinum_keyword WHERE sku_id = in_sku_id;

   DELETE FROM sku_search_term WHERE sku_id = in_sku_id;

   DELETE FROM sku_target_audience WHERE sku_id = in_sku_id;

   DELETE FROM sku_used_for WHERE sku_id = in_sku_id;

   FOR r_prod_related_feed IN c_prod_related_feed LOOP

      DELETE FROM prod_feed_bullet_point WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

      DELETE FROM prod_feed_item_type WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

      DELETE FROM prod_feed_platinum_keyword WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

      DELETE FROM prod_feed_search_term WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

      DELETE FROM prod_feed_target_audience WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

      DELETE FROM prod_feed_used_for WHERE prod_feed_id = r_prod_related_feed.prod_feed_id;

   END LOOP;

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END delete_sku_child_records;
.
/
