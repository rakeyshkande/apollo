CREATE OR REPLACE
PROCEDURE amazon.insert_product_feed (
   in_sku_id               IN prod_related_feed.sku_id%TYPE,
   in_company_id           IN prod_related_feed.company_id%TYPE,
   in_product_id           IN prod_related_feed.product_id%TYPE,
   in_live                 IN prod_feed.live%TYPE,
   in_tax_code             IN prod_feed.tax_code%TYPE,
   in_launch_date_pst      IN prod_feed.launch_date_pst%TYPE,
   in_discontinue_date_pst IN prod_feed.discontinue_date_pst%TYPE,
   in_title                IN prod_feed.title%TYPE,
   in_brand                IN prod_feed.brand%TYPE,
   in_description          IN prod_feed.description%TYPE,
   in_manufacturer         IN prod_feed.manufacturer%TYPE,
   in_max_order_qty        IN prod_feed.max_order_qty%TYPE,
   in_gift_message         IN prod_feed.gift_message%TYPE,
   in_cat_sub_category_id  IN prod_feed.cat_sub_category_id%TYPE,
   in_user_id              IN prod_feed.USER_ID%TYPE,
   out_related_feed_id    OUT prod_feed.prod_related_feed_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a PRODUCT prod_related_feed record.  This is a record with a
   prod_related_feed_type of PRODUCT and a prod_feed_id that is null.

   Then, insert a prod_feed record.  This will be a child of the prod_related_feed.

   Check the product type in the ftd_apps.product_master table.  If it's SPEGFT,
   this is a dropship product feed, which means it needs a product override feed.
   But to do that we need to calculate the shipping amount, for which logic
   is included.

   Because this insert can be part of a larger transaction, rollbacks will be
   done in the app tier.

Input:
   in_sku_id               number
   in_company_id           varchar2
   in_product_id           varchar2
   in_live                 varchar2
   in_tax_code             varchar2
   in_launch_date_pst      date
   in_discontinue_date_pst date
   in_title                varchar2
   in_brand                varchar2
   in_description          varchar2
   in_manufacturer         varchar2
   in_max_order_qty        number
   in_gift_message         varchar2
   in_cat_sub_category_id  number
   in_user_id              varchar2

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN
   out_related_feed_id := keygen('PROD_RELATED_FEED');

   INSERT INTO prod_related_feed (
      prod_related_feed_id,
      prod_related_feed_type,
      sku_id,
      company_id,
      product_id,
      created_date,
      status_date,
      prod_related_feed_status)
   VALUES (
      out_related_feed_id,
      'PRODUCT',
      in_sku_id,
      in_company_id,
      in_product_id,
      SYSDATE,
      SYSDATE,
      'NOT SENT');

   INSERT INTO prod_feed (
      prod_related_feed_id,
      live,
      tax_code,
      launch_date_pst,
      discontinue_date_pst,
      title,
      brand,
      description,
      manufacturer,
      max_order_qty,
      gift_message,
      cat_sub_category_id,
      user_id)
   VALUES (
      out_related_feed_id,
      in_live,
      in_tax_code,
      in_launch_date_pst,
      in_discontinue_date_pst,
      in_title,
      in_brand,
      in_description,
      in_manufacturer,
      in_max_order_qty,
      in_gift_message,
      in_cat_sub_category_id,
      in_user_id);

  out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_product_feed;
.
/
