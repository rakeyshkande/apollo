CREATE OR REPLACE
PROCEDURE amazon.MONITOR_PRICE_VARIANCE AS
/*--------------------------------------------------------------------------------
  This procedure will query the AZ_PRICE_VARIANCE TABLE looking for reportable
  variances.  If any are found, then an email will be sent to the passed recipients.
--------------------------------------------------------------------------------*/

CURSOR pagers_c IS
      SELECT pager_number
      FROM   sitescope.PAGERS
      WHERE  project = 'AMAZON';
   pagers_r pagers_c%ROWTYPE;

v_current_time date := SYSDATE;
v_variance_count number;
v_variance_max_orders number;
v_variance_time_period number;
v_variance_ceiling number;
v_variance_order_limit number;
v_mail utl_smtp.connection;
v_minutes_in_a_day number := 1440;
v_confirmation_number varchar(32);
v_location VARCHAR2(10);

PROCEDURE send_header(name IN VARCHAR2, header IN VARCHAR2) AS
   BEGIN
      utl_smtp.write_data(v_mail, name || ': ' || header || utl_tcp.CRLF);
   END;

BEGIN

SELECT sys_context('USERENV','DB_NAME')
INTO   v_location
FROM   dual;

DECLARE CURSOR var_parms_cur IS
  SELECT VARIANCE_MAX_ORDERS, VARIANCE_TIME_PERIOD, VARIANCE_CEILING, VARIANCE_ORDER_LIMIT
  FROM AMAZON_PARMS;

BEGIN
  OPEN var_parms_cur;
  FETCH var_parms_cur
        INTO v_variance_max_orders,v_variance_time_period,v_variance_ceiling,v_variance_order_limit;
  IF var_parms_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE var_parms_cur;
END;

-- First check to see if multiple orders have exceeded the ceiling variance
SELECT COUNT(*) INTO v_variance_count FROM AZ_PRICE_VARIANCE
WHERE
     (
      abs(ftd_price-amazon_price) > v_variance_ceiling OR
      abs(ftd_shipping-amazon_shipping) > v_variance_ceiling OR
      abs(ftd_taxes-amazon_taxes) > v_variance_ceiling OR
      abs(ftd_shipping_taxes-amazon_shipping_taxes) > v_variance_ceiling OR
      abs(ftd_service_fee-amazon_service_fee) > v_variance_ceiling OR
      abs(ftd_price+
          ftd_shipping+
          ftd_taxes+
          ftd_shipping_taxes+
          ftd_service_fee-
          amazon_price-
          amazon_shipping-
          amazon_taxes-
          amazon_shipping_taxes-
          amazon_service_fee) > v_variance_ceiling
      )
    AND ORDER_TIMESTAMP > (v_current_time-(v_variance_time_period/v_minutes_in_a_day));

IF( v_variance_count > v_variance_max_orders ) THEN
  v_mail := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server', 'NAME'));
  utl_smtp.helo(v_mail, 'ftdi.com');
  utl_smtp.mail(v_mail, 'amazon@ftdi.com');
  --utl_smtp.rcpt(v_mail, IN_EMAIL_RECIPIENT);
  FOR pagers_r IN pagers_c LOOP
     utl_smtp.rcpt(v_mail, pagers_r.pager_number);
  END LOOP;
  utl_smtp.open_data(v_mail);
  send_header('From', '"'||v_location||'" <amazon@ftdi.com>');
  send_header('Subject', 'Amazon Price Variance');
  utl_smtp.write_data(v_mail, v_variance_count || ' orders have exceeded the ' || v_variance_ceiling || ' price variance in the last ' || v_variance_time_period || ' minutes.');
  utl_smtp.close_data(v_mail);
  utl_smtp.quit(v_mail);
END IF;

-- now check to see if individual order prices have exceeded the order limit variance
DECLARE CURSOR max_cur IS
  SELECT CONFIRMATION_NUMBER
  FROM AZ_PRICE_VARIANCE
  WHERE abs(ftd_price-amazon_price) > v_variance_order_limit
    AND ORDER_TIMESTAMP > (v_current_time-(v_variance_time_period/v_minutes_in_a_day));
BEGIN
  OPEN max_cur;

  LOOP
    FETCH max_cur
          INTO v_confirmation_number;
    EXIT WHEN max_cur%NOTFOUND;

    v_mail := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server', 'NAME'));
    utl_smtp.helo(v_mail, 'ftdi.com');
    utl_smtp.mail(v_mail, 'amazon@ftdi.com');
    --utl_smtp.rcpt(v_mail, IN_EMAIL_RECIPIENT);
    FOR pagers_r IN pagers_c LOOP
      utl_smtp.rcpt(v_mail, pagers_r.pager_number);
    END LOOP;
    utl_smtp.open_data(v_mail);
    send_header('From', '"'||v_location||'" <amazon@ftdi.com>');
    send_header('Subject', 'Amazon Price Variance');
    utl_smtp.write_data(v_mail, 'Order ' || v_confirmation_number || ' was received with an invalid price.' );
    utl_smtp.close_data(v_mail);
    utl_smtp.quit(v_mail);

  END LOOP;
  CLOSE max_cur;
END;

END MONITOR_PRICE_VARIANCE;
.
/
