CREATE OR REPLACE
PROCEDURE amazon.mark_settlement_processed (
 in_settlement_id IN settlement_data.az_settlement_data_id%TYPE,
 out_status       OUT VARCHAR2,
 out_message      OUT VARCHAR2
)

/*-----------------------------------------------------------------------------
    This procedure update the record in the SETTLEMENT_DATA table
    for the passed settlement id.

Input:
        in_settlement_id                     number

Output:
        out_status                           varchar
        out_message                          varchar
-----------------------------------------------------------------------------*/

AS
BEGIN
 UPDATE settlement_data
 SET processed_date = SYSDATE
 WHERE az_settlement_data_id = in_settlement_id;

 out_status := 'Y';
EXCEPTION
  WHEN OTHERS THEN
    out_status := 'N';
    out_message := 'ERROR OCCURRED ['||sqlcode||'] '||SUBSTR(sqlerrm,1,256);
END mark_settlement_processed;
.
/
