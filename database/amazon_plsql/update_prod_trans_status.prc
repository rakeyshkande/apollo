CREATE OR REPLACE
PROCEDURE amazon.update_prod_trans_status (
   in_transaction_id    IN az_product_feed_monitor.az_transaction_id%TYPE,
   in_status            IN az_product_feed_monitor.last_status%TYPE,
   out_status          OUT VARCHAR2,
   out_message         OUT VARCHAR2) IS

/*-----------------------------------------------------------------------------
    This new stored procedure will update AZ_ORDER_RELATED record with
    the passed transaction id and ordinal to the passed status

Input:
        transaction id                  varchar2
        ordinal                         number
        status                          varchar2
        result code                     varchar2
        result description              varchar2
        result message code             number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

oldAttempts NUMBER;

BEGIN

SELECT ATTEMPTS INTO oldAttempts
  FROM AZ_PRODUCT_FEED_MONITOR
  WHERE AZ_TRANSACTION_ID = IN_TRANSACTION_ID;

UPDATE AZ_PRODUCT_FEED_MONITOR
  SET
    LAST_STATUS=IN_STATUS,
    LAST_UPDATE=SYSDATE,
    ATTEMPTS=oldAttempts+1
  WHERE AZ_TRANSACTION_ID = IN_TRANSACTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- no record was found
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Status update failed for transacion id  ' || IN_TRANSACTION_ID || '.  Transaction not found.';
END; -- end NO_DATA_FOUND exception

--Ignore duplicate inserts
WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'Y';
        OUT_MESSAGE := 'Transaction ID already in database for ' || IN_TRANSACTION_ID;
END;    -- end exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END update_prod_trans_status;
.
/
