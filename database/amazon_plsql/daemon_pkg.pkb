CREATE OR REPLACE
PACKAGE BODY amazon.daemon_pkg AS

   pg_this_package VARCHAR2(61) := 'amazon.daemon_pkg';

FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN IS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Parameter:
        the name of the job, as found in user_jobs.what, including any parameters.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

   v_check    NUMBER;
   v_return   BOOLEAN;

BEGIN

   SELECT COUNT(*)
   INTO   v_check
   FROM   user_jobs
   WHERE  what = p_job_name;

   IF v_check > 0 THEN
      v_return := TRUE;
   ELSE
      v_return := FALSE;
   END IF;

   RETURN V_RETURN;

END CURRENTLY_RUNNING;



PROCEDURE submit_monitor_price_variance IS

/*-----------------------------------------------------------------------------
Description:
        submit monitor_price_variance
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit_monitor_price_variance';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   v_submit_string := 'MONITOR_PRICE_VARIANCE;';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value('monitor_price_variance','INTERVAL');
      dbms_job.submit(v_job, v_submit_string, SYSDATE, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit_monitor_price_variance;



PROCEDURE remove_monitor_price_variance IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove_monitor_price_variance';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = 'MONITOR_PRICE_VARIANCE;';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' MONITOR_PRICE_VARIANCE REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' MONITOR_PRICE_VARIANCE NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove_monitor_price_variance;


PROCEDURE sb_clean_discontinued_products IS

/*-----------------------------------------------------------------------------
Description:
        submit clean_discontinued_products

        Note that unlike the others, this one runs once per day, at a specified
        time.  So in the submit statement, the submission time and the interval
        are the same.  It works in this case if the expression is carefully
        written.  However, the submission time has to be an actual datetime
        value, while the interval is a character string that is evaluated at
        a later time.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'sb_clean_discontinued_products';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;

BEGIN

   v_submit_string := 'CLEAN_DISCONTINUED_PRODUCTS;';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value('clean_discontinued_products','INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END sb_clean_discontinued_products;


PROCEDURE rm_clean_discontinued_products IS

/*-----------------------------------------------------------------------------
Description:
        Search for and rm the daemon from the DBMS JOB queue.
        This will look for a user job LIKE clean_discontinued_products.
        If there's more than one, it removes the first one (lowest job number).
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'rm_clean_discontinued_products';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = 'CLEAN_DISCONTINUED_PRODUCTS;';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' CLEAN_DISCONTINUED_PRODUCTS REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' CLEAN_DISCONTINUED_PRODUCTS NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END rm_clean_discontinued_products;


PROCEDURE submit (p_procedure IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        submit a scheduled job
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   v_submit_string := p_procedure||'('||frp.misc_pkg.get_global_parm_value(p_procedure,'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (p_procedure) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(p_procedure,'INTERVAL');
      dbms_job.submit(v_job, v_submit_string, SYSDATE, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit;


PROCEDURE remove (p_procedure IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what like p_procedure||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_procedure||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||p_procedure||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove;

FUNCTION az_product_feed_interval RETURN DATE IS
   /* This function should return an hourly schedule between 8:00 and 18:00 Monday through Friday. */
   v_return DATE;
BEGIN
   IF TRIM(TO_CHAR(SYSDATE+(6/24),'DAY')) IN ('SATURDAY','SUNDAY') THEN
      v_return := TRUNC(NEXT_DAY(SYSDATE,'MONDAY'))+(8/24);
   ELSIF TO_NUMBER(TO_CHAR(SYSDATE,'HH24')) < 8 THEN
      v_return := TRUNC(SYSDATE)+(8/24);
   ELSIF TO_NUMBER(TO_CHAR(SYSDATE,'HH24')) > 18 THEN
      v_return := TRUNC(SYSDATE)+1+(8/24);
   ELSE
      v_return := SYSDATE+(1/24);
   END IF;
   RETURN v_return;
END az_product_feed_interval;

END daemon_pkg;
.
/
