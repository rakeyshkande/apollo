CREATE OR REPLACE
PROCEDURE amazon.insert_override_feed (
   in_sku_id               IN prod_related_feed.sku_id%TYPE,
   in_company_id           IN prod_related_feed.company_id%TYPE,
   in_product_id           IN prod_related_feed.product_id%TYPE,
   in_prod_feed_id         IN prod_related_feed.prod_feed_id%TYPE,
   in_ship_amount          IN prod_override_feed.ship_amount%TYPE,
   out_related_id         OUT prod_related_feed.prod_related_feed_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a record into prod_related_feed, then into prod_override_feed.
   prod_related_feed requires the company_id and product_id from the sku.
   This information is duplicated here, because it is possible to have prod_related_feeds
   with company_ids and product_ids, but not skus.  (These are orphan inventory
   feeds.)

   Because this insert can be part of a larger transaction, rollbacks will be
   done in the app tier.

Input:
   in_sku_id                   number
   in_company_id               varchar2
   in_product_id               varchar2
   in_prod_feed_id             number
   in_ship_amount              number
Output:
   out_related_id              number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

   v_date date;

BEGIN
   v_date := SYSDATE;
   out_related_id := keygen('PROD_RELATED_FEED');

   INSERT INTO prod_related_feed(
      prod_related_feed_id,
      prod_related_feed_type,
      prod_feed_id,
      sku_id,
      company_id,
      product_id,
      created_date,
      status_date,
      prod_related_feed_status)
   VALUES (
      out_related_id,
      'OVERRIDE',
      in_prod_feed_id,
      in_sku_id,
      in_company_id,
      in_product_id,
      v_date,
      v_date,
      'NOT SENT');

   INSERT INTO prod_override_feed (
      prod_related_feed_id,
      ship_amount)
   VALUES (
      out_related_id,
      in_ship_amount);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_override_feed;
.
/
