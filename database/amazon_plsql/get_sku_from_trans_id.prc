CREATE OR REPLACE
PROCEDURE amazon.GET_SKU_FROM_TRANS_ID (
 IN_TRANSACTION_ID      IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
 IN_ORDINAL_POSITION    IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
 OUT_CUR               OUT types.ref_cursor
)
as

/*------------------------------------------------------------------------------
   Returns the related record for the passed transaction id and ordinal position

Input:
   in_transaction_id           number
   in_ordinal_position         number

Output:
   out_cur                     ref cursor
------------------------------------------------------------------------------*/
BEGIN
 OPEN OUT_CUR FOR
  SELECT PRODUCT_ID, PROD_RELATED_FEED_TYPE
  	FROM AMAZON.PROD_RELATED_FEED
  	WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID and ORDINAL_SENT=IN_ORDINAL_POSITION;
END GET_SKU_FROM_TRANS_ID;
.
/
