CREATE OR REPLACE
PROCEDURE amazon.insert_override_feed_in_range (
   in_floor                IN NUMBER,
   in_ceiling              IN NUMBER,
   in_ship_amount          IN NUMBER,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

INSERT_OVERRIDE_EXCEPTION EXCEPTION;
v_status varchar2(1);
v_message varchar2(512);
v_dummy number;

BEGIN

   DECLARE CURSOR sku_cur IS
      SELECT s.SKU_ID, s.COMPANY_ID, s.PRODUCT_ID, p.PRODUCT_TYPE, p.STANDARD_PRICE
        FROM AMAZON.SKU S
        JOIN FTD_APPS.PRODUCT_MASTER p ON p.PRODUCT_ID = s.PRODUCT_ID
        JOIN FTD_APPS.PRODUCT_SHIP_METHODS m on m.PRODUCT_ID = p.PRODUCT_ID
        WHERE s.LIVE='Y' AND
              p.PRODUCT_TYPE='SPEGFT' AND
              m.SHIP_METHOD_ID='2F' AND
              p.STANDARD_PRICE >=in_floor AND
              p.STANDARD_PRICE <= in_ceiling;

    BEGIN
    FOR sku_rec IN sku_cur
      LOOP
        UPDATE "AMAZON"."SKU"
          SET LIVE='N',
            LAST_UPDATE_USER_ID='PDB SHIPPING UPDATE',
            LAST_UPDATE=SYSDATE
          WHERE SKU_ID=sku_rec.SKU_ID;

        "AMAZON".INSERT_OVERRIDE_FEED(
          IN_SKU_ID => sku_rec.SKU_ID,
          IN_COMPANY_ID => 'FTD',
          IN_PRODUCT_ID => sku_rec.PRODUCT_ID,
          IN_PROD_FEED_ID => null,
          IN_SHIP_AMOUNT => in_ship_amount,
          OUT_RELATED_ID => v_dummy,
          OUT_STATUS => v_status,
          OUT_MESSAGE => v_message
        );

        IF v_status='N' THEN
          RAISE INSERT_OVERRIDE_EXCEPTION;
        END IF;
      END LOOP;
    END;

OUT_STATUS := 'Y';

EXCEPTION WHEN INSERT_OVERRIDE_EXCEPTION THEN
BEGIN
  OUT_STATUS := v_status;
  OUT_MESSAGE := v_message;
END;

END insert_override_feed_in_range;
.
/
