CREATE OR REPLACE
PROCEDURE amazon.get_unconfirmed_prod_trans (
  IN_COMPANY                    IN AZ_ORDER_FEED_MONITOR.COMPANY_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will for records in the AZ_PRODUCT_FEED_MONITOR
    table with a NEXT_UPDATE date/time earlier then the current time and a
    status that is not one of the following:
    'Completed'
    'Rejected'
    'HOLD'
    'CANCELLED'

Input:
        company id                      varchar2

Output:
        out                             ref cursor
-----------------------------------------------------------------------------*/

BEGIN
OPEN OUT_CUR FOR
  SELECT AZ_TRANSACTION_ID
  FROM AZ_PRODUCT_FEED_MONITOR
  WHERE COMPANY_ID = IN_COMPANY
    AND NEXT_UPDATE < SYSDATE
    AND LAST_STATUS <> 'Rejected'
    AND LAST_STATUS <> 'Complete'
    AND LAST_STATUS <> 'HOLD'
    AND LAST_STATUS <> 'CANCELLED'
  ORDER BY AZ_TRANSACTION_ID;
END get_unconfirmed_prod_trans;
.
/
