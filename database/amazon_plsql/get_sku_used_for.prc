CREATE OR REPLACE
PROCEDURE amazon.get_sku_used_for          (
   in_sku_id     IN sku_used_for.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the "used fors" (whatever those are) for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref cursor containing
      sku_used_for_id          number
      sku_id                   number
      cat_marketing_idx_id     number

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         sku_used_for_id,
         cat_marketing_idx_id
      FROM sku_used_for
      WHERE sku_id = in_sku_id;

END get_sku_used_for;
.
/
