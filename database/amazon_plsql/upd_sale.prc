CREATE OR REPLACE
PROCEDURE amazon.upd_sale (
   in_company_id       IN az_sale.company_id%TYPE,
   in_sale_source_code IN az_sale.sale_source_code%TYPE,
   in_start_date       IN az_sale.start_date%TYPE,
   in_end_date         IN az_sale.end_date%TYPE,
   in_user_id          IN az_sale.LAST_UPDATE_USER_ID%TYPE,
   out_status         OUT VARCHAR,
   out_message        OUT VARCHAR) AS

/*------------------------------------------------------------------------------
   Update a record in az_sale.  Assume company_id is the primary key.
   This procedure is not used to update default_sale_source_code.

   This procedure does not commit -- that is left to the invoker.
   It will rollback, if the call to insert_all_price_feeds returns status N.

Input:
   company_id         varchar2
   sale_source_code   varchar2
   start_date         date
   end_date           date

Output:
   status             Y or N
   message            varchar2

------------------------------------------------------------------------------*/

   v_sale_source_code az_sale.sale_source_code%TYPE;
   v_start_date       az_sale.start_date%TYPE;
   v_end_date         az_sale.end_date%TYPE;

   v_status VARCHAR2(1);
   v_message VARCHAR2(500);

   local_error EXCEPTION;

BEGIN

   SELECT sale_source_code, start_date, end_date
   INTO   v_sale_source_code, v_start_date, v_end_date
   FROM   az_sale
   WHERE  company_id = in_company_id;

   IF (v_sale_source_code <> in_sale_source_code)
   OR (v_start_date       <> in_start_date)
   OR (v_end_date         <> in_end_date)
   OR (v_start_date IS NULL AND in_start_date IS NOT NULL)
   OR (v_start_date IS NOT NULL AND in_start_date IS NULL)
   OR (v_end_date IS NULL AND in_end_date IS NOT NULL)
   OR (v_end_date IS NOT NULL AND in_end_date IS NULL)
   THEN

      UPDATE az_sale
      SET    sale_source_code = in_sale_source_code,
             start_date       = in_start_date,
             end_date         = in_end_date,
             last_update_user_id = in_user_id,
             last_update         = SYSDATE
      WHERE  company_id = in_company_id;

      insert_all_price_feeds (in_company_id, in_start_date, in_end_date, v_status, v_message);

      IF v_status = 'N' THEN
         ROLLBACK;
         RAISE local_error;
      END IF;

   END IF;

   out_status := 'Y';

EXCEPTION
   WHEN local_error THEN
      out_status := 'N';
      out_message := 'insert_all_price_feeds: '||v_message;
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED ['||sqlcode||'] '||SUBSTR(sqlerrm,1,256);

END upd_sale;
.
/
