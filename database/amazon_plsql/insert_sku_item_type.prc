CREATE OR REPLACE
PROCEDURE amazon.insert_sku_item_type        (
   in_sku_id               IN sku_item_type.sku_id%TYPE,
   in_cat_marketing_idx_id IN sku_item_type.cat_marketing_idx_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into sku_item_type.  This table is a child of
   cat_marketing_index, which also has an "index type" field: TARGET AUDIENCE,
   ITEM TYPE or USED FOR.  So this program must verify that the referenced
   cat_marketing_index key is a ITEM TYPE type.  If it isn't, it will
   return status N and not do the insert.

Input:
   in_sku_id                   number
   in_cat_marketing_idx_id     number

Output:
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

   v_index_type cat_marketing_index.idx_type%TYPE;

BEGIN

   SELECT idx_type
   INTO   v_index_type
   FROM   cat_marketing_index
   WHERE  cat_marketing_idx_id = in_cat_marketing_idx_id;

   IF v_index_type IS NULL THEN

      out_status := 'N';
      out_message := 'Referenced marketing index does not exist';

   ELSIF v_index_type <> 'ITEM TYPE' THEN

      out_status := 'N';
      out_message := 'Referenced marketing index is not an ITEM TYPE index';

   ELSE

      INSERT INTO sku_item_type (
         sku_item_type_id,
         sku_id,
         cat_marketing_idx_id)
      VALUES (
         keygen('SKU_ITEM_TYPE'),
         in_sku_id,
         in_cat_marketing_idx_id);

      out_status := 'Y';

   END IF;

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_sku_item_type;
.
/
