CREATE OR REPLACE
PROCEDURE amazon.get_sale(
   in_company_id IN az_sale.company_id%TYPE,
   out_cur      OUT types.ref_cursor) IS
BEGIN
   OPEN out_cur FOR
      SELECT default_source_code, sale_source_code, start_date, end_date
      FROM   az_sale
      WHERE  company_id = in_company_id;
END get_sale;
.
/
