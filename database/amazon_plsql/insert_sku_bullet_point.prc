CREATE OR REPLACE
PROCEDURE amazon.insert_sku_bullet_point   (
   in_sku_id              IN sku_bullet_point.sku_id%TYPE,
   in_bullet_point        IN sku_bullet_point.bullet_point%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a bullet point for the SKU.

Input:
   in_sku_id       number
   in_bullet_point varchar2

Output:
   out_status      varchar2
   out_message     varchar2

------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO sku_bullet_point (
      sku_bullet_point_id,
      sku_id,
      bullet_point )
   VALUES (
      keygen('SKU_BULLET_POINT'),
      in_sku_id,
      in_bullet_point);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_sku_bullet_point;
.
/
