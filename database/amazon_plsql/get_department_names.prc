CREATE OR REPLACE
PROCEDURE amazon.get_department_names (
   out_cur                OUT types.ref_cursor) IS

BEGIN

   OPEN OUT_CUR FOR
    SELECT CAT_CATEGORY_CODE, CAT_CATEGORY_DESC
      FROM CAT_CATEGORY
      ORDER BY CAT_CATEGORY_CODE;

END get_department_names;
.
/
