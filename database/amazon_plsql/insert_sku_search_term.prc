CREATE OR REPLACE
PROCEDURE amazon.insert_sku_search_term    (
   in_sku_id              IN sku_search_term.sku_id%TYPE,
   in_search_term         IN sku_search_term.search_term%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into the SKU_SEARCH_TERM table.

Input:
      in_sku_id      number
      in_search_term varchar2

Output:
      out_status     varchar2
      out_message    varchar2
------------------------------------------------------------------------------*/

BEGIN

   INSERT INTO sku_search_term (
      search_term_id,
      sku_id,
      search_term)
   VALUES (
      keygen('SKU_SEARCH_TERM'),
      in_sku_id,
      in_search_term);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END insert_sku_search_term;
.
/
