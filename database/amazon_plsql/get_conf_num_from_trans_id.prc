CREATE OR REPLACE
PROCEDURE amazon.GET_CONF_NUM_FROM_TRANS_ID (
 IN_TRANSACTION_ID      IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
 IN_ORDINAL_POSITION    IN AZ_ORDER_RELATED.AZ_TRANSACTION_ID%TYPE,
 OUT_CUR               OUT types.ref_cursor
)
as

/*------------------------------------------------------------------------------
   Returns the related record for the passed transaction id and ordinal position

Input:
   in_transaction_id           number
   in_ordinal_position         number

Output:
   out_cur                     ref cursor
------------------------------------------------------------------------------*/
BEGIN
 OPEN OUT_CUR FOR
  SELECT CONFIRMATION_NUMBER, TYPE_FEED
  	FROM AZ_ORDER_RELATED
  	WHERE AZ_TRANSACTION_ID=IN_TRANSACTION_ID AND ORDINAL_SENT = IN_ORDINAL_POSITION;
END GET_CONF_NUM_FROM_TRANS_ID;
.
/
