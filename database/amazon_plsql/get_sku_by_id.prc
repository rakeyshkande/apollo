CREATE OR REPLACE
PROCEDURE amazon.get_sku_by_id (
   in_sku_id     IN sku.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Produce a ref_cursor containing the SKU or SKUs for the specified sku_id.

Input:
   in_sku_id                   number

Output:
   ref cursor containing:
      sku_id                   number
      company_id               varchar2
      product_id               varchar2
      live                     char
      tax_code                 varchar2
      launch_date              date
      discontinue_date         date
      title                    varchar2
      brand                    varchar2
      description              varchar2
      max_order_qty            number
      manufacturer             varchar2
      gift_message             varchar2
      cat_sub_category_id      number
      novator_id               varchar2
      image_url                varchar2

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT
         sku_id,
         company_id,
         product_id,
         live,
         tax_code,
         launch_date,
         discontinue_date,
         title,
         brand,
         description,
         max_order_qty,
         manufacturer,
         gift_message,
         cat_sub_category_id,
         novator_id,
         image_url
      FROM sku
      WHERE sku_id = in_sku_id;

END get_sku_by_id;
.
/
