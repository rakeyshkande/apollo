CREATE OR REPLACE
PROCEDURE amazon.get_short_sku_list (
   IN_COMPANY_ID           IN sku.company_id%TYPE,
   IN_WHERE                IN varchar2,
   OUT_CUR                OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
  This procedure will return all records that match the where clause that is
  passed into the database.
------------------------------------------------------------------------------*/
v_sql varchar2(512);

BEGIN

  IF IN_WHERE IS NOT NULL THEN
    v_sql := 'SELECT sku_id, product_id, title, live, launch_date, discontinue_date FROM sku WHERE company_id=''' || IN_COMPANY_ID || '''' || IN_WHERE || 'ORDER BY product_id';
  ELSE
    v_sql := 'SELECT sku_id, product_id, title, live, launch_date, discontinue_date FROM sku WHERE company_id=''' || IN_COMPANY_ID || '''' || 'ORDER BY product_id';
  END IF;

  OPEN OUT_CUR FOR v_sql;

END get_short_sku_list;
.
/
