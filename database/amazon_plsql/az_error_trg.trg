CREATE OR REPLACE
TRIGGER amazon.az_error_trg
AFTER INSERT ON amazon.az_error
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
/* Send out pages for entries in the az_error table. */
/* To add, delete or modify recipients, look at the table sitescope.pagers. */

   c utl_smtp.connection;
   v_location VARCHAR2(10);

   CURSOR pagers_c IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = 'AMAZON';
   pagers_r pagers_c%ROWTYPE;

   PROCEDURE send_header(name IN VARCHAR2, header IN VARCHAR2) AS
   BEGIN
      utl_smtp.write_data(c, name || ': ' || header || utl_tcp.CRLF);
   END;

BEGIN

   c := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server', 'NAME'));
   SELECT sys_context('USERENV','DB_NAME')
   INTO   v_location
   FROM   dual;
   utl_smtp.helo(c, 'ftdi.com');
   utl_smtp.mail(c, 'oracle@ftdi.com');
   FOR pagers_r IN pagers_c LOOP
      utl_smtp.rcpt(c, pagers_r.pager_number);
   END LOOP;
   utl_smtp.open_data(c);
   send_header('From', '"'||v_location||'" <oracle@ftdi.com>');
   send_header('Subject', 'AZ_ERROR Message');
   utl_smtp.write_data(c, utl_tcp.CRLF||'AZ_ERROR_ID: '||to_char(:new.az_error_id));
   utl_smtp.write_data(c, utl_tcp.CRLF||'TIMESTAMP: '||to_char(:new.time_stamp,'yyyy/mm/dd hh24:mi:ss'));
   utl_smtp.write_data(c, utl_tcp.CRLF||:new.error);
   utl_smtp.close_data(c);
   utl_smtp.quit(c);

EXCEPTION

   WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

      BEGIN

         utl_smtp.quit(c);

      EXCEPTION

         WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

            NULL; -- When the SMTP server is down or unavailable, we don't have
            -- a connection to the server. The quit call will raise an
            -- exception that we can ignore.

      END;

      raise_application_error(-20000, 'Failed to send mail due to the following error: ' || sqlerrm);

END;
.
/
