CREATE OR REPLACE
PROCEDURE amazon.INSERT_INVENTORY_FEED (
   in_sku_id               IN prod_related_feed.sku_id%TYPE,
   in_company_id           IN prod_related_feed.company_id%TYPE,
   in_product_id           IN prod_related_feed.product_id%TYPE,
   in_prod_feed_id         IN prod_related_feed.prod_feed_id%TYPE,
   in_available            IN prod_inventory_feed.available%TYPE,
   out_related_id         OUT prod_related_feed.prod_related_feed_id%TYPE,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
  This procedure will insert a record into the PROD_RELATED_FEED table and a
  corresponding entry into the PROD_INVENTORY_FEED table.

   Because this insert can be part of a larger transaction, rollbacks will be
   done in the app tier.

Input:
   in_sku_id                   number
   in_company_id               varchar2
   in_product_id               varchar2
   in_prod_feed_id             number
   in_available                varchar2

Output:
   out_related_id              number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

v_current_time prod_related_feed.CREATED_DATE%TYPE := SYSDATE;

BEGIN
  out_related_id := keygen('PROD_RELATED_FEED');

  INSERT INTO prod_related_feed (
      prod_related_feed_id,
      prod_related_feed_type,
      prod_feed_id,
      sku_id,
      company_id,
      product_id,
      created_date,
      status_date,
      prod_related_feed_status)
   VALUES (
      out_related_id,
      'INVENTORY',
      in_prod_feed_id,
      in_sku_id,
      in_company_id,
      in_product_id,
      v_current_time,
      v_current_time,
      'NOT SENT');

  INSERT INTO prod_inventory_feed (
      prod_related_feed_id,
      available)
  VALUES (
      out_related_id,
      in_available);

  OUT_STATUS := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_inventory_feed;
.
/
