CREATE OR REPLACE
PROCEDURE amazon.upd_sku (
   in_company_id          IN sku.company_id%TYPE,
   in_product_id          IN sku.product_id%TYPE,
   in_live                IN sku.live%TYPE,
   in_tax_code            IN sku.tax_code%TYPE,
   in_launch_date         IN sku.launch_date%TYPE,
   in_discontinue_date    IN sku.discontinue_date%TYPE,
   in_title               IN sku.title%TYPE,
   in_brand               IN sku.brand%TYPE,
   in_description         IN sku.description%TYPE,
   in_max_order_qty       IN sku.max_order_qty%TYPE,
   in_manufacturer        IN sku.manufacturer%TYPE,
   in_gift_message        IN sku.gift_message%TYPE,
   in_cat_sub_category_id IN sku.cat_sub_category_id%TYPE,
   in_novator_id          IN sku.novator_id%TYPE,
   in_image_url           IN sku.image_url%TYPE,
   in_user_id             IN sku.LAST_UPDATE_USER_ID%TYPE,
   out_sku_id            OUT sku.sku_id%TYPE,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Verify that the sku exists in the SKU table by checking the alternate key
   values (company ID and product ID).  If it does, update the row with the
   supplied data.  If it does not, call INSERT_SKU to create a new row.

Input:
   in_company_id               varchar2
   in_product_id               varchar2
   in_live                     char
   in_tax_code                 varchar2
   in_launch_date              date
   in_discontinue_date         date
   in_title                    varchar2
   in_brand                    varchar2
   in_description              varchar2
   in_max_order_qty            number
   in_manufacturer             varchar2
   in_gift_message             varchar2
   in_cat_sub_category_id      number
   in_novator_id               varchar2
   in_image_url                varchar2
   in_user_id                  varchar2

Output:
   out_sku_id                  number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

   v_new_sku VARCHAR2(1) :='N';
   v_status VARCHAR2(1) := 'N';
   v_message VARCHAR2(500) := 'Undetermined error';

BEGIN
   DECLARE CURSOR sku_cur IS
    SELECT  SKU_ID
      FROM    SKU
      WHERE   COMPANY_ID=in_company_id AND PRODUCT_ID=in_product_id;

  BEGIN
    OPEN sku_cur;
    FETCH sku_cur
        INTO out_sku_id;
    IF sku_cur%NOTFOUND THEN
      v_new_sku := 'Y';
    END IF;
    CLOSE sku_cur;
  END;

   IF v_new_sku = 'N' THEN

      UPDATE sku SET
         live = in_live,
         tax_code = in_tax_code,
         launch_date = in_launch_date,
         discontinue_date = in_discontinue_date,
         title = in_title,
         brand = in_brand,
         description = in_description,
         max_order_qty = in_max_order_qty,
         manufacturer = in_manufacturer,
         gift_message = in_gift_message,
         cat_sub_category_id = in_cat_sub_category_id,
         novator_id = in_novator_id,
         image_url = in_image_url,
         last_update_user_id = in_user_id,
         last_update = SYSDATE
      WHERE  company_id = in_company_id
      AND    product_id = in_product_id;

      v_status := 'Y';

   ELSE

      insert_sku (
         in_company_id => in_company_id,
         in_product_id => in_product_id,
         in_live => in_live,
         in_tax_code => in_tax_code,
         in_launch_date => in_launch_date,
         in_discontinue_date => in_discontinue_date,
         in_title => in_title,
         in_brand => in_brand,
         in_description => in_description,
         in_max_order_qty => in_max_order_qty,
         in_manufacturer => in_manufacturer,
         in_gift_message => in_gift_message,
         in_cat_sub_category_id => in_cat_sub_category_id,
         in_novator_id => in_novator_id,
         in_image_url => in_image_url,
         in_user_id => in_user_id,
         out_sku_id => out_sku_id,
         out_status => v_status,
         out_message => v_message);

   END IF;

   IF v_status = 'N' THEN
      out_status := v_status;
      out_message := v_message;
   ELSE
      out_status := 'Y';
      out_message := NULL;
   END IF;

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END upd_sku;
.
/
