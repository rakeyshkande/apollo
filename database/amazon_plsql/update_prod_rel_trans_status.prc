CREATE OR REPLACE
PROCEDURE amazon.update_prod_rel_trans_status (
   in_az_transaction_id   IN prod_related_feed.az_transaction_id%TYPE,
   in_ordinal             IN NUMBER,
   in_status              IN prod_related_feed.prod_related_feed_status%TYPE,
   in_result_code         IN VARCHAR2,
   in_result_description  IN VARCHAR2,
   in_result_message_code IN NUMBER,
   out_status            OUT VARCHAR2,
   out_message           OUT VARCHAR2) IS

BEGIN

IF IN_ORDINAL=0 THEN
  UPDATE PROD_RELATED_FEED
    SET
      PROD_RELATED_FEED_STATUS=IN_STATUS,
      STATUS_DATE=SYSDATE,
      RESULT_CODE = IN_RESULT_CODE,
      RESULT_DESCRIPTION = IN_RESULT_DESCRIPTION,
      RESULT_MESSAGE_CODE = IN_RESULT_MESSAGE_CODE
    WHERE AZ_TRANSACTION_ID=in_az_transaction_id;
ELSE
  UPDATE PROD_RELATED_FEED
    SET
      PROD_RELATED_FEED_STATUS=IN_STATUS,
      STATUS_DATE=SYSDATE,
      RESULT_CODE = IN_RESULT_CODE,
      RESULT_DESCRIPTION = IN_RESULT_DESCRIPTION,
      RESULT_MESSAGE_CODE = IN_RESULT_MESSAGE_CODE
    WHERE AZ_TRANSACTION_ID=in_az_transaction_id
      AND ORDINAL_SENT=IN_ORDINAL;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END update_prod_rel_trans_status;
.
/
