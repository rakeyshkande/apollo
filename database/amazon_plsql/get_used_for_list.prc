CREATE OR REPLACE
PROCEDURE amazon.get_used_for_list (
   in_cat_sub_category_id  IN amazon.cat_marketing_index.cat_sub_category_id%TYPE,
   out_cur                OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
  Returns a cursor containing the records that belong to the passed
  CAT_SUB_CATEGORY_ID that are an IDX_TYPE of 'USED FOR'

input:
  in_cat_sub_category_id      number

output:
  out_cur                     ref cursor
------------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CUR FOR
    SELECT  CAT_MARKETING_IDX_ID,
            PARENT_IDX_ID,
            IDX_NAME
      FROM CAT_MARKETING_INDEX
      WHERE CAT_SUB_CATEGORY_ID=IN_CAT_SUB_CATEGORY_ID
        AND IDX_TYPE='USED FOR'
      ORDER BY CAT_MARKETING_IDX_ID;
END get_used_for_list;
.
/
