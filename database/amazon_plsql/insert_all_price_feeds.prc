CREATE OR REPLACE
PROCEDURE amazon.INSERT_ALL_PRICE_FEEDS (
   in_company_id IN prod_related_feed.company_id%TYPE,
   in_start_date           IN prod_price_feed.start_date%TYPE,
   in_end_date             IN prod_price_feed.end_date%TYPE,
   out_status   OUT VARCHAR2,
   out_message  OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
 This procedure will insert a record into the PROD_PRICE_FEED for every SKU that
 is live on Amazon for the passed company.

Input:
   company_id         varchar2
   start_date         date
   end_date           date

Output:
   status             Y or N
   message            varchar2

------------------------------------------------------------------------------*/

v_prod_feed_id NUMBER := null;
v_related_id NUMBER;
v_status varchar2(1);
v_message varchar2(512);
local_error EXCEPTION;

BEGIN

DECLARE CURSOR sku_cur IS
  SELECT s.SKU_ID, s.PRODUCT_ID, p.STANDARD_PRICE
    FROM AMAZON.SKU s
    JOIN FTD_APPS.PRODUCT_MASTER p ON p.PRODUCT_ID=s.PRODUCT_ID
    WHERE s.COMPANY_ID=IN_COMPANY_ID
      AND s.LIVE='Y';

BEGIN
  FOR sku_rec IN sku_cur
    LOOP
      AMAZON.insert_price_feed(
        in_sku_id =>sku_rec.sku_id,
        in_company_id => in_company_id,
        in_product_id => sku_rec.product_id,
        in_prod_feed_id => v_prod_feed_id,
        in_standard_price => sku_rec.standard_price,
        in_start_date => in_start_date,
        in_end_date => in_end_date,
        out_related_id => v_related_id,
        out_status => v_status,
        out_message => v_message
      );

      IF v_status = 'N' THEN
         RAISE local_error;
      END IF;

    END LOOP;
END;

out_status := 'Y';

EXCEPTION
  WHEN local_error THEN
    out_status := 'N';
    out_message := v_message;
  WHEN OTHERS THEN
    out_status := 'N';
    out_message := 'ERROR OCCURRED ['||sqlcode||'] '||SUBSTR(sqlerrm,1,256);

END insert_all_price_feeds;
.
/
