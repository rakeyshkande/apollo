CREATE OR REPLACE
PROCEDURE amazon.GET_SKU_BY_PRODUCT_ID (
  IN_COMPANY_ID                 IN AMAZON.SKU.COMPANY_ID%TYPE,
  IN_PRODUCT_ID                 IN AMAZON.SKU.PRODUCT_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will get the sku record for the passed id and company.  The
    id can be a product id or a novatory id.

    Shipping amount is calculated thusly: get the standard price and shipping key
   for the product from product_master.  Use these to query
   ftd_apps.shipping_key_details, looking for the largest min price that is
   less than or equal to the product's standard price.  Get the
   shipping_key_detail_id for this entry, and use this to go into the
   shipping_key_costs.  Find the shipping cost, for the corresponding
   shipping_key_detail_id, where shipping_method_id is 2F.

   Note that standard product prices can be zero or null, but the min costs
   in shipping_key_details are always .01.  So you have to account separately
   for the case of zero or null standard product price.

Input:
        company id                      varchar2
        product_id                      varchar2

Output:
        out                             ref cursor
-----------------------------------------------------------------------------*/

v_product_id varchar(10);
v_tax_code varchar(16);
v_new boolean;
v_product_type ftd_apps.product_master.product_type%TYPE;
v_standard_price ftd_apps.PRODUCT_MASTER.STANDARD_PRICE%TYPE;
v_shipping_key ftd_apps.PRODUCT_MASTER.SHIPPING_KEY%TYPE;
v_shipping_cost number := null;
v_shipping_detail_id   ftd_apps.shipping_key_details.shipping_detail_id%TYPE;

begin

--Check to see if the sku exits
DECLARE CURSOR sku_cur IS
  SELECT  PRODUCT_ID
  FROM AMAZON.SKU
    WHERE COMPANY_ID = IN_COMPANY_ID
      AND (PRODUCT_ID = IN_PRODUCT_ID OR NOVATOR_ID = IN_PRODUCT_ID);

BEGIN
  OPEN sku_cur;
  FETCH sku_cur
        INTO    v_product_id;
  IF sku_cur%NOTFOUND THEN
    v_new :=true;
  ELSE
    v_new :=false;
  END IF;
  CLOSE sku_cur;

  DECLARE CURSOR prod_cur IS
    SELECT NO_TAX_FLAG, PRODUCT_TYPE, STANDARD_PRICE, SHIPPING_KEY
      FROM FTD_APPS.PRODUCT_MASTER
      WHERE PRODUCT_ID = IN_PRODUCT_ID;

    BEGIN
      OPEN prod_cur;
        FETCH prod_cur INTO v_tax_code, v_product_type, v_standard_price, v_shipping_key;
      CLOSE prod_cur;
    END;

    IF v_tax_code='Y' THEN
      v_tax_code := 'A_GEN_NOTAX';
    ELSE
      v_tax_code := 'A_GEN_TAX';
    END IF;

    IF v_product_type = 'SPEGFT' THEN
      IF v_standard_price = 0 OR v_standard_price IS NULL THEN
         SELECT MIN(shipping_cost)
         INTO   v_shipping_cost
         FROM   ftd_apps.shipping_key_costs
         WHERE  shipping_method_id = '2F';

      ELSE

         SELECT shipping_detail_id
         INTO   v_shipping_detail_id
         FROM   ftd_apps.shipping_key_details
         WHERE  shipping_key_id = v_shipping_key
         AND    min_price =
               (SELECT MAX(min_price)
                FROM   ftd_apps.shipping_key_details
                WHERE  shipping_key_id = v_shipping_key
                AND    min_price <= v_standard_price);

         SELECT shipping_cost
         INTO   v_shipping_cost
         FROM   ftd_apps.shipping_key_costs
         WHERE  shipping_method_id = '2F'
         AND    shipping_key_detail_id = v_shipping_detail_id;

      END IF;
   END IF;

  IF v_new=true THEN
    OPEN OUT_CUR FOR
      SELECT
        null,
        IN_COMPANY_ID,
        p.PRODUCT_ID,
        p.NOVATOR_ID,
        'N',
        v_tax_code,
        p.EXCEPTION_START_DATE,
        p.EXCEPTION_END_DATE,
        p.NOVATOR_NAME,
        null,
        p.LONG_DESCRIPTION,
        1,
        NULL,
        'Y',
        null,
        p.STANDARD_PRICE,
        null,
        p.EXCEPTION_START_DATE,
        p.EXCEPTION_END_DATE,
        null,
        v_shipping_cost
      FROM FTD_APPS.PRODUCT_MASTER p
      WHERE p.product_id=IN_PRODUCT_ID OR p.novator_id=IN_PRODUCT_ID;
  ELSE
    OPEN OUT_CUR FOR
      SELECT
        s.SKU_ID,
        s.COMPANY_ID,
        s.PRODUCT_ID,
        s.NOVATOR_ID,
        s.LIVE,
        s.TAX_CODE,
        s.LAUNCH_DATE,
        s.DISCONTINUE_DATE,
        s.TITLE,
        s.BRAND,
        s.DESCRIPTION,
        s.MAX_ORDER_QTY,
        s.MANUFACTURER,
        s.GIFT_MESSAGE,
        s.CAT_SUB_CATEGORY_ID,
        p.STANDARD_PRICE,
        s.IMAGE_URL,
        p.EXCEPTION_START_DATE,
        p.EXCEPTION_END_DATE,
        c.CAT_CATEGORY_CODE,
        v_shipping_cost
      FROM AMAZON.SKU s
      JOIN FTD_APPS.PRODUCT_MASTER p ON p.PRODUCT_ID = s.PRODUCT_ID
      JOIN CAT_SUB_CATEGORY c ON c.CAT_SUB_CATEGORY_ID = s.CAT_SUB_CATEGORY_ID
      WHERE s.COMPANY_ID = IN_COMPANY_ID
        AND (s.PRODUCT_ID = v_product_id);
  END IF;
END;

end GET_SKU_BY_PRODUCT_ID;
.
/
