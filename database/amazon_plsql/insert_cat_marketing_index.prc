CREATE OR REPLACE
PROCEDURE amazon.insert_cat_marketing_index (
   in_cat_sub_category_id  IN amazon.cat_marketing_index.cat_sub_category_id%TYPE,
   in_parent_idx_id        IN amazon.cat_marketing_index.parent_idx_id%TYPE,
   in_idx_type             IN amazon.cat_marketing_index.idx_type%TYPE,
   in_idx_name             IN amazon.cat_marketing_index.idx_name%TYPE,
   out_id                 OUT NUMERIC,
   out_status             OUT VARCHAR2,
   out_message            OUT VARCHAR2) IS

/*------------------------------------------------------------------------------
   Insert a row into cat_marketing_index.
   This table has a self-referential foreign key.  parent_idx_id refers to
   cat_marketing_idx_id.  So there must be at least one row where parent_idx_id
   is null.  This program will not validate this constraint; in other words,
   if the data violates the constraint, it will not insert, but will return
   status 'N' with an ORA-02291 message.  The key to the inserted record is
   returned.

Input:
   in_cat_sub_category_id      number
   in_parent_idx_id            number
   in_idx_type                 varchar2
   in_idx_name                 varchar2

Output:
   out_id                      number
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

BEGIN
   out_id := keygen('CAT_MARKETING_INDEX');

   INSERT INTO cat_marketing_index (
      cat_marketing_idx_id,
      cat_sub_category_id,
      parent_idx_id,
      idx_type,
      idx_name)
   VALUES (
      out_id,
      in_cat_sub_category_id,
      in_parent_idx_id,
      in_idx_type,
      in_idx_name);

   out_status := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END insert_cat_marketing_index;
.
/
