CREATE OR REPLACE
PROCEDURE amazon.get_sku_bullet_points (
   in_sku_id     IN sku_bullet_point.sku_id%TYPE,
   out_cur      OUT types.ref_cursor) IS

/*------------------------------------------------------------------------------
   Return the bullet points for the specified SKU.

Input:
   in_sku_id                   number

Output:
   ref cursor containing
      sku_bullet_point_id      number
      bullet_point             varchar2

------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cur FOR
      SELECT sku_bullet_point_id,
             bullet_point
      FROM   sku_bullet_point
      WHERE  sku_id = in_sku_id;

END get_sku_bullet_points;
.
/
