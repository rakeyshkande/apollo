CREATE OR REPLACE
PROCEDURE amazon.GET_ORIGIN_FROM_SOURCE (
  IN_SOURCE_CODE            IN AZ_SOURCE.SOURCE_CODE%TYPE,
  OUT_ORIGIN               OUT VARCHAR2,
  OUT_STATUS               OUT VARCHAR2,
  OUT_MESSAGE              OUT VARCHAR2
)
as

/*------------------------------------------------------------------------------
   Look up the origin to determine if it is an Amazon source code.  If it is,
   then return the origin.  If it is not, then return null in out_origin.  Only
   return an 'N' in status on database errors.

Input:
   in_source_code              varchar2

Output:
   out_origin                  varchar2
   out_status                  varchar2
   out_message                 varchar2
------------------------------------------------------------------------------*/

begin
  --Look up the SOURCE CODE
  DECLARE CURSOR origin_cur IS
    SELECT  ORIGIN
      FROM    AZ_SOURCE
      WHERE   SOURCE_CODE = IN_SOURCE_CODE;

  BEGIN
    OPEN origin_cur;
    FETCH origin_cur
      INTO OUT_ORIGIN;
    IF origin_cur%NOTFOUND THEN
      --Not am Amazon source code
      OUT_ORIGIN := null;
    END IF;
    CLOSE origin_cur;
  END;

  OUT_STATUS := 'Y';

EXCEPTION
   WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

end GET_ORIGIN_FROM_SOURCE;
.
/
