CREATE OR REPLACE
PROCEDURE amazon.GET_CATEGORY_NAMES (
   in_department_code      IN amazon.cat_sub_category.cat_category_code%TYPE,
   out_cur                OUT types.ref_cursor) IS

BEGIN

  OPEN OUT_CUR FOR
    SELECT CAT_SUB_CATEGORY_ID, SUB_CATEGORY_NAME
      FROM CAT_SUB_CATEGORY
      WHERE CAT_CATEGORY_CODE=in_department_code
      ORDER BY SUB_CATEGORY_NAME;

END get_category_names;
.
/
