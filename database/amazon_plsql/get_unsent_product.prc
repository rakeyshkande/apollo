CREATE OR REPLACE
PROCEDURE amazon.GET_UNSENT_PRODUCT (
  IN_COMPANY_ID                  IN AMAZON.SKU.COMPANY_ID%TYPE,
  IN_LIVE                        IN AMAZON.PROD_FEED.LIVE%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    This procedure will retrieve all AMAZON.PRODUCT_FEED records for the passed
    company_id with a status of NOT_SENT.

Input:
        in_company_id                     varchar2
        in_live                           varchar2

Output:
        out                                    ref cursor
-----------------------------------------------------------------------------*/

BEGIN
OPEN OUT_CUR FOR
	SELECT
		p.PROD_RELATED_FEED_ID,
		s.COMPANY_ID,
		s.PRODUCT_ID,
		p.LIVE,
		p.TAX_CODE,
		p.LAUNCH_DATE_PST,
		p.DISCONTINUE_DATE_PST,
		p.TITLE,
		p.BRAND,
		p.DESCRIPTION,
		p.MANUFACTURER,
		p.MAX_ORDER_QTY,
		p.GIFT_MESSAGE,
		p.CAT_SUB_CATEGORY_ID,
    c.CAT_CATEGORY_CODE,
    cc.PRODUCT_TYPE
	FROM AMAZON.PROD_FEED p
	JOIN AMAZON.PROD_RELATED_FEED pr on pr.PROD_RELATED_FEED_ID = p.PROD_RELATED_FEED_ID
	JOIN AMAZON.SKU s on s.SKU_ID = pr.SKU_ID
  JOIN AMAZON.CAT_SUB_CATEGORY c on c.CAT_SUB_CATEGORY_ID = p.CAT_SUB_CATEGORY_ID
  JOIN AMAZON.CAT_CATEGORY cc on cc.CAT_CATEGORY_CODE = c.CAT_CATEGORY_CODE
	WHERE pr.PROD_RELATED_FEED_STATUS='NOT SENT'
            AND s.COMPANY_ID=IN_COMPANY_ID
            AND p.LIVE=IN_LIVE
	ORDER BY p.PROD_RELATED_FEED_ID;
END GET_UNSENT_PRODUCT;
.
/
