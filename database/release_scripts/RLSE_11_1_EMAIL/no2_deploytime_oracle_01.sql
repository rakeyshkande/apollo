------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                         4/6/2017  --------
-- User  user story 10947   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (  'EMAIL_CONFIG', 'SCHEDULED_FOR_DELIVERY_EMAIL_FLAG', 'Y', 'TFS_10948', sysdate, 'TFS_10948', sysdate, 'Flag indicating if the Scheduled For Delivery emails should be sent.');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (  'EMAIL_CONFIG', 'SCHEDULED_FOR_DELIVERY_EMAIL_TIME', '0600', 'TFS_10948', sysdate, 'TFS_10948', sysdate, 'The time that the Scheduled For Delivery emails should be sent (in the recipient''s time zone).');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (  'EMAIL_CONFIG', 'SCHEDULED_FOR_DELIVERY_STOCK_EMAIL_ID', 'replace me', 'TFS_10948', sysdate, 'TFS_10948', sysdate, 'The CLEAN.STOCK_EMAIL.STOCK_EMAIL_ID value for Scheduled For Delivery emails.');

grant execute on CLEAN.EMAIL_PKG to OSP ;

grant select on PAS.PAS_TIMEZONE_DT to CLEAN;


------------------------------------------------------------------------------------
-- End requested by Tim Schmig                         4/6/2017  --------
-- User  user story 10947   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- BEGIN requested by Tim Schmig                         4/11/2017  --------
-- User  user story 11323   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

update clean.message_tokens
set token_type = 'value',
    token_value = '/root/CUSTOMERS/CUSTOMER/cur_date/text()'
where token_id = 'cur.date';


------------------------------------------------------------------------------------
-- End requested by Tim Schmig                         4/11/2017  --------
-- User  user story 11323   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                               4/13/2018  --------
-- User  task 11493   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------
Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','BATCH_COUNT','30','US_10155',to_date('13-APR-18','DD-MON-RR'),'US_10155',to_date('13-APR-18','DD-MON-RR'),'This is the configured batch of feeds to be fetched to send to Fees Micro Service');

Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','FEED_TYPE_LIST','serviceFeeOverride, serviceFee, SHIPPING_KEY_FEED, SOURCE_FEED','US_10155',to_date('13-APR-18','DD-MON-RR'),'US_10155',to_date('13-APR-18','DD-MON-RR'),'These are the configured Feed Types for which Fees feeds will be sent to Fees Micro Service');

  Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','SOURCE_CODES','350, 552, 14441, 16737','US_10155',to_date('11-APR-18','DD-MON-RR'),'US_10155',to_date('11-APR-18','DD-MON-RR'),'These are the configured source code values for which source codes feeds will be sent to Fees Micro Service');


ALTER TABLE ftd_apps.fees_feeds
  MODIFY status VARCHAR2(20);

drop trigger ftd_apps.FEES_FEED_TRIG_$;

------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                               4/13/2018  --------
-- User task 11493   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------

