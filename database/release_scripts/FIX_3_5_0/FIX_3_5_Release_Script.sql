set define off
--------------------------------
-- Fix_3_5_Release_Script.sql --
--------------------------------

--********************************************
-- PRE-DEPLOY SECTION
--********************************************


create table ftd_apps.price_pay_vendor_update
   (   price_pay_vendor_batch_id       number(11)         not null,
       spreadsheet_row_num              number(7)          not null,
       product_id                       varchar2(2000),
       vendor_id                        varchar2(2000),
       vendor_cost                      varchar2(2000),
       vendor_cost_variance_pct         number(7,2),
       standard_price                   varchar2(2000),
       standard_price_variance_pct      number(7,2),
       deluxe_price                     varchar2(2000),
       deluxe_price_variance_pct        number(7,2),
       premium_price                    varchar2(2000),
       premium_price_variance_pct       number(7,2),
       processed_flag                   varchar2(1)    DEFAULT 'N' NOT NULL,
       error_msg_txt                    varchar2(4000),
       created_by                       varchar2(100)      NOT NULL,
       created_on                       date               NOT NULL,
       updated_by                       varchar2(100)      NOT NULL,
       updated_on                       date               NOT NULL,
    CONSTRAINT price_pay_vendor_update_pk PRIMARY KEY(price_pay_vendor_batch_id, spreadsheet_row_num) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT price_pay_vendor_update_ck1   CHECK(processed_flag IN('Y','N'))
   )
 TABLESPACE ftd_apps_data;

comment on table  ftd_apps.price_pay_vendor_update is 'Store spreadsheet data from merchandising which is to be sent to Novator';
comment on column ftd_apps.price_pay_vendor_update.price_pay_vendor_batch_id is 'Sequence generated batch id - combined with spreadsheet_row_num makes the PK';
comment on column ftd_apps.price_pay_vendor_update.spreadsheet_row_num       is 'Row number on spreadsheet from Merchandising the data values come from - combined with price_pay_vendor_batch_id makes up the PK';
comment on column ftd_apps.price_pay_vendor_update.product_id                is 'Product id of information to change';           
comment on column ftd_apps.price_pay_vendor_update.vendor_id                 is 'Vendor id of information to change (if applicable)';           
comment on column ftd_apps.price_pay_vendor_update.vendor_cost               is 'Vendor cost of information to change (if applicable)';           
comment on column ftd_apps.price_pay_vendor_update.vendor_cost_variance_pct  is 'Vendor cost variance percentage for approval screen';           
comment on column ftd_apps.price_pay_vendor_update.standard_price            is 'Standard price change';       
comment on column ftd_apps.price_pay_vendor_update.standard_price_variance_pct is 'Standard price change variance percentage for approval screen';       
comment on column ftd_apps.price_pay_vendor_update.deluxe_price              is 'Deluxe price change';       
comment on column ftd_apps.price_pay_vendor_update.deluxe_price_variance_pct is 'Deluxe price change variance percentage for approval screen';       
comment on column ftd_apps.price_pay_vendor_update.premium_price             is 'Premium price change';       
comment on column ftd_apps.price_pay_vendor_update.premium_price_variance_pct is 'Premium price change percentage for approval screen';       
comment on column ftd_apps.price_pay_vendor_update.processed_flag            is 'Denotes if the data has been successfully processed and accepted by Novator';       
comment on column ftd_apps.price_pay_vendor_update.error_msg_txt             is 'Stores any error encountered during attempted updated';       
comment on column ftd_apps.price_pay_vendor_update.created_by                is 'User/Process which created the initial row';       
comment on column ftd_apps.price_pay_vendor_update.created_on                is 'Date/time the initial row was created';       
comment on column ftd_apps.price_pay_vendor_update.updated_by                is 'User/Process which last updated the row';       
comment on column ftd_apps.price_pay_vendor_update.updated_on                is 'Date/time the row was last updated';       


CREATE SEQUENCE ftd_apps.price_pay_vendor_batch_sq 
    START WITH 1  INCREMENT BY 1  MINVALUE 1 MAXVALUE 99999999999 CYCLE CACHE 50;


grant select on ftd_apps.PRICE_PAY_VENDOR_UPDATE to rpt;
 
 
--
-- Defect 5373
insert into quartz_schema.groups (group_id,description,active_flag)
values ('OP','Order Processing', 'Y');

insert into quartz_schema.pipeline (pipeline_id, group_id, description,tool_tip,default_schedule,default_payload,
force_default_payload,queue_name,class_code,active_flag)
values ('FLORIST_FORWARD_UPDATE','OP','Update florist forward flag',
'This calls FTD_APPS.FLORIST_MAINT_PKG.UPDATE_FLORIST_N_INSERT_CMMNTS.','',
'<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>FLORIST_FORWARD_UPDATE</event-name><context>VENUS</context><payload></payload></event>','N','OJMS.EM_VENUS','SimpleJmsJob','Y');

insert into events.events(event_name,context_name,description,active)
values('FLORIST_FORWARD_UPDATE','VENUS','Updates florist forward flag.','Y');


--********************************************
-- END PRE-DEPLOY SECTION, START OF DEPLOY
--********************************************


alter table  clean.aafes_billing_details add  req_amt  number(12,2);



--
-- Defect 5373
insert into quartz_schema.groups (group_id,description,active_flag)
values ('OP','Order Processing', 'Y');

insert into quartz_schema.pipeline (pipeline_id, group_id, description,tool_tip,default_schedule,default_payload,force_default_payload,queue_name,class_code,active_flag)
values ('FLORIST_FORWARD_UPDATE','OP','Update florist forward flag','This calls FTD_APPS.FLORIST_MAINT_PKG.UPDATE_FLORIST_N_INSERT_CMMNTS.','','<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>FLORIST_FORWARD_UPDATE</event-name><context>VENUS</context><payload></payload></event>','N','OJMS.EM_VENUS','SimpleJmsJob','Y');

insert into events.events(event_name,context_name,description,active)
values('FLORIST_FORWARD_UPDATE','VENUS','Updates florist forward flag.','Y');





--
-- Defect 5626
--
declare	
 content_detail_id number;
 content_master_id number;
 filter_token_id  number;
 filter_type_id number;
begin
select ftd_apps.content_filter_sq.nextval  into filter_token_id from dual ; select ftd_apps.content_filter_sq.nextval  into filter_type_id from dual;
	
insert into FTD_APPS.CONTENT_FILTER
(CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (filter_token_id, 'STOCK_EMAIL_TOKEN', 'defect 5626', sysdate, 'defect 5626', sysdate); insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (filter_type_id, 'STOCK_EMAIL_TYPE', 'defect 5626', sysdate, 'defect 5626', sysdate);

select ftd_apps.content_master_sq.nextval into content_master_id from dual;

insert into FTD_APPS.CONTENT_MASTER
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values 
(content_master_id, 'STOCK_EMAIL' , 'TOKEN', 'Contains the replaced tokens in the stock email messages', filter_type_id, filter_token_id, 'defect 5626', sysdate, 'defect 5626', sysdate);	

select ftd_apps.content_detail_sq.nextval into content_detail_id from dual;

insert into FTD_APPS.CONTENT_DETAIL
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (content_detail_id, content_master_id, 'HTML', 'FTD.COM_FOOTER_BANNER' , 'FTD.COM_FOOTER_BANNER CONTENT HTML', 'defect 5626', sysdate, 'defect 5626', sysdate);
		
		
select ftd_apps.content_detail_sq.nextval into content_detail_id from dual;

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(content_detail_id, content_master_id, 'TEXT', 'FTD.COM_FOOTER_BANNER' , 'FTD.COM_FOOTER_BANNER CONTENT TEXT', 'defect 5626', sysdate, 'defect 5626', sysdate);

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('MESSAGE_GENERATOR_CONFIG', 'SEND_CONTENT_TYPE', 'MP', 'defect 5626', sysdate, 'defect 5626', sysdate, 'This value controls if text to multipart emails are sent from message_generater (stock emails).  Set it to TXT to do text emails, MP for multipart emails.');

end;
/



-- Defect #5542

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','MIN_AMT_FOR_CODIFY_ALL','30','SYS',sysdate,'SYS',sysdate, 
'New florists with minimum order amounts less than this value will be automatically codified with MIN_AMT_CODIFY_ALL_CODIFICATIONS.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','MIN_AMT_CODIFY_ALL_CODIFICATIONS','''VPR'',''VPF'',''.VPW''','SYS',sysdate,'SYS',sysdate, 
'New florists with minimum order amounts less than MIN_AMT_FOR_CODIFY_ALL will be automatically codified with these codifications ' ||
'(Note each codification must be within single quotes).');




--
-- Defect 5626 - **** QA ONLY ****
--
declare 
 content_detail_id number;
 content_master_id number;
 filter_token_id  number;
 filter_type_id number;

begin
   select ftd_apps.content_filter_sq.nextval  into filter_token_id from dual ;
   select ftd_apps.content_filter_sq.nextval  into filter_type_id from dual;

   insert into FTD_APPS.CONTENT_FILTER
      (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
   values
      (filter_token_id, 'STOCK_EMAIL_TOKEN', 'defect 5626', sysdate, 'defect 5626', sysdate);

   insert into FTD_APPS.CONTENT_FILTER
      (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
   values
      (filter_type_id, 'STOCK_EMAIL_TYPE', 'defect 5626', sysdate, 'defect 5626', sysdate);

   select ftd_apps.content_master_sq.nextval into content_master_id from dual;

   insert into FTD_APPS.CONTENT_MASTER
      (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
   values
      (content_master_id, 'STOCK_EMAIL' , 'TOKEN', 'Contains the replaced tokens in the stock email messages', filter_type_id, filter_token_id, 'defect 5626', sysdate, 'defect 5626', sysdate);       

   select ftd_apps.content_detail_sq.nextval into content_detail_id from dual;

   insert into FTD_APPS.CONTENT_DETAIL
      (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
   values
      (content_detail_id, content_master_id, 'HTML', 'FTD.COM_FOOTER_BANNER' , 'FTD.COM_FOOTER_BANNER CONTENT HTML', 'defect 5626', sysdate, 'defect 5626', sysdate);

   select ftd_apps.content_detail_sq.nextval into content_detail_id from dual;

   insert into FTD_APPS.CONTENT_DETAIL
      (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
   values
      (content_detail_id, content_master_id, 'TEXT', 'FTD.COM_FOOTER_BANNER' , 'FTD.COM_FOOTER_BANNER CONTENT TEXT', 'defect 5626', sysdate, 'defect 5626', sysdate);

   insert into FRP.GLOBAL_PARMS
      (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
   values
      ('MESSAGE_GENERATOR_CONFIG', 'SEND_CONTENT_TYPE', 'MP', 'defect 5626', sysdate, 'defect 5626', sysdate, 'This value controls if text to multipart emails are sent from message_generater (stock emails).  Set it to TXT to do text emails, MP for multipart emails.');

end;
/
--
-- END Defect 5626 - QA ONLY
--



--
-- Defect 3060
-- NOTE - The following was deployed to dev35 - Confirm it needs to go to QA prior to deploy there
--

alter table ftd_apps.vendor_product_override 
    rename column override_date to delivery_override_date;
    
alter table ftd_apps.vendor_product_override
    drop constraint vendor_product_override_pk;

grant references on venus.sds_ship_method_xref to ftd_apps;
 
alter table ftd_apps.vendor_product_override
   add (sds_ship_method     varchar2(100),
        CONSTRAINT vendor_product_override_fk3  FOREIGN KEY(sds_ship_method) REFERENCES venus.sds_ship_method_xref(sds_ship_method)
       );

comment on column ftd_apps.vendor_product_override.sds_ship_method is 'ship method for this vendor/product/carrier combination';

create index ftd_apps.vendor_product_override_n3 
     on ftd_apps.vendor_product_override(sds_ship_method) tablespace ftd_apps_indx_vk;

update ftd_apps.vendor_product_override
    set sds_ship_method = 'GROUND RES' where carrier_id = 'FEDEX';
   
update ftd_apps.vendor_product_override
    set sds_ship_method = 'UPSGR RES' where carrier_id = 'UPS';

alter table ftd_apps.vendor_product_override add constraint vendor_product_override_pk 
       primary key (vendor_id, product_subcode_id, delivery_override_date, carrier_id, sds_ship_method) 
       using index tablespace ftd_apps_indx_vk;
 
 
CREATE TABLE FTD_APPS.VENDOR_PRODUCT_OVERRIDE$ (
          vendor_id                   varchar2(5),
          product_subcode_id          varchar2(10),
          delivery_override_date      date,
          carrier_id                  varchar2(10),
          sds_ship_method             varchar2(100),
          OPERATION$                  VARCHAR2(7)      not null,
          TIMESTAMP$                  TIMESTAMP        not null
        )
 TABLESPACE ftd_apps_DATA;
 


--
-- Defect 4715
As OJMS:
exec dbms_aqadm.STOP_QUEUE('AAFES_BILLING')
exec dbms_aqadm.DROP_QUEUE('AAFES_BILLING')
exec dbms_aqadm.DROP_QUEUE_TABLE('AAFES_BILLING',TRUE)

--
-- Defect 5623
delete from clean.order_hold
where order_detail_id in
(select distinct(od.order_detail_id)
    from clean.order_hold oh
join clean.order_details od
on oh.order_detail_id=od.order_detail_id
JOIN CLEAN.PAYMENTS P
ON P.ORDER_GUID=OD.ORDER_GUID
where oh.reason='RE-AUTH'
AND P.AUTH_NUMBER IS NOT NULL
and od.order_disp_code in ('Processed','Shipped','Printed'));

-- Defect 5261
-- Add new columns
-- 
alter table pas.pas_product_dt add (ship_method_florist varchar2(1) default 'N');
alter table pas.pas_product_dt add (ship_method_carrier varchar2(1) default 'N');
COMMENT ON COLUMN pas.pas_product_dt.ship_method_florist IS 'Indicates if product is available for florist delivery';
COMMENT ON COLUMN pas.pas_product_dt.ship_method_carrier IS 'Indicates if product is available for carrier delivery';

-- Update new columns to correct value
-- 
update pas.pas_product_dt ppd set ppd.ship_method_florist = 
       (select pm.ship_method_florist from ftd_apps.product_master pm where pm.product_id = ppd.product_id);
update pas.pas_product_dt ppd set ppd.ship_method_carrier = 
       (select pm.ship_method_carrier from ftd_apps.product_master pm where pm.product_id = ppd.product_id);




-------------------------------------------
-- DEFECT 4894
-------------------------------------------
--
-- QA ONLY
--
declare
begin

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by)
    values (
    'Price Pay Vendor Link', 'Order Proc', 'Controls access to the Price/Pay Vendor Update link', sysdate, sysdate, 'Defect 4894');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
    values
    ('Merch Level 2', 'Price Pay Vendor Link', 'Order Proc', 'View', sysdate, sysdate, 'Defect 4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_PRICE_VARIANCE_THRESHOLD',
    '50',
    'The allowable variance percentage between the existing cost/price and the new cost/price.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_UPDATE_EXCEPTIONS_ALLOWED',
    '4',
    'The allowable number of update exception before the process will automatically terminate.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_EMAIL_DISTRIBUTION_LIST',
    'tschmig@ftdi.com, mkacprowicz@ftdi.com',
    'A comma-separated list of email recipients for the success and error reports.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_SUCCESS_REPORT_ID',
    '100004',
    'The report Id of the Success Report (RPT.REPORT.REPORT_ID).',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_ERROR_REPORT_ID',
    '100005',
    'The report Id of the Error Report (RPT.REPORT.REPORT_ID).',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

insert into rpt.report
    (report_id, name, description, report_file_prefix, report_type, report_output_type,
        report_category, server_name, holiday_indicator, status, created_on, created_by,
        updated_on, updated_by, oracle_rdf, acl_name, retention_days, run_time_offset,
        end_of_day_required, schedule_parm_value)
    values (
    100004,
    'Price Pay Vendor Success Report',
    'Report of items successfully processed by the Price/Pay Vendor Update tool.',
    'ppv',
    'S',
    'Gen',
    'Merch',
    'http://neon.ftdi.com:7778/reports/rwservlet?',
    'Y',
    'Active',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894',
    'MER12_Price_Pay_Vendor_Success.rdf',
    'BaseAcctReportAccess',
    10,
    120,
    'N',
    'reportEmailExcel');

insert into rpt.report
    (report_id, name, description, report_file_prefix, report_type, report_output_type,
        report_category, server_name, holiday_indicator, status, created_on, created_by,
        updated_on, updated_by, oracle_rdf, acl_name, retention_days, run_time_offset,
        end_of_day_required, schedule_parm_value)
    values (
    100005,
    'Price Pay Vendor Error Report',
    'Report of items not processed by the Price/Pay Vendor Update tool.',
    'ppv',
    'S',
    'Gen',
    'Merch',
    'http://neon.ftdi.com:7778/reports/rwservlet?',
    'Y',
    'Active',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894',
    'MER13_Price_Pay_Vendor_Error.rdf',
    'BaseAcctReportAccess',
    10,
    120,
    'N',
    'reportEmailExcel');

end;
/

--
-- 4894 - P R O D U C T I O N
--

declare
begin

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by)
    values (
    'Price Pay Vendor Link', 'Order Proc', 'Controls access to the Price/Pay Vendor Update link', sysdate, sysdate, 'Defect 4894');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
    values
    ('Merch Level 2', 'Price Pay Vendor Link', 'Order Proc', 'View', sysdate, sysdate, 'Defect 4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_PRICE_VARIANCE_THRESHOLD',
    '50',
    'The allowable variance percentage between the existing cost/price and the new cost/price.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_UPDATE_EXCEPTIONS_ALLOWED',
    '4',
    'The allowable number of update exception before the process will automatically terminate.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_EMAIL_DISTRIBUTION_LIST',
    'kkremer@ftdi.com, agreen@ftdi.com, mtower@ftdi.com, shuff@ftdi.com',
    'A comma-separated list of email recipients for the success and error reports.',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_SUCCESS_REPORT_ID',
    '100004',
    'The report Id of the Success Report (RPT.REPORT.REPORT_ID).',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'PPVU_ERROR_REPORT_ID',
    '100005',
    'The report Id of the Error Report (RPT.REPORT.REPORT_ID).',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894');

insert into rpt.report
    (report_id, name, description, report_file_prefix, report_type, report_output_type,
        report_category, server_name, holiday_indicator, status, created_on, created_by,
        updated_on, updated_by, oracle_rdf, acl_name, retention_days, run_time_offset,
        end_of_day_required, schedule_parm_value)
    values (
    100004,
    'Price Pay Vendor Success Report',
    'Report of items successfully processed by the Price/Pay Vendor Update tool.',
    'ppv',
    'S',
    'Gen',
    'Merch',
    'http://apolloreports.ftdi.com/reports/rwservlet?',
    'Y',
    'Active',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894',
    'MER12_Price_Pay_Vendor_Success.rdf',
    'BaseAcctReportAccess',
    10,
    120,
    'N',
    'reportEmailExcel');

insert into rpt.report
    (report_id, name, description, report_file_prefix, report_type, report_output_type,
        report_category, server_name, holiday_indicator, status, created_on, created_by,
        updated_on, updated_by, oracle_rdf, acl_name, retention_days, run_time_offset,
        end_of_day_required, schedule_parm_value)
    values (
    100005,
    'Price Pay Vendor Error Report',
    'Report of items not processed by the Price/Pay Vendor Update tool.',
    'ppv',
    'S',
    'Gen',
    'Merch',
    'http://apolloreports.ftdi.com/reports/rwservlet?',
    'Y',
    'Active',
    sysdate,
    'Defect_4894',
    sysdate,
    'Defect_4894',
    'MER13_Price_Pay_Vendor_Error.rdf',
    'BaseAcctReportAccess',
    10,
    120,
    'N',
    'reportEmailExcel');

end;
/

------------------------------------
-- END 4894
------------------------------------


























----
---- END Fix 3.5 Release Script
----
