------------------------------------------------------------------------------------
-- Begin requested by Rose 2018/12/28  ---------------------------------------------
-- CAL-10753 (mwoytus)             -------------------------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SOURCE_CODES_ALLOWED', '80888,80889,79000,79000P', 'CAL-10753', sysdate, 'CAL-10753', sysdate,
'Comma separated list of source codes that are allowed to call EFA Service');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SOURCE_CODE_CHECK', 'Y', 'CAL-10753', sysdate, 'CAL-10753', sysdate,
'If Y, then call to EFA Service is retricted by source code');

------------------------------------------------------------------------------------
-- End   requested by Rose 2018/12/28  ---------------------------------------------
-- CAL-10753 (mwoytus)             -------------------------------------------------
------------------------------------------------------------------------------------

