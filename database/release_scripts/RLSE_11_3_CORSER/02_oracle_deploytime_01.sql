
------------------------------------------------------------------------------------
-- Begin requested by Manasa Salla 2018/12/28  -------------------------------------
--  Backlog 10403                  -------------------------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'LDR_PRODUCT_PRICE_PERCENT', '73', 'CAL-10755', sysdate, 'CAL-10755', sysdate,'Price percent for domestic retrans orders that has to be displayed on LDR page');

------------------------------------------------------------------------------------
-- End requested by Manasa Salla 2018/12/28  ---------------------------------------
--  Backlog 10403                  -------------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk 2019/01/04 ----------------------------------------
-- CAL-10756LG                     -------------------------------------------------
------------------------------------------------------------------------------------
-- THIS IS A GOLDEN GATE CHANGE

alter table clean.queue add (codification_id varchar2(5));
alter table clean.queue$ add (codification_id varchar2(5));

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk 2019/01/04 ----------------------------------------
-- CAL-10756LG                     -------------------------------------------------
------------------------------------------------------------------------------------

