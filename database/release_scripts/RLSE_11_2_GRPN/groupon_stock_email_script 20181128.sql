insert into CLEAN.MESSAGE_TOKENS (TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE) 
values ('phone4', 'ALL', 'text', '1-800-SEND-FTD (1-800-736-3383)', null); 

--*************************************************
-- Groupon COM Stock Emails
--*************************************************


--DCONSYS - Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DCONSYS - Groupon',
'Dear ~cust.fname~,
 
Thank you for your recent FTD purchase on Groupon.  We are pleased to inform you that the delivery of your order has been completed as scheduled.
 
Thank you for choosing FTD.
 
If you have any questions, please visit us at https://www.proflowers.com/customerservice

If you have any questions about your order, please contact FTD via ~phone4~.
',
'Confirmation of Delivery',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'Y',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'DCONSYS - Groupon';

--PARTIAL REFUND.Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'PARTIAL REFUND.Groupon',
'Thank you for contacting Customer Support.

We''ve requested that Groupon process a refund in the amount of ~fref.amt~ as Groupon Bucks. 

You will be notified within 3 business days when these funds are available for use.

Sincerely,
~csr.fname~

Contact us
Email us via this link: ~cslink~
Phone: ~phone~
',
'Your Groupon order refund request has been processed',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'PARTIAL REFUND.Groupon';

--MISSED CALL.Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSED CALL.Groupon',
'Thank you for your recent FTD purchase on Groupon.

Please know that FTD attempted to contact you on the telephone number on record regarding the status of your order. Please contact FTD via ~phone4~ at your convenience. 

We look forward to hearing from you. 

Sincerely,
FTD Customer Service
',
'Sorry we missed you',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'MISSED CALL.Groupon';


--ORDER INQUIRY.Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'ORDER INQUIRY.Groupon',
'Thank you for your inquiry regarding your recent order. We are actively working toward an answer for you and will contact you upon completion. We will respond as soon as possible. 

This is an automated response. We will not receive replies to this email.

Thank you for shopping with Groupon and FTD.

Sincerely,
FTD Customer Service
Order number: [~fnordernum~]
',
'Order Inquiry',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'ORDER INQUIRY.Groupon';


--TRACKING NUMBER.Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER.Groupon',
'This email confirms that your order [~fnordernum~] has shipped.  The ~venus.carrier_name~ tracking number is ~venus.tracking_number~.  To track the progress of your order, you can go to ~venus.carrier_url~ (please note that at holiday times, your tracking information may not immediately appear on the ~venus.carrier_name~ web site due to the high volume of packages shipping).

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact ~venus.carrier_name~ at ~venus.carrier_url~ or call ~venus.carrier_phone~.

If you have any questions about your order, please call ~phone4~.

Sincerely,
FTD Customer Service
',
'Tracking Number',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'Y',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'TRACKING NUMBER.Groupon';

--CANCEL ORDER REQUEST.Groupon
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CANCEL ORDER REQUEST.Groupon',
'At your request, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued.  

Please be advised that, although we have processed your refund immediately, some banking institutions may take 7-10 business days to post this credit to your account.  

We hope you''ll give us the opportunity to assist you in the future.  Please feel free to contact us if we can be of any further assistance.

Sincerely,
FTD Customer Service
',
'Order Cancel Confirmation',
SYSDATE,
'CAL-9231',
SYSDATE,
'CAL-9231',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'GRPN', null from clean.stock_email where TITLE = 'CANCEL ORDER REQUEST.Groupon';