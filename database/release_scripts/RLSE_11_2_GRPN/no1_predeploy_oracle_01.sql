------------------------------------------------------------------------------------
-- Begin requested by msalla            --------------------------------------------
--  Backlog 13226                  -------------------------------------------------
------------------------------------------------------------------------------------

-- Insert new suffix mapping for outbound processing.
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MESSAGE_GENERATOR_CONFIG', 'SET_SOCKET_PARAMS', 'Y', 'CORDIAL_INTG', sysdate, 'CORDIAL_INTG', sysdate, 'Set TLS Socket params only when required');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MESSAGE_GENERATOR_CONFIG', 'LOG_SOAP_MESSAGE', 'Y', 'CORDIAL_INTG', sysdate, 'CORDIAL_INTG', sysdate, 'Log soap message for Cordial integration');

------------------------------------------------------------------------------------
-- End   requested by msalla           ---------------------------------------------
-- Backlog 13226                  --------------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk        --------------------------------------------
------------------------------------------------------------------------------------

insert into FTD_APPS.PARTNER_MASTER (
    PARTNER_NAME,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    FILE_SEQUENCE_PREFIX,
    PREFERRED_PROCESSING_RESOURCE,
    PREFERRED_PARTNER_FLAG,
    REPLY_EMAIL_ADDRESS,
    DEFAULT_PHONE_SOURCE_CODE,
    DEFAULT_WEB_SOURCE_CODE,
    BIN_PROCESSING_FLAG,
    FLORIST_RESEND_ALLOWED_FLAG,
    SOURCE_PRIMARY_FLORIST_FLAG,
    DISPLAY_NAME)
values (
    'Groupon',
    SYSDATE, 'CAL-9321', SYSDATE, 'CAL-9321',
    null,
    'Groupon',
    'N',
    'noreply@ftdi.com',
    '34524',
    '34524',
    'N',
    'N',
    'N',
    'Groupon');

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk        --------------------------------------------
------------------------------------------------------------------------------------
