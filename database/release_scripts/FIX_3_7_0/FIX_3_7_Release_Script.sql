set define off
--------------------------------
-- Fix_3_7_Release_Script.sql --
--------------------------------

--********************************************
-- PRE-DEPLOY SECTION
--********************************************



--********************************************
-- END PRE-DEPLOY SECTION, START OF DEPLOY
--********************************************


--
-- 6212
--
insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval,'DELIVERY_CONFIRMATION', 'DCON_ASK_MESSAGE_PARTNER', 'Delivery Confirmation Ask Message',(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), null, 'Defect_6212', sysdate, 'Defect_6212', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY,UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'DELIVERY_CONFIRMATION' AND CONTENT_NAME = 'DCON_ASK_MESSAGE_PARTNER'), 'USAA',  null, 'This is a USAA test dcon ask message', 'Defect_6212', sysdate, 'Defect_6212', sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval,'DELIVERY_CONFIRMATION', 'DCON_ASK_MESSAGE', 'Delivery Confirmation Ask Message',null, null, 'Defect_6212', sysdate, 'Defect_6212', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY,UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'DELIVERY_CONFIRMATION' AND CONTENT_NAME = 'DCON_ASK_MESSAGE'), null,  null, 'This is a regular test dcon ask message', 'Defect_6212', sysdate, 'Defect_6212', sysdate);


















----
---- END Fix 3.7 Release Script
----
