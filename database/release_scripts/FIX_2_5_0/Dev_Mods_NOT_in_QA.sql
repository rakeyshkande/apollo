rem Defect 3594
alter table scrub.credit_cards add (cc_number_masked varchar2(4));
alter table clean.credit_cards add (cc_number_masked varchar2(4));
