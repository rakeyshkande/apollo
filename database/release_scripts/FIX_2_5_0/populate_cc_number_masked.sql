update scrub.credit_cards set cc_number_masked = substr(global.encryption.decrypt_it(cc_number,key_name),-4,4);
update clean.credit_cards set cc_number_masked = substr(global.encryption.decrypt_it(cc_number,key_name),-4,4);
