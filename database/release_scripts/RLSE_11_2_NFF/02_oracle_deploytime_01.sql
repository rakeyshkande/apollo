------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk 2018/12/17  ---------------------------------------
--  Backlog 10224                  -------------------------------------------------
------------------------------------------------------------------------------------

--DEV url only
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SERVICE_URL', 'https://nonprod-gke-primary-1-proxy-dev1.gcp.ftdi.com/extended-florist-availability-service/ftd/api/availability/efa/', 'CAL-10224', sysdate, 'CAL-10224', sysdate, 'URL for EFA service');

--QA url only
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SERVICE_URL', 'https://nonprod-gke-primary-1-proxy-qa1.gcp.ftdi.com/extended-florist-availability-service/ftd/api/availability/efa/', 'CAL-10224', sysdate, 'CAL-10224', sysdate, 'URL for EFA service');

--PROD url only
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SERVICE_URL', 'https://prod-api-gateway.gcp.ftdi.com/internal/extendedfa/ftd/api/availability/efa/', 'CAL-10224', sysdate, 'CAL-10224', sysdate, 'URL for EFA service');

--All environments
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_SERVICE_TIMEOUT', '10', 'CAL-10224', sysdate, 'CAL-10224', sysdate,'Time out param for efa microservice');
declare
   v_key_name frp.global_parms.value%type;
begin
   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';
INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'SERVICE', 'EFA_SERVICE_SECRET', global.encryption.encrypt_it('replaceme', v_key_name),
   'Shared secret for EFA Service', sysdate, 'CAL-10224', sysdate, 'CAL-10224', v_key_name);
end;
/
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'CALL_EFA_SERVICE', 'Y', 'CAL-10244', sysdate, 'CAL-10244', sysdate,'If Y, then call the Extended Florist Availability Service.');
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'EFA_FTDM_FLORIST', '91-8400AA', 'CAL-10224', sysdate, 'CAL-10224', sysdate,'EFA FTDM Florist');

update ftd_apps.florist_master
set status = 'Inactive'
where florist_id = '91-8400AA';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk 2018/12/17  ---------------------------------------
--  Backlog 10224                  -------------------------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Manasa Salla 2018/12/19  -------------------------------------
--  Backlog 10403                  -------------------------------------------------
------------------------------------------------------------------------------------

alter table MERCURY.CALL_OUT_LOG 
add (SHOP_NAME      varchar2(300), 
     SHOP_PHONE     varchar2(100));

------------------------------------------------------------------------------------
-- End requested by Manasa Salla 2018/12/19  ---------------------------------------
--  Backlog 10403                  -------------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Shikha Gupta 2018/12/19  ---------------------------------------
--  Backlog 10610                  -------------------------------------------------
------------------------------------------------------------------------------------
    
--update in QA environments only    
     update FRP.GLOBAL_PARMS
            set VALUE = 'https://nonprod-gke-primary-1-web-api-gateway-qa1.gcp.ftd.com/internal/extendedfa/ftd/api/availability/efa/',
	    updated_on = sysdate,
	    updated_by = 'CAL-10610'
            where Name = 'EFA_SERVICE_URL';
--update in DEV environment only
update FRP.GLOBAL_PARMS
            set VALUE = 'https://nonprod-gke-primary-1-web-api-gateway-dev1.gcp.ftd.com/internal/extendedfa/ftd/api/availability/efa/',
            updated_on = sysdate,	    
            updated_by = 'CAL-10610'
            where Name = 'EFA_SERVICE_URL';

------------------------------------------------------------------------------------
-- End   requested by Shikha Gupta 2018/12/19  ---------------------------------------
--  Backlog 10610                  -------------------------------------------------
------------------------------------------------------------------------------------



