


--
-- Defect 6127 instructions
--

create table ftd_apps.upsell_detail$
   (UPSELL_DETAIL_ID                VARCHAR2(4),
    UPSELL_MASTER_ID                VARCHAR2(4),
    UPSELL_DETAIL_SEQUENCE          NUMBER(1),
    UPSELL_DETAIL_NAME              VARCHAR2(250),
    GBB_UPSELL_DETAIL_SEQUENCE_NUM  NUMBER(3),
    GBB_NAME_OVERRIDE_FLAG          VARCHAR2(1),
    GBB_NAME_OVERRIDE_TXT           VARCHAR2(255),
    GBB_PRICE_OVERRIDE_FLAG         VARCHAR2(1),
    GBB_PRICE_OVERRIDE_TXT          VARCHAR2(255),
    DEFAULT_SKU_FLAG                VARCHAR2(1),
    OPERATION$                      VARCHAR2(7) NOT NULL,
    TIMESTAMP$                      TIMESTAMP   NOT NULL
   )
 TABLESPACE ftd_apps_data;
 
 create index ftd_apps.upsell_detail$_n1 on ftd_apps.upsell_detail$(timestamp$) tablespace ftd_apps_indx;
 
 grant select on ftd_apps.upsell_detail$ to bi_etl;
 
 
 --
 -- Initial Seed
 --
 insert into ftd_apps.upsell_detail$
 select UPSELL_DETAIL_ID,
        UPSELL_MASTER_ID,
        UPSELL_DETAIL_SEQUENCE,
        UPSELL_DETAIL_NAME,
        GBB_UPSELL_DETAIL_SEQUENCE_NUM,
        GBB_NAME_OVERRIDE_FLAG,
        GBB_NAME_OVERRIDE_TXT,
        GBB_PRICE_OVERRIDE_FLAG,
        GBB_PRICE_OVERRIDE_TXT,
        DEFAULT_SKU_FLAG,
        'INS',
        systimestamp
from ftd_apps.upsell_detail;
        

Deploy the following from the FIX_3_4_0 branch:    ftd\database\ftd_apps_plsql\upsell_detail_$.trg    ver 1.1


Add the following to the nightly data purge (cvs dba\local\maint\purge_tables.sql):
   PROMPT Purge ftd_apps.upsell_detail$.
   delete from ftd_apps.upsell_detail$ where timestamp$ < sysdate-14;
   commit;

 


 
 
 
