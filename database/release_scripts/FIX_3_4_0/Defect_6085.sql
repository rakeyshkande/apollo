
--
-- Defect 6085 instructions
--

create table ftd_apps.upsell_master$
   (UPSELL_MASTER_ID                 VARCHAR2(4) NOT NULL,  
    UPSELL_NAME                      VARCHAR2(100),
    UPSELL_DESCRIPTION               VARCHAR2(1024),
    UPSELL_STATUS                    VARCHAR2(1),
    SEARCH_PRIORITY                  VARCHAR2(5),
    CORPORATE_SITE                   VARCHAR2(1),
    GBB_POPOVER_FLAG                 VARCHAR2(1),
    GBB_TITLE_TXT                    VARCHAR2(255),
    OPERATION$                       VARCHAR2(7) NOT NULL,
    TIMESTAMP$                       TIMESTAMP   NOT NULL
   )
 TABLESPACE ftd_apps_data;
 
 create index ftd_apps.upsell_master$_n1 on ftd_apps.upsell_master$(timestamp$) tablespace ftd_apps_indx;
 
 grant select on ftd_apps.upsell_master$ to bi_etl;
 
 --
 -- Initial Seed
 --
 insert into ftd_apps.upsell_master$
 select UPSELL_MASTER_ID, 
        UPSELL_NAME,  
        UPSELL_DESCRIPTION,    
        UPSELL_STATUS,   
        SEARCH_PRIORITY,  
        CORPORATE_SITE,    
        GBB_POPOVER_FLAG,  
        GBB_TITLE_TXT,
        'INS',
        systimestamp
from ftd_apps.upsell_master;
        

Deploy the following from the FIX_3_4_0 branch:    ftd\database\ftd_apps_plsql\upsell_master_$.trg    ver 1.1


Add the following to the nightly data purge (cvs dba\local\maint\purge_tables.sql):
   PROMPT Purge ftd_apps.upsell_master$.
   delete from ftd_apps.upsell_master$ where timestamp$ < sysdate-14;
   commit;

 


 
 
 
