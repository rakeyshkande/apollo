set define off
--------------------------------
-- Fix_3_4_Release_Script.sql --
--------------------------------

--********************************************
-- PRE-DEPLOY SECTION
--********************************************

--*******************************************
-- REMEMBER TO DO SET DEFINE OFF BECAUSE THE
-- DATA CONTAINS AMPERSANDS
--+++++++++++++++++++++++++++++++++++++++++++

--============== QA-SPECIFIC VALUES ==============================
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','FloristWeight_NotifyToAddress','apolloqa@ftdi.com,chu@ftdi.com','chu',sysdate,'chu',sysdate,
'Recipient email addresses to receive florist weight report notifications, including audit report, load to production, and rejection.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','FloristData_NotifyToAddress','apolloqa@ftdi.com,chu@ftdi.com','chu',sysdate,'chu',sysdate,
'Recipient email addresses to receive florist data report notifications, including audit report, load to production, and rejection.');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME) VALUES 
('ship_processing', 'NOSCAN_UPS_USERID', global.encryption.encrypt_it('ftdApollo', 'ORCL_CCNUM_2006'),
'Login to retrieve scan files from UPS', sysdate, 'SYS', sysdate, 'SYS', 'ORCL_CCNUM_2006');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME) VALUES 
('ship_processing', 'NOSCAN_UPS_PASSWORD', global.encryption.encrypt_it('password', 'ORCL_CCNUM_2006'),
'Password to retrieve scan files from UPS', sysdate, 'SYS', sysdate, 'SYS', 'ORCL_CCNUM_2006');

set define off
insert into rpt.report (report_id, name, description, report_file_prefix, report_type, report_output_type, report_category,
server_name, holiday_indicator, status, created_on, created_by, updated_on, updated_by, oracle_rdf, acl_name, retention_days,
run_time_offset, end_of_day_required, schedule_type, schedule_parm_value) values (
    1002043, 'Refund Summary (Daily)',
    'This report displays a summary of all refunds by refund type for the date specified, displayed as florist or drop-ship delivered.  Each refund type is subtotaled and the impact on revenue is broken down into specific revenue components (Net Merch; Add-Ons; Shipping; Service Fee & Tax).',
    'Refund_Summary_Daily',
    'S', 'Both', 'Acct', 'http://neon.ftdi.com:7778/reports/rwservlet?', 'Y', 'Active', sysdate, 'Defect 5881', sysdate,
    'Defect 5881', 'ACC04_Refund_Summary.rdf', 'AcctMktgCustReportAccess', 10, 120, 'N', 'D', '&p_monthly_daily_ind=D');
set define on
--============== END QA-SPECIFIC VALUES ==============================


--============== PRODUCTION-SPECIFIC VALUES ==============================
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','FloristWeight_NotifyToAddress','weboperations@ftdi.com,rkassam@ftdi.com,kgudgel@ftdi.com','chu',
sysdate,'chu',sysdate,
'Recipient email addresses to receive florist weight report notifications, including audit report, load to production, and rejection.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('LOAD_MEMBER_DATA_CONFIG','FloristData_NotifyToAddress','weboperations@ftdi.com,rkassam@ftdi.com,kgudgel@ftdi.com','chu',
sysdate,'chu',sysdate,
'Recipient email addresses to receive florist data report notifications, including audit report, load to production, and rejection.');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME) VALUES 
('ship_processing', 'NOSCAN_UPS_USERID', global.encryption.encrypt_it('ftdApollo', 'APOLLO_PROD_2008'),
'Login to retrieve scan files from UPS', sysdate, 'SYS', sysdate, 'SYS', 'APOLLO_PROD_2008');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME) VALUES 
('ship_processing', 'NOSCAN_UPS_PASSWORD', global.encryption.encrypt_it('password', 'APOLLO_PROD_2008'),
'Password to retrieve scan files from UPS', sysdate, 'SYS', sysdate, 'SYS', 'APOLLO_PROD_2008');

set define off
insert into rpt.report
    (report_id, name, description, report_file_prefix, report_type, report_output_type, report_category, server_name,
     holiday_indicator, status, created_on, created_by, updated_on, updated_by, oracle_rdf, acl_name, retention_days,
     run_time_offset, end_of_day_required, schedule_type, schedule_parm_value) values (
    1002043, 'Refund Summary (Daily)',
    'This report displays a summary of all refunds by refund type for the date specified, displayed as florist or drop-ship delivered.  Each refund type is subtotaled and the impact on revenue is broken down into specific revenue components (Net Merch; Add-Ons; Shipping; Service Fee & Tax).',
    'Refund_Summary_Daily',
    'S',
    'Both',
    'Acct',
    'http://apolloreports.ftdi.com/reports/rwservlet?',
    'Y',
    'Active',
    sysdate,
    'Defect 5881',
    sysdate,
    'Defect 5881',
    'ACC04_Refund_Summary.rdf',
    'AcctMktgCustReportAccess',
    10,
    120,
    'N',
    'D',
    '&p_monthly_daily_ind=D');
set define on
--============== END PRODUCTION-SPECIFIC VALUES ==============================

create Table STATS.PAYMENT_STATS(
PAYMENT_STATS_ID NUMBER        not null,
STAT_TIMESTAMP   TIMESTAMP     not null,
PAYMENT_TYPE     VARCHAR2(2)   not null,
PAYMENT_COUNT    NUMBER        not null) tablespace stats_data;

comment on column stats.payment_stats.payment_stats_id is 'Unique sequence generated id';
comment on column stats.payment_stats.stat_timestamp is 'Timestamp of the stats';
comment on column stats.payment_stats.payment_type is 
'Payment Type on the order, from ftd_apps.payment_methods.payment_method_id, though we don''t want it to be a foreign key';

comment on column stats.payment_stats.payment_count is 'Count of that payment type in the time period';

alter table STATS.PAYMENT_STATS add constraint payment_stats_pk primary key (PAYMENT_STATS_ID) using index tablespace stats_data;

create sequence stats.payment_stats_sq cache 200;

create Table STATS.AUTHORIZATION_STATS (
AUTHORIZATION_STATS_ID NUMBER        not null,
STAT_TIMESTAMP         TIMESTAMP     not null,
CREDIT_CARD_TYPE       VARCHAR2(100) not null,
APPROVAL_COUNT         NUMBER        not null,
DECLINE_COUNT          NUMBER        not null,
SERVER_NAME            VARCHAR2(100) not null,
AUTH_CLIENT            VARCHAR2(100) not null) tablespace stats_data;

comment on column stats.authorization_stats.authorization_stats_id is 'Unique sequence generated id';
comment on column stats.authorization_stats.stat_timestamp is 'Timestamp of the stats';
comment on column stats.authorization_stats.server_name is 'Server the auths happened on (ccas1 or ccas2 )';
comment on column stats.authorization_stats.auth_client is 'Source client requesting the auth (Apollo or Novator)';
comment on column stats.authorization_stats.credit_card_type is
'Credit Card Type passed from the authorization server, generally corresponds to payment_method_id';
  
alter table STATS.AUTHORIZATION_STATS add constraint authorization_stats_pk primary key (AUTHORIZATION_STATS_ID)
using index tablespace stats_data;

alter table stats.authorization_stats add constraint authorization_stats_u1 unique 
(STAT_TIMESTAMP, SERVER_NAME, AUTH_CLIENT, CREDIT_CARD_TYPE) using index tablespace stats_data;

create sequence stats.authorization_stats_sq cache 200;

grant select on clean.payments to stats;

--3401

--=== NOTE these were described as QA values by Gary, however he said the production values would remain unknown until
--    shortly before the production deploy.  Doublecheck this before the deploy.
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) VALUES  
('SHIPPING_PARMS', 'NOSCAN_UPS_URL', 'http://wwwcie.ups.com/ups.app/xml/QVEvents', 'SYS', sysdate, 'SYS', sysdate,
'URL at UPS for scan file retrieval');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) VALUES  
('SHIPPING_PARMS', 'NOSCAN_UPS_LICENSE', 'BC3CEA2F4AEC7BF0', 'SYS', sysdate, 'SYS', sysdate,
'Our UPS license number for scan file retrieval');
--=== END NOTE

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) VALUES  
('SHIPPING_PARMS', 'NOSCAN_UPS_SUBSCRIPTION', 'ftdAppSubscription', 'SYS', sysdate, 'SYS', sysdate,
'Name of Subscription defined at UPS which describes the type of data to be returned');


INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by) values (
    'OE_CONFIG', 'IOTW_PAGESIZE', '10', 'The number of IOTW records to display on the maintenance screen',
    sysdate, 'Defect 5850', sysdate, 'Defect 5850');


--********************************************
-- END PRE-DEPLOY SECTION, START OF DEPLOY
--********************************************

--5487
insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by)
values ('Update Responsible Party','Order Proc','Controls access to the change responsible party in refund after EOD',sysdate,sysdate,'defect 5487');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL) 
values ('FulfillmentAdmin', sysdate, sysdate, 'defect 5487', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('FulfillmentAdmin','Update Responsible Party','Order Proc','Update',sysdate,sysdate,'defect 5487');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values (200083,
'FulfillmentAdmin' , 'Order Proc', 'Advanced fulfillment actions', sysdate, sysdate, 'defect 5916' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values (200083, 'FulfillmentAdmin', sysdate,
sysdate, 'defect 5916');



--5856
alter table ftd_apps.upsell_detail add (default_sku_flag varchar2(1) default 'N');
alter table ftd_apps.upsell_detail add constraint upsell_detail_ck3 check (default_sku_flag in ('Y','N'));


--5878

update clean.order_details
set    miles_points_post_date=null, updated_on=sysdate, updated_by='defect_fix_5878'
where  order_detail_id in (
select orp.order_detail_id
from   clean.order_reward_posting orp
join   clean.order_details od on od.order_detail_id=orp.order_detail_id
join   ftd_apps.product_master pm on pm.product_id=od.product_id
where  orp.program_name='JELLYFISH'
and    orp.file_post_date>to_date('01/01/2009','mm/dd/yyyy')
and    pm.product_id<>pm.novator_id);

delete from clean.order_reward_posting where order_detail_id in (
select orp.order_detail_id
from   clean.order_reward_posting orp
join   clean.order_details od on od.order_detail_id=orp.order_detail_id
join   ftd_apps.product_master pm on pm.product_id=od.product_id
where  orp.program_name='JELLYFISH'
and    orp.file_post_date>to_date('01/01/2009','mm/dd/yyyy')
and    pm.product_id<>pm.novator_id);


--2859

alter table ftd_apps.shipping_keys add constraint shipping_keys_u1 unique (shipping_key_description)
using index tablespace ftd_apps_indx;


--3401
alter table venus.carriers drop constraint carriers_ck3;
alter table venus.carriers add  constraint carriers_ck3 CHECK (SCAN_FORMAT IN ('DHL', 'FEDEX', 'UPS'));

insert into VENUS.CARRIER_SCANS (CARRIER_ID, SCAN_TYPE, COLLECT, DELIVERY_SCAN) values ('UPS', 'Delivery', 'Y', 'Y');
UPDATE VENUS.CARRIERS SET SCAN_FORMAT = 'UPS' WHERE CARRIER_ID = 'UPS';

-- Turn off DHL scanning
UPDATE VENUS.CARRIERS SET SCAN_FORMAT = null, active='N' WHERE CARRIER_ID = 'DHL';

update ftd_apps.product_master set preferred_price_point = 1 where preferred_price_point <> 1;

update ftd_apps.upsell_detail set default_sku_flag = 'Y' where upsell_detail_sequence = 1;

update rpt.report set schedule_parm_value = '&p_monthly_daily_ind=M' where report_id = 1002041;



-- premier collection
alter table ftd_apps.product_master add
  (
        premier_collection_flag        varchar2(1)     DEFAULT 'N' NOT NULL,        
    constraint product_master_ck19     CHECK(premier_collection_flag IN('Y','N'))  
  );

alter table ftd_apps.product_master$ add
  (
        premier_collection_flag        varchar2(1)   
  );






----
---- END Fix 3.4 Release Script
----
