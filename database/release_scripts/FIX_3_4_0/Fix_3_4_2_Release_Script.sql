set define off
--------------------------------
-- Fix_3_4_2_Release_Script.sql --
--------------------------------

--
-- 5999
--

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('VALIDATION_SERVICE','LUX_MAX_ITEM_ADDON_TOTAL','10000.00','chu',sysdate,'chu',sysdate, 'Maximum addon item total for luxury items to bypass fraud check.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('VALIDATION_SERVICE','LUX_MAX_ITEM_TOTAL','10000.00','chu',sysdate,'chu',sysdate, 'Maximum item total for luxury items to bypass fraud check.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('VALIDATION_SERVICE','LUX_MAX_ORDER_TOTAL','10000.00','chu',sysdate,'chu',sysdate, 'Maximum cart total for luxury items to bypass fraud check.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('VALIDATION_SERVICE','LUX_MAX_LINE_ITEMS','100','chu',sysdate,'chu',sysdate, 'Maximum number of items in cart for luxury items to bypass fraud check.');


declare	
 content_master_id number;
 content_detail_id number;
begin
 select ftd_apps.content_master_sq.nextval into content_master_id from dual;

insert into FTD_APPS.CONTENT_MASTER
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values 
(content_master_id, 'PHONE_NUMBER' , 'PREMIER_CALL_CENTER_CONTACT_NUMBER', 'Phone number for the call center for  premier item customer support', null, null, 'defect 5999', sysdate, 'defect 5999', sysdate);	

select ftd_apps.content_detail_sq.nextval into content_detail_id from dual;

insert into FTD_APPS.CONTENT_DETAIL
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (content_detail_id, content_master_id, null, null , '1-866-233-2811', 'defect 5999', sysdate, 'defect 5999', sysdate);


end;
/



database/scrub_plsql/orders_query_pkg.pkb    v1.10.2.3



----
---- END Fix 3.4.2 Release Script
----
