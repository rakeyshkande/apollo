CREATE TABLE FTD_APPS.SERVICE_FEE_OVERRIDE (
   SNH_ID                  varchar2(2) not null,
   OVERRIDE_DATE           Date not null,
   DOMESTIC_CHARGE         Number(5,2) not null,
   INTERNATIONAL_CHARGE    Number(5,2) not null,
   REQUESTED_BY            varchar2(100) not null,
   CREATED_ON              date not null,
   CREATED_BY              varchar2(100) not null,
   UPDATED_ON              Date not null,
   UPDATED_BY              varchar2(100) not null,
CONSTRAINT SERVICE_FEE_OVERRIDE_PK PRIMARY KEY(snh_id,override_date) using index tablespace ftd_apps_indx,
CONSTRAINT SERVICE_FEE_OVERRIDE_FK1 FOREIGN KEY(snh_id) references ftd_apps.snh)
TABLESPACE FTD_APPS_DATA;


CREATE TABLE FTD_APPS.SERVICE_FEE_OVERRIDE$ (
   SNH_ID                  varchar2(2) not null,
   OVERRIDE_DATE           Date not null,
   DOMESTIC_CHARGE         Number(5,2) not null,
   INTERNATIONAL_CHARGE    Number(5,2) not null,
   REQUESTED_BY            varchar2(100) not null,
   CREATED_ON              date not null,
   CREATED_BY              varchar2(100) not null,
   UPDATED_ON              Date not null,
   UPDATED_BY              varchar2(100) not null,
   OPERATION$              varchar2(7)   not null,
   TIMESTAMP$              date          not null
)
TABLESPACE FTD_APPS_DATA;

grant select on FTD_APPS.SERVICE_FEE_OVERRIDE to global;
grant select on FTD_APPS.SERVICE_FEE_OVERRIDE$ to global;
--Defect 943
GRANT SELECT ON FTD_APPS.SERVICE_FEE_OVERRIDE$ TO RPT;
--END Defect 943

CREATE TABLE ftd_apps.personalization_templates (
personalization_template_id VARCHAR2(50),
personalization_description VARCHAR2(100)) TABLESPACE ftd_apps_data;

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name9_Date8_Weight12_Length10', 'Name (9 max), Date (8 max), Weight (12 max), Length (10 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name20_Date20', 'Name (20 max), Date (20 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name16_Years9', 'Name (16 max), Years (9 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name_15', 'Name (15 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name_12', 'Name (12 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Name_9', 'Name (9max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('Line18_Line18', 'Line 1 (18 max), Line 2 (18 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('LName_12', 'Last Name (12 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('F_Name_11', 'First Name (11 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('F_Name_8', 'First Name (8 max)');

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES(PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES 
('NONE', '(None)');

ALTER TABLE ftd_apps.personalization_templates ADD (
CONSTRAINT personalization_templates_pk PRIMARY KEY (personalization_template_id) USING INDEX TABLESPACE ftd_apps_indx);

CREATE TABLE "VENUS"."CARRIER_SCANS"
("CARRIER_ID" VARCHAR2(10) NOT NULL,
  "SCAN_TYPE" VARCHAR2(32) NOT NULL,
  "COLLECT"   CHAR(1) DEFAULT 'Y' NOT NULL,
  "DELIVERY_SCAN" CHAR(1) DEFAULT 'N' NOT NULL,
 CONSTRAINT "CARRIER_SCANS_PK" PRIMARY KEY ("CARRIER_ID", "SCAN_TYPE") using index TABLESPACE venus_indx,
 CONSTRAINT "CARRIER_SCANS_CK1" CHECK(collect IN ('Y','N')),
 CONSTRAINT "CARRIER_SCANS_CK2" CHECK(delivery_scan IN ('Y','N'))) tablespace venus_data;


CREATE SEQUENCE clean.DISC_FILE_SQ MINVALUE 1 MAXVALUE 999999999 START WITH 1 INCREMENT BY 1 NOCACHE;

insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
        VALUES('PARTNER_REWARD','DISC_PROMOTION_VALUE','003773','SYS',SYSDATE,'SYS',SYSDATE);

CREATE TABLE "VENUS"."SCANS"
    ("ID"               NUMBER(22) NOT NULL,
     "CARRIER_ID"       VARCHAR2(10) NOT NULL,
     "SCAN_TYPE"        VARCHAR2(32) NOT NULL,
     "FILE_NAME"        VARCHAR2(256) NOT NULL,
     "SCAN_TIMESTAMP"   DATE      NOT NULL,
     "PROCESSED"        CHAR(1) DEFAULT 'N' NOT NULL,
     "ORDER_ID" VARCHAR2(32) ,
     tracking_number varchar2(200),
     "CREATED_ON"       DATE      NOT NULL,
     "UPDATED_ON"       DATE      NOT NULL,
    CONSTRAINT "SCANS_PK" PRIMARY KEY("ID") using index TABLESPACE venus_indx,
    CONSTRAINT "SCANS_CK1" CHECK(processed in('Y','N')),
    CONSTRAINT "SCANS_FK1" FOREIGN KEY("CARRIER_ID") REFERENCES "VENUS"."CARRIERS"("CARRIER_ID"))
 tablespace venus_data;

CREATE SEQUENCE "VENUS"."SCANS_SQ" INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCYCLE CACHE 20;

insert into sitescope.pagers values ('SERVICE_FEE', 'BACOM@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'bsharif@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'aponce@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'agreen@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'erupp@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'sbedford@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'lmclafferty@ftdi.com');
insert into sitescope.pagers values ('SERVICE_FEE', 'dkamedulski@ftdi.com');

#################
##START DEFECT 1756
#################

insert into aas.role values
(200066, 'QueueManagementAdmin','Order Proc','Queue Management Administrator Role',sysdate,sysdate,'SYS');

insert into aas.resources values
('Queue Management Menu link', 'Order Proc','Queue Management Menu link',sysdate,sysdate,'SYS');

insert into aas.acl values ('QueueManagementAdmin',sysdate,sysdate,'SYS',NULL);

insert into aas.rel_role_acl values (200066,'QueueManagementAdmin',sysdate,sysdate,'SYS');
insert into aas.rel_role_acl values (200066,'Operations',sysdate,sysdate,'SYS');

insert into aas.rel_acl_rp values
('QueueManagementAdmin','Queue Management Menu link','Order Proc','View',sysdate,sysdate,'SYS');

#################
##END DEFECT 1756
#################

CREATE TABLE ftd_apps.florist_zips_removal
(top_level_florist_id  VARCHAR2(9),
 zip_code              VARCHAR2(6),
 do_not_erase_flag     CHAR(1),
 created_on            DATE,
 latest_flag           CHAR(1),
 constraint florst_zips_removal_pk primary key (top_level_florist_id,zip_code,created_on) using index tablespace ftd_apps_indx)
 tablespace ftd_apps_data;

COMMENT ON TABLE  ftd_apps.florist_zips_removal is 
'Helps track which zip codes need to be removed after member load and keep history of such removals';
COMMENT ON COLUMN ftd_apps.florist_zips_removal.do_not_erase_flag IS 
'If Y then this zip overrides removal restriction and is not removed';
COMMENT ON COLUMN ftd_apps.florist_zips_removal.latest_flag IS 'If Y then record was added from most recent member load';

--Defect 952
Insert into RPT."REPORT_PARM_REF" (
        "REPORT_PARM_ID",
        "REPORT_ID",
        "SORT_ORDER",
        "REQUIRED_INDICATOR",
        "CREATED_ON",
        "CREATED_BY",
        "ALLOW_FUTURE_DATE_IND")
values (
        100503,
        100806,
        2,
        'Y',
        sysdate,
        'ddesai',
        null);
--END Defect 952

CREATE SEQUENCE venus.pc_order_number_sq INCREMENT BY 1 START WITH 1 MINVALUE 1 MAXVALUE 9999999 NOORDER CACHE 200;


### Set up Korean Air as a partner
insert into ftd_apps.partner_master
(partner_name,created_on,created_by,updated_on,updated_by,file_sequence_prefix)
values ('KOREAN AIR',sysdate,'SYS',sysdate,'SYS','KORA');
commit;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_koreanair',
       'SUBMITTER',
       'events.daemon_pkg.submit(''ins_partner_reward_koreanair'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_koreanair',
       'REMOVER',
       'events.daemon_pkg.remove(''ins_partner_reward_koreanair'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_koreanair',
       'INTERVAL',
       'TRUNC(next_day(SYSDATE,''WEDNESdAY''))+(30/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_koreanair',
       'SUBMITTER',
       'events.daemon_pkg.submit(''ins_response_koreanair'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_koreanair',
       'REMOVER',
       'events.daemon_pkg.remove(''ins_response_koreanair'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_koreanair',
       'INTERVAL',
       'TRUNC(next_day(SYSDATE,''FRIDAY''))++(12/24)+(15/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by)
VALUES ('partner_reward', 'KOREANAIR_InboundFtpPswd', global.encryption.encrypt_it('reports'),
'Inbound ftp password for Korean Air', sysdate, 'SYS', sysdate, 'SYS');

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by)
VALUES ('partner_reward', 'KOREANAIR_InboundFtpUser', global.encryption.encrypt_it('reports'),
'Inbound ftp username for Korean Air', sysdate, 'SYS', sysdate, 'SYS');

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by)
VALUES ('partner_reward', 'KOREANAIR_OutboundFtpPswd', global.encryption.encrypt_it('reports'),
'Outbound ftp password for Korean Air', sysdate, 'SYS', sysdate, 'SYS');

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by)
VALUES ('partner_reward', 'KOREANAIR_OutboundFtpUser', global.encryption.encrypt_it('reports'),
'Outbound ftp username for Korean Air', sysdate, 'SYS', sysdate, 'SYS');
commit;

INSERT INTO EVENTS.EVENTS (EVENT_NAME, CONTEXT_NAME, DESCRIPTION, ACTIVE) VALUES ('PERSONAL_CREATIONS_OUTBOUND', 'VENUS',
'Personal Creations outbound FTP event.', 'Y');

INSERT INTO EVENTS.EVENTS (EVENT_NAME, CONTEXT_NAME, DESCRIPTION, ACTIVE) VALUES ('PERSONAL_CREATIONS_INBOUND', 'VENUS',
'Personal Creations inbound FTP event.', 'Y');

############
## NOTE:  THE FOLLOWING IS FOR PROD DEPLOY ONLY - THE URL IS NOT VALID FOR OTHER ENVIRONMENTS
############
-- insert into rpt.report table
Insert into RPT.REPORT (
        "REPORT_ID",
        "NAME",
        "DESCRIPTION",
        "REPORT_FILE_PREFIX",
        "REPORT_TYPE",
        "REPORT_OUTPUT_TYPE",
        "REPORT_CATEGORY",
        "SERVER_NAME",
        "HOLIDAY_INDICATOR",
        "STATUS",
        "CREATED_ON",
        "CREATED_BY",
        "UPDATED_ON",
        "UPDATED_BY",
        "ORACLE_RDF",
        "REPORT_DAYS_SPAN",
        "NOTES",
        "ACL_NAME",
        "RETENTION_DAYS",
        "RUN_TIME_OFFSET",
        "END_OF_DAY_REQUIRED",
        "SCHEDULE_DAY",
        "SCHEDULE_TYPE",
        "SCHEDULE_PARM_VALUE")
values (
        100605,
        'Service Fee Edit Rpt',
        'This ad-hoc and automated monthly report provides insight into changes made to FTD.COM''s listing of service fee overrides over the specified time frame.  The report will highlight changes to existing service fees as well as a listing of service fees added or expired.',
        'Service_Fee_Edit_Rpt',
        'U',
        'Both',
        'Acct',
        'http://apolloreports.ftdi.com/reports/rwservlet?',
        'Y',
        'Active',
        sysdate,
        'ddesai',
        sysdate,
        'ddesai',
        'EDIT05_Service_Fee_Edit_Report.rdf',
        31,
        'Date range cannot exceed 31 days',
        'AcctMktgReportAccess',
        10,
        120,
        'N',
        null,
        null,
        null);

-- insert into rpt.report_parm_ref table
Insert into RPT.REPORT_PARM_REF (
        "REPORT_PARM_ID",
        "REPORT_ID",
        "SORT_ORDER",
        "REQUIRED_INDICATOR",
        "CREATED_ON",
        "CREATED_BY",
        "ALLOW_FUTURE_DATE_IND")
values (
        100402,
        100605,
        1,
        'Y',
        sysdate,
        'ddesai',
        null);

-- insert into rpt.report table for the scheduled version of this report
Insert into RPT.REPORT (
        "REPORT_ID",
        "NAME",
        "DESCRIPTION",
        "REPORT_FILE_PREFIX",
        "REPORT_TYPE",
        "REPORT_OUTPUT_TYPE",
        "REPORT_CATEGORY",
        "SERVER_NAME",
        "HOLIDAY_INDICATOR",
        "STATUS",
        "CREATED_ON",
        "CREATED_BY",
        "UPDATED_ON",
        "UPDATED_BY",
        "ORACLE_RDF",
        "REPORT_DAYS_SPAN",
        "NOTES",
        "ACL_NAME",
        "RETENTION_DAYS",
        "RUN_TIME_OFFSET",
        "END_OF_DAY_REQUIRED",
        "SCHEDULE_DAY",
        "SCHEDULE_TYPE",
        "SCHEDULE_PARM_VALUE")
values (
        1006051,
        'Service Fee Edit Report (Monthly)',
        'This ad-hoc and automated monthly report provides insight into changes made to FTD.COM''s listing of service fee overrides over the specified time frame.  The report will highlight changes to existing service fees as well as a listing of service fees added or expired.',
        'Service_Fee_Edit_Rpt_Monthly',
        'S',
        'Both',
        'Acct',
        'http://apolloreports.ftdi.com/reports/rwservlet?',
        'Y',
        'Active',
        sysdate,
        'ddesai',
        sysdate,
        'ddesai',
        'EDIT05_Service_Fee_Edit_Report.rdf',
        null,
        null,
        'AcctMktgReportAccess',
        10,
        120,
        'N',
        '01',
        'M',
        null);
############
### END PROD-ONLY INSERT
############

### Set up Personal Creations jobs
insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_outbound',
       'SUBMITTER',
       'events.daemon_pkg.submit(''job_venus_pc_outbound'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_outbound',
       'REMOVER',
       'events.daemon_pkg.remove(''job_venus_pc_outbound'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_outbound',
       'INTERVAL',
       'events.daemon_pkg.venus_wine_outbound_interval' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_customer_file',
       'SUBMITTER',
       'events.daemon_pkg.submit(''job_venus_pc_customer_file'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_customer_file',
       'REMOVER',
       'events.daemon_pkg.remove(''job_venus_pc_customer_file'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'job_venus_pc_customer_file',
       'INTERVAL',
       'TRUNC(ADD_MONTHS(SYSDATE,1),''MONTH'')+5/1440' ,
       'SYS',sysdate,'SYS',sysdate from dual;

--Scheduler
INSERT INTO QUARTZ_SCHEMA.PIPELINE (PIPELINE.PIPELINE_ID, PIPELINE.GROUP_ID, PIPELINE.DESCRIPTION, PIPELINE.TOOL_TIP,
PIPELINE.QUEUE_NAME)
VALUES( 'DELIVERYCONFIRM','SHIPPROCESSING','Send delivery confirmation',
'Sends a delivery confirmation email for the venus id in the payload','OJMS.SDS_DELIVERY_CONFIRM');

INSERT INTO QUARTZ_SCHEMA.PIPELINE (PIPELINE.PIPELINE_ID, PIPELINE.GROUP_ID, PIPELINE.DESCRIPTION, PIPELINE.TOOL_TIP,
PIPELINE.QUEUE_NAME)
VALUES( 'PROCESSSCANS','SHIPPROCESSING','Process scan file/record',
'Process a scan file or venus.scans record.  Payload is file name or scans id.','OJMS.SDS_PROCESS_SCANS');

UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.SDS_GET_SCANS' WHERE PIPELINE_ID = 'GETSCANS';
UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.SDS_GET_STATUS' WHERE PIPELINE_ID = 'GETSTATUS';
UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.SDS_MANIFEST' WHERE PIPELINE_ID = 'MANIFEST';
UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.SDS_INBOUND_PROCESSING' WHERE PIPELINE_ID = 'PROCESSINBOUND';
UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.SHIP_PROCESSING' WHERE PIPELINE_ID = 'PROCESSSHIP';
UPDATE QUARTZ_SCHEMA.PIPELINE SET QUEUE_NAME = 'OJMS.PDB' WHERE PIPELINE_ID = 'PDB';

--FEDEX
INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES('ship_processing','NOSCAN_FTP_LOGIN_FEDEX','FIXME','Login to FTP server for FedEx scan files',sysdate,'RLSE_2_2_0',sysdate,
'RLSE_2_2_0');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES('ship_processing','NOSCAN_FTP_PASSWORD_FEDEX','FIXME','Password to FTP server for FedEx scan files',sysdate,'RLSE_2_2_0',
sysdate,'RLSE_2_2_0');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_SERVER_FEDEX','hpftd1.ftdi.com','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_REMOTE_DIRECTORY_FEDEX','/home/reports/fedex','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_LOCAL_DIRECTORY_FEDEX','/u02/apollo/noscan/new/fedex','RLSE_2_2_0',SYSDATE,
'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_DELETE_REMOTE_FILE_FEDEX','true','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

--DHL
INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES('ship_processing','NOSCAN_FTP_LOGIN_DHL','FIXME','Login to FTP server for DHL scan files',sysdate,'RLSE_2_2_0',sysdate,
'RLSE_2_2_0');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES('ship_processing','NOSCAN_FTP_PASSWORD_DHL','FIXME','Password to FTP server for DHL scan files',sysdate,'RLSE_2_2_0',
sysdate, 'RLSE_2_2_0');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_SERVER_DHL','hpftd1.ftdi.com','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_REMOTE_DIRECTORY_DHL','/home/reports/dhl','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_LOCAL_DIRECTORY_DHL','/u02/apollo/noscan/new/DHL','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('SHIPPING_PARMS','NOSCAN_FTP_DELETE_REMOTE_FILE_DHL','true','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES('ship_processing','NOSCAN_ARCHIVE_ROOT_DIRECTORY','/u02/apollo/noscan/processed','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

insert into frp.global_parms (context, name, value, created_by, created_on, updated_by, updated_on)
values('UPDATE_DELIVERY_DATE', 'MAX_DELIVERY_DAYS_PAST', '30', 'SYS', SYSDATE, 'SYS', SYSDATE);


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES ('SHIPPING_PARMS','SDS_TIMEOUT_SECONDS','300','RLSE_2_2_0',SYSDATE,'RLSE_2_2_0',SYSDATE);

commit;
