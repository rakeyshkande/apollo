
------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                11/ 3/2016  --------
-- defect DDS-2           (Mark   ) (242266 )           ----------------------------
------------------------------------------------------------------------------------

INSERT INTO quartz_schema.pipeline
(
                pipeline_id,
                group_id,
                description,
                tool_tip,
                default_schedule,
                default_payload,
                force_default_payload,
                queue_name,
                class_code,
                active_flag
)
VALUES
(
                'SHIPWESTPRODUCTUPDATE',
                'SHIP',
                'Retrieve FTD West product availability updates',
                'Retrieves product availability updates from FTD West',
                '300',
                'WEST',
                'Y',
                'OJMS.SHIP_WEST_PRODUCT_UPDATE',
                'SimpleJmsJob',
                'Y'
);
 
------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                11/ 3/2016  --------
-- defect DDS-2           (Mark   ) (242266 )           ----------------------------
------------------------------------------------------------------------------------
 
-- =================================================================================
-- END RELEASE 10.0.0.1
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                11/ 3/2016  --------
-- defect AFE-2           (Mark   ) (242773 )           ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FTD_APPS.FLORIST_COMMENT_TYPES (
    COMMENT_TYPE, DESCRIPTION
) VALUES (
    'Cities', 'Comments that are entered due to a change on the Cities tab. User entered or system generated.'
);

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                11/ 3/2016  --------
-- defect AFE-2           (Mark   ) (242773 )           ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.0.0.2
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Barghava Surimenu                         11/29/2016  --------
-- defect Q1SP17-3        (Mark   ) (250967 )           ----------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE, FTD_APPS GROUP 2
ALTER table ftd_apps.source_master add merch_amt_full_refund_flag char(1)  default 'Y';
-- THIS IS A GOLDEN GATE CHANGE, FTD_APPS GROUP 3
alter table ftd_apps.source_master$ add merch_amt_full_refund_flag char(1)  default 'Y';
Update ftd_apps.source_master set merch_amt_full_refund_flag = �N� where source_code =� 77806�;

------------------------------------------------------------------------------------
-- End   requested by Barghava Surimenu                         11/29/2016  --------
-- defect Q1SP17-3        (Mark   ) (250967 )           ----------------------------
------------------------------------------------------------------------------------


-- =================================================================================
-- END RELEASE 10.0.0.3
-- ---------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                         12/14/2016  --------
-- defect DDS-4        (Syed   ) (259364 )           ----------------------------
------------------------------------------------------------------------------------

ALTER TABLE STATS.SERVICE_RESPONSE_TRACKING  ADD REQUEST CLOB;

ALTER TABLE STATS.SERVICE_RESPONSE_TRACKING  ADD RESPONSE CLOB;

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                         12/14/2016  --------
-- defect DDS-4        (Syed   ) (259364 )           ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by gboddu                         12/16/2016  --------
-- defect MARSENH-2        (kmohamme   ) (260716)           ----------------------------
------------------------------------------------------------------------------------
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_BY,
    UPDATED_ON,  
    DESCRIPTION)
VALUES (
    'MARS_CONFIG',
    'SKIP_EROS_FLORIST',
    '00-0003',
    sysdate,
    'MARSENH-2',
    'MARSENH-2',
    sysdate,
    'EROS rejected the order because the florist did not pick it up in time');

------------------------------------------------------------------------------------
-- End requested by gboddu                         12/16/2016  --------
-- defect MARSENH-2        (kmohamme   ) (260716)           ----------------------------
------------------------------------------------------------------------------------
    
-- =================================================================================
-- END RELEASE 10.0.0.5
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               12/20/2016  --------
-- defect Q1SP17-10        (Mark       ) (263952)       ----------------------------
------------------------------------------------------------------------------------

DELETE FROM FRP.GLOBAL_PARMS WHERE CONTEXT='FTDAPPS_PARMS' and NAME='CSZ_AVAIL_ON';

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               12/20/2016  --------
-- defect Q1SP17-10        (Mark       ) (263952)       ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                               1/9/2017  --------
-- defect Q1SP17-41        (Syed) (273684)              ----------------------------
------------------------------------------------------------------------------------

--First drop the queue OJMS.PHOENIX 

BEGIN
dbms_aqadm.drop_queue_table (queue_table=> 'OJMS.PHOENIX',force=> TRUE); 
END;
/

--Then recreate it with sort option.

set echo on serveroutput on
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PHOENIX',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 '||
      'VARRAY user_data.header.properties STORE AS LOB phoenix_headerlob '||
      '(NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
      'LOB (user_data.text_lob) STORE AS phoenix_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
      'STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',sort_list => 'PRIORITY,ENQ_TIME');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PHOENIX',
      queue_table => 'OJMS.PHOENIX');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PHOENIX');

end;
/

conn / as sysdba
exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PHOENIX', grantee => 'OSP');
exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PHOENIX', grantee => 'EVENTS');
conn /


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                               1/9/2017  --------
-- defect Q1SP17-41        (Syed) (273684)              ----------------------------
------------------------------------------------------------------------------------


