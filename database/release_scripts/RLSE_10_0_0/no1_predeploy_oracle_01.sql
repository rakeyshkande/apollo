------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                   10/23/2016  --------
-- defect AFE-5           (Syed   ) (239040 )       ----------------------------
------------------------------------------------------------------------------------
 
CREATE TABLE FTD_APPS.FLORIST_CITY_BLOCKS
    (
        FLORIST_ID VARCHAR2(9) NOT NULL,
        BLOCK_START_DATE DATE NOT NULL,
        BLOCK_END_DATE DATE,
        BLOCKED_BY_USER_ID VARCHAR2(100),
        CONSTRAINT FLORIST_CITY_BLOCKS_PK PRIMARY KEY (FLORIST_ID, BLOCK_START_DATE),
        CONSTRAINT FLORIST_CITY_BLOCKS_FK1 FOREIGN KEY (FLORIST_ID) REFERENCES FTD_APPS.FLORIST_MASTER
        (FLORIST_ID)
    );


CREATE TABLE FTD_APPS.FLORIST_CITY_BLOCKS$
    (
        FLORIST_ID VARCHAR2(9) NOT NULL,
        BLOCK_START_DATE DATE,
        BLOCK_END_DATE DATE,
        BLOCKED_BY_USER_ID VARCHAR2(100),
        OPERATION$ VARCHAR2(7),
        TIMESTAMP$ TIMESTAMP(6)
    );

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                   10/23/2016  --------
-- defect AFE-5           (Syed   ) (239040 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                11/ 3/2016  --------
-- defect DDS-1              (Mark     ) (242261)       ----------------------------
------------------------------------------------------------------------------------

create table ftd_apps.west_product_update (
west_product_update_id integer       not null,
novator_id             varchar2(10)  not null,
pquad_product_id       integer       not null,
processed              char(1)       not null,
created_on             date          not null,
created_by             varchar2(100) not null,
updated_on             date          not null,
updated_by             varchar2(100) not null) tablespace ftd_apps_data;

alter table ftd_apps.west_product_update add constraint west_product_update_pk primary key (west_product_update_id)
using index tablespace ftd_apps_indx;

create index ftd_apps.west_product_update_n1 on ftd_apps.west_product_update(novator_id)
tablespace ftd_apps_indx;

create index ftd_apps.west_product_update_n2 on ftd_apps.west_product_update(pquad_product_id)
tablespace ftd_apps_indx;

create sequence ftd_apps.west_product_update_id_sq cache 200;

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'SHIPPING_PARMS', 'FTDWEST_UPDATE_PRODUCT_JOB_RUNNING', 'N', 'DDS-1', sysdate, 'DDS-1', sysdate,
'Flag indicating whether or not the FTD West Update Product Job is currently running.');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'SHIPPING_PARMS', 'FTDWEST_UPDATE_PRODUCT_MAX_BATCH_SIZE', '50', 'DDS-1', sysdate, 'DDS-1', sysdate,
'Max number of products allowed per batch.');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'SHIPPING_PARMS', 'FTDWEST_UPDATE_PRODUCT_BATCH_DELAY', '1000', 'DDS-1', sysdate, 'DDS-1', sysdate,
'Time interval in milliseconds at which time next batch of products will be sent to FTD West.');

run database/ojms_objects/ship_west_product_update.sql
run database/ojms_objects/ship_west_vendor_update.sql

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                11/ 3/2016  --------
-- defect DDS-1              (Mark     ) (242261)       ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.0.0.1
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by msalla                                   11/18/2016  --------
-- defect  Q1SP17-14        (kmohamme   ) (247244 )       ----------------------------
------------------------------------------------------------------------------------
insert into frp.global_parms values('DASHBOARD_CONFIG','FLORIST_STATUS_WARN_PERCENTAGES','85;75;75','msalla',sysdate,'msalla',sysdate,'THE WARN THRESHOLD VALUES FOR FLORIST STATUS GRAPH');

insert into frp.global_parms values('DASHBOARD_CONFIG','FLORIST_STATUS_CRIT_PERCENTAGES','95;85;85','msalla',sysdate,'msalla',sysdate,'THE CRITICAL THRESHOLD VALUES FOR FLORIST STATUS GRAPH');




------------------------------------------------------------------------------------
-- END requested by msalla                                   11/18/2016  --------
-- defect  Q1SP17-14         (kmohamme   ) (247244 )       ----------------------------
-------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.0.0.2
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                   11/30/2016  --------
-- defect DDS-1           (Syed   ) (251680 )       ----------------------------
------------------------------------------------------------------------------------

set serveroutput on;

DECLARE

v_service_url	varchar2(200);
v_db_name	varchar2(20);

BEGIN
	select name into v_db_name from v$database;

	if v_db_name = 'JUPITER' then
		v_service_url := 'https://apiservice.providecommerce.com/API/Product/v1/SOAP?singleWsdl';
	else
		v_service_url := 'https://lm79apiservice.providecommerce.com/API/Product/v1/SOAP?singleWsdl';
	end if;

	dbms_output.put_line(v_service_url);

	insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
	values ('SHIPPING_PARMS', 'FTDWEST_PRODUCT_SERVICE_URL', v_service_url, 'DDS-1', sysdate, 'DDS-1', sysdate,'Web Service URL for FTD West Product Service');

END;
/


------------------------------------------------------------------------------------
-- END requested by Rose Lazuk                                   11/30/2016  --------
-- defect DDS-1           (Syed   ) (251680 )       ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by msalla                                   12/01/2016  --------
-- defect  Q1SP17-16         (kmohamme   ) (252221 )       ----------------------------
------------------------------------------------------------------------------------
INSERT INTO FRP.GLOBAL_PARMS VALUES('DASHBOARD_CONFIG','QUEUE_STATUS_WARN_PERCENTAGES','30;30;30','Q1SP17-16',sysdate,'Q1SP17-16',sysdate,'THE WARN THRESHOLD VALUES FOR QUEUE STATUS GRAPH');

INSERT INTO FRP.GLOBAL_PARMS VALUES('DASHBOARD_CONFIG','QUEUE_STATUS_CRIT_PERCENTAGES','50;50;50','Q1SP17-16',sysdate,'Q1SP17-16',sysdate,'THE CRITICAL THRESHOLD VALUES FOR QUEUE STATUS GRAPH');
------------------------------------------------------------------------------------
-- END requested by msalla                                   12/01/2016  --------
-- defect  Q1SP17-16          (kmohamme   ) (252221 )       ----------------------------
-------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by gboddu                                   12/01/2016  --------
-- defect  MARSENH-2         (kmohamme   ) ( 252225 )       ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FTD_APPS.content_master(
 CONTENT_MASTER_ID,
 CONTENT_CONTEXT,
 CONTENT_NAME,
 CONTENT_DESCRIPTION,
 CONTENT_FILTER_ID1,
 CONTENT_FILTER_ID2,
 UPDATED_BY,
 UPDATED_ON,
 CREATED_BY,
 CREATED_ON) 
VALUES (
 ftd_apps.content_master_sq.nextval,
 'MARS_SBLOCK_EMAIL_CONTEXT',
 'MARS_SBLOCK_EMAIL_CONTENT',
 'Email content for GoTo florist meets Soft block condition',
 NULL,
 NULL,
 'MARSENH-4',
 SYSDATE,
 'MARSENH-4',
 SYSDATE
 );
 
 
SET DEFINE OFF
INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
 ftd_apps.content_detail_sq.nextval,
 (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'MARS_SBLOCK_EMAIL_CONTEXT'
 AND CONTENT_NAME = 'MARS_SBLOCK_EMAIL_CONTENT'),
 'GOTO_SBLOCK_SUBJECT',
 NULL,
 'Review Go To ~msgType~ &ndash; Potential holiday close &ndash; ~florist_id~',
 'MARSENH-4',
 SYSDATE,
 'MARSENH-4',
 SYSDATE
);
	
INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
 ftd_apps.content_detail_sq.nextval,
 (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'MARS_SBLOCK_EMAIL_CONTEXT'
 AND CONTENT_NAME = 'MARS_SBLOCK_EMAIL_CONTENT'),
 'GOTO_SBLOCK_BODY',
 NULL,
 '<span>The following Go To shop sent a ~msgTypeTxt~ which met system parameters to indicate potential holiday close, please research and take the appropriate action.<br/><br/><table cellspacing="10"><tr><th>Member Code</th><th>Order #</th><th>Mercury Order #</th><th>Soft Block Florist Conditions</th></tr><tr><td align="center">~membercode~</td><td align="center">~ordernumber~</td><td align="center">~mercuryordernumber~</td><td align="center">~softblockcondition~</td></tr></table><span>',
 'MARSENH-4',
 SYSDATE,
 'MARSENH-4',
 SYSDATE
);


INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_BY,
    UPDATED_ON,  
    DESCRIPTION)
VALUES (
    'MARS_CONFIG',
    'GOTO_SOFT_BLOCK_EMAIL',
    'distributiongroup@ftdi.com',
    sysdate,
    'MARSENH-4',
    'MARSENH-4',
    sysdate,
    'Recipient email address for the sending mail when GoTo shop meets soft block condition.');
	
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_BY,
    UPDATED_ON,  
    DESCRIPTION)
VALUES (
    'MARS_CONFIG',
    'VIEW_QUEUE_END_DATE_CITY',
    '{replace-me}',
    sysdate,
    'MARSENH-2',
    'MARSENH-2',
    sysdate,
    'New Block end date (Date Format - MM/dd/yyyy) for the florist City. Used by MARS for View Queue processing');

------------------------------------------------------------------------------------
-- END requested by gboddu                                   12/01/2016  --------
-- defect  MARSENH-2          (kmohamme   ) ( 252225 )       ----------------------------
-------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                12/ 1/2016  --------
-- Jira ticket AFE-9      (Mark   ) (252287 )           ----------------------------
------------------------------------------------------------------------------------

INSERT INTO AAS.RESOURCES (
    RESOURCE_ID,
    CONTEXT_ID,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY
) VALUES (
    'FM Fulfillment',
    'Order Proc',
    'The ability to use the "Select All" buttons on the Zip Codes and Cities tabs and also create permanent City and Codification blocks',
    sysdate,
    sysdate,
    'AFE-9'
);

INSERT INTO AAS.REL_ACL_RP (
    ACL_NAME,
    RESOURCE_ID,
    CONTEXT_ID,
    PERMISSION_ID,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY
) VALUES (
    'Fulfillment',
    'FM Fulfillment',
    'Order Proc',
    'Update',
    sysdate,
    sysdate,
    'AFE-9'
);

INSERT INTO AAS.REL_ACL_RP (
    ACL_NAME,
    RESOURCE_ID,
    CONTEXT_ID,
    PERMISSION_ID,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY
) VALUES (
    'AllResources',
    'FM Fulfillment',
    'Order Proc',
    'Update',
    sysdate,
    sysdate,
    'AFE-9'
);

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                12/ 1/2016  --------
-- Jira ticket AFE-9      (Mark   ) (252287 )           ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by msalla                                   12/02/2016  --------
-- defect  Q1SP17-15         (kmohamme   ) (252779 )       ----------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms values('DASHBOARD_CONFIG', 'ORDER_FLOW_THRESHOLD_PERCENTAGES', '-10;-10', 'Q1SP17-15', sysdate, 'Q1SP17-15', sysdate, 'THE THRESHOLD VALUES FOR ORDER FLOW GRAPH');

------------------------------------------------------------------------------------
-- END requested by msalla                                   12/02/2016  --------
-- defect  Q1SP17-15          (kmohamme   ) (252779 )       ----------------------------
-------------------------------------------------------------------------------------


-- =================================================================================
-- END RELEASE 10.0.0.3
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               12/ 1/2016  --------
-- Jira ticket Q1SP17-6   (Mark   ) (252636 )           ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
     VALUES ('SEQUENCE_CHECKER', 'PREFIX_IGNORE', '^A.+', 'SYS', sysdate, 'SYS', sysdate, 'Prefixes that sequence checker should ignore (should be formatted for use in regexp_like)');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               12/ 1/2016  --------
-- Jira ticket Q1SP17-6   (Mark   ) (252636 )           ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                   12/12/2016  --------
-- defect DDS-1           (Syed   ) (255900 )       ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 'SHIPPING_PARMS', 'FTDWEST_PRODUCT_SERVICE_TIMEOUT', '10000', 'DDS-1', sysdate, 'DDS-1', sysdate,'FTD WEST Product Service time out in milliseconds.');

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                   12/12/2016  --------
-- defect DDS-1           (Syed   ) (255900 )       ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.0.0.4
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                12/21/2016  --------
-- defect Q1SP17-36       (Mark   ) (261524 )       ----------------------------
------------------------------------------------------------------------------------

CREATE TABLE ftd_apps.HOLIDAY_PRODUCTS (
        PRODUCT_ID VARCHAR2(10) NOT NULL,
        DELIVERY_DATE DATE NOT NULL) tablespace ftd_apps_data;

alter table ftd_apps.holiday_products add CONSTRAINT HOLIDAY_PRODUCTS_PK PRIMARY KEY (PRODUCT_ID, DELIVERY_DATE)
using index tablespace ftd_apps_indx;
 
create index ftd_apps.holiday_products_n1 on ftd_apps.holiday_products (delivery_date) tablespace ftd_apps_indx;

grant select on ftd_apps.holiday_products to pas;

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                12/21/2016  --------
-- defect Q1SP17-36       (Mark   ) (261524 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                   12/22/2016  --------
-- defect DDS-4           (Syed   ) (262693 )       ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'SHIPPING_PARMS', 'SAVE_REQUEST_RESPONSE', 'Y', 'DDS-4', sysdate, 'DDS-4', sysdate,
'Indicator used for saving the service call request and response in the stats table.');

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'SHIPPING_PARMS', 'PROCESS_DELAY', 'Y', 'DDS-4', sysdate, 'DDS-4', sysdate,
'Indicator used for adding a delay to avoid numerous west product update batch processes running at the same time.');

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                   12/22/2016  --------
-- defect DDS-4           (Syed   ) (262693 )       ----------------------------
------------------------------------------------------------------------------------


-- =================================================================================
-- END RELEASE 10.0.0.6
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                12/22/2016  --------
-- defect SP-272          (Mark   ) (266441 )           ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values (
 'PHOENIX', 'PROCESS_DELAY_FOR_BULK_ONLY', '5000', 'SP-272', sysdate, 'SP-272', sysdate,
'Number of milliseconds to pause before calling PAS in Phoenix Process.');

update frp.global_parms
set name = 'PROCESS_DELAY', updated_on = sysdate
where context = 'PHOENIX' and name = 'PROCESS_DELAY_FOR_BULK_ONLY';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                12/22/2016  --------
-- defect SP-272          (Mark   ) (266441 )           ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.0.0.7
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 1/11/2017  --------
-- defect SP-272          (Mark   ) (273950 )           ----------------------------
------------------------------------------------------------------------------------

update frp.global_parms
set value = '20000', updated_on = sysdate
where context = 'PHOENIX' and name = 'PROCESS_DELAY';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                 1/11/2017  --------
-- defect SP-272          (Mark   ) (273950 )           ----------------------------
------------------------------------------------------------------------------------

