------------------------------------------------------------------------------------
-- begin requested by Karthikeyan Datchanamoorthy,              4 /20/2015  --------
-- defect #SYM-1   (Mark )  request #132993         --------------------------------
------------------------------------------------------------------------------------

use mercury;
alter table mercury_rules_tracker 
modify `RULES_FIRED`        varchar(200)  DEFAULT NULL,
modify `ACTION_TAKEN`       varchar(200)  DEFAULT NULL,
modify `ACTION_DESCRIPTION` varchar(500)  DEFAULT NULL,
modify `EXCEPTION_MESSAGE`  varchar(700)  DEFAULT NULL,
modify `MESSAGE_TEXT`       varchar(4000) DEFAULT NULL,
modify `SAK_TEXT`           varchar(600)  DEFAULT NULL,
modify `CUTOFF`             decimal(10,0) DEFAULT NULL,
modify `DELIVERY_ZIP_CODE`  varchar(12)   DEFAULT NULL,
modify `delivery_state`     varchar(20)   DEFAULT NULL,
modify `RECIPIENT_ZIP_CODE` varchar(12)   DEFAULT NULL,
modify `RECIPIENT_STATE`    varchar(20)   DEFAULT NULL,
modify `RECIPIENT_COUNTRY`  varchar(20)   DEFAULT NULL,
modify `CUSTOMER_ZIP_CODE`  varchar(12)   DEFAULT NULL,
modify `CUSTOMER_STATE`     varchar(20)   DEFAULT NULL,
modify `CUSTOMER_COUNTRY`   varchar(20)   DEFAULT NULL;
 
------------------------------------------------------------------------------------
-- end   requested by Karthikeyan Datchanamoorthy,              4 /20/2015  --------
-- defect #SYM-1   (Mark )  request #132993         --------------------------------
------------------------------------------------------------------------------------
