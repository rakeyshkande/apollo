------------------------------------------------------------------------------------ 
-- begin requested by Karthik D,                               4/20/2015  -------- 
-- defect #ML-37   (Pavan)             --------------------------------
------------------------------------------------------------------------------------
 
-- Alter columns on mercury.mercury_rules_tracker table.
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (RULES_FIRED        VARCHAR2(200));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (ACTION_TAKEN       VARCHAR2(200));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (ACTION_DESCRIPTION VARCHAR2(500));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (EXCEPTION_MESSAGE  VARCHAR2(700));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (MESSAGE_TEXT       VARCHAR2(4000));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (SAK_TEXT           VARCHAR2(600));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (CUTOFF             NUMBER(10));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (DELIVERY_ZIP_CODE  VARCHAR2(12));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (DELIVERY_STATE     VARCHAR2(20));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (RECIPIENT_ZIP_CODE VARCHAR2(12));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (RECIPIENT_STATE    VARCHAR2(20));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (RECIPIENT_COUNTRY  VARCHAR2(20));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (CUSTOMER_ZIP_CODE  VARCHAR2(12)); 
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (CUSTOMER_STATE     VARCHAR2(20));
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (CUSTOMER_COUNTRY   VARCHAR2(20));

-- Scheduler Jobs
INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('Mercury Inbound queue', 'OP', 'Manually insert the message into Mercury Inbound queue for re-processing', 
'This is to make the system to re-process the mercury message that got failed', null, 'MERCURY_ID', 'N', 'OJMS.MERCURY_INBOUND', 'SimpleJmsJob', 'Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('Mars Mercury queue', 'MARS', 'Manually insert the message into Mars Mercury queue for re-processing', 
'This is to make the system to re-process the mercury message that got failed', null, 'MERCURY_ID', 'N', 'OJMS.MARS_MERCURY', 'SimpleJmsJob', 'Y');

 
------------------------------------------------------------------------------------ 
-- end   requested by Karthik D,                               4/20/2015  -------- 
-- defect #ML-37   (Pavan)              --------------------------------
------------------------------------------------------------------------------------
 
