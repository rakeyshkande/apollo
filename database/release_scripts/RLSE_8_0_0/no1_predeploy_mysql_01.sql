------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

create database mercury;

use mercury;

create table mercury_rules_tracker (
RULES_TRACKER_ID bigint not null,
RULES_FIRED varchar(100) default null,
ACTION_TAKEN varchar(100) default null,
ACTION_DESCRIPTION varchar(20) default null,
ACTION_STATUS varchar(20) default null,
RULE_TYPE varchar(20) default null,
MERCURY_ID varchar(20) not null,
EXCEPTION_MESSAGE varchar(500) default null,
ORDER_DETAIL_ID decimal(12,0) not null,
ORDER_DATE datetime default null,
ORDER_ORIGIN varchar(10) default null,
SOURCE_CODE varchar(10) default null,
MESSAGE_TYPE varchar(10) default null,
DELIVERY_DATE datetime default null,
PRODUCT_ID varchar(10) default null,
MESSAGE_DIRECTION varchar(10) default null,
MESSAGE_TEXT varchar(200) default null,
SAK_TEXT varchar(200) default null,
CUTOFF decimal(5,0) default null,
OCCASION varchar(63) default null,
DELIVERY_ZIP_CODE varchar(10) default null,
DELIVERY_STATE varchar(2) default null,
DELIVERY_CITY varchar(30) default null,
RECIPIENT_FIRST_NAME varchar(30) default null,
RECIPIENT_LAST_NAME varchar(30) default null,
RECIPIENT_PHONE_NUMBER varchar(20) default null,
RECIPIENT_ADDRESS varchar(200) default null,
RECIPIENT_ZIP_CODE varchar(10) default null,
RECIPIENT_CITY varchar(30) default null,
RECIPIENT_STATE varchar(2) default null,
RECIPIENT_COUNTRY varchar(2) default null,
CUSTOMER_FIRST_NAME varchar(30) default null,
CUSTOMER_LAST_NAME varchar(30) default null,
CUSTOMER_PHONE_NUMBER varchar(20) default null,
CUSTOMER_ADDRESS varchar(200) default null,
CUSTOMER_ZIP_CODE varchar(10) default null,
CUSTOMER_CITY varchar(30) default null,
CUSTOMER_STATE varchar(2) default null,
CUSTOMER_COUNTRY varchar(2) default null,
MERCURY_NUMBER varchar(20) default null,
ORIGINAL_MERCURY_PRICE decimal(10,2) default null,
NEW_MERCURY_PRICE decimal(10,2) default null,
TIMEZONE varchar(8) default null,
PREFERRED_PARTNER_ID varchar(8) default null,
GLOBAL_REJECT_RETRY_LIMIT smallint default null,
ORDER_BOUNCE_COUNT smallint default null,
MARS_ORDER_BOUNCE_COUNT smallint default null,
AUTO_RESP_OFF_BEFORE_CUTOFF smallint default null,
FLORIST_SHOP_SOFT_BLOCK_DAYS smallint default null,
ASKP_THRESHOLD smallint default null,
KEY_PHRASES varchar(50) default null,
PHOENIX_ELIGIBLE_FLAG varchar(1) default null,
CREATED_ON datetime default null,
CREATED_BY varchar(100) default null,
UPDATED_ON datetime default null,
UPDATED_BY varchar(100) default null,
primary key (RULES_TRACKER_ID),
key mercury_rules_tracker_n1 (MERCURY_ID) using btree,
key mercury_rules_tracker_n2 (ORDER_DETAIL_ID) using btree
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- grant select on mercury.* to rpt users

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------
-- begin requested by Chandrana Abishek ,             4/3/2015  --------
-- defect #ML-30   (Mark )                            ------------------
------------------------------------------------------------------------

use mercury;
alter table mercury_rules_tracker add column FTD_COUNT smallint default null;
alter table mercury_rules_tracker add column ORDER_LIVE_FLAG varchar(2) default null;

------------------------------------------------------------------------
-- end   requested by Chandrana Abishek ,             4/3/2015  --------
-- defect #ML-30   (Mark )                            ------------------
------------------------------------------------------------------------
