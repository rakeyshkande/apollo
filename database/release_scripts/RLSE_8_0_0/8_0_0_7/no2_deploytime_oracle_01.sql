------------------------------------------------------------------------------------
-- begin requested by Chandna Abhishek ,                        3 /19/2015  --------
-- modified as per Abhishek's request(
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
INSERT INTO QUARTZ_SCHEMA.GROUPS
(GROUP_ID,DESCRIPTION,ACTIVE_FLAG)
VALUES
('MARS' ,'Mars' ,'Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('FLORIST_MESSAGE_SUMMARY_REPORT', 'MARS', 'Scheduling of Florist Message Summary Report', 'Reports Scheduling', '0 0 0 * * ?', ' Florist_Message_Summary_Report;P_FREQUENCY=86400', 'N', 'OJMS.MARS_REPORTS', 'SimpleJmsJob', 'Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('MARS_ACTION_DETAIL_REPORT', 'MARS', 'Scheduling of MARS Action Detail Report', 'Reports Scheduling', '3600', ' MARS_Action_Detail_Report;P_FREQUENCY=3600', 'N', 'OJMS.MARS_REPORTS', 'SimpleJmsJob', 'Y');


INSERT INTO frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES ('MARS_CONFIG','MARS_REPORTS_URL','TBD','DEFECT_1218',SYSDATE,'DEFECT_1218',SYSDATE,'For Accessing the MARS Reports');

commit;

----/ojms_objects/mars_reports.sql

------------------------------------------------------------------------------------
-- end   requested by Chandna Abhishek ,                        3 /19/2015  --------
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

grant select, flashback on mercury.mercury to ogg;
grant select, flashback on mercury.mercury_rules_tracker to ogg;

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

