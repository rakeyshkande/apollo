------------------------------------------------------------------------------------
-- begin requested by Meka Srivatsala,                          3 /13/2015  --------
-- Last updated by Kishor 					03/17/2015 ---------
-- defect #QE-37  (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE "CLEAN"."BAMS_SETTLEMENT_TOTALS$" (            
                "SETTLEMENT_TOTAL_ID" NUMBER NOT NULL ENABLE, 
                "FILE_CREATED_DATE_JULIAN" NUMBER NOT NULL ENABLE, 
                "SUBMISSION_ID" NUMBER NOT NULL ENABLE, 
                "BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
                "PAYMENT_TOTAL" NUMBER(11,2) NOT NULL ENABLE, 
                "REFUND_TOTAL" NUMBER(11,2) NOT NULL ENABLE, 
                "STATUS" VARCHAR2(20 BYTE), 
                "CREATED_ON" DATE NOT NULL ENABLE, 
                "CREATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                "UPDATED_ON" DATE, 
                "UPDATED_BY" VARCHAR2(100 BYTE),                 
                "FILE_NAME"  VARCHAR2(100),
                "ARCHIVED"   CHAR(1),
                "OPERATION$" VARCHAR2(20) NOT NULL ENABLE,
                "MODIFIED_TIME$"  TIMESTAMP(6) NOT NULL ENABLE                   
);

CREATE TABLE "CLEAN"."BILLING_HEADER$" (    
                "BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
                "BILLING_BATCH_DATE" DATE, 
                "PROCESSED_INDICATOR" CHAR(1 BYTE) DEFAULT 'Y' NOT NULL ENABLE, 
                "CREATED_ON" DATE, 
                "CC_AUTH_PROVIDER" VARCHAR2(100 BYTE), 
                "OPERATION$" VARCHAR2(20) NOT NULL ENABLE,
                "MODIFIED_TIME$"  TIMESTAMP(6) NOT NULL ENABLE   
);


------------------------------------------------------------------------------------
-- end   requested by Meka Srivatsala,                          3 /13/2015  --------
-- defect #QE-37  (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan,             3 /19/2015  --------
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_CLIENT_TIMEOUT', '60', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service client timeout');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_WSDL', 'TBD', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service wsdl name');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_URL', 'TBD', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service url');

commit;

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                               3 /19/2015  --------
-- defect #ML-7   (Pavan)              --------------------------------
------------------------------------------------------------------------------------

