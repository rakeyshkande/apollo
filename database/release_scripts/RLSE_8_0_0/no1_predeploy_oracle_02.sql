------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /23/2015  -------- 
-- defect #971   (mark)  request #130211            --------------------------------
------------------------------------------------------------------------------------

create table ftd_apps.codification_master_stage (
CODIFICATION_ID Varchar2(100) not null,
CATEGORY_ID Varchar2(100) not null,
DESCRIPTION Varchar2(100),
CATEGORY_TYPE Varchar2(100),
ACTIVE_FOR_DOTCOM Varchar2(100)) tablespace ftd_apps_data;

alter table ftd_apps.codification_master_stage add constraint codification_master_stage_pk primary key (codification_id)
using index tablespace ftd_apps_indx;

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               1 /23/2015  -------- 
-- defect #971   (mark)  request #130211            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /26/2015  -------- 
-- defect #971   (mark)  request #132228            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_MIN_ORDER_AMOUNT',
    '30',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Dollar amount threshold for .LOW codification');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_DESCRIPTION',
    'Low Priced Arrangement',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Description for .LOW codification');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_CATEGORY',
    'Other',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Category for .LOW codification');

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               1 /26/2015  -------- 
-- defect #971   (mark)  request #132228            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /27/2015  -------- 
-- defect #971   (mark)  request #132366            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'SAVE_TO_LOCATION',
    '/tmp',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Location on file system of member files');

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /27/2015  -------- 
-- defect #971   (mark)  request #132366            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         1 /30/2015  -------- 
--Modified by Pavan as per request id:SD 141326
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------


Insert into FRP.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
 values
('SYMPATHY_CONTROLS_CONFIG','SYMPATHY_CONTROLS_ACCOUNTS','FTD','DEFECT_595',SYSDATE,
'DEFECT_595',SYSDATE,'Sympathy Company List');

Insert into FRP.global_parms(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values
 ('SYMPATHY_CONTROLS_CONFIG','FUNERAL_CEMETERY_LOCATION_CHECK_FTD','Y','DEFECT_595',SYSDATE,
 'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for displaying messages and preventing CSR from placing drop ship orders to a Funeral/Cemetery.');

Insert into FRP.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values
 ('SYMPATHY_CONTROLS_CONFIG','HOSPITAL_LOCATION_CHECK_FTD','N','DEFECT_595',SYSDATE,
 'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for displaying messages and preventing CSR from placing drop ship orders to a Hospital.');

Insert into FRP.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
 values
 ('SYMPATHY_CONTROLS_CONFIG','FUNERAL_CEMETERY_LEAD_TIME_CHECK_FTD','Y','DEFECT_595', SYSDATE,
  'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for checking the lead time before accepting an order for a Funeral/ Cemetery service.');


insert into FTD_APPS.CONTENT_FILTER values (ftd_apps.content_filter_sq.nextval,'SYMPATHY_CONFIG','DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert INTO FTD_APPS.CONTENT_MASTER(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
VALUES(ftd_apps.content_master_sq.nextval,'SYMPATHY_CONFIG','JOE_MESSAGES','SYMPATHY JOE ERROR MESSAGES',
(select Content_Filter_Id from FTD_APPS.CONTENT_FILTER where Content_Filter_Name='SYMPATHY_CONFIG'),
null,'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'FUNERAL_CEMETERY_LOC_CHECK', null,
'Funeral/Cemetery delivery not available for this product. Select a Florist delivered item and confirm with your customer.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'HOSPITAL_LOC_CHECK', null,'Hospital delivery not available for this product. Select a Florist delivered item and confirm with your customer.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

  insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'FUNERAL_CEMETERY_LEAD_TIME_CHECK', null,
'The service time is less than [FUNERAL_CEMETERY_LEAD_TIME] hours. Attempt to locate a shop to complete delivery by the service time or contact customerto discuss alternate delivery options.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);
 

------------------------------------------------------------------------------------ 
-- end   requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               2 / 5/2015  -------- 
-- defect #QE-7  (mark)  request #134294            --------------------------------
------------------------------------------------------------------------------------

create table FTD_APPS.FLORIST_HOURS_STAGE (
    FLORIST_ID Varchar2(9) not null,
    DAY_OF_WEEK Varchar2(10) not null,
    OPEN_CLOSE_CODE  Varchar2(10),
    OPEN_TIME Varchar2(20),
    CLOSE_TIME Varchar2(20)) tablespace ftd_apps_data;

alter table ftd_apps.florist_hours_stage add constraint florist_hours_stage_pk primary key (florist_id, day_of_week) 
using index tablespace ftd_apps_indx;

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               2 / 5/2015  -------- 
-- defect #QE-7  (mark)  request #134294            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------ 
-- begin requested by Datchanamoorthy Karthikeyan,                               2 / 27/2015  -------- 
-- defect #ML-4  (pavan)             --------------------------------
------------------------------------------------------------------------------------

CREATE SEQUENCE MERCURY.RULES_TRACKER_ID_SQ
START WITH     1000 cache 200;

grant references on CLEAN.ORDER_DETAILS to mercury;

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'ARTIFACT_ID', 'mars-rules', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Artifact Id');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'GROUP_ID', 'com.ftd', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'GROUP_ID');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'VERSION', '1.0', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'VERSION');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'SCAN_INTERVAL', '1000', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'SCAN_INTERVAL');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'IS_MARS_ENABLED', 'N', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'IS_MARS_ENABLED');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'COMMENT_ORIGIN', 'MARS', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'COMMENT_ORIGIN');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'FLORIST_SOFT_BLOCK_DAYS', 'MARS', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'FLORIST_SOFT_BLOCK_DAYS');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','MARS_BRE_URL','TBD','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'BRE URL GLOBAL PARAM');

insert into clean.comment_origin_val values('MARS','MARS');

------------------------------------------------------------------------------------ 
-- end   requested by Datchanamoorthy Karthikeyan,                              2 /27/2015  -------- 
-- defect #ML-4  (pavan)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Abishek Chandna,                           3/ 3/2015  -------- 
-- defect #unknown (pavan)                          --------------------------------
------------------------------------------------------------------------------------

@ojms_objects/mars_mercury.sql

------------------------------------------------------------------------------------ 
-- end   requested by Abishek Chandna,                           3/ 3/2015  -------- 
-- defect #unknown (pavan)                          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138967            --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values ('ACCOUNTING_CONFIG','BAMS_END_OF_DAY_BATCH_OFFSET_DATE','0' ,'defect_QE-6',Sysdate,'defect_QE-6',Sysdate,
'BAMS End of Day Batch Date Offset.  -1 assumes EOD runs after midnight.  0 assumes EOD runs before midnight');

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138967            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Meka Srivatsala,                          3 /13/2015  --------
-- Last updated by Kishor 					03/17/2015 ---------
-- defect #QE-37  (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE "CLEAN"."BAMS_SETTLEMENT_TOTALS$" (            
                "SETTLEMENT_TOTAL_ID" NUMBER NOT NULL ENABLE, 
                "FILE_CREATED_DATE_JULIAN" NUMBER NOT NULL ENABLE, 
                "SUBMISSION_ID" NUMBER NOT NULL ENABLE, 
                "BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
                "PAYMENT_TOTAL" NUMBER(11,2) NOT NULL ENABLE, 
                "REFUND_TOTAL" NUMBER(11,2) NOT NULL ENABLE, 
                "STATUS" VARCHAR2(20 BYTE), 
                "CREATED_ON" DATE NOT NULL ENABLE, 
                "CREATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                "UPDATED_ON" DATE, 
                "UPDATED_BY" VARCHAR2(100 BYTE),                 
                "FILE_NAME"  VARCHAR2(100),
                "ARCHIVED"   CHAR(1),
                "OPERATION$" VARCHAR2(20) NOT NULL ENABLE,
                "MODIFIED_TIME$"  TIMESTAMP(6) NOT NULL ENABLE                   
);

CREATE TABLE "CLEAN"."BILLING_HEADER$" (    
                "BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
                "BILLING_BATCH_DATE" DATE, 
                "PROCESSED_INDICATOR" CHAR(1 BYTE) DEFAULT 'Y' NOT NULL ENABLE, 
                "CREATED_ON" DATE, 
                "CC_AUTH_PROVIDER" VARCHAR2(100 BYTE), 
                "OPERATION$" VARCHAR2(20) NOT NULL ENABLE,
                "MODIFIED_TIME$"  TIMESTAMP(6) NOT NULL ENABLE   
);


------------------------------------------------------------------------------------
-- end   requested by Meka Srivatsala,                          3 /13/2015  --------
-- defect #QE-37  (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan,             3 /19/2015  --------
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_CLIENT_TIMEOUT', '60', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service client timeout');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_WSDL', 'TBD', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service wsdl name');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES 
('MARS_CONFIG',  'WEB_SERVICE_URL', 'TBD', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Web service url');

commit;

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                               3 /19/2015  --------
-- defect #ML-7   (Pavan)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                          3 /27/2015  --------
-- defect #ML-22   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','MARS_ORDER_BOUNCE_COUNT','2','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'Max. Mars retry count');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','BRE_TIMEOUT','10000','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'BRE Timeout value in millis');

delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'WEB_SERVICE_CLIENT_TIMEOUT';
delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'FLORIST_SOFT_BLOCK_DAYS';
delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'WEB_SERVICE_WSDL';


------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                          3 /27/2015  --------
-- defect #ML-22   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                          4 /01/2015  --------
-- defect #ML-30   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES
 ('DASHBOARD_CONFIG',  'JMS_QUEUE_MARS_MERCURY', 'order;MARS_MERCURY;MARS;100;1000;true', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Display Mars queue in the dashboard');

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                          4 /01/2015  --------
-- defect #ML-30    (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
 
 
 ------------------------------------------------------------------------------------
-- begin requested by Chandna Abhishek   ,                      3 /31/2015  --------
-- defect #1218  (Pavan)                    --------------------------------
------------------------------------------------------------------------------------

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_REPORTS','Order Proc','Controls access to view/update MARS Reports', sysdate, sysdate, 
'Defect_1218');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_REPORTS_URL CSR', sysdate, sysdate, 'Defect_1218', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_REPORTS_URL CSR','MARS_REPORTS','Order Proc','View',sysdate,sysdate,'Defect_1218');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_REPORTS_URL CSR' , 'Order Proc', 'MARS_REPORTS_URL CSR View', sysdate, sysdate, 'Defect_1218' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_REPORTS_URL CSR'),
    'MARS_REPORTS_URL CSR', sysdate, sysdate, 'Defect_1218');


insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_BRE','Order Proc','Controls access to view/update MARS BRE', sysdate, sysdate, 
'Defect_1218');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_BRE_URL CSR', sysdate, sysdate, 'Defect_1218', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_BRE_URL CSR','MARS_BRE','Order Proc','View',sysdate,sysdate,'Defect_1218');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_BRE_URL CSR' , 'Order Proc', 'MARS_BRE_URL CSR View', sysdate, sysdate, 'Defect_1218' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_BRE_URL CSR'),
    'MARS_BRE_URL CSR', sysdate, sysdate, 'Defect_1218');

------------------------------------------------------------------------------------
-- end   requested by Chandna Abhishek   ,                      3 /31/2015  --------
-- defect #1218   (Pavan)                    --------------------------------
------------------------------------------------------------------------------------

 
