------------------------------------------------------------------------------------ 
-- begin requested by Gary Sergey,                              1 /29/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------

-- DB update to change existing Same-Day-Gift categories to Other.  Note there are only two (FRT & SAM).
update ftd_apps.codification_master
set category_id = 'Other'
where category_id = 'Same Day Gift';

-- DB update to remove Same-Day-Gift category (so it won't show up in drop-downs).
delete from ftd_apps.codification_categories
where category_id = 'Same Day Gift';

------------------------------------------------------------------------------------ 
-- end   requested by Gary Sergey,                              1 /29/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

-- THIS IS A REPLICATION CHANGE
ALTER TABLE FTD_APPS.SOURCE_MASTER
ADD (funeral_cemetery_loc_chk char(1) default 'N',
     hospital_loc_chck char(1) default 'N',
     funeral_cemetery_lead_time_chk char(1) default 'N',
     funeral_cemetery_lead_time number default 6,
     bo_hrs_mon_fri_start varchar2(5) default '9:00',
     bo_hrs_mon_fri_end varchar2(5) default '17:00',
     bo_hrs_sat_start varchar2(5) default '9:00',
     bo_hrs_sat_end varchar2(5) default '13:00',
     bo_hrs_sun_start varchar2(5) default '9:00',
     bo_hrs_sun_end varchar2(5) default '13:00');

------------------------------------------------------------------------------------ 
-- end   requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Mark Woytus,                              2 /26/2015  -------- 
-- defect #COD-3 (mark)  request #138047            --------------------------------
------------------------------------------------------------------------------------

drop trigger ftd_apps.hp_actions_shadow;
drop table ftd_apps.hp_actions$;

------------------------------------------------------------------------------------ 
-- end   requested by Mark Woytus,                              2 /26/2015  -------- 
-- defect #COD-3 (mark)  request #138047            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Bhargav Swamy                            2 / 26/2015  -------- 
-- defect #SYM-7  (pavan)  request #137878          --------------------------------
------------------------------------------------------------------------------------
 
--- SCRUB Error MESSAGES

insert INTO FTD_APPS.CONTENT_MASTER(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
VALUES(ftd_apps.content_master_sq.nextval,'SYMPATHY_CONFIG','SCRUB_MESSAGES','SYMPATHY SCRUB ALERT MESSAGES',
(select Content_Filter_Id from FTD_APPS.CONTENT_FILTER where Content_Filter_Name='SYMPATHY_CONFIG'),
null,'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'FUNERAL_CEMETERY_LOC_CHECK', null,
'Funeral/Cemetery delivery not recommended for this product. Attempt to convert the order to a Florist delivered item and confirm with your customer. You may submit this order.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'HOSPITAL_LOC_CHECK', null,
'Hospital delivery not recommended for this product. Attempt to convert the order to a Florist delivered item and confirm with your customer. You may submit this order.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'FUNERAL_CEMETERY_LEAD_TIME_CHECK', null,
'The service time is less than [FUNERAL_CEMETERY_LEAD_TIME] hours. Attempt to locate a shop to complete delivery by the service time or contact customer to discuss alternate delivery options.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

 --- TOS Column Scripts

ALTER TABLE JOE.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
ALTER TABLE SCRUB.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
-- THIS IS A REPLICATION CHANGE
ALTER TABLE CLEAN.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
------------------------------------------------------------------------------------ 
-- end   requested by Bhargav Swamy                             2 /26/2015  -------- 
-- defect #SYM-7  (pavan)  request #137878          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Chandna Abhishek ,                        3 /19/2015  --------
-- modified as per Abhishek's request(
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
INSERT INTO QUARTZ_SCHEMA.GROUPS
(GROUP_ID,DESCRIPTION,ACTIVE_FLAG)
VALUES
('MARS' ,'Mars' ,'Y')

INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('FLORIST_MESSAGE_SUMMARY_REPORT', 'MARS', 'Scheduling of Florist Message Summary Report', 'Reports Scheduling', '0 0 0 * * ?', ' Florist_Message_Summary_Report;P_FREQUENCY=86400', 'N', 'OJMS.MARS_REPORTS', 'SimpleJmsJob', 'Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG)
VALUES
('MARS_ACTION_DETAIL_REPORT', 'MARS', 'Scheduling of MARS Action Detail Report', 'Reports Scheduling', '3600', ' MARS_Action_Detail_Report;P_FREQUENCY=3600', 'N', 'OJMS.MARS_REPORTS', 'SimpleJmsJob', 'Y');


INSERT INTO frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES ('MARS_CONFIG','MARS_REPORTS_URL','TBD','DEFECT_1218',SYSDATE,'DEFECT_1218',SYSDATE,'For Accessing the MARS Reports');

commit;

@ojms_objects/mars_reports.sql

------------------------------------------------------------------------------------
-- end   requested by Chandna Abhishek ,                        3 /19/2015  --------
-- defect #ML-7   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

grant select, flashback on mercury.mercury_rules_tracker to ogg;

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,             3 /20/2015  --------
-- defect #ML-7   (Mark )                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep  B,                          3 /27/2015  --------
-- defect #SYM-44   (Pavan)   (SD 141326)                         --------------------------------
------------------------------------------------------------------------------------

SET DEFINE OFF;

update ftd_apps.source_master 
set Funeral_Cemetery_Loc_Chk = 'N',Hospital_Loc_Chck = 'N',Funeral_Cemetery_Lead_Time_Chk = 'Y', 
Funeral_Cemetery_Lead_Time = '4', Bo_Hrs_Mon_Fri_Start = '9:00',Bo_Hrs_Mon_Fri_End = '18:00', 
Bo_Hrs_Sat_Start = '9:00', Bo_Hrs_Sat_End = '18:00', Bo_Hrs_Sun_Start = '9:00', Bo_Hrs_Sun_End = '17:00'
where source_type not in ('USAA','BV FRIENDS AND FAMILY','BV GIFTS','LEGACY','SCI DROP SHIP ONLY',
'SCI FRIENDS AND FAMILY','SCI JEWISH GIFTS','SCI NO DIG MEM BRANDING','BATESVILLE','SCI FRIENDS & FAMILY');

SET DEFINE OFF;

update ftd_apps.source_master 
set Funeral_Cemetery_Loc_Chk = 'Y',Hospital_Loc_Chck = 'N',Funeral_Cemetery_Lead_Time_Chk = 'Y', 
Funeral_Cemetery_Lead_Time = '4', Bo_Hrs_Mon_Fri_Start = '9:00',Bo_Hrs_Mon_Fri_End = '18:00', 
Bo_Hrs_Sat_Start = '9:00', Bo_Hrs_Sat_End = '18:00', Bo_Hrs_Sun_Start = '9:00', Bo_Hrs_Sun_End = '17:00'
where source_type in ('USAA','BV FRIENDS AND FAMILY','BV GIFTS','LEGACY','SCI DROP SHIP ONLY',
'SCI FRIENDS AND FAMILY','SCI JEWISH GIFTS','SCI NO DIG MEM BRANDING','BATESVILLE','SCI FRIENDS & FAMILY');

------------------------------------------------------------------------------------
-- end   requested by Gunadeep  B,                          3 /27/2015  --------
-- defect #SYM-44   (Pavan)     (SD 141326)                       --------------------------------
------------------------------------------------------------------------------------

