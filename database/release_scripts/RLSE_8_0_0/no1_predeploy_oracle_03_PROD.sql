


------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala ,             27/2/2015  --------
-- defect #QE-3   (pavan)                            --------------------------------
------------------------------------------------------------------------------------
 INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'FTD_PUBLIC_ENCRYPTION_KEY', '/home/oracle/.ssh/FTD-Apollo-Prod-pgpkey.pub', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Public encryption key location');

INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'FTD_PRIVATE_DECRYPTION_KEY', '/home/oracle/.ssh/FTD-Apollo-Prod-pgpkey.sec', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Public decryption key location');

INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'BAMS_PTS_SETTLEMENT_FTDCOPY_LOC', '/u02/apollo/accounting/pts/ftd_encrypted/', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Copy of PTS file location');

------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala ,             27/2/2015  --------
-- defect #QE-3   (pavan)                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sergey Gary ,             3/26/2015  --------
-- defect #QE-25   (Pavan)   (Request # 141256)                          --------------------------------
------------------------------------------------------------------------------------
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('SECURITY_MANAGER_CONFIG', 'reportserver', 'apolloreports.ftdi.com', 
                              'QE-25', sysdate, 'QE-25', sysdate, 'Report server for Workforce Reports');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'OrderProcessingAddress', 'http://consumer.ftdi.com/OG/servlet/OrderGatherer', 
                              'QE-25', sysdate, 'QE-25', sysdate, 'Gatherer URL that B2B sends orders to for processing');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'DUNS', '070029199', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Ariba DUNS number assigned to FTD');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'ASNNumber', 'AN01000114321', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Ariba ASN number assigned to FTD');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'mailServer', 'cluster5out.us.messagelabs.com', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Mailserver to send B2B errors');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'EmailTo', 'javasupport@att.blackberry.net,gsergey@ftdi.com', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Comma separated list of emails for B2B errors');


------------------------------------------------------------------------------------
-- end   requested by Sergey Gary  ,             3/26/2015  --------
-- defect #QE-25   (Pavan)        (Request # 141256)                    --------------------------------
------------------------------------------------------------------------------------
