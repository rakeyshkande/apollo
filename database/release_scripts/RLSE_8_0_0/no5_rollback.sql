------------------------------------------------------------------------------------ 
-- rollback defect 971 request #132888              --------------------------------
------------------------------------------------------------------------------------

update ftd_apps.codification_master set category_id = 'Same Day Gift' where category_id = 'Other';

insert into ftd_apps.codification_categories values ('Same Day Gift','Same Day Gift products');

--================================================================================== 

------------------------------------------------------------------------------------
-- rollback defect #ML-7                            --------------------------------
------------------------------------------------------------------------------------

delete from QUARTZ_SCHEMA.GROUPS where GROUP_ID = 'MARS';

delete from QUARTZ_SCHEMA.PIPELINE where GROUP_ID = 'MARS';

--================================================================================== 

------------------------------------------------------------------------------------
-- rollback defect #QE-6 request #138991            --------------------------------
------------------------------------------------------------------------------------

alter user events identified by "onetime" account unlock;
connect events/onetime
set serveroutput on
declare
   v_job number;
begin
   select job into v_job from user_jobs where what = 'ins_run_bams_eod;';
   dbms_job.remove(job=>v_job);
end;
/

commit;
connect &dba_user
alter user events identified by values 'disabled' account lock;

--================================================================================== 
