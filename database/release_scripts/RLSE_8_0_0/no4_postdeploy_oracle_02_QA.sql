-------------------------------------------------------------------------
-- begin requested by Gabriela Gonzalez,             3 / 6/2015  --------
-- defect #QE-3   (pavan) request #139281            --------------------
-------------------------------------------------------------------------

-- THIS IS QA-ONLY (DOES NOT HAVE A PROD EQUIVALENT)
grant execute on events.ins_run_bams_eod to qa_update_role;

-------------------------------------------------------------------------
-- end   requested by Gabriela Gonzalez,             3 / 6/2015  --------
-- defect #QE-3   (pavan) request #139281            --------------------
-------------------------------------------------------------------------

