--------------------------------------------------------------------------
-- begin requested by Chandrana Abishek ,             4/3/2015  ----------
-- defect #ML-30   (Pavan)                            --------------------
--------------------------------------------------------------------------
 
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD FTD_COUNT NUMBER DEFAULT 1;
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD ORDER_LIVE_FLAG VARCHAR2(2) DEFAULT 'Y';

--------------------------------------------------------------------------
-- end   requested by Chandrana Abishek   ,             4/3/2015  --------
-- defect #ML-30   (Pavan)                           ---------------------
--------------------------------------------------------------------------
