
------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                          4 /01/2015  --------
-- defect #ML-30   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES
 ('DASHBOARD_CONFIG',  'JMS_QUEUE_MARS_MERCURY', 'order;MARS_MERCURY;MARS;100;1000;true', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Display Mars queue in the dashboard');

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                          4 /01/2015  --------
-- defect #ML-30    (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
 
 
 ------------------------------------------------------------------------------------
-- begin requested by Chandna Abhishek   ,                      3 /31/2015  --------
-- defect #1218  (Pavan)                    --------------------------------
------------------------------------------------------------------------------------

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_REPORTS','Order Proc','Controls access to view/update MARS Reports', sysdate, sysdate, 
'Defect_1218');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_REPORTS_URL CSR', sysdate, sysdate, 'Defect_1218', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_REPORTS_URL CSR','MARS_REPORTS','Order Proc','View',sysdate,sysdate,'Defect_1218');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_REPORTS_URL CSR' , 'Order Proc', 'MARS_REPORTS_URL CSR View', sysdate, sysdate, 'Defect_1218' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_REPORTS_URL CSR'),
    'MARS_REPORTS_URL CSR', sysdate, sysdate, 'Defect_1218');


insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_BRE','Order Proc','Controls access to view/update MARS BRE', sysdate, sysdate, 
'Defect_1218');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_BRE_URL CSR', sysdate, sysdate, 'Defect_1218', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_BRE_URL CSR','MARS_BRE','Order Proc','View',sysdate,sysdate,'Defect_1218');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_BRE_URL CSR' , 'Order Proc', 'MARS_BRE_URL CSR View', sysdate, sysdate, 'Defect_1218' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_BRE_URL CSR'),
    'MARS_BRE_URL CSR', sysdate, sysdate, 'Defect_1218');

------------------------------------------------------------------------------------
-- end   requested by Chandna Abhishek   ,                      3 /31/2015  --------
-- defect #1218   (Pavan)                    --------------------------------
------------------------------------------------------------------------------------

 