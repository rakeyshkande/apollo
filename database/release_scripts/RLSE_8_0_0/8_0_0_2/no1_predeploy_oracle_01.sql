------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               2 / 5/2015  -------- 
-- defect #QE-7  (mark)  request #134294            --------------------------------
------------------------------------------------------------------------------------

create table FTD_APPS.FLORIST_HOURS_STAGE (
    FLORIST_ID Varchar2(9) not null,
    DAY_OF_WEEK Varchar2(10) not null,
    OPEN_CLOSE_CODE  Varchar2(10),
    OPEN_TIME Varchar2(20),
    CLOSE_TIME Varchar2(20)) tablespace ftd_apps_data;

alter table ftd_apps.florist_hours_stage add constraint florist_hours_stage_pk primary key (florist_id, day_of_week) 
using index tablespace ftd_apps_indx;

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               2 / 5/2015  -------- 
-- defect #QE-7  (mark)  request #134294            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------ 
-- begin requested by Datchanamoorthy Karthikeyan,                               2 / 27/2015  -------- 
-- defect #ML-4  (pavan)             --------------------------------
------------------------------------------------------------------------------------

Create user mars identified by &password account lock default tablespace FTD_APPS_DATA temporary tablespace TEMP3 profile default;
grant create session to mars;

CREATE SEQUENCE MERCURY.RULES_TRACKER_ID_SQ
START WITH     1000 cache 200;

grant references on CLEAN.ORDER_DETAILS to mercury;

CREATE TABLE MERCURY.MERCURY_RULES_TRACKER
(RULES_TRACKER_ID INTEGER NOT NULL,
  RULES_FIRED VARCHAR2(100), 
  ACTION_TAKEN VARCHAR2(100),
  ACTION_DESCRIPTION VARCHAR2(20),
  ACTION_STATUS VARCHAR2(20),
  RULE_TYPE VARCHAR2(20),
  MERCURY_ID VARCHAR2(20) NOT NULL ,
EXCEPTION_MESSAGE VARCHAR2(500),
  ORDER_DETAIL_ID NUMBER NOT NULL,
ORDER_DATE DATE,
  ORDER_ORIGIN VARCHAR2(10),
  SOURCE_CODE VARCHAR2(10),
  MESSAGE_TYPE VARCHAR2(10),
  DELIVERY_DATE DATE,
  PRODUCT_ID VARCHAR2(10),
  MESSAGE_DIRECTION VARCHAR2(10),
  MESSAGE_TEXT VARCHAR2(200),
  SAK_TEXT VARCHAR2(200),
  CUTOFF NUMBER(5),
  OCCASION VARCHAR2(63),
  DELIVERY_ZIP_CODE VARCHAR2(10), 
  DELIVERY_STATE CHAR(2),
  DELIVERY_CITY VARCHAR2(30),
  RECIPIENT_FIRST_NAME VARCHAR2(30),
  RECIPIENT_LAST_NAME VARCHAR2(30),
  RECIPIENT_PHONE_NUMBER VARCHAR2(20),
  RECIPIENT_ADDRESS VARCHAR2(200),
  RECIPIENT_ZIP_CODE VARCHAR2(10),
  RECIPIENT_CITY VARCHAR2(30),
  RECIPIENT_STATE VARCHAR2(2),
  RECIPIENT_COUNTRY VARCHAR2(2),
  CUSTOMER_FIRST_NAME VARCHAR2(30),
  CUSTOMER_LAST_NAME VARCHAR2(30),
  CUSTOMER_PHONE_NUMBER VARCHAR2(20),
  CUSTOMER_ADDRESS VARCHAR2(200),
  CUSTOMER_ZIP_CODE VARCHAR2(10),
  CUSTOMER_CITY VARCHAR2(30),
  CUSTOMER_STATE VARCHAR2(2),
  CUSTOMER_COUNTRY VARCHAR2(2),
  MERCURY_NUMBER VARCHAR2(20),
ORIGINAL_MERCURY_PRICE NUMBER(10,2),
  NEW_MERCURY_PRICE NUMBER(10,2),
  TIMEZONE VARCHAR2(8),
  PREFERRED_PARTNER_ID VARCHAR2(8),
GLOBAL_REJECT_RETRY_LIMIT NUMBER,
ORDER_BOUNCE_COUNT NUMBER,
  MARS_ORDER_BOUNCE_COUNT NUMBER,
  AUTO_RESP_OFF_BEFORE_CUTOFF NUMBER,
  FLORIST_SHOP_SOFT_BLOCK_DAYS NUMBER,
ASKP_THRESHOLD NUMBER,
  KEY_PHRASES VARCHAR2(50),
  PHOENIX_ELIGIBLE_FLAG CHAR(1),
  CREATED_ON DATE,
CREATED_BY VARCHAR2(100),
UPDATED_ON DATE,
  UPDATED_BY VARCHAR2(100),
  CONSTRAINT MERCURY_RULES_TRACKER_PK PRIMARY KEY (RULES_TRACKER_ID) using index tablespace mercury_indx,
  CONSTRAINT MERCURY_FK FOREIGN KEY (MERCURY_ID) REFERENCES MERCURY.MERCURY(MERCURY_ID)
) tablespace mercury_data;

create index mercury.mercury_rules_tracker_n1 on mercury.mercury_rules_tracker (mercury_id) tablespace mercury_indx;

create index mercury.mercury_rules_tracker_n2 on mercury.mercury_rules_tracker (order_detail_id) tablespace mercury_indx;

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'ARTIFACT_ID', 'mars-rules', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'Artifact Id');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'GROUP_ID', 'com.ftd', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'GROUP_ID');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'VERSION', '1.0', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'VERSION');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'SCAN_INTERVAL', '1000', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'SCAN_INTERVAL');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'IS_MARS_ENABLED', 'N', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'IS_MARS_ENABLED');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'COMMENT_ORIGIN', 'MARS', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'COMMENT_ORIGIN');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'FLORIST_SOFT_BLOCK_DAYS', 'MARS', 'Defect_1218', sysdate, 'Defect_1218', sysdate, 'FLORIST_SOFT_BLOCK_DAYS');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','MARS_BRE_URL','TBD','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'BRE URL GLOBAL PARAM');

insert into clean.comment_origin_val values('MARS','MARS');

------------------------------------------------------------------------------------ 
-- end   requested by Datchanamoorthy Karthikeyan,                              2 /27/2015  -------- 
-- defect #ML-4  (pavan)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Abishek Chandna,                           3/ 3/2015  -------- 
-- defect #unknown (pavan)                          --------------------------------
------------------------------------------------------------------------------------

@ojms_objects/mars_mercury.sql

------------------------------------------------------------------------------------ 
-- end   requested by Abishek Chandna,                           3/ 3/2015  -------- 
-- defect #unknown (pavan)                          --------------------------------
------------------------------------------------------------------------------------

