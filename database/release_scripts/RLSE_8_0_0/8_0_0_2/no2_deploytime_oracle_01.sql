------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               2 / 4/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------

-- DB update to change existing Same-Day-Gift categories to Other.  Note there are only two (FRT & SAM).
update ftd_apps.codification_master
set category_id = 'Other'
where category_id = 'Same Day Gift';

-- DB update to remove Same-Day-Gift category (so it won't show up in drop-downs).
delete from ftd_apps.codification_categories
where category_id = 'Same Day Gift';

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               2 / 4/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Mark Woytus,                              2 /26/2015  -------- 
-- defect #COD-3 (mark)  request #138047            --------------------------------
------------------------------------------------------------------------------------

drop trigger ftd_apps.hp_actions_shadow;
drop table ftd_apps.hp_actions$;

------------------------------------------------------------------------------------ 
-- end   requested by Mark Woytus,                              2 /26/2015  -------- 
-- defect #COD-3 (mark)  request #138047            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Bhargav Swamy                            2 / 26/2015  -------- 
-- defect #SYM-7  (pavan)  request #137878          --------------------------------
------------------------------------------------------------------------------------
 
--- SCRUB Error MESSAGES

insert INTO FTD_APPS.CONTENT_MASTER(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
VALUES(ftd_apps.content_master_sq.nextval,'SYMPATHY_CONFIG','SCRUB_MESSAGES','SYMPATHY SCRUB ALERT MESSAGES',
(select Content_Filter_Id from FTD_APPS.CONTENT_FILTER where Content_Filter_Name='SYMPATHY_CONFIG'),
null,'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'FUNERAL_CEMETERY_LOC_CHECK', null,
'Funeral/Cemetery delivery not recommended for this product. Attempt to convert the order to a Florist delivered item and confirm with your customer. You may submit this order.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'HOSPITAL_LOC_CHECK', null,
'Hospital delivery not recommended for this product. Attempt to convert the order to a Florist delivered item and confirm with your customer. You may submit this order.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'SCRUB_MESSAGES'),
'FUNERAL_CEMETERY_LEAD_TIME_CHECK', null,
'The service time is less than [FUNERAL_CEMETERY_LEAD_TIME] hours. Attempt to locate a shop to complete delivery by the service time or contact customer to discuss alternate delivery options.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

 --- TOS Column Scripts

ALTER TABLE JOE.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
ALTER TABLE SCRUB.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
ALTER TABLE CLEAN.ORDER_DETAILS ADD (time_of_service varchar2(10) default null);
------------------------------------------------------------------------------------ 
-- end   requested by Bhargav Swamy                             2 /26/2015  -------- 
-- defect #SYM-7  (pavan)  request #137878          --------------------------------
------------------------------------------------------------------------------------

