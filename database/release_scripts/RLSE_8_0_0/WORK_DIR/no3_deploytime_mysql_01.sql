------------------------------------------------------------------------
-- begin requested by Chandrana Abishek ,             4/3/2015  --------
-- defect #ML-30   (Pavan)                            ------------------
------------------------------------------------------------------------

use mercury;
alter table mercury_rules_tracker add column FTD_COUNT smallint default 1;
alter table mercury_rules_tracker add column ORDER_LIVE_FLAG varchar(2) default 'Y';

------------------------------------------------------------------------
-- begin requested by Chandrana Abishek ,             4/3/2015  --------
-- defect #ML-30   (Pavan)                            ------------------
------------------------------------------------------------------------
