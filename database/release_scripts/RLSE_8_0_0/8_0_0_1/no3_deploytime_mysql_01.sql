------------------------------------------------------------------------------------
-- begin requested by Raghuram salguti,                         1 /30/2015  --------
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
ALTER TABLE source_master
ADD (funeral_cemetery_loc_chk char(1),
     hospital_loc_chck char(1),
     funeral_cemetery_lead_time_chk char(1),
     funeral_cemetery_lead_time smallint,
     bo_hrs_mon_fri_start varchar(5),
     bo_hrs_mon_fri_end varchar(5),
     bo_hrs_sat_start varchar(5),
     bo_hrs_sat_end varchar(5),
     bo_hrs_sun_start varchar(5),
     bo_hrs_sun_end varchar(5));

------------------------------------------------------------------------------------
-- end   requested by Raghuram salguti,                         1 /30/2015  --------
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------
