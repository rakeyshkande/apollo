------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /26/2015  -------- 
-- defect #971   (mark)  request #132228            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_MIN_ORDER_AMOUNT',
    '30',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Dollar amount threshold for .LOW codification');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_DESCRIPTION',
    'Low Priced Arrangement',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Description for .LOW codification');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'LOW_CODIFICATION_CATEGORY',
    'Other',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Category for .LOW codification');

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               1 /26/2015  -------- 
-- defect #971   (mark)  request #132228            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /27/2015  -------- 
-- defect #971   (mark)  request #132366            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'LOAD_MEMBER_DATA_CONFIG',
    'SAVE_TO_LOCATION',
    '/tmp',
    'DEFECT_971',
    sysdate,
    'DEFECT_971',
    sysdate,
    'Location on file system of member files');

------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /27/2015  -------- 
-- defect #971   (mark)  request #132366            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         1 /30/2015  -------- 
--Modified by Pavan as per request id:SD 141326
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------


Insert into FRP.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
 values
('SYMPATHY_CONTROLS_CONFIG','SYMPATHY_CONTROLS_ACCOUNTS','FTD','DEFECT_595',SYSDATE,
'DEFECT_595',SYSDATE,'Sympathy Company List');

Insert into FRP.global_parms(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values
 ('SYMPATHY_CONTROLS_CONFIG','FUNERAL_CEMETERY_LOCATION_CHECK_FTD','Y','DEFECT_595',SYSDATE,
 'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for displaying messages and preventing CSR from placing drop ship orders to a Funeral/Cemetery.');

Insert into FRP.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values
 ('SYMPATHY_CONTROLS_CONFIG','HOSPITAL_LOCATION_CHECK_FTD','N','DEFECT_595',SYSDATE,
 'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for displaying messages and preventing CSR from placing drop ship orders to a Hospital.');

Insert into FRP.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
 values
 ('SYMPATHY_CONTROLS_CONFIG','FUNERAL_CEMETERY_LEAD_TIME_CHECK_FTD','Y','DEFECT_595', SYSDATE,
  'DEFECT_595',SYSDATE,
 'This control will provide the ability for an Apollo admin to turn ON/OFF  functionality for checking the lead time before accepting an order for a Funeral/ Cemetery service.');


insert into FTD_APPS.CONTENT_FILTER values (ftd_apps.content_filter_sq.nextval,'SYMPATHY_CONFIG','DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert INTO FTD_APPS.CONTENT_MASTER(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
VALUES(ftd_apps.content_master_sq.nextval,'SYMPATHY_CONFIG','JOE_MESSAGES','SYMPATHY JOE ERROR MESSAGES',
(select Content_Filter_Id from FTD_APPS.CONTENT_FILTER where Content_Filter_Name='SYMPATHY_CONFIG'),
null,'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'FUNERAL_CEMETERY_LOC_CHECK', null,
'Funeral/Cemetery delivery not available for this product. Select a Florist delivered item and confirm with your customer.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'HOSPITAL_LOC_CHECK', null,'Hospital delivery not available for this product. Select a Florist delivered item and confirm with your customer.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);

  insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values 
(ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTERWHERE CONTENT_CONTEXT = 'SYMPATHY_CONFIG' AND CONTENT_NAME = 'JOE_MESSAGES'),
'FUNERAL_CEMETERY_LEAD_TIME_CHECK', null,
'The service time is less than [FUNERAL_CEMETERY_LEAD_TIME] hours. Attempt to locate a shop to complete delivery by the service time or contact customerto discuss alternate delivery options.',
'DEFECT_595',sysdate,'DEFECT_595',sysdate);
 

------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Gary Sergey,                              1 /29/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------

-- DB update to change existing Same-Day-Gift categories to Other.  Note there are only two (FRT & SAM).
update ftd_apps.codification_master
set category_id = 'Other'
where category_id = 'Same Day Gift';

-- DB update to remove Same-Day-Gift category (so it won't show up in drop-downs).
delete from ftd_apps.codification_categories
where category_id = 'Same Day Gift';

------------------------------------------------------------------------------------ 
-- end   requested by Gary Sergey,                              1 /29/2015  -------- 
-- defect #971   (mark)  request #132888            --------------------------------
------------------------------------------------------------------------------------

