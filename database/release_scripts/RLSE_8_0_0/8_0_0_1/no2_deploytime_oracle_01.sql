------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               1 /23/2015  -------- 
-- defect #971   (mark)  request #130211            --------------------------------
------------------------------------------------------------------------------------

create table ftd_apps.codification_master_stage (
CODIFICATION_ID Varchar2(100) not null,
CATEGORY_ID Varchar2(100) not null,
DESCRIPTION Varchar2(100),
CATEGORY_TYPE Varchar2(100),
ACTIVE_FOR_DOTCOM Varchar2(100)) tablespace ftd_apps_data;

alter table ftd_apps.codification_master_stage add constraint codification_master_stage_pk primary key (codification_id)
using index tablespace ftd_apps_indx;

------------------------------------------------------------------------------------ 
-- end   requested by Tim Schmig,                               1 /23/2015  -------- 
-- defect #971   (mark)  request #130211            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE FTD_APPS.SOURCE_MASTER
ADD (funeral_cemetery_loc_chk char(1) default 'N',
     hospital_loc_chck char(1) default 'N',
     funeral_cemetery_lead_time_chk char(1) default 'N',
     funeral_cemetery_lead_time number default 6,
     bo_hrs_mon_fri_start varchar2(5) default '9:00',
     bo_hrs_mon_fri_end varchar2(5) default '17:00',
     bo_hrs_sat_start varchar2(5) default '9:00',
     bo_hrs_sat_end varchar2(5) default '13:00',
     bo_hrs_sun_start varchar2(5) default '9:00',
     bo_hrs_sun_end varchar2(5) default '13:00');

------------------------------------------------------------------------------------ 
-- end   requested by Raghuram salguti,                         1 /30/2015  -------- 
-- defect #SYM-1   (Pavan)  request #132993         --------------------------------
------------------------------------------------------------------------------------
