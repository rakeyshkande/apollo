------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138967            --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values ('ACCOUNTING_CONFIG','BAMS_END_OF_DAY_BATCH_OFFSET_DATE','0' ,'defect_QE-6',Sysdate,'defect_QE-6',Sysdate,
'BAMS End of Day Batch Date Offset.  -1 assumes EOD runs after midnight.  0 assumes EOD runs before midnight');

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138967            --------------------------------
------------------------------------------------------------------------------------

