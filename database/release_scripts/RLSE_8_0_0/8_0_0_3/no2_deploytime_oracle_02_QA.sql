------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala ,             27/2/2015  --------
-- defect #QE-3   (pavan)                            --------------------------------
------------------------------------------------------------------------------------
 INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'FTD_PUBLIC_ENCRYPTION_KEY', '/home/oracle/.ssh/ftd-apollo-test-pub.asc', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Public encryption key location');

INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'FTD_PRIVATE_DECRYPTION_KEY', '/home/oracle/.ssh/ftd-apollo-test-sec.asc', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Public decryption key location');

INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES 
('ACCOUNTING_CONFIG', 'BAMS_PTS_SETTLEMENT_FTDCOPY_LOC', '/u02/apollo/accounting/pts/ftd_encrypted/', 'DEFECT_QE-3', SYSDATE, 'DEFECT_QE-3', Sysdate, 
'FTD Copy of PTS file location');

------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala ,             27/2/2015  --------
-- defect #QE-3   (pavan)                            --------------------------------
------------------------------------------------------------------------------------