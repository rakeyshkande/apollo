------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138991            --------------------------------
------------------------------------------------------------------------------------

alter user events identified by "onetime" account unlock;
connect events/onetime
set serveroutput on
declare
   v_job number;
begin
   dbms_job.submit(job=>v_job,what=>'ins_run_bams_eod;',next_date=>trunc(sysdate)+1+(23/24),interval=>'trunc(sysdate)+1+(23/24)');
   dbms_output.put_line(to_char(v_job));
end;
/

commit;
connect &dba_user
alter user events identified by values 'disabled' account lock;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138991            --------------------------------
------------------------------------------------------------------------------------

