------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                          3 /27/2015  --------
-- defect #ML-22   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------
insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','MARS_ORDER_BOUNCE_COUNT','2','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'Max. Mars retry count');

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG','BRE_TIMEOUT','10000','Defect_1218',SYSDATE,'Defect_1218',SYSDATE,'BRE Timeout value in millis');

delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'WEB_SERVICE_CLIENT_TIMEOUT';
delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'FLORIST_SOFT_BLOCK_DAYS';
delete from frp.global_parms where context  = 'MARS_CONFIG' and name = 'WEB_SERVICE_WSDL';


------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan,                          3 /27/2015  --------
-- defect #ML-22   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Gunadeep  B,                          3 /27/2015  --------
-- defect #SYM-44   (Pavan)   (SD 141326)                         --------------------------------
------------------------------------------------------------------------------------

SET DEFINE OFF;

update ftd_apps.source_master set Funeral_Cemetery_Loc_Chk = 'N',Hospital_Loc_Chck = 'N',Funeral_Cemetery_Lead_Time_Chk = 'Y', Funeral_Cemetery_Lead_Time = '4', Bo_Hrs_Mon_Fri_Start = '9:00',Bo_Hrs_Mon_Fri_End = '18:00', Bo_Hrs_Sat_Start = '9:00', Bo_Hrs_Sat_End = '18:00', Bo_Hrs_Sun_Start = '9:00', Bo_Hrs_Sun_End = '17:00'
where source_type not in ('USAA','BV FRIENDS AND FAMILY','BV GIFTS','LEGACY','SCI DROP SHIP ONLY','SCI FRIENDS AND FAMILY','SCI JEWISH GIFTS','SCI NO DIG MEM BRANDING','BATESVILLE','SCI FRIENDS & FAMILY');

SET DEFINE OFF;

update ftd_apps.source_master set Funeral_Cemetery_Loc_Chk = 'Y',Hospital_Loc_Chck = 'N',Funeral_Cemetery_Lead_Time_Chk = 'Y', Funeral_Cemetery_Lead_Time = '4', Bo_Hrs_Mon_Fri_Start = '9:00',Bo_Hrs_Mon_Fri_End = '18:00', Bo_Hrs_Sat_Start = '9:00', Bo_Hrs_Sat_End = '18:00', Bo_Hrs_Sun_Start = '9:00', Bo_Hrs_Sun_End = '17:00'
where source_type in ('USAA','BV FRIENDS AND FAMILY','BV GIFTS','LEGACY','SCI DROP SHIP ONLY','SCI FRIENDS AND FAMILY','SCI JEWISH GIFTS','SCI NO DIG MEM BRANDING','BATESVILLE','SCI FRIENDS & FAMILY');

------------------------------------------------------------------------------------
-- end   requested by Gunadeep  B,                          3 /27/2015  --------
-- defect #SYM-44   (Pavan)     (SD 141326)                       --------------------------------
------------------------------------------------------------------------------------