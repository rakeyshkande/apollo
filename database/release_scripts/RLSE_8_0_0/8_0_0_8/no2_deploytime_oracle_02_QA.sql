------------------------------------------------------------------------------------
-- begin requested by Sergey Gary ,             3/26/2015  --------
-- defect #QE-25   (Pavan)   (Request # 141256)                          --------------------------------
------------------------------------------------------------------------------------
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('SECURITY_MANAGER_CONFIG', 'reportserver', 'neon.ftdi.com:7778', 
                              'QE-25', sysdate, 'QE-25', sysdate, 'Report server for Workforce Reports');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'OrderProcessingAddress', 'http://consumer-test.ftdi.com/OG/servlet/OrderGatherer', 
                              'QE-25', sysdate, 'QE-25', sysdate, 'Gatherer URL that B2B sends orders to for processing');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'DUNS', '070029199-T', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Ariba DUNS number assigned to FTD');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'ASNNumber', 'AN01000114321-T', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Ariba ASN number assigned to FTD');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'mailServer', 'sodium.ftdi.com', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Mailserver to send B2B errors');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
                      values ('B2B_ARIBA_CONFIG', 'EmailTo', 'qabuild@sodium.ftdi.com', 
                             'QE-25', sysdate, 'QE-25', sysdate, 'Comma separated list of emails for B2B errors');


------------------------------------------------------------------------------------
-- end   requested by Sergey Gary  ,             3/26/2015  --------
-- defect #QE-25   (Pavan)        (Request # 141256)                    --------------------------------
------------------------------------------------------------------------------------