------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138991            --------------------------------
------------------------------------------------------------------------------------

alter user events identified by "onetime" account unlock;
connect events/onetime
set serveroutput on
declare
   v_job number;
begin
   dbms_job.submit(job=>v_job,what=>'ins_run_bams_eod;',next_date=>trunc(sysdate)+1+(22/24)+(30/1440),interval=>'trunc(sysdate)+1+(22/24)+(30/1440)');
   dbms_output.put_line(to_char(v_job));
end;
/

commit;
connect &dba_user
alter user events identified by values 'disabled' account lock;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 / 5/2015  --------
-- defect #QE-6  (mark)  request #138991            --------------------------------
------------------------------------------------------------------------------------

--------------------------------------------------------------------------
-- begin requested by Chandrana Abishek ,             4/3/2015  ----------
-- defect #ML-30   (Pavan)                            --------------------
--------------------------------------------------------------------------
 
grant execute on CLEAN.ORDER_MESG_PKG to mercury;

--------------------------------------------------------------------------
-- end   requested by Chandrana Abishek   ,             4/3/2015  --------
-- defect #ML-30   (Pavan)                           ---------------------
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                    4/15/2015  ----------
-- defect #QE-6    (Mark )                            --------------------
--------------------------------------------------------------------------
 
-- Schedule /usr/local/admin/scripts/monitor/bams_settlement_file_did_not_send.sh to run daily at 00:05

--------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                    4/15/2015  ----------
-- defect #QE-6    (Mark )                            --------------------
--------------------------------------------------------------------------
