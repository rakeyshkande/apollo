set serveroutput on size 999999



select 'load_counts.sql starting at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');


--PROMPT  Truncate venus.inv_trk_count...
--truncate table venus.inv_trk_count;
--PROMPT  Truncate venus.inv_trk_assigned_count...
--truncate table venus.inv_trk_assigned_count;


--######################################################
-- Load venus.inv_trk_count AND venus.inv_trk_assigned_count
--
-- Logic:  Always load venus.inv_trk_count.
--         If vendor_id not null then load venus.inv_trk_assigned_count
--######################################################

--Input Parameter: IN_INV_START_DATE

declare
   IN_INV_START_DATE    date := trunc(sysdate-2); 
   V_ASSIGNED_COUNT_DATE date;
   inc_ftd_count        number;
   inc_can_count        number;
   inc_rej_count        number;
   v_inv_trk_num_recs   number;

   cursor venus_cur is
       select v.rowid,v.venus_id, v.product_id, v.vendor_id, v.msg_type, v.ship_date
       from venus.venus v
       where v.ship_date >= trunc(IN_INV_START_DATE)
         and v.msg_type in ('FTD','CAN','REJ')
         and v.product_id in 
              (select product_id from ftd_apps.product_master
               where product_id = v.product_id);

BEGIN
  -- dbms_output.put_line('In pl/sql script.  Load ship_dates >= ' || to_char(IN_INV_START_DATE,'MM/DD/YYYY'));
   FOR venus_rec in venus_cur LOOP
    --  dbms_output.put_line('Looping:  venus_id=' || venus_rec.venus_id ||
    --                               '   vendor=' || venus_rec.vendor_id ||
    --                               '   product=' || venus_rec.product_id ||
    --                               '   msg_type=' || venus_rec.msg_type);
      inc_ftd_count := 0;
      inc_can_count := 0;
      inc_rej_count := 0;

      if venus_rec.msg_type = 'FTD' then
         inc_ftd_count := 1;
      elsif venus_rec.msg_type = 'CAN' then
         inc_can_count := 1;
      else
         inc_rej_count := 1;
      end if;

--      dbms_output.put_line('FTD=' || inc_ftd_count ||
--                    '   CAN=' || inc_can_count ||
--                    '   REJ=' || inc_rej_count);


      ----------------------------------
      -- Update venus.inv_trk_count
      ----------------------------------
      select count(*)
      into v_inv_trk_num_recs
      from venus.inv_trk_count
      where product_id = venus_rec.product_id
        and ship_date  = venus_rec.ship_date;

      if v_inv_trk_num_recs > 0 then
     --    dbms_output.put_line('Updating venus.inv_trk_count');
         begin
            update venus.inv_trk_count
            set ftd_msg_count = ftd_msg_count + inc_ftd_count,
                can_msg_count = can_msg_count + inc_can_count,
                rej_msg_count = rej_msg_count + inc_rej_count
            where product_id = venus_rec.product_id
              and ship_date  = venus_rec.ship_date;
          exception
             when others then
                   dbms_output.put_line('Upd err to ITC ' || 
                                    ' p='   || venus_rec.product_id ||
                                    '   v=' || venus_rec.vendor_id  || 
                                    ' shpdt=' || venus_rec.ship_date ||
                                    '  ERR=' || sqlerrm);
          end;

      else
      --   dbms_output.put_line('Inserting to venus.inv_trk_count');
         begin
            insert into venus.inv_trk_count
               (product_id,ship_date,ftd_msg_count,can_msg_count,rej_msg_count)
            values (venus_rec.product_id, venus_rec.ship_date,
                     inc_ftd_count, inc_can_count, inc_rej_count);
         exception
            when others then
                          dbms_output.put_line('Ins err to ITC ' || 
                                    ' p='   || venus_rec.product_id ||
                                    '   v=' || venus_rec.vendor_id  || 
                                    ' shpdt=' || venus_rec.ship_date ||
                                    '  ERR=' || sqlerrm);
         end;

      end if;

      ----------------------------------
      -- Update venus.inv_trk_assigned_count
      ----------------------------------
      V_ASSIGNED_COUNT_DATE := NULL;

      if venus_rec.vendor_id IS NOT NULL then
       --  dbms_output.put_line('Need to insert/update venus.inv_trk_assigned_count for ' ||
       --                        venus_rec.vendor_id);

         V_ASSIGNED_COUNT_DATE := sysdate;

         select count(*)
         into v_inv_trk_num_recs
         from venus.inv_trk_assigned_count
         where product_id = venus_rec.product_id
           and vendor_id  = venus_rec.vendor_id
           and ship_date  = venus_rec.ship_date;

         if v_inv_trk_num_recs > 0 then
            begin
               update venus.inv_trk_assigned_count
               set ftd_msg_count = ftd_msg_count + inc_ftd_count,
                   can_msg_count = can_msg_count + inc_can_count,
                   rej_msg_count = rej_msg_count + inc_rej_count
               where product_id = venus_rec.product_id
                 and vendor_id  = venus_rec.vendor_id
                 and ship_date  = venus_rec.ship_date;
             exception
                when others then
                   dbms_output.put_line('Upd err to ITAC ' || 
                                    ' p='   || venus_rec.product_id ||
                                    '   v=' || venus_rec.vendor_id  || 
                                    ' shpdt=' || venus_rec.ship_date ||
                                    '  ERR=' || sqlerrm);

             end;

         else
            begin
               insert into venus.inv_trk_assigned_count
                  (product_id,vendor_id,ship_date,ftd_msg_count,can_msg_count,rej_msg_count)
               values (venus_rec.product_id, venus_rec.vendor_id, venus_rec.ship_date,
                        inc_ftd_count, inc_can_count, inc_rej_count);
             exception
                when others then
                   dbms_output.put_line('Ins err to ITAC ' || 
                                    ' p='   || venus_rec.product_id ||
                                    '   v=' || venus_rec.vendor_id  || 
                                    ' shpdt=' || venus_rec.ship_date ||
                                    '  ERR=' || sqlerrm);
             end;

         end if;

      end if;


      -- Update dates on venus.venus the record went to the tracking table
      update venus.venus
      set inv_trk_count_date = sysdate,
          inv_trk_assigned_count_date = V_ASSIGNED_COUNT_DATE
      where rowid = venus_rec.rowid;

   END LOOP;
   commit;
END;
/




