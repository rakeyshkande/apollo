
--
-- 2619 SKUIT
--
create table FTD_APPS.INV_TRK_EMAIL (
          inv_trk_email_id        NUMBER(11)         NOT NULL,            
          email_address           varchar2(100)      NOT NULL,    
          ACTIVE_FLAG             varchar2(1)    DEFAULT 'Y' NOT NULL,
          created_by              varchar2(100)      NOT NULL,
          created_on              date               NOT NULL,
          updated_by              varchar2(100)      NOT NULL,
          updated_on              date               NOT NULL,
    CONSTRAINT inv_trk_email_pk PRIMARY KEY(inv_trk_email_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT inv_trk_email_ck1   CHECK(active_flag IN('Y','N')))
 TABLESPACE ftd_apps_data;

comment on table ftd_apps.inv_trk_email is 'Stores all email addresses used by the inventory tracking (SKU-IT) system.  These addresses will be tied to vendors or inventory tracking data.';
comment on column ftd_apps.inv_trk_email.inv_trk_email_id is 'Synthetic key for emails - allows changing of an email address in child tables';
comment on column ftd_apps.inv_trk_email.email_address is 'Valid email address';
comment on column ftd_apps.inv_trk_email.active_flag is 'Denotes whether the email_address is actively used for any inventory tracking monitoring';
COMMENT ON COLUMN ftd_apps.inv_trk_email.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.inv_trk_email.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.inv_trk_email.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.inv_trk_email.updated_by is 'Date record was last updated';

create index ftd_apps.inv_trk_email_uf1 on ftd_apps.inv_trk_email(lower(email_address),active_flag) tablespace ftd_apps_indx;


CREATE SEQUENCE ftd_apps.inv_trk_email_sq 
    START WITH 1  INCREMENT BY 1  MINVALUE 1 MAXVALUE 99999999999 CYCLE CACHE 50;


create table FTD_APPS.INV_TRK_VENDOR_EMAIL (
          Vendor_id               varchar2(5)        NOT NULL,    
          inv_trk_email_id        number(11)         NOT NULL,    
          created_by              varchar2(100)      NOT NULL,
          created_on              date               NOT NULL,
          updated_by              varchar2(100)      NOT NULL,
          updated_on              date               NOT NULL,
    CONSTRAINT inv_trk_vendor_email_pk PRIMARY KEY(vendor_id,inv_trk_email_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT inv_trk_vendor_email_fk1 FOREIGN KEY(vendor_id)  REFERENCES ftd_apps.vendor_master(vendor_id),
    CONSTRAINT inv_trk_vendor_email_fk2 FOREIGN KEY(inv_trk_email_id)  REFERENCES ftd_apps.inv_trk_email(inv_trk_email_id))
 TABLESPACE ftd_apps_data;
    
comment on table ftd_apps.inv_trk_vendor_email is 'Stores vendor email addresses used by the inventory tracking (SKU-IT) system.';
comment on column ftd_apps.inv_trk_vendor_email.vendor_id     is 'Vendor id - validated by ftd_apps.vendor_master';
comment on column ftd_apps.inv_trk_vendor_email.inv_trk_email_id is 'Valid email address';
COMMENT ON COLUMN ftd_apps.inv_trk_vendor_email.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.inv_trk_vendor_email.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.inv_trk_vendor_email.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.inv_trk_vendor_email.updated_by is 'Date record was last updated';

create table FTD_APPS.INV_TRK (
          inv_trk_id                 NUMBER(11)         NOT NULL,            
          product_id                 varchar2(10)       NOT NULL,    
          vendor_id                  varchar2(5)        NOT NULL,    
          start_date                 date               NOT NULL,   
          end_date                   date               NOT NULL,    
          forecast_qty               number(7)          NOT NULL,    
          revised_forecast_qty       number(7)          NOT NULL,    
          warning_threshold_qty      number(7)          NULL,    
          shutdown_threshold_qty     number(7)          NULL,    
          availability_option_code   varchar2(5)   DEFAULT 'NONE' NOT NULL, 
          comments                   varchar(1000),    
          created_by                 varchar2(100)      NOT NULL,
          created_on                 date               NOT NULL,
          updated_by                 varchar2(100)      NOT NULL,
          updated_on                 date               NOT NULL,
    CONSTRAINT inv_trk_pk PRIMARY KEY(inv_trk_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT inv_trk_fk1 FOREIGN KEY(product_id)  REFERENCES ftd_apps.product_master(product_id),        
    CONSTRAINT inv_trk_fk2 FOREIGN KEY(vendor_id)   REFERENCES ftd_apps.vendor_master(vendor_id),
    CONSTRAINT inv_trk_ck1   CHECK(availability_option_code in ('NONE','NSW')))
 TABLESPACE ftd_apps_data;

create unique index ftd_apps.inv_trk_u1 on ftd_apps.inv_trk(product_id, vendor_id, start_date, end_date) tablespace ftd_apps_indx;
create index ftd_apps.inv_trk_n1 on ftd_apps.inv_trk(vendor_id) tablespace ftd_apps_indx;

comment on table ftd_apps.inv_trk is 'Stores SKU-IT inventory tracking information for a product/vendor/date range.';
comment on column ftd_apps.inv_trk.inv_trk_id     is 'Synthetic Key to mask Business Key of Product/Vendor/Start Date/End Date';
comment on column ftd_apps.inv_trk.product_id    is 'Product id - validated by ftd_apps.product_master';
comment on column ftd_apps.inv_trk.vendor_id     is 'Vendor id - validated by ftd_apps.vendor_master';
comment on column ftd_apps.inv_trk.start_date is 'starting date for this inventory control record';               
comment on column ftd_apps.inv_trk.end_date  is 'ending date for this inventory control record - NULL = no end date';            
comment on column ftd_apps.inv_trk.forecast_qty is 'original forecast';             
comment on column ftd_apps.inv_trk.revised_forecast_qty is 'revised forecast';         
comment on column ftd_apps.inv_trk.warning_threshold_qty is 'warning threshold';    
comment on column ftd_apps.inv_trk.shutdown_threshold_qty is 'point where product availability should be shut down';   
comment on column ftd_apps.inv_trk.availability_option_code is 'What to do if product is not available NONE=Nothing, NSW=Next Ship Week'; 
comment on column ftd_apps.inv_trk.comments = 'Optional comments';                 
COMMENT ON COLUMN ftd_apps.inv_trk.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.inv_trk.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.inv_trk.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.inv_trk.updated_by is 'Date record was last updated';


CREATE SEQUENCE ftd_apps.inv_trk_sq 
    START WITH 1  INCREMENT BY 1  MINVALUE 1 MAXVALUE 99999999999 CYCLE CACHE 50;




create table FTD_APPS.INV_TRK$ (
          inv_trk_id                 NUMBER(11)         NOT NULL,            
          product_id                 varchar2(10)       NOT NULL,    
          vendor_id                  varchar2(5)        NOT NULL,    
          start_date                 date               NOT NULL,   
          end_date                   date,    
          forecast_qty               number(7)          NOT NULL,    
          revised_forecast_qty       number(7)          NOT NULL,    
          warning_threshold_qty      number(7)          NULL,    
          shutdown_threshold_qty     number(7)          NULL,    
          availability_option_code   varchar2(5)        NOT NULL, 
          comments                   varchar(1000),    
          created_by                 varchar2(100)      NOT NULL,
          created_on                 date               NOT NULL,
          updated_by                 varchar2(100)      NOT NULL,
          updated_on                 date               NOT NULL,
          OPERATION$                 varchar2(7)        NOT NULL,
          TIMESTAMP$                 timestamp          NOT NULL)
 TABLESPACE ftd_apps_data;




create table FTD_APPS.INV_TRK_INV_CTRL_EMAIL (
          inv_trk_id              NUMBER(11)         NOT NULL,   
          inv_trk_email_id        NUMBER(11)         NOT NULL,    
          created_by              varchar2(100)      NOT NULL,
          created_on              date               NOT NULL,
          updated_by              varchar2(100)      NOT NULL,
          updated_on              date               NOT NULL,
    CONSTRAINT inv_trk_inv_ctrl_email_pk PRIMARY KEY(inv_trk_id,inv_trk_email_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT inv_trk_inv_ctrl_email_fk1 FOREIGN KEY(inv_trk_id)  REFERENCES ftd_apps.inv_trk(inv_trk_id),
    CONSTRAINT inv_trk_inv_ctrl_email_fk2 FOREIGN KEY(inv_trk_email_id)  REFERENCES ftd_apps.inv_trk_email(inv_trk_email_id))
 TABLESPACE ftd_apps_data;

comment on table ftd_apps.inv_trk_inv_ctrl_email is 'Stores inventory tracking email addresses used by the inventory tracking (SKU-IT) system.';
comment on column ftd_apps.inv_trk_inv_ctrl_email.inv_trk_id     is 'Inventory Tracking id - validated by ftd_apps.inv_trk';
comment on column ftd_apps.inv_trk_inv_ctrl_email.inv_trk_email_id is 'Valid email address';
COMMENT ON COLUMN ftd_apps.inv_trk_inv_ctrl_email.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.inv_trk_inv_ctrl_email.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.inv_trk_inv_ctrl_email.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.inv_trk_inv_ctrl_email.updated_by is 'Date record was last updated';


create table FTD_APPS.INV_TRK_INV_CTRL_EMAIL$ (
          inv_trk_id              NUMBER(11)         NOT NULL,   
          inv_trk_email_id        NUMBER(11)         NOT NULL,    
          created_by              varchar2(100)      NOT NULL,
          created_on              date               NOT NULL,
          updated_by              varchar2(100)      NOT NULL,
          updated_on              date               NOT NULL,
          OPERATION$              varchar2(7)        NOT NULL,
          TIMESTAMP$              timestamp          NOT NULL)
 TABLESPACE ftd_apps_data;




grant references on ftd_apps.vendor_master to venus;
grant references on ftd_apps.product_master to venus;

create table VENUS.INV_TRK_ASSIGNED_COUNT (
          product_id                 varchar2(10)       NOT NULL,    
          vendor_id                  varchar2(5)        NOT NULL,    
          ship_date                  date               NOT NULL,   
          ftd_msg_count              number(7)          NOT NULL,    
          can_msg_count              number(7)          NOT NULL,    
          rej_msg_count              number(7)          NOT NULL,    
    CONSTRAINT inv_trk_assigned_count_pk PRIMARY KEY(product_id,vendor_id,ship_date) 
            USING INDEX  TABLESPACE venus_INDX,
    CONSTRAINT inv_trk_assigned_count_fk1 FOREIGN KEY(product_id)  REFERENCES ftd_apps.product_master(product_id),        
    CONSTRAINT inv_trk_assigned_count_fk2 FOREIGN KEY(vendor_id)  REFERENCES ftd_apps.vendor_master(vendor_id))
 TABLESPACE venus_data;

create index venus.inv_trk_assigned_count_n1 on venus.inv_trk_assigned_count(vendor_id) tablespace venus_data;


comment on table venus.inv_trk_assigned_count is 'Stores real-time counts by product/vendor/date of product activity used by SKU-IT inventory tracking.';
comment on column venus.inv_trk_assigned_count.product_id    is 'Product being tracked - validated by ftd_apps.product_id';
comment on column venus.inv_trk_assigned_count.vendor_id     is 'Vendor being tracked - validated by ftd_apps.vendor_id';
comment on column venus.inv_trk_assigned_count.ship_date     is 'ship_date';
comment on column venus.inv_trk_assigned_count.ftd_msg_count     is 'Count of FTD messages received for this inventory control record.';
comment on column venus.inv_trk_assigned_count.can_msg_count     is 'Count of CAN messages received for this inventory control record.';
comment on column venus.inv_trk_assigned_count.rej_msg_count     is 'Count of REJ messages received for this inventory control record.';


create table venus.INV_TRK_COUNT (
          product_id                 varchar2(10)       NOT NULL,    
          ship_date                  date               NOT NULL,   
          ftd_msg_count              number(7)          NOT NULL,    
          can_msg_count              number(7)          NOT NULL,    
          rej_msg_count              number(7)          NOT NULL,    
    CONSTRAINT inv_trk_count_pk PRIMARY KEY(product_id,ship_date) 
            USING INDEX  TABLESPACE venus_INDX,
    CONSTRAINT inv_trk_count_fk1 FOREIGN KEY(product_id)  REFERENCES ftd_apps.product_master(product_id))
 TABLESPACE venus_data;

comment on table venus.inv_trk_count is 'Stores real-time counts by product/date of product activity used by SKU-IT inventory tracking.';
comment on column venus.inv_trk_count.product_id    is 'Product being tracked - validated by ftd_apps.product_id';
comment on column venus.inv_trk_count.ship_date     is 'ship_date';
comment on column venus.inv_trk_count.ftd_msg_count     is 'Count of FTD messages received for this inventory control record.';
comment on column venus.inv_trk_count.can_msg_count     is 'Count of CAN messages received for this inventory control record.';
comment on column venus.inv_trk_count.rej_msg_count     is 'Count of REJ messages received for this inventory control record.';



grant select,insert on venus.inv_trk_assigned_count to ftd_apps;
grant select,insert on venus.inv_trk_count to ftd_apps;





create table FTD_APPS.INV_TRK_EVENT_HISTORY (
          inv_trk_id                 NUMBER(11)         NOT NULL,           
          event_timestamp            TIMESTAMP          NOT NULL,
          event_type                 varchar2(100)      NOT NULL,
          updated_by                 varchar2(100)      NOT NULL,
          product_avail_start_date   date,
          product_avail_end_date     date,
    CONSTRAINT inv_trk_event_history_pk PRIMARY KEY(inv_trk_id,event_timestamp) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT inv_trk_event_history_fk1 FOREIGN KEY(inv_trk_id)  REFERENCES ftd_apps.inv_trk(inv_trk_id),        
    CONSTRAINT inv_trk_event_history_ck1   CHECK(event_type in ('LOCATION_SHUTDOWN','PRODUCT_SHUTDOWN','PRODUCT_AVAILABILITY_DATES')))
 TABLESPACE ftd_apps_data;


comment on table ftd_apps.inv_trk_EVENT_HISTORY is 'Captures system generated events related to SKU Inventory Tracking.';
comment on column ftd_apps.inv_trk_EVENT_HISTORY.inv_trk_id      is 'Inventory record which generated the event';
comment on column ftd_apps.inv_trk_EVENT_HISTORY.event_timestamp is 'Timestamp of when the event happened.';
comment on column ftd_apps.inv_trk_EVENT_HISTORY.event_type      is 'Type of Event';
comment on column ftd_apps.inv_trk_EVENT_HISTORY.updated_by      is 'Identifies the process which triggered the event.';               
comment on column ftd_apps.inv_trk_EVENT_HISTORY.product_avail_start_date  is 'From product_master.';               
comment on column ftd_apps.inv_trk_EVENT_HISTORY.product_avail_end_date  is 'From product_master.';               
               



create global temporary table ftd_apps.inv_trk_search_temp 
     (inv_trk_id                  number(11), 
      product_id                  varchar2(10),
      product_name                varchar2(25),
      product_status              varchar2(1),      
      product_UPDATED_BY          VARCHAR2(100),
      exception_start_date        date,
      vendor_id                   varchar2(5),
      vendor_name                 varchar2(50),
      vendor_product_status       varchar2(1), 
      vendor_product_UPDATED_BY                  VARCHAR2(100),
      shutdown_threshold_qty      number(7),    
      start_date                  date,
      end_date                    date,
      forecast_qty                number(7),
      revised_forecast_qty        number(7),    
      ftd_msg_count               number(7),
      can_msg_count               number(7),
      rej_msg_count               number(7),
      on_hand                     number(7)
      )
  on commit preserve rows;
comment on table ftd_apps.inv_trk_search_temp is 'Temp work table for dashboard edit and report queries';


create global temporary table ftd_apps.inv_trk_search_prod_vndr_temp 
     (inv_trk_id                  number(11), 
      product_id                  varchar2(10),
      product_name                varchar2(25),
      product_status              varchar2(1),      
      product_UPDATED_BY          VARCHAR2(100),
      exception_start_date        date,
      vendor_id                   varchar2(5),
      vendor_name                 varchar2(50),
      vendor_product_status       varchar2(1), 
      vendor_product_UPDATED_BY   VARCHAR2(100),
      shutdown_threshold_qty      number(7),    
      start_date                  date,
      end_date                    date,
      forecast_qty                number(7),
      revised_forecast_qty        number(7),    
      ftd_msg_count               number(7),
      can_msg_count               number(7),
      rej_msg_count               number(7),
      on_hand                     number(7)
      )
  on commit preserve rows;
comment on table ftd_apps.inv_trk_search_prod_vndr_temp is 'Temp work table for dashboard edit and report queries - product vendur summary';


create global temporary table ftd_apps.inv_trk_vendor_temp 
     (vendor_id                   varchar2(5)
      )
  on commit preserve rows;
comment on table ftd_apps.inv_trk_vendor_temp is 'Temp work table to store vendors to be used in reports / queries';

create global temporary table ftd_apps.inv_trk_product_temp 
     (product_id                 varchar2(10)
      )
  on commit preserve rows;
comment on table ftd_apps.inv_trk_product_temp is 'Temp work table to store productss to be used in reports / queries';

grant select,insert,delete on ftd_apps.inv_trk_vendor_temp to rpt;
grant select,insert,delete on ftd_apps.inv_trk_product_temp to rpt;
grant select,insert,delete on ftd_apps.inv_trk_search_prod_vndr_temp to rpt; 
grant select,insert,delete on ftd_apps.inv_trk_search_temp to rpt;


create global temporary table ftd_apps.inv_trk_search_product_temp 
     (inv_trk_id                  number(11), 
      product_id                  varchar2(10),
      product_name                varchar2(25),
      product_status              varchar2(1),      
      product_UPDATED_BY          VARCHAR2(100),
      exception_start_date        date,
      vendor_id                   varchar2(5),
      vendor_name                 varchar2(50),
      vendor_product_status       varchar2(1), 
      vendor_product_UPDATED_BY   VARCHAR2(100),
      shutdown_threshold_qty      number(7),    
      start_date                  date,
      end_date                    date,
      forecast_qty                number(7),
      revised_forecast_qty        number(7),    
      ftd_msg_count               number(7),
      can_msg_count               number(7),
      rej_msg_count               number(7),
      on_hand                     number(7)
      )
  on commit preserve rows;
comment on table ftd_apps.inv_trk_search_product_temp is 'Temp work table for dashboard edit and report queries - product summary';


insert into FRP.GLOBAL_PARMS
             (context, name, value, created_by, created_on, updated_by, updated_on, description)
values ('FTDAPPS_PARMS', 'MAX_RECORDS_INV_TRK_DASHBOARD', '200', 'SYSTEM_INV_TRK', sysdate, 'SYSTEM_INV_TRK', sysdate, 'This number refers to the maximum number of records that will be displayed to the user on the Inventory Tracking Dashboard screen');

 
  CREATE GLOBAL TEMPORARY TABLE FTD_APPS.REP_TAB 
   (COL1   VARCHAR2(150) DEFAULT 0, 
    COL2   VARCHAR2(150) DEFAULT 0, 
    COL3   VARCHAR2(150) DEFAULT 0, 
    COL4   VARCHAR2(150) DEFAULT 0, 
    COL5   VARCHAR2(150) DEFAULT 0, 
    COL6   VARCHAR2(150) DEFAULT 0, 
    COL7   VARCHAR2(150) DEFAULT 0, 
    COL8   VARCHAR2(150) DEFAULT 0, 
    COL9   VARCHAR2(150) DEFAULT 0, 
    COL10  VARCHAR2(150) DEFAULT 0, 
    COL11  VARCHAR2(150) DEFAULT 0, 
    COL12  VARCHAR2(150) DEFAULT 0, 
    COL13  VARCHAR2(150) DEFAULT 0, 
    COL14  VARCHAR2(150) DEFAULT 0, 
    COL15  VARCHAR2(150) DEFAULT 0, 
    COL16  VARCHAR2(150) DEFAULT 0, 
    COL17  VARCHAR2(150) DEFAULT 0, 
    COL18  VARCHAR2(150) DEFAULT 0, 
    COL19  VARCHAR2(150) DEFAULT 0, 
    COL20  VARCHAR2(150) DEFAULT 0, 
    COL21  VARCHAR2(150) DEFAULT 0, 
    COL22  VARCHAR2(150) DEFAULT 0, 
    COL23  VARCHAR2(150) DEFAULT 0, 
    COL24  VARCHAR2(150) DEFAULT 0, 
    COL25  VARCHAR2(150) DEFAULT 0, 
    COL26  VARCHAR2(150) DEFAULT 0, 
    COL27  VARCHAR2(150) DEFAULT 0, 
    COL28  VARCHAR2(150) DEFAULT 0, 
    COL29  VARCHAR2(150) DEFAULT 0, 
    COL30  VARCHAR2(150) DEFAULT 0, 
    COL31  VARCHAR2(150) DEFAULT 0, 
    COL32  VARCHAR2(150) DEFAULT 0, 
    COL33  VARCHAR2(150) DEFAULT 0, 
    COL34  VARCHAR2(150) DEFAULT 0, 
    COL35  VARCHAR2(150) DEFAULT 0, 
    COL36  VARCHAR2(150) DEFAULT 0, 
    COL37  VARCHAR2(150) DEFAULT 0, 
    COL38  VARCHAR2(150) DEFAULT 0, 
    COL39  VARCHAR2(150) DEFAULT 0, 
    COL40  VARCHAR2(150) DEFAULT 0, 
    COL41  VARCHAR2(150) DEFAULT 0, 
    COL42  VARCHAR2(150) DEFAULT 0, 
    COL43  VARCHAR2(150) DEFAULT 0, 
    COL44  VARCHAR2(150) DEFAULT 0, 
    COL45  VARCHAR2(150) DEFAULT 0, 
    COL46  VARCHAR2(150) DEFAULT 0, 
    COL47  VARCHAR2(150) DEFAULT 0, 
    COL48  VARCHAR2(150) DEFAULT 0, 
    COL49  VARCHAR2(150) DEFAULT 0, 
    COL50  VARCHAR2(150) DEFAULT 0
   ) ON COMMIT PRESERVE ROWS;


grant select on venus.venus to ftd_apps;
grant select on ftd_apps.inv_trk to rpt;
grant select on ftd_apps.inv_trk_event_history to rpt;
grant select on venus.inv_trk_count to rpt;
grant select on venus.inv_trk_assigned_count to rpt;
-- END 2619




--
--Defect 5075
--
alter table ftd_apps.product_master  add (
     GBB_POPOVER_FLAG           varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_TITLE_TXT              Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_1   varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_NAME_OVERRIDE_TXT_1    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_1  varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_PRICE_OVERRIDE_TXT_1   Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_2   varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_NAME_OVERRIDE_TXT_2    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_2  varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_PRICE_OVERRIDE_TXT_2   Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_3   varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_NAME_OVERRIDE_TXT_3    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_3  varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_PRICE_OVERRIDE_TXT_3   Varchar2(255),
 CONSTRAINT product_master_ck8   CHECK(gbb_popover_flag IN('Y','N')), 
 CONSTRAINT product_master_ck9   CHECK(GBB_NAME_OVERRIDE_FLAG_1 IN('Y','N')), 
 CONSTRAINT product_master_ck10  CHECK(GBB_PRICE_OVERRIDE_FLAG_1 IN('Y','N')), 
 CONSTRAINT product_master_ck11  CHECK(GBB_NAME_OVERRIDE_FLAG_2 IN('Y','N')), 
 CONSTRAINT product_master_ck12  CHECK(GBB_PRICE_OVERRIDE_FLAG_2 IN('Y','N')), 
 CONSTRAINT product_master_ck13  CHECK(GBB_NAME_OVERRIDE_FLAG_3 IN('Y','N')), 
 CONSTRAINT product_master_ck14  CHECK(GBB_PRICE_OVERRIDE_FLAG_3 IN('Y','N')));

alter table ftd_apps.product_master$  add (
     GBB_POPOVER_FLAG           varchar2(1),
     GBB_TITLE_TXT              Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_1   varchar2(1),
     GBB_NAME_OVERRIDE_TXT_1    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_1  varchar2(1),
     GBB_PRICE_OVERRIDE_TXT_1   Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_2   varchar2(1),
     GBB_NAME_OVERRIDE_TXT_2    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_2  varchar2(1),
     GBB_PRICE_OVERRIDE_TXT_2   Varchar2(255),
     GBB_NAME_OVERRIDE_FLAG_3   varchar2(1),
     GBB_NAME_OVERRIDE_TXT_3    Varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG_3  varchar2(1),
     GBB_PRICE_OVERRIDE_TXT_3   Varchar2(255));

alter table ftd_apps.upsell_master  add (
     GBB_POPOVER_FLAG           varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_TITLE_TXT              varchar2(255),
 CONSTRAINT upsell_master_ck1   CHECK(gbb_popover_flag IN('Y','N')));

alter table ftd_apps.upsell_detail  add (
     GBB_UPSELL_DETAIL_SEQUENCE_NUM   NUMBER(3),
     GBB_NAME_OVERRIDE_FLAG           varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_NAME_OVERRIDE_TXT            varchar2(255),
     GBB_PRICE_OVERRIDE_FLAG          varchar2(1) DEFAULT 'N' NOT NULL,
     GBB_PRICE_OVERRIDE_TXT           varchar2(255),
 CONSTRAINT upsell_detail_ck1   CHECK(gbb_name_override_flag IN('Y','N')),
 CONSTRAINT upsell_detail_ck2   CHECK(gbb_price_override_flag IN('Y','N')) );


 




create table joe.CROSS_SELL_MASTER 
     (cross_sell_master_id           NUMBER(11)         NOT NULL,
      product_id                     varchar2(10),
      upsell_master_id               varchar2(4),
      created_by                     varchar2(100)      NOT NULL,
      created_on                     date               NOT NULL,
      updated_by                     varchar2(100)      NOT NULL,
      updated_on                     date               NOT NULL,
    CONSTRAINT cross_sell_master_pk PRIMARY KEY(cross_sell_master_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT cross_sell_master_fk1   foreign key (product_id) references ftd_apps.product_master(product_id),
    CONSTRAINT cross_sell_master_fk2   foreign key (upsell_master_id) references ftd_apps.upsell_master(upsell_master_id))
 TABLESPACE joe_data;

create index joe.cross_sell_master_n1 on joe.cross_sell_master(product_id) tablespace joe_indx;
create index joe.cross_sell_master_n2 on joe.cross_sell_master(upsell_master_id) tablespace joe_indx;

COMMENT ON TABLE  joe.cross_sell_master is 'Normalized cross sell master/detail tables';
COMMENT ON COLUMN joe.cross_sell_master.cross_sell_master_id is 'Unique sequence generated id';
COMMENT ON COLUMN joe.cross_sell_master.product_id is 'Product ID - FK to ftd_apps.product_master.product_id';
COMMENT ON COLUMN joe.cross_sell_master.upsell_master_id is 'Upsell ID - FK to ftd_apps.upsell_master.upsell_master_id';
COMMENT ON COLUMN joe.cross_sell_master.created_on is 'Id of user that created the record';
COMMENT ON COLUMN joe.cross_sell_master.created_by is 'Date record was created';
COMMENT ON COLUMN joe.cross_sell_master.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN joe.cross_sell_master.updated_by is 'Date record was last updated';


create table joe.CROSS_SELL_DETAIL
     (cross_sell_master_id           NUMBER(11)         NOT NULL,
      display_order_seq_num          NUMBER(3)          NOT NULL,
      cross_sell_product_id          varchar2(10),
      cross_sell_upsell_id           varchar2(4),
      created_by                     varchar2(100)      NOT NULL,
      created_on                     date               NOT NULL,
      updated_by                     varchar2(100)      NOT NULL,
      updated_on                     date               NOT NULL,
    CONSTRAINT cross_sell_detail_pk PRIMARY KEY(cross_sell_master_id,display_order_seq_num) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT cross_sell_detail_fk1   foreign key (cross_sell_master_id) references joe.cross_sell_master(cross_sell_master_id),
    CONSTRAINT cross_sell_detail_fk2   foreign key (cross_sell_product_id) references ftd_apps.product_master(product_id),
    CONSTRAINT cross_sell_detail_fk3   foreign key (cross_sell_upsell_id) references ftd_apps.upsell_master(upsell_master_id))
 TABLESPACE joe_data;

create index joe.cross_sell_detail_n2 on joe.cross_sell_detail(cross_sell_product_id) tablespace joe_indx;
create index joe.cross_sell_detail_n3 on joe.cross_sell_detail(cross_sell_upsell_id) tablespace joe_indx;


COMMENT ON TABLE  joe.cross_sell_detail is 'Normalized cross sell master/detail tables';
COMMENT ON COLUMN joe.cross_sell_detail.cross_sell_master_id is 'Unique sequence generated id - FK to joe.cross_sell_master.cross_sell_master_id';
COMMENT ON COLUMN joe.cross_sell_detail.display_order_seq_num is 'Contols order of cross-sell products';
COMMENT ON COLUMN joe.cross_sell_detail.cross_sell_product_id is 'Product ID - FK to ftd_apps.product_detail.product_id';
COMMENT ON COLUMN joe.cross_sell_detail.cross_sell_upsell_id is 'Upsell ID - FK to ftd_apps.upsell_detail.upsell_detail_id';
COMMENT ON COLUMN joe.cross_sell_detail.created_on is 'Id of user that created the record';
COMMENT ON COLUMN joe.cross_sell_detail.created_by is 'Date record was created';
COMMENT ON COLUMN joe.cross_sell_detail.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN joe.cross_sell_detail.updated_by is 'Date record was last updated';


create table joe.CROSS_SELL_MASTER$ 
     (cross_sell_master_id           NUMBER(11)         NOT NULL,
      product_id                     varchar2(10),
      upsell_master_id               varchar2(4),
      created_by                     varchar2(100)      NOT NULL,
      created_on                     date               NOT NULL,
      updated_by                     varchar2(100)      NOT NULL,
      updated_on                     date               NOT NULL,
      OPERATION$                     VARCHAR2(7)        not null,
			TIMESTAMP$                     timestamp          not null)
 TABLESPACE joe_data;



create table joe.CROSS_SELL_DETAIL$
     (cross_sell_master_id           NUMBER(11)         NOT NULL,
      display_order_seq_num          NUMBER(3)          NOT NULL,
      cross_sell_product_id          varchar2(10),
      cross_sell_upsell_id           varchar2(4),
      created_by                     varchar2(100)      NOT NULL,
      created_on                     date               NOT NULL,
      updated_by                     varchar2(100)      NOT NULL,
      updated_on                     date               NOT NULL,
      OPERATION$                     VARCHAR2(7)        not null,
			TIMESTAMP$                     timestamp          not null)
 TABLESPACE joe_data;


CREATE SEQUENCE joe.cross_sell_master_sq 
    START WITH 1  INCREMENT BY 1  MAXVALUE 99999999999 NOCYCLE CACHE 100;





-- --# 5705  T. Schmig
create or replace view ftd_apps.global_parms as
SELECT 1 global_parms_key, 
       to_number(a01.value) gnadd_level, 
       to_date(a02.value,'Month DD, YYYY') gnadd_date,
       to_number(a03.value) intl_override_days, 
       a04.value monday_cutoff, 
       a05.value tuesday_cutoff, 
       a06.value wednesday_cutoff,
       a07.value thursday_cutoff, 
       a08.value friday_cutoff, 
       a09.value saturday_cutoff, 
       a10.value sunday_cutoff,
       a11.value freshcuts_cutoff, 
       a12.value specialitygift_cutoff, 
       to_number(a13.value) delivery_days_out, 
       a14.value intl_cutoff,
       a15.value exotic_cutoff, 
       a16.value check_jc_penney, 
       to_number(a17.value) freshcuts_svc_charge,
       to_number(a18.value) special_svc_charge, 
       a19.value freshcuts_svc_charge_trigger, 
       to_number(a20.value) canadian_exchange_rate,
       to_number(a21.value) freshcuts_sat_charge, 
       a22.value dispatch_order_flag,
       to_number(a23.value) delivery_days_out_max,
       to_Number(a24.value) delivery_days_out_min, 
       a25.value allow_substitution,
       a26.value latest_cutoff,
       a27.value FLORAL_LABEL_STANDARD,
       a28.value FLORAL_LABEL_DELUXE,
       a29.value FLORAL_LABEL_PREMIUM
FROM   frp.global_parms a01, 
       frp.global_parms a02, 
       frp.global_parms a03, 
       frp.global_parms a04, 
       frp.global_parms a05,
       frp.global_parms a06, 
       frp.global_parms a07, 
       frp.global_parms a08, 
       frp.global_parms a09, 
       frp.global_parms a10,
       frp.global_parms a11, 
       frp.global_parms a12, 
       frp.global_parms a13, 
       frp.global_parms a14, 
       frp.global_parms a15,
       frp.global_parms a16, 
       frp.global_parms a17, 
       frp.global_parms a18, 
       frp.global_parms a19, 
       frp.global_parms a20,
       frp.global_parms a21, 
       frp.global_parms a22, 
       frp.global_parms a23, 
       frp.global_parms a24, 
       frp.global_parms a25,
       frp.global_parms a26,
       frp.global_parms a27,
       frp.global_parms a28,
       frp.global_parms a29
WHERE  (a01.context = 'FTDAPPS_PARMS'  AND   a01.name = 'GNADD_LEVEL')
  AND  (a02.context = 'FTDAPPS_PARMS'  AND   a02.name = 'GNADD_DATE')
  AND  (a03.context = 'FTDAPPS_PARMS'  AND   a03.name = 'INTL_OVERRIDE_DAYS')
  AND  (a04.context = 'FTDAPPS_PARMS'  AND   a04.name = 'MONDAY_CUTOFF')
  AND  (a05.context = 'FTDAPPS_PARMS'  AND   a05.name = 'TUESDAY_CUTOFF')
  AND  (a06.context = 'FTDAPPS_PARMS'  AND   a06.name = 'WEDNESDAY_CUTOFF')
  AND  (a07.context = 'FTDAPPS_PARMS'  AND   a07.name = 'THURSDAY_CUTOFF')
  AND  (a08.context = 'FTDAPPS_PARMS'  AND   a08.name = 'FRIDAY_CUTOFF')
  AND  (a09.context = 'FTDAPPS_PARMS'  AND   a09.name = 'SATURDAY_CUTOFF')
  AND  (a10.context = 'FTDAPPS_PARMS'  AND   a10.name = 'SUNDAY_CUTOFF')
  AND  (a11.context = 'FTDAPPS_PARMS'  AND   a11.name = 'FRESHCUTS_CUTOFF')
  AND  (a12.context = 'FTDAPPS_PARMS'  AND   a12.name = 'SPECIALITYGIFT_CUTOFF')
  AND  (a13.context = 'FTDAPPS_PARMS'  AND   a13.name = 'DELIVERY_DAYS_OUT')
  AND  (a14.context = 'FTDAPPS_PARMS'  AND   a14.name = 'INTL_CUTOFF')
  AND  (a15.context = 'FTDAPPS_PARMS'  AND   a15.name = 'EXOTIC_CUTOFF')
  AND  (a16.context = 'FTDAPPS_PARMS'  AND   a16.name = 'CHECK_JC_PENNEY')
  AND  (a17.context = 'FTDAPPS_PARMS'  AND   a17.name = 'FRESHCUTS_SVC_CHARGE')
  AND  (a18.context = 'FTDAPPS_PARMS'  AND   a18.name = 'SPECIAL_SVC_CHARGE')
  AND  (a19.context = 'FTDAPPS_PARMS'  AND   a19.name = 'FRESHCUTS_SVC_CHARGE_TRIGGER')
  AND  (a20.context = 'FTDAPPS_PARMS'  AND   a20.name = 'CANADIAN_EXCHANGE_RATE')
  AND  (a21.context = 'FTDAPPS_PARMS'  AND   a21.name = 'FRESHCUTS_SAT_CHARGE')
  AND  (a22.context = 'FTDAPPS_PARMS'  AND   a22.name = 'DISPATCH_ORDER_FLAG')
  AND  (a23.context = 'FTDAPPS_PARMS'  AND   a23.name = 'DELIVERY_DAYS_OUT_MAX')
  AND  (a24.context = 'FTDAPPS_PARMS'  AND   a24.name = 'DELIVERY_DAYS_OUT_MIN')
  AND  (a25.context = 'FTDAPPS_PARMS'  AND   a25.name = 'ALLOW_SUBSTITUTION')
  AND  (a26.context = 'FTDAPPS_PARMS'  AND   a26.name = 'LATEST_CUTOFF')
  AND  (a27.context = 'FTDAPPS_PARMS'  AND   a27.name = 'FLORAL_LABEL_STANDARD')
  AND  (a28.context = 'FTDAPPS_PARMS'  AND   a28.name = 'FLORAL_LABEL_DELUXE')
  AND  (a29.context = 'FTDAPPS_PARMS'  AND   a29.name = 'FLORAL_LABEL_PREMIUM')
/

-- 5335
grant select on ftd_apps.upsell_company_xref to joe;


-- 2619
-- NOTE - VENUS.VENUS WILL NEED NEW SNAPSHOT IN EDB!!
alter table venus.venus add (
   inv_trk_count_date             date,
   inv_trk_assigned_count_date    date);
 
comment on column venus.venus.inv_trk_count_date is 'Date/time when inventory tracking message count is updated.';
comment on column venus.venus.inv_trk_assigned_count_date is 'Date/time when inventory tracking assigned message count is updated.';


-- 2562
grant select on ftd_apps.novator_fraud_codes to scrub;

-- 5046
update FRP.GLOBAL_PARMS 
set UPDATED_BY = 'DEFECT_5046', UPDATED_ON = SYSDATE, 
DESCRIPTION = 'Set to Y to enable AAFES End Of Day processing.  Also forces reports to check for AAFES EOD completion before run.' 
where CONTEXT = 'ACCOUNTING_CONFIG' and NAME = 'DISPATCH_AAFES_EOD_FLAG';



-- 5075
set serveroutput on size 99999
DECLARE

v_master_sequence     varchar2(100);

cursor get_xsell is
    select * from joe.product_cross_sell
        order by product_cross_sell_id;

BEGIN

    for x in get_xsell loop

        SELECT joe.cross_sell_master_sq.nextval into v_master_sequence FROM dual;

        insert into joe.cross_sell_master
            (cross_sell_master_id, product_id, upsell_master_id, created_by, created_on, updated_by, updated_on)
          values (
            v_master_sequence,
            x.product_id,
            x.upsell_master_id,
            x.created_by, x.created_on, x.updated_by, x.updated_on);

        insert into joe.cross_sell_detail
            (cross_sell_master_id, display_order_seq_num, cross_sell_product_id, cross_sell_upsell_id, 
               created_by, created_on, updated_by, updated_on)
          values (
            v_master_sequence,
            1,
            x.cross_sell_1_product_id,
            x.cross_sell_1_upsell_master_id,
            x.created_by, x.created_on, x.updated_by, x.updated_on);

        if (x.cross_sell_2_product_id is not null or x.cross_sell_2_upsell_master_id is not null) then
            insert into joe.cross_sell_detail
                (cross_sell_master_id, display_order_seq_num, cross_sell_product_id, cross_sell_upsell_id, 
                   created_by, created_on, updated_by, updated_on)
              values (
                v_master_sequence,
                2,
                x.cross_sell_2_product_id,
                x.cross_sell_2_upsell_master_id,
                x.created_by, x.created_on, x.updated_by, x.updated_on);
        end if;

        if (x.cross_sell_3_product_id is not null or x.cross_sell_3_upsell_master_id is not null) then
            insert into joe.cross_sell_detail
                (cross_sell_master_id, display_order_seq_num, cross_sell_product_id, cross_sell_upsell_id, 
                   created_by, created_on, updated_by, updated_on)
              values (
                v_master_sequence,
                3,
                x.cross_sell_3_product_id,
                x.cross_sell_3_upsell_master_id,
                x.created_by, x.created_on, x.updated_by, x.updated_on);
        end if;

    end loop;

    EXCEPTION WHEN OTHERS THEN
        dbms_output.put_line('ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256));

end;
/



set define off


INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'GBB_DEFAULT_TITLE',
    'Upgrade Your Bouquet!',
    'The default Good, Better, Best Title',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'PDB_IMAGE_URL_PREFIX',
    'http://www.ftd.com/pics/design2007/gbb/products/gbb_popover_',
    'The image url for PDB images. Make sure that it ends with a slash.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'PDB_IMAGE_URL_TYPE',
    'png',
    'The image type for PDB images.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'PDB_IMAGE_URL_STANDARD_SUFFIX',
    '_330x370',
    'The image url suffix for PDB images.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'PDB_IMAGE_URL_DELUXE_SUFFIX',
    '_deluxe_330x370',
    'The image url suffix for PDB images.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'PDB_CONFIG',
    'PDB_IMAGE_URL_PREMIUM_SUFFIX',
    '_premium_330x370',
    'The image url suffix for PDB images.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'FTDAPPS_PARMS',
    'FLORAL_LABEL_STANDARD',
    'The NEW Standard',
    'The label replacement for STANDARD on all front-end applications.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'FTDAPPS_PARMS',
    'FLORAL_LABEL_DELUXE',
    'The NEW Deluxe',
    'The label replacement for DELUXE on all front-end applications.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'FTDAPPS_PARMS',
    'FLORAL_LABEL_PREMIUM',
    'The NEW Premium',
    'The label replacement for PREMIUM on all front-end applications.',
    sysdate,
    'Defect 5075',
    sysdate,
    'Defect 5075');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'OE_CONFIG',
    'NUM_CROSS_SELL_SKUS',
    '3',
    'The maximum number of detail skus that can be assigned to a Cross Sell Master Id',
    sysdate,
    'Defect 5335',
    sysdate,
    'Defect 5335');


-- END 5075




grant execute on ftd_apps.sp_update_product_novator to osp;
 


-----------------------------------
-- 5499 - 
-----------------------------------

insert into rpt.report_output_type (report_output_type, description, status)
values ('Gen', 'Use Key Mapping Specified in Schedule_Parm_value in Report table', 'Active');



--
--  ### QA ONLY ###
--
set define off;


    update frp.global_parms
      set  value = 'reportEmail',
           updated_on = sysdate,
           updated_by = 'Defect 5499'
      where context = 'ACCOUNTING_CONFIG'
      and name = 'LOGIN_REPORT_KEY';

    insert into rpt.report
      (report_id,
      name,
      description,
      report_file_prefix,
      report_type,
      report_output_type,
      report_category,
      server_name,
      holiday_indicator,
      status,
      created_on,
      created_by,
      updated_on,
      updated_by,
      oracle_rdf,
      acl_name,
      run_time_offset,
      end_of_day_required,
      schedule_day,
      schedule_type,
      schedule_parm_value)
    values
      (100003,
      'United Redemption Invoice',
      'Bi-weekly report of orders/refunds using United Miles (email version)',
      'United',
      'S',
      'Gen',
      'Acct',
      'http://neon.ftdi.com:7778/reports/rwservlet?',
      'Y',
      'Active',
      sysdate,
      'Defect 5499',
      sysdate,
      'Defect 5499',
      'ACC43_United_Invoice.rdf',
      'BaseAcctReportAccess',
      120,
      'N',
      '1',
      'B',
      '&background=yes&desname=apolloqa@ftdi.com');

set define on

------------------------
------------------------
--- ### 5499 PRODUCTION ONLY ###
--
set define off;


    update frp.global_parms
      set  value = 'reportEmail',
           updated_on = sysdate,
           updated_by = 'Defect 5499'
      where context = 'ACCOUNTING_CONFIG'
      and name = 'LOGIN_REPORT_KEY';

    insert into rpt.report
      (report_id,
      name,
      description,
      report_file_prefix,
      report_type,
      report_output_type,
      report_category,
      server_name,
      holiday_indicator,
      status,
      created_on,
      created_by,
      updated_on,
      updated_by,
      oracle_rdf,
      acl_name,
      run_time_offset,
      end_of_day_required,
      schedule_day,
      schedule_type,
      schedule_parm_value)
    values
      (100003,
      'United Redemption Invoice',
      'Bi-weekly report of orders/refunds using United Miles (email version)',
      'United',
      'S',
      'Gen',
      'Acct',
      'http://apolloreports.ftdi.com/reports/rwservlet?',
      'Y',
      'Active',
      sysdate,
      'Defect 5499',
      sysdate,
      'Defect 5499',
      'ACC43_United_Invoice.rdf',
      'BaseAcctReportAccess',
      120,
      'N',
      '1',
      'B',
      '&background=yes&desname=ConsumerAccounting@ftdi.com');

------
---- END 5499
---------------------

set define on




--
--  2619 DML
--


/*********** START QA ************/
--rpt.report
INSERT INTO RPT.REPORT(REPORT_ID,NAME,DESCRIPTION,REPORT_FILE_PREFIX,REPORT_TYPE,REPORT_OUTPUT_TYPE,REPORT_CATEGORY,SERVER_NAME,HOLIDAY_INDICATOR,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,REPORT_DAYS_SPAN,NOTES,ACL_NAME,RETENTION_DAYS,RUN_TIME_OFFSET,END_OF_DAY_REQUIRED,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
VALUES (100810,'Drop Ship Vendor Inventory Control','This is a summary report displaying current level counts for products as well as sales order count for the product within a rolling week time frame based on the inventory tracking system.','Drop_Ship_Vendor_Inv_Cntl','U','Char','Merch','http://neon.ftdi.com:7778/reports/rwservlet?','Y','Active',SYSDATE,'chu',SYSDATE,'chu','MER04_Drop_Ship_Inv_Control.rdf',null,null,'BaseMerchReportAccess',10,120,'N',null,null,null);

--rpt.report
INSERT INTO RPT.REPORT(REPORT_ID,NAME,DESCRIPTION,REPORT_FILE_PREFIX,REPORT_TYPE,REPORT_OUTPUT_TYPE,REPORT_CATEGORY,SERVER_NAME,HOLIDAY_INDICATOR,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,REPORT_DAYS_SPAN,NOTES,ACL_NAME,RETENTION_DAYS,RUN_TIME_OFFSET,END_OF_DAY_REQUIRED,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
VALUES (100811,'Inventory Tracking Shutdown','This is a summary reporting listing Inventory Tracking System triggered events including: LOCATION SHUTDOWN, PRODUCT SHUTDOWN, and PRODUCT AVAILABILITY DATES.','Inv_Tracking_Shutdown','U','Char','Merch','http://neon.ftdi.com:7778/reports/rwservlet?','Y','Active',SYSDATE,'chu',SYSDATE,'chu','MER05_Inv_Tracking_Shutdown.rdf',null,null,'BaseMerchReportAccess',10,120,'N',null,null,null);
/*********** END QA *************/

/*********** START PROD ************/
--rpt.report
INSERT INTO RPT.REPORT(REPORT_ID,NAME,DESCRIPTION,REPORT_FILE_PREFIX,REPORT_TYPE,REPORT_OUTPUT_TYPE,REPORT_CATEGORY,SERVER_NAME,HOLIDAY_INDICATOR,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,REPORT_DAYS_SPAN,NOTES,ACL_NAME,RETENTION_DAYS,RUN_TIME_OFFSET,END_OF_DAY_REQUIRED,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
VALUES (100810,'Drop Ship Vendor Inventory Control','This is a summary report displaying current level counts for products as well as sales order count for the product within a rolling week time frame based on the inventory tracking system.','Drop_Ship_Vendor_Inv_Cntl','U','Char','Merch','http://apolloreports.ftdi.com/reports/rwservlet?','Y','Active',SYSDATE,'chu',SYSDATE,'chu','MER04_Drop_Ship_Inv_Control.rdf',null,null,'BaseMerchReportAccess',10,120,'N',null,null,null);

--rpt.report
INSERT INTO RPT.REPORT(REPORT_ID,NAME,DESCRIPTION,REPORT_FILE_PREFIX,REPORT_TYPE,REPORT_OUTPUT_TYPE,REPORT_CATEGORY,SERVER_NAME,HOLIDAY_INDICATOR,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,REPORT_DAYS_SPAN,NOTES,ACL_NAME,RETENTION_DAYS,RUN_TIME_OFFSET,END_OF_DAY_REQUIRED,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
VALUES (100811,'Inventory Tracking Shutdown','This is a summary reporting listing Inventory Tracking System triggered events including: LOCATION SHUTDOWN, PRODUCT SHUTDOWN, and PRODUCT AVAILABILITY DATES.','Inv_Tracking_Shutdown','U','Char','Merch','http://apolloreports.ftdi.com/reports/rwservlet?','Y','Active',SYSDATE,'chu',SYSDATE,'chu','MER05_Inv_Tracking_Shutdown.rdf',null,null,'BaseMerchReportAccess',10,120,'N',null,null,null);
/*********** END PROD ************/

--------------------------------------------------
---- Drop Ship Vendor Inventory Control Report ---
--------------------------------------------------

-- report parms
insert into rpt.report_parm(REPORT_PARM_ID,DISPLAY_NAME,REPORT_PARM_TYPE,ORACLE_PARM_NAME,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,VALIDATION) 
values(101040,'Vendors','TE','p_vendor_id',sysdate,'chu',sysdate,'chu',null);
insert into rpt.report_parm(REPORT_PARM_ID,DISPLAY_NAME,REPORT_PARM_TYPE,ORACLE_PARM_NAME,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,VALIDATION) 
values(101050,'Product IDs (line delimited)','TE','p_product_id',sysdate,'chu',sysdate,'chu',null);


-- report parm ref
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (101040,100810,1,'N',sysdate,'chu',null);
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (101050,100810,2,'N',sysdate,'chu',null); 
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (100801,100810,3,'N',sysdate,'chu',null); 

--report parm value
insert into rpt.report_parm_value(REPORT_PARM_VALUE_ID,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,DISPLAY_TEXT,DB_STATEMENT_ID,DEFAULT_FORM_VALUE)
values (1009031, sysdate, 'chu',sysdate,'chu','VENDOR_ID_LIST_TEMPLATE','GET_ACTIVE_VENDORS','ALL');
insert into rpt.report_parm_value(REPORT_PARM_VALUE_ID,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,DISPLAY_TEXT,DB_STATEMENT_ID,DEFAULT_FORM_VALUE)
values (1009041, sysdate, 'chu',sysdate,'chu','PRODUCT_ID_TEMPLATE',null,'ALL');

--report parm varlue ref
insert into rpt.report_parm_value_ref(REPORT_PARM_ID,REPORT_PARM_VALUE_ID,SORT_ORDER,CREATED_ON,CREATED_BY)
values (101040,1009031,1,sysdate,'chu');
insert into rpt.report_parm_value_ref(REPORT_PARM_ID,REPORT_PARM_VALUE_ID,SORT_ORDER,CREATED_ON,CREATED_BY)
values (101050,1009041,2,sysdate,'chu');

--------------------------------------------------
---- Drop Ship Vendor Inventory Control Report ---
--------------------------------------------------

--rpt.report_parm_ref: 
--VENDOR_ID_LIST_TEMPLATE, 
--PRODUCT_ID_TEMPLATE
--START_END_DATE_TEMPLATE
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (101040,100811,1,'N',sysdate,'chu',null);
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (101050,100811,2,'N',sysdate,'chu',null);
insert into rpt.report_parm_ref(report_parm_id,report_id,sort_order,required_indicator,created_on,created_by,allow_future_date_ind)
values (100402,100811,3,'Y',sysdate,'chu',null);


update RPT.REPORT set ORACLE_RDF='MER09_Drop_Ship_Inv_Control.rdf' where report_id=100810;
update RPT.REPORT set ORACLE_RDF='MER10_Inv_Tracking_Shutdown.rdf' where report_id=100811;

--
-- END 2619
--


update rpt.report_parm_value set default_form_value=null where report_parm_value_id=1009031;
update rpt.report_parm set validation='ProductId' where report_parm_id=101050;

drop trigger pop.INBOUND_ORDERDETAIL_MAPPING1;







--### Defect 5530 (Tom Lynema)
--Create a new resource
insert into aas.resources     (resource_id, context_id, description, created_on, updated_on, updated_by)
values                        ('Carrier Zip Blocking Link','Order Proc','Controls access to the carrier zip blocking link',sysdate,sysdate,'tlynema');

insert into aas.resources     (resource_id, context_id, description, created_on, updated_on, updated_by)
values                        ('Reprocess Orders Link','Order Proc','Controls access to the carrier zip blocking link',sysdate,sysdate,'tlynema');

--Create a new resource group
insert into aas.acl           (acl_name, created_on, updated_on, updated_by, acl_level)
values                        ('Merch Level 2',sysdate,sysdate,'tlynema',null);


insert into aas.rel_acl_rp    (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values                        ('Merch Level 2','Carrier Zip Blocking Link','Order Proc','View',sysdate,sysdate,'tlynema');

insert into aas.rel_acl_rp    (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values                        ('Merch Level 2','Reprocess Orders Link','Order Proc','View',sysdate,sysdate,'tlynema');


insert into aas.rel_role_acl    (role_id, acl_name, created_on, updated_on, updated_by) 
values                        ('200026','Merch Level 2',sysdate,sysdate,'tlynema');

--### end Defect 5530



--Create a new resource
insert into aas.resources     (resource_id, context_id, description, created_on, updated_on, updated_by)
values                        ('Inventory Tracking','Order Proc',null,sysdate,sysdate,'SYSTEM_INV_TRK');

 
--Create relationship between resource and resource group
insert into aas.rel_acl_rp    (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values                        ('Merch Level 2','Inventory Tracking','Order Proc','Update',sysdate,sysdate,'SYSTEM_INV_TRK');

 

#
# 5273
#

set serveroutput on size 99999
declare
   --
   -- 5273 Data Fix - Convert Security Number values to masks in pop.inbound_transmissions
   -- ### THIS SCRIPT WILL TAKE ABOUT 18 MINUTES TO RUN IN PRODUCTION ###
   --
   CVV_FOUND_FLAG       VARCHAR2(1);
   CVV_FOUND_CNT        number := 0;
   CVV_NOT_FOUND_CNT    number := 0;

   xmlInDoc                    XMLType;
   v_transmission_sec_node     varchar2(2000) := '/CC_TRANSMISSION/CC_ORDER/PAYMENT_METHOD/CREDIT_CARD/CC_SECURITY_NUMBER/text()';

   w_TRANSMISSION              POP.INBOUND_TRANSMISSIONS.TRANSMISSION%TYPE;

   -- Get all XML
   cursor c1 is
      select rowid
      from pop.inbound_transmissions
      where to_char(substr(transmission,1,2)) = '<?';


begin
   dbms_output.put_line('Fixit started at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));

   for c1_rec in c1 LOOP

      BEGIN
         CVV_FOUND_FLAG := 'Y';

         select transmission
         INTO w_transmission
         from pop.inbound_transmissions
         where rowid = c1_rec.rowid
           and extractvalue(xmltype(transmission),'/CC_TRANSMISSION/CC_ORDER/PAYMENT_METHOD/CREDIT_CARD/CC_SECURITY_NUMBER') is not null;

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            CVV_FOUND_FLAG := 'N';
         WHEN OTHERS THEN
            dbms_output.put_line('Unexpected error getting transmission for rowid ' || c1_rec.rowid ||
                                    '    MSG=' || sqlerrm);
      END;

      if CVV_FOUND_FLAG = 'Y' THEN
         CVV_FOUND_CNT := CVV_FOUND_CNT + 1;

         -- Mask the Security Number
    --     dbms_output.put_line('Mask the security number...');

         xmlInDoc := XMLType(w_TRANSMISSION);

         BEGIN
            UPDATE pop.INBOUND_TRANSMISSIONS
            SET transmission =
               updatexml(xmlInDoc, v_transmission_sec_node, '***').getclobval()
               where rowid = c1_rec.rowid;
         EXCEPTION
            WHEN OTHERS THEN
               dbms_output.put_line('Unexpected error updating rowid ' || c1_rec.rowid ||
                                    '    MSG=' || sqlerrm);
         END;



      else
         CVV_NOT_FOUND_CNT := CVV_NOT_FOUND_CNT + 1;
      end if;


   END LOOP;

   dbms_output.put_line('CVV Found=' || cvv_found_cnt || '       CVV Not Found=' || cvv_not_found_cnt);
   dbms_output.put_line('Fixit ended started at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));
   commit;

end;
/

--
-- END 5273
--


## For 3.2.0.4

 alter table rpt.report modify (schedule_day varchar2(20));




----
---- END Fix 3.2 Release Script
----