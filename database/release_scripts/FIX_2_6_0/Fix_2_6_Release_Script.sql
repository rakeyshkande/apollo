---- FIX 2.6.0 RELEASE SCRIPT ------------------


CREATE TABLE ftd_apps.zj_injection_hub (
          injection_hub_id           number(11)       NOT NULL,
          city_name                  varchar2(100)    NOT NULL,
          state_master_id            varchar2(2)      NOT NULL,
          carrier_id                 varchar2(10)     NOT NULL,
          address_desc               varchar2(250),
          zip_code                   varchar2(20),
          created_by                 varchar2(100)    not null,
          created_on                 date             not null,
          updated_by                 varchar2(100)    not null,
          updated_on                 date             not null,
    CONSTRAINT zj_injection_hub_pk PRIMARY KEY(injection_hub_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT zj_injection_hub_FK1 FOREIGN KEY(state_master_id) REFERENCES ftd_apps.state_master(state_master_id),
    CONSTRAINT zj_injection_hub_FK2 FOREIGN KEY(carrier_id)  REFERENCES venus.carriers(carrier_id))
 TABLESPACE ftd_apps_DATA;

CREATE INDEX ftd_apps.zj_injection_hub_N1 ON ftd_apps.zj_injection_hub(state_master_id) TABLESPACE ftd_apps_INDX;
CREATE INDEX ftd_apps.zj_injection_hub_N2 ON ftd_apps.zj_injection_hub(carrier_id) TABLESPACE ftd_apps_INDX;

COMMENT ON TABLE ftd_apps.zj_injection_hub IS 'Contains injection hub information for use with zonejump functionality';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.injection_hub_id is 'Primary key';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.city_name is 'Name of the city where the injection hub is located';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.state_master_id is 'State Abbreviation - validated by state_master';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.carrier_id is 'Carrier which will transport to the injection hub';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.adress_desc is 'Street address of the injection hub';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.zip_code is 'Zip code where the injection hub is located';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.zj_injection_hub.updated_by is 'Date record was last updated';

CREATE SEQUENCE ftd_apps.injection_hub_sq 
    START WITH 1  INCREMENT BY 1  MAXVALUE 99999999999 NOCYCLE NOCACHE;


CREATE TABLE ftd_apps.zj_trip_status (
          trip_status_code           varchar2(25)     NOT NULL,
          trip_status_desc           varchar2(100)    NOT NULL,
          created_by                 varchar2(100)    not null,
          created_on                 date             not null,
          updated_by                 varchar2(100)    not null,
          updated_on                 date             not null,
    CONSTRAINT zj_trip_status_pk PRIMARY KEY(trip_status_code) 
            USING INDEX  TABLESPACE ftd_apps_INDX)
 TABLESPACE ftd_apps_DATA;

COMMENT ON TABLE ftd_apps.zj_trip_status IS 'Contains valid statuses of zonejump trips';
COMMENT ON COLUMN ftd_apps.zj_trip_status.trip_status_code is 'Primary key - Code denoting status of trip e.g. Closed, Actived';
COMMENT ON COLUMN ftd_apps.zj_trip_status.trip_status_desc is 'Long Description of trip status code';


CREATE TABLE ftd_apps.zj_trip_vendor_status (
          trip_vendor_status_code   varchar2(25)     NOT NULL,
          trip_vendor_status_desc   varchar2(100)    NOT NULL,
          created_by                 varchar2(100)    not null,
          created_on                 date             not null,
          updated_by                 varchar2(100)    not null,
          updated_on                 date             not null,
    CONSTRAINT zj_trip_vendor_status_pk PRIMARY KEY(trip_vendor_status_code) 
            USING INDEX  TABLESPACE ftd_apps_INDX)
 TABLESPACE ftd_apps_DATA;

COMMENT ON TABLE ftd_apps.zj_trip_vendor_status IS 'Contains valid statuses for vendors on a zonejump trip';
COMMENT ON COLUMN ftd_apps.zj_trip_vendor_status.trip_vendor_status_code is 'Primary key - Code denoting status of vendor on a trip e.g. Closed, Actived';
COMMENT ON COLUMN ftd_apps.zj_trip_vendor_status.trip_vendor_status_desc is 'Long Description of trip vendor status code';


grant references on ftd_apps.zj_injection_hub to venus;
grant references on ftd_apps.zj_trip_status to venus;


CREATE TABLE venus.zj_trip (
          trip_id                    number(11)       NOT NULL,
          trip_origination_desc      varchar2(250)    NOT NULL,
          injection_hub_id           number(11)       NOT NULL,
          delivery_date              date             NOT NULL,
          departure_date             date             NOT NULL,
          trip_status_code           varchar2(25)     NOT NULL,
          sort_code_seq_num          number(2)        NOT NULL,
          sort_code_state_master_id  varchar2(2)      NOT NULL,
          sort_code_date_MMDD        varchar2(4)      NOT NULL,
          total_pallet_qty           number(5)        NOT NULL,
          ftd_pallet_qty             number(5)        NOT NULL,
          third_party_pallet_qty     number(5)        NOT NULL,
          created_by                 varchar2(100)    not null,
          created_on                 date             not null,
          updated_by                 varchar2(100)    not null,
          updated_on                 date             not null,
    CONSTRAINT zj_trip_pk PRIMARY KEY(trip_id) 
            USING INDEX  TABLESPACE venus_INDX,
    CONSTRAINT zj_trip_FK1 FOREIGN KEY(injection_hub_id) REFERENCES ftd_apps.zj_injection_hub(injection_hub_id),
    CONSTRAINT zj_trip_FK2 FOREIGN KEY(trip_status_code)  REFERENCES ftd_apps.zj_trip_status(trip_status_code))
 TABLESPACE venus_DATA;

CREATE INDEX venus.zj_trip_N1 ON venus.zj_trip(injection_hub_id) tablespace venus_indx;
CREATE INDEX venus.zj_trip_N2 ON venus.zj_trip(trip_status_code) tablespace venus_indx;

COMMENT ON TABLE  venus.zj_trip IS 'Contains data related to a zonejump trip';
COMMENT ON COLUMN venus.zj_trip.trip_id is 'Primary Key - Sequence';
comment on column venus.zj_trip.trip_origination_desc is 'Location where the zonejump is starting from';
comment on column venus.zj_trip.injection_hub_id is 'Destination injection hub - valided by ftd_apps.zj_injection_hub';
COMMENT ON COLUMN venus.zj_trip.delivery_date is 'Date of the product delivery for the items on the trip';
COMMENT ON COLUMN venus.zj_trip.departure_date is 'Date of the truck leaves on the trip';
COMMENT ON COLUMN venus.zj_trip.trip_status_code is 'Validated by ftd_apps.zj_trip_status';
COMMENT ON COLUMN venus.zj_trip.sort_code_seq_NUM is '1st set of 3 part sort code - daily sequence';
COMMENT ON COLUMN venus.zj_trip.sort_code_state_master_id is '2nd set of 3 part sort code - state';
COMMENT ON COLUMN venus.zj_trip.sort_code_date_MMDD is '3rd set of 3 part sort code - date';
COMMENT ON COLUMN venus.zj_trip.total_pallet_qty is 'Total number of pallets on the trip';
COMMENT ON COLUMN venus.zj_trip.ftd_pallet_qty is 'Number of FTD pallets on the trip';
COMMENT ON COLUMN venus.zj_trip.third_party_pallet_qty is 'Number of third party pallets on the trip';
COMMENT ON COLUMN venus.zj_trip.created_on is 'Id of user that created the record';
COMMENT ON COLUMN venus.zj_trip.created_by is 'Date record was created';
COMMENT ON COLUMN venus.zj_trip.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN venus.zj_trip.updated_by is 'Date record was last updated';

CREATE SEQUENCE venus.trip_hub_sq 
    START WITH 1  INCREMENT BY 1  MAXVALUE 99999999999 NOCYCLE NOCACHE;


grant references on ftd_apps.zj_trip_vendor_status to venus;
grant references on ftd_apps.vendor_master to venus;

CREATE TABLE venus.zj_trip_vendor (
          trip_id                         number(11)       NOT NULL,
          vendor_id                       varchar2(5)      NOT NULL,
          trip_vendor_status_code         varchar2(25)     NOT NULL,
          order_cutoff_time               varchar2(4)      NOT NULL,
          full_pallet_qty                 number(5)        NOT NULL,
          full_pallet_order_qty           number(5)       NOT NULL,
          partial_pallet_in_use_flag      VARCHAR2(1) DEFAULT 'N' NOT NULL,
          partial_pallet_order_qty        number(5)       NOT NULL,
          pallet_cubic_inch_allowed_qty   number(7,2)     NOT NULL,
          part_pallet_cubic_in_used_qty   number(7,2)     NOT NULL,
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT zj_trip_vendor_pk PRIMARY KEY(trip_id,vendor_id) 
            USING INDEX  TABLESPACE venus_INDX,
    CONSTRAINT zj_trip_vendor_ck1 CHECK(partial_pallet_in_use_flag IN('Y','N')), 
    CONSTRAINT zj_trip_vendor_FK1 FOREIGN KEY(trip_id) REFERENCES venus.zj_trip(trip_id),
    CONSTRAINT zj_trip_vendor_FK2 FOREIGN KEY(vendor_id) REFERENCES ftd_apps.vendor_master(vendor_id),
    CONSTRAINT zj_trip_vendor_FK3 FOREIGN KEY(trip_vendor_status_code)  REFERENCES ftd_apps.zj_trip_vendor_status(trip_vendor_status_code))
 TABLESPACE venus_DATA;

CREATE INDEX venus.zj_trip_vendor_N1 ON venus.zj_trip_vendor(trip_vendor_status_code) tablespace venus_indx;


COMMENT ON TABLE  venus.zj_trip_vendor IS 'Contains data related to a zonejump vendor on a trip';
COMMENT ON COLUMN venus.zj_trip_vendor.trip_id is 'Trip Identifier - validated by venus.zj_trip';
COMMENT ON COLUMN venus.zj_trip_vendor.vendor_id is 'Vendor - validated by ftd_apps.vendor_master';
COMMENT ON COLUMN venus.zj_trip_vendor.trip_vendor_status_code is 'Validated by ftd_apps.zj_trip_vendor_status';
COMMENT ON COLUMN venus.zj_trip_vendor.order_cutoff_time is 'Time of the vendor cutoff';
COMMENT ON COLUMN venus.zj_trip_vendor.full_pallet_qty is 'Number of full pallets the vendor has on the trip';
COMMENT ON COLUMN venus.zj_trip_vendor.full_pallet_order_qty is 'Number of full order pallets the vendor has on the trip';
COMMENT ON COLUMN venus.zj_trip_vendor.partial_pallet_in_use_flag is 'Flag denoting whether or not the vendor has a partial pallet present on the trip';
COMMENT ON COLUMN venus.zj_trip_vendor.pallet_cubic_inch_allowed_qty is 'Amount of space the vendor has allocated for the trip';
COMMENT ON COLUMN venus.zj_trip_vendor.part_pallet_cubic_in_used_qty is 'Cubic Inches the vendor has on a partial pallet';
COMMENT ON COLUMN venus.zj_trip_vendor.created_on is 'Id of user that created the record';
COMMENT ON COLUMN venus.zj_trip_vendor.created_by is 'Date record was created';
COMMENT ON COLUMN venus.zj_trip_vendor.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN venus.zj_trip_vendor.updated_by is 'Date record was last updated';



grant references on clean.order_details to venus;

CREATE TABLE venus.zj_trip_vendor_order (
          trip_id                         number(11)       NOT NULL,
          vendor_id                       varchar2(5)      NOT NULL,
          order_detail_id                 number           NOT NULL,
          venus_id                        varchar2(20)     NOT NULL,
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT zj_trip_vendor_order_pk PRIMARY KEY(trip_id,vendor_id,order_detail_id) 
            USING INDEX  TABLESPACE venus_INDX,
    CONSTRAINT zj_trip_vendor_order_FK1 FOREIGN KEY(trip_id,vendor_id) REFERENCES venus.zj_trip_vendor(trip_id,vendor_id),
    CONSTRAINT zj_trip_vendor_order_FK2 FOREIGN KEY(order_detail_id) REFERENCES clean.order_details(order_detail_id),
    CONSTRAINT zj_trip_vendor_order_FK3 FOREIGN KEY(venus_id)  REFERENCES venus.venus(venus_id))
 TABLESPACE venus_DATA;

CREATE INDEX venus.zj_trip_vendor_order_N1 ON venus.zj_trip_vendor_order(venus_id) tablespace venus_indx;


COMMENT ON TABLE  venus.zj_trip_vendor_order IS 'Contains data related to an order for a vendor on a zonejump trip';
COMMENT ON COLUMN venus.zj_trip_vendor_order.trip_id is 'Trip Identifier - validated by venus.zj_trip_vendor';
COMMENT ON COLUMN venus.zj_trip_vendor_order.vendor_id is 'Vendor - validated by zj_trip_vendor';
COMMENT ON COLUMN venus.zj_trip_vendor_order.order_detail_id is 'Order - validated by clean.order_details';
COMMENT ON COLUMN venus.zj_trip_vendor_order.venus_id is 'Validated by venus.venus.venus_id';
COMMENT ON COLUMN venus.zj_trip_vendor_order.created_on is 'Id of user that created the record';
COMMENT ON COLUMN venus.zj_trip_vendor_order.created_by is 'Date record was created';
COMMENT ON COLUMN venus.zj_trip_vendor_order.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN venus.zj_trip_vendor_order.updated_by is 'Date record was last updated';




CREATE TABLE ftd_apps.box (
          box_id                        number(11)         NOT NULL,
          box_name                      varchar2(100)      NOT NULL,
          box_desc                      varchar2(1000)     NOT NULL,
          width_inch_qty                number(7,2)        NOT NULL,
          height_inch_qty               number(7,2)        NOT NULL,
          length_inch_qty               number(7,2)        NOT NULL,
          standard_flag                 VARCHAR2(1) DEFAULT 'N' NOT NULL,
          created_by                    varchar2(100)      not null,
          created_on                    date               not null,
          updated_by                    varchar2(100)      not null,
          updated_on                    date               not null,
    CONSTRAINT box_pk PRIMARY KEY(box_id) 
            USING INDEX  TABLESPACE ftd_apps_INDX,
    CONSTRAINT box_ck1 CHECK(standard_flag IN('Y','N')))
 TABLESPACE ftd_apps_DATA;


COMMENT ON TABLE  ftd_apps.box IS 'Contains information related to different boxes used by FTD for products';
COMMENT ON COLUMN ftd_apps.box.box_id is 'Box Identifier - Sequence';
COMMENT ON COLUMN ftd_apps.box.box_name is 'Short Description';
COMMENT ON COLUMN ftd_apps.box.box_desc is 'Long Description';
COMMENT ON COLUMN ftd_apps.box.width_inch_qty is 'Width in inches of the box';
COMMENT ON COLUMN ftd_apps.box.height_inch_qty is 'Height in inches of the box';
COMMENT ON COLUMN ftd_apps.box.length_inch_qty is 'Length in inches of the box';
COMMENT ON COLUMN ftd_apps.box.standard_flag is 'Denotes whether the box is a standard sized box';
COMMENT ON COLUMN ftd_apps.box.created_on is 'Id of user that created the record';
COMMENT ON COLUMN ftd_apps.box.created_by is 'Date record was created';
COMMENT ON COLUMN ftd_apps.box.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN ftd_apps.box.updated_by is 'Date record was last updated';





---- END FIX 2.6.0 RELEASE SCRIPT -------------------
