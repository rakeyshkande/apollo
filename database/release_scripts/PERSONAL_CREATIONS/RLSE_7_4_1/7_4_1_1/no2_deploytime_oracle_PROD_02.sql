------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

update FRP.SECURE_CONFIG set value = global.encryption.encrypt_it('ftd_orderfeedprod','APOLLO_PROD_2014'), 
updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'order_processing' and name = 'INBOUND_FTP_LOGON_00135';

update FRP.SECURE_CONFIG set value = global.encryption.encrypt_it('ftd_orderfeedprod','APOLLO_PROD_2014'), 
updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'order_processing' and name = 'VENDOR_00135_FTP_LOGON_1';

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/20/2015  --------
-- defect #1024 (mark) request #131295              --------------------------------
------------------------------------------------------------------------------------

INSERT INTO SITESCOPE.PAGERS (PROJECT, PAGER_NUMBER) VALUES ('Merchandising Monitors', 'agorman@ftdi.com');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/20/2015  --------
-- defect #1024 (mark) request #131295              --------------------------------
------------------------------------------------------------------------------------

