------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130389              --------------------------------
------------------------------------------------------------------------------------

set serveroutput on 
alter user events identified by "one-time" account unlock;
conn events/one-time
declare

   v_job number;
   v_date date;

begin

   select daemon_pkg.venus_pc_outbound_interval into v_date from dual;
   dbms_job.SUBMIT(job=>v_job,what=>'job_venus_pc_outbound;',next_date=>v_date,interval=>'daemon_pkg.venus_pc_outbound_interval');
   dbms_output.put_line(to_char(v_job));

   select daemon_pkg.venus_pc_inbound_interval into v_date from dual;
   dbms_job.SUBMIT(job=>v_job,what=>'job_venus_pc_inbound;',next_date=>v_date,interval=>'daemon_pkg.venus_pc_inbound_interval');
   dbms_output.put_line(to_char(v_job));
   
end;
/

conn &&dba_user
alter user events identified by values 'disabled' account lock;

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130389              --------------------------------
------------------------------------------------------------------------------------

