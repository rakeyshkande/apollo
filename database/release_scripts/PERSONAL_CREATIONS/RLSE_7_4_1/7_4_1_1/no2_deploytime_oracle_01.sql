------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/13/2015  --------
-- defect #1024 (mark) request #129958              --------------------------------
------------------------------------------------------------------------------------

update rpt.report_parm
set display_name = 'Non-Argo Vendor', updated_on = sysdate, updated_by = 'DEFECT_1024'
where REPORT_PARM_ID in (100227);

update rpt.report
set name = 'Non-Argo Orders Taken',
description = 'The Non-Argo Orders Taken Detail report is an ad-hoc report that can be executed for up a 31-day order taken range.  The report will show line-item detail of all orders taken within the time frame specified by the user.',
report_file_prefix = 'Non_Argo_Orders_Taken', updated_on = sysdate, updated_by = 'DEFECT_1024'
where REPORT_ID = 100227;

update rpt.report
set name = 'Non-Argo Refunds',
description = 'The Non-Argo Refunds Detail report is an ad-hoc report that can be executed for up a 31-day transaction date range.  The report will show line-item detail of all refunds within the time frame specified by the user.',
report_file_prefix = 'Non_Argo_Refunds', updated_on = sysdate, updated_by = 'DEFECT_1024'
where REPORT_ID = 100228;

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/13/2015  --------
-- defect #1024 (mark) request #129958              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

update frp.global_parms set value = '/cygdrive/f/FTP/ftd_orderfeedqa/Inbound', updated_on = sysdate, updated_by = 'DEFECT_1024' where context = 'ORDER_PROCESSING_CONFIG' and name = 'INBOUND_REMOTE_SOURCE_PATH_00135';

update frp.global_parms set value = 'Inbound/', updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'ORDER_PROCESSING_CONFIG' and name = 'INBOUND_REMOTE_DIRECTORY_00135';

update frp.global_parms set value = 'Outbound/', updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'ORDER_PROCESSING_CONFIG' and name = 'VENDOR_00135_REMOTE_DIRECTORY';

update frp.global_parms set value = '10.64.40.71', updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'ORDER_PROCESSING_CONFIG' and name = 'VENDOR_00135_REMOTE_LOCATION_1';

update frp.global_parms set value = '10.64.40.71', updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'ORDER_PROCESSING_CONFIG' and name = 'INBOUND_REMOTE_LOCATION_00135';

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

