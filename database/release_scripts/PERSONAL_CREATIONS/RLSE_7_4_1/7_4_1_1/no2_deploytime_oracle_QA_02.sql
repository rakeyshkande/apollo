------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

update FRP.SECURE_CONFIG set value = global.encryption.encrypt_it('ftd_orderfeedqa','APOLLO_TEST_2010'), 
updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'order_processing' and name = 'INBOUND_FTP_LOGON_00135';

update FRP.SECURE_CONFIG set value = global.encryption.encrypt_it('ftd_orderfeedqa','APOLLO_TEST_2010'), 
updated_on = sysdate, updated_by = 'DEFECT_1024'  where context = 'order_processing' and name = 'VENDOR_00135_FTP_LOGON_1';

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/15/2015  --------
-- defect #1024 (mark) request #130382              --------------------------------
------------------------------------------------------------------------------------

