------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/26/2014  -------- 
-- defect #1024 (mark) request #128122              --------------------------------
------------------------------------------------------------------------------------

CREATE SEQUENCE VENUS.PC_ORDER_NUMBER_SQ MINVALUE 1 INCREMENT BY 1 START WITH 1 CACHE 200 NOORDER NOCYCLE;

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/26/2014  -------- 
-- defect #1024 (mark) request #128122              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/ 9/2015  --------
-- defect #1024 (mark) request #129490              --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ORDER_PROCESSING_CONFIG','INBOUND_REMOTE_SOURCE_PATH_00135','/home/reports/inbound','DEFECT_1024',
SYSDATE,'DEFECT_1024',SYSDATE, 'Remote source path that contains inbound files');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/ 9/2015  --------
-- defect #1024 (mark) request #129490              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                                1/13/2015  --------
-- defect #1024 (mark) request #129958              --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES ( 'WINE VENDORS', 'PERSONAL CREATIONS', '00135', 'DEFECT_1024', sysdate, 'DEFECT_1024', sysdate);

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                                1/13/2015  --------
-- defect #1024 (mark) request #129958              --------------------------------
------------------------------------------------------------------------------------

