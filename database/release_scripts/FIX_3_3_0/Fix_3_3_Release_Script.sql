--------------------------------
-- Fix_3_3_Release_Script.sql --
--------------------------------

-- PRE-DEPLOY SECTION

-- 5541

create table clean.segmentation_stage (customer_id number, tier_value varchar2(10), created_on date) tablespace stage;

alter table clean.customer add (tier_value varchar2(10));

comment on column clean.customer.tier_value is 'segmentation tier for this customer, fed from data warehouse';

 
--
-- 5388 
--
#################
#  REMINDER - the following table needs a purge script.  Sent threshold request to JP and Cindy on 11/10
#################
create table venus.shipment_option 
(      SHIPMENT_option_ID          NUMBER(11)     NOT NULL,
       SDS_VENUS_ORDER_NUMBER         VARCHAR2(11),
       SDS_CARRIER_ID                 VARCHAR2(100),
       SDS_SHIP_METHOD_ID             VARCHAR2(100),
       SDS_ZONE_CODE                  NUMBER(10),
       SDS_RATE_WEIGHT                NUMBER(10,4),
       SDS_RATE_CHARGE                NUMBER(12,7),
       SDS_HANDLING_CHARGE            NUMBER(12,7),
       SDS_TOTAL_CHARGE               NUMBER(12,7),
       SDS_FREIGHT_CHARGE             NUMBER(12,7),
       SDS_RATE_CALC_TYPE             VARCHAR2(20),
       SDS_DATE_SHIPPED               DATE,
       SDS_EXPECTED_DELIVERY_DATE     DATE,
       SDS_DAYS_IN_TRANSIT            NUMBER(3),
       SDS_DISTRIBUTION_CENTER        VARCHAR2(50),
       SDS_SHIP_POINT_ID              VARCHAR2(50),
       SDS_ERROR_TXT                  VARCHAR2(2000),
       ZONE_JUMP_FLAG                 VARCHAR2(1) DEFAULT 'N' NOT NULL,
       IDEAL_RANK_NUMBER              NUMBER(5)     NOT NULL,
       ACTUAL_RANK_NUMBER             NUMBER(5)     NOT NULL,
       CREATED_ON                     TIMESTAMP     NOT NULL,
    CONSTRAINT shipment_option_pk PRIMARY KEY(shipment_option_id) 
            USING INDEX  TABLESPACE venus_INDX
    )
 TABLESPACE venus_data;

create index venus.shipment_option_n1 on venus.shipment_option(created_on) tablespace venus_indx;

comment on table venus.shipment_option IS 'Shipment data captured for DataWarehouse.  Is not used by Apollo.';
comment on column venus.shipment_option.SHIPMENT_option_ID IS
'Primary key     Unique identifier of the record.  Will be defined as a sequence';

comment on column venus.shipment_option.SDS_VENUS_ORDER_NUMBER IS
'Comes from sds rate response xml CARTON field.  No validation performed.  For additional info on this column, refer to - venus.venus.venus_order_number';

comment on column venus.shipment_option.SDS_CARRIER_ID IS
'Comes from sds rate response xml.  For additional info on this column, refer to - venus.venus.final_carrier';

comment on column venus.shipment_option.SDS_SHIP_METHOD_ID IS
'Comes from sds rate response xml.  For additional info on this column, refer to - venus.venus.final_ship_method';

comment on column venus.shipment_option.SDS_ZONE_CODE                IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_RATE_WEIGHT              IS
'Comes from sds rate response xml.  For additional info on this column, refer to - venus.venus.product_weight';

comment on column venus.shipment_option.SDS_RATE_CHARGE              IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_HANDLING_CHARGE          IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_TOTAL_CHARGE             IS  
'Comes from sds rate response xml.  Free form, no validation required';

comment on column venus.shipment_option.SDS_FREIGHT_CHARGE           IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_RATE_CALC_TYPE           IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_DATE_SHIPPED             IS  'Comes from sds rate response xml.'; 

comment on column venus.shipment_option.SDS_EXPECTED_DELIVERY_DATE   IS
'Comes from sds rate response xml.  No validation performed.  This field can refer to an injection hub or a vendor id, depending on the type of shipment.';

comment on column venus.shipment_option.SDS_DAYS_IN_TRANSIT          IS
'Comes from sds rate response xml.  For additional info on this column, refer to - venus.zj_injection_hub.ship_point_id';

comment on column venus.shipment_option.SDS_DISTRIBUTION_CENTER      IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.SDS_SHIP_POINT_ID            IS  
'Comes from sds rate response xml.  For additional info on this column, refer to - venus.venus.zone_jump_label_date';

comment on column venus.shipment_option.SDS_ERROR_TXT                IS  'Comes from sds rate response xml.';  

comment on column venus.shipment_option.ZONE_JUMP_FLAG               IS  'Indicates whether shipment option is for Zone Jump';

comment on column venus.shipment_option.IDEAL_RANK_NUMBER            IS  'Ideal shipping rank as calculated by scan data.';

comment on column venus.shipment_option.ACTUAL_RANK_NUMBER           IS  'Actual shipping rank used.';

comment on column venus.shipment_option.CREATED_ON                   IS  'Date/time this entry created.';

alter table venus.shipment_option add constraint shipment_option_ck1 check (zone_jump_flag in ('Y','N'));

create sequence venus.shipment_option_sq
   minvalue 1 maxvalue 99999999999 start with 1 increment by 1 cache 50;
  
insert into ops$oracle.index_rebuild_schedule (owner,index_name,days_between_rebuilds) values ('VENUS','SHIPMENT_OPTION_PK',14);  
insert into ops$oracle.index_rebuild_schedule (owner,index_name,days_between_rebuilds) values ('VENUS','SHIPMENT_OPTION_N1',14);  

grant select on venus.shipment_option to bi_etl;
--
--  END 5388
--

-- END PRE-DEPLOY SECTION

--
-- 5169
--

alter table clean.payments_update add
  (NC_APPROVAL_IDENTITY_ID             VARCHAR2(255),
      CONSTRAINT payments_update_fk4  FOREIGN KEY(nc_approval_identity_id)  REFERENCES aas.identity(identity_id));

alter table ftd_apps.session_order_master add
  (NC_APPROVAL_IDENTITY_ID             VARCHAR2(255));
  
--
-- END 5169
--







-- 5540 AND 5620

alter table ftd_apps.product_master rename column recipe to standard_recipe;
alter table ftd_apps.product_master$ rename column recipe to standard_recipe;

-- this will take several (many) minutes    NOTE - Requires EDB re-snapshot
-- **NOTE** - This will fail because of a couple of triggers.  Disable those triggers.  One will
--           be recreated for the shadow table, and one when the table is re-snapshotted for EDB
alter table ftd_apps.product_master add 
  (
        deluxe_recipe                 varchar2(255),
        premium_recipe                varchar2(255),
        send_standard_recipe_flag     varchar2(1)     DEFAULT 'N' NOT NULL,
        send_deluxe_recipe_flag       varchar2(1)     DEFAULT 'N' NOT NULL,
        send_premium_recipe_flag      varchar2(1)     DEFAULT 'N' NOT NULL,
        personal_greeting_flag        varchar2(1)     DEFAULT 'N' NOT NULL,        
    constraint product_master_ck15     CHECK(send_standard_recipe_flag IN('Y','N')),
    constraint product_master_ck16     CHECK(send_deluxe_recipe_flag IN('Y','N')),
    constraint product_master_ck17     CHECK(send_premium_recipe_flag IN('Y','N')),  
    constraint product_master_ck18     CHECK(personal_greeting_flag IN('Y','N'))  
  );

comment on column ftd_apps.product_master.personal_greeting_flag is 'Denotes whether the product can have a personal (voice) greeting associated with it.';

alter table ftd_apps.product_master$ add 
  (
        deluxe_recipe                 varchar2(255),
        premium_recipe                varchar2(255),
        send_standard_recipe_flag     varchar2(1) ,
        send_deluxe_recipe_flag       varchar2(1),
        send_premium_recipe_flag      varchar2(1),
        personal_greeting_flag        varchar2(1));

alter table scrub.order_details add (PERSONAL_GREETING_ID varchar2(10));

comment on column scrub.order_details.personal_greeting_id is 'Personal Greeting (Voice Express) ID';

alter table clean.order_details add (PERSONAL_GREETING_ID varchar2(10));

comment on column clean.order_details.personal_greeting_id is 'Personal Greeting (Voice Express) ID'; 

alter table venus.venus add (PERSONAL_GREETING_ID varchar2(10));

comment on column venus.venus.personal_greeting_id is 'Personal Greeting (Voice Express) ID';

create sequence ftd_apps.content_filter_sq;

create Table FTD_APPS.CONTENT_FILTER (
CONTENT_FILTER_ID NUMBER not null,
CONTENT_FILTER_NAME VARCHAR2(100) not null,
UPDATED_BY VARCHAR2(100) not null,
UPDATED_ON DATE not null,
CREATED_BY VARCHAR2(100) not null,
CREATED_ON DATE not null) tablespace ftd_apps_data;

comment on column ftd_apps.content_filter.content_filter_id is 'Unique sequence generated id';

alter table ftd_apps.content_filter add constraint content_filter_pk primary key (content_filter_id) using index tablespace
ftd_apps_indx;

create sequence ftd_apps.content_master_sq;

create Table FTD_APPS.CONTENT_MASTER (
CONTENT_MASTER_ID NUMBER not null,
CONTENT_CONTEXT VARCHAR2(100) not null,
CONTENT_NAME VARCHAR2(100) not null,
CONTENT_DESCRIPTION VARCHAR2(4000) not null,
CONTENT_FILTER_ID1 NUMBER,
CONTENT_FILTER_ID2 NUMBER,
UPDATED_BY VARCHAR2(100) not null,
UPDATED_ON DATE not null,
CREATED_BY VARCHAR2(100) not null,
CREATED_ON DATE not null) tablespace ftd_apps_data;

comment on column ftd_apps.content_master.content_master_id is 'Unique sequence generated id';
 
comment on column ftd_apps.content_master.CONTENT_FILTER_ID1 is 'ID for the first Filter for the Content';

alter table ftd_apps.content_master add (constraint content_master_pk primary key (content_master_id) using index tablespace
ftd_apps_indx);

alter table ftd_apps.content_master add (constraint content_master_fk1 foreign key (content_filter_id1) references
ftd_apps.content_filter(content_filter_id));

comment on column ftd_apps.content_master.CONTENT_FILTER_ID2 is 'ID for the second Filter for the Content';

alter table ftd_apps.content_master add (constraint content_master_fk2 foreign key (content_filter_id2) references
ftd_apps.content_filter(content_filter_id));

create sequence ftd_apps.content_detail_sq;
 
create Table FTD_APPS.CONTENT_DETAIL (
CONTENT_DETAIL_ID NUMBER not null,
CONTENT_MASTER_ID NUMBER not null,
FILTER_1_VALUE VARCHAR2(100),
FILTER_2_VALUE VARCHAR2(100),
CONTENT_TXT VARCHAR2(4000) not null,
UPDATED_BY VARCHAR2(100) not null,
UPDATED_ON DATE not null,
CREATED_BY VARCHAR2(100) not null,
CREATED_ON DATE not null) tablespace ftd_apps_indx;

comment on column ftd_apps.content_detail.CONTENT_DETAIL_ID is 'Unique sequence generated id';

alter table ftd_apps.content_detail add constraint content_detail_pk primary key (content_detail_id) using index tablespace
ftd_apps_indx;
 
alter table ftd_apps.content_detail add constraint content_detail_fk1 foreign key (CONTENT_MASTER_ID) references
ftd_apps.content_master;

insert into FTD_APPS.CONTENT_FILTER 
(CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'CUSTOMER_TIER', 'tlynema', sysdate, 'tlynema', sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PERSONAL_GREETINGS', 'RECORDING_PHONE', 'Recording Phone for personal greeting', null, null, 'tlynema' , sysdate ,  'tlynema' , sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PERSONAL_GREETINGS', 'IDENTIFIER', 'The text that is displayed before the personal greeting id in confirmation emails', null, null, 'tlynema' , sysdate ,  'tlynema' , sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PERSONAL_GREETINGS', 'INSTRUCTIONS_TEXT', 'Instructions for the personal greeting in text format', null, null, 'tlynema' , sysdate ,  'tlynema' , sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PERSONAL_GREETINGS', 'INSTRUCTIONS_HTML', 'Instructions for the personal greeting in HTML format', null, null, 'tlynema' , sysdate ,  'tlynema' , sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PERSONAL_GREETINGS', 'ORDER_COMMENT', 'Order comment to be added for personal greeting order', null, null, 'gsergy' , sysdate ,  'gsergy' , sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PHONE_NUMBER', 'CALL_CENTER_CONTACT_NUMBER', 'Phone number for the call center for customer support', (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'CUSTOMER_TIER'), null, 'tlynema' , sysdate ,  'tlynema' , sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE,  FILTER_2_VALUE,  CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PERSONAL_GREETINGS' AND CONTENT_NAME = 'RECORDING_PHONE') , null,  null, '1-203-454-6994' , 'tlynema', sysdate, 'tlynema', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,  CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PERSONAL_GREETINGS' AND CONTENT_NAME = 'IDENTIFIER'), null, null, 'Personal Greeting Code:' , 'tlynema', sysdate, 'tlynema', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE,  FILTER_2_VALUE,  CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PERSONAL_GREETINGS' AND CONTENT_NAME = 'INSTRUCTIONS_TEXT'), null,  null,  
'To record your Personal Voice Greeting:
  1) Call ~pgphone~
  2) Enter your Personal Greeting Code when prompted
  3) Record your Personal Greeting by following the prompts.
   We recommend that you record your personal greeting immediately after you order. You have 1 hour after you place your order to record your message. After 1 hour if your personal voice greeting is not recorded we will use the default message of "Someone thinks you''re special! Enjoy this beautiful bouquet from FTD!"
   Don''t know what to say? Here are a few sentiments: 1) Happy Valentine''s Day! Will you be Mine? 2) Be My Valentine!' , 'tlynema', sysdate, 'tlynema', sysdate);
   
 insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE,  FILTER_2_VALUE,  CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PERSONAL_GREETINGS' AND CONTENT_NAME = 'INSTRUCTIONS_HTML'), null, null, '<br>To record your Personal Voice Greeting:<br>
  1) Call ~pgphone~<br>
  2) Enter your Personal Greeting Code when prompted<br>
  3) Record your Personal Greeting by following the prompts.<br>
   We recommend that you record your personal greeting immediately after you order. You have 1 hour after you place your order to record your message. After 1 hour if your personal voice greeting is not recorded we will use the default message of "Someone thinks you''re special! Enjoy this beautiful bouquet from FTD!"<br>
   Don''t know what to say? Here are a few sentiments: 1) Happy Valentine''s Day! Will you be Mine? 2) Be My Valentine!<br>' , 'tlynema', sysdate, 'tlynema', sysdate);
  
insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PERSONAL_GREETINGS' AND CONTENT_NAME = 'ORDER_COMMENT'), null, null, 'This is a Personal Greeting product.  Their Personal Greeting Code is ~pgid~.  To record their personal message, please have the customer call the following number:  ', 'tlynema', sysdate, 'tlynema', sysdate);
   
insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALL_CENTER_CONTACT_NUMBER'), null, null, '1-800-SEND-FTD', 'tlynema', sysdate, 'tlynema', sysdate);
   
insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALL_CENTER_CONTACT_NUMBER'), 'A',  null, '1-866-233-2811', 'tlynema', sysdate, 'tlynema', sysdate);
  
 
-- END 5540 AND 5620

-- 2564
create table ftd_apps.avs_response_codes (
avs_code          char(1) not null,
payment_method_id varchar2(2),
description       varchar2(255) not null) tablespace ftd_apps_data;

comment on table ftd_apps.avs_response_codes is
'dropdown descriptions of Address Verification System (AVS) codes associated with credit card types';

comment on column ftd_apps.avs_response_codes.avs_code is 'one-letter code that can be returned from AVS';

comment on column ftd_apps.avs_response_codes.payment_method_id is 'credit card type';

comment on column ftd_apps.avs_response_codes.description is 'meaning of this one-letter AVS code for this credit card type';

alter table ftd_apps.avs_response_codes add constraint avs_response_codes_u1 unique (avs_code,payment_method_id) using index
tablespace ftd_apps_indx;

grant select on ftd_apps.avs_response_codes to clean;

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING',
    'FLORIST_REFERENCE_NUMBER_SUFFIX_DELUXE',
    'D',
    'The suffix to be appended to the florist reference number for deluxe price points',
    sysdate,
    'Defect 5540',
    sysdate,
    'Defect 5540');

INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING',
    'FLORIST_REFERENCE_NUMBER_SUFFIX_PREMIUM',
    'P',
    'The suffix to be appended to the florist reference number for premium price points',
    sysdate,
    'Defect 5540',
    sysdate,
    'Defect 5540');

insert into ftd_apps.avs_response_codes values
    ('Y', 'AX', 'Yes, Billing Address and Postal Code are both correct.');
insert into ftd_apps.avs_response_codes values
    ('N', 'AX', 'No, Billing Address and Postal Code are both incorrect.');
insert into ftd_apps.avs_response_codes values
    ('A', 'AX', 'Billing Address only correct.');
insert into ftd_apps.avs_response_codes values
    ('Z', 'AX', 'Billing Postal Code only correct.');
insert into ftd_apps.avs_response_codes values
    ('U', 'AX', 'Information unavailable.');
insert into ftd_apps.avs_response_codes values
    ('S', 'AX', 'SE not allowed AAV function.');
insert into ftd_apps.avs_response_codes values
    ('R', 'AX', 'System unavailable; retry.');
insert into ftd_apps.avs_response_codes values
    ('L', 'AX', 'CM Name and Billing Postal Code match.');
insert into ftd_apps.avs_response_codes values
    ('M', 'AX', 'CM Name, Billing Address, and Postal Code match.');
insert into ftd_apps.avs_response_codes values
    ('O', 'AX', 'CM Name and Billing Address match.');
insert into ftd_apps.avs_response_codes values
    ('K', 'AX', 'CM Name matches.');
insert into ftd_apps.avs_response_codes values
    ('D', 'AX', 'CM Name incorrect, Billing Postal Code matches.');
insert into ftd_apps.avs_response_codes values
    ('E', 'AX', 'CM Name incorrect, Billing Address and Postal Code match.');
insert into ftd_apps.avs_response_codes values
    ('F', 'AX', 'CM Name incorrect, Billing Address matches.');
insert into ftd_apps.avs_response_codes values
    ('W', 'AX', 'No, CM Name, Billing Address, and Postal Code are all incorrect.');

insert into ftd_apps.avs_response_codes values
    ('A', null, 'Address: Address matches, ZIP code does not match');
insert into ftd_apps.avs_response_codes values
  ('B', null, 'Incompatible formats (postal code): Street addresses match. Postal code not verified due to incompatible formats.');
insert into ftd_apps.avs_response_codes values
   ('C', null, 'Incompatible formats (all information): Street address and postal code not verified due to incompatible formats.');
insert into ftd_apps.avs_response_codes values
    ('D', null, 'Street addresses and postal codes match.');
insert into ftd_apps.avs_response_codes values
    ('E', null, 'Edit error: For example, AVS not allowed for this transaction.');
insert into ftd_apps.avs_response_codes values
    ('G', null, 'Global non-AVS participant.');
insert into ftd_apps.avs_response_codes values
    ('I', null, 'International Transaction: Address information not verified for international transaction.');
insert into ftd_apps.avs_response_codes values
    ('M', null, 'Match: Street addresses and postal codes match.');
insert into ftd_apps.avs_response_codes values
    ('N', null, 'No: Address and ZIP code do not match.');
insert into ftd_apps.avs_response_codes values
    ('P', null, 'Postal codes match. Street address not verified due to incompatible formats.');
insert into ftd_apps.avs_response_codes values
    ('R', null, 'Retry: System unavailable or timed out.');
insert into ftd_apps.avs_response_codes values
    ('S', null, 'Service not supported: Issuer does not support AVS at Visa, INAS, or the issuer processing center.');
insert into ftd_apps.avs_response_codes values
    ('U', null, 'Unavailable: Address information not verified for domestic transactions.');
insert into ftd_apps.avs_response_codes values
    ('W', null, 'Whole ZIP: Nine-digit ZIP code matches, address does not match.');
insert into ftd_apps.avs_response_codes values
    ('X', null, 'Exact: Address and nine-digit ZIP code match.');
insert into ftd_apps.avs_response_codes values
    ('Y', null, 'Yes: Address and five-digit ZIP code match.');
insert into ftd_apps.avs_response_codes values
    ('Z', null, 'Zip: Five-digit ZIP code matches, address does not match.');


-- END 2564

-- 2033

update clean.gc_coupon_request
set    company_id='FTD', updated_on=sysdate, updated_by='defect_fix_2033'
where  company_id is null;



-- Cleanup from 3.2
drop trigger joe.product_cross_sell_$;
drop table joe.product_cross_sell$;
drop table joe.product_cross_sell;



-- 5642
alter table ftd_apps.inv_trk add 
  (  CONSTRAINT inv_trk_fk3  FOREIGN KEY(vendor_id, product_id)  REFERENCES ftd_apps.vendor_product(vendor_id, product_subcode_id));




-- 5219   
-- DB *** QA ONLY ***

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','JELLYFISH_OutboundFtpUser',global.encryption.encrypt_it('devftp','APOLLO_TEST_2008'),
'Jellyfish file transfer user name',sysdate,'chu', sysdate,'chu','APOLLO_TEST_2008');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','JELLYFISH_OutboundFtpPswd',global.encryption.encrypt_it('lli7tst','APOLLO_TEST_2008'),
'Jellyfish file transfer user password',sysdate,'chu', sysdate,'chu','APOLLO_TEST_2008');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('PARTNER_REWARD_CONFIG','JELLYFISH_OUTBOUND_SERVER','sodium.ftdi.com','chu',sysdate,'chu',sysdate, 'FTP remote server for Jellyfish refund file.');


-- DB *** PROD ONLY ***

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','JELLYFISH_OutboundFtpUser',global.encryption.encrypt_it('ftd','APOLLO_PROD_2008'),
'Jellyfish file transfer user name',sysdate,'chu', sysdate,'chu','APOLLO_PROD_2008');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','JELLYFISH_OutboundFtpPswd',global.encryption.encrypt_it('digg4w7fh','APOLLO_PROD_2008'),
'Jellyfish file transfer user password',sysdate,'chu', sysdate,'chu','APOLLO_TEST_2008');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('PARTNER_REWARD_CONFIG','JELLYFISH_OUTBOUND_SERVER','datafeeds.jellyfish.com','chu',sysdate,'chu',sysdate, 'FTP remote server for Jellyfish refund file.');

-- END 5219   
 

####################################################
## FIX 3.3.2 CODE
####################################################

-- 5752
alter table joe.order_details add (PERSONAL_GREETING_ID varchar2(10));
comment on column joe.order_details.personal_greeting_id is 'Personal Greeting (Voice Express) ID'; 


alter table clean.mo_xref add (PERSONAL_GREETING_ID varchar2(10));
comment on column clean.mo_xref.personal_greeting_id is 'Personal Greeting (Voice Express) ID'; 

alter table clean.order_detail_update add (PERSONAL_GREETING_ID varchar2(10));
comment on column clean.order_detail_update.personal_greeting_id is 'Personal Greeting (Voice Express) ID'; 


INSERT INTO FRP.GLOBAL_PARMS
    (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values (
    'ORDER_PROCESSING_MAINT_CONFIG',
    'OPM_URL',
    'http://consumer-test.ftdi.com/orderprocessingmaint',
    'URL prefix for order processing maintenance',
    sysdate,
    'Defect 5575',
    sysdate,
    'Defect 5575');




alter table venus.zj_trip add (days_in_transit_qty number(3)  null);
update venus.zj_trip set days_in_transit_qty = 1;
alter table venus.zj_trip modify (days_in_transit_qty not null);



create sequence key.personal_greeting_sq
   minvalue 1 maxvalue 9999999999 start with 1 increment by 7 cache 100;
 grant execute on key.keygen to osp;


----
---- END Fix 3.3 Release Script
----
