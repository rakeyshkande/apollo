


 CREATE TABLE clean.order_hold$ (
           ORDER_DETAIL_ID         NUMBER             NOT NULL,
           REASON                  VARCHAR2(20)       NOT NULL,
           CREATED_ON              DATE               NOT NULL,
           CREATED_BY              VARCHAR2(100)      NOT NULL,
           UPDATED_ON              DATE               NOT NULL,
           UPDATED_BY              VARCHAR2(100)      NOT NULL,
           TIMEZONE                NUMBER,
           AUTH_COUNT              NUMBER,
           RELEASE_DATE            DATE,
           STATUS                  CHAR(1),
           REASON_TEXT             VARCHAR2(30),
           OPERATION$              VARCHAR2(7)        not null,
           TIMESTAMP$              timestamp          not null)
  TABLESPACE CLEAN_DATA;


create index clean.order_hold$_n1 on clean.order_hold$ (timestamp$) tablespace clean_indx;

insert into ops$oracle.index_rebuild_schedule select 'CLEAN','ORDER_HOLD$_N1', 30 from dual;
 

deploy ftd/database/clean_plsql/clean_order_hold_$.trg   Version 1.1


deploy dba/local/maint/purge_tables.sql   Version 1.17
