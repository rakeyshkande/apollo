------------------------------------------------------------------------------------
-- Begin requested by Bhargava Surimenu                         05/08/2017  --------
-- user story 16          (mwoytus    )                 ----------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.florist_zips to clean;

------------------------------------------------------------------------------------
-- End   requested by Bhargava Surimenu                         05/08/2017  --------
-- user story 16          (mwoytus    )                 ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Manasa salla		                        05/22/2017  --------
-- user story 1401	(10.2.0.24) (kmohamme )		        ----------------------------
------------------------------------------------------------------------------------

--1--
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'PARTNER_REWARD',
    'SITEWIDE_DISCOUNT_SOURCE_CODE',
    '33952',
    sysdate,
    'US_1401',
    sysdate,
    'US_1401',
    'The site wide discount source code');

--2--	
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'PARTNER_REWARD',
    'SITEWIDE_DISCOUNT_ALLOWED',
    'Y',
    sysdate,
    'US_1401',
    sysdate,
    'US_1401',
    'The flag to set Y/N for the SITEWIDE_DISCOUNT_SOURCE_CODE');
	
--3--
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'PARTNER_REWARD',
    'IMAGE_URL',
    'https://www.ftdimg.com/pics/products/zoom/',
    sysdate,
    'US_1401',
    sysdate,
    'US_1401',
    'The image URL for CI feed');

--4--	
INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'PARTNER_REWARD',
    'IMAGE_SIZE',
    '_600x600.jpg',
    sysdate,
    'US_1401',
    sysdate,
    'US_1401',
    'The image size for the CI feed');
	
------------------------------------------------------------------------------------
-- End   requested by Manasa salla		                        05/22/2017  --------
-- user story 1401 (10.2.0.24)   (kmohamme )     	    ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by   Gary Sergey                       05/23/2017  --------
-- sprint 3          (srizvi    )                 ----------------------------
------------------------------------------------------------------------------------

UPDATE VENUS.CARRIERS SET TRACKING_URL = 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=' WHERE CARRIER_ID = 'USPS';


------------------------------------------------------------------------------------
-- End requested by   Gary Sergey                       05/23/2017  --------
-- sprint 3          (srizvi    )                 ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by   Gary Sergey                       05/31/2017  --------
-- story story 22:          (srizvi    )                 ----------------------------
------------------------------------------------------------------------------------


grant select on ftd_apps.price_header to clean;


------------------------------------------------------------------------------------
-- End requested by   Gary Sergey                       05/31/2017  --------
-- story story 22:          (srizvi    )                 ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by   Naveenkumar K                      07/26/2017  --------
-- story story 2407:          (pthakur    )                 ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DESCRIPTION)
VALUES (
    'PAS_CONFIG', 'PAS_SVC_CONN_TIMEOUT', '5000', sysdate, 'SYS', sysdate, 'SYS', 'Apollo Connection time out for PAS Service');


INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DESCRIPTION)
VALUES (
    'PAS_CONFIG', 'PAS_SVC_RECV_TIMEOUT', '5000', sysdate, 'SYS', sysdate, 'SYS', 'Apollo Receive time out for PAS Service');

	

------------------------------------------------------------------------------------
-- End requested by    Naveenkumar K                      07/26/2017 --------
-- story story 2407:          (pthakur  )                 ----------------------------
------------------------------------------------------------------------------------




