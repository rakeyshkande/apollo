-----------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                        10/22/2014  --------
-- defect #582 (Mark)                              --------------------------
-----------------------------------------------------------------------------

insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS ACK',
    'ACCOUNTING',
    'Process BAMS Acknowledgment Files',
    'Retrieves Daily BAMS Acknowledgment Files',
    '0 0 2-6 * * ?',
    'BAMS-ACK',
    'N',
    'OJMS.BAMS_ACK',
    'SimpleJmsJob',
    'Y');

-----------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                        10/22/2014  --------
-- defect #582 (Mark)                              --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Ragu Salguti,                      10/24/2014  --------
-- defect #1017 (Mark)                             --------------------------
-----------------------------------------------------------------------------

alter table joe.order_details add (total_tax_rate number);

-----------------------------------------------------------------------------
-- end   requested by Ragu Salguti,                      10/24/2014  --------
-- defect #1017 (Mark)                             --------------------------
-----------------------------------------------------------------------------
