-------------------------------------------------------------------
-- begin requested by Swami Patsa,             10/22/2014  --------
-- defect #582                                   ------------------
-------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckSFtpServer','www.mftcat.firstdataclients.com','defect_582',Sysdate,'defect_582',Sysdate,
'BAMS Acknowledgement SFTP Server');

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckSFtpLocation','/MNOR-001751','defect_582',Sysdate,'defect_582',Sysdate,
'BAMS Acknowledgement SFTP remote location');

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckLocalLocation','/u02/apollo/accounting/pts/ack/','defect_582',Sysdate,'defect_582',Sysdate,
'BAMS Acknowledgement local folder');

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckLocalArchiveLocation','/u02/apollo/accounting/pts/ack/archive/','defect_582',Sysdate,'defect_582',
Sysdate,'BAMS Acknowledgement local archive folder');

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckSupportEmailGroup','qabuild@sodium.ftdi.com','defect_582',Sysdate,'defect_582',Sysdate,
'BAMS Acknowledgement Support email group');

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values 
('ACCOUNTING_CONFIG','BamsAckSecurityPrivateKeyPath','/home/oracle/.ssh/ftd-apollo-test-sec.asc','defect_582',sysdate,'defect_582',
sysdate,'BAMS Acknowledgement private key location');



Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME) 
Values 
('ACCOUNTING_CONFIG','BamsAckSFtpUsername',Global.Encryption.Encrypt_It('MNOR-001751', 'APOLLO_TEST_2010'),
'BAMS Acknowledgement SFTP user name',Sysdate,'defect_582',Sysdate,'defect_582','APOLLO_TEST_2010');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME) 
Values 
('ACCOUNTING_CONFIG','BamsAckSFtpPassword',Global.Encryption.Encrypt_It('testptspwd1234', 'APOLLO_TEST_2010'),
'BAMS Acknowledgement SFTP password',Sysdate,'defect_582',Sysdate,'defect_582','APOLLO_TEST_2010');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME) 
Values 
('ACCOUNTING_CONFIG','BamsAckSecurityPrivateKeyPassphrase',Global.Encryption.Encrypt_It('Ap01Lo-t3st_3113', 'APOLLO_TEST_2010'),
'BAMS Acknowledgement Private key passphrase',Sysdate,'defect_582',Sysdate,'defect_582','APOLLO_TEST_2010');


-------------------------------------------------------------------
-- end   requested by Swami Patsa,             10/22/2014  --------
-- defect #582                                   ------------------
-------------------------------------------------------------------

