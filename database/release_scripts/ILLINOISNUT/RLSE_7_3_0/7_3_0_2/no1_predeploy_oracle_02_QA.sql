-------------------------------------------------------------------
-- begin requested by Swami Patsa,              9/ 3/2014  --------
-- defect #568                                   ------------------
-------------------------------------------------------------------

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,
CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME) values 
('ACCOUNTING_CONFIG','MERCHANT_SECURITY_CODE',
global.encryption.encrypt_it('fixme', 'APOLLO_TEST_2010'),
'Merchant Security Code',SYSDATE,'defect_568-570',SYSDATE,'defect_568-570',
'APOLLO_TEST_2010');

-------------------------------------------------------------------
-- begin requested by Swami Patsa,              9/ 3/2014  --------
-- defect #568                                   ------------------
-------------------------------------------------------------------



-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-- modification requested  by Santhosh pilli
-- modification requested by Vasantala Kishor Kumar    9/12/2014 ------
-------------------------------------------------------------------


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.url',
    'http://paymentservice-qa01.ftdi.com/payment/authorizeCC/',
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'REST Web service URL for JCCAS client');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.username',
    global.encryption.encrypt_it('fixme','APOLLO_TEST_2010'),
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'user name  for authenticating Jccas rest service');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.password',
    global.encryption.encrypt_it('fixme','APOLLO_TEST_2010'),
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'password  for authenticating Jccas rest service');
    
    -------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-- modification requested  by Santhosh pilli
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Srivathsala Meka,         9/ 9/2014  --------
-- defect #199 (Woytus)                          ------------------
-------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ('TAX_CONFIG', 'TAX_SERVICE_URL', 'http://10.222.72.117/tax-service/svc/taxService', 'DEFECT_199', SYSDATE, 'DEFECT_199', 
SYSDATE, 'The tax service URL. Replace the host name as per the environment.');

-------------------------------------------------------------------
-- end   requested by Srivathsala Meka,         9/ 9/2014  --------
-- defect #199 (Woytus)                          ------------------
-------------------------------------------------------------------

