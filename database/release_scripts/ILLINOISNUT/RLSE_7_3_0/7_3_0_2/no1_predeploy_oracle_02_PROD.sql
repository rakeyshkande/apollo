-------------------------------------------------------------------
-- begin requested by Swami Patsa,              9/ 2/2014  --------
-- defect #568                                   ------------------
-------------------------------------------------------------------

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,
CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME) values 
('ACCOUNTING_CONFIG','MERCHANT_SECURITY_CODE',
global.encryption.encrypt_it('fixme', 'APOLLO_PROD_2013'),
'Merchant Security Code',SYSDATE,'defect_568-570',SYSDATE,'defect_568-570',
'APOLLO_PROD_2013');

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              9/ 2/2014  --------
-- defect #568                                   ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-- modification requested  by Santhosh pilli
-------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.url',
    'fixme',
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'REST Web service URL for JCCAS client');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.username',
    global.encryption.encrypt_it('fixme','APOLLO_PROD_2013'),
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'user name  for authenticating Jccas rest service');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'SERVICE',
    'jccas.ws.password',
    global.encryption.encrypt_it('fixme','APOLLO_PROD_2013'),
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'password  for authenticating Jccas rest service');

-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-- modification requested  by Santhosh pilli
-------------------------------------------------------------------

