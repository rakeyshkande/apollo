-------------------------------------------------------------------
-- begin requested by Swami Patsa,              7/10/2014  --------
-- defect #97 (JLIN)                             ------------------
-------------------------------------------------------------------

alter table JOE.order_payments add  CC_AUTH_PROVIDER varchar2(100);
alter table SCRUB.payments add  CC_AUTH_PROVIDER varchar2(100);
alter table CLEAN.payments add CC_AUTH_PROVIDER varchar2(100);
alter table CLEAN.payments_update add CC_AUTH_PROVIDER varchar2(100); 

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              7/10/2014  --------
-- defect #97 (JLIN)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #557 (Pavan)                             ------------------
-- modified by Santhosh Kumar, Pilli on 9/3/2014
-------------------------------------------------------------------
CREATE TABLE JOE.PAYMENTS_EXT (
	PAYMENT_ID	NUMBER,
	AUTH_PROPERTY_NAME 		VARCHAR2(100)	NOT NULL,
	AUTH_PROPERTY_VALUE 	VARCHAR2(2000),
	CREATED_ON             DATE   NOT NULL, 
	CREATED_BY             VARCHAR2(100)  NOT NULL,
	UPDATED_ON             DATE  NOT NULL, 
	UPDATED_BY             VARCHAR2(100)  NOT NULL,
	CONSTRAINT PAYMENTS_EXT_FK1    FOREIGN KEY (PAYMENT_ID)    REFERENCES JOE.ORDER_PAYMENTS(PAYMENT_ID) ENABLE
);

-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #557 (Pavan)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #550 (Pavan)                             ------------------
-------------------------------------------------------------------

CREATE TABLE SCRUB.PAYMENTS_EXT (
	PAYMENT_ID	NUMBER,
	AUTH_PROPERTY_NAME 		VARCHAR2(100)	NOT NULL,
	AUTH_PROPERTY_VALUE 	VARCHAR2(100),
	CREATED_ON             DATE   NOT NULL, 
	CREATED_BY             VARCHAR2(100)  NOT NULL,
	UPDATED_ON             DATE  NOT NULL, 
	UPDATED_BY             VARCHAR2(100)  NOT NULL,
	CONSTRAINT PAYMENTS_EXT_FK1    FOREIGN KEY (PAYMENT_ID)    REFERENCES SCRUB.PAYMENTS(PAYMENT_ID) ENABLE
);


ALTER TABLE SCRUB.PAYMENTS ADD (	
	INITIAL_AUTH_AMOUNT NUMBER(12,2)
);

-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #550 (Pavan)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #575 (Pavan)                             ------------------
-------------------------------------------------------------------

CREATE TABLE CLEAN.PAYMENTS_EXT (
	PAYMENT_ID	NUMBER,
	AUTH_PROPERTY_NAME 		VARCHAR2(100)	NOT NULL,
	AUTH_PROPERTY_VALUE 	VARCHAR2(100),
	CREATED_ON             DATE   NOT NULL, 
	CREATED_BY             VARCHAR2(100)  NOT NULL,
	UPDATED_ON             DATE  NOT NULL, 
	UPDATED_BY             VARCHAR2(100)  NOT NULL,
	CONSTRAINT PAYMENTS_EXT_FK1    FOREIGN KEY (PAYMENT_ID)    REFERENCES CLEAN.PAYMENTS(PAYMENT_ID) ENABLE
);


CREATE TABLE CLEAN.PAYMENTS_EXT$ (
	PAYMENT_ID	NUMBER	NOT NULL,
	AUTH_PROPERTY_NAME 		VARCHAR2(100)	NOT NULL,
	AUTH_PROPERTY_VALUE 	VARCHAR2(100),
	CREATED_ON             DATE   NOT NULL, 
	CREATED_BY             VARCHAR2(100)  NOT NULL,
	UPDATED_ON             DATE  NOT NULL, 
	UPDATED_BY             VARCHAR2(100)  NOT NULL,
	OPERATION$            VARCHAR2(7),
	TIMESTAMP$            DATE           
);


ALTER TABLE CLEAN.PAYMENTS ADD (	
	IS_VOICE_AUTH 	CHAR(1),
	INITIAL_AUTH_AMOUNT NUMBER(12,2)	
);
-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #575 (Pavan)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Srivathsala Meka,         9/ 9/2014  --------
-- defect #199 (Woytus)                          ------------------
-------------------------------------------------------------------

--Insert values into FTD_APPS.TAX_DISPLAY_ORDER_VAL
INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('State tax', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('County tax', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('City tax', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('Zip tax', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('Surcharge*', '1', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('GST/HST', '1', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('PST', '2', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('QST', '2', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

-------------------------------------------------------------------
-- end   requested by Srivathsala Meka,         9/ 9/2014  --------
-- defect #199 (Woytus)                          ------------------
-------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Salguti Raghu Ram,                         9/12/2014  --------
-- defect #601 (Pavan)                              --------------------------
-----------------------------------------------------------------------------

alter table JOE.ORDER_DETAILS add (tax_service_performed VARCHAR2(1),tax_total_description VARCHAR2(20));

-----------------------------------------------------------------------------
-- end   requested by Salguti Raghu Ram,                         9/12/2014  --------
-- defect #601 (Pavan)                              --------------------------
-----------------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Swami Patsa,              8/ 5/2014  --------
-- defect #18008                                 ------------------
-------------------------------------------------------------------

alter table clean.eod_payment_recovery add CC_AUTH_PROVIDER varchar2(100);
alter table clean.Billing_Header add CC_AUTH_PROVIDER varchar2(100);

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              8/ 5/2014  --------
-- defect #18008                                 ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Ritesh kumar singh,              8/14/2014  --------
-- defect #552   (pavan)                       ------------------
--- modification requested by Surimenu Bhargava Swamy    9/12/2014  
-------------------------------------------------------------------

-- This table gets replicated
ALTER TABLE FTD_APPS.addon  ADD (florist_addon_flag CHAR(1) default 'Y',vendor_addon_flag CHAR(1) default 'N');

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              8/14/2014  --------
-- defect #552   (pavan)                 ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Patsa, Swami,              8/27/2014  --------
-- defect #568 & 570 (JLIN)                       ------------------
-------------------------------------------------------------------

alter table CLEAN.BILLING_DETAIL add INITIAL_AUTH_AMOUNT NUMBER(12,2);

alter table CLEAN.BILLING_DETAIL add IS_VOICE_AUTH CHAR(1);

-------------------------------------------------------------------
-- end requested by Patsa, Swami,              8/27/2014  --------
-- defect #568 & 570 (JLIN)                       ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              9/9/2014  --------
-- defect #551 (Pavan)                             ------------------
-------------------------------------------------------------------

update ftd_apps.payment_methods set active_flag='N',joe_flag='N' where payment_method_id='CB';
commit;

-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,             9/9/2014  --------
-- defect #551 (Pavan)                             ------------------
-------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                         9/11/2014  --------
-- defect #636 (Mark)                              --------------------------
-----------------------------------------------------------------------------

update ftd_apps.addon a
set florist_addon_flag = (select decode(at.delivery_type, 'Florist', 'Y', 'N')
    from tschmig.addon_temp at
    where at.addon_id = a.addon_id),
    vendor_addon_flag = (select decode(at.delivery_type, 'Drop Ship', 'Y', 'N')
    from tschmig.addon_temp at
    where at.addon_id = a.addon_id),
    updated_on = sysdate,
    updated_by = 'Defect_636';

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                         9/11/2014  --------
-- defect #636 (Mark)                              --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                         9/11/2014  --------
-- defect #555 (Mark)                              --------------------------
-----------------------------------------------------------------------------

update joe.element_config set recalc_flag = 'Y' where element_id = 'recipientZip';

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                         9/11/2014  --------
-- defect #555 (Mark)                              --------------------------
-----------------------------------------------------------------------------

