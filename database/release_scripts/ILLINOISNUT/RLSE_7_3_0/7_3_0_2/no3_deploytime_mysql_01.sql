
-------------------------------------------------------------------
-- begin requested by Ritesh kumar singh,              8/14/2014  --------
-- defect #552   (pavan)                       ------------------
-------------------------------------------------------------------
- Take the following steps:
-- 1. on source, stop extract
-- 2. on target, stop replicat
-- 3. make change on source side on FTD_APPS.ADDON table with two additional columns
-- 4. regenerate defgen file on source side
-- 5. move defgen file to target
-- 6. make the below change on the target. Should replicate to slaves
MYSQL> use ftd_apps;
MYSQL> alter table addon add (florist_addon_flag CHAR(1),vendor_addon_flag CHAR(1));
-- 7. on source, start extract
-- 8. on target, start replicat

-- ALTER TABLE FTD_APPS.addon  ADD (florist_addon_flag CHAR(1) default 'N',vendor_addon_flag CHAR(1) default 'Y');
-- UPDATE FTD_APPS.addon set florist_addon_flag ='Y' where addon_type in (1,2,4,5);

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              8/14/2014  --------
-- defect #552   (pavan)                 ------------------
-------------------------------------------------------------------

