-------------------------------------------------------------------
-- begin requested by Swami Patsa,              6/26/2014  --------
-- defect #97 (JLIN)                             ------------------
-------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('SHIPPING_PARMS','USPS_TRACKING_REGEX','(\d{22})$','defect_97',SYSDATE,'defect_97',SYSDATE,
'This is regular expression to extract last 22 numbers from USPS tracking number from scan file');

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              6/26/2014  --------
-- defect #97 (JLIN)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'AUTH_CONFIG',
    'cc.auth.provider',
    'BAMS',
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'Credit card authorization provider');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'AUTH_CONFIG',
    'bams.cc.list',
    'VI,DI,DC,MC',
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'Comma separated credit card list that are accepted by BAMS for auth');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'AUTH_CONFIG',
    'jccas.ws.socket.timeout',
    '6000',
    sysdate,
    'Defect_558',
    'Defect_558',
    sysdate,
    'http socket timeout for jccas service');
    
    

Commit;

-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #558 (Pavan)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar,              8/21/2014  --------
-- defect #560 (Pavan)                             ------------------
-- modified by Vasantala Kishor Kumar 8/26/2014
-------------------------------------------------------------------

CREATE SEQUENCE key.jccas_requestid_sq START WITH 1000  INCREMENT BY 1  minvalue 1000 MAXVALUE 999999 CYCLE CACHE 50;
-------------------------------------------------------------------
-- end   requested by Vasantala Kishor Kumar ,              8/21/2014  --------
-- defect #560 (Pavan)                             ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Rose Lazuk,               8/25/2014  --------
-- defect #565 (Woytus)                          ------------------
-------------------------------------------------------------------

-- Run ojms_objects/bams_eod_cart.sql
-- Run ojms_objects/bams_eod_dispatcher.sql

-------------------------------------------------------------------
-- end   requested by Rose Lazuk,               8/25/2014  --------
-- defect #565 (Woytus)                          ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Rose Lazuk,              8/27/2014  --------
-- defect #565   (JLIN)                       ------------------
-------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','DISPATCH_BAMS_EOD_FLAG','Y','DEFECT_565',SYSDATE,'DEFECT_565',SYSDATE,
'Set to Y to enable BAMS End of Day processing');

INSERT INTO EVENTS.EVENTS ( EVENT_NAME, CONTEXT_NAME, DESCRIPTION, ACTIVE ) 
VALUES ( 'BAMS-EOD-FINALIZER', 'ACCOUNTING', 'BAMS End-Of-Day finalizer', 'Y' );

-------------------------------------------------------------------
-- end   requested by Rose Lazuk,              8/27/2014  --------
-- defect #565   (JLIN)                 ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Patsa, Swami,              8/27/2014  --------
-- defect #568 & 570 (JLIN)                       ------------------
-------------------------------------------------------------------
--Merchant Names
Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_NAME_FTD','ftd.com','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company Name for FTD Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_NAME_FTDCA','ftdca.com','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company Name for FTDCA Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_NAME_FLORIST','florist.com','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company Name for Florist Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_NAME_FLYFLW','FlyingFlowers.com','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company Name for Flying Flowers Merchant');

--Merchant Cities
Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_CITY_FTD','800-736-3383','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company City for FTD Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_CITY_FTDCA','800-425-0622','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company City for FTDCA Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_CITY_FLORIST','800-794-6383','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company City for Florist Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_CITY_FLYFLW','855-489-5568','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company City for Flying Flowers Merchant');

--Merchant Account
Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_ACCOUNT_FTD','372603207880','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company ACCOUNT for FTD Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_ACCOUNT_FTDCA','372603209886','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company ACCOUNT for FTDCA Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_ACCOUNT_FLORIST','372603211882','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company ACCOUNT for Florist Merchant');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_ACCOUNT_FLYFLW','TBD','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Company ACCOUNT for Flying Flowers Merchant');

--MISC
Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','BAMS_CC_TYPES','DC,DI,MC,VI','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'CC types that are being processed by BAMS');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_CATEGORY_CODE','5965','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Merchant category code used in settlement file');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','MERCHANT_ACCOUNTS','FTD,FTDCA,FLORIST,FLYFLW','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Different company names used to generate PTS files. Comma separated without any spaces');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','FTP_SERVER_PTS','TBD','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Remote ftp server for PTS settlement file');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','FTP_REMOTE_DIRECTORY_PTS','TBD','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Remote directory for PTS settlement file');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','FTP_local_DIRECTORY_PTS','TBD','defect_568-570',SYSDATE,'defect_568-570',SYSDATE,
'Local directory for PTS settlement file');

-------------------------------------------------------------------
-- end requested by Patsa, Swami,              8/27/2014  --------
-- defect #568 & 570 (JLIN)                       ------------------
-------------------------------------------------------------------

