

------------------------------------------------------------------------------------
-- begin requested by Pilli Santhosh Kumar ,                         9/15/2014  --------
-- defect #566  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

CREATE SEQUENCE key.jccas_ref_num_sq START WITH 70000000 INCREMENT BY 1 MINVALUE 70000000 MAXVALUE 99999999 CYCLE CACHE 50;

------------------------------------------------------------------------------------
-- end   requested by Pilli Santhosh Kumar ,                         9/15/2014  --------
-- defect #566  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                                 9/16/2014  --------
-- defect #583  (Mark)                               ---------------------------------
--------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','MAX_PTS_SEQ_NUMBER','999999','defect_583',SYSDATE,'defect_583',SYSDATE,
'This is used to store maximum record sequence number for pts settlement file');

commit;
--------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                                 9/16/2014  --------
-- defect #583  (Mark)                               ---------------------------------
--------------------------------------------------------------------------------------





------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala ,                         9/19/2014  --------
-- defect #744  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE clean.order_bills MODIFY (
tax1_rate number, tax1_amount number,
tax2_rate number, tax2_amount number,
tax3_rate number, tax3_amount number,
tax4_rate number, tax4_amount number,
tax5_rate number, tax5_amount number
);


------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala  ,                         9/19/2014  --------
-- defect #744  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------







