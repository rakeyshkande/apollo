------------------------------------------------------------------------------------
-- begin requested by Salguti Raghu Ram,                          9/24/2014  --------
-- defect #808  (pavan)                              --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE joe.order_details MODIFY (
tax1_rate number, tax1_amount number,
tax2_rate number, tax2_amount number,
tax3_rate number, tax3_amount number,
tax4_rate number, tax4_amount number,
tax5_rate number, tax5_amount number
);

------------------------------------------------------------------------------------
-- end   requested by Salguti Raghu Ram,                          9/24/2014  --------
-- defect #808  (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE clean.refund MODIFY (
tax1_rate number, tax1_amount number,
tax2_rate number, tax2_amount number,
tax3_rate number, tax3_amount number,
tax4_rate number, tax4_amount number,
tax5_rate number, tax5_amount number
);

------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------
insert into QUARTZ_SCHEMA.GROUPS values('TAX ADMIN', 'Tax admin', 'Y');

insert into QUARTZ_SCHEMA.PIPELINE values('Tax Admin', 'TAX ADMIN', 'Download Vertex Tax Rates',  'downloadTaxRates - Download vertex tax rates file',null,null,'N',
'OJMS.TAX_RATE_UPDATES','SimpleJmsJob','Y');
 
------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Pilli  Santhosh Kumar,                         10/8/2014  --------
-- defect #579   (pavan)                              --------------------------------
------------------------------------------------------------------------------------
 ALTER TABLE CLEAN.PAYMENTS$ ADD ( 
                CC_AUTH_PROVIDER varchar2(100),
                IS_VOICE_AUTH               CHAR(1),
                INITIAL_AUTH_AMOUNT NUMBER(12,2)             
);

------------------------------------------------------------------------------------
-- end   requested by Pilli  Santhosh Kumar,                         10/8/2014  --------
-- defect #579   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                         9/16/2014  --------
-- defect #571 (Mark)                              --------------------------
-----------------------------------------------------------------------------

alter table clean.billing_detail add (csc_response_code varchar2(1));

-----------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                         9/16/2014  --------
-- defect #571 (Mark)                              --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                         9/23/2014  --------
-- defect #813 (Mark)                              --------------------------
-----------------------------------------------------------------------------

grant select on clean.order_bills to joe;
grant select on clean.payments    to joe;
grant select on clean.credit_cards to joe;
grant select on clean.order_add_ons to joe;

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                         9/23/2014  --------
-- defect #813 (Mark)                              --------------------------
-----------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

delete frp.GLOBAL_PARMS where context = 'ACCOUNTING_CONFIG' and name in ('FTP_SERVER_PTS','FTP_REMOTE_DIRECTORY_PTS',
'FTP_local_DIRECTORY_PTS');

delete from frp.secure_config where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_SECURITY_CODE';

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                         9/29/2014  --------
-- defect #813 (Mark)                              --------------------------
-----------------------------------------------------------------------------

grant select on ftd_apps.program_reward to joe;
grant select on ftd_apps.source_program_ref to joe;
grant select on ftd_apps.ship_methods to joe;
grant select on ftd_apps.addon_history to joe;

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                         9/29/2014  --------
-- defect #813 (Mark)                              --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Swami Patsa,                        9/29/2014  --------
-- defect #572 (Mark)                              --------------------------
-----------------------------------------------------------------------------

delete from FRP.global_parms where context='ACCOUNTING_CONFIG' and name='BAMS_CC_TYPES';

-----------------------------------------------------------------------------
-- begin requested by Swami Patsa,                        9/29/2014  --------
-- defect #572 (Mark)                              --------------------------
-----------------------------------------------------------------------------

