------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamspublickeypath','/home/oracle/.ssh/FD_MW_ssh.pub','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'BAMS public key path');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamsSFTPServer','WWW.MFT3.FIRSTDATACLIENTS.COM','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'SFTP server name');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamsSFTPLocation','/MNOS-TO-FDC','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,
'SFTP remote directory location');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_BY,CREATED_ON,UPDATED_ON,UPDATED_BY,KEY_NAME) 
values ('accounting_reporting','bamsSFTPUsername',global.encryption.encrypt_it('MNOR-001751', 'APOLLO_PROD_2013'), 'BAMS sftp user name', 
'defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'APOLLO_PROD_2013');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_BY,CREATED_ON,UPDATED_ON,UPDATED_BY,KEY_NAME) 
values ('accounting_reporting','bamsSFTPPassword',global.encryption.encrypt_it('fixme', 'APOLLO_PROD_2013'), 'BAMS sftp password', 
'defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'APOLLO_PROD_2013');

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

