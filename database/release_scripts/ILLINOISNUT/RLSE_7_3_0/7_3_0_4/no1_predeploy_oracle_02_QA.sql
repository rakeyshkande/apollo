------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

Insert into frp.secure_config (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION,KEY_NAME) 
values ('TAX_CONFIG', 'FTP_SERVER_USERNAME', global.encryption.encrypt_it('taxrates','APOLLO_TEST_2010'), 
'defect_199', SYSDATE, 'defect_199', SYSDATE, 'Remote FTP server login username','APOLLO_TEST_2010');

Insert into frp.secure_config (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION,KEY_NAME) 
values ('TAX_CONFIG', 'FTP_SERVER_PWD', global.encryption.encrypt_it('fixme', 'APOLLO_TEST_2010'),
'defect_199', SYSDATE, 'defect_199', SYSDATE, 'Remote FTP server login password','APOLLO_TEST_2010');

------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamspublickeypath','/home/oracle/.ssh/FirstData_armor.asc','defect_572-583',SYSDATE,
'defect_572-583',SYSDATE,'BAMS public key path');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamsSFTPServer','www.mftcat.firstdataclients.com','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,
'SFTP server name');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','bamsSFTPLocation','/MNOR-To-FDC','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,
'SFTP remote directory location');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,KEY_NAME) 
values ('accounting_reporting','bamsSFTPUsername',global.encryption.encrypt_it('MNOR-001751', 'APOLLO_TEST_2010'), 
'BAMS sftp user name', 'defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'APOLLO_TEST_2010');

Insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,KEY_NAME) 
values ('accounting_reporting','bamsSFTPPassword',global.encryption.encrypt_it('testptspwd1234', 'APOLLO_TEST_2010'), 
'BAMS sftp password', 'defect_572-583',SYSDATE,'defect_572-583',SYSDATE,'APOLLO_TEST_2010');

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------
Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('TAX_CONFIG', 'FTP_TAX_RATES_DIR', '/QA', 'defect_199', SYSDATE, 'defect_199', SYSDATE,
'Remote FTP directory from root to find tax rates');


 
------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala,                         10/8/2014  --------
-- defect #199   (pavan)                              --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan ,                         10/9/2014  --------
-- defect #548   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

 INSERT INTO FRP.GLOBAL_PARMS VALUES ('TAX_CONFIG','TAX_ADMIN_URL','http://taxservice-qa01.ftdi.com/tax-admin/taxAdmin','DEFECT_199',sysdate,'DEFECT_199',
 sysdate,'Tax Admin URL to upload the tax rules');
 
INSERT INTO FRP.GLOBAL_PARMS VALUES ('TAX_CONFIG','APOLLO_SECURITY_SERVICE_URL','http://consumer-test.ftdi.com/secadmin/services/ApolloSecurityService?wsdl','DEFECT_199',sysdate,'DEFECT_199',
sysdate,'Apollo Security Service URL to validate the security token');

INSERT INTO FRP.SECURE_CONFIG VALUES ('TAX_CONFIG','APOLLO_SECURITY_SERVICE_UNAME',global.encryption.encrypt_it('SECURITY_SERVICE_TAX_ID', 'APOLLO_TEST_2010'),'DEFECT_199',
sysdate,'DEFECT_199',sysdate,'Apollo Security Service User Name', 'APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG VALUES ('TAX_CONFIG','APOLLO_SECURITY_SERVICE_PWD',global.encryption.encrypt_it('Q7A8A6X1N', 'APOLLO_TEST_2010'),'DEFECT_199',sysdate,'DEFECT_199',
sysdate,'Apollo Security Service Password', 'APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG VALUES ('SERVICE','SECURITY_SERVICE_TAX_ID',global.encryption.encrypt_it('Q7A8A6X1N', 'APOLLO_TEST_2010'),'DEFECT_199',sysdate,'DEFECT_199',
sysdate,'SECURITY SERVCIE TAX ADMIN AUTHENTICATION', 'APOLLO_TEST_2010');
 
 commit;
------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan ,                         10/9/2014  --------
-- defect #548   (pavan)                              --------------------------------
------------------------------------------------------------------------------------
