------------------------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar   ,                         9/22/2014  --------
-- defect #560  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

CREATE SEQUENCE STATS.AUTHORIZATION_RESPOSE_ID_SEQ START WITH 1000  INCREMENT BY 1  minvalue 1000 MAXVALUE 999999 CYCLE CACHE 50;

------------------------------------------------------------------------------------
-- end   requested by  Vasantala Kishor Kumar ,                         9/22/2014  --------
-- defect #560  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Vasantala Kishor Kumar   ,                         9/22/2014  --------
-- defect #713  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE STATS.AUTHORIZATION_RESPONSE_STATS (
                                                                AUTHORIZATION_RESPONSE_ID              NUMBER NOT NULL,
                                                                REFERENCE_NUMBER    VARCHAR2(100),
                                                                CREDIT_CARD_TYPE       VARCHAR2(20),
                                                                VALIDATION_STATUS_CODE      VARCHAR(1),
                                                                REQUEST_XML                  CLOB,
                                                                RESPONSE_XML               CLOB,
                                                                TIMESTAMP                       DATE,
                CONSTRAINT AUTH_RESPONSE_STATS_PK1   PRIMARY KEY (AUTHORIZATION_RESPONSE_ID)   
);

------------------------------------------------------------------------------------
-- end   requested by  Vasantala Kishor Kumar ,                         9/22/2014  --------
-- defect #713  (Pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Srivathsala Meka,                          9/23/2014  --------
-- defect #199  (Mark)                              --------------------------------
------------------------------------------------------------------------------------
--
-- execute ojms_objects/tax_rate_updates.sql
--
------------------------------------------------------------------------------------
-- end   requested by Srivathsala Meka,                          9/23/2014  --------
-- defect #199  (Mark)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','MERCHANT_SECURITY_CODE','4884','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,
'This is unique identifier used in settlement file in M record and it does not require encryption as this is displayed as part of file name as clear text.');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('ACCOUNTING_CONFIG','DEPOSIT_CLASS_ID','CPTD4884','defect_572-583',SYSDATE,'defect_572-583',SYSDATE,
'Class id is used in settlement file name.');

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                               9/24/2014  --------
-- defect #572 #583 (mark)                          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Meka Srivathsala,                         10/6/2014  --------
-- defect #689   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('State', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');
INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('City', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');
INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('Zip', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');
INSERT INTO FTD_APPS.TAX_DISPLAY_ORDER_VAL (TAX_NAME, DISPLAY_ORDER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
VALUES ('County', '0', SYSDATE, 'DEFECT_199', SYSDATE, 'DEFECT_199');

------------------------------------------------------------------------------------
-- end   requested by Meka Srivathsala,                         10/6/2014  --------
-- defect #689   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Chandna  Abhishek,                         10/7/2014  --------
-- defect #607   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

INSERT INTO rpt.report
(REPORT_ID, NAME,DESCRIPTION, REPORT_FILE_PREFIX,REPORT_TYPE,REPORT_OUTPUT_TYPE,REPORT_CATEGORY,SERVER_NAME,
HOLIDAY_INDICATOR,STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,REPORT_DAYS_SPAN,NOTES,ACL_NAME,
RETENTION_DAYS,RUN_TIME_OFFSET,END_OF_DAY_REQUIRED,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
VALUES
(1002093,
'Refunds By Ship-To State (FTD.CA)',
'This monthly report is automatically executed on the first day of each month to outline all refunds for the entire previous month (first day through last day of month) and displays a summary of these orders first by US State, then by Canadian Province, and finally grouping all International orders together. The month end report will create 3 copies: (1) All Origins (2) Wal-Mart (WLMTI) only, and (3) Target only (TARGI)',
'Refunds_By_Ship_State_FTDCA','S','Char','Acct','http://neon.ftdi.com:7778/reports/rwservlet?','Y',
'Active',sysdate,'achand',sysdate,'achand','ACC09_Refunds_by_Ship_to_State.rdf',31,'Date range cannot exceed 31 days',
'BaseAcctReportAccess',10,120,'N','01','M','&p_origin_code=FTDCAI');

------------------------------------------------------------------------------------
-- end   requested by Chandna  Abhishek,                         10/7/2014  --------
-- defect #607   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Chandna  Abhishek  ,                         10/10/2014  --------
-- defect #471   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

---------This script has been removed as per request by Chandrana Abishek
 
------------------------------------------------------------------------------------
-- end   requested by Chandna  Abhishek  ,                         10/10/2014  --------
-- defect #471   (pavan)                              --------------------------------
------------------------------------------------------------------------------------

