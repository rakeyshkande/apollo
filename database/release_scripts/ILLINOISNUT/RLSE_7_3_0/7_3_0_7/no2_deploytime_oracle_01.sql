-----------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                        10/29/2014  --------
-- defect #1028 (Mark)                             --------------------------
-----------------------------------------------------------------------------

insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS DAILY ACK CHECK',
    'ACCOUNTING',
    'Daily BAMS ACK File Check Job',
    'Checks to See if All BAMS Acknowledgment Files for the Day have been Received',
    '0 0 9 ? * *',
    'DAILY-BAMS-ACK-CHECK',
    'N',
    'OJMS.BAMS_ACK',
    'SimpleJmsJob',
    'Y');

-----------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                        10/29/2014  --------
-- defect #1028 (Mark)                             --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Gary Sergey,                       10/29/2014  --------
-- defect #1028 (Mark)                             --------------------------
-----------------------------------------------------------------------------

update FTD_APPS.DNIS set DEFAULT_SOURCE_CODE = '8999' where DNIS_ID = 8887;

-----------------------------------------------------------------------------
-- begin requested by Gary Sergey,                       10/29/2014  --------
-- defect #1028 (Mark)                             --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                        10/31/2014  --------
-- defect #1056 (Mark)                             --------------------------
-----------------------------------------------------------------------------

insert into joe.element_config (
APP_CODE,
ELEMENT_ID,
LOCATION_DESC,
LABEL_ELEMENT_ID,
COUNT_ELEMENT_ID,
REQUIRED_FLAG,
REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME,
VALIDATE_FLAG,
VALIDATE_EXP,
VALIDATE_EXP_TYPE_NAME,
ERROR_TXT,
TITLE_TXT,
VALUE_MAX_BYTES_QTY,
TAB_SEQ,
TAB_INIT_SEQ,
TAB_COND_DESC,
TAB_COND_TYPE_NAME,
ACCORDION_INDEX_NUM,
TAB_CTRL_INDEX_NUM,
RECALC_FLAG,
PRODUCT_VALIDATE_FLAG,
ACCESS_KEY_NAME,
XML_NODE_NAME,
CALC_XML_FLAG
) values (
'JOE',
'productDeliveryMethod',
'DETAIL',
'productDeliveryMethodLabel',
null,
'N',
null,
null,
'N',
null,
null,
null,
'Item Delivery Method',
null,
2107,
-1,
null,
null,
0,
0,
'Y',
'N',
null,
null,
'N'
);

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                        10/31/2014  --------
-- defect #1056 (Mark)                             --------------------------
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- begin requested by Tim Schmig,                        10/31/2014  --------
-- defect #1048 (Mark)                             --------------------------
-----------------------------------------------------------------------------

update scrub.buyer_addresses
set state_province = 'NA'
where state_province = 'NotApplicable';

update clean.customer
set state = 'NA',
    updated_on = sysdate,
    updated_by = 'Defect_1048'
where state = 'NotApplicable';

-----------------------------------------------------------------------------
-- end   requested by Tim Schmig,                        10/31/2014  --------
-- defect #1048 (Mark)                             --------------------------
-----------------------------------------------------------------------------

