 ------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------

 insert into FRP.GLOBAL_PARMS values ('TAX_CONFIG','TAX_ADMIN_EMAIL_GROUP','kdatchan@sodium.ftdi.com','DEFECT_199',sysdate,'DEFECT_199',sysdate,'Email group(s) will receive the alerts whenever Tax Rules file is uploaded');
 
------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------

