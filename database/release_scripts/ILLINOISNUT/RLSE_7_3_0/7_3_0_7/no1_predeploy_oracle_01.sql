 ------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
 insert into FRP.GLOBAL_PARMS values 
 ('TAX_CONFIG','SMTP_PARAM_NAME','SMTP_ALTERNATE_HOST_NAME','DEFECT_199',sysdate,'DEFECT_199',sysdate,'Get the actual SMTP hostname from Message Generator Config using this value');

------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                              10/31/2014  --------
-- defect #1028    (mark)                           --------------------------------
------------------------------------------------------------------------------------

alter table clean.bams_settlement_totals drop primary key;

alter table clean.bams_settlement_totals add constraint bams_settlement_totals_pk primary key 
(settlement_total_id) using index tablespace clean_indx;

alter table clean.bams_settlement_totals add constraint bams_settlement_totals_u1 unique 
(FILE_CREATED_DATE_JULIAN,SUBMISSION_ID,BILLING_HEADER_ID) using index tablespace clean_indx;

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                              10/31/2014  --------
-- defect #1028    (mark)                           --------------------------------
------------------------------------------------------------------------------------

