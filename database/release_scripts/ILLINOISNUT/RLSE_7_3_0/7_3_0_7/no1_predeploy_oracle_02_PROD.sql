 ------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
 insert into FRP.GLOBAL_PARMS values ('TAX_CONFIG','TAX_ADMIN_EMAIL_GROUP', 'COMAccounting@ftdi.com','DEFECT_199',sysdate,'DEFECT_199',sysdate,'Email group(s) will receive the alerts whenever Tax Rules file is uploaded');
 
 
------------------------------------------------------------------------------------
-- end   requested by Datchanamoorthy  Karthikeyan  ,                              10/28/2014  --------
-- defect #199 (pavan)                           --------------------------------
------------------------------------------------------------------------------------


 ------------------------------------------------------------------------------------
-- begin requested by Vasantala  Kishor Kumar    ,                              10/29/2014  --------
-- defect #558 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
update FRP.GLOBAL_PARMS  set VALUE='https://jccas.prod.ftd.com/payment/authorizeCC/',UPDATED_BY='Defect_558',UPDATED_ON=sysdate where context='SERVICE' and name='jccas.ws.url';
 
------------------------------------------------------------------------------------
-- end   requested by Vasantala  Kishor Kumar    ,                              10/29/2014  --------
-- defect #558 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
