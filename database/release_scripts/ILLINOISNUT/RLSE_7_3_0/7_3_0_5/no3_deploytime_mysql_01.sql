------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                              10/17/2014  --------
-- defect #942 (mark)                               --------------------------------
------------------------------------------------------------------------------------

-- There will probably be some problems here. NEWSLETTER_COMPANY_ID and SENDING_FLORIST_NUMBER
-- are from previous releases, mistakenly did not get added to GG. After carefully adding these,
-- so the columns get replicated to CMS, will need to do a manual data update to synchronize 
-- all the MySQL values with Oracle.

use ftd_apps;
alter table company_master add NEWSLETTER_COMPANY_ID varchar(12);
alter table company_master add SENDING_FLORIST_NUMBER varchar(2000);
alter table company_master add DEFAULT_DNIS smallint;

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                              10/17/2014  --------
-- defect #942 (mark)                               --------------------------------
------------------------------------------------------------------------------------
 
