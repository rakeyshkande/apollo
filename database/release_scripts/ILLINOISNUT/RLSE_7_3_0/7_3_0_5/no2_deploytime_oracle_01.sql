------------------------------------------------------------------------------------
-- begin requested by Salguti  Raghu Ram  ,                     10/15/2014  --------
-- defect #199 (pavan)                              --------------------------------
------------------------------------------------------------------------------------
 
ALTER TABLE clean.order_bills_update MODIFY (
tax1_rate number, tax1_amount number,
tax2_rate number, tax2_amount number,
tax3_rate number, tax3_amount number,
tax4_rate number, tax4_amount number,
tax5_rate number, tax5_amount number
);

ALTER TABLE clean.refund$ MODIFY (
tax1_rate number, tax1_amount number,
tax2_rate number, tax2_amount number,
tax3_rate number, tax3_amount number,
tax4_rate number, tax4_amount number,
tax5_rate number, tax5_amount number
);

------------------------------------------------------------------------------------
-- end   requested by Salguti  Raghu Ram  ,                     10/15/2014  --------
-- defect #199 (pavan)                              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                              10/17/2014  --------
-- defect #942 (mark)                               --------------------------------
------------------------------------------------------------------------------------

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8880, 'Flowers Direct (Modify Order Default)', 'FDIRECTP', 'A', 'Y', 'N', 'DG', '10909', 'FDIRECT', 'FDIRECT', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8881, 'Florist.com (Modify Order Default)', 'FLORISTP', 'A', 'Y', 'N', 'DG', '10172', 'FLORIST', 'FLORIST', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8882, 'Flying Flowers (Modify Order Default)', 'FLYFLWP', 'A', 'Y', 'N', 'DG', '17677', 'FLYFLW', 'FLYFLW', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8884, 'FTD.CA (Modify Order Default)', 'FTDCAP', 'A', 'Y', 'N', 'DG', 'FTDCA', 'FTDCA', 'FTDCA', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8885, 'Flowers USA (Modify Order Default)', 'FUSAP', 'A', 'Y', 'N', 'DG', '1904', 'FUSA', 'FUSA', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8886, 'Gift Sense (Modify Order Default)', 'GIFTP', 'A', 'Y', 'N', 'DG', '8500', 'GIFT', 'GIFT', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (8887, 'Butterfield Blooms(Modify Order Default)', 'BLOMP', 'A', 'Y', 'N', 'DG', '9999', 'HIGH', 'HIGH', 'Default');

-- This is a GG/MySQL change 
alter table ftd_apps.COMPANY_MASTER add DEFAULT_DNIS NUMBER(4);

alter table ftd_apps.COMPANY_MASTER add CONSTRAINT company_master_FK2  FOREIGN KEY(default_dnis) REFERENCES ftd_apps.dnis(dnis_id);

COMMENT ON COLUMN FTD_APPS.COMPANY_MASTER.DEFAULT_DNIS IS 'Used as default DNIS for Modify Orders (since Web orders won''t have DNIS to start with)';

-- Update values for new column
--
update ftd_apps.company_master set default_dnis=8888;
update ftd_apps.company_master set default_dnis=8880 where company_id='FDIRECT';
update ftd_apps.company_master set default_dnis=8881 where company_id='FLORIST';
update ftd_apps.company_master set default_dnis=8882 where company_id='FLYFLW';
update ftd_apps.company_master set default_dnis=8884 where company_id='FTDCA';
update ftd_apps.company_master set default_dnis=8885 where company_id='FUSA';
update ftd_apps.company_master set default_dnis=8886 where company_id='GIFT';
update ftd_apps.company_master set default_dnis=8887 where company_id='HIGH';
 
------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                              10/17/2014  --------
-- defect #942 (mark)                               --------------------------------
------------------------------------------------------------------------------------
 
