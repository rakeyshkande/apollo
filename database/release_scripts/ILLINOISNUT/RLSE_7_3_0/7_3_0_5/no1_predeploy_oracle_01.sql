------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                              10/14/2014  --------
-- defect #568-570 (mark)                           --------------------------------
------------------------------------------------------------------------------------
-- I left the updated_on and updated_by fields unset because these entries are created in this same release

update FRP.global_parms set value = 'FTD.COM'           where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_NAME_FTD';
update FRP.global_parms set value = 'FTD.CA'            where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_NAME_FTDCA';
update FRP.global_parms set value = 'Florist.COM'       where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_NAME_FLORIST';
update FRP.global_parms set value = 'FlyingFlowers.COM' where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_NAME_FLYFLW';
update FRP.global_parms set value = '372603630883'      where context = 'ACCOUNTING_CONFIG' and name = 'MERCHANT_ACCOUNT_FLYFLW';

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                              10/14/2014  --------
-- defect #568-570 (mark)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                              10/15/2014  --------
-- defect #582 (mark)                               --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE CLEAN.BAMS_SETTLEMENT_TOTALS (
SETTLEMENT_TOTAL_ID      NUMBER NOT NULL ENABLE, 
FILE_CREATED_DATE_JULIAN NUMBER NOT NULL ENABLE, 
SUBMISSION_ID            NUMBER NOT NULL ENABLE, 
BILLING_HEADER_ID        Number Not Null Enable, 
PAYMENT_TOTAL            Number(11,2) Not Null Enable, 
REFUND_TOTAL             NUMBER(11,2) NOT NULL ENABLE, 
STATUS                   VARCHAR2(20 BYTE), 
CREATED_ON               DATE NOT NULL ENABLE, 
CREATED_BY               VARCHAR2(100 BYTE) NOT NULL ENABLE, 
UPDATED_ON               DATE, 
UPDATED_BY               Varchar2(100 Byte)) tablespace clean_data;

alter table clean.bams_settlement_totals add CONSTRAINT BAMS_SETTLEMENT_TOTALS_PK PRIMARY KEY (FILE_CREATED_DATE_JULIAN, 
SUBMISSION_ID) using index tablespace clean_indx;

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                              10/15/2014  --------
-- defect #582 (mark)                               --------------------------------
------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               10/15/2014  --------
-- defect #582 (mark)                               --------------------------------
------------------------------------------------------------------------------------

-- execute ojms_objects/bams_ack.sql
 
------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               10/15/2014  --------
-- defect #582 (mark)                               --------------------------------
------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------
-- begin requested by Abishek chandrana  ,                              10/17/2014  --------
-- defect #972 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
INSERT INTO FRP.GLOBAL_PARMS
(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES
('ORDER_GATHERER_CONFIG','TRANSFORM_STATE_CODES','QC:PQ|NF:NL','SYS','17-OCT-2014','SYS','17-OCT-2014',
'For handling Data Transformation for PI Orders');

commit;

------------------------------------------------------------------------------------
-- end   requested by Abishek chandrana  ,                              10/17/2014  --------
-- defect #972 (pavan)                           --------------------------------
------------------------------------------------------------------------------------
 
