
--###########################
--####    R E A D M E    ####
--###########################

--This script contains Apollo 2.3.0 database udpates which have been applied to dev AFTER 2.3.0 went to QA (QA5).
--The script is tagged FIX_23_INT_RTB because the mods are in DEV.  
--
--To deploy these changes to QA:
--      1. Deploy mod to QA
--      2. Copy mod to appropriate script (i.e. release_scripts.sql, grants.sql, etc.) and tag new version
--      3. Remove mod from this script and tag new version
--     
