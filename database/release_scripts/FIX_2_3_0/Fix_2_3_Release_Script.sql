
-- FIX 2.3.0 RELEASE SCRIPT


##########################################################################################
###### ITEMS AFTER THIS POINT HAVE BEEN DEPLOYED PRIOR TO 1/19
##########################################################################################

-- Defect 3024   #### ALREADY DEPLOYED TO PRODUCTION ####
CREATE TABLE VENUS.SCAN_FILES (
    SCAN_FILES_ID NUMBER(22) NOT NULL,
    CARRIER_ID    VARCHAR2(10) NOT NULL,
    FILE_NAME     VARCHAR2(256) NOT NULL,
    CREATED_ON    DATE NOT NULL,
    CREATED_BY    VARCHAR2(100) NOT NULL,
    CONTENT_TXT   CLOB) tablespace venus_data
    LOB (content_txt) STORE AS scan_files_content_lob (TABLESPACE venus_data DISABLE STORAGE IN ROW CACHE READS LOGGING);

alter table venus.scan_files add (CONSTRAINT SCAN_FILES_PK PRIMARY
KEY(SCAN_FILES_ID) USING INDEX TABLESPACE VENUS_INDX);

CREATE INDEX venus.scan_files_n1 on venus.scan_files (created_on) tablespace venus_indx;

CREATE SEQUENCE VENUS.SCAN_FILES_SQ INCREMENT BY 1 START WITH 1 NOCYCLE NOCACHE NOORDER;
-- END Defect 3024

 
--### DEFECT 1791 ###
INSERT  INTO FRP.GLOBAL_PARMS
(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
VALUES
('ORDER_PROCESSING','FLORIST_SOFT_BLOCK_DAYS','1','SYS',SYSDATE,'SYS',SYSDATE);
--### END DEFECT 1791 ###
 
---DEFECT 1105
ALTER TABLE clean.billing_detail          add (key_name VARCHAR2(30));
ALTER TABLE clean.cc_settlement_errors    ADD (key_name VARCHAR2(30));
ALTER TABLE clean.credit_cards            ADD (key_name VARCHAR2(30));
ALTER TABLE clean.credit_cards_update     ADD (key_name VARCHAR2(30));
ALTER TABLE clean.customer_hold_entities  ADD (key_name VARCHAR2(30));
ALTER TABLE frp.secure_config             ADD (key_name VARCHAR2(30));
ALTER TABLE ftd_apps.cpc_info             ADD (key_name VARCHAR2(30));
ALTER TABLE ftd_apps.session_order_master ADD (key_name VARCHAR2(30));
ALTER TABLE scrub.credit_cards            ADD (key_name VARCHAR2(30));

INSERT INTO frp.global_parms (context, name, value, created_on, created_by, updated_on, updated_by) values (
'Ingrian','Current Key','ORCL_CCNUM_2006',sysdate,user,sysdate,user);
---END DEFECT 1105

CREATE INDEX clean.order_reward_posting_n2 ON clean.order_reward_posting(file_post_date) tablespace clean_indx online compute
statistics;




##########################################################################################
###### ITEMS AFTER THIS POINT WILL BE DEPLOYED 1/19
##########################################################################################


alter TABLE ftd_apps.florist_master add
(pos_indicator   char(1), 
 CONSTRAINT florist_master_ck5 CHECK(pos_indicator IN ('P','A'))); 


alter TABLE ftd_apps.florist_weight_calc_history add
(pos_indicator   char(1), 
 pos_weight      number, 
 CONSTRAINT florist_weight_calc_historyck4 CHECK(pos_indicator IN ('P','A'))); 
 



 
 

/*#######################
--####  REMINDER - NEW VENDOR NEEDS SET UP FOR FRP.SECURE_CONFIG - MICKY WANTS REMINDED TO DO THIS AND WILL
--####            SUPPLY INSERTS SIMILAR TO THE FOLLOWING
--INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by) VALUES ('order_processing', 'VENDOR_00135_FTP_LOGON_1', global.encryption.encrypt_it('temp',NULL), 'The logon for the personal creations outbound ftp server', sysdate, 'SYS', sysdate, 'SYS');
--INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by) VALUES ('order_processing', 'VENDOR_00135_FTP_PASSWORD_1', global.encryption.encrypt_it('temp',NULL), 'The password for the personal creations outbound ftp server', sysdate, 'SYS', sysdate, 'SYS');
--INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by) VALUES ('order_processing', 'INBOUND_FTP_LOGON_00135', global.encryption.encrypt_it('temp',NULL), 'The logon for the personal creations inbound ftp server', sysdate, 'SYS', sysdate, 'SYS');
--INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by) VALUES ('order_processing', 'INBOUND_FTP_PASSWORD_00135', global.encryption.encrypt_it('temp',NULL), 'The password for the personal creations inbound ftp server', sysdate, 'SYS', sysdate, 'SYS');
-- ###########END OF MICKEY REMINDER #################
/*#######################


commit;
 
 

--Defect 2057
alter table ftd_apps.session_customer add (bfh_info_code varchar2(20), address_type_code varchar2(1));
alter table ftd_apps.session_customer modify(company_name varchar2(40));
comment on column ftd_apps.session_customer.bfh_info_code is 'A possible room number for a customer.  A corresponding column is FTD_APPS.CUSTOMER.BFH_INFO.';
comment on column ftd_apps.session_customer.address_type_code is 'Examples are B for Business, R for Residential, F for Funeral...etc.  The corresponding column is FTD_APPS.CUSTOMER.ADDRESS_TYPE.';

 
--### DEFECT 2882 ###  (already deployed to qa6)

-- insert into rpt.report table for the ACC31_Personal_Creation_orders report
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	100230,
	'Personal Creation Order Detail Report',
	'The Personal Creation Order Detail report is an ad-hoc report that can be executed for up to 31-day order delivery date range.  The report will show line-item detail of all orders delivered within the time frame specified by the user.',
	'Personal_Creation_Orders',
	'U',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?',
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC31_Personal_Creation_orders.rdf',
	31,
	'Date range cannot exceed 31 days',
	'AcctMktgReportAccess',
	10,
	120,
	'N',
	null,
	null,
	null);

-- insert into rpt.report_parm_ref table for the ACC31_Personal_Creation_orders report
Insert into RPT.REPORT_PARM_REF (
	"REPORT_PARM_ID",
	"REPORT_ID",
	"SORT_ORDER",
	"REQUIRED_INDICATOR",
	"CREATED_ON",
	"CREATED_BY",
	"ALLOW_FUTURE_DATE_IND") 
values (
	100402,
	100230,
	1,
	'Y',
	sysdate,
	'ddesai',
	null);
	
-- insert into rpt.report table for the scheduled version of ACC31_Personal_Creation_orders report
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	1002301,
	'Personal Creation Order Detail (Monthly)',
	'This ad-hoc and automated monthly report provides insight into all orders delivered within a specified time frame.  The report will show all orders for Personal Creation that were estimated to be delivered or will be actually delivered in the given timeframe.',
	'Personal_Creation_Orders_Monthly',
	'S',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?',
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC31_Personal_Creation_orders.rdf',
	null,
	null,
	'AcctMktgReportAccess',
	10,
	120,
	'N',
	'01',
	'M',
	null);	
	
-- insert into rpt.report table for the ACC30_Personal_Creation_refunds report
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	100231,
	'Personal Creation Refund Detail Report',
	'The Personal Creation Refund Detail report is an ad-hoc report that can be executed for up to 31-day transaction date range.  The report will show line-item detail of all refunds within the time frame specified by the user.',
	'Personal_Creation_Refunds',
	'U',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?',
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC30_Personal_Creation_refunds.rdf',
	31,
	'Date range cannot exceed 31 days',
	'AcctMktgReportAccess',
	10,
	120,
	'N',
	null,
	null,
	null);	
	
-- insert into rpt.report_parm_ref table for the ACC30_Personal_Creation_refunds report
Insert into RPT.REPORT_PARM_REF (
	"REPORT_PARM_ID",
	"REPORT_ID",
	"SORT_ORDER",
	"REQUIRED_INDICATOR",
	"CREATED_ON",
	"CREATED_BY",
	"ALLOW_FUTURE_DATE_IND") 
values (
	100402,
	100231,
	1,
	'Y',
	sysdate,
	'ddesai',
	null);	

-- insert into rpt.report table for the scheduled version of the ACC30_Personal_Creation_refunds report
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	1002311,
	'Personal Creation Refunds Report (Monthly)',
	'This ad-hoc and automated monthly report provides insight into all orders for the Personal Creation Vendor that were refunded within a specified time frame.',
	'Personal_Creation_Refunds',
	'S',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?',
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC30_Personal_Creation_refunds.rdf',
	null,
	null,
	'AcctMktgReportAccess',
	10,
	120,
	'N',
	'01',
	'M',
	null);	

--### END DEFECT 2882 ### 







--### DEFECT 1791 ###
insert into pop.partner (partner_id, partner_name) values ('CSPI', 'CSP');
--### END DEFECT 1791 ###


 
----- THE FOLLOWING INSERTS ARE PER MARK CAMPBELL.  HE WILL SUPPLY A DIFFERENT SET FOR PRODUCTION,
----- BUT THIS SERVES AS A REMINDER
----- ############### QA VALUES - PROD FOLLOW THIS ##################
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'EMAIL_HISTORY_URL', 'http://consumer-test.ftdi.com/marketing/custSubscriptionHist.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'LOAD_MEMBER_DATA_URL', 'http://consumer-test.ftdi.com/loadmemberdata/servlet/FloristSearchServlet?', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'MAIN_MENU_URL', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'SCRUB_URL', 'http://consumer-test.ftdi.com/scrub/servlet/ScrubSearchServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'VIEW_BILLING_URL', 'http://consumer-test.ftdi.com/accountingreporting/ViewBilling.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'VIEW_RECON_URL', 'http://consumer-test.ftdi.com/accountingreporting/ViewRecon.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'WEB_OE_URL', 'http://consumer-test.ftdi.com/oe/servlet/IntroductionServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'error_page', 'http://consumer-test.ftdi.com/customerordermanagement/html/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'login_page', 'http://consumer-test.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'sitename', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'sitesslport', '4443', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'PR_EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_PARTNER_REWARD/partner-reward', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('QUEUE_CONFIG', 'error_page', 'http://consumer-test.ftdi.com/queue/error.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('QUEUE_CONFIG', 'login_page', 'http://consumer-test.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SECURITY_MANAGER_CONFIG', 'sitename', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SECURITY_MANAGER_CONFIG', 'sitesslport', '4443', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.active', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.default.port', '15723', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.default.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.jcpenney.port', '15524', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.jcpenney.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.sfmb.port', '15725', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.sfmb.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.gift.port', '15726', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.gift.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.high.port', '15727', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.high.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fusa.port', '15737', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fusa.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fdirect.port', '15735', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fdirect.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.florist.port', '15733', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.florist.server', 'ccas3.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.active', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.facnbr', '37891554', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.default.server', 'http://consumer-test.ftdi.com/aafes/AafesCreditCardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ssl.auth.cert.check', 'OFF', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.timeout', '60000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.max.active.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.min.idle.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.max.idle.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.connect.timeout', '60000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.site.name', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.site.sslport', '4443', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'keyword.server.ip', '10.3.10.52', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'keyword.server.port', '1035', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'security.login.page', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'security.menu.page', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.default', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.west', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdnf', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdp', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdfs', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('RECROF_CONFIG', 'RECROF_REMOTE_LOCATION', 'dirt.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.main.menu', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.error.page', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.login.page', 'http://consumer-test.ftdi.com/secadmin/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.db.datasource', 'PDB', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.live', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.test', 'TEST', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.uat', 'UAT', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.content', 'CONTENT', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendtocnt', '2', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendto1', '4675973@skytel.net', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendto2', '8779771542@skytel.net', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.number.of.servers', '4', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip1', 'illiad.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory1', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip2', 'euryale-dev.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory2', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip3', 'apollo.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory3', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip4', 'stheno-dev.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory4', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip5', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory5', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'error_page', 'http://consumer-test.ftdi.com/pdb/html/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'login_page', 'http://consumer-test.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'SCRUB_LOCATION', 'http://consumer-test.ftdi.com/scrub', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'ORDER_ENTRY_LOCATION', 'http://consumer-test.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'SECURITY_LOCATION', 'http://consumer-test.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'OPS_ADMIN_LOCATION', 'http://consumer-test.ftdi.com/opsadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'login_page', 'http://consumer-test.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'error_page', 'http://consumer-test.ftdi.com/orderprocessingmaint/error.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'MAIN_MENU_URL', 'http:/consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.server.ip', '10.3.10.52', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.server.port', '1037', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.live', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.test', '1', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.uat', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.content', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ESCALATE_REMOTE_LOCATION_1', 'http://hercules-dev.ftdi.com:7778/venusupload/orderupload', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ESCALATE_HTTP_TIMEOUT_1', '60', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_LOCATION_00119', 'apollo.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_DIRECTORY_00119', '/u02/log4j_logs/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_LOCAL_DIRECTORY_00119', '/u02/apollo/orderprocessing/samswine/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00119_REMOTE_DIRECTORY', '/u02/log4j_logs/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00119_REMOTE_LOCATION_1', 'apollo.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'WORKINGDIR', '/u02/apollo/orderprocessing/escalate/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ARCHIVEDIR', '/u02/apollo/orderprocessing/escalate/archive/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENUS_TEST_MODE', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_LOCATION_00096', 'apollo.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_DIRECTORY_00096', '/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_LOCAL_DIRECTORY_00096', '/u02/apollo/orderprocessing/winecom/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00096_REMOTE_DIRECTORY', '/outbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00096_REMOTE_LOCATION_1', 'apollo.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'PROXY_FLORIST', 'true', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'AUTO_RESPOND', 'false', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'BIPASS_CREDIT_CARD_VALIDATION', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'hpPort', '2401', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'local_order_archive_dir', '/u02/archive/orders', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'archive_server_hostname', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'hpPort', '2420', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'hpNewsletterPort', '2301', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'cancel_hp_port', '2301', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'CANCEL_SERVLET', 'http://consumer-test.ftdi.com/emailrequestprocessing/EmailRequestGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'url', 'http://consumer-test.ftdi.com/opsadmin/ManagerDashboardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'logonpage', 'http://consumer-test.ftdi.com/secadmin/security/html/SingleSignOn.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'exitpage', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'scrub_url', 'http://consumer-test.ftdi.com/scrub/servlet/ScrubSearchServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'boxx_url', 'http://consumer-test.ftdi.com/opsadmin/BoxXDashboardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'venus_order_reprocess_url', 'http://consumer-test.ftdi.com/orderprocessingmaint/VenusOrderReprocessAction.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'novator_update_url', 'http://consumer-test.ftdi.com/pdb/showUpdateNovatorProducts.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'novator_vendor_update_url', 'http://consumer-test.ftdi.com/orderprocessingmaint/NovatorVendorUpdateAction.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'pop_url', 'http://aphrodite.ftdi.com:7778/pop/WalmartServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'sitename', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'sitesslport', '4443', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'scheduler_url', 'http://consumer-test.ftdi.com/scheduler/showSchedule.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('NEWSLETTER_CONFIG', 'novator_server', '10.3.10.52', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('NEWSLETTER_CONFIG', 'novator_server_port', '1027', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MESSAGE_GENERATOR_CONFIG', 'TEST_EMAILS_ONLY', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_IPADDRESS_1', 'FTDCOMX03', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_PORT_1', '40000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_IPADDRESS_2', 'FTDCOMX03', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_PORT_2', '40000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'SECURITY_MAIN_MENU_REDIRECT', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'login_page', 'http://consumer-test.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'error_page', 'http://consumer-test.ftdi.com/marketing/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'SECURITY_LOCATION', 'http://consumer-test.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'http_location_novator', 'inventory-test.q9.ftd.com/feed/inventory_listener.epl', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'MemberData_server', 'iris01', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'MemberData_dir', 'hpwrklib', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristSuspendDataFromEFOS_server', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristSuspendDataFromEFOS_dir', '/export/home/unisys', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristRevenueData_server', 'iris01', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristRevenueData_dir', 'hpwrklib', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'COM_EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_COM/customerordermanagement', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'COM_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_COM', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_ADMIN_CONFIG', 'exitPage', 'http://consumer-test.ftdi.com/secadmin/security/Main?adminAction=operations', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('DELIVERY_CONFIRMATION_CONFIG', 'TEST_EMAILS_ONLY', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('DELIVERY_CONFIRMATION_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer-test:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'ordergathererurl', 'http://consumer-test.ftdi.com/OG/OrderGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'logonpage', 'http://consumer-test.ftdi.com/secadmin/security/html/SingleSignOn.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'exitpage', 'http://consumer-test.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'TEST_EMAILS_ONLY', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'SDS_URL', 'http://SHIPTESTWEB.ftdi.com/FTDVSAMSSERVICE/vsams.asmx', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'WTM_URL', 'http://SHIPTESTWEB.ftdi.com/WTM/wtmservice.asmx', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
----- ############### QA VALUES - PROD FOLLOW THIS ##################
Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'EMAIL_HISTORY_URL', 'http://consumer.ftdi.com/marketing/custSubscriptionHist.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'LOAD_MEMBER_DATA_URL', 'http://consumer.ftdi.com/loadmemberdata/servlet/FloristSearchServlet?', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'MAIN_MENU_URL', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'SCRUB_URL', 'http://consumer.ftdi.com/scrub/servlet/ScrubSearchServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'VIEW_BILLING_URL', 'http://consumer.ftdi.com/accountingreporting/ViewBilling.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'VIEW_RECON_URL', 'http://consumer.ftdi.com/accountingreporting/ViewRecon.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'WEB_OE_URL', 'http://consumer.ftdi.com/oe/servlet/IntroductionServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'error_page', 'http://consumer.ftdi.com/customerordermanagement/html/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'login_page', 'http://consumer.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'sitename', 'consumer.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'sitesslport', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'EJB_PROVIDER_URL', 'opmn:ormi://consumer:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CUSTOMER_ORDER_MANAGEMENT', 'PR_EJB_PROVIDER_URL', 'opmn:ormi://consumer:6003:OC4J_PARTNER_REWARD/partner-reward', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('QUEUE_CONFIG', 'error_page', 'http://consumer.ftdi.com/queue/error.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('QUEUE_CONFIG', 'login_page', 'http://consumer.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SECURITY_MANAGER_CONFIG', 'sitename', 'consumer-test.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SECURITY_MANAGER_CONFIG', 'sitesslport', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.active', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.default.port', '15623', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.default.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.jcpenney.port', '15534', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.jcpenney.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.sfmb.port', '15625', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.sfmb.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.gift.port', '15626', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.gift.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.high.port', '15627', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.high.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fusa.port', '15629', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fusa.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fdirect.port', '15628', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.fdirect.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.florist.port', '15630', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.florist.server', 'ccas-int.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.active', 'Y', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.facnbr', '37921529', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'aafes.auth.default.server', 'http://olympia.ftdi.com:7778/aafes/AafesCreditCardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ssl.auth.cert.check', 'OFF', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.auth.timeout', '60000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.max.active.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.min.idle.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.max.idle.connections', '9', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('CCAS_CONFIG', 'ccas.connect.timeout', '60000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.site.name', 'consumer.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.site.sslport', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'keyword.server.ip', '10.5.16.51', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'keyword.server.port', '1035', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'security.login.page', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'security.menu.page', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.default', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.west', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdnf', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdp', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('WEBOE_CONFIG', 'application.url.ftdfs', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('RECROF_CONFIG', 'RECROF_REMOTE_LOCATION', 'iris01.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.main.menu', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.error.page', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'security.login.page', 'http://consumer.ftdi.com/secadmin/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.db.datasource', 'PDB', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.live', 'LIVE', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.test', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.uat', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'novator.content', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendtocnt', '2', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendto1', '4675973@skytel.net', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'application.mail.sendto2', 'jtelford@ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.number.of.servers', '4', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip1', 'cronus.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory1', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip2', 'midas.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory2', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip3', 'europa.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory3', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip4', 'eros.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory4', '/u01/app/oracle/product/10g/Apache/Apache/htdocs/product_images', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.ftp.server.ip5', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'ftd.products.server.image.directory5', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'error_page', 'http://consumer.ftdi.com/pdb/html/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PDB_CONFIG', 'login_page', 'http://consumer.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'SCRUB_LOCATION', 'http://consumer.ftdi.com/scrub', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'ORDER_ENTRY_LOCATION', 'http://consumer.ftdi.com/oe', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'SECURITY_LOCATION', 'http://consumer.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCRUB_CONFIG', 'OPS_ADMIN_LOCATION', 'http://consumer.ftdi.com/opsadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'login_page', 'http://consumer.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'error_page', 'http://consumer.ftdi.com/orderprocessingmaint/error.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'MAIN_MENU_URL', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.server.ip', '10.5.16.51', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.server.port', '1035', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.live', '1', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.test', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.uat', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_MAINT_CONFIG', 'novator.content', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ESCALATE_REMOTE_LOCATION_1', 'http://olympia.ftdi.com:7778/venusupload/orderupload', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ESCALATE_HTTP_TIMEOUT_1', '60', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_LOCATION_00119', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_DIRECTORY_00119', 'data/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_LOCAL_DIRECTORY_00119', '/u02/apollo/orderprocessing/winecom/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00119_REMOTE_DIRECTORY', 'data/outbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00119_REMOTE_LOCATION_1', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'WORKINGDIR', '/u02/apollo/orderprocessing/escalate/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'ARCHIVEDIR', '/u02/apollo/orderprocessing/escalate/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENUS_TEST_MODE', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_LOCATION_00096', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_DIRECTORY_00096', 'data/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_LOCAL_DIRECTORY_00096', '/u02/apollo/orderprocessing/samswine/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00096_REMOTE_DIRECTORY', 'data/outbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00096_REMOTE_LOCATION_1', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'PROXY_FLORIST', 'FALSE', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'AUTO_RESPOND', 'FALSE', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'BIPASS_CREDIT_CARD_VALIDATION', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'hpPort', 'priam-prod.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'local_order_archive_dir', '2500', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_GATHERER_CONFIG', 'archive_server_hostname', 'consumer.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'hpPort', '2521', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'hpNewsletterPort', '2300', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'cancel_hp_port', '2302', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_DISPATCHER_CONFIG', 'CANCEL_SERVLET', 'http://localhost:7778/emailrequestprocessing/EmailRequestGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'url', 'http://consumer.ftdi.com/opsadmin/ManagerDashboardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'logonpage', 'http://consumer.ftdi.com/secadmin/security/html/SingleSignOn.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'exitpage', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'scrub_url', 'http://consumer.ftdi.com/scrub/servlet/ScrubSearchServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'boxx_url', 'http://consumer.ftdi.com/opsadmin/BoxXDashboardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'venus_order_reprocess_url', 'http://consumer.ftdi.com/orderprocessingmaint/VenusOrderReprocessAction.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'novator_update_url', 'http://consumer.ftdi.com/pdb/showUpdateNovatorProducts.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'novator_vendor_update_url', 'http://consumer.ftdi.com/orderprocessingmaint/NovatorVendorUpdateAction.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'pop_url', 'http://consumer.ftdi.com/pop/WalmartServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'sitename', 'consumer.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'sitesslport', '', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('OPS_ADMIN_CONFIG', 'scheduler_url', 'http://consumer.ftdi.com/scheduler/showSchedule.do', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('NEWSLETTER_CONFIG', 'novator_server', '10.5.16.51', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('NEWSLETTER_CONFIG', 'novator_server_port', '1027', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MESSAGE_GENERATOR_CONFIG', 'TEST_EMAILS_ONLY', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_IPADDRESS_1', 'FTDCOMX01', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_PORT_1', '40000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_IPADDRESS_2', 'FTDCOMX02', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MERCURY_INTERFACE_CONFIG', 'BOX_X_PORT_2', '40000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'SECURITY_MAIN_MENU_REDIRECT', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'login_page', 'http://consumer.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('MARKETING_CONFIG', 'error_page', 'http://consumer.ftdi.com/marketing/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'SECURITY_LOCATION', 'http://consumer.ftdi.com/secadmin/security', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'http_location_novator', 'http://inventory.chi.ftd.com/feed/inventory_listener.epl', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'MemberData_server', 'iris01', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'MemberData_dir', 'hpwrklib', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristSuspendDataFromEFOS_server', 'hawk', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristSuspendDataFromEFOS_dir', '/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristRevenueData_server', 'iris01', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('LOAD_MEMBER_DATA_CONFIG', 'FloristRevenueData_dir', 'hpwrklib', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'COM_EJB_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_COM/customerordermanagement', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_REQUEST_PROCESSING_CONFIG', 'COM_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_COM', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('EMAIL_ADMIN_CONFIG', 'exitPage', 'http://consumer.ftdi.com/secadmin/security/Main?adminAction=operations', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('DELIVERY_CONFIRMATION_CONFIG', 'TEST_EMAILS_ONLY', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('DELIVERY_CONFIRMATION_CONFIG', 'EJB_PROVIDER_URL', 'opmn:ormi://apollo-rmi-int.ftdi.com:6003:OC4J_ORDER_PROCESSING/orderprocessing', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'ordergathererurl', 'http://consumer.ftdi.com/OG/OrderGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'logonpage', 'http://consumer.ftdi.com/secadmin/security/html/SingleSignOn.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BULK_LOAD_CONFIG', 'exitpage', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'TEST_EMAILS_ONLY', 'N', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'SDS_URL', 'http://ShipApp.ftdi.com/FTDVSAMSSERVICE/vsams.asmx', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SHIP_PROCESSING_CONFIG', 'WTM_URL', 'http://ShipApp.ftdi.com/WTM/wtmservice.asmx', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'error_page', 'http://consumer.ftdi.com/accountingreporting/html/systemerror.html', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'login_page', 'http://consumer.ftdi.com/secadmin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'recon_email_addresses', 'chu@ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'recon_mail_server', 'mailserver.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gc_feed_email_addresses', 'chu@ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gc_feed_mail_server', 'mailserver.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'eodFtpServer', 'iris01.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'eodFtpLocation', '//usr/dotcom', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'AAFES_CC_SERVLET_URL', 'http://olympia.ftdi.com:7778/aafes/AafesCreditCardServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'AAFES_facility', '37921529', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'NOVATOR_GIFT_CERT_URL', 'http://inventory.chi.ftd.com/350/feed/gift_cert_listener.epl', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'mercReconFtpServer', 'iris01.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'mercReconFtpLocation', 'HPWRKLIB', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'venusReconFtpServer', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'venusReconFtpLocation', 'data/inbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'ccSettlementFtpServer', 'iris01.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'ccSettlementFtpLocation', '//usr/dotcom/outbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcProgramGenericFtpServer', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcProgramGenericFtpLocationGC', 'data/outbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcProgramGenericFtpLocationFF', 'data/inbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcProgramGenericFtpLocationRD', 'data/outbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcIntuitLocalLocationInbound', 'data/inbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcIntuitLocalLocationOutbound', 'data/outbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcIntuitFtpServer', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcIntuitFtpLocationInbound', '/u02/apollo/accounting/giftcert/feed/intuit/inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcIntuitFtpLocationOutbound', '/u02/apollo/accounting/giftcert/feed/intuit/outbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcVanillaLocalLocationInbound', '/u02/apollo/accounting/giftcert/feed/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcVanillaLocalLocationOutbound', '/u02/apollo/accounting/giftcert/feed/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcVanillaFtpServer', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'gcVanillaFtpLocationInbound', 'data/inbound', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'VENDOR_EOM_FTP_SERVER', 'hawk.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'VENDOR_EOM_FTP_LOCATION', '.', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'VENDOR_EOM_LOCAL_LOCATION', '/u02/apollo/accounting/vendor_eom/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'VENDOR_EOM_ARCHIVE_LOCATION', '/u02/apollo/accounting/vendor_eom/archive/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'securityPrivateKeyPath', '/home/oracle/.ssh/ftdcom-secret.asc', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'securityPublicKeyPathIseries', '/home/oracle/.ssh/FTDIRIS.asc', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'BLOB_REPORT_KEY', 'reportBlob', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'CLOB_REPORT_KEY', 'reportClob', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ACCOUNTING_CONFIG', 'LOGIN_REPORT_KEY', 'reportLogin', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AAFES_CONFIG', 'aafes.auth.default.server', 'https://thor.aafes.com/val/venddpp.asp', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'security.main.menu', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'security.error.page', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'security.login.page', 'http://consumer.ftdi.com/secadmin/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'gatherer_url', 'http://consumer.ftdi.com/OG/OrderGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'merchant_identifier', 'M_FTDCOM_11783', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('AMAZON_CONFIG', 'amazon_url', 'https://merchant-api.amazon.com/exec/merchantdata', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('SCHEDULER_CONFIG', 'MAIN_MENU_URL', 'http://consumer.ftdi.com/secadmin/security/Main', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PAGER_CONFIG', 'pop3_host', 'mail.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PAGER_CONFIG', 'pop3_user', 'system_message', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('PAGER_CONFIG', 'pop3_password', 'sys080406', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_CONFIG', 'ORDER_GATHERER_ORDER_URL', 'http://ecommerce.ftdi.com/OG/OrderGatherer', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_CONFIG', 'ORDER_GATHERER_CANCEL_URL', 'http://ecommerce.ftdi.com/OG/OrderCancelServlet', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'TARGET_URL', 'https://ebob.walmart.com/cgi-bin/vendorIf', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'REFUND_TO_EMAIL_ADDRESS', 'ftdrefunds@ftdi.com, ftdadjustments@walmart.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'REFUND_FROM_EMAIL_ADDRESS', 'ftdcombatchsupport@ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'SMTP_HOST', 'mail4.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'FTD_OPERATIONS_NAME', 'FTD.COM Support', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'FTD_OPERATIONS_EMAIL', 'bacom@ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'FTD_OPERATIONS_PHONE', '6305449329', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('POP_WALMART_CONFIG', 'FTD_OPERATIONS_PHONEEXT', '000', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('BASE_CONFIG', 'BASE_URL', 'http://consumer.ftdi.com', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_LOCATION_00135', '63.149.45.197', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_REMOTE_DIRECTORY_00135', 'inbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'INBOUND_LOCAL_DIRECTORY_00135', '/u02/apollo/orderprocessing/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00135_REMOTE_DIRECTORY', 'outbound/', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 Insert into frp.global_parms values ('ORDER_PROCESSING_CONFIG', 'VENDOR_00135_REMOTE_LOCATION_1', '63.149.45.197', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 

 




----- END MARK CAMPBELL INSERTS


--- MORE GLOBAL PARMS CONFIG - DIFFERENT VALUES DEPENDING ON THE DATABASE - 
set define on
insert into frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on)
values ('GLOBAL_CONFIG','sitename','&global_config_sitename','chu',sysdate,'chu',sysdate);
insert into frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on)
values ('GLOBAL_CONFIG','sitesslport','&global_config_sitesslport','chu',sysdate,'chu',sysdate);
insert into frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on)
values ('MARKETING_CONFIG','appcontext','&marketing_config_appcontext','chu',sysdate,'chu',sysdate);



---DEFECT 2662
Insert into RPT.REPORT_PARM
   (REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values
   (100705, 'State', 'S', 'p_state', SYSDATE, 'chu', SYSDATE, 'chu', null);

Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100705, 100705, 1, 'Y', SYSDATE, 'chu', null);

Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (1007051, SYSDATE, 'chu', SYSDATE, 'chu', null, 'GET_REPORT_STATE_LIST', NULL);

Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100705, 1007051, 1, SYSDATE, 'chu');
---END DEFECT 2662
 



insert into events.events (event_name, context_name, description, active) 
values ('CSP-REFUND-EVENT', 'PARTNER_ORDER_PROCESSING', 'Run Csp Refund Process', 'Y');


insert into events.events (event_name, context_name, description, active) 
values ('POST-FILE-KOREAN-AIR', 'PARTNER-REWARD', 'Partner Rewards Posting File Korean Air', 'Y');


--DEFECT 1400
insert into CLEAN.REFUND_RESPONSIBILITY 
(REFUND_RESPONSIBILITY.RESPONSIBLE_ID,REFUND_RESPONSIBILITY.RESPONSIBLE_PARTY,REFUND_RESPONSIBILITY.STATUS) 
VALUES('DHL','DHL','Active');

insert into CLEAN.REFUND_RESPONSIBILITY 
(REFUND_RESPONSIBILITY.RESPONSIBLE_ID,REFUND_RESPONSIBILITY.RESPONSIBLE_PARTY,REFUND_RESPONSIBILITY.STATUS) 
VALUES('UPS','UPS','Active');
--END DEFECT 1400

insert into frp.global_parms (context, name, value, created_by, created_on, updated_by, updated_on)
values('PARTNER_ORDER_PROCESSING', 'EJB_PROVIDER_URL',
'&ejb_provider_url', 'SYS', SYSDATE, 'SYS', SYSDATE);



--Defect 1100
ALTER TABLE CLEAN.REFUND ADD (POP_REFUND_flag VARCHAR2(1) DEFAULT 'N' NOT NULL
    CONSTRAINT REFUND_CK1 CHECK(pop_refund_flag in ('Y', 'N')));
ALTER TABLE POP.ORDER_GATHERER_MESSAGES ADD (CC_NUMBER VARCHAR2(4000), KEY_NAME VARCHAR2(30));

GRANT SELECT ON CLEAN.ORDERS TO POP;
GRANT SELECT ON CLEAN.ORDER_DETAILS TO POP;
GRANT SELECT ON CLEAN.REFUND TO POP;
GRANT UPDATE ON CLEAN.REFUND TO POP;
GRANT EXECUTE ON GLOBAL.ENCRYPTION TO POP;
--end defect 1100

DROP INDEX clean.payments_n7;
CREATE INDEX clean.payments_n3 ON clean.payments (updated_on) tablespace clean_indx online compute statistics;
CREATE INDEX clean.refund_n5 ON clean.refund(updated_on) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;
DROP INDEX clean.order_details_n11;
CREATE INDEX clean.order_details_n11 ON clean.order_details(updated_on) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;
CREATE INDEX clean.order_add_ons_n3 ON clean.order_add_ons(updated_on) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;
CREATE INDEX clean.customer_phones_n3 ON clean.customer_phones(updated_on) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;
CREATE INDEX clean.email_n4 ON clean.email(updated_on) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;

--defect 3117
create table frp.application_counter(application_name varchar2(100) not null, timestamp_datetime date not null);
grant select, insert on frp.application_counter to ftd_apps;
--end defect 3117

--defect 3028
-- insert into frp.global_parms entries for the WINE VENDORS
INSERT INTO FRP.GLOBAL_PARMS (
	CONTEXT,
	NAME,
	VALUE,
	CREATED_BY,
	CREATED_ON,
	UPDATED_BY,
	UPDATED_ON)
VALUES (
	'WINE VENDORS',
	'NEW WINE VENDOR',
	'00136',
	'DDESAI',
	sysdate,
	'DDESAI',
	sysdate);

INSERT INTO FRP.GLOBAL_PARMS (
	CONTEXT,
	NAME,
	VALUE,
	CREATED_BY,
	CREATED_ON,
	UPDATED_BY,
	UPDATED_ON)
VALUES (
	'WINE VENDORS',
	'SAM''S WINE AND SPIRITS',
	'00119',
	'DDESAI',
	sysdate,
	'DDESAI',
	sysdate);

INSERT INTO FRP.GLOBAL_PARMS (
	CONTEXT,
	NAME,
	VALUE,
	CREATED_BY,
	CREATED_ON,
	UPDATED_BY,
	UPDATED_ON)
VALUES (
	'WINE VENDORS',
	'WINE.COM',
	'00096',
	'DDESAI',
	sysdate,
	'DDESAI',
	sysdate);
	
-- update RPT.REPORT set the repors to "Inactive"
UPDATE RPT.REPORT set status = 'Inactive' WHERE REPORT_ID IN (100225, 100226);

-- include additional parameter "Wine Vendor" to "Sams Wine and Spirits"
Insert into RPT.REPORT_PARM
   (REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values
   (100227, 'Wine Vendor', 'S', 'p_vendor', SYSDATE, 'ddesai', SYSDATE, 'ddesai', NULL);
   
-- Inserting into rpt.report_parm_ref to include the "Wine Vendor" parameter in "Sams Wine and Spirits" reports
Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100227, 100227, 1, 'Y', SYSDATE, 'ddesai', NULL);
   
Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100227, 100228, 1, 'Y', SYSDATE, 'ddesai', NULL);   
 
-- Inserting into rpt.report_parm_value to include the new "Wine Vendor" parameter to "Sams Wine and Spirits" reports
Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (1002271, SYSDATE, 'ddesai', SYSDATE, 'ddesai', NULL, 'GET_WINE_VENDORS', NULL);
   
Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100227, 1002271, 1, SYSDATE, 'ddesai'); 
   
-- Update the rpt.report to change the name of "Sams Wine and Spirits" reports to "Wine" reports    
Update RPT.REPORT 
	set name = 'Wine Orders Taken',
		 description = 'The Wine Orders Taken Detail report is an ad-hoc report that can be executed for up a 31-day order taken range.  The report will show line-item detail of all orders taken within the time frame specified by the user.',
		 report_file_prefix = 'Wine_Orders_Taken',
		 updated_on = sysdate,
		 updated_by = 'ddesai',
		 oracle_rdf = 'ACC27_Wine_orders_taken.rdf'
 where report_id = 100227;
 
Update RPT.REPORT 
	set name = 'Wine Refunds',
		 description = 'The Wine Refunds Detail report is an ad-hoc report that can be executed for up a 31-day transaction date range.  The report will show line-item detail of all refunds within the time frame specified by the user.',
		 report_file_prefix = 'Wine_Refunds',
		 updated_on = sysdate,
		 updated_by = 'ddesai',
		 oracle_rdf = 'ACC28_Wine_refunds.rdf'
 where report_id = 100228; 
-- end defect 3028


-- defect 3145
ALTER TABLE clean.vendor_jde_audit_log 
 ADD adj_created_datetime  date; 
-- end 3145

UPDATE frp.global_parms
   SET name = 'BEVERAGE SOLUTIONS',
       value = '00137'
 WHERE CONTEXT = 'WINE VENDORS'
   AND name = 'NEW WINE VENDOR';

-- defect 2746 2747

UPDATE ftd_apps.program_reward
set calculation_basis = 'M'
where program_name like 'DISC%';

---- END FIX 2.3.0 RELEASE SCRIPT -------------------
