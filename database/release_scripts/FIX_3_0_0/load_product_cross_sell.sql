
delete from joe.product_cross_sell;
-----
--- Load joe.product_cross_sell
-----
set serveroutput on size 99999

declare
   cursor c1 is
      select * from joe.cross_sell_stage_temp;
   v_prod_or_upsell varchar2(1);
   total_cntr number := 0;
   v_err_cntr number := 0;
   ins_err_cntr number := 0;
   ins_cnt number := 0;
   v_keep_going number;

   -- These are the fields we'll load then use for the inserts
   ld_product_id                  joe.product_cross_sell.product_id%type;
   ld_cross_sell_1_product_id     joe.product_cross_sell.cross_sell_1_product_id%type;
   ld_cross_sell_2_product_id     joe.product_cross_sell.cross_sell_2_product_id%type;
   ld_cross_sell_3_product_id     joe.product_cross_sell.cross_sell_3_product_id%type;
   ld_upsell_master_id            joe.product_cross_sell.upsell_master_id%type;
   ld_cross_sell_1_upsell_mast_id joe.product_cross_sell.cross_sell_1_upsell_master_id%type;
   ld_cross_sell_2_upsell_mast_id joe.product_cross_sell.cross_sell_2_upsell_master_id%type;
   ld_cross_sell_3_upsell_mast_id joe.product_cross_sell.cross_sell_3_upsell_master_id%type;

   v_looked_up_product            ftd_apps.product_master.product_id%type;

   -- Return 'P' if its a product, 'U' if upsell, 'N' if neither or an error
   function is_sku_prod_or_upsell (in_sku varchar2) return varchar2 is
      v_lookup varchar2(100);
   begin
      begin
         select product_id into v_looked_up_product
         from ftd_apps.product_master
         where novator_id = in_sku;

         return 'P';

      exception
         when no_data_found then
            begin
               -- Check to see if it's an upsell
               select upsell_master_id into v_lookup
               from ftd_apps.upsell_master
               where upsell_master_id = in_sku;
               return 'U';
            exception
               when others then
                  return 'N';
            end;
         when others then
            return 'N';
      end;
   end is_sku_prod_or_upsell;


   procedure load_next_cs_product(in_product varchar2) is
   begin
      if ld_cross_sell_1_product_id is null then
         ld_cross_sell_1_product_id := in_product;
      elsif ld_cross_sell_2_product_id is null then
         ld_cross_sell_2_product_id := in_product;
      else
         ld_cross_sell_3_product_id := in_product;
      end if;
   end load_next_cs_product;


   procedure load_next_cs_upsell(in_sku varchar2) is
   begin
      if ld_cross_sell_1_upsell_mast_id is null then
         ld_cross_sell_1_upsell_mast_id := in_sku;
      elsif ld_cross_sell_2_upsell_mast_id is null then
         ld_cross_sell_2_upsell_mast_id := in_sku;
      else
         ld_cross_sell_3_upsell_mast_id := in_sku;
      end if;
   end load_next_cs_upsell;

begin
   for c1_rec in c1 LOOP
      -- Initialize the load fields
      ld_product_id                  := NULL;
      ld_cross_sell_1_product_id     := NULL;
      ld_cross_sell_2_product_id     := NULL;
      ld_cross_sell_3_product_id     := NULL;
      ld_upsell_master_id            := NULL;
      ld_cross_sell_1_upsell_mast_id := NULL;
      ld_cross_sell_2_upsell_mast_id := NULL;
      ld_cross_sell_3_upsell_mast_id := NULL;

      total_cntr := total_cntr + 1;
      v_keep_going := 1;

      --
      -- Load the SKU
      --
      v_prod_or_upsell := is_sku_prod_or_upsell(c1_rec.sku_id);
   --   dbms_output.put_line('SKU ID ' || c1_rec.sku_id || ' is a/an ' ||
   --         v_prod_or_upsell);

      if v_prod_or_upsell = 'P' then
         ld_product_id := v_looked_up_product;
      elsif v_prod_or_upsell = 'U' then
         ld_upsell_master_id := c1_rec.sku_id;
      else
         v_keep_going := 0;
         v_err_cntr := v_err_cntr + 1;
         dbms_output.put_line('SKU=' || c1_rec.sku_id || ': Neither a product or upsell - bypass');
      end if;


      --
      -- Load the Cross Sell SKU 1
      --
      if v_keep_going = 1 then
         v_prod_or_upsell := is_sku_prod_or_upsell(c1_rec.cross_sell_sku_1);

         if v_prod_or_upsell = 'P' then
            load_next_cs_product(v_looked_up_product);
         elsif v_prod_or_upsell = 'U' then
            load_next_cs_upsell(c1_rec.cross_sell_sku_1);
         else
            v_keep_going := 0;
            v_err_cntr := v_err_cntr + 1;
            dbms_output.put_line('SKU=' || c1_rec.sku_id || ',   Cross_Sell_SKU_1=' ||
                       c1_rec.cross_sell_sku_1 || ': Neither a product or upsell - bypass');
         end if;
      end if;

      --
      -- Load the Cross Sell SKU 2
      --
      if v_keep_going = 1 and
         c1_rec.cross_sell_sku_2 is not null then
         v_prod_or_upsell := is_sku_prod_or_upsell(c1_rec.cross_sell_sku_2);

         if v_prod_or_upsell = 'P' then
            load_next_cs_product(v_looked_up_product);
         elsif v_prod_or_upsell = 'U' then
            load_next_cs_upsell(c1_rec.cross_sell_sku_2);
         else
            v_keep_going := 0;
            v_err_cntr := v_err_cntr + 1;
            dbms_output.put_line('SKU=' || c1_rec.sku_id || ',   Cross_Sell_SKU_2=' ||
                          c1_rec.cross_sell_sku_2 || ': Neither a product or upsell - bypass');
         end if;
      end if;


      --
      -- Load the Cross Sell SKU 3
      --
      if v_keep_going = 1 and
         c1_rec.cross_sell_sku_3 is not null then
         v_prod_or_upsell := is_sku_prod_or_upsell(c1_rec.cross_sell_sku_3);

         if v_prod_or_upsell = 'P' then
            load_next_cs_product(v_looked_up_product);
         elsif v_prod_or_upsell = 'U' then
            load_next_cs_upsell(c1_rec.cross_sell_sku_3);
         else
            v_keep_going := 0;
            v_err_cntr := v_err_cntr + 1;
            dbms_output.put_line('SKU=' || c1_rec.sku_id || ',   Cross_Sell_SKU_3=' ||
                          c1_rec.cross_sell_sku_3 || ': Neither a product or upsell - bypass');
         end if;
      end if;

      if v_keep_going = 1 then
         -- Looks like we have everything - go ahead and insert!!
         begin
            insert into joe.product_cross_sell
               (PRODUCT_CROSS_SELL_ID,
                PRODUCT_ID,
                CROSS_SELL_1_PRODUCT_ID,
                CROSS_SELL_2_PRODUCT_ID,
                CROSS_SELL_3_PRODUCT_ID,
                UPSELL_MASTER_ID,
                CROSS_SELL_1_UPSELL_MASTER_ID,
                CROSS_SELL_2_UPSELL_MASTER_ID,
                CROSS_SELL_3_UPSELL_MASTER_ID,
                CREATED_BY,
                CREATED_ON,
                UPDATED_BY,
                UPDATED_ON)
            values
               (joe.product_cross_sell_sq.nextval,
                ld_product_id,
                ld_cross_sell_1_product_id,
                ld_cross_sell_2_product_id,
                ld_cross_sell_3_product_id,
                ld_upsell_master_id,
                ld_cross_sell_1_upsell_mast_id,
                ld_cross_sell_2_upsell_mast_id,
                ld_cross_sell_3_upsell_mast_id,
                'FIX 3.0 Initial Load - Defect 1787',
                sysdate,
                'FIX 3.0 Initial Load - Defect 1787',
                sysdate);
            ins_cnt := ins_cnt + 1;

         exception
            when others then
               dbms_output.put_line('Unexpected Error on insert for sku ' || c1_rec.sku_id
                            || '   Msg=' || sqlerrm);
               ins_err_cntr := ins_err_cntr + 1;
         end;
      end if;
   end loop;
   dbms_output.put_line('Total cntr = ' || total_cntr);
   dbms_output.put_line('Edit Errors=' || v_err_cntr);
   dbms_output.put_line('Inserts=' || ins_cnt);
   dbms_output.put_line('Insert Errors=' || ins_err_cntr);
end;
/
