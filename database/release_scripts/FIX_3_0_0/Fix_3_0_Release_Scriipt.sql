---- FIX 3.0.0 RELEASE SCRIPT ------------------



--- NEW TABLESPACES and USERS ------------------------
-- NOTE  These will be different for Hera and Zeus raw devices!!
---------------------------------------------
create tablespace joe_data datafile '/u04/oradata/qa3/joe_data_01.dbf' size 100m
extent management local autoallocate segment space management auto;

create tablespace joe_indx datafile '/u04/oradata/qa3/joe_indx_01.dbf' size 75m
extent management local autoallocate segment space management auto;

create user joe identified by values 'disabled' account lock default tablespace joe_data
temporary tablespace temp quota unlimited on joe_data quota unlimited on joe_indx;

grant create session to joe;


create tablespace pas_data datafile '/u04/oradata/qa3/pas_data_01.dbf' size 1024m
extent management local autoallocate segment space management auto;

create tablespace pas_indx datafile '/u04/oradata/qa3/pas_indx_01.dbf' size 500m
extent management local autoallocate segment space management auto;

create user pas identified by values 'disabled' account lock default tablespace pas_data
temporary tablespace temp quota unlimited on pas_data quota unlimited on pas_indx;

grant create session to pas;


---------------------------------------------


ALTER TABLE FTD_APPS.OCCASION ADD (
        DEFAULT_OCCASION_INDICATOR      VARCHAR2(1)         DEFAULT NULL   NULL, 
    CONSTRAINT OCCASION_CK3      CHECK(DEFAULT_OCCASION_INDICATOR IN('Y',NULL))); 

COMMENT ON COLUMN FTD_APPS.OCCASION.DEFAULT_OCCASION_INDICATOR IS 'Denotes which occasion is the current default one - only one Y in the table';
     
CREATE UNIQUE INDEX FTD_APPS.OCCASION_UK1 ON FTD_APPS.OCCASION  (DEFAULT_OCCASION_INDICATOR)  
     TABLESPACE FTD_APPS_INDX;




alter table ftd_apps.product_master add (popularity_order_cnt number(11) DEFAULT 0 NOT NULL,
                         custom_flag    VARCHAR2(1) DEFAULT 'N' NOT NULL,
                         keywords_txt   varchar2(4000),
                         CONSTRAINT PRODUCT_MASTER_CK7      CHECK(CUSTOM_FLAG IN('N','Y')));

alter table ftd_apps.product_master$ add (popularity_order_cnt number(11),
                                          custom_flag  varchar2(1),
                                          keywords_txt   varchar2(4000));

COMMENT ON COLUMN ftd_apps.product_master.popularity_order_cnt is 'Stores count of orders over a time period';
COMMENT ON COLUMN ftd_apps.product_master.custom_flag is 'Denotes whether a product is custom.  Should have only one Y in the table, enforced by the application.';



alter table ftd_apps.source_master  add (last_used_on_order_date    date  default sysdate);
COMMENT ON COLUMN ftd_apps.source_master.last_used_on_order_date is 'Stores last time the corresponding source was used on an order';

alter table ftd_apps.source_master$ add (last_used_on_order_date    date);



grant references on ftd_apps.source_master to joe;
grant references on ftd_apps.product_master to joe;

CREATE TABLE JOE.IOTW (
          iotw_id                      number(11)       NOT NULL,
          source_code                  varchar2(10)     NOT NULL,
          product_id                   varchar2(10)     NOT NULL,
          start_date                   date             NOT NULL,
          end_date                     date,
          product_page_messaging_txt   varchar2(255),
          calendar_messaging_txt       varchar2(255),
          iotw_source_code             varchar2(10)     NOT NULL,
          special_offer_flag           VARCHAR2(1) DEFAULT 'N' NOT NULL,
          wording_color_txt            varchar2(4000),
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT IOTW_PK PRIMARY KEY(iotw_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT IOTW_CK1  CHECK(special_offer_flag IN('Y','N')), 
    CONSTRAINT IOTW_FK1 FOREIGN KEY(source_code) REFERENCES ftd_apps.source_master(source_Code),
    CONSTRAINT IOTW_FK2 FOREIGN KEY(product_id)  REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT IOTW_FK3 FOREIGN KEY(iotw_source_code) REFERENCES ftd_apps.source_master(source_Code))
 TABLESPACE joe_DATA;

CREATE INDEX JOE.IOTW_N1 ON JOE.IOTW(SOURCE_CODE) TABLESPACE JOE_INDX;
CREATE INDEX JOE.IOTW_N2 ON JOE.IOTW(product_id) TABLESPACE JOE_INDX;
CREATE INDEX JOE.IOTW_N3 ON JOE.IOTW(iotw_SOURCE_CODE) TABLESPACE JOE_INDX;

COMMENT ON TABLE JOE.IOTW IS 'Contains product/source code special promotions';
COMMENT ON COLUMN JOE.IOTW.iotw_id is 'Primary key';
COMMENT ON COLUMN JOE.IOTW.source_code is 'Source Code';
COMMENT ON COLUMN JOE.IOTW.product_id is 'Product Id';
COMMENT ON COLUMN JOE.IOTW.start_date is 'Discount start date';
COMMENT ON COLUMN JOE.IOTW.end_date is 'Discount end date';
COMMENT ON COLUMN JOE.IOTW.product_page_messaging_txt is 'Product page messaging';
COMMENT ON COLUMN JOE.IOTW.calendar_messaging_txt is 'Calendar page messaging';
COMMENT ON COLUMN JOE.IOTW.iotw_source_code is 'Item of the week source code';
COMMENT ON COLUMN JOE.IOTW.special_offer_flag is 'Special offer switch';
COMMENT ON COLUMN JOE.IOTW.wording_color_txt is 'Determines the color of the wording on the Novator product page';
COMMENT ON COLUMN JOE.IOTW.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.IOTW.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.IOTW.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.IOTW.updated_by is 'Date record was last updated';


CREATE TABLE JOE.IOTW$ (
          iotw_id                      number(11)       NOT NULL,
          source_code                  varchar2(10)     NOT NULL,
          product_id                   varchar2(10)     NOT NULL,
          start_date                   date             NOT NULL,
          end_date                     date,
          product_page_messaging_txt   varchar2(255),
          calendar_messaging_txt       varchar2(255),
          iotw_source_code             varchar2(10)     NOT NULL,
          special_offer_flag           VARCHAR2(1) DEFAULT 'N' NOT NULL,
          wording_color_txt            varchar2(4000),
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
          OPERATION$                   VARCHAR2(7)      not null,
          TIMESTAMP$                   DATE             not null
        )
 TABLESPACE joe_DATA;


grant select on joe.iotw to ftd_apps;

CREATE SEQUENCE JOE.IOTW_SQ 
    START WITH 1  INCREMENT BY 1  MAXVALUE 99999999999 NOCYCLE NOCACHE;



CREATE TABLE JOE.IOTW_DELIVERY_DISCOUNT_DATES (
          iotw_id                      number(11)       NOT NULL,
          discount_date                date             NOT NULL,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT IOTW_DELIVERY_DISC_DATES_PK PRIMARY KEY(iotw_id, discount_date) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT IOTW_DELIVERY_DISC_DATES_FK1 FOREIGN KEY(iotw_id) REFERENCES joe.iotw(iotw_id))
 TABLESPACE joe_DATA;


COMMENT ON TABLE JOE.IOTW_DELIVERY_DISCOUNT_DATES IS 'Contains product/source code special promotions';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.iotw_id is 'Primary key';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.discount_date is 'Date the discount applies';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.IOTW_DELIVERY_DISCOUNT_DATES.updated_by is 'Date record was last updated';



CREATE TABLE JOE.PRODUCT_CROSS_SELL (
          product_id                   varchar2(10)     NOT NULL,
          cross_sell_1_product_id      varchar2(10)     NOT NULL,
          cross_sell_2_product_id      varchar2(10) ,
          cross_sell_3_product_id      varchar2(10) ,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT PRODUCT_CROSS_SELL_PK PRIMARY KEY(product_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT CROSS_SELL_FK1 FOREIGN KEY(product_id)  REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT CROSS_SELL_FK2 FOREIGN KEY(cross_sell_1_product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT CROSS_SELL_FK3 FOREIGN KEY(cross_sell_3_product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT CROSS_SELL_FK4 FOREIGN KEY(cross_sell_2_product_id) REFERENCES ftd_apps.product_master(product_id) )
 TABLESPACE joe_DATA;


COMMENT ON TABLE JOE.product_cross_sell IS 'Contains product/source code special promotions';
COMMENT ON COLUMN JOE.product_cross_sell.product_id is 'Product Id';
COMMENT ON COLUMN JOE.product_cross_sell.cross_sell_product_id is 'Cross Sell Product Id';
COMMENT ON COLUMN JOE.product_cross_sell.cross_sell_1_product_id is 'Cross Sell Product Id';
COMMENT ON COLUMN JOE.product_cross_sell.cross_sell_2_product_id is 'Cross Sell Product Id';
COMMENT ON COLUMN JOE.product_cross_sell.cross_sell_3_product_id is 'Cross Sell Product Id';
COMMENT ON COLUMN JOE.product_cross_sell.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.product_cross_sell.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.product_cross_sell.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.product_cross_sell.updated_by is 'Date record was last updated';

CREATE TABLE JOE.PRODUCT_CROSS_SELL$ (
          product_id                   varchar2(10)     NOT NULL,
          cross_sell_1_product_id        varchar2(10)     NOT NULL,
          cross_sell_2_product_id        varchar2(10)  ,
          cross_sell_3_product_id        varchar2(10)  ,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
          OPERATION$                   VARCHAR2(7)      not null,
          TIMESTAMP$                   DATE             not null
        )
 TABLESPACE joe_DATA;



grant select on clean.order_details to ftd_apps;



grant references on ftd_apps.florist_master to pas;
grant references on ftd_apps.product_master to pas;
grant references on ftd_apps.country_master to pas;
grant references on ftd_apps.country_keys to pas;
grant references on ftd_apps.vendor_master to pas;



CREATE TABLE PAS.PAS_FLORIST_DT (
          florist_id                   varchar2(9)      NOT NULL,
          delivery_date                date             NOT NULL,
          cutoff_time                  varchar2(4)      NOT NULL,
          status_code                  varchar2(1)      NOT NULL,
    CONSTRAINT PAS_FLORIST_DT_PK PRIMARY KEY(florist_id, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_FLORIST_DT_FK1 FOREIGN KEY(florist_id) REFERENCES ftd_apps.florist_master(florist_id))
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_FLORIST_DT IS 'Stores florist delivery date availability and cutoff times';
COMMENT ON COLUMN pas.PAS_FLORIST_DT.florist_id is 'Unique identifier for florist from florist_master table';
COMMENT ON COLUMN pas.PAS_FLORIST_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_FLORIST_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  if null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_FLORIST_DT.status_code is 'A=Available, U=Unavailable (subject to change)';



CREATE TABLE PAS.PAS_STATE_DT (
          state_code                   varchar2(2)      NOT NULL,
          delivery_date                date             NOT NULL,
          cutoff_time                  varchar2(4)      NOT NULL,
    CONSTRAINT PAS_STATE_DT_PK PRIMARY KEY(state_code, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX)
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_STATE_DT IS 'Stores florist delivery date availability and cutoff times';
COMMENT ON COLUMN pas.PAS_STATE_DT.state_code is 'Unique identifier for a state';
COMMENT ON COLUMN pas.PAS_STATE_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_STATE_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  If null, no delivery is available.';




CREATE TABLE PAS.PAS_PRODUCT_STATE_DT (
          product_id                   varchar2(10)      NOT NULL,
          state_code                   varchar2(2)       NOT NULL,
          delivery_date                date              NOT NULL,
          delivery_available_flag     VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT PAS_PRODUCT_STATE_DT_pk     PRIMARY KEY(product_id, state_code, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_PRODUCT_STATE_DT_FK1 FOREIGN KEY(product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT PAS_PRODUCT_STATE_DT_CK1  CHECK(delivery_available_flag IN('Y','N')) )
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_PRODUCT_STATE_DT IS 'Stores delivery date availability and cutoff times specific to products and states.';
COMMENT ON COLUMN pas.PAS_PRODUCT_STATE_DT.product_id is 'Unique identifier for a product from product_master table';
COMMENT ON COLUMN pas.PAS_PRODUCT_STATE_DT.state_code is 'Unique identifier for a state';
COMMENT ON COLUMN pas.PAS_PRODUCT_STATE_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_PRODUCT_STATE_DT.delivery_available_flag is 'Y=Yes, N=No';



CREATE TABLE PAS.PAS_COUNTRY_DT (
          country_id                   varchar2(2)      NOT NULL,
          delivery_date                date              NOT NULL,
          cutoff_time                  varchar2(4)      NOT NULL,
          addon_days_qty               number(3)         NOT NULL,
          ship_allowed_flag            VARCHAR2(1) DEFAULT 'N' NOT NULL,
          delivery_available_flag      VARCHAR2(1) DEFAULT 'N' NOT NULL,
          transit_allowed_flag         VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT PAS_COUNTRY_DT_pk     PRIMARY KEY(country_id, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_COUNTRY_DT_FK1 FOREIGN KEY(country_id) REFERENCES ftd_apps.country_keys(country_id),
    CONSTRAINT PAS_COUNTRY_DT_CK1  CHECK(ship_allowed_flag IN('Y','N')),
    CONSTRAINT PAS_COUNTRY_DT_CK2  CHECK(delivery_available_flag IN('Y','N')),
    CONSTRAINT PAS_COUNTRY_DT_CK3  CHECK(transit_allowed_flag IN('Y','N')))
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_COUNTRY_DT IS 'Stores delivery date availability and cutoff times specific to countries.';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.country_id is 'Unique identifier for a country as found in country_keys table';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  If null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.addon_days_qty is 'Addon days for delivering to this country.';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.ship_allowed_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.delivery_available_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_COUNTRY_DT.transit_allowed_flag is 'Y=Yes, N=No';


CREATE TABLE PAS.PAS_PRODUCT_DT (
          product_id                   varchar2(10)      NOT NULL,
          delivery_date                date              NOT NULL,
          cutoff_time                  varchar2(4)       NOT NULL,
          addon_days_qty               number(3)         NOT NULL,
          transit_days_qty             number(3)         NOT NULL,         
          ship_allowed_flag            VARCHAR2(1) DEFAULT 'N' NOT NULL,
          ship_restricted_flag         VARCHAR2(1) DEFAULT 'N' NOT NULL,
          delivery_available_flag      VARCHAR2(1) DEFAULT 'N' NOT NULL,
          codification_id              varchar2(5),
          product_type                 varchar2(25),
    CONSTRAINT PAS_PRODUCT_DT_pk     PRIMARY KEY(product_id, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_PRODUCT_DT_FK1 FOREIGN KEY(product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT PAS_PRODUCT_DT_CK1  CHECK(ship_allowed_flag IN('Y','N')) ,
    CONSTRAINT PAS_PRODUCT_DT_CK2  CHECK(ship_restricted_flag IN('Y','N')) ,
    CONSTRAINT PAS_PRODUCT_DT_CK3  CHECK(delivery_available_flag IN('Y','N'))  )
 tablespace pas_DATA;

create index pas.pas_product_dt_n1 on pas.pas_product_dt(delivery_date,codification_id) 
tablespace pas_indx compute statistics;


COMMENT ON TABLE pas.PAS_PRODUCT_DT IS 'Stores delivery date availability and cutoff times specific to products.';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.product_id is 'Unique identifier for a product from product_master table';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  If null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.addon_days_qty is 'Addon days for this product.';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.transit_days_qty is 'Number of additional transit days for this product.';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.ship_allowed_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.ship_restricted_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.delivery_available_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.codification_id is 'Codification, if it has one';
COMMENT ON COLUMN pas.PAS_PRODUCT_DT.product_type is 'product type';




CREATE TABLE PAS.PAS_VENDOR_DT (
          vendor_id                     varchar2(5)      NOT NULL,
          delivery_date                date              NOT NULL,
          cutoff_time                  varchar2(4)       NOT NULL,
          ship_allowed_flag            VARCHAR2(1) DEFAULT 'N' NOT NULL,
          delivery_available_flag      VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT PAS_VENDOR_DT_pk     PRIMARY KEY(vendor_id, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_VENDOR_DT_FK1  FOREIGN KEY(vendor_id) REFERENCES ftd_apps.vendor_master(vendor_id),
    CONSTRAINT PAS_VENDOR_DT_CK1  CHECK(ship_allowed_flag IN('Y','N')) ,
    CONSTRAINT PAS_VENDOR_DT_CK2  CHECK(delivery_available_flag IN('Y','N'))  )
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_VENDOR_DT IS 'Stores delivery date availability and cutoff times specific to vendors.';
COMMENT ON COLUMN pas.PAS_VENDOR_DT.vendor_id is 'Unique identifier for a vendor from vendor_master table';
COMMENT ON COLUMN pas.PAS_VENDOR_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_VENDOR_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  If null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_VENDOR_DT.ship_allowed_flag is 'Y=Yes, N=No';
COMMENT ON COLUMN pas.PAS_VENDOR_DT.delivery_available_flag is 'Y=Yes, N=No';



#
#   NOTE:  FKs intentionally left off for performance.  No process can maintain this
#          except the PAS load, which rebuilds daily, so FKs should be unnecessary.
#
CREATE TABLE PAS.PAS_VENDOR_PRODUCT_STATE_DT (
          vendor_id                    varchar2(5)       NOT NULL,
          product_id                   varchar2(10)      NOT NULL,
          state_code                   varchar2(2)       NOT NULL,
          delivery_date                date              NOT NULL,
          ship_nd_date                 date,
          ship_2d_date                 date,
          ship_gr_date                 date,
          ship_sat_date                date,
          ship_nd_cutoff_time          varchar2(4),         
          ship_2d_cutoff_time          varchar2(4),         
          ship_gr_cutoff_time          varchar2(4),         
          ship_sat_cutoff_time         varchar2(4),         
    CONSTRAINT PAS_VENDOR_PROD_STATE_DATE_pk     PRIMARY KEY(product_id,delivery_date,state_code,vendor_id) 
            USING INDEX  TABLESPACE pas_INDX  )
 tablespace pas_DATA;

create index pas.PAS_VENDOR_PRODUCT_STATE_DT_N1 
on pas.PAS_VENDOR_PRODUCT_STATE_DT(delivery_date, state_code) 
tablespace pas_indx compute statistics;


COMMENT ON TABLE pas.PAS_VENDOR_PRODUCT_STATE_DT IS 'Stores delivery date availability and cutoff times specific to the combinatino of vendor, product and state.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.vendor_id is 'Unique identifier for a vendor from vendor_master table';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.product_id is 'Unique identifier for a product from product_master table';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.state_code is 'Unique identifier for a state';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_nd_date is 'Ship date for next day shiping for the delivery date.  Null means not available.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_2d_date is 'Ship date for two day shiping for the delivery date.  Null means not available.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_gr_date is 'Ship date for ground shiping for the delivery date.  Null means not available.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_sat_date is 'Ship date for Saturday shiping for the delivery date.  Null means not available.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_nd_cutoff_time is 'Ship cutoff for the day the shipping would occur.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_2d_cutoff_time is 'Ship cutoff for the day the shipping would occur.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_gr_cutoff_time is 'Ship cutoff for the day the shipping would occur.';
COMMENT ON COLUMN pas.PAS_VENDOR_PRODUCT_STATE_DT.ship_sat_cutoff_time is 'Ship cutoff for the day the shipping would occur.';




CREATE TABLE PAS.PAS_COUNTRY_PRODUCT_DT (
          country_id                   varchar2(2)      NOT NULL,
          product_id                   varchar2(10)      NOT NULL,
          delivery_date                date              NOT NULL,
          cutoff_date                  date              NOT NULL,
          cutoff_time                  varchar2(4)       NOT NULL,
          delivery_available_flag      VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT PAS_COUNTRY_PRODUCT_DT_pk     PRIMARY KEY(country_id, product_id, delivery_date) 
            USING INDEX  TABLESPACE pas_INDX,
    CONSTRAINT PAS_COUNTRY_PRODUCT_DT_FK1  FOREIGN KEY(country_id) REFERENCES ftd_apps.country_keys(country_id),
    CONSTRAINT PAS_COUNTRY_PRODUCT_DT_FK2  FOREIGN KEY(product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT PAS_COUNTRY_PRODUCT_DT_CK1  CHECK(delivery_available_flag IN('Y','N')) )
 tablespace pas_DATA;


COMMENT ON TABLE pas.PAS_COUNTRY_PRODUCT_DT IS 'Stores delivery date availability and cutoff times specific to international entities';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.country_id is 'Unique identifier for a country as found in country_keys table';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.product_id is 'Unique identifier for a product from product_master table';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.delivery_date is 'Delivery Date';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.cutoff_date is 'Cutoff Date for achieving delivery for the delivery_date.  If null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.cutoff_time is 'Cutoff Time for achieving delivery for the delivery_date.  If null, no delivery is available.';
COMMENT ON COLUMN pas.PAS_COUNTRY_PRODUCT_DT.delivery_available_flag is 'Y=Yes, N=No';



CREATE TABLE PAS.PAS_TIMEZONE_DT (
      TIME_ZONE_CODE           varchar2(5) NOT NULL,
      DAYLIGHT_SAVINGS_FLAG    varchar2(1) DEFAULT 'Y' NOT NULL,
      DELIVERY_DATE            Date        NOT NULL,
      DELTA_TO_CST_HHMM     varchar2(5) NOT NULL,
   CONSTRAINT PAS_TIMEZONE_DT_PK     PRIMARY KEY(time_zone_code, daylight_savings_flag, delivery_date) 
             USING INDEX  TABLESPACE pas_INDX,
   CONSTRAINT PAS_TIMEZONE_DT_CK1  CHECK(daylight_savings_flag IN('Y','N')) )
 tablespace pas_DATA;
 
 
comment on table pas.pas_timezone_dt is 'Stores the time zone codes and the delta to get it to central time.  The data is by date because of daylight savings time and time differences that causes.';
comment on column pas.pas_timezone_dt.TIME_ZONE_CODE is 'Unique time zone code';
comment on column pas.pas_timezone_dt.DAYLIGHT_SAVINGS_FLAG IS 'Y = Follows, N = Does not Follow';
comment on column pas.pas_timezone_dt.delivery_date is 'Delivery Date';
comment on column pas.pas_timezone_dt.DELTA_TO_CST_HHMM is 'Delta value between time zone and CST';




grant select on ftd_apps.florist_blocks to pas;
grant select on ftd_apps.florist_codifications to pas;
grant select on ftd_apps.florist_products to pas;
grant select on ftd_apps.florist_zips to pas;
grant select on ftd_apps.zip_code to pas;
grant select on ftd_apps.product_subcodes to pas;



grant select on ftd_apps.Codification_master to pas;
grant select on ftd_apps.Codified_products to pas;
grant select on ftd_apps.Country_master to pas;
grant select on ftd_apps.Florist_master to pas;
grant select on ftd_apps.Holiday_country to pas;
grant select on ftd_apps.Product_master to pas;
grant select on ftd_apps.Product_index to pas;
grant select on ftd_apps.Product_index_xref to pas;
grant select on ftd_apps.Product_excluded_states to pas;
grant select on ftd_apps.Product_ship_methods to pas;
grant select on ftd_apps.Product_ship_dates to pas;
grant select on ftd_apps.State_master to pas;
grant select on ftd_apps.Vendor_master to pas;
grant select on ftd_apps.Vendor_product to pas;
grant select on ftd_apps.Vendor_shipping_restrictions to pas;
grant select on ftd_apps.Vendor_delivery_restrictions to pas;



CREATE OR REPLACE VIEW pas.pas_florist_product_zip_dt_vw (florist_id,
                                                                delivery_date,
                                                                zip_code,
                                                                codification_id,
                                                                product_id,
                                                                florist_cutoff_time,
                                                                zip_cutoff_time,
                                                                addon_days_qty,
                                                                fc_b_s_d,
                                                                fc_b_e_d,
                                                                fz_b_s_d,
                                                                fz_b_e_d,
                                                                fb_b_s_d,
                                                                fb_b_e_d
                                                               ) as
SELECT pf.florist_id, pf.delivery_date, fz.zip_code, fc.codification_id,
          pp.product_id,
          --pf.cutoff_time AS florist_cutoff_time,
          (pf.cutoff_time + pt.delta_to_cst_hhmm) AS florist_cutoff_time,
          (fz.cutoff_time + pt.delta_to_cst_hhmm) AS zip_cutoff_time,
          --fz.cutoff_time AS zip_cutoff_time,
          pp.addon_days_qty, fc.block_start_date AS fc_b_s_d,
          fc.block_end_date AS fc_b_e_d, fz.block_start_date AS fz_b_s_d,
          fz.block_end_date AS fz_b_e_d, fb.block_start_date AS fb_b_s_d,
          fb.block_end_date AS fb_b_e_d
     FROM pas_florist_dt pf,
          ftd_apps.florist_zips fz,
          ftd_apps.florist_codifications fc,
          pas_product_dt pp,
          ftd_apps.florist_blocks fb,
          ftd_apps.zip_code zc,
          ftd_apps.state_master sm,
          pas_timezone_dt pt,
          pas.pas_country_dt pc
    WHERE 1 = 1
      AND pt.time_zone_code = sm.TIME_ZONE
      AND pt.delivery_date = pf.delivery_date
      AND sm.state_master_id = zc.state_id
      AND zc.zip_code_id = fz.zip_code
      AND pf.florist_id = fz.florist_id
      AND pf.florist_id = fc.florist_id
      AND fc.codification_id = pp.codification_id
      AND pf.delivery_date = pp.delivery_date
      AND fb.florist_id (+)= pf.florist_id
      AND pc.delivery_date = pf.delivery_date
      AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
      AND pc.delivery_available_flag = 'Y'
      AND pp.delivery_available_flag = 'Y'
          AND pf.status_code  = 'Y'
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fc.block_start_date)
                                        AND fc.block_end_date
              )
           OR (fc.block_start_date IS NULL)
          )
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fz.block_start_date)
                                        AND fz.block_end_date
              )
           OR (fz.block_start_date IS NULL)
          )
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fb.block_start_date)
                                        AND fb.block_end_date
              )
           OR (fb.block_start_date IS NULL)
          )
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fb.suspend_start_date)
                                        AND fb.suspend_end_date
              )
           OR (fb.suspend_start_date IS NULL)
          )
   UNION
   SELECT pf.florist_id, pf.delivery_date, fz.zip_code, '', pp.product_id,
          (pf.cutoff_time + pt.delta_to_cst_hhmm) AS florist_cutoff_time,
          (fz.cutoff_time + pt.delta_to_cst_hhmm) AS zip_cutoff_time,
          --pf.cutoff_time AS florist_cutoff_time,
          --fz.cutoff_time AS zip_cutoff_time,
          pp.addon_days_qty, NULL AS fc_b_s_d, NULL AS fc_b_e_d,
          fz.block_start_date AS fz_b_s_d, fz.block_end_date AS fz_b_e_d,
          fb.block_start_date AS fb_b_s_d, fb.block_end_date AS fb_b_e_d
     FROM pas_florist_dt pf,
          ftd_apps.florist_zips fz,
          pas_product_dt pp,
          ftd_apps.florist_blocks fb,
          ftd_apps.zip_code zc,
          ftd_apps.state_master sm,
          pas_timezone_dt pt,
          pas.pas_country_dt pc
    WHERE 1 = 1
      AND pt.time_zone_code = sm.TIME_ZONE
      AND pt.delivery_date = pf.delivery_date
      AND sm.state_master_id = zc.state_id
      AND zc.zip_code_id = fz.zip_code
      AND pf.florist_id = fz.florist_id
      AND pf.delivery_date = pp.delivery_date
      AND (pp.codification_id = '' OR pp.codification_id IS NULL)
      AND fb.florist_id (+)= pf.florist_id
      AND pp.product_type IN ('FLORAL', 'SDFC', 'SDG')
      AND pc.delivery_date = pf.delivery_date
      AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
      AND pc.delivery_available_flag = 'Y'
      AND pp.delivery_available_flag = 'Y'
          AND pf.status_code  = 'Y'
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fz.block_start_date)
                                        AND fz.block_end_date
              )
           OR (fz.block_start_date IS NULL)
          )
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fb.block_start_date)
                                        AND fb.block_end_date
              )
           OR (fb.block_start_date IS NULL)
          )
      AND (   (pf.delivery_date NOT BETWEEN TRUNC(fb.suspend_start_date)
                                        AND fb.suspend_end_date
              )
           OR (fb.suspend_start_date IS NULL)
          );


CREATE OR REPLACE FORCE VIEW pas.pas_vendor_product_zip_dt_vw (vendor_id,
                                                               product_id,
                                                               zip_code_id,
                                                               delivery_date,
                                                               ship_nd_date,
                                                               ship_2d_date,
                                                               ship_gr_date,
                                                               ship_sat_date,
                                                               ship_nd_cutoff_time,
                                                               ship_2d_cutoff_time,
                                                               ship_gr_cutoff_time,
                                                               ship_sat_cutoff_time
                                                              )
AS
   SELECT pvpsd.vendor_id, pvpsd.product_id, zip.zip_code_id,
          pvpsd.delivery_date, pvpsd.ship_nd_date, pvpsd.ship_2d_date,
          pvpsd.ship_gr_date, pvpsd.ship_sat_date, pvpsd.ship_nd_cutoff_time,
          pvpsd.ship_2d_cutoff_time, pvpsd.ship_gr_cutoff_time,
          pvpsd.ship_sat_cutoff_time
     FROM pas_vendor_product_state_dt pvpsd, ftd_apps.zip_code zip
    WHERE pvpsd.state_code = zip.state_id;


grant select on  FTD_APPS.COUNTRY_MASTER to joe;
grant select on  FTD_APPS.STATE_MASTER to joe;
grant select on  FTD_APPS.DNIS to joe;
grant select on  FTD_APPS.SOURCE_MASTER to joe;
grant select on  FTD_APPS.ZIP_CODE to joe;
grant select on  FTD_APPS.OCCASION to joe;
grant select on  FTD_APPS.ADDON to joe;
grant select on  FTD_APPS.PAYMENT_METHODS to joe;
grant select on  FTD_APPS.PAYMENT_METHODS_TEXT to joe;
grant select on  ftd_apps.source_program_ref to joe;
grant select on  FTD_APPS.OCCASION_ADDON to joe;
grant select on  frp.global_parms to joe;
grant select on  FTD_APPS.COLOR_MASTER         to joe;     
grant select on  FTD_APPS.PRODUCT_COLORS    to joe;        
grant select on  FTD_APPS.PRODUCT_COMPANY_XREF  to joe;    
grant select on  FTD_APPS.PRODUCT_SECOND_CHOICE  to joe;    
grant select on  FTD_APPS.PRODUCT_SUBCODES   to joe;       
grant select on  FTD_APPS.UPSELL_DETAIL    to joe;         
grant select on  FTD_APPS.UPSELL_MASTER   to joe;          
grant select on  FTD_APPS.PARTNER_PROGRAM  to joe;         
grant select on  FTD_APPS.SOURCE  to joe;                  
grant select on ftd_apps.product_master to joe;
grant select on ftd_apps.product_subcodes to joe;
grant execute on ftd_apps.oe_get_delivery_date_ranges to joe;
grant select on clean.gc_coupons to joe;


CREATE TABLE JOE.product_favorite (
          display_seq                  number(3)         NOT NULL,
          product_id                   varchar2(10)      NOT NULL,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT product_favorite_pk     PRIMARY KEY(display_seq) 
            USING INDEX  TABLESPACE joe_INDX ,
    CONSTRAINT product_favorite_FK1  FOREIGN KEY(product_id) REFERENCES ftd_apps.product_master(product_id))
 tablespace joe_DATA;


COMMENT ON TABLE JOE.PRODUCT_FAVORITE IS 'Stores product favorites for use by product search';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.display_seq is 'Stores the order/sequence the product is displayed on the screen';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.product_id is 'Product Id found in ftd_apps.product_master';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.PRODUCT_FAVORITE.updated_by is 'Date record was last updated';



CREATE TABLE JOE.card_message (
          display_seq                  number(3)        NOT NULL,
          message_txt                  varchar2(1000)   NOT NULL,
          active_flag                  VARCHAR2(1) DEFAULT 'N' NOT NULL,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT card_message_pk     PRIMARY KEY(display_seq) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT card_message_CK1  CHECK(active_flag IN('Y','N'))    )
 tablespace joe_DATA;


COMMENT ON TABLE JOE.card_message IS 'Stores pre-configured card messages for use during order taking';
COMMENT ON COLUMN JOE.card_message.display_seq is 'Stores the order/sequence the product is displayed on the screen';
COMMENT ON COLUMN JOE.card_message.message_txt is 'Card Message';
COMMENT ON COLUMN JOE.card_message.active_flag is 'Flag to determine whether the message is available for use';
COMMENT ON COLUMN JOE.card_message.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.card_message.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.card_message.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.card_message.updated_by is 'Date record was last updated';


CREATE TABLE JOE.ADDRESS_TYPE (
       ADDRESS_TYPE_CODE           VARCHAR2(32) NOT NULL, 
       DESCRIPTION                 VARCHAR2(1000) NOT NULL, 
       PROMPT_FOR_BUSINESS_FLAG    VARCHAR2(1) DEFAULT 'N' NOT NULL, 
       BUSINESS_LABEL_TXT          VARCHAR2(250), 
       PROMPT_FOR_LOOKUP_FLAG      VARCHAR2(1) DEFAULT 'N' NOT NULL, 
       LOOKUP_LABEL_TXT            VARCHAR2(250), 
       PROMPT_FOR_ROOM_FLAG        VARCHAR2(1) DEFAULT 'N' NOT NULL, 
       ROOM_LABEL_TXT              VARCHAR2(250), 
       PROMPT_FOR_HOURS_FLAG       VARCHAR2(1) DEFAULT 'N' NOT NULL, 
       HOURS_LABEL_TXT             VARCHAR2(250), 
       DEFAULT_FLAG                VARCHAR2(1), 
    CONSTRAINT address_type_pk     PRIMARY KEY(address_type_code) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT ADDRESS_TYPE_CK1 CHECK(PROMPT_FOR_BUSINESS_FLAG IN ('Y','N')), 
    CONSTRAINT ADDRESS_TYPE_CK2 CHECK(PROMPT_FOR_LOOKUP_FLAG IN ('Y','N')), 
    CONSTRAINT ADDRESS_TYPE_CK3 CHECK(PROMPT_FOR_ROOM_FLAG IN ('Y','N')), 
    CONSTRAINT ADDRESS_TYPE_CK4 CHECK(PROMPT_FOR_HOURS_FLAG IN ('Y','N')), 
    CONSTRAINT ADDRESS_TYPE_CK5 CHECK(DEFAULT_FLAG IN ('Y'))  )
  tablespace joe_data;

           
CREATE UNIQUE INDEX  JOE.ADDRESS_TYPE_UK1 ON  JOE.ADDRESS_TYPE  (DEFAULT_FLAG) tablespace joe_indx;

INSERT INTO JOE.ADDRESS_TYPE VALUES('BUSINESS','Business','Y','Business Name','N',null,'Y','Room','Y','Work Hours',null);
INSERT INTO JOE.ADDRESS_TYPE VALUES('FUNERAL_HOME','Funeral Home','Y','Funeral Home','Y','Funeral home search','Y','Room/Chapel','Y','Time of Service', null);
INSERT INTO JOE.ADDRESS_TYPE VALUES('HOME','Residential','N',null,'N',null,'N',null,'N',null,'Y');
INSERT INTO JOE.ADDRESS_TYPE VALUES('HOSPITAL','Hospital','Y','Hospital Name','Y','Hospital search','Y','Room/Ward','N',null,null);
INSERT INTO JOE.ADDRESS_TYPE VALUES('NURSING_HOME','Nursing Home','Y','Nursing Home','Y','Nursing home search','Y','Room/Ward','N',null,null);
INSERT INTO JOE.ADDRESS_TYPE VALUES('OTHER','Other','N',null,'N',null,'N',null,'N',null,null);


alter table scrub.order_details add (qms_result_code   varchar2(20) );
COMMENT ON COLUMN scrub.order_details.qms_result_code is 'QMS Result Code';

grant references on aas.identity to scrub;
grant references on aas.identity to clean;

alter TABLE scrub.payments add (
      nc_approval_identity_id	varchar2(255),
      nc_type_code              varchar2(50), 
      nc_reason_code            varchar2(50),
      nc_order_detail_id        number(38),
    CONSTRAINT payments_CK1  CHECK(nc_type_Code IN('COMP','RESEND','FLOWERS_MONTHLY')), 
    CONSTRAINT payments_CK2  CHECK(nc_reason_Code IN('NONDELIVERY','NOSCAN','LATE','OTHER','QUALITY','SUBSTITUTION')), 
    CONSTRAINT payments_FK1 FOREIGN KEY(nc_approval_identity_id)  REFERENCES aas.identity(identity_id),
    CONSTRAINT payments_FK2 FOREIGN KEY(nc_order_detail_id) REFERENCES scrub.order_details(order_detail_id)
    );

comment on column scrub.payments.nc_approval_identity_id is 'User that approved the no charge';
comment on column scrub.payments.nc_type_code is 'Type of no charge ';
comment on column scrub.payments.nc_reason_code is 'Reason for no charge';
comment on column scrub.payments.nc_order_detail_id is 'Original order number associated with the no charge';

create index scrub.payments_n6 on scrub.payments(nc_order_detail_id) tablespace op_indx compute statistics;


alter table clean.order_details add (qms_result_code   varchar2(20) );
COMMENT ON COLUMN clean.order_details.qms_result_code is 'QMS Result Code';

alter TABLE clean.payments add (
      nc_approval_identity_id	varchar2(255),
      nc_type_code              varchar2(50), 
      nc_reason_code            varchar2(50),
      nc_order_detail_id        number,
    CONSTRAINT payments_CK2  CHECK(nc_type_Code IN('COMP','RESEND','FLOWERS_MONTHLY')), 
    CONSTRAINT payments_CK3  CHECK(nc_reason_Code IN('NONDELIVERY','NOSCAN','LATE','OTHER','QUALITY','SUBSTITUTION')), 
    CONSTRAINT payments_FK2 FOREIGN KEY(nc_approval_identity_id)  REFERENCES aas.identity(identity_id),
    CONSTRAINT payments_FK3 FOREIGN KEY(nc_order_detail_id) REFERENCES clean.order_details(order_detail_id)
    );

comment on column clean.payments.nc_approval_identity_id is 'User that approved the no charge';
comment on column clean.payments.nc_type_code is 'Type of no charge ';
comment on column clean.payments.nc_reason_code is 'Reason for no charge';
comment on column clean.payments.nc_order_detail_id is 'Original order number associated with the no charge';

create index clean.payments_n4 on clean.payments(nc_order_detail_id) tablespace clean_indx compute statistics;


grant select on clean.customer to joe;
grant select on clean.customer_phones to joe; 
grant select on clean.memberships to joe;
grant select on clean.customer_email_ref to joe;
grant select on clean.email to joe;



alter table ftd_apps.state_master add (
          drop_ship_available_flag           VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT state_master_CK1  CHECK(drop_ship_available_flag IN('Y','N')));

Update ftd_apps.state_master set drop_ship_available_flag = 'Y';
Update ftd_apps.state_master set drop_ship_available_flag = 'N' where country_code is not null;
Update ftd_apps.state_master set drop_ship_available_flag = 'N' where state_master_id in ('VI','PR');




ALTER TABLE FTD_APPS.PAYMENT_METHODS ADD (
     ACTIVE_FLAG        varCHAR2(1) DEFAULT 'Y' NOT NULL, 
     JOE_FLAG           varCHAR2(1) DEFAULT 'Y' NOT NULL, 
     MOD10_FLAG         varCHAR2(1) DEFAULT 'N' NOT NULL, 
     REGEX_PATTERN      VARCHAR2(100), 
  CONSTRAINT PAYMENT_METHODS_CK2 CHECK(active_flag IN ('Y','N')), 
  CONSTRAINT PAYMENT_METHODS_CK3 CHECK(joe_flag IN ('Y','N')),
  CONSTRAINT PAYMENT_METHODS_CK4 CHECK(mod10_flag IN ('Y','N'))
);


UPDATE FTD_APPS.PAYMENT_METHODS p SET p.ACTIVE_FLAG = 'N' WHERE p.PAYMENT_METHOD_ID = 'JP';
UPDATE FTD_APPS.PAYMENT_METHODS p SET p.JOE_FLAG = 'N' WHERE p.PAYMENT_METHOD_ID IN ('JP','PP','PC');
UPDATE FTD_APPS.PAYMENT_METHODS p SET p.MOD10_FLAG = 'Y' WHERE p.PAYMENT_METHOD_ID IN ('AX','DC','DI','MC','NC','VI','CB');



grant select on ftd_apps.florist_master to joe;
grant select on ftd_apps.florist_zips to joe;
grant select on ftd_apps.florist_codifications to joe;
grant select on ftd_apps.codified_products to joe;



alter table ftd_apps.country_master add (
         SUNDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         MONDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         TUESDAY_TRANSIT_FLAG      VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         WEDNESDAY_TRANSIT_FLAG    VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         THURSDAY_TRANSIT_FLAG     VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         FRIDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         SATURDAY_TRANSIT_FLAG     VARCHAR2(1)     DEFAULT 'N' NOT NULL,
    CONSTRAINT COUNTRY_MASTER_CK9  CHECK(SUNDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK10  CHECK(MONDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK12 CHECK(TUESDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK13 CHECK(WEDNESDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK14 CHECK(THURSDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK15 CHECK(FRIDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK16  CHECK(SATURDAY_TRANSIT_FLAG IN('Y','N')));



update ftd_apps.country_master
     set  SUNDAY_TRANSIT_FLAG       = 'N',
          MONDAY_TRANSIT_FLAG       = 'Y',
          TUESDAY_TRANSIT_FLAG      = 'Y',
          WEDNESDAY_TRANSIT_FLAG    = 'Y',
          THURSDAY_TRANSIT_FLAG     = 'Y',
          FRIDAY_TRANSIT_FLAG       = 'Y',
          SATURDAY_TRANSIT_FLAG     = 'N'
   where country_id = 'US';

grant select on ftd_apps.ship_methods to pas;


CREATE TABLE JOE.ELEMENT_CONFIG ( 
        APP_CODE                    VARCHAR2(64)      NOT NULL, 
        ELEMENT_ID                  VARCHAR2(32)      NOT NULL, 
        LOCATION_DESC               VARCHAR2(10)      NOT NULL, 
        LABEL_ELEMENT_ID            VARCHAR2(32), 
        COUNT_ELEMENT_ID            VARCHAR2(32), 
        REQUIRED_FLAG               VARCHAR2(1) DEFAULT 'N' NOT NULL, 
        REQUIRED_COND_DESC          VARCHAR2(4000), 
        REQUIRED_COND_TYPE_NAME     VARCHAR2(32), 
        VALIDATE_FLAG               VARCHAR2(1) DEFAULT 'N' NOT NULL, 
        VALIDATE_EXP                VARCHAR2(4000), 
        VALIDATE_EXP_TYPE_NAME      VARCHAR2(32), 
        ERROR_TXT                   VARCHAR2(4000), 
        TITLE_TXT                   VARCHAR2(4000), 
        VALUE_MAX_BYTES_QTY         NUMBER(4), 
        TAB_SEQ                     NUMBER(5)         NOT NULL, 
        TAB_INIT_SEQ                NUMBER(5)         NOT NULL, 
        TAB_COND_DESC               VARCHAR2(4000), 
        TAB_COND_TYPE_NAME          VARCHAR2(32), 
        ACCORDION_INDEX_NUM         NUMBER(2), 
        TAB_CTRL_INDEX_NUM          NUMBER(2), 
        RECALC_FLAG                 VARCHAR2(1) DEFAULT 'N' NOT NULL, 
        PRODUCT_VALIDATE_FLAG       VARCHAR2(1) DEFAULT 'N' NOT NULL, 
        ACCESS_KEY_NAME             VARCHAR2(1), 
        XML_NODE_NAME               VARCHAR2(256), 
        CALC_XML_FLAG               VARCHAR2(1) DEFAULT 'Y' NOT NULL, 
     CONSTRAINT ELEMENT_CONFIG_PK   PRIMARY KEY (APP_CODE, ELEMENT_ID)
             USING INDEX TABLESPACE JOE_INDX,
     CONSTRAINT ELEMENT_CONFIG_U1   UNIQUE (APP_CODE,TAB_SEQ)  USING INDEX TABLESPACE JOE_INDX,  
     CONSTRAINT ELEMENT_CONFIG_CK1  CHECK (RECALC_FLAG IN ('Y','N')), 
     CONSTRAINT ELEMENT_CONFIG_CK2  CHECK (REQUIRED_FLAG IN ('Y','N')), 
     CONSTRAINT ELEMENT_CONFIG_CK3  CHECK (REQUIRED_COND_TYPE_NAME IN ('FUNCTION','PASSED_FUNCTION')), 
     CONSTRAINT ELEMENT_CONFIG_CK4  CHECK (VALIDATE_FLAG IN ('Y','N')), 
     CONSTRAINT ELEMENT_CONFIG_CK5  CHECK (VALIDATE_EXP_TYPE_NAME IN ('REGEX','FUNCTION','PASSED_FUNCTION')), 
     CONSTRAINT ELEMENT_CONFIG_CK6  CHECK (VALUE_MAX_BYTES_QTY > 0), 
     CONSTRAINT ELEMENT_CONFIG_CK7  CHECK (APP_CODE IN ('JOE','IOTW','CROSSSELL','FAVORITES','PHRASE')), 
     CONSTRAINT ELEMENT_CONFIG_CK8  CHECK (TAB_SEQ >= -1 AND TAB_SEQ <= 32767), 
     CONSTRAINT ELEMENT_CONFIG_CK9  CHECK (TAB_INIT_SEQ >= -1 AND TAB_INIT_SEQ <= 32767),
     CONSTRAINT ELEMENT_CONFIG_CK10 CHECK (TAB_COND_TYPE_NAME IN ('REGEX','FUNCTION','PASSED_FUNCTION')), 
     CONSTRAINT ELEMENT_CONFIG_CK11 CHECK (LOCATION_DESC IN ('HEADER','DETAIL','SEARCH')), 
     CONSTRAINT ELEMENT_CONFIG_CK12 CHECK (PRODUCT_VALIDATE_FLAG IN ('Y','N')), 
     CONSTRAINT ELEMENT_CONFIG_CK13 CHECK (CALC_XML_FLAG IN ('Y','N')))
 TABLESPACE JOE_DATA;


COMMENT ON TABLE JOE.ELEMENT_CONFIG IS 'Contains html page configuration data';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.APP_CODE IS 'Application page identifier';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.ELEMENT_ID IS 'Element id from the HTML page.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.LOCATION_DESC IS 'Identifies which part of the HTML page where the element is located.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.LABEL_ELEMENT_ID IS 'Label element id for the label element associated with the element from the HTML page';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.COUNT_ELEMENT_ID IS 'Counter element id associated with the element from the HTML page';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.REQUIRED_FLAG IS 'Is the element required to contain data?';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.required_cond_desc IS 'Under what conditions the element required to contain data?  Null indicates that it''s always required.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.required_cond_type_name IS 'What type of condition is "required_cond_desc"?';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.VALIDATE_FLAG IS 'Is the element value to be validated?';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.validate_exp IS 'Expression used to validate the data.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.validate_exp_type_name IS 'What type of expression is "validate_exp".';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.ERROR_TXT IS 'Error to be displayed if the element value fails validation.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.TITLE_TXT IS 'String to be populated in the element "title" attribute.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.value_max_bytes_qty IS 'Maximum allowable size of the element value.  Populates the "maxlength" attribute of the element.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.tab_seq IS '"tabindex" attribute assigned to the html element.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.tab_init_seq IS '"tabindex" attribute assigned to the html element upon page load.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.tab_cond_desc IS 'Under what conditions is the element''s tabindex changed from "tab_init_seq" to the "tab_seq" value.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.tab_cond_type_name IS 'What type of expression is the "tab_cond_desc" value.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.accordion_index_num IS 'Index to the accordion control division which contains the element.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.tab_ctrl_index_num IS 'Index to the tab control division which contains the element.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.RECALC_FLAG IS 'Will changes to the element''s value cause the shopping cart to be revalidated?';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.PRODUCT_VALIDATE_FLAG IS 'Will changes to the element''s value cause the shopping cart to be revalidated?';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.access_key_name IS 'Hot key for the element.  Populated in the element''s "accesskey" attribute.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.xml_node_name IS 'Node name used when generating the order xml.';
COMMENT ON COLUMN JOE.ELEMENT_CONFIG.CALC_XML_FLAG IS 'Determines if the node is sent to the server for recalculating shopping cart totals.';



CREATE TABLE JOE.SPELL_CHECK_WORDS ( 
    WORD        VARCHAR2(100) NOT NULL, 
   CONSTRAINT SPELL_CHECK_WORDS_PK     PRIMARY KEY (WORD)
           using index tablespace joe_indx)
   tablespace joe_data;


grant execute on events.post_a_message_flex to pas;
grant execute on frp.misc_pkg to pas;


alter table ftd_apps.payment_methods add (min_auth_amt number(5,2));


grant select on ftd_apps.product_excluded_states to joe;
grant select on ftd_apps.billing_info to joe;
grant select on ftd_apps.billing_info_options to joe;
grant select on ftd_apps.shipping_key_details to joe;
grant select on ftd_apps.shipping_key_costs to joe;


ALTER TABLE FTD_APPS.BILLING_INFO ADD (
     VALIDATION_REGEX      VARCHAR2(512));

COMMENT ON COLUMN FTD_APPS.BILLING_INFO.VALIDATION_REGEX IS 'Regular expression used to validate input on the JOE screen';


grant select on ftd_apps.product_company_xref to pas;


grant select on ftd_apps.big_sequence to joe;
grant select on ftd_apps.oe_order_id to joe;

grant select on aas.identity to joe;
 

-- DNIS updates
alter table ftd_apps.dnis add (
          company_id        varchar2(12),
          script_type_code  varchar2(100)  DEFAULT 'Default' NOT NULL,
    CONSTRAINT DNIS_FK2 FOREIGN KEY(company_id) REFERENCES ftd_apps.company_master(company_id));

CREATE INDEX ftd_apps.dnis_n3 ON ftd_apps.dnis(company_id) TABLESPACE ftd_apps_INDX;

COMMENT ON COLUMN ftd_apps.dnis.company_id is 'Validated by company_master - will obsolete script_id in the future.';
COMMENT ON COLUMN ftd_apps.dnis.script_type_code is 'Identifies which type of script is applicable to this dnis';

update ftd_apps.dnis set company_id = script_id;

alter table ftd_apps.dnis modify (company_id not null);
-- END DNIS Updates

CREATE TABLE JOE.OE_SCRIPT_MASTER (
          script_id                   varchar2(100)    NOT NULL,
          script_type_code            varchar2(100) DEFAULT 'Default'    NOT NULL,
          script_txt                  varchar2(4000)   NOT NULL,
          created_by                  varchar2(100)    not null,
          created_on                  date             not null,
          updated_by                  varchar2(100)    not null,
          updated_on                  date             not null,
    CONSTRAINT OE_SCRIPT_MASTER_PK PRIMARY KEY(script_id) 
            USING INDEX  TABLESPACE joe_INDX)
 TABLESPACE joe_DATA;


COMMENT ON TABLE JOE.OE_SCRIPT_MASTER IS 'Contains order entry scripts used by customer service';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.script_id is 'Primary key';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.script_type_code is 'Identifies which type of script is applicable';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.script_txt is 'The script';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.OE_SCRIPT_MASTER.updated_by is 'Date record was last updated';


alter table ftd_apps.source_master add (
          iotw_flag        varchar2(1)    DEFAULT 'N' NOT NULL,
          invoice_password varchar2(60),
    CONSTRAINT source_master_CK9  CHECK(iotw_flag IN('Y','N')));



CREATE TABLE JOE.QMS_RESPONSE (
          qms_response_code           varchar2(10)     NOT NULL,
          qms_response_desc           varchar2(500)    NOT NULL,
          created_by                  varchar2(100)    not null,
          created_on                  date             not null,
          updated_by                  varchar2(100)    not null,
          updated_on                  date             not null,
    CONSTRAINT qms_response_PK PRIMARY KEY(qms_response_code) 
            USING INDEX  TABLESPACE joe_INDX)
 TABLESPACE joe_DATA;


COMMENT ON TABLE JOE.qms_response IS 'Stores details on information received from QMS calls';
COMMENT ON COLUMN JOE.qms_response.qms_response_code is 'Primary key';
COMMENT ON COLUMN JOE.qms_response.qms_response_Desc is 'Long description of the response';
COMMENT ON COLUMN JOE.qms_response.created_on is 'Id of user that created the record';
COMMENT ON COLUMN JOE.qms_response.created_by is 'Date record was created';
COMMENT ON COLUMN JOE.qms_response.updated_on is 'Id of user that last updated the record';
COMMENT ON COLUMN JOE.qms_response.updated_by is 'Date record was last updated';

INSERT INTO JOE.QMS_RESPONSE VALUES ('S00', 'No change in last line, no change in address line','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S02', 'No change in last line, pre-directional was changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S04', 'No change in last line, post-directional was changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S06', 'No change in last line, pre-directional and post-directional were changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S80', 'ZIP+4 was changed, no change in address line','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S82', 'ZIP+4 was changed, pre-directional was changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S84', 'ZIP+4 was changed, post-directional was changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
INSERT INTO JOE.QMS_RESPONSE VALUES ('S86', 'ZIP+4 was changed, pre-directional and post-directional were changed','FIX_3_0_0',sysdate,'FIX_3_0_0',sysdate);
 

grant references on ftd_apps.partner_program to joe;
grant references on clean.memberships to joe;

CREATE TABLE JOE.ORDER_CUSTOMER (
          customer_id                     number           NOT NULL,
          first_name                      varchar2(25)     NOT NULL,
          last_name                       varchar2(25)     NOT NULL,
          address                         varchar2(90)     NOT NULL,
          city                            varchar2(30),
          state                           varchar2(20),
          zip_code                        varchar2(12),
          country                         varchar2(20),
          buyer_recipient_ind             varchar2(1) DEFAULT 'B' NOT NULL,
          daytime_phone                   varchar2(20),
          daytime_phone_ext               varchar2(10),
          evening_phone                   varchar2(20),
          evening_phone_ext               varchar2(10),
          email_address                   varchar2(100),
          newsletter_flag                 varchar2(1) default 'N' not null,
          qms_response_code               varchar2(10),
          address_type                    varchar2(20),
          business_name                   varchar2(50),
          business_info                   varchar2(30),
          program_name                    varchar2(10),
          membership_id                   varchar2(100),
          membership_First_name           varchar2(25),
          membership_last_name            varchar2(25),
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT order_customer_PK PRIMARY KEY(customer_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT order_customer_ck1  CHECK(buyer_recipient_ind IN('B','R')),
    CONSTRAINT order_customer_ck2  CHECK(newsletter_flag IN('Y','N')),
    CONSTRAINT order_customer_fk1 FOREIGN KEY(program_name) REFERENCES ftd_apps.partner_program)
 TABLESPACE joe_DATA;

create index joe.order_customer_n1 on joe.order_customer(program_name) tablespace joe_indx;


CREATE SEQUENCE JOE.CUSTOMER_ID_SQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;


grant references on ftd_apps.dnis to joe;
grant references on ftd_apps.origins to joe;
grant references on aas.identity to joe;
grant references on scrub.fraud_order_dispositions to joe;

CREATE TABLE JOE.ORDERS (
          master_order_number          varchar2(100)    NOT NULL,
          buyer_id                     number(11)       NOT NULL,
          dnis_id                      number(4)        NOT NULL,
          source_Code                  varchar2(10)     NOT NULL,
          origin_id                    varchar2(10)     NOT NULL,
          order_date                   date             NOT NULL,
          order_taken_by_identity_id   varchar2(255)    NOT NULL,
          item_cnt                     number(5)        NOT NULL,
          order_amt                    number(12,2)     NOT NULL,
          fraud_id                     varchar2(2),
          fraud_cmnt                   varchar2(100),
          sent_to_apollo_retry_cnt     number(3) DEFAULT 0 NOT NULL,
          sent_to_apollo_date          date,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT orders_PK PRIMARY KEY(master_order_number) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT orders_FK1 FOREIGN KEY(buyer_id) REFERENCES joe.order_customer(customer_id),
    CONSTRAINT orders_FK2 FOREIGN KEY(dnis_id) REFERENCES ftd_apps.dnis(dnis_id),
    CONSTRAINT orders_FK3 FOREIGN KEY(source_code) REFERENCES ftd_apps.source_master(source_Code),
    CONSTRAINT orders_FK4 FOREIGN KEY(origin_id) REFERENCES ftd_apps.origins(origin_id),
    CONSTRAINT orders_FK5 FOREIGN KEY(order_taken_by_identity_id) REFERENCES aas.identity(identity_id),
    CONSTRAINT orders_FK6 FOREIGN KEY(fraud_id) REFERENCES scrub.fraud_order_dispositions(fraud_id))
 TABLESPACE joe_DATA;


create index joe.orders_n1 on joe.orders(dnis_id) tablespace joe_indx;
create index joe.orders_n2 on joe.orders(source_code) tablespace joe_indx;

grant references on ftd_apps.occasion to joe;
grant references on ftd_apps.florist_master to joe;
grant references on ftd_apps.product_subcodes to joe;

CREATE TABLE JOE.ORDER_DETAILS (
          external_order_number        varchar2(20)     NOT NULL,
          master_order_number          varchar2(100)    NOT NULL,
          source_Code                  varchar2(10)     NOT NULL,
          delivery_date                date             NOT NULL,
          recipient_id                 number           NOT NULL,
          product_id                   varchar2(10)     NOT NULL,
          product_subcode_id           varchar2(10),
          first_color_choice           varchar2(2),
          second_Color_Choice          varchar2(2),
          iotw_flag                    varchar2(1) DEFAULT 'N' NOT NULL,
          occasion_id                  number,
          card_message_signature       varchar2(1000),
          order_cmnts                  varchar2(1000),
          florist_cmnts                varchar2(1000),
          florist_id                   varchar2(9),
          ship_method_id               varchar2(10),
          delivery_date_range_end      date,
          size_ind                     varchar2(3),
          product_amt                  number(12,2)     NOT NULL,
          add_on_amt                   number(12,2)     NOT NULL,
          shipping_Fee_amt             number(12,2)     NOT NULL,
          service_fee_amt              number(12,2)     NOT NULL,
          discount_amt                 number(12,2)     NOT NULL,
          tax_amt                      number(12,2)     NOT NULL,
          item_total_amt               number(12,2)     NOT NULL,
          miles_points_qty             number,
          created_by                   varchar2(100)    not null,
          created_on                   date             not null,
          updated_by                   varchar2(100)    not null,
          updated_on                   date             not null,
    CONSTRAINT order_details_PK PRIMARY KEY(external_order_number) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT order_details_ck1  CHECK(iotw_flag IN('Y','N')),
    CONSTRAINT order_Details_FK1 FOREIGN KEY(master_order_number) REFERENCES joe.orders(master_order_number),
    CONSTRAINT order_Details_FK2 FOREIGN KEY(source_Code) REFERENCES ftd_apps.source_master(source_code),
    CONSTRAINT order_Details_FK3 FOREIGN KEY(recipient_id) REFERENCES joe.order_customer(customer_id),
    CONSTRAINT order_Details_FK4 FOREIGN KEY(product_id) REFERENCES ftd_apps.product_master(product_id),
    CONSTRAINT order_Details_FK5 FOREIGN KEY(occasion_id) REFERENCES ftd_apps.occasion(occasion_id),
    CONSTRAINT order_Details_FK6 FOREIGN KEY(florist_id) REFERENCES ftd_apps.florist_master(florist_id))
 TABLESPACE joe_DATA;
 
 create index joe.order_details_n1 on joe.order_details(master_order_number) tablespace joe_indx;
 create index joe.order_details_n2 on joe.order_details(source_code) tablespace joe_indx;
 create index joe.order_details_n3 on joe.order_details(recipient_id) tablespace joe_indx;
 create index joe.order_details_n4 on joe.order_details(product_id) tablespace joe_indx;
 create index joe.order_details_n6 on joe.order_details(florist_id) tablespace joe_indx;
 
 
grant references on ftd_apps.payment_methods to joe;
grant references on clean.order_details to joe;

CREATE TABLE JOE.ORDER_PAYMENTS (
          payment_id                      number           NOT NULL,
          master_order_number             varchar2(100)    NOT NULL,
          payment_method_id               varchar2(2),
          cc_number                       varchar2(2500),
          key_name                        varchar2(30),
          cc_expiration                   varchar2(20),
          auth_amt                        number(12,2),
          approval_code                   varchar2(20),
          approval_verbiage               varchar2(20),
          approval_action_code            varchar2(20),
          acq_reference_number            varchar2(50),
          avs_result_code                 varchar2(100),
          gc_coupon_number                varchar2(20),
          gc_amt                          number(12,2),
          aafes_ticket_number             varchar2(50),
          nc_amt                          number(12,2),
          nc_approval_identity_id         varchar2(255),
          nc_type_code                    varchar2(50),
          nc_reason_code                  varchar2(50),
          nc_order_detail_id              number,
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT order_payments_PK PRIMARY KEY(payment_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT order_payments_FK1 FOREIGN KEY(master_order_number) REFERENCES joe.orders(master_order_number),
    CONSTRAINT order_payments_FK2 FOREIGN KEY(payment_method_id) REFERENCES ftd_apps.payment_methods(payment_method_id),
    CONSTRAINT order_payments_FK3 FOREIGN KEY(nc_approval_identity_id) REFERENCES aas.identity(identity_id),
    CONSTRAINT order_payments_FK4 FOREIGN KEY(nc_order_detail_id) REFERENCES clean.order_details(order_detail_id))
 TABLESPACE joe_DATA;

CREATE SEQUENCE JOE.payment_ID_SQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;

 create index joe.order_payments_n1 on joe.order_payments(master_order_number) tablespace joe_indx;
 create index joe.order_payments_n2 on joe.order_payments(nc_order_detail_id) tablespace joe_indx;



grant references on ftd_apps.addon to joe;

CREATE TABLE JOE.ORDER_ADD_ONS (
          order_add_on_id                 number           NOT NULL,
          external_order_number           varchar2(20)     NOT NULL,
          add_on_code                     varchar2(10)     NOT NULL,
          add_on_qty                      number(7)        NOT NULL,
          funeral_banner_txt              varchar2(200),
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT order_add_ons_PK PRIMARY KEY(order_add_on_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT order_add_ons_FK1 FOREIGN KEY(external_order_number) REFERENCES joe.order_details(external_order_number),
    CONSTRAINT order_add_ons_FK2 FOREIGN KEY(add_on_code) REFERENCES ftd_apps.addon(addon_id))
 TABLESPACE joe_DATA;


create index joe.order_add_ons_n1 on joe.order_add_ons(external_order_number) tablespace joe_indx;
create index joe.order_add_ons_n2 on joe.order_add_ons(add_on_code) tablespace joe_indx;


CREATE SEQUENCE JOE.ORDER_ADD_ON_ID_SQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;
  
  
CREATE TABLE JOE.ORDER_BILLING_INFO (
          billing_info_id                 number           NOT NULL,
          master_order_number             varchar2(100)    NOT NULL,
          billing_info_name               varchar2(1000)   NOT NULL,
          billing_info_data               varchar2(1000)   NOT NULL,
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT order_billing_info_PK PRIMARY KEY(billing_info_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT order_billing_info_FK1 FOREIGN KEY(master_order_number) REFERENCES joe.orders(master_order_number))
 TABLESPACE joe_DATA;
  
create index joe.order_billing_info_n1 on joe.order_billing_info(master_order_number) tablespace joe_indx;

CREATE SEQUENCE JOE.BILLING_INFO_ID_SQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;


CREATE TABLE JOE.CALL_TIME_HISTORY (
          call_time_id                    number           NOT NULL,
          dnis_id                         number(4)        NOT NULL,
          csr_identity_id                 varchar2(255)    NOT NULL,
          master_order_number             varchar2(100),
          start_datetime                  date             NOT NULL,
          end_datetime                    date             NOT NULL,
          created_by                      varchar2(100)    not null,
          created_on                      date             not null,
          updated_by                      varchar2(100)    not null,
          updated_on                      date             not null,
    CONSTRAINT call_time_history_PK PRIMARY KEY(call_time_id) 
            USING INDEX  TABLESPACE joe_INDX,
    CONSTRAINT call_time_history_FK1 FOREIGN KEY(dnis_id) REFERENCES ftd_apps.dnis(dnis_id),
    CONSTRAINT call_time_history_FK2 FOREIGN KEY(csr_identity_id) REFERENCES aas.identity(identity_id),
    CONSTRAINT call_time_history_FK3 FOREIGN KEY(master_order_number) REFERENCES joe.orders(master_order_number))
 TABLESPACE joe_DATA;

create index joe.call_time_history_n1 on joe.call_time_history(dnis_id) tablespace joe_indx;
create index joe.call_time_history_n2 on joe.call_time_history(csr_identity_id) tablespace joe_indx;
create index joe.call_time_history_n3 on joe.call_time_history(master_order_number) tablespace joe_indx;

CREATE SEQUENCE JOE.call_time_ID_SQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 1000
  NOORDER;




grant execute on joe.oe_maint_pkg to osp;
grant select on joe.orders to ops$oracle;
grant select on joe.order_details to ops$oracle;
grant select on ftd_apps.company_master to joe;
grant execute on global.encryption to joe;
grant select on clean.order_details to joe;
grant select on ftd_apps.aaa_members to joe;
grant select on ftd_apps.cost_Centers to joe;
grant select on ftd_apps.program_reward to joe;
grant select on ftd_apps.partner_program to joe;
grant select on ftd_apps.calculation_basis_val to joe;


DELETE FROM JOE.ELEMENT_CONFIG;
set define off

--IOTW live feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'liveFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'liveFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator production web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
250,--TAB_SEQ
250,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW content feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'contentFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'contentFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator content web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
300,--TAB_SEQ
300,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW test feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'testFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'testFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator test web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
350,--TAB_SEQ
350,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW uat feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'uatFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'uatFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator test web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
400,--TAB_SEQ
400,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW remove selected button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'removeSelectedButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
450,--TAB_SEQ
450,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW add button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'addButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
500,--TAB_SEQ
500,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW send all button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'sendFeedAllButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
550,--TAB_SEQ
550,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW exit button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'exitButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
600,--TAB_SEQ
600,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);
--*****************END IOTW LIST PAGE**************************


--*****************START IOTW EDIT PAGE**************************
--IOTW Program Source Codes
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'sourceCodes',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'sourceCodesLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'Y',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
'You must enter at least one program source code',--ERROR_TXT
'For multiple source codes, enter space/line delimited data',--TITLE_TXT
90,--VALUE_MAX_BYTES_QTY
650,--TAB_SEQ
650,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program Product Ids
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'productIds',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'productIdsLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'Y',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
'You must enter at least one product id',--ERROR_TXT
'For multiple products, enter space/line delimited data',--TITLE_TXT
90,--VALUE_MAX_BYTES_QTY
700,--TAB_SEQ
700,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Source Code
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'iotwSourceCode',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'iotwSourceCodeLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'Y',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
'You must enter an IOTW source code',--ERROR_TXT
'IOTW Source Code',--TITLE_TXT
10,--VALUE_MAX_BYTES_QTY
750,--TAB_SEQ
750,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program start date
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'startDate',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'startDateLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'Y',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
'You must enter a program start date',--ERROR_TXT
'Program start date',--TITLE_TXT
10,--VALUE_MAX_BYTES_QTY
800,--TAB_SEQ
-1,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program start date calendar widget
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'startDateCalButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
850,--TAB_SEQ
850,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program end date
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'endDate',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'endDateLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Program end date',--TITLE_TXT
10,--VALUE_MAX_BYTES_QTY
900,--TAB_SEQ
-1,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program end date calendar widget
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'endDateCalButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
950,--TAB_SEQ
950,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Delivery dates
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'deliveryDates',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'deliveryDatesLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Promotion is restricted to the following delivery dates.  No dates indicates that the promotion does not require delivery on a specific date',--TITLE_TXT
1000,--VALUE_MAX_BYTES_QTY
1000,--TAB_SEQ
-1,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW Program delivery date calendar widget
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'deliveryDatesCalButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1050,--TAB_SEQ
1050,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW special offer
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'specialOffer',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'specialOfferLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Indicates a special offer',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1100,--TAB_SEQ
1100,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW wording color
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'wordingColor',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'wordingColorLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Novator wording color',--TITLE_TXT
32,--VALUE_MAX_BYTES_QTY
1150,--TAB_SEQ
1150,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW product page messaging
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'productPageMessage',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'productPageMessageLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Product page messaging',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1200,--TAB_SEQ
1200,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW calendar widget messaging
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'calendarMessage',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'calendarMessageLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Novator calendar widget messaging',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1250,--TAB_SEQ
1250,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW view errors button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'viewErrorsButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1300,--TAB_SEQ
1300,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW live feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'liveEditFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'liveEditFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator production web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1350,--TAB_SEQ
1350,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW content feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'contentEditFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'contentEditFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator content web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1400,--TAB_SEQ
1400,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW test feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'testEditFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'testEditFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator test web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1450,--TAB_SEQ
1450,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW uat feed checkbox
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'uatEditFeed',--ELEMENT_ID
'HEADER',--LOCATION_DESC
'uatEditFeedLabel',--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
'Send updates to Novator test web site',--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1500,--TAB_SEQ
1500,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW save button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'saveButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1550,--TAB_SEQ
1550,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW reset button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'resetButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1600,--TAB_SEQ
1600,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW remove button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'removeButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1650,--TAB_SEQ
1650,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

--IOTW back button
insert into JOE.ELEMENT_CONFIG (APP_CODE, ELEMENT_ID, LOCATION_DESC, LABEL_ELEMENT_ID, COUNT_ELEMENT_ID, REQUIRED_FLAG, REQUIRED_COND_DESC,
REQUIRED_COND_TYPE_NAME, VALIDATE_FLAG, VALIDATE_EXP, VALIDATE_EXP_TYPE_NAME, ERROR_TXT, TITLE_TXT, VALUE_MAX_BYTES_QTY, TAB_SEQ, TAB_INIT_SEQ,
TAB_COND_DESC, TAB_COND_TYPE_NAME, ACCORDION_INDEX_NUM, TAB_CTRL_INDEX_NUM, RECALC_FLAG, PRODUCT_VALIDATE_FLAG, ACCESS_KEY_NAME, XML_NODE_NAME,
CALC_XML_FLAG)
values (
'IOTW',--APP_CODE
'backButton',--ELEMENT_ID
'HEADER',--LOCATION_DESC
null,--LABEL_ELEMENT_ID
null,--COUNT_ELEMENT_ID
'N',--REQUIRED_FLAG
null,--REQUIRED_COND_DESC
null,--REQUIRED_COND_TYPE_NAME
'N',--VALIDATE_FLAG
null,--VALIDATE_EXP
null,--VALIDATE_EXP_TYPE_NAME
null,--ERROR_TXT
null,--TITLE_TXT
1,--VALUE_MAX_BYTES_QTY
1700,--TAB_SEQ
1700,--TAB_INIT_SEQ
null,--TAB_COND_DESC
null,--TAB_COND_TYPE_NAME
1,--ACCORDION_INDEX_NUM
-1,--TAB_CTRL_INDEX_NUM
'N',--RECALC_FLAG
'N',--PRODUCT_VALIDATE_FLAG
null,--ACCESS_KEY_NAME
null,--XML_NODE_NAME for the order feed
'N'--CALC_XML_FLAG
);

INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientType','DETAIL','recipientTypeLabel',null,'Y',null,null,'N',null,null,'You must select a location type','Recipient type',null,'2440','2440',null,null,'0','2','N','N',null,'ship-to-type','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientBusiness','DETAIL','recipientBusinessLabel',null,'Y','JOE_REQUIRED.recipientBusiness','FUNCTION','N',null,null,'You must enter the business name','Recipient business name','50','2450','2450',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientFacilitySearch','DETAIL','recipientFacilitySearchLabel',null,'N',null,null,'N',null,null,null,'Press to open the facility search',null,'2460','-1',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientRoom','DETAIL','recipientRoomLabel',null,'N',null,null,'N',null,null,null,'Area in the business where the product should be delivered to','20','2470','2470',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientFirstName','DETAIL','recipientFirstNameLabel',null,'Y',null,null,'N',null,null,'You must enter the recipient''s first name','Recipient first name','25','2480','2480',null,null,'0','2','N','N',null,'recip-first-name','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientLastName','DETAIL','recipientLastNameLabel',null,'Y',null,null,'N',null,null,'You must enter the recipient''s last name','Recipient last name','25','2490','2490',null,null,'0','2','N','N',null,'recip-last-name','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientAddress','DETAIL','recipientAddressLabel','recipientAddressCount','Y',null,null,'Y','JOE_VALIDATE.recipientAddress','FUNCTION','You must enter the recipient''s address.  It cannot be a PO, APO, or FPO.','Recipient address','90','2500','2500',null,null,'0','2','N','N',null,'recip-address1','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientZip','DETAIL','recipientZipLabel',null,'Y','JOE_REQUIRED.recipientZip','FUNCTION','Y','JOE_VALIDATE.recipientZip','FUNCTION','Recipient zip/postal code required.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters allowed.','Recipient zip/postal code.  US zips must be in the format of "NNNNN".  Canadian zips must be in the format of "ANA" or "ANANAN".  No special characters allowed.','6','2510','2510',null,null,'0','2','N','N',null,'recip-postal-code','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientCountry','DETAIL','recipientCountryLabel',null,'Y',null,null,'N',null,null,'You must select the recipient country','Recipient country',null,'2520','-1',null,null,'0','2','N','N',null,'recip-country','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientCity','DETAIL','recipientCityLabel',null,'Y',null,null,'N',null,null,'You must enter the recipient''s city','Recipient city','30','2530','2530',null,null,'0','2','N','N',null,'recip-city','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientState','DETAIL','recipientStateLabel',null,'Y','JOE_REQUIRED.recipientState','FUNCTION','N',null,null,'You must select the recipient state (US & CA only)','Recipient state',null,'2540','2540',null,null,'0','2','N','N',null,'recip-state','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientHoursFrom','DETAIL','recipientHoursFromLabel',null,'N',null,null,'N',null,null,null,'Start of the time period',null,'2550','2550',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientHoursTo','DETAIL','recipientHoursToLabel',null,'N',null,null,'N',null,null,null,'End of the time period',null,'2560','2560',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','orderComments','DETAIL','orderCommentsLabel','orderCommentsCount','N',null,null,'N',null,null,null,'Enter general comments here.  These comments will not be seen by a florist or vendor.','160','2570','-1',null,null,'0','2','N','N','o','order-comments','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','avResultCode','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2580','-1',null,null,'0','2','N','N',null,'qms-result-code','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientPhoneExt','DETAIL','recipientPhoneExtLabel',null,'N',null,null,'N',null,null,null,'Recipient phone number extension (numbers only)','10','2420','-1',null,null,'0','2','N','N',null,'recip-phone-ext','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientPhoneSearch','DETAIL','recipientPhoneSearchLabel',null,'N',null,null,'N',null,null,null,'Press to search for a recipient',null,'2430','2430',null,null,'0','2','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','cardTypeButton','DETAIL',null,null,'N',null,null,'N',null,null,null,'Move text to the card message at the cursor location',null,'2365','2365',null,null,'0','1','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','iotwFlag','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2035','-1',null,null,'0','0','N','N',null,'item-of-the-week-flag','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataSourceCodePwdButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to validate the source code password',null,'1070','1070',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','membershipId','HEADER','membershipIdLabel',null,'Y','JOE_REQUIRED.membershipId','FUNCTION','N',null,null,'Partner requires membership information','Partner membership id','100','3170','3170',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','membershipSkip','HEADER','membershipSkipLabel',null,'N',null,null,'N',null,null,null,'Skip entry of membership number.  The order will be placed in queue until the customer provides this information.',null,'3171','3171',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','membershipFirstName','HEADER','membershipFirstNameLabel',null,'N',null,null,'N',null,null,null,'First name on the membership account','100','3172','3172',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','membershipLastName','HEADER','membershipLastNameLabel',null,'N',null,null,'N',null,null,null,'Last name on the membership account','100','3173','3173',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientZipSearch','DETAIL',null,null,'N',null,null,'N',null,null,null,'Press to perform a zip look-up for the recipient',null,'2515','2515',null,null,'0','2','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','pagetop','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'995','995',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tab1bottom','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2355','2355',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tab2top','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2357','2357',null,null,'0','1','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tab2bottom','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2403','2403',null,null,'0','1','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tab3top','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2405','2405',null,null,'0','2','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tab3bottom','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2585','2585',null,null,'0','2','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','billingtop','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3000','3000',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','billingbottom','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3575','3575',null,null,'1',null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','confExitButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to exit order entry',null,'3630','3630',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','confNewOrderButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to create a new order for a new customer',null,'3620','3620',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','confNewOrderSameCustButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to create a new order for the same customer',null,'3610','3610',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryOptGR','DETAIL','carrierDeliveryOptGRLabel',null,'Y','JOE_REQUIRED.isCarrierDelivery','FUNCTION','Y','JOE_VALIDATE.carrierDeliveryOptions','FUNCTION','You must select a delivery option','Standard delivery',null,'2290','2290',null,null,'0','0','Y','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyRadioSameSame','HEADER','copyRadioSameSameLabel',null,'N',null,null,'N',null,null,null,'Press to create a new order with the same recipient and product',null,'6000','6000',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryDateSA','DETAIL',null,null,'N',null,null,'N',null,null,'You must select a delivery date option','Available Saturday delivery dates',null,'2325','-1',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryDateGR','DETAIL',null,null,'N',null,null,'N',null,null,'You must select a delivery date option','Available standard delivery dates',null,'2295','-1',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryDate2F','DETAIL',null,null,'N',null,null,'N',null,null,'You must select a delivery date option','Available 2-day delivery dates',null,'2305','-1',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryDateND','DETAIL',null,null,'N',null,null,'N',null,null,'You must select a delivery date option','Available next day delivery dates',null,'2315','-1',null,null,'0','0','N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyRadioSameDiff','HEADER','copyRadioSameDiffLabel',null,'N',null,null,'N',null,null,null,'Press to create a new order with the same product but a different recipient',null,'6010','6010',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyRadioDiffSame','HEADER','copyRadioDiffSameLabel',null,'N',null,null,'N',null,null,null,'Press to create a new order with the same recipient but a different product',null,'6020','6020',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyRadioDiffDiff','HEADER','copyRadioDiffDiffLabel',null,'N',null,null,'N',null,null,null,'Press to create a new order with a different product and recipient',null,'6030','6030',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyOrderButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to create a new order based on the selection above',null,'6040','6040',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','copyCancelButton','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to cancel creation of a new order in this shopping cart',null,'6050','6050',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchDeliveryDate','SEARCH','productSearchDeliveryDateLabel',null,'N',null,null,'N',null,null,null,'Requested delivery date (may be blank)',null,'7060','7060',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchCalendarButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Open the delivery date calendar',null,'7070','7070',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchZip','SEARCH','productSearchZipLabel',null,'N',null,null,'N',null,null,null,'Filter your results to only the listed zip (may be blank)',null,'7080','7080',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchZipSearch','SEARCH','productSearchZipSearchLabel',null,'N',null,null,'N',null,null,null,'Open the zip code search',null,'7090','7090',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchCountryCombo','SEARCH','productSearchCountryComboLabel',null,'N',null,null,'N',null,null,null,'Country to be delivered to',null,'7100','-1',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchGoButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Start the product search',null,'7110','7110',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchDisabledButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Unable to stop the search at this time',null,'7120','7120',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchStopButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Stop the current search',null,'7130','7130',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psRecentlyViewedLink','SEARCH',null,null,'N',null,null,'N',null,null,null,'Show the recently viewed products',null,'7140','-1',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopSellersLink','SEARCH',null,null,'N',null,null,'N',null,null,null,'Show the most popular purchases',null,'7150','-1',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchCloseButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Close product search',null,'7160','7160',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchSortCombo','SEARCH',null,null,'N',null,null,'N',null,null,null,'Change the sort order',null,'7170','7170',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psNavBeginning','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the first page of search results',null,'7180','7180',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psNavPrevious','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the previous page of search results',null,'7190','7190',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psNavNext','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the next page of search results',null,'7200','7200',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psNavEnd','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the last page of search results',null,'7210','7210',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psDetailCloseButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Close the product detail panel',null,'7220','7220',null,null,null,null,'N','N','e',null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productDetailSelectButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Select the displayed item for purchase',null,'7230','7230',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psSuggestCloseButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Close the suggested products panel',null,'7240','7240',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopCloseButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Close the top products panel',null,'7250','7250',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopNavBeginning','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the first page of search results',null,'7260','7260',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopNavPrevious','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the previous page of search results',null,'7270','7270',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearchKeywords','SEARCH','productSearchKeywordsLabel',null,'N',null,null,'N',null,null,null,'Filter your search by entering words to describe the product you want to find (may be blank)','50','7050','7050',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopNavNext','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the next page of search results',null,'7280','7280',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','psTopNavEnd','SEARCH',null,null,'N',null,null,'N',null,null,null,'Go to the last page of search results',null,'7290','7290',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tszCity','SEARCH','tszCityLabel',null,'N',null,null,'N',null,null,null,'City to search on','30','7550','7550',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tszState','SEARCH','tszStateLabel',null,'N',null,null,'N',null,null,null,'State to search on',null,'7560','7560',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tszCountry','SEARCH',null,null,'N',null,null,'N',null,null,null,'Country to search on ',null,'7570','-1',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tssSearchType','SEARCH','tssSearchTypeLabel',null,'N',null,null,'N',null,null,null,'Search type',null,'7580','7580',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tssKeyword','SEARCH','tssKeywordLabel',null,'N',null,null,'N',null,null,null,'Filter on the entered key words','50','7590','7590',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tspPhone','SEARCH','tspPhoneLabel',null,'N',null,null,'N',null,null,null,'Phone number to search on','20','7600','7600',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsfSearchType','SEARCH','tsfSearchTypeLabel',null,'N',null,null,'N',null,null,null,'Type of search',null,'7610','7610',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsfKeyword','SEARCH','tsfKeywordLabel',null,'N',null,null,'N',null,null,null,'Filter on the entered key words','50','7620','7620',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsfCity','SEARCH','tsfCityLabel',null,'N',null,null,'N',null,null,null,'City to search on','30','7630','7630',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsfCountry','SEARCH',null,null,'N',null,null,'N',null,null,null,'Country to search on',null,'7640','-1',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsflZip','SEARCH','tsflZipLabel',null,'N',null,null,'N',null,null,null,'Zip code to search on','6','7650','7650',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsflZipSearch','SEARCH',null,null,'N',null,null,'N',null,null,null,'Open zip code search',null,'7660','7660',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','textSearchGoButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Start the search',null,'7670','7670',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','textSearchDisabledButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Unable to stop the search at this time',null,'7680','7680',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','textSearchStopButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Stop the search',null,'7690','7690',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','textSearchProcessing','SEARCH',null,null,'N',null,null,'N',null,null,null,'Processing your search request',null,'7700','7700',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','tsCloseButton','SEARCH',null,null,'N',null,null,'N',null,null,null,'Close the search panel',null,'7710','7710',null,null,null,null,'N','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataDNIS','HEADER','callDataDNISLabel',null,'Y',null,null,'N',null,null,'DNIS is invalid','DNIS showing on your phone','4','1000','1000',null,null,null,null,'Y','Y',null,'dnis-id','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','actionButtonExit','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to exit order entry',null,'3579','3579',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataOrigin','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'1020','-1',null,null,null,null,'N','N',null,'origin','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataCompany','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'1030','-1',null,null,null,null,'N','N',null,'company','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataSourceCode','HEADER','callDataSourceCodeLabel',null,'Y',null,null,'Y','^[A-Z0-9]{1,6}$','REGEX','Invalid source code','Source code','6','1040','1040',null,null,null,null,'Y','Y','s','source-code','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataSourceCodeSearch','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to search for a source code',null,'1050','1050',null,null,null,null,'N','N','l',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','callDataSourceCodePwd','HEADER',null,null,'N',null,null,'N',null,null,'Invalid password','This source code requires a password','60','1060','1060',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerRecipSameCheckbox','HEADER','customerRecipSameCheckboxLabel',null,'N',null,null,'N',null,null,null,'Is the customer information the same as one of the entered recipients?',null,'3005','3005',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerRecipCombo','HEADER','customerRecipComboLabel',null,'N',null,null,'N',null,null,'You must select a recipient','Select a recipient',null,'3010','3010',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerBillingPhone','HEADER','customerBillingPhoneLabel',null,'Y',null,null,'Y','JOE_VALIDATE.customerPhoneNumber','FUNCTION','You enter the customer''s billing phone number.  Domestic phone number must be 10 digits.  International can be up to 20 digits.','Customer''s billing phone number.  Domestic phone number must be 10 digits.  International can be up to 20 digits.','20','3020','3020',null,null,'1',null,'N','N',null,'buyer-daytime-phone','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerBillingPhoneExt','HEADER','customerBillingPhoneExtLabel',null,'N',null,null,'N',null,null,'Invalid entry (numbers only)','Primary phone number extension (numbers only)','10','3030','-1',null,null,'1',null,'N','N',null,'buyer-work-ext','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerPhoneSearch','HEADER','customerPhoneSearchLabel',null,'N',null,null,'N',null,null,null,'Press to search for a customer by phone number',null,'3040','-1',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerEveningPhone','HEADER','customerEveningPhoneLabel',null,'N',null,null,'Y','JOE_VALIDATE.customerPhoneNumber','FUNCTION','Optional field but if you enter the customer''s evening phone number, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.','Optional field, but, if you enter the customer''s evening phone number, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.','20','3050','-1',null,null,'1',null,'N','N',null,'buyer-evening-phone','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerEveningPhoneExt','HEADER','customerEveningPhoneExtLabel',null,'N',null,null,'N',null,null,'Invalid entry (numbers only)','Evening phone extension (numbers only)','10','3060','-1',null,null,'1',null,'N','N',null,'buyer-evening-ext','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerFirstName','HEADER','customerFirstNameLabel',null,'Y',null,null,'N',null,null,'You must enter the customer''s first name','First name of the person you are talking to','25','3070','3070',null,null,'1',null,'N','N',null,'buyer-first-name','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerLastName','HEADER','customerLastNameLabel',null,'Y',null,null,'N',null,null,'You must enter the customer''s last name','Last name of the persion you are talking to','25','3080','3080',null,null,'1',null,'N','N',null,'buyer-last-name','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerAddress','HEADER','customerAddressLabel','customerAddressCount','Y',null,null,'N',null,null,'You must enter the customer''s address ','Address of the person you are talking to','90','3090','3090',null,null,'1',null,'N','N',null,'buyer-address1','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerZip','HEADER','customerZipLabel',null,'Y','JOE_REQUIRED.customerZip','FUNCTION','Y','JOE_VALIDATE.customerZip','FUNCTION','Customer zip/postal code required.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters allowed.','Customer zip/postal code.  US zips must be in the format of "NNNNN".  Canadian zips must be in the format of "ANA" or "ANANAN".  No special characters allowed.','6','3100','3100',null,null,'1',null,'N','N',null,'buyer-postal-code','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerZipSearch','HEADER','customerZipSearchLabel',null,'N',null,null,'N',null,null,null,'Press to search for a zip/postal code',null,'3110','-1',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerCountryCombo','HEADER','customerCountryComboLabel',null,'Y',null,null,'N',null,null,'You must select the customer''s country','Select the country for the person you are talking to',null,'3120','-1',null,null,'1',null,'N','N',null,'buyer-country','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerCity','HEADER','customerCityLabel',null,'Y',null,null,'N',null,null,'You must enter the customer''s city','Country of the person you are talking to','30','3130','3130',null,null,'1',null,'N','N',null,'buyer-city','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerStateCombo','HEADER','customerStateComboLabel',null,'Y','JOE_REQUIRED.customerState','FUNCTION','N',null,null,'You must select the customer''s state (US & CA only)','Select the state of the person you are talking to',null,'3140','3140',null,null,'1',null,'N','N',null,'buyer-state','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerEmailAddress','HEADER','customerEmailAddressLabel',null,'N',null,null,'Y','^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$','REGEX','Invalid email address','Email address of the person you are talking to','40','3150','3150',null,null,'1',null,'N','N',null,'buyer-email-address','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','customerSubscribeCheckbox','HEADER','customerSubscribeCheckboxLabel',null,'N',null,null,'N',null,null,null,'Check to subscribe this customer to receive promotional emails',null,'3160','3160',null,null,'1',null,'N','N',null,'news-letter-flag','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentTypeCombo','HEADER','paymentTypeComboLabel',null,'Y',null,null,'N',null,null,'You must select a payment type','Select the payment type',null,'3210','3210',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentFraud','HEADER','paymentFraudLabel',null,'N',null,null,'N',null,null,null,'Check if you think this may be a fraudulent order',null,'3220','-1',null,null,'1',null,'N','N',null,'fraud-flag','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentGCAmount','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3230','-1',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentGCNumber','HEADER','paymentGCNumberLabel',null,'Y','JOE_REQUIRED.paymentGC','FUNCTION','N',null,null,'You must enter a certificate/coupon number','Gift certficate/coupon number','25','3240','3240',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentGCBalanceCombo','HEADER','paymentGCBalanceComboLabel',null,'Y','JOE_REQUIRED.paymentGCBalance','FUNCTION','N',null,null,'You must select the payment method that will be used to process the balance of the amount due','Select the payment method that will be used to process the balance of the amount due',null,'3250','3250',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCAmount','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3260','-1',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCNumber','HEADER','paymentCCNumberLabel',null,'Y','JOE_REQUIRED.paymentCC','FUNCTION','N',null,null,'You must enter a credit card number','Enter the credit card number without dashes','16','3270','3270',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCTypeCombo','HEADER','paymentCCTypeComboLabel',null,'Y','JOE_REQUIRED.paymentCC','FUNCTION','N',null,null,'You must enter a credit card type','Select the type of credit card',null,'3280','3280',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCExpMonthCombo','HEADER','paymentCCExpMonthComboLabel',null,'Y','JOE_REQUIRED.paymentCCExp','FUNCTION','N',null,null,'You must select an expiration month','Select the expiration date',null,'3290','3290',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCExpYear','HEADER','paymentCCExpMonthComboLabel',null,'Y','JOE_REQUIRED.paymentCCExp','FUNCTION','N',null,null,'You must select an expiration year','Select the expiration date',null,'3300','3300',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCManualAuth','HEADER','paymentCCManualAuthLabel',null,'Y','JOE_REQUIRED.paymentCCManual','FUNCTION','N',null,null,'You must enter the manual authorization number','Enter the authorization number you received from the issuer','20','3310','3310',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCApprovalVerbage','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3320','-1',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCApprovalActionCode','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3330','-1',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCAVSResult','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3340','-1',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentCCAcqData','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3350','-1',null,null,null,null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentNCType','HEADER','paymentNCTypeLabel',null,'Y','JOE_REQUIRED.paymentNC','FUNCTION','N',null,null,'You must select a no charge type','Select the no charge type',null,'3360','3360',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentNCReason','HEADER','paymentNCReasonLabel',null,'Y','JOE_REQUIRED.paymentNC','FUNCTION','N',null,null,'You must select a no charge reason','Select the no charge reason',null,'3370','3370',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentNCRefOrder','HEADER','paymentNCRefOrderLabel',null,'Y','JOE_REQUIRED.paymentNCOrderNumber','FUNCTION','N',null,null,'You must enter a reference order number','Enter the order number this no charge relates to','20','3380','3380',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentNCAuthId','HEADER','paymentNCAuthIdLabel',null,'Y','JOE_REQUIRED.paymentNC','FUNCTION','N',null,null,'You must enter an approving manager user id','Enter the authorizing user id','20','3390','3390',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','paymentNCAuthPwd','HEADER','paymentNCAuthPwdLabel',null,'Y','JOE_REQUIRED.paymentNC','FUNCTION','N',null,null,'Invalid password','Enter the authorizing user password','20','3400','3400',null,null,'1',null,'N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','billingButtonSubmit','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to process this shopping cart',null,'3570','3570',null,null,null,null,'N','N','u',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','actionButtonAbandon','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to abandon this shopping cart',null,'3580','3580',null,null,null,null,'N','N','a',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','confMasterOrderNumber','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3590','-1',null,null,null,null,'N','N',null,'master-order-number','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','cartOrdersInCart','HEADER',null,null,'N',null,null,'N',null,null,null,null,null,'3600','-1',null,null,null,null,'N','N',null,'order-count','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productId','DETAIL','productIdLabel',null,'Y',null,null,'Y','^[A-Z0-9\-]{1,10}$','REGEX','You must provide a valid product id','Product id','10','2000','2000',null,null,'0','0','Y','Y',null,'productid','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSearch','DETAIL','productSearchLabel',null,'N',null,null,'N',null,null,null,'Product Search',null,'2010','2010',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productSourceCode','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2020','-1',null,null,'0','0','N','N',null,'item-source-code','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','externalOrderNumber','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2030','-1',null,null,'0','0','N','N',null,'order-number','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productImage','DETAIL','productImageLabel',null,'N',null,null,'N',null,null,null,null,null,'2040','-1',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOccasion','DETAIL','productOccasionLabel',null,'Y',null,null,'N',null,null,'You must select an occasion','Select the occasion',null,'2050','2050',null,null,'0','0','N','N',null,'occassion','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productZip','DETAIL','productZipLabel',null,'Y','JOE_REQUIRED.productZipCode','FUNCTION','Y','JOE_VALIDATE.productZipCode','FUNCTION','You must enter a zip/postal code','Zip/Postal code',null,'2060','2060',null,null,'0','0','Y','Y',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productZipSearch','DETAIL','productZipSearchLabel',null,'N',null,null,'N',null,null,null,'Press to search for a zip code',null,'2070','2070',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productCountry','DETAIL','productCountryLabel',null,'Y',null,null,'N',null,null,'You must select a country','Country that item will be delivered to',null,'2080','-1',null,null,'0','0','Y','Y',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productDeliveryDate','DETAIL','productDeliveryDateLabel',null,'Y',null,null,'N',null,null,'You must select a delivery date','Select a delivery date or press on the calendar.',null,'2090','2090',null,null,'0','0','Y','Y',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productCalendarButton','DETAIL','productCalendarButtonLabel',null,'N',null,null,'N',null,null,null,'Press to select a delivery date that is not in the drop down.',null,'2100','2100',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionStandard','DETAIL','productOptionStandardLabel',null,'Y',null,null,'Y','JOE_VALIDATE.productOptions','FUNCTION','You must select a product option','Standard price',null,'2110','2110',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionPremium','DETAIL','productOptionPremiumLabel',null,'Y',null,null,'Y','JOE_VALIDATE.productOptions','FUNCTION','You must select a product option','Premium price',null,'2130','2130',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionDeluxe','DETAIL','productOptionDeluxeLabel',null,'Y',null,null,'Y','JOE_VALIDATE.productOptions','FUNCTION','You must select a product option','Deluxe price',null,'2120','2120',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionVariable','DETAIL','productOptionVariableLabel',null,'Y',null,null,'Y','JOE_VALIDATE.productOptions','FUNCTION','You must select a product option','Variable price (specify the price to the right)',null,'2140','2140',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionVariablePrice','DETAIL',null,null,'Y','JOE_REQUIRED.productOptionVariablePrice','FUNCTION','Y','JOE_VALIDATE.productOptionVariablePrice','FUNCTION','You must specify a price between the standard and maximum price (inclusive) in the format of 999.99','Price >= standard and <= maximum','6','2150','2150',null,null,'0','0','Y','Y',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionSubCode','DETAIL','productOptionSubCodeLabel',null,'N',null,null,'N',null,null,'You must select a product option','Select a product option',null,'2160','2160',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionUpsell','DETAIL','productOptionUpsellLabel',null,'N',null,null,'N',null,null,'You must select a product option','Select a product option',null,'2170','2170',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionColor','DETAIL','productOptionColorLabel',null,'Y','JOE_REQUIRED.productColor','FUNCTION','N',null,null,'You must select a color','Select a color',null,'2180','2180',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','productOptionColor2','DETAIL','productOptionColor2Label',null,'Y','JOE_REQUIRED.productColor','FUNCTION','N',null,null,'You must select a color','Select a color',null,'2190','2190',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnACheckbox','DETAIL','addOnACheckboxLabel',null,'N',null,null,'N',null,null,null,'Check to purchase the addon',null,'2200','-1',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnAQty','DETAIL','addOnAQtyLabel',null,'Y','JOE_REQUIRED.addons','FUNCTION','Y','^[123456789]$','REGEX','You must specify a quantity > 0','Enter a quantity between 1 - 9','1','2210','2210',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnBCheckbox','DETAIL','addOnBCheckboxLabel',null,'N',null,null,'N',null,null,null,'Check to purchase the addon',null,'2220','-1',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnBQty','DETAIL','addOnBQtyLabel',null,'Y','JOE_REQUIRED.addons','FUNCTION','Y','^[123456789]$','REGEX','You must specify a quantity > 0','Enter a quantity between 1 - 9','1','2230','2230',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnCCheckbox','DETAIL','addOnCCheckboxLabel',null,'N',null,null,'N',null,null,null,'Check to purchase the addon',null,'2240','-1',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnCQty','DETAIL','addOnCQtyLabel',null,'Y','JOE_REQUIRED.addons','FUNCTION','Y','^[123456789]$','REGEX','You must specify a quantity > 0','Enter a quantity between 1 - 9','1','2250','2250',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnFCheckbox','DETAIL','addOnFCheckboxLabel',null,'N',null,null,'N',null,null,null,'Check to purchase the addon',null,'2260','-1',null,null,'0','0','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnFBannerText','DETAIL','addOnFBannerTextLabel',null,'Y','JOE_REQUIRED.addons','FUNCTION','Y','^[a-zA-Z0-9 &]{1,30}$','REGEX','You must include the funeral banner text which can only contain numbers, characters, spaces and the & character.  i.e. "Mother & Grandmother"','Enter the funeral banner text','30','2270','2270',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnFQty','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,'2280','-1',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryOpt2F','DETAIL','carrierDeliveryOpt2FLabel',null,'Y','JOE_REQUIRED.isCarrierDelivery','FUNCTION','Y','JOE_VALIDATE.carrierDeliveryOptions','FUNCTION','You must select a delivery option','Two day delivery',null,'2300','2300',null,null,'0','0','Y','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryOptND','DETAIL','carrierDeliveryOptNDLabel',null,'Y','JOE_REQUIRED.isCarrierDelivery','FUNCTION','Y','JOE_VALIDATE.carrierDeliveryOptions','FUNCTION','You must select a delivery option','Next day delivery',null,'2310','2310',null,null,'0','0','Y','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','carrierDeliveryOptSA','DETAIL','carrierDeliveryOptSALabel',null,'Y','JOE_REQUIRED.isCarrierDelivery','FUNCTION','Y','JOE_VALIDATE.carrierDeliveryOptions','FUNCTION','You must select a delivery option','Saturday delivery',null,'2320','2320',null,null,'0','0','Y','N',null,null,'N');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','floristId','DETAIL','floristIdLabel',null,'Y','JOE_REQUIRED.floristData','FUNCTION','Y','^\d\d-\d\d\d\d[A-Z][A-Z]$','REGEX','You must specify a florist id in the format of NN-NNNNAA','Florist id',null,'2330','-1',null,null,'0','0','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','floristSearch','DETAIL','floristSearchLabel',null,'N',null,null,'N',null,null,null,'Press to search for a florist',null,'2340','-1',null,null,'0','0','N','N','f',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','floristComments ','DETAIL','floristCommentsLabel','floristCommentsCount','Y','JOE_REQUIRED.floristData','FUNCTION','N',null,null,'You must provide florist comments for custom products','Comments that will be forwarded on to the florist with the order','160','2350','-1',null,null,'0','0','N','N','m',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','cardTypeList ','DETAIL','cardTypeListLabel',null,'N',null,null,'N',null,null,null,'Card greetings',null,'2360','2360',null,null,'0','1','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','cardMessage','DETAIL','cardMessageLabel','cardMessageCount','Y',null,null,'N',null,null,'You must enter a card message','Card message','240','2370','2370',null,null,'0','1','N','N',null,'card-message','Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','cardSpellCheckButton','DETAIL','cardSpellCheckButtonLabel',null,'N',null,null,'N',null,null,null,'Press to spell check the card message',null,'2380','-1',null,null,'0','1','N','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnCardCheckbox','DETAIL','addOnCardCheckboxLabel',null,'N',null,null,'N',null,null,null,'Press to purchase a full-size greeting card',null,'2390','-1',null,null,'0','1','Y','N','g',null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','addOnCardSelect','DETAIL','addOnCardSelectLabel',null,'Y','JOE_REQUIRED.addonCard','FUNCTION','N',null,null,'You must select a greeting card','Select a full-size greeting card',null,'2400','2400',null,null,'0','1','Y','N',null,null,'Y');
INSERT INTO JOE.ELEMENT_CONFIG VALUES('JOE','recipientPhone','DETAIL','recipientPhoneLabel',null,'Y',null,null,'Y','JOE_VALIDATE.recipientPhoneNumber','FUNCTION','You must enter the recipient''s phone number (numbers only).  Domestic recipient phone number must be 10 digits.  International can be up to 20 digits.','Recipient''s phone number (numbers only).  Domestic recipient phone number must be 10 digits.  International can be up to 20 digits.','20','2410','2410',null,null,'0','2','N','N',null,'recip-phone','Y');
set define on

grant select on scrub.fraud_order_dispositions to joe;
grant execute on global.md5_hash to joe;
grant execute on ftd_apps.sp_get_product_by_id_2247 to osp;
grant select on ftd_apps.product_master to osp;
create synonym osp.product_master for ftd_apps.product_master;
grant execute on ftd_apps.oe_get_upsell_details to osp;






----------------------------
-- QA - PRODUCTION VALUES FOLLOW ONLY!!
----------------------------
-- consumer-qa
set define off
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'MAX_DAYS', '20', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'MAX_DAYS_BACK', '2', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'PAS_URL', 'http://consumer-qa.ftdi.com/pasquery/services/ProductAvailability', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE', 'facility.txt', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE_DIR', '/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE_SERVER', '192.168.141.73', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_LOCAL_DIR', '/u02', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_LOCAL_FILE', 'facility.txt', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_REINDEX_URL', 'http://consumer-qa.ftdi.com/textsearchfacilityserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_SEARCH_URL', 'http://consumer-qa.ftdi.com/textsearchfacility/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_REINDEX_URL', 'http://consumer-qa.ftdi.com/textsearchiotwserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_SEARCH_URL', 'http://consumer-qa.ftdi.com/textsearchiotw/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'LAST_USED_DAYS', '30', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_REINDEX_URL', 'http://consumer-qa.ftdi.com/textsearchproductserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_SEARCH_URL', 'http://consumer-qa.ftdi.com/textsearchproduct/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_REINDEX_URL', 'http://consumer-qa.ftdi.com/textsearchsourcecodeserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_SEARCH_URL', 'http://consumer-qa.ftdi.com/textsearchsourcecode/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'CLIENT_LIST', 'iron1.ftdi.com iron3.ftdi.com', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'CLIENT_URL_LIST', 'iron1.ftdi.com:7778 iron3.ftdi.com:7778', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_FUZZY_SEARCH', '0.3', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_ip', '10.3.10.52', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_port', '1037', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'feed_timeout', '180000', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_allowed', 'NO', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_ip', '10.5.16.51', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_port', '1035', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_ip', '10.3.10.52', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_port', '1035', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_ip', '10.5.16.51', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_port', '1036', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
----------------------------
-- END QA SECTION
----------------------------



----------------------------
-- Production ONLY!!
----------------------------
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'MAX_DAYS', '120', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'MAX_DAYS_BACK', '2', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'PAS_CONFIG', 'PAS_URL', 'http://consumer.ftdi.com/pasquery/services/ProductAvailability', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE', 'facility.txt', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE_DIR', '/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FILE_SERVER', '192.168.141.73', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_LOCAL_DIR', '/u02', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_LOCAL_FILE', 'facility.txt', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_REINDEX_URL', 'http://consumer.ftdi.com/textsearchfacilityserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_SEARCH_URL', 'http://consumer.ftdi.com/textsearchfacility/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_REINDEX_URL', 'http://consumer.ftdi.com/textsearchiotwserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_SEARCH_URL', 'http://consumer.ftdi.com/textsearchiotw/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'LAST_USED_DAYS', '30', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_REINDEX_URL', 'http://consumer.ftdi.com/textsearchproductserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_SEARCH_URL', 'http://consumer.ftdi.com/textsearchproduct/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_REINDEX_URL', 'http://consumer.ftdi.com/textsearchsourcecodeserver/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_SEARCH_URL', 'http://consumer.ftdi.com/textsearchsourcecode/', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'CLIENT_LIST', 'gold.ftdi.com silver.ftdi.com platinum.ftdi.com', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'CLIENT_URL_LIST', 'gold.ftdi.com:7778 silver.ftdi.com:7778 platinum.ftdi.com:7778', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'FACILITY_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'PRODUCT_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'IOTW_FUZZY_SEARCH', '0.5', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'TEXT_SEARCH', 'SOURCE_CODE_FUZZY_SEARCH', '0.3', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);

INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_ip', '10.3.10.52', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'content_feed_port', '1037', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'feed_timeout', '180000', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_ip', '10.5.16.51', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'production_feed_port', '1035', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_ip', '10.3.10.52', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'test_feed_port', '1035', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_allowed', 'YES', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_ip', '10.5.16.51', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);
INSERT INTO FRP.GLOBAL_PARMS ( GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
  VALUES ( 'NOVATOR_CONFIG', 'uat_feed_port', '1036', 'RLSE_3_0_0', SYSDATE, 'RLSE_3_0_0', SYSDATE);

----------------------------
-- END PRODUCTION SECTION
----------------------------
set define on


select global.encryption.encrypt_it('Hello',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')) from dual;

-----------------------------------
-- consumer-qa
-- THESE USE ENCRYPTION - RUN AS OPS$ORACLE
-----------------------------------
INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name) 
  VALUES ('text_search', 
          'FACILITY_FILE_LOGIN', 
          global.encryption.encrypt_it('ftdftp',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')), 
          'Login for Facility file', 
          sysdate, 'RLSE_3_0_0', sysdate, 'RLSE_3_0_0',
          frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));
INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name) 
  VALUES ('text_search', 'FACILITY_FILE_PASSWORD', global.encryption.encrypt_it('ftdftp',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')), 
    'Password for Facility file', sysdate, 'RLSE_3_0_0', sysdate, 'RLSE_3_0_0',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));

----------------------------
-- END QA SECTION
----------------------------





-----------------------------------
-- Production
-- THESE USE ENCRYPTION - RUN AS OPS$ORACLE
-----------------------------------

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name) 
  VALUES ('text_search', 
          'FACILITY_FILE_LOGIN', 
          global.encryption.encrypt_it('ftdftp',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')), 
          'Login for Facility file', 
          sysdate, 'RLSE_3_0_0', sysdate, 'RLSE_3_0_0',
          frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));
INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name) 
  VALUES ('text_search', 
          'FACILITY_FILE_PASSWORD', 
          global.encryption.encrypt_it('ftdftp',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')), 
          'Password for Facility file', 
          sysdate, 'RLSE_3_0_0', sysdate, 'RLSE_3_0_0',
          frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));
----------------------------
-- END PRODUCTION SECTION
----------------------------








insert into joe.oe_script_master values (
    'callDataDNIS', 'Default', 'Enter the DNIS number that shows on your phone display',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'callDataSourceCode', 'Default', 'Thank you for calling (Company).  This is (Rep Name) . May I take your order?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'callDataSourceCodeSearch', 'Default', 'How did you hear about us today?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'productId', 'Default', 'Do you know the item number of the product you would like to order?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'productOccasion', 'Default', 'What is the occasion for this purchase?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'productZipCode', 'Default', 'What is the recipient''s zip code?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'productZipCodeLookup', 'Default', 'What is the recipient''s city and state?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'productDeliveryDate', 'Default', 'What date would you like to have the product delivered?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'cardTypeList', 'Default', 'How would you like the card message to read?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'cardMessage', 'Default', 'How would you like the card message to read?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'addOnCardCheckbox', 'Default', 'Would you like to add a full sized greeting card?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'recipientPhone', 'Default', 'May I have the recipient''s phone number please?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'recipientFirstName', 'Default', 'May I have the recipient''s first and last name?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'recipientAddress', 'Default', 'May I have the recipient''s address?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'recipientZip', 'Default', 'May I have the recipient''s zip code?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'customerPhone', 'Default', 'What is the best number to reach you?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'customerFirstName', 'Default', 'May I have your name and billing address please?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'customerEmailAddress', 'Default', 'May I have your email address?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'customerSubscribeCheckbox', 'Default', 'Would you like to receive notification of promotions and special discounts via email?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'paymentCCNumber', 'Default', 'May I have your credit card number?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'paymentCCExpMonthCombo', 'Default', 'Expiration date?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);

insert into joe.oe_script_master values (
    'paymentCCExpYear', 'Default', 'Expiration date?',
    'Joe initial load', sysdate, 'JOE initial load', sysdate);



----------------------------------------------------------------------
-- QA ONLY INSERTS - SEE FOLLOWING SECTION FOR PROD
----------------------------------------------------------------------
insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'CALL_WARNING_TIME',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds before warning message is displayed');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'CALL_CRITICAL_TIME',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds before critical message is displayed');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'WARNING_DROPDOWN_SECONDS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that all dropdown messages will appear');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'SOURCE_CODE_SEARCH_HISTORY_DAYS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of days in the past that a source code is considered active for the search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'DELIVERY_DATE_DROPDOWN_DAYS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of days to appear in the delivery date dropdown field');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULTS_DISPLAY',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of product search results to display on a page');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULTS_TOTAL',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Total number of product search results to be returned');

INSERT INTO FRP.GLOBAL_PARMS p
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULT_ROWS',
    '2',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of rows to display on a page in product search');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULT_COLUMNS',
    '5',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of columns to display on a page in product search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'FACILITY_SEARCH_TOTAL_RESULTS',
    '25',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of results to return from the PAS facility search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'ADDON_CHOCOLATES_AVAILABLE',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Global chocolate availability flag');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'DELIVERY_DAYS_OUT_MAX',
    '120',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The maximum number of days in the future for an allowable delivery date');

INSERT INTO FRP.GLOBAL_PARMS
  VALUES
  ('OE_CONFIG',
    'SKIP_PHONE_NUMBER_VAL_REGEX',
    '(0000000000|9999999999)',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Regular expression used to determine if phone number validation should be skipped');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'SEARCH_CANCEL_DELAY_SECONDS',
    '5',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of seconds before a user can cancel a search request');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'NOTIFY_PANEL_HIDE_SECONDS',
    '10',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of seconds before the notify panel will automatically close');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'RECALC_ORDER_SECONDS',
    '5',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that JOE client will wait before checking to see if the cart should be recalculated.');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'IMAGE_SERVER_HTTP_URL',
    'http://consumer-test.ftdi.com/product_images',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The url prefix used to retrieve product images');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'IMAGE_SERVER_URL',
    'https://consumer-test.ftdi.com/product_images',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The secure url prefix used to retrieve product images');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'ORDER_GATHERER_URL',
    'http://zinc3.ftdi.com:7778/OG/servlet/OrderGatherer',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Order Gatherer URL used for JOE order transmissions');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'ORDER_SEND_RETRY_MAX',
    '3',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of attempts to send JOE order to order gatherer');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'MAIN_MENU_URL',
    'http://consumer-test.ftdi.com/secadmin/security/Main.do',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Landing spot when leaving a joe application.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'CHECK_AVAILABILITY_SECONDS',
    '5','Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that JOE client will wait before checking to see if the product should be checked for availability.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'SECURE_URL_PREFIX',
    'https://consumer-test.ftdi.com/joe',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'URL prefix for making secure AJAX requests.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'COM_SEARCH_URL',
    'http://consumer-test.ftdi.com/customerordermanagement/customerOrderSearch.do',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'URL to redirect user to the order just placed in COM.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerName1',
    'addrver1.ftdi.com',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server #1');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerName2',
    'addrver2.ftdi.com',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server #2');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerPort1',
    '4660',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server port #1');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerPort2',
    '4660',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server port #2');

update ftd_apps.dnis set origin = 'FTDP'
  where origin in ('FTDNF', 'WEST');

insert into scrub.fraud_order_dispositions
  (fraud_id, fraud_description)
  values
  ('OE', 'CSR indicated Fraud during Order Entry');

insert into scrub.fraud_order_dispositions
  (fraud_id, fraud_description)
  values
  ('LI', 'High number of line items');

delete from ftd_apps.state_master
  where state_master_id = 'UK';
--------------------------------------------------------------
-- END QA ONLY SECTION
--------------------------------------------------------------

--------------------------------------------------------------
-- BEGIN PROD ONLY SECTION
--------------------------------------------------------------
insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'CALL_WARNING_TIME',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds before warning message is displayed');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'CALL_CRITICAL_TIME',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds before critical message is displayed');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'WARNING_DROPDOWN_SECONDS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that all dropdown messages will appear');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'SOURCE_CODE_SEARCH_HISTORY_DAYS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of days in the past that a source code is considered active for the search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'DELIVERY_DATE_DROPDOWN_DAYS',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of days to appear in the delivery date dropdown field');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULTS_DISPLAY',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of product search results to display on a page');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULTS_TOTAL',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Total number of product search results to be returned');

INSERT INTO FRP.GLOBAL_PARMS p
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULT_ROWS',
    '2',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of rows to display on a page in product search');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'PRODUCT_SEARCH_RESULT_COLUMNS',
    '5',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of columns to display on a page in product search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'FACILITY_SEARCH_TOTAL_RESULTS',
    '25',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of results to return from the PAS facility search');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'ADDON_CHOCOLATES_AVAILABLE',
    '300',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Global chocolate availability flag');

insert into frp.global_parms
  (context, name, value, created_by, created_on, updated_by, updated_on, description)
  values
  ('OE_CONFIG',
    'DELIVERY_DAYS_OUT_MAX',
    '120',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The maximum number of days in the future for an allowable delivery date');

INSERT INTO FRP.GLOBAL_PARMS
  VALUES
  ('OE_CONFIG',
    'SKIP_PHONE_NUMBER_VAL_REGEX',
    '(0000000000|9999999999)',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Regular expression used to determine if phone number validation should be skipped');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'SEARCH_CANCEL_DELAY_SECONDS',
    '5',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of seconds before a user can cancel a search request');

INSERT INTO FRP.GLOBAL_PARMS p 
  (p.CONTEXT, p.NAME, p.VALUE, p.CREATED_BY, p.CREATED_ON, p.UPDATED_BY, p.UPDATED_ON, p.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'NOTIFY_PANEL_HIDE_SECONDS',
    '10',
    'Initial JOE load',
    SYSDATE,
    'Initial JOE load',
    SYSDATE,
    'Number of seconds before the notify panel will automatically close');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'RECALC_ORDER_SECONDS',
    '5',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that JOE client will wait before checking to see if the cart should be recalculated.');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'IMAGE_SERVER_HTTP_URL',
    'http://consumer.ftdi.com/product_images',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The url prefix used to retrieve product images');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'IMAGE_SERVER_URL',
    'https://consumer.ftdi.com/product_images',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'The secure url prefix used to retrieve product images');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'ORDER_GATHERER_URL',
    'http://zinc3.ftdi.com:7778/OG/servlet/OrderGatherer',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Order Gatherer URL used for JOE order transmissions');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'ORDER_SEND_RETRY_MAX',
    '3',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of attempts to send JOE order to order gatherer');

INSERT INTO FRP.GLOBAL_PARMS g 
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION) 
  VALUES
  ('OE_CONFIG',
    'MAIN_MENU_URL',
    'http://consumer.ftdi.com/secadmin/security/Main.do',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Landing spot when leaving a joe application.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'CHECK_AVAILABILITY_SECONDS',
    '5','Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'Number of seconds that JOE client will wait before checking to see if the product should be checked for availability.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'SECURE_URL_PREFIX',
    'https://consumer.ftdi.com/joe',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'URL prefix for making secure AJAX requests.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('OE_CONFIG',
    'COM_SEARCH_URL',
    'http://consumer.ftdi.com/customerordermanagement/customerOrderSearch.do',
    'Initial JOE load',
    sysdate,
    'Initial JOE load',
    sysdate,
    'URL to redirect user to the order just placed in COM.');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerName1',
    'addrver1.ftdi.com',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server #1');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerName2',
    'addrver2.ftdi.com',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server #2');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerPort1',
    '4660',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server port #1');

INSERT INTO FRP.GLOBAL_PARMS g
  (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES
  ('QMS_CONFIG',
    'qmsServerPort2',
    '4660',
    'Defect XXXX',
    sysdate,
    'Defect XXXX',
    sysdate,
    'QMS address server port #2');

update ftd_apps.dnis set origin = 'FTDP'
  where origin in ('FTDNF', 'WEST');

insert into scrub.fraud_order_dispositions
  (fraud_id, fraud_description)
  values
  ('OE', 'CSR indicated Fraud during Order Entry');

insert into scrub.fraud_order_dispositions
  (fraud_id, fraud_description)
  values
  ('LI', 'High number of line items');

delete from ftd_apps.state_master
  where state_master_id = 'UK';

--------------------------------------------------------------
-- END PROD ONLY SECTION
--------------------------------------------------------------


commit;






--- SET JOE, MAINTENANCE, AND WEBOE ACCESS ------------------------
-------------------------------------------------------------------

--------------
--Create configuration parameters
----
---QA VALUE
insert into frp.global_parms gp values('CUSTOMER_ORDER_MANAGEMENT', 'JOE_URL', 'https://consumer-test.ftdi.com/joe/oe.do', 'SYS', sysdate, 'SYS', sysdate, 'JOE URL');


----
-- PROD VALUE
insert into frp.global_parms gp values('CUSTOMER_ORDER_MANAGEMENT', 'JOE_URL', 'https://consumer.ftdi.com/joe/oe.do', 'SYS', sysdate, 'SYS', sysdate, 'JOE URL');

-- END TARGET SPECFIC VALUES
----------


--Create resources for OE access and maintenance screens
insert into aas.resources values('OldWebOE','Order Proc','Determines if the user has access to WebOE.  WebOE is the old order entry system.',sysdate,sysdate,'SYS');
insert into aas.resources values('Item Of The Week Maint Link','Order Proc','Determines if the user has access to the Item of the Week maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Product Favorites Maint Link','Order Proc','Determines if the user has access to the Product Favorites maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Cross Sell Maint Link','Order Proc','Determines if the user has access to the Cross Sell maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Card Message Maint Link','Order Proc','Determines if the user has access to the Card Message maintenance screen.',sysdate,sysdate,'SYS');

--Create resource group for OE access
insert into aas.acl values('OldWebOE',sysdate,sysdate,'SYS',null);

--Assign resources to resource groups for OE access and maintenance screens
insert into aas.rel_acl_rp values ('OldWebOE','OldWebOE','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Operations','Item Of The Week Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Director','Product Favorites Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Merchandising','Cross Sell Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Director','Card Message Maint Link','Order Proc','View',sysdate,sysdate,'SYS');

--Assign resource group to role for OE access
insert into aas.rel_role_acl values ((select role_id from aas.role where role_name='CSRLevel B'), 'OldWebOE',sysdate,sysdate,'SYS');

/*Delete security assignments related to OE access and maintenance screens if needed
delete from aas.rel_acl_rp where resource_id = 'OldWebOE';
delete from aas.rel_role_acl where acl_name = 'OldWebOE';
delete from aas.acl where acl_name = 'OldWebOE';
delete from aas.rel_acl_rp where resource_id = 'Item Of The Week Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Product Favorites Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Cross Sell Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Card Message Maint Link';
delete from aas.resources where resource_id = 'OldWebOE';
delete from aas.resources where resource_id = 'Item Of The Week Maint Link';
delete from aas.resources where resource_id = 'Product Favorites Maint Link';
delete from aas.resources where resource_id = 'Cross Sell Maint Link';
delete from aas.resources where resource_id = 'Card Message Maint Link';
*/

--- END SET JOE, MAINTENANCE, AND WEBOE ACCESS --------------------
-------------------------------------------------------------------

--- Set minimum authorization amount for all credit cards ---------
-------------------------------------------------------------------

UPDATE ftd_apps.payment_methods pm SET pm.min_auth_amt = .25 WHERE pm.payment_method_id IN ('MS', 'DC', 'DI', 'MC', 'VI', 'CB', 'JP');
UPDATE ftd_apps.payment_methods pm SET pm.min_auth_amt = 0 WHERE pm.payment_method_id = 'AX';

--- END Set minimum authorization amount for all credit cards -----
-------------------------------------------------------------------


commit;

grant select on ftd_apps.price_header to joe;
grant select on ftd_apps.price_header_details to joe;


update JOE.OE_SCRIPT_MASTER s set s.SCRIPT_ID = 'customerBillingPhone' where s.SCRIPT_ID = 'customerPhone';
update JOE.OE_SCRIPT_MASTER s set s.SCRIPT_ID = 'productZip' where s.SCRIPT_ID = 'productZipCode';

alter table ftd_apps.source_master modify(description varchar2(80));
alter table ftd_apps.source_master$ modify(description varchar2(80));
alter table ftd_apps.source_master_reserve modify(description varchar2(80));
alter table ftd_apps.source_master_reserve$ modify(description varchar2(80));
alter table ftd_apps.source_master_reserve_stage modify(description varchar2(80));


update JOE.ELEMENT_CONFIG e set e.ERROR_TXT = 'Recipient zip/postal code required.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters are allowed.', e.TITLE_TXT = 'Recipient zip/postal code.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters allowed.' where e.APP_CODE = 'JOE' and e.ELEMENT_ID = 'recipientZip';

update JOE.ELEMENT_CONFIG e set e.ERROR_TXT = 'Customer zip/postal code required.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters are allowed.', e.TITLE_TXT = 'Customer zip/postal code.  US zips must be in the format of "NNNNN".  Canadian postal codes must be in the format of "ANA" or "ANANAN".  No special characters allowed.' where e.APP_CODE = 'JOE' and e.ELEMENT_ID = 'customerZip';

INSERT INTO QUARTZ_SCHEMA.GROUPS g (g.GROUP_ID, g.DESCRIPTION, g.ACTIVE_FLAG) VALUES('JOE','JOE','Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE p (p.PIPELINE_ID, p.GROUP_ID, p.DESCRIPTION, p.TOOL_TIP, p.DEFAULT_SCHEDULE, p.DEFAULT_PAYLOAD, p.FORCE_DEFAULT_PAYLOAD, p.QUEUE_NAME, p.CLASS_CODE, p.ACTIVE_FLAG) VALUES('SQL','JOE','SQL Jobs','Run a SQL job and optionally send an email notification',null,null,'N',null,'SQLJob','Y');


INSERT INTO FRP.GLOBAL_PARMS g (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES('OE_CONFIG','CALL_CRITICAL_MESSAGE','','Initial JOE load',sysdate,'Initial JOE load',sysdate,'Message to display if time has elapsed');

INSERT INTO FRP.GLOBAL_PARMS g (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
  VALUES('OE_CONFIG','CALL_WARNING_MESSAGE','','Initial JOE load',sysdate,'Initial JOE load',sysdate,'Message to display if time has elapsed');      


update JOE.ELEMENT_CONFIG e set e.TITLE_TXT = 'Master order number' where e.APP_CODE='JOE' and e.ELEMENT_ID = 'confMasterOrderNumber';
update JOE.ELEMENT_CONFIG e set e.PRODUCT_VALIDATE_FLAG='N' where e.APP_CODE='JOE' and e.ELEMENT_ID in ('callDataDNIS','productOptionVariablePrice');
update JOE.ELEMENT_CONFIG e set e.ACCORDION_INDEX_NUM=1 where e.APP_CODE='JOE' and e.ELEMENT_ID='billingButtonSubmit';


INSERT INTO FRP.GLOBAL_PARMS (
        CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, 
        UPDATED_BY, UPDATED_ON, DESCRIPTION
    ) VALUES (
        'PARTNER_REWARD_CONFIG', 'VIRGINAIRWAYS_OUTBOUND_SERVER', 
        'hawk.ftdi.com', 'SYS', sysdate, 'SYS', sysdate, 
        'Outbound FTP server for Virgin Airways'
    );

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE,
                                   DESCRIPTION, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, key_name)
    VALUES('partner_reward','VIRGINAIRWAYS_OutboundFtpUser',global.encryption.encrypt_it('reports',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
               'Outbound ftp user for Virgin Airways', 'SYS',sysdate,'SYS',sysdate,frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE,
                                   DESCRIPTION, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, key_name)
    VALUES('partner_reward','VIRGINAIRWAYS_OutboundFtpPswd',global.encryption.encrypt_it('reports',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
               'Outbound ftp password for Virgin Airways', 'SYS',sysdate,'SYS',sysdate,frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));    



UPDATE FRP.GLOBAL_PARMS g set g.VALUE='300' where g.CONTEXT='OE_CONFIG' and g.NAME='CALL_CRITICAL_TIME';

UPDATE FRP.GLOBAL_PARMS g set g.VALUE='360' where g.CONTEXT='OE_CONFIG' and g.NAME='CALL_WARNING_TIME';

update ftd_apps.payment_methods set joe_flag = 'Y' where payment_method_id = 'PC';


CREATE TABLE JOE.IOTW_DELIVERY_DISCOUNT_DATES$ (
           iotw_id                      number(11)       NOT NULL,
           discount_date                date             NOT NULL,
           created_by                   varchar2(100)    not null,
           created_on                   date             not null,
           updated_by                   varchar2(100)    not null,
           updated_on                   date             not null,
           OPERATION$                   VARCHAR2(7)      not null,
           TIMESTAMP$                   DATE             not null)
TABLESPACE joe_DATA;

update JOE.ELEMENT_CONFIG e 
set e.REQUIRED_FLAG = 'Y', 
    e.REQUIRED_COND_DESC = 'JOE_REQUIRED.customerEmailAddress', 
    e.REQUIRED_COND_TYPE_NAME='FUNCTION',
    e.ERROR_TXT = 'A valid email address is required if the customer is "opted in" to receive promotional emails.  If you do not see the the "Subscription Opt In" checkbox, it''s because they have already opted in.'
WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='customerEmailAddress';

update JOE.ELEMENT_CONFIG e set e.VALUE_MAX_BYTES_QTY = 55 WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='membershipId'; 

update JOE.ELEMENT_CONFIG e set e.VALUE_MAX_BYTES_QTY = 25 WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='membershipFirstName'; 

update JOE.ELEMENT_CONFIG e set e.VALUE_MAX_BYTES_QTY = 25 WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='membershipLastName';

update JOE.ELEMENT_CONFIG e set e.VALUE_MAX_BYTES_QTY = 6 WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='productZip';


alter table venus.zj_trip_vendor_order drop constraint zj_trip_vendor_order_pk;

alter table venus.zj_trip_vendor_order add (CONSTRAINT zj_trip_vendor_order_pk 
        PRIMARY KEY(trip_id,vendor_id,order_detail_id,venus_id));


insert into JOE.ELEMENT_CONFIG e (e.APP_CODE, e.ELEMENT_ID, e.LOCATION_DESC, e.LABEL_ELEMENT_ID, e.COUNT_ELEMENT_ID, e.REQUIRED_FLAG, e.REQUIRED_COND_DESC, e.REQUIRED_COND_TYPE_NAME, e.VALIDATE_FLAG, e.VALIDATE_EXP, e.VALIDATE_EXP_TYPE_NAME, e.ERROR_TXT, e.TITLE_TXT, e.VALUE_MAX_BYTES_QTY, e.TAB_SEQ, e.TAB_INIT_SEQ, e.TAB_COND_DESC, e.TAB_COND_TYPE_NAME, e.ACCORDION_INDEX_NUM, e.TAB_CTRL_INDEX_NUM, e.RECALC_FLAG, e.PRODUCT_VALIDATE_FLAG, e.ACCESS_KEY_NAME, e.XML_NODE_NAME, e.CALC_XML_FLAG)
VALUES('JOE','avIgnoreButton','DETAIL',null,null,'N',null,null,'N',null,null,null,'Press to ignore address verification suggestion',null,2575,2575,null,null,0,2,'N','N',null,null,'N');

insert into JOE.ELEMENT_CONFIG e (e.APP_CODE, e.ELEMENT_ID, e.LOCATION_DESC, e.LABEL_ELEMENT_ID, e.COUNT_ELEMENT_ID, e.REQUIRED_FLAG, e.REQUIRED_COND_DESC, e.REQUIRED_COND_TYPE_NAME, e.VALIDATE_FLAG, e.VALIDATE_EXP, e.VALIDATE_EXP_TYPE_NAME, e.ERROR_TXT, e.TITLE_TXT, e.VALUE_MAX_BYTES_QTY, e.TAB_SEQ, e.TAB_INIT_SEQ, e.TAB_COND_DESC, e.TAB_COND_TYPE_NAME, e.ACCORDION_INDEX_NUM, e.TAB_CTRL_INDEX_NUM, e.RECALC_FLAG, e.PRODUCT_VALIDATE_FLAG, e.ACCESS_KEY_NAME, e.XML_NODE_NAME, e.CALC_XML_FLAG)
VALUES('JOE','avAcceptButton','DETAIL',null,null,'N',null,null,'N',null,null,null,'Press to accept address verification suggestion',null,2576,2576,null,null,0,2,'N','N',null,null,'N');


GRANT EXECUTE ON POP.GET_GLOBAL_PARMS TO RPT;

update JOE.OE_SCRIPT_MASTER s set s.SCRIPT_TXT='May I please have the expiration date of your credit card?' where s.SCRIPT_ID='paymentCCExpMonthCombo' or s.SCRIPT_ID='paymentCCExpYear';

update JOE.ADDRESS_TYPE a set a.PROMPT_FOR_HOURS_FLAG = 'Y', a.HOURS_LABEL_TXT='Hours' where a.ADDRESS_TYPE_CODE='HOSPITAL';

INSERT INTO FRP.GLOBAL_PARMS g (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON, g.DESCRIPTION)
VALUES ('OE_CONFIG','VALIDATE_ADDRESS','Y','Initial JOE load',sysdate,'Initial JOE load',sysdate,'Should validations be performed on recipient addresses? Y=yes anything else = no');

INSERT INTO JOE.ELEMENT_CONFIG e
(e.APP_CODE, e.ELEMENT_ID, e.LOCATION_DESC, e.LABEL_ELEMENT_ID, e.COUNT_ELEMENT_ID, e.REQUIRED_FLAG, e.REQUIRED_COND_DESC, e.REQUIRED_COND_TYPE_NAME, e.VALIDATE_FLAG, e.VALIDATE_EXP, e.VALIDATE_EXP_TYPE_NAME, e.ERROR_TXT, e.TITLE_TXT, e.VALUE_MAX_BYTES_QTY, e.TAB_SEQ, e.TAB_INIT_SEQ, e.TAB_COND_DESC, e.TAB_COND_TYPE_NAME, e.ACCORDION_INDEX_NUM, e.TAB_CTRL_INDEX_NUM, e.RECALC_FLAG, e.PRODUCT_VALIDATE_FLAG, e.ACCESS_KEY_NAME, e.XML_NODE_NAME, e.CALC_XML_FLAG)
VALUES('JOE','tsfState','SEARCH','tsfStateLabel',null,'N',null,null,'N',null,null,null,'State to search on',null,7635,7635,null,null,null,null,'N','N',null,null,'N');

update JOE.ELEMENT_CONFIG e set e.ERROR_TXT='You must enter the customer''s billing phone number.  Domestic phone number must be 10 digits.  International can be up to 20 digits.'
where e.APP_CODE='JOE' and e.ELEMENT_ID='customerBillingPhone';

INSERT INTO JOE.ELEMENT_CONFIG e
(e.APP_CODE, e.ELEMENT_ID, e.LOCATION_DESC, e.LABEL_ELEMENT_ID, e.COUNT_ELEMENT_ID, e.REQUIRED_FLAG, e.REQUIRED_COND_DESC, e.REQUIRED_COND_TYPE_NAME, e.VALIDATE_FLAG, e.VALIDATE_EXP, e.VALIDATE_EXP_TYPE_NAME, e.ERROR_TXT, e.TITLE_TXT, e.VALUE_MAX_BYTES_QTY, e.TAB_SEQ, e.TAB_INIT_SEQ, e.TAB_COND_DESC, e.TAB_COND_TYPE_NAME, e.ACCORDION_INDEX_NUM, e.TAB_CTRL_INDEX_NUM, e.RECALC_FLAG, e.PRODUCT_VALIDATE_FLAG, e.ACCESS_KEY_NAME, e.XML_NODE_NAME, e.CALC_XML_FLAG)
VALUES('JOE','productAvailable','DETAIL',null,null,'N',null,null,'N',null,null,null,null,null,2036,-1,null,null,null,null,'N','N',null,null,'N');

UPDATE JOE.ELEMENT_CONFIG e set e.XML_NODE_NAME = null WHERE e.APP_CODE='JOE' and e.ELEMENT_ID='cartOrdersInCart';

UPDATE JOE.ADDRESS_TYPE a set a.ADDRESS_TYPE_CODE = 'FUNERAL HOME' where a.ADDRESS_TYPE_CODE='FUNERAL_HOME';

UPDATE JOE.ADDRESS_TYPE a set a.ADDRESS_TYPE_CODE = 'NURSING HOME' where a.ADDRESS_TYPE_CODE='NURSING_HOME';

update JOE.ELEMENT_CONFIG e set e.VALIDATE_EXP_TYPE_NAME = 'FUNCTION', e.VALIDATE_EXP='JOE_VALIDATE.addOnQuantity' where e.APP_CODE='JOE' and e.ELEMENT_ID in ('addOnAQty','addOnBQty','addOnCQty');

update JOE.ELEMENT_CONFIG e set e.RECALC_FLAG = 'Y' where e.APP_CODE='JOE' and e.ELEMENT_ID='productSourceCode';

update JOE.ELEMENT_CONFIG e set e.CALC_XML_FLAG = 'N' where e.APP_CODE='JOE' and e.ELEMENT_ID='billingButtonSubmit';

insert into JOE.ELEMENT_CONFIG e (e.APP_CODE, e.ELEMENT_ID, e.LOCATION_DESC, e.LABEL_ELEMENT_ID, e.COUNT_ELEMENT_ID, e.REQUIRED_FLAG, e.REQUIRED_COND_DESC, e.REQUIRED_COND_TYPE_NAME, e.VALIDATE_FLAG, e.VALIDATE_EXP, e.VALIDATE_EXP_TYPE_NAME, e.ERROR_TXT, e.TITLE_TXT, e.VALUE_MAX_BYTES_QTY, e.TAB_SEQ, e.TAB_INIT_SEQ, e.TAB_COND_DESC, e.TAB_COND_TYPE_NAME, e.ACCORDION_INDEX_NUM, e.TAB_CTRL_INDEX_NUM, e.RECALC_FLAG, e.PRODUCT_VALIDATE_FLAG, e.ACCESS_KEY_NAME, e.XML_NODE_NAME, e.CALC_XML_FLAG)
values('JOE','billingButtonCalculate','HEADER',null,null,'N',null,null,'N',null,null,null,'Press to recalculate the shopping cart',null,3560,-1,null,null,1,null,'N','N',null,null,'N');



grant execute on scrub.misc_pkg to joe;

update JOE.ELEMENT_CONFIG e set e.XML_NODE_NAME=null where e.APP_CODE='JOE' and e.ELEMENT_ID='customerSubscribeCheckbox';

INSERT INTO QUARTZ_SCHEMA.GROUPS g (g.GROUP_ID, g.DESCRIPTION, g.ACTIVE_FLAG) VALUES('ACCOUNTING','Accounting','Y');

INSERT INTO QUARTZ_SCHEMA.PIPELINE p (p.PIPELINE_ID, p.GROUP_ID, p.DESCRIPTION, p.TOOL_TIP, p.DEFAULT_SCHEDULE, p.DEFAULT_PAYLOAD, p.FORCE_DEFAULT_PAYLOAD, p.QUEUE_NAME, p.CLASS_CODE, p.ACTIVE_FLAG)
VALUES('EOM SQL','ACCOUNTING','EOM SQL jobs','SQL job to run as part of EOM',null,null,'N',null,'SQLJob','Y');


alter table ftd_apps.vendor_product$ modify (timestamp$ timestamp);
drop index ftd_apps.PRODUCT_CHANGE$_ACCT_N2;  
alter table ftd_apps.product_change$_acct modify (timestamp$ timestamp);
create index ftd_apps.PRODUCT_CHANGE$_ACCT_N2 on ftd_apps.PRODUCT_CHANGE$_ACCT(timestamp$) tablespace ftd_apps_indx;

































---- END FIX 3.0.0 RELEASE SCRIPT -------------------
