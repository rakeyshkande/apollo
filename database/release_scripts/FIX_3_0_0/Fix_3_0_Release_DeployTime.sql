---- FIX 3.0.0 RELEASE SCRIPT ------------------

--************************************************************
--** DEPLOYTIME SCRIPT
--************************************************************

ALTER TABLE FTD_APPS.OCCASION ADD (
        DEFAULT_OCCASION_INDICATOR      VARCHAR2(1)         DEFAULT NULL   NULL, 
    CONSTRAINT OCCASION_CK3      CHECK(DEFAULT_OCCASION_INDICATOR IN('Y',NULL))); 

COMMENT ON COLUMN FTD_APPS.OCCASION.DEFAULT_OCCASION_INDICATOR IS 'Denotes which occasion is the current default one - only one Y in the table';
     
CREATE UNIQUE INDEX FTD_APPS.OCCASION_UK1 ON FTD_APPS.OCCASION  (DEFAULT_OCCASION_INDICATOR)  
     TABLESPACE FTD_APPS_INDX;


alter table ftd_apps.product_master add (
                         popularity_order_cnt number(11) DEFAULT 0 NOT NULL,
                         custom_flag          VARCHAR2(1) DEFAULT 'N' NOT NULL,
                         keywords_txt         varchar2(4000),
                 CONSTRAINT PRODUCT_MASTER_CK7      CHECK(CUSTOM_FLAG IN('N','Y')));

alter table ftd_apps.product_master$ add (popularity_order_cnt number(11),
                                          custom_flag          varchar2(1),
                                          keywords_txt         varchar2(4000));

COMMENT ON COLUMN ftd_apps.product_master.popularity_order_cnt is 'Stores count of orders over a time period';
COMMENT ON COLUMN ftd_apps.product_master.custom_flag is 'Denotes whether a product is custom.  Should have only one Y in the table, enforced by the application.';



alter table ftd_apps.source_master  add (last_used_on_order_date    date  default sysdate);
COMMENT ON COLUMN ftd_apps.source_master.last_used_on_order_date is 'Stores last time the corresponding source was used on an order';

alter table ftd_apps.source_master$ add (last_used_on_order_date    date);


alter table scrub.order_details add (qms_result_code   varchar2(20) );
COMMENT ON COLUMN scrub.order_details.qms_result_code is 'QMS Result Code';


alter TABLE scrub.payments add (
      nc_approval_identity_id	varchar2(255),
      nc_type_code              varchar2(50), 
      nc_reason_code            varchar2(50),
      nc_order_detail_id        number(38),
    CONSTRAINT payments_CK1  CHECK(nc_type_Code IN('COMP','RESEND','FLOWERS_MONTHLY')), 
    CONSTRAINT payments_CK2  CHECK(nc_reason_Code IN('NONDELIVERY','NOSCAN','LATE','OTHER','QUALITY','SUBSTITUTION')), 
    CONSTRAINT payments_FK1 FOREIGN KEY(nc_approval_identity_id)  REFERENCES aas.identity(identity_id),
    CONSTRAINT payments_FK2 FOREIGN KEY(nc_order_detail_id) REFERENCES scrub.order_details(order_detail_id)
    );

comment on column scrub.payments.nc_approval_identity_id is 'User that approved the no charge';
comment on column scrub.payments.nc_type_code is 'Type of no charge ';
comment on column scrub.payments.nc_reason_code is 'Reason for no charge';
comment on column scrub.payments.nc_order_detail_id is 'Original order number associated with the no charge';

create index scrub.payments_n6 on scrub.payments(nc_order_detail_id) tablespace op_indx compute statistics;


alter table clean.order_details add (qms_result_code   varchar2(20) );
COMMENT ON COLUMN clean.order_details.qms_result_code is 'QMS Result Code';

alter TABLE clean.payments add (
      nc_approval_identity_id	varchar2(255),
      nc_type_code              varchar2(50), 
      nc_reason_code            varchar2(50),
      nc_order_detail_id        number,
    CONSTRAINT payments_CK2  CHECK(nc_type_Code IN('COMP','RESEND','FLOWERS_MONTHLY')), 
    CONSTRAINT payments_CK3  CHECK(nc_reason_Code IN('NONDELIVERY','NOSCAN','LATE','OTHER','QUALITY','SUBSTITUTION')), 
    CONSTRAINT payments_FK2 FOREIGN KEY(nc_approval_identity_id)  REFERENCES aas.identity(identity_id),
    CONSTRAINT payments_FK3 FOREIGN KEY(nc_order_detail_id) REFERENCES clean.order_details(order_detail_id)
    );

comment on column clean.payments.nc_approval_identity_id is 'User that approved the no charge';
comment on column clean.payments.nc_type_code is 'Type of no charge ';
comment on column clean.payments.nc_reason_code is 'Reason for no charge';
comment on column clean.payments.nc_order_detail_id is 'Original order number associated with the no charge';

create index clean.payments_n4 on clean.payments(nc_order_detail_id) tablespace clean_indx compute statistics;


alter table ftd_apps.state_master add (
          drop_ship_available_flag           VARCHAR2(1) DEFAULT 'N' NOT NULL,
    CONSTRAINT state_master_CK1  CHECK(drop_ship_available_flag IN('Y','N')));

Update ftd_apps.state_master set drop_ship_available_flag = 'Y';
Update ftd_apps.state_master set drop_ship_available_flag = 'N' where country_code is not null;
Update ftd_apps.state_master set drop_ship_available_flag = 'N' where state_master_id in ('VI','PR');



ALTER TABLE FTD_APPS.PAYMENT_METHODS ADD (
     ACTIVE_FLAG        varCHAR2(1) DEFAULT 'Y' NOT NULL, 
     JOE_FLAG           varCHAR2(1) DEFAULT 'Y' NOT NULL, 
     MOD10_FLAG         varCHAR2(1) DEFAULT 'N' NOT NULL, 
     REGEX_PATTERN      VARCHAR2(100), 
     min_auth_amt       number(5,2),
  CONSTRAINT PAYMENT_METHODS_CK2 CHECK(active_flag IN ('Y','N')), 
  CONSTRAINT PAYMENT_METHODS_CK3 CHECK(joe_flag IN ('Y','N')),
  CONSTRAINT PAYMENT_METHODS_CK4 CHECK(mod10_flag IN ('Y','N'))
);


UPDATE FTD_APPS.PAYMENT_METHODS p SET p.ACTIVE_FLAG = 'N' WHERE p.PAYMENT_METHOD_ID = 'JP';
UPDATE FTD_APPS.PAYMENT_METHODS p SET p.JOE_FLAG = 'N' WHERE p.PAYMENT_METHOD_ID IN ('JP','PP','PC');
UPDATE FTD_APPS.PAYMENT_METHODS p SET p.MOD10_FLAG = 'Y' WHERE p.PAYMENT_METHOD_ID IN ('AX','DC','DI','MC','NC','VI','CB');
UPDATE ftd_apps.payment_methods pm SET pm.min_auth_amt = .25 WHERE pm.payment_method_id IN ('MS', 'DC', 'DI', 'MC', 'VI', 'CB', 'JP');
UPDATE ftd_apps.payment_methods pm SET pm.min_auth_amt = 0 WHERE pm.payment_method_id = 'AX';
update ftd_apps.payment_methods set joe_flag = 'Y' where payment_method_id = 'PC';


alter table ftd_apps.country_master add (
         SUNDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         MONDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         TUESDAY_TRANSIT_FLAG      VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         WEDNESDAY_TRANSIT_FLAG    VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         THURSDAY_TRANSIT_FLAG     VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         FRIDAY_TRANSIT_FLAG       VARCHAR2(1)     DEFAULT 'N' NOT NULL,
         SATURDAY_TRANSIT_FLAG     VARCHAR2(1)     DEFAULT 'N' NOT NULL,
    CONSTRAINT COUNTRY_MASTER_CK9  CHECK(SUNDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK10  CHECK(MONDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK12 CHECK(TUESDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK13 CHECK(WEDNESDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK14 CHECK(THURSDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK15 CHECK(FRIDAY_TRANSIT_FLAG IN('Y','N')), 
    CONSTRAINT COUNTRY_MASTER_CK16  CHECK(SATURDAY_TRANSIT_FLAG IN('Y','N')));


update ftd_apps.country_master
     set  SUNDAY_TRANSIT_FLAG       = 'N',
          MONDAY_TRANSIT_FLAG       = 'Y',
          TUESDAY_TRANSIT_FLAG      = 'Y',
          WEDNESDAY_TRANSIT_FLAG    = 'Y',
          THURSDAY_TRANSIT_FLAG     = 'Y',
          FRIDAY_TRANSIT_FLAG       = 'Y',
          SATURDAY_TRANSIT_FLAG     = 'N'
   where country_id = 'US';


ALTER TABLE FTD_APPS.BILLING_INFO ADD (
     VALIDATION_REGEX      VARCHAR2(512));

COMMENT ON COLUMN FTD_APPS.BILLING_INFO.VALIDATION_REGEX IS 'Regular expression used to validate input on the JOE screen';



-- DNIS updates
alter table ftd_apps.dnis add (
          company_id        varchar2(12),
          script_type_code  varchar2(100)  DEFAULT 'Default' NOT NULL,
    CONSTRAINT DNIS_FK2 FOREIGN KEY(company_id) REFERENCES ftd_apps.company_master(company_id));

CREATE INDEX ftd_apps.dnis_n3 ON ftd_apps.dnis(company_id) TABLESPACE ftd_apps_INDX;

COMMENT ON COLUMN ftd_apps.dnis.company_id is 'Validated by company_master - will obsolete script_id in the future.';
COMMENT ON COLUMN ftd_apps.dnis.script_type_code is 'Identifies which type of script is applicable to this dnis';

update ftd_apps.dnis set company_id = script_id;

alter table ftd_apps.dnis modify (company_id not null);
-- END DNIS Updates


alter table ftd_apps.source_master add (
          iotw_flag        varchar2(1)    DEFAULT 'N' NOT NULL,
          invoice_password varchar2(60),
    CONSTRAINT source_master_CK9  CHECK(iotw_flag IN('Y','N')));


create synonym osp.product_master for ftd_apps.product_master;

update ftd_apps.dnis set origin = 'FTDP'
  where origin in ('FTDNF', 'WEST');


delete from ftd_apps.state_master
  where state_master_id = 'UK';

commit;


-------------------------------------------------------------------
--- SET JOE, MAINTENANCE, AND WEBOE ACCESS ------------------------
-------------------------------------------------------------------

--Create resources for OE access and maintenance screens
insert into aas.resources values('OldWebOE','Order Proc','Determines if the user has access to WebOE.  WebOE is the old order entry system.',sysdate,sysdate,'SYS');
insert into aas.resources values('Item Of The Week Maint Link','Order Proc','Determines if the user has access to the Item of the Week maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Product Favorites Maint Link','Order Proc','Determines if the user has access to the Product Favorites maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Cross Sell Maint Link','Order Proc','Determines if the user has access to the Cross Sell maintenance screen.',sysdate,sysdate,'SYS');
insert into aas.resources values('Card Message Maint Link','Order Proc','Determines if the user has access to the Card Message maintenance screen.',sysdate,sysdate,'SYS');

--Create resource group for OE access
insert into aas.acl values('OldWebOE',sysdate,sysdate,'SYS',null);

--Assign resources to resource groups for OE access and maintenance screens
insert into aas.rel_acl_rp values ('OldWebOE','OldWebOE','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Operations','Item Of The Week Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Director','Product Favorites Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Merchandising','Cross Sell Maint Link','Order Proc','View',sysdate,sysdate,'SYS');
insert into aas.rel_acl_rp values ('Director','Card Message Maint Link','Order Proc','View',sysdate,sysdate,'SYS');

--Assign resource group to role for OE access
insert into aas.rel_role_acl values ((select role_id from aas.role where role_name='CSRLevel B'), 'OldWebOE',sysdate,sysdate,'SYS');

/*Delete security assignments related to OE access and maintenance screens if needed
delete from aas.rel_acl_rp where resource_id = 'OldWebOE';
delete from aas.rel_role_acl where acl_name = 'OldWebOE';
delete from aas.acl where acl_name = 'OldWebOE';
delete from aas.rel_acl_rp where resource_id = 'Item Of The Week Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Product Favorites Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Cross Sell Maint Link';
delete from aas.rel_acl_rp where resource_id = 'Card Message Maint Link';
delete from aas.resources where resource_id = 'OldWebOE';
delete from aas.resources where resource_id = 'Item Of The Week Maint Link';
delete from aas.resources where resource_id = 'Product Favorites Maint Link';
delete from aas.resources where resource_id = 'Cross Sell Maint Link';
delete from aas.resources where resource_id = 'Card Message Maint Link';
*/

--- END SET JOE, MAINTENANCE, AND WEBOE ACCESS --------------------
-------------------------------------------------------------------



alter table ftd_apps.source_master modify(description varchar2(80));
alter table ftd_apps.source_master$ modify(description varchar2(80));
alter table ftd_apps.source_master_reserve modify(description varchar2(80));
alter table ftd_apps.source_master_reserve$ modify(description varchar2(80));
alter table ftd_apps.source_master_reserve_stage modify(description varchar2(80));


alter table venus.zj_trip_vendor_order drop constraint zj_trip_vendor_order_pk;

alter table venus.zj_trip_vendor_order add (CONSTRAINT zj_trip_vendor_order_pk 
        PRIMARY KEY(trip_id,vendor_id,order_detail_id,venus_id));



-- Defect 4424
alter table ftd_apps.vendor_product$ modify (timestamp$ timestamp);

drop index ftd_apps.PRODUCT_CHANGE$_ACCT_N2;  
alter table ftd_apps.product_change$_acct modify (timestamp$ timestamp);
create index ftd_apps.PRODUCT_CHANGE$_ACCT_N2 on ftd_apps.PRODUCT_CHANGE$_ACCT(timestamp$) tablespace ftd_apps_indx;
-- End Defect 4424




---- END FIX 3.0.0 DEPLOYTIME RELEASE SCRIPT -------------------
