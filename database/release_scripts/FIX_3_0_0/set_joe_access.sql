declare

v_users varchar2(4000);
v_identity_id varchar2(255);
v_csrlevel_a_role_id varchar2(255);
v_csrlevel_b_role_id varchar2(255);
v_user_exists number(2);
v_user_has_role number(2);
v_next_user_position number(2);

v_users_do_not_exist varchar2(4000);
v_users_have_role varchar2(4000);
v_users_given_access varchar2(4000);

begin
--COMMA DELIMITED LIST OF USERS YOU WOULD LIKE TO GIVE CSRLevel A ACCESS
--LIST MUST CONTAIN AT LEAST ONE
--EXAMPLE:  v_users := 'mkruger,jimmy,mkrugertest,jessie';
v_users := 'mkruger,jimmy,mkrugertest,jessie';


--Obtain the CSRLevel A and B role id
select r.role_id into v_csrlevel_a_role_id from aas.role r where r.role_name = 'CSRLevel A';
select r.role_id into v_csrlevel_b_role_id from aas.role r where r.role_name = 'CSRLevel B';

loop

    v_user_exists := 0;
    v_user_has_role := 0;
    v_identity_id := '';
    v_next_user_position := instr(v_users, ',');

    DBMS_OUTPUT.PUT_LINE('Next user position ' || v_next_user_position);
    DBMS_OUTPUT.PUT_LINE('Users ' || v_users);

    --If a ',' exists
    if(v_next_user_position > 0) then
      v_identity_id := substr(v_users, 0, (v_next_user_position - 1));
    --If no ',' exists only one user is left
    else
      v_identity_id := v_users;
      v_users := null;
    end if;

    DBMS_OUTPUT.PUT_LINE('User ' || v_identity_id);

    --Throw away extra commas if they occur
    if(v_identity_id != ',') then
      --Determine if the user exists
      select count(*) into v_user_exists from aas.identity i where i.identity_id = v_identity_id;
  
      --Determine if the user already has the CSRLevel A role
      select count(*)
      into v_user_has_role
      from aas.rel_identity_role rir
      where rir.role_id = (select r.role_id from aas.role r where r.role_name = 'CSRLevel A')
      and rir.identity_id = v_identity_id;
  
      DBMS_OUTPUT.PUT_LINE('User exists ' || v_user_exists);
      DBMS_OUTPUT.PUT_LINE('User has role ' || v_user_has_role);
  
      --If the user exists
      if(v_user_exists > 0) then
         --If the user does not have the CSRLevel A role assign the role to them
         if(v_user_has_role = 0) then
           insert into aas.rel_identity_role rir values(v_identity_id, v_csrlevel_a_role_id, SYSDATE, SYSDATE, 'JOE_ROLLOUT');
           delete from aas.rel_identity_role rir where rir.identity_id = v_identity_id and rir.role_id = v_csrlevel_b_role_id;
           v_users_given_access := v_users_given_access || v_identity_id || ', ';
         --If the user already has CSRLevel A access
         else
           delete from aas.rel_identity_role rir where rir.identity_id = v_identity_id and rir.role_id = v_csrlevel_b_role_id;
           v_users_have_role := v_users_have_role || v_identity_id || ', ';
         end if;
      --If the user does not exist
      else
         v_users_do_not_exist := v_users_do_not_exist || v_identity_id || ', ';
      end if;
    end if;

    --Remove the user we attempted to insert from v_users
    if(v_next_user_position > 0) then
      v_users := substr(v_users, (instr(v_users, ',')) + 1);
    end if;

    DBMS_OUTPUT.PUT_LINE('Users ' || v_users);

    exit when v_users is null;
end loop;

--Summary of actions
if(length(v_users_given_access) > 1) then v_users_given_access := substr(v_users_given_access, 0, (length(v_users_given_access) - 2)); end if;
if(length(v_users_do_not_exist) > 1) then v_users_do_not_exist := substr(v_users_do_not_exist, 0, (length(v_users_do_not_exist) - 2)); end if;
if(length(v_users_have_role) > 1) then v_users_have_role := substr(v_users_have_role, 0, (length(v_users_have_role) - 2)); end if;
DBMS_OUTPUT.PUT_LINE('Users given CSRLevel A access: ' || v_users_given_access);
DBMS_OUTPUT.PUT_LINE('Users that do not exist: ' || v_users_do_not_exist);
DBMS_OUTPUT.PUT_LINE('Users that already have CSRLevel A access: ' || v_users_have_role);

DBMS_OUTPUT.PUT_LINE('***********************');
DBMS_OUTPUT.PUT_LINE('COMMIT IF ALL WENT WELL');
DBMS_OUTPUT.PUT_LINE('***********************');

end;
/