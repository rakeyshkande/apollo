

set serveroutput on size 99999

-- 
-- Load JOE.IOTW
--

--delete from joe.iotw_delivery_discount_dates;
delete from joe.iotw;

declare
   cursor c1 is
      select * from joe.iotw_stage_temp;

   cntr number := 0;
   bad_lookup_cntr number := 0;
   notfound_cntr number := 0;
   ins_err_cntr number := 0;
   sm_upd_err_cntr number := 0;
   successful_ins_cntr number := 0;
   v_upsell_found_cntr number := 0;
   v_pm_found_cntr number := 0;

   v_found number := 1;
   v_successful_insert  number := 1;

   v_product_id   ftd_apps.product_master.product_id%type;
   v_upsell_master_id   ftd_apps.upsell_master.upsell_master_id%type;
   v_err varchar2(1000);

begin
   for c1_rec in c1 LOOP
      cntr := cntr + 1;
      v_found := 0;
      v_successful_insert := 0;
      v_product_id := NULL;
      v_upsell_master_id := NULL;
      begin
         select product_id into v_product_id
         from ftd_apps.product_master
         where novator_id = c1_rec.product_id;
         v_pm_found_cntr := v_pm_found_cntr + 1;

         v_found := 1;
      exception
         when no_data_found then
            begin
               -- NO DATA IS OK - IT MAY BE AN UPSELL
               -- SEE IF UPSELL, IF IT IS, MOVE ON (BUT DO NOT INSERT TO IOTW)
               select upsell_master_id into v_upsell_master_id
               from ftd_apps.upsell_master
               where upsell_master_id = c1_rec.product_id;
               v_found := 1;

               v_upsell_found_cntr := v_upsell_found_cntr + 1;

            exception
               when no_data_found then
                  dbms_output.put_line('Product_ID Not in Prod Master OR Upsell_Master:  ' || c1_rec.product_id);
                  notfound_cntr := notfound_cntr + 1;

               when others then
                  dbms_output.put_line('Unexpected Error Looking up Upsell ID for novator_id ' || c1_rec.product_id
                         || '    Sqlcode=' || sqlcode);
            end;
         when others then
            dbms_output.put_line('Unexpected Lookup for novator_id ' || c1_rec.product_id
                         || '    Sqlcode=' || sqlcode);
            bad_lookup_cntr := bad_lookup_cntr + 1;
      end;

      if v_found = 1 then
         begin
            insert into joe.iotw
               (IOTW_ID,
                SOURCE_CODE,
                PRODUCT_ID,
                UPSELL_MASTER_ID  ,
                START_DATE,
                END_DATE,
                PRODUCT_PAGE_MESSAGING_TXT,
                CALENDAR_MESSAGING_TXT,
                IOTW_SOURCE_CODE,
                SPECIAL_OFFER_FLAG,
                WORDING_COLOR_TXT,
                CREATED_BY,
                CREATED_ON,
                UPDATED_BY,
                UPDATED_ON)
            values
               (joe.iotw_sq.nextval,
                c1_rec.website_id,
                v_product_id,
                v_upsell_master_id,
                nvl(to_date(c1_rec.start_date,'YYYY/MM/DD'),NULL),
                nvl(to_date(c1_rec.end_date,'YYYY/MM/DD'),NULL),
                nvl(c1_rec.product_page_messaging,NULL),
                nvl(c1_rec.calendar_messaging,NULL),
                nvl(c1_rec.iotw_source_code,NULL),
                decode(c1_rec.special_offer_switch,'Off','N','On','Y','N'),
                nvl(c1_rec.colour_code,NULL),
                'FIX 3.0 Initial Load - Defect 1787',
                sysdate,
                'FIX 3.0 Initial Load - Defect 1787',
                sysdate);

             successful_ins_cntr := successful_ins_cntr + 1;

          -- begin
          --    update ftd_apps.source_master
          --    set iotw_flag = 'Y'
          --    where source_code = c1_rec.iotw_source_code;
              --dbms_output.put_line('Source_Master.IOTW_FLAG set for source_code ' || c1_rec.iotw_source_code);
          -- exception
          --    when others then
          --       dbms_output.put_line('Unexpected Error on updating ftd_apps.source_master.iotw_flag for' || c1_rec.website_id
          --                 || '   product_id=' || c1_rec.product_id ||
          --                 '    Sqlcode=' || sqlcode || '  msg=' || sqlerrm);
          --       sm_upd_err_cntr := sm_upd_err_cntr + 1;
          -- end;


         exception
            when others then
               if instr(sqlerrm,'JOE.IOTW_FK1') > 0 THEN
                  v_err := '     Source_Code Does not Exist';
               else
                  if instr(sqlerrm,'JOE.IOTW_FK2') > 0 THEN
                     v_err := '     Product_Code Does not Exist';
                  else
                     if instr(sqlerrm,'JOE.IOTW_FK3') > 0 THEN
                        v_err := '    IOTW_Source_Code Does not Exist';
                     else
                        v_err := sqlerrm;
                     end if;
                 end if;
              end if;

               dbms_output.put_line('Unexpected Error on insert website=' || c1_rec.website_id
                            || '   product_id=' || c1_rec.product_id || v_err);

               ins_err_cntr := ins_err_cntr + 1;
         end;
      end if;
   end loop;
   commit;
   dbms_output.put_line('.');
   dbms_output.put_line('Looped ' || cntr || ' times');
   dbms_output.put_line('Successful IOTW Inserts: ' || successful_ins_cntr);
   dbms_output.put_line('Number found in Product_Master:' || v_pm_found_cntr);
   dbms_output.put_line('Number found in Upsell_Master :' || v_upsell_found_cntr);
   dbms_output.put_line('Novator ID Not Found in Prod Master OR Upsell_Master = ' || notfound_cntr);
   dbms_output.put_line('Bad Product Master Lookups = ' || bad_lookup_cntr);
   dbms_output.put_line('Ins Errors = ' || ins_err_cntr);
   dbms_output.put_line('Source Master Update Errors = ' || sm_upd_err_cntr);
end;
/
