declare

--Determine which users do not have the CSRLevelB role
cursor no_csrlevel_b_cursor is
select i.identity_id
from aas.identity i
where not exists
    (
        select 1
        from aas.rel_identity_role rir
        where rir.role_id = (select r.role_id from aas.role r where r.role_name = 'CSRLevel B')
        and rir.identity_id = i.identity_id
    );

--Obtain number of CSRLevel A users
cursor total_csrlevel_a_users_cursor is
select count(*) count
from  aas.rel_identity_role rir
where rir.role_id = (select r.role_id from aas.role r where r.role_name = 'CSRLevel A');

--Obtain number of CSRLevel B users
cursor total_csrlevel_b_users_cursor is
select count(*) count
from  aas.rel_identity_role rir
where rir.role_id = (select r.role_id from aas.role r where r.role_name = 'CSRLevel B');

--Obtain number of total users
cursor total_users_cursor is
select count(*) count
from  aas.identity;

v_csrlevel_b_role_id varchar2(255);
v_total_users number(10);
v_total_csrlevel_b_users number(10);
v_total_csrlevel_a_users number(10);

begin
open total_csrlevel_a_users_cursor; fetch total_csrlevel_a_users_cursor into v_total_csrlevel_a_users; close total_csrlevel_a_users_cursor;
open total_csrlevel_b_users_cursor; fetch total_csrlevel_b_users_cursor into v_total_csrlevel_b_users; close total_csrlevel_b_users_cursor;
open total_users_cursor; fetch total_users_cursor into v_total_users; close total_users_cursor;
DBMS_OUTPUT.PUT_LINE('Total number of CSRLevel A users: ' || v_total_csrlevel_a_users);
DBMS_OUTPUT.PUT_LINE('Total number of CSRLevel B users: ' || v_total_csrlevel_b_users);
DBMS_OUTPUT.PUT_LINE('Total number of users:            ' || v_total_users);

--Delete the CSRLevel A role from all users
delete from aas.rel_identity_role rir
where rir.role_id = (select r.role_id from aas.role r where r.role_name = 'CSRLevel A');

--Add the CSRLevel B role
select r.role_id into v_csrlevel_b_role_id from aas.role r where r.role_name = 'CSRLevel B';

for x in no_csrlevel_b_cursor loop
    insert into aas.rel_identity_role rir values(x.identity_id, v_csrlevel_b_role_id, SYSDATE, SYSDATE, 'JOE_ROLLOUT');
end loop;

open total_csrlevel_a_users_cursor; fetch total_csrlevel_a_users_cursor into v_total_csrlevel_a_users; close total_csrlevel_a_users_cursor;
open total_csrlevel_b_users_cursor; fetch total_csrlevel_b_users_cursor into v_total_csrlevel_b_users; close total_csrlevel_b_users_cursor;
open total_users_cursor; fetch total_users_cursor into v_total_users; close total_users_cursor;

if(v_total_csrlevel_a_users > 0) then
  DBMS_OUTPUT.PUT_LINE('******************************************************************************************************************************************');
  DBMS_OUTPUT.PUT_LINE('BAD THINGS JUST HAPPENED - DO NOT COMMIT - There should not be any users with CSRLevel A and there are ' || v_total_csrlevel_a_users || '.');
  DBMS_OUTPUT.PUT_LINE('******************************************************************************************************************************************');
end if;

if(v_total_csrlevel_b_users != v_total_users) then
  DBMS_OUTPUT.PUT_LINE('*************************************************************************************************************************************************************************************');
  DBMS_OUTPUT.PUT_LINE('BAD THINGS JUST HAPPENED - DO NOT COMMIT - All users should have CSRLevel B access.  There are ' || v_total_users || ' users and ' || v_total_csrlevel_b_users || ' CSRLevel B users.');
  DBMS_OUTPUT.PUT_LINE('*************************************************************************************************************************************************************************************');
end if;

DBMS_OUTPUT.PUT_LINE('Total number of CSRLevel A users: ' || v_total_csrlevel_a_users);
DBMS_OUTPUT.PUT_LINE('Total number of CSRLevel B users: ' || v_total_csrlevel_b_users);
DBMS_OUTPUT.PUT_LINE('Total number of users:            ' || v_total_users);

DBMS_OUTPUT.PUT_LINE('***********************');
DBMS_OUTPUT.PUT_LINE('COMMIT IF ALL WENT WELL');
DBMS_OUTPUT.PUT_LINE('***********************');

end;
/