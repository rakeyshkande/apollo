------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------

-- Required Golden Gate change

use ptn_pi;
alter table ptn_pi.partner_mapping add column UPDATE_ORDER_FLAG  varchar(1) default null;



------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthikeya D,                                 7/7/2015  --------
-- defect #FEPHII-12    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

-- Required Golden Gate change

use clean; 
alter table order_details add column ACTUAL_ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table order_bills add column ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table accounting_transactions add column ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table refund add column REFUND_ADDON_DISCOUNT_AMT decimal(12,2) default null;

------------------------------------------------------------------------------------
-- end   requested by Karthikeya D,                                 7/7/2015  --------
-- defect #FEPHII-12    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                7/22/2015  --------
-- defect FEPHII-3      (Syed      )   (156757 )      --------------------------------
------------------------------------------------------------------------------------
 

GRANT SELECT ON JOE.PARTNER_PRODUCT_UPDATE TO RPT;

GRANT SELECT ON PTN_PI.PARTNER_ORDER_DETAIL TO RPT;


------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                                7/22/2015  --------
-- defect FEPHII-3      (Syed      )   (156757 )      --------------------------------
-------------------------------------------------------------------------------------
