------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------

-- Required Golden Gate change

ALTER TABLE PTN_PI.PARTNER_MAPPING ADD UPDATE_ORDER_FLAG CHAR(1);


update ptn_pi.partner_mapping
set update_order_flag = 'Y',
updated_on = sysdate,
updated_by = 'FEPHII-1'
where partner_id = 'PRO';

update ptn_pi.partner_mapping
set update_order_flag = 'N',
updated_on = sysdate,
updated_by = 'FEPHII-1'
where partner_id <> 'PRO';

COMMIT;

GRANT SELECT ON PTN_PI.PARTNER_ORDER_DETAIL TO JOE;

GRANT SELECT ON PTN_PI.PARTNER_MAPPING TO JOE;

GRANT SELECT ON PTN_PI.PARTNER_MAPPING TO FRP;

------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthikeya D,                                 7/7/2015  --------
-- defect #FEPHII-12    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE SCRUB.ADD_ONS ADD (ADD_ON_DISCOUNT_AMOUNT VARCHAR2(4000 BYTE));
ALTER TABLE SCRUB.ORDER_DETAILS ADD (ADD_ON_DISCOUNT_AMOUNT VARCHAR2(4000 BYTE));
ALTER TABLE SCRUB.ORDERS ADD (ADD_ON_DISCOUNT_AMOUNT VARCHAR2(4000 BYTE));

ALTER TABLE CLEAN.ORDER_ADD_ONS ADD (ADD_ON_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_ADD_ONS ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_ADD_ONS_UPDATE ADD (ADD_ON_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_ADD_ONS_UPDATE ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_DETAILS ADD (ACTUAL_ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_DETAIL_UPDATE ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_DETAIL_UPDATE ADD (ACTUAL_ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_BILLS ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ORDER_BILLS_UPDATE ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ACCOUNTING_TRANSACTIONS ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ACCOUNTING_TRANSACTIONS$ ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));
ALTER TABLE CLEAN.ACCOUNTING_TRANSACTIONS_UPD ADD (ADD_ON_DISCOUNT_AMOUNT NUMBER(10,2));

ALTER TABLE PTN_PI.PARTNER_ORDER_DETAIL ADD (ADD_ON_RETAIL_PRICE NUMBER);
ALTER TABLE PTN_PI.PARTNER_ORDER_DETAIL ADD (ADD_ON_SALE_PRICE NUMBER); 

CREATE TABLE PTN_PI.PARTNER_ORDER_ADD_ONS (PARTNER_ORDER_DETAIL_ID NUMBER NOT NULL, ADD_ON_ID VARCHAR2(100) NOT NULL, 
ADD_ON_RETAIL_PRICE NUMBER NOT NULL, 
ADD_ON_SALE_PRICE NUMBER, QUANTITY NUMBER, CREATED_ON DATE NOT NULL, CREATED_BY VARCHAR2(100) NOT NULL, UPDATED_ON DATE NOT NULL, 
UPDATED_BY VARCHAR2(100) NOT NULL,
CONSTRAINT "PK_ADD_ONS" PRIMARY KEY ("ADD_ON_ID","PARTNER_ORDER_DETAIL_ID"),
CONSTRAINT "FK_PARTNER_ORDER_DETAIL_ID" FOREIGN KEY ("PARTNER_ORDER_DETAIL_ID")
                  REFERENCES PTN_PI.PARTNER_ORDER_DETAIL ("PARTNER_ORDER_DETAIL_ID")
);
 
------------------------------------------------------------------------------------
-- begin requested by Bhargav swamy,                                 7/8/2015  --------
-- defect #PISFEII-1   (Pavan )                    --------------------------------
------------------------------------------------------------------------------------
Update Frp.Global_Parms 
Set Value=Concat(Value,concat(',','add_ons')), updated_by='SYSTEM', updated_on=sysdate
Where context='PI_CONFIG' and name ='PROCESS_FEEDS';

------------------------------------------------------------------------------------
-- end   requested by Bhargav swamy,                                 7/8/2015  --------
-- defect #PISFEII-1    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B,                                 7/10/2015  --------
-- defect #FEPHII-6    (Pavan )                    --------------------------------
-------------------------------------------------------------------------------------

ALTER TABLE clean.refund ADD (REFUND_ADDON_DISCOUNT_AMT number(12,2));
ALTER TABLE clean.Refund_Update ADD (REFUND_ADDON_DISCOUNT_AMT number(12,2));
ALTER TABLE PTN_PI.PARTNER_ORDER_ADJUSTMENT ADD (REFUND_PRINCIPAL_AMT number default null);
ALTER TABLE PTN_PI.PARTNER_ORDER_ADJUSTMENT ADD (ADDON_AMT number default null);
ALTER TABLE PTN_PI.PARTNER_ORDER_ADJUSTMENT ADD (REFUND_ADDON_AMT number default null);

-------------------------------------------------------------------------------------
-- end   requested by Gunadeep B,                                 7/10/2015  --------
-- defect #FEPHII-6     (Pavan )                    --------------------------------
-------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                6/26/2015  --------
-- defect SP-53       (Mark      )   (153432 )      --------------------------------
------------------------------------------------------------------------------------

grant select on ptn_pi.partner_mapping to frp;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                                6/26/2015  --------
-- defect SP-53       (Mark      )   (153432 )      --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 7/8/2015  --------
-- defect FEPHII-2    (Syed      )   (155379 )      --------------------------------
------------------------------------------------------------------------------------

grant update on CLEAN.ORDER_DETAILS to JOE;
 
------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                                 7/8/2015  --------
-- defect FEPHII-2    (Syed      )   (155379 )      --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- begin requested by Karthik D    ,                      7/24/2015  --------
-- defect FEPHII-44  (Pavan)                    --------------------------
------------------------------------------------------------------------------

ALTER TABLE MERCURY.MERCURY_RULES_TRACKER MODIFY (PREFERRED_PARTNER_ID VARCHAR2(20));

------------------------------------------------------------------------------
-- end   requested by Karthik D   ,                      7/24/2015  --------
-- defect FEPHII-44  (Pavan)                   --------------------------
------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthikeyan,                                8/14/2015  --------
-- defect  SP-91     (Pavan  )                          -----------------------------
------------------------------------------------------------------------------------
 
ALTER TABLE clean.refund$ ADD (REFUND_ADDON_DISCOUNT_AMT number(12,2));

------------------------------------------------------------------------------------
-- begin requested by Karthikeyan,                                8/14/2015  --------
-- defect  SP-91     (pavan  )                          -----------------------------
------------------------------------------------------------------------------------


