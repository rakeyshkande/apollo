------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Mark      )   (153757 )      --------------------------------
------------------------------------------------------------------------------------

set sqlblanklines on
insert into FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values ( 
 'OE_CONFIG', 'MODIFY_ORDER_THRESHOLD_PRO', '10', 'FEPHII-1', sysdate, 'FEPHII-1', sysdate, 'Modify Order Threshold for ProFlowers');

insert into FTD_APPS.CONTENT_FILTER ( CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_filter_sq.nextval, 'PARTNER_ID', 'FEPHII-1', sysdate, 'FEPHII-1', sysdate); 

insert into FTD_APPS.CONTENT_MASTER ( CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, 
 CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_master_sq.nextval, 'PARTNER_INTEGRATION', 'THRESHOLD_MESSAGE_GREATER_THAN',
 'Message to display if the new product/addon total is greater than the old amount',
 (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PARTNER_ID'),
 null, 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

insert into FTD_APPS.CONTENT_MASTER ( CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
 CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_master_sq.nextval, 'PARTNER_INTEGRATION', 'THRESHOLD_MESSAGE_LESS_THAN',
 'Message to display if the new product/addon total is less than the old amount',
 (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PARTNER_ID'),
 null, 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'PARTNER_INTEGRATION' AND CONTENT_NAME = 'THRESHOLD_MESSAGE_GREATER_THAN'),
'PRO', null, 
'Product total is greater than the allowable price change.
If a product substitution is needed,
Please select a product with a price within $~threshold~ of the original price.

If customer is requesting a new product,
Please transfer the call to an agent who can create a new order for the customer.
Transfer the call to 6384607',
 'FEPHII-1', sysdate, 'FEPHII-1', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
 UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
 (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'PARTNER_INTEGRATION' AND CONTENT_NAME = 'THRESHOLD_MESSAGE_LESS_THAN'),
 'PRO', null, 
 'Product price is less than the original product price.
Please process a refund for the difference and advise the customer.',
 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Mark      )   (153757 )      --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                7/14/2015  --------
-- defect FEPHII-3     (Mark  ) request 155974, 155985 -----------------------------
------------------------------------------------------------------------------------
 
create table JOE.PARTNER_PRODUCT_UPDATE (
    PARTNER_PRODUCT_UPDATE_ID integer not null,
    ORDER_DETAIL_ID integer,
    DELIVERY_DATE   Date,
    ORIGINAL_PRODUCT_ID Varchar2(10),
    NEW_PRODUCT_ID  Varchar2(10),
    ORIGINAL_MERCH_AMOUNT Number,
    NEW_MERCH_AMOUNT Number) tablespace joe_data;

alter table joe.partner_product_update add constraint partner_product_update_pk primary key (partner_product_update_id)
using index tablespace joe_indx;

alter table joe.partner_product_update add constraint partner_product_update_fk1 foreign key (order_detail_id)
references clean.order_details;

create sequence JOE.PARTNER_PRODUCT_UPDATE_ID_SQ cache 200;

alter table joe.partner_product_update add (created_on date, created_by varchar2(100),
                                            updated_on date, updated_by varchar2(100));

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                7/14/2015  --------
-- defect FEPHII-3     (Mark  ) request 155974, 155985 -----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- begin requested by Tim Schmig    ,                      7/23/2015  --------
-- defect FEPHII-3 (Mark)     (157042)              --------------------------
------------------------------------------------------------------------------

SET serveroutput on;

DECLARE
   v_url varchar2(1000);
   v_db_name    VARCHAR2(9); 
BEGIN
   SELECT name INTO v_db_name FROM v$database;
   IF v_db_name = 'ZEUS' THEN
      v_url := 'http://apolloreports.ftdi.com/reports/rwservlet?';
   ELSE    
      v_url := 'http://neon.ftdi.com:7778/reports/rwservlet?';
   END IF;

   insert into rpt.report (
       report_id,
       name,
       description,
       report_file_prefix,
       report_type,
       report_output_type,
       report_category,
       server_name,
       holiday_indicator,
       status,
       created_on,
       created_by,
       updated_on,
       updated_by,
       oracle_rdf,
       report_days_span,
       notes,
       acl_name,
       retention_days,
       run_time_offset,
       end_of_day_required,
       schedule_day,
       schedule_type,
       schedule_parm_value
   ) values (
       100400,
       'Product Swap',
       'Report of product and/or add-on amounts changed in Modify Order for Partner Integration orders',
       'Product_Swap Report',
       'U',
       'Char',
       'Acct',
       v_url,
       'N',
       'Active',
       sysdate,
       'FEPHII-3',
       sysdate,
       'FEPHII-3',
       'EDIT07_Product_Swap_Report.rdf',
       null,
       null,
       'BaseAcctReportAccess',
       10,
       120,
       'N',
       null,
       null,
       null
   );

end;
/

insert into rpt.report_parm (
    report_parm_id,
    display_name,
    report_parm_type,
    oracle_parm_name,
    created_on,
    created_by,
    updated_on,
    updated_by,
    validation
) values (
    100400,
    'Partner Id',
    'MS',
    'p_partner_id',
    sysdate,
    'FEPHII-3',
    sysdate,
    'FEPHII-3',
    null
);

insert into rpt.report_parm_ref (
    report_parm_id,
    report_id,
    sort_order,
    required_indicator,
    created_on,
    created_by,
    allow_future_date_ind
) values (
    100400,
    100400,
    2,
    'Y',
    sysdate,
    'FEPHII-3',
    null
);

insert into rpt.report_parm_ref (
    report_parm_id,
    report_id,
    sort_order,
    required_indicator,
    created_on,
    created_by,
    allow_future_date_ind
) values (
    100402,
    100400,
    1,
    'Y',
    sysdate,
    'FEPHII-3',
    null
);

insert into rpt.report_parm_value (
    report_parm_value_id,
    created_on,
    created_by,
    updated_on,
    updated_by,
    display_text,
    db_statement_id,
    default_form_value
) values (
    101400,
    sysdate,
    'FEPHII-3',
    sysdate,
    'FEPHII-3',
    null,
    'GET_MODIFY_ORDER_PARTNERS',
    'ALL'
);

insert into rpt.report_parm_value_ref (
    report_parm_id,
    report_parm_value_id,
    sort_order,
    created_on,
    created_by
) values (
    100400,
    101400,
    1,
    sysdate,
    'FEPHII-3'
);

------------------------------------------------------------------------------
-- end   requested by Tim Schmig    ,                      7/23/2015  --------
-- defect FEPHII-3 (Mark)     (157042)              --------------------------
------------------------------------------------------------------------------
 
 ------------------------------------------------------------------------------
-- begin requested by Gunadeep B    ,                      7/24/2015  --------
-- modified by Pavan
-- defect FEPHII-27  (Pavan)                    --------------------------
------------------------------------------------------------------------------
Insert into FRP.global_parms(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values
 ('PI_CONFIG','PARTNER_DATE_RANGE_CONFIG','PRO','SYS',SYSDATE,'SYS',SYSDATE,
 'This field contains the partner Ids mapped to have delivery date range');
 ------------------------------------------------------------------------------
-- end   requested by Gunadeep B  ,                      7/24/2015  --------
-- defect FEPHII-27  (Pavan)                   --------------------------
------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by DharmaTeja,                                7/31/2015  --------
-- defect  SP-41     (Pavan  )                          -----------------------------
------------------------------------------------------------------------------------
 
 insert into FRP.GLOBAL_PARMS 
 values('SERVICE','CAMS_WS_SOCKET_TIMEOUT','10000','SYS',SYSDATE,'SYS',SYSDATE,'Timeout value for CAMS service.');

------------------------------------------------------------------------------------
-- begin requested by DharmaTeja,                                7/31/2015  --------
-- defect  SP-41     (pavan  )                          -----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharmateja,                                8/14/2015  --------
-- defect  SP-89     (Pavan  )                          -----------------------------
------------------------------------------------------------------------------------
 
insert into frp.global_parms values('SERVICE','CAMS_TIMEOUT_ENABLE_FLAG','ON','SYS',SYSDATE,
'SYS',SYSDATE,'Introducting to handling CAMSTimeoutException .If this flag is Off the exiting behavior will run.');

------------------------------------------------------------------------------------
-- begin requested by Dharmateja,                                8/14/2015  --------
-- defect  SP-89     (pavan  )                          -----------------------------
------------------------------------------------------------------------------------


