------------------------------------------------------------------------------------
-- begin requested by Karthikeya D,                                 7/7/2015  --------
-- defect #FEPHII-12    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

-- Required Golden Gate change

use clean; 
alter table order_details add column ACTUAL_ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table order_bills add column ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table accounting_transactions add column ADD_ON_DISCOUNT_AMOUNT decimal(10,2) default null;
alter table refund add column REFUND_ADDON_DISCOUNT_AMT decimal(12,2) default null;

------------------------------------------------------------------------------------
-- end   requested by Karthikeya D,                                 7/7/2015  --------
-- defect #FEPHII-12    (Pavan )                    --------------------------------
------------------------------------------------------------------------------------

