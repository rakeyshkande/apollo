------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Mark      )   (153757 )      --------------------------------
------------------------------------------------------------------------------------

set sqlblanklines on
insert into FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values ( 
 'OE_CONFIG', 'MODIFY_ORDER_THRESHOLD_PRO', '10', 'FEPHII-1', sysdate, 'FEPHII-1', sysdate, 'Modify Order Threshold for ProFlowers');

insert into FTD_APPS.CONTENT_FILTER ( CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_filter_sq.nextval, 'PARTNER_ID', 'FEPHII-1', sysdate, 'FEPHII-1', sysdate); 

insert into FTD_APPS.CONTENT_MASTER ( CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, 
 CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_master_sq.nextval, 'PARTNER_INTEGRATION', 'THRESHOLD_MESSAGE_GREATER_THAN',
 'Message to display if the new product/addon total is greater than the old amount',
 (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PARTNER_ID'),
 null, 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

insert into FTD_APPS.CONTENT_MASTER ( CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
 CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_master_sq.nextval, 'PARTNER_INTEGRATION', 'THRESHOLD_MESSAGE_LESS_THAN',
 'Message to display if the new product/addon total is less than the old amount',
 (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PARTNER_ID'),
 null, 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
WHERE CONTENT_CONTEXT = 'PARTNER_INTEGRATION' AND CONTENT_NAME = 'THRESHOLD_MESSAGE_GREATER_THAN'),
'PRO', null, 
'Product total is greater than the allowable price change.
If a product substitution is needed,
Please select a product with a price within $~threshold~ of the original price.

If customer is requesting a new product,
Please transfer the call to an agent who can create a new order for the customer.
Transfer the call to 6384607',
 'FEPHII-1', sysdate, 'FEPHII-1', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
 UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
 (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'PARTNER_INTEGRATION' AND CONTENT_NAME = 'THRESHOLD_MESSAGE_LESS_THAN'),
 'PRO', null, 
 'Product price is less than the original product price.
Please process a refund for the difference and advise the customer.',
 'FEPHII-1', sysdate, 'FEPHII-1', sysdate);

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Mark      )   (153757 )      --------------------------------
------------------------------------------------------------------------------------

