------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------

-- Required Golden Gate change

ALTER TABLE PTN_PI.PARTNER_MAPPING ADD UPDATE_ORDER_FLAG CHAR(1);


update ptn_pi.partner_mapping
set update_order_flag = 'Y',
updated_on = sysdate,
updated_by = 'FEPHII-1'
where partner_id = 'PRO';

update ptn_pi.partner_mapping
set update_order_flag = 'N',
updated_on = sysdate,
updated_by = 'FEPHII-1'
where partner_id <> 'PRO';

COMMIT;

GRANT SELECT ON PTN_PI.PARTNER_ORDER_DETAIL TO JOE;

GRANT SELECT ON PTN_PI.PARTNER_MAPPING TO JOE;

GRANT SELECT ON PTN_PI.PARTNER_MAPPING TO FRP;

------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                                 6/8/2015  --------
-- defect #FEPHII-1   (Syed Rizvi)   (152206 )      --------------------------------
------------------------------------------------------------------------------------



