
--###########################
--####    R E A D M E    ####
--###########################

--This script contains 2.1.0 database udpates which were requested for DEV? AFTER 2.1.0 went to QA (hera).
--The script is tagged FIX_21_INT_RTB because the mods are in DEV.  
--
--To deploy these changes to QA:
--      1. Deploy mod to QA
--      2. Copy mod to appropriate script (i.e. release_scripts.sql, grants.sql, etc.) and tag new version
--      3. Remove mod from this script and tag new version
--     

UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Next Day Delivery' WHERE SHIP_METHOD_ID='ND';

UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Two Day Delivery' WHERE SHIP_METHOD_ID='2F';

UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Standard Delivery' WHERE SHIP_METHOD_ID='GR';

UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Saturday Delivery' WHERE SHIP_METHOD_ID='SA';

