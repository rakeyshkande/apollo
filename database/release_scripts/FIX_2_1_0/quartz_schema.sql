--
-- A hint submitted by a user: Oracle DB MUST be created as "shared" and the
-- job_queue_processes parameter  must be greater than 2, otherwise a DB lock
-- will happen.   However, these settings are pretty much standard after any
-- Oracle install, so most users need not worry about this.
--

create user quartz_schema identified by values 'disabled' account lock default tablespace tools temporary tablespace temp profile default
quota unlimited on tools;

create user quartz identified by quartz default tablespace tools temporary tablespace temp profile default quota unlimited on tools;
grant create session to quartz;
grant create session to quartz_schema;


CREATE TABLE quartz_schema.qrtz_job_details (
   JOB_NAME  VARCHAR2(80) NOT NULL,
   JOB_GROUP VARCHAR2(80) NOT NULL,
   DESCRIPTION VARCHAR2(120) NULL,
   JOB_CLASS_NAME   VARCHAR2(128) NOT NULL,
   IS_DURABLE VARCHAR2(1) NOT NULL,
   IS_VOLATILE VARCHAR2(1) NOT NULL,
   IS_STATEFUL VARCHAR2(1) NOT NULL,
   REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
   JOB_DATA BLOB NULL,
constraint qrtz_job_details_PK PRIMARY KEY (JOB_NAME,JOB_GROUP));

CREATE SYNONYM quartz.qrtz_job_details FOR quartz_schema.qrtz_job_details;

grant select, insert, update, delete on quartz_schema.qrtz_job_details to quartz;

CREATE TABLE quartz_schema.qrtz_job_listeners (
   JOB_NAME  VARCHAR2(80) NOT NULL,
   JOB_GROUP VARCHAR2(80) NOT NULL,
   JOB_LISTENER VARCHAR2(80) NOT NULL,
   constraint qrtz_job_listeners_pk PRIMARY KEY (JOB_NAME,JOB_GROUP,JOB_LISTENER),
   constraint qrtz_job_listeners_fk1 FOREIGN KEY (JOB_NAME,JOB_GROUP)
      REFERENCES quartz_schema.QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP));

CREATE SYNONYM quartz.qrtz_job_listeners for quartz_schema.qrtz_job_listeners;

grant select, insert, update, delete on quartz_schema.qrtz_job_listeners to quartz;

CREATE TABLE quartz_schema.qrtz_triggers (
   TRIGGER_NAME VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   JOB_NAME  VARCHAR2(80) NOT NULL,
   JOB_GROUP VARCHAR2(80) NOT NULL,
   IS_VOLATILE VARCHAR2(1) NOT NULL,
   DESCRIPTION VARCHAR2(120) NULL,
   NEXT_FIRE_TIME NUMBER(13) NULL,
   PREV_FIRE_TIME NUMBER(13) NULL,
   TRIGGER_STATE VARCHAR2(16) NOT NULL,
   TRIGGER_TYPE VARCHAR2(8) NOT NULL,
   START_TIME NUMBER(13) NOT NULL,
   END_TIME NUMBER(13) NULL,
   CALENDAR_NAME VARCHAR2(80) NULL,
   MISFIRE_INSTR NUMBER(2) NULL,
   JOB_DATA BLOB NULL,
   constraint qrtz_triggers_pk PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
   constraint qrtz_triggers_fk1 FOREIGN KEY (JOB_NAME,JOB_GROUP)
      REFERENCES quartz_schema.QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP));

CREATE synonym quartz.qrtz_triggers for quartz_schema.qrtz_triggers;

grant select, insert, update, delete on quartz_schema.qrtz_triggers to quartz;

CREATE TABLE quartz_schema.qrtz_simple_triggers (
   TRIGGER_NAME VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   REPEAT_COUNT NUMBER(7) NOT NULL,
   REPEAT_INTERVAL NUMBER(12) NOT NULL,
   TIMES_TRIGGERED NUMBER(7) NOT NULL,
   constraint qrtz_simple_triggers_pk PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
   constraint qrtz_simple_triggers_fk1 FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES quartz_schema.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP));

CREATE synonym quartz.qrtz_simple_triggers for quartz_schema.qrtz_simple_triggers;

grant select, insert, update, delete on quartz_schema.qrtz_simple_triggers to quartz;

CREATE TABLE quartz_schema.qrtz_cron_triggers (
   TRIGGER_NAME VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   CRON_EXPRESSION VARCHAR2(80) NOT NULL,
   TIME_ZONE_ID VARCHAR2(80),
   constraint qrtz_cron_triggers_pk PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
   constraint qrtz_cron_triggers_fk1 FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES quartz_schema.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP));

CREATE synonym quartz.qrtz_cron_triggers for quartz_schema.qrtz_cron_triggers;

grant select, insert, update, delete on quartz_schema.qrtz_cron_triggers to quartz;

CREATE TABLE quartz_schema.qrtz_blob_triggers (
   TRIGGER_NAME VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   BLOB_DATA BLOB NULL,
   constraint qrtz_blob_triggers_pk PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP),
   constraint qrtz_blob_triggers_fk1 FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES quartz_schema.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP));

CREATE synonym quartz.qrtz_blob_triggers for quartz_schema.qrtz_blob_triggers;

grant select, insert, update, delete on quartz_schema.qrtz_blob_triggers to quartz;

CREATE TABLE quartz_schema.qrtz_trigger_listeners (
   TRIGGER_NAME  VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
   constraint qrtz_trigger_listeners_pk PRIMARY KEY (TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER),
   constraint qrtz_trigger_listeners_fk1 FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES quartz_schema.QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP));

CREATE synonym quartz.qrtz_trigger_listeners for quartz_schema.qrtz_trigger_listeners;

grant select, insert, update, delete on quartz_schema.qrtz_trigger_listeners to quartz;

CREATE TABLE quartz_schema.qrtz_calendars (
   CALENDAR_NAME  VARCHAR2(80) NOT NULL,
   CALENDAR BLOB NOT NULL,
   constraint qrtz_calendars_pk PRIMARY KEY (CALENDAR_NAME));

CREATE SYNONYM quartz.qrtz_calendars for quartz_schema.qrtz_calendars;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.qrtz_calendars TO QUARTZ;

CREATE TABLE quartz_schema.qrtz_paused_trigger_grps (
   TRIGGER_GROUP  VARCHAR2(80) NOT NULL,
   constraint qrtz_paused_trigger_grps_pk PRIMARY KEY (TRIGGER_GROUP));

CREATE synonym quartz.qrtz_paused_trigger_grps for quartz_schema.qrtz_paused_trigger_grps;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.qrtz_paused_trigger_grps TO QUARTZ;

CREATE TABLE quartz_schema.qrtz_fired_triggers (
   ENTRY_ID VARCHAR2(95) NOT NULL,
   TRIGGER_NAME VARCHAR2(80) NOT NULL,
   TRIGGER_GROUP VARCHAR2(80) NOT NULL,
   IS_VOLATILE VARCHAR2(1) NOT NULL,
   INSTANCE_NAME VARCHAR2(80) NOT NULL,
   FIRED_TIME NUMBER(13) NOT NULL,
   STATE VARCHAR2(16) NOT NULL,
   JOB_NAME VARCHAR2(80) NULL,
   JOB_GROUP VARCHAR2(80) NULL,
   IS_STATEFUL VARCHAR2(1) NULL,
   REQUESTS_RECOVERY VARCHAR2(1) NULL,
   constraint qrtz_fired_triggers_pk PRIMARY KEY (ENTRY_ID));

CREATE SYNONYM quartz.qrtz_fired_triggers for quartz_schema.qrtz_fired_triggers;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.qrtz_fired_triggers TO QUARTZ;

CREATE TABLE quartz_schema.qrtz_scheduler_state (
   INSTANCE_NAME VARCHAR2(80) NOT NULL,
   LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
   CHECKIN_INTERVAL NUMBER(13) NOT NULL,
   RECOVERER VARCHAR2(80) NULL,
   constraint qrtz_scheduler_state_pk PRIMARY KEY (INSTANCE_NAME));

CREATE SYNONYM quartz.qrtz_scheduler_state for quartz_schema.qrtz_scheduler_state;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.qrtz_scheduler_state TO QUARTZ;

CREATE TABLE quartz_schema.qrtz_locks (
   LOCK_NAME  VARCHAR2(40) NOT NULL,
   constraint qrtz_locks_pk PRIMARY KEY (LOCK_NAME));

CREATE SYNONYM quartz.qrtz_locks for quartz_schema.qrtz_locks;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.qrtz_locks TO QUARTZ;

INSERT INTO quartz_schema.qrtz_locks values('TRIGGER_ACCESS');

INSERT INTO quartz_schema.qrtz_locks values('JOB_ACCESS');

INSERT INTO quartz_schema.qrtz_locks values('CALENDAR_ACCESS');

INSERT INTO quartz_schema.qrtz_locks values('STATE_ACCESS');

INSERT INTO quartz_schema.qrtz_locks values('MISFIRE_ACCESS');

Create index quartz_schema.qrtz_job_details_n1 on quartz_schema.qrtz_job_details(REQUESTS_RECOVERY);

create index quartz_schema.qrtz_triggers_n1 on quartz_schema.qrtz_triggers(NEXT_FIRE_TIME);

create index quartz_schema.qrtz_triggers_n2 on quartz_schema.qrtz_triggers(TRIGGER_STATE);

create index quartz_schema.qrtz_triggers_n3 on quartz_schema.qrtz_triggers(NEXT_FIRE_TIME,TRIGGER_STATE);

create index quartz_schema.qrtz_triggers_n4 on quartz_schema.qrtz_triggers(IS_VOLATILE);

create index quartz_schema.qrtz_fired_triggers_n1 on quartz_schema.qrtz_fired_triggers(TRIGGER_NAME);

create index quartz_schema.qrtz_fired_triggers_n2 on quartz_schema.qrtz_fired_triggers(TRIGGER_GROUP);

create index quartz_schema.qrtz_fired_triggers_n3 on quartz_schema.qrtz_fired_triggers(TRIGGER_NAME,TRIGGER_GROUP);

create index quartz_schema.qrtz_fired_triggers_n4 on quartz_schema.qrtz_fired_triggers(IS_VOLATILE);

create index quartz_schema.qrtz_fired_triggers_n5 on quartz_schema.qrtz_fired_triggers(INSTANCE_NAME);

create index quartz_schema.qrtz_fired_triggers_n6 on quartz_schema.qrtz_fired_triggers(JOB_NAME);

create index quartz_schema.qrtz_fired_triggers_n7 on quartz_schema.qrtz_fired_triggers(JOB_GROUP);

create index quartz_schema.qrtz_fired_triggers_n8 on quartz_schema.qrtz_fired_triggers(IS_STATEFUL);

create index quartz_schema.qrtz_fired_triggers_n9 on quartz_schema.qrtz_fired_triggers(REQUESTS_RECOVERY);

CREATE TABLE "QUARTZ_SCHEMA"."GROUPS" 
 ("GROUP_ID"     VARCHAR2(32) NOT NULL,
  "DESCRIPTION"  VARCHAR2(256) NOT NULL, 
 CONSTRAINT GROUPS_PK PRIMARY KEY("GROUP_ID") USING index tablespace tools) tablespace tools;

 create synonym quartz.groups for quartz_schema.groups;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.groups TO QUARTZ;

CREATE TABLE "QUARTZ_SCHEMA"."PIPELINE" 
("PIPELINE_ID"           VARCHAR2(32) NOT NULL, 
 "GROUP_ID"              VARCHAR2(32) NOT NULL, 
 "DESCRIPTION"           VARCHAR2(256), 
 "TOOL_TIP"              VARCHAR2(256), 
 "DEFAULT_SCHEDULE"      VARCHAR2(80), 
 "DEFAULT_PAYLOAD"       VARCHAR2(4000), 
 "FORCE_DEFAULT_PAYLOAD" CHAR(1) DEFAULT 'N' NOT NULL, 
 "QUEUE_NAME"            VARCHAR2(32),
 CONSTRAINT PIPELINE_PK PRIMARY KEY("PIPELINE_ID", "GROUP_ID") USING INDEX TABLESPACE TOOLS,
 constraint PIPELINE_CK1 CHECK(force_default_payload IN ('Y', 'N') )) TABLESPACE TOOLS;

create synonym quartz.pipeline for quartz_schema.pipeline;

GRANT SELECT, INSERT, UPDATE, DELETE ON quartz_schema.pipeline TO QUARTZ;
CREATE SEQUENCE "QUARTZ_SCHEMA"."JOB_SQ" 
    start with 1
    INCREMENT BY 1  
    MAXVALUE 1.0E28 
    NOCYCLE 
    NOCACHE;

grant select on quartz_schema.job_sq to quartz; 

CREATE SEQUENCE "QUARTZ_SCHEMA"."TRIGGER_SQ" 
   start with 1
   INCREMENT BY 1
   mAXVALUE 1.0E28 
   NOCYCLE 
   NOCACHE;

 
grant select on quartz_schema.trigger_sq to quartz;
grant execute on quartz_schema.scheduler_pkg to quartz;
grant execute on events.post_a_message_flex to quartz;




