grant execute on ftd_apps.sp_get_vendor_list to osp;
grant execute on ftd_apps.sp_get_vp_overrides to osp;
grant execute on ftd_apps.sp_get_vendor_active_products to osp;
grant execute on ftd_apps.payment_method_is_cc to clean;
grant execute on ftd_apps.payment_method_is_cc to osp;
