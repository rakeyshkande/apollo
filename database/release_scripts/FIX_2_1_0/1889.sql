-- ATA scripting override for WOE
insert into ftd_apps.scripting_override
(trigger_id, trigger_type, page_name, field_name, script_text) values ('12279', 'SOURCE_CODE', 'BILLING', 'MEMBERSHIPID', 
'Can I have your ATA Rewards number?');

-- ATA  sequence
CREATE SEQUENCE clean.ata_record_sq 
minvalue 1 maxvalue 999999999999999 
start with 1 cycle order nocache;

-- Add ATA event to EVENTS schema:
insert into EVENTS.EVENTS 
  (EVENT_NAME, CONTEXT_NAME, DESCRIPTION, ACTIVE) 
values 
  ('POST-FILE-ATA', 'PARTNER-REWARD',
   'Partner Rewards Posting File ATA', 'Y');

-- Add ATA Partner to database:
INSERT INTO ftd_apps.partner_master
(partner_name, created_on, created_by, 
 updated_on, updated_by, file_sequence_prefix)
values ('ATA', sysdate, 'SYS', sysdate, 'SYS', 'ATA');

-- Add ATA FTP login secure config info to database:
insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) 
values ('partner_reward', 'ATA_OutboundFtpUser', 'abc', 'Outbound ftp username for ATA', sysdate, 'SYS', sysdate, 'SYS');

insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) 
values ('partner_reward', 'ATA_OutboundFtpPswd', 'abc', 'Outbound ftp password for ATA', sysdate, 'SYS', sysdate, 'SYS');

insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) 
values ('partner_reward', 'ATA_InboundFtpUser', 'abc', 'Inbound ftp username for ATA', sysdate, 'SYS', sysdate, 'SYS'); 

insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) 
values ('partner_reward', 'ATA_InboundFtpPswd', 'abc', 'Inbound ftp password for ATA', sysdate, 'SYS', sysdate, 'SYS');

