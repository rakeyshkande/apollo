
########## DEPLOYED PRIOR TO 10/27





--Add new table FTD_APPS.VENDOR_PRODUCT
CREATE TABLE FTD_APPS.VENDOR_PRODUCT (
   VENDOR_ID          VARCHAR2(5) NOT NULL, 
   PRODUCT_SUBCODE_ID VARCHAR2(10) NOT NULL, 
   VENDOR_SKU         VARCHAR2(25) NOT NULL, 
   VENDOR_COST        NUMBER(22,2) NOT NULL, 
   AVAILABLE          CHAR(1) DEFAULT 'Y' NOT NULL, 
   REMOVED            CHAR(1) DEFAULT 'N' NOT NULL,
   CREATED_BY         VARCHAR2(100) NOT NULL, 
   CREATED_ON         DATE DEFAULT SYSDATE NOT NULL, 
   UPDATED_BY         VARCHAR2(100) NOT NULL, 
   UPDATED_ON         DATE DEFAULT SYSDATE NOT NULL, 
CONSTRAINT VENDOR_PRODUCT_PK PRIMARY KEY(VENDOR_ID,PRODUCT_SUBCODE_ID) using index tablespace ftd_apps_indx,
CONSTRAINT VENDOR_PRODUCT_FK1 FOREIGN KEY(VENDOR_ID) REFERENCES FTD_APPS.VENDOR_MASTER(VENDOR_ID), 
CONSTRAINT VENDOR_PRODUCT_CK1 CHECK(available IN ('Y', 'N')),  
CONSTRAINT VENDOR_PRODUCT_CK2 CHECK(removed IN ('Y', 'N')))
TABLESPACE FTD_APPS_DATA;


create table FTD_APPS.VENDOR_PRODUCT_OVERRIDE 
(VENDOR_ID VARCHAR2(5)              not null, 
 PRODUCT_SUBCODE_ID VARCHAR2(10)    not null, 
 OVERRIDE_DATE DATE                 not null,
 CARRIER_ID VARCHAR2(10)            not null ,
constraint VENDOR_PRODUCT_OVERRIDE_PK 
   primary key (VENDOR_id, product_subcode_id, override_date) 
   using index 
   tablespace ftd_apps_indx);


ALTER TABLE FTD_APPS.VENDOR_PRODUCT_OVERRIDE
       ADD  ( CONSTRAINT VENDOR_PRODUCT_OVERRIDE_fk1
              FOREIGN KEY (VENDOR_ID)
                             REFERENCES ftd_apps.vendor_master) ;

grant references on venus.carriers to ftd_apps;
ALTER TABLE FTD_APPS.VENDOR_PRODUCT_OVERRIDE
       ADD  ( CONSTRAINT VENDOR_PRODUCT_OVERRIDE_fk2
              FOREIGN KEY (carrier_id)
                             REFERENCES venus.carriers) ;

CREATE INDEX ftd_apps.vendor_product_override_n1 ON ftd_apps.vendor_product_override
(vendor_id    ASC)       TABLESPACE ftd_apps_indx;

CREATE INDEX ftd_apps.vendor_product_override_n2 ON ftd_apps.vendor_product_override
(carrier_id    ASC)       TABLESPACE ftd_apps_indx;



-- Create sequence 
create sequence FTD_APPS.VENDOR_PRODUCT_OVERRIDE_SQ
start with 1
increment by 1;


--Add a new record into the FTD_APPS.GLOBAL_PARMS table.
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) VALUES
('SHIPPING_PARMS','LASTEST_CUTOFF_VENDOR','2330','SYS',SYSDATE,'SYS',SYSDATE); 


grant references on ftd_apps.ship_methods to venus;

CREATE TABLE VENUS.SDS_SHIP_METHOD_XREF
        (SDS_SHIP_METHOD    VARCHAR2(100) NOT NULL,
         FTD_SHIP_METHOD_ID VARCHAR2(10) NOT NULL,
         CARRIER_ID         VARCHAR2(10) NOT NULL,
         AIR_METHOD         CHAR(1) DEFAULT 'Y' NOT NULL,
    CONSTRAINT SDS_SHIP_METHOD_XREF_PK
             PRIMARY KEY(SDS_SHIP_METHOD)
             using index
             tablespace venus_indx,
    CONSTRAINT SDS_SHIP_METHOD_XREF_FK1
             FOREIGN KEY(FTD_SHIP_METHOD_ID)
             REFERENCES FTD_APPS.SHIP_METHODS(SHIP_METHOD_ID),
    CONSTRAINT SDS_SHIP_METHOD_XREF_FK2
             FOREIGN KEY(CARRIER_ID)
             REFERENCES VENUS.cARRIERS(CARRIER_ID),
    CONSTRAINT SDS_SHIP_METHOD_XREF_CK1
             CHECK(AIR_METHOD in ('Y', 'N')));

ALTER TABLE VENUS."SDS_SHIP_METHOD_XREF" 
    ADD ("ACTIVE" CHAR(1) DEFAULT 'Y' NOT NULL, 
    CONSTRAINT "SDS_SHIP_METHOD_XREF_CK2" CHECK(ACTIVE in ('Y', 'N')));



create index venus.sds_ship_method_xref_n1 on venus.sds_ship_method_xref(ftd_ship_method_id)
    tablespace venus_indx;

create index venus.sds_ship_method_xref_n2 on venus.sds_ship_method_xref(carrier_id)
    tablespace venus_indx;

create table VENUS.SDS_ERROR 
(error_id       number(11) not null, 
 source         varchar2(10) not null,
 disposition    VARCHAR2(32) not null, 
 page_support   CHAR(1) not null, 
 order_comment  VARCHAR2(4000) ,
constraint sds_error_pk primary key(error_id,source) using index tablespace venus_indx);

ALTER TABLE VENUS.SDS_ERROR
    ADD (CONSTRAINT SDS_ERROR_CK1 CHECK(SOURCE IN ('SDS', 'FEDEX')));


create sequence venus.sds_error_sq
start with 1
increment by 1
maxvalue 99999999999;


CREATE TABLE "FTD_APPS"."PRODUCT_SUBCODES$" 
   (	"PRODUCT_SUBCODE_ID"       VARCHAR2(10)   NOT NULL, 
	"PRODUCT_ID"               VARCHAR2(10)   NOT NULL, 
	"SUBCODE_DESCRIPTION"      VARCHAR2(100)  NOT NULL, 
	"SUBCODE_PRICE"            NUMBER(8,2), 
	"SUBCODE_REFERENCE_NUMBER" VARCHAR2(50), 
	"HOLIDAY_SKU"              VARCHAR2(6), 
	"HOLIDAY_PRICE"            NUMBER(8,2), 
	"ACTIVE_FLAG"              VARCHAR2(1), 
	"VENDOR_PRICE"             NUMBER(8,2), 
	"VENDOR_SKU"               VARCHAR2(25), 
	"DIM_WEIGHT"               VARCHAR2(10), 
	"DISPLAY_ORDER"            NUMBER(3,0)    NOT NULL,
        "CREATED_ON"               DATE, 
	"CREATED_BY"               VARCHAR2(100), 
	"UPDATED_ON"               DATE, 
	"UPDATED_BY"               VARCHAR2(100),
	"OPERATION$"               varchar2(7) NOT NULL, 
	"TIMESTAMP$"               date NOT NULL);


CREATE TABLE "FTD_APPS"."VENDOR_PRODUCT$" 
   (	"VENDOR_ID"             VARCHAR2(5)    NOT NULL, 
	"PRODUCT_SUBCODE_ID"    VARCHAR2(10)   NOT NULL, 
	"VENDOR_SKU"            VARCHAR2(25) , 
	"VENDOR_COST"           NUMBER(22,2)   NOT NULL, 
	"AVAILABLE"             CHAR(1)        NOT NULL, 
	"REMOVED"               CHAR(1)        NOT NULL,
        "CREATED_ON"            DATE           NOT NULL, 
	"CREATED_BY"            VARCHAR2(100)  NOT NULL, 
	"UPDATED_ON"            DATE           NOT NULL, 
	"UPDATED_BY"            VARCHAR2(100)  NOT NULL,
        "OPERATION$"            varchar2(7)    NOT NULL, 
	"TIMESTAMP$"            date           NOT NULL);

CREATE TABLE "FTD_APPS"."PRODUCT_MASTER$" 
   (	"PRODUCT_ID"                    VARCHAR2(10) NOT NULL, 
	"NOVATOR_ID"                    VARCHAR2(10), 
	"PRODUCT_NAME"                  VARCHAR2(25) NOT NULL, 
	"NOVATOR_NAME"                  VARCHAR2(100), 
	"STATUS"                        VARCHAR2(1) NOT NULL, 
	"DELIVERY_TYPE"                 VARCHAR2(1), 
	"CATEGORY"                      VARCHAR2(5), 
	"PRODUCT_TYPE"                  VARCHAR2(25), 
	"PRODUCT_SUB_TYPE"              VARCHAR2(25), 
	"COLOR_SIZE_FLAG"               VARCHAR2(1), 
	"STANDARD_PRICE"                NUMBER(8,2), 
	"DELUXE_PRICE"                  NUMBER(8,2), 
	"PREMIUM_PRICE"                 NUMBER(8,2), 
	"PREFERRED_PRICE_POINT"         NUMBER(2,0), 
	"VARIABLE_PRICE_MAX"            NUMBER(8,2), 
	"SHORT_DESCRIPTION"             VARCHAR2(80), 
	"LONG_DESCRIPTION"              VARCHAR2(1024), 
	"FLORIST_REFERENCE_NUMBER"      VARCHAR2(62), 
	"MERCURY_DESCRIPTION"           VARCHAR2(62), 
	"ITEM_COMMENTS"                 VARCHAR2(62), 
	"ADD_ON_BALLOONS_FLAG"          VARCHAR2(1), 
	"ADD_ON_BEARS_FLAG"             VARCHAR2(1), 
	"ADD_ON_CARDS_FLAG"             VARCHAR2(1), 
	"ADD_ON_FUNERAL_FLAG"           VARCHAR2(1), 
	"CODIFIED_FLAG"                 VARCHAR2(5), 
	"EXCEPTION_CODE"                VARCHAR2(1), 
	"EXCEPTION_START_DATE"          DATE, 
	"EXCEPTION_END_DATE"            DATE, 
	"EXCEPTION_MESSAGE"             VARCHAR2(280), 
	"VENDOR_ID"                     VARCHAR2(5), 
	"VENDOR_COST"                   NUMBER(8,2), 
	"VENDOR_SKU"                    VARCHAR2(25), 
	"SECOND_CHOICE_CODE"            VARCHAR2(2), 
	"DROPSHIP_CODE"                 VARCHAR2(2), 
	"DISCOUNT_ALLOWED_FLAG"         VARCHAR2(1), 
	"DELIVERY_INCLUDED_FLAG"        VARCHAR2(1), 
	"SERVICE_FEE_FLAG"              VARCHAR2(1), 
	"EXOTIC_FLAG"                   VARCHAR2(1), 
	"EGIFT_FLAG"                    VARCHAR2(1), 
	"COUNTRY_ID"                    VARCHAR2(2), 
	"ARRANGEMENT_SIZE"              VARCHAR2(25), 
	"ARRANGEMENT_COLORS"            VARCHAR2(255), 
	"DOMINANT_FLOWERS"              VARCHAR2(255), 
	"SEARCH_PRIORITY"               VARCHAR2(5), 
	"RECIPE"                        VARCHAR2(255), 
	"SUBCODE_FLAG"                  VARCHAR2(1), 
	"DIM_WEIGHT"                    VARCHAR2(10), 
	"NEXT_DAY_UPGRADE_FLAG"         VARCHAR2(1), 
	"CORPORATE_SITE"                VARCHAR2(1), 
	"UNSPSC_CODE"                   VARCHAR2(40), 
	"PRICE_RANK_1"                  VARCHAR2(1), 
	"PRICE_RANK_2"                  VARCHAR2(1), 
	"PRICE_RANK_3"                  VARCHAR2(1), 
	"SHIP_METHOD_CARRIER"           VARCHAR2(1), 
	"SHIP_METHOD_FLORIST"           VARCHAR2(1), 
	"SHIPPING_KEY"                  VARCHAR2(3), 
	"LAST_UPDATE"                   DATE, 
	"VARIABLE_PRICE_FLAG"           VARCHAR2(1), 
	"HOLIDAY_SKU"                   VARCHAR2(6), 
	"HOLIDAY_PRICE"                 NUMBER(8,2), 
	"CATALOG_FLAG"                  VARCHAR2(1), 
	"HOLIDAY_SECOND_CHOICE_CODE"    VARCHAR2(2), 
	"HOLIDAY_DELUXE_PRICE"          NUMBER(8,2), 
	"HOLIDAY_PREMIUM_PRICE"         NUMBER(8,2), 
	"HOLIDAY_START_DATE"            DATE, 
	"HOLIDAY_END_DATE"              DATE, 
	"MERCURY_SECOND_CHOICE"         VARCHAR2(2), 
	"MERCURY_HOLIDAY_SECOND_CHOICE" VARCHAR2(2), 
	"ADD_ON_CHOCOLATE_FLAG"         VARCHAR2(1), 
	"NO_TAX_FLAG"                   VARCHAR2(1), 
	"JCP_CATEGORY"                  VARCHAR2(1), 
	"CROSS_REF_NOVATOR_ID"          VARCHAR2(7), 
	"GENERAL_COMMENTS"              VARCHAR2(1024), 
	"SENT_TO_NOVATOR_PROD"          VARCHAR2(1), 
	"SENT_TO_NOVATOR_UAT"           VARCHAR2(1), 
	"SENT_TO_NOVATOR_TEST"          VARCHAR2(1), 
	"SENT_TO_NOVATOR_CONTENT"       VARCHAR2(1), 
	"COMPANY_ID"                    VARCHAR2(12), 
	"DEFAULT_CARRIER"               VARCHAR2(10), 
	"HOLD_UNTIL_AVAILABLE"          CHAR(1) NOT NULL,
	"LAST_UPDATE_USER_ID"           VARCHAR2(100), 
	"LAST_UPDATE_SYSTEM"            VARCHAR2(40), 
	"MONDAY_DELIVERY_FRESHCUT"      CHAR(1) NOT NULL, 
	"TWO_DAY_SAT_FRESHCUT"          CHAR(1) NOT NULL, 
	"WEBOE_BLOCKED"                 VARCHAR2(1) NOT NULL, 
	"EXPRESS_SHIPPING_ONLY"         CHAR(1) NOT NULL, 
	"OVER_21"                       CHAR(1) NOT NULL, 
	"SHIPPING_SYSTEM"               VARCHAR2(20) NOT NULL,
        "CREATED_ON"                    DATE, 
	"CREATED_BY"                    VARCHAR2(100), 
	"UPDATED_ON"                    DATE, 
	"UPDATED_BY"                    VARCHAR2(100),        
        "OPERATION$"                    VARCHAR2(7) not null, 
	"TIMESTAMP$"                    DATE not null); 




CREATE TABLE "CLEAN"."VENDOR_JDE_AUDIT_LOG" 
    (LOG_ID                NUMBER(13) NOT NULL, 
      vendor_id              varchar2(5) not null,
     filling_VENDOR        VARCHAR2(9) , 
     VENUS_ORDER_NUMBER    VARCHAR2(11) NOT NULL, 
     EXTERNAL_ORDER_NUMBER VARCHAR2(20), 
     SHIP_DATE             DATE NOT NULL, 
     DELIVERY_DATE         DATE NOT NULL, 
     VENDOR_SKU            VARCHAR2(20) NOT NULL, 
     PRODUCT_ID            VARCHAR2(10) NOT NULL, 
     ORIG_AMOUNT           NUMBER(11, 2) NOT NULL, 
     ADJ_AMOUNT            NUMBER(11, 2) NOT NULL, 
     ORDER_STATUS          VARCHAR2(30), 
     RECORD_TYPE           VARCHAR2(10) NOT NULL,
     CANCEL_REASON_CODE    VARCHAR2(3),
     CREATED_ON            DATE NOT NULL)
   tablespace clean_data;


ALTER TABLE clean.vendor_jde_audit_log
       ADD  ( CONSTRAINT vendor_jde_audit_log_pk 
              PRIMARY KEY (log_id)
      USING INDEX
      TABLESPACE clean_indx ) ;

ALTER TABLE clean.vendor_jde_audit_log
       ADD  ( CONSTRAINT vendor_jde_audit_log_ck1  CHECK(RECORD_TYPE IN('SHIP','CAN','ADJ')));


grant references on ftd_apps.product_master to clean;
grant references on ftd_apps.vendor_master to clean;

ALTER TABLE clean.vendor_jde_audit_log
       ADD  ( CONSTRAINT VENDOR_JDE_AUDIT_LOG_FK1 FOREIGN KEY(vendor_id) 
            REFERENCES FTD_APPS.vendor_master);


grant references on venus.cancel_reason_code_val to clean;
ALTER TABLE clean.vendor_jde_audit_log
       ADD  ( CONSTRAINT VENDOR_JDE_AUDIT_LOG_FK2 FOREIGN KEY(cancel_reason_code) 
            REFERENCES venus.cancel_reason_code_val);


create index clean.vendor_jde_audit_log_n1 on clean.vendor_jde_audit_log (vendor_id)
          tablespace clean_indx compute statistics;


create index clean.vendor_jde_audit_log_n2 on clean.vendor_jde_audit_log (product_id)
          tablespace clean_indx compute statistics;

create index clean.vendor_jde_audit_log_n3 on clean.vendor_jde_audit_log (cancel_reason_code)
          tablespace clean_indx compute statistics;


create sequence CLEAN.VENDOR_JDE_AUDIT_LOG_sq
start with 1
increment by 1
maxvalue 9999999999999;



CREATE TABLE "FTD_APPS"."PRODUCT_CHANGE$_ACCT"
   ("CHANGE_ID"    NUMBER(22),
    "UPDATED_BY"   VARCHAR2(100),
    "OPERATION$"  VARCHAR2(7),
    "TIMESTAMP$"  DATE)
  tablespace ftd_apps_data;

CREATE TABLE "FTD_APPS"."PRODUCT_MASTER$_ACCT"
   ("CHANGE_ID"        NUMBER(22),
    "PRODUCT_ID"       VARCHAR2(10),
   "NOVATOR_ID"        VARCHAR2(10),
   "STATUS"            VARCHAR2(1),
   "PRODUCT_TYPE"      VARCHAR2(25),
   "STANDARD_PRICE"    NUMBER(8,2),
   "SUBCODE_PRICE"     NUMBER(8,2),
   "DELUXE_PRICE"      NUMBER(8,2),
   "PREMIUM_PRICE"     NUMBER(8,2),
   "VENDOR_COST"       NUMBER(8,2),
   "SUBCODE_VENDOR_COST"  NUMBER(8,2),
   "DIM_WEIGHT"           VARCHAR2(10),
   "SUBCODE_DIM_WEIGHT"   VARCHAR2(10),
   "SHIP_METHOD_CARRIER"  VARCHAR2(1),
   "SHIP_METHOD_FLORIST"  VARCHAR2(1),
   "NO_TAX_FLAG"          VARCHAR2(1))
 tablespace ftd_apps_data;

CREATE TABLE "FTD_APPS"."VENDOR_PRODUCT$_ACCT"
   ("CHANGE_ID"      NUMBER(22),
    "VENDOR_ID"      VARCHAR2(5),
    "PRODUCT_ID"     VARCHAR2(10),
    "VENDOR_COST"     NUMBER(22,2),
    "AVAILABLE"     CHAR(1))
  tablespace ftd_apps_data;

 CREATE INDEX ftd_apps.product_change$_acct_n1 
   on ftd_apps.product_change$_acct  (change_id)
  TABLESPACE ftd_apps_indx;


 CREATE INDEX ftd_apps.product_change$_acct_n2 
   on ftd_apps.product_change$_acct  (TRUNC("TIMESTAMP$"))
  TABLESPACE ftd_apps_indx;

 CREATE INDEX ftd_apps.product_master$_acct_n1 
   on ftd_apps.product_master$_acct  (change_id)
  TABLESPACE ftd_apps_indx;

 CREATE INDEX ftd_apps.vendor_product$_acct_n1 
   on ftd_apps.vendor_product$_acct  (change_id)
  TABLESPACE ftd_apps_indx;

create sequence ftd_apps.product_change$_acct_sq
start with 1
increment by 1
maxvalue 9999999999999999999999;

CREATE TABLE VENUS.SDS_STATUS_MESSAGE
( ID                     NUMBER(22) NOT NULL,
  CARTON_NUMBER          VARCHAR2(11) NOT NULL,
  TRACKING_NUMBER        VARCHAR2(32),
  PRINT_BATCH_NUMBER     VARCHAR2(100),
  PRINT_BATCH_SEQUENCE   NUMBER(22),
  SHIP_DATE              DATE,
  STATUS                 varchar2(10) NOT NULL,
  STATUS_DATE            DATE   NOT NULL,
  PROCESSED              CHAR(1) DEFAULT 'N',
  MESSAGE_DIRECTION      VARCHAR2(8) DEFAULT 'INBOUND' NOT NULL,
  CREATED_ON             DATE,
  UPDATED_ON             DATE,
constraint sds_status_message_ck1 CHECK(processed in ('Y', 'N') ) ,
constraint sds_status_message_ck2 CHECK(status in ('CANCELED','SHIPPED','LABELED','REJECTED') ) )
tablespace venus_data;



ALTER TABLE venus.sds_status_message
       ADD  ( CONSTRAINT sds_status_message_pk 
              PRIMARY KEY (id)
      USING INDEX
      TABLESPACE venus_indx ) ;

ALTER TABLE VENUS."SDS_STATUS_MESSAGE" 
    ADD ( 
    CONSTRAINT "SDS_STATUS_MESSAGE_CK3" CHECK(message_direction in('INBOUND','OUTBOUND')));

create sequence venus.sds_status_message_sq
start with 1
increment by 1
maxvalue 9999999999999999999999;


CREATE TABLE VENUS.SDS_TRANSACTION 
   (ID            NUMBER(22) NOT NULL,
    VENUS_ID      VARCHAR2(20) NOT NULL, 
    CREATED_ON    DATE NOT NULL,
    REQUEST       CLOB, 
    RESPONSE      CLOB, 
    CONSTRAINT SDS_TRANSACTION_PK  PRIMARY KEY(ID) using index tablespace venus_indx); 


create sequence venus.sds_transaction_sq
start with 1
increment by 1
maxvalue 9999999999999999999999
cache 20;



VVVVVVVVVVVVVV START 10/27 BELOW HERE VVVVVVVVVVVVVVVVVVVV







-- BACKUP PRODUCT MASTER AND SUBCODES
create table ftd_apps.product_master_pre2_1_0 as select * from ftd_apps.product_master;
create table ftd_apps.product_subcodes_pre2_1_0 as select * from ftd_apps.product_subcodes;


-- THIS IS ALL OF THE VENUS.VENUS ALTERS MERGED INTO 1 SO WE DON'T HAVE TO WAIT MULT. TIMES

create index venus.venus_n9 on venus.venus(ship_date) tablespace venus_indx online compute statistics;
create index venus.venus_n10 on venus.venus(delivery_date) tablespace venus_indx online compute statistics;

ALTER TABLE VENUS.VENUS 
    ADD (SHIPPING_SYSTEM  VARCHAR2(20) DEFAULT 'SDS' NOT NULL, 
         TRACKING_NUMBER  VARCHAR2(100),
         PRINTED           DATE,
         SHIPPED           DATE,
         CANCELLED         DATE,
         REJECTED          DATE,
         FINAL_CARRIER     VARCHAR2(100), 
         FINAL_SHIP_METHOD VARCHAR2(100),
         SDS_STATUS        VARCHAR2(30),
         FORCED_SHIPMENT   CHAR(1) DEFAULT 'N' NOT NULL, 
         RATED_SHIPMENT    CHAR(1) DEFAULT 'N' NOT NULL, 
         batch_number      number(32), 
         batch_sequence    number(7),
         LAST_SCAN         DATE, 
         DELIVERY_SCAN     DATE,
         GET_SDS_RESPONSE  CHAR(1) DEFAULT 'N' NOT NULL, 
    CONSTRAINT VENUS_CK2 CHECK(shipping_system IN ('ESCALATE','FTP','SDS')),
    constraint venus_ck3 check(sds_status in ('AVAILABLE','NEW','SHIPPED','PRINTED','CANCELLED','REJECTED')),
    CONSTRAINT VENUS_CK4 CHECK(forced_shipment in ('Y','N')), 
    CONSTRAINT VENUS_CK5 CHECK(rated_shipment  in ('Y','N')),
    CONSTRAINT VENUS_CK6 CHECK(get_sds_response in ('Y','N'))
    );

ALTER TABLE VENUS.VENUS MODIFY("VENUS_STATUS" VARCHAR2(30));
    
UPDATE VENUS.VENUS SET SHIPPING_SYSTEM = 'ESCALATE' WHERE VENDOR_ID NOT IN ('00096','00119');
UPDATE VENUS.VENUS SET SHIPPING_SYSTEM = 'FTP' WHERE VENDOR_ID IN ('00096','00119');
UPDATE VENUS.VENUS SET SHIPPING_SYSTEM = 'ESCALATE' WHERE SHIPPING_SYSTEM = 'SDS';

--####2.1.0 QA DEPLOY FAILURE ON FOLLOWING UPDATE####
--UPDATE VENUS.VENUS v1 SET v1."SHIPPING_SYSTEM" = 
--    (SELECT v2.SHIPPING_SYSTEM FROM VENUS.VENUS v2 WHERE v1."VENUS_ORDER_NUMBER" = v2."VENUS_ORDER_NUMBER" AND MSG_TYPE='FTD' AND ROWNUM = 1)
--WHERE v1."VENDOR_ID" IS NULL AND v1."MSG_TYPE"<>'FTD';
--
--REPLACED WITH THIS ONE PER TIM  9/19 WITH DIRECTION TO USE IN PRODUCTION
Update venus.venus set shipping_system ='FTP'
WHERE FILLING_VENDOR = '90-9086SM' OR FILLING_VENDOR = '90-9086WI';

COMMIT;


comment on column venus.venus.final_carrier is 'data returned from SDS when using ''AUTO SHOP''';
comment on column venus.venus.final_ship_method is 'data returned from SDS when using ''AUTO SHOP''';


-- Get things named properly...

alter table venus.venus_corrupt rename constraint venus_ck1 to venus_corrupt_ck1;
alter table venus.venus_corrupt rename constraint venus_fk1 to venus_corrupt_fk1;
alter table venus.venus_corrupt rename constraint venus_fk2 to venus_corrupt_fk2;
alter table venus.venus_corrupt rename constraint venus_pk  to venus_corrupt_pk;
alter index venus.venus_n1 rename to venus_corrupt_n1;
alter index venus.venus_n2 rename to venus_corrupt_n2;
alter index venus.venus_n3 rename to venus_corrupt_n3;
alter index venus.venus_n4 rename to venus_corrupt_n4;
alter index venus.venus_n5 rename to venus_corrupt_n5;
alter index venus.venus_pk rename to venus_corrupt_pk;

alter table venus.venus rename constraint ovenus_fk1 to venus_fk1;
alter table venus.venus rename constraint ovenus_fk2 to venus_fk2;
alter index venus.ovenus_n1 rename to venus_n1;
alter index venus.ovenus_n2 rename to venus_n2;
alter index venus.ovenus_n3 rename to venus_n3;
alter index venus.ovenus_n4 rename to venus_n4;
alter index venus.ovenus_n5 rename to venus_n5;
alter index venus.ovenus_n6 rename to venus_n6;
alter index venus.ovenus_n7 rename to venus_n7;
alter index venus.ovenus_n8 rename to venus_n8;


-- Swap Unique key on venus.venus with PK so we can put FKs to it
drop index venus.ovenus_pk;

alter table venus.venus add (CONSTRAINT VENUS_PK PRIMARY KEY(VENUS_ID) using index tablespace venus_indx);



--Add a new record into the FTD_APPS.VENDOR_TYPES table.
INSERT INTO FTD_APPS.VENDOR_TYPES (VENDOR_CODE, DISPLAY_NAME, DESCRIPTION) VALUES
('SDS','SDS','Indicates orders that are sent to a vendor via SDS');

--Add a new record into the FTD_APPS.GLOBAL_PARMS table.
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) VALUES
('SHIPPING_PARMS','BACKEND_CUTOFF_DELAY','30','SYS',SYSDATE,'SYS',SYSDATE); 

--Modify check constraint VENDOR_MASTER_CK1 so that it allows the new vendor type of �SDS�
alter table FTD_APPS.VENDOR_MASTER DROP CONSTRAINT VENDOR_MASTER_CK1;
alter table FTD_APPS.VENDOR_MASTER ADD CONSTRAINT VENDOR_MASTER_CK1 CHECK (vendor_type IN ('ESCALATE','FTP','SDS'));


    
-- Add column VENDER_ID VARCHAR2(5) to FTD_APPS.INVENTORY_CONTROL
ALTER TABLE FTD_APPS.INVENTORY_CONTROL ADD (VENDOR_ID VARCHAR2(5));
CREATE INDEX ftd_apps.inventory_control_n1 ON ftd_apps.inventory_control(vendor_id) TABLESPACE ftd_apps_indx COMPUTE STATISTICS;
ALTER TABLE ftd_apps.inventory_control ADD (CONSTRAINT inventory_control_fk1 FOREIGN KEY (vendor_id) 
REFERENCES ftd_apps.vendor_master(vendor_id));

-- populate the vendor id field in inventory_control
update ftd_apps.inventory_control a set vendor_id = (
select b.vendor_id
from   ftd_apps.product_master b
where  a.product_id = b.product_id);

update ftd_apps.inventory_control a set vendor_id = (
select c.vendor_id from ftd_apps.product_subcodes b,ftd_apps.product_master c
where b.product_id = c.product_id
  and a.product_id = b.product_subcode_id)
where vendor_id is null;

PROMPT END OF PART 1 - 
PROMPT BEFORE PROCEEDING:  delete from ftd_apps.inventory_control where vendor_id is null and updated_by = 'CONVERSION';
PROMPT BEFORE PROCEEDING:  select count(*) from ftd_apps.inventory_control where vendor_id is null;
PROMPT BEFORE PROCEEDING:  COUNT SHOULD BE 0 - COMMIT AND PROCEED

PROMPT BEGIN PART 2

-- Add column vendor_id to the primary key
ALTER TABLE FTD_APPS.INVENTORY_CONTROL DROP PRIMARY KEY;
ALTER TABLE FTD_APPS.INVENTORY_CONTROL add (constraint INVENTORY_CONTROL_PK primary key (PRODUCT_ID, VENDOR_ID) 
using INDEX TABLESPACE FTD_APPS_INDX compute statistics);

--Alter the ship methods
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='1 day transit' WHERE SHIP_METHOD_ID='ND';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='2 days transit' WHERE SHIP_METHOD_ID='2F';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='3 days transit' WHERE SHIP_METHOD_ID='GR';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Allow Saturday Delivery' WHERE SHIP_METHOD_ID='SA';
commit;

--Remove the unavailable product exception
UPDATE FTD_APPS.PRODUCT_MASTER SET EXCEPTION_CODE=null WHERE EXCEPTION_CODE='N';
DELETE FROM FTD_APPS.EXCEPTIONS WHERE EXCEPTION_ID='U';
commit;

--Add an active flage to vendor_types
ALTER TABLE FTD_APPS.VENDOR_TYPES
    ADD (ACTIVE CHAR(1) DEFAULT 'Y' NOT NULL,
    CONSTRAINT VENDOR_TYPES_CK1 CHECK(ACTIVE IN ('Y','N')));

--Add Over 21 Flag
ALTER TABLE FTD_APPS.PRODUCT_MASTER ADD (OVER_21 CHAR(1) DEFAULT 'N' NOT NULL);
ALTER TABLE ftd_apps.product_master ADD (CONSTRAINT PRODUCT_MASTER_CK5 CHECK(over_21 IN ('Y','N')));
    
--Add shipping system to the product master table
ALTER TABLE FTD_APPS.PRODUCT_MASTER 
    ADD (SHIPPING_SYSTEM VARCHAR2(20) DEFAULT 'NONE' NOT NULL, 
    CONSTRAINT PRODUCT_MASTER_CK4 CHECK(shipping_system IN ('ESCALATE','FTP','SDS','NONE')));

ALTER TABLE FTD_APPS.PRODUCT_MASTER 
    ADD (created_on date,
         created_by varchar2(100),
         updated_on date,
         updated_by varchar2(100));

UPDATE FTD_APPS.PRODUCT_MASTER SET SHIPPING_SYSTEM = 'ESCALATE';
UPDATE FTD_APPS.PRODUCT_MASTER SET SHIPPING_SYSTEM = 'NONE' WHERE PRODUCT_TYPE = 'FLORAL';
--UPDATE FTD_APPS.PRODUCT_MASTER SET SHIPPING_SYSTEM = 'ESCALATE' WHERE VENDOR_ID IS NOT NULL AND VENDOR_ID NOT IN ('00096','00119');
UPDATE FTD_APPS.PRODUCT_MASTER SET SHIPPING_SYSTEM = 'FTP' WHERE VENDOR_ID IN ('00096','00119');

-- following should update 26 rows
update ftd_apps.product_master
set over_21 = 'Y'
where product_id in ('W206', 'W207', 'W208', 'W209', 'W201', 'W203', 'W205', 
'W202', 'W200', 'W204', 'W605', 'W600', 'W607', 'W212', 'W214', 'W213', 'W217', 
'W610', 'W611', 'W604', 'W606', 'W608', 
'W609', 'W601', 'W602', 'W603');


UPDATE FTD_APPS.PRODUCT_MASTER SET EXPRESS_SHIPPING_ONLY='N';
commit;


--Populate the Vendor_Product table
BEGIN
DECLARE
CURSOR vp_prod_cur IS 
(
SELECT VENDOR_ID, VENDOR_SKU, PRODUCT_ID, VENDOR_COST, DECODE(STATUS, 'A', 'Y', 'N') available, 'N' removed  
FROM FTD_APPS.PRODUCT_MASTER 
WHERE PRODUCT_TYPE IN ('FRECUT','SDFC','SDG','SPEGFT') 
AND SUBCODE_FLAG = 'N' 
--AND STATUS='A'
AND VENDOR_ID IS NOT NULL
--AND VENDOR_SKU IS NOT NULL
--AND VENDOR_COST IS NOT NULL
);
vp_prod_rec vp_prod_cur%ROWTYPE;
BEGIN
  OPEN vp_prod_cur;
  LOOP
    FETCH vp_prod_cur INTO vp_prod_rec;
    EXIT WHEN vp_prod_cur%NOTFOUND;
    --dbms_output.put_line(vp_prod_rec.VENDOR_ID || chr(9) || vp_prod_rec.PRODUCT_ID);
    INSERT INTO FTD_APPS.VENDOR_PRODUCT (VENDOR_ID, PRODUCT_SUBCODE_ID, VENDOR_SKU, VENDOR_COST, AVAILABLE, REMOVED, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
      VALUES(vp_prod_rec.VENDOR_ID,vp_prod_rec.PRODUCT_ID, vp_prod_rec.VENDOR_SKU,vp_prod_rec.VENDOR_COST,vp_prod_rec.available,vp_prod_rec.removed,'SYS',SYSDATE,'SYS',SYSDATE);
  END LOOP;
  CLOSE vp_prod_cur;
END;
END;
/
commit;

BEGIN
DECLARE
CURSOR vp_prod_cur IS 
(
SELECT pm.VENDOR_ID, s.VENDOR_SKU, s.PRODUCT_SUBCODE_ID AS PRODUCT_ID , s.VENDOR_PRICE AS VENDOR_COST, DECODE(pm.STATUS, 'A', 'Y', 'N') available, 'N' removed 
FROM FTD_APPS.PRODUCT_MASTER pm 
JOIN FTD_APPS."PRODUCT_SUBCODES" s ON pm."PRODUCT_ID" = s."PRODUCT_ID"
WHERE PRODUCT_TYPE IN ('FRECUT','SDFC','SDG','SPEGFT') 
AND SUBCODE_FLAG = 'Y' 
--AND STATUS='A'
AND pm.VENDOR_ID IS NOT NULL
and ((pm.vendor_id,s.product_subcode_id) not in 
      (select vendor_id,product_subcode_id 
      from ftd_apps.vendor_product 
      where vendor_id = pm.vendor_id and product_subcode_id = s.product_subcode_id))
and s.VENDOR_PRICE IS NOT NULL
);
vp_prod_rec vp_prod_cur%ROWTYPE;
BEGIN
  OPEN vp_prod_cur;
  LOOP
    FETCH vp_prod_cur INTO vp_prod_rec;
    EXIT WHEN vp_prod_cur%NOTFOUND;
    --dbms_output.put_line(vp_prod_rec.VENDOR_ID || chr(9) || vp_prod_rec.PRODUCT_ID);
    INSERT INTO FTD_APPS.VENDOR_PRODUCT (VENDOR_ID, PRODUCT_SUBCODE_ID, VENDOR_SKU, VENDOR_COST, AVAILABLE, REMOVED, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
      VALUES(vp_prod_rec.VENDOR_ID,vp_prod_rec.PRODUCT_ID, vp_prod_rec.VENDOR_SKU,vp_prod_rec.VENDOR_COST,vp_prod_rec.available,vp_prod_rec.removed,'SYS',SYSDATE,'SYS',SYSDATE);
  END LOOP;
  CLOSE vp_prod_cur;
END;
END;
/
COMMIT;


comment on column ftd_apps.product_master.vendor_id is 'no longer used';
comment on column ftd_apps.product_master.vendor_sku is 'no longer used';
comment on column ftd_apps.product_master.vendor_cost is 'no longer used';



update FTD_APPS."VENDOR_MASTER" set "CUTOFF_TIME" = '13:00' where "CUTOFF_TIME" is null;

commit;


ALTER TABLE FTD_APPS.VENDOR_MASTER
    ADD (ACCOUNTS_PAYABLE_ID VARCHAR2(8)  DEFAULT 'N/A' NOT NULL,
         GL_ACCOUNT_NUMBER   VARCHAR2(30) DEFAULT 'N/A' NOT NULL);


-- dml's performed on VENUS.CANCEL_REASON_CODE_VAL 
UPDATE VENUS.CANCEL_REASON_CODE_VAL SET STATUS = 'Inactive' WHERE CANCEL_REASON_CODE IN ('UPX', 'UPD', 'OTH');

UPDATE VENUS.CANCEL_REASON_CODE_VAL SET DESCRIPTION = 'Carrier Delivery/Shipping Issue' WHERE CANCEL_REASON_CODE IN ('SHP');

INSERT INTO VENUS.CANCEL_REASON_CODE_VAL(CANCEL_REASON_CODE, DESCRIPTION, STATUS) 
VALUES ('CVC', 'Customer/Vendor Cancelled', 'Active'); 

INSERT INTO VENUS.CANCEL_REASON_CODE_VAL(CANCEL_REASON_CODE, DESCRIPTION, STATUS) 
VALUES ('SUB', 'Substitution', 'Active');


         

ALTER TABLE FTD_APPS.SHIP_METHODS 
    ADD (MAX_TRANSIT_DAYS NUMBER(3) DEFAULT 1 NOT NULL,
         sds_ship_via     varchar2(20),
         sds_ship_via_air varchar2(20));

UPDATE FTD_APPS.SHIP_METHODS SET MAX_TRANSIT_DAYS = 2 WHERE SHIP_METHOD_ID = '2F';
UPDATE FTD_APPS.SHIP_METHODS SET MAX_TRANSIT_DAYS = 3 WHERE SHIP_METHOD_ID = 'GR';
UPDATE FTD_APPS.SHIP_METHODS SET MAX_TRANSIT_DAYS = 2 WHERE SHIP_METHOD_ID = 'SA';
UPDATE FTD_APPS.SHIP_METHODS SET "SDS_SHIP_VIA" = 'COM_1D', "SDS_SHIP_VIA_AIR" = 'COM_1D_AIR'  WHERE SHIP_METHOD_ID='ND';
UPDATE FTD_APPS.SHIP_METHODS SET "SDS_SHIP_VIA" = 'COM_2D', "SDS_SHIP_VIA_AIR" = 'COM_2D_AIR'  WHERE SHIP_METHOD_ID='2F';
UPDATE FTD_APPS.SHIP_METHODS SET "SDS_SHIP_VIA" = 'COM_3D', "SDS_SHIP_VIA_AIR" = 'COM_3D_AIR'  WHERE SHIP_METHOD_ID='GR';
UPDATE FTD_APPS.SHIP_METHODS SET "SDS_SHIP_VIA" = 'COM_SA', "SDS_SHIP_VIA_AIR" = 'COM_SA_AIR'  WHERE SHIP_METHOD_ID='SA';



alter table VENUS.VENUS 
   ADD CONSTRAINT VENUS_FK3 FOREIGN KEY (FINAL_SHIP_METHOD) 
   REFERENCES VENUS.SDS_SHIP_METHOD_XREF(SDS_SHIP_METHOD);
   
ALTER TABLE VENUS.CARRIERS 
    ADD (TRACKING_URL     varchar2(1000),
    ACTIVE CHAR(1) DEFAULT 'Y' NOT NULL
    CONSTRAINT CARRIERS_CK2 CHECK(ACTIVE IN ('Y', 'N')));



ALTER TABLE FTD_APPS.PRODUCT_SUBCODES ADD 
        ("CREATED_ON" DATE, 
	"CREATED_BY" VARCHAR2(100), 
	"UPDATED_ON" DATE, 
	"UPDATED_BY" VARCHAR2(100));




--Populate VENUS.SDS_SHIP_METHOD_XREF
INSERT INTO VENUS."CARRIERS" ("CARRIER_ID", "CARRIER_NAME", "GROUND_INDICATOR", "METHOD_OF_PAYMENT", "WEBSITE_URL", "PHONE_NUMBER", "TRACKING_URL", "ACTIVE") VALUES('UPS','UPS','Y','X','www.ups.com','1-800-PICK-UPS','http://www.ups.com/WebTracking/track','Y');

INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('EXPSAVPKG','GR','FEDEX','Y','Y');         
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('FRSTONTPKG','ND','FEDEX','Y','Y');        
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GROUND','GR','FEDEX','Y','Y');            
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('PRIO OVNT','ND','FEDEX','Y','Y');         
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SMARTPOST+','ND','FEDEX','Y','N');        
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('STNDONTPKG','ND','FEDEX','Y','Y');        
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('ECN2DAYPKG','2F','FEDEX','Y','Y');        
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSGR RES','GR','UPS','Y','Y');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSSTDCAN','ND','UPS','Y','N');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPS2DAM','2F','UPS','Y','Y');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPS3D COM','GR','UPS','Y','Y');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPS2D','2F','UPS','Y','Y');               
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSND','ND','UPS','Y','Y');               
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSGR COM','GR','UPS','Y','Y');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSND SAVE','ND','UPS','Y','Y');          
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPS3D RES','GR','UPS','Y','Y');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSWWEXD','ND','UPS','Y','N');            
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSWWEXS','ND','UPS','Y','N');            
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSWWEXSP','ND','UPS','Y','N');           
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('UPSNDAM','ND','UPS','Y','Y');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('EXP','ND','DHL','Y','Y');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('EXP AM','ND','DHL','Y','Y');              
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('EXP AM LTR','ND','DHL','Y','N');          
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('EXP LTR','ND','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GDS','GR','DHL','Y','Y');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GDS ABP','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GDS AHM','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GDS AMM','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GMP','ND','DHL','Y','N');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('GMS','ND','DHL','Y','N');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('INTDOC','ND','DHL','Y','N');              
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('INTDOC LTR','ND','DHL','Y','N');          
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('NAS','ND','DHL','Y','Y');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('NAS LTR','ND','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SDS','2F','DHL','Y','Y');                 
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SDS ABP','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SDS AHM','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SDS AMM','GR','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('SDS LTR','2F','DHL','Y','N');             
INSERT INTO VENUS."SDS_SHIP_METHOD_XREF" ("SDS_SHIP_METHOD", "FTD_SHIP_METHOD_ID", "CARRIER_ID", "AIR_METHOD", "ACTIVE") VALUES('WPX','ND','DHL','Y','N');                 
commit;

--Populate VENUS.SDS_ERROR table
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(999,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101001,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101002,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101011,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101012,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101013,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-101016,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50504,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50505,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50506,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50507,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50508,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50509,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50510,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50511,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50512,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50513,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50514,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50515,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50516,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50517,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50518,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50519,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50520,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50521,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50522,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50523,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50524,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50525,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50526,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50527,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50528,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50529,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50530,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50531,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50532,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50533,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50534,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50535,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50536,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50537,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50538,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50539,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50540,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50541,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50542,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50543,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50544,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50545,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50546,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50547,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50548,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50549,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50550,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50551,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50552,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50553,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50554,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50555,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50556,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50557,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50558,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50559,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50560,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50561,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50562,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50563,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50564,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50565,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50566,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50567,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50568,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50569,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50570,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50571,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50572,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50573,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50574,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50575,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50576,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50577,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50578,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50579,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50580,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50581,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50582,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50583,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50584,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50585,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50586,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50587,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50588,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50589,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50590,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50591,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50592,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50593,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50594,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50595,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50596,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50597,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50598,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50599,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50600,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50601,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50602,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50603,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50604,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50605,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50606,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50607,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50608,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50609,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50610,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50611,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50612,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50613,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50614,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50615,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50616,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50617,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50618,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50619,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50620,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50621,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50622,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50623,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50624,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50625,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50626,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50627,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50628,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50629,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50630,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50631,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50632,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50633,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50634,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50635,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50636,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50637,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50638,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50639,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50640,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50641,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50642,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50643,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50644,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50645,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50646,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50647,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50648,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50649,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50650,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50651,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50652,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50653,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50654,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50655,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50656,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50657,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50658,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50659,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50660,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50661,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50662,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50663,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50664,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50665,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50666,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50667,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50668,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50669,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50670,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50671,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50672,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50673,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50674,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-50675,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52001,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52002,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52003,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52004,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52005,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52006,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52007,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52008,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52009,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52010,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52011,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52012,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52013,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52014,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52015,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52016,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52017,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52018,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52019,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-52020,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-70801,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-70802,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-70803,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-70804,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-70805,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-72701,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-73701,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-73702,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-73703,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-74801,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-74802,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75201,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75202,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75203,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75204,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75205,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-75206,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-83101,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-83102,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-83103,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT") VALUES(-83106,'SDS','QUEUE','Y');
INSERT INTO VENUS."SDS_ERROR" ("ERROR_ID", "SOURCE", "DISPOSITION", "PAGE_SUPPORT", "ORDER_COMMENT") VALUES(-83173,'SDS','QUEUE','N','A carrier could not be found to ship this order.');
--commit;

INSERT INTO FRP."GLOBAL_PARMS" ("CONTEXT", "NAME", "VALUE", "CREATED_BY", "CREATED_ON", "UPDATED_BY", "UPDATED_ON") VALUES('SHIPPING_PARMS','COMM_ERROR_DELAY_MINUTES','5','SYS',SYSDATE,'SYS',SYSDATE);
INSERT INTO FRP."GLOBAL_PARMS" ("CONTEXT", "NAME", "VALUE", "CREATED_BY", "CREATED_ON", "UPDATED_BY", "UPDATED_ON") VALUES('SHIPPING_PARMS','MAX_SHIP_DAYS_OUT','10','SYS',SYSDATE,'SYS',SYSDATE);

UPDATE CLEAN."STOCK_EMAIL" 
SET 
  BODY = 'Attention!  The inventory for product ~inventory.product_id~ is now ~inventory.inventory_level~ for vendor ~inventory.vendor_id~.  An alert results when it is at or below ~inventory.threshold~.',
  SUBJECT = 'Product ~inventory.product_id~/~inventory.vendor_id~ threshold reached'
WHERE "TITLE" = 'INVENTORY CONTROL';
commit;


alter table ftd_apps.company_master add (sds_brand varchar2(2));


update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'AA';
update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'BB' WHERE COMPANY_ID = 'HIGH';
update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'FC' WHERE COMPANY_ID = 'FLORIST';
update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'FD' WHERE COMPANY_ID = 'FDIRECT';
update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'FU' WHERE COMPANY_ID = 'FUSA';
update FTD_APPS."COMPANY_MASTER" set "SDS_BRAND" = 'GC' WHERE COMPANY_ID = 'GIFT';

UPDATE FTD_APPS."VENDOR_TYPES" SET "DISPLAY_NAME" = 'ARGO' , "DESCRIPTION" = 'Indicates orders that are sent to a vendor via Argo' WHERE "VENDOR_CODE"='SDS';

INSERT INTO VENUS.VENDOR_CARRIER_REF (VENDOR_ID, CARRIER_ID, USAGE_PERCENT)
SELECT VENDOR_ID, 'UPS', 0 FROM FTD_APPS.VENDOR_MASTER;

COMMIT;

--Update the SDS_SHIP_METHOD_XREF table
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPS2DAM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPS2D';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='ECN2DAYPKG';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='SDS LTR';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='SDS';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPS3D COM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPSGR COM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='EXPSAVPKG';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPSGR RES';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='SDS ABP';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='SDS AHM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='SDS AMM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPS3D RES';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GDS';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GDS ABP';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GDS AHM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GDS AMM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GROUND';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='NAS';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='NAS LTR';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='FRSTONTPKG';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='WPX';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='PRIO OVNT';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='SMARTPOST+';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='STNDONTPKG';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPSSTDCAN';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPSND';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPSND SAVE';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPSWWEXD';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPSWWEXS';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='UPSWWEXSP';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='UPSNDAM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='EXP';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='EXP AM';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='EXP AM LTR';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'Y' WHERE "SDS_SHIP_METHOD"='EXP LTR';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GMP';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='GMS';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='INTDOC';
UPDATE VENUS."SDS_SHIP_METHOD_XREF" SET "AIR_METHOD" = 'N' WHERE "SDS_SHIP_METHOD"='INTDOC LTR';
commit;

-- Add column vendor_id
ALTER TABLE ftd_apps.inv_ctrl_notifications ADD ("VENDOR_ID" VARCHAR2(5));
  
-- populate column vendor_id
UPDATE ftd_apps.inv_ctrl_notifications a
SET a.vendor_id = (
	SELECT b.vendor_id
	  FROM ftd_apps.vendor_product b
	 WHERE a.product_id = b.product_subcode_id);
COMMIT;


-- QA DEPLOY - STILL SOME NULL VENDOR_IDS - ADD THESE 2 IS LIKE ABOVE TO SEE IF IT FILLS IN ALL VENDOR_IDS
-- ADDED TO SCRIPT AND EMAIL SENT TO TIM PETERSON FOR CONFIRMATION
update ftd_apps.inv_ctrl_notifications a set vendor_id = (
    select vendor_id from ftd_apps.product_master c
    where product_id = a.product_id)
where vendor_id is null;
update ftd_apps.inv_ctrl_notifications a set vendor_id = (
    select c.vendor_id from ftd_apps.product_subcodes b,ftd_apps.product_master c
    where b.product_id = c.product_id
      and a.product_id = b.product_subcode_id)
where vendor_id is null;

PROMPT ### 
PROMPT ### BEFORE PROCEEDING, RUN FOLLOWING TO CONFIRM NO NULL VENDOR_IDS IN INV_CTRL_NOTIFICATIONS
PROMPT ### 

-- make sure there are no NULL values in vendor_id. If so populate them, before you proceed.
SELECT * FROM FTD_APPS.INV_CTRL_NOTIFICATIONS WHERE VENDOR_ID IS NULL;

-- Add column vendor_id to the primary key
ALTER TABLE ftd_apps.inv_ctrl_notifications DROP PRIMARY KEY;
ALTER TABLE ftd_apps.inv_ctrl_notifications add (constraint INV_CTRL_NOTIFICATIONS_PK primary key 
	(PRODUCT_ID, EMAIL_ADDRESS, VENDOR_ID) 
	USING INDEX TABLESPACE FTD_APPS_INDX compute statistics); 

-- Add foreign key for vendor_id
ALTER TABLE FTD_APPS.INV_CTRL_NOTIFICATIONS 
    ADD (CONSTRAINT INV_CTL_NOTIFICATIONS_FK1 FOREIGN KEY(VENDOR_ID) 
    REFERENCES FTD_APPS.VENDOR_MASTER);

create index ftd_apps.inv_ctrl_notifications_n1 on ftd_apps.inv_ctrl_notifications (vendor_id)
          tablespace ftd_apps_indx;
    



ALTER TABLE "FTD_APPS"."PRODUCT_MASTER" 
    MODIFY("NEXT_DAY_UPGRADE_FLAG" DEFAULT 'Y');


insert into clean.comment_origin_val values ('ShipProc', 'Ship Processing');
commit;





-- dml's performed on VENUS.CANCEL_REASON_CODE_VAL
DELETE FROM VENUS.CANCEL_REASON_CODE_VAL WHERE CANCEL_REASON_CODE = 'CVC';

INSERT INTO VENUS.CANCEL_REASON_CODE_VAL(CANCEL_REASON_CODE, DESCRIPTION, STATUS) 
VALUES ('CCN', 'Customer Cancelled', 'Active');

INSERT INTO VENUS.CANCEL_REASON_CODE_VAL(CANCEL_REASON_CODE, DESCRIPTION, STATUS) 
VALUES ('VCN', 'Vendor Cancelled', 'Active');
commit;


-- update ftd_apps.vendor_master with data from supplier_ftd.vendor_data
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2062449', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0054AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990FE';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051066', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0094BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2012964', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6536BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071137', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0143AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071184', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0146AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071176', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0144AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071181', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0145AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087FG';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071295', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0150AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2071321', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0149AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2065101', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0093AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051066', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0094AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2022537', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6718FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2050932', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3301FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2065101', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0093FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051815', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1750FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051810', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3103FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2054147', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1925FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2052935', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2675FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2053815', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0017FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087FE';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087FF';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2062601', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0074FD';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2052935', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2675AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2068288', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0053AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2070959', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0142AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2070958', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0140AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2007591', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5348AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '1567206', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2873AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2012712', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0003AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2003187', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5413AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2009777', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1198AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2022537', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6718AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051084', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6726AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2012235', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6627AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2050932', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3301AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2054146', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2733AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2003186', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5355AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2009597', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1362AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051322', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3434AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '1563486', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1461AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2045668', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0005AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051815', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1750AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2004733', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6510AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2020093', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6544AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051811', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3053AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2009621', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5363AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051829', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3251AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051829', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3251AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051225', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3293AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051085', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3244AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2008512', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1826AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2012964', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6536AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2033969', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3277AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2008511', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1529AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2000319', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0001AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2011400', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6668AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2026771', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1057AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2007607', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1545AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051813', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3020AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051810', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3103AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051809', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0006AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2045283', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5330AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2045283', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5330AC';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2045283', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5330AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2008700', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1594AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2052935', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2675AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2008700', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0041AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087AC';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990AC';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051808', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0002AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2003911', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5322AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2007591', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-5348BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2022537', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-6718BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2050932', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3301BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051322', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3434BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051815', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1750BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990BA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2051810', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3103BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2052935', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-2675BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2044061', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1990BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087BB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2054147', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1925AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2053815', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0017AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2052818', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1834AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2033969', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3277AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2054147', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-1925AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2035619', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-3087AB';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2066505', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0107AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2065933', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0102AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2062600', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0073AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2062601', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0074AA';
 UPDATE FTD_APPS."VENDOR_MASTER" SET "ACCOUNTS_PAYABLE_ID" = '2046014', "GL_ACCOUNT_NUMBER" = '7810.5728' WHERE "MEMBER_NUMBER"='91-0116AA';

 COMMIT;



insert into EVENTS.EVENTS 
(EVENT_NAME, CONTEXT_NAME, DESCRIPTION, ACTIVE) 
values 
('LOAD-PRODUCT-DATA-FEED', 'PARTNER-REWARD', 
'Creates Product Data Feed for Channel Intelligence', 'Y');


ALTER TABLE VENUS.SDS_STATUS_MESSAGE 
    ADD (CONSTRAINT SDS_STATUS_MESSAGE_U1 UNIQUE(CARTON_NUMBER,STATUS_DATE,STATUS)
    using index tablespace venus_indx);


alter table VENUS.SDS_TRANSACTION add 
   (CONSTRAINT SDS_TRANSACTION_FK1 FOREIGN KEY(VENUS_ID) REFERENCES VENUS.VENUS(VENUS_ID)) ;

create index venus.sds_transaction_n1 on venus.sds_transaction(venus_id)
    tablespace venus_indx;

 
INSERT INTO VENUS."CARRIER_DELIVERY" ("CARRIER_DELIVERY", "DESCRIPTION", "CARRIER_ID", "SHIP_METHOD", "MAX_SHIP_DAYS") 
VALUES('2DU','Express 2 Day','UPS','2F',2);

INSERT INTO VENUS."CARRIER_DELIVERY" ("CARRIER_DELIVERY", "DESCRIPTION", "CARRIER_ID", "SHIP_METHOD", "MAX_SHIP_DAYS") 
VALUES('3DU','Express 3 Day','UPS','GR',3);

INSERT INTO VENUS."CARRIER_DELIVERY" ("CARRIER_DELIVERY", "DESCRIPTION", "CARRIER_ID", "SHIP_METHOD", "MAX_SHIP_DAYS") 
VALUES('NDU','Express Next Day','UPS','ND',1);

INSERT INTO VENUS."CARRIER_DELIVERY" ("CARRIER_DELIVERY", "DESCRIPTION", "CARRIER_ID", "SHIP_METHOD", "MAX_SHIP_DAYS") 
VALUES('POU','Express Priority Overnight','UPS','PO',1);


INSERT INTO VENUS."CARRIER_DELIVERY" ("CARRIER_DELIVERY", "DESCRIPTION", "CARRIER_ID", "SHIP_METHOD", "MAX_SHIP_DAYS") 
VALUES('SAU','Express Saturday Delivery','UPS','SA',2);


insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'vendor_batch_pkg.run_vendor_eod',
       'SUBMITTER',
       'clean.daemon_pkg.submit(''vendor_batch_pkg.run_vendor_eod'')' ,
       'SYS',sysdate,'SYS',sysdate from dual;
       
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'vendor_batch_pkg.run_vendor_eod',
       'REMOVER',
       'clean.daemon_pkg.remove(''vendor_batch_pkg.run_vendor_eod'')' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'vendor_batch_pkg.run_vendor_eod',
       'INTERVAL',
       'TRUNC(SYSDATE)+1+(1410/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Next Day Delivery' WHERE SHIP_METHOD_ID='ND';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Two Day Delivery' WHERE SHIP_METHOD_ID='2F';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Standard Delivery' WHERE SHIP_METHOD_ID='GR';
UPDATE FTD_APPS.SHIP_METHODS SET DESCRIPTION='Saturday Delivery' WHERE SHIP_METHOD_ID='SA';
commit;



--DEFECT 1837
From Ithaca:  @events_plsql/ins_partner_reward_productfeed.prc

insert into events.events
  (event_name, context_name, description, active)
values
  ('LOAD-PRODUCT-DATA-FEED', 'PARTNER-REWARD', ' Creates Product Data Feed for Channel Intelligence', 'Y');



insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_productfeed',
       'SUBMITTER',
       'events.daemon_pkg.submit(''ins_partner_reward_productfeed'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;
       
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_productfeed',
       'REMOVER',
       'events.daemon_pkg.remove(''ins_partner_reward_productfeed'')' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_productfeed',
       'INTERVAL',
        'trunc(sysdate)+1+(30/1440)',
       'SYS',sysdate,'SYS',sysdate from dual;

exec frp.jobs_pkg.submit('ins_partner_reward_productfeed');
-- END DEFECT 1837





--#### DO LAST - MAKE SURE ALL OBJECTS ARE ANALYZED ####
select 'analyze table ' || owner || '.' || table_name || ' estimate statistics;'
from dba_tables where last_analyzed is null
and owner not in ('SYS','SYSTEM','OPS$BBLOUNT','CREYES','POP','OPS$ORACLE',
        'OPS$ESO','OPS$MWOYTUS','MMORAN','DBSNMP','WMSYS','DDESAI','CDC_SOURCE',
        'CDC_SOURCE_PUB','OPS$SYEAZEL','JWEISS')
order by owner,table_name
/


select 'analyze index ' || owner || '.' || index_name || ' estimate statistics;'
from dba_indexes where last_analyzed is null
and owner not in ('SYS','SYSTEM','OPS$BBLOUNT','CREYES','POP','OPS$ORACLE',
        'OPS$ESO','OPS$MWOYTUS','MMORAN','DBSNMP','WMSYS','DDESAI','CDC_SOURCE',
        'CDC_SOURCE_PUB','OPS$SYEAZEL','JWEISS')
and index_name not like 'SYS%'
order by owner,index_name
/



--############### GRANTS ################


grant select on ftd_apps.vendor_product to global;
GRANT SELECT ON FTD_APPS.VENDOR_PRODUCT TO RPT;
GRANT SELECT ON VENUS.VENDOR_CARRIER_REF TO RPT;
grant select on ftd_apps.vendor_product   to clean;
grant select on ftd_apps.product_subcodes to clean;
grant select,insert,update on venus.sds_status_message to venus;
grant select on ftd_apps.vendor_master to venus;
grant select on ftd_apps.ship_methods to venus;
GRANT SELECT ON VENUS.SDS_SHIP_METHOD_XREF TO RPT;
GRANT SELECT ON clean.vendor_jde_audit_log TO RPT;
grant select on ftd_apps.VENDOR_MASTER to venus;
grant select on ftd_apps.VENDOR_PRODUCT to venus;
grant select on ftd_apps.VENDOR_PRODUCT_OVERRIDE to venus;
grant select on ftd_apps.PRODUCT_SUBCODES to venus;
grant select on ftd_apps.product_master to venus;
grant select on ftd_apps.product_ship_methods to venus;
grant update on venus.venus to clean;
grant select on ftd_apps.VENDOR_DELIV_BLOCK_LIST_VW to venus;
grant select on ftd_apps.VENDOR_SHIP_BLOCK_LIST_VW to venus;
grant execute on venus.ship_pkg to ftd_apps;
GRANT EXECUTE ON VENUS.VENUS_PKG TO CLEAN;
GRANT EXECUTE ON FTD_APPS.SP_GET_CARRIER_LIST TO OSP;

-- THE FOLLOWING WILL FAIL BECAUSE THE OBJECTS ARE NOT CREATED YET
grant select on ftd_apps.prod_ship_block_list_vw to global;
grant select on ftd_apps.prod_deliv_block_list_vw to global;
grant execute on clean.vendor_batch_pkg to osp;
GRANT EXECUTE ON FTD_APPS.GET_MAX_VENDOR_CUTOFF TO OSP;
GRANT EXECUTE ON FTD_APPS.SP_UPDATE_PRODUCT_OVERRIDE TO OSP;
GRANT EXECUTE ON FTD_APPS.SP_DELETE_PRODUCT_OVERRIDE TO OSP;







#######################
### THE FOLLOWING WAS A LATE 2.1 DEPLOY AFTER DEVELOPMENT NOTICED IT WAS MISSING  11/20/2006
#######################
### Set up ATA as a partner

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_ata',
       'SUBMITTER',
       'events.daemon_pkg.submit(''ins_partner_reward_ata'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;
       
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_ata',
       'REMOVER',
       'events.daemon_pkg.remove(''ins_partner_reward_ata'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_partner_reward_ata',
       'INTERVAL',
       'trunc(next_day(sysdate,''WEDNESDAY''))+(20/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_ata',
       'SUBMITTER',
       'events.daemon_pkg.submit(''ins_response_ata'',null,null)' ,
       'SYS',sysdate,'SYS',sysdate from dual;
       
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_ata',
       'REMOVER',
       'events.daemon_pkg.remove(''ins_response_ata'')',
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ins_response_ata',
       'INTERVAL',
       'trunc(next_day(sysdate,''FRIDAY''))+(20/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

exec frp.jobs_pkg.submit('ins_partner_reward_ata');
exec frp.jobs_pkg.submit('ins_response_ata');






--end of file




