insert into events.events (event_name,context_name,description,active)
values ('POST-FILE-CONTINENTAL-AIRLINES','PARTNER-REWARD','Partner Rewards Posting File Continental Airlines','Y');

insert into frp.global_parms values ('ins_partner_reward_continental','REMOVER',
'events.daemon_pkg.remove(''ins_partner_reward_continental'')');

insert into frp.global_parms values ('ins_partner_reward_continental','SUBMITTER',
'events.daemon_pkg.submit(''ins_partner_reward_continental'',null,null)');

insert into frp.global_parms values ('ins_partner_reward_continental','INTERVAL',
'case when to_number(to_char(sysdate,''dd'')) < 5 then trunc(sysdate,''mm'')+4+(20/1440) when to_number(to_char(sysdate,''dd'')) > 15 then add_months(trunc(sysdate,''mm''),1)+4+(20/1440) else trunc(sysdate,''mm'')+14+(20/1440) end';

insert into events.events (event_name,context_name,description,active)
values ('RESPONSE-FILE-CONTINENTAL-AIRLINES','PARTNER-REWARD','Partner Rewards Response File Continental Airlines','Y');

insert into frp.global_parms values ('ins_response_continental','REMOVER',
'events.daemon_pkg.remove(''ins_response_continental'')');

insert into frp.global_parms values ('ins_response_continental','SUBMITTER',
'events.daemon_pkg.submit(''ins_response_continental'',null,null)');

insert into frp.global_parms values ('ins_response_continental','INTERVAL',
'case when to_number(to_char(sysdate,''dd'')) < 7 then trunc(sysdate,''mm'')+6+(12/24)+(20/1440) when to_number(to_char(sysdate,''dd'')) > 17 then add_months(trunc(sysdate,''mm''),1)+6+(12/24)+(20/1440) else trunc(sysdate,''mm'')+16+(12/24)+(20/1440) end';

update ftd_apps.partner_master
set file_sequence_prefix = 'CONT'
where partner_name = 'CONTINENTAL AIRLINES';

CREATE SEQUENCE clean.cont_record_sq minvalue 1 maxvalue 9999999999999 increment by 1 start with 1 cycle nocache;