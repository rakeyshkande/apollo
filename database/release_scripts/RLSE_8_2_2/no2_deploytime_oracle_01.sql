
 
 ------------------------------------------------------------------------------
-- begin requested by Gunadeep B    ,                      9/18/2015  --------
-- modified by Pavan
-- defect Addon adjustment#prod issue  (Pavan)                    --------------------------
------------------------------------------------------------------------------
alter table ptn_pi.partner_mapping add (INCLUDE_ADDON varchar2(10) default 'N');
------------------------------------------------------------------------------
-- end   requested by Gunadeep B     ,                      9/18/2015  --------
-- defect Addon adjustment#prod issue (Pavan)                 --------------------------
------------------------------------------------------------------------------