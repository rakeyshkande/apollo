
 
 ------------------------------------------------------------------------------
-- begin requested by Gunadeep B    ,                      9/18/2015  --------
-- modified by Pavan
-- defect Addon adjustment#prod issue  (Pavan)                    --------------------------
------------------------------------------------------------------------------
use ptn_pi;
alter table partner_mapping add INCLUDE_ADDON varchar(10) default 'N';
------------------------------------------------------------------------------
-- end   requested by Gunadeep B     ,                      9/18/2015  --------
-- defect Addon adjustment#prod issue (Pavan)                 --------------------------
------------------------------------------------------------------------------
