------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/10/2015  --------
-- defect DI-14         (Mark      )   (158852 )    --------------------------------
------------------------------------------------------------------------------------

create table venus.vendor_order_status (
ORDER_STATUS_ID integer not null,
VENUS_ORDER_NUMBER varchar2(11) not null,
PARTNER_ORDER_NUMBER number(12,0) not null,
PARTNER_ID           varchar2(100) not null,
TRACKING_NUMBER      varchar2(100),
CARRIER              varchar2(100),
ORDER_STATUS         varchar2(30) not null,
PRINTED              date,
SHIPPED              date,
DELIVERED            date,
REJECTED             date,
REJECT_CODE          integer,
REJECT_MESSAGE       varchar2(4000),
STATUS               varchar2( 100) not null,
CREATED_ON           date not null,
CREATED_BY           varchar2(100) not null,
UPDATED_ON           date not null,
UPDATED_BY           varchar2(100) not null) tablespace venus_data;

alter table venus.vendor_order_status add constraint vendor_order_status_pk primary key (order_status_id)
using index tablespace venus_indx;

create index venus.vendor_order_status_n1 on venus.vendor_order_status (venus_order_number) tablespace venus_indx;
create index venus.vendor_order_status_n2 on venus.vendor_order_status (created_on        ) tablespace venus_indx;
create index venus.vendor_order_status_n3 on venus.vendor_order_status (tracking_number,carrier,order_status) 
tablespace venus_indx;

alter table venus.vendor_order_status add constraint vendor_order_status_ck1 check (status in ('NEW','PROCESSED'));

create sequence venus.vendor_order_status_id_sq cache 200;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                8/10/2015  --------
-- defect DI-14         (Mark      )   (158852 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                                8/18/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('GLOBAL_CONFIG', 'FTDW_LATE_CUTOFF_HOURS', '1200', 'DI-8', SYSDATE, 'DI-8', SYSDATE, 
'specifies the late cutoff time for all specialty gifts and freshcut products assigned to the FTD West vendor.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('GLOBAL_CONFIG', 'FTDW_LATE_CUTOFF_FEE', '4.99', 'DI-8', SYSDATE, 'DI-8', SYSDATE,
'The dollar value that will be charged to the customer when Late Cutoff � FTD West is enabled.');

------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                                8/18/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                               8/26/2015  --------
-- defect DI-9         (Mark  ) (161349)            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('SHIPPING_PARMS', 'FTDWEST_CUSTOMER_ID', 'REPLACEME', 'DI-9', sysdate, 'DI-9', sysdate,
        'Customer ID recognized by FTD West for all CreateOrder requests');

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('SHIPPING_PARMS', 'FTDWEST_SERVICE_URL', 'REPLACEME', 'DI-9', sysdate, 'DI-9', sysdate,
        'Web Service URL for FTD West Order Service');

declare
   v_key_name varchar2(30);
begin

   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_APP_TOKEN',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Application Token required by FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_LOGIN',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Login/email for FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_PASSWORD',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Password for FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

end;
/

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                               8/26/2015  --------
-- defect DI-9         (Mark  ) (161349)            --------------------------------
------------------------------------------------------------------------------------

