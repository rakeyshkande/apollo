------------------------------------------------------------------------------------
-- begin requested by Sri Meka,                                  8/14/2015  --------
-- defect DI-8          (Mark      )   (159900 )    --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
ALTER TABLE `snh`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `snh`    ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `snh_hist`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `snh_hist`    ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `service_fee_override`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `service_fee_override`   ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `service_fee_override_hist`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `service_fee_override_hist`   ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
--Product
ALTER TABLE `product_master`  ADD COLUMN `PQUAD_PRODUCT_ID` INT(32) DEFAULT NULL AFTER `PRODUCT_ID`;
 
--Add-on
ALTER TABLE `addon`  ADD COLUMN PQUAD_ACCESSORY_ID INT(32) DEFAULT NULL;
ALTER TABLE `addon`  ADD COLUMN `IS_FTD_WEST_ADDON` CHAR(1) DEFAULT NULL;

------------------------------------------------------------------------------------
-- begin requested by Sri Meka,                                  8/14/2015  --------
-- defect DI-8          (Mark      )   (159900 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE occasion ADD COLUMN FTDWEST_ID BIGINT DEFAULT NULL;

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by DharmaTeja,                                8/27/2015  --------
-- defect DI-43        (Pavan      )   (  161371 )    ------------------------------
------------------------------------------------------------------------------------

Alter table vendor_product MODIFY VENDOR_SKU VARCHAR(32) default null;

------------------------------------------------------------------------------------
-- end   requested by DharmaTeja,                                8/27/2015  --------
-- defect DI-43        (Pavan      )   (  161371 )    ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )     --------------------------------
------------------------------------------------------------------------------------

use venus;
ALTER TABLE venus ADD column FTDWEST_ORDER_ID varchar(20) default null;

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk,                                  8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )     --------------------------------
------------------------------------------------------------------------------------

