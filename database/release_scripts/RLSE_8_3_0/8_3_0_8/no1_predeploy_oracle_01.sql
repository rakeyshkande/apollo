------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk ,                               9/21/2015  --------
-- defect DI-28         (Syed  ) (163899 )          --------------------------------
------------------------------------------------------------------------------------

set sqlblanklines on


--************************************************************************************************************************************************
--TRACKING NUMBER GENERIC
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER GENERIC',
'This email confirms that your order has shipped.    

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact us at ~phone2~

Sincerely,
~companyname~

Contact Us:
E-mail:   http://~cslink~
Phone:    ~phone~
Shop:     http://~website~ 
',
'Tracking Number',
SYSDATE,
'DI-28',
SYSDATE,
'DI-28',
'Y',
'N',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', 'NULL', null from clean.stock_email where TITLE = 'TRACKING NUMBER GENERIC';

--************************************************************************************************************************************************
--TRACKING NUMBER GENERIC.SCI
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER GENERIC.SCI',
'This email confirms that your order has shipped. 

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact us at 
~sci.phone~

Thank you for shopping with us.
Online: ~sci.email.link~
Phone: ~sci.phone~
',
'Tracking Number',
SYSDATE,
'DI-28',
SYSDATE,
'DI-28',
'Y',
'N',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'SCI' from clean.stock_email where TITLE = 'TRACKING NUMBER GENERIC.SCI';

--************************************************************************************************************************************************
--TRACKING NUMBER GENERIC.SYMPATHY
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER GENERIC.SYMPATHY',
'This email confirms that your order has shipped. 

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact us at 
~sympathy.phone~

Thank you for shopping with us.
Online: ~sympathy.email.link~
Phone: ~sympathy.phone~
',
'Tracking Number',
SYSDATE,
'DI-28',
SYSDATE,
'DI-28',
'Y',
'N',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'SYMPATHY' from clean.stock_email where TITLE = 'TRACKING NUMBER GENERIC.SYMPATHY';

--************************************************************************************************************************************************
--TRACKING NUMBER GENERIC.USAA
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'TRACKING NUMBER GENERIC.USAA',
'This email confirms that your order has shipped. 

If you have any questions regarding the shipping or delivery status of your order, please do not reply to this email.  You can contact us at 
~usaa.phone~

Thank you for shopping with us.
Online: ~usaa.email.link~
Phone: ~usaa.phone~
',
'Tracking Number',
SYSDATE,
'DI-28',
SYSDATE,
'DI-28',
'Y',
'N',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'USAA' from clean.stock_email where TITLE = 'TRACKING NUMBER GENERIC.USAA';


commit;


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk ,                                 9/21/2015  --------
-- defect DI-28         (Syed  ) (163899 )          --------------------------------
------------------------------------------------------------------------------------





------------------------------------------------------------------------------------
-- begin requested by Karthikeyan D ,                               9/25/2015  --------
-- defect DI-47         (Pavan  ) (164787 )          --------------------------------
------------------------------------------------------------------------------------
 
insert into FTD_APPS.CONTENT_MASTER values (ftd_apps.content_master_sq.nextval, 'SUNDAY_UPCHARGE', 'CONFIRMATION_EMAIL_LABEL',
'Sunday Upcharge display label', null, null, 'Defect_DI-47', sysdate, 'Defect_DI-47', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'SUNDAY_UPCHARGE' AND CONTENT_NAME = 'CONFIRMATION_EMAIL_LABEL'),
    null,
    null,
    'Sunday Charge',
    'Defect_DI-47', sysdate, 'Defect_DI-47', sysdate);


------------------------------------------------------------------------------------
-- End requested by Karthikeyan D,                                 9/25/2015  --------
-- defect DI-47          (Pavan  ) (164787 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B,                               9/25/2015  --------
-- defect DI-26         (Pavan  ) (164794 )          --------------------------------
------------------------------------------------------------------------------------
insert into JOE.element_config values ('JOE','carrierDeliveryDateSU','DETAIL',null,null,'N',null,null,'N',null,null,'You must select a delivery date option',
'Available Sunday delivery dates',null,'2322','-1',null,null,0,0,'N','N',null,null,'N');

insert into JOE.element_config values ('JOE','carrierDeliveryOptSU','DETAIL','carrierDeliveryOptSULabel',null,'Y','JOE_REQUIRED.isCarrierDelivery','FUNCTION',
'Y','JOE_VALIDATE.carrierDeliveryOptions','FUNCTION','You must select a delivery option','Sunday delivery',null,'2321','2321',null,null,0,0,'Y','N',null,null,'N');


------------------------------------------------------------------------------------
-- End requested by Gunadeep B,                                 9/25/2015  --------
-- defect DI-26          (Pavan  ) (164794 )          --------------------------------
------------------------------------------------------------------------------------

