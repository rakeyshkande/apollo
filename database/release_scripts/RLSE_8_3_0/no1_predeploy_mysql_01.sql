------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;

CREATE TABLE product_ship_methods (
 PRODUCT_ID varchar(10) NOT NULL,
 SHIP_METHOD_ID varchar(10) NOT NULL,
 DATE_LAST_MODIFIED varchar(10) DEFAULT NULL,
 DEFAULT_CARRIER varchar(10) DEFAULT NULL,
 PRIMARY KEY (PRODUCT_ID,SHIP_METHOD_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



use frp;


CREATE TABLE global_parms$ (
 CONTEXT varchar(100) NOT NULL,
 NAME varchar(100) NOT NULL,
 VALUE varchar(1000) DEFAULT NULL,
 CREATED_BY varchar(100) NOT NULL,
 CREATED_ON datetime NOT NULL,
 UPDATED_BY varchar(100) NOT NULL,
 UPDATED_ON datetime NOT NULL,
 OPERATION$ varchar(7) NOT NULL,
 TIMESTAMP$ datetime NOT NULL,
 KEY idx_comp_n1 (CONTEXT,NAME,OPERATION$,TIMESTAMP$)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;


------------------------------------------------------------------------------------
-- End requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
------------------------------------------------------------------------------------

