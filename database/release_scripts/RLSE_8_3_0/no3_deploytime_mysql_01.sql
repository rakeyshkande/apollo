------------------------------------------------------------------------------------
-- begin requested by Sri Meka,                                  8/14/2015  --------
-- defect DI-8          (Mark      )   (159900 )    --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
ALTER TABLE `snh`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `snh`    ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `snh_hist`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `snh_hist`    ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `service_fee_override`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `service_fee_override`   ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
ALTER TABLE `service_fee_override_hist`    ADD COLUMN `VENDOR_SUN_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `SAME_DAY_UPCHARGE`;
ALTER TABLE `service_fee_override_hist`   ADD COLUMN `VENDOR_MON_UPCHARGE` DECIMAL(5,2) DEFAULT NULL 
AFTER `VENDOR_SUN_UPCHARGE`;
 
--Product
ALTER TABLE `product_master`  ADD COLUMN `PQUAD_PRODUCT_ID` INT(32) DEFAULT NULL AFTER `PRODUCT_ID`;
 
--Add-on
ALTER TABLE `addon`  ADD COLUMN PQUAD_ACCESSORY_ID INT(32) DEFAULT NULL;
ALTER TABLE `addon`  ADD COLUMN `IS_FTD_WEST_ADDON` CHAR(1) DEFAULT NULL;

------------------------------------------------------------------------------------
-- begin requested by Sri Meka,                                  8/14/2015  --------
-- defect DI-8          (Mark      )   (159900 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE occasion ADD COLUMN FTDWEST_ID BIGINT DEFAULT NULL;

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by DharmaTeja,                                8/27/2015  --------
-- defect DI-43        (Pavan      )   (  161371 )    ------------------------------
------------------------------------------------------------------------------------

Alter table vendor_product MODIFY VENDOR_SKU VARCHAR(32) default null;

------------------------------------------------------------------------------------
-- end   requested by DharmaTeja,                                8/27/2015  --------
-- defect DI-43        (Pavan      )   (  161371 )    ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )     --------------------------------
------------------------------------------------------------------------------------

use venus;
ALTER TABLE venus ADD column FTDWEST_ORDER_ID varchar(20) default null;

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk,                                  8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )     --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri Vatsala,                               9/16/2015  --------
-- defect DI-8         (Syed  ) ( )                 --------------------------------
------------------------------------------------------------------------------------

use clean;

ALTER TABLE order_details ADD VENDOR_SUN_UPCHARGE decimal(5,2) DEFAULT NULL;
ALTER TABLE order_details ADD VENDOR_MON_UPCHARGE decimal(5,2) DEFAULT NULL;
ALTER TABLE order_details ADD LATE_CUTOFF_FEE decimal(5,2) DEFAULT NULL;

ALTER TABLE order_bills ADD VENDOR_SUN_UPCHARGE decimal(5,2) DEFAULT NULL;
ALTER TABLE order_bills ADD VENDOR_MON_UPCHARGE decimal(5,2) DEFAULT NULL;
ALTER TABLE order_bills ADD LATE_CUTOFF_FEE decimal(5,2) DEFAULT NULL;


------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                               9/16/2015  --------
-- defect DI-8         (Pavan  ) ( )                --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gary       ,                               9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------


use ftd_apps;

ALTER TABLE product_master ADD pquad_pc_id varchar(20) DEFAULT NULL;
ALTER TABLE product_master ADD pquad_pc_display_names varchar(100) DEFAULT NULL;

------------------------------------------------------------------------------------
-- end requested by Gary       ,                                 9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik Datchanamoorthy,                  10/30/2015  --------
-- defect SP-105         (Mark   ) (169046 )        --------------------------------
------------------------------------------------------------------------------------

grant select on ptn_pi.partner_mapping to mercury;
grant select on ptn_pi.partner_order_fulfillment to mercury;
grant select on ptn_pi.partner_order_detail to mercury;

grant execute on ptn_pi.pi_maint_pkg  to mercury;

------------------------------------------------------------------------------------
-- end   requested by Karthik Datchanamoorthy,                  10/30/2015  --------
-- defect SP-105         (Mark   ) (169046 )        --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               10/12/2015  --------
-- defect SP-107         (pavan  ) (166669 )          --------------------------------
------------------------------------------------------------------------------------
grant execute on ftd_apps.SP_GET_ACTIVE_OCCASIONS to global;
grant execute on global.global_pkg  to osp;

------------------------------------------------------------------------------------
-- End requested by Karthik D ,                               10/12/2015  --------
-- defect SP-107         (pavan  ) (166669 )          --------------------------------
------------------------------------------------------------------------------------

