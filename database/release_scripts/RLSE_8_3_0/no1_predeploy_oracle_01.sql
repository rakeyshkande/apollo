------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/10/2015  --------
-- defect DI-14         (Mark      )   (158852 )    --------------------------------
------------------------------------------------------------------------------------

create table venus.vendor_order_status (
ORDER_STATUS_ID integer not null,
VENUS_ORDER_NUMBER varchar2(11) not null,
PARTNER_ORDER_NUMBER number(12,0) not null,
PARTNER_ID           varchar2(100) not null,
TRACKING_NUMBER      varchar2(100),
CARRIER              varchar2(100),
ORDER_STATUS         varchar2(30) not null,
PRINTED              date,
SHIPPED              date,
DELIVERED            date,
REJECTED             date,
REJECT_CODE          integer,
REJECT_MESSAGE       varchar2(4000),
STATUS               varchar2( 100) not null,
CREATED_ON           date not null,
CREATED_BY           varchar2(100) not null,
UPDATED_ON           date not null,
UPDATED_BY           varchar2(100) not null) tablespace venus_data;

alter table venus.vendor_order_status add constraint vendor_order_status_pk primary key (order_status_id)
using index tablespace venus_indx;

create index venus.vendor_order_status_n1 on venus.vendor_order_status (venus_order_number) tablespace venus_indx;
create index venus.vendor_order_status_n2 on venus.vendor_order_status (created_on        ) tablespace venus_indx;
create index venus.vendor_order_status_n3 on venus.vendor_order_status (tracking_number,carrier,order_status) 
tablespace venus_indx;

alter table venus.vendor_order_status add constraint vendor_order_status_ck1 check (status in ('NEW','PROCESSED'));

create sequence venus.vendor_order_status_id_sq cache 200;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                8/10/2015  --------
-- defect DI-14         (Mark      )   (158852 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                                8/18/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('GLOBAL_CONFIG', 'FTDW_LATE_CUTOFF_HOURS', '1200', 'DI-8', SYSDATE, 'DI-8', SYSDATE, 
'specifies the late cutoff time for all specialty gifts and freshcut products assigned to the FTD West vendor.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('GLOBAL_CONFIG', 'FTDW_LATE_CUTOFF_FEE', '4.99', 'DI-8', SYSDATE, 'DI-8', SYSDATE,
'The dollar value that will be charged to the customer when Late Cutoff � FTD West is enabled.');

------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                                8/18/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                               8/26/2015  --------
-- defect DI-9         (Mark  ) (161349)            --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('SHIPPING_PARMS', 'FTDWEST_CUSTOMER_ID', 'REPLACEME', 'DI-9', sysdate, 'DI-9', sysdate,
        'Customer ID recognized by FTD West for all CreateOrder requests');

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('SHIPPING_PARMS', 'FTDWEST_SERVICE_URL', 'REPLACEME', 'DI-9', sysdate, 'DI-9', sysdate,
        'Web Service URL for FTD West Order Service');

declare
   v_key_name varchar2(30);
begin

   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_APP_TOKEN',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Application Token required by FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_LOGIN',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Login/email for FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDWEST_PASSWORD',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Password for FTD West Order Service',sysdate,'DI-9',sysdate,'DI-9',v_key_name);

end;
/

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                               8/26/2015  --------
-- defect DI-9         (Mark  ) (161349)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharma Teja,                               9/16/2015  --------
-- defect DI-18         (Pavan  ) (163754 )            --------------------------------
------------------------------------------------------------------------------------
 insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_SVC_URL', 'http://metisdapp01v1.ftdi.com:38080/pacs/', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service URL');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_CONN_TIMEOUT', '10000', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service connection time out in milliseconds');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_SOCKET_TIMEOUT', '10000', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service Socket time out in milliseconds');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_CHANNEL_NAME', 'APOLLO', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service channel name for each apollo request');

  insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_DAYS_IN_RESP', '120', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service - no of days to be retured in response');
 


------------------------------------------------------------------------------------
-- end   requested by Dharma Teja,                               9/16/2015  --------
-- defect DI-18         (Pavan  ) (163754 )            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change

grant select, flashback on frp.global_parms$ to ogg;

grant select, flashback on FTD_APPS.PRODUCT_SHIP_METHODS to ogg;

------------------------------------------------------------------------------------
-- End   requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by D karthikeyan,                               10/30/2015  --------
-- defect DI-18         (Pavan  ) (168965 )          --------------------------------
------------------------------------------------------------------------------------
update frp.global_parms set value = 'REPLACE ME', updated_on = sysdate, updated_by = 'DI-18'
 where name = 'PAC_SVC_URL' and context = 'SERVICE';

------------------------------------------------------------------------------------
-- End requested by D karthikeyan,                                 10/30/2015  --------
-- defect DI-18          (Pavan  ) (168965 )          --------------------------------
------------------------------------------------------------------------------------

