------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                7/30/2015  --------
-- defect DI-5          (Mark      )   (157867 )    --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FTD_APPS.VENDOR_TYPES (
        VENDOR_CODE, 
        DISPLAY_NAME, 
        DESCRIPTION, 
        ACTIVE) 
VALUES (
        'FTD WEST', 
        'FTD WEST', 
        'Indicates orders that are processed by the FTD West system', 
        'Y');

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                7/30/2015  --------
-- defect DI-5          (Mark      )   (157867 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                7/30/2015  --------
-- defect DI-12         (Mark      )   (157869 )    --------------------------------
------------------------------------------------------------------------------------

alter table ftd_apps.vendor_master drop constraint vendor_master_ck1;
alter table ftd_apps.vendor_master add  constraint vendor_master_ck1 check (
vendor_type in ('ESCALATE','FTP','SDS','FTD WEST'));

create sequence venus.west_order_number_sq minvalue 1 increment by 1 start with 1
cache 200 noorder nocycle;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                7/30/2015  --------
-- defect DI-12         (Mark      )   (157869 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                7/31/2015  --------
-- defect DI-5,DI-38    (Mark      )   (157928 )    --------------------------------
------------------------------------------------------------------------------------

alter table FTD_APPS.PRODUCT_MASTER drop constraint PRODUCT_MASTER_CK4;

alter table FTD_APPS.PRODUCT_MASTER add constraint PRODUCT_MASTER_CK4
check (shipping_system IN ('ESCALATE','FTP','SDS','NONE','FTD WEST'));

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                7/31/2015  --------
-- defect DI-5,DI-38    (Mark      )   (157928 )    --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/5/2015  --------
-- defect DI-12         (Syed      )   (158538 )    --------------------------------
------------------------------------------------------------------------------------


alter table venus.venus drop constraint venus_ck2;

alter table venus.venus add  constraint venus_ck2 check (
shipping_system in ('ESCALATE','FTP','SDS','FTD WEST'));


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                                8/5/2015  --------
-- defect DI-12         (Syed      )   (158538 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sunil rao,                                8/6/2015  --------
-- defect DI-4         (Pavan )                     --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE ftd_apps.addon ADD pquad_accessory_id NUMBER(32) DEFAULT NULL;
ALTER TABLE ftd_apps.addon ADD is_ftd_west_addon char(1) DEFAULT 'N' NOT NULL;
 
ALTER TABLE ftd_apps.addon_history ADD pquad_accessory_id NUMBER(32) DEFAULT NULL;
ALTER TABLE ftd_apps.addon_history ADD is_ftd_west_addon char(1) DEFAULT 'N' NOT NULL;

------------------------------------------------------------------------------------
-- end   requested by Sunil rao,                                8/6/2015  --------
-- defect DI-4    (Pavan )                          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                                 8/7/2015  --------
-- defect DI-8    (Pavan )                          --------------------------------
------------------------------------------------------------------------------------

-- THESE ARE GOLDEN GATE CHANGES

ALTER TABLE ftd_apps.snh ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.snh ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

ALTER TABLE ftd_apps.snh$ ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.snh$ ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

ALTER TABLE ftd_apps.snh_hist ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.snh_hist ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

ALTER TABLE ftd_apps.service_fee_override ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.service_fee_override ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

ALTER TABLE ftd_apps.service_fee_override$ ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.service_fee_override$ ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

ALTER TABLE ftd_apps.service_fee_override_hist ADD VENDOR_SUN_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;
ALTER TABLE ftd_apps.service_fee_override_hist ADD VENDOR_MON_UPCHARGE NUMBER(5,2) DEFAULT '0' NOT NULL;

insert into ftd_apps.ship_methods (ship_method_id, description, novator_tag, max_transit_days, sds_ship_via, sds_ship_via_air) 
values ('SU', 'Sunday Delivery', 'allow_sunday', '1', 'COM_SU', 'COM_SU_AIR');

------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                                8/7/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                                8/7/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

-- THESE ARE GOLDEN GATE CHANGES
ALTER  TABLE  FTD_APPS.PRODUCT_MASTER   ADD PQUAD_PRODUCT_ID NUMBER(32) DEFAULT NULL;
ALTER  TABLE  FTD_APPS.PRODUCT_MASTER$  ADD PQUAD_PRODUCT_ID NUMBER(32) DEFAULT NULL;

------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                                8/7/2015  --------
-- defect DI-8    (Pavan )                --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/17/2015  --------
-- defect DI-5          (Mark      )   (160083 )    --------------------------------
------------------------------------------------------------------------------------

update ftd_apps.vendor_master
set vendor_type = 'FTD WEST'
where vendor_name = 'FTD WEST';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                8/17/2015  --------
-- defect DI-5          (Mark      )   (160083 )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sana venkata,                                8/18/2015  --------
-- defect DI-4          (Pavan      )         --------------------------------
------------------------------------------------------------------------------------

-- deploy database/ftd_apps_plsql/snh_$.trg and deploy database/ftd_apps_plsql/snh_hist_trg.trg at this point

update  ftd_apps.snh
set     VENDOR_SUN_UPCHARGE = 14.99,
        VENDOR_MON_UPCHARGE = 0.00,
        UPDATED_BY = 'DI-46',
        UPDATED_ON = sysdate;
        
insert into ftd_apps.shipping_key_costs
(
	select  distinct shipping_key_detail_id,
			'SU',
			14.99,
			'DI-4',
			sysdate,
			'DI-4',
			sysdate
	from    ftd_apps.shipping_key_costs
	where   shipping_key_detail_id in
			(select distinct shipping_key_detail_id from ftd_apps.shipping_key_costs)
);

commit;

------------------------------------------------------------------------------------
-- end   requested by Sana venkata,                                8/18/2015  --------
-- defect DI-4          (Pavan      )         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------

-- THESE ARE GOLDEN GATE CHANGES
ALTER TABLE ftd_apps.occasion ADD (ftdwest_id number);

update FTD_APPS.OCCASION set FTDWEST_ID=33;
update FTD_APPS.OCCASION set FTDWEST_ID=11 where OCCASION_ID=1;
update FTD_APPS.OCCASION set FTDWEST_ID=6  where OCCASION_ID=2;
update FTD_APPS.OCCASION set FTDWEST_ID=3  where OCCASION_ID=3;
update FTD_APPS.OCCASION set FTDWEST_ID=38 where OCCASION_ID=4;
update FTD_APPS.OCCASION set FTDWEST_ID=2  where OCCASION_ID=6;
update FTD_APPS.OCCASION set FTDWEST_ID=1  where OCCASION_ID=7;
update FTD_APPS.OCCASION set FTDWEST_ID=5  where OCCASION_ID=9;
update FTD_APPS.OCCASION set FTDWEST_ID=9  where OCCASION_ID=10;
update FTD_APPS.OCCASION set FTDWEST_ID=12 where OCCASION_ID=12;
update FTD_APPS.OCCASION set FTDWEST_ID=18 where OCCASION_ID=14;
update FTD_APPS.OCCASION set FTDWEST_ID=15 where OCCASION_ID=15;
update FTD_APPS.OCCASION set FTDWEST_ID=4  where OCCASION_ID=16;
update FTD_APPS.OCCASION set FTDWEST_ID=13 where OCCASION_ID=17;
update FTD_APPS.OCCASION set FTDWEST_ID=17 where OCCASION_ID=18;
update FTD_APPS.OCCASION set FTDWEST_ID=28 where OCCASION_ID=19;
update FTD_APPS.OCCASION set FTDWEST_ID=19 where OCCASION_ID=20;
update FTD_APPS.OCCASION set FTDWEST_ID=16 where OCCASION_ID=21;
update FTD_APPS.OCCASION set FTDWEST_ID=26 where OCCASION_ID=26;
update FTD_APPS.OCCASION set FTDWEST_ID=20 where OCCASION_ID=34;
update FTD_APPS.OCCASION set FTDWEST_ID=34 where OCCASION_ID=37;
update FTD_APPS.OCCASION set FTDWEST_ID=23 where OCCASION_ID=38;
update FTD_APPS.OCCASION set FTDWEST_ID=7  where OCCASION_ID=40;
update FTD_APPS.OCCASION set FTDWEST_ID=42 where OCCASION_ID=41;
update FTD_APPS.OCCASION set FTDWEST_ID=36 where OCCASION_ID=42;
update FTD_APPS.OCCASION set FTDWEST_ID=35 where OCCASION_ID=45;
update FTD_APPS.OCCASION set FTDWEST_ID=10 where OCCASION_ID=47;
update FTD_APPS.OCCASION set FTDWEST_ID=30 where OCCASION_ID=51;
update FTD_APPS.OCCASION set FTDWEST_ID=29 where OCCASION_ID=58;
update FTD_APPS.OCCASION set FTDWEST_ID=55 where OCCASION_ID=59;
update FTD_APPS.OCCASION set FTDWEST_ID=27 where OCCASION_ID=60;
update FTD_APPS.OCCASION set FTDWEST_ID=41 where OCCASION_ID=64;
update FTD_APPS.OCCASION set FTDWEST_ID=14 where OCCASION_ID=65;

commit;

ALTER TABLE ftd_apps.occasion MODIFY ftdwest_id DEFAULT 33 NOT NULL; 



ALTER TABLE venus.venus ADD (ftdwest_order_id varchar2(20));


------------------------------------------------------------------------------------
-- end requested by Rose Lazuk,                                8/24/2015  --------
-- defect DI-9        (Syed      )   (160533  )    --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by DharmaTeja,                                8/27/2015  --------
-- defect DI-43        (Pavan      )   (  161371 )    --------------------------------
------------------------------------------------------------------------------------

-- THESE ARE GOLDEN GATE CHANGES
Alter table FTD_APPS.VENDOR_PRODUCT MODIFY VENDOR_SKU VARCHAR2(32);
Alter table FTD_APPS.VENDOR_PRODUCT$  MODIFY  VENDOR_SKU VARCHAR2(32);

------------------------------------------------------------------------------------
-- end requested by DharmaTeja,                                8/272015  --------
-- defect DI-43        (Pavan      )   ( 161371  )    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri Vatsala,                               9/16/2015  --------
-- defect DI-8         (Pavan  ) ( )                --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change

ALTER TABLE joe.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE joe.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE joe.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE scrub.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE scrub.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE scrub.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE clean.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE clean.order_bills ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_bills ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_bills ADD LATE_CUTOFF_FEE NUMBER(5,2);


------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                               9/16/2015  --------
-- defect DI-8         (Pavan  ) ( )                --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gary       ,                               9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change

ALTER TABLE ftd_apps.product_master ADD (pquad_pc_id varchar2(20));
ALTER TABLE ftd_apps.product_master ADD (pquad_pc_display_names varchar2(100));

ALTER TABLE ftd_apps.product_master$ ADD (pquad_pc_id varchar2(20));
ALTER TABLE ftd_apps.product_master$ ADD (pquad_pc_display_names varchar2(100));

grant execute on ftd_apps.oe_get_pquad_pc_info to osp;


------------------------------------------------------------------------------------
-- end requested by Gary       ,                                 9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sri vathsala,                               9/29/2015  --------
-- defect DI-101         (Pavan  ) (165131 )          --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE scrub.order_details ADD ORIGINAL_ORDER_HAS_LCF CHAR(1)  DEFAULT  NULL;

------------------------------------------------------------------------------------
-- End requested by Sri vathsala,                                 9/29/2015  --------
-- defect DI-101          (Pavan  ) (165131 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               10/ 8/2015  --------
-- defect DI-117         (Mark   ) (166356 )        --------------------------------
------------------------------------------------------------------------------------

update joe.element_config
set product_validate_flag = 'Y'
where element_id in ('addOnCheckbox1',
'addOnCheckbox2',
'addOnCheckbox3',
'addOnCheckbox4',
'productAddonVase');

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               10/ 8/2015  --------
-- defect DI-117         (Mark   ) (166356 )        --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               10/ 23/2015  --------
-- defect QE-91        (Syed   ) (168131 )        --------------------------------
------------------------------------------------------------------------------------

update clean.stock_email
 set subject = substr(subject, 5),
     updated_on = sysdate,
     updated_by = 'QE-91'
 where subject like 'Re: %';


------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                               10/ 23/2015  --------
-- defect QE-91         (Syed) (168131 )        --------------------------------
------------------------------------------------------------------------------------

