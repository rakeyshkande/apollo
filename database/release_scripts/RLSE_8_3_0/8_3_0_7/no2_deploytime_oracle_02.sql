------------------------------------------------------------------------------------
-- begin requested by Sri Vatsala,                               9/16/2015  --------
-- defect DI-8         (Pavan  ) ( )                --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change

ALTER TABLE joe.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE joe.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE joe.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE scrub.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE scrub.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE scrub.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE clean.order_details ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_details ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_details ADD LATE_CUTOFF_FEE NUMBER(5,2);

ALTER TABLE clean.order_bills ADD VENDOR_SUN_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_bills ADD VENDOR_MON_UPCHARGE NUMBER(5,2);
ALTER TABLE clean.order_bills ADD LATE_CUTOFF_FEE NUMBER(5,2);


------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                               9/16/2015  --------
-- defect DI-8         (Pavan  ) ( )                --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gary       ,                               9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change

ALTER TABLE ftd_apps.product_master ADD (pquad_pc_id varchar2(20));
ALTER TABLE ftd_apps.product_master ADD (pquad_pc_display_names varchar2(100));

ALTER TABLE ftd_apps.product_master$ ADD (pquad_pc_id varchar2(20));
ALTER TABLE ftd_apps.product_master$ ADD (pquad_pc_display_names varchar2(100));

grant execute on ftd_apps.oe_get_pquad_pc_info to osp;


------------------------------------------------------------------------------------
-- end requested by Gary       ,                                 9/16/2015  --------
-- defect DI-71         (Syed  ) (163863 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate Change



grant select, flashback on frp.global_parms$ to ogg;

grant select, flashback on FTD_APPS.PRODUCT_SHIP_METHODS to ogg;




------------------------------------------------------------------------------------
-- End   requested by Datchanamoorthy, Karthikeyan  ,            9/18/2015  --------
-- defect PACDI-22          (Syed  )   (164015 )    --------------------------------
-----------------------------------------------------------------------------------
