------------------------------------------------------------------------------------
-- begin requested by Dharma Teja,                               9/16/2015  --------
-- defect DI-18         (Pavan  ) (163754 )            --------------------------------
------------------------------------------------------------------------------------
 insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_SVC_URL', 'http://metisdapp01v1.ftdi.com:38080/pacs/', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service URL');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_CONN_TIMEOUT', '10000', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service connection time out in milliseconds');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_SOCKET_TIMEOUT', '10000', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service Socket time out in milliseconds');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_CHANNEL_NAME', 'APOLLO', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service channel name for each apollo request');

  insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_DAYS_IN_RESP', '120', 'DI-18', sysdate, 'DI-18', sysdate, 'PACS Service - no of days to be retured in response');
 


------------------------------------------------------------------------------------
-- end   requested by Dharma Teja,                               9/16/2015  --------
-- defect DI-18         (Pavan  ) (163754 )            --------------------------------
------------------------------------------------------------------------------------
