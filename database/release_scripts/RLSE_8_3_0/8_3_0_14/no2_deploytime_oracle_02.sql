------------------------------------------------------------------------------------
-- begin requested by Karthik Datchanamoorthy,                  10/30/2015  --------
-- defect SP-105         (Mark   ) (169046 )        --------------------------------
------------------------------------------------------------------------------------

grant select on ptn_pi.partner_mapping to mercury;
grant select on ptn_pi.partner_order_fulfillment to mercury;
grant select on ptn_pi.partner_order_detail to mercury;

grant execute on ptn_pi.pi_maint_pkg  to mercury;

------------------------------------------------------------------------------------
-- end   requested by Karthik Datchanamoorthy,                  10/30/2015  --------
-- defect SP-105         (Mark   ) (169046 )        --------------------------------
------------------------------------------------------------------------------------

