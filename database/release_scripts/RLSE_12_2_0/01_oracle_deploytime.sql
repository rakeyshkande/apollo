------------------------------------------------------------------------------------
-- Begin requested by Gautam 2019/04/02  ---------------------------------------------
-- CAL-???? (mwoytus)             -------------------------------------------------
------------------------------------------------------------------------------------


UPDATE FRP.GLOBAL_PARMS SET VALUE='/home/oracle/BAMS/FirstData_PGP_PublicKey_1024' WHERE NAME='bamspublickeypath'
UPDATE FRP.GLOBAL_PARMS SET VALUE='prod2-gw-na.firstdataclients.com' WHERE NAME='bamsSFTPServer'
UPDATE FRP.GLOBAL_PARMS SET VALUE='prod2-gw-na.firstdataclients.com' WHERE NAME='BamsAckSFtpServer'
UPDATE FRP.GLOBAL_PARMS SET VALUE='/available' WHERE NAME='BamsAckSFtpLocation'
UPDATE FRP.GLOBAL_PARMS SET VALUE='/home/oracle/.ssh/id_rsa' WHERE NAME='BamsSSHPrivateKeyPath'
UPDATE FRP.GLOBAL_PARMS SET VALUE='/' WHERE NAME='bamsSFTPLocation'

------------------------------------------------------------------------------------
-- CAL-???? (mwoytus)             -------------------------------------------------
-- End   requested by Gautam 2019/04/02  ---------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose 2019/04/17  ---------------------------------------------
-- MMS-158  (mwoytus)              -------------------------------------------------
------------------------------------------------------------------------------------

create or replace view FTD_APPS.SOURCE_CODE_VW as
SELECT
        rownum as n, 
        sm.source_code
FROM    ftd_apps.source_master sm;

create or replace view FTD_APPS.FLORIST_MASTER_VW as
SELECT
        rownum as n,
        fm.florist_id
FROM    ftd_apps.florist_master fm;

create or replace view FTD_APPS.FLORIST_HOURS_VW as
SELECT
        rownum as n,
        fh.florist_id
FROM    ftd_apps.florist_hours fh;

grant select on ftd_apps.source_code_vw to sdc;
grant select on ftd_apps.florist_hours_vw to sdc;
grant select on ftd_apps.florist_master_vw to sdc;

------------------------------------------------------------------------------------
-- End   requested by Rose 2019/04/17  ---------------------------------------------
-- MMS-158  (mwoytus)              -------------------------------------------------
------------------------------------------------------------------------------------

