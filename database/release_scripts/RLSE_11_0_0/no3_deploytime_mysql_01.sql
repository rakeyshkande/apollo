------------------------------------------------------------------------------------
-- Begin requested by Rose                               1/08/2018  --------
-- User task 9359   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------


-- Groupon MySQL Database Release Scripts

USE groupon;
UPDATE order_status_val SET cancel_allowed = 'N' WHERE status = 'cancelled';

------------------------------------------------------------------------------------
-- End requested by Rose                               1/08/2018  --------
-- User task 9359   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                             1/09/2018  --------
-- User task 9370   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------

-- Groupon MySQL Database Release Scripts

USE groupon;
alter table order_details add column source_code varchar(10);

------------------------------------------------------------------------------------
-- END requested by Tim Schmig                              1/09/2018  --------
-- User task 9370   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                              1/09/2018  --------
-- User task 9423   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------

-- Groupon MySQL Database Release Scripts

create table groupon.order_status$ (
    reservation_id VARCHAR(50) NOT NULL,
    status VARCHAR(20),
    created_on DATETIME,
    created_by VARCHAR(50),
    updated_on DATETIME,
    updated_by VARCHAR(50),
    operation$ VARCHAR(10),
    timestamp$ TIMESTAMP,
    INDEX order_status_shadow_ndx (reservation_id)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS groupon.order_status_insert_$;
delimiter %%%
CREATE TRIGGER groupon.order_status_insert_$ AFTER
INSERT
ON
    groupon.order_status FOR EACH row BEGIN
INSERT
INTO
    groupon.order_status$
    (
        reservation_id,
        status,
        created_on,
        created_by,
        updated_on,
        updated_by,
        operation$,
        timestamp$
    )
    VALUES
    (
        NEW.reservation_id,
        NEW.status,
        NEW.created_on,
        NEW.created_by,
        NEW.updated_on,
        NEW.updated_by,
        'INS',
        CURRENT_TIMESTAMP
    );

END;
%%%
delimiter ;


DROP TRIGGER IF EXISTS groupon.order_status_update_$;
delimiter %%%
CREATE TRIGGER groupon.order_status_update_$ AFTER
UPDATE
ON
    groupon.order_status FOR EACH row BEGIN
INSERT
INTO
    groupon.order_status$
    (
        reservation_id,
        status,
        created_on,
        created_by,
        updated_on,
        updated_by,
        operation$,
        timestamp$
    )
    VALUES
    (
        OLD.reservation_id,
        OLD.status,
        OLD.created_on,
        OLD.created_by,
        OLD.updated_on,
        OLD.updated_by,
        'UPD_OLD',
        CURRENT_TIMESTAMP
    );

INSERT
INTO
    groupon.order_status$
    (
        reservation_id,
        status,
        created_on,
        created_by,
        updated_on,
        updated_by,
        operation$,
        timestamp$
    )
    VALUES
    (
        NEW.reservation_id,
        NEW.status,
        NEW.created_on,
        NEW.created_by,
        NEW.updated_on,
        NEW.updated_by,
        'UPD_NEW',
        CURRENT_TIMESTAMP
    );

END;
%%%
delimiter ;

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                              1/09/2018  --------
-- User task 9423   (Syed Rizvi)         ----------------------------------
------------------------------------------------------------------------------------






