------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               12/07/2017  --------
-- User story 8335, task 8520   (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.si_template_index_prod to joe;
grant select on ftd_apps.si_source_template_ref to joe;

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('OE_CONFIG', 'GROUPON_SOURCE_CODES', '??? REPLACE-ME !!!', 'SYS', sysdate, 'SYS', sysdate, 
        'Sourcecodes used exclusively for Groupon (Note each must be within single quotes).');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               12/07/2017  --------
-- User story 8335, task 8520   (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               12/21/2017  --------
-- User story 8572, task 8973   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------
SET DEFINE OFF

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GROUPON_MSG_PRODUCER_URL', 'REPLACE ME http://localhost:8703/msgproducer/publish?topic=<topic>&msg=<msg>', 'SYS', sysdate, 'SYS', sysdate, 
        'URL for microservice that adds entry to messaging service (Kafka)');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GROUPON_MSG_PRODUCER_TOPIC', 'prodmod', 'SYS', sysdate, 'SYS', sysdate, 
        'Default topic for indicating product changes to messaging service (Kafka)');


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GROUPON_MSG_PRODUCER_TIMEOUT', '10', 'SYS', sysdate, 'SYS', sysdate, 
        'Timeout (in seconds) for sending to messaging service (Kafka)');


INSERT INTO quartz_schema.pipeline (
pipeline_id, group_id, description, 
        tool_tip, default_schedule, 
        default_payload, force_default_payload, queue_name, class_code, active_flag
) VALUES (
'MSG_PRODUCER_GROUPON','PDB', 'Send to Groupon MsgProducer',
'Sends message to topic via MsgProducer microservice (GROUPON_MSG_PRODUCER|message|topic)', null,  
 'GROUPON_MSG_PRODUCER||', 'N', 'OJMS.PDB', 'SimpleJmsJob', 'Y'
);

------------------------------------------------------------------------------------
-- END requested by Gary Sergey                               12/21/2017  --------
-- User story 8572, task 8973   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               12/28/2017  --------
-- User story 8572, task 9083   (mwoytus)        -----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GROUPON_MSG_PRODUCER_TOPIC_DEL', 'proddel', 'SYS', sysdate, 'SYS', sysdate, 
        'Topic for indicating product has been made unavailable to messaging service (Kafka)');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               12/28/2017  --------
-- User story 8572, task 9083   (mwoytus)        -----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Bhargav Surimen                              01/12/2018  --------
-- User story 7300, task    (kmohamme)        -----------------------------------
------------------------------------------------------------------------------------

Insert into ftd_apps.source_master (SOURCE_CODE,DESCRIPTION,SNH_ID,PRICE_HEADER_ID,SEND_TO_SCRUB,FRAUD_FLAG,ENABLE_LP_PROCESSING,EMERGENCY_TEXT_FLAG,REQUIRES_DELIVERY_CONFIRMATION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SOURCE_TYPE,DEPARTMENT_CODE,START_DATE,END_DATE,PAYMENT_METHOD_ID,LIST_CODE_FLAG,DEFAULT_SOURCE_CODE_FLAG,BILLING_INFO_PROMPT,BILLING_INFO_LOGIC,MARKETING_GROUP,EXTERNAL_CALL_CENTER_FLAG,HIGHLIGHT_DESCRIPTION_FLAG,DISCOUNT_ALLOWED_FLAG,BIN_NUMBER_CHECK_FLAG,ORDER_SOURCE,YELLOW_PAGES_CODE,JCPENNEY_FLAG,COMPANY_ID,PROMOTION_CODE,BONUS_PROMOTION_CODE,REQUESTED_BY,RELATED_SOURCE_CODE,PROGRAM_WEBSITE_URL,COMMENT_TEXT,PARTNER_BANK_ID,WEBLOYALTY_FLAG,LAST_USED_ON_ORDER_DATE,IOTW_FLAG,INVOICE_PASSWORD,MP_REDEMPTION_RATE_ID,ADD_ON_FREE_ID,DISPLAY_SERVICE_FEE_CODE,DISPLAY_SHIPPING_FEE_CODE,PRIMARY_BACKUP_RWD_FLAG,RECPT_LOCATION_TYPE,RECPT_BUSINESS_NAME,RECPT_LOCATION_DETAIL,RECPT_ADDRESS,RECPT_ZIP_CODE,RECPT_CITY,RECPT_STATE_ID,RECPT_COUNTRY_ID,RECPT_PHONE,RECPT_PHONE_EXT,CUST_FIRST_NAME,CUST_LAST_NAME,CUST_DAYTIME_PHONE,CUST_DAYTIME_PHONE_EXT,CUST_EVENING_PHONE,CUST_EVENING_PHONE_EXT,CUST_ADDRESS,CUST_ZIP_CODE,CUST_CITY,CUST_STATE_ID,CUST_COUNTRY_ID,CUST_EMAIL_ADDRESS,SURCHARGE_DESCRIPTION,DISPLAY_SURCHARGE_FLAG,SURCHARGE_AMOUNT,APPLY_SURCHARGE_CODE,ALLOW_FREE_SHIPPING_FLAG,SAME_DAY_UPCHARGE,DISPLAY_SAME_DAY_UPCHARGE,MORNING_DELIVERY_FLAG,MORNING_DELIVERY_FREE_SHIPPING,DELIVERY_FEE_ID,AUTO_PROMOTION_ENGINE,APE_PRODUCT_CATALOG,OSCAR_SELECTION_ENABLED_FLAG,OSCAR_SCENARIO_GROUP_ID,FUNERAL_CEMETERY_LOC_CHK,HOSPITAL_LOC_CHCK,FUNERAL_CEMETERY_LEAD_TIME_CHK,FUNERAL_CEMETERY_LEAD_TIME,BO_HRS_MON_FRI_START,BO_HRS_MON_FRI_END,BO_HRS_SAT_START,BO_HRS_SAT_END,BO_HRS_SUN_START,BO_HRS_SUN_END,CALCULATE_TAX_FLAG,CUSTOM_SHIPPING_CARRIER,MERCH_AMT_FULL_REFUND_FLAG,SAME_DAY_UPCHARGE_FS) 
values ('99399','Groupon Marketplace','69','00','N','Y','Y','N','N',to_date(sysdate,'DD-MON-RR'),'US_7300',to_date(sysdate,'DD-MON-RR'),'US_7300 proc','GROUPON',null,to_date(sysdate,'DD-MON-RR'),null,null,null,'N',null,null,null,null,'N','N',null,'I',null,null,'FTD',null,null,'209961',null,null,null,null,'N',to_date(sysdate,'DD-MON-RR'),'N',null,null,null,'D','D','Y',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Fuel Surcharge','D',2.11,'OFF','Y','Y','Y','N','N','00','N',null,'N',1,'N','N','Y',4,'9:00','18:00','9:00','18:00','9:00','17:00','Y',null,'Y','D');

-- Partner_Mapping
Insert into ptn_pi.partner_mapping (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,INCLUDE_ADDON,APPLY_DEF_PTN_BEHAVIOR,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,SEND_SHIP_STATUS_UPDATE,IS_INCR_TOTAL_ALLOWED)
values ('GRPN','Groupon','GRPN','GRPN','GRPN','partner_groupon.png','99399','99399','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US','Y','Y','Y',to_date(sysdate,'DD-MON-RR'),'US_7300',to_date(sysdate,'DD-MON-RR'),'US_7300','Y','Y','Y','Y','Y','Y','N','N','N','Y','N','N','Y','Y','N','Y');


-- FTD Origin
Insert into FTD_APPS.ORIGINS (ORIGIN_ID,DESCRIPTION,ORIGIN_TYPE,ACTIVE,PARTNER_REPORT_FLAG,SEND_FLORIST_PDB_PRICE,ORDER_CALC_TYPE) 
values ('GRPN','Groupon Marketplace','internet','Y','Y','Y','PI');

------------------------------------------------------------------------------------
-- End requested by Bhargav Surimen                              01/12/2018  --------
-- User story 7300, task    (kmohamme)        -----------------------------------
------------------------------------------------------------------------------------