------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                09/26/2017  --------
-- User story 6429              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

create table FTD_APPS.EXCLUSION_ZONE_HEADER (
EXCLUSION_ZONE_HEADER_ID INTEGER not null,
EXCLUSION_DESCRIPTION VARCHAR2(100) not null,
BLOCK_START_DATE DATE not null,
BLOCK_END_DATE DATE not null,
CREATED_ON DATE not null,
CREATED_BY VARCHAR2(100) not null,
UPDATED_ON DATE not null,
UPDATED_BY VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table ftd_apps.exclusion_zone_header add constraint
exclusion_zone_header_pk primary key (exclusion_zone_header_id)
using index tablespace ftd_apps_indx;

create table FTD_APPS.EXCLUSION_ZONE_DETAIL (
EXCLUSION_ZONE_DETAIL_ID integer not null,
EXCLUSION_ZONE_HEADER_ID integer not null,
BLOCK_SOURCE VARCHAR2(100) not null,
BLOCK_SOURCE_KEY VARCHAR2(1000) not null,
CREATED_ON DATE not null,
CREATED_BY VARCHAR2(100) not null,
UPDATED_ON DATE not null,
UPDATED_BY VARCHAR2(100) not null) tablespace ftd_apps_data;

create index ftd_apps.exclusion_zone_detail_n1
on ftd_apps.exclusion_zone_detail(exclusion_zone_header_id)
tablespace ftd_apps_indx;

alter table ftd_apps.exclusion_zone_detail add constraint
exclusion_zone_detail_pk primary key (exclusion_zone_detail_id)
using index tablespace ftd_apps_indx;

alter table ftd_apps.exclusion_zone_detail add constraint
exclusion_zone_detail_fk1 foreign key (exclusion_zone_header_id)
references ftd_apps.exclusion_zone_header;

create sequence FTD_APPS.EXCLUSION_ZONE_HEADER_ID_SEQ;
create sequence FTD_APPS.EXCLUSION_ZONE_DETAIL_ID_SEQ;

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                09/26/2017  --------
-- User story 6429              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Gary Serget                              10/20/2017  --------
-- Task 7228  - Story 5570        (Syed)         ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES ('SHIPPING_PARMS', 'PERSONALIZATION_SORT_ORDER_ENABLED', 'N', 'Story_5570', sysdate, 'Story_5570', sysdate,
        'If Y, then any PDP SortOrder data is included in CreateOrder calls to West');

------------------------------------------------------------------------------------
-- End requested by Gary Serget                              10/20/2017  --------
-- Task 7228    - Story 5570       (Syed)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gunadeep                              10/24/2017  --------
-- Task 7532  - Story 7114        (kmohamme)         ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'SERVICE',
�PDP_PRODUCT_PRIORITY_SHIPMETHOD�,
    'Standard',
    sysdate,
    'US_7114',
    sysdate,
    'US_7114',
    'PAC Service gives the priority to this ship method for PDP PC Products. Possible values are Overnight and Standard.');

------------------------------------------------------------------------------------
-- End requested by Gunadeep                              10/24/2017  --------
-- Task 7532    - Story 7114       (kmohamme)         ----------------------------------
------------------------------------------------------------------------------------


