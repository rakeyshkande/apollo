------------------------------------------------------------------------------------
-- Begin requested by  Manasa Salla                           09/21/2017   ---------
-- sprint 1                                    -------------------------------------
------------------------------------------------------------------------------------

INSERT INTO SITESCOPE.PROJECTS(PROJECT, MAILSERVER_NAME, DESCRIPTION ) 
VALUES('REVERT_CUTOFF_TIME', 'barracuda.ftdi.com', 'Mail alert to be triggered when there is any error updating the CUTOFF time parms');

INSERT INTO SITESCOPE.PAGERS(PROJECT, PAGER_NUMBER) VALUES ('REVERT_CUTOFF_TIME','productionsupport@ftdi.com');
INSERT INTO SITESCOPE.PAGERS(PROJECT, PAGER_NUMBER) VALUES ('REVERT_CUTOFF_TIME','devcom@ftdi.com');

INSERT INTO FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ( 'FTDAPPS_PARMS', 'REVERT_CUTOFF_TIME_DATE', '09/20/2017', 'US_2173', SYSDATE, 'US_2173', SYSDATE,
'DB CRON JOB will run on this day(format : MM/DD/YYYY) to revert the cut off time to original');

grant UPDATE on FRP.GLOBAL_PARMS to FTD_APPS;


------------------------------------------------------------------------------------
-- End   requested by Manasa Salla                              09/21/2017  --------
-- User story 2173 (10.4.0.1)   (kmohamme)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by  Tim Schmig                             09/22/2017   ---------
-- sprint 2        (Syed Rizvi)                -------------------------------------
------------------------------------------------------------------------------------


delete from ftd_apps.efos_city_state ecs where not exists (select 'Y'
    from ftd_apps.efos_city_state_stage ecss
    where ecss.city_state_code = ecs.city_state_code);


------------------------------------------------------------------------------------
-- End   requested by  Tim Schmig                             09/22/2017   ---------
-- sprint 2        (Syed Rizvi)                -------------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by  Manasa Salla                           09/26/2017   ---------
-- sprint 2                                    -------------------------------------
------------------------------------------------------------------------------------

--1. Insert a new role
INSERT INTO AAS.ROLE (
ROLE_ID,
ROLE_NAME,
CONTEXT_ID,
DESCRIPTION,
CREATED_ON,
UPDATED_ON,
UPDATED_BY) 
VALUES (
aas.role_id.nextval, 
 'PEZ Maintenance',
'Order Proc',
'ONLY this role has the ability to view the PAS Exclusion Zones Maintenance link on Distribution/Fulfillment Menu',
sysdate,
sysdate,
'US_6017');

--2. Insert a new resource
INSERT INTO AAS.RESOURCES (
RESOURCE_ID,
CONTEXT_ID,
DESCRIPTION,
CREATED_ON,
UPDATED_ON,
UPDATED_BY) 
VALUES (
'PAS Exclusion Zone Maintenance Link',
'Order Proc',
'View- Ability to view the "PAS Exclusion Zone Maintenance" link on Distribution/Fulfillment Menu Where: load_member_data\web\xsl\exclusionZones.xsl',
sysdate,
sysdate,
'US_6017');

-- 3.Insert a new ACL (Resource Group)
INSERT INTO AAS.ACL (
ACL_NAME,
CREATED_ON,
UPDATED_ON,
UPDATED_BY,
ACL_LEVEL) 
VALUES (
'PEZ',
sysdate,
sysdate,
'US_6017',
null);

-- 4.Insert a new join on role and ACL
INSERT INTO AAS.REL_ROLE_ACL (
ROLE_ID,
ACL_NAME,
CREATED_ON,
UPDATED_ON,
UPDATED_BY) 
VALUES (
(select role_id from aas.role where role_name='PEZ Maintenance'),
'PEZ',
sysdate,
sysdate,
'US_6017');

-- 5.Insert a new join on acl, resource, context and permission
INSERT INTO AAS.REL_ACL_RP (
ACL_NAME,
RESOURCE_ID,
CONTEXT_ID,
PERMISSION_ID,
CREATED_ON,
UPDATED_ON,
UPDATED_BY) 
VALUES (
'PEZ',
'PAS Exclusion Zone Maintenance Link',
'Order Proc',
'Yes',
sysdate,
sysdate,
'US_6017');


------------------------------------------------------------------------------------
-- End   requested by Manasa Salla                               09/26/2017  --------
-- User story 6017 (10.4.0.4)   (kmohamme)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               09/26/2017  --------
-- User story 5566              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FTD_APPS.PERSONALIZATION_TEMPLATES (PERSONALIZATION_TEMPLATE_ID, PERSONALIZATION_DESCRIPTION) VALUES ('PDP', 'PDP');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               09/26/2017  --------
-- User story 5566              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                09/26/2017  --------
-- User story 6429              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

--- ---------------------------------------------------------------------
--- THIS IS A GOLDEN GATE CHANGE
--- ---------------------------------------------------------------------

alter table ftd_apps.florist_zips add (blocked_by_user_id varchar2(100));

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                09/26/2017  --------
-- User story 6429              (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 11/2/2017  --------
-- User Stories: 7367             (srizvi)        ----------------------------------
------------------------------------------------------------------------------------

-- -- clean.order_details table

-- 1 - Adding TEMP column personalization_data_temp clob       
alter table clean.order_details add personalization_data_temp clob;

-- 2 - Copy Data from personalization_data to personalization_data_temp         
update     clean.order_details
set        personalization_data_temp = personalization_data
where      personalization_data is not null; 

-- 3 - Verify
select count(*) from clean.order_details where personalization_data is not null;
select count(*) from clean.order_details where personalization_data_temp is not null;

-- 4 - Drop original column
alter table clean.order_details drop column personalization_data;

-- 5 - Rename the clob column
alter table clean.order_details rename column personalization_data_temp to personalization_data;

-- check for invalid or unusable indexes
select distinct STATUS from dba_indexes
where TABLE_OWNER='CLEAN' AND TABLE_NAME='ORDER_DETAILS';


-- -- scrub.order_details table

-- 1 - Adding TEMP column personalization_data_temp clob              
alter table scrub.order_details add personalization_data_temp clob;

-- 2 - Copy Data from personalization_data to personalization_data_temp         
update     scrub.order_details
set        personalization_data_temp = personalization_data
where      personalization_data is not null; 

-- 3 - Verify
select count(*) from scrub.order_details where personalization_data is not null;
select count(*) from scrub.order_details where personalization_data_temp is not null;

-- 4 - Drop original column
alter table scrub.order_details drop column personalization_data;

-- 5 - Rename the clob column
alter table scrub.order_details rename column personalization_data_temp to personalization_data;

-- check for invalid or unusable indexes
select distinct status from dba_indexes;

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 11/2/2017  --------
-- User Stories: 7367             (srizvi)        ----------------------------------
------------------------------------------------------------------------------------

