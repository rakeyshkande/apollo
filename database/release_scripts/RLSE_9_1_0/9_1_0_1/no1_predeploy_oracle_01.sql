------------------------------------------------------------------------------------
-- begin requested by manasa S                                  4/18/2016  --------
-- defect QE3SP-16        (Pavan  ) (196638)           ------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS values
('ORDER_GATHERER_CONFIG','VALID_ORDER_PREFIXES','ATC,ATA,ARS,AM,AN,AT,AA,AR,AI,AG,AJ,AH,CAS,C,FA,FR,FM,FN,FT,FI,FG,FJ,FH,BC,Z,Y,X,T',
'QE3SP-16',SYSDATE,'QE3SP-16',SYSDATE,'This parm is used to validate the order prefixes');  


------------------------------------------------------------------------------------
-- end   requested by Manasa s                                  4/18/2016  --------
-- defect QE3SP-16        (Pavan  ) (196638)           ------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Sunil sana                                  4/22/2016  --------
-- defect  DCE-7        (Pavan  ) (197340)           ------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('CUSTOMER_ORDER_MANAGEMENT', 'ORDER_COMMENTS_SUPPRESS_USERIDs', '{replace-me}', 'DCE-7', SYSDATE, 'DCE-7', SYSDATE,
 'Order comments provided by these userIDs (comma seperated) will be suppressed from display in OrderComments page - Comments tab.');

------------------------------------------------------------------------------------
-- end   requested by Sunil sana                                  4/22/2016  --------
-- defect  DCE-7        (Pavan  ) (197340)           ------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gutala venu                                  4/22/2016  --------
-- defect  QE3SP-4        (Pavan  ) (197542)           ------------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'SKIP_MASSAGES_BY_MARS', 'GEN', 'QE3SP-4', sysdate, 'QE3SP-4', sysdate, 
'MARs will not process the specified Message Codes ');

------------------------------------------------------------------------------------
-- end   requested by Gutala Venu                                  4/22/2016  --------
-- defect  QE3SP-4        (Pavan  ) (197542)           ------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                  4/22/2016  --------
-- defect  QE3SP-3       (Pavan  ) (197555)           ------------------------------
------------------------------------------------------------------------------------
update frp.global_parms set value='0' where name='FS_AUTO_RENEW_DATE_RANGE' and context='SERVICES_FREESHIPPING';

update frp.global_parms set value='30' where name='MAX_AUTO_RENEW_DATE_RANGE' and context='SERVICES_FREESHIPPING';
------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                  4/22/2016  --------
-- defect  QE3SP-3        (Pavan  ) (197555)           ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                5/09/2016  --------
-- defect  QE3SP-24      (Mark   ) (201839)           ------------------------------
------------------------------------------------------------------------------------

update frp.global_parms set description='Adjustable value from 1 to MAX_AUTO_RENEW_DATE_RANGE. Default to 0.',
updated_on = sysdate, updated_by = 'QE3SP-24' where name='FS_AUTO_RENEW_DATE_RANGE';

update frp.global_parms set description='MAX_AUTO_RENEW_DATE_RANGE should be >= FS_AUTO_RENEW_DATE_RANGE. Default to 30.',
updated_on = sysdate, updated_by = 'QE3SP-24' where name='MAX_AUTO_RENEW_DATE_RANGE';

------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                5/09/2016  --------
-- defect  QE3SP-24      (Mark   ) (201839)           ------------------------------
------------------------------------------------------------------------------------

