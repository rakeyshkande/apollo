------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

grant execute on clean.order_invoice_pkg to osp;

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 7/12/2016  --------
-- defect QE3SP-45        (Mark   ) (211560)            ----------------------------
------------------------------------------------------------------------------------

grant execute on ojms.delete_mercury_process_poll to osp, quartz;

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                 7/12/2016  --------
-- defect QE3SP-45        (Mark   ) (211560)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 9/15/2016  --------
-- defect ABA-58          (Mark   ) (225658)            ----------------------------
------------------------------------------------------------------------------------

-- This job runs at 6am Monday through Friday. The script assumes it is being scheduled
-- on a Friday or Saturday. You may need to adjust if you run it on a different day.
alter user clean identified by "serveroutput" account unlock;
connect clean/serveroutput
set serveroutput on
declare
   v_job number;
begin
   dbms_job.submit(v_job,'order_invoice_pkg.start_ariba_invoicing;',next_day(trunc(sysdate)+(6/24),'MONDAY'),
   'case when trim(to_char(sysdate,''DAY'')) in (''FRIDAY'',''SATURDAY'',''SUNDAY'') then next_day(trunc(sysdate)+(6/24),''MONDAY'') else trunc(sysdate)+1+(6/24) end');
   dbms_output.put_line(to_char(v_job));
end;
/
commit;
connect <dba>
alter user clean account lock;

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 9/15/2016  --------
-- defect ABA-58          (Mark   ) (225658)            ----------------------------
------------------------------------------------------------------------------------
