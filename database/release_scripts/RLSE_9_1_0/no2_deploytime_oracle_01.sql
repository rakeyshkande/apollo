------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 5/25/2016  --------
-- defect QE3SP-19        (Pavan  ) (204164)         --------------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
ALTER TABLE FTD_APPS.PRODUCT_MASTER ADD PERSONALIZATION_CASE_FLAG VARCHAR2(1);

COMMENT ON COLUMN FTD_APPS.PRODUCT_MASTER.PERSONALIZATION_CASE_FLAG IS 
'Flag to check if the personalization data sent to vendors for PC orders has to be capitalized or not';

ALTER TABLE FTD_APPS.PRODUCT_MASTER$ ADD PERSONALIZATION_CASE_FLAG VARCHAR2(1);

------------------------------------------------------------------------------------
-- End requested by Manasa S                                   5/25/2016  --------
-- defect QE3SP-19        (Pavan  ) (204164)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
alter table ftd_apps.source_master add calculate_tax_flag char  default 'Y';
alter table ftd_apps.source_master add custom_shipping_carrier varchar2(3);
alter table ftd_apps.source_master$ add calculate_tax_flag char  default 'Y';
alter table ftd_apps.source_master$ add custom_shipping_carrier varchar2(3);

-- For UPS partner
update ftd_apps.source_master set calculate_tax_flag = 'N',custom_shipping_carrier = 'UPS' where source_code ='77806';
 
------------------------------------------------------------------------------------
-- End requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                  4/22/2016  --------
-- defect  QE3SP-3       (Pavan  ) (197555)           ------------------------------
------------------------------------------------------------------------------------
update frp.global_parms set value='0' where name='FS_AUTO_RENEW_DATE_RANGE' and context='SERVICES_FREESHIPPING';

update frp.global_parms set value='30' where name='MAX_AUTO_RENEW_DATE_RANGE' and context='SERVICES_FREESHIPPING';
------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                  4/22/2016  --------
-- defect  QE3SP-3        (Pavan  ) (197555)           ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                5/09/2016  --------
-- defect  QE3SP-24      (Mark   ) (201839)           ------------------------------
------------------------------------------------------------------------------------

update frp.global_parms set description='Adjustable value from 1 to MAX_AUTO_RENEW_DATE_RANGE. Default to 0.',
updated_on = sysdate, updated_by = 'QE3SP-24' where name='FS_AUTO_RENEW_DATE_RANGE';

update frp.global_parms set description='MAX_AUTO_RENEW_DATE_RANGE should be >= FS_AUTO_RENEW_DATE_RANGE. Default to 30.',
updated_on = sysdate, updated_by = 'QE3SP-24' where name='MAX_AUTO_RENEW_DATE_RANGE';

------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                5/09/2016  --------
-- defect  QE3SP-24      (Mark   ) (201839)           ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE  MERCURY.LIFECYCLE_STATUS  ADD STATUS_URL VARCHAR2(4000);

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 4/29/2016  --------
-- defect QE3SP-20        (Pavan  ) (198933)            --------------------------------
------------------------------------------------------------------------------------

update joe.element_config set VALIDATE_FLAG='Y',
VALIDATE_EXP='JOE_VALIDATE.BillingFieldsValidation', VALIDATE_EXP_TYPE_NAME='FUNCTION' 
WHERE  ELEMENT_ID in ('customerFirstName','customerLastName','customerAddress','customerCity');

update joe.element_config set VALIDATE_EXP_TYPE_NAME='FUNCTION', VALIDATE_EXP='JOE_VALIDATE.EmailValidation'
where ELEMENT_ID='customerEmailAddress';

update joe.element_config set VALIDATE_FLAG='Y',VALIDATE_EXP_TYPE_NAME='FUNCTION', 
VALIDATE_EXP='JOE_VALIDATE.CardMessageValidation' where ELEMENT_ID='cardMessage';


------------------------------------------------------------------------------------
-- End requested by Manasa S                                 4/29/2016  --------
-- defect QE3SP-20        (Pavan  ) (198933)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Dharma teja                                 5/10/2016  --------
-- defect QE3SP-24        (Pavan  ) (201839)            --------------------------------
------------------------------------------------------------------------------------


update frp.global_parms set description='Adjustable value from 1 to MAX_AUTO_RENEW_DATE_RANGE. Default to 0.',UPDATED_BY='QE3SP-24',
UPDATED_ON=trunc(sysdate) where name='FS_AUTO_RENEW_DATE_RANGE';

update frp.global_parms set description='MAX_AUTO_RENEW_DATE_RANGE should be >= FS_AUTO_RENEW_DATE_RANGE. Default to 30.',
UPDATED_BY='QE3SP-24',UPDATED_ON=trunc(sysdate) where name='MAX_AUTO_RENEW_DATE_RANGE';

------------------------------------------------------------------------------------
-- End requested by Dharma teja                                 5/10/2016  --------
-- defect QE3SP-24        (pavan  ) (201839)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 5/13/2016  --------
-- defect QE3SP-15        (Syed  ) (202490)         --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE JOE.ORDERS ADD BUYER_HAS_FREE_SHIPPING CHAR(1);

ALTER TABLE SCRUB.ORDERS ADD BUYER_HAS_FREE_SHIPPING CHAR(1);

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                   5/13/2016  --------
-- defect QE3SP-15        (Syed  ) (202490)         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 5/30/2016  --------
-- defect QE3SP-23        (Pavan  ) (204742)         --------------------------------
------------------------------------------------------------------------------------
ALTER TABLE SCRUB.BUYER_PHONES ADD (PHONE_NUMBER_TYPE varchar2(4000),SMS_OPT_IN varchar2(1));
------------------------------------------------------------------------------------
-- End requested by Gunadeep B                                   5/30/2016  --------
-- defect QE3SP-23        (Pavan  ) (204742)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 6/2/2016  --------
-- defect QE3SP-23        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------

update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-primary-phone' where XML_NODE_NAME = 'buyer-daytime-phone';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-primary-phone-ext' where XML_NODE_NAME = 'buyer-work-ext';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-secondary-phone',
error_txt = 'Optional field, but, if you enter the Phone 2, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.', 
Title_Txt = 'Optional field, but, if you enter the Phone 2, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.'
where XML_NODE_NAME = 'buyer-evening-phone';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-secondary-phone-ext',Title_Txt = 'Phone 2 extension (numbers only)' where XML_NODE_NAME = 'buyer-evening-ext';

------------------------------------------------------------------------------------
-- End requested by Manasa S                                   6/2/2016  --------
-- defect QE3SP-23        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 6/2/2016  --------
-- defect QE3SP-1        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------

grant update on clean.order_florist_used to venus;
grant update on clean.order_details to venus;

------------------------------------------------------------------------------------
-- End requested by Manasa S                                   6/2/2016  --------
-- defect QE3SP-1        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by bhargav swami                                 6/15/2016  --------
-- defect ABA-21        (Pavan  ) ( 207430)         --------------------------------
------------------------------------------------------------------------------------
grant select on ftd_apps.source_master to venus;
 
------------------------------------------------------------------------------------
-- End requested by bhargav swami                                 6/15/2016  --------
-- defect ABA-21        (Pavan  ) ( 207430)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 6/16/2016  --------
-- defect QE3SP-5        (Syed  ) (206725)         --------------------------------
------------------------------------------------------------------------------------


INSERT INTO quartz_schema.pipeline
(
                pipeline_id,
                group_id,
                description,
                tool_tip,
                default_schedule,
                default_payload,
                force_default_payload,
                queue_name,
                class_code,
                active_flag
)
VALUES
(
                'ORDER_LIFECYCLE_UPDATE',
                'OP',
                'Mark Order as Confirmed in MNAPI',
                'This process will mark the status id passed in as confirmed in MNAPI.  The payload for confirming an order is the status id + |CONFIRM_ONLY (i.e. 123456789|CONFIRM_ONLY )',
                null,
                null,
                'N',
                'OJMS.DCON_LIFECYCLE_UPDATE',
                'SimpleJmsJob',
                'Y'
);

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk                                 6/16/2016  --------
-- defect QE3SP-5        (Syed  ) (206725)         --------------------------------
------------------------------------------------------------------------------------

 ------------------------------------------------------------------------------------
-- Begin requested by Manasa salla                                 6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )         --------------------------------
------------------------------------------------------------------------------------

--------Flags to ptn_pi.partner_mapping
-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
Alter table ptn_pi.partner_mapping add apply_def_ptn_behavior char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_STATUS_UPDATE char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_INVOICE char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_ADJ_FEED char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_FMT_FEED char(1) default 'N';

------- Update the flags for existing partners
Update ptn_pi.partner_mapping set apply_def_ptn_behavior = 'Y'  where ftd_origin_mapping <> 'ARI';
Update ptn_pi.partner_mapping set SEND_ORD_ADJ_FEED = 'Y'  where ftd_origin_mapping <> 'ARI';
Update ptn_pi.partner_mapping set SEND_ORD_FMT_FEED = 'Y'  where ftd_origin_mapping <> 'ARI';

----------Create new Partner Ari
Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,
BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,
CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,
APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) VALUES (
'ARB_SCHWAB','Ariba Charles Schwab','ARI','-NA-','-NA-','-NA-','9095','9095','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US',
'Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,
BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,
CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,
APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_SAKS','Ariba SAKS','ARI','-NA-','-NA-','-NA-','7798','7798','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US','Y','Y','Y',
'N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,
SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,
SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_MS','Ariba Money Services','ARI','-NA-','-NA-','-NA-','7657','7657','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US','Y',
'Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');


Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,
SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,
SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY,
 UPDATED_ON, UPDATED_BY) VALUES (
'ARB_UPS','Ariba UPS','ARI','-NA-','-NA-','-NA-','77806','77806','3113 Woodcreek Drive',' ','Downers Grove','IL',
'60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','Y','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,
PARTNER_IMAGE,DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,
SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,
SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_5STAR','Ariba 5 Star Senior Living','ARI','-NA-','-NA-','-NA-','24796','24796','3113 Woodcreek Drive',' ','Downers Grove',
'IL','60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,
PARTNER_IMAGE,DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,
BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,
SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,
AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,
INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_LFG','Ariba Lincoln Financial Group','ARI','-NA-','-NA-','-NA-','20233','20233','3113 Woodcreek Drive',' ','Downers Grove',
'IL','60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');


----------Update the flags for Ari partners
Update ptn_pi.partner_mapping set SEND_ORD_STATUS_UPDATE = 'Y'  where ftd_origin_mapping = 'ARI';

----------Create new table
CREATE TABLE "PTN_PI"."ORDER_STATUS_UPDATE" ( 
        "ORDER_STATUS_UPDATE_ID" NUMBER NOT NULL ENABLE,  
                "CONFIRMATION_NUMBER" VARCHAR(100) NOT NULL ENABLE,  
        "FEED_ID" NUMBER, 
                "TYPE" VARCHAR(20) NOT NULL ENABLE,
                        "CREATED_ON" DATE NOT NULL ENABLE, 
                        "CREATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                        "UPDATED_ON" DATE NOT NULL ENABLE, 
                        "UPDATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                        CONSTRAINT "ORDER_STATUS_UPD_PK" PRIMARY KEY ("ORDER_STATUS_UPDATE_ID"),   
                        CONSTRAINT "FK_ORD_STATUS_UPD_FEED_ID" FOREIGN KEY ("FEED_ID") 
                                        REFERENCES "PTN_PI"."PARTNER_FEEDS" ("FEED_ID") ENABLE 
   );

--------------Create a new sequence for partner order status updates
CREATE SEQUENCE  PTN_PI.ORD_STATUS_UPD_REF_SQ  
                                MINVALUE 1 MAXVALUE 9999999999 
                                INCREMENT BY 1 
                                START WITH 1 
                                CACHE 20 NOORDER  NOCYCLE ;

-------------- Insert scripts


INSERT INTO FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'PI_CONFIG',
    'ORD_STAT_TO_PROCESS',
    'ACCEPTED,REMOVED,ADJUSTED,SHIPPED',
    'ABA-19',
    sysdate,
    'ABA-19',
    sysdate,
    'Order Status Update will be triggered only for these configured types currently'
    );
	
INSERT INTO FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'PI_CONFIG',
    'EsbOrderStatusUpdGathererUrl',
    '{replace-me}',
    'ABA-19',
    sysdate,
    'ABA-19',
    sysdate,
    'ESB Order Status Update Gatherer URL used for sending Order Status Update feeds to ESB'
    );

INSERT INTO quartz_schema.pipeline (
	pipeline_id, group_id, description, tool_tip, default_schedule, force_default_payload,
	queue_name, class_code, active_flag
) VALUES (
	'PI_ORDER_STATUS_UPDATE','PARTNER_INTEGRATION','Create Order Status Updates',
	'action:PROCESS_CREATE_ORD_STATUS_UPDATES|orderNumbers:comma separated order numbers|operation:valid action type', '300',   
	'N', 'OJMS.PARTNER_ORDERS', 'SimpleJmsJob','Y'
);

INSERT INTO quartz_schema.pipeline (
	pipeline_id, group_id, description, tool_tip, default_schedule, default_payload, force_default_payload,
	queue_name, class_code, active_flag
) VALUES (
	'PI_SEND_ORDER_STATUS_UPDATE','PARTNER_INTEGRATION', 'Send Order Status Updates to ESB',
	'Sends Order Status updates with status NEW to ESB', '300',  
	'PROCESS_SEND_ORDER_STATUS_UPDATES', 'Y', 'OJMS.PARTNER_ORDERS', 'SimpleJmsJob', 'Y'
);

  
------------------------------------------------------------------------------------
-- End requested by Manasa salla                                 6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gunaddep B                                7/1/2016  --------
-- defect QE3SP-23        (Pavan  ) (210104)            --------------------------------
------------------------------------------------------------------------------------
 
-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
alter table clean.customer_phones add (PHONE_NUMBER_TYPE VARCHAR2(4000),SMS_OPT_IN VARCHAR2(1)) ;
  
------------------------------------------------------------------------------------
-- End requested by Gunaddep B                                 7/1/2016  --------
-- defect QE3SP-23         (Pavan  ) ( 210104 )         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
ALTER TABLE clean.orders ADD invoice_status VARCHAR2(20);

COMMENT ON COLUMN clean.orders.invoice_status IS 'Order invoice status - Currently applicable for Ariba orders only';

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Bhargav                                 7/13/2016  --------
-- defect ABA-20        (Pavan  ) (211681)            --------------------------------
------------------------------------------------------------------------------------
 
-- =================================================================================
-- THIS IS A GOLDEN GATE CHANGE
-- ---------------------------------------------------------------------------------
Alter table ptn_pi.partner_mapping add SEND_SHIP_STATUS_UPDATE char(1) default 'N';

update Ptn_Pi.Partner_Mapping set Send_Ship_Status_Update='Y' Where Ftd_Origin_Mapping='ARI';

------------------------------------------------------------------------------------
-- End requested by Bhargav                                7/13/2016  --------
-- defect ABA-20         (Pavan  ) ( 211681 )         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by UNKNOWN                                    9/21/2016  --------
-- defect UNKNOWN         (Mark   ) (      )            ----------------------------
------------------------------------------------------------------------------------

-- This was discovered during the training deploy. Not sure how this got missed.
grant select on scrub.order_details to ftd_apps;

------------------------------------------------------------------------------------
-- End   requested by UNKNOWN                                    9/21/2016  --------
-- defect UNKNOWN         (Mark   ) (      )            ----------------------------
------------------------------------------------------------------------------------

