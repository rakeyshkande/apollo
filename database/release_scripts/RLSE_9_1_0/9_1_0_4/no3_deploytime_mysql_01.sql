------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                6/1/2016  --------
-- defect  QE3SP-35        (Syed  ) (204613 )           --------------------------------
------------------------------------------------------------------------------------


-- This Required GG changes to add this existing table in replication, initialization need to be performed


use ftd_apps;

CREATE TABLE codified_products (
	PRODUCT_ID varchar(10) NOT NULL,
	HOLIDAY_CODIFIED_FLAG varchar(1) NOT NULL,	
	CHECK_OE_FLAG varchar(10) NOT NULL,
	FLAG_SEQUENCE decimal(38,0) ,
	CODIFIED_SPECIAL varchar(1) NOT NULL,
	CODIFICATION_ID varchar(5) ,
  PRIMARY KEY (PRODUCT_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                6/1/2016  --------
-- defect  QE3SP-35        (Syed  ) (204613 )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------
ALTER TABLE ftd_apps.source_master ADD calculate_tax_flag CHAR  DEFAULT 'Y';
ALTER TABLE ftd_apps.source_master ADD custom_shipping_carrier VARCHAR(3);
ALTER TABLE ftd_apps.source_master$ ADD calculate_tax_flag CHAR  DEFAULT 'Y';
ALTER TABLE ftd_apps.source_master$ ADD custom_shipping_carrier VARCHAR(3);

------------------------------------------------------------------------------------
-- End requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Manasa salla                               6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )       --------------------------------
------------------------------------------------------------------------------------

use ptn_pi;
Alter table partner_mapping add column APPLY_DEF_PTN_BEHAVIOR char(1);
Alter table partner_mapping add column SEND_ORD_STATUS_UPDATE char(1);
Alter table partner_mapping add column SEND_ORD_INVOICE char(1);
Alter table partner_mapping add column SEND_ORD_ADJ_FEED char(1);
Alter table partner_mapping add column SEND_ORD_FMT_FEED char(1);

------------------------------------------------------------------------------------
-- End   requested by Manasa salla                               6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )       --------------------------------
------------------------------------------------------------------------------------

