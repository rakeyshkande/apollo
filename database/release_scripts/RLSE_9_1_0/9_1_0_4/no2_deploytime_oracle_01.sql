
------------------------------------------------------------------------------------
-- Begin requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------
alter table ftd_apps.source_master add calculate_tax_flag char  default 'Y';
alter table ftd_apps.source_master add custom_shipping_carrier varchar2(3);
alter table ftd_apps.source_master$ add calculate_tax_flag char  default 'Y';
alter table ftd_apps.source_master$ add custom_shipping_carrier varchar2(3);

-- For UPS partner
update ftd_apps.source_master set calculate_tax_flag = 'N',custom_shipping_carrier = 'UPS' where source_code ='77806';
 
------------------------------------------------------------------------------------
-- End requested by Dharma teja                                 6/15/2016  --------
-- defect ABA-22        (Pavan  ) ( 207222)         --------------------------------
------------------------------------------------------------------------------------





------------------------------------------------------------------------------------
-- Begin requested by bhargav swami                                 6/15/2016  --------
-- defect ABA-21        (Pavan  ) ( 207430)         --------------------------------
------------------------------------------------------------------------------------
grant select on ftd_apps.source_master to venus;
 
------------------------------------------------------------------------------------
-- End requested by bhargav swami                                 6/15/2016  --------
-- defect ABA-21        (Pavan  ) ( 207430)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 6/16/2016  --------
-- defect QE3SP-5        (Syed  ) (206725)         --------------------------------
------------------------------------------------------------------------------------


INSERT INTO quartz_schema.pipeline
(
                pipeline_id,
                group_id,
                description,
                tool_tip,
                default_schedule,
                default_payload,
                force_default_payload,
                queue_name,
                class_code,
                active_flag
)
VALUES
(
                'ORDER_LIFECYCLE_UPDATE',
                'OP',
                'Mark Order as Confirmed in MNAPI',
                'This process will mark the status id passed in as confirmed in MNAPI.  The payload for confirming an order is the status id + |CONFIRM_ONLY (i.e. 123456789|CONFIRM_ONLY )',
                null,
                null,
                'N',
                'OJMS.DCON_LIFECYCLE_UPDATE',
                'SimpleJmsJob',
                'Y'
);

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk                                 6/16/2016  --------
-- defect QE3SP-5        (Syed  ) (206725)         --------------------------------
------------------------------------------------------------------------------------



 ------------------------------------------------------------------------------------
-- Begin requested by Manasa salla                                 6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )         --------------------------------
------------------------------------------------------------------------------------

--------Flags to ptn_pi.partner_mapping
Alter table ptn_pi.partner_mapping add apply_def_ptn_behavior char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_STATUS_UPDATE char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_INVOICE char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_ADJ_FEED char(1) default 'N';
Alter table ptn_pi.partner_mapping add SEND_ORD_FMT_FEED char(1) default 'N';

------- Update the flags for existing partners
Update ptn_pi.partner_mapping set apply_def_ptn_behavior = 'Y'  where ftd_origin_mapping <> 'ARI';
Update ptn_pi.partner_mapping set SEND_ORD_ADJ_FEED = 'Y'  where ftd_origin_mapping <> 'ARI';
Update ptn_pi.partner_mapping set SEND_ORD_FMT_FEED = 'Y'  where ftd_origin_mapping <> 'ARI';

----------Create new Partner � Ari
Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,
BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,
CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,
APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) VALUES (
'ARB_SCHWAB','Ariba Charles Schwab','ARI','-NA-','-NA-','-NA-','9095','9095','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US',
'Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,
BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,
CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,
APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_SAKS','Ariba SAKS','ARI','-NA-','-NA-','-NA-','7798','7798','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US','Y','Y','Y',
'N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,
SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,
SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_MS','Ariba Money Services','ARI','-NA-','-NA-','-NA-','7657','7657','3113 Woodcreek Drive',' ','Downers Grove','IL','60515','US','Y',
'Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');


Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,PARTNER_IMAGE,
DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,
SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,
SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY,
 UPDATED_ON, UPDATED_BY) VALUES (
'ARB_UPS','Ariba UPS','ARI','-NA-','-NA-','-NA-','77806','77806','3113 Woodcreek Drive',' ','Downers Grove','IL',
'60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','Y','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,
PARTNER_IMAGE,DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE,
BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,
SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,
SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_5STAR','Ariba 5 Star Senior Living','ARI','-NA-','-NA-','-NA-','24796','24796','3113 Woodcreek Drive',' ','Downers Grove',
'IL','60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');

Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,CONF_NUMBER_PREFIX,
PARTNER_IMAGE,DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,BILLING_ADDRESS_LINE_2,BILLING_CITY,
BILLING_STATE_PROVINCE,BILLING_POSTAL_CODE,BILLING_COUNTRY,SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,
SUN_DELIVERY_ALLOWED_FLORAL,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL,SEND_DELIVERY_CONF_EMAIL,
AVS_FLORAL,AVS_DROPSHIP,SEND_DCON_FEED,UPDATE_ORDER_FLAG,SEND_ORD_STATUS_UPDATE,SEND_ORD_INVOICE,APPLY_DEF_PTN_BEHAVIOR,
INCLUDE_ADDON,SEND_ORD_ADJ_FEED,SEND_ORD_FMT_FEED,CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (
'ARB_LFG','Ariba Lincoln Financial Group','ARI','-NA-','-NA-','-NA-','20233','20233','3113 Woodcreek Drive',' ','Downers Grove',
'IL','60515','US','Y','Y','Y','N','N','N','N','N','Y','N','N','Y','N','N','N','N','N',SYSDATE,'ARA-19',SYSDATE,'ARA-19');


----------Update the flags for Ari partners
Update ptn_pi.partner_mapping set SEND_ORD_STATUS_UPDATE = 'Y'  where ftd_origin_mapping = 'ARI';

----------Create new table
CREATE TABLE "PTN_PI"."ORDER_STATUS_UPDATE" ( 
        "ORDER_STATUS_UPDATE_ID" NUMBER NOT NULL ENABLE,  
                "CONFIRMATION_NUMBER" VARCHAR(100) NOT NULL ENABLE,  
        "FEED_ID" NUMBER, 
                "TYPE" VARCHAR(20) NOT NULL ENABLE,
                        "CREATED_ON" DATE NOT NULL ENABLE, 
                        "CREATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                        "UPDATED_ON" DATE NOT NULL ENABLE, 
                        "UPDATED_BY" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
                        CONSTRAINT "ORDER_STATUS_UPD_PK" PRIMARY KEY ("ORDER_STATUS_UPDATE_ID"),   
                        CONSTRAINT "FK_ORD_STATUS_UPD_FEED_ID" FOREIGN KEY ("FEED_ID") 
                                        REFERENCES "PTN_PI"."PARTNER_FEEDS" ("FEED_ID") ENABLE 
   );

--------------Create a new sequence for partner order status updates
CREATE SEQUENCE  PTN_PI.ORD_STATUS_UPD_REF_SQ  
                                MINVALUE 1 MAXVALUE 9999999999 
                                INCREMENT BY 1 
                                START WITH 1 
                                CACHE 20 NOORDER  NOCYCLE ;

-------------- Insert scripts


INSERT INTO FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'PI_CONFIG',
    'ORD_STAT_TO_PROCESS',
    'ACCEPTED,REMOVED,ADJUSTED,SHIPPED',
    'ABA-19',
    sysdate,
    'ABA-19',
    sysdate,
    'Order Status Update will be triggered only for these configured types currently'
    );
	
INSERT INTO FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'PI_CONFIG',
    'EsbOrderStatusUpdGathererUrl',
    '{replace-me}',
    'ABA-19',
    sysdate,
    'ABA-19',
    sysdate,
    'ESB Order Status Update Gatherer URL used for sending Order Status Update feeds to ESB'
    );

INSERT INTO quartz_schema.pipeline (
	pipeline_id, group_id, description, tool_tip, default_schedule, force_default_payload,
	queue_name, class_code, active_flag
) VALUES (
	'PI_ORDER_STATUS_UPDATE','PARTNER_INTEGRATION','Create Order Status Updates',
	'action:PROCESS_CREATE_ORD_STATUS_UPDATES|orderNumbers:comma separated order numbers|operation:valid action type', '300',   
	'N', 'OJMS.PARTNER_ORDERS', 'SimpleJmsJob','Y'
);

INSERT INTO quartz_schema.pipeline (
	pipeline_id, group_id, description, tool_tip, default_schedule, default_payload, force_default_payload,
	queue_name, class_code, active_flag
) VALUES (
	'PI_SEND_ORDER_STATUS_UPDATE','PARTNER_INTEGRATION', 'Send Order Status Updates to ESB',
	'Sends Order Status updates with status NEW to ESB', '300',  
	'PROCESS_SEND_ORDER_STATUS_UPDATES', 'Y', 'OJMS.PARTNER_ORDERS', 'SimpleJmsJob', 'Y'
);

  
------------------------------------------------------------------------------------
-- End requested by Manasa salla                                 6/23/2016  --------
-- defect ABA-19         (Pavan  ) ( 208622 )         --------------------------------
------------------------------------------------------------------------------------

