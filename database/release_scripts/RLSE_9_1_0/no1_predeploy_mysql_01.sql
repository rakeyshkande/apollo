------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                6/1/2016  --------
-- defect  QE3SP-35        (Syed  ) (204613 )           --------------------------------
------------------------------------------------------------------------------------

-- This Required GG changes to add this existing table in replication, initialization need to be performed
-- Remember to add supplemental logging. Use FTD_APPS group 3.

use ftd_apps;

CREATE TABLE codified_products (
	PRODUCT_ID varchar(10) NOT NULL,
	HOLIDAY_CODIFIED_FLAG varchar(1) NOT NULL,	
	CHECK_OE_FLAG varchar(10) NOT NULL,
	FLAG_SEQUENCE decimal(38,0) ,
	CODIFIED_SPECIAL varchar(1) NOT NULL,
	CODIFICATION_ID varchar(5) ,
  PRIMARY KEY (PRODUCT_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                6/1/2016  --------
-- defect  QE3SP-35        (Syed  ) (204613 )           --------------------------------
------------------------------------------------------------------------------------

