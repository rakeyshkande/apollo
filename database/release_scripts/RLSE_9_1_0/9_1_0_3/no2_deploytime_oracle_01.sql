------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE  MERCURY.LIFECYCLE_STATUS  ADD STATUS_URL VARCHAR2(4000);

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------

 
 
------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 5/13/2016  --------
-- defect QE3SP-15        (Syed  ) (202490)         --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE JOE.ORDERS ADD BUYER_HAS_FREE_SHIPPING CHAR(1);

ALTER TABLE SCRUB.ORDERS ADD BUYER_HAS_FREE_SHIPPING CHAR(1);


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                   5/13/2016  --------
-- defect QE3SP-15        (Syed  ) (202490)         --------------------------------
------------------------------------------------------------------------------------


 


------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 5/25/2016  --------
-- defect QE3SP-19        (Pavan  ) (204164)         --------------------------------
------------------------------------------------------------------------------------


ALTER TABLE FTD_APPS.PRODUCT_MASTER ADD PERSONALIZATION_CASE_FLAG VARCHAR2(1);

COMMENT ON COLUMN FTD_APPS.PRODUCT_MASTER.PERSONALIZATION_CASE_FLAG IS 'Flag to check if the personalization data sent to vendors for PC orders has to be capitalized or not';

ALTER TABLE FTD_APPS.PRODUCT_MASTER$ ADD PERSONALIZATION_CASE_FLAG VARCHAR2(1);


------------------------------------------------------------------------------------
-- End requested by Manasa S                                   5/25/2016  --------
-- defect QE3SP-19        (Pavan  ) (204164)         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 5/30/2016  --------
-- defect QE3SP-23        (Pavan  ) (204742)         --------------------------------
------------------------------------------------------------------------------------
ALTER TABLE SCRUB.BUYER_PHONES ADD (PHONE_NUMBER_TYPE varchar2(4000),SMS_OPT_IN varchar2(1));
------------------------------------------------------------------------------------
-- End requested by Gunadeep B                                   5/30/2016  --------
-- defect QE3SP-23        (Pavan  ) (204742)         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 6/2/2016  --------
-- defect QE3SP-23        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-primary-phone' where XML_NODE_NAME = 'buyer-daytime-phone';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-primary-phone-ext' where XML_NODE_NAME = 'buyer-work-ext';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-secondary-phone',
error_txt = 'Optional field, but, if you enter the Phone 2, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.', 
Title_Txt = 'Optional field, but, if you enter the Phone 2, it must be 10 digits for domestic phone numbers.  International can be up to 20 digits.'
where XML_NODE_NAME = 'buyer-evening-phone';
update JOE.ELEMENT_CONFIG set XML_NODE_NAME = 'buyer-secondary-phone-ext',Title_Txt = 'Phone 2 extension (numbers only)' where XML_NODE_NAME = 'buyer-evening-ext';


------------------------------------------------------------------------------------
-- End requested by Manasa S                                   6/2/2016  --------
-- defect QE3SP-23        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 6/2/2016  --------
-- defect QE3SP-1        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------
grant update on clean.order_florist_used to venus;
grant update on clean.order_details to venus;
------------------------------------------------------------------------------------
-- End requested by Manasa S                                   6/2/2016  --------
-- defect QE3SP-1        (Pavan  ) (205209)         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Tim Schming                                 6/2/2016  --------
-- defect DCE-1        (Syed  ) (204805)         --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ORDER_LIFECYCLE',
    'LIFECYCLE_DELIVERY_EXPIRED_DAYS',
    '180',
    'DCE-1',
    sysdate,
    'DCE-1',
    sysdate,
    'Number of days an Order Lifecycle Delivery Confirmation status will be available');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ORDER_LIFECYCLE',
    'LIFECYCLE_DELIVERY_EXPIRED_MSG',
    'Delivery Confirmation Information is no longer available',
    'DCE-1',
    sysdate,
    'DCE-1',
    sysdate,
    'Hover message to be displayed after an Order Lifecycle Delivery Confirmation status has expired'); 

------------------------------------------------------------------------------------
-- End requested by Tim Schming                                 6/2/2016  --------
-- defect DCE-1        (Syed  ) (204805)         --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 6/2/2016  --------
-- defect QE3SP-10.        (Syed  ) ( 205207)         --------------------------------
------------------------------------------------------------------------------------



insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_TIMEOUT_MAX_RETRIES', '10', 'QE3SP-10', sysdate, 'QE3SP-10', sysdate, 'Number of attempts to retry getting product availability from PAC before defaulting ship date to delivery date');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_TIMEOUT_DELAY', '30000', 'QE3SP-10', sysdate, 'QE3SP-10', sysdate, 'Time interval in milliseconds at which time PAC product availability will be retried.');

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                 6/2/2016  --------
-- defect QE3SP-10.        (Syed  ) ( 205207)         --------------------------------
------------------------------------------------------------------------------------


 