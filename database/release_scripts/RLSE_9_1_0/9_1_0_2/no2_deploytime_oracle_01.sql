------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE  MERCURY.LIFECYCLE_STATUS  ADD STATUS_URL VARCHAR2(4000);

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                 4/26/2016  --------
-- defect DCE-1        (Syed  ) (197631)            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Manasa S                                 4/29/2016  --------
-- defect QE3SP-20        (Pavan  ) (198933)            --------------------------------
------------------------------------------------------------------------------------

update joe.element_config set VALIDATE_FLAG='Y',
VALIDATE_EXP='JOE_VALIDATE.BillingFieldsValidation', VALIDATE_EXP_TYPE_NAME='FUNCTION' 
WHERE  ELEMENT_ID in ('customerFirstName','customerLastName','customerAddress','customerCity');

update joe.element_config set VALIDATE_EXP_TYPE_NAME='FUNCTION', VALIDATE_EXP='JOE_VALIDATE.EmailValidation'
where ELEMENT_ID='customerEmailAddress';

update joe.element_config set VALIDATE_FLAG='Y',VALIDATE_EXP_TYPE_NAME='FUNCTION', 
VALIDATE_EXP='JOE_VALIDATE.CardMessageValidation' where ELEMENT_ID='cardMessage';


------------------------------------------------------------------------------------
-- End requested by Manasa S                                 4/29/2016  --------
-- defect QE3SP-20        (Pavan  ) (198933)            --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Venu G                                 5/13/2016  --------
-- defect  QE3SP-4        (Pavan  ) (202300)            --------------------------------
------------------------------------------------------------------------------------
update frp.global_parms set name ='SKIP_MESSAGES_BY_MARS' where name ='SKIP_MASSAGES_BY_MARS';

------------------------------------------------------------------------------------
-- End requested by Venu G                                5/13/2016  --------
-- defect  QE3SP-4        (Pavan  ) (202300)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Dharma teja                                 5/10/2016  --------
-- defect QE3SP-24        (Pavan  ) (201839)            --------------------------------
------------------------------------------------------------------------------------


update frp.global_parms set description='Adjustable value from 1 to MAX_AUTO_RENEW_DATE_RANGE. Default to 0.',UPDATED_BY='QE3SP-24',
UPDATED_ON=trunc(sysdate) where name='FS_AUTO_RENEW_DATE_RANGE';

update frp.global_parms set description='MAX_AUTO_RENEW_DATE_RANGE should be >= FS_AUTO_RENEW_DATE_RANGE. Default to 30.',
UPDATED_BY='QE3SP-24',UPDATED_ON=trunc(sysdate) where name='MAX_AUTO_RENEW_DATE_RANGE';

------------------------------------------------------------------------------------
-- End requested by Dharma teja                                 5/10/2016  --------
-- defect QE3SP-24        (pavan  ) (201839)            --------------------------------
------------------------------------------------------------------------------------

