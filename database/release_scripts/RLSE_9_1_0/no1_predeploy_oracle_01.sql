------------------------------------------------------------------------------------
-- begin requested by manasa S                                  4/18/2016  --------
-- defect QE3SP-16        (Pavan  ) (196638)           ------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS values
('ORDER_GATHERER_CONFIG','VALID_ORDER_PREFIXES','ATC,ATA,ARS,AM,AN,AT,AA,AR,AI,AG,AJ,AH,CAS,C,FA,FR,FM,FN,FT,FI,FG,FJ,FH,BC,Z,Y,X,T',
'QE3SP-16',SYSDATE,'QE3SP-16',SYSDATE,'This parm is used to validate the order prefixes');  


------------------------------------------------------------------------------------
-- end   requested by Manasa s                                  4/18/2016  --------
-- defect QE3SP-16        (Pavan  ) (196638)           ------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Sunil sana                                  4/22/2016  --------
-- defect  DCE-7        (Pavan  ) (197340)           ------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('CUSTOMER_ORDER_MANAGEMENT', 'ORDER_COMMENTS_SUPPRESS_USERIDs', '{replace-me}', 'DCE-7', SYSDATE, 'DCE-7', SYSDATE,
 'Order comments provided by these userIDs (comma seperated) will be suppressed from display in OrderComments page - Comments tab.');

------------------------------------------------------------------------------------
-- end   requested by Sunil sana                                  4/22/2016  --------
-- defect  DCE-7        (Pavan  ) (197340)           ------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gutala venu                                  4/22/2016  --------
-- defect  QE3SP-4        (Pavan  ) (197542)           ------------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms (CONTEXT, NAME,VALUE, CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
VALUES ('MARS_CONFIG',  'SKIP_MESSAGES_BY_MARS', 'GEN', 'QE3SP-4', sysdate, 'QE3SP-4', sysdate, 
'MARs will not process the specified Message Codes ');

------------------------------------------------------------------------------------
-- end   requested by Gutala Venu                                  4/22/2016  --------
-- defect  QE3SP-4        (Pavan  ) (197542)           ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schming                                 6/2/2016  --------
-- defect DCE-1        (Syed  ) (204805)         --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ORDER_LIFECYCLE',
    'LIFECYCLE_DELIVERY_EXPIRED_DAYS',
    '180',
    'DCE-1',
    sysdate,
    'DCE-1',
    sysdate,
    'Number of days an Order Lifecycle Delivery Confirmation status will be available');

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ORDER_LIFECYCLE',
    'LIFECYCLE_DELIVERY_EXPIRED_MSG',
    'Delivery Confirmation Information is no longer available',
    'DCE-1',
    sysdate,
    'DCE-1',
    sysdate,
    'Hover message to be displayed after an Order Lifecycle Delivery Confirmation status has expired'); 

------------------------------------------------------------------------------------
-- End requested by Tim Schming                                 6/2/2016  --------
-- defect DCE-1        (Syed  ) (204805)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                 6/2/2016  --------
-- defect QE3SP-10.        (Syed  ) ( 205207)         --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_TIMEOUT_MAX_RETRIES', '10', 'QE3SP-10', sysdate, 'QE3SP-10', sysdate, 
'Number of attempts to retry getting product availability from PAC before defaulting ship date to delivery date');

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'PAC_TIMEOUT_DELAY', '30000', 'QE3SP-10', sysdate, 'QE3SP-10', sysdate, 
'Time interval in milliseconds at which time PAC product availability will be retried.');

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                                 6/2/2016  --------
-- defect QE3SP-10.        (Syed  ) ( 205207)         --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

-- Global parm addition
--
insert into FRP.GLOBAL_PARMS (
CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION
) values (
'PI_CONFIG', 'ARIBA_INVOICE_DAYS_PAST_DELIVERY', '8', 'ABA-25', sysdate, 'ABA-25', sysdate, 
'Number of days after delivery date for which an order cart becomes eligible for Ariba invoicing.  For multi-cart orders the most recent delivery date of all orders in cart applies.');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Smeka                                9/13/2016  --------
-- defect ABA-19          (Pavan   ) (224955)            ----------------------------
------------------------------------------------------------------------------------

GRANT SELECT ON scrub.ORDER_details TO ptn_pi; 
GRANT SELECT ON scrub.ORDERs TO ptn_pi;

------------------------------------------------------------------------------------
-- End   requested by Smeka                                9/13/2016  --------
-- defect ABA-19          (Pavan   ) (224955)            ----------------------------
------------------------------------------------------------------------------------

