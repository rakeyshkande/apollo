------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

-- Global parm addition
--
insert into FRP.GLOBAL_PARMS (
CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION
) values (
'PI_CONFIG', 'ARIBA_INVOICE_DAYS_PAST_DELIVERY', '8', 'ABA-25', sysdate, 'ABA-25', sysdate, 
'Number of days after delivery date for which an order cart becomes eligible for Ariba invoicing.  For multi-cart orders the most recent delivery date of all orders in cart applies.');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                                7/12/2016  --------
-- defect ABA-24          (Mark   ) (211637)            ----------------------------
------------------------------------------------------------------------------------

