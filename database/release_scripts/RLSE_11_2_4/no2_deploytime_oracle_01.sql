------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi     ---------------------------------------------
-- User  task US_11802  ()         -------------------------------------------------
------------------------------------------------------------------------------------

Insert into SITESCOPE.PROJECTS (PROJECT,MAILSERVER_NAME,DESCRIPTION) values ('TAX_MS_ALERTS','barracuda.ftdi.com','Mail alert to be triggered when there is any update in tax rules');
Insert into SITESCOPE.PAGERS (PROJECT,PAGER_NUMBER) values ('TAX_MS_ALERTS', �tax-ms-dev@ftdi.com�);

GRANT EXECUTE ON ops$oracle.mail_pkg to tax;

---------------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi  ---------------------------------------------------------
-- User task US_11802         ---------------------------------------------------------------
---------------------------------------------------------------------------------------------