create database frp;
create database ftd_apps;
create database joe;
create database pas;
create database osp;

grant select, insert, update, delete, execute on osp.* to 'osp'@'%';
grant select, insert, update, delete, execute on pas.* to 'osp'@'%';
grant select, insert, update, delete, execute on joe.* to 'osp'@'%';
grant select, insert, update, delete, execute on frp.* to 'osp'@'%';
grant select, insert, update, delete, execute on ftd_apps.* to 'osp'@'%';
grant select, insert, update, delete, execute on users.* to 'osp'@'%';
grant select on mysql.* to 'osp'@'%';
grant select, insert, update, delete, execute on frp.* to 'frp'@'%';
grant select on ftd_apps.* to 'frp'@'%';
grant select, insert, update, delete, execute on ftd_apps.* to 'ftd_apps'@'%';
grant select, insert, update, delete, execute on joe.* to 'joe'@'%';
grant select, insert, update, delete, execute on pas.* to 'pas'@'%';
grant select, insert, update, delete, execute on users.* to 'users'@'%';

use frp;

create table system_messages (source varchar(100) not null, type varchar(100), message varchar(2000),
read_flag char(1) not null, timestamp datetime, 
system_message_id int(11) not null AUTO_INCREMENT,
computer varchar(256), email_subject varchar(100), escalation_level int(10), escalation_time datetime,
PRIMARY KEY(system_message_id))  engine = INNODB;

create unique index system_messages_pk on system_messages (system_message_id);

create index system_messages_n1 on system_messages (source);

create index system_messages_n2 on system_messages (type);

create index system_messages_n3 on system_messages (timestamp);

#  Add default value for frp.system_messages.read_flag
use frp;
alter table system_messages change read_flag read_flag char(1) NOT NULL DEFAULT 'N';

use ftd_apps;
alter table order_service_xsl modify column xsl_txt varchar(20000) not null;

# 6414  Allow decimals in md_redeption_rate_amt
use ftd_apps;
alter table miles_points_redemption_rate change mp_redemption_rate_amt mp_redemption_rate_amt decimal(16,6) NULL ;

# Let ftd-apps call frp function
grant execute  on  function frp.get_global_parm_value to 'ftd_apps'@'%';
grant Select   on           ftd_apps.zip_code               to 'pas'@'%';
grant Select   on           ftd_apps.state_master           to 'pas'@'%';
grant Select   on           ftd_apps.florist_codifications  to 'pas'@'%';
grant Select   on           ftd_apps.florist_zips           to 'pas'@'%';
grant Select   on           ftd_apps.florist_blocks         to 'pas'@'%';
grant Select   on           ftd_apps.florist_master         to 'pas'@'%';



## Add tables which were discovered as missing during performance test

 CREATE TABLE `cache_statistics` (
  `node_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `app_name` varchar(200) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `cache_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `cache_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `preload_flag` varchar(1) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `access_count` int(10) DEFAULT NULL,
  `refresh_count` int(10) DEFAULT NULL,
  `refresh_rate` int(10) DEFAULT NULL,
  `expire_rate` int(10) DEFAULT NULL,
  `expire_check_rate` int(10) DEFAULT NULL,
  `first_load_time` datetime DEFAULT NULL,
  `last_refresh_time` datetime DEFAULT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`node_name`,`app_name`,`cache_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



grant Execute on FUNCTION frp.get_global_parm_value to 'ftd_apps'@'%'; 
 grant execute on function frp.is_db_up to 'osp'@'%';


use ftd_apps;

create index addon_n1 on addon (product_id );

create index company_master_n1 on company_master (internet_origin );

create index country_master_n1 on country_master (country_id );

create unique index fuel_surcharge_u1 on fuel_surcharge (end_date );

create index partner_program_n1 on partner_program (program_type );

create index partner_program_n2 on partner_program (partner_name );

create index price_header_details_n1 on price_header_details (min_dollar_amt );

create index price_header_details_n2 on price_header_details (max_dollar_amt );

create index product_company_xref_n1 on product_company_xref (company_id );

create index product_master_n1 on product_master (company_id );

create unique index product_master_uk1 on product_master (novator_id );

create index product_master_n2 on product_master (category );

create index product_master_n3 on product_master (second_choice_code );

create index program_reward_n1 on program_reward (reward_type );

create index program_reward_n2 on program_reward (bonus_calculation_basis );

create index program_reward_n3 on program_reward (calculation_basis );

create unique index shipping_key_details_uk1 on shipping_key_details (shipping_key_id , min_price );

create index shopping_index_src_n1 on shopping_index_src (parent_index_name , source_code );

create index shopping_index_src_prod_n1 on shopping_index_src_prod (product_id );

create index source_master_n1 on source_master (source_type );

create index source_master_n2 on source_master (partner_bank_id );

create index source_master_n5 on source_master (company_id );

create unique index source_program_ref_u1 on source_program_ref (source_code , start_date );

create index source_program_ref_n2 on source_program_ref (program_name );

create index upsell_detail_n1 on upsell_detail (upsell_master_id , upsell_detail_sequence );

create index upsell_master_n1 on upsell_master (upsell_status );

create index vendor_master_n1 on vendor_master (member_number , vendor_type , vendor_id );

create index vendor_master_n2 on vendor_master (company ) ;

create index vendor_product_n1 on vendor_product (product_subcode_id );

create index zip_code_n001 on zip_code (city , state_id );

use joe;

create index iotw_n1 on iotw (source_code );

create index iotw_n4 on iotw (iotw_source_code );

create index iotw_n2 on iotw (product_id );

create index iotw_n3 on iotw (upsell_master_id );

use pas;

create index pas_product_dt_n1 on pas_product_dt (delivery_date , codification_id );

create index pas_vendor_product_state_dt_n1 on pas_vendor_product_state_dt (delivery_date , state_code );

use ftd_apps;

create index delivery_date_range_n01 on delivery_date_range (active_flag);

create index florist_codifications_n1 on florist_codifications (codification_id);

create index florist_master_n02 on florist_master (status);

create index florist_master_n03 on florist_master (top_level_florist_id);

create index florist_master_n04 on florist_master (parent_florist_id);

create index florist_master_n07 on florist_master (zip_master_florist_id);

create index florist_master_new_n01 on florist_master (delivery_city, delivery_state);

create index florist_zips_new_n01 on florist_zips (florist_id, zip_code);

create unique index occasion_uk1 on occasion (default_occasion_indicator);

create index occasion_addon_n1 on occasion_addon (addon_id);

create unique index product_attr_restr_u1 on product_attr_restr (product_attr_restr_name, product_attr_restr_value);
