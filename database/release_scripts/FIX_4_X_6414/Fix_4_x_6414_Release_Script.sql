PROMPT examples of smart quotes ����  Search and remove
PROMPT look for code that is known to be wrong.  Look for "On Sale!".
PROMPT look for QA versus PROD sections.
PROMPT look for predeploy versus deploy-time sections.
set define off

####################################################################
# Defect 6414 - Allurent - Was removed from the 4.4 Release.
# Below are the release script items that were removed.
####################################################################



--------------------------------
-- Fix_4_x_6414_Release_Script.sql --
--------------------------------

--*************************************************
--*************************************************
--*************************************************
-- PRE-DEPLOY SECTION
--*************************************************
--*************************************************
--*************************************************

--
-- 6414
--
create table ftd_apps.shopping_index_src
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      parent_index_name      varchar2(1000),
      index_desc             varchar2(4000)   not null,
      country_id             varchar2(2),
      display_seq_num        number(5)        not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_pk primary key (index_name, source_code)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_fk1 foreign key (source_code)
                         references ftd_apps.source_master,
         constraint shopping_index_src_fk2 foreign key (parent_index_name, source_code)
                         references ftd_apps.shopping_index_src
     ) tablespace ftd_apps_data;
     
create index ftd_apps.shopping_index_src_n1 
    on ftd_apps.shopping_index_src(parent_index_name,source_code) tablespace ftd_apps_indx;


create table ftd_apps.shopping_index_src_stage
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      parent_index_name      varchar2(1000),
      index_desc             varchar2(4000)   not null,
      country_id             varchar2(2),
      display_seq_num        number(5)        not null,
      failure_reason_txt     varchar2(4000),
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_stg_pk primary key (index_name, source_code)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_stg_fk1 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;


create table ftd_apps.shopping_index_src_prod
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      product_id             varchar2(10)     not null,
      display_seq_num        number(5)        not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_prod_pk primary key (index_name, source_code, product_id)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_prod_fk1 foreign key (source_code)
                         references ftd_apps.source_master
--         constraint shopping_index_src_prod_fk2 foreign key (product_id)
--                         references ftd_apps.product_master
     ) tablespace ftd_apps_data;
     
create index ftd_apps.shopping_index_src_prod_n1 
    on ftd_apps.shopping_index_src_prod(product_id) tablespace ftd_apps_indx;


create table ftd_apps.shopping_index_src_prod_stage
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      product_id             varchar2(10)     not null,
      display_seq_num        number(5)        not null,
      failure_reason_txt     varchar2(4000),
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shop_index_src_prod_stage_pk primary key (index_name, source_code, product_id)
                         using index tablespace ftd_apps_indx,
         constraint shop_inde_src_prod_stage_fk1 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;



create table ftd_apps.product_source
     (product_id             varchar2(10)     not null,
      source_Code            varchar2(10)     not null,
      last_modified_date     date             not null,
         constraint product_source_pk primary key (product_id, source_code)
                         using index tablespace ftd_apps_indx,
         constraint product_source_fk1 foreign key (product_id)
                         references ftd_apps.product_master,
         constraint product_source_fk2 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;


create table ftd_apps.upsell_source
     (upsell_master_id       varchar2(4)      not null,
      source_Code            varchar2(10)     not null,
      last_modified_date     date             not null,
         constraint upsell_source_pk primary key (upsell_master_id, source_Code)
                         using index tablespace ftd_apps_indx,
         constraint upsell_source_fk1 foreign key (upsell_master_id)
                         references ftd_apps.upsell_master,
         constraint upsell_source_fk2 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;


create table ftd_apps.product_attr_restr
     (product_attr_restr_id        number(11)      not null,
      product_attr_restr_name      varchar2(30)    not null,
      product_attr_restr_oper      varchar2(30)    not null,
      product_attr_restr_value     varchar2(100),
      product_attr_restr_desc      varchar2(100)   not null,
      java_method_name             varchar2(50)    not null,
      created_on                   date            not null,
      created_by                   varchar2(100)   not null,
      updated_on                   date            not null,
      updated_by                   varchar2(100)   not null,
         constraint product_attr_restr_pk primary key (product_attr_restr_id)
                         using index tablespace ftd_apps_indx
     ) tablespace ftd_apps_data;

create sequence ftd_apps.product_attr_restr_id_sq start with 1 increment by 1 maxvalue 99999999999 cache 20;

create unique index ftd_apps.product_attr_restr_u1 
     on ftd_apps.product_attr_restr(product_attr_restr_name,product_attr_restr_value) 
     tablespace ftd_apps_indx;

create table ftd_apps.product_attr_restr_source_excl
     (source_code                  varchar2(10)    not null,
      product_attr_restr_id        number(11)      not null,
      created_on                   date            not null,
      created_by                   varchar2(100)   not null,
      updated_on                   date            not null,
      updated_by                   varchar2(100)   not null,
         constraint product_attr_rstr_src_excl_pk primary key (source_code,product_attr_restr_id)
                         using index tablespace ftd_apps_indx,
         constraint product_attr_rstr_src_excl_fk1 foreign key (source_code)
                         references ftd_apps.source_master,
         constraint product_attr_rstr_src_excl_fk2 foreign key (product_attr_restr_id)
                         references ftd_apps.product_attr_restr
     ) tablespace ftd_apps_data;


create table ftd_apps.product_attr_rstr_src_excl$
     (source_code                  varchar2(10)    not null,
      product_attr_restr_id        number(11)      not null,
      created_on                   date            not null,
      created_by                   varchar2(100)   not null,
      OPERATION$                   VARCHAR2(7)     not null,
      TIMESTAMP$                   timestamp       not null
     );

create table ftd_apps.ORDER_SERVICE_CLIENT
     (client_id              varchar2(10)     not null,
      client_name            varchar2(50)     not null,
      active_Flag            varchar2(1) default 'Y' not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint order_service_client_pk primary key (client_id)
                         using index tablespace ftd_apps_indx,
         constraint order_service_client_ck1 check (active_Flag in ('Y','N'))
     ) tablespace ftd_apps_data;

create table ftd_apps.ORDER_SERVICE_xsl
     (xsl_code               varchar2(30)     not null,
      xsl_desc               varchar2(100)    not null,
      xsl_txt                CLOB             not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint order_service_xsl_pk primary key (xsl_code)
                         using index tablespace ftd_apps_indx
     ) tablespace ftd_apps_data
       LOB (xsl_txt) STORE AS order_service_xsl_lob (TABLESPACE ftd_apps_data ENABLE STORAGE IN ROW CACHE READS LOGGING);

create table ftd_apps.ORDER_SERVICE_xsl$
     (xsl_code               varchar2(30)     not null,
      xsl_desc               varchar2(100),
      xsl_txt                CLOB         ,
      created_on             date         ,
      created_by             varchar2(100),
      updated_on             date         ,
      updated_by             varchar2(100),
      OPERATION$             VARCHAR2(7)      NOT NULL,
      TIMESTAMP$             TIMESTAMP(6)     NOT NULL
     ) tablespace ftd_apps_data
       LOB (xsl_txt) STORE AS order_service_xsl$_lob 
                     (TABLESPACE ftd_apps_data ENABLE STORAGE IN ROW CACHE READS LOGGING);

create table ftd_apps.ORDER_SERVICE_CLIENT_xsl
     (client_id              varchar2(10)     not null,
      action_type            varchar2(40)     not null,
      xsl_code               varchar2(30)     not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint order_service_client_xsl_pk primary key (client_id, action_type)
                         using index tablespace ftd_apps_indx,
         constraint order_service_client_fk1 foreign key (client_id)
                         references ftd_apps.order_service_Client,
         constraint order_service_client_fk2 foreign key (xsl_code)
                         references ftd_apps.order_service_xsl
     ) tablespace ftd_apps_data;

create table pas.pas_florist_zipcode_dt
     (zip_code_id            varchar2(10)     not null,
      delivery_date          date             not null,
      cutoff_time            varchar2(4)      not null,
      status_code            varchar2(1)      default 'Y' not null,
         constraint pas_florist_zipcode_dt_pk primary key (zip_code_id, delivery_date)
                         using index tablespace pas_indx
     ) tablespace pas_data;


alter table ftd_apps.shopping_index_src modify (index_name   varchar2(300));
alter table ftd_apps.shopping_index_src_prod modify (index_name   varchar2(300));
alter table ftd_apps.shopping_index_src_prod_stage modify (index_name   varchar2(300));
alter table ftd_apps.shopping_index_src_stage modify (index_name   varchar2(300));

alter table ftd_apps.shopping_index_src modify (parent_index_name   varchar2(300));
alter table ftd_apps.shopping_index_src modify (index_desc          varchar2(100));
alter table ftd_apps.shopping_index_src_stage modify (parent_index_name     varchar2(300));
alter table ftd_apps.shopping_index_src_stage modify (index_desc          varchar2(100));

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (1,'DELIVERY_TYPE','=','I','International', sysdate, 'Defect_6414','getDeliveryType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (2,'PRODUCT_TYPE','=','FLORAL','Floral', sysdate, 'Defect_6414','getProductType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (3,'PRODUCT_TYPE','=','FRECUT','Fresh Cut', sysdate, 'Defect_6414','getProductType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (4,'PRODUCT_TYPE','=','SPEGFT','Specialty Gift', sysdate, 'Defect_6414','getProductType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (5,'PRODUCT_TYPE','=','SDG','Same Day Gift', sysdate, 'Defect_6414','getProductType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (6,'PRODUCT_TYPE','=','SDFC','Same Day Fresh Cut', sysdate, 'Defect_6414','getProductType',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (7,'OVER_21','=','Y','Over 21', sysdate, 'Defect_6414','getOver21Flag',sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (8,'PERSONALIZATION_TEMPLATE_ID','IS NOT NULL', null,'Personalization', sysdate, 'Defect_6414','getPersonalizationId',
sysdate,'Defect_6414');

insert into ftd_apps.product_attr_restr
(product_attr_restr_id,product_attr_restr_name,product_attr_restr_oper,product_attr_restr_value,product_attr_restr_desc,
created_on,created_by,java_method_name,updated_on,updated_by)
values (9,'PRODUCT_SUB_TYPE','=', 'EXOTIC' ,'Exotic', sysdate, 'Defect_6414','getProductSubType',sysdate,'Defect_6414');

grant select on ftd_apps.product_attr_restr to scrub;
grant select on ftd_apps.product_attr_restr_source_excl to scrub;

create table frp.cache_statistics
     (NODE_NAME                   VARCHAR2(100)  NOT NULL,
      APP_NAME                    VARCHAR2(200)  NOT NULL,
      CACHE_NAME                  VARCHAR2(50)   NOT NULL,
      CACHE_TYPE                  VARCHAR2(20)   NOT NULL,
      PRELOAD_FLAG                VARCHAR2(1),
      ACCESS_COUNT                NUMBER,
      REFRESH_COUNT               NUMBER,
      REFRESH_RATE                NUMBER,
      EXPIRE_RATE                 NUMBER,
      EXPIRE_CHECK_RATE           NUMBER,
      FIRST_LOAD_TIME             DATE,
      LAST_REFRESH_TIME           DATE,
      UPDATED_ON                  DATE NOT NULL
     ) tablespace frp_data;

alter table frp.cache_statistics add constraint cache_statistics_pk primary key (node_name, app_name, cache_name)
using index tablespace frp_indx;

comment on table frp.cache_statistics is 
'Stores statistics for Apollo application caches';

comment on column frp.cache_statistics.node_name is 'Originating node for this cache';
comment on column frp.cache_statistics.app_name is 'Originating application for this cache';
comment on column frp.cache_statistics.cache_type is 'Fixed or Transient';
comment on column frp.cache_statistics.preload_flag is 'Y = Cache was preloaded';
comment on column frp.cache_statistics.refresh_rate is 'Refresh interval in minutes (Fixed only)';
comment on column frp.cache_statistics.expire_rate is 'Cache element purge interval in minutes (Transient only)';
comment on column frp.cache_statistics.expire_check_rate is 'Expiration check timer interval in minutes (Transient only)';
comment on column frp.cache_statistics.last_refresh_time is 'Most recent cache refresh time (Fixed only)';
comment on column frp.cache_statistics.refresh_count is 'Number of cache refreshes (Fixed only)';
comment on column frp.cache_statistics.first_load_time is 'Time when cache was first loaded';
comment on column frp.cache_statistics.access_count is 'Number of cache accesses';

-- content messages for product attributes
insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'PRODUCT_ATTRIBUTE_ID', 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PRODUCT_ATTRIBUTE_EXCLUSION', 'EXCLUSION_ERROR_MESSAGE',
'The message to be displayed if a Product Attribute Exclusion is discovered.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PRODUCT_ATTRIBUTE_ID'),
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'1', NULL, 'International products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'2', NULL, 'Floral products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'3', NULL, 'Fresh-cut products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'4', NULL, 'Specialty Gift products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'5', NULL, 'Same-day gifts are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'6', NULL, 'Same-day fresh-cut products are not offered for this source code. Please choose another product.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'7', NULL, 'Over-21 products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'8', NULL, 'Personalization products are not offered for this source code. Please choose another product.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'9', NULL, 'Exotic products are not offered for this source code. Please choose another product.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);


-- content messages for errors:
insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_MISSING_CLIENT_ID',
'The message to be displayed if a request is received without a client id.', null, null, 'Defect_6414', sysdate, 'Defect_6414',
sysdate);    

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_CLIENT_ID'),
NULL, NULL, 'Invalid request due to missing client id. Please provide a valid client id.', 'Defect_6414', sysdate, 'Defect_6414',
sysdate);
 
insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_CLIENT_ID',
'The message to be displayed if a request is received without a valid client id.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_CLIENT_ID'),
NULL, NULL, 'Invalid request due to invalid client id. Please provide a valid client id.', 'Defect_6414', sysdate, 'Defect_6414',
sysdate);


insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'ORDER_SERVICE_CLIENT_ID', 'Defect_6414', sysdate, 'Defect_6414', sysdate);   

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_MISSING_SOURCE_CODE',
'The message to be displayed if a request is received without a source code.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_SOURCE_CODE'),
NULL, NULL, 'Invalid request due to missing source code. Please provide a valid source code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_SOURCE_CODE'),
'Allurent', NULL, 'Invalid request due to missing source code. Please provide a valid source code.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_SOURCE_CODE'),
'FTD', NULL, 'FTD: Invalid request due to missing source code. Please provide a valid source code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY,
UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_SOURCE_CODE',
'The message to be displayed if a request is received without a valid source code.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_SOURCE_CODE'),
NULL, NULL, 'Invalid request due to invalid source code. Please provide a valid source code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY,
CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_SOURCE_CODE'),
'Allurent', NULL, 'Invalid request due to invalid source code. Please provide a valid source code.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_SOURCE_CODE'),
'FTD', NULL, 'FTD: Invalid request due to invalid source code. Please provide a valid source code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_MISSING_PRODUCT_ID',
'The message to be displayed if a request is received without a valid product id.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRODUCT_ID'),
NULL, NULL, 'Invalid request due to missing product id. Please provide valid product ids.', 'Defect_6414', sysdate, 'Defect_6414',
sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRODUCT_ID'),
'Allurent', NULL, 'Invalid request due to missing product id. Please provide valid product ids.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRODUCT_ID'),
'FTD', NULL, 'FTD: Invalid request due to missing product id. Please provide valid product ids.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_TOO_MANY_PRODUCT_ID',
'The message to be displayed if a request contains too many product ids.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_TOO_MANY_PRODUCT_ID'),
'FTD', NULL, 'FTD: Too many product ids are requested.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_TOO_MANY_PRODUCT_ID'),
'Allurent', NULL, 'Too many product ids are requested.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_TOO_MANY_PRODUCT_ID'),
NULL, NULL, 'Too many product ids are requested.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_CATEGORY_ID',
'The message to be displayed if a request is received without a valid category id.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_CATEGORY_ID'),
NULL, NULL, 'Invalid request due to invalid category id. Please provide a valid category id.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_CATEGORY_ID'),
'Allurent', NULL, 'Invalid request due to invalid category id. Please provide a valid category id.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_CATEGORY_ID'),
'FTD', NULL, 'FTD: Invalid request due to invalid category id. Please provide a valid category id.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_PRODUCT_ID',
'The message to be displayed if a request is received without a valid product id.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ID'),
NULL, NULL, 'Invalid request due to invalid product id. Please provide a valid product id.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ID'),
'Allurent', NULL, 'Invalid request due to invalid product id. Please provide a valid product id.', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ID'),
'FTD', NULL, 'FTD: Invalid request due to invalid product id. Please provide a valid product id.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY,
UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_MISSING_ZIP_CODE',
'The message to be displayed if a request does not include zip code.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_ZIP_CODE'),
'FTD', NULL, 'FTD: Missing zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_ZIP_CODE'),
'Allurent', NULL,
'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.',
'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_ZIP_CODE'),
NULL, NULL, 'Missing zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_ZIP_CODE',
'The message to be displayed if a request does not include a valid zip code.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_ZIP_CODE'),
'FTD', NULL, 'FTD: Invalid zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_ZIP_CODE'),
'Allurent', NULL,
'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.',
'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_ZIP_CODE'),
NULL, NULL, 'Invalid zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_COUNTRY_ID',
'The message to be displayed if a request does not include a valid country.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_COUNTRY_ID'),
'FTD', NULL, 'FTD: Invalid country id.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_COUNTRY_ID'),
'Allurent', NULL, 'Please <a href="http://www.ftd.com/~sourceCode~/catalog/international.epl">click here</a> for products available for International deliveries.',
'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_COUNTRY_ID'),
NULL, NULL, 'Invalid country id.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT',
'The message to be displayed if a request does not include a valid delivery date.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT'),
'FTD', NULL, 'FTD: Invalid delivery date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT'),
'Allurent', NULL, 'Invalid delivery date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT'),
NULL, NULL, 'Invalid delivery date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_DELIVERY_DATE_PASSED',
'The message to be displayed if a request does not include a delivery date in the future.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_DELIVERY_DATE_PASSED'),
'FTD', NULL, 'FTD: Delivery date has passed.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_DELIVERY_DATE_PASSED'),
'Allurent', NULL, 'Delivery date has passed.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_DELIVERY_DATE_PASSED'),
NULL, NULL, 'Delivery date has passed.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY',
'The message to be displayed if a request does not specify a valid product order by value.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY'),
'FTD', NULL, 'FTD: Product order by parameter is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY'),
'Allurent', NULL, 'Product order by parameter is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY'),
NULL, NULL, 'Product order by parameter is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_MISSING_PRICE',
'The message to be displayed if a request does not specify a price value.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRICE'),
'FTD', NULL, 'FTD: Product price is required.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRICE'),
'Allurent', NULL, 'Product price is required.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_MISSING_PRICE'),
NULL, NULL, 'Product price is required.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_PRICE',
'The message to be displayed if a request does not specify a valid price value.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRICE'),
'FTD', NULL, 'FTD: Product price is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRICE'),
'Allurent', NULL, 'Product price is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_PRICE'),
NULL, NULL, 'Product price is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


-----------
insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_LOAD_LEVEL',
'The message to be displayed if a request does not specify a valid load level.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_LOAD_LEVEL'),
'FTD', NULL, 'FTD: Load level is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_LOAD_LEVEL'),
'Allurent', NULL, 'Load Level is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_LOAD_LEVEL'),
NULL, NULL, 'Load Level is invalid.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);
---------




insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST',
'The message to be displayed when the service cannot process the request.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST'),
'FTD', NULL, 'FTD: Cannot process request.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST'),
'Allurent', NULL,
'We''re sorry we couldn''t find that page, please <a href="http://www.ftd.com/~sourceCode~/best-sellers-ctg/product-flowers-bestsellers">click here</a> to shop our best sellers',
'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST'),
NULL, NULL, 'Cannot process request.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);

-- content for image URL
insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'GOOD_BETTER_BEST', 'Defect_6414', sysdate, 'Defect_6414', sysdate);   

insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'IMAGE_TYPE', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


-- content for image URL
insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'GOOD_BETTER_BEST', 'Defect_6414', sysdate, 'Defect_6414', sysdate);   

insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'IMAGE_TYPE', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_IMAGE_URL', 'IMAGE_CATEGORY',
'This control will allow for the 200x225 category image path to be changed for the web service.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_CATEGORY'),
NULL, NULL, 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/~REPLACE_TOKEN_1~_200x225.jpg', 'Defect_6414', sysdate,
'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_IMAGE_URL', 'IMAGE_CATEGORY_INTL',
'This control will allow for the 120x120 category image path to be changed for the web service.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_CATEGORY_INTL'),
NULL, NULL, 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/~REPLACE_TOKEN_1~_120x120.jpg', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_IMAGE_URL', 'IMAGE_GBB',
'This control will allow for the GBB image path to be changed for the web service.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'GOOD', 'CATEGORY',
'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/gbb_popover_~REPLACE_TOKEN_1~.png',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BETTER', 'CATEGORY',
'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/gbb_popover_~REPLACE_TOKEN_1~_deluxe.png',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BEST', 'CATEGORY',
'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/gbb_popover_~REPLACE_TOKEN_1~_premium.png',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'GOOD', 'PRODUCT', 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/~REPLACE_TOKEN_1~_330x370.jpg', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BETTER', 'PRODUCT', 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/~REPLACE_TOKEN_1~_330x370_deluxe.jpg',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BEST', 'PRODUCT', 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/~REPLACE_TOKEN_1~_330x370_premium.jpg',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'GOOD', 'THUMBNAIL', 'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/~REPLACE_TOKEN_1~_99x99.jpg',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BETTER', 'THUMBNAIL',
'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/~REPLACE_TOKEN_1~_99x99_deluxe.jpg',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_GBB'),
'BEST', 'THUMBNAIL',
'http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/design2007/gbb/products/~REPLACE_TOKEN_1~_99x99_premium.jpg',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_IMAGE_URL', 'IMAGE_ADDON',
'This control will allow for the Add On image path to be changed for the web service.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_ADDON'),
NULL, NULL, 'http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/add-ons~REPLACE_TOKEN_1~_99x99.jpg', 'Defect_6414',
sysdate, 'Defect_6414', sysdate);


--IOTW messaging
insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CONTEXT', 'IOTW_messaging_API', 'IOTW product page messaging.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PRODUCT_ATTRIBUTE_ID'),
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate);     

------------------------- THIS VALUE NEEDS REPLACEMENT------------------------
--------------------------- per Christy Hu, 2010/9/1 ------------------------
insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CONTEXT' AND CONTENT_NAME = 'IOTW_messaging_API'),
NULL, NULL, 'On Sale!', 'Defect_6414', sysdate, 'Defect_6414', sysdate);
------------------------- THIS VALUE NEEDS REPLACEMENT------------------------


-- service clients
insert into ftd_apps.order_service_client (CLIENT_ID, CLIENT_NAME, ACTIVE_FLAG, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
values ('Allurent','Allurent','Y',sysdate,'Defect_6414',sysdate,'Defect_6414');

insert into ftd_apps.order_service_client (CLIENT_ID, CLIENT_NAME, ACTIVE_FLAG, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
values ('FTD','FTD Test','Y',sysdate,'Defect_6414',sysdate,'Defect_6414');

-- global parm for occasion name
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GET_OCCASION_INDEX_NAME','occasion','Defect_6414',sysdate,'Defect_6414',sysdate,
'Index name for occasion.');

-- global parm for max number of products requested.
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GET_PRODUCT_MAX_COUNT','10','Defect_6414',sysdate,'Defect_6414',sysdate,
'Max number of products allowed in getProduct request.');

-- global parm for nonsearch index
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','EXCLUDED_INDEX','nonsearchitems,exclude_products','Defect_6414',sysdate,'Defect_6414',sysdate,
'Excluded indexes for search.');

-- global parm for addon
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','RETURN_ALL_ADDON_FLAG','N','Defect_6414',sysdate,'Defect_6414',sysdate, 'Return all addon flag.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','INCLUDE_FUNERAL_BANNER_FLAG','N','Defect_6414',sysdate,'Defect_6414',sysdate,
'Include funeral banner flag.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GENERIC_OCCASION_DISP_NAME','Occasion','Defect_6414',sysdate,'Defect_6414',sysdate,
'Occasion description to be displayed with generic card.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','DISPLAY_SHIP_FEE','Y','Defect_6414',sysdate,'Defect_6414',sysdate,
'Global flag indicating if shipping fee will be displayed to client.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','DISPLAY_SERVICE_FEE','N','Defect_6414',sysdate,'Defect_6414',sysdate,
'Global flag indicating if service fee will be displayed to client.');

insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'ADDON_ID', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CONTEXT', 'ADDON_OCCASION_DISPLAY_NAME',
'This control defines the display name for the addon occasion.', null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CONTEXT' AND CONTENT_NAME = 'ADDON_OCCASION_DISPLAY_NAME'),
'RC999', NULL, 'Occasion', 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CONTEXT', 'DATE_UNAVAILABLE',
'This is the message that is displayed when the date requested is no longer available.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CONTEXT' AND CONTENT_NAME = 'DATE_UNAVAILABLE'),
NULL, NULL, 'The delivery date ~REPLACE_TOKEN_1~ is no longer available, please select another date.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PRODUCT_ATTRIBUTE_EXCLUSION' AND CONTENT_NAME = 'EXCLUSION_ERROR_MESSAGE'),
'-1', NULL, 'Product not available for this source code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID,
    CONTENT_CONTEXT,
    CONTENT_NAME,
    CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1,
    CONTENT_FILTER_ID2,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_master_sq.nextval,
    'ORDER_SERVICE_CLIENT_ERROR',
    'VALIDATION_ERROR_PRODUCT_UNAVAILABLE', 
    'The message to be displayed if the selected product is unavailable.',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'),
    null,
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_PRODUCT_UNAVAILABLE'),
    null,
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_PRODUCT_UNAVAILABLE'),
    'FTD',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_PRODUCT_UNAVAILABLE'),
    'Allurent',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID,
    CONTENT_CONTEXT,
    CONTENT_NAME,
    CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1,
    CONTENT_FILTER_ID2,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_master_sq.nextval,
    'ORDER_SERVICE_CLIENT_ERROR',
    'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US', 
    'The message to be displayed when no delivery dates are available for a floral product delivered in the U.S.',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'),
    null,
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US'),
    null,
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. <BR>If the zip code you have entered is correct please shop our <a href="http://www.ftd.com/~sourceCode~/catalog/category.epl?index_id=specialtygifts/">Specialty Gift Collection</a> that is available for delivery to the zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US'),
    'FTD',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. <BR>If the zip code you have entered is correct please shop our <a href="http://www.ftd.com/~sourceCode~/catalog/category.epl?index_id=specialtygifts/">Specialty Gift Collection</a> that is available for delivery to the zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US'),
    'Allurent',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD florist. <BR>If the zip code you have entered is correct please shop our <a href="http://www.ftd.com/~sourceCode~/catalog/category.epl?index_id=specialtygifts/">Specialty Gift Collection</a> that is available for delivery to the zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     



insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID,
    CONTENT_CONTEXT,
    CONTENT_NAME,
    CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1,
    CONTENT_FILTER_ID2,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_master_sq.nextval,
    'ORDER_SERVICE_CLIENT_ERROR',
    'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA', 
    'The message to be displayed when no delivery dates are available for a floral product delivered in Canada.',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'),
    null,
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA'),
    null,
    null,
    'The recipient''s postal code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct postal code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA'),
    'FTD',
    null,
    'The recipient''s postal code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct postal code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA'),
    'Allurent',
    null,
    'The recipient''s postal code you''ve entered is not presently serviced by an FTD florist. Please verify that you have entered the correct postal code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     



insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID,
    CONTENT_CONTEXT,
    CONTENT_NAME,
    CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1,
    CONTENT_FILTER_ID2,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_master_sq.nextval,
    'ORDER_SERVICE_CLIENT_ERROR',
    'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT', 
    'The message to be displayed when no delivery dates are available for a floral product delivered international',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'),
    null,
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT'),
    null,
    null,
    'The product you selected is only available in the U.S.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
   ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT'),
    'FTD',
    null,
    'The product you selected is only available in the U.S.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT'),
    'Allurent',
    null,
    'We''re sorry the product you selected is only available in the U.S. Please <a href="http://www.ftd.com/~sourceCode~/catalog/international.epl>click here</a> for products available for international deliveries.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     


insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID,
    CONTENT_CONTEXT,
    CONTENT_NAME,
    CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1,
    CONTENT_FILTER_ID2,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_master_sq.nextval,
    'ORDER_SERVICE_CLIENT_ERROR',
    'VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES',
    'The message to be displayed when no delivery dates are available for a drop-ship product.',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'),
    null,
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES'),
    null,
    null,
    'The product you''ve selected is not available for delivery in this zip / postal code.  Please <a href="http://www.ftd.com/~sourceCode~/same-day-delivery-error-ctg/samedayzip_error/">click here</a> for products available in this zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES'),
    'FTD',
    null,
    'The product you''ve selected is not available for delivery in this zip / postal code.  Please <a href="http://www.ftd.com/~sourceCode~/same-day-delivery-error-ctg/samedayzip_error/">click here</a> for products available in this zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES'),
    'Allurent',
    null,
    'The product you''ve selected is not available for delivery in this zip / postal code.  Please <a href="http://www.ftd.com/~sourceCode~/same-day-delivery-error-ctg/samedayzip_error/">click here</a> for products available in this zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     
    
update FTD_APPS.CONTENT_DETAIL 
set CONTENT_TXT = 'The delivery date ~deliveryDate~ is no longer available, please select another date.'
where content_master_id = (select content_master_id from ftd_apps.content_master 
                           where content_context = 'ORDER_SERVICE_CONTEXT' 
                           AND content_name = 'DATE_UNAVAILABLE');
                           
update ftd_apps.content_detail 
set content_txt='The delivery date ~deliveryDate~ is no longer available. Please select another date.'
where content_master_id = (select content_master_id FROM ftd_apps.content_master 
      WHERE content_context = 'ORDER_SERVICE_CLIENT_ERROR' AND content_name = 'VALIDATION_ERROR_DELIVERY_DATE_PASSED');

-- Late updates for 7377 from mgiovenco
insert into FTD_APPS.CONTENT_FILTER (
	CONTENT_FILTER_ID,
	CONTENT_FILTER_NAME,
	UPDATED_BY,
	UPDATED_ON,
	CREATED_BY,
	CREATED_ON)
values (
	ftd_apps.content_filter_sq.nextval,
	'SCRIPT_ID',
	'Defect_7377',
	sysdate,
	'Defect_7377',
	sysdate);
	
insert into FTD_APPS.CONTENT_MASTER (
	CONTENT_MASTER_ID,
	CONTENT_CONTEXT,
	CONTENT_NAME,
	CONTENT_DESCRIPTION,
	CONTENT_FILTER_ID1,
	CONTENT_FILTER_ID2,
	UPDATED_BY,
	UPDATED_ON,
	CREATED_BY,
	CREATED_ON)
values (
	ftd_apps.content_master_sq.nextval,
	'PREFERRED_PARTNER',
	'JOE_SCRIPT_TOKEN',
	'Data to use to replace script tokens for Preferred Partners',
	(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'),
	(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'SCRIPT_ID'),
	'Defect_7377',
	sysdate,
	'Defect_7377',
	sysdate);
	
insert into FTD_APPS.CONTENT_DETAIL (
	CONTENT_DETAIL_ID,
	CONTENT_MASTER_ID,
	FILTER_1_VALUE,
	FILTER_2_VALUE,
	CONTENT_TXT,
	UPDATED_BY,
	UPDATED_ON,
	CREATED_BY,
	CREATED_ON)
values (
	ftd_apps.content_detail_sq.nextval,
	(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'JOE_SCRIPT_TOKEN'),
	'USAA',
	'COMPANY',
	'FTD',
	'Defect_7377',
	sysdate,
	'Defect_7377',
	sysdate);
	
insert into FTD_APPS.CONTENT_DETAIL (
	CONTENT_DETAIL_ID,
	CONTENT_MASTER_ID,
	FILTER_1_VALUE,
	FILTER_2_VALUE,
	CONTENT_TXT,
	UPDATED_BY,
	UPDATED_ON,
	CREATED_BY,
	CREATED_ON)
values (
	ftd_apps.content_detail_sq.nextval,
	(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'JOE_SCRIPT_TOKEN'),
	'USAA',
	'PreferredPartnerExtra',
	' where USAA members always receive 25% off',
	'Defect_7377',
	sysdate,
	'Defect_7377',
	sysdate);

--*************************************************
--*************************************************
--*************************************************
-- THIS IS A QA-ONLY SECTION
--*************************************************
--*************************************************
--*************************************************

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_ftp_server','sodium.ftdi.com','Defect_6414',sysdate,'Defect_6414',sysdate,
'FTP server for shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_working_directory','/u02/apollo/pdb/sif','Defect_6414',sysdate,'Defect_6414',sysdate,
'Local directory to save shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_remote_directory','sif','Defect_6414',sysdate,'Defect_6414',sysdate,
'Remote directory to get shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_archive_directory','/u02/apollo/pdb/sif/archive','Defect_6414',sysdate,'Defect_6414',sysdate,
'Local archive directory keep processed shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('OPS_ADMIN_CONFIG','order_service_simulator_url','http://steel1.ftdi.com:8080/orderservice/menu.do','Defect_6414',sysdate,
'Defect_6414',sysdate, 'URL that links to the order service simulator on JBoss.');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('product_database','sif_ftp_login',global.encryption.encrypt_it('devftp','APOLLO_TEST_2010'),
'Shopping index FTP username',sysdate,'Defect_6414', sysdate,'Defect_6414','APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('product_database','sif_ftp_password',global.encryption.encrypt_it('lli7tst','APOLLO_TEST_2010'),
'Shopping index FTP password',sysdate,'Defect_6414', sysdate,'Defect_6414','APOLLO_TEST_2010');

--*************************************************
--*************************************************
--*************************************************
-- END QA-ONLY SECTION, BEGIN PRODUCTION SECTION
--*************************************************
--*************************************************
--*************************************************

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_ftp_server','hawk.ftdi.com','Defect_6414',sysdate,'Defect_6414',sysdate,
'FTP server for shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_working_directory','/u02/apollo/pdb/sif','Defect_6414',sysdate,'Defect_6414',sysdate,
'Local directory to save shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_remote_directory','sif','Defect_6414',sysdate,'Defect_6414',sysdate,
'Remote directory to get shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','sif_archive_directory','/u02/apollo/pdb/sif/archive','Defect_6414',sysdate,'Defect_6414',sysdate,
'Local archive directory keep processed shopping index files.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('OPS_ADMIN_CONFIG','order_service_simulator_url','http://steel1.ftdi.com:8080/orderservice/menu.do','Defect_6414',sysdate,
'Defect_6414',sysdate, 'URL that links to the order service simulator on JBoss.');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('product_database','sif_ftp_login',global.encryption.encrypt_it('changeme','APOLLO_PROD_2010'),
'Shopping index FTP username',sysdate,'Defect_6414', sysdate,'Defect_6414','APOLLO_PROD_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('product_database','sif_ftp_password',global.encryption.encrypt_it('changeme','APOLLO_TEST_2010'),
'Shopping index FTP password',sysdate,'Defect_6414', sysdate,'Defect_6414','APOLLO_TEST_2010');

--*************************************************
--*************************************************
--*************************************************
-- END PRODUCTION-ONLY SECTION
--*************************************************
--*************************************************
--*************************************************

--*************************************************
--*************************************************
--*************************************************
-- END PRE-DEPLOY SECTION, --- START OF DEPLOY ---
--*************************************************
--*************************************************
--*************************************************

alter table ftd_apps.source_master add
    (DISPLAY_SERVICE_FEE_CODE      VARCHAR2(1) DEFAULT 'D' not null,
     DISPLAY_SHIPPING_FEE_CODE     VARCHAR2(1) DEFAULT 'D' not null,
          constraint source_master_ck11   check (DISPLAY_SERVICE_FEE_CODE in ('Y','N','D')),
          constraint source_master_ck12   check (DISPLAY_SHIPPING_FEE_CODE in ('Y','N','D'))
    );

comment on column ftd_apps.source_master.display_service_fee_code is 
'Y = display service fee, N = do not display service fee, D = use default value from global parm ORDER_SERVICE_CONFIG DISPLAY_SERVICE_FEE';

comment on column ftd_apps.source_master.display_shipping_fee_code is 
'Y = display shipping fee, N = do not display shipping fee, D = use default value from global parm ORDER_SERVICE_CONFIG DISPLAY_SHIP_FEE';

alter table ftd_apps.source_master$ add
    (DISPLAY_SERVICE_FEE_CODE      VARCHAR2(1),
     DISPLAY_SHIPPING_FEE_CODE     VARCHAR2(1) 
    );


alter table ftd_apps.company_master add
     (DEFAULT_SOURCE_CODE         VARCHAR2(10) DEFAULT '9999'  NOT NULL,
         constraint company_master_fk2 foreign key (default_source_code)
                         references ftd_apps.source_master
     );
  
update ftd_apps.company_master set default_source_code='350';
update ftd_apps.company_master set default_source_code='10171' where company_id = 'FLORIST';

delete from ftd_apps.occasion_addon where addon_id='RC999' and occasion_id <> 13;

grant execute on clean.csr_viewed_locked_pkg to ftd_apps;


-- convert existing data.  NOTE - Do these before dropping the columns from source_master!!
insert into ftd_apps.product_attr_restr_source_excl(source_code,product_attr_restr_id, created_on,created_by,updated_on,updated_by)
    select source_code, 5, sysdate, 'SYS', sysdate, 'SYS'
        from ftd_apps.source_master
        where same_day_gift_flag='N';
 
insert into ftd_apps.product_attr_restr_source_excl(source_code,product_attr_restr_id, created_on,created_by,updated_on,updated_by)
    select source_code, 6, sysdate, 'SYS', sysdate, 'SYS'
        from ftd_apps.source_master
        where same_day_freshcut_flag='N';


ALTER TABLE FTD_APPS.SOURCE_MASTER DROP (over_21_flag, same_day_gift_flag, same_day_freshcut_flag);
ALTER TABLE FTD_APPS.SOURCE_MASTER$ DROP (over_21_flag, same_day_gift_flag, same_day_freshcut_flag);


alter table ftd_apps.personalization_templates add
     (regexp_text            varchar2(200));


grant select on frp.global_parms to pas;


--
-- The following view has been created and is being unit tested for functionality by T. Schmig.
-- Once functionally correct, we should probably confirm it has acceptable performance.
--
CREATE OR REPLACE FORCE VIEW PAS.PAS_FLORIST_PRODUCT_ZIP_DT_VW (PRODUCT_ID, DELIVERY_DATE, ZIP_CODE, CODIFICATION_ID,
CUTOFF_TIME, ADDON_DAYS_QTY) AS 
SELECT pp.product_id, pz.delivery_date, pz.zip_code_id, pp.codification_id,
          (pz.cutoff_time + pt.delta_to_cst_hhmm) AS cutoff_time, pp.addon_days_qty
    FROM pas.pas_florist_zipcode_dt pz, pas.pas_product_dt pp, ftd_apps.zip_code zc, ftd_apps.state_master sm,
          pas.pas_timezone_dt pt, pas.pas_country_dt pc
    WHERE 1 = 1
          AND pt.time_zone_code = sm.TIME_ZONE
          AND pt.delivery_date = pz.delivery_date
          AND sm.state_master_id = zc.state_id
          AND zc.zip_code_id = pz.zip_code_id
          AND pt.delivery_date = pp.delivery_date
          AND pc.delivery_date = pz.delivery_date
          AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
          AND pc.delivery_available_flag = 'Y'
          AND pp.delivery_available_flag = 'Y'
          AND pz.status_code = 'Y'
          AND pp.product_type IN ('FLORAL', 'SDFC', 'SDG')
          AND (pp.codification_id is null OR pp.codification_id = '')
  UNION
    SELECT pp.product_id, pz.delivery_date, pz.zip_code_id, pp.codification_id,
          (pz.cutoff_time + pt.delta_to_cst_hhmm) AS cutoff_time, pp.addon_days_qty
    FROM pas.pas_florist_zipcode_dt pz, pas.pas_product_dt pp, ftd_apps.zip_code zc, ftd_apps.state_master sm,
          pas.pas_timezone_dt pt, pas.pas_country_dt pc
    WHERE 1 = 1
          AND pt.time_zone_code = sm.TIME_ZONE
          AND pt.delivery_date = pz.delivery_date
          AND sm.state_master_id = zc.state_id
          AND zc.zip_code_id = pz.zip_code_id
          AND pt.delivery_date = pp.delivery_date
          AND pc.delivery_date = pz.delivery_date
          AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
          AND pc.delivery_available_flag = 'Y'
          AND pp.delivery_available_flag = 'Y'
          AND pz.status_code = 'Y'
AND (select 'Y' from ftd_apps.florist_codifications fc, ftd_apps.florist_zips fz, ftd_apps.florist_blocks fb, ftd_apps.florist_master fm
               where fc.codification_id = pp.codification_id
               and fz.florist_id = fc.florist_id
               and fz.zip_code = pz.zip_code_id
               and fb.florist_id (+) = fc.florist_id
               and fm.florist_id = fc.florist_id
               and fm.status <> 'Inactive'
               AND (   (pz.delivery_date NOT BETWEEN TRUNC(fc.block_start_date) AND fc.block_end_date)
                    OR (fc.block_start_date IS NULL)  )
               AND (   (pz.delivery_date NOT BETWEEN TRUNC(fz.block_start_date) AND fz.block_end_date)
                    OR (fz.block_start_date IS NULL)  )
               AND (   (pz.delivery_date NOT BETWEEN TRUNC(fb.block_start_date) AND fb.block_end_date)
                    OR (fb.block_start_date IS NULL)  )
               AND (   (
                   (   (pz.delivery_date = trunc(sysdate) and sysdate not between fb.suspend_start_date and fb.suspend_end_date)
                      OR (pz.delivery_date > sysdate and pz.delivery_date NOT BETWEEN fb.suspend_start_date AND fb.suspend_end_date)   )
                      OR (fb.suspend_type <> 'G' and fm.super_florist_flag ='Y'))
                   OR (fb.suspend_start_date IS NULL)  )
               AND (   ((pz.delivery_date - trunc(sysdate)) > pp.addon_days_qty)

                    OR (((pz.delivery_date - trunc(sysdate)) = pp.addon_days_qty)
                       AND (fz.cutoff_time + pt.delta_to_cst_hhmm) > to_char(sysdate,'HH24mm')))
               and rownum=1) = 'Y';



# 2292 - CVV2
alter table FTD_APPS.PAYMENT_METHODS add (
CSC_REQUIRED_FLAG Varchar2(1) default 'N' not null,
constraint payment_methods_ck5 check (csc_required_flag in ('Y','N')));

update ftd_apps.payment_methods
    set CSC_REQUIRED_FLAG = 'Y'
    where payment_method_id in ('AX', 'VI', 'MC', 'DI', 'DC', 'CB');

comment on column ftd_apps.payment_methods.csc_required_flag is 'CSC = card security code';

alter table scrub.payments add (
CSC_RESPONSE_CODE  Varchar2(50),
CSC_VALIDATED_FLAG Varchar2(50),
CSC_FAILURE_CNT    Varchar2(50));

alter table clean.payments add (
CSC_RESPONSE_CODE  Varchar2(1),
CSC_VALIDATED_FLAG Varchar2(1),
CSC_FAILURE_CNT    INTEGER);

ALTER TABLE JOE.ORDER_PAYMENTS add (
CSC_RESPONSE_CODE Varchar2(1),
CSC_VALIDATED_FLAG Varchar2(1) default 'N' not null,
CSC_FAILURE_CNT INTEGER,
constraint order_payments_ck1 check (csc_validated_flag in ('Y','N','S')));

comment on column joe.order_payments.csc_validated_flag is 'Y = validated, N = rejected, S = submitted-awaiting response';

insert into joe.element_config (app_code, element_id, location_desc, label_element_id, required_flag, required_cond_desc,
required_cond_type_name, validate_flag, error_txt, title_txt, value_max_bytes_qty, tab_seq, tab_init_seq, accordion_index_num,
recalc_flag, product_validate_flag, calc_xml_flag) values (
'JOE', 'paymentCSC', 'HEADER', 'paymentCSCLabel', 'Y', 'JOE_REQUIRED.paymentCSC', 'FUNCTION', 'N',
'May I please have your card security code?', 'Enter the 3 or 4 digit card security code', 4, 3305, 3305, 1, 'N', 'N', 'Y');

insert into joe.element_config
    (app_code, element_id, location_desc, label_element_id, required_flag, validate_flag, title_txt, tab_seq, tab_init_seq, accordion_index_num, recalc_flag, product_validate_flag, calc_xml_flag)
values 
    ('JOE', 'paymentCSCOverride', 'HEADER', 'paymentCSCOverrideLabel', 'N', 'N', 'Check if you have to override the CSC', 3225, -1, 1, 'N', 'N', 'Y');

insert into joe.oe_script_master (script_id, script_type_code, script_txt, created_by, created_on, updated_by, updated_on)
values ('paymentCSC', 'Default', 'May I have your card security code?', 'Defect_2292', sysdate, 'Defect_2292', sysdate);




--7229
alter table ftd_apps.source_master add 
     (primary_backup_rwd_flag            char(1) default 'Y' not null,
         constraint source_master_ck13  check (primary_backup_rwd_flag in ('Y','N'))
     );

alter table ftd_apps.source_master$ add 
     (primary_backup_rwd_flag            char(1)
     );


--Included here to make life easier - the module in ftd_apps_objects was also changed.
create or replace view ftd_apps.source as
SELECT
        sm.source_code,
        sm.marketing_group source_type,
        sm.department_code,
        sm.description,
        sm.start_date,
        sm.end_date,
        sm.price_header_id pricing_code,
        sm.snh_id shipping_code,
        spr.program_name partner_id,
        sm.payment_method_id valid_pay_method,
        sm.list_code_flag,
        sm.default_source_code_flag,
        sm.billing_info_prompt,
        sm.billing_info_logic,
        sm.source_type marketing_group,
        sm.external_call_center_flag,
        sm.highlight_description_flag,
        sm.discount_allowed_flag,
        sm.bin_number_check_flag,
        sm.order_source,
        null mileage_bonus,
        sm.promotion_code,
        pr.calculation_basis mileage_calculation_source,
        pr.bonus_points,
        pr.bonus_calculation_basis bonus_type,
        pr.bonus_separate_data separate_data,
        sm.yellow_pages_code,
        pr.points points_miles_per_dollar,
        pr.maximum_points maximum_points_miles,
        sm.jcpenney_flag,
        sm.default_source_code_flag oe_default_flag,
        sm.company_id,
        sm.send_to_scrub,
        sm.fraud_flag,
        sm.enable_lp_processing,
        sm.emergency_text_flag,
        sm.requires_delivery_confirmation,
        sm.webloyalty_flag,
        cm.company_name,
        cm.internet_origin,
        sm.primary_backup_rwd_flag
FROM    ftd_apps.source_master sm
LEFT OUTER JOIN
(
        select  sp.source_code,
                sp.program_name
        from    ftd_apps.source_program_ref sp
        where   sp.start_date =
                (
                        select  max(b.start_date)
                        from    ftd_apps.source_program_ref b
                        where   b.source_code = sp.source_code
                        and     b.start_date <= sysdate
                )
) spr
ON      sm.source_code = spr.source_code
LEFT OUTER JOIN ftd_apps.program_reward pr
ON      spr.program_name = pr.program_name
JOIN ftd_apps.company_master cm
ON      cm.company_id = sm.company_id;



update FTD_APPS.PARTNER_MASTER set SOURCE_PRIMARY_FLORIST_FLAG='N';

-- needed for DBMoto replication
alter table ftd_apps.DELIVERY_DATE_RANGE add constraint DELIVERY_DATE_RANGE_PK primary key (start_date, end_date)
using index tablespace ftd_apps_indx;

alter table ftd_apps.price_header_details drop constraint price_header_details_uk;
alter table ftd_apps.price_header_details add constraint price_header_details_pk primary key (PRICE_HEADER_ID,MIN_DOLLAR_AMT)
using index tablespace ftd_apps_indx;

alter table ftd_apps.product_colors drop constraint product_colors_uk1;
alter table ftd_apps.product_colors add constraint product_colors_pk primary key (PRODUCT_ID,PRODUCT_COLOR)
using index tablespace ftd_apps_indx;

alter table ftd_apps.upsell_detail drop constraint UPSELL_DETAIL_UK1;
alter table ftd_apps.upsell_detail add constraint upsell_detail_pk primary key (UPSELL_DETAIL_ID,UPSELL_MASTER_ID)
using index tablespace ftd_apps_indx;

alter table ftd_apps.partner_master add (display_name varchar2(100));

grant execute on aas.authenticate_pkg to clean;


alter table joe.address_type add (sort_order  number(22));
Update joe.address_type set sort_order = 0;
alter table joe.address_type modify(sort_order not null);



insert into aas.resources     (resource_id, context_id, description, created_on, updated_on, updated_by)
values                        ('SourceFloristPriority','Order Proc',null,sysdate,sysdate,'Defect_7229');


--Create relationship between resource and resource group
insert into aas.rel_acl_rp    (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values                        ('FulfillmentAdmin','SourceFloristPriority','Order Proc','View',sysdate,sysdate,'Defect_7229');


update joe.address_type set sort_order = 1 where address_type_code = 'HOME';

update joe.address_type set sort_order = 2 where address_type_code = 'BUSINESS';

update joe.address_type set sort_order = 3 where address_type_code = 'FUNERAL HOME';

update joe.address_type set sort_order = 5 where address_type_code = 'HOSPITAL';

update joe.address_type set sort_order = 6 where address_type_code = 'NURSING HOME';

update joe.address_type set sort_order = 7 where address_type_code = 'OTHER';

insert into joe.address_type 
(ADDRESS_TYPE_CODE, DESCRIPTION, PROMPT_FOR_BUSINESS_FLAG, BUSINESS_LABEL_TXT, PROMPT_FOR_LOOKUP_FLAG, LOOKUP_LABEL_TXT, PROMPT_FOR_ROOM_FLAG, ROOM_LABEL_TXT, PROMPT_FOR_HOURS_FLAG, HOURS_LABEL_TXT, DEFAULT_FLAG, SORT_ORDER)
values 
('CEMETERY', 'Cemetery', 'Y', 'Cemetery', 'Y', 'Cemetery search', 'Y', 'Gravesite', 'Y', 'Time of Service', null, 4); 

update joe.address_type set description = 'Funeral Home', business_label_txt = 'Funeral Home' where  address_type_code = 'FUNERAL HOME';



-- global parm for occasion name
INSERT INTO FRP.GLOBAL_PARMS
  (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES
  ('OE_CONFIG','MAX_PHONE_RECORDS','8','Defect_7229',sysdate,'Defect_7229',sysdate, 'Maximum number of rows to return for the phone number search.');

update JOE.ADDRESS_TYPE set SORT_ORDER = SORT_ORDER * 10;

alter table joe.address_type add constraint address_type_uk2 unique (sort_order) using index tablespace joe_indx;

update JOE.ELEMENT_CONFIG set TAB_SEQ = 32700, TAB_INIT_SEQ = 32700 where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientType';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 32701, TAB_INIT_SEQ = 32701 where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhone';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 32702, TAB_INIT_SEQ = 32702 where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhoneExt';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 32703, TAB_INIT_SEQ = 32703 where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhoneSearch';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 2410,  TAB_INIT_SEQ = 2410  where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientType';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 2420,  TAB_INIT_SEQ = 2420  where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhone';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 2430,  TAB_INIT_SEQ = -1    where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhoneExt';
update JOE.ELEMENT_CONFIG set TAB_SEQ = 2440,  TAB_INIT_SEQ = 2440  where APP_CODE = 'JOE' and LOCATION_DESC = 'DETAIL' and ELEMENT_ID = 'recipientPhoneSearch';
commit;

alter table frp.address_types drop constraint address_types_ck1;
alter table frp.address_types add constraint address_types_ck1 check (code in ('B','F','H','R','O','N','C'));
update FRP.ADDRESS_TYPES set DESCRIPTION = 'FUNERAL HOME' where ADDRESS_TYPE = 'FUNERAL HOME';
insert into FRP.ADDRESS_TYPES (ADDRESS_TYPE, CODE, DESCRIPTION, SORT_ORDER) values ('CEMETERY', 'C', 'CEMETERY', '40');
update FRP.ADDRESS_TYPES set SORT_ORDER = 10 where ADDRESS_TYPE = 'HOME';
update FRP.ADDRESS_TYPES set SORT_ORDER = 20 where ADDRESS_TYPE = 'BUSINESS';
update FRP.ADDRESS_TYPES set SORT_ORDER = 30 where ADDRESS_TYPE = 'FUNERAL HOME';
update FRP.ADDRESS_TYPES set SORT_ORDER = 50 where ADDRESS_TYPE = 'HOSPITAL';
update FRP.ADDRESS_TYPES set SORT_ORDER = 60 where ADDRESS_TYPE = 'NURSING HOME';
update FRP.ADDRESS_TYPES set SORT_ORDER = 70 where ADDRESS_TYPE = 'OTHER';

alter table frp.address_types modify (sort_order not null);

alter table frp.address_types add constraint address_types_uk1 unique (sort_order) using index tablespace frp_indx;

grant select  on frp.global_parms to global;
grant execute on ftd_apps.get_src_prg_ref_max_start_date to global;
grant select on ftd_apps.miles_points_redemption_rate to global;
grant select on ftd_apps.partner_program to global;
grant select on ftd_apps.payment_method_miles_points to global;
grant select on ftd_apps.product_attr_restr to global;
grant select on ftd_apps.product_attr_restr_source_excl to global;
grant select on ftd_apps.product_category  to global;
grant select on ftd_apps.product_master to global;
grant select on ftd_apps.product_source to global;
grant select on ftd_apps.product_sub_types  to global;
grant select on ftd_apps.product_types to global;
grant select on ftd_apps.program_reward to global;
grant select on ftd_apps.shopping_index_src to global;
grant select on ftd_apps.shopping_index_src_prod to global;
grant select on ftd_apps.source_master to global;
grant select on ftd_apps.source_program_ref to global;
grant select on ftd_apps.upsell_detail to global;
grant select on ftd_apps.upsell_master to global;
grant select on ftd_apps.upsell_source to global;

alter table frp.address_types add (
prompt_for_business_flag varchar2(1) default 'N' not null,
prompt_for_room_flag varchar2(1) default 'N' not null,
prompt_for_hours_flag varchar2(1) default 'N' not null,
room_label_txt varchar2(250),
hours_label_txt varchar2(250));

alter table frp.address_types add constraint address_types_ck2 check (prompt_for_business_flag in ('Y','N'));
alter table frp.address_types add constraint address_types_ck3 check (prompt_for_room_flag in ('Y','N'));
alter table frp.address_types add constraint address_types_ck4 check (prompt_for_hours_flag in ('Y','N'));

UPDATE FRP.ADDRESS_TYPES SET PROMPT_FOR_BUSINESS_FLAG= 'Y', PROMPT_FOR_ROOM_FLAG='Y', PROMPT_FOR_HOURS_FLAG='Y',
ROOM_LABEL_TXT='Room', HOURS_LABEL_TXT='Work Hours' WHERE CODE='B';

UPDATE FRP.ADDRESS_TYPES SET PROMPT_FOR_BUSINESS_FLAG='Y', PROMPT_FOR_ROOM_FLAG='Y', PROMPT_FOR_HOURS_FLAG='Y',
ROOM_LABEL_TXT='Room/Chapel', HOURS_LABEL_TXT='Time of Service' WHERE CODE='F';

UPDATE FRP.ADDRESS_TYPES SET PROMPT_FOR_BUSINESS_FLAG='Y', PROMPT_FOR_ROOM_FLAG='Y', PROMPT_FOR_HOURS_FLAG='Y',
ROOM_LABEL_TXT='Gravesite', HOURS_LABEL_TXT='Time of Service' WHERE CODE='C';

UPDATE FRP.ADDRESS_TYPES SET PROMPT_FOR_BUSINESS_FLAG='Y', PROMPT_FOR_ROOM_FLAG='Y', PROMPT_FOR_HOURS_FLAG='Y',
ROOM_LABEL_TXT='Room/Ward', HOURS_LABEL_TXT='Hours' WHERE CODE='H';

UPDATE FRP.ADDRESS_TYPES SET PROMPT_FOR_BUSINESS_FLAG='Y', PROMPT_FOR_ROOM_FLAG='Y', PROMPT_FOR_HOURS_FLAG='N',
ROOM_LABEL_TXT='Room/Ward' WHERE CODE='N';

create sequence stats.order_service_stats_sq start with 1 increment by 1 cache 200;

create table stats.order_service_stats (
ORDER_SERVICE_STATS_ID NUMBER not null,
NODE                   VARCHAR2(50),
CLIENT_ID              VARCHAR2(50),
ACTION_TYPE            VARCHAR2(100),
stat_count             NUMBER(10),
STATUS_CODE            VARCHAR2(20),
stat_timestamp         timestamp) tablespace stats_data;

alter table stats.order_service_stats add constraint order_service_stats_pk primary key (order_service_stats_id)
using index tablespace stats_data;

alter table stats.order_service_stats add constraint order_service_stats_ck1 check 
(status_code in ('SUCCESS','FAILURE','PARTIAL_SUCCESS'));

# Defect 7377
alter table ftd_apps.stock_messages modify (stock_message_id varchar2(50));
alter table scrub.order_disposition modify (stock_message_id varchar2(50));

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'ZIP_CODE_AND_DATE_UNAVAILABLE',
'This is the message that is displayed when no products available for the zip code / delivery dates.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'ZIP_CODE_AND_DATE_UNAVAILABLE'),
NULL, NULL, 'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

grant select on ftd_apps.content_master to global;
grant select on ftd_apps.content_detail to global;

update JOE.ADDRESS_TYPE set DESCRIPTION = 'Funeral', BUSINESS_LABEL_TXT = 'Funeral' where ADDRESS_TYPE_CODE = 'FUNERAL HOME';

alter table ftd_apps.product_keywords add (product_keyword_id integer);
create sequence ftd_apps.product_keywords_sq start with 1 increment by 1 cache 20;
update ftd_apps.product_keywords set product_keyword_id = ftd_apps.product_keywords_sq.nextval;
alter table ftd_apps.product_keywords add constraint product_keywords_pk primary key (product_keyword_id)
using index tablespace ftd_apps_indx;

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GET_DELIVERY_DAYS_MAX_COUNT','120','Defect_6414',sysdate,'Defect_6414',sysdate, 'Max number of days returned in getDeliveryDate request.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('product_database', 'sif_error_directory', '/u02/apollo/pdb/sif/error', '6414', sysdate, '6414', sysdate,
'Local error directory to keep processed shopping index files that had processing errors');

update ftd_apps.partner_master set display_name = 'SCI' where partner_name = 'SCI';

update ftd_apps.partner_master set display_name = 'USAA' where partner_name = 'USAA';

-- changes for 4.6.1
alter table ftd_apps.shopping_index_src modify (index_desc null);

alter table ftd_apps.shopping_index_src_stage modify (index_desc null);

alter table ftd_apps.company_master drop constraint company_master_fk2;

alter table ftd_apps.company_master rename column default_source_code to default_domain;

update ftd_apps.company_master set default_domain = 'ftd';

update ftd_apps.company_master set default_domain = 'florist' where company_id = 'FLORIST';

alter table ftd_apps.SHOPPING_INDEX_SRC drop constraint SHOPPING_INDEX_SRC_FK1;

alter table ftd_apps.SHOPPING_INDEX_SRC_PROD drop constraint SHOPPING_INDEX_SRC_PROD_FK1;

alter table ftd_apps.SHOPPING_INDEX_SRC_PROD_STAGE drop constraint SHOP_INDE_SRC_PROD_STAGE_FK1;

alter table ftd_apps.SHOPPING_INDEX_SRC_STAGE drop constraint SHOPPING_INDEX_SRC_STG_FK1;

alter table ftd_apps.shopping_index_src_prod_stage modify (product_id varchar2(20));

update FRP.ADDRESS_TYPES set DESCRIPTION = 'FUNERAL' where ADDRESS_TYPE = 'FUNERAL HOME';

update FRP.ADDRESS_TYPES set DESCRIPTION = 'HOSPITAL' where ADDRESS_TYPE = 'HOSPITAL';

delete from ftd_apps.content_detail where content_master_id = 
                (select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CONTEXT' and content_name='DATE_UNAVAILABLE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CONTEXT' and content_name='DATE_UNAVAILABLE';

insert into ftd_apps.content_master 
(content_master_id, content_context, content_name, content_description, content_filter_id1, content_filter_id2, updated_by, updated_on, created_by, created_on) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'DATE_UNAVAILABLE',
'This is the message that is displayed when the date requested is no longer available.',
null, null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into ftd_apps.content_detail 
(content_detail_id, content_master_id, filter_1_value, filter_2_value, content_txt, updated_by, updated_on, created_by, created_on) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'DATE_UNAVAILABLE'),
'Allurent', NULL, 'The delivery date ~deliveryDate~ is no longer available, please select another date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'DATE_UNAVAILABLE'),
'FTD', NULL, 'The delivery date ~deliveryDate~ is no longer available, please select another date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


update FTD_APPS.CONTENT_DETAIL 
set content_txt='http://a80.g.akamai.net/f/80/71/6h/www.ftd.com/pics/products/addons/~REPLACE_TOKEN_1~_99x99.jpg',
updated_on=sysdate,
updated_by='Defect_6414'
where content_master_id=(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_IMAGE_URL' AND CONTENT_NAME = 'IMAGE_ADDON');

delete from ftd_apps.content_detail where content_master_id = 
	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_PRODUCT_UNAVAILABLE');    

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_PRODUCT_UNAVAILABLE'),
    'FTD',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT, 
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
        AND CONTENT_NAME = 'VALIDATION_ERROR_PRODUCT_UNAVAILABLE'),
    'Allurent',
    null,
    'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.',
    'Defect_6414',
    sysdate,
    'Defect_6414',
    sysdate);     
    

    
    delete from ftd_apps.content_detail where content_master_id = 
        	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US');
 
    
    insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
        CONTENT_MASTER_ID,
        FILTER_1_VALUE,
        FILTER_2_VALUE,
        CONTENT_TXT, 
        UPDATED_BY,
        UPDATED_ON,
        CREATED_BY,
        CREATED_ON) 
    values (
        ftd_apps.content_detail_sq.nextval, 
        (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
            WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
            AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US'),
        'FTD',
        null,
        'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist. <BR>If the zip code you have entered is correct please shop our <a href="http://www.ftd.com/~sourceCode~/catalog/category.epl?index_id=specialtygifts/">Specialty Gift Collection</a> that is available for delivery to the zip code.',
        'Defect_6414',
        sysdate,
        'Defect_6414',
        sysdate);     
    
    insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
        CONTENT_MASTER_ID,
        FILTER_1_VALUE,
        FILTER_2_VALUE,
        CONTENT_TXT, 
        UPDATED_BY,
        UPDATED_ON,
        CREATED_BY,
        CREATED_ON) 
    values (
        ftd_apps.content_detail_sq.nextval, 
        (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
            WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
            AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US'),
        'Allurent',
        null,
        'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist. <BR>If the zip code you have entered is correct please shop our <a href="http://www.ftd.com/~sourceCode~/catalog/category.epl?index_id=specialtygifts/">Specialty Gift Collection</a> that is available for delivery to the zip code.',
        'Defect_6414',
        sysdate,
        'Defect_6414',
        sysdate);     
    
    
    
    
    
    delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA');
 
    
    insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
        CONTENT_MASTER_ID,
        FILTER_1_VALUE,
        FILTER_2_VALUE,
        CONTENT_TXT, 
        UPDATED_BY,
        UPDATED_ON,
        CREATED_BY,
        CREATED_ON) 
    values (
        ftd_apps.content_detail_sq.nextval, 
        (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
            WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
            AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA'),
        'FTD',
        null,
        'The recipient''s postal code you''ve entered is not presently serviced by an FTD&reg; florist. Please verify that you have entered the correct postal code.',
        'Defect_6414',
        sysdate,
        'Defect_6414',
        sysdate);     
    
    insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
        CONTENT_MASTER_ID,
        FILTER_1_VALUE,
        FILTER_2_VALUE,
        CONTENT_TXT, 
        UPDATED_BY,
        UPDATED_ON,
        CREATED_BY,
        CREATED_ON) 
    values (
        ftd_apps.content_detail_sq.nextval, 
        (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
            WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR'
            AND CONTENT_NAME = 'VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA'),
        'Allurent',
        null,
        'The recipient''s postal code you''ve entered is not presently serviced by an FTD&reg; florist. Please verify that you have entered the correct postal code.',
        'Defect_6414',
        sysdate,
        'Defect_6414',
        sysdate);     
 
---------- add filter id
delete from ftd_apps.content_detail where content_master_id = 
	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='DATE_UNAVAILABLE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='DATE_UNAVAILABLE';

insert into ftd_apps.content_master 
(content_master_id, content_context, content_name, content_description, content_filter_id1, content_filter_id2, updated_by, updated_on, created_by, created_on) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'DATE_UNAVAILABLE', 'This is the message that is displayed when the date requested is no longer available.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'), 
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into ftd_apps.content_detail 
(content_detail_id, content_master_id, filter_1_value, filter_2_value, content_txt, updated_by, updated_on, created_by, created_on) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'DATE_UNAVAILABLE'),
'Allurent', NULL, 'The delivery date ~deliveryDate~ is no longer available, please select another date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'DATE_UNAVAILABLE'),
'FTD', NULL, 'The delivery date ~deliveryDate~ is no longer available, please select another date.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='GENERIC_ERROR_CANNOT_FULFILL_REQUEST');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='GENERIC_ERROR_CANNOT_FULFILL_REQUEST';

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST', 'The message to be displayed when the service cannot process the request.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'), 
 null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST'),
'FTD', NULL, 
'We''re sorry we couldn''t find that page, please <a href="http://www.ftd.com/~sourceCode~/best-sellers-ctg/product-flowers-bestsellers">click here</a> to shop our bestsellers.', 
'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'GENERIC_ERROR_CANNOT_FULFILL_REQUEST'),
'Allurent', NULL,
'We''re sorry we couldn''t find that page, please <a href="http://www.ftd.com/~sourceCode~/best-sellers-ctg/product-flowers-bestsellers">click here</a> to shop our bestsellers.',
'Defect_6414', sysdate, 'Defect_6414', sysdate);

delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_COUNTRY_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_COUNTRY_ID';

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_COUNTRY_ID', 'The message to be displayed if a request does not include a valid country.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'), 
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_COUNTRY_ID'),
'FTD', NULL, 'Please <a href="http://www.ftd.com/~sourceCode~/catalog/international.epl">click here</a> for products available for International deliveries.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_COUNTRY_ID'),
'Allurent', NULL, 'Please <a href="http://www.ftd.com/~sourceCode~/catalog/international.epl">click here</a> for products available for International deliveries.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);



delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_ZIP_CODE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_ZIP_CODE';

insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'VALIDATION_ERROR_INVALID_ZIP_CODE', 'The message to be displayed if a request does not include a valid zip code.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'), 
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_ZIP_CODE'),
'FTD', NULL, 'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'VALIDATION_ERROR_INVALID_ZIP_CODE'),
'Allurent', NULL, 'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CONTEXT' and content_name='ADDON_OCCASION_DISPLAY_NAME');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CONTEXT' and content_name='ADDON_OCCASION_DISPLAY_NAME';


insert into FTD_APPS.CONTENT_MASTER 
(CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CONTEXT', 'ADDON_OCCASION_DISPLAY_NAME', 'This control defines the display name for the addon occasion.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ADDON_ID'), 
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL 
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values 
(ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CONTEXT' AND CONTENT_NAME = 'ADDON_OCCASION_DISPLAY_NAME'),
'RC999', NULL, 'Occasion', 'Defect_6414', sysdate, 'Defect_6414', sysdate);


delete from ftd_apps.content_detail where content_master_id in 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='ZIP_CODE_AND_DATE_UNAVAILABLE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='ZIP_CODE_AND_DATE_UNAVAILABLE';

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'ORDER_SERVICE_CLIENT_ERROR', 'ZIP_CODE_AND_DATE_UNAVAILABLE',
'This is the message that is displayed when no products available for the zip code / delivery dates.',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'ORDER_SERVICE_CLIENT_ID'), 
null, 'Defect_6414', sysdate, 'Defect_6414', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'ZIP_CODE_AND_DATE_UNAVAILABLE'),
'Allurent', NULL, 'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'ORDER_SERVICE_CLIENT_ERROR' AND CONTENT_NAME = 'ZIP_CODE_AND_DATE_UNAVAILABLE'),
'FTD', NULL, 'The recipient''s zip code you''ve entered is not presently serviced by an FTD&reg; florist.  Please verify that you have entered the correct zip code.', 'Defect_6414', sysdate,
'Defect_6414', sysdate);

delete from ftd_apps.content_detail where content_master_id = 
            	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_DELIVERY_DATE_PASSED');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_DELIVERY_DATE_PASSED';
            	
delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT';            	


delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_CATEGORY_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_CATEGORY_ID';            	

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_CLIENT_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_CLIENT_ID';            	

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_LOAD_LEVEL');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_LOAD_LEVEL'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRICE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRICE';            	

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRODUCT_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRODUCT_ID'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_PRODUCT_ORDER_BY'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_SOURCE_CODE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_SOURCE_CODE'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_CLIENT_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_CLIENT_ID'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_PRICE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_PRICE'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_PRODUCT_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_PRODUCT_ID'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_SOURCE_CODE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_SOURCE_CODE'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_ZIP_CODE');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_MISSING_ZIP_CODE'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_TOO_MANY_PRODUCT_ID');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_TOO_MANY_PRODUCT_ID'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_INVALID_DELIVERY_DATE_FORMAT');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_INVALID_DELIVERY_DATE_FORMAT'; 

delete from ftd_apps.content_detail where content_master_id = 
    	(select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT');
delete from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR' and content_name='VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT'; 


-- Remove the messages for no client id
delete 
from ftd_apps.content_detail
where content_master_id in (select content_master_id from ftd_apps.content_master where content_context='ORDER_SERVICE_CLIENT_ERROR')
and filter_1_value is null;

-- changes for 4.6.2
grant select on frp.address_types to clean;

update JOE.OE_SCRIPT_MASTER
set script_txt = 'May I please have your card security code?'
where script_id = 'paymentCSC';

update JOE.ELEMENT_CONFIG
set ERROR_TXT = 'Please enter the 3 or 4 digit card security code found on the customer''s card.'
where APP_CODE = 'JOE'
and ELEMENT_ID = 'paymentCSC';

alter table ftd_apps.shopping_index_src drop constraint shopping_index_src_fk2;

alter table ftd_apps.shopping_index_src drop primary key;

alter table ftd_apps.shopping_index_src drop column parent_index_name;

alter table ftd_apps.shopping_index_src add (updated_on varchar2(100), updated_by date);

alter table ftd_apps.shopping_index_src_prod add (updated_on varchar2(100), updated_by date);

alter table ftd_apps.shopping_index_src_stage drop (parent_index_name, failure_reason_txt);

alter table ftd_apps.shopping_index_src add constraint shopping_index_src_pk primary key
(index_name, source_code) using index tablespace ftd_apps_indx;

create table ftd_apps.shopping_index_sub_index (
index_name varchar2(300) not null,
source_code varchar2(10) not null,
sub_index_name varchar2(300) not null,
display_seq_num number not null,
created_on date not null,
created_by varchar2(100) not null, 
updated_on date not null,
updated_by varchar2(100) not null) tablespace ftd_apps_data;

alter table ftd_apps.shopping_index_sub_index add (constraint shopping_index_sub_index_pk
primary key (index_name, source_code, sub_index_name) using index tablespace ftd_apps_indx);

create table ftd_apps.shopping_index_sub_index_stage (
index_name varchar2(300) not null,
source_code varchar2(10) not null,
sub_index_name varchar2(300) not null,
display_seq_num number not null
created_on date not null,
created_by varchar2(100) not null) tablespace ftd_apps_data;

alter table ftd_apps.shopping_index_sub_index_stage add (constraint shopping_index_sub_index_stgpk
primary key (index_name, source_code, sub_index_name) using index tablespace ftd_apps_indx);

grant select on ftd_apps.shopping_index_sub_index to global;

--*************************************************
--*************************************************
--*************************************************
-- DO THESE CLEAN.PAYMENTS MODS LAST DUE TO TIME
--*************************************************
--*************************************************
--*************************************************

alter table clean.payments add constraint payments_ck4 check (csc_validated_flag in ('S','Y','N'));

update clean.payments set csc_validated_flag = 'N';

alter table clean.payments modify (csc_validated_flag default 'N' not null);


----
---- END Fix 4.6 Release Script
----
