------------------------------------------------------------------------------------
-- begin requested by Meka Srivatsala ,                          3/25/2015  --------
-- defect #FE-2   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE
alter table clean.orders add is_joint_cart char(1);
alter table scrub.orders add is_joint_cart char(1);

------------------------------------------------------------------------------------
-- end   requested by Meka Srivatsala ,                          3/25/2015  --------
-- defect #FE-2   (Pavan)                           --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk ,                          5/7/2015  --------
-- defect #FE-14  (Rizvi)                           --------------------------------
------------------------------------------------------------------------------------


update clean.stock_email se
set subject = 'Re: ProFlowers Comment'
where se.title = 'qc.co.queue.ProFlowers';


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk ,                          5/7/2015  --------
-- defect #FE-14   (Rizvi)                           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Salguti  Raghu Ram    ,                      3 /31/2015  --------
-- defect #FE-4   (Pavan)     (141668)              --------------------------------
------------------------------------------------------------------------------------
-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add AVS_FLORAL char(1) default 'N';
alter table ptn_pi.partner_mapping add AVS_DROPSHIP char(1) default 'N';

update PTN_PI.PARTNER_MAPPING set AVS_FLORAL = 'Y', avs_dropship = 'N' where partner_id = 'PRO';
update PTN_PI.partner_mapping set avs_dropship = 'Y', avs_floral = 'N' where partner_id <> 'PRO';

------------------------------------------------------------------------------------
-- end   requested by Salguti  Raghu Ram    ,                      3 /31/2015  --------
-- defect #FE-4   (Pavan)       (141668)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /10/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add SEND_DCON_FEED varchar(1) default 'N' not null; 
alter table PTN_PI.PARTNER_ORDER_FULFILLMENT add DELIVERY_STATUS varchar(20);

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /10/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               4 / 1/2015  --------
-- defect #FE-15  (Mark )     (142114)              --------------------------------
------------------------------------------------------------------------------------

-- Add ProFlowers to Excluded Preferred Partners for Phoenix
update frp.global_parms
set value = 'SCI SYMPATHY ProFlowers', updated_on = sysdate, updated_by = 'FE-15'
where context = 'PHOENIX'
and name = 'EXCLUDED_PREFERRED_PARTNERS';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               4 / 1/2015  --------
-- defect #FE-15  (Mark )     (142114)              --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /13/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

update PTN_PI.PARTNER_MAPPING set SEND_DCON_FEED='Y'  where partner_id='PRO';
commit;
------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /13/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /15/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

alter table PTN_PI.PARTNER_ORDER_FULFILLMENT add DELIVERY_STATUS_DATETIME DATE;

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /15/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /22/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

Insert into quartz_schema.PIPELINE (PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,
DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG) values
 ('PROCESS_SEND_PARTNER_DCON_FEEDS','PARTNER_INTEGRATION','Post Partner Order DCON Feeds',
 'Process to get partner order delivery confirmation feeds and post it to ESB','300',
 'PROCESS_SEND_PARTNER_DCON_FEEDS','Y','OJMS.PARTNER_ORDERS','SimpleJmsJob','Y');
 ------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /22/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------

SET DEFINE OFF
------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-12  (Syed )     (143940)              --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE ftd_apps.stock_messages_8_1_0 as select * from ftd_apps.stock_messages;


delete
from ftd_apps.stock_messages
where DESCRIPTION like '%ADDON UNAVAILABLE%'
or description like '%ADDON SUBSTITUTION%' 
or description like '%ADDON VASE SUBSTITUTION%' 
or description like '%TWO ORDERS PLACED WITH%' 
or description like '%YOUR MOTHER''S DAY DELIVERY DATE%' 
or description like '%MOTHER''S DAY DELIVERY DATE%' 
or description like '%MOTHER''S DAY DATE REQUEST%'
or description like '%YOUR VALENTINE''S DAY ORDER%';

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-12  (Syed )     (143940)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                              4 / 22/2015  --------
-- defect #FE-12  (Mark )     (145272)              --------------------------------
------------------------------------------------------------------------------------

-- the CREATE TYPE statement is PL/SQL and requires a trailing slash.
CREATE TYPE rpt.temp_table_type AS TABLE OF VARCHAR2(100);
/

set define off
--
-- Daily Billing Log
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100203,
    2,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=D&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002031;
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=M&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002032;

--
-- Deferred Revenue Report
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100233,
    1,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002331;

--
-- Invoice Statement Additional Billing Info
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100212,
    3,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002121;

--
-- Orders By Delivery Date
--
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=D&p_month=2&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002051;
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=M&p_month=2&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002052;

--
-- Orders By Source
--
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA&p_accounting_view=Gross&p_summary_detail=Detail&p_sort_field=Source&p_daily=Y'
    where report_id = 1005015;

--
-- Orders Delivered summary
--
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002028;
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=D&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1003021;

--
-- Orders For Future Delivery
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100206,
    2,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_origin_code=ALL&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002063;

--
-- Orders Not Billed
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100215,
    2,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002151;

--
-- Orders Taken Summary
--
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=D&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002018;
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=M&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1003018;

--
-- Refund Summary
--
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=D&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002043;
update rpt.report
    set schedule_parm_value = '&p_monthly_daily_ind=M&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002041;

--
-- Refunds by Ship-to State
--
insert into rpt.report_parm_ref (REPORT_PARM_ID,
    REPORT_ID,
    SORT_ORDER,
    REQUIRED_INDICATOR,
    CREATED_ON,
    CREATED_BY,
    ALLOW_FUTURE_DATE_IND
) values (
    100200,
    100209,
    3,
    'Y',
    sysdate,
    'FE-16',
    null
);
update rpt.report
    set schedule_parm_value = '&p_origin_code=ALL&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1002092;

--
-- Source Code Edit Report
--
update rpt.report
    set schedule_parm_value = '&p_company_code=FTD,FLORIST,HIGH,FDIRECT,FUSA,GIFT,FTDCA'
    where report_id = 1006011;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                              4 / 22/2015  --------
-- defect #FE-12  (Mark )     (145272)              --------------------------------
------------------------------------------------------------------------------------

