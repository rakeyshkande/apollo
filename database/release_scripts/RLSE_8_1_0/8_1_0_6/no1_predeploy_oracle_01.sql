------------------------------------------------------------------------------------
-- begin requested by Gary Sergey   ,                            5/18/2015  --------
-- defect #FE-78   (Mark)     (150067)              --------------------------------
------------------------------------------------------------------------------------
 
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MESSAGE_GENERATOR_CONFIG', 'EMAIL_PARTNER_ORDER', 'ProFlowers', 
        'FE-78', sysdate, 'FE-78', sysdate, 
        'List of (space-separated) companies that should use the partner order number in email instead of external order number.  Currently only used in some auto-replies.');

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey   ,                            5/18/2015  --------
-- defect #FE-78   (Mark)     (150067)              --------------------------------
------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------
-- begin requested by Tim Schmig    ,                            5/20/2015  --------
-- defect FE-13    (Mark)     (150354)              --------------------------------
------------------------------------------------------------------------------------
 
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES(
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'ot.queue.ProFlowers',
'Thank you for your inquiry regarding your recent order. 
We are actively working toward an answer for you and will contact you upon completion.  Our goal is to respond to your email within 24 hours. 
This is an automated response.  We will not receive replies to this email. 
Sincerely,
ProFlowers Customer Service
Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'ot.queue.ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig    ,                            5/20/2015  --------
-- defect FE-13    (Mark)     (150354)              --------------------------------
------------------------------------------------------------------------------------
 
