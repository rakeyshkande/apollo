 
--------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                    5/ 5/2015  ----------
-- defect #FE-14   (Mark ) request 147369             --------------------
--------------------------------------------------------------------------

alter user events identified by "one-time" account unlock;
conn events/one-time
set serveroutput on
declare
   v_job number;
begin
   dbms_job.submit(v_job, 'ins_novator_mailbox_monitor(''ProFlowers'');',
   sysdate+(10/1440),'sysdate+(10/1440)');
   dbms_output.put_line(to_char(v_job));
end;
/
conn &dba_user
alter user events identified by values 'disabled' account lock;

--------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                    5/ 5/2015  ----------
-- defect #FE-14   (Mark ) request 147369             --------------------
--------------------------------------------------------------------------

