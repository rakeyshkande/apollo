

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk    ,                      4/29/2015  --------
-- defect #FE-14   (Syed)     (145909)              --------------------------------
------------------------------------------------------------------------------------



INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'ProFlowers EXCLUDE DOMAINS', 'walmart.com',
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.',
    sysdate, 'FE-14', sysdate, 'FE-14');


INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'ProFlowers DEFAULT PARTNER NAME', 'ProFlowers',
    'Partner associated with the mailbox.  The default partner source code will be used when populating the CLEAN.POINT_OF_CONTACT.SOURCE_CODE field for unattached emails.',
    sysdate, 'FE-14', sysdate, 'FE-14');


SET serveroutput on;

DECLARE

v_key_value  VARCHAR2(1000);
v_db_name    VARCHAR2(9); 
v_desc_1     VARCHAR2(255);
v_box_value  VARCHAR2(1000);

BEGIN


	SELECT value INTO v_key_value FROM frp.global_parms WHERE context ='Ingrian';


	SELECT name INTO v_db_name FROM v$database;

	IF v_db_name = 'ZEUS' THEN
		v_desc_1      := 'ProFlowers mailbox user name.';
		v_box_value := 'sulfur.ftdi.com';
	ELSE    
		v_desc_1 := 'Login to pull erp emails for ProFlowers.';
		v_box_value := 'sodium.ftdi.com';
	END IF;



	Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
	VALUES ('email_request_processing', 'ProFlowers_mailbox_monitor_USERNAME',global.encryption.encrypt_it('fixme',v_key_value),v_desc_1, 
    		sysdate,'FE-14',sysdate,'FE-14',v_key_value);


	Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
	VALUES ('email_request_processing','ProFlowers_mailbox_monitor_PASSWORD',global.encryption.encrypt_it('fixme', v_key_value),'Password to pull erp emails for ProFlowers.',
    		sysdate,'FE-14',sysdate,'FE-14',v_key_value);


	INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    	values ( 'ins_novator_mailbox_monitor', 'ProFlowers MAIL SERVER', v_box_value,
    		'ProFlowers mail server.',sysdate, 'FE-14', sysdate, 'FE-14');



EXCEPTION

	WHEN OTHERS THEN

	DBMS_OUTPUT.PUT_LINE(substr(sqlerrm,1,100));


END;
/

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk    ,                      4/29/2015  --------
-- defect #FE-14   (Syed)     (145909)              --------------------------------
------------------------------------------------------------------------------------

