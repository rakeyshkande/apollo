set sqlblanklines on
------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-13  (Rizvi )     (143939)              --------------------------------
------------------------------------------------------------------------------------
-- New ProFlowers Message Tokens
insert into CLEAN.MESSAGE_TOKENS (TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE) 
values ('proflowers.phone', 'ALL', 'text', '877-383-6147', null); 

insert into CLEAN.MESSAGE_TOKENS (TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE) 
values ('proflowers.csemail', 'ALL', 'text', 'customercare@fe.proflowers.com', null);


-- COM Stock Emails

--APOLOGIES.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'APOLOGIES.ProFlowers',
'On behalf of ProFlowers, please accept our sincere apologies for your recent experience.  We always strive to provide our customers with the very finest service and we regret that this was not the case.

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Our Sincere Apologies re:',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'APOLOGIES.ProFlowers';

--CANCEL ORDER REQUEST.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CANCEL ORDER REQUEST.ProFlowers',
'At your request, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~.  

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We hope you''ll give us the opportunity to assist you in the future.  Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Confirmation',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'CANCEL ORDER REQUEST.ProFlowers';

--CANCEL ORDER REQUEST CANNOT CANCEL.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CANCEL ORDER REQUEST CANNOT CANCEL.ProFlowers',
'Thank you for your recent order.

We received your request to cancel your order and, unfortunately, we are unable to process the cancellation as your order is already in the process of being shipped or delivered.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancellation Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'CANCEL ORDER REQUEST CANNOT CANCEL.ProFlowers';

--CREDIT CARD AUTH.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'CREDIT CARD AUTH.ProFlowers',
'Thank you for your recent order.

We regret to inform you that the credit card you selected for payment would not authorize.  Please contact us to verify that we have the correct information so that we can ensure the delivery is completed without delay.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Payment Authorization Error',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'CREDIT CARD AUTH.ProFlowers';

--DELIVERY CONFIRMATION - Residence.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION - Residence.ProFlowers',
'We are pleased to inform you that the delivery of your order has been completed as scheduled.  
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Confirmation of Delivery',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION - Residence.ProFlowers';

--DELIVERY CONFIRMATION - Funeral Home.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION - Funeral Home.ProFlowers',
'We are pleased to inform you that the delivery of your order to ~facility.name~ has been completed as scheduled. 
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Confirmation of Delivery',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION - Funeral Home.ProFlowers';


--DELIVERY CONFIRMATION PENDING.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY CONFIRMATION PENDING.ProFlowers',
'Thank you for your recent order.

We have received your request to confirm that your order was delivered and as soon as we receive confirmation of delivery, we will notify you via e-mail.

If your order was delivered internationally, it may take up to 72 hours to receive a response.  We will promptly relay a confirmation to you upon receipt.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Confirmation of Delivery Pending',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DELIVERY CONFIRMATION PENDING.ProFlowers';

--DELIVERY DELAY.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY DELAY.ProFlowers',
'We regret to inform you that there is a one day delay in the delivery of your order.  We sincerely apologize for any inconvenience this may cause.

Your new delivery date is:

The shipping charges have been adjusted where applicable.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Delivery Date Change',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DELIVERY DELAY.ProFlowers';

--DELIVERY DELAY - WEATHER.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DELIVERY DELAY - WEATHER.ProFlowers',
'We regret to inform you that, due to severe weather conditions in the delivery area, the delivery of your order may be slightly delayed.  Please be assured that the delivery will be completed as soon as the weather permits.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Weather Related Delivery Delay',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DELIVERY DELAY - WEATHER.ProFlowers';

--DISCOUNT APPLIED.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DISCOUNT APPLIED.ProFlowers',
'We have received your request to apply a promotional discount to your order.  At this time, the discount has been applied and a refund in the amount of $~fref.amt~ has been posted to your ~payment.method.text~.

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  .  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Discount Applied Confirmation',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DISCOUNT APPLIED.ProFlowers';

--DOOR TAG - ATTEMPTED DELIVERY.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DOOR TAG - ATTEMPTED DELIVERY.ProFlowers',
'Thank you for your recent order.

The delivery of your order was attempted as scheduled; however, no one was home to accept the delivery at the time.  The recipient has been notified to contact the florist directly in order to arrange for a convenient time to complete the delivery.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Delivery Attempted',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DOOR TAG - ATTEMPTED DELIVERY.ProFlowers';

--DUPLICATE ORDERS.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DUPLICATE ORDERS.ProFlowers',
'Thank you for your recent order.

In reviewing your account, we noted that you have placed two identical orders with us. Please contact us to verify that you did intend to place both orders that we can ensure the delivery is completed without delay.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Duplicate Orders Received',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DUPLICATE ORDERS.ProFlowers';

--FUNERAL ORDER - ADDRESS REQUEST.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'FUNERAL ORDER - ADDRESS REQUEST.ProFlowers',
'Thank you for your recent order.

We have been advised that the family is requesting your address so that they may send you a thank you card.  It is our policy not to give this information to the family without your permission.  Please contact us and let us know if we may honor their request.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Address Permission Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'FUNERAL ORDER - ADDRESS REQUEST.ProFlowers';

--FUNERAL ORDER - CANCEL.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'FUNERAL ORDER - CANCEL.ProFlowers',
'We regret to inform you that, despite our best efforts, we were unable to complete the delivery of your order due to the time constraints of the funeral service.  We apologize for the notification of this inconvenience via e-mail, but we were unable to reach you at the phone number provided.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We sincerely apologize for any inconvenience this may have caused.  We hope you''ll give us the opportunity to assist you in the future.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Notification',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'FUNERAL ORDER - CANCEL.ProFlowers';

--GENERIC REPLY TEMPLATE.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'GENERIC REPLY TEMPLATE.ProFlowers',
'Thank you for your recent order.



Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Your ProFlowers Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'GENERIC REPLY TEMPLATE.ProFlowers';

--LP - CANNOT PROCESS YOUR ORDER.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'LP - CANNOT PROCESS YOUR ORDER.ProFlowers',
'We regret to inform you that we are unable to process your order.

At this time, the order has been cancelled and the ~payment.method.text~ has not been charged.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Unable to Process Your Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'LP - CANNOT PROCESS YOUR ORDER.ProFlowers';

--MISSING ADD ON.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING ADD ON.ProFlowers',
'On behalf of ProFlowers, please accept our sincere apologies for the missing item that was not included with your order.  We regret any inconvenience this may have caused.

At this time, we have refunded your ~payment.method.text~ in the amount of $~fref.amt~.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Our Sincere Apologies re:',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MISSING ADD ON.ProFlowers';

--MISSING CARD SIGNATURE.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING CARD SIGNATURE.ProFlowers',
'Thank you for your recent order.

In preparing for the delivery of your order, it has been brought to our attention that the card message was left unsigned. In the event that this was unintentional, we wanted to provide you the opportunity to add your name to the card.

If we do not hear back from you in time to process the change, we will ensure your order is delivered as scheduled.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Missing Card Signature',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MISSING CARD SIGNATURE.ProFlowers';

--MISSING DELIVERY INFORMATION.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MISSING DELIVERY INFORMATION.ProFlowers',
'Thank you for your recent order.

To complete the delivery, we will need additional information.

Please contact us to ensure we have all the necessary details.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Missing Delivery Information',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MISSING DELIVERY INFORMATION.ProFlowers';

--MODIFY ORDER - CANNOT CHANGE.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER - CANNOT CHANGE.ProFlowers',
'Thank you for your recent order.

We received your request to modify your order and, unfortunately, we cannot apply the change(s) you requested as your order is already in the process of being shipped or delivered.

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Changes to Your Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MODIFY ORDER - CANNOT CHANGE.ProFlowers';

--MODIFY ORDER - UPDATE ORDER PROCESSED.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER - UPDATE ORDER PROCESSED.ProFlowers',
'Thank you for your recent order.

We received your request to modify your order and are pleased to inform you that we will be able to make the change(s) that you requested and your order will arrive without delay.

To accommodate your change(s), the original order was cancelled and a new order was created with the updated information.  As a result, you will see a refund and a new charge on your ~payment.method.text~.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Changes to Your Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MODIFY ORDER - UPDATE ORDER PROCESSED.ProFlowers';

--MODIFY ORDER REQUEST.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'MODIFY ORDER REQUEST.ProFlowers',
'Thank you for your recent order.

We received your request to modify your order and are pleased to inform you that we will be able to make the change(s) that you requested and your order will arrive without delay.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Changes to Your Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'MODIFY ORDER REQUEST.ProFlowers';

--NO FLORIST - CANCEL.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'NO FLORIST - CANCEL.ProFlowers',
'We regret to inform you that, despite our best efforts, we were unable to locate a filling florist to complete the delivery.  We would like to offer our sincere apologies for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Notification',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'NO FLORIST - CANCEL.ProFlowers';

--ORDER CONFIRMATION - RECEIPT.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'ORDER CONFIRMATION - RECEIPT.ProFlowers',
'Thank you for your recent order.

Per your request, the details of your order are below.  

~order.details~

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Confirmation',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'ORDER CONFIRMATION - RECEIPT.ProFlowers';

--PENDING ORDER - CANCEL.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'PENDING ORDER - CANCEL.ProFlowers',
'We regret to inform you that we are unable to complete the delivery of your order at this time.  We have attempted to contact you by phone and email for additional information regarding your order without success and sincerely apologize for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Notification',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'PENDING ORDER - CANCEL.ProFlowers';

--POSITIVE FEEDBACK REPLY.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'POSITIVE FEEDBACK REPLY.ProFlowers',
'Thank you for taking the time to let us know about the pleasant experience you had with one of our customer service representatives.  

We take a lot of pride in what we do and it means a great deal to us to hear about experiences such as yours.  Your comments will be passed on to the appropriate staff member for acknowledgment.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Thank You',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'POSITIVE FEEDBACK REPLY.ProFlowers';

--PRODUCT UNAVAILABLE - CANCEL.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'PRODUCT UNAVAILABLE - CANCEL.ProFlowers',
'We regret to inform you that, despite our best efforts, we were unable to fill your order with the selected item.  We would like to offer our sincere apologies for any inconvenience this may cause.

At this time, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~. 

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Notification',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'PRODUCT UNAVAILABLE - CANCEL.ProFlowers';

--QUALITY ISSUE - REDELIVERY.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'QUALITY ISSUE - REDELIVERY.ProFlowers',
'Thank you for your recent order.

We sincerely apologize for any concerns regarding the quality of the flowers received.   We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

At this time, we have arranged for a fresh replacement to be delivered for your recipient to enjoy.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Redelivery of Your Order',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'QUALITY ISSUE - REDELIVERY.ProFlowers';

--QUALITY ISSUE - REFUND.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'QUALITY ISSUE - REFUND.ProFlowers',
'Thank you for your recent order.

We sincerely apologize for any concerns regarding the quality of the flowers received.   We assure you that your experience is not typical and that all your future orders will be filled with the special care and attention that they deserve.

At your request, your order has been cancelled and a full refund in the amount of $~fref.amt~ has been issued to your ~payment.method.text~.  

Please be advised that, although we have processed your refund immediately, some banking institutions may take up to 5 business days to post this credit to your account.  

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Order Cancel Confirmation',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'QUALITY ISSUE - REFUND.ProFlowers';

--REROUTING FEE.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'REROUTING FEE.ProFlowers',
'Thank you for your recent order.

We have received your request to change the address on your carrier-delivered product. 

Per our Delivery Policy, there will be an additional shipping fee charge for shipping address changes after the order has been placed and processed.  Please contact us to confirm this request and approve the additional charge.  

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Rerouting Fee Required',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'REROUTING FEE.ProFlowers';

--SECOND CHOICE NEEDED.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'SECOND CHOICE NEEDED.ProFlowers',
'Thank you for your recent order.

We regret to inform you that the item you selected is no longer available in the delivery area.  We sincerely apologize for any inconvenience this may cause.

Please contact us so that we may assist you in selecting another product.

We look forward to hearing from you.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Second Choice Selection Needed',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'SECOND CHOICE NEEDED.ProFlowers';

--SUBSTITUTION REQUIRED.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'SUBSTITUTION REQUIRED.ProFlowers',
'Thank you for your recent order.

We regret to inform you that the item you ordered was unavailable in the delivery area. To ensure the timely delivery of your gift, a similar item has been substituted.  When it is necessary to substitute, the utmost care and attention is given to your order to ensure that it is as similar as possible to the requested item.  

We sincerely apologize for any inconvenience this may cause.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Substitution Notification',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'SUBSTITUTION REQUIRED.ProFlowers';

--UNSUBSCRIBE REQUEST.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'UNSUBSCRIBE REQUEST.ProFlowers',
'Thank you for contacting us.

We have processed your request to unsubscribe from future notifications and special offers.  Please allow up to 72 hours for the removal of your email address from promotional campaigns that may already be underway.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Unsubscribe Request Processed',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'UNSUBSCRIBE REQUEST.ProFlowers';

--UPDATE ORDER - PROMOTION APPLIED.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'UPDATE ORDER - PROMOTION APPLIED.ProFlowers',
'Thank you for your recent order.

We have processed your request to have a promotion applied to your order.

To accommodate your change(s), the original order was cancelled and a new order was created with the updated information.  As a result, you will see a refund and a new charge on your ~payment.method.text~.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Promotion Applied',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
null
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'UPDATE ORDER - PROMOTION APPLIED.ProFlowers';


--DCONSYS - ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'DCONSYS - ProFlowers',
'We are pleased to inform you that the delivery of your order to ~facility.name~ has been completed.
Please feel free to contact us if we can be of any further assistance.
Sincerely,
~csr.fname~
Contact us:
Email us via this link: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Confirmation of Delivery',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'N',
'N',
'DCON'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ProFlowers', null, 'ProFlowers' from clean.stock_email where TITLE = 'DCONSYS - ProFlowers';



--*************************************************
-- COM Customer Inquiry Response Emails
--*************************************************
--cx.after.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.after.ProFlowers',
'Thank you for contacting ProFlowers.  

We received your recent request to cancel your order.  Unfortunately, your order was already in the process of being delivered.  

We are so sorry that we were unable to accommodate your request.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'cx.after.ProFlowers';


--cx.autocancel.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.autocancel.ProFlowers',
'Thank you for contacting ProFlowers.  

As you requested, your order has been cancelled. We are in the process of refunding your
~payment.method.text~.  You will be notified by email once that is completed.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'cx.autocancel.ProFlowers';


--cx.autocancelrefund.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.autocancelrefund.ProFlowers',
'Thank you for contacting ProFlowers.

As you requested, your order has been canceled and your ~payment.method.text~ has been refunded. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'cx.autocancelrefund.ProFlowers';


--cx.cancelled.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.cancelled.ProFlowers',
'Thank you for contacting ProFlowers.  

This email confirms that your order has been cancelled.  We look forward to assisting you with your floral and gift giving needs in the future.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'cx.cancelled.ProFlowers';


--cx.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'cx.queue.ProFlowers',
'Thank you for contacting ProFlowers.  

We have received your request to cancel your ProFlowers order. We will cancel your order if it is not already in the process of being designed or delivered.  A customer service agent will contact you with the status of your cancellation request once the investigation is complete.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Cancel Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'cx.queue.ProFlowers';


--default.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'default.queue.ProFlowers',
'Thank you for contacting ProFlowers.  

Our goal is to answer your email within 24 hours. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'default.queue.ProFlowers';


--di.after.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.after.ProFlowers',
'Thank you for you inquiry regarding your recent purchase from ProFlowers.

We are pleased to inform you that we have received your order and it has been processed for delivery on ~auto.delivery.date~. When we receive confirmation of delivery we will contact you.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null,
'ProFlowers' from clean.stock_email where TITLE = 'di.after.ProFlowers';


--di.after.holiday.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
	AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.after.holiday.ProFlowers',
'Thank you for your recent request regarding the delivery of your order.

We are working to confirm the delivery and we will contact you upon completion.  Our goal is to provide a response within 48 hours.  Please note that due to holiday volumes, our investigation may require slightly more time than usual.  We appreciate your patience.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'di.after.holiday.ProFlowers';


--di.before.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.before.ProFlowers',
'Thank you for your recent request regarding the delivery of your order. 

We''ve confirmed that you order is scheduled to be delivered ~auto.delivery.date~. This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'di.before.ProFlowers';


--di.inquiry.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.inquiry.ProFlowers',
'Thank you for your recent request regarding the delivery of your order.  

Our Customer Service Department is currently reviewing your inquiry and we will contact you with further information.  Our goal is to have confirmation of delivery to you within 24 hours after the delivery date.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'di.inquiry.ProFlowers';


--di.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.queue.ProFlowers',
'Thank you for your recent request regarding the delivery of your order.

We are processing your request and will contact you upon completion. Our goal is to have a response to you within 24 hours.

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with ProFlowers.

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'di.queue.ProFlowers';


--di.track.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'di.track.ProFlowers',
'Thank you for your recent request regarding the delivery of your order.  

We have confirmed that your order is scheduled to be delivered ~auto.delivery.date~ via ~auto.shipper~.  Your tracking information is as follows:

Tracking Number:  ~auto.track.num~
Click here to track:  ~auto.track.url~

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with ProFlowers.

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Delivery Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'di.track.ProFlowers';


--mo.after.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.after.ProFlowers',
'Thank you for contacting us regarding your order.  

We received your request to make a change to your order.  We found that your order was already in the process of being delivered and could not be changed.  We''re sorry that we are unable to honor your request.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'mo.after.ProFlowers';


--mo.changed.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.changed.ProFlowers',
'Thank you for your recent request to modify your order.  

The following changes have been made to your order:

~auto.order.change~

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with ProFlowers.

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'mo.changed.ProFlowers';


--mo.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'mo.queue.ProFlowers',
'Thank you for contacting us regarding your recent order.   

We received your request to make a change to your order.  We are processing your request and will contact you upon completion.  

This is an automated response.  We will not receive replies to this email. 

Thank you for shopping with ProFlowers.

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Modify Order Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'mo.queue.ProFlowers';


--nd.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'nd.queue.ProFlowers',
'Thank you for contacting us regarding your recent order.  

We are sorry to hear that there are questions surrounding your delivery.  We will investigate this immediately and get back to you with the details.  Our goal is to respond to you within 24 hours.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Delivery Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'nd.queue.ProFlowers';


--nd.queue.holiday.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'nd.queue.holiday.ProFlowers',
'Thank you for contacting us regarding your recent order.  

We are sorry to hear that there are questions regarding the delivery of your order.  We will begin investigating immediately to get the details of the delivery.  Our goal is to respond to you within 48 hours.  However, due to holiday volumes, our investigation may require slightly more time than usual. We appreciate your patience.

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Delivery Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'nd.queue.holiday.ProFlowers';


--oa.found.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'oa.found.ProFlowers',
'Thank you for your recent inquiry regarding your order.  

Our records indicate that your order was successfully received and processed for delivery as requested.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Order Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'oa.found.ProFlowers';


--oa.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'oa.queue.ProFlowers',
'Thank you for your inquiry regarding your recent order. 

We are in the process of reviewing your request and we will contact you with a confirmation.  

This is an automated response.  We will not receive replies to this email.    

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'oa.queue.ProFlowers';


--op.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'op.queue.ProFlowers',
'Thank you for your inquiry regarding your recent order.  

We are actively working toward an answer for you and will contact you upon completion.  Our goal is to respond to your email within 24 hours.  

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'op.queue.ProFlowers';


--qc.co.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'qc.co.queue.ProFlowers',
'Thank you for the feedback.    

Once reviewed, we will pass your comments to the appropriate staff member or department.  If there are questions, we will contact you at the email address provided. 

This is an automated response.  We will not receive replies to this email. 

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Comment',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'qc.co.queue.ProFlowers';


--qc.queue.ProFlowers
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES( 
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'qc.queue.ProFlowers',
'Thank you for contacting us.  

We are in the process of reviewing your email and will contact you with a response upon completion of our review.  Our goal is to respond within 24 hours.

This is an automated response.  We will not receive replies to this email.  

Sincerely,

ProFlowers Customer Service

Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~  Question',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'qc.queue.ProFlowers';

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-13  (Rizvi )     (143939)              --------------------------------
------------------------------------------------------------------------------------
