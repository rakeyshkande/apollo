------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

-- production value, and any environment refreshed from production
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 110241
where company_id = 'ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

-- MERCURY_EAPI 
-- note this is production only. There is no corresponding QA insert.

INSERT INTO MERCURY.MERCURY_EAPI_OPS ( MAIN_MEMBER_CODE, IS_AVAILABLE, IN_USE, CREATED_BY, CREATED_ON, 
UPDATED_BY, UPDATED_ON ) VALUES ( '90-8401AA', 'Y', 'N', 'FE-7', sysdate, 'FE-7', sysdate );

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

