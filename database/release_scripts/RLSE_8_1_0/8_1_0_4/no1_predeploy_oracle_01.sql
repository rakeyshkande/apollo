

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk    ,                      5/15/2015  --------
-- defect #FE-32   (Syed)     (149535)              --------------------------------
------------------------------------------------------------------------------------


-- Content_Details


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
  CONTENT_MASTER_ID,
  FILTER_1_VALUE,
  FILTER_2_VALUE,
  CONTENT_TXT,
  UPDATED_BY,
  UPDATED_ON,
  CREATED_BY,
  CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
  (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
      WHERE CONTENT_CONTEXT = 'EMAIL_REQUEST_PROCESSING' AND CONTENT_NAME = 'COMPANY_ID'),
  'ProFlowers',
  null,
  'ProFlowers',
  'FE-32',
  sysdate,
  'FE-32',
  sysdate)



-- Global_Param


insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_PEND_REMOVE_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers Scrub Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_CUSTOMER_SERVICE_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers CS Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_AUTO_REPLY_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers Auto-Reply Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'EXACT_TARGET_SEPARATE_COMPANIES',
    'ProFlowers',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Space delimited list of companies that require separate processing via Exact Target'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'BOUNCE_TEMPLATE_ID_LIST_ProFlowers',
    'TEMPLATE_ID_PEND_REMOVE_ProFlowers TEMPLATE_ID_AUTO_REPLY_ProFlowers TEMPLATE_ID_CUSTOMER_SERVICE_ProFlowers',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' List of Template IDS for which ProFlowers email bounce emails are processed');



SET serveroutput on;

DECLARE

v_db_name    VARCHAR2(9); 
v_key_name   VARCHAR2(255);

BEGIN


    SELECT name INTO v_db_name FROM v$database;

    IF v_db_name = 'ZEUS' THEN
		v_key_name      := 'APOLLO_PROD_2014';
    ELSE    
		v_key_name      := 'APOLLO_TEST_2010';
    END IF;

	INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON, DESCRIPTION, KEY_NAME) VALUES
	('SERVICE', 'EXACT_TARGET_WS_USERNAME_ProFlowers', global.encryption.encrypt_it('REPLACE ME', v_key_name),
         sysdate, 'FE-32', 'FE-32', sysdate, 'Username for ProFlowers ExactTarget Email Web Service', v_key_name);

        INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON, DESCRIPTION, KEY_NAME) VALUES
        ('SERVICE', 'EXACT_TARGET_WS_PASSWORD_ProFlowers', global.encryption.encrypt_it('REPLACE ME', v_key_name),
         sysdate, 'FE-32', 'FE-32', sysdate, 'Password for ProFlowers ExactTarget Email Web Service', v_key_name);
	
EXCEPTION

	WHEN OTHERS THEN

	DBMS_OUTPUT.PUT_LINE(substr(sqlerrm,1,100));

END;



commit;


------------------------------------------------------------------------------------
-- end requested by Rose Lazuk    ,                      5/15/2015  --------
-- defect #FE-32   (Syed)     (149535)              --------------------------------
------------------------------------------------------------------------------------

