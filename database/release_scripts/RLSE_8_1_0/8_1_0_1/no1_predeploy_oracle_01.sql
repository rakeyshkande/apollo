------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore ,                      3 /23/2015  --------
-- defect #FE-6   (Pavan)     (140715)              --------------------------------
------------------------------------------------------------------------------------
----PRO
Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,
CONF_NUMBER_PREFIX,PARTNER_IMAGE, DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,
BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE, BILLING_POSTAL_CODE,BILLING_COUNTRY,
SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL, CREATED_ON,CREATED_BY,
UPDATED_ON,UPDATED_BY,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL)
 values 
('PRO','ProFlowers','PRO','PRO','PRO','partner_proflowers.png','36030','36030','3113 Woodcreek Drive',
' ','Downers Grove','IL','60515','US','Y', 'Y','Y',sysdate,'FE-6',sysdate, 'FE-6','Y','Y','N');

Insert into ftd_apps.origins (ORIGIN_ID,DESCRIPTION,ORIGIN_TYPE,ACTIVE,PARTNER_REPORT_FLAG,SEND_FLORIST_PDB_PRICE,order_calc_type) 
values ('PRO', 'ProFlowers Orders','internet','Y','Y','Y','PI');

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore ,                      3 /23/2015  --------
-- defect #FE-6   (Pavan)       (140715)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               3 /24/2015  --------
-- defect #FE-5   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

--*************************************************
-- Partner Master
--*************************************************
insert into FTD_APPS.PARTNER_MASTER (
    PARTNER_NAME,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    FILE_SEQUENCE_PREFIX,
    PREFERRED_PROCESSING_RESOURCE,
    PREFERRED_PARTNER_FLAG,
    REPLY_EMAIL_ADDRESS,
    DEFAULT_PHONE_SOURCE_CODE,
    DEFAULT_WEB_SOURCE_CODE,
    BIN_PROCESSING_FLAG,
    FLORIST_RESEND_ALLOWED_FLAG,
    DISPLAY_NAME)
values (
    'ProFlowers',
    SYSDATE, 'Defect_FE-5', SYSDATE, 'Defect_FE-5',
    null,
    'ProFlowers',
    'Y',
    'procustserv@ftd.com',
    '36030',
    '36030',
    'N',
    'Y',
    'ProFlowers');
insert into FTD_APPS.PARTNER_PROGRAM (
    PROGRAM_NAME,
    PARTNER_NAME,
    PROGRAM_TYPE,
    EMAIL_EXCLUDE_FLAG,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    LAST_POST_DATE,
    PROGRAM_LONG_NAME,
    MEMBERSHIP_DATA_REQUIRED)
values (
    'ProFlowers',
    'ProFlowers',
    'Default',
    'Y',
    SYSDATE, 'Defect_FE-5', SYSDATE, 'Defect_FE-5',
    null,
    null,
    'N');
insert into FTD_APPS.PROGRAM_REWARD (
    PROGRAM_NAME,
    CALCULATION_BASIS,
    POINTS,
    REWARD_TYPE,
    CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,
    CUSTOMER_INFO,
    REWARD_NAME,
    PARTICIPANT_ID_LENGTH_CHECK,
    PARTICIPANT_EMAIL_CHECK,
    MAXIMUM_POINTS,
    BONUS_CALCULATION_BASIS,
    BONUS_POINTS,
    BONUS_SEPARATE_DATA)
values (
    'ProFlowers',
    'M',
    '0',
    'Points',
    sysdate, 'Defect_FE-5', sysdate, 'Defect_FE-5',
    null, null, null, null, null, null, null, null);


--*************************************************
-- Preferred Partner Resource
--*************************************************
insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('ProFlowers','Order Proc','Controls access to view/update ProFlowers order and customer information', sysdate, sysdate, 
'Defect_FE-5');
insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('ProFlowers CSR', sysdate, sysdate, 'Defect_FE-5', null);
insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('ProFlowers CSR','ProFlowers','Order Proc','View',sysdate,sysdate,'Defect_FE-5');
insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'ProFlowers CSR' , 'Order Proc', 'ProFlowers CSR View', sysdate, sysdate, 'Defect_FE-5' );
insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'ProFlowers CSR'),
    'ProFlowers CSR', sysdate, sysdate, 'Defect_FE-5');



--*************************************************
-- Global Parms
--*************************************************
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('MERCURY_INTERFACE_CONFIG', 'OUTBOUND_ID_LIST_90-8401', 'AA', 'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate,
     'List of available suffixes for ProFlowers floral messaging, separated by a space.');
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('MERCURY_INTERFACE_CONFIG', 'DELIVERY_CONFIRMATION_SUFFIX_LIST_90-8401', 'AA', 'Defect_FE-5', sysdate, 'Defect_FE-5', 
sysdate,
'List of suffixes for ProFlowers floral messaging used for lifecycle delivery confirmation status retrieval, separated by a space.'
);
insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values
('VALIDATION_SERVICE', 'ProFlowers_MAX_ORDER_TOTAL', '4000.00', 'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE,
'Maximum allowable ProFlowers shopping cart limit before placing entire cart into FRAUD');
insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values
('VALIDATION_SERVICE', 'ProFlowers_MAX_ITEM_TOTAL', '2000.00', 'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE,
'Maximum allowable ProFlowers single order limit before placing into FRAUD');
insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values
('VALIDATION_SERVICE', 'ProFlowers_MAX_ITEM_ADDON_TOTAL', '500.00', 'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE,
'Maximum allowable ProFlowers addon limit before placing into FRAUD');
insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values
('ORDER_PROCESSING', 'ProFlowers_LP_INDICATOR_TIMEFRAME', '6', 'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE,
'Timeframe in HOURS where allowed number of ProFlowers orders specified in LP_INDICATOR_THRESHOLD before placing subsequent orders into FRAUD');
insert into FRP.GLOBAL_PARMS
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values
('ORDER_PROCESSING', 'ProFlowers_LP_INDICATOR_THRESHOLD', '10', 'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE,
'Number of ProFlowers orders allowable in LP_INDICATOR_TIMEFRAME before placing subsequent orders into FRAUD');


--*************************************************
-- Secure Config
--*************************************************
INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8401AA_USER_NAME', 
global.encryption.encrypt_it('fixme',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
'User Id for 90-8401AA', sysdate, 'Defect_FE-5', sysdate, 'Defect_FE-5', 
frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));
INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8401AA_PASSWORD', 
global.encryption.encrypt_it('fixme',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
'Password for 90-8401AA', sysdate, 'Defect_FE-5', sysdate, 'Defect_FE-5', 
frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));

--*************************************************
-- Content Detail
--*************************************************
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'COM_SOURCE_CODE_RESTRICTION'),
    'ProFlowers',
    null,
    'Stop: You are attempting to change an order to a ProFlowers source code.  Please advise the customer:<br><br>''I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you.''<br><br>Transfer the call to XXXX',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'SOURCE_CODE_RESTRICTION'),
    'ProFlowers',
    null,
    'Stop: You are attempting to change an order to a ProFlowers source code.  Please advise the customer: "I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you." Transfer the call to XXXX',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'ORDER_ACCESS_RESTRICTION'),
    'ProFlowers',
    null,
    'Stop: This is a ProFlowers order. Please advise the customer: <br/><br/>"I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you." <br/><br/>Transfer the call to XXXX',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'PHONE'),
    'ProFlowers',
    null,
    '1-877-383-6147',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'STOCK_EMAIL' AND CONTENT_NAME = 'TOKEN'),
    'TEXT',
    'ProFlowers_EMAIL_LINK',
    'http://www.proflowers.com/~sourcecode~/custserv/',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'STOCK_EMAIL' AND CONTENT_NAME = 'TOKEN'),
    'HTML',
    'ProFlowers_EMAIL_LINK',
    '<a href="www.proflowers.com/~sourcecode~/custserv">http://www.proflowers.com/~sourcecode~/custserv/">www.proflowers.com/~sourcecode~/custserv</a>',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALL_CENTER_CONTACT_NUMBER'),
    'ProFlowers',
    null,
    '1-877-383-6147',
    'Defect_FE-5', sysdate, 'Defect_FE-5', sysdate);
   
insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
    ftd_apps.content_detail_sq.nextval,
    (select content_master_id from ftd_apps.content_master
        where CONTENT_NAME = 'TRANSFER_EXTENSION'),
    'ProFlowers',
    null,
    'XXXX',
    'Defect_FE-5', SYSDATE, 'Defect_FE-5', SYSDATE);

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               3 /24/2015  --------
-- defect #FE-5   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
-- further updated 3/30 as requested by Rose        --------------------------------
-- further updated 4/29, SDR 146248                 --------------------------------
------------------------------------------------------------------------------------

insert into ftd_apps.company ( COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE)
values ( 'ProFlowers', 'ProFlowers', 5941, '36030');

insert into FTD_APPS.COMPANY_MASTER (COMPANY_ID, COMPANY_NAME, INTERNET_ORIGIN, DEFAULT_PROGRAM_ID,
                                     PHONE_NUMBER, CLEARING_MEMBER_NUMBER, LOGO_FILENAME, 
                                     ENABLE_LP_PROCESSING, URL, SORT_ORDER, EMAIL_DIRECT_MAIL_FLAG, 
                                     ORDER_FLAG, REPORT_FLAG, SDS_BRAND, DEFAULT_DOMAIN, 
                                     NEWSLETTER_COMPANY_ID, SENDING_FLORIST_NUMBER, DEFAULT_DNIS)
                             values ('ProFlowers', 'ProFlowers', 'PRO', 100341,
                                    '877-383-6147', '90-8401', 'ProFlowers_Logo.jpg', 'N',
                                    'ProFlowers.com', 80, 'N', 'Y' ,'Y',
                                    'PF', 'proflowers', 'FTD', '90-8401', null);
-- =================================================================================
-- ENVIRONMENT-SPECIFIC
-- ---------------------------------------------------------------------------------
-- borgt
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 110441
where company_id = 'ProFlowers';
-- borq1
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 100201
where company_id = 'ProFlowers';
-- production, and any environment like borgu1 refreshed from production
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 110241
where company_id = 'ProFlowers';
-- =================================================================================
-- END OF ENVIRONMENT-SPECIFIC
-- ---------------------------------------------------------------------------------

insert into ftd_apps.script_master (script_master_id, script_description)
    values ('ProFlowers', 'ProFlowers Default script');

insert into ftd_apps.dnis_script_code (script_code, description)
    values ('PF', 'PF');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (5941, 'ProFlowers', 'PRO', 'A', 'Y', 'N', 'PF', '36030', 'ProFlowers', 'ProFlowers', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (5942, 'ProFlowers', 'PRO', 'A', 'Y', 'N', 'PF', '36030', 'ProFlowers', 'ProFlowers', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (5943, 'ProFlowers', 'PRO', 'A', 'Y', 'N', 'PF', '36030', 'ProFlowers', 'ProFlowers', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (5944, 'ProFlowers', 'PRO', 'A', 'Y', 'N', 'PF', '36030', 'ProFlowers', 'ProFlowers', 'Default');

update ftd_apps.company_master
set default_dnis = 5941
where company_id = 'ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Meka Srivatsala ,                               3/27/2015  --------
-- defect #FE-7   (Pavan)                             --------------------------------
------------------------------------------------------------------------------------

INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('AK', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('WY', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('AZ', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('CO', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('DC', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('DE', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('HI', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('IA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('ID', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('IN', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('KS', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('KY', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('LA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MD', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('ME', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MI', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MN', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MO', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MS', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MT', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NC', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NE', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NH', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NM', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NV', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('OH', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('OK', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('OR', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('PA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('PR', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('RI', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('SC', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('SD', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('TN', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('UT', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('VA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('VI', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('VT', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('WA', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('WI', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('WV', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('AL', 'PRO', 'N', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('AB', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('AR', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('BC', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('CA', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('CT', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('FL', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('GA', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('IL', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('MB', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NB', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('ND', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NL', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NJ', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NS', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NT', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('NY', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('ON', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('PE', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('PQ', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('SK', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('TX', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');
INSERT INTO TAX.STATE_COMPANY_TAX_CONTROLS (STATE_MASTER_ID, COMPANY_ID, IS_TAX_ON, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES ('YT', 'PRO', 'Y', SYSDATE, 'DEFECT_3171', SYSDATE, 'DEFECT_3171');


------------------------------------------------------------------------------------
-- end   requested by Meka Srivatsala ,                               3/27/2015  --------
-- defect #FE-7   (Pavan)                            --------------------------------
------------------------------------------------------------------------------------




