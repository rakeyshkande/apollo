 
--------------------------------------------------------------------------
-- begin requested by Salguti Raghu Ram   ,             4/22/2015  ----------
-- defect #FE-22   (Pavan)                            --------------------
--------------------------------------------------------------------------
 
grant select  on ptn_pi.partner_order  to scrub ;
grant select  on ptn_pi.partner_order  to clean ;
grant select  on ptn_pi.partner_order_detail   to clean ;
--------------------------------------------------------------------------
-- end   requested by Salguti Raghu Ram   ,             4/22/2015  --------
-- defect #FE-22   (Pavan)                           ---------------------
--------------------------------------------------------------------------
