------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                       4 /10/2015  --------
-- defect FE-8  (Mark  )                            --------------------------------
------------------------------------------------------------------------------------

use ptn_pi;
alter table ptn_pi.partner_mapping add column AVS_FLORAL     varchar(1) default null;
update ptn_pi.partner_mapping set AVS_FLORAL='N';
alter table ptn_pi.partner_mapping add column AVS_DROPSHIP   varchar(1) default null;
update ptn_pi.partner_mapping set AVS_DROPSHIP='N';
alter table ptn_pi.partner_mapping add column SEND_DCON_FEED varchar(1) default null;
update ptn_pi.partner_mapping set SEND_DCON_FEED='N';
commit;

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                       4 /10/2015  --------
-- defect FE-8  (Mark  )                            --------------------------------
------------------------------------------------------------------------------------
