------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-12  (Syed )     (143941)              --------------------------------
------------------------------------------------------------------------------------

--Change Preferred Partner From Email Address
update ftd_apps.partner_master
set reply_email_address = 'customercare@fe.proflowers.com' , updated_on = sysdate , updated_by = 'FE-12'
where partner_name = 'ProFlowers';

--Scrub Email - Add new email company data for ProFlowers
insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'Phone', '877-383-6147');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'Email', 'customercare@fe.proflowers.com');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'Website', 'http://www.proflowers.com');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'CustServHours', 'We are here to assist you 24 hours a day, 7 days a week.');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'FromAddress', 'customercare@fe.proflowers.com');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'ProFlowers', 'NoReplyFromAddress', 'no-reply@fe.proflowers.com');


--Add New Scrub Stock Emails
insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_BLNK',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">YOUR<xsl:text> </xsl:text><xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">YOUR<xsl:text> </xsl:text><xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial\n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  <xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n \n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact us:\nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
100,
1,
'ALL',
null,
'ProFlowers');
 
 
insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_CC',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CREDIT CARD AUTHORIZATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CREDIT CARD AUTHORIZATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nThe credit card you selected for payment would not authorize.  Please contact\nus to confirm that we have the correct information.\n \nDue to the time sensitive nature of your order, a prompt response from you will\nguarantee timely delivery of your gift.  Please e-mail us by clicking on this\nlink <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.\n<xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n \nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely, \n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n \n \nContact us:  \nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
200,
1,
'ALL',
null,
'ProFlowers');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_CCAN',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">ORDER CANCEL CONFIRMATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">ORDER CANCEL CONFIRMATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nAt your request, your <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>order has been canceled and a refund in\nthe amount of <xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/><xsl:text> </xsl:text>has been posted to your original payment method.  We hope    \nyou will give us the opportunity to assist you with your gift giving in\nthe future.\n \nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial\n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  <xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>. \n \n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely, \n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n \n\nContact us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
300,
1,
'ALL',
null,
'ProFlowers');
 
insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_CLP',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">UNABLE TO PROCESS YOUR ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">UNABLE TO PROCESS YOUR ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/>.  Unfortunately, we are unable \nto process your order at this time.  Your order has been canceled and a full\nrefund in the amount of <xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/><xsl:text> </xsl:text>has been posted to your original payment method.  We\nsincerely apologize for any inconvenience this may cause.\n\nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial\n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  We are here to assist you 24 hours a day, 7 \ndays a week.\n\nThank you for choosing <xsl:value-of select="header/company_name"/>.  \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/> \n\n\nContact us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
400,
1,
'ALL',
null,
 'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_FSND',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">YOUR <xsl:text> </xsl:text><xsl:value-of select="header/company_name"/><xsl:text> </xsl:text> FUNERAL ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">YOUR<xsl:text> </xsl:text><xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>FUNERAL ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/>.  Despite our best efforts, the\ndelivery could not be made as requested due to time constraints of the funeral\nservice.  We apologize for the notification of this inconvenience via e-mail,\nbut we were unable to reach you at the phone number you provided when placing\nyour order.  A full refund in the amount of <xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/><xsl:text> </xsl:text>has been posted to your \noriginal payment method.\n\nWe would be happy to reschedule a delivery to the home of the deceased''s family\nfor you.  We will provide you with a 10% discount on this order for any \ninconvenience we may have caused.\n\nPlease contact us at <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> <xsl:text> </xsl:text> to place your new order\nwith this discount.  <xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you again for your understanding and for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\n</xsl:template></xsl:stylesheet>',
500,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_IRIN',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">INTERNET ORDER MISSING INFO</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">INTERNET ORDER MISSING INFO</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n \nThank you for your recent purchase from <xsl:value-of select="header/company_name"/>.  Unfortunately, your order\nis missing some vital delivery information and was not processed.  You will\nneed to place another order with complete recipient information, including\nfull name, address (add apartment or suite numbers), and phone number.  To\nplace a new order, go to <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>.\n \nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial \n<xsl:text> </xsl:text><xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  We are here to assist you 24 hours a day, 7  \ndays a week.\n \nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely, \n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n \n\nContact us:\nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
600,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_MRI',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MISSING RECIPIENT INFORMATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MISSING RECIPIENT INFORMATION</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/>.  Your order is being \nprocessed by the florist that will be creating and delivering your floral\narrangement.  However, in order for the florist to deliver your gift we  \nneed some more information.  \n\nPlease contact us with your recipient''s full name, address (including\napartment or suite number), zip code and phone number.  To speak with a  \nCustomer Service Representative, please email us by clicking on this link \n<xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.\n<xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>.  \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nEmail:   <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> \nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
700,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_MRPH',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MISSING RECIPIENT PHONE NUMBER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MISSING RECIPIENT PHONE NUMBER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.    \nYour order is being processed by the florist that will be creating and  \ndelivering your floral arrangement.  However, in order for the florist to\ndeliver your gift we need some more information.  \n\nPlease contact us with your recipient''s phone number.  To speak with a     \nCustomer Service Representative, please email us by clicking on this link   \n<xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> <xsl:text> </xsl:text>. \n<xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>. \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>      \n</xsl:template></xsl:stylesheet>',
800,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_NOFF',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO FLORIST AVAILABLE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO FLORIST AVAILABLE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nUnfortunately, there is no filling florist available in your recipient''s \ndelivery area.  We apologize for the inconvenience this may cause.  A full   \nrefund in the amount of <xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/><xsl:text> </xsl:text>has been issued to your original payment method. \n\nAs an alternative, may we suggest choosing an item that we offer via a shipping\ncourier.  Visit us online at <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/> <xsl:text> </xsl:text> to select from our expanded \ngift line.\n\nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial \n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> <xsl:text> </xsl:text><xsl:value-of select="company_data_list/company_data[data_name=''PhoneAlt'']/data"/>.  We are here to assist you 24 hours a day, 7  \ndays a week.\n\nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> \nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/> \n</xsl:template></xsl:stylesheet>',
900,
1,
'ALL',
null,
'ProFlowers');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_NOIF',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO INT''L FLORIST AVAILABLE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">NO INT''L FLORIST AVAILABLE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>. \nUnfortunately, there is no filling florist available in your recipient''s \ndelivery area.  We apologize for the inconvenience this may cause.  A full  \nrefund in the amount of <xsl:value-of select="format-number(header/order_amount, ''#,###,##0.00'')"/><xsl:text> </xsl:text>has been issued to your original payment method.\n\nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial       \n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  We are here to assist you 24 hours a day, 7 \ndays a week.   \n\nThank you for choosing <xsl:value-of select="header/company_name"/>.  \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:   <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> \nShop:    <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>  \n</xsl:template></xsl:stylesheet>',
900,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_PEND',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CANNOT PROCESS YOUR ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">CANNOT PROCESS YOUR ORDER</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/>.  We have attempted to \ncontact you through phone or email.  As this order has been pending for 48 hours \nwithout contact from you, we cannot move forward to process it.  We have \ncanceled your order and your original payment method has not been charged. We apologize for any \ninconvenience this may cause.\n\nIf you would like to speak with a Customer Service Representative, please email\nus by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.\n<xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>.  \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/> \n\n\nContact us:\nE-mail:  <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
1000,
1,
'ALL',
null,
'ProFlowers');


insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_RDD',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">RESCHEDULE DELIVERY DATE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">RESCHEDULE DELIVERY DATE</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nUnfortunately, we are unable to meet your requested delivery date of \n<xsl:value-of select="items/item/delivery_date"/>, and we would like to reschedule delivery as soon as possible.  We\napologize for notification of this inconvenience via e-mail, but we were unable\nto reach you at the phone number you provided when placing your order.\n\nPlease contact us to arrange a new delivery date.  To contact a Customer \nService Representative, send us an e-mail by clicking on this link\n<xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/> <xsl:text> </xsl:text>.  We\nare here to assist you 24 hours a day, 7 days a week.\n\nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n \n\nContact us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
1100,
1,
'ALL',
null,
'ProFlowers');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_SCN',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SECOND CHOICE NEEDED</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SECOND CHOICE NEEDED</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nUnfortunately, the item you ordered is unavailable in your recipient''s \ndelivery area.  Please contact us so that we can assist you in selecting \nanother item.\n\nTo contact a Customer Service Representative, please email us by clicking on \nthis link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.\n<xsl:text> </xsl:text>  <xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>. \n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact Us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
1200,
1,
'ALL',
null,
'ProFlowers');

insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) 
values (
'PRO_SR',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SIGNATURE REQUEST</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">SIGNATURE REQUEST</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from <xsl:value-of select="header/company_name"/><xsl:text> </xsl:text>for <xsl:value-of select="items/item/recip_first_name"/><xsl:text> </xsl:text><xsl:value-of select="items/item/recip_last_name"/>.\nWe noticed that your gift card message did not include a signature from you,\nand we would like to give you this opportunity to add it in case this was an \noversight.\n\nPlease contact us with your permission to sign your name to the gift card.  To\ncontact a Customer Service Representative, please email us by clicking on this\nlink <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.\n<xsl:value-of select="company_data_list/company_data[data_name=''CustServHours'']/data"/>  \n\nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\n~language.message~ <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n \n\nContact us:\nE-mail:   <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/> \nPhone:    <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:     <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>',
1300,
1,
'ALL',
null,
'ProFlowers');


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                               4 / 15/2015  --------
-- defect #FE-12  (Syed )     (143941)              --------------------------------
------------------------------------------------------------------------------------
