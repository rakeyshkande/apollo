------------------------------------------------------------------------------------
-- begin requested by Salguti  Raghu Ram    ,                      3 /31/2015  --------
-- defect #FE-4   (Pavan)     (141668)              --------------------------------
------------------------------------------------------------------------------------
-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add AVS_FLORAL char(1) default 'N';
alter table ptn_pi.partner_mapping add AVS_DROPSHIP char(1) default 'N';

update PTN_PI.PARTNER_MAPPING set AVS_FLORAL = 'Y', avs_dropship = 'N' where partner_id = 'PRO';
update PTN_PI.partner_mapping set avs_dropship = 'Y', avs_floral = 'N' where partner_id <> 'PRO';

------------------------------------------------------------------------------------
-- end   requested by Salguti  Raghu Ram    ,                      3 /31/2015  --------
-- defect #FE-4   (Pavan)       (141668)            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               4 / 1/2015  --------
-- defect #FE-15  (Mark )     (142114)              --------------------------------
------------------------------------------------------------------------------------

-- Add ProFlowers to Excluded Preferred Partners for Phoenix
update frp.global_parms
set value = 'SCI SYMPATHY ProFlowers', updated_on = sysdate, updated_by = 'FE-15'
where context = 'PHOENIX'
and name = 'EXCLUDED_PREFERRED_PARTNERS';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               4 / 1/2015  --------
-- defect #FE-15  (Mark )     (142114)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               4 / 6/2015  --------
-- defect FE-20  (Mark )     (142556)              --------------------------------
------------------------------------------------------------------------------------

insert into ftd_apps.company (COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE
) values (
'PROFLWSP', 'ProFlowers', 5941, '36030');

insert into ftd_apps.origins (ORIGIN_ID, DESCRIPTION, ORIGIN_TYPE, ACTIVE, PARTNER_REPORT_FLAG, SEND_FLORIST_PDB_PRICE, ORDER_CALC_TYPE
) values (
'PROFLWSP', 'ProFlowers Phone Orders', 'phone', 'N', 'N', 'Y', 'GENERIC');

update ftd_apps.dnis
    set origin = 'PROFLWSP', script_code = 'DG'
    where origin = 'ProFlowers';

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (8889, 'ProFlowers (Replacement Order Default)', 'PROFLWSP', 'A', 'Y', 'N', 'DG', '36031', 'ProFlowers', 'ProFlowers', 'Default');

delete from ftd_apps.company
    where company_id = 'ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               4 / 6/2015  --------
-- defect FE-20  (Mark )                             --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /10/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add SEND_DCON_FEED varchar(1) default 'N' not null; 
alter table PTN_PI.PARTNER_ORDER_FULFILLMENT add DELIVERY_STATUS varchar(20);

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /10/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /13/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

update PTN_PI.PARTNER_MAPPING set SEND_DCON_FEED='Y'  where partner_id='PRO';
commit;
------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /13/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /15/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

alter table PTN_PI.PARTNER_ORDER_FULFILLMENT add DELIVERY_STATUS_DATETIME DATE;

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /15/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore,                               4 /22/2015  --------
-- defect FE-8  (Pavan )               --------------------------------
------------------------------------------------------------------------------------

Insert into quartz_schema.PIPELINE (PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,
DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG) values
 ('PROCESS_SEND_PARTNER_DCON_FEEDS','PARTNER_INTEGRATION','Post Partner Order DCON Feeds',
 'Process to get partner order delivery confirmation feeds and post it to ESB','300',
 'PROCESS_SEND_PARTNER_DCON_FEEDS','Y','OJMS.PARTNER_ORDERS','SimpleJmsJob','Y');
 ------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore,                               4 /22/2015  --------
-- defect FE-8  (Pavan )                            --------------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- begin requested by Salguti  Raghu Ram    ,                      4/22/2015  --------
-- defect #FE-22   (Pavan)     (144961)              --------------------------------
------------------------------------------------------------------------------------
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, 
CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) values ('GLOBAL_CONFIG',
 'PRO_ORDER_SEARCH_APPENDER', '-', 'Defect_3171', sysdate, 'Defect_3171', sysdate, 
'Pro Order number will be appended by this delimiter before search');

commit;
------------------------------------------------------------------------------------
-- end   requested by Salguti  Raghu Ram    ,                      4/22/2015  --------
-- defect #FE-22   (Pavan)       (144961)            --------------------------------
------------------------------------------------------------------------------------
 
