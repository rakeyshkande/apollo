

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk    ,                      4/29/2015  --------
-- defect #FE-14   (Syed)     (145909)              --------------------------------
------------------------------------------------------------------------------------



INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'ProFlowers EXCLUDE DOMAINS', 'walmart.com',
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.',
    sysdate, 'FE-14', sysdate, 'FE-14');


INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'ProFlowers DEFAULT PARTNER NAME', 'ProFlowers',
    'Partner associated with the mailbox.  The default partner source code will be used when populating the CLEAN.POINT_OF_CONTACT.SOURCE_CODE field for unattached emails.',
    sysdate, 'FE-14', sysdate, 'FE-14');


SET serveroutput on;

DECLARE

v_key_value  VARCHAR2(1000);
v_db_name    VARCHAR2(9); 
v_desc_1     VARCHAR2(255);
v_box_value  VARCHAR2(1000);

BEGIN


	SELECT value INTO v_key_value FROM frp.global_parms WHERE context ='Ingrian';


	SELECT name INTO v_db_name FROM v$database;

	IF v_db_name = 'ZEUS' THEN
		v_desc_1      := 'ProFlowers mailbox user name.';
		v_box_value := 'sulfur.ftdi.com';
	ELSE    
		v_desc_1 := 'Login to pull erp emails for ProFlowers.';
		v_box_value := 'sodium.ftdi.com';
	END IF;



	Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
	VALUES ('email_request_processing', 'ProFlowers_mailbox_monitor_USERNAME',global.encryption.encrypt_it('fixme',v_key_value),v_desc_1, 
    		sysdate,'FE-14',sysdate,'FE-14',v_key_value);


	Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
	VALUES ('email_request_processing','ProFlowers_mailbox_monitor_PASSWORD',global.encryption.encrypt_it('fixme', v_key_value),'Password to pull erp emails for ProFlowers.',
    		sysdate,'FE-14',sysdate,'FE-14',v_key_value);


	INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    	values ( 'ins_novator_mailbox_monitor', 'ProFlowers MAIL SERVER', v_box_value,
    		'ProFlowers mail server.',sysdate, 'FE-14', sysdate, 'FE-14');



EXCEPTION

	WHEN OTHERS THEN

	DBMS_OUTPUT.PUT_LINE(substr(sqlerrm,1,100));


END;
/

------------------------------------------------------------------------------------
-- end requested by Rose Lazuk    ,                      4/29/2015  --------
-- defect #FE-14   (Syed)     (145909)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk    ,                      5/15/2015  --------
-- defect #FE-32   (Syed)     (149535)              --------------------------------
------------------------------------------------------------------------------------


-- Content_Details


insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID,
  CONTENT_MASTER_ID,
  FILTER_1_VALUE,
  FILTER_2_VALUE,
  CONTENT_TXT,
  UPDATED_BY,
  UPDATED_ON,
  CREATED_BY,
  CREATED_ON)
values (ftd_apps.content_detail_sq.nextval,
  (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
      WHERE CONTENT_CONTEXT = 'EMAIL_REQUEST_PROCESSING' AND CONTENT_NAME = 'COMPANY_ID'),
  'ProFlowers',
  null,
  'ProFlowers',
  'FE-32',
  sysdate,
  'FE-32',
  sysdate);



-- Global_Param


insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_PEND_REMOVE_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers Scrub Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_CUSTOMER_SERVICE_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers CS Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_AUTO_REPLY_ProFlowers',
    '30',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Template Id for ProFlowers Auto-Reply Emails'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'EXACT_TARGET_SEPARATE_COMPANIES',
    'ProFlowers',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' Space delimited list of companies that require separate processing via Exact Target'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'MESSAGE_GENERATOR_CONFIG',
    'BOUNCE_TEMPLATE_ID_LIST_ProFlowers',
    'TEMPLATE_ID_PEND_REMOVE_ProFlowers TEMPLATE_ID_AUTO_REPLY_ProFlowers TEMPLATE_ID_CUSTOMER_SERVICE_ProFlowers',
    'FE-32',
    sysdate,
    'FE-32',
    sysdate,
    ' List of Template IDS for which ProFlowers email bounce emails are processed');



SET serveroutput on;

DECLARE

v_key_name   VARCHAR2(255);

BEGIN

    select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';

    INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON, DESCRIPTION, KEY_NAME) VALUES
    ('SERVICE', 'EXACT_TARGET_WS_USERNAME_ProFlowers', global.encryption.encrypt_it('REPLACE ME', v_key_name),
    sysdate, 'FE-32', 'FE-32', sysdate, 'Username for ProFlowers ExactTarget Email Web Service', v_key_name);

    INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON, DESCRIPTION, KEY_NAME) VALUES
    ('SERVICE', 'EXACT_TARGET_WS_PASSWORD_ProFlowers', global.encryption.encrypt_it('REPLACE ME', v_key_name),
    sysdate, 'FE-32', 'FE-32', sysdate, 'Password for ProFlowers ExactTarget Email Web Service', v_key_name);
	
end;
/
commit;


------------------------------------------------------------------------------------
-- end requested by Rose Lazuk    ,                      5/15/2015  --------
-- defect #FE-32   (Syed)     (149535)              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey   ,                            5/18/2015  --------
-- defect #FE-78   (Mark)     (150067)              --------------------------------
------------------------------------------------------------------------------------
 
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MESSAGE_GENERATOR_CONFIG', 'EMAIL_PARTNER_ORDER', 'ProFlowers', 
        'FE-78', sysdate, 'FE-78', sysdate, 
        'List of (space-separated) companies that should use the partner order number in email instead of external order number.  Currently only used in some auto-replies.');

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey   ,                            5/18/2015  --------
-- defect #FE-78   (Mark)     (150067)              --------------------------------
------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------
-- begin requested by Tim Schmig    ,                            5/20/2015  --------
-- defect FE-13    (Mark)     (150354)              --------------------------------
------------------------------------------------------------------------------------
 
insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
VALUES(
(clean.STOCK_EMAIL_ID_SQ.nextval ),
'ot.queue.ProFlowers',
'Thank you for your inquiry regarding your recent order. 
We are actively working toward an answer for you and will contact you upon completion.  Our goal is to respond to your email within 24 hours. 
This is an automated response.  We will not receive replies to this email. 
Sincerely,
ProFlowers Customer Service
Online: ~proflowers.csemail~
Phone: ~proflowers.phone~
',
'Re: ~auto.company.name~ Inquiry Request',
SYSDATE,
'FE-13',
SYSDATE,
'FE-13',
'Y',
'Y',
'Normal'
);

--Insert the company reference
insert into clean.stock_email_company_ref(STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)  select stock_email_id, 'ALL', null, 'ProFlowers' from clean.stock_email where TITLE = 'ot.queue.ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig    ,                            5/20/2015  --------
-- defect FE-13    (Mark)     (150354)              --------------------------------
------------------------------------------------------------------------------------
 
