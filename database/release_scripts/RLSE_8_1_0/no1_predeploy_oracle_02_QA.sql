------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

-- use no1_predeploy_oracle_02_PROD.sql for any environment refreshed from production
-- borgt
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 110441
where company_id = 'ProFlowers';
-- borq1
update ftd_apps.company_master
set DEFAULT_PROGRAM_ID = 100201
where company_id = 'ProFlowers';

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               3 /24/2015  --------
-- defect #FE-7   (Mark)                            --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Vasanthala Kishore ,                      3 /23/2015  --------
-- defect #FE-6   (Pavan)     (140715)              --------------------------------
------------------------------------------------------------------------------------
-- PRO1
Insert into PTN_PI.PARTNER_MAPPING (PARTNER_ID,PARTNER_NAME,FTD_ORIGIN_MAPPING,MASTER_ORDER_NO_PREFIX,
CONF_NUMBER_PREFIX,PARTNER_IMAGE, DEFAULT_SOURCE_CODE,DEFAULT_RECALC_SOURCE_CODE,BILLING_ADDRESS_LINE_1,
BILLING_ADDRESS_LINE_2,BILLING_CITY,BILLING_STATE_PROVINCE, BILLING_POSTAL_CODE,BILLING_COUNTRY,
SAT_DELIVERY_ALLOWED_DROPSHIP,SAT_DELIVERY_ALLOWED_FLORAL,SUN_DELIVERY_ALLOWED_FLORAL, CREATED_ON,CREATED_BY,
UPDATED_ON,UPDATED_BY,SEND_ORD_CONF_EMAIL,SEND_SHIP_CONF_EMAIL,CREATE_STOCK_EMAIL) values 
('PRO1','ProFlower1','PRO1','PRO1','PRO1','partner_proflower1.png','61181','61181','3113 Woodcreek Drive',
' ','Downers Grove','IL','60515','US','Y', 'Y','Y',sysdate,'FE-6',sysdate, 'FE-6','Y','Y','N');

Insert into ftd_apps.origins (ORIGIN_ID,DESCRIPTION,ORIGIN_TYPE,ACTIVE,PARTNER_REPORT_FLAG,SEND_FLORIST_PDB_PRICE,order_calc_type) 
values ('PRO1', 'ProFlower1','internet','Y','Y','Y','PI');

------------------------------------------------------------------------------------
-- end   requested by Vasanthala Kishore ,                      3 /23/2015  --------
-- defect #FE-6   (Pavan)       (140715)            --------------------------------
------------------------------------------------------------------------------------

