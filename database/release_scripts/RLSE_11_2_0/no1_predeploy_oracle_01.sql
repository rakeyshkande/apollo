------------------------------------------------------------------------------------
-- Begin requested by Gautam Saluja                             06/05/2018  --------
-- User task CAL-1179   (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

declare
   v_key_name frp.global_parms.value%type;
begin
   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';
INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'SERVICE', 'GIFT_CODE_SERVICE_SECRET', global.encryption.encrypt_it('replaceme', v_key_name),
   'Shared secret for Payment Gateway Service', sysdate, 'CAL-1179', sysdate, 'CAL-1179', v_key_name);
end;
/

------------------------------------------------------------------------------------
-- End   requested by Gautam Saluja                             06/05/2018  --------
-- User task CAL-1179   (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Start requested by Shikha Gupta                   06/26/2018  --------
-- User Story CAL 3132  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GIFT_CODE_SERVICE_TIMEOUT', '10', 'CAL-3132', sysdate, 'CAL-3132', sysdate,'Time out param for gift code microservice');

------------------------------------------------------------------------------------
-- End requested by Shikha Gupta                   06/26/2018  --------
-- User Story CAL 3132  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Self                            07/05/2018  --------
-- User task CAL-3285   (bsurimen)                 ----------------------------------
------------------------------------------------------------------------------------

declare
   v_key_name frp.global_parms.value%type;
begin
   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';
INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'SERVICE', 'PAYMENT_GATEWAY_SERVICE_SECRET', global.encryption.encrypt_it('replaceme', v_key_name),
   'Shared secret for Payment Gateway Service', sysdate, 'CAL-3285', sysdate, 'CAL-3285', v_key_name);
end;
/

------------------------------------------------------------------------------------
-- End requested by Self                  07/05/2018  --------
-- User Story CAL-3285  (bsurimen)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                07/26/2018  --------
-- User Story CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'GCS_TIMEOUT_MAX_RETRIES', '10', 'CAL-1170', sysdate, 'CAL-1170', sysdate, 
'Number of attempts to retry redeeming a Gift Code before sending alert to Java Support');
 
insert into FRP.GLOBAL_PARMS 
(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 
 'SERVICE', 'GCS_TIMEOUT_DELAY', '30000', 'CAL-1170', sysdate, 'CAL-1170', sysdate, 
'Time interval in milliseconds at which time Gift Code Service redemption will be retried.');

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                07/26/2018  --------
-- User Story CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                07/30/2018  --------
-- User Story CAL-3672  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'REFUND_TO_REINSTATE', 'RefundtoReinstate', 'CAL-3672', sysdate, 'CAL-3672', sysdate, 'End of day jobs Type');
            
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'ACTIVE_TO_ISSUEDEXPIRED', 'ActivetoIssuedExpired', 'CAL-3756', sysdate, 'CAL-3756', sysdate, 'End of day jobs Type'); 

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                07/30/2018  --------
-- User Story CAL-3672  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Tim Schmig                                07/31/2018  --------
-- User task  FLTD-5136 (Arman)                   ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PAYMENT_GATEWAY_DETOKENIZATION_URL', 'replace me', 'FLTD-5136', sysdate, 'FLTD-5136', sysdate,'URL for payment microservice');

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                  07/31/2018  --------
-- User task  FLTD-5136  (Arman)                  ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Self                            08/21/2018  --------
-- User task 12399   (smeka)                 ----------------------------------
------------------------------------------------------------------------------------

declare
   v_key_name frp.global_parms.value%type;
begin
   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';
INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'SERVICE', 'PROFILE_SERVICE_SECRET', global.encryption.encrypt_it('replaceme', v_key_name),
   'Shared secret for Profile Service', sysdate, '12399', sysdate, '12399', v_key_name);
end;
/

------------------------------------------------------------------------------------
-- End requested by Self                  08/21/2018  --------
-- User Story 12399  (smeka)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Rose Lazuk                                08/22/2018  --------
-- User task  CAL-1170 (Mark)                     ----------------------------------
------------------------------------------------------------------------------------

update frp.global_parms
set value = '5'
where context = 'SERVICE'
and name = 'GCS_TIMEOUT_DELAY';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                08/22/2018  --------
-- User task  CAL-1170 (Mark)                     ----------------------------------
------------------------------------------------------------------------------------

