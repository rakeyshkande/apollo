------------------------------------------------------------------------------------
-- Begin requested by Surimenu, Bhargava Swamy                     03/04/2018  --------
-- User task  FLTD-5156 (pthakur)         ----------------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_SVC_URL','ReplaceMe','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Payment Gateway Micro Service URL.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_SVC_TIMEOUT','10','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Apollo time out for Payment Gateway Service in seconds.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_SVC_RETRY_LIMIT','3','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Retry limit for the requests from Apollo to Payment Gateway Service.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_SVC_ENABLED','N','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Flag to enable the Payment Gateway Service route.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_SVC_DELAY_TIME_IN_RETRY','10','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Delay in each retry of requests before re-processing.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_ROUTE_ORIGINS','MOBILE,TABLET,WEB','FLTD-5156',sysdate,'FLTD-5156',sysdate,'List of origins that Apollo routes Orders to new Payment Gateway.');
insert into frp.global_parms values('SERVICE','PAYMENT_GATEWAY_ROUTES','PG-PS','FLTD-5156',sysdate,'FLTD-5156',sysdate,'Payment Gateway Routes. Amex Cards and Paypal belongs to PG-PS and BAMS Cards belongs to PG-JCCAS.'); 

------------------------------------------------------------------------------------
-- End requested by Surimenu, Bhargava Swamy                     03/04/2018  --------
-- User task  FLTD-5156 (Pthakur)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                04/23/2018  --------
-- User task  FLTD-5950 (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

alter table scrub.payments add (token_id varchar2(100));

alter table clean.payments add (token_id varchar2(100));

alter table scrub.credit_cards add (cc_encrypted_flag varchar2(1));

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                04/23/2018  --------
-- User task  FLTD-5950 (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Surimenu, Bhargava Swamy                  04/26/2018  --------
-- User task  FLTD-3242 (pthakur)                 ----------------------------------
------------------------------------------------------------------------------------

alter table scrub.payments add route varchar2(20) default null;
alter table clean.payments add route varchar2(20) default null;


------------------------------------------------------------------------------------
-- Begin requested by Surimenu, Bhargava Swamy                  04/26/2018  --------
-- User task  FLTD-3242 (pthakur)                 ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                     04/26/2018  --------
-- Jira ticket is 5950  (srizvi)         ----------------------------------
------------------------------------------------------------------------------------


insert into ftd_apps.origins
    (origin_id, description, origin_type, active, partner_report_flag, send_florist_pdb_price, order_calc_type)
values
    ('MOBILE', 'Ftd.com Mobile Orders', 'internet', 'Y', 'N', 'N', 'SKIP');
insert into ftd_apps.origins
    (origin_id, description, origin_type, active, partner_report_flag, send_florist_pdb_price, order_calc_type)
values
    ('TABLET', 'Ftd.com Tablet Orders', 'internet', 'Y', 'N', 'N', 'SKIP');
insert into ftd_apps.origins
    (origin_id, description, origin_type, active, partner_report_flag, send_florist_pdb_price, order_calc_type)
values
    ('WEB', 'Ftd.com Web Orders', 'internet', 'Y', 'N', 'N', 'SKIP');


------------------------------------------------------------------------------------
-- ENS requested by Tim Schmig                     04/26/2018  --------
-- Jira ticket is 5950  (srizvi)         ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                      04/27/2018  --------
-- User task  FLTD_5950 (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

alter table scrub.payments add AUTHORIZATION_TRANSACTION_ID varchar2(100) default null;
alter table clean.payments add AUTHORIZATION_TRANSACTION_ID varchar2(100) default null;


------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                      04/27/2018  --------
-- User task  FLTD_5950 (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- Begin requested by Rose Lazul                     05/22/2018  --------
-- User task  FLTD-5957 (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.GIFT_CODE_MAINT',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'gift_code_maint_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS gift_code_maint_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.GIFT_CODE_MAINT',
      queue_table => 'OJMS.GIFT_CODE_MAINT',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.GIFT_CODE_MAINT');
end;
/


exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'EVENTS')
exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'OSP')
exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'OSP_QUEUE')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'EVENTS')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'OSP')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.GIFT_CODE_MAINT',grantee=>'OSP_QUEUE')

grant EXECUTE on EVENTS.POST_A_MESSAGE_FLEX to scrub;

------------------------------------------------------------------------------------
-- End requested by Rose Lazul                     05/22/2018  --------
-- User task  FLTD-5957 (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Bhargava Surimenu                      05/23/2018  --------
-- User task  FLTD_2822 (Bhargav)                 ----------------------------------
------------------------------------------------------------------------------------

alter table clean.payments add SETTLEMENT_TRANSACTION_ID varchar2(100) default null;
alter table clean.payments add REFUND_TRANSACTION_ID varchar2(100) default null;


insert into QUARTZ_SCHEMA.PIPELINE 
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,DEFAULT_SCHEDULE,DEFAULT_PAYLOAD,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG) 
values ('SCHEDULED_PG_EOD_JOB','ACCOUNTING','PAYMENT GATEWAY EOD JOB','Submit PG-EOD job','0 30 23 * * ?','<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>RUN-EOD</event-name><context>ACCOUNTING</context><payload>PG-EOD</payload></event>','N','OJMS.EM_ACCOUNTING','SimpleJmsJob','Y');


insert into frp.global_parms values('ACCOUNTING_CONFIG','DISPATCH_PG_EOD_FLAG','Y','FLTD-2822',sysdate,'FLTD-2822',sysdate,'Dispatch Payment Gateway EOD Job Flag');
insert into frp.global_parms values('ACCOUNTING_CONFIG','PG_EOD_BATCH_OFFSET_DATE','0','FLTD-2822',sysdate,'FLTD-2822',sysdate,'PG End of Day Batch Date Offset.  -1 assumes EOD runs after midnight.  0 assumes EOD runs before midnight');

-- insert into events.EVENTS(EVENT_NAME,CONTEXT_NAME,DESCRIPTION,ACTIVE) values ('PG-EOD-FINALIZER','ACCOUNTING','PG End-Of-Day finalizer','Y');


-- DDL for  BILLING_HEADER_PG table

  CREATE TABLE "CLEAN"."BILLING_HEADER_PG" 
   (	"BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
	"BILLING_BATCH_DATE" DATE, 
	"PROCESSED_INDICATOR" CHAR(1 BYTE) DEFAULT 'Y' NOT NULL ENABLE, 
	"PAYMENT_TYPE" VARCHAR2(100 BYTE), 
	"CREATED_ON" DATE, 
	 CONSTRAINT "BILLING_HEADER_PG_PK" PRIMARY KEY ("BILLING_HEADER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_INDX"  ENABLE, 
	 CONSTRAINT "BILLING_HEADER_PG_CK1" CHECK (processed_indicator IN ('N','Y')) ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_DATA" ;

   COMMENT ON TABLE "CLEAN"."BILLING_HEADER_PG"  IS 'PG end of day processing batch number';

   CREATE SEQUENCE  "CLEAN"."BILLING_HEADER_ID_PG_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  ORDER  NOCYCLE ;
   
   CREATE TABLE "CLEAN"."BILLING_HEADER_PG$" 
   (           "BILLING_HEADER_ID" NUMBER, 
               "BILLING_BATCH_DATE" DATE, 
               "PROCESSED_INDICATOR" CHAR(1 BYTE) DEFAULT 'Y', 
               "CREATED_ON" DATE, 
               "PAYMENT_TYPE" VARCHAR2(100 BYTE), 
               "OPERATION$" VARCHAR2(20 BYTE), 
               "MODIFIED_TIME$" TIMESTAMP (6)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_DATA" ;
   
   
 create or replace trigger "CLEAN"."BILLING_HEADER_PG_$"
	AFTER UPDATE OR DELETE ON CLEAN.BILLING_HEADER_PG
	REFERENCING OLD AS OLD NEW AS NEW
	FOR EACH ROW

DECLARE
   V_CURR_TIME TIMESTAMP;

BEGIN
   V_CURR_TIME := CURRENT_TIMESTAMP;

	IF UPDATING  THEN
		INSERT INTO CLEAN.BILLING_HEADER_PG$
		(
			BILLING_HEADER_ID, BILLING_BATCH_DATE, PROCESSED_INDICATOR, CREATED_ON, PAYMENT_TYPE, OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:OLD.BILLING_HEADER_ID, :OLD.BILLING_BATCH_DATE, :OLD.PROCESSED_INDICATOR, :OLD.CREATED_ON, :OLD.PAYMENT_TYPE, 'BEFORE_UPDATE', V_CURR_TIME
		);

		INSERT INTO CLEAN.BILLING_HEADER_PG$
		(
			BILLING_HEADER_ID, BILLING_BATCH_DATE, PROCESSED_INDICATOR, CREATED_ON, PAYMENT_TYPE, OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:NEW.BILLING_HEADER_ID, :NEW.BILLING_BATCH_DATE, :NEW.PROCESSED_INDICATOR, :NEW.CREATED_ON, :NEW.PAYMENT_TYPE, 'AFTER_UPDATE', V_CURR_TIME
		);

	ELSIF DELETING  THEN

     INSERT INTO CLEAN.BILLING_HEADER_PG$
		(
			BILLING_HEADER_ID, BILLING_BATCH_DATE, PROCESSED_INDICATOR, CREATED_ON, PAYMENT_TYPE, OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:OLD.BILLING_HEADER_ID, :OLD.BILLING_BATCH_DATE, :OLD.PROCESSED_INDICATOR, :OLD.CREATED_ON, :OLD.PAYMENT_TYPE, 'DELETE', V_CURR_TIME
		);

   END IF;

END;



/
ALTER TRIGGER "CLEAN"."BILLING_HEADER_PG_$" ENABLE;


-- BILLING_DETAIL_PG_CC


  CREATE TABLE "CLEAN"."BILLING_DETAIL_PG_CC" 
   (	"BILLING_DETAIL_ID" NUMBER NOT NULL ENABLE, 
	"BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
	"ORDER_NUMBER" VARCHAR2(20 BYTE), 
	"PAYMENT_ID" NUMBER, 
	"ORDER_AMOUNT" NUMBER, 
	"TRANSACTION_TYPE" CHAR(1 BYTE), 
	"AUTH_TRANSACTION_ID" VARCHAR2(100 BYTE), 
	"SETTLEMENT_TRANSACTION_ID" VARCHAR2(100 BYTE), 
	"REFUND_TRANSACTION_ID" VARCHAR2(100 BYTE), 
	"SETTLEMENT_DATE" DATE, 
	"RESPONSE_CODE" VARCHAR2(2 BYTE), 
	"STATUS" VARCHAR2(20 BYTE), 
	"RESPONSE_MESSAGE" VARCHAR2(4000 BYTE), 
	"BILL_TYPE" VARCHAR2(20 BYTE), 
	"IS_VOICE_AUTH" CHAR(1 BYTE), 
	"REQUEST_ID" VARCHAR2(100 BYTE), 
	"CREATED_ON" DATE, 
	"UPDATED_ON" DATE, 
	 CONSTRAINT "BILLING_DETAIL_PG_CC_CK1" CHECK (bill_type IN ('Bill','Add_Bill','Refund')) ENABLE, 
	 CONSTRAINT "BILLING_DETAIL_PG_CC_PK" PRIMARY KEY ("BILLING_DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 851968 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_INDX"  ENABLE, 
	 CONSTRAINT "BILLING_DETAIL_PG_CC_FK1" FOREIGN KEY ("BILLING_HEADER_ID")
	  REFERENCES "CLEAN"."BILLING_HEADER_PG" ("BILLING_HEADER_ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 46137344 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_DATA" ;

   COMMENT ON COLUMN "CLEAN"."BILLING_DETAIL_PG_CC"."TRANSACTION_TYPE" IS '1=purchase 3=refund';
   COMMENT ON COLUMN "CLEAN"."BILLING_DETAIL_PG_CC"."BILL_TYPE" IS 'Bill, Add Bill, Refund';
   COMMENT ON TABLE "CLEAN"."BILLING_DETAIL_PG_CC"  IS 'PG CC end of day transactions';

  CREATE INDEX "CLEAN"."BILLING_DETAIL_PG_CC_N1" ON "CLEAN"."BILLING_DETAIL_PG_CC" ("BILLING_HEADER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 2097152 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_INDX" ;

  CREATE INDEX "CLEAN"."BILLING_DETAIL_PG_CC_N2" ON "CLEAN"."BILLING_DETAIL_PG_CC" ("ORDER_NUMBER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  STORAGE(INITIAL 2097152 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_INDX" ;

  CREATE SEQUENCE  "CLEAN"."BILLING_DETAIL_ID_PG_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 CACHE 1000 NOORDER  NOCYCLE; 
  

  -- Queue Creation OJMS.PG_EOD_DISPATCHER
  
set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_EOD_DISPATCHER',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'PG_eod_dispatcher_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS PG_eod_dispatcher_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_EOD_DISPATCHER',
      queue_table => 'OJMS.PG_EOD_DISPATCHER',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_EOD_DISPATCHER');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_DISPATCHER', grantee => 'OSP');
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_DISPATCHER', grantee => 'OSP_QUEUE');

connect /
 
-- Creation of Queue OJMS.PG_EOD_CART

set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_EOD_CART',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'PG_eod_CART_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS PG_eod_CART_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_EOD_CART',
      queue_table => 'OJMS.PG_EOD_CART',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_EOD_CART');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_CART', grantee => 'OSP');
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_CART', grantee => 'OSP_QUEUE');
connect /


------------------------------------------------------------------------------------
-- End requested by Bhargava Surimenu                      05/23/2018  --------
-- User task  FLTD_2822 (Bhargav)                 ----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Start requested by Bhargava Surimenu                      05/23/2018  --------
-- User task  CAL-862 (Bhargav)                 ----------------------------------
------------------------------------------------------------------------------------

CREATE TABLE "CLEAN"."BILLING_DETAIL_PG_PP" 
   (  "BILLING_DETAIL_ID" NUMBER NOT NULL ENABLE, 
      "BILLING_HEADER_ID" NUMBER NOT NULL ENABLE, 
      "ORDER_NUMBER" VARCHAR2(20 BYTE), 
      "PAYMENT_ID" NUMBER, 
      "ORDER_AMOUNT" NUMBER, 
      "TRANSACTION_TYPE" CHAR(1 BYTE), 
      "AUTH_TRANSACTION_ID" VARCHAR2(100 BYTE), 
      "SETTLEMENT_TRANSACTION_ID" VARCHAR2(100 BYTE), 
      "REFUND_TRANSACTION_ID" VARCHAR2(100 BYTE), 
      "SETTLEMENT_DATE" DATE, 
      "RESPONSE_CODE" VARCHAR2(2 BYTE), 
      "STATUS" VARCHAR2(20 BYTE), 
      "RESPONSE_MESSAGE" VARCHAR2(4000 BYTE), 
      "BILL_TYPE" VARCHAR2(20 BYTE), 
      "IS_VOICE_AUTH" CHAR(1 BYTE), 
      "REQUEST_ID" VARCHAR2(100 BYTE), 
      "Response_token" VARCHAR2(200 BYTE),
      "CREATED_ON" DATE, 
      "UPDATED_ON" DATE, 
       CONSTRAINT "BILLING_DETAIL_PG_PP_CK1" CHECK (bill_type IN ('Bill','Add_Bill','Refund')) ENABLE, 
       CONSTRAINT "BILLING_DETAIL_PG_PP_PK" PRIMARY KEY ("BILLING_DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 851968 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_INDX"  ENABLE, 
       CONSTRAINT "BILLING_DETAIL_PG_PP_FK1" FOREIGN KEY ("BILLING_HEADER_ID")
        REFERENCES "CLEAN"."BILLING_HEADER_PG" ("BILLING_HEADER_ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 46137344 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CLEAN_DATA" ;

CREATE SEQUENCE  "CLEAN"."BILLING_DETAIL_ID_PG_PP_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 2 CACHE 1000 NOORDER  NOCYCLE;


insert into frp.global_parms 
values('ACCOUNTING_CONFIG','PG_ALT_PAY_EOD_RUNDATE_OFFSET','0','CAL-962',sysdate,'CAL-962',sysdate,'Billing date offset (in days) for PG Alt Pay End-Of-Day processing');

insert into frp.global_parms 
values('ACCOUNTING_CONFIG','DISPATCH_PG_PAYPAL_EOD_FLAG','Y','CAL-962',sysdate,'CAL-962',sysdate,'Dispatch Payment Gateway EOD PayPal Job Flag');


set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_ALTPAY_EOD',
      storage_clause     => 'VARRAY user_data.header.properties STORE AS LOB PG_PP_EOD_hdrlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS PG_ALTPAY_EOD_txtlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'PG Alt Pay end-of-day');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_ALTPAY_EOD',
      queue_table => 'OJMS.PG_ALTPAY_EOD',
      comment     => 'PG AltPay end-of-day');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_ALTPAY_EOD');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_PG_ALTPAY_EOD_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PG_ALTPAY_EOD',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PG_ALTPAY_EOD',
      grantee => 'OSP');

end;

---- OJMS.PG_ALTPAY_CART QUEUE
set echo on serveroutput on size 1000000

begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_ALTPAY_CART',
      storage_clause     => 'VARRAY user_data.header.properties STORE AS LOB PG_ALTPAY_cart_hdrlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS PG_ALTPAY_cart_txtlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'PG Altpay cart processing');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_ALTPAY_CART',
      queue_table => 'OJMS.PG_ALTPAY_CART',
      comment     => 'PG Altpay cart processing');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_ALTPAY_CART');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_PG_ALTPAY_CART_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PG_ALTPAY_CART',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PG_ALTPAY_CART',
      grantee => 'OSP');

end;



------------------------------------------------------------------------------------
-- End requested by Bhargava Surimenu                      05/23/2018  --------
-- User task  CAL-862 (Bhargav)                 ----------------------------------
------------------------------------------------------------------------------------







------------------------------------------------------------------------------------
-- Start requested by Rose Lazuk                    06/6/2018  --------
-- User task  FLTD-5136 and CAL-705  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'GIFT_CODE_SERVICE_QUANTITY_LIMIT', '25', ' CAL-705', sysdate, 'CAL-705', sysdate, 'Limit of Quantity for Gift Certificates to be created');



-- DEV Version Don't run in production

-- INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
-- VALUES ('SERVICE', 'GIFT_CODE_SERVICE_URL', ' https://nonprod-gke-primary-1-proxy-dev2.gcp.ftdi.com/payment-giftcode---service/ftd/payment/giftcode/', 'FLTD-5136', sysdate, 'FLTD-5136', sysdate, 'URL for gift code microservice');


-- Consumer QA Version Don't run in production

-- INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
-- VALUES ('SERVICE', 'GIFT_CODE_SERVICE_URL', ' https://nonprod-gke-primary-1-proxy-qa2.gcp.ftdi.com/payment-giftcode---service/ftd/payment/giftcode/', 'FLTD-5136', sysdate, 'FLTD-5136', sysdate, 'URL for gift code microservice');

-- Production version don't run anywhere else
-- INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
-- VALUES ('SERVICE', 'GIFT_CODE_SERVICE_URL', 'https://prod-api-gateway.gcp.ftdi.com/internal/payment-gc/ftd/payment/giftcode/' 'FLTD-5136', sysdate, 'FLTD-5136', sysdate, 'URL for gift code microservice');



------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                    06/6/2018  --------
-- User task  FLTD-5136 and CAL-705  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Bhargav                    06/14/2018  --------
-- User task  FLTD-5136 and CAL-861  (Self)                 ----------------------------------
------------------------------------------------------------------------------------


alter table clean.payments add request_token varchar2(4000) default null;
alter table clean.payments add merchant_ref_id varchar2(100) default null;
alter table clean.billing_header_pg add DISPATCHED_COUNT NUMBER(22) default 0;
alter table clean.billing_header_pg add PROCESSED_COUNT NUMBER(22) default 0;;
alter table clean.BILLING_DETAIL_PG_CC add process_account varchar2(50) default null;
alter table clean.BILLING_DETAIL_PG_PP add process_account varchar2(50) default null;

ALTER TABLE clean.BILLING_DETAIL_PG_CC  MODIFY response_code varchar2(5);
ALTER TABLE clean.BILLING_DETAIL_PG_PP  MODIFY response_code varchar2(5); 


------------------------------------------------------------------------------------
-- End requested by Bhargav                    06/14/2018  --------
-- User task  FLTD-5136 and CAL-861  (Self)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Rose Lazuk                    06/18/2018  --------
-- User Story CAL-2657  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------

-- This Change requires Goldengate Change

alter table ftd_apps.company_master
modify GIFT_CODE_DISPLAY_FLAG CHAR(1);

update ftd_apps.company_master 
set GIFT_CODE_DISPLAY_FLAG='N';

alter table ftd_apps.company_master
modify GIFT_CODE_DISPLAY_FLAG CHAR(1) DEFAULT 'N' NOT NULL ENABLE;


------------------------------------------------------------------------------------
-- End requested by Rose Lazuk                    06/18/2018  --------
-- User Story CAL-2657  (srizvi)                 ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Start requested by Bhargav                    06/23/2018  --------
-- User task  CAL-1934  (Self)                 ----------------------------------
------------------------------------------------------------------------------------

insert into QUARTZ_SCHEMA.PIPELINE 
(PIPELINE_ID,GROUP_ID,DESCRIPTION,TOOL_TIP,FORCE_DEFAULT_PAYLOAD,QUEUE_NAME,CLASS_CODE,ACTIVE_FLAG) 
values ('PGEODREPROCESS','ACCOUNTING','Reproces PG EOD Records','Processing of failed records of PG EOD Job. Payload is Billing Header ID|Payment Type.','N','OJMS.PG_EOD_REPROCESS','SimpleJmsJob','Y');


set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_EOD_REPROCESS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'PG_EOD_REPROCESS_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS PG_EOD_REPROCESS_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_EOD_REPROCESS',
      queue_table => 'OJMS.PG_EOD_REPROCESS',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_EOD_REPROCESS');
end;
/

exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'EVENTS')
exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'OSP')
exec dbms_aqadm.grant_queue_privilege(privilege=>'DEQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'OSP_QUEUE')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'EVENTS')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'OSP')
exec dbms_aqadm.grant_queue_privilege(privilege=>'ENQUEUE',queue_name=>'OJMS.PG_EOD_REPROCESS',grantee=>'OSP_QUEUE')

------------------------------------------------------------------------------------
-- End requested by Bhargav                    06/23/2018  --------
-- User task  CAL-1934  (Self)                 ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Start requested by Bhargav                    06/27/2018  --------
-- User task  CAL-2990  (Self)                 ----------------------------------
------------------------------------------------------------------------------------

Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('ACCOUNTING_CONFIG','PG_UPDATE_REMOVED_ORDER_PMT_LAST_DATE',null,'CAL-2990',sysdate,'CAL-2990',sysdate,'Date when removed order payments were updated.');

------------------------------------------------------------------------------------
-- End requested by Bhargav                    06/27/2018  --------
-- User task  CAL-2990  (Self)                 ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Start requested by Bhargav                    07/13/2018  --------
-- User task  CAL-3570  (Self)                 ----------------------------------
------------------------------------------------------------------------------------

alter table clean.BILLING_DETAIL_PG_CC add merchant_ref_id varchar2(2000) default null;
alter table clean.BILLING_DETAIL_PG_PP add merchant_ref_id varchar2(2000) default null; 

------------------------------------------------------------------------------------
-- End requested by Bhargav                    07/13/2018  --------
-- User task  CAL-3570   (Self)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Bhargav                    07/18/2018  --------
-- User task  CAL-4074  (Self)                 ----------------------------------
------------------------------------------------------------------------------------

 ALTER TABLE CLEAN.PAYMENTS$ ADD ( 
                ROUTE                          VARCHAR2(20),
               TOKEN_ID                     VARCHAR2(100), 
AUTHORIZATION_TRANSACTION_ID          VARCHAR2(100) ,
SETTLEMENT_TRANSACTION_ID          VARCHAR2(100), 
REFUND_TRANSACTION_ID          VARCHAR2(100), 
REQUEST_TOKEN          VARCHAR2(4000), 
MERCHANT_REF_ID          VARCHAR2(100)
);

------------------------------------------------------------------------------------
-- End requested by Bhargav                    07/18/2018  --------
-- User task  CAL-4074   (Self)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Tim Schmig                                07/25/2018  --------
-- User task  CAL-3975  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

update ftd_apps.origins
set description = 'Calyx Mobile Orders',
    order_calc_type = 'CALYX'
where origin_id = 'MOBILE';

update ftd_apps.origins
set description = 'Calyx Tablet Orders',
    order_calc_type = 'CALYX'
where origin_id = 'TABLET';

update ftd_apps.origins
set description = 'Calyx Web Orders',
    order_calc_type = 'CALYX'
where origin_id = 'WEB';

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                07/25/2018  --------
-- User task  CAL-3975  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                07/25/2018  --------
-- User task  CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
   CLASS_CODE,
    ACTIVE_FLAG)
values (
    'Retry Gift Code Redemption',
    'ACCOUNTING',
    'Insert Gift Code Maint Record',
    'This process sends a JMS message to OJMS.GIFT_CODE_MAINT. The payload is REDEEM-GIFT-CODE+|GIFT CODE ID + |ORDER DETAIL ID +|SYS +|ORDER TOTAL + |SCRUB.ORDER_DETAILS.LINE NUMBER (for example REDEEM-GIFT-CODE|CP31479780242|2884305|SYS|60.98|1)',
    null,
    null,
    'N',
    'OJMS.GIFT_CODE_MAINT',
    'SimpleJmsJob',
    'Y');


------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                07/25/2018  --------
-- User task  CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Start requested by Tim Schmig                                07/31/2018  --------
-- User task  CAL-4488  (Self)                    ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'DETOKENIZE_MAX_ATTEMPTS', '30', 'CAL-4488', sysdate, 'CAL-4488', sysdate,'Maximum attempts to detokenize an order before sending to Scrub without payment info');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('FTDAPPS_PARMS', 'CALYX_DEFAULT_CARD_MESSAGE', 'Gift message not provided by sender', 'CAL-4032', sysdate, 'CAL-4032', sysdate,'Default gift message if one was not provided by new website');

------------------------------------------------------------------------------------
-- End requested by Tim Schmig                                  07/31/2018  --------
-- User task  CAL-4488   (Self)                   ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                08/05/2018  --------
-- User task  CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

update quartz_schema.pipeline
set tool_tip = 
'This process sends a message to OJMS.GIFT_CODE_MAINT. The payload is REDEEM-GIFT-CODE+|GIFT CODE ID + |ORDER DETAIL ID +|SYS +|ORDER TOTAL + |SCRUB.ORDER_DETAILS.LINE NUMBER +|RETRY COUNT  (for example REDEEM-GIFT-CODE|CP31479780242|2884305|SYS|60.98|1|0)'
where pipeline_id = 'Retry Gift Code Redemption' and group_id = 'ACCOUNTING';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                08/05/2018  --------
-- User task  CAL-1170  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                08/23/2018  --------
-- User task FLTD-5136  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

delete
from clean.gc_coupon_status_val
where gc_coupon_status = 'Active and Clear';

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                08/23/2018  --------
-- User task FLTD-5136  (mwoytus)                 ----------------------------------
------------------------------------------------------------------------------------

