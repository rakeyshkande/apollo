------------------------------------------------------------------------------------
-- Begin requested by  Manasa Salla                           06/27/2017   ---------
-- sprint 1                                    -------------------------------------
------------------------------------------------------------------------------------

alter table scrub.payments add CARDINAL_VERIFIED_FLAG varchar2(1);
alter table clean.payments add CARDINAL_VERIFIED_FLAG varchar2(1);
alter table clean.payments$ add CARDINAL_VERIFIED_FLAG varchar2(1);

------------------------------------------------------------------------------------
-- End   requested by Manasa Salla                               06/27/2017  --------
-- User story 2699 (10.3.0.1)   (Preethi)         ----------------------------------
------------------------------------------------------------------------------------


