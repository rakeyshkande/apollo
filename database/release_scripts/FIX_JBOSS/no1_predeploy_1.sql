insert into FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 'ins_novator_mailbox_monitor', 'FTDCA MAIL SERVER', 'sodium.ftdi.com', 'DEFECT_9128_Release_4_9_0', sysdate,
    'DEFECT_9128_Release_4_9_0', sysdate, 'mail server to retrieve FTD.CA customer service emails'); 

insert into FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ( 'ins_novator_mailbox_monitor', 'FTDCA EXCLUDE DOMAINS ', 'amazon.com, walmart.com', 'DEFECT_9128_Release_4_9_0',
    sysdate, 'DEFECT_9128_Release_4_9_0', sysdate,
    ' Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.'); 

