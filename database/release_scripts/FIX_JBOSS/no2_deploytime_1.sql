UPDATE ftd_apps.CONTENT_DETAIL set CONTENT_TXT=replace(CONTENT_TXT, 'ftd.com', '~companyurl~') where CONTENT_TXT like '%ftd.com%';

alter table FTD_APPS.COMPANY_MASTER add ( NEWSLETTER_COMPANY_ID   Varchar2(12));

update ftd_apps.company_master set newsletter_company_id = 'FTD';

alter table FTD_APPS.COMPANY_MASTER modify (NEWSLETTER_COMPANY_ID not null);

update ftd_apps.company_master set default_domain = 'ftd-ca' where company_id = 'FTDCA';

INSERT INTO MGIOVENCO.DUPLICATE_ACCOUNT_DATA
    select distinct ae.account_master_id, ae.email_address, ae.active_flag, am.company_id, ae.account_email_id
    from account.account_email ae
        join account.account_email ae1
        on ae.email_address = ae1.email_address
        and ae.account_master_id <> ae1.account_master_id
        join account.account_master am
        on am.account_master_id = ae.account_master_id
        join account.account_master am1
        on am1.account_master_id = ae1.account_master_id
    where
    am.company_id <> am1.company_id
    and am.company_id <> 'FTD'
    and ae.active_flag = 'Y';

delete from account.account_email ae where ae.account_email_id in (
select account_email_id from MGIOVENCO.DUPLICATE_ACCOUNT_DATA
where email_address in(
select email_address
from MGIOVENCO.DUPLICATE_ACCOUNT_DATA 
group by email_address
having (count(email_address) = 1 )));

delete from account.account_program where account_master_id in (
select account_master_id from MGIOVENCO.DUPLICATE_ACCOUNT_DATA
where email_address in(
select email_address
from MGIOVENCO.DUPLICATE_ACCOUNT_DATA 
group by email_address
having (count(email_address) = 1 )));

delete from account.account_master am where am.account_master_id in (
select account_master_id from MGIOVENCO.DUPLICATE_ACCOUNT_DATA
where email_address in(
select email_address
from MGIOVENCO.DUPLICATE_ACCOUNT_DATA 
group by email_address
having (count(email_address) = 1 )));

alter table ACCOUNT.ACCOUNT_MASTER drop column COMPANY_ID;

drop function ftd_apps.index_exists;
drop procedure ftd_apps.fetch_index_products;
drop function ftd_apps.is_product_allowed_by_index;
drop procedure ftd_apps.get_source_index_child;
drop procedure  ftd_apps.get_source_index_product;
drop function ftd_apps.is_product_in_index;

drop table ftd_apps.SHOPPING_INDEX_TRACKING;
drop table ftd_apps.SHOPPING_INDEX_SUB_INDEX_STAGE;
drop table ftd_apps.SHOPPING_INDEX_SUB_INDEX;
drop table ftd_apps.SHOPPING_INDEX_SRC_STAGE;
drop table ftd_apps.SHOPPING_INDEX_SRC_PROD_STAGE;
drop table ftd_apps.SHOPPING_INDEX_SRC_PROD;
drop table ftd_apps.SHOPPING_INDEX_SRC;


