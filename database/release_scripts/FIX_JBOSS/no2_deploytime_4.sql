insert into ftd_apps.company ( COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE)
values ( 'FTDCAP', 'FTD.CA', 1234, 'FTDCA');

insert into ftd_apps.company ( COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE)
values ( 'FTDCAI', 'FTD.CA', 1234, 'FTDCA');

insert into ftd_apps.script_master ( SCRIPT_MASTER_ID, SCRIPT_DESCRIPTION)
values ( 'FTDCA', 'FTD.CA default script');

insert into ftd_apps.dnis ( DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values ( 1234, 'FTD.CA', 'FTDCAP', 'A', 'Y', 'N', 'DG', 'FTDCA', 'FTDCA', 'FTDCA', 'Default');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'Phone', '1-800-123-4567');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'Email', 'http://custserv.ftd.ca/email.epl');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'Website', 'http://www.ftd.ca');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'CustServHours', 'We are here to assist you 24 hours a day, 7 days a week.');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'FromAddress', 'custserv@ftd.ca');

insert into ftd_apps.email_company_data ( COMPANY_ID, NAME, VALUE)
values ( 'FTDCA', 'NoReplyFromAddress', 'noreply@ftd.ca');

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'companyname', 'FTDCA', 'text', 'FTD.CA', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'csemail', 'FTDCA', 'text', 'custserv@ftd.ca', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'cslink', 'FTDCA', 'text', 'custserv.ftd.ca', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'inquiry', 'FTDCA', 'text', 'FTD.CA', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'name', 'FTDCA', 'text', 'FTD.CA', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'phone', 'FTDCA', 'text', '1-800-123-4567', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'phone2', 'FTDCA', 'text', '1-800-123-4567', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'producttype', 'FTDCA', 'text', 'gift', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'producttype2', 'FTDCA', 'text', 'gift', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'producttype3', 'FTDCA', 'text', 'gift', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'producttype4', 'FTDCA', 'text', 'gift', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'producttype5', 'FTDCA', 'text', 'gift', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'purchasemsg', 'FTDCA', 'text', 'purchase', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'specmsg', 'FTDCA', 'text',
    'If you select this option, we''d be happy to call your recipient to let them know about your gift and apologize for the delay',
    null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'specmsg2', 'FTDCA', 'text', 'recent purchase from FTD.CA', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'submsg2', 'FTDCA', 'text',
    'If you would like to speak with a Customer Service Representative, please email us by clicking on this link http://custserv.ftd.ca, or dial 1-800-123-4567. We are here to assist you 24 hours a day, 7 days a week.',
    null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'upcasename', 'FTDCA', 'text', 'FTD.CA', null);

insert into clean.message_tokens ( TOKEN_ID, COMPANY_ID, TOKEN_TYPE, TOKEN_VALUE, DEFAULT_VALUE)
values ( 'website', 'FTDCA', 'text', 'www.ftd.ca', null);

