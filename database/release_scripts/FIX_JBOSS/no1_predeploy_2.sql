REM To be run as ops$oracle

INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ( 'email_request_processing', 'FTDCA_mailbox_monitor_USERNAME',
    global.encryption.encrypt_it('replace', 'APOLLO_TEST_2010'), 'Login to pull erp emails for FTD.CA.', sysdate,
    'DEFECT_9128_Release_4_9_0', sysdate, 'DEFECT_9128_Release_4_9_0', 'APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ( 'email_request_processing', 'FTDCA_mailbox_monitor_PASSWORD',
    global.encryption.encrypt_it('replace', 'APOLLO_TEST_2010'), 'Password to pull erp emails for FTD.CA.', sysdate,
    'DEFECT_9128_Release_4_9_0', sysdate, 'DEFECT_9128_Release_4_9_0', 'APOLLO_TEST_2010');

create table ftd_apps.SI_TEMPLATE_TYPE                        (SI_TEMPLATE_TYPE_CODE     VARCHAR2(2),
                                                               SI_TEMPLATE_TYPE_DESC     VARCHAR2(50),
                                                               SORT_NUM                  integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.si_template_type add constraint si_template_type_pk primary key
(si_template_type_code) using index tablespace ftd_apps_indx;

create table ftd_apps.SI_TEMPLATE                             (SI_TEMPLATE_ID            integer,
                                                               SI_TEMPLATE_TYPE_CODE     VARCHAR2(2),
                                                               SI_TEMPLATE_KEY           VARCHAR2(100),
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE add constraint si_template_pk primary key
(si_template_id) using index tablespace ftd_apps_indx;

create index ftd_apps.si_template_n1 on ftd_apps.si_template (si_template_type_code) tablespace ftd_apps_indx;

alter table ftd_apps.si_template add constraint si_template_fk1 foreign key
(si_template_type_code) references ftd_apps.si_template_type;

create table ftd_apps.SI_SOURCE_TEMPLATE_REF                  (SOURCE_CODE               VARCHAR2(10) NOT NULL,
                                                               SI_TEMPLATE_ID            integer NOT NULL,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_SOURCE_TEMPLATE_REF add constraint SI_SOURCE_TEMPLATE_REF_PK primary key
(source_code, si_template_id) using index tablespace ftd_apps_indx;

create index ftd_apps.si_source_template_ref_n1 on ftd_apps.si_source_template_ref
(si_template_id) tablespace ftd_apps_indx;

alter table ftd_apps.SI_SOURCE_TEMPLATE_REF add constraint SI_SOURCE_TEMPLATE_REF_FK1 foreign key 
(si_template_id) references ftd_apps.si_template;

create table ftd_apps.SI_TEMPLATE_INDEX                       (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               INDEX_DESC                VARCHAR2(100),
                                                               COUNTRY_ID                VARCHAR2(2),
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX add constraint SI_TEMPLATE_INDEX_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME) using index tablespace ftd_apps_indx;

alter table ftd_apps.SI_TEMPLATE_INDEX add constraint SI_TEMPLATE_INDEX_FK1 foreign key
(si_template_id) references ftd_apps.si_template;

create table ftd_apps.SI_TEMPLATE_INDEX_PROD                  (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               PRODUCT_ID                VARCHAR2(100) NOT NULL,
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX_PROD add constraint SI_TEMPLATE_INDEX_PROD_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME, PRODUCT_ID) using index tablespace ftd_apps_indx;

alter table ftd_apps.SI_TEMPLATE_INDEX_PROD add constraint SI_TEMPLATE_INDEX_PROD_FK1 foreign key
(SI_TEMPLATE_ID, INDEX_NAME) references ftd_apps.si_template_index;

create table ftd_apps.SI_TEMPLATE_INDEX_SUB                   (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               SUB_INDEX_NAME            VARCHAR2(300) NOT NULL,
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX_SUB add constraint SI_TEMPLATE_INDEX_SUB_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME, SUB_INDEX_NAME) using index tablespace ftd_apps_indx;

alter table ftd_apps.SI_TEMPLATE_INDEX_SUB add constraint SI_TEMPLATE_INDEX_SUB_FK1 foreign key
(si_template_id) references ftd_apps.si_template;

create table ftd_apps.SI_FILE_NAME_TRACKER                    (FILE_NAME                 VARCHAR2(100) NOT NULL,
                                                               LAST_INDEX_DATE           DATE,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100),
                                                               UPDATED_ON                DATE,
                                                               UPDATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_FILE_NAME_TRACKER add constraint SI_FILE_NAME_TRACKER_PK primary key
(file_name) using index tablespace ftd_apps_indx;

create table ftd_apps.SI_TEMPLATE_STAGE                       (SI_TEMPLATE_ID            integer,
                                                               SI_TEMPLATE_TYPE_CODE     VARCHAR2(2),
                                                               SI_TEMPLATE_KEY           VARCHAR2(100),
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_STAGE add constraint si_template_stage_pk primary key
(si_template_id) using index tablespace ftd_apps_indx;

create index ftd_apps.si_template_stage_n1 on ftd_apps.si_template_stage
(si_template_type_code) tablespace ftd_apps_indx;

create table ftd_apps.SI_SOURCE_TEMPLATE_REF_STAGE            (SOURCE_CODE               VARCHAR2(10) NOT NULL,
                                                               SI_TEMPLATE_ID            integer NOT NULL,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_SOURCE_TEMPLATE_REF_STAGE add constraint SI_SOURCE_TEMPLATE_REF_STG_PK primary key
(source_code, si_template_id) using index tablespace ftd_apps_indx;

create index ftd_apps.si_source_template_ref_stg_n1 on ftd_apps.si_source_template_ref_stage
(si_template_id) tablespace ftd_apps_indx;

create table ftd_apps.SI_TEMPLATE_INDEX_STAGE                 (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               INDEX_DESC                VARCHAR2(100),
                                                               COUNTRY_ID                VARCHAR2(2),
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX_STAGE add constraint SI_TEMPLATE_INDEX_STG_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME) using index tablespace ftd_apps_indx;

create table ftd_apps.SI_TEMPLATE_INDEX_PROD_STAGE            (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               PRODUCT_ID                VARCHAR2(100) NOT NULL,
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX_PROD_STAGE add constraint SI_TEMPLATE_INDEX_PROD_STG_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME, PRODUCT_ID) using index tablespace ftd_apps_indx;

create table ftd_apps.SI_TEMPLATE_INDEX_SUB_STAGE             (SI_TEMPLATE_ID            integer NOT NULL,
                                                               INDEX_NAME                VARCHAR2(300) NOT NULL,
                                                               SUB_INDEX_NAME            VARCHAR2(300) NOT NULL,
                                                               DISPLAY_SEQ_NUM           integer,
                                                               CREATED_ON                DATE,
                                                               CREATED_BY                VARCHAR2(100))
tablespace ftd_apps_data;

alter table ftd_apps.SI_TEMPLATE_INDEX_SUB_STAGE add constraint SI_TEMPLATE_INDEX_SUB_STG_PK primary key
(SI_TEMPLATE_ID, INDEX_NAME, SUB_INDEX_NAME) using index tablespace ftd_apps_indx;

insert into ftd_apps.si_template_type values ('CM', 'custom', 1, sysdate, 'DEFECT_8031_Release_4_9_0', sysdate, 'DEFECT_8031_Release_4_9_0');
insert into ftd_apps.si_template_type values ('DT', 'default', 2, sysdate, 'DEFECT_8031_Release_4_9_0', sysdate, 'DEFECT_8031_Release_4_9_0');
insert into ftd_apps.si_template_type values ('TL', 'template', 3, sysdate, 'DEFECT_8031_Release_4_9_0', sysdate, 'DEFECT_8031_Release_4_9_0');
insert into ftd_apps.si_template_type values ('DN', 'domain', 4, sysdate, 'DEFECT_8031_Release_4_9_0', sysdate, 'DEFECT_8031_Release_4_9_0');

create sequence ftd_apps.si_template_id_sq start with 100000 increment by 1 cache 200;

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','SHOPPING_INDEX_REQUEUE_DELAY_SECONDS','300','DEFECT_8031_Release_4_9_0',sysdate,'DEFECT_8031_Release_4_9_0',sysdate,
'Number of seconds to delay for index to be reprocessed.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('product_database','SHOPPING_INDEX_MAX_RETRY_COUNT','10','DEFECT_8031_Release_4_9_0',sysdate,'DEFECT_8031_Release_4_9_0',sysdate,
'Number of attempts to retry before sending system message.');

# For the next qa release
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GET_PRODUCT_BY_CATEGORY_MAX_COUNT','100','DEFECT_8031_Release_4_9_0',sysdate,'DEFECT_8031_Release_4_9_0',sysdate, 
'Number of products sent to PAS call each time.');

