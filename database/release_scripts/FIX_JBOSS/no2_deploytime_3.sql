This script is very slow.  Do everything you can think of to make the inserts and updates go fast!

alter table clean.orders disable constraint orders_email_fk;

alter table clean.customer_email_ref disable constraint customer_email_ref_fk2;

UPDATE CLEAN.ORDERS O
SET O.EMAIL_ID = (
    SELECT E2.EMAIL_ID
    FROM CLEAN.EMAIL E1
    JOIN CLEAN.EMAIL E2
    ON E2.EMAIL_ADDRESS = E1.EMAIL_ADDRESS
    AND E2.COMPANY_ID = 'FTD'
    WHERE E1.EMAIL_ID = O.EMAIL_ID
), UPDATED_ON = SYSDATE,
    UPDATED_BY = 'Defect_9129'
WHERE O.ORDER_GUID IN (
    SELECT O2.ORDER_GUID
    FROM CLEAN.ORDERS O2
    JOIN CLEAN.EMAIL E3
    ON E3.EMAIL_ID = O2.EMAIL_ID
    AND E3.COMPANY_ID <> 'FTD'
    WHERE O2.EMAIL_ID IS NOT NULL
);

DELETE FROM CLEAN.CUSTOMER_EMAIL_REF
WHERE EMAIL_ID IN (
    SELECT EMAIL_ID FROM CLEAN.EMAIL
    WHERE COMPANY_ID <> 'FTD');

create table email as select * from clean.email where company_id = 'FTD';

TRUNCATE TABLE CLEAN.EMAIL;

INSERT /*+ APPEND */ INTO clean.EMAIL SELECT * FROM email;

drop table email;

create table direct_mail as select * from clean.direct_mail where company_id = 'FTD';

truncate table CLEAN.DIRECT_MAIL;

insert /*+ APPEND */ into clean.direct_mail select * from direct_mail;

drop table direct_mail;

alter table clean.orders enable constraint orders_email_fk;

alter table clean.customer_email_ref enable constraint customer_email_ref_fk2;

UPDATE CLEAN.DIRECT_MAIL SET SUBSCRIBE_STATUS = 'Unsubscribe'
    WHERE SUBSCRIBE_STATUS IS NULL;

UPDATE CLEAN.EMAIL SET SUBSCRIBE_STATUS = 'Unsubscribe'
    WHERE SUBSCRIBE_STATUS IS NULL;

rebuild indexes:
account.account_email
account.account_program
account.account_master
ftd_apps.product_company_xref
clean.orders
clean.customer_email_ref
clean.email
clean.direct_mail
