create table CLEAN.QUEUE_DELETE_HEADER (
QUEUE_DELETE_HEADER_ID NUMBER(22)   not null,
BATCH_DESCRIPTION      VARCHAR2(100) not null,
BATCH_COUNT            NUMBER(22) not null,
BATCH_PROCESSED_FLAG   CHAR(1) not null,
INCLUDE_PARTNER_NAME   VARCHAR2(2000),
EXCLUDE_PARTNER_NAME   VARCHAR2(2000),
DELETE_QUEUE_MSG_FLAG  CHAR(1) not null,
DELETE_QUEUE_MSG_TYPE  VARCHAR2(2000),
SEND_MESSAGE_FLAG      VARCHAR2(20),
MESSAGE_TYPE           VARCHAR2(8),
ANSP_AMOUNT            NUMBER(10,2),
GIVE_FLORIST_ASKING_PRICE CHAR(1) not null,
CANCEL_REASON_CODE     VARCHAR2(3),
TEXT_OR_REASON         VARCHAR2(4000),
RESUBMIT_ORDER_FLAG    CHAR(1) not null,
INCLUDE_COMMENT_FLAG   CHAR(1) not null,
COMMENT_TEXT           VARCHAR2(4000),   
SEND_EMAIL_FLAG        CHAR(1) not null,
EMAIL_TITLE            VARCHAR2(1000),
REFUND_ORDER_FLAG      CHAR(1) not null,
REFUND_DISP_CODE       VARCHAR2(5),
RESPONSIBLE_PARTY      VARCHAR2(20),
DELETE_BATCH_BY_TYPE   VARCHAR2(50),
CREATED_ON             DATE not null,
CREATED_BY             VARCHAR2(100) not null,
UPDATED_ON             DATE not null,
UPDATED_BY             VARCHAR2(100) not null) tablespace clean_data;

alter table clean.queue_delete_header add constraint queue_delete_header_pk primary key (queue_delete_header_id)
using index tablespace clean_indx;

alter table clean.queue_delete_header add constraint queue_delete_header_fk1 foreign key (cancel_reason_code)
references venus.cancel_reason_code_val;

alter table clean.queue_delete_header add constraint queue_delete_header_fk2 foreign key (refund_disp_code)
references clean.refund_disposition_val;

comment on column CLEAN.QUEUE_DELETE_HEADER.BATCH_DESCRIPTION is 'User defined description of the batch';
comment on column CLEAN.QUEUE_DELETE_HEADER.BATCH_COUNT is 'Total number of messages in a batch';
comment on column CLEAN.QUEUE_DELETE_HEADER.BATCH_PROCESSED_FLAG is 'Flag indicating if the entire batch has been processed';
comment on column CLEAN.QUEUE_DELETE_HEADER.INCLUDE_PARTNER_NAME is 'Comma separated list of partners to be included in batch';
comment on column CLEAN.QUEUE_DELETE_HEADER.EXCLUDE_PARTNER_NAME is 'Comma separated list of partners to be excluded from batch';
comment on column CLEAN.QUEUE_DELETE_HEADER.DELETE_QUEUE_MSG_FLAG is
'Flag indicating if messages in associated queues should be deleted';
comment on column CLEAN.QUEUE_DELETE_HEADER.DELETE_QUEUE_MSG_TYPE is
'Comma separated list of queue types associated messages should be deleted from';
comment on column CLEAN.QUEUE_DELETE_HEADER.SEND_MESSAGE_FLAG is 'Flag indicating if a message will be sent';
comment on column CLEAN.QUEUE_DELETE_HEADER.MESSAGE_TYPE is 'Message type to be sent to florist or vendor';
comment on column CLEAN.QUEUE_DELETE_HEADER.ANSP_AMOUNT is
'The amount of additional money to be added to the mercury value for the florists';
comment on column CLEAN.QUEUE_DELETE_HEADER.GIVE_FLORIST_ASKING_PRICE is
'Flag indicating if florist will be given the amount they asked for';
comment on column CLEAN.QUEUE_DELETE_HEADER.CANCEL_REASON_CODE is 'Reason for the CAN';
comment on column CLEAN.QUEUE_DELETE_HEADER.TEXT_OR_REASON is
'Reason for cancelling the order.  This data goes out as comments on the mercury or venus message';
comment on column CLEAN.QUEUE_DELETE_HEADER.RESUBMIT_ORDER_FLAG is
'Flag indicating whether the order will be resubmitted for processing';
comment on column CLEAN.QUEUE_DELETE_HEADER.INCLUDE_COMMENT_FLAG is 'Flag indicating if comment will be included on the order';
comment on column CLEAN.QUEUE_DELETE_HEADER.COMMENT_TEXT is 'Comment to be added to the order';
comment on column CLEAN.QUEUE_DELETE_HEADER.SEND_EMAIL_FLAG is 'Flag indicating if an email will be sent to the customer';
comment on column CLEAN.QUEUE_DELETE_HEADER.EMAIL_TITLE is 'Title of the email sent to the customer';
comment on column CLEAN.QUEUE_DELETE_HEADER.REFUND_ORDER_FLAG is 'Flag indicating if the order will be refunded';
comment on column CLEAN.QUEUE_DELETE_HEADER.REFUND_DISP_CODE is 'Refund disposition code';
comment on column CLEAN.QUEUE_DELETE_HEADER.RESPONSIBLE_PARTY is 'Party responsible for refund';
comment on column CLEAN.QUEUE_DELETE_HEADER.DELETE_BATCH_BY_TYPE is
'Defines which type of delete to perform on the batch : message_id or external_order_number';

create table CLEAN.QUEUE_DELETE_DETAIL (
QUEUE_DELETE_DETAIL_ID NUMBER(22) not null,
QUEUE_DELETE_HEADER_ID NUMBER(22) not null,
MESSAGE_ID             NUMBER(22),
EXTERNAL_ORDER_NUMBER  VARCHAR2(20),
PROCESSED_STATUS       VARCHAR2(100),
ERROR_TEXT             VARCHAR2(100),
CREATED_ON             DATE not null,
CREATED_BY             VARCHAR2(100) not null,
UPDATED_ON             DATE not null,
UPDATED_BY             VARCHAR2(100) not null) tablespace clean_indx;

alter table clean.queue_delete_detail add constraint queue_delete_detail_fk1 foreign key (queue_delete_header_id)
references clean.queue_delete_header;

create index clean.queue_delete_detail_n1 on clean.queue_delete_detail (queue_delete_header_id) tablespace clean_indx;
create index clean.queue_delete_detail_n2 on clean.queue_delete_detail (message_id) tablespace clean_indx;
create index clean.queue_delete_detail_n3 on clean.queue_delete_detail (external_order_number) tablespace clean_indx;

alter table clean.queue_delete_detail add constraint queue_delete_detail_pk primary key (queue_delete_detail_id)
using index tablespace clean_indx;

comment on column clean.queue_delete_detail.MESSAGE_ID is 'clean.queue message id';
comment on column clean.queue_delete_detail.EXTERNAL_ORDER_NUMBER is 'clean.queue external order number';
comment on column clean.queue_delete_detail.PROCESSED_STATUS is 'Success, Failure, Duplicate or null';
comment on column clean.queue_delete_detail.ERROR_TEXT is 'Reason message failed processing';

create sequence clean.queue_delete_header_sq start with 1 increment by 1 nocycle noorder cache 200;
create sequence clean.queue_delete_detail_sq start with 1 increment by 1 nocycle noorder cache 200;

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('QUEUE_CONFIG','BATCH_HISTORY_HOURS', '48' ,'Defect_7419_Release_4_7_0', SYSDATE, 'Defect_7419_Release_4_7_0' ,SYSDATE,
'number of hours to be specified as history to pull records from QUEUE_DELETE_HEADER table');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values ('QUEUE_CONFIG','MAX_RECORDS_PER_PAGE', '20' ,'Defect_7419_Release_4_7_0', SYSDATE, 'Defect_7419_Release_4_7_0' ,SYSDATE,
'number of QUEUE_DELETE_HEADER records to display per page');

grant execute on global.global_pkg to joe;

alter table clean.queue_delete_header add (
email_body clob,
email_subject varchar2(1000))
lob (email_body) store as qdh_email_body_lob (tablespace clean_data);

create table clean.mqd_queue_type_val (
queue_type varchar2(20) not null,
description varchar2(100),
queue_indicator varchar2(20),
active varchar2(1) not null,
created_by varchar2(100) not null,
created_on date not null,
updated_by varchar2(100) not null,
updated_on date not null) tablespace clean_data;

alter table clean.mqd_queue_type_val add constraint mqd_queue_type_val_pk primary key (queue_type)
using index tablespace clean_indx;

alter table clean.mqd_queue_type_val add constraint mqd_queue_type_val_ck1 check (active in ('Y','N'));

create table frp.pgp_crypto_info (
PGP_KEY_CODE          VARCHAR2(50) not null,
PRIVATE_KEY_FILE_PATH VARCHAR2(1000) not null,
PASS_PHRASE           VARCHAR2(100) not null,
KEY_NAME              VARCHAR2(100) not null,
CREATED_ON            DATE not null,
CREATED_BY            VARCHAR2(100) not null,
UPDATED_ON            DATE not null,
UPDATED_BY            VARCHAR2(100) not null) tablespace frp_data;

alter table frp.pgp_crypto_info add constraint pgp_crypto_info_pk primary key (pgp_key_code) using index tablespace frp_indx;
SET ESCAPE ON; 
-- MY_BUYS FEED_ENABLED_FLAG (FOR BOTH QA AND PRODUCTION)
--
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, 
                             CREATED_BY, CREATED_ON, 
                             UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ('MY_BUYS', 'FEED_ENABLED_FLAG', 'Y',
        'DEFECT_7724_RELEASE_4_7_0', sysdate, 'DEFECT_7724_RELEASE_4_7_0', sysdate, 
        'Feed indicating if product unavailability feed to MyBuys is enabled');


-- MY_BUYS FEED_REQUEST_VALUE (FOR QA ONLY) !!!
--
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, 
                             CREATED_BY, CREATED_ON, 
                             UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ('MY_BUYS', 'FEED_REQUEST_URL', 'http://a.devss.mybuys.com/ws/stock/idos?client=FTD\&cpc=<productid>\&sku=<skuid>',
        'DEFECT_7724_RELEASE_4_7_0', sysdate, 'DEFECT_7724_RELEASE_4_7_0', sysdate, 
        'URL for product unavailability feed to MyBuys 
         (must contain <productid> and <skuid> tokens)');


-- MY_BUYS FEED_REQUEST_VALUE (FOR PRODUCTION ONLY) !!!
--
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, 
                             CREATED_BY, CREATED_ON, 
                             UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ('MY_BUYS', 'FEED_REQUEST_URL', 'http://a.p.mybuys.com/ws/stock/idos?client=FTD\&cpc=<productid>\&sku=<skuid>',
        'DEFECT_7724_RELEASE_4_7_0', sysdate, 'DEFECT_7724_RELEASE_4_7_0', sysdate, 
        'URL for product unavailability feed to MyBuys 
         (must contain <productid> and <skuid> tokens)');

INSERT INTO CLEAN.MQD_QUEUE_TYPE_VAL(QUEUE_TYPE,DESCRIPTION,QUEUE_INDICATOR,ACTIVE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON)
(SELECT QUEUE_TYPE, DESCRIPTION, QUEUE_INDICATOR, 'Y' AS ACTIVE, 'DEFECT_7419_RELEASE_4_7_0', SYSDATE, 'DEFECT_7419_RELEASE_4_7_0', SYSDATE FROM CLEAN.QUEUE_TYPE_VAL);

Insert into CLEAN.MQD_QUEUE_TYPE_VAL (QUEUE_TYPE,DESCRIPTION,QUEUE_INDICATOR,ACTIVE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON) 
values ('ASKP','ASKP','Mercury','Y','DEFECT_7419_RELEASE_4_7_0',SYSDATE,'DEFECT_7419_RELEASE_4_7_0',SYSDATE);

--Insert a new role
insert into aas.role(ROLE_ID,ROLE_NAME,CONTEXT_ID,DESCRIPTION,CREATED_ON,UPDATED_ON,UPDATED_BY) 
values (aas.role_id.nextval,'QueueDeleteManager','Order Proc','Queue Delete Management Role',sysdate,sysdate,
'DEFECT_7419_RELEASE_4_7_0');

--Insert a new resource
insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values 
('QueueBatchStatusByUser','Order Proc','Controls access to view batches submitted by all or selected user', sysdate, sysdate,
'DEFECT_7419_RELEASE_4_7_0');

--Insert a new ACL
insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL) 
values ('QueueDeleteManager', sysdate, sysdate, 'DEFECT_7419_RELEASE_4_7_0', null);

--Insert a new join on role and ACL
insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values 
((select role_id from aas.role where role_name='QueueDeleteManager'), 'QueueDeleteManager', sysdate, sysdate,
'DEFECT_7419_RELEASE_4_7_0');

--Insert a new join on acl, resource, context and permission
insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('QueueDeleteManager','QueueBatchStatusByUser','Order Proc','View',sysdate,sysdate,'DEFECT_7419_RELEASE_4_7_0');

Insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values
('QUEUE_CONFIG','MAX_ANSP_AMOUNT', '10' ,' DEFECT_7419_RELEASE_4_7_0', SYSDATE, 'DEFECT_7419_RELEASE_4_7_0' ,SYSDATE,
'The global parameter to restrict the maximum amount that can be specified for ANSP');
--The below statements should be run by OPS$ORACLE
--FOR QA:
    INSERT INTO FRP.PGP_CRYPTO_INFO(
         PGP_KEY_CODE, PRIVATE_KEY_FILE_PATH, PASS_PHRASE, KEY_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY
    )
    VALUES
    (
         'orderxml1',
         '/home/oracle/.ssh/orderxmltest-secret.asc',
         global.encryption.encrypt_it('orderxmlpcitest','APOLLO_TEST_2010'),
         'APOLLO_TEST_2010',
         sysdate,
         'arajoo',
         sysdate,
         'arajoo'
    );

--FOR PRODUCTION:
    INSERT INTO FRP.PGP_CRYPTO_INFO(
         PGP_KEY_CODE, PRIVATE_KEY_FILE_PATH, PASS_PHRASE, KEY_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY
    )
    VALUES
    (
         <Key Code>,
         <Path to Private Key File on Server>,
         global.encryption.encrypt_it(<Passphrase>,<Key Name For Column Encryption>),
         <Key Name For Column Encryption>,
         sysdate,
         <user>,
         sysdate,
         <user>'
    );
--END OF 4.7.0.2

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('ORDER_SERVICE_CONFIG','GET_PRODUCT_BY_CATEGORY_MAX_COUNT','100','Defect_6414',sysdate,'Defect_6414',sysdate,
'Max number of products allowed in getCategory request.');
--END OF 4.7.0.3

delete from ftd_apps.order_service_client_xsl
where client_id='FTD'
and xsl_code='Identity Transformation';
--END OF 4.7.0.4
