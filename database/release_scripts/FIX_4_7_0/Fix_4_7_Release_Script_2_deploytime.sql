ALTER TABLE FTD_APPS.UPSELL_MASTER MODIFY UPSELL_MASTER_ID varchar2(10);

ALTER TABLE FTD_APPS.UPSELL_MASTER$ MODIFY UPSELL_MASTER_ID varchar2(10);

ALTER TABLE FTD_APPS.UPSELL_DETAIL MODIFY 
(UPSELL_MASTER_ID varchar2(10), UPSELL_DETAIL_ID varchar2(10));

ALTER TABLE FTD_APPS.UPSELL_DETAIL$ MODIFY 
(UPSELL_MASTER_ID varchar2(10), UPSELL_DETAIL_ID varchar2(10));

alter table joe.iotw modify (upsell_master_id varchar2(10));

alter table joe.iotw$ modify (upsell_master_id varchar2(10));

alter table joe.cross_sell_master modify (upsell_master_id varchar2(10));

alter table joe.cross_sell_master$ modify (upsell_master_id varchar2(10));

alter table ftd_apps.upsell_source modify (upsell_master_id varchar2(10));

alter table clean.queue add (
PRICE_FTD        NUMBER(10,2),
PRICE_ASKP       NUMBER(10,2),
MERCURY_COMMENTS VARCHAR2(4000));

alter table clean.queue$ add (
PRICE_FTD        NUMBER(10,2),
PRICE_ASKP       NUMBER(10,2),
MERCURY_COMMENTS VARCHAR2(4000));

alter table clean.queue_delete_history add (
PRICE_FTD        NUMBER(10,2),
PRICE_ASKP       NUMBER(10,2),
MERCURY_COMMENTS VARCHAR2(4000));

comment on column clean.queue.PRICE_FTD is 'data from Price column of mercury.mercury';
comment on column clean.queue.PRICE_ASKP is 'data from Price column of mercury.mercury';
comment on column clean.queue.MERCURY_COMMENTS is 'Comments is data from comments column of mercury.mercury';

alter table clean.order_details drop constraint order_details_ck4;

alter table clean.order_details add constraint order_details_ck4 check (
delivery_confirmation_status in (null,'Pending','Sent','Queued','Confirmed','No Email'));

grant select, references on frp.address_types to ftd_apps;
grant select, references on frp.address_types to joe;

ALTER TABLE FTD_APPS.SOURCE_MASTER ADD (
RECPT_LOCATION_TYPE         VARCHAR2(40),
RECPT_BUSINESS_NAME         VARCHAR2(50),
RECPT_LOCATION_DETAIL       VARCHAR2(20),
RECPT_ADDRESS               VARCHAR2(90),
RECPT_ZIP_CODE              VARCHAR2(12),
RECPT_CITY                  VARCHAR2(30),
RECPT_STATE_ID              VARCHAR2(2),
RECPT_COUNTRY_ID            VARCHAR2(2),
RECPT_PHONE                 VARCHAR2(20),
RECPT_PHONE_EXT             VARCHAR2(10), 
CUST_FIRST_NAME             VARCHAR2(25),
CUST_LAST_NAME              VARCHAR2(25),
CUST_DAYTIME_PHONE          VARCHAR2(20),
CUST_DAYTIME_PHONE_EXT      VARCHAR2(10),
CUST_EVENING_PHONE          VARCHAR2(20),
CUST_EVENING_PHONE_EXT      VARCHAR2(10),
CUST_ADDRESS                VARCHAR2(90),
CUST_ZIP_CODE               VARCHAR2(12),
CUST_CITY                   VARCHAR2(30),
CUST_STATE_ID               VARCHAR2(2),
CUST_COUNTRY_ID             VARCHAR2(2),
CUST_EMAIL_ADDRESS          VARCHAR2(100),
CONSTRAINT SOURCE_MASTER_FK6 FOREIGN KEY(RECPT_LOCATION_TYPE) REFERENCES FRP.ADDRESS_TYPES(ADDRESS_TYPE),
CONSTRAINT SOURCE_MASTER_FK7 FOREIGN KEY(RECPT_STATE_ID) REFERENCES FTD_APPS.STATE_MASTER(STATE_MASTER_ID),
CONSTRAINT SOURCE_MASTER_FK8 FOREIGN KEY(CUST_STATE_ID) REFERENCES FTD_APPS.STATE_MASTER(STATE_MASTER_ID));

COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_LOCATION_TYPE      IS 'Default Order Info Recepient Location Type';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_BUSINESS_NAME      IS 'Default Order Info Recepient Business Name';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_LOCATION_DETAIL    IS 'Default Order Info Room, Ward, Gravesite or other information';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_ADDRESS            IS 'Default Order Info Recepient Address';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_ZIP_CODE           IS 'Default Order Info Recepient Zip Code';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_CITY               IS 'Default Order Info Recepient City';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_STATE_ID           IS 'Default Order Info Recepient State';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_COUNTRY_ID         IS 'Default Order Info Recepient Country';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_PHONE              IS 'Default Order Info Recepient Phone';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.RECPT_PHONE_EXT          IS 'Default Order Info Recepient Phone Extension';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_FIRST_NAME          IS 'Default Order Info Customer First Name';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_LAST_NAME           IS 'Default Order Info Customer Last Name';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_DAYTIME_PHONE       IS 'Default Order Info Customer Daytime Phone';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_DAYTIME_PHONE_EXT   IS 'Default Order Info Customer Daytime Phone Extension';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_EVENING_PHONE       IS 'Default Order Info Customer Evening Phone';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_EVENING_PHONE_EXT   IS 'Default Order Info Customer Evening Phone Extension';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_ADDRESS             IS 'Default Order Info Customer Address';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_ZIP_CODE            IS 'Default Order Info Customer Zip Code';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_CITY                IS 'Default Order Info Customer City';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_STATE_ID            IS 'Default Order Info Customer State';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_COUNTRY_ID          IS 'Default Order Info Customer Country';
COMMENT ON COLUMN FTD_APPS.SOURCE_MASTER.CUST_EMAIL_ADDRESS       IS 'Default Order Info Customer Email Address';

alter table FRP.ADDRESS_TYPES add (
  BUSINESS_LABEL_TXT VARCHAR2(250), 
  PROMPT_FOR_LOOKUP_FLAG VARCHAR2(1) DEFAULT 'N' NOT NULL,
  LOOKUP_LABEL_TXT VARCHAR2(250),
  DEFAULT_FLAG VARCHAR2(1));

alter table frp.address_types add CONSTRAINT ADDRESS_TYPES_CK5 CHECK(PROMPT_FOR_LOOKUP_FLAG IN ('Y','N'));
alter table frp.address_types add CONSTRAINT ADDRESS_TYPES_CK6 CHECK(DEFAULT_FLAG IN (null,'Y'));

update FRP.ADDRESS_TYPES
set BUSINESS_LABEL_TXT='Business Name', PROMPT_FOR_LOOKUP_FLAG='N'
where ADDRESS_TYPE='BUSINESS';

update FRP.ADDRESS_TYPES
set BUSINESS_LABEL_TXT='Funeral', PROMPT_FOR_LOOKUP_FLAG='Y', LOOKUP_LABEL_TXT='Funeral home search'
where ADDRESS_TYPE='FUNERAL HOME';

update FRP.ADDRESS_TYPES
set PROMPT_FOR_LOOKUP_FLAG='N', DEFAULT_FLAG='Y'
where ADDRESS_TYPE='HOME';

update FRP.ADDRESS_TYPES
set BUSINESS_LABEL_TXT='Hospital Name', PROMPT_FOR_LOOKUP_FLAG='Y', LOOKUP_LABEL_TXT='Hospital search'
where ADDRESS_TYPE='HOSPITAL';

update FRP.ADDRESS_TYPES
set BUSINESS_LABEL_TXT='Nursing Home', PROMPT_FOR_LOOKUP_FLAG='Y', LOOKUP_LABEL_TXT='Nursing home search'
where ADDRESS_TYPE='NURSING HOME';

update FRP.ADDRESS_TYPES
set PROMPT_FOR_LOOKUP_FLAG='N'
where ADDRESS_TYPE='OTHER';

update FRP.ADDRESS_TYPES
set BUSINESS_LABEL_TXT='Cemetery', PROMPT_FOR_LOOKUP_FLAG='Y', LOOKUP_LABEL_TXT='Cemetery search'
where ADDRESS_TYPE='CEMETERY';

