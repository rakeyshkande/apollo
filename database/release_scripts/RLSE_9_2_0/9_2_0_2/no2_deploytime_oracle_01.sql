------------------------------------------------------------------------------------
-- Begin requested by Manasa s                                   8/10/2016  --------
-- defect Q4SP16-11           (Pavan   ) (216309)       ----------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE
 ALTER TABLE FTD_APPS.PRODUCT_MASTER ADD ALL_ALPHA_FLAG VARCHAR2(1);
COMMENT ON COLUMN FTD_APPS.PRODUCT_MASTER.ALL_ALPHA_FLAG IS 'flag to check if the personalization data should include only alpha characters or not';
ALTER TABLE FTD_APPS.PRODUCT_MASTER$ ADD ALL_ALPHA_FLAG VARCHAR2(1);

------------------------------------------------------------------------------------
-- End   requested by Manasa s                                   8/10/2016  --------
-- defect Q4SP16-11           (Pavan   ) (216309)       ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-2           (Mark   ) (218302)            ----------------------------
------------------------------------------------------------------------------------

delete from ftd_apps.florist_zips fz where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fz.florist_id and fm.status = 'Inactive');

delete from ftd_apps.florist_blocks fb where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fb.florist_id and fm.status = 'Inactive');

delete from ftd_apps.florist_suspends fs where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fs.florist_id and fm.status = 'Inactive');

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-2           (Mark   ) (218302)            ----------------------------
------------------------------------------------------------------------------------
