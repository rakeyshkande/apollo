------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-5               (Mark    ) (218302)       ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ( 'UPDATE_FLORIST_SUSPEND', 'BUFFER_MINUTES', '30', 'FAM-5', sysdate, 'FAM-5', sysdate,
'The number of minutes before a suspend start time that it becomes active');

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-5               (Mark    ) (218302)       ----------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-5               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------
alter table ftd_apps.phoenix_email_types add (display_order number);

update ftd_apps.phoenix_email_types set display_order = 1 where email_type_key = 'doc_email';
update ftd_apps.phoenix_email_types set display_order = 2 where email_type_key = 'apology_email';
update ftd_apps.phoenix_email_types set display_order = 3 where email_type_key = 'none';
update ftd_apps.phoenix_email_types set display_order = 4 where email_type_key = 'phoenix_email';
update ftd_apps.phoenix_email_types set display_order = 5 where email_type_key = 'stock_email';


------------------------------------------------------------------------------------
-- End   requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-5               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-3               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------

INSERT INTO QUARTZ_SCHEMA.Pipeline (
PIPELINE_ID,         
GROUP_ID,            
DESCRIPTION,
TOOL_TIP,     
DEFAULT_SCHEDULE,
DEFAULT_PAYLOAD,  
FORCE_DEFAULT_PAYLOAD,
QUEUE_NAME,
CLASS_CODE,        
ACTIVE_FLAG)
VALUES (
'BULK PHOENIX',
'OP',
'Bulk Phoenix Eligible Orders',
'Creates an OJMS.PHOENIX JMS message for Bulk processing the Phoenix eligible orders',
'0 50 * * * ?',
'BulkPhoenix',
'Y',
'OJMS.PHOENIX',
'SimpleJmsJob',
'Y');

INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
   CREATED_ON,
   CREATED_BY,
   UPDATED_BY,
   UPDATED_ON,               
   DESCRIPTION)
VALUES (
    'PHOENIX',
    'PHOENIX_ELIGIBLE_QUEUES',
    'FTD REJ ZIP',
    sysdate,
    'PL-3',
    'PL-3',
    sysdate,
    'Global parm indicating the queue types from where bulk orders are picked for phoenix processing.');
                
                



------------------------------------------------------------------------------------
-- End   requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-3               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------
