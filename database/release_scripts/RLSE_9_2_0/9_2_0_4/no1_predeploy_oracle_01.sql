------------------------------------------------------------------------------------
-- Begin requested by Manasa S                               9/15/2016  --------
-- defect Q4SP16-12              (Pavan    ) (225736)       ----------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE  
ALTER TABLE CLEAN.CUSTOMER ADD VIP_CUSTOMER VARCHAR2(1);
GRANT SELECT ON CLEAN.CUSTOMER TO SCRUB;

--1. Insert a new role
INSERT INTO AAS.ROLE (
 ROLE_ID,
 ROLE_NAME,
 CONTEXT_ID,
 DESCRIPTION,
 CREATED_ON,
 UPDATED_ON,
 UPDATED_BY) 
VALUES (
 aas.role_id.nextval, 
 'VIP Modify',
 'Order Proc',
 'ONLY this role has the ability to mark/unmark customers as VIP.',
 sysdate,
 sysdate,
 'Q4SP16-12');

--2. Insert a new resource
INSERT INTO AAS.RESOURCES (
 RESOURCE_ID,
 CONTEXT_ID,
 DESCRIPTION,
 CREATED_ON,
 UPDATED_ON,
 UPDATED_BY) 
VALUES (
 'VIP Customer Update',
 'Order Proc',
 'View- Ability to view the "VIP" label in the Update Customer Account page when clicked on the Update Customer link on Customer Account page. Where:FTD\customer_order_management\web\xsl\updateCustomerAccount.xsl',
 sysdate,
 sysdate,
 'Q4SP16-12');

-- 3.Insert a new ACL (Resource Group)
INSERT INTO AAS.ACL (
 ACL_NAME,
 CREATED_ON,
 UPDATED_ON,
 UPDATED_BY,
 ACL_LEVEL) 
VALUES (
 'VIP Updates',
 sysdate,
 sysdate,
 'Q4SP16-12',
 null);

-- 4.Insert a new join on role and ACL
INSERT INTO AAS.REL_ROLE_ACL (
 ROLE_ID,
 ACL_NAME,
 CREATED_ON,
 UPDATED_ON,
 UPDATED_BY) 
VALUES (
 (select role_id from aas.role where role_name='VIP Modify'),
 'VIP Updates',
 sysdate,
 sysdate,
 'Q4SP16-12');

-- 5.Insert a new join on acl, resource, context and permission
INSERT INTO AAS.REL_ACL_RP (
 ACL_NAME,
 RESOURCE_ID,
 CONTEXT_ID,
 PERMISSION_ID,
 CREATED_ON,
 UPDATED_ON,
 UPDATED_BY) 
VALUES (
 'VIP Updates',
 'VIP Customer Update',
 'Order Proc',
 'Yes',
 sysdate,
 sysdate,
 'Q4SP16-12');

INSERT INTO FTD_APPS.content_master(
 CONTENT_MASTER_ID,
 CONTENT_CONTEXT,
 CONTENT_NAME,
 CONTENT_DESCRIPTION,
 CONTENT_FILTER_ID1,
 CONTENT_FILTER_ID2,
 UPDATED_BY,
 UPDATED_ON,
 CREATED_BY,
 CREATED_ON) 
VALUES (
 ftd_apps.content_master_sq.nextval,
 'VIP_CUSTOMER',
 'CUSTOMER_ORDER_MANAGEMENT',
 'Message to display when the CSR accesses the VIP customer orders',
 NULL,
 NULL,
 'Q4SP16-12',
 SYSDATE,
 'Q4SP16-12',
 SYSDATE
 );

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
 ftd_apps.content_detail_sq.nextval,
 (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'VIP_CUSTOMER'
 AND CONTENT_NAME = 'CUSTOMER_ORDER_MANAGEMENT'),
 'VIP_CUSTOMER_UPDATE_DENIED_MESSAGE',
 NULL,
 'This customer is a VIP. Please have a manager alert the VIP team. Agent- Please do not make any changes to the order.',
 'Q4SP16-12',
 SYSDATE,
 'Q4SP16-12',
 SYSDATE
    );

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
 ftd_apps.content_detail_sq.nextval,
 (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
 WHERE CONTENT_CONTEXT = 'VIP_CUSTOMER'
 AND CONTENT_NAME = 'CUSTOMER_ORDER_MANAGEMENT'),
 'VIP_CUSTOMER_MESSAGE',
 NULL,
 'This customer is a VIP. Please do not make any changes to the order.',
 'Q4SP16-12',
 SYSDATE,
 'Q4SP16-12',
 SYSDATE
    );

------------------------------------------------------------------------------------
-- End   requested by ManasaS                                 9/15/2016  --------
-- defect Q4SP16-12              (Pavan     ) (225736)       ----------------------------
------------------------------------------------------------------------------------
