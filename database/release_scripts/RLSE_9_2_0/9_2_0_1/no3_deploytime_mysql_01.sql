------------------------------------------------------------------------------------
-- Begin requested by Mark Campbell                              8/22/2016  --------
-- defect FAM-13              (Mark    ) (219317)       ----------------------------
------------------------------------------------------------------------------------

use dba;
alter table capacity_for_map add column REJECT_COUNT decimal(10,0) default null;
alter table capacity_for_map_1 add column REJECT_COUNT decimal(10,0) default null;

------------------------------------------------------------------------------------
-- End   requested by Mark Campbell                              8/22/2016  --------
-- defect FAM-13              (Mark    ) (219317)       ----------------------------
------------------------------------------------------------------------------------

