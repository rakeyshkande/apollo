------------------------------------------------------------------------------------
-- Begin requested by Gunaddep B                                8/30/2016  --------
-- defect PL-3              (Pavan    ) (221264)       ----------------------------
------------------------------------------------------------------------------------
 INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
                CREATED_ON,
                CREATED_BY,
                UPDATED_BY,
                UPDATED_ON,  
                DESCRIPTION)
VALUES (
    'PHOENIX',
    'BULK_PHOENIX_RUNNING_STATUS',
    'N',
    sysdate,
    'PL-3',
    'PL-3',
    sysdate,
    'Global parm displaying Y if Bulk Phoenix processing is already running or N if not.');
------------------------------------------------------------------------------------
-- End   requested by Gunadeep B                                 8/30/2016  --------
-- defect PL-3               (Pavan    ) (221264)       ----------------------------
------------------------------------------------------------------------------------

    
------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 9/ 1/2016  --------
-- defect FAM-1              (Mark     ) (222136)       ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION)
values (
    'FTDAPPS_PARMS',
    'CSZ_AVAIL_ON',
    'Y',
    'FAM-1',
    sysdate,
    'FAM-1',
    sysdate,
    'If this is set to N, the events.florist_enqueue procedure will not post a record to OJMS.FLORIST');

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 9/ 1/2016  --------
-- defect FAM-1              (Mark     ) (222136)       ----------------------------
------------------------------------------------------------------------------------
