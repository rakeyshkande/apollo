------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-5               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------
alter table ftd_apps.phoenix_email_types add (display_order number);

update ftd_apps.phoenix_email_types set display_order = 1 where email_type_key = 'doc_email';
update ftd_apps.phoenix_email_types set display_order = 2 where email_type_key = 'apology_email';
update ftd_apps.phoenix_email_types set display_order = 3 where email_type_key = 'none';
update ftd_apps.phoenix_email_types set display_order = 4 where email_type_key = 'phoenix_email';
update ftd_apps.phoenix_email_types set display_order = 5 where email_type_key = 'stock_email';


------------------------------------------------------------------------------------
-- End   requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-5               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- Begin requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-3               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------

INSERT INTO QUARTZ_SCHEMA.Pipeline (
PIPELINE_ID,         
GROUP_ID,            
DESCRIPTION,
TOOL_TIP,     
DEFAULT_SCHEDULE,
DEFAULT_PAYLOAD,  
FORCE_DEFAULT_PAYLOAD,
QUEUE_NAME,
CLASS_CODE,        
ACTIVE_FLAG)
VALUES (
'BULK PHOENIX',
'OP',
'Bulk Phoenix Eligible Orders',
'Creates an OJMS.PHOENIX JMS message for Bulk processing the Phoenix eligible orders',
'0 50 * * * ?',
'BulkPhoenix',
'Y',
'OJMS.PHOENIX',
'SimpleJmsJob',
'Y');

INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
   CREATED_ON,
   CREATED_BY,
   UPDATED_BY,
   UPDATED_ON,               
   DESCRIPTION)
VALUES (
    'PHOENIX',
    'PHOENIX_ELIGIBLE_QUEUES',
    'FTD REJ ZIP',
    sysdate,
    'PL-3',
    'PL-3',
    sysdate,
    'Global parm indicating the queue types from where bulk orders are picked for phoenix processing.');
                
                



------------------------------------------------------------------------------------
-- End   requested by Gunadeep B                                 8/26/2016  --------
-- defect PL-3               (Pavan    ) (220443)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Manasa s                                   8/10/2016  --------
-- defect Q4SP16-11           (Pavan   ) (216309)       ----------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE
 ALTER TABLE FTD_APPS.PRODUCT_MASTER ADD ALL_ALPHA_FLAG VARCHAR2(1);
COMMENT ON COLUMN FTD_APPS.PRODUCT_MASTER.ALL_ALPHA_FLAG IS 'flag to check if the personalization data should include only alpha characters or not';
ALTER TABLE FTD_APPS.PRODUCT_MASTER$ ADD ALL_ALPHA_FLAG VARCHAR2(1);

------------------------------------------------------------------------------------
-- End   requested by Manasa s                                   8/10/2016  --------
-- defect Q4SP16-11           (Pavan   ) (216309)       ----------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-2           (Mark   ) (218302)            ----------------------------
------------------------------------------------------------------------------------

delete from ftd_apps.florist_zips fz where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fz.florist_id and fm.status = 'Inactive');

delete from ftd_apps.florist_blocks fb where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fb.florist_id and fm.status = 'Inactive');

delete from ftd_apps.florist_suspends fs where exists (
select 'Y' from ftd_apps.florist_master fm where fm.florist_id = fs.florist_id and fm.status = 'Inactive');

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 8/17/2016  --------
-- defect FAM-2           (Mark   ) (218302)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Manasa S                               9/15/2016  --------
-- defect Q4SP16-12              (Pavan    ) (225736)       ----------------------------
------------------------------------------------------------------------------------

-- THIS IS A GOLDEN GATE CHANGE  
ALTER TABLE CLEAN.CUSTOMER ADD VIP_CUSTOMER VARCHAR2(1);
GRANT SELECT ON CLEAN.CUSTOMER TO SCRUB;

------------------------------------------------------------------------------------
-- End   requested by Manasa S                               9/15/2016  --------
-- defect Q4SP16-12              (Pavan    ) (225736)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                   9/30/2016  --------
-- defect Q4SP16-4           (Syed   ) (231057)       ----------------------------
------------------------------------------------------------------------------------
 
update quartz_schema.pipeline
set tool_tip = 'Process a staged message in MERCURY.MERCURY_EAPI_MSGS_STAGING. The payload is the MESSAGE_ID.'
where group_id = 'MERCURY_EAPI'
and pipeline_id = 'MERCURY_EAPI_PROCESS_POLL';


------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 9/30/2016  --------
-- defect Q4SP16-4           (Syed   ) (231057)            ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Konduri DharmaTeja                                   10/07/2016  --------
-- Ariba Scrub CR - ASE-1,2           (bsurimen) (233344)       ----------------------------
------------------------------------------------------------------------------------

update Frp.Global_Parms 
set Value=Concat(Value,concat(',','SCRUB_ADJUSTED')), updated_by='SYSTEM', updated_on=sysdate
where Context='PI_CONFIG' And Name ='ORD_STAT_TO_PROCESS';

-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add is_incr_total_allowed char(1) default 'Y';
update ptn_pi.partner_mapping set is_incr_total_allowed='N' where  default_source_code='77806';

------------------------------------------------------------------------------------
-- End   requested by Konduri DharmaTeja                                  10/07/2016  --------
-- Ariba Scrub CR - ASE-1,2          (bsurimen) (233344)       ----------------------------
------------------------------------------------------------------------------------

