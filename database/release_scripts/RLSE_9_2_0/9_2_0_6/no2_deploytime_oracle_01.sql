------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                   9/30/2016  --------
-- defect Q4SP16-4           (Syed   ) (231057)       ----------------------------
------------------------------------------------------------------------------------
 
update quartz_schema.pipeline
set tool_tip = 'Process a staged message in MERCURY.MERCURY_EAPI_MSGS_STAGING. The payload is the MESSAGE_ID.'
where group_id = 'MERCURY_EAPI'
and pipeline_id = 'MERCURY_EAPI_PROCESS_POLL';


------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                 9/30/2016  --------
-- defect Q4SP16-4           (Syed   ) (231057)            ----------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin requested by Konduri DharmaTeja                                   10/07/2016  --------
-- Ariba Scrub CR - ASE-1,2           (bsurimen) (233344)       ----------------------------
------------------------------------------------------------------------------------

update Frp.Global_Parms 
set Value=Concat(Value,concat(',','SCRUB_ADJUSTED')), updated_by='SYSTEM', updated_on=sysdate
where Context='PI_CONFIG' And Name ='ORD_STAT_TO_PROCESS';

-- THIS IS A GOLDEN GATE CHANGE
alter table ptn_pi.partner_mapping add is_incr_total_allowed char(1) default 'Y';
update ptn_pi.partner_mapping set is_incr_total_allowed='N' where  default_source_code='77806';

------------------------------------------------------------------------------------
-- End   requested by Konduri DharmaTeja                                  10/07/2016  --------
-- Ariba Scrub CR - ASE-1,2          (bsurimen) (233344)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Mark Woytus                               10/31/2016  --------
-- Purge Fix - SP-252         (mwoytus)  (240988)       ----------------------------
------------------------------------------------------------------------------------

create index ptn_pi.partner_order_n1 on ptn_pi.partner_order(partner_order_feed_id) tablespace pi_indx online;

create index ptn_pi.order_status_update_n1 on ptn_pi.order_status_update(feed_id) tablespace pi_indx online;

create index ptn_pi.order_status_update_n2 on ptn_pi.order_status_update(confirmation_number) tablespace pi_indx online;

------------------------------------------------------------------------------------
-- END   requested by Mark Woytus                               10/31/2016  --------
-- Purge Fix - SP-252         (mwoytus)  (240988)       ----------------------------
------------------------------------------------------------------------------------
