------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175803 )          --------------------------------
-----------------------------------------------------------------------------------
Create user mars identified by &password account lock default tablespace FTD_APPS_DATA temporary tablespace TEMP3 profile default;
grant create session to mars;

--- MARS user is already present on lower environments . please skip above step if the user already present.

CREATE SEQUENCE MARS.TEMPLATE_HIST_ID_SQ MINVALUE 1 INCREMENT BY 1 START WITH 1 CACHE 200 NOORDER NOCYCLE;
CREATE SEQUENCE MARS.RESULT_ID_SQ MINVALUE 1 INCREMENT BY 1 START WITH 1 CACHE 200 NOORDER NOCYCLE;

CREATE TABLE MARS.SIMULATION_RESULTS (RESULT_ID NUMBER NOT NULL PRIMARY KEY, VERSION_NO VARCHAR2(20), INPUT BLOB, 
OUTPUT BLOB, STATUS VARCHAR2(20), ERROR_MESSAGE VARCHAR2(400), CREATED_ON DATE, CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));

CREATE TABLE MARS.SIMULATOR_TEMPLATES (VERSION_NO VARCHAR2(20) NOT NULL PRIMARY KEY, TEMPLATE BLOB, CREATED_ON DATE, 
CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));

CREATE TABLE MARS.SIMULATOR_TEMPLATES_HIST (TEMPLATE_HIST_ID NUMBER NOT NULL PRIMARY KEY, VERSION_NO VARCHAR2(20) NOT NULL, TEMPLATE BLOB,
CREATED_ON DATE, CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));



set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.MARS_SIMULATOR',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'MARS_SIMULATOR (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS MARS_SIMULATORlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.MARS_SIMULATOR',
      queue_table => 'OJMS.MARS_SIMULATOR',
      max_retries => 3,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.MARS_SIMULATOR');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'OSP_QUEUE');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'EVENTS');


connect /


------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175803 )          --------------------------------
------------------------------------------------------------------------------------


