------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               12/15/2015  --------
-- defect QE-97         (Syed  ) (175238 )          --------------------------------
------------------------------------------------------------------------------------

alter table clean.order_florist_used drop column FLORIST_SELECTION_LOG_ID;

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                               12/15/2015  --------
-- defect QE-97         (Syed  ) (175238 )          --------------------------------
------------------------------------------------------------------------------------





------------------------------------------------------------------------------------
-- begin requested by Gunadeep B,                               12/17/2015  --------
-- defect RS-7         (Pavan  ) (175830 )          --------------------------------
------------------------------------------------------------------------------------

update TAX.STATE_COMPANY_TAX_CONTROLS set company_id = 'ROSES' where company_id = 'FLYFLW';

------------------------------------------------------------------------------------
-- End requested by Gunadeep B,                               12/17/2015  --------
-- defect RS-7         (Pavan  ) (175830 )          --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/17/2015  --------
-- defect QE-85         (Pavan  ) (175832 )          --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'SHOW_SIMULATION_RESULTS_COUNT', '10', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Param to limit the Simulation Results on the Simulator page');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'TEMP_UPLOAD_DIRECTORY', '/u02/Upload', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Temp directory for staging the input excel file');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'TEMP_DOWNLOAD_DIRECTORY', '/u02/Download', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Temp directory for staging the output excel file');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'EXCEL_ROW_NUM', '9', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'First position of the input data in the excel');

------------------------------------------------------------------------------------
-- End requested by Karthik D,                               12/17/2015  --------
-- defect QE-85         (Pavan  ) (175832 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/17/2015  --------
-- defect RS-4          (Mark  ) (175906 )          --------------------------------
------------------------------------------------------------------------------------

insert into ftd_apps.SOURCE_TYPE_VAL (SOURCE_TYPE, STATUS, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON) 
values ('ROSES', 'Active', sysdate, 'RS-4', 'RS-4', sysdate);

update ftd_apps.source_master
set source_type = 'ROSES'
where source_type = 'ROSES.COM';

delete from ftd_apps.source_type_val
where source_type = 'ROSES.COM';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/17/2015  --------
-- defect RS-4          (Mark  ) (175906 )          --------------------------------
------------------------------------------------------------------------------------






------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/18/2015  --------
-- defect QE-85          (Pavan  ) (176057 )          --------------------------------
------------------------------------------------------------------------------------

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_SIMULATOR','Order Proc','Controls access to view/update MARS Simulator', sysdate, sysdate, 
'QE-85');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_SIMULATOR_URL CSR', sysdate, sysdate, 'QE-85', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_SIMULATOR_URL CSR','MARS_SIMULATOR','Order Proc','View',sysdate,sysdate,'QE-85');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_SIMULATOR_URL CSR' , 'Order Proc', 'MARS_SIMULATOR_URL CSR View', sysdate, sysdate, 'QE-85' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_SIMULATOR_URL CSR'),
    'MARS_SIMULATOR_URL CSR', sysdate, sysdate, 'SIMULATOR');
    
    ------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/18/2015  --------
-- defect QE-85          (Pavan  ) (176057 )          --------------------------------
------------------------------------------------------------------------------------
    
------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------

-- DNIS
insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3984, 'Roses.com', 'ROSESP', 'A', 'Y', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3985, 'Roses.com', 'ROSESP', 'A', 'N', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3986, 'Roses.com', 'ROSESP', 'A', 'N', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');


-- COMPANY
update ftd_apps.company
set default_dnis_id = '3984'
where company_id in ('ROSES', 'ROSESP');


-- COMPANY_MASTER
update ftd_apps.company_master
set phone_number = '877-315-6315'
where company_id = 'ROSES';


-- GLOBAL_PARMS
update frp.global_parms
set value = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where context = 'ACCOUNTING_CONFIG'
and name = 'MERCHANT_CITY_ROSES';


-- CONTENT_DETAIL
update ftd_apps.content_detail
set content_txt = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where content_master_id = (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
    WHERE CONTENT_CONTEXT = 'LANGUAGE'
    AND CONTENT_NAME = 'PHONE_NUMBER')
and filter_1_value = 'ROSES';

update ftd_apps.content_detail
set content_txt = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where content_master_id = (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
    WHERE CONTENT_CONTEXT = 'LANGUAGE'
    AND CONTENT_NAME = 'PHONE_NUMBER2')
and filter_1_value = 'ROSES';


-- MESSAGE_TOKENS
update clean.message_tokens
set token_value = 'If you would like to speak with a Customer Service Representative, please email us by clicking on this link http://custserv.Roses.com, or dial 1-877-315-6315. We are here to assist you 24 hours a day, 7 days a week.'
where token_id = 'submsg2'
and company_id = 'ROSES'
and token_type = 'text';


-- EMAIL_COMPANY_DATA
update ftd_apps.email_company_data
set value = '877-315-6315'
where company_id = 'ROSES'
and name = 'Phone';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------
