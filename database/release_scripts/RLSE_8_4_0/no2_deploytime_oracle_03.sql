------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               12/15/2015  --------
-- defect QE-97         (Syed  ) (175238 )          --------------------------------
------------------------------------------------------------------------------------

alter table clean.order_florist_used drop column FLORIST_SELECTION_LOG_ID;

------------------------------------------------------------------------------------
-- End requested by Rose Lazuk,                               12/15/2015  --------
-- defect QE-97         (Syed  ) (175238 )          --------------------------------
------------------------------------------------------------------------------------





------------------------------------------------------------------------------------
-- begin requested by Gunadeep B,                               12/17/2015  --------
-- defect RS-7         (Pavan  ) (175830 )          --------------------------------
------------------------------------------------------------------------------------

update TAX.STATE_COMPANY_TAX_CONTROLS set company_id = 'ROSES' where company_id = 'FLYFLW';

------------------------------------------------------------------------------------
-- End requested by Gunadeep B,                               12/17/2015  --------
-- defect RS-7         (Pavan  ) (175830 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/17/2015  --------
-- defect RS-4          (Mark  ) (175906 )          --------------------------------
------------------------------------------------------------------------------------

insert into ftd_apps.SOURCE_TYPE_VAL (SOURCE_TYPE, STATUS, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON) 
values ('ROSES', 'Active', sysdate, 'RS-4', 'RS-4', sysdate);

update ftd_apps.source_master
set source_type = 'ROSES'
where source_type = 'ROSES.COM';

delete from ftd_apps.source_type_val
where source_type = 'ROSES.COM';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/17/2015  --------
-- defect RS-4          (Mark  ) (175906 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/18/2015  --------
-- defect QE-85          (Pavan  ) (176057 )          --------------------------------
------------------------------------------------------------------------------------

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values
('MARS_SIMULATOR','Order Proc','Controls access to view/update MARS Simulator', sysdate, sysdate, 
'QE-85');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL)
values ('MARS_SIMULATOR_URL CSR', sysdate, sysdate, 'QE-85', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('MARS_SIMULATOR_URL CSR','MARS_SIMULATOR','Order Proc','View',sysdate,sysdate,'QE-85');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values
(aas.role_id.nextval, 'MARS_SIMULATOR_URL CSR' , 'Order Proc', 'MARS_SIMULATOR_URL CSR View', sysdate, sysdate, 'QE-85' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values
((select role_id from aas.role where role_name = 'MARS_SIMULATOR_URL CSR'),
    'MARS_SIMULATOR_URL CSR', sysdate, sysdate, 'SIMULATOR');
    
------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/18/2015  --------
-- defect QE-85          (Pavan  ) (176057 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 12/24/2015  --------
-- defect QE-105        (Pavan ) (177480 )          --------------------------------
-----------------------------------------------------------------------------------

UPDATE AAS.ROLE SET ROLE_NAME = 'MARS_SIMULATOR_CSR' WHERE 
ROLE_ID = (SELECT ROLE_ID FROM AAS.ROLE WHERE ROLE_NAME = 'MARS_SIMULATOR_URL CSR');

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 12/24/2015  --------
-- defect QE-105        (Pavan ) (177480 )          --------------------------------
------------------------------------------------------------------------------------

    
------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------

-- DNIS
insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3984, 'Roses.com', 'ROSESP', 'A', 'Y', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3985, 'Roses.com', 'ROSESP', 'A', 'N', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE,
    DEFAULT_SOURCE_CODE, SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
values (3986, 'Roses.com', 'ROSESP', 'A', 'N', 'N', 'DG', '17677', 'ROSES', 'ROSES', 'Default');


-- COMPANY
update ftd_apps.company
set default_dnis_id = '3984'
where company_id in ('ROSES', 'ROSESP');


-- COMPANY_MASTER
update ftd_apps.company_master
set phone_number = '877-315-6315'
where company_id = 'ROSES';

-- CONTENT_DETAIL
update ftd_apps.content_detail
set content_txt = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where content_master_id = (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
    WHERE CONTENT_CONTEXT = 'LANGUAGE'
    AND CONTENT_NAME = 'PHONE_NUMBER')
and filter_1_value = 'ROSES';

update ftd_apps.content_detail
set content_txt = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where content_master_id = (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
    WHERE CONTENT_CONTEXT = 'LANGUAGE'
    AND CONTENT_NAME = 'PHONE_NUMBER2')
and filter_1_value = 'ROSES';


-- MESSAGE_TOKENS
update clean.message_tokens
set token_value = 'If you would like to speak with a Customer Service Representative, please email us by clicking on this link http://custserv.Roses.com, or dial 1-877-315-6315. We are here to assist you 24 hours a day, 7 days a week.'
where token_id = 'submsg2'
and company_id = 'ROSES'
and token_type = 'text';


-- EMAIL_COMPANY_DATA
update ftd_apps.email_company_data
set value = '877-315-6315'
where company_id = 'ROSES'
and name = 'Phone';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/18/2015  ---------
-- defect QE-88          (Mark   ) (176765 )          ------------------------------
------------------------------------------------------------------------------------

-- NOTE THIS IS A GOLDEN GATE AND MYSQL CHANGE
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD (EXTERNAL_ORDER_NUMBER VARCHAR2(20) DEFAULT NULL,
                                               FLORIST_ID VARCHAR2(9) DEFAULT NULL);

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/18/2015  ---------
-- defect QE-88          (Mark   ) (176765 )          ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 01/26/2016 --------
-- defect QE-113        (Pavan ) (178930 )          --------------------------------
-----------------------------------------------------------------------------------
 UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'Florist_Message_Summary_Report;1H'
  WHERE PIPELINE_ID = 'FLORIST_MESSAGE_HOURLY_SUMMARY' AND GROUP_ID = 'MARS';
  
UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'Florist_Message_Summary_Report;1D' 
WHERE PIPELINE_ID = 'FLORIST_MESSAGE_SUMMARY_REPORT' AND GROUP_ID = 'MARS';

UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'MARS_Action_Detail_Report;1H' 
WHERE PIPELINE_ID = 'MARS_ACTION_DETAIL_REPORT' AND GROUP_ID = 'MARS';
 
------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 01/26/2016  --------
-- defect QE-113        (Pavan ) (178930 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sunil SV,                                 12/28/2015  --------
-- defect DIPII-12        (Pavan ) (177631 )          --------------------------------
-----------------------------------------------------------------------------------

 grant select on venus.venus_add_ons  to clean;

------------------------------------------------------------------------------------
-- end   requested by Sunil SV,                                 12/28/2015  --------
-- defect DIPII-12        (Pavan ) (177631 )          --------------------------------
------------------------------------------------------------------------------------

