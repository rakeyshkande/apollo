------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-1          (Mark   ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

-- FTD_APPS.ORIGINS

insert into FTD_APPS.ORIGINS (ORIGIN_ID, DESCRIPTION, ORIGIN_TYPE, ACTIVE, PARTNER_REPORT_FLAG, SEND_FLORIST_PDB_PRICE, ORDER_CALC_TYPE) 
values ('ROSES', 'Roses.com', 'internet', 'Y', 'N', 'N', 'GENERIC');

insert into FTD_APPS.ORIGINS (ORIGIN_ID, DESCRIPTION, ORIGIN_TYPE, ACTIVE, PARTNER_REPORT_FLAG, SEND_FLORIST_PDB_PRICE, ORDER_CALC_TYPE) 
values ('ROSESP', 'Roses.com', 'phone', 'N', 'N', 'N', 'GENERIC');

-- FTD_APPS.COMPANY_MASTER / FTD_APPS.COMPANY

insert into FTD_APPS.COMPANY_MASTER (COMPANY_ID, COMPANY_NAME, INTERNET_ORIGIN, DEFAULT_PROGRAM_ID, PHONE_NUMBER, CLEARING_MEMBER_NUMBER, LOGO_FILENAME, ENABLE_LP_PROCESSING, URL, SORT_ORDER, SDS_BRAND, DEFAULT_DOMAIN,  NEWSLETTER_COMPANY_ID, SENDING_FLORIST_NUMBER, DEFAULT_DNIS) 
values ('ROSES', 'Roses.com', 'ROSES', 100083, '855-489-5568', '90-7550', 'Roses_Logo.jpg', 'Y',  'roses.com', 90, 'RO', 'roses', 'FTD', '90-8400', 8882);

insert into FTD_APPS.COMPANY (COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE) 
values ('ROSES', 'Roses.com', 9027, '17477');

insert into FTD_APPS.COMPANY (COMPANY_ID, COMPANY_NAME, DEFAULT_DNIS_ID, DEFAULT_SOURCE_CODE) 
values ('ROSESP', 'Roses.com', 9027, '17677');

-- FTD_APPS.DNIS

insert into FTD_APPS.SCRIPT_MASTER (SCRIPT_MASTER_ID, SCRIPT_DESCRIPTION) 
values ('ROSES', 'Roses.com default script');

update FTD_APPS.DNIS
set description = 'Roses.com',
        origin = 'ROSESP',
        company_id = 'ROSES',
        script_id = 'ROSES'
where dnis_id in (9026, 9027, 9028);

update FTD_APPS.DNIS
set description = 'Roses.com (Modify Order Default)',
        origin = 'ROSESP',
        company_id = 'ROSES',
        script_id = 'ROSES'
where dnis_id = 8882;

-- Miscellaneous

insert into ftd_apps.SOURCE_TYPE_VAL (SOURCE_TYPE, STATUS, CREATED_ON, CREATED_BY, UPDATED_BY, UPDATED_ON) 
values ('ROSES.COM', 'Active', sysdate, 'RS-1', 'RS-1', sysdate);

update ftd_apps.state_company_tax
set company_id = 'ROSES'
where company_id = 'FLYFLW';

update quartz_schema.pipeline
set pipeline_id = 'BAMS ROSES SETTLEMENT FILE',
description = 'Process BAMS ROSES Settlement Files',
tool_tip = 'Sends Daily ROSES BAMS Settlement File',
default_payload = 'ROSES'
where pipeline_id = 'BAMS FLYFLW SETTLEMENT FILE';

-- FRP.GLOBAL_PARMS

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ins_novator_mailbox_monitor',
    'ROSES MAIL SERVER',
    'sodium.ftdi.com',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Mail server to retrieve Roses.com customer service emails'); 

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ins_novator_mailbox_monitor',
    'ROSES EXCLUDE DOMAINS',
    'amazon.com, walmart.com',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.'); 

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'ROSES_DEFAULT_SOURCE_CODE',
    '17477',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Default source code used on Roses.com Internet orders');

update FRP.GLOBAL_PARMS
set value = ' ROSES ROSESP ',
        updated_by = 'RS-1',
        updated_on = sysdate
where CONTEXT = 'SCRUB_CONFIG'
and name = 'SKIP_RECALC_ORIGINS';

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_NAME_ROSES', 'Roses.COM', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company Name for Roses.com Merchant');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_CITY_ROSES','855-489-5568', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company City for Roses.com Merchant');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_ACCOUNT_ROSES', '372603630883', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company ACCOUNT for Roses.com Merchant');

update FRP.GLOBAL_PARMS
set value = ' FTD,FTDCA,FLORIST,ROSES',
        updated_by = 'RS-1',
        updated_on = sysdate
where CONTEXT = 'ACCOUNTING_CONFIG'
and name = 'MERCHANT_ACCOUNTS';

-- EMAIL CONFIRMATION DATA

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'text',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Subject',
    'Roses.com Subject',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Subject');

INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Subject'
        and section_name = 'Roses.com Subject'
        and content_type = 'text'),
    0,
    'confirmation@roses.com~Roses.com Order Confirmation');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'text',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Header',
    'Roses.com Header',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Header');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'html',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Header',
    'Roses.com Header',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Header');
 
INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS 
        where section_type = 'Header'
        and SECTION_NAME='Roses.com Header' 
        and CONTENT_TYPE='text'),
    0,
    'Thank you for ordering from Roses.com.');

INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Header'
        and SECTION_NAME='Roses.com Header' 
        and CONTENT_TYPE='html'),
    0,
    '<table cellspacing="0" cellpadding="0" width="600" border="0">
		<tbody>
		<tr align="left"><td colspan="2">
		<p>
		</p>
		<p><font size="2">Thank you for ordering from Roses.com. The details of the order are below. <br />Please do not reply to this email message. 
		If you have questions about <br />your order, please click here: 
		</font><a href="http://custserv.roses.com/"><font size="2">http://custserv.roses.com</font></a><font color="#000000"><font size="2">.
		</font><font size="3"> </font></font></font /></p><p><span style="FONT-SIZE: 10pt; FONT-FAMILY: Verdana"></span>
		<font face="Verdana" size="1"><span style="FONT-SIZE: 9pt; FONT-FAMILY: Verdana"></span></font></p></font /></td></tr></tbody></table>'
    );

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'text',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Guarantee',
    'Roses.com Guarantee',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Guarantee');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'html',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Guarantee',
    'Roses.com Guarantee',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Guarantee');
 
INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Guarantee'
        and SECTION_NAME='Roses.com Guarantee' 
        and CONTENT_TYPE='text'),
    0,
    'Please note, to honor Our Guarantee:
- The recipient may be called to schedule delivery. 
- We do not accept requests for delivery at specific times of day.
- Substitutions may be necessary to ensure your arrangement or specialty gift is delivered in a timely manner.
- For residential deliveries, if the recipient is not available at the time of delivery, the delivery person may leave the item in a safe place for the recipient to retrieve when they return or attempt delivery the next day. During the holidays deliveries may be made as late as 9pm.
- For business deliveries, if the business is closed or not accepting deliveries, delivery will be attempted the next business day. During the holidays deliveries may be made as late as 5pm.
- Our goal is to respond to your inquiry within 48 hours.');

INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Guarantee'
        and SECTION_NAME='Roses.com Guarantee' 
        and CONTENT_TYPE='html'),
    0,
    '<p><font size="2">Please note, to honor Our Guarantee:<br />- The recipient may be called to schedule delivery. 
		<br />- We do not accept requests for delivery at specific times of day.<br />- 
		Substitutions may be necessary to ensure your arrangement or specialty gift is delivered in a timely manner.
		<br />- For residential deliveries, if the recipient is not available at the time of delivery, the delivery 
		<br />person may leave the item in a safe place for the recipient to retrieve when they return or attempt delivery <br />the next day. 
		During the holidays deliveries may be made as late as 9pm.<br />- For business deliveries, if the business is closed or not accepting deliveries, delivery will 
		<br />be attempted the next business day. 
		During the holidays deliveries may be made as late as 5pm.<br />- Our goal is to respond to your inquiry within 48 hours.</font></p>');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'text',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Marketing',
    'Roses.com Marketing',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Marketing');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'html',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Marketing',
    'Roses.com Marketing',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Marketing');

 
INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Marketing'
        and SECTION_NAME='Roses.com Marketing' 
        and CONTENT_TYPE='text'),
    0,
    'Roses.com Marketing message goes here');

INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Marketing'
        and SECTION_NAME='Roses.com Marketing' 
        and CONTENT_TYPE='html'),
    0,
    'Roses.com Marketing message goes here');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'text',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Contact',
    'Roses.com Contact',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Contact');

INSERT INTO FRP.EMAIL_SECTIONS (
    SECTION_ID,
    CONTENT_TYPE,
    START_DATE,
    END_DATE,
    DEFAULT_SECTION,
    SECTION_TYPE,
    DESCRIPTION,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    SECTION_NAME)
VALUES (
    KEY.EMAIL_SECTIONS_SQ.nextval,
    'html',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'N',
    'Contact',
    'Roses.com Contact',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Contact');
 
INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Contact'
        and SECTION_NAME='Roses.com Contact' 
        and CONTENT_TYPE='text'),
    0,
    'Contact Us:
		 Email: Please click on or go to [<A href=]http://custserv.roses.com[>Customer Service<A>]
		 Phone: ~customertierphone~
		 Shop: [<A href=]http://www.roses.com[>www.roses.com<A>]');

INSERT INTO FRP.EMAIL_SECTION_CONTENT (
    SECTION_ID,
    CONTENT_SECTION_ORDER_ID,
    CONTENT)
VALUES (
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Contact'
        and SECTION_NAME='Roses.com Contact' 
        and CONTENT_TYPE='html'),
    0,
    '<font size="2">Contact Us:<br />Email: Please click on or go to </font><a href="http://custserv.roses.com/"><font size="2">http://custserv.roses.com</font></a><br /><font size="2">Phone: <span style="FONT-SIZE: 10pt; COLOR: black; FONT-FAMILY: Arial">~customertierphone~</span><br />Shop: </font><a href="http://www.roses.com/"><font size="2">http://www.roses.com</font></a>');

INSERT INTO FRP.PROGRAM_MAPPING (
    PROGRAM_ID,
    DESCRIPTION,
    START_DATE,
    END_DATE,
    DEFAULT_PROGRAM,
    CREATED_ON,
    UPDATED_ON,
    UPDATED_BY,
    PROGRAM_NAME)
VALUES (
    KEY.PROGRAM_MAPPING_SQ.nextval,
    'Roses.com Program',
    sysdate-1,
    TO_DATE('2099-12-31', 'yyyy-mm-dd'),
    'Y',
    sysdate,
    sysdate,
    'RS-1',
    'Roses.com Program');

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Subject'
        and SECTION_NAME='Roses.com Subject' 
        and CONTENT_TYPE='text'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Header'
        and SECTION_NAME='Roses.com Header' 
        and CONTENT_TYPE='text'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Header'
        and SECTION_NAME='Roses.com Header' 
        and CONTENT_TYPE='html'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Guarantee'
        and SECTION_NAME='Roses.com Guarantee' 
        and CONTENT_TYPE='text'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Guarantee'
        and SECTION_NAME='Roses.com Guarantee' 
        and CONTENT_TYPE='html'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Marketing'
        and SECTION_NAME='Roses.com Marketing' 
        and CONTENT_TYPE='text'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Marketing'
        and SECTION_NAME='Roses.com Marketing' 
        and CONTENT_TYPE='html'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Contact'
        and SECTION_NAME='Roses.com Contact' 
        and CONTENT_TYPE='text'));

INSERT INTO FRP.PROGRAM_SECTION_MAPPING (
    PROGRAM_ID,
    SECTION_ID)
VALUES (
    (select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program'),
    (select SECTION_ID from FRP.EMAIL_SECTIONS
        where section_type = 'Contact'
        and SECTION_NAME='Roses.com Contact' 
        and CONTENT_TYPE='html'));

update FTD_APPS.COMPANY_MASTER
    set DEFAULT_PROGRAM_ID = (
        select PROGRAM_ID from FRP.PROGRAM_MAPPING
        where PROGRAM_NAME='Roses.com Program') 
where COMPANY_ID='ROSES';

-- FTD_APPS.CONTENT_DETAIL

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
    ftd_apps.content_detail_sq.nextval,
    (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'LANGUAGE'
        AND CONTENT_NAME = 'PHONE_NUMBER'),
    'ROSES',
    null,
    '1-855-489-5568',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate);

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
    ftd_apps.content_detail_sq.nextval,
    (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'LANGUAGE'
        AND CONTENT_NAME = 'PHONE_NUMBER2'),
    'ROSES',
    null,
    '1-855-489-5568',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate);

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
VALUES (
    ftd_apps.content_detail_sq.nextval,
    (SELECT CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER
        WHERE CONTENT_CONTEXT = 'EMAIL_REQUEST_PROCESSING'
        AND CONTENT_NAME = 'COMPANY_ID'),
    'ROSES',
    null,
    'ROSES',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate);

INSERT INTO FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    FILTER_1_VALUE,
    CONTENT_MASTER_ID,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY,
    UPDATED_ON,
    CREATED_BY,
    CREATED_ON)
SELECT 
    ftd_apps.content_detail_sq.nextval,
    'ROSES',
    CONTENT_MASTER_ID,
    FILTER_2_VALUE,
    CONTENT_TXT,
    'RS-1',
    sysdate,
    'RS-1',
    sysdate
FROM (select cd.content_master_id, cd.filter_2_value, cd.content_txt
        from ftd_apps.content_master cm
        join ftd_apps.content_detail cd on cd.CONTENT_MASTER_ID=cm.CONTENT_MASTER_ID
       where cm.content_context='TAX' and cm.content_name='TAX_DESCRIPTION'
         and cd.filter_1_value='FTD'
);

-- CLEAN.MESSAGE_TOKENS

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'companyname',
    'ROSES',
    'text',
    'Roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'csemail',
    'ROSES',
    'text',
    'custserv@roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'cslink',
    'ROSES',
    'text',
    'custserv.roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'inquiry',
    'ROSES',
    'text',
    'Roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'name',
    'ROSES',
    'text',
    'Roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'phone',
    'ROSES',
    'content',
    '<content><context type="text">LANGUAGE</context><name type="text">PHONE_NUMBER</name><filter1value type="text">ROSES</filter1value><filter2value type="value">/root/ORDERS/ORDER/language_id/text()</filter2value></content>');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'phone2',
    'ROSES',
    'content',
    '<content><context type="text">LANGUAGE</context><name type="text">PHONE_NUMBER2</name><filter1value type="text">ROSES</filter1value><filter2value type="value">/root/ORDERS/ORDER/language_id/text()</filter2value></content>');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'producttype',
    'ROSES',
    'text',
    'gift');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'producttype2',
    'ROSES',
    'text',
    'gift');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'producttype3',
    'ROSES',
    'text',
    'gift');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'producttype4',
    'ROSES',
    'text',
    'gift');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'producttype5',
    'ROSES',
    'text',
    'gift');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'purchasemsg',
    'ROSES',
    'text',
    'purchase');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'sci.email.link',
    'ROSES',
    'function',
    '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text" indent="no"/><xsl:template match="/root"> http://www.Roses.com/<xsl:value-of select="ORDERS/ORDER/source_code"/>/custserv/</xsl:template><xsl:template match="/autoemail"> http://www.Roses.com/<xsl:value-of select="sourceCode"/>/custserv/</xsl:template></xsl:stylesheet>');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'specmsg',
    'ROSES',
    'text',
    'If you select this option, we''d be happy to call your recipient to let them know about your gift and apologize for the delay');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'specmsg2',
    'ROSES',
    'text',
    'recent purchase from Roses.com');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'submsg2',
    'ROSES',
    'text',
    'If you would like to speak with a Customer Service Representative, please email us by clicking on this link http://custserv.Roses.com, or dial 1-855-489-5568. We are here to assist you 24 hours a day, 7 days a week.');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'sympathy.email.link',
    'ROSES',
    'function',
    '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text" indent="no"/><xsl:template match="/root"> http://www.Roses.com/<xsl:value-of select="ORDERS/ORDER/source_code"/>/custserv/</xsl:template><xsl:template match="/autoemail"> http://www.Roses.com/<xsl:value-of select="sourceCode"/>/custserv/</xsl:template></xsl:stylesheet>');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'upcasename',
    'ROSES',
    'text',
    'ROSES.COM');

INSERT INTO CLEAN.MESSAGE_TOKENS (
    TOKEN_ID,
    COMPANY_ID,
    TOKEN_TYPE,
    TOKEN_VALUE)
VALUES (
    'website',
    'ROSES',
    'text',
    'www.Roses.com');

-- FTD_APPS.EMAIL_COMPANY_DATA

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'CustServHours',
    'We are here to assist you 24 hours a day, 7 days a week.');

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'Email',
    'http://custserv.roses.com/email.epl');

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'FromAddress',
    'custserv@roses.com');

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'NoReplyFromAddress',
    'no-reply@roses.com');

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'Phone',
    '1-855-489-5568');

INSERT INTO FTD_APPS.EMAIL_COMPANY_DATA (
    COMPANY_ID,
    NAME,
    VALUE)
VALUES (
    'ROSES',
    'Website',
    'http://www.roses.com');

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-1          (Mark   ) (174473 )          -------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-2          (Mark   ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

delete from FTD_APPS.COMPANY where company_id in ('FLYFLW', 'FLYFLWP');

delete from FTD_APPS.SCRIPT_MASTER where script_master_id = 'FLYFLW';

update FTD_APPS.COMPANY_MASTER set internet_origin = 'ROSES' where company_id = 'FLYFLW';

delete from FTD_APPS.ORIGINS where origin_id in ('FLYFLW', 'FLYFLWP');

delete from FTD_APPS.SOURCE_TYPE_VAL where source_type = 'FLYING FLOWERS';

delete from FTD_APPS.PRODUCT_COMPANY_XREF where company_id = 'FLYFLW';

delete from FTD_APPS.UPSELL_COMPANY_XREF where company_id = 'FLYFLW';

delete from FTD_APPS.EMAIL_COMPANY_DATA where company_id = 'FLYFLW';

delete from CLEAN.MESSAGE_TOKENS where company_id = 'FLYFLW';


delete from FRP.GLOBAL_PARMS where CONTEXT = 'ins_novator_mailbox_monitor' and NAME = 'FLYFLW MAIL SERVER'; 

delete from FRP.GLOBAL_PARMS where CONTEXT = 'ins_novator_mailbox_monitor' and NAME = 'FLYFLW EXCLUDE DOMAINS';

delete from FRP.GLOBAL_PARMS where CONTEXT = 'FTDAPPS_PARMS' and NAME = 'FLYFLW_DEFAULT_SOURCE_CODE';

delete from FRP.GLOBAL_PARMS where CONTEXT = 'ACCOUNTING_CONFIG' and NAME like 'MERCHANT_%FLYFLW';

delete from FRP.SECURE_CONFIG where CONTEXT = 'email_request_processing' and NAME = 'FLYFLW_mailbox_monitor_USERNAME';

delete from FRP.SECURE_CONFIG where CONTEXT = 'email_request_processing' and NAME = 'FLYFLW_mailbox_monitor_PASSWORD';

delete from FTD_APPS.COMPANY_MASTER where company_id = 'FLYFLW';

delete from FRP.EMAIL_SECTION_CONTENT where section_id in (
    select section_id from FRP.EMAIL_SECTIONS where description like 'FlyingFlowers%');

delete from FRP.PROGRAM_SECTION_MAPPING where section_id in (
    select section_id from FRP.EMAIL_SECTIONS where description like 'FlyingFlowers%');

delete from FRP.EMAIL_SECTIONS where description like 'FlyingFlowers%';

delete from FRP.PROGRAM_MAPPING where description like 'FlyingFlowers%';

delete from FTD_APPS.CONTENT_DETAIL Where filter_1_value = 'FLYFLW';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-2          (Mark   ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

