------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/ 2/2015  --------
-- defect QE-97         (Mark  ) (173412 )          --------------------------------
------------------------------------------------------------------------------------

alter table clean.order_florist_used add (
selection_data varchar2(100),
florist_selection_log_id number);

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/ 2/2015  --------
-- defect QE-97         (Mark  ) (173412 )          --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                               12/7/2015  --------
-- defect SP-121         (Pavan  ) (174514 )          --------------------------------
------------------------------------------------------------------------------------


update frp.global_parms 
set description='This Param is not in use', 
    updated_by = 'SP-121', 
    updated_on = sysdate 
where name = 'EXOTIC_CUTOFF';


------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                               12/7/2015  --------
-- defect SP-121         (Pavan  ) (174514 )          --------------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- begin requested by Sri vatsala,                               12/7/2015  --------
-- defect SP-73         (Pavan  ) (174515 )          --------------------------------
------------------------------------------------------------------------------------

insert into FTD_APPS.CONTENT_MASTER 
(content_master_id, content_context, content_name, content_description, 
content_filter_id1, content_filter_id2, updated_by, updated_on, created_by, created_on)
values 
(ftd_apps.content_master_sq.nextval, 'PHONE_NUMBER', 'CALLOUT_PHONE', 'Callout phone number per partner', 
(select content_filter_id from ftd_apps.content_filter where content_filter_name = 'CUSTOMER_TIER'), null, 'SP-73', sysdate, 'SP-73', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (
   CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
     ftd_apps.content_detail_sq.nextval,
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALLOUT_PHONE'),
     null,
     null,
     '1-800-554-0993',
     'SP_73', 
     sysdate, 
     'SP_73', 
     sysdate
);

insert into FTD_APPS.CONTENT_DETAIL (
    CONTENT_DETAIL_ID,
    CONTENT_MASTER_ID,
    FILTER_1_VALUE,
    FILTER_2_VALUE,
    CONTENT_TXT,
    UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON)
values (
     ftd_apps.content_detail_sq.nextval,
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PHONE_NUMBER' AND CONTENT_NAME = 'CALLOUT_PHONE'),
     'ProFlowers',
     null,
     '1-877-690-3050',
     'SP_73', 
     sysdate, 
     'SP_73', 
     sysdate
);


------------------------------------------------------------------------------------
-- end   requested by Sri vatsala,                               12/7/2015  --------
-- defect SP-73         (Pavan  ) (174515 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                              12/11/2015  --------
-- defect AAF-1         (Mark   ) (174777 )          -------------------------------
------------------------------------------------------------------------------------

-- Prevent Military Star payment type from appearing in JOE
update FTD_APPS.PAYMENT_METHODS set JOE_FLAG='N' where PAYMENT_METHOD_ID='MS';

-- Global Parms
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_service_url', 'https://shop.aafes.com/val/MilStarVendor.asmx?WSDL', 'AAF-1', sysdate, 'AAF-1', 
sysdate, 'Web Service URL for Military Star credit application');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_region', 'T', 'AAF-1', sysdate, 'AAF-1', sysdate, 
' Indicates to AAFES if order is for test (T) or production (P)');

update FRP.GLOBAL_PARMS set VALUE='37891552', DESCRIPTION='AAFES Facility number assigned to FTD' 
where CONTEXT='ACCOUNTING_CONFIG' and NAME='AAFES_facility';

update FRP.GLOBAL_PARMS set VALUE='N', DESCRIPTION = 'Deprecated - AAFES Authorizations are no longer allowed from Apollo' 
where CONTEXT='CCAS_CONFIG' and NAME='aafes.auth.active';

delete from FRP.GLOBAL_PARMS where CONTEXT='CCAS_CONFIG' and NAME='aafes.auth.facnbr';

delete from FRP.GLOBAL_PARMS where CONTEXT='CCAS_CONFIG' and NAME='aafes.auth.default.server';

delete from FRP.GLOBAL_PARMS where CONTEXT='AAFES_CONFIG' and NAME='aafes.auth.default.server';

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                              12/11/2015  --------
-- defect AAF-1         (Mark   ) (174777 )          -------------------------------
------------------------------------------------------------------------------------


