------------------------------------------------------------------------------------
-- begin requested by Sri Meka,                                 11/30/2015  --------
-- defect SP-121        (Pavan ) (173378 )          --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('FTDAPPS_PARMS', 'exotic_lead_days', '1', 'SP-121', SYSDATE, 'SP-121', SYSDATE, 'Configurable Lead Exotic days');

------------------------------------------------------------------------------------
-- end   requested by Sri Meka,                                 11/30/2015  --------
-- defect SP-121        (Pavan ) (173378 )          --------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- begin requested by Sunil Rao,                                 12/11/2015  --------
-- defect DIPII-5         (Pavan ) (174723 )          --------------------------------
------------------------------------------------------------------------------------

 -- Create new table CLEAN.WEST_VENDOR_JDE_AUDIT_LOG
CREATE TABLE clean.west_vendor_jde_audit_log
(
	log_id                NUMBER(13) NOT NULL, 
	vendor_id              VARCHAR2(5) NOT NULL,
	filling_vendor        VARCHAR2(9) , 
	venus_order_number    VARCHAR2(11) NOT NULL, 
	external_order_number VARCHAR2(20), 
	ship_date             DATE NOT NULL, 
	delivery_date         DATE NOT NULL, 
	vendor_sku            VARCHAR2(20) NOT NULL, 
	product_id            VARCHAR2(10) NOT NULL, 
	orig_amount           NUMBER(11, 2) NOT NULL, 
	adj_amount            NUMBER(11, 2) NOT NULL, 
	order_status          VARCHAR2(30), 
	record_type           VARCHAR2(10) NOT NULL,
	cancel_reason_code    VARCHAR2(3),
	created_on            DATE NOT NULL,
	adj_created_datetime  DATE,
	order_guid 			  VARCHAR2(2000),
	zone_jump_label_date  DATE 
)
TABLESPACE clean_data;
-- Table CLEAN.WEST_VENDOR_JDE_AUDIT_LOG created.

-- Add Primary Key constraint on column log_id
ALTER TABLE clean.west_vendor_jde_audit_log 
ADD   ( 
		CONSTRAINT west_vendor_jde_audit_log_pk PRIMARY KEY (log_id)
		USING INDEX TABLESPACE clean_indx 
	  );
-- Primary Key constraint on column log_id	added  

-- Add Check constraint on RECORD_TYPE
ALTER TABLE clean.west_vendor_jde_audit_log
ADD  ( 
		CONSTRAINT west_vendor_jde_audit_log_ck1 CHECK(record_type IN('SHIP','CAN','ADJ'))
	 );
-- Check constraint on RECORD_TYPE added
	   
-- Add Foreign Key constraint on vendor_id
ALTER TABLE clean.west_vendor_jde_audit_log
ADD  ( 
		CONSTRAINT west_vendor_jde_audit_log_fk1 FOREIGN KEY(vendor_id) 
        REFERENCES ftd_apps.vendor_master
	 );
-- Foreign Key constraint on vendor_id added

-- Add Foreign Key constraint on cancel_reason_code
ALTER TABLE clean.west_vendor_jde_audit_log
ADD  ( 
		CONSTRAINT west_vendor_jde_audit_log_fk2 FOREIGN KEY(cancel_reason_code)
		REFERENCES venus.cancel_reason_code_val
	 );
-- Foreign Key constraint on cancel_reason_code added
	 
-- Add Foreign Key constraint on order_guid
ALTER TABLE clean.west_vendor_jde_audit_log 
ADD (
		CONSTRAINT west_vendor_jde_audit_log_fk3 FOREIGN KEY (order_guid) 
		REFERENCES clean.orders(order_guid)
	);
-- Foreign Key constraint on order_guid added
			
-- Creating indexes			
CREATE INDEX clean.west_vendor_jde_audit_log_n1 ON clean.west_vendor_jde_audit_log (vendor_id) TABLESPACE clean_indx COMPUTE STATISTICS;

CREATE INDEX clean.west_vendor_jde_audit_log_n2 ON clean.west_vendor_jde_audit_log (product_id) TABLESPACE clean_indx COMPUTE STATISTICS;

CREATE INDEX clean.west_vendor_jde_audit_log_n3 ON clean.west_vendor_jde_audit_log (cancel_reason_code) TABLESPACE clean_indx COMPUTE STATISTICS;

CREATE INDEX clean.west_vendor_jde_audit_log_n4 ON clean.west_vendor_jde_audit_log(order_guid) tablespace clean_indx compute statistics;
-- Indexes created

-- Creating new sequence WEST_VENDOR_JDE_AUDIT_LOG_sq
CREATE SEQUENCE clean.west_vendor_jde_audit_log_sq
START WITH 1
INCREMENT BY 1
MAXVALUE 9999999999999;			
-- Sequence created

GRANT SELECT ON clean.west_vendor_jde_audit_log TO RPT;


insert into events.events
values
(
'WEST-VENDOR-EOM',
'ACCOUNTING',
'Run the West Vendor EOM Process',
'Y'
);



Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_FTP_SERVER', '{replace-me}', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the FTP server the west vendor order updates to be fetched from.');

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_FTP_DIR', '/home/ftdwest/vendor_updates', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the directory in FTP server the west vendor order updates files to be fetched from.');

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_VENDOR_UPDATES_FILE_NAME_PATTERN', 'wms_transfer_cost_[2][0-9][0-9][0-9][0-1][0-9][0-3][0-9][/.]csv', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the name of the file sent by FTDWest to FTP server .');

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_VENDOR_UPDATES_LOCAL_DIR', '/u02/ftdwest', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the directory in local server where the west vendor order updates files to be staged.');

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_INTERNAL_ARCHIVE_SERVER', '{replace-me}', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the server where west vendor updates file to be archived.');

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_INTERNAL_ARCHIVE_DIR', '/u02/apollo/ship/ftdwest/archive', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the directory on internal archive server where west vendor updates file to be archived.');


Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('SHIP_CONFIG', 'FTDW_FTP_PRIVATE_KEY_PATH', '{replace-me}', 'DIPII-5', SYSDATE, 'DIPII-5', SYSDATE, 
'Specifies the name of the file sent by FTDWest to FTP server .');



INSERT INTO quartz_schema.pipeline
(
	pipeline_id,
	group_id,
	description,
	tool_tip,
	default_schedule,
	default_payload,
	force_default_payload,
	queue_name,
	class_code,
	active_flag
)
VALUES
(
	'SHIPWESTVENDORUPDATE',
	'SHIP',
	'Get West vendor updates',
	'Download the file sent by FTDWest from condor, which has vendor fulfilled order updates',
	'0 0 3 * * ?',
	'WEST',
	'Y',
	'OJMS.SHIP_WEST_VENDOR_UPDATE',
	'SimpleJmsJob',
	'Y'
);



declare
   v_key_name varchar2(30);
begin

   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDW_FTP_USERNAME',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Username by which Apollo connects to FTP server to download vendor updates file sent by FTDWest.',sysdate,'DIPII-5',sysdate,'DIPII-5',v_key_name);

   insert into FRP.SECURE_CONFIG (CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
   values ('ship','FTDW_FTP_PRIVATE_KEY_PASSPHRASE',Global.Encryption.Encrypt_It('REPLACEME', v_key_name),
   'Passphrase for the private key used while logging into FTP server.',sysdate,'DIPII-5',sysdate,'DIPII-5',v_key_name);

end;
/


set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.SHIP_WEST_VENDOR_UPDATE',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'SHIP_WEST_VENDOR_UPDATE (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS SHIP_WEST_VENDOR_UPDATElob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.SHIP_WEST_VENDOR_UPDATE',
      queue_table => 'OJMS.SHIP_WEST_VENDOR_UPDATE',
      max_retries => 3,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.SHIP_WEST_VENDOR_UPDATE');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_VENDOR_UPDATE', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_VENDOR_UPDATE', grantee => 'OSP_QUEUE');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_VENDOR_UPDATE', grantee => 'EVENTS');


connect &&dba_user


 
------------------------------------------------------------------------------------
-- end   requested by Sunil Rao,                                 12/11/2015  --------
-- defect DIPII-5        (Pavan ) (174723 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                                12/9/2015  --------
-- defect RS-1           (Mark  ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

declare
   v_key_name frp.global_parms.value%type;
begin

   select value into v_key_name from frp.global_parms where context = 'Ingrian' and name = 'Current Key';

   INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'email_request_processing', 'ROSES_mailbox_monitor_USERNAME', global.encryption.encrypt_it('replace', v_key_name),
   'Login to pull CS emails for Roses.com.', sysdate, 'RS-1', sysdate, 'RS-1', v_key_name);

   INSERT INTO FRP.SECURE_CONFIG ( CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
   VALUES ( 'email_request_processing', 'ROSES_mailbox_monitor_PASSWORD', global.encryption.encrypt_it('replace', v_key_name),
   'Password to pull CS emails for Roses.com.', sysdate, 'RS-1', sysdate, 'RS-1', v_key_name);

end;
/

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                                12/9/2015  --------
-- defect DIPII-5        (Mark  ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                                12/9/2015  --------
-- defect SP-125         (Mark  ) (174386 )          -------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
values ('ACCOUNTING_CONFIG','BAMS_UPDATE_REMOVED_ORDER_PMT_LAST_DATE','REPLACE ME','SP-125',
SYSDATE,'SP-125',SYSDATE, 'Date when removed BAMS order payments were updated.');

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                                12/9/2015  --------
-- defect SP-125         (Mark  ) (174386 )          -------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175803 )          --------------------------------
-----------------------------------------------------------------------------------
Create user mars identified by &password account lock default tablespace FTD_APPS_DATA temporary tablespace TEMP3 profile default;
grant create session to mars;

--- MARS user is already present on lower environments . please skip above step if the user already present.

CREATE SEQUENCE MARS.TEMPLATE_HIST_ID_SQ MINVALUE 1 INCREMENT BY 1 START WITH 1 CACHE 200 NOORDER NOCYCLE;
CREATE SEQUENCE MARS.RESULT_ID_SQ MINVALUE 1 INCREMENT BY 1 START WITH 1 CACHE 200 NOORDER NOCYCLE;

CREATE TABLE MARS.SIMULATION_RESULTS (RESULT_ID NUMBER NOT NULL PRIMARY KEY, VERSION_NO VARCHAR2(20), INPUT BLOB, 
OUTPUT BLOB, STATUS VARCHAR2(20), ERROR_MESSAGE VARCHAR2(400), CREATED_ON DATE, CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));

CREATE TABLE MARS.SIMULATOR_TEMPLATES (VERSION_NO VARCHAR2(20) NOT NULL PRIMARY KEY, TEMPLATE BLOB, CREATED_ON DATE, 
CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));

CREATE TABLE MARS.SIMULATOR_TEMPLATES_HIST (TEMPLATE_HIST_ID NUMBER NOT NULL PRIMARY KEY, VERSION_NO VARCHAR2(20) NOT NULL, TEMPLATE BLOB,
CREATED_ON DATE, CREATED_BY VARCHAR2(100), UPDATED_ON DATE, UPDATED_BY VARCHAR2(100));



set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.MARS_SIMULATOR',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'MARS_SIMULATOR (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS MARS_SIMULATORlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.MARS_SIMULATOR',
      queue_table => 'OJMS.MARS_SIMULATOR',
      max_retries => 3,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.MARS_SIMULATOR');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'OSP_QUEUE');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_SIMULATOR', grantee => 'EVENTS');


connect &&dba_user


------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175803 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 12/24/2015  --------
-- defect QE-110        (Pavan ) (177481 )          --------------------------------
-----------------------------------------------------------------------------------

ALTER SEQUENCE MARS.TEMPLATE_HIST_ID_SQ NOCACHE;
ALTER SEQUENCE MARS.RESULT_ID_SQ NOCACHE;

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 12/24/2015  --------
-- defect QE-110        (Pavan ) (177481 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                              12/11/2015  --------
-- defect AAF-1         (Mark   ) (174777 )          -------------------------------
------------------------------------------------------------------------------------

-- Global Parms
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_service_url', 'https://shop.aafes.com/val/MilStarVendor.asmx?WSDL', 'AAF-1', sysdate, 'AAF-1', 
sysdate, 'Web Service URL for Military Star credit application');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_region', 'T', 'AAF-1', sysdate, 'AAF-1', sysdate, 
' Indicates to AAFES if order is for test (T) or production (P)');

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey,                              12/11/2015  --------
-- defect AAF-1         (Mark   ) (174777 )          -------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-1          (Mark   ) (174473 )          -------------------------------
------------------------------------------------------------------------------------

-- FRP.GLOBAL_PARMS

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ins_novator_mailbox_monitor',
    'ROSES MAIL SERVER',
    'sodium.ftdi.com',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Mail server to retrieve Roses.com customer service emails'); 

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION) 
values (
    'ins_novator_mailbox_monitor',
    'ROSES EXCLUDE DOMAINS',
    'amazon.com, walmart.com',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.'); 

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'ROSES_DEFAULT_SOURCE_CODE',
    '17477',
    'RS-1',
    sysdate,
    'RS-1',
    sysdate,
    'Default source code used on Roses.com Internet orders');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_NAME_ROSES', 'Roses.COM', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company Name for Roses.com Merchant');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_CITY_ROSES','855-489-5568', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company City for Roses.com Merchant');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
values ('ACCOUNTING_CONFIG', 'MERCHANT_ACCOUNT_ROSES', '372603630883', 'RS-1', SYSDATE, 'RS-1', SYSDATE, 'Company ACCOUNT for Roses.com Merchant');

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/17/2015  --------
-- defect QE-85         (Pavan  ) (175832 )          --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'SHOW_SIMULATION_RESULTS_COUNT', '10', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Param to limit the Simulation Results on the Simulator page');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'TEMP_UPLOAD_DIRECTORY', '/u02/Upload', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Temp directory for staging the input excel file');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'TEMP_DOWNLOAD_DIRECTORY', '/u02/Download', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'Temp directory for staging the output excel file');
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'EXCEL_ROW_NUM', '9', 'QE-85', SYSDATE, 'QE-85', SYSDATE, 'First position of the input data in the excel');

------------------------------------------------------------------------------------
-- End requested by Karthik D,                               12/17/2015  --------
-- defect QE-85         (Pavan  ) (175832 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------

-- GLOBAL_PARMS
update frp.global_parms
set value = '877-315-6315',
updated_on = sysdate,
updated_by = 'RS-6'
where context = 'ACCOUNTING_CONFIG'
and name = 'MERCHANT_CITY_ROSES';

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/18/2015  --------
-- defect QE-85          (Mark   ) (175912 )        --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/28/2015  ---------
-- defect QE-105          (Pavan   ) (177642 )          ------------------------------
------------------------------------------------------------------------------------

UPDATE FRP.GLOBAL_PARMS SET VALUE = '/u02/apollo/MARS/' WHERE NAME = 'TEMP_UPLOAD_DIRECTORY' AND CONTEXT = 'MARS_CONFIG'; 
UPDATE FRP.GLOBAL_PARMS SET VALUE = '/u02/apollo/MARS/' WHERE NAME = 'TEMP_DOWNLOAD_DIRECTORY' AND CONTEXT = 'MARS_CONFIG';

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/28/2015  ---------
-- defect QE-105          (Pavan   ) (177642 )          ------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                                 01/6/2016 --------
-- defect QE-113        (Syed ) (179062 )          --------------------------------
-----------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_test_mode', 'N', 'AAF-6', sysdate, 'AAF-6', sysdate, 
' Must be N in production. For QA testing only � if set to Y, Web Service call is bypassed');
 
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_test_responses', ' X, 123456, 12345678901234,|X, , ,|X, , ,', 'AAF-6', sysdate, 'AAF-6', sysdate, 
' If test mode enabled, then these stock responses are returned for request types');

------------------------------------------------------------------------------------
-- end requested by Gary Sergey,                                 01/6/2016 --------
-- defect QE-113        (Syed ) (179062 )          --------------------------------
-----------------------------------------------------------------------------------

