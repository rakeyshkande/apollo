
------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/18/2015  ---------
-- defect QE-88          (Mark   ) (176765 )          ------------------------------
------------------------------------------------------------------------------------

-- NOTE THIS IS A GOLDEN GATE AND MYSQL CHANGE
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD (EXTERNAL_ORDER_NUMBER VARCHAR2(20) DEFAULT NULL,
                                               FLORIST_ID VARCHAR2(9) DEFAULT NULL);

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/18/2015  ---------
-- defect QE-88          (Mark   ) (176765 )          ------------------------------
------------------------------------------------------------------------------------
    

    

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                               12/28/2015  ---------
-- defect QE-105          (Pavan   ) (177642 )          ------------------------------
------------------------------------------------------------------------------------


UPDATE FRP.GLOBAL_PARMS SET VALUE = '/u02/apollo/MARS/' WHERE NAME = 'TEMP_UPLOAD_DIRECTORY' AND CONTEXT = 'MARS_CONFIG'; 
UPDATE FRP.GLOBAL_PARMS SET VALUE = '/u02/apollo/MARS/' WHERE NAME = 'TEMP_DOWNLOAD_DIRECTORY' AND CONTEXT = 'MARS_CONFIG';


------------------------------------------------------------------------------------
-- end   requested by Karthik D,                               12/28/2015  ---------
-- defect QE-105          (Pavan   ) (177642 )          ------------------------------
------------------------------------------------------------------------------------
    
