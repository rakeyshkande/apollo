------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 01/26/2016 --------
-- defect QE-113        (Pavan ) (178930 )          --------------------------------
-----------------------------------------------------------------------------------
 UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'Florist_Message_Summary_Report;1H'
  WHERE PIPELINE_ID = 'FLORIST_MESSAGE_HOURLY_SUMMARY' AND GROUP_ID = 'MARS';
  
UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'Florist_Message_Summary_Report;1D' 
WHERE PIPELINE_ID = 'FLORIST_MESSAGE_SUMMARY_REPORT' AND GROUP_ID = 'MARS';

UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_PAYLOAD = 'MARS_Action_Detail_Report;1H' 
WHERE PIPELINE_ID = 'MARS_ACTION_DETAIL_REPORT' AND GROUP_ID = 'MARS';
 
------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 01/26/2016  --------
-- defect QE-113        (Pavan ) (178930 )          --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Gary Sergey,                                 01/6/2016 --------
-- defect QE-113        (Syed ) (179062 )          --------------------------------
-----------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_test_mode', 'N', 'AAF-6', sysdate, 'AAF-6', sysdate, ' Must be N in production. For QA testing only � if set to Y, Web Service call is bypassed');
 
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('AAFES_CONFIG', 'aafes_test_responses', ' X, 123456, 12345678901234,|X, , ,|X, , ,', 'AAF-6', sysdate, 'AAF-6', sysdate, ' If test mode enabled, then these stock responses are returned for request types');

------------------------------------------------------------------------------------
-- end requested by Gary Sergey,                                 01/6/2016 --------
-- defect QE-113        (Syed ) (179062 )          --------------------------------
-----------------------------------------------------------------------------------


 
