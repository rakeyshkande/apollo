------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-1          (Mark  ) (174479 )          --------------------------------
------------------------------------------------------------------------------------

Identify the scheduled job ins_novator_mailbox_monitor ('FLYFLW'). Change it to
ins_novator_mailbox_monitor('ROSES').

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               12/ 9/2015  --------
-- defect RS-1          (Mark  ) (174479 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Sunil Rao,                                 12/11/2015  --------
-- defect DIPII-5         (Pavan ) (174723 )          --------------------------------
------------------------------------------------------------------------------------
alter user events identified by "onetime" account unlock;
connect events/onetime

Set serveroutput on
Declare
   V_job number;
begin
     dbms_job.submit(
      job => V_job,
      what => 'events.ins_west_vendor_eom;',
      next_date => last_day(sysdate)+1,
      interval => 'trunc(add_months(sysdate,1),''MONTH'')+(4/24)'
   );
   end;
/

commit;
connect &dba_user
alter user events identified by values "disabled" account lock;

------------------------------------------------------------------------------------
-- end   requested by Sunil Rao,                                 12/11/2015  --------
-- defect DIPII-5        (Pavan ) (174723 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175804 )          --------------------------------
-----------------------------------------------------------------------------------
 grant execute on MARS.SIMULATOR_QUERY_PKG  to osp;
  grant execute on MARS.SIMULATOR_MAINT_PKG to osp;

------------------------------------------------------------------------------------
-- end   requested by Karthik D,                                 12/17/2015  --------
-- defect QE-85        (Pavan ) (175804 )          --------------------------------
------------------------------------------------------------------------------------


