ALTER TABLE ftd_apps.partner_program ADD (membership_data_required CHAR(1) DEFAULT 'Y');
UPDATE ftd_apps.partner_program SET membership_data_required = 'Y';
ALTER TABLE ftd_apps.partner_program MODIFY (membership_data_required NOT NULL);
ALTER TABLE ftd_apps.partner_program ADD (CONSTRAINT partner_program_ck2 CHECK (membership_data_required IN ('Y','N')));
