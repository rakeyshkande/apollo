GRANT SELECT ON FTD_APPS.COUNTRY_KEYS TO CLEAN;

alter table ftd_apps.country_keys add
(retrans_country_id varchar2(2));

update ftd_apps.country_keys
set retrans_country_id = country_id;

update ftd_apps.country_keys
set retrans_country_id = 'GB'
where country_id = 'UK';

insert into FTD_APPS.FLORIST_MASTER 
(FLORIST_ID, FLORIST_NAME, ADDRESS, CITY, STATE, ZIP_CODE, PHONE_NUMBER, CITY_STATE_NUMBER, RECORD_TYPE, MERCURY_FLAG, 
FLORIST_WEIGHT, SUPER_FLORIST_FLAG, SUNDAY_DELIVERY_FLAG, DELIVERY_CITY, DELIVERY_STATE, OWNERS_NAME, MINIMUM_ORDER_AMOUNT, 
STATUS, TERRITORY, FAX_NUMBER, EMAIL_ADDRESS, INITIAL_WEIGHT, ADJUSTED_WEIGHT, ADJUSTED_WEIGHT_START_DATE, 
ADJUSTED_WEIGHT_END_DATE, ADJUSTED_WEIGHT_PERMENANT_FLAG, TOP_LEVEL_FLORIST_ID, PARENT_FLORIST_ID, LAST_UPDATED_BY, 
LAST_UPDATED_DATE, ALT_PHONE_NUMBER, ORDERS_SENT, SENDING_RANK, SUNDAY_DELIVERY_BLOCK_ST_DT, SUNDAY_DELIVERY_BLOCK_END_DT, 
LOCKED_BY, LOCKED_DATE, INTERNAL_LINK_NUMBER, VENDOR_FLAG, SUSPEND_OVERRIDE_FLAG, ALLOW_MESSAGE_FORWARDING_FLAG, IS_FTO_FLAG) 
values ('90-0266GB', 'RETRANS - UNITED KINGDOM', '3113 Woodcreek Dr', 'DOWNERS GROVE', 'IL', '60515', '6307197800', '131990', 
'R', 'M', 1, null, null, 'DOWNERS GROVE', 'IL', null, null, 'Active', null, null, null, 0, null, null, null, null, '90-0266GB', 
null, 'OSP', '05-JAN-05', null, null, 99999, null, null, null, null, null, 'Y', 'N', 'Y', 'Y');

insert into ftd_apps.FLORIST_CODIFICATIONS 
(FLORIST_ID, CODIFICATION_ID, BLOCK_START_DATE, BLOCK_END_DATE, HOLIDAY_PRICE_FLAG, MIN_PRICE, CODIFIED_DATE) 
values ('90-0266GB', '.PT', null, null, null, null, to_date('2005-12-08 15:56:20','yyyy-mm-dd hh24:mi:ss'));

insert into ftd_apps.FLORIST_COMMENTS 
(FLORIST_ID, COMMENT_DATE, COMMENT_TYPE, FLORIST_COMMENT, MANAGER_ONLY_FLAG, CREATED_DATE, CREATED_BY) 
values ('90-0266GB', to_date('2005-12-08 15:56:20', 'yyyy-mm-dd hh24:mi:ss'), 'Codification', 
'Codification: .PT added.  This was done for all florists.', 'N', 
to_date('2005-12-08 15:56:20', 'yyyy-mm-dd hh24:mi:ss'), 'mpetrie');

update FTD_APPS.FLORIST_MASTER
set status='Inactive'
where florist_id='90-0266UK';
