set define ~

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/22/2006
***              Reason : Enhancement1150: Schedule a Complimentary Orders Report to run daily
***             Purpose : This script populates the RPT.REPORT table for this report to be scheduled
***
*** Special Instructions: rptsvr_url = 'http://apollo.ftdi.com:7778/reports/rwservlet?' for dev5
***                                  = 'http://illiad-prod.ftdi.com:7779/reports/rwservlet?' for dev6
***
*****************************************************************************************/

SELECT 'Inserting into rpt.report' FROM DUAL;

INSERT INTO RPT.REPORT(
   REPORT_ID, 
   NAME, 
   DESCRIPTION, 
   REPORT_FILE_PREFIX, 
   REPORT_TYPE, 
   REPORT_OUTPUT_TYPE, 
   REPORT_CATEGORY, 
   SERVER_NAME, 
   HOLIDAY_INDICATOR, 
   STATUS, 
   CREATED_ON, 
   CREATED_BY, 
   UPDATED_ON, 
   UPDATED_BY, 
   ORACLE_RDF, 
   REPORT_DAYS_SPAN, 
   NOTES, 
   ACL_NAME, 
   RETENTION_DAYS, 
   RUN_TIME_OFFSET, 
   END_OF_DAY_REQUIRED, 
   SCHEDULE_DAY, 
   SCHEDULE_TYPE, 
   SCHEDULE_PARM_VALUE)
VALUES(
   1002132, 
   'Complimentary Orders (Daily)', 
   'This daily report is automatically executed daily to display transactions for the entire previous day, to outline all complimentary orders ', 
   'Complimentary_Orders_Daily', 
   'S', 
   'Both', 
   'Acct', 
   '~reports_server',
   'Y', 
   'Active', 
   SYSDATE, 
   'ddesai', 
   SYSDATE, 
   'ddesai', 
   'ACC13_Complimentary_Orders.rdf', 
   NULL, 
   NULL, 
   'AcctCustReportAccess', 
   10, 
   120, 
   'Y', 
   NULL, 
   'D', 
   '&p_monthly_daily_ind=D');

-- Populate the schedule_parm_vale for the montly version of this report   
UPDATE rpt.report
SET 
   schedule_parm_value = '&p_monthly_daily_ind=M',
   updated_on = SYSDATE,
   updated_by = 'ddesai'
WHERE
	report_id = 1002131;
    
COMMIT;
