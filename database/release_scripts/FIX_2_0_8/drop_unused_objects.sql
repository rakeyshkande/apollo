drop package frp.buyers_maint_pkg;
drop package frp.buyers_query_pkg;
drop package frp.orders_maint_pkg;
drop package frp.orders_query_pkg;
drop package frp.recipients_maint_pkg;
drop package frp.recipients_query_pkg;
drop package frp.dispatcher_pkg;
drop table frp.add_ons;
drop table frp.order_disposition;
drop table frp.order_details;
drop table frp.jcpenney;
drop table frp.co_brand;
drop table frp.order_contact_info;
drop table frp.payments;
drop table frp.orders;
