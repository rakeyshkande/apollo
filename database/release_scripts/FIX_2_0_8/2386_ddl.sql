create or replace view clean.cust_recap_order_vw as
select	o.customer_id,
	decode (o.company_id, 'GSC', 'FTD', 'SFMB', 'FTD', o.company_id) company_id,
	o.order_date,
	od.order_detail_id,
	c.concat_id
from	clean.orders o
join	clean.order_details od
on	o.order_guid = od.order_guid
and	od.order_disp_code not in ('In-Scrub','Pending','Removed')
join	clean.customer c
on 	o.customer_id = c.customer_id
where	not exists
(
	select	1
	from	clean.payments p
	where	p.order_guid = o.order_guid
	and	p.payment_type = 'NC'
	and	p.additional_bill_id is null
	and	p.refund_id is null
)
and	not exists
(
	select	1
	from	clean.refund r
	where	r.order_detail_id = od.order_detail_id
	and	r.refund_disp_code like 'A%'
);
	
	
create table rpt.rep_cust_recap tablespace rpt_data as
select * from rpt.rep_tab where 1=0;

alter table rpt.rep_cust_recap add
constraint rep_cust_recap_pk primary key (col1, col2)
using index tablespace rpt_data;

grant select on clean.cust_recap_order_vw to rpt;

