/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/16/2006
***              Reason : Enhancement825: Create new member order distribution report
***             Purpose : This script re-populates the RPT report tables for this new report
***
*** Special Instructions: 
***
*****************************************************************************************/

--alter table rpt.report_submission_log disable constraint report_submission_log_fk1;
delete from rpt.report_detail_char rdc
	where exists (select 1 from rpt.report_submission_log rsl 
              where rsl.report_id=100901 
              and rsl.report_submission_id=rdc.report_submission_id);
delete from rpt.report_submission_log where report_id = 100901;
--delete from rpt.report where report_file_prefix = 'CSR02_Member_Order_Distribution';
delete from rpt.report_parm_ref where report_parm_id like '10090%';
delete from rpt.report_parm_value_ref where report_parm_value_id like '10090%' OR report_parm_value_id like '10190%';
delete from rpt.report_parm where report_parm_id like '10090%';
delete from rpt.report_parm_value where report_parm_value_id like '10090%' OR report_parm_value_id like '10190%';
delete from rpt.report where report_id = 100901;
commit;
--alter table rpt.report_submission_log enable constraint report_submission_log_fk1;
/
