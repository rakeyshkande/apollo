/******** DDL's required for Defect 1243 *********************************************************
********* New Columns "updated_by" and "created_by" are initialized to 'SYS'**********************
********* New Columns "created_on" and "updated_on" are initialized to SYSDATE *******************/

/******** Add New Columns to the Tables in FRP Schema ********************************************/

-- Add Created_By, Created_On, Updated_By, Updated_On to FRP.GLOBAL_PARMS Table
ALTER TABLE frp.global_parms ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);
	
UPDATE frp.global_parms SET 
   created_by = 'SYS',
   updated_by = 'SYS';
   
ALTER TABLE frp.global_parms MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);
   
/******** Add New Columns to the Tables in FTD_APPS Schema ***************************************/

-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.SNH Table 
ALTER TABLE ftd_apps.snh ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	

UPDATE ftd_apps.snh SET
   created_by = 'SYS',
   updated_by = 'SYS';
   
ALTER TABLE ftd_apps.snh MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);
 
-- Add Updated_By, Updated_On to FTD_APPS.SOURCE_TYPE_VAL Table 
ALTER TABLE ftd_apps.source_type_val ADD (
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);
	
UPDATE ftd_apps.source_type_val SET
   updated_by = 'SYS';	

ALTER TABLE ftd_apps.source_type_val MODIFY (
   updated_by NOT NULL,
   updated_on NOT NULL);
   
-- Add Created_By, Created_On, Last_Updated_By to FTD_APPS.FTDM_SHUTDOWN Table
ALTER TABLE ftd_apps.ftdm_shutdown ADD (
	last_updated_by VARCHAR2(100));
   
-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.SHIPPING_KEYS Table
ALTER TABLE ftd_apps.shipping_keys ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.shipping_keys SET
   created_by = 'SYS',
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.shipping_keys MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);   
   
-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.SHIPPING_KEY_DETAILS Table
ALTER TABLE ftd_apps.shipping_key_details ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.shipping_key_details SET
   created_by = 'SYS',
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.shipping_key_details MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);
   
-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.SHIPPING_KEY_COSTS Table
ALTER TABLE ftd_apps.shipping_key_costs ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.shipping_key_costs SET
   created_by = 'SYS',
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.shipping_key_costs MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);   
   
-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.PRICE_HEADER Table
ALTER TABLE ftd_apps.price_header ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.price_header SET
   created_by = 'SYS',
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.price_header MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);  
   
-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.PRICE_HEADER_DETAILS Table
ALTER TABLE ftd_apps.price_header_details ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.price_header_details SET
   created_by = 'SYS',
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.price_header_details MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL); 
   
-- Add Updated_By, Updated_On to FTD_APPS.SOURCE_MASTER_RESERVE Table
ALTER TABLE ftd_apps.source_master_reserve ADD (
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);	
	
UPDATE ftd_apps.source_master_reserve SET
   updated_by = 'SYS';	
   
ALTER TABLE ftd_apps.source_master_reserve MODIFY (
   updated_by NOT NULL,
   updated_on NOT NULL);   
   
/******** Create New Table AZ_REINSTATE_ORDER_LOG in AMAZON Schema ***************************************/  
CREATE TABLE AMAZON.AZ_REINSTATE_ORDER_LOG (
   master_order_number		VARCHAR2(100) 				NOT NULL,
   context						VARCHAR2(255) 				NOT NULL,
   xml							CLOB 							NOT NULL,
   created_by					VARCHAR2(100)				NOT NULL,
   created_on					DATE DEFAULT SYSDATE		NOT NULL)
TABLESPACE AMAZON;

--CREATE UNIQUE INDEX AZ_REINSTATE_ORDER_PK1 ON AZ_REINSTATE_ORDER(MASTER_ORDER_NUMBER);

--CREATE UNIQUE INDEX AZ_REINSTATE_ORDER_UK1 ON AZ_REINSTATE_ORDER(CONTEXT);

--ALTER TABLE AZ_REINSTATE_ORDER ADD (CONSTRAINT AZ_REINSTATE_ORDER_PK1 PRIMARY KEY(MASTER_ORDER_NUMBER) USING INDEX TABLESPACE AMAZON_DATA;

--ALTER TABLE AZ_REINSTATE_ORDER ADD (CONSTRAINT AZ_REINSTATE_ORDER_UK1 UNIQUE (CONTEXT) USING INDEX TABLESPACE AMAZON_I;

--GRANT DELETE, INSERT, SELECT, UPDATE ON  AZ_REINSTATE_ORDER TO HP_PICK;

/******** Create New Tables in POP Schema ***************************************/  

-- POP.WLMT_INBND_TRANSMISSIONS_LOG 
CREATE TABLE POP.WLMT_INBND_TRANSMISSIONS_LOG (
   transmission_id			NUMBER		 				NOT NULL,
   xml							CLOB 							NOT NULL,
   created_by					VARCHAR2(100)				NOT NULL,
   created_on					DATE DEFAULT SYSDATE		NOT NULL)
TABLESPACE AMAZON;

-- POP.WLMT_ORDER_GATHERER_MSGS_LOG
CREATE TABLE POP.WLMT_ORDER_GATHERER_MSGS_LOG (
   id								NUMBER		 				NOT NULL,
   xml							CLOB 							NOT NULL,
   created_by					VARCHAR2(100)				NOT NULL,
   created_on					DATE DEFAULT SYSDATE		NOT NULL)
TABLESPACE AMAZON;

-- POP.WLMT_OUTBND_PARTNER_MSGS_LOG
CREATE TABLE POP.WLMT_OUTBND_PARTNER_MSGS_LOG (
   message_id					NUMBER		 				NOT NULL,
   xml							CLOB 							NOT NULL,
   created_by					VARCHAR2(100)				NOT NULL,
   created_on					DATE DEFAULT SYSDATE		NOT NULL)
TABLESPACE AMAZON;

/******** Create New Table REPROCESS_ORDERS_LOG in SCRUB Schema ***************************************/  
CREATE TABLE SCRUB.REPROCESS_ORDERS_LOG(
   order_guid					VARCHAR2(3000)				NOT NULL,
   created_by					VARCHAR2(100)				NOT NULL,
   created_on					DATE DEFAULT SYSDATE		NOT NULL)
TABLESPACE OP_DATA;

-- grant update privilege to global
GRANT UPDATE ON FTD_APPS.PRICE_HEADER_DETAILS TO GLOBAL;

update frp.global_parms set CONTEXT = 'FLORIST_RESUME_JOB_PKG.CHECK_FTDM_SHUTDOWN(''SYS'');'
where CONTEXT = 'FLORIST_RESUME_JOB_PKG.CHECK_FTDM_SHUTDOWN;';
