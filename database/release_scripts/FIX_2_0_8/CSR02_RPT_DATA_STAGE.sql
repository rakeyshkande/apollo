set define on

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/16/2006
***              Reason : Enhancement825: Create new member order distribution report
***             Purpose : This script re-populates the RPT report tables for this new report
***
*** Special Instructions: rptsvr_url = 'http://apollo.ftdi.com:7778/reports/rwservlet?' for dev5
***                                  = 'http://illiad-prod.ftdi.com:7779/reports/rwservlet?' for dev6
***
*****************************************************************************************/

select 'Inserting into rpt.report' from dual;
Insert into RPT.REPORT
   (REPORT_ID, NAME, DESCRIPTION, REPORT_FILE_PREFIX, REPORT_TYPE, REPORT_OUTPUT_TYPE, REPORT_CATEGORY, SERVER_NAME, HOLIDAY_INDICATOR, STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, ORACLE_RDF, REPORT_DAYS_SPAN, NOTES, ACL_NAME, RETENTION_DAYS, RUN_TIME_OFFSET, END_OF_DAY_REQUIRED, SCHEDULE_DAY, SCHEDULE_TYPE, SCHEDULE_PARM_VALUE)
 Values
   (100901, 'Member Order Distribution', 'This is a summary report detailing by zip code and month, the total orders taken for that zip code (dropship and floral) and how many of the floral orders were sent and filled by a specific florist number (or florist prefix number or florist central account number)', 'CSR02_Member_Order_Distribution', 
    'U', 'Char', 'Cust',
    '&&rptsvr_url',
    'Y', 'Active', SYSDATE, 'ddesai', SYSDATE, 'ddesai', 
    'CSR02_Member_Order_Distribution.rdf', 400, 'Date range cannot exceed 13 months', 'BaseCustReportAccess', 10, 
    120, 'N', NULL, NULL, NULL);

--------------------------------------------------------------------------------------------------------
select 'Inserting into rpt.report_parm' from dual;
Insert into RPT.REPORT_PARM
   (REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values
   (100901, 'Date Type', 'R', 'p_date_type', SYSDATE, 'ddesai', SYSDATE, 'ddesai', NULL);
   
Insert into RPT.REPORT_PARM
   (REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values
   (100902, null, 'TE', 'p_start_date,p_end_date', SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'StartDateEndDateCS02');
   
Insert into RPT.REPORT_PARM
   (REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values
   (100903, null, 'TE', 'p_option,p_data', SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'FloristInput');

--------------------------------------------------------------------------------------------------------
select 'Inserting into rpt.report_parm_ref' from dual;
Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100901, 100901, 1, 'Y', SYSDATE, 'ddesai', NULL);
   
Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100902, 100901, 2, 'Y', SYSDATE, 'ddesai', 'N');
   
Insert into RPT.REPORT_PARM_REF
   (REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values
   (100903, 100901, 3, 'Y', SYSDATE, 'ddesai', NULL);

--------------------------------------------------------------------------------------------------------
select 'Inserting into rpt.report_parm_value' from dual;
Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (1009011, SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'By Delivery Date', NULL, 'D');

Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (1009012, SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'By Order Date', NULL, 'O');
   
Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (100902, SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'START_END_DATE_TEMPLATE', NULL, NULL);
   
Insert into RPT.REPORT_PARM_VALUE
   (REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values
   (100903, SYSDATE, 'ddesai', SYSDATE, 'ddesai', 'FLORIST_INPUT_TEMPLATE', NULL, NULL); 
   
--------------------------------------------------------------------------------------------------------
select 'Inserting into rpt.report_parm_value_ref' from dual;
Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100901, 1009011, 1, SYSDATE, 'ddesai');

Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100901, 1009012, 2, SYSDATE, 'ddesai');
   
Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100902, 100902, 1, SYSDATE, 'ddesai');
   
Insert into RPT.REPORT_PARM_VALUE_REF
   (REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values
   (100903, 100903, 1, SYSDATE, 'ddesai');

--------------------------------------------------------------------------------------------------------

GRANT EXECUTE ON MERCURY.GET_MERCURY_ORDER_STATUS TO RPT;

commit;
/
