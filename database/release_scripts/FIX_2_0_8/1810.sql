ALTER TABLE clean.refund_disposition_val ADD (
auto_cancel_flag CHAR(1) DEFAULT('N') NOT NULL,
auto_cancel_a_flag CHAR(1) DEFAULT('N') NOT NULL,
auto_askp_flag CHAR(1) DEFAULT('N') NOT NULL);

ALTER TABLE clean.refund_disposition_val ADD (CONSTRAINT refund_disposition_val_ck4 CHECK (auto_cancel_flag IN ('Y','N')));
ALTER TABLE clean.refund_disposition_val ADD (CONSTRAINT refund_disposition_val_ck5 CHECK (auto_cancel_a_flag IN ('Y','N')));
ALTER TABLE clean.refund_disposition_val ADD (CONSTRAINT refund_disposition_val_ck6 CHECK (auto_askp_flag IN ('Y','N')));

update clean.refund_disposition_val
  set auto_cancel_flag = 'Y'
  where refund_disp_code in ('B10', 'B20', 'B22', 'B25', 'B30', 'B40', 'B50');
 
update clean.refund_disposition_val
  set auto_askp_flag = 'Y'
  where refund_disp_code in ('B10', 'B22', 'B25', 'B30', 'B50', 'B60');
 
update clean.refund_disposition_val
  set auto_cancel_a_flag = 'Y'
  where refund_disp_code = 'A10';

