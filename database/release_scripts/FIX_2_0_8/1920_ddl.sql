
GRANT EXECUTE ON FTD_APPS.FLORIST_QUERY_PKG TO RPT;

CREATE INDEX ftd_apps.florist_master_n06 ON ftd_apps.florist_master(TO_NUMBER(NVL(internal_link_number,'0000')))
TABLESPACE ftd_apps_indx ONLINE COMPUTE STATISTICS;

CREATE INDEX clean.order_details_n13 ON clean.order_details (delivery_date) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;

CREATE INDEX clean.order_details_n14 ON clean.order_details(ship_date) TABLESPACE clean_indx ONLINE COMPUTE STATISTICS;
