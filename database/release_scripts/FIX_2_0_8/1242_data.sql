insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values (
'Secure Configuration','Order Proc','Security surrounding Secure Configuration Parameters',sysdate,sysdate,'SYS');

insert into aas.acl (acl_name, created_on, updated_on, updated_by) values ('Configuration',sysdate,sysdate,'SYS');

INSERT into aas.rel_role_acl(role_id, acl_name, created_on, updated_on, updated_by) values (
200029, 'Configuration', sysdate,sysdate,'SYS');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) values (
'Configuration','Secure Configuration','Order Proc','View',sysdate,sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','ccSettlementFtpPassword','tmp','FTP password to retrieve the CC Settlement File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','ccSettlementFtpUsername','tmp','FTP username to retrieve the CC Settlement File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','eodFtpPassword','tmp','End of day ftp password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','eodFtpUsername','tmp','End of day ftp username',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','gcIntuitServerPassword','tmp','GC Intuit partner FTP password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','gcIntuitServerUsername','tmp','GC Intuit partner FTP username',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','gcVanillaServerPassword','tmp','GC Vanilla partner FTP password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','gcVanillaServerUsername','tmp','GC Vanilla partner FTP username',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','mercReconFtpPassword','tmp','FTP password to retrieve the Mercury Recon File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','mercReconFtpUsername','tmp','FTP username to retrieve the Mercury Recon File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','securityPrivateKeyPassphrase','tmp','Security private key passphrase',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','venusReconFtpPassword','tmp','FTP password to retrieve the Venus Recon File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('accounting_reporting','venusReconFtpUsername','tmp','FTP username to retrieve the Venus Recon File',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('amazon','user_name','tmp','Amazon user name',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('amazon','user_password','tmp','Amazon password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('email_request_processing','novator_mailbox_monitor_PASSWORD','tmp','Novator mailbox password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('email_request_processing','novator_mailbox_monitor_USERNAME','tmp','Novator mailbox user name',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','FloristRevenueData_FtpPswd','tmp','FloristRevenueData FTP password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','FloristRevenueData_FtpUser','tmp','FloristRevenueData FTP user',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','FloristSuspendDataFromEFOS_FtpPswd','tmp','FloristSuspendDataFromEFOS FTP password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','FloristSuspendDataFromEFOS_FtpUser','tmp','FloristSuspendDataFromEFOS FTP user',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','MemberData_FtpPswd','tmp','MemberData FTP password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('load_member_data','MemberData_FtpUser','tmp','MemberData FTP user',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','INBOUND_FTP_LOGON_00096','tmp','The logon for the wine.com inbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','INBOUND_FTP_LOGON_00119','tmp','The logon for the sams inbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','INBOUND_FTP_PASSWORD_00096','tmp','The password for the wine.com inbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','INBOUND_FTP_PASSWORD_00119','tmp','The password for the sams inbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','RECROF_FTP_LOGON','tmp','Recrof ftp username',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','RECROF_FTP_PASSWORD','tmp','Recrof ftp password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','SUSRES_FTP_LOGON','tmp','Susres ftp username',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','SUSRES_FTP_PASSWORD','tmp','Susres ftp password',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','VENDOR_00096_FTP_LOGON_1','tmp','The logon for the wine.com outbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','VENDOR_00096_FTP_PASSWORD_1','tmp','The password for the wine.com outbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','VENDOR_00119_FTP_LOGON_1','tmp','The logon for the sams outbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('order_processing','VENDOR_00119_FTP_PASSWORD_1','tmp','The password for the sams outbound ftp server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_order_processing','WALMART_HTTP_LOGON_VALUE','tmp','WalMart outbound username for partner order processing',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_order_processing','WALMART_HTTP_PASSWORD_VALUE','tmp','WalMart outbound password for partner order processing',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AIRCANADA_InboundFtpPswd','tmp','Inbound ftp password for Air Canada',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AIRCANADA_InboundFtpUser','tmp','Inbound ftp username for Air Canada',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AIRCANADA_OutboundFtpPswd','tmp','Outbound ftp password for Air Canada',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AIRCANADA_OutboundFtpUser','tmp','Outbound ftp username for Air Canada',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','ALASKAAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Alaska Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','ALASKAAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Alaska Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','ALLANT_OutboundFtpPswd','tmp','Outbound ftp password for Allant',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','ALLANT_OutboundFtpUser','tmp','Outbound ftp username for Allant',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAIRLINES_InboundFtpPswd','tmp','Inbound ftp password for American Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAIRLINES_InboundFtpUser','tmp','Inbound ftp username for American Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for American Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for American Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAUTOASSOC_OutboundFtpPswd','tmp','Outbound ftp password for American Auto Assoc',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANAUTOASSOC_OutboundFtpUser','tmp','Outbound ftp username for American Auto Assoc',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANEXPRESS_OutboundFtpPswd','tmp','Outbound ftp password for American Express',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','AMERICANEXPRESS_OutboundFtpUser','tmp','Outbound ftp username for American Express',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BASSHOTELS_InboundFtpPswd','tmp','Inbound ftp password for Bass Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BASSHOTELS_InboundFtpUser','tmp','Inbound ftp username for Bass Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BASSHOTELS_OutboundFtpPswd','tmp','Outbound ftp password for Bass Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BASSHOTELS_OutboundFtpUser','tmp','Outbound ftp username for Bass Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BENEVOLINK_OutboundFtpPswd','tmp','Outbound ftp password for Benevolink',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BENEVOLINK_OutboundFtpUser','tmp','Outbound ftp username for Benevolink',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BESTWESTERN_InboundFtpPswd','tmp','Inbound ftp password for Best Western',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BESTWESTERN_InboundFtpUser','tmp','Inbound ftp username for Best Western',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BESTWESTERN_OutboundFtpPswd','tmp','Outbound ftp password for Best Western',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','BESTWESTERN_OutboundFtpUser','tmp','Outbound ftp username for Best Western',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CENDANTHOTELS_InboundFtpPswd','tmp','Inbound ftp password for Cendant Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CENDANTHOTELS_InboundFtpUser','tmp','Inbound ftp username for Cendant Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CENDANTHOTELS_OutboundFtpPswd','tmp','Outbound ftp password for Cendant Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CENDANTHOTELS_OutboundFtpUser','tmp','Outbound ftp username for Cendant Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CONTINENTALAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Continental Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','CONTINENTALAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Continental Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DELTAAIRLINES_InboundFtpPswd','tmp','Inbound ftp password for Delta Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DELTAAIRLINES_InboundFtpUser','tmp','Inbound ftp username for Delta Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DELTAAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Delta Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DELTAAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Delta Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DISCOVER_OutboundFtpPswd','tmp','Outbound ftp password for Discover',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','DISCOVER_OutboundFtpUser','tmp','Outbound ftp username for Discover',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','FLORISTZIPCODECOUNT_OutboundFtpPswd','tmp','Outbound ftp password for Florist Zip Code Count',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','FLORISTZIPCODECOUNT_OutboundFtpUser','tmp','Outbound ftp username for Florist Zip Code Count',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','GOLDPOINTS_OutboundFtpPswd','tmp','Outbound ftp password for Gold Points',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','GOLDPOINTS_OutboundFtpUser','tmp','Outbound ftp username for Gold Points',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','GREENPOINTS_OutboundFtpPswd','tmp','Outbound ftp password for Green Points',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','GREENPOINTS_OutboundFtpUser','tmp','Outbound ftp username for Green Points',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','HAWAIIANAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Hawaiian Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','HAWAIIANAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Hawaiian Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','HILTONHOTELS_OutboundFtpPswd','tmp','Outbound ftp password for Hilton Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','HILTONHOTELS_OutboundFtpUser','tmp','Outbound ftp username for Hilton Hotels',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MARRIOTT_InboundFtpPswd','tmp','Inbound ftp password for Marriot',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MARRIOTT_InboundFtpUser','tmp','Inbound ftp username for Marriot',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MARRIOTT_OutboundFtpPswd','tmp','Outbound ftp password for Marriot',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MARRIOTT_OutboundFtpUser','tmp','Outbound ftp username for Marriot',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MIDWESTAIRLINES_InboundFtpPswd','tmp','Inbound ftp password for Midwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MIDWESTAIRLINES_InboundFtpUser','tmp','Inbound ftp username for Midwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MIDWESTAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Midwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','MIDWESTAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Midwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','NORTHWESTAIRLINES_InboundFtpPswd','tmp','Inbound ftp password for Northwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','NORTHWESTAIRLINES_InboundFtpUser','tmp','Inbound ftp username for Northwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','NORTHWESTAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for Northwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','NORTHWESTAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for Northwest Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','QUIXTARFLORAGIFT_OutboundFtpPswd','tmp','Outbound ftp password for QUIXTAR/FLORAGIFT',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','QUIXTARFLORAGIFT_OutboundFtpUser','tmp','Outbound ftp username for QUIXTAR/FLORAGIFT',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UNITEDAIRLINES_InboundFtpPswd','tmp','Inbound ftp password for United Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UNITEDAIRLINES_InboundFtpUser','tmp','Inbound ftp username for United Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UNITEDAIRLINES_OutboundFtpPswd','tmp','Outbound ftp password for United Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UNITEDAIRLINES_OutboundFtpUser','tmp','Outbound ftp username for United Airlines',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UPROMISE_OutboundFtpPswd','tmp','Outbound ftp password for Upromise',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','UPROMISE_OutboundFtpUser','tmp','Outbound ftp username for Upromise',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','USAIRWAYS_InboundFtpPswd','tmp','Inbound ftp password for US Airways',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','USAIRWAYS_InboundFtpUser','tmp','Inbound ftp username for US Airways',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','USAIRWAYS_OutboundFtpPswd','tmp','Outbound ftp password for US Airways',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('partner_reward','USAIRWAYS_OutboundFtpUser','tmp','Outbound ftp username for US Airways',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','application.mail.login','tmp','Username for dot com server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','application.mail.password','tmp','Password for dot com server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.login1','tmp','Username for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.login2','tmp','Username for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.login3','tmp','Username for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.login4','tmp','Username for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.password1','tmp','Password for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.password2','tmp','Password for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.password3','tmp','Password for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','ftd.server.password4','tmp','Password for FTD image server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.content.server.login1','tmp','Username for Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.content.server.password1','tmp','Password for Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.live.server.login1','tmp','Username for live Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.live.server.password1','tmp','Password for live Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.test.server.login1','tmp','Username for test Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.test.server.password1','tmp','Password for test Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.uat.server.login1','tmp','Username for uat Novator server',sysdate,'SYS',sysdate,'SYS');

insert into frp.secure_config (context, name, value, description, created_on, created_by, updated_on, updated_by) values
('product_database','novator.uat.server.password1','tmp','Password for uat Novator server',sysdate,'SYS',sysdate,'SYS');

insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) values
('accounting_reporting', 'gcProgramGenericServerUsername', 'tmp', 'GC Generic FTP username', sysdate, 'SYS', sysdate, 'SYS');

insert into FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) values
('accounting_reporting', 'gcProgramGenericServerPassword', 'tmp', 'GC Generic FTP password', sysdate, 'SYS', sysdate, 'SYS');

