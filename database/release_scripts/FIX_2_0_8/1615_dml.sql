update clean.refund_disposition_val
set    description = 'Discount Applied', short_description = 'Discount Applied', refund_accounting_type = 'CR'
where  refund_disp_code = 'C10';

delete from clean.refund_disp_partner_ref where refund_disp_code = 'C20';

delete from clean.refund_disposition_val where refund_disp_code = 'C20';

update clean.refund_disposition_val
set    short_description = 'GC/Coupon'
where  refund_disp_code = 'C30';

