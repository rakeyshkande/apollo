update rpt.report
set report_output_type = 'Char',
    status = 'Active',
    updated_by = user,
    updated_on = sysdate
where name = 'Customer Recap (Monthly)';

grant execute on clean.rebuild_cust_recap_tab to rpt;