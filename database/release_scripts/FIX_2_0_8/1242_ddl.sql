CREATE TABLE FRP.SECURE_CONFIG
  (
    context VARCHAR2(100) NOT NULL,
    name VARCHAR2(100) NOT NULL,
    value VARCHAR2(2500) NOT NULL,
    description VARCHAR2(255) NOT NULL,
    created_on DATE NOT NULL,
    created_by varchar2(100) NOT NULL,
    updated_on DATE NOT NULL,
    updated_by varchar2(100) NOT NULL
  ) TABLESPACE frp_data;

ALTER TABLE frp.secure_config ADD (CONSTRAINT secure_config_pk PRIMARY KEY (context,name) USING INDEX TABLESPACE frp_indx);

CREATE TABLE frp.secure_config$ TABLESPACE frp_data AS SELECT * FROM frp.secure_config WHERE ROWNUM < 1;

ALTER TABLE frp.secure_config$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE DEFAULT SYSDATE NOT NULL);
