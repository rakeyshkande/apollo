insert into aas.acl (acl_name, created_on, updated_on, updated_by)
values ('Comp Order', sysdate, sysdate, 'rlazuk');

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by)
values ('CompOrder','Order Proc','Comp Order Resource',sysdate,sysdate,'rlazuk');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by)
values ('Comp Order','CompOrder','Order Proc','Yes', sysdate, sysdate,'rlazuk');

insert into aas.rel_role_acl(role_id, acl_name, created_on, updated_on, updated_by)
values (200020,'Comp Order', sysdate, sysdate, 'rlazuk');
