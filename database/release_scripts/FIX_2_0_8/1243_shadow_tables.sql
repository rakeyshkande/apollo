/******** Create Shadow Tables in the FRP Schema ****************************************************/

-- FRP.GLOBAL_PARMS$

CREATE TABLE frp.global_parms$ TABLESPACE frp_data AS SELECT * FROM frp.global_parms WHERE ROWNUM < 1;
ALTER TABLE frp.global_parms$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX frp.global_parms$_N1 ON frp.global_parms$ (timestamp$) TABLESPACE frp_shadow_indx;

COMMENT ON TABLE FRP.GLOBAL_PARMS$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the global_parms table.';

--GRANT SELECT ON  GLOBAL_PARMS$ TO RPT;

/******** Create Shadow Tables in the FTD_APPS Schema ****************************************************/

-- FTD_APPS.SNH$

CREATE TABLE ftd_apps.snh$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.snh WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.snh$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.snh$_N1 ON ftd_apps.snh$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SNH$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the snh table.';

-- FTD_APPS.SOURCE_TYPE_VAL$

CREATE TABLE ftd_apps.source_type_val$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.source_type_val WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.source_type_val$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.source_type_val$_N1 ON ftd_apps.source_type_val$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SOURCE_TYPE_VAL$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the source_type_val table.';

-- FTD_APPS.FTDM_SHUTDOWN$

CREATE TABLE ftd_apps.ftdm_shutdown$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.ftdm_shutdown WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.ftdm_shutdown$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.ftdm_shutdown$_N1 ON ftd_apps.ftdm_shutdown$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.FTDM_SHUTDOWN$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the ftdm_shutdown table.';

-- FTD_APPS.SHIPPING_KEYS$

CREATE TABLE ftd_apps.shipping_keys$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.shipping_keys WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.shipping_keys$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.shipping_keys$_N1 ON ftd_apps.shipping_keys$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SHIPPING_KEYS$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the shipping_keys table.';

-- FTD_APPS.SHIPPING_KEY_DETAILS$

CREATE TABLE ftd_apps.shipping_key_details$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.shipping_key_details WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.shipping_key_details$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.shipping_key_details$_N1 ON ftd_apps.shipping_key_details$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SHIPPING_KEY_DETAILS$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the shipping_key_details table.';

-- FTD_APPS.SHIPPING_KEY_COSTS$

CREATE TABLE ftd_apps.shipping_key_costs$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.shipping_key_costs WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.shipping_key_costs$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.shipping_key_costs$_N1 ON ftd_apps.shipping_key_costs$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SHIPPING_KEY_COSTS$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the shipping_key_costs table.';

-- FTD_APPS.SOURCE_MASTER_RESERVE$

CREATE TABLE ftd_apps.source_master_reserve$ TABLESPACE ftd_apps_data AS SELECT * FROM ftd_apps.source_master_reserve WHERE ROWNUM < 1;
ALTER TABLE ftd_apps.source_master_reserve$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX ftd_apps.source_master_reserve$_N1 ON ftd_apps.source_master_reserve$ (timestamp$) TABLESPACE ftd_apps_indx;

COMMENT ON TABLE FTD_APPS.SOURCE_MASTER_RESERVE$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the source_master_reserve table.';

/******** Create Shadow Tables in the CLEAN Schema ****************************************************/

-- CLEAN.GC_COUPON_REQUEST$

CREATE TABLE clean.gc_coupon_request$ TABLESPACE clean_data AS SELECT * FROM clean.gc_coupon_request WHERE ROWNUM < 1;
ALTER TABLE clean.gc_coupon_request$ ADD (operation$ VARCHAR2(7) NOT NULL, timestamp$ DATE NOT NULL);
--CREATE INDEX clean.gc_coupon_request$_N1 ON clean.gc_coupon_request$ (timestamp$) TABLESPACE clean_indx;

COMMENT ON TABLE CLEAN.GC_COUPON_REQUEST$ IS 'Used primarily for auditing purposes, this table captures a snapshot of changes made to a source record including the type of operation and the timestamp of the operation.  This history table is populated by a trigger in the gc_coupon_request table.';

/******** Alter Shadow Tables in the FTD_APPS Schema ****************************************************/

-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.PRICE_HEADER$ Table
ALTER TABLE ftd_apps.price_header$ ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);
	
UPDATE ftd_apps.price_header$ SET 
   created_by = 'SYS',
   updated_by = 'SYS';
   
ALTER TABLE ftd_apps.price_header$ MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);

-- Add Created_By, Created_On, Updated_By, Updated_On to FTD_APPS.PRICE_HEADER_DETAILS$ Table
ALTER TABLE ftd_apps.price_header_details$ ADD (
	created_by VARCHAR2(100), 
	created_on DATE DEFAULT SYSDATE,
	updated_by VARCHAR2(100),
	updated_on DATE DEFAULT SYSDATE);
	
UPDATE ftd_apps.price_header_details$ SET 
   created_by = 'SYS',
   updated_by = 'SYS';
   
ALTER TABLE ftd_apps.price_header_details$ MODIFY (
   created_by NOT NULL,
   created_on NOT NULL,
   updated_by NOT NULL,
   updated_on NOT NULL);

