------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                                   01/24/2017  --------
-- defect SGC-1           (kmohamme   ) (278810 )       ----------------------------
------------------------------------------------------------------------------------

-- This requires GG change (Stop EXTRACT pex and ppump, stop REPLICAT pftd1 and pftd3)

ALTER TABLE FTD_APPS.SNH ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;
ALTER TABLE FTD_APPS.SERVICE_FEE_OVERRIDE ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;
ALTER TABLE FTD_APPS.SNH_HIST ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;
ALTER TABLE FTD_APPS.SERVICE_FEE_OVERRIDE_HIST ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;


ALTER TABLE FTD_APPS.SNH$ ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;
ALTER TABLE FTD_APPS.SERVICE_FEE_OVERRIDE$ ADD SAME_DAY_UPCHARGE_FS NUMBER default 0 NOT NULL;


------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                                01/24/2017  --------
-- defect SGC-1              (kmohamme ) (278810)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                                   01/24/2017  --------
-- defect SGC-2           (kmohamme   ) (278810 )       ----------------------------
------------------------------------------------------------------------------------

-- This requires GG change (Stop EXTRACT pex and ppump, stop REPLICAT pftd2 and pftd3)

ALTER TABLE FTD_APPS.SOURCE_MASTER ADD SAME_DAY_UPCHARGE_FS CHAR(1) default 'D' NOT NULL;
ALTER TABLE FTD_APPS.SOURCE_MASTER$ ADD SAME_DAY_UPCHARGE_FS CHAR(1) default 'D' NOT NULL;



ALTER TABLE  FTD_APPS.SOURCE_MASTER ADD CONSTRAINT SOURCE_MASTER_CK15 CHECK (SAME_DAY_UPCHARGE_FS IN ('Y','N','D'));

create or replace view ftd_apps.source as
SELECT
        sm.source_code,
        sm.marketing_group source_type,
        sm.department_code,
        sm.description,
        sm.start_date,
        sm.end_date,
        sm.price_header_id pricing_code,
        sm.snh_id shipping_code,
        spr.program_name partner_id,
        sm.payment_method_id valid_pay_method,
        sm.list_code_flag,
        sm.default_source_code_flag,
        sm.billing_info_prompt,
        sm.billing_info_logic,
        sm.source_type marketing_group,
        sm.external_call_center_flag,
        sm.highlight_description_flag,
        sm.discount_allowed_flag,
        sm.bin_number_check_flag,
        sm.order_source,
        null mileage_bonus,
        sm.promotion_code,
        pr.calculation_basis mileage_calculation_source,
        pr.bonus_points,
        pr.bonus_calculation_basis bonus_type,
        pr.bonus_separate_data separate_data,
        sm.yellow_pages_code,
        pr.points points_miles_per_dollar,
        pr.maximum_points maximum_points_miles,
        sm.jcpenney_flag,
        sm.default_source_code_flag oe_default_flag,
        sm.company_id,
        sm.send_to_scrub,
        sm.fraud_flag,
        sm.enable_lp_processing,
        sm.emergency_text_flag,
        sm.requires_delivery_confirmation,
        sm.webloyalty_flag,
        cm.company_name,
        cm.internet_origin,
        sm.primary_backup_rwd_flag,
        sm.related_source_code,
        sm.apply_surcharge_code,
	sm.surcharge_description,
	sm.display_surcharge_flag,
	sm.surcharge_amount,
	sm.allow_free_shipping_flag,
	pp.partner_name,
	pp.program_type,
	pr.reward_type,
	sm.same_day_upcharge,
    sm.display_same_day_upcharge,
    sm.morning_delivery_flag,
    sm.morning_delivery_free_shipping,
    sm.same_day_upcharge_fs
FROM    ftd_apps.source_master sm
LEFT OUTER JOIN
(
        select  sp.source_code,
                sp.program_name
        from    ftd_apps.source_program_ref sp
        where   sp.start_date =
                (
                        select  max(b.start_date)
                        from    ftd_apps.source_program_ref b
                        where   b.source_code = sp.source_code
                        and     b.start_date <= sysdate
                )
) spr
ON      sm.source_code = spr.source_code
LEFT OUTER JOIN ftd_apps.program_reward pr
ON      spr.program_name = pr.program_name
LEFT OUTER JOIN	ftd_apps.partner_program pp
ON	pp.program_name = spr.program_name
JOIN ftd_apps.company_master cm
ON      cm.company_id = sm.company_id;


------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                                01/24/2017  --------
-- defect SGC-2              (kmohamme ) (278810)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Rose Lazuk                                02/01/2017  --------
-- defect SGC-3           (mark       ) (282179 )       ----------------------------
------------------------------------------------------------------------------------

alter table scrub.order_details add (original_order_has_sdufs char(1));

------------------------------------------------------------------------------------
-- End   requested by Rose Lazuk                                02/01/2017  --------
-- defect SGC-3           (mark       ) (282179 )       ----------------------------
------------------------------------------------------------------------------------


 -- =================================================================================
-- END RELEASE 10.1.0.1
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               02/21/2017  --------
-- defect ANO-1           (mark       ) (298716 )       ----------------------------
------------------------------------------------------------------------------------

-- Ariba NYL invoicing (ANO-1)
-- Don't allow price increases in scrub for Ariba orders
update ptn_pi.partner_mapping set is_incr_total_allowed='N' where ftd_origin_mapping='ARI';

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               02/21/2017  --------
-- defect ANO-1           (mark       ) (298716 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                02/28/2017  --------
-- defect Q2SP17-32       (mark       ) (303143 )       ----------------------------
------------------------------------------------------------------------------------

update frp.global_parms
set name = 'EFOS_SPECIAL_DOWN_END_TIME', updated_on = sysdate, updated_by = 'Q2SP17-32'
where name = 'EFOS_SUNDAY_DOWN_END_TIME';

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                02/28/2017  --------
-- defect Q2SP17-32       (mark       ) (303143 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Kurella, Swathi                           03/03/2017  --------
-- defect Q2sp17-17       (kmohamme     ) (305532 )       ----------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms values('LOAD_MEMBER_DATA_CONFIG','MEMBER_WEIGHT','8','Q2SP17-17',sysdate,'Q2SP17-17',sysdate,'This Parameter is used to set the Participate points or weight for new members');
insert into frp.global_parms values('LOAD_MEMBER_DATA_CONFIG','MEMBER_DURATION','180','Q2SP17-17',sysdate,'Q2SP17-17',sysdate,'This Parameter is used to set the duration i.e start and end date for new members'); 

------------------------------------------------------------------------------------
-- End   requested by Kurella, Swathi                           03/03/2017  --------
-- defect Q2sp17-17       (kmohamme    ) (305532 )       ----------------------------
------------------------------------------------------------------------------------

 -- =================================================================================
-- END RELEASE 10.1.0.2
-- --------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by  Surimenu, Bhargava Swamy                           03/23/2017  --------
-- defect SP-305          (kmohamme   ) ( 312255 )   ----------------------------
------------------------------------------------------------------------------------

exec dbms_aqadm.alter_queue(queue_name=>'OJMS.ACCOUNT', max_retries=>5, retry_delay=>10, comment=> 'Retry and Delay is set to 5 times and 10 Sec Resp.');

------------------------------------------------------------------------------------
-- End   requested by Surimenu, Bhargava Swamy                            03/23/2017  --------
-- defect SP-305         (kmohamme   ) ( 312255 )   ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               04/06/2017  --------
-- defect ANO-5           (mark       ) (316502 )       ----------------------------
------------------------------------------------------------------------------------

UPDATE PTN_PI.PARTNER_MAPPING SET SEND_ORD_INVOICE = 'Y' WHERE PARTNER_ID = 'ARB_SAKS';
UPDATE PTN_PI.PARTNER_MAPPING SET SEND_ORD_INVOICE = 'Y' WHERE PARTNER_ID = 'ARB_MS';

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               04/06/2017  --------
-- defect ANO-5           (mark       ) (316502 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               04/02/2017  --------
-- defect ANO-2           (mark       ) (315250 )       ----------------------------
------------------------------------------------------------------------------------

-- Ariba NYL invoicing (ANO-2)
INSERT INTO PTN_PI.PARTNER_MAPPING (PARTNER_ID, PARTNER_NAME, FTD_ORIGIN_MAPPING, 
MASTER_ORDER_NO_PREFIX, CONF_NUMBER_PREFIX, PARTNER_IMAGE, DEFAULT_SOURCE_CODE, 
DEFAULT_RECALC_SOURCE_CODE, BILLING_ADDRESS_LINE_1, BILLING_ADDRESS_LINE_2, BILLING_CITY, 
BILLING_STATE_PROVINCE, BILLING_POSTAL_CODE, BILLING_COUNTRY, SAT_DELIVERY_ALLOWED_DROPSHIP, 
SAT_DELIVERY_ALLOWED_FLORAL, SUN_DELIVERY_ALLOWED_FLORAL, CREATED_ON, CREATED_BY, 
UPDATED_ON, UPDATED_BY, SEND_ORD_CONF_EMAIL, SEND_SHIP_CONF_EMAIL, CREATE_STOCK_EMAIL, 
SEND_DELIVERY_CONF_EMAIL, AVS_FLORAL, AVS_DROPSHIP, SEND_DCON_FEED, UPDATE_ORDER_FLAG, 
INCLUDE_ADDON, APPLY_DEF_PTN_BEHAVIOR, SEND_ORD_STATUS_UPDATE, SEND_ORD_INVOICE, 
SEND_ORD_ADJ_FEED, SEND_ORD_FMT_FEED, SEND_SHIP_STATUS_UPDATE, IS_INCR_TOTAL_ALLOWED) 
VALUES ('ARB_NYL', 'Ariba New York Life', 'ARI', '-NA-', '-NA-', '-NA-', '35457', '35457', '3113 Woodcreek Drive', 
' ', 'Downers Grove', 'IL', '60515', 'US', 'Y', 'Y', 'Y', sysdate, 'ANO-2', sysdate, 'ANO-2', 'N', 'N', 'N', 'N', 
'N', 'Y', 'N', 'N', 'N', 'N', 'Y', 'Y', 'N', 'N', 'Y', 'N');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               04/02/2017  --------
-- defect ANO-2           (mark       ) (315250 )       ----------------------------
------------------------------------------------------------------------------------

