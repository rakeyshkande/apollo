------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               04/5/2017  --------
-- defect         (Syed       ) (Change 17262 )       ----------------------------
------------------------------------------------------------------------------------

A new cron job has to be scheduled as below

0,30 23,0,1,2,3,4,5 * * * /usr/local/admin/scripts/monitor/mobi_alerts.sh jupiter1 TBLT,MOBI 0 30
0                 6 * * * /usr/local/admin/scripts/monitor/mobi_alerts.sh jupiter1 TBLT,MOBI 0 30
15,30,45          6 * * * /usr/local/admin/scripts/monitor/mobi_alerts.sh jupiter1 TBLT,MOBI 0 15
0,15,30,45 7-22 * * * /usr/local/admin/scripts/monitor/mobi_alerts.sh jupiter1 TBLT,MOBI 0 15

The scripts are checked in to DBA repository - 
 DBA\Oracle\Apollo\monitor\mobi_alerts.sh
 DBA\Oracle\Apollo\monitor\mobi_alerts.sql

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               04/5/2017  --------
-- defect         (Syed       ) (Change 17262 )       ----------------------------
------------------------------------------------------------------------------------

