------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                            01/24/2017  --------
-- defect SGC-1           (kmohamme   ) (278810 )       ----------------------------
------------------------------------------------------------------------------------

-- This requires GG change (Stop EXTRACT pex and ppump, stop REPLICAT pftd1 and pftd3)

use ftd_apps;

ALTER TABLE snh ADD SAME_DAY_UPCHARGE_FS decimal(5,2);
ALTER TABLE service_fee_override ADD SAME_DAY_UPCHARGE_FS decimal(5,2);
ALTER TABLE snh_hist ADD SAME_DAY_UPCHARGE_FS decimal(5,2);
ALTER TABLE service_fee_override_hist ADD SAME_DAY_UPCHARGE_FS decimal(5,2);

UPDATE snh SET SAME_DAY_UPCHARGE_FS = 0;
UPDATE service_fee_override SET SAME_DAY_UPCHARGE_FS = 0;
UPDATE snh_hist SET SAME_DAY_UPCHARGE_FS = 0;
UPDATE service_fee_override_hist SET SAME_DAY_UPCHARGE_FS = 0;


------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                            01/24/2017  --------
-- defect SGC-1              (kmohamme ) (278810)       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                            01/24/2017  --------
-- defect SGC-2           (kmohamme   ) (278810 )       ----------------------------
------------------------------------------------------------------------------------

-- This requires GG change (Stop EXTRACT pex and ppump, stop REPLICAT pftd2 and pftd3)

use ftd_apps;

ALTER TABLE source_master  ADD SAME_DAY_UPCHARGE_FS CHAR(1);
ALTER TABLE source_master$ ADD SAME_DAY_UPCHARGE_FS CHAR(1);

UPDATE source_master  SET SAME_DAY_UPCHARGE_FS = 'D';
UPDATE source_master$ SET SAME_DAY_UPCHARGE_FS = 'D';


------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                            01/24/2017  --------
-- defect SGC-2              (kmohamme ) (278810)       ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.1.0.1
-- ---------------------------------------------------------------------------------

