------------------------------------------------------------------------------------
-- Begin requested by Sri Meka                                  02/22/2017  --------
-- defect ACOR-4             (mark       ) ( 301267 )   ----------------------------
------------------------------------------------------------------------------------

-- Stop replicats on clean, venus and mercury as needed to avoid locks during DML operations.

use clean;
create index orders_n8 on orders (order_date) using btree;

use mercury;
alter table mercury add column REFERENCE_NUMBER_INT decimal(12,0) default null;

update mercury set reference_number_int = convert(reference_number, decimal(12,0));

create index mercury_refno_int_ndx on mercury(reference_number_int) using btree;

alter table venus add column REFERENCE_NUMBER_INT decimal(12,0) default null;

update venus set reference_number_int = convert(reference_number, decimal(12,0));

create index venus_refno_int_ndx on venus(reference_number_int) using btree;

-- Start the clean replicat

-- Modify the replicats for venus and mercury to populate the REFERENCE_NUMBER_INT field
-- with a COLMAP clause. Start the venus and mercury replicats

------------------------------------------------------------------------------------
-- End   requested by Sri Meka                                  02/22/2017  --------
-- defect ACOR-4             (mark       ) ( 301267 )   ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.1.0.2
-- ---------------------------------------------------------------------------------

