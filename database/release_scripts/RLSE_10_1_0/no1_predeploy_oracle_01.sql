------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                                   02/01/2017  --------
-- defect SGC-2           (kmohamme   ) (281873 )       ----------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS
( context ,name ,value ,created_on,created_by ,updated_on ,updated_by ,description)
values ( 'FTDAPPS_PARMS' ,'SAME_DAY_UPCHARGE_FS_MEMBERS','N',sysdate ,'SGC-2'
,sysdate ,' SGC-2','This parm is used to turn ON or OFF the Same Day Upcharge FS Members functionality.');

------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                                02/01/2017  --------
-- defect SGC-2           (kmohamme   ) (281873 )       ----------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Salla Manasa                                   02/03/2017  --------
-- defect Q2SP17-7           (kmohamme   ) (282979 )       ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS ( CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES ( 'GLOBAL_CONFIG', 'TABLET_MOBILE_ALERTS', 'Y', 'Q2SP17-7', SYSDATE, 'Q2SP17-7', SYSDATE,
'Flag to check if mailing is enabled for mobile/tablet orders counts');
 
 
 INSERT INTO SITESCOPE.PROJECTS(PROJECT, MAILSERVER_NAME, DESCRIPTION ) 
 VALUES('Order Count Alerts', 'alerts1.ftdalerts.com', 'Mail alert for Minimum order count');

 INSERT INTO SITESCOPE.PAGERS(PROJECT, PAGER_NUMBER) VALUES ('Order Count Alerts','NOC@ftdi.com');

------------------------------------------------------------------------------------
-- End   requested by Salla Manasa                                02/03/2017  --------
-- defect Q2SP17-7              (kmohamme ) (282979)       ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.1.0.1
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Salla Manasa                                   02/16/2017  --------
-- defect Q2SP17-9           (kmohamme   ) ( 296124 )       ----------------------------
------------------------------------------------------------------------------------

insert into frp.global_parms values('ORDER_PROCESSING','DELIVERY_CONFIRMATION_CONTROL_DAYS','5','Q2SP17-9',sysdate,'Q2SP17-9',sysdate,'Control to configure number of days Dcon emails can be sent.');

------------------------------------------------------------------------------------
-- End   requested by Salla Manasa                                02/16/2017  --------
-- defect Q2SP17-9              (kmohamme ) ( 296124 )       ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.1.0.2
-- ---------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------
-- Begin requested by Tim Schmig                                02/28/2017  --------
-- defect Q2SP17-32          (mark       ) ( 303143 )   ----------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY,
    DESCRIPTION)
VALUES (
    'MERCURY_INTERFACE_CONFIG',
    'EFOS_SPECIAL_END_TIME_DAYS',
    'MONDAY',
    sysdate,
    'Q2SP17-32',
    sysdate,
    'Q2SP17-32',
    'Space delimited list of days that need to have a later EROS morning resume time');

------------------------------------------------------------------------------------
-- End   requested by Tim Schmig                                02/28/2017  --------
-- defect Q2SP17-32          (mark       ) ( 303143 )   ----------------------------
------------------------------------------------------------------------------------

    
 ------------------------------------------------------------------------------------
-- Begin requested by  Surimenu, Bhargava Swamy                           03/10/2017  --------
-- defect Q2SP17-16          (kmohamme   ) ( 307985 )   ----------------------------
------------------------------------------------------------------------------------

update Ptn_Pi.Partner_Mapping set Avs_Floral ='Y',Updated_On = Sysdate,Updated_By='Q2SP17-16' where Apply_Def_Ptn_Behavior='Y' and Avs_Floral ='N' ;
    
------------------------------------------------------------------------------------
-- End   requested by Surimenu, Bhargava Swamy                               02/28/2017  --------
-- defect Q2SP17-16          (kmohamme   ) ( 307985 )   ----------------------------
------------------------------------------------------------------------------------

-- =================================================================================
-- END RELEASE 10.1.0.3
-- ---------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               04/06/2017  --------
-- defect ANO-1              (mark       ) ( 316303 )   ----------------------------
------------------------------------------------------------------------------------

update clean.orders
set invoice_status = 'N/A'
where source_code in ('24796','7657','7798')
and origin_id = 'ARI'
and created_on < to_date('2017-04-01','YYYY-MM-DD');

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               04/06/2017  --------
-- defect ANO-1              (mark       ) ( 316303 )   ----------------------------
------------------------------------------------------------------------------------

