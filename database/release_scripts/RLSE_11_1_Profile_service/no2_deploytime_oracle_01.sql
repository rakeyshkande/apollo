-----------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- Begin requested by Manasa Salla     ---------------------------------------------
-- User  task US_12399  ()         -------------------------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PAYMENT_GATEWAY_DETOKENIZATION_URL', 'REPLACE ME', 'US_12399', sysdate, 'US_12399', sysdate, 'Detokenization URL for retrieving the CC numbers');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PROFILE_SERVICE_URL', 'REPLACE ME', 'US_12399', sysdate, 'US_12399', sysdate, 'Profile Service URL');


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PROFILE_SERVICE_ENABLED', 'N', 'US_12399', sysdate, 'US_12399', sysdate, 'Profile Service enable flag');


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PROFILE_WS_SOCKET_TIMEOUT', '10000', 'US_12399', sysdate, 'US_12399', sysdate, 'Profile Service socket timeout');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'PROFILE_TIMEOUT_ENABLE_FLAG', 'OFF', 'US_12399', sysdate, 'US_12399', sysdate, 'Profile Service timeout enable flag');

---------------------------------------------------------------------------------------------
-- End   requested by Manasa Salla  ---------------------------------------------------------
-- User task US_12399         ---------------------------------------------------------------
---------------------------------------------------------------------------------------------
