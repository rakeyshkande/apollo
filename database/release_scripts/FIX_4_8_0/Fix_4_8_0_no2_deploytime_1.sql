set define off

alter table ftd_apps.fuel_surcharge add (
send_surcharge_to_florist_flag varchar2(1) default 'N' not null,
surcharge_description varchar2(200),
display_surcharge_flag varchar2(1) default 'N' not null,
apply_surcharge varchar2(1) default'N' not null);

alter table ftd_apps.fuel_surcharge add constraint fuel_surcharge_ck1 check
(send_surcharge_to_florist_flag in ('Y','N'));

alter table ftd_apps.fuel_surcharge add constraint fuel_surcharge_ck2 check
(display_surcharge_flag in ('Y','N'));

alter table ftd_apps.fuel_surcharge add constraint fuel_surcharge_ck3 check
(apply_surcharge in ('Y','N'));

comment on column FTD_APPS.FUEL_SURCHARGE.SEND_SURCHARGE_TO_FLORIST_flag is 
'flag for for finding out if we need to pass on surcharge to florist';
comment on column FTD_APPS.FUEL_SURCHARGE.SURCHARGE_DESCRIPTION is 'Surcharge desription';
comment on column FTD_APPS.FUEL_SURCHARGE.DISPLAY_SURCHARGE_flag is 
'Flag for finding out we need to show surcharge in order confirmation email as a separate line';
comment on column FTD_APPS.FUEL_SURCHARGE.APPLY_SURCHARGE is 
'Flag for specifying if we want to use a surcharge fee or not';

alter table ftd_apps.source_master add (
apply_surcharge varchar2(1) default 'N' not null,
surcharge_description varchar2(200),
display_surcharge_flag varchar2(1) default 'N' not null,
surcharge_amount number(12,2));

alter table ftd_apps.source_master add constraint source_master_ck14 check (apply_surcharge in ('Y','N'));

alter table ftd_apps.source_master add constraint source_master_ck15 check (display_surcharge_flag in ('Y','N'));

comment on column ftd_apps.SOURCE_MASTER.APPLY_SURCHARGE is
'flag for for finding out if we need to use a surcharge fee or not';
comment on column ftd_apps.SOURCE_MASTER.SURCHARGE_DESCRIPTION is 'Surcharge desription';
comment on column ftd_apps.SOURCE_MASTER.DISPLAY_SURCHARGE_flag is
'Flag for finding out we need to show surcharge in order confirmation email as a separate line';
comment on column ftd_apps.SOURCE_MASTER.SURCHARGE_AMOUNT is
'Surcharge fee for shipping/fuel/service charged to customer';

alter table ftd_apps.source_master$ add (
RECPT_LOCATION_TYPE                                VARCHAR2(40),
RECPT_BUSINESS_NAME                                VARCHAR2(50),
RECPT_LOCATION_DETAIL                              VARCHAR2(20),
RECPT_ADDRESS                                      VARCHAR2(90),
RECPT_ZIP_CODE                                     VARCHAR2(12),
RECPT_CITY                                         VARCHAR2(30),
RECPT_STATE_ID                                     VARCHAR2(2),
RECPT_COUNTRY_ID                                   VARCHAR2(2),
RECPT_PHONE                                        VARCHAR2(20),
RECPT_PHONE_EXT                                    VARCHAR2(10),
CUST_FIRST_NAME                                    VARCHAR2(25),
CUST_LAST_NAME                                     VARCHAR2(25),
CUST_DAYTIME_PHONE                                 VARCHAR2(20),
CUST_DAYTIME_PHONE_EXT                             VARCHAR2(10),
CUST_EVENING_PHONE                                 VARCHAR2(20),
CUST_EVENING_PHONE_EXT                             VARCHAR2(10),
CUST_ADDRESS                                       VARCHAR2(90),
CUST_ZIP_CODE                                      VARCHAR2(12),
CUST_CITY                                          VARCHAR2(30),
CUST_STATE_ID                                      VARCHAR2(2),
CUST_COUNTRY_ID                                    VARCHAR2(2),
CUST_EMAIL_ADDRESS                                 VARCHAR2(100),
apply_surcharge varchar2(1),
surcharge_description varchar2(200),
display_surcharge_flag varchar2(1),
surcharge_amount number(12,2));

alter table ftd_apps.fuel_surcharge modify (surcharge_description varchar2(50));

alter table ftd_apps.fuel_surcharge drop constraint fuel_surcharge_ck3;

alter table ftd_apps.fuel_surcharge drop column apply_surcharge;

alter table ftd_apps.fuel_surcharge add (apply_surcharge_code varchar2(4) default 'OFF' not null);

comment on column ftd_apps.fuel_surcharge.apply_surcharge_code is 
'Code for specifying if we want to use a surcharge fee or not.';

alter table ftd_apps.fuel_surcharge add constraint fuel_surcharge_ck3 check
(apply_surcharge_code in ('ON','OFF','OFFC'));

alter table ftd_apps.source_master modify (surcharge_description varchar2(50));
alter table ftd_apps.source_master$ modify (surcharge_description varchar2(50));

alter table ftd_apps.source_master drop constraint source_master_ck14;

alter table ftd_apps.source_master drop column apply_surcharge;
alter table ftd_apps.source_master$ drop column apply_surcharge;

alter table ftd_apps.source_master add (apply_surcharge_code varchar2(4) default 'OFF' not null);
alter table ftd_apps.source_master$ add (apply_surcharge_code varchar2(4));

comment on column ftd_apps.source_master.apply_surcharge_code is 
'Code for specifying if we want to use a surcharge fee or not.';

alter table ftd_apps.source_master add constraint source_master_ck14 check
(apply_surcharge_code in ('ON','OFF','DEFAULT'));

alter table clean.order_details add (
apply_surcharge_code varchar2(4),
surcharge_description varchar2(50),
send_surcharge_to_florist_flag varchar2(1),
display_surcharge_flag varchar2(1));

alter table scrub.order_details add (
apply_surcharge_code varchar2(4),
surcharge_description varchar2(50),
send_surcharge_to_florist_flag varchar2(1),
display_surcharge_flag varchar2(1));

-- do these last as they may take a great deal of time.
alter table clean.order_details add constraint order_details_ck8 check (apply_surcharge_code in ('ON','OFF',null));
alter table clean.order_details add constraint order_details_ck9 check (send_surcharge_to_florist_flag in ('Y','N',null));
alter table clean.order_details add constraint order_details_ck10 check (display_surcharge_flag in ('Y','N',null));

-- begin inclusions from FIX_4_8_0_IT1
-- Insert new Product Type And Category for Services and Free Shipping
insert into ftd_apps.product_category (product_category_id, description)
values  ('SERVICES', 'Services');
insert into ftd_apps.product_types     (type_id, type_description) 
values  ('SERVICES', 'Services');
insert into ftd_apps.product_sub_types (sub_type_id, sub_type_description, type_id)
values ('FREESHIP', 'Free Shipping Membership', 'SERVICES');

insert into ftd_apps.product_category (product_category_id, description)
values  ('SRVCS', 'Services');
delete from ftd_apps.product_category where product_category_id='SERVICES';
insert into ftd_apps.product_attr_restr
    (product_attr_restr_id,
    product_attr_restr_name,
    product_attr_restr_oper,
    product_attr_restr_value,
    product_attr_restr_desc,
    java_method_name,
    created_on,
    created_by,
    updated_on,
    updated_by,
    display_order_seq_num)
  values
    (10,
    'PRODUCT_SUB_TYPE',
    '=',
    'FREESHIP',
    'Free Shipping Membership',
    'getProductSubType',
    sysdate,
    'Defect_6896_Release_4_8_0',
    sysdate,
    'Defect_6896_Release_4_8_0',
    10);

alter table clean.orders add (buyer_signed_in_flag char(1));
alter table clean.orders add constraint orders_ck1 check (buyer_signed_in_flag in ('Y','N',null));
alter table clean.order_details add (
	auto_renew_flag char(1), free_shipping_flag char(1));
alter table clean.order_details add constraint order_details_ck11 check (auto_renew_flag IN ('Y','N',null));
alter table clean.order_details add constraint order_details_ck12 check (free_shipping_flag IN ('Y','N',null));

alter table SCRUB.ORDER_DETAILS add (
SHIPPING_FEE_AMOUNT_SAVED Number,
SERVICE_FEE_AMOUNT_SAVED Number);

comment on column scrub.order_details.SHIPPING_FEE_AMOUNT_SAVED is 'Shipping Fee saved due to Free Shipping';
comment on column scrub.order_details.SERVICE_FEE_AMOUNT_SAVED is 'Service Fee saved due to Free Shipping';

alter table CLEAN.ORDER_BILLS add (
SHIPPING_FEE_SAVED Number,
SERVICE_FEE_SAVED Number);

comment on column clean.order_bills.SHIPPING_FEE_SAVED is 'Shipping Fee saved due to Free Shipping';
comment on column clean.order_bills.SERVICE_FEE_SAVED is 'Service Fee saved due to Free Shipping';

alter table CLEAN.ORDER_BILLS_UPDATE Add (
SHIPPING_FEE_SAVED Number,
SERVICE_FEE_SAVED Number);

comment on column clean.order_bills_update.SHIPPING_FEE_SAVED is 'Shipping Fee saved due to Free Shipping';
comment on column clean.order_bills_update.SERVICE_FEE_SAVED is 'Service Fee saved due to Free Shipping';

alter table scrub.orders add (buyer_signed_in_flag char(1));

alter table scrub.order_details add (
	auto_renew_flag char(1), free_shipping_flag char(1));

alter table ftd_apps.product_master add (
service_duration number,
allow_free_shipping_flag char(1) default 'Y' not null);

alter table ftd_apps.product_master add constraint product_master_ck20 check (allow_free_shipping_flag in ('Y','N'));

comment on column ftd_apps.product_master.service_duration is 'Duration of free shipping offer, in months';

alter table ftd_apps.product_master$ add (
service_duration number,
allow_free_shipping_flag char(1));

alter table ftd_apps.source_master add (allow_free_shipping_flag char(1) default 'Y' not null);
alter table ftd_apps.source_master add constraint source_master_ck6 check (allow_free_shipping_flag in ('Y','N'));
alter table ftd_apps.source_master$ add (allow_free_shipping_flag char(1));
-- end inclusions from FIX_4_8_0_IT1

insert into clean.comment_origin_val(comment_origin,description) values ('Call Disposition', 'Call Disposition');

alter table ftd_apps.fuel_surcharge drop constraint fuel_surcharge_ck2;

alter table ftd_apps.fuel_surcharge modify (display_surcharge_flag char(1));

alter table ftd_apps.fuel_surcharge modify (send_surcharge_to_florist_flag char(1));

alter table ftd_apps.source_master modify (display_surcharge_flag char(1) default 'D');

alter table ftd_apps.source_master drop constraint source_master_ck15;

alter table ftd_apps.source_master$ modify (display_surcharge_flag char(1));

alter table ftd_apps.source_master modify (apply_surcharge_code varchar2(7));

alter table ftd_apps.source_master$ modify (apply_surcharge_code varchar2(7));
-- END OF 4.8.0.1 QA DEPLOY

update clean.order_details od
set od.miles_points_post_date=(
select file_sent_date from bi_etl.va_order_rewarded where dd_order_detail_id=od.order_detail_id),
od.updated_on=sysdate,
od.updated_by='Defect_8223_Release_4_8_0'
where od.order_detail_id in
(select dd_order_detail_id from bi_etl.va_order_rewarded);

insert into clean.order_reward_posting(ORDER_DETAIL_ID,FILE_POST_DATE,FILE_NAME,SOURCE_CODE,PROGRAM_NAME,REWARD,
REWARD_DATE,FILE_RECORD_ID)
select bi.dd_order_detail_id, bi.file_sent_date, 'FTD_awards_'||to_char(bi.file_sent_date,'yyyymmdd'),
bi.source_code,spr.program_name,bi.miles_points,bi.order_taken_date,null
from bi_etl.va_order_rewarded bi
join ftd_apps.source_program_ref spr
on bi.source_code = spr.source_code;
-- END OF 4.8.0.2 QA DEPLOY
-- ABOVE TWO STATEMENTS NOT RUN BECAUSE BI_ETL.VA_ORDER_REWARDED NOT PRESENT IN BORGT

alter table ftd_apps.source_master modify (apply_surcharge_code default 'DEFAULT',
                                           surcharge_amount default 0);

update ftd_apps.source_master set apply_surcharge_code = 'DEFAULT', surcharge_amount = 0;

update ftd_apps.source_master set display_surcharge_flag = 'D';

alter table ftd_apps.fuel_surcharge modify (fuel_surcharge_amt default 0);

-- end of 4.8.0.3

update rpt.report
    set status = 'Inactive'
    where report_id in (100603, 100207, 100602, 100223, 100222, 100305, 100301, 100303, 100306);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION,
CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'SERVICE_CONFIG' , 'ORDER_FEES_SAVED_MESSAGE',
'This is the message that tells the CSR how much money has been saved on an order by being a Free Shipping member.',
null, null, 'Defect_7966', sysdate, 'Defect_7966', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'ORDER_FEES_SAVED_MESSAGE'),
NULL, NULL, '~program_display_name~ member saved $~total_order_savings~ on this order.', 'Defect_7966', sysdate,
'Defect_7966', sysdate);

-- Added on 03/18/2011 as per Rose Lazuk's request
insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION,
CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'SERVICE_CONFIG', 'CART_FEES_SAVED_MESSAGE',
'This is the message that tells the CSR how much money has been saved on the cart by being a Free Shipping member.',
null, null, 'Defect_7966', sysdate, 'Defect_7966', sysdate); 

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'CART_FEES_SAVED_MESSAGE'),
NULL, NULL, '~program_display_name~ member saved $~total_cart_savings~ on this cart.', 'Defect_7966', sysdate,
'Defect_7966', sysdate);

-- Further QA stuff
update joe.element_config set recalc_flag = 'Y', product_validate_flag = 'Y'
where app_code = 'JOE' and element_id = 'customerEmailAddress';

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_master_sq.nextval, 'SERVICE_CONFIG', 'JOE_MSG', 
    'The messages to be displayed in JOE for Services scenarios.',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'SCRIPT_ID'),
    null, 'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

alter table ftd_apps.content_master add constraint content_master_u1 unique (content_context, content_name) using index
tablespace ftd_apps_indx;

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG'
        AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_APPLIED_MSG', null,
    'Customer is a ~fsProgramName~ Member and is eligible to receive free shipping or no service fees on this order. The order has been recalculated to reflect applicable savings.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG'
        AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_NOT_APPLIED_SC_MSG', null,
    'Customer is a ~fsProgramName~ Member, however the selected source code does not allow the use of ~fsProgramName~ benefits.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (
    ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_NOT_APPLIED_SC_UPDATE_MSG', null,
    'The updated source code does not allow the use of ~fsProgramName~ benefits. The order has been recalculated to reverse ant previously applied savings.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_NOT_APPLIED_EMAIL_NONE_MSG', null,
    'The customer''s email address must be provided to apply ~fsProgramName~ benefits. The order has been recalculated to reverse any previously applied savings.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_NOT_APPLIED_EMAIL_UPDATE_MSG', null,
    'The updated email address is not associated with an active ~fsProgramName~ membership. The order has been recalculated to reverse any previously applied savings.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SHADE_FS_NOT_APPLIED_PRODUCT_MSG', null,
    'Customer is a ~fsProgramName~ Member, however there are no products in the cart that are eligible for ~fsProgramName~ benefits.',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_SUBMIT_FS_APPLIED_MSG', null,
    'As a ~fsProgramName~ Member, you saved $~freeShippingSavingsToken~ on your order today in free shipping or no service fees',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_OC_FS_APPLIED_ORDER_SAVINGS_LABEL', null,
    'Total ~fsProgramName~ Savings',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);     

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE,
    CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values ( ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'SERVICE_CONFIG' AND CONTENT_NAME = 'JOE_MSG'),
    'JOE_OC_FS_APPLIED_LIFETIME_SAVINGS_LABEL', null, 'Total ~fsProgramName~ Lifetime Savings',
    'Defect_7965_Release_4_8_0', sysdate, 'Defect_7965_Release_4_8_0', sysdate);

-- Delete JP Penney Stock Messages (UC 19267)
delete from ftd_apps.stock_messages where stock_message_id='JCPB';
delete from ftd_apps.stock_messages where stock_message_id='JCPI';

-- Add Free Shipping Membership Cancelled Message (UC 19267)
-- Due to the size of the content field you will probably have to run this statement in a separate script.
declare
   v_content varchar2(4000) :=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">Dear <xsl:value-of select="header/buyer_first_name"/>:\n\nThank you for your recent purchase from FTD.COM. Your enrollment into <xsl:value-of select="accountprogrammaster_list/accountprogrammaster[name=''FREESHIP'']/displayname"/> has been cancelled and your membership is no longer active.\n\nA full refund has been posted to your <xsl:value-of select="header/payment_method_text"/> account. We apologize for any inconvenience this may cause.\n\nIf you would like to speak with a Customer Service Representative, please email us by clicking on this link <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>, or dial \n<xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>.  We are here to assist you 24 hours a day, 7 days a week.\n\nThank you for choosing <xsl:value-of select="header/company_name"/>.\n\nSincerely,\n<xsl:value-of select="email_data/data[name=''csr_firstname'']/value"/>\n\n\nContact us:\nE-mail: <xsl:value-of select="company_data_list/company_data[data_name=''Email'']/data"/>\nPhone:  <xsl:value-of select="company_data_list/company_data[data_name=''Phone'']/data"/>\nShop:   <xsl:value-of select="company_data_list/company_data[data_name=''Website'']/data"/>\n</xsl:template></xsl:stylesheet>';
begin
insert into ftd_apps.stock_messages (stock_message_id, description, subject, content, sort_order, section_id, company_id, origin_id, partner_name) values ('PROG_CANFS', 
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">MEMBERSHIP CANCELLED</xsl:template></xsl:stylesheet>',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="text"/><xsl:template match="/order">YOUR <xsl:value-of select="accountprogrammaster_list/accountprogrammaster[name=''FREESHIP'']/displayname"/> ACCOUNT - IMPORTANT NOTICE</xsl:template></xsl:stylesheet>',
v_content, 7000, 1, 'ALL', null, null);
end;
/
-- END OF RLSE_4_8_0_6
