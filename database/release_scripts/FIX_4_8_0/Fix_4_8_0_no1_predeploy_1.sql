set define off

grant references on aas.resources to ftd_apps;

create table FTD_APPS.DCSN_FX_DECISION_TYPES (
DECISION_TYPE_CODE VARCHAR2(50) not null,
NAME               VARCHAR2(100) not null,
DESCRIPTION        VARCHAR2(100) not null,
ADMIN_RESOURCE_ID  VARCHAR2(255) not null,
ADMIN_CONTEXT_ID   VARCHAR2(10) NOT NULL,
WIDGET_RESOURCE_ID VARCHAR2(255) not null,
WIDGET_CONTEXT_ID  VARCHAR2(10) NOT NULL,
CREATED_ON         DATE not null,
CREATED_BY         VARCHAR2(100) not null,
UPDATED_ON         DATE not null,
UPDATED_BY         VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_DECISION_TYPES add constraint DCSN_FX_DECISION_TYPES_PK primary key (DECISION_TYPE_CODE)
using index tablespace ftd_apps_indx;

alter table FTD_APPS.DCSN_FX_DECISION_TYPES add constraint DCSN_FX_DECISION_TYPES_ADMN_FK foreign key 
(admin_resource_id,admin_context_id) references aas.resources(resource_id,context_id);

alter table FTD_APPS.DCSN_FX_DECISION_TYPES add constraint DCSN_FX_DECISION_TYPES_WDGT_FK foreign key
(widget_resource_id,widget_context_id) references aas.resources(resource_id,context_id);

comment on column ftd_apps.dcsn_fx_decision_types.DECISION_TYPE_CODE is 'primary key';
comment on column ftd_apps.dcsn_fx_decision_types.NAME is 'displayed name of the type';
comment on column ftd_apps.dcsn_fx_decision_types.ADMIN_RESOURCE_ID is
'The Resource ID for access to perform management on this decision type. FK to AAS.RESOURCES';
comment on column ftd_apps.dcsn_fx_decision_types.ADMIN_CONTEXT_ID is
'Needed to complete foreign key reference to aas.resources';
comment on column ftd_apps.dcsn_fx_decision_types.WIDGET_RESOURCE_ID is
'The Resource ID for access to the Widget for this decision type. FK to AAS.RESOURCES';
comment on column ftd_apps.dcsn_fx_decision_types.WIDGET_CONTEXT_ID is
'Needed to complete foreign key reference to aas.resources';

create table FTD_APPS.DCSN_FX_RULES (
RULE_CODE          VARCHAR2(50) not null,
DECISION_TYPE_CODE VARCHAR2(50) not null,
NAME               VARCHAR2(200) not null,
NEXT_LEVEL_FLAG    VARCHAR2(1) default 'N' not null,
IS_ACTIVE_FLAG	   VARCHAR2(1) default 'N' not null,
DISPLAY_ORDER      INTEGER not null,
CREATED_ON         DATE	not null,
CREATED_BY         VARCHAR2(100) not null,
UPDATED_ON         DATE not null,
UPDATED_BY         VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_RULES add constraint DCSN_FX_RULES_PK primary key (RULE_CODE)
using index tablespace ftd_apps_indx;

alter table FTD_APPS.DCSN_FX_RULES add constraint DCSN_FX_RULES_DECSN_FK foreign key (decision_type_code)
references ftd_apps.dcsn_fx_decision_types;

alter table FTD_APPS.DCSN_FX_RULES add constraint DCSN_FX_RULES_NEXT_CK check
(next_level_flag in ('Y','N'));

alter table FTD_APPS.DCSN_FX_RULES add constraint DCSN_FX_RULES_ACTIVE_CK check
(is_active_flag in ('Y','N'));

comment on column ftd_apps.dcsn_fx_rules.RULE_CODE is 'primary key';
comment on column ftd_apps.dcsn_fx_rules.DECISION_TYPE_CODE is
'String Code of the Decision Type that this rule is for. FK to DCSN_FX_DECISION_TYPES::DECISION_TYPE_CODE';
comment on column ftd_apps.dcsn_fx_rules.NAME is
'Displayed Text for the Rule. E.g. "Show only if product Florist Delivered"';
comment on column ftd_apps.dcsn_fx_rules.NEXT_LEVEL_FLAG is
'Flag indicating if the rule applies to the current level, or the next level. (I.E. Business Rules apply to the Level. "Do not show if Refund" is also Rule, but it applies to the ''next'' level of the heirarchy). If Y, this is for the next level';
comment on column ftd_apps.dcsn_fx_rules.IS_ACTIVE_FLAG is 
'Y if the rule is active/displayed. Or ''N'' if it is not. Default to ''Y''. Use ''N'' if  a rule is no longer to be utilized';
comment on column ftd_apps.dcsn_fx_rules.DISPLAY_ORDER is 'The order the rule is displayed on the page';

create table FTD_APPS.DCSN_FX_STATUS_CODE_VAL (
STATUS_CODE VARCHAR2(10) not null,
DESCRIPTION VARCHAR2(100) not null,
CREATED_ON  DATE not null,
CREATED_BY  VARCHAR2(100) not null,
UPDATED_ON  DATE not null,
UPDATED_BY  VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_STATUS_CODE_VAL add constraint dcsn_fx_status_code_val_pk primary key (STATUS_CODE)
using index tablespace ftd_apps_indx;

insert into ftd_apps.dcsn_fx_status_code_val (status_code, description, created_on, created_by, updated_on, updated_by)
values ('DRAFT','The configuration is in being edited. There are changes that are not available to the widget.',
sysdate, 'Defect_5815_Release_4_8_0', sysdate, 'Defect_5815_Release_4_8_0');

insert into ftd_apps.dcsn_fx_status_code_val (status_code, description, created_on, created_by, updated_on, updated_by)
values ('PUBLISH','The configuration is active and in use. New processes should utilize this configuration.',
sysdate, 'Defect_5815_Release_4_8_0', sysdate, 'Defect_5815_Release_4_8_0');

insert into ftd_apps.dcsn_fx_status_code_val (status_code, description, created_on, created_by, updated_on, updated_by)
values ('ARCHIVE','The configuration is no longer active. Existing processes can continue to utilize this configuration',
sysdate, 'Defect_5815_Release_4_8_0', sysdate, 'Defect_5815_Release_4_8_0');

create table FTD_APPS.DCSN_FX_DECISION_CONFIG (
DECISION_CONFIG_ID integer not null,
DECISION_TYPE_CODE VARCHAR2(50)	not null,
STATUS_CODE        VARCHAR2(10)	not null,
CREATED_ON         DATE not null,
CREATED_BY         VARCHAR2(100) not null,
UPDATED_ON         DATE not null,
UPDATED_BY         VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_DECISION_CONFIG add constraint DCSN_FX_DECISION_CONFIG_PK primary key (DECISION_CONFIG_ID)
using index tablespace ftd_apps_indx;

alter table FTD_APPS.DCSN_FX_DECISION_CONFIG add constraint DCSN_FX_DECISION_CONFIG_TYP_FK foreign key (DECISION_TYPE_CODE)
references ftd_apps.dcsn_fx_decision_types;

alter table FTD_APPS.DCSN_FX_DECISION_CONFIG add constraint DCSN_FX_DECISION_CONFIG_STA_FK foreign key (STATUS_CODE)
references ftd_apps.dcsn_fx_status_code_val;

comment on column ftd_apps.dcsn_fx_decision_config.DECISION_CONFIG_ID is
'primary key. A new version is created when an existing version is edited';
comment on column ftd_apps.dcsn_fx_decision_config.DECISION_TYPE_CODE is
'String Code of the Decision Type that this configuration is for. FK to DCSN_FX_DECISION_TYPES::DECISION_TYPE_CODE';
comment on column ftd_apps.dcsn_fx_decision_config.STATUS_CODE is
'Status of the Configuration. Only 1 Record can be in DRAFT or PUBLISH Status. Older versions are in ARCHIVE status. FK to DCSN_FX_STATUS_TYPE table.';

create sequence ftd_apps.dcsn_fx_decision_config_id_sq start with 1;
create sequence ftd_apps.dcsn_fx_value_id_sq start with 1;

create table FTD_APPS.DCSN_FX_CONFIG_VALUE (
VALUE_ID            integer not null,
DECISION_CONFIG_ID  integer not null,
NAME                VARCHAR2(30) not null,
IS_ACTIVE_FLAG      VARCHAR2(1) default 'Y' not null,
PARENT_VALUE_ID     integer,
SCRIPT_TEXT         VARCHAR2(75) not null,
ALLOW_COMMENTS_FLAG VARCHAR2(1)	default 'Y' not null,
GROUP_ORDER         integer not null,
CREATED_ON          DATE not null,
CREATED_BY          VARCHAR2(100) not null,
UPDATED_ON          DATE not null,
UPDATED_BY          VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE add constraint DCSN_FX_CONFIG_VALUE_PK primary key (value_id, decision_config_id)
using index tablespace ftd_apps_indx;

create index ftd_apps.dcsn_fx_config_value_n1 on ftd_apps.dcsn_fx_config_value (decision_config_id) tablespace ftd_apps_indx;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE add constraint DCSN_FX_CONFIG_VALUE_DECISN_FK foreign key (decision_config_id)
references ftd_apps.dcsn_fx_decision_config;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE add constraint DCSN_FX_CONFIG_VALUE_PARENT_FK foreign key (parent_value_id, decision_config_id)
references ftd_apps.dcsn_fx_config_value (value_id, decision_config_id);

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE add constraint DCSN_FX_CONFIG_VALUE_ACTIVE_CK check (is_active_flag in ('Y','N'));

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE add constraint DCSN_FX_CONFIG_VALUE_COMMEN_CK check (allow_comments_flag in ('Y','N'));

comment on column ftd_apps.dcsn_fx_config_value.VALUE_ID is
'Primary identifier of a dcsn_fx_config.  With the decision_config_id, it becomes the primary key';
comment on column ftd_apps.dcsn_fx_config_value.DECISION_CONFIG_ID is
'The other part of the primary key.  FK to FTD_APPS.DCSN_FX_DECISION_CONFIG';
comment on column ftd_apps.dcsn_fx_config_value.IS_ACTIVE_FLAG is 'Status of the Value. Y = ACTIVE, N = INACTIVE';
comment on column ftd_apps.dcsn_fx_config_value.PARENT_VALUE_ID	is 'Id of the Parent Record. If null, this is a root item';
comment on column ftd_apps.dcsn_fx_config_value.SCRIPT_TEXT is 'Text used to prompt the user for the next level values';
comment on column ftd_apps.dcsn_fx_config_value.ALLOW_COMMENTS_FLAG is 'Indicates if Comments are Allowed. Values = [Y,N]';
comment on column ftd_apps.dcsn_fx_config_value.GROUP_ORDER is 'Sort order of the node under the parent';

create table FTD_APPS.DCSN_FX_CONFIG_VALUE_RULES (
VALUE_ID           integer not null,
DECISION_CONFIG_ID integer not null,
RULE_CODE          VARCHAR2(50) not null,
CREATED_ON         DATE not null,
CREATED_BY         VARCHAR2(100) not null,
UPDATED_ON         DATE not null,
UPDATED_BY         VARCHAR2(100) not null) tablespace ftd_apps_data;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE_RULES add constraint DCSN_FX_CONFIG_VALUE_RULES_PK primary key
(VALUE_ID,DECISION_CONFIG_ID,RULE_CODE) using index tablespace ftd_apps_indx;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE_RULES add constraint DCSN_FX_CONFIG_VALUE_RULES_FK1 foreign key
(VALUE_ID, DECISION_CONFIG_ID) references ftd_apps.dcsn_fx_config_value;

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE_RULES add constraint DCSN_FX_CONFIG_VALUE_RULES_FK2 foreign key (rule_code)
references ftd_apps.dcsn_fx_rules;

comment on column ftd_apps.dcsn_fx_config_value_rules.VALUE_ID is
'Id of the Value configuration. FK to DCSN_FX_CONFIG_VALUE Primary Key';
comment on column ftd_apps.dcsn_fx_config_value_rules.DECISION_CONFIG_ID is
'Id of the Value configuration. FK to DCSN_FX_CONFIG_VALUE Primary Key';
comment on column ftd_apps.dcsn_fx_config_value_rules.RULE_CODE is
'Id of the Value configuration. Code of the Selected Rule. FK to FTD_APPS.DCSN_FX_RULES';

alter table FTD_APPS.DCSN_FX_CONFIG_VALUE modify (script_text null);

create table clean.dcsn_fx_results (
result_id           integer not null,
DECISION_CONFIG_ID  integer not null,
STATUS              VARCHAR2(30) not null,
CURRENT_LEVEL       integer,
CREATED_BY          VARCHAR2(100) not null,
CREATED_ON          DATE  not null,
UPDATED_BY          VARCHAR2(100) not null,
UPDATED_ON          DATE  not null) tablespace clean_data;

alter table clean.dcsn_fx_results add constraint dcsn_fx_results_pk primary key (result_id) using index tablespace clean_indx;

grant references on ftd_apps.dcsn_fx_decision_config to clean;
grant select     on ftd_apps.dcsn_fx_decision_config to clean;

alter table clean.dcsn_fx_results add constraint dcsn_fx_results_fk1 foreign key (decision_config_id)
references ftd_apps.dcsn_fx_decision_config;

alter table clean.dcsn_fx_results add constraint dcsn_fx_results_ck1 check 
(status in ('COMPLETE','INPROGRESS','DECLINE','VIEW'));

comment on column clean.dcsn_fx_results.decision_config_id is 'references ftd_apps.dcsn_fx_decision_config';
comment on column clean.dcsn_fx_results.current_level is 'trace current level of disposition displayed in UI';

create sequence clean.dcsn_fx_result_id_sq start with 1 increment by 1 cache 200;
create sequence clean.dcsn_fx_result_details_id_sq start with 1 increment by 1 cache 200;

create table CLEAN.dcsn_fx_result_ENTITIES (
result_entity_ID  integer not null,
result_id         integer not null,
CALL_ENTITY_NAME  VARCHAR2(100) not null,
CALL_ENTITY_VALUE NUMBER not null,
CREATED_BY        VARCHAR2(100) not null,
CREATED_ON        DATE not null,
UPDATED_BY        VARCHAR2(100) not null,
UPDATED_ON        DATE not null) tablespace clean_data;

create index clean.dcsn_fx_result_entities_n1 on clean.dcsn_fx_result_entities (result_id)
tablespace clean_indx;

alter table clean.dcsn_fx_result_entities add constraint dcsn_fx_result_ENTITIES_pk primary key (
result_entity_id) using index tablespace clean_indx;

alter table clean.dcsn_fx_result_entities add constraint dcsn_fx_result_ENTITIES_fk1 foreign key 
(result_id) references clean.dcsn_fx_results;

alter table clean.dcsn_fx_result_entities add constraint dcsn_fx_result_ENTITIES_ck1 check (
call_entity_name in ('CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID','CLEAN.CUSTOMER.CUSTOMER_ID'));

create sequence clean.dcsn_fx_result_entities_id_sq start with 1 increment by 1 cache 200;

comment on column clean.dcsn_fx_result_entities.call_entity_value is 'order_detail_id or customer_id';

Create table CLEAN.dcsn_fx_result_DETAILS (
result_DETAIL_ID integer not null,
result_id        integer not null,
VALUE_ID         integer not null,
PARENT_VALUE_ID  integer,
NAME             VARCHAR2(30) not null,
COMMENT_TEXT     VARCHAR2(150),
SCRIPT_TEXT      VARCHAR2(75),
CREATED_BY       VARCHAR2(100) not null,
CREATED_ON       DATE not null,
UPDATED_BY       VARCHAR2(100) not null,
UPDATED_ON       DATE not null) tablespace clean_data;

create sequence clean.result_detail_id_sq start with 1 increment by 1 cache 200;

alter table clean.dcsn_fx_result_details add constraint dcsn_fx_result_details_pk primary key 
(result_detail_id) using index tablespace clean_indx;

alter table clean.dcsn_fx_result_details add constraint dcsn_fx_result_details_fk1 foreign key 
(result_id) references clean.dcsn_fx_results;

grant references on ftd_apps.dcsn_fx_config_value to clean;

alter table clean.dcsn_fx_result_details add constraint dcsn_fx_result_details_u1 unique 
(result_id, value_id) using index tablespace clean_indx;

comment on column clean.dcsn_fx_result_details.value_id is 'references ftd_apps.dcsn_fx_config_value.value_id';
comment on column clean.dcsn_fx_result_details.parent_value_id is 
'references ftd_apps.dcsn_fx_config_value.value_id';
comment on column clean.dcsn_fx_result_details.name is 'name of the selected node';
comment on column clean.dcsn_fx_result_details.comment_text is 'comment for the current level value';
comment on column clean.dcsn_fx_result_details.script_text is 'script for next level disposition';

-- begin inclusions from FIX_4_8_0_IT1
create sequence account.account_master_id_sq start with 1 increment by 1 cache 200;

create sequence account.account_email_id_sq start with 1 increment by 1 cache 200;

create sequence account.program_master_id_sq start with 1 increment by 1 cache 200;

create sequence account.account_program_id_sq start with 1 increment by 1 cache 200;

create table account.account_master
(account_master_id number,
 company_id varchar2(12) not null,
 created_on date not null,
 created_by varchar2(100) not null,
 updated_on date not null,
 updated_by varchar2(100) not null,
 constraint account_master_pk primary key (account_master_id)
 using index
 tablespace account_indx)
tablespace account_data; 

create table account.account_email
(account_email_id number,
 account_master_id number not null,
 email_address varchar2(200) not null,
 active_flag char(1) not null,
 created_on date not null,
 created_by varchar2(100) not null,
 updated_on date not null,
 updated_by varchar2(100) not null,
 constraint account_email_pk primary key (account_email_id)
 using index
 tablespace account_indx,
 constraint account_email_fk1 foreign key (account_master_id)
 references account.account_master)
tablespace account_data;

create index account.account_email_fk1 on account.account_email (account_master_id)
tablespace account_indx;

create table account.program_master
(program_master_id number,
 program_name varchar2(50),
 program_description varchar2(255) not null,
 active_flag char(1) not null,
 created_on date not null,
 created_by varchar2(100) not null,
 updated_on date not null,
 updated_by varchar2(100) not null,
 constraint program_master_pk primary key (program_master_id)
 using index
 tablespace account_indx,
 constraint programt_master_uk1 unique (program_name)
 using index
 tablespace account_indx)
tablespace account_data;

create table account.account_program
(account_program_id number,
 account_master_id number not null,
 program_master_id number not null,
 account_program_status char(1),
 auto_renew_flag char(1),
 expiration_date date,
 start_date date,
 created_on date not null,
 created_by varchar2(100) not null,
 updated_on date not null,
 updated_by varchar2(100) not null,
 constraint account_program_pk primary key (account_program_id)
 using index
 tablespace account_indx,
 constraint account_program_fk1 foreign key (account_master_id)
 references account.account_master,
 constraint account_program_fk2 foreign key (account_program_id)
 references account.account_program)
tablespace account_data;

create index account_program_fk1 on account.account_program (account_master_id)
tablespace account_indx;

insert into account.program_master( program_master_id, program_name, program_description,
active_flag, created_on, created_by, updated_on, updated_by) values(
account.program_master_id_sq.nextval, 'FREESHIP', 'Free Shipping Membership', 'Y',
sysdate, 'Defect_7964_Release_4_8_0', sysdate, 'Defect_7964_Release_4_8_0');

insert into frp.global_parms values ( 'PDB_CONFIG', 'SERVICE_DURATION_VALUES',
'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24',
'Defect_7964_Release_4_8_0', SYSDATE, 'Defect_7964_Release_4_8_0', SYSDATE,
'Service duration values for free shipping.');
-- end inclusions from FIX_4_8_0_IT1

Insert into FTD_APPS.DCSN_FX_DECISION_CONFIG 
  (DECISION_CONFIG_ID,DECISION_TYPE_CODE,STATUS_CODE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) 
values 
  (FTD_APPS.DCSN_FX_DECISION_CONFIG_ID_SQ.nextval, 'calldisposition','DRAFT',sysdate,'Defect_5815',sysdate,'Defect_5815');

Insert into FTD_APPS.DCSN_FX_CONFIG_VALUE (VALUE_ID,DECISION_CONFIG_ID,NAME,IS_ACTIVE_FLAG,PARENT_VALUE_ID,SCRIPT_TEXT,
ALLOW_COMMENTS_FLAG,GROUP_ORDER,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) values 
(FTD_APPS.DCSN_FX_VALUE_ID_SQ.nextval, (select max(DECISION_CONFIG_ID) from FTD_APPS.DCSN_FX_DECISION_CONFIG where 
DECISION_TYPE_CODE = 'calldisposition' and status_code = 'DRAFT')  ,'Call Disposition','Y',null,' Who is Calling?','Y',1,
sysdate,'Defect_5815',sysdate,'Defect_5815');
 
alter table account.account_program add (external_order_number varchar2(3000));

create index account.account_program_n1 on account.account_program (external_order_number) tablespace account_indx;

comment on column account.account_program.external_order_number is
'External order number in which the Program was purchased';

alter table account.program_master add (program_url varchar2(4000), display_name varchar2(4000));

comment on column account.program_master.program_url is 'URL for information on the Program';
comment on column account.program_master.display_name is 'Program Name that is displayed to users';

update account.program_master set display_name = 'FTD Gold Membership', program_url='http://www.ftd.com/free-shipping/'
where program_name = 'FREESHIP';

insert into events.events (event_name,context_name,description,active)
values ('POST-FILE-VIRGIN-AMERICA','PARTNER-REWARD','Partner Rewards Posting File Virgin America','Y');

insert into events.events (event_name,context_name,description,active)
values ('RESPONSE-FILE-VIRGIN-AMERICA','PARTNER-REWARD','Partner Rewards Response File Virgin America','Y');

-- QA ONLY - RUN AS OPS$ORACLE
INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_OutboundFtpUser',global.encryption.encrypt_it('changeme','APOLLO_TEST_2010'),
'Outbound ftp username for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_OutboundFtpPswd',global.encryption.encrypt_it('changeme','APOLLO_TEST_2010'),
'Outbound ftp password for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_InboundFtpUser',global.encryption.encrypt_it('changeme','APOLLO_TEST_2010'),
'Inbound ftp username for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_TEST_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_InboundFtpPswd',global.encryption.encrypt_it('changeme','APOLLO_TEST_2010'),
'Inbound ftp password for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_TEST_2010');

insert into FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by, description) values
('PARTNER_REWARD_CONFIG', 'VIRGINAMERICA_INBOUND_SERVER', 'sodium.ftdi.com', sysdate, 'Defect_8223_Release_4_8_0', sysdate,
'Defect_8223_Release_4_8_0', 'Virgin America Airlines Inbound FTP server URL');

insert into FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by, description) values
('PARTNER_REWARD_CONFIG', 'VIRGINAMERICA_OUTBOUND_SERVER', 'sodium.ftdi.com', sysdate, 'Defect_8223_Release_4_8_0', sysdate,
'Defect_8223_Release_4_8_0', 'Virgin America Airlines Outbound FTP server URL');

-- PRODUCTION ONLY - RUN AS OPS$ORACLE

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_OutboundFtpUser',global.encryption.encrypt_it('changeme','APOLLO_PROD_2010'),
'Outbound ftp username for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_PROD_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_OutboundFtpPswd',global.encryption.encrypt_it('changeme','APOLLO_PROD_2010'),
'Outbound ftp password for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_PROD_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_InboundFtpUser',global.encryption.encrypt_it('changeme','APOLLO_PROD_2010'),
'Inbound ftp username for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_PROD_2010');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('partner_reward','VIRGINAMERICA_InboundFtpPswd',global.encryption.encrypt_it('changeme','APOLLO_PROD_2010'),
'Inbound ftp password for Virgin America Airlines',sysdate,'Defect_8223_Release_4_8_0', sysdate,'Defect_8223_Release_4_8_0','APOLLO_PROD_2010');

insert into FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by, description) values
('PARTNER_REWARD_CONFIG', 'VIRGINAMERICA_INBOUND_SERVER', 'condor.ftdi.com', sysdate, 'Defect_8223_Release_4_8_0', sysdate,
'Defect_8223_Release_4_8_0', 'Virgin America Airlines Inbound FTP server URL');

insert into FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by, description) values
('PARTNER_REWARD_CONFIG', 'VIRGINAMERICA_OUTBOUND_SERVER', 'condor.ftdi.com', sysdate, 'Defect_8223_Release_4_8_0', sysdate,
'Defect_8223_Release_4_8_0', 'Virgin America Airlines Outbound FTP server URL');

-- END OF ENVIRONMENT-SPECIFIC STUFF
-- END OF 4.8.0.1
-- END OF 4.8.0.2

--Insert a new role
insert into aas.role(ROLE_ID,ROLE_NAME,CONTEXT_ID,DESCRIPTION,CREATED_ON,UPDATED_ON,UPDATED_BY) 
values (aas.role_id.nextval,'CallDispositionRole','Order Proc','Call Disposition Role',sysdate,sysdate,
'Defect_5815_Release_4_8_0');

--Insert a new resource
insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values 
('CallDisposition','Order Proc','Determines if user sees Call Disposition Widget in customer_order_management', sysdate, sysdate,
'Defect_5815_Release_4_8_0');

--Insert a new ACL
insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL) 
values ('CallDisposition CSR', sysdate, sysdate, 'Defect_5815_Release_4_8_0', null);

--Insert a new join on role and ACL
insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values 
((select role_id from aas.role where role_name='CallDispositionRole'), 'CallDisposition CSR', sysdate, sysdate,
'Defect_5815_Release_4_8_0');

--Insert a new join on acl, resource, context and permission
insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('CallDisposition CSR','CallDisposition','Order Proc','View',sysdate,sysdate,'Defect_5815_Release_4_8_0');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('CallDisposition CSR','CallDisposition','Order Proc','Add',sysdate,sysdate,'Defect_5815_Release_4_8_0');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('CallDisposition CSR','CallDisposition','Order Proc','Delete',sysdate,sysdate,'Defect_5815_Release_4_8_0');

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('CallDisposition CSR','CallDisposition','Order Proc','Update',sysdate,sysdate,'Defect_5815_Release_4_8_0');

insert into FRP.GLOBAL_PARMS ( context ,name ,value ,created_on ,created_by ,updated_on ,updated_by ,description) 
values ( 'CUSTOMER_ORDER_MANAGEMENT' ,'DECISION_WIDGET_DISPLAY' ,'Y'
,sysdate ,'Defect_5815_Release_4_8_0' ,sysdate ,'Defect_5815_Release_4_8_0'
,'This parm used to turn ON or OFF the decision widget functionality.');

insert into FRP.GLOBAL_PARMS ( context ,name ,value ,created_on ,created_by ,updated_on ,updated_by ,description) 
values ( 'CUSTOMER_ORDER_MANAGEMENT' ,'CDISP_OPTIONAL_REMINDER' ,'Y'
,sysdate ,'Defect_5815_Release_4_8_0' ,sysdate ,'Defect_5815_Release_4_8_0'
,'Displays Optional Disposition Reminder message at COM exit point if Y and no orders have had disposition submitted (and all are optional)');

-- END OF 4.8.0.3
grant select on FTD_APPS.DCSN_FX_DECISION_TYPES to bi_etl;
grant select on FTD_APPS.DCSN_FX_RULES to bi_etl;
grant select on FTD_APPS.DCSN_FX_STATUS_CODE_VAL to bi_etl;
grant select on FTD_APPS.DCSN_FX_DECISION_CONFIG to bi_etl;
grant select on FTD_APPS.DCSN_FX_CONFIG_VALUE to bi_etl;
grant select on FTD_APPS.DCSN_FX_CONFIG_VALUE_RULES to bi_etl;
grant select on clean.dcsn_fx_results to bi_etl;
grant select on CLEAN.dcsn_fx_result_ENTITIES to bi_etl;
grant select on CLEAN.dcsn_fx_result_DETAILS to bi_etl;
grant select on account.account_master to bi_etl;
grant select on account.account_email to bi_etl;
grant select on account.program_master to bi_etl;
grant select on account.account_program to bi_etl;
-- END OF 4.8.0.5
alter table CLEAN.ORDER_DETAIL_UPDATE add ( SERVICE_FEE_SAVED number,  SHIPPING_FEE_SAVED number);
-- END OF 4.8.0.6
