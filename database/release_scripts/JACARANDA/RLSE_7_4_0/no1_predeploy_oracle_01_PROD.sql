------------------------------------------------------------------------------------ 
-- begin requested by Swami Patsa,                              12/ 1/2014  -------- 
-- defect #1183 (mark) request #123757              --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
Values ('ACCOUNTING_CONFIG', 'bamsFtpServer', 'titanium.ftdi.com', 'defect_1183', Sysdate, 'defect_1183', Sysdate, 'bams ftp server');

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
values ('ACCOUNTING_CONFIG', 'bamsFtpLocation', '/u02/apollo/accounting/pts/', 'defect_1183', sysdate, 'defect_1183', sysdate, 'bams ftp location');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','bamsFtpPassword',global.encryption.encrypt_it('fixme','APOLLO_PROD_2014'),'BAMS ftp password',Sysdate,'defect_1183',Sysdate,'defect_1183','APOLLO_PROD_2014');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','bamsFtpUsername',global.encryption.encrypt_it('fixme','APOLLO_PROD_2014'),'BAMS ftp username',sysdate,'defect_1183',sysdate,'defect_1183','APOLLO_PROD_2014');

------------------------------------------------------------------------------------ 
-- end   requested by Swami Patsa,                              12/ 1/2014  -------- 
-- defect #1183 (mark) request #123757              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Abishek Chandna,                          12/18/2014  -------- 
-- defect #1204 (mark) request #                    --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT,NAME,VALUE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,KEY_NAME)
VALUES
('ACCOUNTING_CONFIG','BamsSSHPrivateKeyPassphrase',global.encryption.encrypt_it('fixme','APOLLO_PROD_2014')
,'BAMS SSH Private Key PassPhrase',sysdate    ,'Defect_1204',sysdate    ,'Defect_1204','APOLLO_PROD_2014');

------------------------------------------------------------------------------------ 
-- end   requested by Abishek Chandna,                          12/18/2014  -------- 
-- defect #1204 (mark) request #                    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk                                12/19/2014  -------- 
-- defect #1183 (mark) request #127038              --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
Values ('ACCOUNTING_CONFIG', 'ptsInternalFtpServer', 'titanium.ftdi.com', 'defect_1183', Sysdate, 'defect_1183', Sysdate, 
'internal ftp server pts settlement files will be staged on');

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
values ('ACCOUNTING_CONFIG', 'ptsInternalFtpLocation', '/u02/apollo/accounting/pts/', 'defect_1183', sysdate, 'defect_1183', sysdate, 
'internal pts ftp location');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','ptsInternalFtpPassword',global.encryption.encrypt_it('fixme','APOLLO_PROD_2014'),'internal pts ftp password',
Sysdate,'defect_1183',Sysdate,'defect_1183','APOLLO_PROD_2014');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','ptsInternalFtpUsername',global.encryption.encrypt_it('FTPCOM02','APOLLO_PROD_2014'),'internal pts ftp username',
sysdate,'defect_1183',sysdate,'defect_1183','APOLLO_PROD_2014');

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk                                12/19/2014  -------- 
-- defect #1183 (mark) request #127038              --------------------------------
------------------------------------------------------------------------------------


