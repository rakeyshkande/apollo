------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

use venus;
alter table venus add (ORIG_SHIP_DATE datetime default null, SECONDARY_SHIP_DATE datetime default null);

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

