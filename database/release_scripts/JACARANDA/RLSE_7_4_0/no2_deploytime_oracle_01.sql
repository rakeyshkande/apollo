------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               11/25/2014  -------- 
-- defect #236   (mark)                             --------------------------------
------------------------------------------------------------------------------------

-- BE SURE THE QUEUE IS EMPTY BEFORE RUNNING THIS COMMAND
@ojms_objects/pas_command.sql

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               11/25/2014  --------
-- defect #236   (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

-- GG IMPLICATIONS
alter table venus.venus add (ORIG_SHIP_DATE date, SECONDARY_SHIP_DATE date);

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                              11/20/2014  --------
-- defect #1148  (mark)                             --------------------------------
------------------------------------------------------------------------------------

Alter Table clean.Bams_Settlement_Totals Add (File_Name Varchar2(100));

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                              11/20/2014  --------
-- defect #1148  (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep Penchala ,                              12/05/2014  --------
-- defect #164  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

alter table RPT.REPORT_SUBMISSION_LOG MODIFY (ORACLE_REPORT_URL varchar2(4000));

------------------------------------------------------------------------------------
-- end   requested by Gunadeep Penchala ,                              12/05/2014  --------
-- defect #164  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #978  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE FTD_APPS.ADDON_HISTORY  ADD (florist_addon_flag CHAR(1) default 'Y',vendor_addon_flag CHAR(1) default 'N');

Update ftd_apps.addon_history ah
set  FLORIST_ADDON_FLAG =  (select FLORIST_ADDON_FLAG from ftd_apps.addon a where a.addon_id = ah.addon_id),
VENDOR_ADDON_FLAG = (select VENDOR_ADDON_FLAG from ftd_apps.addon a where a.addon_id = ah.addon_id),
updated_on = sysdate,
updated_by = 'Defect_978';

------------------------------------------------------------------------------------
-- end   requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #978  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #1051  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE JOE.ORDER_PAYMENTS ADD (wallet_indicator   number (3));
------------------------------------------------------------------------------------
-- end   requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #1051  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/18/2014  -------- 
-- defect #1183  (mark) request #126861             --------------------------------
------------------------------------------------------------------------------------

alter table CLEAN.BAMS_SETTLEMENT_TOTALS add (ARCHIVED CHAR(1));

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/19/2014  -------- 
-- defect #1183  (mark) request #127038             --------------------------------
------------------------------------------------------------------------------------

delete from frp.global_parms where context = 'ACCOUNTING_CONFIG' and name = 'bamsFtpServer';
delete from frp.global_parms where context = 'ACCOUNTING_CONFIG' and name = 'bamsFtpLocation';
delete from frp.secure_config where context = 'accounting_reporting' and name = 'bamsFtpPassword';
delete from frp.secure_config where context = 'accounting_reporting' and name = 'bamsFtpUsername';

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/19/2014  -------- 
-- defect #1183  (mark) request #127038             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Pilli santosh,                               12/23/2014  -------- 
-- defect #1226  (pavan) request #127610             --------------------------------
------------------------------------------------------------------------------------

alter table ptn_pi.partner_order_adjustment add refund_id number;

------------------------------------------------------------------------------------
-- end   requested by Pilli santosh,                            12/23/2014  -------- 
-- defect #1226  (pavan) request #127610             -------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Raghuram salguti,                         12/26/2014  -------- 
-- defect #695  (pavan) request #128071             --------------------------------
------------------------------------------------------------------------------------

update JOE.ELEMENT_CONFIG set 
ERROR_TXT='You must enter a AAA Club Membership number or perform a AAA Club Search for a AAA Club Membership number'
where ELEMENT_ID like 'membershipId';

------------------------------------------------------------------------------------
-- end   requested by Raghuram salguti,                         12/26/2014  -------- 
-- defect #695  (pavan) request #128071             --------------------------------
------------------------------------------------------------------------------------


