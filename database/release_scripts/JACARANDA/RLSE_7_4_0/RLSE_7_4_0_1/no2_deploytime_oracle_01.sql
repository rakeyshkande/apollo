------------------------------------------------------------------------------------ 
-- begin requested by Tim Schmig,                               11/25/2014  -------- 
-- defect #236   (mark)                             --------------------------------
------------------------------------------------------------------------------------

-- BE SURE THE QUEUE IS EMPTY BEFORE RUNNING THIS COMMAND
@ojms_objects/pas_command.sql

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig,                               11/25/2014  --------
-- defect #236   (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

alter table venus.venus add (ORIG_SHIP_DATE date, SECONDARY_SHIP_DATE date);

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               11/19/2014  --------
-- defect #517   (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Swami Patsa,                              11/20/2014  --------
-- defect #1148  (mark)                             --------------------------------
------------------------------------------------------------------------------------

Alter Table clean.Bams_Settlement_Totals Add (File_Name Varchar2(100));

------------------------------------------------------------------------------------
-- end   requested by Swami Patsa,                              11/20/2014  --------
-- defect #1148  (mark)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep Penchala ,                              12/05/2014  --------
-- defect #164  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

alter table RPT.REPORT_SUBMISSION_LOG MODIFY (ORACLE_REPORT_URL varchar2(4000));

------------------------------------------------------------------------------------
-- end   requested by Gunadeep Penchala ,                              12/05/2014  --------
-- defect #164  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #978  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE FTD_APPS.ADDON_HISTORY  ADD (florist_addon_flag CHAR(1) default 'Y',vendor_addon_flag CHAR(1) default 'N');

Update ftd_apps.addon_history ah
set  FLORIST_ADDON_FLAG =  (select FLORIST_ADDON_FLAG from ftd_apps.addon a where a.addon_id = ah.addon_id),
VENDOR_ADDON_FLAG = (select VENDOR_ADDON_FLAG from ftd_apps.addon a where a.addon_id = ah.addon_id),
updated_on = sysdate,
updated_by = 'Defect_978';

------------------------------------------------------------------------------------
-- end   requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #978  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #1051  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

ALTER TABLE JOE.ORDER_PAYMENTS ADD (wallet_indicator   number (3));
------------------------------------------------------------------------------------
-- end   requested by Ritesh Singh ,                              12/05/2014  --------
-- defect #1051  (pavan)                             --------------------------------
------------------------------------------------------------------------------------

