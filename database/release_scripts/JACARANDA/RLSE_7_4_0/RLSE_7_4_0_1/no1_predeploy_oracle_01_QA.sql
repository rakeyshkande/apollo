------------------------------------------------------------------------------------ 
-- begin requested by Swami Patsa,                              12/ 1/2014  -------- 
-- defect #1183 (mark) request #123757              --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
Values ('ACCOUNTING_CONFIG', 'bamsFtpServer', 'jade1.ftdi.com', 'defect_1183', Sysdate, 'defect_1183', Sysdate, 'bams ftp server');

Insert Into Frp.Global_Parms (Context, Name, Value, Created_By, Created_On, Updated_By, Updated_On, Description)
values ('ACCOUNTING_CONFIG', 'bamsFtpLocation', '/u02/apollo/accounting/pts/', 'defect_1183', sysdate, 'defect_1183', sysdate, 'bams ftp location');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','bamsFtpPassword',global.encryption.encrypt_it('fixme','APOLLO_TEST_2010'),'BAMS ftp password',Sysdate,'defect_1183',Sysdate,'defect_1183','APOLLO_TEST_2010');

Insert Into Frp.Secure_Config (Context,Name,Value,Description,Created_On,Created_By,Updated_On,Updated_By,Key_Name) 
Values ('accounting_reporting','bamsFtpUsername',global.encryption.encrypt_it('oracle','APOLLO_TEST_2010'),'BAMS ftp username',sysdate,'defect_1183',sysdate,'defect_1183','APOLLO_TEST_2010');

------------------------------------------------------------------------------------ 
-- end   requested by Swami Patsa,                              12/ 1/2014  -------- 
-- defect #1183 (mark) request #123757              --------------------------------
------------------------------------------------------------------------------------

