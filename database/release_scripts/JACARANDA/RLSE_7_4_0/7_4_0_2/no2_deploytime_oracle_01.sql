------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/12/2014  -------- 
-- defect #1230  (mark) request #125992             --------------------------------
------------------------------------------------------------------------------------

grant select on FTD_APPS.HOLIDAY_COUNTRY to venus;
 
------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk,                               12/12/2014  -------- 
-- defect #1230  (mark) request #125992             --------------------------------
------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/18/2014  -------- 
-- defect #1183  (mark) request #126861             --------------------------------
------------------------------------------------------------------------------------

alter table CLEAN.BAMS_SETTLEMENT_TOTALS add (ARCHIVED CHAR(1));

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/19/2014  -------- 
-- defect #1183  (mark) request #127038             --------------------------------
------------------------------------------------------------------------------------

delete from frp.global_parms where context = 'ACCOUNTING_CONFIG' and name = 'bamsFtpServer';
delete from frp.global_parms where context = 'ACCOUNTING_CONFIG' and name = 'bamsFtpLocation';
delete from frp.secure_config where context = 'accounting_reporting' and name = 'bamsFtpPassword';
delete from frp.secure_config where context = 'accounting_reporting' and name = 'bamsFtpUsername';

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/19/2014  -------- 
-- defect #1183  (mark) request #127038             --------------------------------
------------------------------------------------------------------------------------

