------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1175 (mark) request #125757              --------------------------------
------------------------------------------------------------------------------------

insert into FTD_APPS.ORIGINS (ORIGIN_ID, DESCRIPTION, ORIGIN_TYPE, ACTIVE, PARTNER_REPORT_FLAG, 
                              SEND_FLORIST_PDB_PRICE, ORDER_CALC_TYPE) 
                      values ('TBLT', 'Tablet Internet Orders', 'internet', 'Y', 'N', 'N', 'GENERIC');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1175 (mark) request #125757              --------------------------------
------------------------------------------------------------------------------------

