------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1183 (mark) request #125758              --------------------------------
------------------------------------------------------------------------------------

insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS FTD SETTLEMENT FILE',
    'ACCOUNTING',
    'Process BAMS FTD Settlement File',
    'Sends Daily FTD BAMS Settlement File',
    '0 0 2-6 * * ?',
    'FTD',
    'N',
    'OJMS.BAMS_SETTLEMENT',
    'SimpleJmsJob',
    'Y');

    insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS FTDCA SETTLEMENT FILE',
    'ACCOUNTING',
    'Process BAMS FTDCA Settlement File',
    'SendS Daily FTDCA BAMS Settlement File',
    '0 15 2-6 * * ?',
    'FTDCA',
    'N',
    'OJMS.BAMS_SETTLEMENT',
    'SimpleJmsJob',
    'Y');

    insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS FLORIST SETTLEMENT FILE',
    'ACCOUNTING',
    'Process BAMS FLORIST Settlement File',
    'Sends Daily FLORIST BAMS Settlement File',
    '0 30 2-6 * * ?',
    'FLORIST',
    'N',
    'OJMS.BAMS_SETTLEMENT',
    'SimpleJmsJob',
    'Y');

    insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
    CLASS_CODE,
    ACTIVE_FLAG)
values (
    'BAMS FLYFLW SETTLEMENT FILE',
    'ACCOUNTING',
    'Process BAMS FLYFLW Settlement Files',
    'Sends Daily FLYFLW BAMS Settlement File',
    '0 45 2-6 * * ?',
    'FLYFLW',
    'N',
    'OJMS.BAMS_SETTLEMENT',
    'SimpleJmsJob',
    'Y');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1183 (mark) request #125758              --------------------------------
------------------------------------------------------------------------------------
