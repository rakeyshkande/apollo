------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1183 (mark) request #125758              --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values ('ACCOUNTING_CONFIG','BAMS_SETTLEMENT_FILE_OFFSET_DATE','0' ,'defect_1183',Sysdate,'defect_1183',Sysdate,
'BAMS Settlement date offset (in days) for Settlement File processing');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                               12/11/2014  -------- 
-- defect #1183 (mark) request #125758              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Rose Lazuk,                               12/16/2014  -------- 
-- defect #1183 (mark) request #126451              --------------------------------
------------------------------------------------------------------------------------

Insert Into Frp.Global_Parms (Context,Name,Value,Created_By,Created_On,Updated_By,Updated_On,Description) 
Values ('ACCOUNTING_CONFIG','BamsSettlementLocalArchiveLocation','/u02/apollo/accounting/pts/archive','defect_1183',
Sysdate,'defect_1183',Sysdate,'BAMS Settlement local archive folder');

------------------------------------------------------------------------------------ 
-- end   requested by Rose Lazuk,                               12/16/2014  -------- 
-- defect #1183 (mark) request #126451              --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------ 
-- begin requested by Abishek Chandna,                          12/18/2014  -------- 
-- defect #1204 (mark) request #                    --------------------------------
------------------------------------------------------------------------------------

INSERT INTO frp.global_parms
(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES
('ACCOUNTING_CONFIG','BamsSSHPrivateKeyPath','/home/oracle/.ssh/ftd-ssh-test-sec.asc','Defect_1204',sysdate,    'Defect_1204',sysdate,    'BAMS SSH Private Key Location')

------------------------------------------------------------------------------------ 
-- end   requested by Abishek Chandna,                          12/18/2014  -------- 
-- defect #1204 (mark) request #                    --------------------------------
------------------------------------------------------------------------------------

