-------------------------------------------------------------------
-- begin requested by Kishor Vasantala,         6/ 4/2014  --------
-- defect #18499                                 ------------------
-------------------------------------------------------------------

-- global param for auto-reply emails template
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_AUTO_REPLY',
    'Standard Test',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'Template Id for auto-reply mails');
	
-- global param for COM stock emails template	
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_CUSTOMER_SERVICE',
    'Standard Test',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'Template Id for COM stock emails');
	
-- global param for devliery confirmation emails template	
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_DCON',
    'Ship Delivery Test',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'Template Id for delivery confirmation mails');

-------------------------------------------------------------------
-- end   requested by Kishor Vasantala,         6/ 4/2014  --------
-- defect #18499                                 ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Tim Schmig      ,         6/ 5/2014  --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------

insert into quartz_schema.groups (
    GROUP_ID,
    DESCRIPTION,
    ACTIVE_FLAG)
values (
    'MESSAGE_GENERATOR',
    'Message Generator',
    'Y');

insert into quartz_schema.pipeline (
    PIPELINE_ID,
    GROUP_ID,
    DESCRIPTION,
    TOOL_TIP,
    DEFAULT_SCHEDULE,
    DEFAULT_PAYLOAD,
    FORCE_DEFAULT_PAYLOAD,
    QUEUE_NAME,
   CLASS_CODE,
    ACTIVE_FLAG)
values (
    'INSERT_MESSAGE_GENERATOR',
    'MESSAGE_GENERATOR',
    'Insert Message Generator record',
    'Sends a JMS message to Message Generator. The payload is a POINT_OF_CONTACT_ID.',
    null,
    null,
    'N',
    'OJMS.MESSAGE_GENERATOR',
    'SimpleJmsJob',
    'Y');
-------------------------------------------------------------------
-- end requested by Tim Schmig      ,           6/ 5/2014  --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------