-------------------------------------------------------------------
-- begin requested by Gary Sergey,               6/4/2014  --------
-- defect #18499                                 ------------------
-------------------------------------------------------------------

update FRP.GLOBAL_PARMS set VALUE='N', UPDATED_BY='defect_18499', 
UPDATED_ON=sysdate where CONTEXT='MESSAGE_GENERATOR_CONFIG' 
and NAME='MAILSERVER_TRANSFORMER_ENABLED';

-------------------------------------------------------------------
-- end   requested by Gary Sergey,               6/4/2014  --------
-- defect #18499                                 ------------------
-------------------------------------------------------------------

