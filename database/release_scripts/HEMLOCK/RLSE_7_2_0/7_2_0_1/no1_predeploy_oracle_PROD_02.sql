-------------------------------------------------------------------
-- begin requested by Swami Patsa,              4/ 4/2014  --------
-- defect #19435                                 ------------------
-------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS 
(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values 
('SECURITY_MANAGER_CONFIG','releasenumber','TBD','Defect_19435',
SYSDATE,'Defect_19435',SYSDATE,'This field has value for release number');

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              4/ 4/2014  --------
-- defect #19435                                 ------------------
-------------------------------------------------------------------

