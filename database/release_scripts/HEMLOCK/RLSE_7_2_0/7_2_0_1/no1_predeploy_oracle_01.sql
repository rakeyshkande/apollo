-------------------------------------------------------------------
-- begin requested by Swami Patsa,              4/ 8/2014  --------
-- defect #18229                                 ------------------
-------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS 
(CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) 
values 
('OPM_INV_CTXT','IT_DEFAULT_ENDDATE','12/31/2049','Defect_18229',
SYSDATE,'Defect_18229',SYSDATE,'This field is used store default SKU-IT record end date.');

-------------------------------------------------------------------
-- end   requested by Swami Patsa,              4/ 8/2014  --------
-- defect #18229                                 ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Tim Schmig,              5/1/2014    --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'WEB_SERVICE_ENABLED',
    'Y',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'If Y, send emails via web service. If N, send as emails through SMTP_HOST_NAME');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'DEFAULT_TEMPLATE_ID',
    'Standard Test',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'Default Exact Target Template Id, if one is not assigned.');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION)
VALUES (
    'MESSAGE_GENERATOR_CONFIG',
    'TEMPLATE_ID_PEND_REMOVE',
    'Standard Test',
    sysdate,
    'Defect_18499',
    'Defect_18499',
    sysdate,
    'Template Id for Scrub Emails');

-------------------------------------------------------------------
-- end requested by Tim Schmig,                5/1/2014    --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------

-------------------------------------------------------------------
-- begin requested by Gary Sergy,              5/15/2014    --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------

-- Global Parm and Secure Config properties for FICO/ExactTarget #18499
-- per Gary, QA team will change the value for BORGT from front end.

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION) VALUES
('SERVICE','EXACT_TARGET_WS_URL','REPLACE ME',sysdate,'DEFECT_18499','DEFECT_18499',sysdate,
'Web Service URL for ExactTarget email service');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION,KEY_NAME) VALUES
('SERVICE','EXACT_TARGET_WS_USERNAME',global.encryption.encrypt_it('REPLACE ME','APOLLO_PROD_2013'),
sysdate,'DEFECT_18499','DEFECT_18499',sysdate,'Username for ExactTarget Email Web Service','APOLLO_PROD_2013');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT,NAME,VALUE,CREATED_ON,CREATED_BY,UPDATED_BY,UPDATED_ON,DESCRIPTION,KEY_NAME) VALUES
('SERVICE','EXACT_TARGET_WS_PASSWORD',global.encryption.encrypt_it('REPLACE ME','APOLLO_PROD_2013'),
sysdate,'DEFECT_18499','DEFECT_18499',sysdate,'Password for ExactTarget Email Web Service','APOLLO_PROD_2013');

-------------------------------------------------------------------
-- end requested by Gary Sergy,              5/15/2014    --------
-- defect #18499 (JLIN)                          ------------------
-------------------------------------------------------------------
