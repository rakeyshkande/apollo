set define off
--------------------------------
-- Fix_4_4_Release_Script.sql --
--------------------------------

--*************************************************
-- PRE-DEPLOY SECTION
--*************************************************

--
-- 6414
--
create table ftd_apps.shopping_index_src
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      parent_index_name      varchar2(1000),
      index_desc             varchar2(4000)   not null,
      country_id             varchar2(2),
      display_seq_num        number(5)        not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_pk primary key (index_name, source_code)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_fk1 foreign key (source_code)
                         references ftd_apps.source_master,
         constraint shopping_index_src_fk2 foreign key (parent_index_name, source_code)
                         references ftd_apps.shopping_index_src,
         constraint shopping_index_src_fk3 foreign key (country_id)
                         references ftd_apps.country_master
     ) tablespace ftd_apps_data;
     
create index ftd_apps.shopping_index_src_n1 
    on ftd_apps.shopping_index_src(parent_index_name,source_code) tablespace ftd_apps_indx;


create table ftd_apps.shopping_index_src_stage
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      parent_index_name      varchar2(1000),
      index_desc             varchar2(4000)   not null,
      country_id             varchar2(2),
      display_seq_num        number(5)        not null,
      failure_reason_txt     varchar2(4000),
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_stg_pk primary key (index_name, source_code)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_stg_fk1 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;


create table ftd_apps.shopping_index_src_prod
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      product_id             varchar2(10)     not null,
      display_seq_num        number(5)        not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shopping_index_src_prod_pk primary key (index_name, source_code, product_id)
                         using index tablespace ftd_apps_indx,
         constraint shopping_index_src_prod_fk1 foreign key (source_code)
                         references ftd_apps.source_master,
         constraint shopping_index_src_prod_fk2 foreign key (product_id)
                         references ftd_apps.product_master
     ) tablespace ftd_apps_data;
     
create index ftd_apps.shopping_index_src_prod_n1 
    on ftd_apps.shopping_index_src_prod(product_id) tablespace ftd_apps_indx;


create table ftd_apps.shopping_index_src_prod_stage
     (index_name             varchar2(1000)   not null,
      source_Code            varchar2(10)     not null,
      product_id             varchar2(10)     not null,
      display_seq_num        number(5)        not null,
      failure_reason_txt     varchar2(4000),
      created_on             date             not null,
      created_by             varchar2(100)    not null,
         constraint shop_index_src_prod_stage_pk primary key (index_name, source_code, product_id)
                         using index tablespace ftd_apps_indx,
         constraint shop_inde_src_prod_stage_fk1 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;



create table ftd_apps.product_source
     (product_id             varchar2(10)     not null,
      source_Code            varchar2(10)     not null,
      last_modified_date     date             not null,
         constraint product_source_pk primary key (product_id, source_code)
                         using index tablespace ftd_apps_indx,
         constraint product_source_fk1 foreign key (product_id)
                         references ftd_apps.product_master,
         constraint product_source_fk2 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;


create table ftd_apps.upsell_source
     (upsell_master_id       varchar2(4)      not null,
      source_Code            varchar2(10)     not null,
      last_modified_date     date             not null,
         constraint upsell_source_pk primary key (upsell_master_id, source_Code)
                         using index tablespace ftd_apps_indx,
         constraint upsell_source_fk1 foreign key (upsell_master_id)
                         references ftd_apps.upsell_master,
         constraint upsell_source_fk2 foreign key (source_code)
                         references ftd_apps.source_master
     ) tablespace ftd_apps_data;



--
-- 6106
--

create table ftd_apps.product_addon
     (product_id             varchar2(10)     not null,
      addon_id               varchar2(10)     not null,
      display_seq_num        number(5),
      active_Flag            varchar2(1) default 'Y' not null,
      max_qty                number(5),
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint product_addon_pk primary key (product_id, addon_id)
                         using index tablespace ftd_apps_indx,
         constraint product_addon_fk1 foreign key (product_id)
                         references ftd_apps.product_master,
         constraint product_addon_fk2 foreign key (addon_id)
                         references ftd_apps.addon,
         constraint product_addon_ck1 check (active_Flag in ('Y','N'))
     ) tablespace ftd_apps_data;
     
comment on table ftd_apps.product_addon is 'Show the add-on assignment per product';
comment on column ftd_apps.product_addon.product_id        is 'Product id IF that product has an add-on assigned to it';
comment on column ftd_apps.product_addon.addon_id          is 'Add-on id assigned to a product';
comment on column ftd_apps.product_addon.display_seq_num   is 'Order of display when showing the available add-ons to the user during order entry';
comment on column ftd_apps.product_addon.active_Flag       is 'Shows if the add-on is assigned to a product';
comment on column ftd_apps.product_addon.max_qty           is 'Max quantity of add-on allowed per product';



create table ftd_apps.vendor_addon
     (vendor_id              varchar2(5)      not null,
      addon_id               varchar2(10)     not null,
      active_Flag            varchar2(1) default 'Y' not null,
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint vendor_addon_pk primary key (vendor_id, addon_id)
                         using index tablespace ftd_apps_indx,
         constraint vendor_addon_fk1 foreign key (vendor_id)
                         references ftd_apps.vendor_master,
         constraint vendor_addon_fk2 foreign key (addon_id)
                         references ftd_apps.addon,
         constraint vendor_addon_ck1 check (active_Flag in ('Y','N'))
     ) tablespace ftd_apps_data;

comment on table ftd_apps.vendor_addon is 'Show the add-on availability at different vendors';
comment on column ftd_apps.vendor_addon.vendor_id        is 'Vendor id IF that vendor carries that add-on';
comment on column ftd_apps.vendor_addon.addon_id         is 'Add-on id assigned to a vendor.';
comment on column ftd_apps.vendor_addon.active_Flag       is 'Shows if the add-on is carried by a vendor';


grant references on ftd_apps.addon to venus;

create table venus.venus_addon
     (venus_id               varchar2(20)     not null,
      addon_id               varchar2(10)     not null,
      addon_qty              number(5)        not null,
      per_addon_price        number(8,2)      not null,
      per_addon_weight       varchar2(10)     not null,        
      created_on             date             not null,
      created_by             varchar2(100)    not null,
      updated_on             date             not null,
      updated_by             varchar2(100)    not null,
         constraint venus_addon_pk primary key (venus_id, addon_id)
                         using index tablespace venus_indx,
         constraint venus_addon_fk1 foreign key (venus_id)
                         references venus.venus,
         constraint venus_addon_fk2 foreign key (addon_id)
                         references ftd_apps.addon
     ) tablespace venus_data;
     
comment on table venus.venus_addon is 'show all the add-ons for a given venus_id';
comment on column venus.venus_addon.venus_id          is  'Venus id of the FTD that has the add-on';
comment on column venus.venus_addon.addon_id          is  'Add-on id for a given order on the FTD';
comment on column venus.venus_addon.addon_qty         is  'Quantity per add-on id on the FTD';
comment on column venus.venus_addon.per_addon_price   is  'Price of each individual addon';
comment on column venus.venus_addon.per_addon_weight  is  'Weight of each individual addon';


--*************************************************
-- END PRE-DEPLOY SECTION, --- START OF DEPLOY ---
--*************************************************


--
-- 6106
--

alter table ftd_apps.addon modify     (addon_type             varchar2(4));

alter table ftd_apps.addon add
     (product_id             varchar2(10),
      active_Flag            varchar2(1) default 'Y' NOT NULL,
      created_on             date,
      created_by             varchar2(100),
      updated_on             date,
      updated_by             varchar2(100),
         constraint addon_fk1 foreign key (product_id)
                         references ftd_apps.product_master,
         constraint addon_ck1 check (active_Flag in ('Y','N'))
     );

create index ftd_apps.addon_n1 on ftd_apps.addon(product_id) tablespace ftd_apps_indx;

comment on column ftd_apps.addon.product_id is 'Product id for an add-on.  This product should be Unavailable, and customer not be able to order this product.  The only purpose of this id is to utilize current component SKU logic to track vendor costs for an add-on';
comment on column ftd_apps.addon.active_flag is 'Shows if the addon_id is available';


alter table ftd_apps.addon_type add
     (product_feed_Flag            varchar2(1) default 'Y' NOT NULL,
         constraint addon_type_ck1 check (product_feed_Flag in ('Y','N'))
     );

comment on column ftd_apps.addon_type.product_feed_flag is 'This flag controls whether (Y) or not (N) the add-ons of this type are sent to the web site';

  





----
---- END Fix 4.4 Release Script
----
