set define off;

declare

procedure insert_addon(v_addon_id varchar2, v_addon_type varchar2, v_description varchar2, v_price number, v_text varchar2,
            v_unspsc varchar2, v_product_id varchar2, v_active_flag varchar2, v_default varchar2, v_display_price varchar2, v_weight varchar2) is

begin

    insert into FTD_APPS.ADDON (ADDON_ID, ADDON_TYPE, DESCRIPTION, PRICE, ADDON_TEXT, UNSPSC, PRODUCT_ID, ACTIVE_FLAG,
         DEFAULT_PER_TYPE_FLAG, DISPLAY_PRICE_FLAG, ADDON_WEIGHT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
    values
        (v_addon_id, v_addon_type, v_description, v_price, v_text, v_unspsc, v_product_id, v_active_flag,
         v_default, v_display_price, v_weight, sysdate, 'Defect_6106', sysdate, 'Defect_6106');

end insert_addon;


begin

	  update ftd_apps.addon a
	set a.ADDON_TEXT = 'Add a Balloon - A mylar balloon creates a beautiful accompaniment to our bouquets and gifts.  We will select a balloon appropriate for your special occasion.',
	    a.PRODUCT_ID = null,
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'A';
	
	update ftd_apps.addon a
	set a.ADDON_TEXT = 'Add a bear to your bouquet and we will select a cute & cuddly bear that will send the perfect sentiment. Size and color may vary.',
	    a.PRODUCT_ID = 'BKJ',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0.25,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'B';
	
	update ftd_apps.addon a
	set a.ADDON_TEXT = 'Add a Box of Chocolates to your bouquet and we will include a box of delicious chocolates to send sweet wishes their way. Size and flavor may vary. (May include nuts).',
	    a.PRODUCT_ID = 'CKJ',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0.2,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'C';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'F';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Free 4 piece Box of Chocolates',
	    a.PRICE = 0,
	    a.ADDON_TEXT = 'FREE 4-piece box of Russell Stover&amp;reg; Chocolates ',
	    a.UNSPSC = '5016181300',
	    a.PRODUCT_ID = 'FCT',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0.2,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'FC';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'New Baby',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC100';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Birthday',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC84';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Sympathy',
	    a.ADDON_TEXT = 'Add a Full-Size Card.  We will select a card appropriate for the occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC86';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Get Well',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC89';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Thinking of You',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC90';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Thank You',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC93';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Anniversary',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC95';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Love and Romance',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC97';
	
	update ftd_apps.addon a
	set a.DESCRIPTION = 'Congratulations',
	    a.ADDON_TEXT = 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.',
	    a.ACTIVE_FLAG = 'Y',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'Y',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC99';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC110';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC111';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC112';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC113';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC114';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC115';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC116';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC117';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC118';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC83';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC85';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC87';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC88';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC91';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC92';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC94';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC96';
	
	update ftd_apps.addon a
	set a.ACTIVE_FLAG = 'N',
	    a.DEFAULT_PER_TYPE_FLAG = 'N',
	    a.DISPLAY_PRICE_FLAG = 'N',
	    a.ADDON_WEIGHT = 0,
	    a.CREATED_ON = sysdate,
	    a.CREATED_BY = 'DEFECT_6106',
	    a.UPDATED_ON = sysdate,
	    a.UPDATED_BY = 'DEFECT_6106'
	where a.ADDON_ID = 'RC98';

  insert_addon('RC999', '4', 'Occasion', 3.5, 'Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.', '1411160500', null, 'Y', 'N', 'Y', 0);
  insert_addon('C11', '5', 'Godiva&amp;reg; 4 piece box of chocolates', 9.99, 'Add a Box of Chocolates and we will sweeten their day with a 4-piece box of sumptuous Godiva&amp;reg; Chocolates to accompany your bouquet.', '5016181300', 'C11T', 'Y', 'N', 'Y', 0.2);
  insert_addon('G11', '9', 'Gourmet Cookies', 4.99, 'Add a Box of Cookies and your special recipient will receive 2 ounces of chocolate cookies that will add to the joy and beauty of your gift.', '5018190500', 'G11T', 'Y', 'N', 'Y', 0.2);
  insert_addon('M11', '15', 'Musical Sound Chip Greeting', 6.99, 'Add a musical greeting.  When the recipient opens the box, they will be greeted with a happy tune.', '6013160000', 'M11T', 'Y', 'N', 'Y', 0.2);
  insert_addon('PB111', '8', 'Brown FTD Bear', 9.99, 'Add a bear to your bouquet and we will select a cute & cuddly brown bear that will send the perfect sentiment.', '6014100400', 'PB11', 'Y', 'N', 'Y', 0.25);
  insert_addon('PB122', '8', 'White FTD Bear', 9.99, 'Add a bear to your bouquet and we will select a cute & cuddly white bear that will send the perfect sentiment.', '6014100400', 'PB12', 'Y', 'N', 'Y', 0.25);
  insert_addon('V000', '7', 'No Vase', 0, 'This bouquet is available without a vase.  We recommend you select a vase to create the perfect presentation.', '9518540800', 'V000', 'Y', 'N', 'N', 0);
  insert_addon('V11L', '7', 'Cut Glass Vase ', 19.99, 'Elegantly Sophisticated Cut Glass Vase - 9 inches in height', '4910161300', 'V11L', 'Y', 'N', 'Y', 1);
  insert_addon('V11S', '7', 'Cut Glass Vase ', 18.99, 'Elegantly Sophisticated Cut Glass Vase - 8 inches in height', '4910161300', 'V11S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V12L', '7', 'Cut Glass Vase ', 10, 'Elegantly Sophisticated Cut Glass Vase - 9 inches in height', '4910161300', 'V12L', 'Y', 'N', 'Y', 1);
  insert_addon('V12S', '7', 'Cut Glass Vase ', 9, 'Elegantly Sophisticated Cut Glass Vase - 8 inches in height', '4910161300', 'V12S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V2122', '7', 'Glass Vase Included', 0, 'This modern rectangular vase creates a stunning presentation.  8 inch high.', '9518540800', 'V212', 'Y', 'N', 'N', 0.8);
  insert_addon('V2123', '7', 'Rectangular Vase ', 10.99, 'This modern rectangular vase creates a stunning presentation.  8HX4L X4W.', '9518540800', 'V213', 'Y', 'N', 'Y', 0.8);
  insert_addon('V23L', '7', 'Tapered Glass Vase ', 19.99, 'Frosted Glass Design Tapered Glass Vase - 10 inches in height', '9518540800', 'V23L', 'Y', 'N', 'Y', 1);
  insert_addon('V23S', '7', 'Tapered Glass Vase ', 18.99, 'Frosted Glass Design Tapered Glass Vase - 8 inches in height', '9518540800', 'V23S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V24L', '7', 'Tapered Glass Vase ', 10, 'Frosted Glass Design Tapered Glass Vase - 10 inches in height', '9518540800', 'V24L', 'Y', 'N', 'Y', 1);
  insert_addon('V24S', '7', 'Tapered Glass Vase ', 9, 'Frosted Glass Design Tapered Glass Vase - 8 inches in height', '9518540800', 'V24S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V3112', '7', 'Glass Vase Included', 0, 'This modern rectangular vase creates a stunning presentation.  6 inch high', '9518540800', 'V312', 'Y', 'N', 'N', 0.7);
  insert_addon('V3113', '7', 'Rectangular Vase ', 9.99, 'This modern rectangular vase creates a stunning presentation.  6H x 4L x 3L', '9518540800', 'V313', 'Y', 'N', 'Y', 0.7);
  insert_addon('V31L', '7', 'Silver Finish Ceramic Cup ', 17.99, 'Luminous Silver Finish Ceramic Julep Cup - 8 inches in height', '1110180200', 'V31L', 'Y', 'N', 'Y', 1);
  insert_addon('V31S', '7', 'Silver Finish Ceramic Cup ', 16.99, 'Luminous Silver Finish Ceramic Julep Cup - 6 inches in height', '1110180200', 'V31S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V32L', '7', 'Silver Finish Ceramic Cup ', 8, 'Luminous Silver Finish Ceramic Julep Cup - 8 inches in height', '1100180200', 'V32L', 'Y', 'N', 'Y', 1);
  insert_addon('V32S', '7', 'Silver Finish Ceramic Cup ', 7, 'Luminous Silver Finish Ceramic Julep Cup - 6 inches in height', '1100180200', 'V32S', 'Y', 'N', 'Y', 0.8);
  insert_addon('V4123', '7', 'Glass Vase Included', 0, 'Sleek Glass Vase, 6.5HX4WX3.75B', '9518540800', 'V423', 'Y', 'N', 'N', 0.7);
  insert_addon('V4124', '7', 'Glass Gathering Vase ', 9.99, 'Sleek Glass Vase, 6.5HX4WX3.75B', '9518540800', 'V424', 'Y', 'N', 'Y', 0.7);
  insert_addon('V5234', '7', 'Glass Vase Included', 0, 'Sleek Glass Gathering Vase, 8HX4.5WX4.75B', '9518540800', 'V534', 'Y', 'N', 'N', 0.8);
  insert_addon('V5235', '7', 'Glass Gathering Vase ', 10.99, 'Sleek Glass Gathering Vase, 8HX4.5WX4.75B', '9518540800', 'V535', 'Y', 'N', 'Y', 0.8);
  insert_addon('V6345', '7', 'Glass Vase Included', 0, 'Slender Clear Glass Gathering Vase, 8HX4WX4B', '9518540800', 'V634', 'Y', 'N', 'N', 0.8);
  insert_addon('V6346', '7', 'Slender Gathering Vase ', 10.99, 'Slender Clear Glass Gathering Vase, 8HX4WX4B', '9518540800', 'V635', 'Y', 'N', 'Y', 0.8);
  insert_addon('V7456', '7', 'Glass Vase Included', 0, 'Rounded glass vase, 9HX4.5WX3.5B', '9518540800', 'V756', 'Y', 'N', 'N', 0.8);
  insert_addon('V7457', '7', 'Glass Rounded Vase ', 10.99, 'Rounded glass vase, 9HX4.5WX3.5B', '9518540800', 'V757', 'Y', 'N', 'Y', 0.8);
  insert_addon('V8555', '7', 'Glass Vase Included', 0, 'Sweetheart glass vase, 8HX4.25WX3B', '9518540800', 'V855', 'Y', 'N', 'N', 0.8);
  insert_addon('V8556', '7', 'Sweetheart Glass Rounded Vase', 10.99, 'Sweetheart glass vase, 8HX4.25WX3B', '9518540800', 'V856', 'Y', 'N', 'Y', 0.8);
end;
/