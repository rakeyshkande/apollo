set define off
--------------------------------
-- Fix_4_2_Release_Script.sql --
--------------------------------

--********************************************
-- PRE-DEPLOY SECTION
--********************************************


--
-- 5024
--

create table ftd_apps.reward_master 
     (reward_code           varchar2(10)   NOT NULL,
      display_name          varchar2(100)  NOT NULL,
      reward_description    varchar2(4000) NOT NULL,
      active_flag           varchar2(1) default 'Y' NOT NULL,
      created_on            date           NOT NULL,
      created_by            varchar2(100)  NOT NULL,
      updated_on            date           NOT NULL,
      updated_by            varchar2(100)  NOT NULL,
      constraint reward_master_pk primary key (reward_code) 
                 using index tablespace ftd_apps_indx,
      constraint reward_master_ck1  check (active_flag in ('Y','N'))
     ) tablespace ftd_apps_data;

comment on table ftd_apps.reward_master is 'Models a reward for a program.';


create table ftd_apps.source_reward_exclusion 
     (source_code           varchar2(10)   NOT NULL,
      reward_code           varchar2(10)   NOT NULL,
      active_flag           varchar2(1) default 'Y' NOT NULL,
      created_on            date           NOT NULL,
      created_by            varchar2(100)  NOT NULL,
      updated_on            date           NOT NULL,
      updated_by            varchar2(100)  NOT NULL,
      constraint source_reward_exclusion_pk primary key (source_code,reward_code) 
                 using index tablespace ftd_apps_indx,
      constraint source_reward_exclusion_fk1 foreign key(source_code) references ftd_apps.source_master(source_code),
      constraint source_reward_exclusion_ck1  check (active_flag in ('Y','N'))
     ) tablespace ftd_apps_data;

comment on table ftd_apps.source_reward_exclusion is 'Stores references between a source code and reward program.';


create table ftd_apps.product_reward_exclusion 
     (product_id            varchar2(10)   NOT NULL,
      reward_code           varchar2(10)   NOT NULL,
      active_flag           varchar2(1) default 'Y' NOT NULL,
      created_on            date           NOT NULL,
      created_by            varchar2(100)  NOT NULL,
      updated_on            date           NOT NULL,
      updated_by            varchar2(100)  NOT NULL,
      constraint product_reward_exclusion_pk primary key (product_id,reward_code) 
                 using index tablespace ftd_apps_indx,
      constraint product_reward_exclusion_fk1 foreign key(product_id) references ftd_apps.product_master(product_id),
      constraint product_reward_exclusion_ck1  check (active_flag in ('Y','N'))
     ) tablespace ftd_apps_data;

comment on table ftd_apps.product_reward_exclusion is 'Stores references between a product and reward program.';


--
-- PRODUCTION  (NONPROD - use filesystem file names for datafile)
--
create tablespace account_data datafile '+ZEUS_DATA_VK' 
size 1000m autoextend on next 100m maxsize 20480m
extent management local autoallocate 
segment space management auto;

create tablespace account_indx datafile '+ZEUS_DATA_VK' 
size 1000m autoextend on next 100m maxsize 20480m
extent management local autoallocate 
segment space management auto;
-- END PRODUCTION SPECIFIC CODE


create user account identified by values 'disabled' account lock default tablespace account_data
temporary tablespace temp quota unlimited on account_data quota unlimited on account_indx;

grant create session to account;


create table account.account_master 
     (account_master_id          number(11)     NOT NULL,
      email_address              varchar2(1000) NOT NULL,
      active_flag                varchar2(1) default 'Y' NOT NULL,
      last_email_transfer_date   date,
      created_on                 date           NOT NULL,
      created_by                 varchar2(100)  NOT NULL,
      updated_on                 date           NOT NULL,
      updated_by                 varchar2(100)  NOT NULL,
      constraint account_master_pk primary key (account_master_id) 
                 using index tablespace account_indx,
      constraint account_master_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.account_master is 'Models an account that is uniquely identified by an email address.';

create index account.account_master_n1 on account.account_master(email_address) tablespace account_indx;

create sequence account.account_master_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;



create table account.account_elig_reason 
     (account_elig_reason_code     varchar2(5)     NOT NULL,
      active_flag                  varchar2(1) default 'Y' NOT NULL,
      account_elig_reason_desc     varchar2(1000)   NOT NULL,
      created_on            date           NOT NULL,
      created_by            varchar2(100)  NOT NULL,
      updated_on            date           NOT NULL,
      updated_by            varchar2(100)  NOT NULL,
      constraint account_elig_reason_pk primary key (account_elig_reason_code) 
                 using index tablespace account_indx,
      constraint account_elig_reason_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.account_elig_reason is 'Stores valid codes for eligible reasons.';



grant references on ftd_apps.reward_master to account;

create table account.account_reward 
     (account_master_id              number(11)     NOT NULL,
      reward_code                    varchar2(10)   NOT NULL,
      active_flag                    varchar2(1) default 'Y' NOT NULL,
      eligible_since_date            date           NOT NULL,
      account_elig_reason_code       varchar2(5)    NOT NULL,
      created_on                     date           NOT NULL,
      created_by                     varchar2(100)  NOT NULL,
      updated_on                     date           NOT NULL,
      updated_by                     varchar2(100)  NOT NULL,
      constraint account_reward_pk primary key (account_master_id, reward_code) 
                 using index tablespace account_indx,
      constraint account_reward_fk1 foreign key(account_master_id) references account.account_master(account_master_id),
      constraint account_reward_fk2 foreign key(reward_code) references ftd_apps.reward_master(reward_code),
      constraint account_reward_fk3 foreign key(account_elig_reason_code) references account.account_elig_reason(account_elig_reason_code),
      constraint account_reward_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.account_reward is 'Stores association between an account and a reward program.';
create index account.account_reward_n1 on account.account_reward(account_elig_reason_code);

create table account.rw_creation_batch 
     (rw_creation_batch_id       number(11)     NOT NULL,
      end_date                   date           NOT NULL,
      reward_code                varchar2(10)   NOT NULL,
      completed_flag             varchar2(1) default 'N' NOT NULL,
      created_on                 date           NOT NULL,
      created_by                 varchar2(100)  NOT NULL,
      updated_on                 date           NOT NULL,
      updated_by                 varchar2(100)  NOT NULL,
      constraint rw_creation_batch_pk primary key (rw_creation_batch_id) 
                 using index tablespace account_indx,
      constraint rw_creation_batch_fk1 foreign key(reward_code) references ftd_apps.reward_master(reward_code),
      constraint rw_creation_batch_ck1  check (completed_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.rw_creation_batch is 'Reward certificate creation batch records.';
create index account.rw_creation_batch_n1 on account.rw_creation_batch(reward_code) tablespace account_indx;

create sequence account.rw_creation_batch_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;

grant references on clean.gc_coupons to account;


create table account.rw_creation_detail 
     (rw_creation_detail_id      number(11)     NOT NULL,
      rw_creation_batch_id       number(11)     NOT NULL,
      account_master_id          number(11)     NOT NULL,
      reward_code                varchar2(10)   NOT NULL,
      gc_coupon_number           varchar2(20)   NOT NULL,
      created_on                 date           NOT NULL,
      created_by                 varchar2(100)  NOT NULL,
      updated_on                 date           NOT NULL,
      updated_by                 varchar2(100)  NOT NULL,
      constraint rw_creation_detail_pk primary key (rw_creation_detail_id) 
                 using index tablespace account_indx,
      constraint rw_creation_detail_fk1 foreign key(rw_creation_batch_id) references account.rw_creation_batch(rw_creation_batch_id),
      constraint rw_creation_detail_fk2 foreign key(account_master_id,reward_code) references account.account_reward(account_master_id,reward_code),
      constraint rw_creation_detail_fk3 foreign key(gc_coupon_number) references clean.gc_coupons(gc_coupon_number)
     ) tablespace account_data;

comment on table account.rw_creation_detail is 'Reward certification batch creation details.';

create sequence account.rw_creation_detail_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;

create index account.rw_creation_detail_n1 on account.rw_creation_detail(rw_creation_batch_id) tablespace account_indx;
create index account.rw_creation_detail_n2 on account.rw_creation_detail(account_master_id,reward_code) tablespace account_indx;
create index account.rw_creation_detail_n3 on account.rw_creation_detail(gc_coupon_number) tablespace account_indx;


grant references on clean.order_details to account;

create table account.account_reward_order 
     (account_reward_order_id    number(11)     NOT NULL,
      account_master_id          number(11)     NOT NULL,
      reward_code                varchar2(10)   NOT NULL,
      order_detail_id            number         NOT NULL,
      reward_order_value         number(12,2),
      reward_order_status_code   varchar2(15)   NOT NULL,
      unapplied_reason_code      varchar2(15),
      ineligible_date            date,
      pending_date               date,
      did_not_qualify_date       date,
      qualified_date             date,
      disqualified_date          date,
      expiration_date            date,
      applied_date               date,
      planned_qualifying_date    date,
      planned_expiring_date      date,
      rw_creation_detail_id      number(11),
      created_on                 date           NOT NULL,
      created_by                 varchar2(100)  NOT NULL,
      updated_on                 date           NOT NULL,
      updated_by                 varchar2(100)  NOT NULL,
      constraint account_reward_order_pk primary key (account_reward_order_id) 
                 using index tablespace account_indx,
      constraint account_reward_order_fk1 foreign key(account_master_id,reward_code) references account.account_reward(account_master_id,reward_code),
      constraint account_reward_order_fk2 foreign key(order_detail_id) references clean.order_details(order_detail_id),
      constraint account_reward_order_fk3 foreign key(reward_order_status_code) references account.reward_order_status(reward_order_status_code),
      constraint account_reward_order_fk4 foreign key(rw_creation_detail_id) references account.rw_creation_detail(rw_creation_detail_id)
     ) tablespace account_data;

comment on table account.account_reward_order is 'Stores orders for the reward account.';

create unique index account.account_reward_order_u1 on account.account_reward_order(account_master_id, reward_code, order_detail_id) tablespace account_indx;
create index account.account_reward_order_n1 on account.account_reward_order(reward_order_status_code) tablespace account_indx;
create index account.account_reward_order_n2 on account.account_reward_order(planned_qualifying_date) tablespace account_indx;
create index account.account_reward_order_n3 on account.account_reward_order(planned_expiring_date) tablespace account_indx;
create index account.account_reward_order_n4 on account.account_reward_order(rw_creation_detail_id) tablespace account_indx;

create sequence account.account_reward_order_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 1000;


create table account.account_reward_order$
     (account_reward_order_id    number(11)     NOT NULL,
      account_master_id          number(11),
      reward_code                varchar2(10),
      order_detail_id            number,
      reward_order_value         number(12,2),
      reward_order_status_code   varchar2(15),
      unapplied_reason_code      varchar2(15),
      ineligible_date            date,
      pending_date               date,
      did_not_qualify_date       date,
      qualified_date             date,
      disqualified_date          date,
      expiration_date            date,
      applied_date               date,
      planned_qualifying_date    date,
      planned_expiring_date      date,
      rw_creation_detail_id      number(11),
      created_on                 date,
      created_by                 varchar2(100),
      updated_on                 date,
      updated_by                 varchar2(100),
      OPERATION$                 VARCHAR2(7)      not null,
      TIMESTAMP$                 TIMESTAMP        not null
     ) tablespace account_data;



create table account.reward_order_status 
     (reward_order_status_code       varchar2(10)   NOT NULL,
      active_flag                    varchar2(1) default 'Y' NOT NULL,
      reward_order_status_desc       varchar2(4000) NOT NULL,
      created_on                     date,
      created_by                     varchar2(100),
      updated_on                     date,
      updated_by                     varchar2(100),
      constraint reward_order_status_pk primary key (reward_order_status_code) 
                 using index tablespace account_indx,
      constraint reward_order_status_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.reward_order_status is 'Association between a reward program and its order statuses.';


create table account.order_transfer_reason
     (order_transfer_reason_code    varchar2(10)   NOT NULL,
      active_flag                   varchar2(1) default 'Y' NOT NULL,
      order_transfer_reason_desc    varchar2(4000) NOT NULL,
      created_on                     date,
      created_by                     varchar2(100),
      updated_on                     date,
      updated_by                     varchar2(100),
      constraint order_transfer_reason_pk primary key (order_transfer_reason_code) 
                 using index tablespace account_indx,
      constraint order_transfer_reason_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.order_transfer_reason is 'Valid reasons orders can be transfered from one email to another.';


create table account.order_transfer_hist 
     (order_transfer_hist_id          number(11)     NOT NULL,
      account_reward_order_id         number(11)     NOT NULL,
      from_email_address              varchar2(1000) NOT NULL,
      to_email_address                varchar2(1000) NOT NULL,
      transfer_date                   date           NOT NULL,
      order_transfer_reason_code      varchar2(10)   NOT NULL,
      transfer_type_code              varchar2(10)   DEFAULT 'Order' NOT NULL,
      approved_csr_id                 varchar2(1000) NOT NULL,
      created_on                      date           NOT NULL,
      created_by                      varchar2(100)  NOT NULL,
      updated_on                      date           NOT NULL,
      updated_by                      varchar2(100)  NOT NULL,
      constraint order_transfer_hist_pk primary key (order_transfer_hist_id) 
                 using index tablespace account_indx,
      constraint order_transfer_hist_fk1 foreign key(account_reward_order_id) 
                   references account.account_reward_order(account_reward_order_id),
      constraint order_transfer_hist_fk2 foreign key(order_transfer_reason_code) 
                   references account.order_transfer_reason(order_transfer_reason_code),
      constraint order_transfer_hist_ck1  check (transfer_type_code in ('Order','Email'))
     ) tablespace account_data;

comment on table account.order_transfer_hist is 'Order transfer history for the reward program.';

create index account.order_transfer_hist_n1 on account.order_transfer_hist(account_reward_order_id) tablespace account_indx;

create sequence account.order_trnasfer_hist_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;


create table account.unapplied_reason
     (unapplied_reason_code         varchar2(10)   NOT NULL,
      active_flag                   varchar2(1) default 'Y' NOT NULL,
      unapplied_reason_desc         varchar2(4000) NOT NULL,
      created_on                     date,
      created_by                     varchar2(100),
      updated_on                     date,
      updated_by                     varchar2(100),
      constraint unapplied_reason_pk primary key (unapplied_reason_code) 
                 using index tablespace account_indx,
      constraint unapplied_reason_ck1  check (active_flag in ('Y','N'))
     ) tablespace account_data;

comment on table account.unapplied_reason is 'Valid reasons orders are not applied to a reward.';


create table account.email_transfer_hist 
     (email_transfer_hist_id          number(11)     NOT NULL,
      from_email_address              varchar2(1000) NOT NULL,
      to_email_address                varchar2(1000) NOT NULL,
      transfer_date                   date           NOT NULL,
      created_on                      date           NOT NULL,
      created_by                      varchar2(100)  NOT NULL,
      updated_on                      date           NOT NULL,
      updated_by                      varchar2(100)  NOT NULL,
      constraint email_transfer_hist_pk primary key (email_transfer_hist_id) 
                 using index tablespace account_indx
     ) tablespace account_data;

comment on table account.email_transfer_hist is 'Email transfer history.';

create sequence account.email_transfer_hist_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;


create table clean.batch_job_type 
     (batch_job_type_code        varchar2(10)   NOT NULL,
      batch_job_type_desc        varchar2(1000) NOT NULL,
      constraint batch_job_type_pk primary key (batch_job_type_code) 
                 using index tablespace clean_indx
     ) tablespace clean_data;

comment on table clean.batch_job_type is 'Stores types of jobs batch jobs.';

create table clean.batch_job_log
     (batch_job_log_id           number(11)     NOT NULL,
      batch_job_type_code        varchar2(10)   NOT NULL,
      batch_job_ref_id           varchar2(4000),
      completed_flag             varchar2(1) default 'N' NOT NULL,
      created_on                 date           NOT NULL,
      created_by                 varchar2(100)  NOT NULL,
      updated_on                 date           NOT NULL,
      updated_by                 varchar2(100)  NOT NULL,
      constraint batch_job_log_pk primary key (batch_job_log_id) 
                 using index tablespace clean_indx,
      constraint batch_job_log_fk1 foreign key(batch_job_type_code) references clean.batch_job_type(batch_job_type_code),
      constraint batch_job_log_ck1  check (completed_flag in ('Y','N'))
     ) tablespace clean_data;

comment on table clean.batch_job_log is 'Records execution history of jobs.';

create sequence clean.batch_job_log_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;



--
-- Calculates earnings qty and amounts by account and reward
--
create or replace view account.account_reward_earnings_vw as
select cd.account_master_id,
       cd.reward_code,
       count(*)              total_certificates_earned_qty, 
       sum(gc.issue_amount)  total_certificates_earned_amt
from account.rw_creation_detail cd,
     clean.gc_coupons gc
where cd.gc_coupon_number = gc.gc_coupon_number
group by cd.account_master_id,
         cd.reward_code;






--
--  5024
--

create table clean.privacy_policy_option 
     (privacy_policy_option_code      varchar2(100)  NOT NULL,
      privacy_policy_opt_type_desc    varchar2(1000) NOT NULL,
      privacy_policy_opt_detail_desc  varchar2(1000) NOT NULL,
      bypass_priv_policy_verif_flag   varchar2(1) default 'N' NOT NULL,
      created_on                      date           NOT NULL,
      created_by                      varchar2(100)  NOT NULL,
      updated_on                      date           NOT NULL,
      updated_by                      varchar2(100)  NOT NULL,
      constraint privacy_policy_option_pk primary key (privacy_policy_option_code) 
                 using index tablespace clean_indx,
      constraint privacy_policy_option_ck1  check (bypass_priv_policy_verif_flag in ('Y','N'))
     ) tablespace clean_data;
   
comment on table clean.privacy_policy_option is 'Stores options the customer can use to supply verification for order lookup.';

create table clean.privacy_policy_verification
     (privacy_policy_verification_id  number(11)     NOT NULL,
      call_log_id                     number         NOT NULL,
      privacy_policy_option_code      varchar2(100)  NOT NULL,
      customer_id                     number,
      order_detail_id                 number,
      created_on                      date           NOT NULL,
      created_by                      varchar2(100)  NOT NULL,
      updated_on                      date           NOT NULL,
      updated_by                      varchar2(100)  NOT NULL,
      constraint privacy_policy_verification_pk primary key (privacy_policy_verification_id) 
                 using index tablespace clean_indx,
      constraint privacy_policy_verif_fk1 foreign key (call_log_id)
                 references clean.call_log,
      constraint privacy_policy_verif_fk2 foreign key (privacy_policy_option_code)
                 references clean.privacy_policy_option,
      constraint privacy_policy_verif_fk3 foreign key (customer_id)
                 references clean.customer,
      constraint privacy_policy_verif_fk4 foreign key (order_detail_id)
                 references clean.order_details
     ) tablespace clean_data;
   
comment on table clean.privacy_policy_verification is 'Stores options the customer actually used to supply verification for order lookup.';
   
create sequence clean.privacy_policy_verif_id_sq 
    start with 1 increment by 1 maxvalue 99999999999 nocycle cache 100;





--********************************************
-- END PRE-DEPLOY SECTION, START OF DEPLOY
--********************************************



















----
---- END Fix 4.2 Release Script
----
