------------------------------------------------------------------------------------
-- Begin requested by Meka, Srivathsala --------------------------------------------
-- AP-1                           -------------------------------------------------
------------------------------------------------------------------------------------

-- Map the ProFlowers company to new member code
update FTD_APPS.COMPANY_MASTER 
  set SENDING_FLORIST_NUMBER = '90-8400' 
where company_name = 'ProFlowers';

--Insert new member codes into mercury OPS for inbound processing. Do not remove OLD member code, this allows the orders/messages during transition to be succesfully processed.
INSERT  INTO MERCURY.MERCURY_EAPI_OPS (MAIN_MEMBER_CODE,  IS_AVAILABLE,  IN_USE,  CREATED_BY,  CREATED_ON,  UPDATED_BY,  UPDATED_ON) 
VALUES ('90-8400AG',  'Y',  'N',  'AP-1',  sysdate,  'AP-1',  sysdate);
INSERT  INTO MERCURY.MERCURY_EAPI_OPS (MAIN_MEMBER_CODE,  IS_AVAILABLE,  IN_USE,  CREATED_BY,  CREATED_ON,  UPDATED_BY,  UPDATED_ON) 
VALUES ('90-8400AH',  'Y',  'N',  'AP-1',  sysdate,  'AP-1',  sysdate);
INSERT  INTO MERCURY.MERCURY_EAPI_OPS (MAIN_MEMBER_CODE,  IS_AVAILABLE,  IN_USE,  CREATED_BY,  CREATED_ON,  UPDATED_BY,  UPDATED_ON) 
VALUES ('90-8400AI',  'Y',  'N',  'AP-1',  sysdate,  'AP-1',  sysdate);

-- Insert new suffix mapping for outbound processing.
insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MERCURY_INTERFACE_CONFIG', 'EAPI_OUTBOUND_ID_LIST_90-8400_DEFAULT', 'AA AB AC', 'AP-1', sysdate, 'AP-1', sysdate, 'List of available suffixes for FTD.com floral messaging, separated by a space.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MERCURY_INTERFACE_CONFIG', 'EAPI_OUTBOUND_ID_LIST_90-8400_ProFlowers', 'AG AH AI', 'AP-1', sysdate, 'AP-1', sysdate, 'List of available suffixes for ProFlowers floral messaging, separated by a space.');

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values ('MERCURY_INTERFACE_CONFIG', 'EAPI_OUTBOUND_ID_LIST_90-8500_FTDCA', 'AA AB', 'AP-1', sysdate, 'AP-1', sysdate, 'List of available suffixes for FTD.CA floral messaging, separated by a space.');

-- No updates to the parms like DELIVERY_CONFIRMATION_SUFFIX_LIST%. DCON now refers to MERCURY_EAPI_OPS.

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AG_USER_NAME', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'User Id for 90-8400AG', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AG_PASSWORD', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'Password for 90-8400AG', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AH_USER_NAME', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'User Id for 90-8400AH', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AH_PASSWORD', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'Password for 90-8400AH', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AI_USER_NAME', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'User Id for 90-8400AI', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

INSERT INTO FRP.SECURE_CONFIG
(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES
('MERCURY_EAPI', '90-8400AI_PASSWORD', global.encryption.encrypt_it('REPLACEME','APOLLO_PROD_2018'), 'Password for 90-8400AI', sysdate, 'AP-1', sysdate, 'AP-1', 'APOLLO_PROD_2018');

------------------------------------------------------------------------------------
-- End   requested by Meka, Srivathsala---------------------------------------------
-- AP-1                           -------------------------------------------------
------------------------------------------------------------------------------------

