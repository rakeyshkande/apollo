set define off
--------------------------------
-- Fix_3_6_Release_Script.sql --
--------------------------------

--********************************************
-- PRE-DEPLOY SECTION
--********************************************


--
-- Defect 5577
-- 

create table ftd_apps.product_index_country_xref (
product_index_country_xref_id varchar2(20) not null,
index_country_id varchar2(2) not null,
country_id varchar2(2) not null,
created_on date not null,
created_by varchar2(100) not null,
updated_on date not null,
updated_by varchar2(100) not null) tablespace ftd_apps_data;

alter table ftd_apps.product_index_country_xref add constraint product_index_country_xref_pk primary key
(product_index_country_xref_id) using index tablespace ftd_apps_indx;

comment on column ftd_apps.product_index.country_id is
'country_id from Novator, use ftd_apps.product_index_country_xref to map to country_master';

comment on table ftd_apps.product_index_country_xref is 'Mapping between product_index.country_id and country_master.country_id';

grant select on ftd_apps.product_index_country_xref to pas;


--
-- Defect 5851
--
create table MERCURY.AUTO_DCON_KEY_PHRASE (
DCON_KEY_PHRASE_ID Number not null,
KEY_PHRASE_TXT Varchar2(4000) not null,
INCLUDE_EXCLUDE_FLAG char(1) not null,
CREATED_ON date not null,
CREATED_BY varchar2(100) not null,
UPDATED_ON date not null,
UPDATED_BY varchar2(100) not null) tablespace mercury_data;

comment on table mercury.auto_dcon_key_phrase is
'text values used to determine if an inbound ASK/ANS message is a delivery confirmation';

alter table mercury.auto_dcon_key_phrase add constraint auto_dcon_key_phrase_pk primary key (dcon_key_phrase_id)
using index tablespace mercury_indx;

alter table mercury.auto_dcon_key_phrase add constraint auto_dcon_key_phrase_ck1 check (include_exclude_flag in ('I','E'));

comment on column mercury.auto_dcon_key_phrase.include_exclude_flag is
'I means to include the message as a delivery confirmation and E means to exclude the message';

insert into mercury.auto_dcon_key_phrase 
    (dcon_key_phrase_id, key_phrase_txt, include_exclude_flag, created_on, created_by, updated_on, updated_by)
values (1, 'ACTUAL DELIVERY', 'I', sysdate, 'Defect_5851', sysdate, 'Defect_5851');

insert into mercury.auto_dcon_key_phrase
    (dcon_key_phrase_id, key_phrase_txt, include_exclude_flag, created_on, created_by, updated_on, updated_by)
values (2, ' NOT ', 'E', sysdate, 'Defect_5851', sysdate, 'Defect_5851');

grant select on mercury.auto_dcon_key_phrase to clean;


create table mercury.LIFECYCLE_STATUS 
     (mercury_id         varchar2(20)  NOT NULL,
      order_status_code  varchar2(100) NOT NULL,
      status_timestamp   date          NOT NULL,
      status_txt         varchar2(4000),
      created_on         date not null,
      created_by         varchar2(100) not null,
      updated_on         date not null,
      updated_by         varchar2(100) not null,
      constraint lifecycle_status_pk primary key (mercury_id, order_status_code) 
                 using index tablespace mercury_indx,
      constraint lifecycle_status_fk1 foreign key(mercury_id) references mercury.mercury(mercury_id)
     ) tablespace mercury_data;


comment on table mercury.lifecycle_status is 'Status updates received from MNAPI system';
comment on column mercury.lifecycle_status.mercury_id is 'Mercury_ID of order with the status update';
comment on column mercury.lifecycle_status.order_status_code is 'status of order';
comment on column mercury.lifecycle_status.status_timestamp is 'date/time of this status';
comment on column mercury.lifecycle_status.status_txt is 'description of status from MNAPI';



create table clean.complaint_comm_type 
     (complaint_comm_type_id       varchar2(20)  NOT NULL,
      description                  varchar2(4000) NOT NULL,
      created_on         date not null,
      created_by         varchar2(100) not null,
      updated_on         date not null,
      updated_by         varchar2(100) not null,
      constraint complaint_comm_type_pk primary key (complaint_comm_type_id) 
                 using index tablespace clean_indx
     ) tablespace clean_data;


comment on table clean.complaint_comm_type is 'Communication types and descriptions';
comment on column clean.complaint_comm_type.complaint_comm_type_id is 'ID of the complaint type';
comment on column clean.complaint_comm_type.description is 'Description of the complaint type';


create table clean.stock_email_type 
   (
          stock_email_type_id     varchar2(20)       NOT NULL,    
          description             varchar2(400)      NOT NULL,    
          created_by              varchar2(100)      NOT NULL,
          created_on              date               NOT NULL,
          updated_by              varchar2(100)      NOT NULL,
          updated_on              date               NOT NULL,
    CONSTRAINT stock_email_type_pk PRIMARY KEY(stock_email_type_id) 
            USING INDEX  TABLESPACE clean_INDX
   )
 TABLESPACE clean_data;

--********************************************
-- END PRE-DEPLOY SECTION, START OF DEPLOY
--********************************************



--
-- Defect 5843
--

alter table ftd_apps.snh add
   (vendor_charge          number(5,2),
    vendor_sat_upcharge    number(5,2));

alter table ftd_apps.snh$ add
   (vendor_charge          number(5,2),
    vendor_sat_upcharge    number(5,2));

REM At this point deploy the trigger ftd_apps.snh_$

update ftd_apps.snh
set vendor_charge = first_order_domestic,
    vendor_sat_upcharge = (select freshcuts_sat_charge from ftd_apps.global_parms);

alter table ftd_apps.snh modify
   (vendor_charge           NOT NULL,
    vendor_sat_upcharge     NOT NULL);

create table ftd_apps.snh_hist (
 SNH_ID                                    VARCHAR2(5) NOT NULL ,
 FIRST_ORDER_DOMESTIC                      NUMBER(5,2) NOT NULL ,
 FIRST_ORDER_INTERNATIONAL                 NUMBER(5,2) NOT NULL ,
 UPDATED_ON                                DATE NOT NULL ,
 VENDOR_CHARGE                             NUMBER(5,2) NOT NULL ,
 VENDOR_SAT_UPCHARGE                       NUMBER(5,2) NOT NULL ,
 expired_on                                date) tablespace ftd_apps_data;

insert into ftd_apps.snh_hist
select snh_id,
       first_order_domestic,
       first_order_international,
       to_date('19700101','yyyymmdd'),
       vendor_charge,
       vendor_sat_upcharge,
       null
from   ftd_apps.snh;

REM At this point deploy the trigger ftd_apps.snh_hist_trg

create index ftd_apps.snh_hist_n1 on ftd_apps.snh_hist(snh_id,updated_on) 
   tablespace ftd_apps_indx;

alter table ftd_apps.service_fee_override add
   (vendor_charge          number(5,2),
    vendor_sat_upcharge    number(5,2));

alter table ftd_apps.service_fee_override$ add
   (vendor_charge          number(5,2),
    vendor_sat_upcharge    number(5,2));

REM At this point deploy the ftd_apps.service_fee_override_$ trigger.

update ftd_apps.service_fee_override
set vendor_charge = domestic_charge,
    vendor_sat_upcharge = (select freshcuts_sat_charge from ftd_apps.global_parms);

alter table ftd_apps.service_fee_override modify
   (vendor_charge           NOT NULL,
    vendor_sat_upcharge     NOT NULL);

create table ftd_apps.service_fee_override_hist (
 SNH_ID                                    VARCHAR2(5) not null,
 OVERRIDE_DATE                             DATE not null,
 DOMESTIC_CHARGE                           NUMBER(5,2) not null,
 INTERNATIONAL_CHARGE                      NUMBER(5,2) not null,
 UPDATED_ON                                DATE not null,
 VENDOR_CHARGE                             NUMBER(5,2) not null,
 VENDOR_SAT_UPCHARGE                       NUMBER(5,2) not null,
 expired_on                                date) tablespace ftd_apps_data;

insert into ftd_apps.service_fee_override_hist
select snh_id, override_date, domestic_charge, international_charge, updated_on, vendor_charge, vendor_sat_upcharge, null
from   ftd_apps.service_fee_override;

REM At this point deploy the trigger ftd_apps.service_fee_override_hist_trg

create index ftd_apps.service_fee_override_hist_n1 on ftd_apps.service_fee_override_hist (snh_id,updated_on) 
   tablespace ftd_apps_indx;

create table ftd_apps.source_master_hist (
source_code varchar2(10) not null,
snh_id      varchar2(5) not null,
updated_on  date not null,
expired_on  date) tablespace ftd_apps_data;

insert into ftd_apps.source_master_hist
select source_code, snh_id, updated_on, null
from   ftd_apps.source_master;

REM At this point deploy the trigger ftd_apps.source_master_hist_trg

create index ftd_apps.source_master_hist_n1 on ftd_apps.source_master_hist(source_code,updated_on) 
   tablespace ftd_apps_indx;

alter table scrub.order_details modify (ship_method varchar2(4000));

alter table scrub.order_details add (orig_source_code varchar2(4000), orig_product_id varchar2(4000),
orig_ship_method varchar2(4000), orig_delivery_date varchar2(4000), orig_total_fee_amount varchar2(4000));

------------------------------------
-- END 5843
------------------------------------


 grant select on ftd_apps.billing_info to global;
 grant select on ftd_apps.SOURCE_MASTER_hist to global;
 grant select on ftd_apps.source to global;
 grant select on ftd_apps.source_master to global;
grant select on ftd_apps.snh_hist to global; 
grant select on ftd_apps.service_fee_override_hist to global; 



--
-- Defect 5851
-- 

grant references on ftd_apps.partner_master to clean;
grant select on ftd_apps.partner_master to joe;
 
alter table clean.stock_email_company_ref add
    (partner_name         varchar2(50),
     constraint stock_email_company_ref_fk2 foreign key(partner_name) references ftd_apps.partner_master(partner_name)
    );

create index clean.stock_email_company_ref_n2 on clean.stock_email_company_ref(partner_name) tablespace clean_indx;


alter table clean.stock_letter_company_ref add
    (partner_name         varchar2(50),
     constraint stock_letter_company_ref_fk2 foreign key(partner_name) references ftd_apps.partner_master(partner_name)
    );

create index clean.stock_letter_company_ref_n2 on clean.stock_letter_company_ref(partner_name) tablespace clean_indx;


alter table clean.stock_email add
    (stock_email_type_id             varchar2(20),
     constraint stock_email_fk1 foreign key(stock_email_type_id) references clean.stock_email_type(stock_email_type_id)
    );

create index clean.stock_email_n1 on clean.stock_email(stock_email_type_id) tablespace clean_indx;



alter table ftd_apps.stock_messages add
    (company_id              varchar2(12),    
     origin_id               varchar2(10),    
     partner_name            varchar2(50),    
     constraint stock_messages_fk2 foreign key(origin_id) 
                references ftd_apps.origins(origin_id),
     constraint stock_messages_fk3 foreign key(partner_name) 
                references ftd_apps.partner_master(partner_name)
    );

update ftd_apps.stock_messages   set company_id = 'ALL';

alter table ftd_apps.stock_messages modify (company_id not null);

create index ftd_apps.stock_messages_n2 on ftd_apps.stock_messages(origin_id) tablespace ftd_apps_indx;
create index ftd_apps.stock_messages_n3 on ftd_apps.stock_messages(partner_name) tablespace ftd_apps_indx;


--
-- 5851 - USAA
--

alter table clean.order_details add (delivery_confirmation_status varchar2(25));

alter table clean.order_details add constraint order_details_ck4 check (delivery_confirmation_status in 
(null,'Pending','Sent','Queued','Confirmed'));

create index clean.order_details_n15 on clean.order_details(delivery_confirmation_status) tablespace clean_indx online;

insert into aas.resources (resource_id, context_id, description, created_on, updated_on, updated_by) values 
('USAA','Order Proc','Controls access to view/update USAA order and customer information', sysdate, sysdate, 'Defect_5851');

insert into AAS.ACL (ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY, ACL_LEVEL) 
values ('USAA CSR', sysdate, sysdate, 'Defect_5851', null);

insert into aas.rel_acl_rp (acl_name, resource_id, context_id, permission_id, created_on, updated_on, updated_by) 
values ('USAA CSR','USAA','Order Proc','View',sysdate,sysdate,'Defect_5851');

insert into AAS.ROLE (ROLE_ID, ROLE_NAME, CONTEXT_ID, DESCRIPTION, CREATED_ON, UPDATED_ON, UPDATED_BY) values 
(200084, 'USAA CSR' , 'Order Proc', 'USAA CSR View', sysdate, sysdate, 'Defect_5851' );

insert into AAS.REL_ROLE_ACL (ROLE_ID, ACL_NAME, CREATED_ON, UPDATED_ON, UPDATED_BY) values 
(200084, 'USAA CSR', sysdate, sysdate, 'Defect_5851');

alter table ftd_apps.PARTNER_MASTER add (PREFERRED_PROCESSING_RESOURCE Varchar2(100));

insert into ftd_apps.partner_master
(partner_name, created_on, created_by, updated_on, updated_by, preferred_processing_resource)
values ('USAA', sysdate, 'Defect_5851', sysdate, 'Defect_5851', 'USAA' );

alter table CLEAN.POINT_OF_CONTACT add (SOURCE_CODE Varchar2(10));

grant references on ftd_apps.source_master to clean;

alter table clean.point_of_contact add constraint point_of_contact_source_fk foreign key (source_code)
references FTD_APPS.SOURCE_MASTER (source_code);

create index clean.point_of_contact_n5 on clean.point_of_contact(source_code) tablespace clean_indx online;

alter trigger ftd_apps.SOURCE_MASTER_HIST_TRG disable;
alter trigger ftd_apps.DEFAULT_PROGRAM_SOURCE disable;
alter trigger ftd_apps.SOURCE_MASTER_$ disable;

alter table FTD_APPS.SOURCE_MASTER add (ADD_ON_FREE_ID Varchar2(100),
                                        same_day_freshcut_flag varchar2(1) default 'Y' not null);
alter table FTD_APPS.SOURCE_MASTER$ add (ADD_ON_FREE_ID Varchar2(100),
                                        same_day_freshcut_flag varchar2(1));

alter trigger ftd_apps.SOURCE_MASTER_HIST_TRG enable;
alter trigger ftd_apps.DEFAULT_PROGRAM_SOURCE enable;
alter trigger ftd_apps.SOURCE_MASTER_$ enable;

alter table ftd_apps.source_master add constraint source_master_addon_fk foreign key (add_on_free_id)
references FTD_APPS.ADDON(ADDON_ID);

alter table ftd_apps.source_master add constraint source_master_ck10 check (same_day_freshcut_flag in ('Y','N'));

alter table FTD_APPS.PRODUCT_MASTER add (ADD_ON_FREE_ID Varchar2(100));

alter table FTD_APPS.PRODUCT_MASTER$ add (ADD_ON_FREE_ID Varchar2(100));

alter table ftd_apps.product_master add constraint product_master_addon_fk foreign key (add_on_free_id)
references FTD_APPS.ADDON(ADDON_ID);

alter table FTD_APPS.ADDON add (VENDOR_SKU_TXT Varchar2(100), ARGO_DISPLAY_TXT Varchar2(4000));

comment on column ftd_apps.addon.vendor_sku_txt is 'Text to be appended to the Vendor Sku in Argo';

comment on column ftd_apps.addon.argo_display_txt is 'Displays in the new NOTE section of the Argo label';


insert into FTD_APPS.ADDON_TYPE (addon_type_id, description) values (6, 'Complimentary');

insert into FTD_APPS.ADDON (addon_id, addon_type, description, price, addon_text, unspsc, vendor_sku_txt, argo_display_txt)
values ('FC', '6', 'Chocolates', 0.00, 'This item includes a complimentary box of chocolates.', 'CHOC', 'FREE CHOCOLATES',
'FREE CHOCOLATES');



 grant select on FTD_APPS.ADDON_type to global;
 

alter table FTD_APPS.partner_MASTER add (preferred_partner_flag Varchar2(1) default 'N' not null,
                                         constraint partner_master_ck1 check (preferred_partner_flag in ('Y','N'))
                                        );



insert into clean.complaint_comm_type (complaint_comm_type_id,description,created_on,created_by,updated_on,updated_by)
values('NC','Not a complaint',sysdate,'Defect 5851',sysdate,'Defect 5851');

insert into clean.complaint_comm_type (complaint_comm_type_id,description,created_on,created_by,updated_on,updated_by)
values('Email','Email',sysdate,'Defect 5851',sysdate,'Defect 5851');

insert into clean.complaint_comm_type (complaint_comm_type_id,description,created_on,created_by,updated_on,updated_by)
values('Phone','Phone',sysdate,'Defect 5851',sysdate,'Defect 5851');

insert into clean.complaint_comm_type (complaint_comm_type_id,description,created_on,created_by,updated_on,updated_by)
values('Fax','Fax',sysdate,'Defect 5851',sysdate,'Defect 5851');

insert into clean.complaint_comm_type (complaint_comm_type_id,description,created_on,created_by,updated_on,updated_by)
values('Mail','Mail',sysdate,'Defect 5851',sysdate,'Defect 5851');

alter table clean.refund add
     (origin_complaint_comm_type_id       varchar2(20) default 'NC', 
      notif_complaint_comm_type_id        varchar2(20) default 'NC',
      constraint refund_fk2 foreign key(origin_complaint_comm_type_id) references clean.complaint_comm_type(complaint_comm_type_id),
      constraint refund_fk3 foreign key(notif_complaint_comm_type_id)  references clean.complaint_comm_type(complaint_comm_type_id)
     );

alter table clean.refund$ add
     (origin_complaint_comm_type_id       varchar2(20), 
      notif_complaint_comm_type_id        varchar2(20)     
     );


alter table clean.refund_disposition_val add 
     (complaint_comm_type_req_flag   varchar2(1) default 'N' NOT NULL,
      constraint refund_disposition_val_ck7 check (complaint_comm_type_req_flag in ('Y','N'))
     );


update clean.refund_disposition_val
set complaint_comm_type_req_flag = 'Y'
where refund_disp_code in ('B10','B20','B30','B40','B50','B60');

alter table mercury.mercury add
     (origin_complaint_comm_type_id       varchar2(20), 
      notif_complaint_comm_type_id        varchar2(20)     
     );

alter table venus.venus add
     (origin_complaint_comm_type_id       varchar2(20), 
      notif_complaint_comm_type_id        varchar2(20)     
     );


grant select on clean.order_details to mercury;

alter table scrub.dashboard_query_counts_temp add 
      (order_detail_id           number(22), 
       preferred_partner_name    varchar2(50)
      );

alter table ftd_apps.partner_master add (reply_email_address varchar2(400));

grant select on ftd_apps.partner_master to global;

alter table ftd_apps.partner_master add (default_source_code varchar2(10));

alter table ftd_apps.partner_master add constraint partner_master_source_fk foreign key (default_source_code)
references ftd_apps.source_master(source_code);

insert into FTD_APPS.CONTENT_FILTER (CONTENT_FILTER_ID, CONTENT_FILTER_NAME, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_filter_sq.nextval, 'PREFERRED_PARTNER_RESOURCE', 'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
     CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PREFERRED_PARTNER', 'ORDER_ACCESS_RESTRICTION', 
    'Popup message when CSR (without proper resource) attempts to access Preferred Partner order',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), 
    null, 'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, 
     UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'ORDER_ACCESS_RESTRICTION'), 
      'USAA',  null, 
      'Stop: This is a USAA order.  Please advise the customer: <br/><br/> "I apologize, I need to transfer your call to an agent who can better assist you.  Can you please hold for one moment while I transfer you?  Thank you." <br/><br/> Transfer the call to 36716', 
     'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
     CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'PREFERRED_PARTNER', 'SOURCE_CODE_RESTRICTION', 
    'Popup message when CSR (without proper resource) attempts to change to a Preferred Partner source code',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), 
    null, 'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, 
     UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
      WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'SOURCE_CODE_RESTRICTION'), 
      'USAA',  null, 
      'Stop: You are attempting to change an order to a USAA source code.  Please advise the customer: "I apologize, but I am unable to make this change to your order.  In order to process your request, I will cancel this order and ask that you replace the order online through the USAA site."  Remove the order or transfer the call to a USAA trained agent at 36716 if the customer needs further assistance.', 
     'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
     CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'SAME_DAY_RESTRICTION', 'SAME_DAY_GIFT_NOT_ALLOWED',  
    'Popup message when CSR attempts to change order item to Same-day gift product but source code does not allow it',
    null, null, 'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, 
     UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 
      'SAME_DAY_RESTRICTION' AND CONTENT_NAME = 'SAME_DAY_GIFT_NOT_ALLOWED'), 
      null, null, 'Same-day gifts are not offered for this source code. Please choose another product.',
     'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
     CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_master_sq.nextval, 'SAME_DAY_RESTRICTION', 'SAME_DAY_FRESHCUT_NOT_ALLOWED',  
    'Popup message when CSR attempts to change order item to Same-day fresh-cut product but source code does not allow it',
    null, null, 'SYS', sysdate, 'SYS', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, 
     UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
values (ftd_apps.content_detail_sq.nextval, 
     (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 
      'SAME_DAY_RESTRICTION' AND CONTENT_NAME = 'SAME_DAY_FRESHCUT_NOT_ALLOWED'), 
      null, null,  
      'Same-day fresh-cut products are not offered for this source code.  Please choose another product.',
     'SYS', sysdate, 'SYS', sysdate);

grant select on ftd_apps.source_program_ref to scrub;
grant select on ftd_apps.partner_master to scrub;

update ftd_apps.stock_messages 
   set origin_id = 'WLMTI'
where stock_message_id in ('WLMTI_PEND','WLMTI_NOFF','WLMTI_MRPH','WLMTI_MRI','WLMTI_MDDR','WLMTI_MDDD','WLMTI_MDAY','WLMTI_SCN',
'WLMTI_SR','WLMTI_VALD','WLMTI_BLNK','WLMTI_CANF','WLMTI_CLP','WLMTI_DUPY','WLMTI_FSND','WLMTI_ICU','WLMTI_MBL','WLMTI_MBR',
'WLMTI_MCH');

insert into clean.message_tokens values ('usaa.phone','ALL','text','1-800-769-7673',null);

update ftd_apps.partner_master set reply_email_address = 'ussacustserv@ftdi.com' where partner_name = 'USAA';

insert into ftd_apps.product_index_country_xref values ('AEAE','AE','AE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ARAR','AR','AR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ATAT','AT','AT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('AUAU','AU','AU',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('BEBE','BE','BE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('BOBO','BO','BO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('BRBR','BR','BR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('BYBY','BY','BY',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('BZBZ','BZ','BZ',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CHCH','CH','CH',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CLCL','CL','CL',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CNCN','CN','CN',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('COCO','CO','CO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CRCR','CR','CR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CYCY','CY','CY',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('CZCZ','CZ','CZ',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('DEDE','DE','DE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('DKDK','DK','DK',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('DODO','DO','DO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ECEC','EC','EC',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('EGEG','EG','EG',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ESES','ES','ES',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('FIFI','FI','FI',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('FRFR','FR','FR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('GRGR','GR','GR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('GTGT','GT','GT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('HKHK','HK','HK',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('HNHN','HN','HN',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('HRHR','HR','HR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('HUHU','HU','HU',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('IDID','ID','ID',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('IEIE','IE','IE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ILIL','IL','IL',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ININ','IN','IN',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ITIT','IT','IT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('JPJP','JP','JP',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('KHKH','KH','KH',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('KRKR','KR','KR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('KWKW','KW','KW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('LBLB','LB','LB',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('LULU','LU','LU',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('MTMT','MT','MT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('MXMX','MX','MX',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('MYMY','MY','MY',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('NINI','NI','NI',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('NLNL','NL','NL',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('NONO','NO','NO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('NZNZ','NZ','NZ',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PAPA','PA','PA',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PEPE','PE','PE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PKPK','PK','PK',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PLPL','PL','PL',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PTPT','PT','PT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('PYPY','PY','PY',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('RORO','RO','RO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('RURU','RU','RU',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SASA','SA','SA',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SESE','SE','SE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SGSG','SG','SG',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SISI','SI','SI',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SKSK','SK','SK',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('SVSV','SV','SV',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('THTH','TH','TH',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('TRTR','TR','TR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('UAUA','UA','UA',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('UKUK','UK','UK',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('UYUY','UY','UY',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('VEVE','VE','VE',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('VNVN','VN','VN',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAAO','ZA','AO',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAMW','ZA','MW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZANA','ZA','NA',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZASZ','ZA','SZ',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAZW','ZA','ZW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAZM','ZA','ZM',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAMZ','ZA','MZ',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZABW','ZA','BW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('ZAXA','ZA','XA',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('02GU','02','GU',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('02PW','02','PW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('02PH','02','PH',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('02TW','02','TW',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('02KR','02','KR',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('01BS','01','BS',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('01BM','01','BM',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');
insert into ftd_apps.product_index_country_xref values ('01HT','01','HT',sysdate,'DEFECT_5851',sysdate,'DEFECT_5851');

----
---- PRODUCTION ONLY
----

begin

insert into quartz_schema.pipeline (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload,
    force_default_payload, queue_name, class_code, active_flag)
    values ('Delivery Confirmation ASK', 'OP', 'Send Delivery Confirmation ASK messages',
    'This process sends Delivery Confirmation ASK messages to florists.', '0 0 * * * ?', 'DCON-ASK', 'Y', 'OJMS.DCON_ASK',
    'SimpleJmsJob', 'Y');

insert into quartz_schema.pipeline (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload,
    force_default_payload, queue_name, class_code, active_flag)
    values ('Delivery Confirmation DC Queue', 'OP', 'Add orders to DC queue',
    'This process will add orders to the Delivery Confirmation queue.', '0 0 * * * ?', 'DCON-QUEUE', 'Y', 'OJMS.DCON_QUEUE',
    'SimpleJmsJob', 'Y');

insert into quartz_schema.pipeline
    (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload, force_default_payload, queue_name,
    class_code, active_flag)
    values ('Order Lifecycle Status Pull', 'OP', 'Retrieve Order Lifecycle Status from MNAPI',
    'This process will retrieve all unconfirmed MNAPI order status updates.', '900', 'DCON-LIFECYCLE', 'Y',
    'OJMS.DCON_LIFECYCLE', 'SimpleJmsJob', 'Y');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_ASK_DELAY', '16',
    'The number of hours past 12:00am on the delivery date to send a Delivery Confirmation ASK message.', 
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_QUEUE_DELAY', '16',
    'The number of hours past 12:00am on the day after the delivery date to send an order to the DC Queue.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ORDER_LIFECYCLE', 'PERFORM_ORDER_LIFECYCLE_UPDATES', 'Y',
    'Flag to control if Apollo checks for individual MNAPI Order Lifecyle updates.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ORDER_LIFECYCLE', 'GET_ALL_STATUS_UPDATES', 'Y',
    'Flag to control if Apollo retrieves all status updates for an order or just the unconfirmed updates.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'USAA_DCON_EMAIL_TITLE', 'DELIVERY CONFIRMATION',
    'The title of the Delivery Confirmation Stock Email for USAA.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_EMAIL_TITLE', 'DCONSYS',
    'The title of the default Delivery Confirmation Stock Email.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

update joe.element_config set value_max_bytes_qty = 10 where element_id = 'callDataSourceCode';

insert into joe.oe_script_master (script_id, script_type_code, script_txt, created_by, created_on, updated_by, updated_on)
    values ('membershipId', 'Defeault', 'May I please have your Membership Info?', 'Defect_5851', sysdate, 'Defect_5851', sysdate);

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6714, 'FTD.COM/USAA', 'FTDP', 'A', 'Y', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6715, 'FTD.COM/USAA', 'FTDP', 'A', 'N', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6716, 'FTD.COM/USAA', 'FTDP', 'A', 'N', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into clean.queue_type_val (QUEUE_TYPE, DESCRIPTION, QUEUE_INDICATOR, SORT_ORDER, PRIORITY)
    values ('DC', 'DC', 'Mercury', 53, null);

insert into FRP.GLOBAL_PARMS 
    (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
    VALUES 
    ('ORDER_LIFECYCLE', 'WS_URL', 'http://bigip-mercurydirect.ftdi.com/ws/orderstatus.asmx',
    'defect 5851 A', sysdate, 'defect 5851 A', sysdate, 'URL to the Mercury Direct order lifecycle web site.');


INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_ACCOUNT_ID', global.encryption.encrypt_it('27', 'APOLLO_PROD_2008'),
    'Account ID for the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_PROD_2008');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_USER_ID', global.encryption.encrypt_it('c0mstatus', 'APOLLO_PROD_2008'),
    'Login to the the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_PROD_2008');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_PASSWORD_ID', global.encryption.encrypt_it('EUG5J9', 'APOLLO_PROD_2008'),
    'Password to the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_PROD_2008');

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
    values (ftd_apps.content_master_sq.nextval, 'PREFERRED_PARTNER', 'TRANSFER_EXTENSION', 
    'Extension to transfer preferred partner calls',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), 
    null, 'Defect_5851', sysdate, 'Defect_5851', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY,
 UPDATED_ON, CREATED_BY, CREATED_ON) 
    values (ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'TRANSFER_EXTENSION'), 
    'USAA',  null, '36716', 'Defect_5851', sysdate, 'Defect_5851', sysdate);

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA MAIL SERVER', 'ftd-mailer1.ftdi.com',
    'USAA mail server.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA EXCLUDE DOMAINS', 'amazon.com, walmart.com',
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA DEFAULT PARTNER NAME', 'USAA',
    'Partner associated with the mailbox.  The default partner source code will be used when populating the CLEAN.POINT_OF_CONTACT.SOURCE_CODE field for unattached emails.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

--SECURE CONFIG
INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('email_request_processing', 'USAA_mailbox_monitor_USERNAME',
global.encryption.encrypt_it('usaacustserv','APOLLO_PROD_2008'),
    'USAA mailbox user name.', sysdate, 'Defect_5851', sysdate, 'Defect_5851','APOLLO_PROD_2008');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('email_request_processing', 'USAA_mailbox_monitor_PASSWORD', 'change me',
    'USAA mailbox user name.', sysdate, 'Defect_5851', sysdate, 'Defect_5851','APOLLO_PROD_2008');

end;
/

----
---- TEST ONLY
----

begin

insert into quartz_schema.pipeline (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload,
    force_default_payload, queue_name, class_code, active_flag)
    values ('Delivery Confirmation ASK', 'OP', 'Send Delivery Confirmation ASK messages',
    'This process sends Delivery Confirmation ASK messages to florists.', '0 0 * * * ?', 'DCON-ASK', 'Y', 'OJMS.DCON_ASK',
    'SimpleJmsJob', 'Y');

insert into quartz_schema.pipeline (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload,
    force_default_payload, queue_name, class_code, active_flag)
    values ('Delivery Confirmation DC Queue', 'OP', 'Add orders to DC queue',
    'This process will add orders to the Delivery Confirmation queue.',
    '0 0 * * * ?', 'DCON-QUEUE', 'Y', 'OJMS.DCON_QUEUE', 'SimpleJmsJob', 'Y');

insert into quartz_schema.pipeline (pipeline_id, group_id, description, tool_tip, default_schedule, default_payload,
    force_default_payload, queue_name, class_code, active_flag)
    values ('Order Lifecycle Status Pull', 'OP', 'Retrieve Order Lifecycle Status from MNAPI',
    'This process will retrieve all unconfirmed MNAPI order status updates.',
    '900', 'DCON-LIFECYCLE', 'Y', 'OJMS.DCON_LIFECYCLE', 'SimpleJmsJob', 'Y');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_ASK_DELAY', '16',
    'The number of hours past 12:00am on the delivery date to send a Delivery Confirmation ASK message.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_QUEUE_DELAY', '16',
    'The number of hours past 12:00am on the day after the delivery date to send an order to the DC Queue.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ORDER_LIFECYCLE', 'PERFORM_ORDER_LIFECYCLE_UPDATES', 'Y',
    'Flag to control if Apollo checks for individual MNAPI Order Lifecyle updates.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ORDER_LIFECYCLE', 'GET_ALL_STATUS_UPDATES', 'Y',
    'Flag to control if Apollo retrieves all status updates for an order or just the unconfirmed updates.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'USAA_DCON_EMAIL_TITLE', 'DELIVERY CONFIRMATION',
    'The title of the Delivery Confirmation Stock Email for USAA.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'PREFERRED_PARTNER_CONFIG', 'DCON_EMAIL_TITLE', 'DCONSYS',
    'The title of the default Delivery Confirmation Stock Email.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

update joe.element_config set value_max_bytes_qty = 10 where element_id = 'callDataSourceCode';

insert into joe.oe_script_master (script_id, script_type_code, script_txt, created_by, created_on, updated_by, updated_on)
    values ('membershipId', 'Defeault', 'May I please have your Membership Info?', 'Defect_5851', sysdate, 'Defect_5851', sysdate);

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6714, 'FTD.COM/USAA', 'FTDP', 'A', 'Y', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6715, 'FTD.COM/USAA', 'FTDP', 'A', 'N', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into ftd_apps.dnis (DNIS_ID, DESCRIPTION, ORIGIN, STATUS_FLAG, OE_FLAG, YELLOW_PAGES_FLAG, SCRIPT_CODE, DEFAULT_SOURCE_CODE,
 SCRIPT_ID, COMPANY_ID, SCRIPT_TYPE_CODE)
    values (6716, 'FTD.COM/USAA', 'FTDP', 'A', 'N', 'N', 'DG', 'USAA', 'FTD', 'FTD', 'Default');

insert into clean.queue_type_val (QUEUE_TYPE, DESCRIPTION, QUEUE_INDICATOR, SORT_ORDER, PRIORITY)
    values ('DC', 'DC', 'Mercury', 53, null);

insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
    VALUES ('ORDER_LIFECYCLE', 'WS_URL', 'http://md2test.mercurynetwork.com/ws/orderstatus.asmx',
    'defect 5851 A', sysdate, 'defect 5851 A', sysdate, 'URL to the Mercury Direct order lifecycle web site.');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_ACCOUNT_ID', global.encryption.encrypt_it('27', 'APOLLO_TEST_2008'),
    'Account ID for the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_TEST_2008');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_USER_ID', global.encryption.encrypt_it('c0mstatus', 'APOLLO_TEST_2008'),
    'Login to the the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_TEST_2008');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('ORDER_LIFECYCLE', 'MD_PASSWORD_ID', global.encryption.encrypt_it('passw0rd', 'APOLLO_TEST_2008'),
    'Password to the Mercury Direct order lifecycle web site.', sysdate, 'defect 5851 A', sysdate, 'defect 5851 A', 
'APOLLO_TEST_2008');

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, 
    CONTENT_FILTER_ID1, CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
    values (ftd_apps.content_master_sq.nextval, 'PREFERRED_PARTNER', 'TRANSFER_EXTENSION', 
    'Extension to transfer preferred partner calls',
    (select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), 
    null, 'Defect_5851', sysdate, 'Defect_5851', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY,
 UPDATED_ON, CREATED_BY, CREATED_ON) 
    values (ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'TRANSFER_EXTENSION'), 
    'USAA',  null, '36716', 'Defect_5851', sysdate, 'Defect_5851', sysdate);

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA MAIL SERVER', 'sodium.ftdi.com',
    'USAA mail server.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA EXCLUDE DOMAINS', 'amazon.com, walmart.com',
    'Do not send an auto response email if the following domain(s) is part of the sender email address.  Domains should be in the form of xxx.com and delimited by a comma.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

INSERT INTO FRP.GLOBAL_PARMS (context, name, value, description, created_on, created_by, updated_on, updated_by)
    values ( 'ins_novator_mailbox_monitor', 'USAA DEFAULT PARTNER NAME', 'USAA',
    'Partner associated with the mailbox.  The default partner source code will be used when populating the CLEAN.POINT_OF_CONTACT.SOURCE_CODE field for unattached emails.',
    sysdate, 'Defect_5851', sysdate, 'Defect_5851');

--SECURE CONFIG
INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('email_request_processing', 'USAA_mailbox_monitor_USERNAME',
global.encryption.encrypt_it('usaacsqa','ORCL_CCNUM_2006'), 'USAA mailbox user name.', sysdate, 'Defect_5851', sysdate, 
'Defect_5851','ORCL_CCNUM_2006');

INSERT INTO FRP.SECURE_CONFIG (CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
    VALUES ('email_request_processing', 'USAA_mailbox_monitor_PASSWORD',
global.encryption.encrypt_it('usaa@0731','ORCL_CCNUM_2006'), 'USAA mailbox user name.', sysdate, 'Defect_5851', sysdate, 
'Defect_5851','ORCL_CCNUM_2006');

end;
/

-----
----- END environment-specific values
-----

insert into clean.stock_email_type values ('Normal','Normal Email','Defect_5851',sysdate,'Defect_5851',sysdate);
insert into clean.stock_email_type values ('DCON','Delivery Confirmation','Defect_5851',sysdate,'Defect_5851',sysdate);

update clean.stock_email set stock_email_type_id = 'Normal';
update clean.stock_email set stock_email_type_id = 'DCON' where title in 
('DCONSYS','.REPLY DELIVERY CONFIRMATION','WDCON','di.after','DCON');


## USER NEEDS TO MANUALLY INSERT USAA INTO SOURCE_MASTER VIA GUI BEFORE THIS WILL WORK
update ftd_apps.partner_master set preferred_partner_flag='Y', default_source_code = 'USAA' where partner_name='USAA';

----
---- STOCK EMAILS
----
declare
 stock_email_id number;
 cursor stock_email_cursor is
  select *
  from clean.stock_email se
  where se.AUTO_RESPONSE_INDICATOR = 'Y'; begin
  FOR stock_email_rec in stock_email_cursor
  LOOP
   select clean.STOCK_EMAIL_ID_SQ.nextval into stock_email_id from dual;

   insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR, 
   USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
   VALUES(stock_email_id,stock_email_rec.TITLE || '.USAA','
****Replace with desired stock email text****
Online: ~usaa.email.link~
Phone: ~usaa.phone~
',stock_email_rec.SUBJECT,SYSDATE,'DEPLOY_3_6_0',SYSDATE,'DEPLOY_3_6_0','Y',
stock_email_rec.USE_NO_REPLY_FLAG,stock_email_rec.STOCK_EMAIL_TYPE_ID);

   --Insert the company reference
   insert into clean.stock_email_company_ref
   (STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)
   values (stock_email_id,'ALL',null,'USAA');
  
  

  END LOOP;


  --insert the delivery confirmation stock email for USAA
  select clean.STOCK_EMAIL_ID_SQ.nextval into stock_email_id from dual;
  insert into clean.stock_email (STOCK_EMAIL_ID, TITLE, BODY, SUBJECT, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, AUTO_RESPONSE_INDICATOR,
  USE_NO_REPLY_FLAG, STOCK_EMAIL_TYPE_ID)
  VALUES(stock_email_id, 'DELIVERY CONFIRMATION', 'Thank you for your recent purchase for ~recip.fname~ ~recip.lname~.

We are pleased to inform you that the delivery of your order has been completed.

Please feel free to contact us if we can be of any further assistance.

Sincerely,
~csr.fname~

Contact us:
Email us via this link:  ~usaa.email.link~
Phone:  ~usaa.phone~
','Confirmation of Delivery',SYSDATE,'DEPLOY_3_6_0',SYSDATE,'DEPLOY_3_6_0','N',
'N','DCON');

   --Insert the company reference
   insert into clean.stock_email_company_ref
   (STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID,PARTNER_NAME)
   values (stock_email_id,'ALL',null,'USAA');


end;
/

----
---- Corrections
----

update joe.oe_script_master set script_type_code = 'Default' where script_id = 'membershipId';

insert into clean.message_tokens
values ('usaa.email.link','ALL','text','http://www.usaa.com/flowers?FTDurl=http://www.ftd.com/19010/custserv/',null);

update FTD_APPS.ADDON set vendor_sku_txt = 'CHOC', argo_display_txt = 'FREE CHOCOLATES' where addon_id = 'FC';

insert into clean.comment_origin_val (comment_origin, description) values ('DC/Q', 'Delivery Confirmation Queue');

----
---- 6230
----

update rpt.report set name = 'Service Charge Edit Rpt',
description = 'This ad-hoc and automated monthly report provides insight into changes made to FTD.COM''s listing of service charge overrides over the specified time frame.  The report will highlight changes to existing service charges as well as a listing of service charges added or expired.',
    report_file_prefix = 'Service_Charge_Edit_Rpt',
    updated_on = sysdate,
    updated_by = 'Defect_6230'
    where report_id = 100605;

insert into FTD_APPS.CONTENT_DETAIL
(CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) 
    values
    (ftd_apps.content_detail_sq.nextval, 
    (select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
        WHERE CONTENT_CONTEXT = 'STOCK_EMAIL' AND CONTENT_NAME = 'TOKEN'), 
    'TEXT', 'USAA_EMAIL_LINK',
    '<a href=\"http://www.usaa.com/flowers?FTDurl=http://www.ftd.com/19010/custserv/\">www.usaa.com/flowers</a>',
    'Defect_6248', sysdate, 'Defect_6248', sysdate);



---- 6185

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('WEBLOYALTY','email_slogan_1','Click here to claim your Special Offer from our preferred partner for your next FTD order when you join their service. Terms and conditions apply.','alakhani',sysdate,'alakhani',sysdate, 'Text for the first Webloyalty link in the order confirmation email.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('WEBLOYALTY','email_slogan_2','Remember to click here to claim your Special Offer from our preferred partner for your next FTD order when you join their service. Terms and conditions apply.','alakhani',sysdate,'alakhani',sysdate, 'Text for the second Webloyalty link in the order confirmation email.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('WEBLOYALTY','url_parameter_1','p','alakhani',sysdate,'alakhani',sysdate, 'First parameter in the URL.');

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION)
VALUES('WEBLOYALTY','url_parameter_2','q','alakhani',sysdate,'alakhani',sysdate, 'Second parameter in the URL.');

UPDATE FRP.GLOBAL_PARMS
SET VALUE = 'Click here to claim your Special Offer from our preferred partner for your next FTD order when you join their service. Terms and conditions apply.'
WHERE CONTEXT = 'DELIVERY_CONFIRMATION_CONFIG'
AND  NAME = 'WEB_LOYALTY_EMAIL_SLOGAN'; 

insert into FTD_APPS.CONTENT_MASTER (CONTENT_MASTER_ID, CONTENT_CONTEXT, CONTENT_NAME, CONTENT_DESCRIPTION, CONTENT_FILTER_ID1,
CONTENT_FILTER_ID2, UPDATED_BY, UPDATED_ON, CREATED_BY, CREATED_ON) values (ftd_apps.content_master_sq.nextval,
'PREFERRED_PARTNER', 'PHONE', 'Partner phone number',(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER WHERE
CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), null, 'Defect_5851', sysdate, 'Defect_5851', sysdate);

insert into FTD_APPS.CONTENT_DETAIL (CONTENT_DETAIL_ID, CONTENT_MASTER_ID, FILTER_1_VALUE, FILTER_2_VALUE, CONTENT_TXT,
UPDATED_BY,UPDATED_ON, CREATED_BY, CREATED_ON) values (ftd_apps.content_detail_sq.nextval, (select CONTENT_MASTER_ID FROM
FTD_APPS.CONTENT_MASTER WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' AND CONTENT_NAME = 'PHONE'), 'USAA',  null, '1-800-769-7673',
'Defect_5851', sysdate, 'Defect_5851', sysdate);

--6285
update ftd_apps.partner_master set reply_email_address = 'usaacustserv@ftd.com' where partner_name = 'USAA';


-- 6284 

create table clean.accounting_transactions$ 
   (ACCOUNTING_TRANSACTION_ID              NUMBER,
    TRANSACTION_TYPE                       VARCHAR2(20),
    TRANSACTION_DATE                       DATE,
    ORDER_DETAIL_ID                        NUMBER,
    ADDITIONAL_ORDER_SEQ                   NUMBER(38),
    DELIVERY_DATE                          DATE,
    PRODUCT_ID                             VARCHAR2(20),
    SOURCE_CODE                            VARCHAR2(20),
    SHIP_METHOD                            VARCHAR2(20),
    PRODUCT_AMOUNT                         NUMBER(12,2),
    ADD_ON_AMOUNT                          NUMBER(12,2),
    SHIPPING_FEE                           NUMBER(12,2),
    SERVICE_FEE                            NUMBER(12,2),
    DISCOUNT_AMOUNT                        NUMBER(12,2),
    SHIPPING_TAX                           NUMBER(12,2),
    SERVICE_FEE_TAX                        NUMBER(12,2),
    TAX                                    NUMBER(12,2),
    PAYMENT_TYPE                           VARCHAR2(20),
    REFUND_DISP_CODE                       VARCHAR2(20),
    COMMISSION_AMOUNT                      NUMBER(12,2),
    WHOLESALE_AMOUNT                       NUMBER(12,2),
    ADMIN_FEE                              NUMBER(12,2),
    REFUND_ID                              NUMBER,
    ORDER_BILL_ID                          NUMBER,
    WHOLESALE_SERVICE_FEE                  NUMBER(12,2),
    OPERATION$                             VARCHAR2(7) NOT NULL,
    TIMESTAMP$                             TIMESTAMP   NOT NULL
   ) tablespace clean_data;

create index clean.accounting_transactions$_n1 on clean.accounting_transactions$(timestamp$) tablespace clean_indx;
grant select on clean.accounting_transactions$ to bi_etl;



######################
# FIX 3.6.1
######################
insert into FTD_APPS.CONTENT_MASTER (
CONTENT_MASTER_ID, 
CONTENT_CONTEXT, 
CONTENT_NAME, 
CONTENT_DESCRIPTION, 
                CONTENT_FILTER_ID1, 
CONTENT_FILTER_ID2, 
UPDATED_BY, 
UPDATED_ON, 
CREATED_BY, 
CREATED_ON) 
    values (
ftd_apps.content_master_sq.nextval, 
'PREFERRED_PARTNER', 
'DCON_SIGNATURE', 
'Signature to use for automated Delivery Confirmation Emails',
(select CONTENT_FILTER_ID FROM FTD_APPS.CONTENT_FILTER 
WHERE CONTENT_FILTER_NAME = 'PREFERRED_PARTNER_RESOURCE'), 
null, 
'Defect_6304', 
sysdate, 
'Defect_6304', 
sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
CONTENT_DETAIL_ID, 
CONTENT_MASTER_ID, 
FILTER_1_VALUE, 
FILTER_2_VALUE, 
CONTENT_TXT, 
UPDATED_BY, 
UPDATED_ON, 
CREATED_BY, 
CREATED_ON) 
    values (
ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' 
AND CONTENT_NAME = 'DCON_SIGNATURE'), 
'USAA', 
null, 
'FTD Customer Service', 
'Defect_6304', 
sysdate, 
'Defect_6304', 
sysdate);
 
insert into FTD_APPS.CONTENT_DETAIL (
CONTENT_DETAIL_ID, 
CONTENT_MASTER_ID, 
FILTER_1_VALUE, 
FILTER_2_VALUE, 
CONTENT_TXT, 
UPDATED_BY, 
UPDATED_ON, 
CREATED_BY, 
CREATED_ON) 
    values (
ftd_apps.content_detail_sq.nextval, 
(select CONTENT_MASTER_ID FROM FTD_APPS.CONTENT_MASTER 
WHERE CONTENT_CONTEXT = 'PREFERRED_PARTNER' 
AND CONTENT_NAME = 'DCON_SIGNATURE'), 
null, 
null, 
'FTD.COM Customer Service', 
'Defect_6304', 
sysdate, 
'Defect_6304', 
sysdate);

-
- Plus these procs...
-
ftd/database/clean_plsql/accounting_transactions_$.trg
ftd/database/customer_query_pkg.pkg
ftd/database/customer_query_pkg.pkb
ftd/database/pas_plsql/pas_query_pkg.pkb
ftd/database/venus_plsql/ship_pkg.pkb 




----
---- END Fix 3.5 Release Script
----
