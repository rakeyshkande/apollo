------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                3/1/2016  --------
-- defect QE-119        (Syed  ) (188865)           --------------------------------
------------------------------------------------------------------------------------

GRANT EXECUTE ON B2B.PENDINGMOVE_FUNC TO OSP;

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                3/1/2016  --------
-- defect QE-119        (Syed  ) (188865)           --------------------------------
------------------------------------------------------------------------------------




------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate chnage


alter table PAS.PAS_FLORIST_ZIPCODE_DT add  FIT_STATUS_CODE VARCHAR2(1);

update  PAS.PAS_FLORIST_ZIPCODE_DT set FIT_STATUS_CODE=STATUS_CODE;

alter table PAS.PAS_FLORIST_ZIPCODE_DT modify FIT_STATUS_CODE VARCHAR2(1) not null;


------------------------------------------------------------------------------------
-- end requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharmateja Konduri                         3/ 2/2016  --------
-- defect  FHDC-12       (Mark  ) (189096  )        --------------------------------
------------------------------------------------------------------------------------

drop function ftd_apps.oe_get_advance_search_md04;
drop function ftd_apps.oe_get_florist_list;
drop function ftd_apps.oe_get_products_by_id;
drop function ftd_apps.oe_get_products_by_id_sdg;
drop function ftd_apps.oe_get_zipcode_info_md04;
drop function ftd_apps.oe_novator_keyword_search_sdg;
drop function ftd_apps.oe_novator_keyword_search_sfmb;
drop function ftd_apps.oe_novator_kwrd_srch_sdg_2247;
drop function ftd_apps.oe_product_details_sfmb;
drop function ftd_apps.oe_product_list_by_index;
drop function ftd_apps.oe_product_list_by_index_2247;
drop function ftd_apps.oe_product_list_by_index_ivan;
drop function ftd_apps.oe_product_list_by_index_sdg;
drop function ftd_apps.oe_product_list_by_index_sfmb;
drop procedure ftd_apps.oel_update_csz_zip_code;
drop procedure ftd_apps.oel_update_florist;
drop function ftd_apps.oe_get_florist_by_id;

------------------------------------------------------------------------------------
-- end   requested by Dharmateja Konduri                         3/ 2/2016  --------
-- defect  FHDC-12       (Mark  ) (189096  )        --------------------------------
------------------------------------------------------------------------------------

