------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/13/2016  --------
-- defect FHDC-1        (Mark  ) (180099,181036)    --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE FTD_APPS.FLORIST_HOURS 
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OPEN_CLOSE_CODE VARCHAR2(10), 
        OPEN_TIME VARCHAR2(20), 
        CLOSE_TIME VARCHAR2(20) 
    ) tablespace ftd_apps_data;

ALTER  TABLE FTD_APPS.FLORIST_HOURS add CONSTRAINT FLORIST_HOURS_PK PRIMARY KEY (FLORIST_ID, DAY_OF_WEEK) 
using index tablespace ftd_apps_indx;

alter table ftd_apps.florist_hours add (
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100));

CREATE TABLE FTD_APPS.FLORIST_HOURS$
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OPEN_CLOSE_CODE VARCHAR2(10), 
        OPEN_TIME VARCHAR2(20), 
        CLOSE_TIME VARCHAR2(20), 
        OPERATION$ VARCHAR2(10),
        TIMESTAMP$ TIMESTAMP
    ) tablespace ftd_apps_data;

create index ftd_apps.florist_hours$_n1 on ftd_apps.florist_hours$(timestamp$) tablespace ftd_apps_indx;
create index ftd_apps.florist_hours$_n2 on ftd_apps.florist_hours$(florist_id) tablespace ftd_apps_indx;

alter  TABLE FTD_APPS.FLORIST_HOURS$ add (
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100));

CREATE TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OVERRIDE_DATE DATE NOT NULL, 
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100)
    ) tablespace ftd_apps_data;

alter  TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE add CONSTRAINT FLORIST_HOURS_OVERRIDE_PK PRIMARY KEY 
(FLORIST_ID, DAY_OF_WEEK, OVERRIDE_DATE) using index tablespace ftd_apps_indx;

CREATE TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE$
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OVERRIDE_DATE DATE NOT NULL, 
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100),
        OPERATION$ VARCHAR2(10),
        TIMESTAMP$ TIMESTAMP
    ) tablespace ftd_apps_data;

create index ftd_apps.florist_hours_override$_n1 on ftd_apps.florist_hours_override$(timestamp$) tablespace ftd_apps_indx;
create index ftd_apps.florist_hours_override$_n2 on ftd_apps.florist_hours_override$(florist_id) tablespace ftd_apps_indx;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/13/2016  --------
-- defect FHDC-1        (Mark  ) (180099,181036)    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------
-- *** THIS IS A GOLDEN GATE CHANGE ***

grant select, flashback on ftd_apps.florist_hours to ogg;
grant select, flashback on ftd_apps.florist_hours_override to ogg;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B                                 2/18/2016  --------
-- defect LPSF-1        (Pavan  ) (186883)          --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE  FTD_APPS.SOURCE_LEGACY_ID_MAPPING(SOURCE_CODE  VARCHAR2(10) NOT NULL,VARCHAR2(100) NOT NULL, 
CREATED_ON   TIMESTAMP(6) NOT NULL, CREATED_BY VARCHAR2(100) NOT NULL, UPDATED_ON  TIMESTAMP(6)   NOT NULL,
 UPDATED_BY   VARCHAR2(100) NOT NULL);

------------------------------------------------------------------------------------
-- end   requested by Gunadeep B                                 2/18/2016  --------
-- defect LPSF-1        (Pavan  ) (186883)          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_END_DATE_PRODUCT','REPLACE ME','MLII-1', SYSDATE, 
'MLII-1', SYSDATE, 'New Block end date for the codified product. Used by MARS for View Queue processing');

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_END_DATE_ZIPCODE','REPLACE ME','MLII-1', SYSDATE,
 'MLII-1', SYSDATE, 'New Block end date for the florist zipcode. Used by MARS for View Queue processing');
 
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_ZIP_CUTOFF','REPLACE ME','MLII-1', SYSDATE, 'MLII-1',
 SYSDATE, 'New Cutoff for the florist zipcode. Used by MARS for View Queue processing');
 
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_HOLIDAY_DATE','REPLACE ME','MLII-1', 
SYSDATE, 'MLII-1', SYSDATE, 'Holiday date to be used by MARS for View Queue processing');

------------------------------------------------------------------------------------
-- end   requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/23/2016  --------
-- defect SP-160        (Mark  ) (187681)           --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION)
values (
    'SEQUENCE_CHECKER',
    'PROCESSING_DELAY_MINUTES',
    '5',
    'SP-160',
    sysdate,
    'SP-160',
    sysdate,
    'The number of minutes to wait before an order is eligible for the sequence checker');

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 2/23/2016  --------
-- defect SP-160        (Mark  ) (187681)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B                                2/29/2016  --------
-- defect LPSF-4        (Pavan  ) (187560)           --------------------------------
------------------------------------------------------------------------------------

insert into FTD_APPS.Source_Legacy_Id_Mapping select * from gboddu.source_legacy_mapping;

------------------------------------------------------------------------------------
-- end   requested by Gunadeep B                                2/29/2016  --------
-- defect LPSF-4        (Pavan  ) (187560)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Manasa S                                 3/11/2016  --------
-- defect  SP-64         (Pavan  ) (190251)           --------------------------------
------------------------------------------------------------------------------------
insert into FRP.GLOBAL_PARMS values
('ACCOUNTING_CONFIG','PP_ERR_SHORT_MSG_TRANS_REFUSED','Transaction refused','SP-64',SYSDATE,'SP-64',SYSDATE,
'Paypal Error with Code 10009 and short message as Transaction Refused ');  

insert into FRP.GLOBAL_PARMS values
('ACCOUNTING_CONFIG','PP_ERR_LONG_MSG_FULLY_REFUNDED','This transaction has already been fully refunded',
'SP-64',SYSDATE,'SP-64',SYSDATE,'Paypal Error with Code 10009 and Long message as mentioned in value');  

insert into FRP.GLOBAL_PARMS values
('ACCOUNTING_CONFIG','PP_ERR_LONG_MSG_CHARGE_BCK_FILED','This transaction already has a chargeback filed',
'SP-64',SYSDATE,'SP-64',SYSDATE,'Paypal Error with Code 10009 and Long message as mentioned in value');  

 
-------------------------------------------------------------------------------------
-- end   requested by Manasa S                                 3/11/2016  --------
-- defect  SP-64         (Pavan  ) (190251)           --------------------------------
------------------------------------------------------------------------------------

 
-------------------------------------------------------------------
-- begin requested by Mark Campbell,           03/16/2015  --------
-- defect #FHDC-39  (Mark )       (SD191248) ----------------------
-------------------------------------------------------------------

grant select, flashback on fit.threshold_actions to ogg;

-------------------------------------------------------------------
-- end   requested by Mark Campbell,           03/16/2015  --------
-- defect #FHDC-39  (Mark )       (SD191248) ----------------------
-------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 3/24/2016  --------
-- defect MLII-22        (Pavan  ) (192470)           --------------------------------
------------------------------------------------------------------------------------

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Block end date (Date Format - MM/dd/yyyy) for the codified product. Used by MARS for View Queue processing.' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_END_DATE_PRODUCT';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Block end date (Date Format - MM/dd/yyyy) for the florist zipcode. Used by MARS for View Queue processing' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_END_DATE_ZIPCODE';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'Holiday date (Date Format - MM/dd/yyyy) to be used by MARS for View Queue processing' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_HOLIDAY_DATE';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Cutoff (Time format - HHMM) for the florist zipcode. Used by MARS for View Queue processing'
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_ZIP_CUTOFF';


-------------------------------------------------------------------------------------
-- end   requested by karthik D                                3/24/2016  --------
-- defect MLII-22        (Pavan  ) (192470)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                 4/1/2016  --------
-- defect SP-184        (Pavan  ) (193826 )           --------------------------------
------------------------------------------------------------------------------------

exec dbms_aqadm.alter_queue(queue_name=>'OJMS.FS_AUTO_RENEW', max_retries=>5, retry_delay=>2, comment=> 'Retry is set to 5 times');
exec dbms_aqadm.alter_queue(queue_name=>'OJMS.ACCOUNT_ORDER_FEED', max_retries=>5, retry_delay=>2, comment=> 'Retry is set to 5 times');

-------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                4/1/2016  --------
-- defect SP-184        (Pavan  ) (193826 )           --------------------------------
------------------------------------------------------------------------------------

