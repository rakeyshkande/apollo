------------------------------------------------------------------------------------
-- begin requested by Dharma Teja                                 4/1/2016  --------
-- defect SP-184        (Pavan  ) (193826 )           --------------------------------
------------------------------------------------------------------------------------

exec dbms_aqadm.alter_queue(queue_name=>'OJMS.FS_AUTO_RENEW', max_retries=>5, retry_delay=>2, comment=> 'Retry is set to 5 times');
exec dbms_aqadm.alter_queue(queue_name=>'OJMS.ACCOUNT_ORDER_FEED', max_retries=>5, retry_delay=>2, comment=> 'Retry is set to 5 times');
 

-------------------------------------------------------------------------------------
-- end   requested by Dharma Teja                                4/1/2016  --------
-- defect SP-184        (Pavan  ) (193826 )           --------------------------------
------------------------------------------------------------------------------------



 
