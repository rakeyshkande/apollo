------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-2        (Mark  ) (181036)           --------------------------------
------------------------------------------------------------------------------------

grant execute on ftd_apps.florist_query_pkg to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-2        (Mark  ) (181036)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-9        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.florist_hours to pas;
grant select on ftd_apps.florist_hours_override to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-9        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------
-- this is a Golden Gate change

alter table ftd_apps.florist_master add (
LAST_FHDC_OPEN_CLOSE_DATE   date,
LAST_FHDC_OPEN_CLOSE_STATUS Varchar2(100));

alter table ftd_apps.florist_master$ add (
LAST_FHDC_OPEN_CLOSE_DATE   date,
LAST_FHDC_OPEN_CLOSE_STATUS Varchar2(100));

grant update on ftd_apps.florist_master to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/1/2016  --------
-- defect FHDC-2        (Syed  ) (182824  )           --------------------------------
------------------------------------------------------------------------------------


insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'PM_CLOSED_DELIVERY_CUTOFF',
    '1000',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'Same day delivery cutoff time for florists that are PM closed'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'PM_CLOSED_ORDER_CUTOFF',
    '1130',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'Future order cutoff time for florists that are PM closed'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'EOD_CLOSED_ORDER_CUTOFF',
    '2300',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'The time of day where we start looking at tomorrow for availability to receive orders'); 


------------------------------------------------------------------------------------
-- end requested by Tim Schmig                                 2/1/2016  --------
-- defect FHDC-2        (Syed  ) (182824  )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Bhargava Surimenu                          1/28/2016  --------
-- defect LPSF-2        (Pavan ) (182683)           --------------------------------
------------------------------------------------------------------------------------

Alter Table PTN_PI.partner_order_detail ADD legacy_Id Varchar2(100);

Alter Table Scrub.Order_Details Add legacy_Id Varchar2(100);
-- this is a golden gate change
Alter Table Clean.Order_Details Add  legacy_Id Varchar2(100);

------------------------------------------------------------------------------------
-- end   requested by Bhargava Surimenu                          1/28/2016  --------
-- defect LPSF-2        (Pavan ) (182683)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                2/17/2016  --------
-- defect QE-119        (Mark  ) (186848)           --------------------------------
------------------------------------------------------------------------------------

alter table b2b.pending add updated_on date;
alter table b2b.pending_line_item add updated_on date;
alter table b2b.approved add updated_on date;
alter table b2b.approved_line_item add updated_on date;

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                2/17/2016  --------
-- defect QE-119        (Mark  ) (186848)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/16/2016  --------
-- defect FHDC-26       (Mark  ) (186591)           --------------------------------
------------------------------------------------------------------------------------

update ftd_apps.state_master
set time_zone = '6'
where state_master_id = 'HI';

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/16/2016  --------
-- defect FHDC-26       (Mark  ) (186591)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                2/11/2016  --------
-- defect QE-120        (Mark  ) (185665)           --------------------------------
------------------------------------------------------------------------------------

alter table b2b.approved_line_item modify (ftd_order_number          varchar2(20));
alter table b2b.approved_line_item modify (bill_to_home_phone        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_work_phone        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_fax_number        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_work_ext          varchar2(10));
alter table b2b.approved_line_item modify (bill_to_email             varchar2(55));
alter table b2b.approved_line_item modify (ship_to_address_line2     varchar2(45));
alter table b2b.approved_line_item modify (ship_to_home_phone        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_work_phone        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_work_ext          varchar2(10));
alter table b2b.approved_line_item modify (ship_to_fax_number        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_email             varchar2(55));
alter table b2b.approved_line_item modify (master_order_number       varchar2(100));

alter table b2b.pending_line_item modify (ftd_order_number           varchar2(20));
alter table b2b.pending_line_item modify (bill_to_email              varchar2(55));
alter table b2b.pending_line_item modify (master_order_number        varchar2(100));
alter table b2b.pending_line_item modify (bill_to_work_phone         varchar2(20));
alter table b2b.pending_line_item modify (bill_to_work_ext           varchar2(10));
alter table b2b.pending_line_item modify (ship_to_ext                varchar2(10));
alter table b2b.pending_line_item modify (ship_to_phone              varchar2(20));

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                2/11/2016  --------
-- defect QE-120        (Mark  ) (185665)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk                                 2/19/2016  --------
-- defect FHDC-25       (Mark  ) (187175)           --------------------------------
------------------------------------------------------------------------------------

grant execute on ftd_apps.florist_query_pkg to joe;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk                                 2/19/2016  --------
-- defect FHDC-25       (Mark  ) (187175)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk                                 2/22/2016  --------
-- defect FHDC-25       (Mark  ) (187465)           --------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.florist_hours to joe;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk                                 2/22/2016  --------
-- defect FHDC-25       (Mark  ) (187465)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------
-- *** THIS IS A GOLDEN GATE CHANGE *** -- 
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD(CODIFICATION_ID VARCHAR2(5), CODIFIED_MINIMUM NUMBER(38));

------------------------------------------------------------------------------------
-- end   requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                2/24/2016  --------
-- defect QE-119        (Mark  ) (188055)           --------------------------------
------------------------------------------------------------------------------------

alter table b2b.approved_line_item modify (bill_to_address_line2 varchar2(45));
alter table b2b.pending_line_item modify (bill_to_address_line2 varchar2(45));

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                2/24/2016  --------
-- defect QE-119        (Mark  ) (188055)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                3/1/2016  --------
-- defect QE-119        (Syed  ) (188865)           --------------------------------
------------------------------------------------------------------------------------

GRANT EXECUTE ON B2B.PENDINGMOVE_FUNC TO OSP;

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                3/1/2016  --------
-- defect QE-119        (Syed  ) (188865)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

-- This required Golden Gate chnage


alter table PAS.PAS_FLORIST_ZIPCODE_DT add  FIT_STATUS_CODE VARCHAR2(1);

update  PAS.PAS_FLORIST_ZIPCODE_DT set FIT_STATUS_CODE=STATUS_CODE;

alter table PAS.PAS_FLORIST_ZIPCODE_DT modify FIT_STATUS_CODE VARCHAR2(1) not null;


------------------------------------------------------------------------------------
-- end requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Dharmateja Konduri                         3/ 2/2016  --------
-- defect  FHDC-12       (Mark  ) (189096  )        --------------------------------
------------------------------------------------------------------------------------

drop function ftd_apps.oe_get_advance_search_md04;
drop function ftd_apps.oe_get_florist_list;
drop function ftd_apps.oe_get_products_by_id;
drop function ftd_apps.oe_get_products_by_id_sdg;
drop function ftd_apps.oe_get_zipcode_info_md04;
drop function ftd_apps.oe_novator_keyword_search_sdg;
drop function ftd_apps.oe_novator_keyword_search_sfmb;
drop function ftd_apps.oe_novator_kwrd_srch_sdg_2247;
drop function ftd_apps.oe_product_details_sfmb;
drop function ftd_apps.oe_product_list_by_index;
drop function ftd_apps.oe_product_list_by_index_2247;
drop function ftd_apps.oe_product_list_by_index_ivan;
drop function ftd_apps.oe_product_list_by_index_sdg;
drop function ftd_apps.oe_product_list_by_index_sfmb;
drop procedure ftd_apps.oel_update_csz_zip_code;
drop procedure ftd_apps.oel_update_florist;
drop function ftd_apps.oe_get_florist_by_id;

------------------------------------------------------------------------------------
-- end   requested by Dharmateja Konduri                         3/ 2/2016  --------
-- defect  FHDC-12       (Mark  ) (189096  )        --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 3/11/2016  --------
-- defect SP-184        (Pavan  ) (190258)           --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS VALUES('AUTH_CONFIG','jccas.ws.socket.timeout.fsautorenew','10000','SP-184',
SYSDATE,'SP-184',SYSDATE,'Custom JCCAS Socket Timeout for FS Auto Renew calls');

UPDATE QUARTZ_SCHEMA.PIPELINE SET DEFAULT_SCHEDULE = '0 0 * * * ?', 
DEFAULT_PAYLOAD = 'MARS_Action_Detail_Report;1H;ALL'
 WHERE PIPELINE_ID = 'MARS_ACTION_DETAIL_REPORT' AND GROUP_ID = 'MARS';

------------------------------------------------------------------------------------
-- end   requested by Karthik D                                 3/11/2016  --------
-- defect SP-184        (Pavan  ) (190258)           --------------------------------
------------------------------------------------------------------------------------

