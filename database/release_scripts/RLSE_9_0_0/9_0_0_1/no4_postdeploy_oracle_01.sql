------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               2/ 1/2016  --------
-- defect FHDC-1          (Mark  ) (182841 )          --------------------------------
------------------------------------------------------------------------------------

declare
v_status varchar2(4000);
v_message varchar2(4000);
begin
    ftd_apps.florist_load_audit_pkg.load_florist_hours(v_status, v_message);
    dbms_output.put_line('status: ' || v_status);
    -- sunday availability has already been processed so this can be skipped
    delete from ojms.pas_command pc
    where  pc.user_data.text_vc like '%MODIFIED_FLORIST%SUNDAY';
    commit;
end;
/

------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                               2/ 1/2016  --------
-- defect FHDC-1          (Mark  ) (182841 )          --------------------------------
------------------------------------------------------------------------------------

