------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/13/2016  --------
-- defect FHDC-1        (Mark  ) (180099,181036)    --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE FTD_APPS.FLORIST_HOURS 
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OPEN_CLOSE_CODE VARCHAR2(10), 
        OPEN_TIME VARCHAR2(20), 
        CLOSE_TIME VARCHAR2(20) 
    ) tablespace ftd_apps_data;

ALTER  TABLE FTD_APPS.FLORIST_HOURS add CONSTRAINT FLORIST_HOURS_PK PRIMARY KEY (FLORIST_ID, DAY_OF_WEEK) 
using index tablespace ftd_apps_indx;

alter table ftd_apps.florist_hours add (
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100));

CREATE TABLE FTD_APPS.FLORIST_HOURS$
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OPEN_CLOSE_CODE VARCHAR2(10), 
        OPEN_TIME VARCHAR2(20), 
        CLOSE_TIME VARCHAR2(20), 
        OPERATION$ VARCHAR2(10),
        TIMESTAMP$ TIMESTAMP
    ) tablespace ftd_apps_data;

create index ftd_apps.florist_hours$_n1 on ftd_apps.florist_hours$(timestamp$) tablespace ftd_apps_indx;
create index ftd_apps.florist_hours$_n2 on ftd_apps.florist_hours$(florist_id) tablespace ftd_apps_indx;

alter  TABLE FTD_APPS.FLORIST_HOURS$ add (
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100));

CREATE TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OVERRIDE_DATE DATE NOT NULL, 
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100)
    ) tablespace ftd_apps_data;

alter  TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE add CONSTRAINT FLORIST_HOURS_OVERRIDE_PK PRIMARY KEY 
(FLORIST_ID, DAY_OF_WEEK, OVERRIDE_DATE) using index tablespace ftd_apps_indx;

CREATE TABLE FTD_APPS.FLORIST_HOURS_OVERRIDE$
    ( 
        FLORIST_ID VARCHAR2(9) NOT NULL, 
        DAY_OF_WEEK VARCHAR2(10) NOT NULL, 
        OVERRIDE_DATE DATE NOT NULL, 
        CREATED_ON DATE,
        CREATED_BY VARCHAR2(100),
        UPDATED_ON DATE,
        UPDATED_BY VARCHAR2(100),
        OPERATION$ VARCHAR2(10),
        TIMESTAMP$ TIMESTAMP
    ) tablespace ftd_apps_data;

create index ftd_apps.florist_hours_override$_n1 on ftd_apps.florist_hours_override$(timestamp$) tablespace ftd_apps_indx;
create index ftd_apps.florist_hours_override$_n2 on ftd_apps.florist_hours_override$(florist_id) tablespace ftd_apps_indx;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/13/2016  --------
-- defect FHDC-1        (Mark  ) (180099,181036)    --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------
-- *** THIS IS A GOLDEN GATE CHANGE ***

grant select, flashback on ftd_apps.florist_hours to ogg;
grant select, flashback on ftd_apps.florist_hours_override to ogg;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

