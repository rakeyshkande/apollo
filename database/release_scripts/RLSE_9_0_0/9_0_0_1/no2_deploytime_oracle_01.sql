------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-2        (Mark  ) (181036)           --------------------------------
------------------------------------------------------------------------------------

grant execute on ftd_apps.florist_query_pkg to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-2        (Mark  ) (181036)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-9        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.florist_hours to pas;
grant select on ftd_apps.florist_hours_override to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-9        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------

alter table ftd_apps.florist_master add (
LAST_FHDC_OPEN_CLOSE_DATE   date,
LAST_FHDC_OPEN_CLOSE_STATUS Varchar2(100));

alter table ftd_apps.florist_master$ add (
LAST_FHDC_OPEN_CLOSE_DATE   date,
LAST_FHDC_OPEN_CLOSE_STATUS Varchar2(100));

grant update on ftd_apps.florist_master to pas;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/1/2016  --------
-- defect FHDC-2        (Syed  ) (182824  )           --------------------------------
------------------------------------------------------------------------------------


insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'PM_CLOSED_DELIVERY_CUTOFF',
    '1000',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'Same day delivery cutoff time for florists that are PM closed'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'PM_CLOSED_ORDER_CUTOFF',
    '1130',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'Future order cutoff time for florists that are PM closed'); 

insert into FRP.GLOBAL_PARMS ( 
    CONTEXT, 
    NAME, 
    VALUE, 
    CREATED_BY, 
    CREATED_ON, 
    UPDATED_BY, 
    UPDATED_ON,
    DESCRIPTION) 
values (
    'FTDAPPS_PARMS',
    'EOD_CLOSED_ORDER_CUTOFF',
    '2300',
    'N/A',
    sysdate,
    'N/A',
    sysdate,
    'The time of day where we start looking at tomorrow for availability to receive orders'); 


------------------------------------------------------------------------------------
-- end requested by Tim Schmig                                 2/1/2016  --------
-- defect FHDC-2        (Syed  ) (182824  )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Bhargava Surimenu                          1/25/2016  --------
-- defect LPSF-2        (Mark  ) (182683)           --------------------------------
------------------------------------------------------------------------------------

----Removed as per request by Bhargava Surimenu (192468)

------------------------------------------------------------------------------------
-- end   requested by Bhargava Surimenu                          1/25/2016  --------
-- defect LPSF-2        (Mark  ) (182683)           --------------------------------
------------------------------------------------------------------------------------

