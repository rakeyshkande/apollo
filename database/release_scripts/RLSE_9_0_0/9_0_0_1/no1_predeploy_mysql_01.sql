------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
CREATE TABLE florist_hours ( 
FLORIST_ID varchar(9) NOT NULL, 
DAY_OF_WEEK varchar(10) NOT NULL,
OPEN_CLOSE_CODE varchar(10) DEFAULT NULL,
OPEN_TIME varchar(20) DEFAULT NULL,
CLOSE_TIME varchar(20) DEFAULT NULL,
CREATED_ON datetime DEFAULT NULL,
CREATED_BY varchar(100) DEFAULT NULL,
UPDATED_ON datetime DEFAULT NULL,
UPDATED_BY varchar(100) DEFAULT NULL,
PRIMARY KEY (FLORIST_ID, DAY_OF_WEEK)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE florist_hours_override (
FLORIST_ID varchar(9) NOT NULL,
DAY_OF_WEEK varchar(10) NOT NULL, 
OVERRIDE_DATE datetime NOT NULL, 
CREATED_ON datetime DEFAULT NULL,
CREATED_BY varchar(100) DEFAULT NULL,
UPDATED_ON datetime DEFAULT NULL,
UPDATED_BY varchar(100) DEFAULT NULL,
PRIMARY KEY (FLORIST_ID, DAY_OF_WEEK, OVERRIDE_DATE)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/20/2016  --------
-- defect FHDC-1        (Mark  ) (181249)           --------------------------------
------------------------------------------------------------------------------------

