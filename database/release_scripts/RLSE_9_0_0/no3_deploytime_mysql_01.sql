
------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
alter table florist_master add (
LAST_FHDC_OPEN_CLOSE_DATE   datetime default null,
LAST_FHDC_OPEN_CLOSE_STATUS Varchar(100) default null);

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 1/21/2016  --------
-- defect FHDC-9        (Mark  ) (181534)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Bhargava Surimenu                          1/25/2016  --------
-- defect LPSF-2        (Mark  ) (182683)           --------------------------------
------------------------------------------------------------------------------------

use clean;
Alter Table Order_Details Add  legacy_Id Varchar(100);

------------------------------------------------------------------------------------
-- end   requested by Bhargava Surimenu                          1/25/2016  --------
-- defect LPSF-2        (Mark  ) (182683)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                  2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)          --------------------------------
------------------------------------------------------------------------------------

use mercury;
alter table mercury_rules_tracker add (CODIFICATION_ID VARCHAR(5), CODIFIED_MINIMUM bigint);

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                  2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

use PAS;

alter table pas_florist_zipcode_dt add  FIT_STATUS_CODE VARCHAR(1);

------------------------------------------------------------------------------------
-- end requested by Tim Schmig                                3/1/2016  --------
-- defect  FHDC-27       (Syed  ) (188785  )           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B                                 3/24/2016  --------
-- defect LPSF-9        (Pavan  ) (192468)           --------------------------------
------------------------------------------------------------------------------------

Insert into FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
values 
('PI_CONFIG', 'PRMY_BCKP_FLORIST_LEGACY_ID_LOOK_UP', 'Y', 'LPSF-5', SYSDATE, 'LPSF-5', SYSDATE, 
'This field has the control to Switch ON and OFF of the new Legacy ID functionality of showing the Primary/Secondary florist.');
 
Alter table rpt.report modify Report_File_Prefix varchar2(100);

SET DEFINE OFF

SET ESCAPE '\'

Insert Into Rpt.Report(Report_Id,Name,Description,Report_File_Prefix,Report_Type,REPORT_OUTPUT_TYPE,Report_Category,Server_Name,Holiday_Indicator,
STATUS,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY,ORACLE_RDF,
Acl_Name,Retention_Days,Run_Time_Offset,End_Of_Day_Required)
Values(rpt.Report_Id_Sq.Nextval,'Primary/Backup Florist Availability',
'This report displays the current status of florists that are assigned as a Primary or Backup florist to a Source Code.',
'PrimaryORBackup_Florist_Availability','U','Char','Cust','http://apolloreports.ftdi.com/reports/rwservlet?','N','Active',Sysdate,
'LPSF-9',SYSDATE,'LPSF-9','CSR05_Primary_Bkp_Florist_Avail.rdf','Marketing',10,0,'N');


Insert Into Rpt.Report(Report_Id,Name,Description,Report_File_Prefix,Report_Type,REPORT_OUTPUT_TYPE,Report_Category,Server_Name,Holiday_Indicator,
Status,Created_On,Created_By,Updated_On,Updated_By,Oracle_Rdf,
Acl_Name,Retention_Days,Run_Time_Offset,End_Of_Day_Required,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
Values(Rpt.Report_Id_Sq.Nextval,'Primary/Backup Florist Availability (Legacy, Weekly)',
'This is a scheduled report, it will identify florists that are assigned as a Primary or Backup Florist to one or more Source Codes of Legacy Source Type',
'PrimaryORBackup_Florist_Availability_LEGACY_WEEKLY','S','Char','Cust','http://apolloreports.ftdi.com/reports/rwservlet?','N','Active',Sysdate,
'LPSF-10',Sysdate,'LPSF-10','CSR06_Primary_Bkp_Florist_Avail_W.rdf','Marketing',10,60,'N','3','W','\&p_source_types=LEGACY,LEGACY.COM,LEGACY MARKETPLACE,SYMPATHY CORPORATE GIFTS&p_florist_status=Inactive,Opt Out&p_source_codes=');


Insert Into Rpt.Report(Report_Id,Name,Description,Report_File_Prefix,Report_Type,REPORT_OUTPUT_TYPE,Report_Category,Server_Name,Holiday_Indicator,
Status,Created_On,Created_By,Updated_On,Updated_By,Oracle_Rdf,
Acl_Name,Retention_Days,Run_Time_Offset,End_Of_Day_Required,SCHEDULE_DAY,SCHEDULE_TYPE,SCHEDULE_PARM_VALUE)
Values(Rpt.Report_Id_Sq.Nextval,'Primary/Backup Florist Availability (Batesville , Weekly)',
'This is a scheduled report, it will identify florists that are assigned as a Primary or Backup Florist to one or more Source Codes of Batesville Source Type',
'PrimaryORBackup_Florist_Availability_BATESVILLE_WEEKLY','S','Char','Cust','http://apolloreports.ftdi.com/reports/rwservlet?','N','Active',Sysdate,
'LPSF-10',Sysdate,'LPSF-10','CSR06_Primary_Bkp_Florist_Avail_W.rdf','Marketing',10,60,'N','3','W','\&p_source_types=BV GIFTS,BV FRIENDS AND FAMILY,BATESVILLE,BV FRIEND AND FAMILY&p_florist_status=Inactive,Opt Out&p_source_codes=');


Insert Into Rpt.Report(Report_Id,Name,Description,Report_File_Prefix,Report_Type,REPORT_OUTPUT_TYPE,Report_Category,Server_Name,Holiday_Indicator,
Status,Created_On,Created_By,Updated_On,Updated_By,Oracle_Rdf,
Acl_Name,Retention_Days,Run_Time_Offset,End_Of_Day_Required,Schedule_Day,Schedule_Type,Schedule_Parm_Value)
Values(Rpt.Report_Id_Sq.Nextval,'Primary/Backup Florist Availability (SCI, Weekly)',
'This is a scheduled report, it will identify florists that are assigned as a Primary or Backup Florist to one or more Source Codes of SCI Source Type',
'PrimaryORBackup_Florist_Availability_SCI_WEEKLY','S','Char','Cust','http://apolloreports.ftdi.com/reports/rwservlet?','N','Active',Sysdate,
'LPSF-10',Sysdate,'LPSF-10','CSR06_Primary_Bkp_Florist_Avail_W.rdf','Marketing',10,60,'N','3','W','\&p_source_types=SCI FRIENDS %26 FAMILY,SCI,SCI CEMETARY,SCI DROP SHIP ONLY,SCI FUNERAL HOME,SCI JEWISH GIFTS,SCI NO DIG MEM BRANDING&p_florist_status=Inactive,Opt Out&p_source_codes=');


-- Params

Insert Into Rpt.Report_Parm(Report_Parm_Id,Display_Name,Report_Parm_Type,Oracle_Parm_Name,Created_On,Created_By,Updated_On,Updated_By)
VALUES(RPT.REPORT_PARM_ID_SQ.NEXTVAL,'Source Code','TE','p_source_codes',SYSDATE,'LPSF-9',SYSDATE,'LPSF-9');


Insert Into Rpt.Report_Parm(Report_Parm_Id,Display_Name,Report_Parm_Type,Oracle_Parm_Name,Created_On,Created_By,Updated_On,Updated_By)
Values(Rpt.Report_Parm_Id_Sq.Nextval,'Florist Status','MS','p_florist_status',Sysdate,'LPSF-9',Sysdate,'LPSF-9');

Insert Into Rpt.Report_Parm(Report_Parm_Id,Display_Name,Report_Parm_Type,Oracle_Parm_Name,Created_On,Created_By,Updated_On,Updated_By)
Values(Rpt.Report_Parm_Id_Sq.Nextval,'Source Type','TE','p_source_types',Sysdate,'LPSF-9',Sysdate,'LPSF-9');

-- Report - Param Mappings

-- p_source_codes mapping
Insert Into Rpt.Report_Parm_Ref(Report_Parm_Id,Report_Id,Sort_Order,Required_Indicator,Created_On,Created_By)
Values((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_source_codes' And Created_By='LPSF-9'),
(select Report_Id from rpt.report where name ='Primary/Backup Florist Availability'),'1','N',Sysdate,'LPSF-9'); 

-- p_florist_status
Insert Into Rpt.Report_Parm_Ref(Report_Parm_Id,Report_Id,Sort_Order,Required_Indicator,Created_On,Created_By)
Values((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_florist_status' And Created_By='LPSF-9'),
(select Report_Id from rpt.report where name ='Primary/Backup Florist Availability'),'2','Y',Sysdate,'LPSF-9'); 

-- p_source_types 
Insert Into Rpt.Report_Parm_Ref(Report_Parm_Id,Report_Id,Sort_Order,Required_Indicator,Created_On,Created_By)
Values((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_source_types' And Created_By='LPSF-9'),
(select Report_Id from rpt.report where name ='Primary/Backup Florist Availability'),'3','Y',Sysdate,'LPSF-9'); 

-- Report Param Values
insert into RPT.Report_Parm_Value values (rpt.report_parm_value_id_sq.Nextval,sysdate,'LPSF-9',sysdate,'LPSF-9','SOURCE_CODES_TEMPLATE',null,null);

insert into RPT.Report_Parm_Value values (rpt.report_parm_value_id_sq.Nextval,sysdate,'LPSF-9',sysdate,'LPSF-9','Florist Status','GET_FLORIST_STATUSES','ALL');

insert into RPT.Report_Parm_Value values (rpt.report_parm_value_id_sq.Nextval,sysdate,'LPSF-9',sysdate,'LPSF-9','SOURCE_TYPES_TEMPLATE','GET_SOURCE_TYPES',null);

insert into RPT.Report_parm_Value_ref  values ((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_source_codes' And Report_Parm_Type='TE'),
(select Report_Parm_Value_id from RPT.Report_Parm_Value where Display_Text = 'SOURCE_CODES_TEMPLATE'),'1',sysdate,'LPSF-9');

insert into RPT.Report_Parm_Value_Ref values ((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_florist_status' And Created_By='LPSF-9'),
(select report_parm_value_id from RPT.Report_Parm_Value where created_by='LPSF-9' and DB_STATEMENT_ID='GET_FLORIST_STATUSES'),'2',sysdate,'LPSF-9');


insert into RPT.Report_Parm_Value_Ref values ((Select Report_Parm_Id From Rpt.Report_Parm Where Oracle_Parm_Name='p_source_types' And Created_By='LPSF-9')
,(select report_parm_value_id from RPT.Report_Parm_Value where created_by='LPSF-9' and DB_STATEMENT_ID='GET_SOURCE_TYPES'),'3',sysdate,'LPSF-9');


Create Table Ftd_Apps.Prim_Bkp_Florist_Status(Status Varchar2(30) Not Null, Description Varchar2(100));

Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('Active','Active');
Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('Inactive','Inactive');
Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('Blocked','Blocked');
Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('Opt Out','Opt Out');
Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('GoTo Suspend','Suspend for Goto Florists');
Insert Into Ftd_Apps.Prim_Bkp_Florist_Status Values('Mercury Suspend','Suspend for Mercury Florists');


create table ftd_apps.fl_avl_srtp_temp (source_type varchar2(1000));
create table ftd_apps.fl_avl_src_temp (source_code varchar2(1000));
create table ftd_apps.fl_avl_fl_sts_temp (florist_status varchar2(1000));

grant select , insert,update,delete on ftd_apps.fl_avl_srtp_temp to RPT;
grant select , insert,update,delete on ftd_apps.fl_avl_src_temp  to RPT;
grant select , insert,update,delete on ftd_apps.fl_avl_fl_sts_temp to RPT;
grant select on ftd_apps.source_florist_priority to RPT;

GRANT SELECT ON Ftd_Apps.Prim_Bkp_Florist_Status TO RPT;


Insert into FRP.GLOBAL_PARMS 
(
 CONTEXT, 
 NAME, 
 VALUE, 
 CREATED_BY, 
 CREATED_ON, 
 UPDATED_BY, 
 UPDATED_ON, 
 DESCRIPTION
) 
values 
(
 'FLORIST_MAINTENANCE', 
 'PRIM_BKP_FLORIST_REPORT_RECIPIENTS', 
 '{replace-me}',
  'LPSF-8', 
 SYSDATE, 
 'LPSF-8',
  SYSDATE, 
 'Distro the primary/backup florist reports to be emailed to.'
);


grant execute on rpt.report_pkg to events;

-------Note: The ACL mentioned below should work for all environments. All have a "barracuda" ACL. ------

BEGIN
   DBMS_NETWORK_ACL_ADMIN.add_privilege (
    acl          => 'barracuda.xml',
    principal    => 'RPT',
    is_grant     => TRUE,
    privilege    => 'connect');
   COMMIT;
END;
/


set define on

-------------------------------------------------------------------------------------
-- end   requested by Gunaddep B                                3/24/2016  --------
-- defect LPSF-9        (Pavan  ) (192468)           --------------------------------
------------------------------------------------------------------------------------

