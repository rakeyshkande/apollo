------------------------------------------------------------------------------------
-- begin requested by Tim Schmig,                               2/ 1/2016  --------
-- defect FHDC-1          (Mark  ) (182841 )          --------------------------------
------------------------------------------------------------------------------------

declare
v_status varchar2(4000);
v_message varchar2(4000);
begin
    ftd_apps.florist_load_audit_pkg.load_florist_hours(v_status, v_message);
    dbms_output.put_line('status: ' || v_status);
    -- sunday availability has already been processed so this can be skipped
    delete from ojms.pas_command pc
    where  pc.user_data.text_vc like '%MODIFIED_FLORIST%SUNDAY';
    commit;
end;
/

------------------------------------------------------------------------------------
-- end requested by Tim Schmig,                               2/ 1/2016  --------
-- defect FHDC-1          (Mark  ) (182841 )          --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gunadeep B                                 3/24/2016  --------
-- defect LPSF-9        (Pavan  ) (192468)           --------------------------------
------------------------------------------------------------------------------------

set serveroutput on
alter user events identified by "down_out" account unlock;
conn events/down_out

Declare
   V_job number;
Begin
   Dbms_job.submit
   (
v_job,
'events.trigger_florist_reports_email;',
trunc(next_day(sysdate,'Tuesday'))+(1/24)+(25/1440),
'trunc(next_day(sysdate,''Tuesday''))+(1/24)+(25/1440)'
);
   Dbms_output.put_line(to_char(v_job));
End;
/


conn &dba_user
alter user events account lock;

-------------------------------------------------------------------------------------
-- end   requested by Gunaddep B                                3/24/2016  --------
-- defect LPSF-9        (Pavan  ) (192468)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 3/10/2016  --------
-- defect SP-187        (Mark  ) (190233)           --------------------------------
------------------------------------------------------------------------------------

grant execute on pas.post_pas_command_delay to ftd_apps;

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 3/10/2016  --------
-- defect SP-187        (Mark  ) (190233)           --------------------------------
------------------------------------------------------------------------------------

