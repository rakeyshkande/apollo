------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 3/24/2016  --------
-- defect MLII-22        (Pavan  ) (192470)           --------------------------------
------------------------------------------------------------------------------------

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Block end date (Date Format - MM/dd/yyyy) for the codified product. Used by MARS for View Queue processing.' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_END_DATE_PRODUCT';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Block end date (Date Format - MM/dd/yyyy) for the florist zipcode. Used by MARS for View Queue processing' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_END_DATE_ZIPCODE';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'Holiday date (Date Format - MM/dd/yyyy) to be used by MARS for View Queue processing' 
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_HOLIDAY_DATE';

UPDATE FRP.GLOBAL_PARMS SET DESCRIPTION = 'New Cutoff (Time format - HHMM) for the florist zipcode. Used by MARS for View Queue processing'
WHERE CONTEXT = 'MARS_CONFIG' AND NAME = 'VIEW_QUEUE_ZIP_CUTOFF';


-------------------------------------------------------------------------------------
-- end   requested by karthik D                                3/24/2016  --------
-- defect MLII-22        (Pavan  ) (192470)           --------------------------------
------------------------------------------------------------------------------------



 
