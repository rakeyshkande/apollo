------------------------------------------------------------------------------------
-- begin requested by Gunadeep B                                 2/18/2016  --------
-- defect LPSF-1        (Pavan  ) (186883)           --------------------------------
------------------------------------------------------------------------------------

CREATE TABLE  FTD_APPS.SOURCE_LEGACY_ID_MAPPING(SOURCE_CODE  VARCHAR2(10) NOT NULL,VARCHAR2(100) NOT NULL, 
CREATED_ON   TIMESTAMP(6) NOT NULL, CREATED_BY VARCHAR2(100) NOT NULL, UPDATED_ON  TIMESTAMP(6)   NOT NULL,
 UPDATED_BY   VARCHAR2(100) NOT NULL);
 
 Alter Table PTN_PI.partner_order_detail ADD legacy_Id Varchar2(100);

Alter Table Scrub.Order_Details Add legacy_Id Varchar2(100);

-- This is a Golden Gate change
Alter Table Clean.Order_Details Add  legacy_Id Varchar2(100);

------------------------------------------------------------------------------------
-- end   requested by Gunadeep B                                 2/18/2016  --------
-- defect LPSF-1        (Pavan  ) (186883)           --------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_END_DATE_PRODUCT','REPLACE ME','MLII-1', SYSDATE, 
'MLII-1', SYSDATE, 'New Block end date for the codified product. Used by MARS for View Queue processing');

INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_END_DATE_ZIPCODE','REPLACE ME','MLII-1', SYSDATE,
 'MLII-1', SYSDATE, 'New Block end date for the florist zipcode. Used by MARS for View Queue processing');
 
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_ZIP_CUTOFF','REPLACE ME','MLII-1', SYSDATE, 'MLII-1',
 SYSDATE, 'New Cutoff for the florist zipcode. Used by MARS for View Queue processing');
 
INSERT INTO FRP.GLOBAL_PARMS VALUES('MARS_CONFIG', 'VIEW_QUEUE_HOLIDAY_DATE','REPLACE ME','MLII-1', 
SYSDATE, 'MLII-1', SYSDATE, 'Holiday date to be used by MARS for View Queue processing');

------------------------------------------------------------------------------------
-- end   requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/23/2016  --------
-- defect SP-160        (Mark  ) (187681)           --------------------------------
------------------------------------------------------------------------------------

insert into FRP.GLOBAL_PARMS (
    CONTEXT,
    NAME,
    VALUE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON,
    DESCRIPTION)
values (
    'SEQUENCE_CHECKER',
    'PROCESSING_DELAY_MINUTES',
    '5',
    'SP-160',
    sysdate,
    'SP-160',
    sysdate,
    'The number of minutes to wait before an order is eligible for the sequence checker');

------------------------------------------------------------------------------------
-- end   requested by Tim Schmig                                 2/23/2016  --------
-- defect SP-160        (Mark  ) (187681)           --------------------------------
------------------------------------------------------------------------------------

