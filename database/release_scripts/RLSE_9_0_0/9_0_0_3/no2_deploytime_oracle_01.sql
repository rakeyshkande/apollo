------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                2/17/2016  --------
-- defect QE-119        (Mark  ) (186848)           --------------------------------
------------------------------------------------------------------------------------

alter table b2b.pending add updated_on date;
alter table b2b.pending_line_item add updated_on date;
alter table b2b.approved add updated_on date;
alter table b2b.approved_line_item add updated_on date;

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                2/17/2016  --------
-- defect QE-119        (Mark  ) (186848)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/16/2016  --------
-- defect FHDC-26       (Mark  ) (186591)           --------------------------------
------------------------------------------------------------------------------------

update ftd_apps.state_master
set time_zone = '6'
where state_master_id = 'HI';

------------------------------------------------------------------------------------
-- begin requested by Tim Schmig                                 2/16/2016  --------
-- defect FHDC-26       (Mark  ) (186591)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Gary Sergey                                2/11/2016  --------
-- defect QE-120        (Mark  ) (185665)           --------------------------------
------------------------------------------------------------------------------------

alter table b2b.approved_line_item modify (ftd_order_number          varchar2(20));
alter table b2b.approved_line_item modify (bill_to_home_phone        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_work_phone        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_fax_number        varchar2(20));
alter table b2b.approved_line_item modify (bill_to_work_ext          varchar2(10));
alter table b2b.approved_line_item modify (bill_to_email             varchar2(55));
alter table b2b.approved_line_item modify (ship_to_address_line2     varchar2(45));
alter table b2b.approved_line_item modify (ship_to_home_phone        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_work_phone        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_work_ext          varchar2(10));
alter table b2b.approved_line_item modify (ship_to_fax_number        varchar2(20));
alter table b2b.approved_line_item modify (ship_to_email             varchar2(55));
alter table b2b.approved_line_item modify (master_order_number       varchar2(100));

alter table b2b.pending_line_item modify (ftd_order_number           varchar2(20));
alter table b2b.pending_line_item modify (bill_to_email              varchar2(55));
alter table b2b.pending_line_item modify (master_order_number        varchar2(100));
alter table b2b.pending_line_item modify (bill_to_work_phone         varchar2(20));
alter table b2b.pending_line_item modify (bill_to_work_ext           varchar2(10));
alter table b2b.pending_line_item modify (ship_to_ext                varchar2(10));
alter table b2b.pending_line_item modify (ship_to_phone              varchar2(20));

------------------------------------------------------------------------------------
-- end   requested by Gary Sergey                                2/11/2016  --------
-- defect QE-120        (Mark  ) (185665)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk                                 2/19/2016  --------
-- defect FHDC-25       (Mark  ) (187175)           --------------------------------
------------------------------------------------------------------------------------

grant execute on ftd_apps.florist_query_pkg to joe;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk                                 2/19/2016  --------
-- defect FHDC-25       (Mark  ) (187175)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Rose Lazuk                                 2/22/2016  --------
-- defect FHDC-25       (Mark  ) (187465)           --------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.florist_hours to joe;

------------------------------------------------------------------------------------
-- end   requested by Rose Lazuk                                 2/22/2016  --------
-- defect FHDC-25       (Mark  ) (187465)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------
-- *** THIS IS A GOLDEN GATE CHANGE *** -- 
ALTER TABLE MERCURY.MERCURY_RULES_TRACKER ADD(CODIFICATION_ID VARCHAR2(5), CODIFIED_MINIMUM NUMBER(38));

------------------------------------------------------------------------------------
-- end   requested by Karthik D                                 2/18/2016  --------
-- defect MLII-3        (Pavan  ) (186885)           --------------------------------
------------------------------------------------------------------------------------

