------------------------------------------------------------------------------------
-- begin requested by Mark Campbell                              3/16/2016  --------
-- defect FHDC-39       (Mark  ) (191248)           --------------------------------
------------------------------------------------------------------------------------

use fit;
CREATE TABLE threshold_actions (
ACTION_ID              bigint not null,
FLORIST_ID             varchar(10) not null,
DELIVERY_DATE          datetime not null,
ACTION_CODE            varchar(10) not null,
ACTION_REASON_CODE     varchar(10) not null,
NOTES                  varchar(200) default null,
THRESHOLD              int not null,
ORDER_COUNT            int not null,
CREATED_ON             datetime not null,
CREATED_BY             varchar(100) not null,
UPDATED_ON             datetime not null,
UPDATED_BY             varchar(100) not null,
SUCCESS                varchar(100) not null,
REJECT_COUNT_DURATION  int not null,
CONTEXT                varchar(100) not null,
TOTAL_REJECT_COUNT     int not null,
PRIMARY KEY (ACTION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

create index threshold_actions_n1 using btree on threshold_actions(
FLORIST_ID, DELIVERY_DATE, CONTEXT);

create index threshold_actions_n2 using btree on threshold_actions (
DELIVERY_DATE, CONTEXT);

------------------------------------------------------------------------------------
-- end   requested by Mark Campbell                              3/16/2016  --------
-- defect FHDC-39       (Mark  ) (191248)           --------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- begin requested by Mark Campbell                              3/ 9/2016  --------
-- defect FHDC-39       (Mark  ) (189901)           --------------------------------
------------------------------------------------------------------------------------

use ftd_apps;
CREATE TABLE `codification_master` (
  `CODIFICATION_ID` varchar(5) NOT NULL,
  `CATEGORY_ID` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `HP_ATTRIBUTE` varchar(20) DEFAULT NULL,
  `FILE_POSITION` bigint(20) DEFAULT NULL,
  `FILE_LENGTH` bigint(20) DEFAULT NULL,
  `LOAD_FROM_FILE` varchar(1) DEFAULT NULL,
  `CODIFY_ALL_FLORIST_FLAG` char(1) DEFAULT NULL,
  `PRIORITY_FLAG` char(1) DEFAULT NULL,
  `CODIFICATION_REQUIRED` char(1) DEFAULT NULL,
  PRIMARY KEY (`CODIFICATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

------------------------------------------------------------------------------------
-- begin requested by Mark Campbell                              3/ 9/2016  --------
-- defect FHDC-39       (Mark  ) (189901)           --------------------------------
------------------------------------------------------------------------------------

