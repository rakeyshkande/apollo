------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                               4/13/2018  --------
-- User  task 10155   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------
Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','BATCH_COUNT','30','US_10155',to_date('13-APR-18','DD-MON-RR'),'US_10155',to_date('13-APR-18','DD-MON-RR'),'This is the configured batch of feeds to be fetched to send to Fees Micro Service');

Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','FEED_TYPE_LIST','serviceFeeOverride, serviceFee, SHIPPING_KEY_FEED, SOURCE_FEED','US_10155',to_date('13-APR-18','DD-MON-RR'),'US_10155',to_date('13-APR-18','DD-MON-RR'),'These are the configured Feed Types for which Fees feeds will be sent to Fees Micro Service');

  Insert into frp.global_parms (CONTEXT,NAME,VALUE,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,DESCRIPTION) values 
('FEES_FEEDS_CONFIG','SOURCE_CODES','350, 552, 14441, 16737','US_10155',to_date('11-APR-18','DD-MON-RR'),'US_10155',to_date('11-APR-18','DD-MON-RR'),'These are the configured source code values for which source codes feeds will be sent to Fees Micro Service');


ALTER TABLE ftd_apps.fees_feeds
  MODIFY status VARCHAR2(20);

drop trigger ftd_apps.FEES_FEED_TRIG_$;

------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                               4/13/2018  --------
-- User task 10155   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- Begin requested by Manasa Salla                               4/13/2018  --------
-- User  task US_11241  (pthakur)         ----------------------------------
------------------------------------------------------------------------------------

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('CLIENT_SERVICES', 'APOLLO_CLIENT_TLS_VERSIONS', 'TLSv1,TLSv1.1,TLSv1.2', 'US_11241', sysdate, 'US_11241', sysdate, 'Compatible TLS versions for Apollo-CAMS and Apollo-JCCAS');

------------------------------------------------------------------------------------
-- End   requested by Manasa Salla                               4/13/2018  --------
-- User task US_11241  (pthakur)         ----------------------------------
------------------------------------------------------------------------------------
