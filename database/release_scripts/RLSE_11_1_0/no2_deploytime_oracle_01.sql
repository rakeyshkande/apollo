------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               1/31/2017  --------
-- User  task 2931   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.product_ship_methods to joe;

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                               12/07/2017  --------
-- User task 2931   (mwoytus)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Kurella Swathi                               2/08/2018  --------
-- User  task 10155   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------

grant execute on ftd_apps.fees_feeds_pkg to OSP;

CREATE SEQUENCE ftd_apps.fuel_sur_seq
START WITH     1000
INCREMENT BY   1
NOCACHE
NOCYCLE;

CREATE SEQUENCE ftd_apps.mor_del_seq
START WITH     1000
INCREMENT BY   1
NOCACHE
NOCYCLE;


Insert into SITESCOPE.PROJECTS (PROJECT,DESCRIPTION,MAILSERVER_NAME) values ('FEES_FEED_ALERTS','Mail alert to be triggered when there is any error while sending fees feed to fees micro service','barracuda.ftdi.com');

Insert into SITESCOPE.PAGERS (PROJECT,PAGER_NUMBER) values ('FEES_FEED_ALERTS','Java_issues@ftdi.com');
Insert into SITESCOPE.PAGERS (PROJECT,PAGER_NUMBER) values ('FEES_FEED_ALERTS','scrub_alerts@ftdalerts.com');


CREATE TABLE "FTD_APPS"."FEES_FEEDS" 
   (           "FEED_ID" VARCHAR2(30) NOT NULL, 
               "FEED_XML" CLOB, 
               "FEED_TYPE" VARCHAR2(100), 
               "STATUS" VARCHAR2(10 BYTE) DEFAULT 'NEW', 
               "CREATED_ON" DATE, 
               "UPDATED_ON" DATE
   );

      
   
------------------------------------------------------------------------------------
-- End   requested by Kurella Swathi                               2/08/2018  --------
-- User task 10155   (pthakur)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               2/9/2018  --------
-- User  User Story FLTD-2932   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.product_index_xref to joe;
grant select on ftd_apps.product_index to joe;

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'FRESH_IMAGE_BASE_URL', 'http://www.ftdimg.com/pics/products/', 'Project_Fresh', 
                sysdate, 'Project_Fresh', sysdate, 'Base image URL for Project Fresh');

------------------------------------------------------------------------------------
-- End requested by Gary Sergey                               2/9/2018  --------
-- User  User Story FLTD-2932   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                               2/23/2018  --------
-- User  User Story FLTD-2931   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

ALTER TABLE ftd_apps.product_master ADD bullet_description varchar2(1024);
ALTER TABLE ftd_apps.product_master$ ADD bullet_description varchar2(1024);
ALTER TABLE ftd_apps.color_master ADD marker_type varchar2(20);

------------------------------------------------------------------------------------
-- End requested by Gary Sergey                               2/23/2018  --------
-- User  User Story FLTD-2931   (srizvi)         ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                3/ 5/2018  --------
-- User Story FLTD-2932   (mwoytus)               ----------------------------------
------------------------------------------------------------------------------------

grant execute on stats.stats_pkg to ftd_apps;

------------------------------------------------------------------------------------
-- End   requested by Gary Sergey                                3/ 5/2018  --------
-- User Story FLTD-2932   (mwoytus)               ----------------------------------
------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
-- Begin requested by Gary Sergey                                3/ 9/2018  --------
-- User Story FLTD-2931   (srizvi)               ----------------------------------
------------------------------------------------------------------------------------


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'FRESH_MSG_PRODUCER_URL', 'REPLACE ME', 'SYS', sysdate, 'SYS', sysdate, 
            'URL for microservice that adds entry to messaging service (Google Pubsub)');

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'FRESH_MSG_PRODUCER_TIMEOUT', '10', 'SYS', sysdate, 'SYS', sysdate, 
            'Timeout (in seconds) for sending to messaging service (Google Pubsub)');



INSERT INTO quartz_schema.pipeline (
            pipeline_id, group_id, description, 
              tool_tip, default_schedule, 
              default_payload, force_default_payload, queue_name, class_code, active_flag
) VALUES (
            'MSG_PRODUCER_FRESH','PDB', 'Send to Project Fresh Pubsub',
            'Sends message to Project Fresh PubSub topic (FRESH_MSG_PRODUCER|source|reason|id)', null,  
            'FRESH_MSG_PRODUCER|product|manual|', 'N', 'OJMS.PDB', 'SimpleJmsJob', 'Y'
      );


------------------------------------------------------------------------------------
-- End requested by Gary Sergey                                3/ 9/2018  --------
-- User Story FLTD-2931   (srizvi)               ----------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Begin requested by Garey Sergey                              03/16/2018  --------
-- User story FLTD-2931     (mwoytus)            -----------------------------------
------------------------------------------------------------------------------------

-- Scheduler definition (so we can manually trigger OJMS entries for all products that have changed since certain date)
--
INSERT INTO quartz_schema.pipeline ( pipeline_id, group_id, description, tool_tip, default_schedule, 
            default_payload, force_default_payload, queue_name, class_code, active_flag
      ) VALUES (
'MSG_PRODUCER_FRESH_ALL','PDB', 'Send all updated products to Project Fresh Pubsub',
'Enqueues all updated products (FRESH_MULTI_MSG_PRODUCER|source|reason|date) so they get sent to Pubsub', null,  
'FRESH_MULTI_MSG_PRODUCER|product|manual_all|YYYY-MM-DD HH24:MI', 'N', 'OJMS.PDB', 'SimpleJmsJob', 'Y'
      );

------------------------------------------------------------------------------------
-- End   requested by Garey Sergey                              03/16/2018  --------
-- User story FLTD-2931     (mwoytus)            -----------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
-- Begin   requested by Garey Sergey                              03/20/2018  --------
-- User story FLTD-2931     (srizvi)            -----------------------------------
------------------------------------------------------------------------------------

grant select on ftd_apps.product_source to joe;

INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON, DESCRIPTION) 
VALUES ('SERVICE', 'FRESH_TRIGGERS_ENABLED', 'N', 'SYS', sysdate, 'SYS', sysdate, 
               'Set to Y to enable all Project Fresh DB triggers');


------------------------------------------------------------------------------------
-- end   requested by Garey Sergey                              03/20/2018  --------
-- User story FLTD-2931     (srizvi)            -----------------------------------
------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
-- Begin   requested by Campbell, Mark                           03/25/2018  ---------------
-- User story 10705    (msalla)            -------------------------------------------------
--------------------------------------------------------------------------------------------

insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMITEM_FTP_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMITEM_OUTBOUND_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMPRICE_FTP_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMPRICE_OUTBOUND_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMUPSELL_FTP_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('PARTNER_REWARD_CONFIG','PRODFEEDBOOMUPSELL_OUTBOUND_SERVER','falcondg01.ftdi.com','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooitem','INTERVAL','trunc(sysdate)+1+(300/1440)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooitem','REMOVER','events.daemon_pkg.remove(''ins_partner_reward_prodbooitem'')','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooitem','SUBMITTER','events.daemon_pkg.submit(''ins_partner_reward_prodbooitem'',null,null)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodboopric','INTERVAL','trunc(sysdate)+1+(300/1440)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodboopric','REMOVER','events.daemon_pkg.remove(''ins_partner_reward_prodboopric'')','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodboopric','SUBMITTER','events.daemon_pkg.submit(''ins_partner_reward_prodboopric'',null,null)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooup','INTERVAL','trunc(sysdate)+1+(300/1440)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooup','REMOVER','events.daemon_pkg.remove(''ins_partner_reward_prodbooup'')','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');
insert into frp.global_parms values ('ins_partner_reward_prodbooup','SUBMITTER','events.daemon_pkg.submit(''ins_partner_reward_prodbooup'',null,null)','RLSE_11_1_0',sysdate,'RLSE_11_1_0',sysdate,'Boomerang File Setting');


insert into events.events values ('LOAD-PROD-DATA-FEED-BOOM-ITEM','PARTNER-REWARD','Creates Product Data Feed for Boomerang','Y');
insert into events.events values ('LOAD-PROD-DATA-FEED-BOOM-PRICE','PARTNER-REWARD','Creates Price Data Feed for Boomerang','Y');
insert into events.events values ('LOAD-PROD-DATA-FEED-BOOM-UPSELL','PARTNER-REWARD','Creates Upsell Data Feed for Boomerang','Y');

--------------------------------------------------------------------------------------------
-- end   requested by Campbell, Mark                           03/25/2018  -----------------
-- User story 10705    (msalla)            -------------------------------------------------
--------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
-- Begin   requested by Gary Sergey                              03/28/2018  ---------------
-- User story ?????    (mwoytus)           -------------------------------------------------
--------------------------------------------------------------------------------------------

truncate table ftd_apps.product_colors;
truncate table ftd_apps.color_master;

-- 1. log in to opal:3306. Make sure the truncates replicated -- row counts in mysql versions of product_colors and color_master 
--    should be zero. I'm pretty sure it WON'T so I've included truncates in the mysql script.
-- 2. log in to opal
-- 3. sudo -iu ogg
-- 4. cd $OGG_HOME
-- 5. ggsci
-- 6. stop pftd2
-- 7. Deploy changes to oracle.

alter table ftd_apps.color_master modify (color_master_id varchar2(8));
alter table ftd_apps.color_master modify (description varchar2(40));
alter table ftd_apps.product_colors modify (product_color varchar2(8));

-- 8. Deploy changes to opal:3306, from the mysql script
-- 9. In ggsci, start pftd2
-- 10. In ggsci, info all

--------------------------------------------------------------------------------------------
-- End     requested by Gary Sergey                              03/28/2018  ---------------
-- User story ?????    (mwoytus)           -------------------------------------------------
--------------------------------------------------------------------------------------------

