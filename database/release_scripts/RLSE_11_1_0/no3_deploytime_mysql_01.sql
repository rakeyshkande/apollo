------------------------------------------------------------------------------------

-- Begin requested by Tim Schmig                        03/12/2018  --------
--  Changeset 115675: 10365 - fix for go-to suspends   (syed Rizvi   )--------------  
----------------------------
--------------------------------------------------------


Compile function in opal

$/Consumer/Apollo/Releases/RLSE_11_1_0/database_mysql/pas/is_florist_suspended.fnc


------------------------------------------------------------------------------------

--  End requested by Tim Schmig                        03/12/2018           --------
--  Changeset 115675: 10365 - fix for go-to suspends   (syed Rizvi  ) --------------  
----------------------------
--------------------------------------------------------

--------------------------------------------------------------------------------------------
-- Begin   requested by Gary Sergey                              03/28/2018  ---------------
-- User story ?????    (mwoytus)           -------------------------------------------------
--------------------------------------------------------------------------------------------

use ftd_apps;
truncate table product_colors;
truncate table color_master;

alter table color_master modify column color_master_id varchar(8) not null;
alter table color_master modify column description varchar(40) not null;
alter table color_master add column marker_type varchar(20);
alter table product_colors modify column product_color varchar(8) not null;

--------------------------------------------------------------------------------------------
-- End     requested by Gary Sergey                              03/28/2018  ---------------
-- User story ?????    (mwoytus)           -------------------------------------------------
--------------------------------------------------------------------------------------------

