
--###########################
--####    R E A D M E    ####
--###########################

--After a module goes to production, it is difficult to accurately derive subsequent changes in CVS using tags
--and queries.  Once in production, the code base should be less volatile so we will manually track changes.
--
--This script contains Apollo 2.4.0 database udpates which have been applied to dev AFTER 2.4.0 went to QA (QA6).
--
--The script is tagged FIX_24_INT_RTB because the mods are in DEV.  
--
--To deploy these changes to QA:
--      1. If code, tag with FIX_XX_QA_RTB
--      2. Deploy to QA
--      3. Move reference from here to appropriate script (i.e. release_scripts.sql, README, etc.) and tag new version
--      3. Tag modules with RLSE tag
--     





---- END FIX 2.4.0 Dev_Mods_NOT_in_QA.sql -------------------
