---- FIX 2.4.0 RELEASE SCRIPT ------------------

--#################################################################################
-- PRE-DEPLOY ITEMS
--#################################################################################

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-certificate-filename','paypal_cert_live.p12','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-endpoint-environment','live','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','ap-report-full-error-flag','N','chu',sysdate,'chu',sysdate);


INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('accounting_reporting','pp-api-username',global.encryption.encrypt_it('dwhalen_api1.ftdi.com','ORCL_CCNUM_2007'),
'PayPal API username',sysdate,'chu',sysdate,'chu','ORCL_CCNUM_2007');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('accounting_reporting','pp-api-password',global.encryption.encrypt_it('TFQPS47NRNHF4U9W','ORCL_CCNUM_2007'),
'PayPal API user password',sysdate,'chu',sysdate,'chu','ORCL_CCNUM_2007');

INSERT INTO FRP.SECURE_CONFIG(CONTEXT, NAME, VALUE, DESCRIPTION, CREATED_ON,CREATED_BY,UPDATED_ON, UPDATED_BY, KEY_NAME)
VALUES ('accounting_reporting','pp-private-key-password',global.encryption.encrypt_it('ftd2pbuyer','ORCL_CCNUM_2007'),
'PayPal SSL private key Password',sysdate,'chu',sysdate,'chu','ORCL_CCNUM_2007');



INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name)
VALUES ('product_database', 'pif_ftp_login',
  global.encryption.encrypt_it('reports',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
  'Novator''s PIF logon for the corporate ftp server', sysdate, 'SYS', sysdate, 'SYS',
  frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));

INSERT INTO FRP.SECURE_CONFIG (context, name, value, description, created_on, created_by, updated_on, updated_by,key_name)
VALUES ('product_database', 'pif_ftp_password',
  global.encryption.encrypt_it('reports',frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
'Novator''s PIF password for the corporate ftp server', sysdate, 'SYS', sysdate, 'SYS',
frp.misc_pkg.get_global_parm_value('Ingrian','Current Key'));

INSERT INTO FRP.GLOBAL_PARMS g (g.CONTEXT, g.NAME, g.VALUE, g.CREATED_BY, g.CREATED_ON, g.UPDATED_BY, g.UPDATED_ON) VALUES 
('DELIVERY_CONFIRMATION_CONFIG','WEB_LOYALTY_EMAIL_SLOGAN',
'Remember to click below to claim your Special Reward for your order from Reservation Rewards!',
'DEFECT 3415',sysdate,'DEFECT 3415',SYSDATE);

insert into frp.global_parms
	(context, name, value, created_by, created_on, updated_by, updated_on) 
values
	('POP_CSP_CONFIG', 'TARGET_URL', 'http://www.exchangeshopping.org/ftd/orderstatus.ihtml', 'SYS', SYSDATE, 'SYS', SYSDATE);

-- Have the monitor scheduled by inserting records in the frp.global_parms table 
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('monitor_csp_price_variance', 'INTERVAL', 'SYSDATE + (30/1440)', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('monitor_csp_price_variance', 'REMOVER', 'pop.daemon_pkg.remove(''monitor_csp_price_variance'')', 'SYS', SYSDATE, 
'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('monitor_csp_price_variance', 'SUBMITTER', 'pop.daemon_pkg.submit(''monitor_csp_price_variance'')', 'SYS', SYSDATE, 
'SYS', SYSDATE);

-- insert into sitescope.pagers table for CSPI
INSERT INTO sitescope.pagers
SELECT 'CSPI', pager_number
  FROM sitescope.pagers
 WHERE project = 'WLMTI';

-- inserts into rpt.* tables for new ACC33 Price Variance CSP report
SET DEFINE OFF
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	100232,
	'Price Variance Report CSP',
	'This report will be used to identify differences between FTD.COM''s expected price and the price received on order files from partners. ',
	'Price_Variance_CSP',
	'U',
	'Char',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?', 
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC33_Price_Variance_Report_CSP.rdf',
	31,
	'Date range cannot exceed 31 days',
	'AcctMktgReportAccess',
	10,
	120,
	'Y',
	null,
	null,
	null);
	
-- insert into rpt.report_parm_ref table for PDST report for the existing Origin ID parameter
Insert into RPT.REPORT_PARM_REF (
	"REPORT_PARM_ID",
	"REPORT_ID",
	"SORT_ORDER",
	"REQUIRED_INDICATOR",
	"CREATED_ON",
	"CREATED_BY",
	"ALLOW_FUTURE_DATE_IND") 
values (
	100402,
	100232,
	1,
	'Y',
	sysdate,
	'ddesai',
	null);
SET DEFINE ON

-- insert threshold values for CSPI in pop.global_parms table (verify that it does not already exist in the rlse script)
insert into pop.partner (partner_id,partner_name) values ('CSPI','CSP');
INSERT INTO pop.global_parms (partner_id, name, value) VALUES ('CSPI', 'VARIANCE_MAX_ORDERS', 10);
INSERT INTO pop.global_parms (partner_id, name, value) VALUES ('CSPI', 'VARIANCE_CEILING', 0.02);
INSERT INTO pop.global_parms (partner_id, name, value) VALUES ('CSPI', 'VARIANCE_ORDER_LIMIT', 5);
INSERT INTO pop.global_parms (partner_id, name, value) VALUES ('CSPI', 'VARIANCE_TIME_PERIOD', 30);

Insert into frp.global_parms values
('LOAD_MEMBER_DATA_CONFIG', 'ENQUEUE_OFFSET_MINUTES', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);

REM New global parms for EFOS interface
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_DOWN_START_TIME', '22:45', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_DOWN_END_TIME', '03:30', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_ALERT_TIME', '04:00', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_SUNDAY_DOWN_START_TIME', '18:00', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_SUNDAY_DOWN_END_TIME', '03:30', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
 
Insert into frp.global_parms values
('MERCURY_INTERFACE_CONFIG', 'EFOS_SUNDAY_ALERT_TIME', '04:00', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);
Insert into frp.global_parms values
('LOAD_MEMBER_DATA_CONFIG', 'ENQUEUE_OFFSET_MINUTES', '0', 'MCAMPBEL',SYSDATE,'MCAMPBEL',SYSDATE);

CREATE SEQUENCE clean.ap_accounts_sq START WITH 100000 NOCYCLE NOORDER CACHE 200;

CREATE TABLE clean.ap_accounts (
ap_account_id     NUMBER(22),
ap_account_txt    VARCHAR2(200),
payment_method_id VARCHAR2(2),
customer_id       NUMBER) TABLESPACE clean_data;

ALTER TABLE clean.ap_accounts ADD constraint ap_accounts_pk PRIMARY KEY (ap_account_id) USING INDEX TABLESPACE clean_indx;
ALTER TABLE clean.ap_accounts ADD CONSTRAINT ap_accounts_fk1 FOREIGN KEY (payment_method_id) REFERENCES ftd_apps.payment_methods;
ALTER TABLE clean.ap_accounts ADD CONSTRAINT ap_accounts_fk2 FOREIGN KEY (customer_id) REFERENCES clean.customer;

CREATE INDEX clean.ap_accounts_n1 ON clean.ap_accounts (customer_id) TABLESPACE clean_indx;

COMMENT ON COLUMN clean.ap_accounts.ap_account_txt IS 'alternate payment account identifier.  For PayPal this is an email address';

grant select on clean.ap_accounts to scrub;

CREATE SEQUENCE clean.alt_pay_billing_header_sq START WITH 100000 NOCYCLE NOORDER CACHE 200;

CREATE TABLE clean.alt_pay_billing_header (
alt_pay_billing_header_id NUMBER(22),
payment_method_id         VARCHAR2(2),
processed_flag            CHAR(1) DEFAULT 'N' NOT NULL,
dispatched_count          NUMBER(22),
processed_count           NUMBER(22),
created_on                DATE,
billing_batch_date        DATE) TABLESPACE clean_data;

ALTER TABLE clean.alt_pay_billing_header ADD CONSTRAINT alt_pay_billing_header_pk PRIMARY KEY (alt_pay_billing_header_id)
USING INDEX TABLESPACE clean_indx;
ALTER TABLE clean.alt_pay_billing_header ADD CONSTRAINT alt_pay_billing_header_fk1 FOREIGN KEY (payment_method_id)
REFERENCES ftd_apps.payment_methods;
ALTER TABLE clean.alt_pay_billing_header ADD CONSTRAINT alt_pay_billing_header_ck1 CHECK (processed_flag IN ('Y','N'));

COMMENT ON COLUMN clean.alt_pay_billing_header.processed_flag IS
'set to Y by external monitor when we''ve finished EOD processing for this batch';
COMMENT ON COLUMN clean.alt_pay_billing_header.processed_count IS
'number of items in this batch that have completed EOD processing';
COMMENT ON COLUMN clean.alt_pay_billing_header.dispatched_count IS
'number of items in this batch that have been dispatched for EOD processing';

CREATE SEQUENCE clean.alt_pay_billing_detail_pp_sq START WITH 100000 NOCYCLE NOORDER CACHE 200;

CREATE TABLE clean.alt_pay_billing_detail_pp (
alt_pay_billing_detail_id NUMBER(22) NOT NULL,
alt_pay_billing_header_id NUMBER(22) NOT NULL,
payment_id                NUMBER,
payment_code              CHAR(1) NOT NULL,
master_order_number       VARCHAR2(100),
req_transaction_txt       VARCHAR2(100),
req_amt                   NUMBER(20,2),
res_transaction_txt       VARCHAR2(100),
res_ack_txt               VARCHAR2(100),
ack_error_txt             VARCHAR2(4000),
RES_CORRELATION_TXT       VARCHAR2(200),
res_gross_amt             NUMBER(20,2),
res_fee_amt               NUMBER(20,2),
res_net_amt               NUMBER(20,2),
created_on                DATE,
updated_on                DATE) TABLESPACE clean_data;

ALTER TABLE clean.alt_pay_billing_detail_pp ADD CONSTRAINT alt_pay_billing_detail_pp_pk PRIMARY KEY (alt_pay_billing_detail_id)
USING INDEX TABLESPACE clean_indx;
ALTER TABLE clean.alt_pay_billing_detail_pp ADD CONSTRAINT alt_pay_billing_detail_pp_ck1 CHECK (payment_code IN ('P','R'));
ALTER TABLE clean.alt_pay_billing_detail_pp ADD CONSTRAINT alt_pay_billing_detail_pp_fk1 FOREIGN KEY (alt_pay_billing_header_id)
REFERENCES clean.alt_pay_billing_header;
ALTER TABLE clean.alt_pay_billing_detail_pp ADD CONSTRAINT alt_pay_billing_detail_pp_fk2 FOREIGN KEY (payment_id)
REFERENCES clean.payments;
ALTER TABLE clean.alt_pay_billing_detail_pp ADD CONSTRAINT alt_pay_billing_detail_pp_fk3 FOREIGN KEY (master_order_number)
REFERENCES clean.orders(master_order_number);

COMMENT ON TABLE clean.alt_pay_billing_detail_pp IS 'alt pay detail table for PayPal, each row = one cart';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.alt_pay_billing_header_id IS 'link to parent';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.payment_id IS 'foreign key back to clean.payments';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.payment_code IS 'P=payment, R=refund';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.req_transaction_txt IS
'we send this info to alt pay partner to identify the transaction';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.req_amt IS 'dollar amount we''re trying to settle for';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.res_transaction_txt IS 'transaction identifier received from alt pay partner';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.res_ack_txt IS 'additional transaction info received from alt pay partner';
COMMENT ON COLUMN clean.alt_pay_billing_detail_pp.ack_error_txt IS
'if the alt pay partner response is accompanied by errors, they go here';

CREATE INDEX clean.alt_pay_billing_detail_pp_n1 ON clean.alt_pay_billing_detail_pp (alt_pay_billing_header_id) 
TABLESPACE clean_indx;
CREATE INDEX clean.alt_pay_billing_detail_pp_n2 ON clean.alt_pay_billing_detail_pp (payment_id) TABLESPACE clean_indx;
CREATE INDEX clean.alt_pay_billing_detail_pp_n3 ON clean.alt_pay_billing_detail_pp (master_order_number) TABLESPACE clean_indx;

CREATE TABLE CLEAN.QUEUE_COUNTS_CACHE
   (	
       QUEUE_TYPE                  VARCHAR2(20), 
	DESCRIPTION                 VARCHAR2(100), 
	QUEUE_INDICATOR             VARCHAR2(20), 
	SORT_ORDER                  NUMBER(22),
       TAGGED_COUNT                NUMBER(22),
       UNTAGGED_COUNT              NUMBER(22),
       TAGGED_UNATTACHED_COUNT     NUMBER(22),
       UNTAGGED_UNATTACHED_COUNT   NUMBER(22),
       UPDATED_ON                  DATE
   );

COMMENT ON TABLE clean.queue_counts_cache IS 
'Table to store current queue counts.  Meant to be a cache for counts since calculation of them is expensive.  Hence this table is to be updated periodically via a scheduled process.';

CREATE TABLE CLEAN.QUEUE_GROUP_COUNTS_CACHE
   (	
       CS_GROUP_ID                 VARCHAR2(6), 
	DESCRIPTION                 VARCHAR2(50), 
       TAGGED_COUNT                NUMBER(22),
       UPDATED_ON                  DATE
   );

COMMENT ON TABLE clean.queue_group_counts_cache IS 
'Table to store current queue counts for CS groups.  Meant to be a cache for counts since calculation of them is expensive.  Hence this table is to be updated periodically via a scheduled process.';  

CREATE TABLE QUARTZ_SCHEMA.APOLLO_JOBS (CLASS_CODE VARCHAR2(32) NOT NULL,
                                        CLASS_PACKAGE VARCHAR2(256) NOT NULL) TABLESPACE TOOLS;

alter table quartz_schema.apollo_jobs add CONSTRAINT APOLLO_JOBS_PK PRIMARY KEY(CLASS_CODE) using index tablespace tools;

alter table quartz_schema.apollo_jobs add CONSTRAINT APOLLO_JOBS_U1 UNIQUE(CLASS_PACKAGE) using index tablespace tools;

INSERT INTO QUARTZ_SCHEMA.APOLLO_JOBS (CLASS_CODE, CLASS_PACKAGE) 
VALUES('SimpleJmsJob','com.ftd.scheduler.job.SimpleJmsJob');

INSERT INTO QUARTZ_SCHEMA.APOLLO_JOBS (CLASS_CODE, CLASS_PACKAGE)
VALUES('SQLJob','com.ftd.scheduler.job.SQLJob');

CREATE TABLE QUARTZ_SCHEMA.PARAMETERS 
   (PARAMETER_CODE         VARCHAR2(32) NOT NULL,
    ALWAYS_REQUIRED_FLAG   CHAR(1)  DEFAULT 'N' NOT NULL,
  CONSTRAINT PARAMETERS_PK  PRIMARY KEY(PARAMETER_CODE) USING INDEX TABLESPACE TOOLS,
  CONSTRAINT PARMAMETERS_CK1 CHECK(ALWAYS_REQUIRED_FLAG IN ('Y','N'))
  )
  TABLESPACE TOOLS;

CREATE TABLE QUARTZ_SCHEMA.CLASS_PARAMETERS 
   (CLASS_CODE        VARCHAR2(32) NOT NULL,
    PARAMETER_CODE    VARCHAR2(32) NOT NULL,
    REQUIRED_FLAG     CHAR(1) DEFAULT 'N' NOT NULL,
  CONSTRAINT CLASS_PARAMETERS_PK PRIMARY KEY(CLASS_CODE,PARAMETER_CODE)
         using index tablespace tools,
  CONSTRAINT CLASS_PARAMETERS_CK1 CHECK(REQUIRED_FLAG IN('Y','N')),
  CONSTRAINT CLASS_PARAMETERS_FK1 FOREIGN KEY(CLASS_CODE) REFERENCES QUARTZ_SCHEMA.APOLLO_JOBS(CLASS_CODE),
  CONSTRAINT CLASS_PARAMETERS_FK2 FOREIGN KEY(PARAMETER_CODE) REFERENCES QUARTZ_SCHEMA.PARAMETERS(PARAMETER_CODE)
) 
TABLESPACE TOOLS;

CREATE TABLE quartz_schema.email_addresses 
   (EMAIL_ADDRESS        VARCHAR2(64) NOT NULL,
 CONSTRAINT EMAIL_ADDRESSES_PK PRIMARY KEY(EMAIL_ADDRESS) using index tablespace TOOLS)
 tablespace tools;
  
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('CLASS_CODE','Y');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('DEFAULT_SCHEDULE','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('JMS_QUEUE_NAME','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('MODULE','Y');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('PAYLOAD','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('PIPELINE','Y');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SCHEDULE_TYPE','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_DEFAULT_EMAIL_BODY',
'N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES 
('SQL_DEFAULT_EMAIL_SUBJECT','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES 
('SQL_EMAIL_ADDRESSES','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_EMAIL_ON_ERROR','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_EMAIL_ON_SUCCESS',
'N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_PARAMETERS','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_STATEMENT_ID','N');
INSERT INTO QUARTZ_SCHEMA.PARAMETERS (PARAMETERS.PARAMETER_CODE, PARAMETERS.ALWAYS_REQUIRED_FLAG) VALUES ('SQL_STATEMENT_NAME',
'N');

INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, 
REQUIRED_FLAG) VALUES ('SimpleJmsJob','PAYLOAD','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','PIPELINE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','JMS_QUEUE_NAME','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','DEFAULT_SCHEDULE',
'N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','MODULE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','CLASS_CODE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SimpleJmsJob','SCHEDULE_TYPE','N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_PARAMETERS','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','MODULE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES 
('SQLJob','SQL_DEFAULT_EMAIL_SUBJECT','N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_DEFAULT_EMAIL_BODY',
'N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','CLASS_CODE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_STATEMENT_NAME','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SCHEDULE_TYPE','N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','PIPELINE','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','DEFAULT_SCHEDULE','N');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_EMAIL_ADDRESSES','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_STATEMENT_ID','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_EMAIL_ON_ERROR','Y');
INSERT INTO QUARTZ_SCHEMA.CLASS_PARAMETERS (CLASS_CODE, PARAMETER_CODE, REQUIRED_FLAG) VALUES ('SQLJob','SQL_EMAIL_ON_SUCCESS',
'Y');

-- Defect 1692 
insert into FRP.GLOBAL_PARMS 
	(CONTEXT,NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
values 
	('SCRUB_CONFIG', 'DAYS_LOCK_PENDING_REMOVED_ORDER', '14', 'SYS', sysdate, 'SYS', sysdate)
;

create table FTD_APPS.PAYMENT_METHODS_TEXT (
PAYMENT_METHOD_ID Varchar2 (2) NOT NULL,
TYPE_TXT          Varchar2(25) NOT NULL,
TEXT_TXT          Varchar2(50) NOT NULL) tablespace ftd_apps_data;

comment on table ftd_apps.payment_methods_text is 
'store text used within correspondence to describe a payment method.  Ex. PP text will be PayPal Account.';

alter table ftd_apps.payment_methods_text add constraint payment_methods_text_pk primary key (payment_method_id, type_txt)
using index tablespace ftd_apps_indx;

alter table ftd_apps.payment_methods_text add constraint payment_methods_text_fk1 Foreign key (payment_method_id)
references FTD_APPS.PAYMENT_METHODS;
 
comment on column ftd_apps.payment_methods_text.type_txt is 'Type of correspondence.  Ex.  generic, email, message, letter, etc.';
 
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ap_eod_completed_monitor',
       'SUBMITTER',
       'clean.daemon_pkg.submit(''ap_eod_completed_monitor'')' ,
       'SYS',sysdate,'SYS',sysdate from dual;
       
insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ap_eod_completed_monitor',
       'REMOVER',
       'clean.daemon_pkg.remove(''ap_eod_completed_monitor'')' ,
       'SYS',sysdate,'SYS',sysdate from dual;

insert into frp.global_parms 
(CONTEXT, NAME, VALUE ,CREATED_BY, CREATED_ON , UPDATED_BY, UPDATED_ON )
select 'ap_eod_completed_monitor',
       'INTERVAL',
       'TRUNC(SYSDATE)+1+(165/1440)' ,
       'SYS',sysdate,'SYS',sysdate from dual;

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('SCRUB_CONFIG','PAY_PAL_PRE_EDIT_WARNING_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','DISPATCH_JDE_EOD_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','DISPATCH_ALTPAY_EOD_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','DISPATCH_PAYPAL_EOD_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','DISPATCH_ALTPAY_REPORT_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','ALTPAY_REPORT_DELAY','21600','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_CONNECT_TIMER_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_CONNECT_TIMER_MS','1800000','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_CONNECT_RETRY_COUNT','10','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_RES_ERROR_SIZE','2000','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PP_TRANSACTION_SEARCH_MONTH_SPAN','12','chu',sysdate,'chu',sysdate);

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-certificate-file-location','/home/oracle/.ssh/','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-endpoint-environment','sandbox','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-max-capture-amt','10000.00','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','ap-report-full-error-flag','Y','chu',sysdate,'chu',sysdate);

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','pp-certificate-filename','paypal_cert_buyer.p12','chu',sysdate,'chu',sysdate);

insert into frp.global_parms values
   ('CUSTOMER_ORDER_MANAGEMENT', 'MAX_MOUSEOVER_COMMENT_LEN', '400', 'SYS', sysdate, 'SYS', sysdate);
insert into frp.global_parms values
   ('CUSTOMER_ORDER_MANAGEMENT', 'MAX_MOUSEOVER_EMAIL_LEN', '600', 'SYS', sysdate, 'SYS', sysdate);

Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	100234,
	'Invoice Report Motorola',
	'This report will be used to print invoices for all orders billed for a particular source code and a selected prior month. ',
	'Invoice_Motorola',
	'U',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?', 
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC34_Motorola_Invoice.rdf',
	null,
	null,
	'AcctMktgReportAccess',
	10,
	120,
	'Y',
	null,
	null,
	null);

Insert into RPT.REPORT_PARM
(REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values (100234, 'Source Code', 'S', 'p_source_code', SYSDATE, 'ddesai', SYSDATE, 'ddesai', null);

Insert into RPT.REPORT_PARM
(REPORT_PARM_ID, DISPLAY_NAME, REPORT_PARM_TYPE, ORACLE_PARM_NAME, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, VALIDATION)
 Values (100235, 'Month', 'S', 'p_month', SYSDATE, 'ddesai', SYSDATE, 'ddesai', null);
	
Insert into RPT.REPORT_PARM_REF
(REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values (100234, 100234, 1, 'Y', SYSDATE, 'ddesai', null);

Insert into RPT.REPORT_PARM_REF
(REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values (100235, 100234, 2, 'Y', SYSDATE, 'ddesai', null);
	
Insert into RPT.REPORT_PARM_VALUE
(REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values (1002341, SYSDATE, 'ddesai', SYSDATE, 'ddesai', null, 'GET_SOURCE_CODES', NULL);

Insert into RPT.REPORT_PARM_VALUE
(REPORT_PARM_VALUE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, DISPLAY_TEXT, DB_STATEMENT_ID, DEFAULT_FORM_VALUE)
 Values (1002351, SYSDATE, 'ddesai', SYSDATE, 'ddesai', null, 'GET_PRIOR_MONTHS', NULL);
	
Insert into RPT.REPORT_PARM_VALUE_REF
(REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values (100234, 1002341, 1, SYSDATE, 'ddesai');

Insert into RPT.REPORT_PARM_VALUE_REF
(REPORT_PARM_ID, REPORT_PARM_VALUE_ID, SORT_ORDER, CREATED_ON, CREATED_BY)
 Values (100235, 1002351, 2, SYSDATE, 'ddesai');
	
-- inserts into rpt.* tables for ACC35 Motorola Credit Memo report
Insert into RPT.REPORT (
	"REPORT_ID",
	"NAME",
	"DESCRIPTION",
	"REPORT_FILE_PREFIX",
	"REPORT_TYPE",
	"REPORT_OUTPUT_TYPE",
	"REPORT_CATEGORY",
	"SERVER_NAME",
	"HOLIDAY_INDICATOR",
	"STATUS",
	"CREATED_ON",
	"CREATED_BY",
	"UPDATED_ON",
	"UPDATED_BY",
	"ORACLE_RDF",
	"REPORT_DAYS_SPAN",
	"NOTES",
	"ACL_NAME",
	"RETENTION_DAYS",
	"RUN_TIME_OFFSET",
	"END_OF_DAY_REQUIRED",
	"SCHEDULE_DAY",
	"SCHEDULE_TYPE",
	"SCHEDULE_PARM_VALUE") 
values (
	100235,
	'Credit Memo Report Motorola',
	'This report will be used to print refunds for all orders for a particular source code and a selected prior month. ',
	'Credit_Memo_Motorola',
	'U',
	'Both',
	'Acct',
	'http://apolloreports.ftdi.com/reports/rwservlet?', 
	'Y',
	'Active',
	sysdate,
	'ddesai',
	sysdate,
	'ddesai',
	'ACC35_Motorola_Credit_Memo.rdf',
	null,
	null,
	'AcctMktgReportAccess',
	10,
	120,
	'Y',
	null,
	null,
	null);

Insert into RPT.REPORT_PARM_REF
(REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values (100234, 100235, 1, 'Y', SYSDATE, 'ddesai', null);

Insert into RPT.REPORT_PARM_REF
(REPORT_PARM_ID, REPORT_ID, SORT_ORDER, REQUIRED_INDICATOR, CREATED_ON, CREATED_BY, ALLOW_FUTURE_DATE_IND)
 Values (100235, 100235, 2, 'Y', SYSDATE, 'ddesai', null);

   INSERT INTO FRP.GLOBAL_PARMS (GLOBAL_PARMS.CONTEXT, GLOBAL_PARMS.NAME, 
   GLOBAL_PARMS.VALUE, GLOBAL_PARMS.CREATED_BY, GLOBAL_PARMS.CREATED_ON, 
   GLOBAL_PARMS.UPDATED_BY, GLOBAL_PARMS.UPDATED_ON) 
   VALUES('OPS_ADMIN_CONFIG','carrier_zip_block_url','http://consumer.ftdi.com/pdb/showDeliveryZipMaintPage.do','DEFECT 3431',
sysdate,'DEFECT 3431',sysdate);

insert into frp.global_parms (context, name, value, created_by, created_on, updated_by, updated_on)
values('POP_CSP_CONFIG', 'TARGET_URL', 'http://www.exchangeshopping.org/ftd/processstat.ihtml', 'SYS', SYSDATE, 'SYS', SYSDATE)


INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, "VALUE", CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) VALUES 
('SHIPPING_PARMS','MISSING_STATUS_DAYS','7','DEFECT 3350',sysdate,'DEFECT 3350',sysdate);
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, "VALUE", CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) VALUES 
('SHIPPING_PARMS','MISSING_STATUS_MAX_REQUESTS','100','DEFECT 3350',sysdate,'DEFECT 3350',sysdate);
INSERT INTO FRP.GLOBAL_PARMS (CONTEXT, NAME, "VALUE", CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) VALUES 
('SHIPPING_PARMS','MISSING_STATUS_QUEUE_DELAY_SECONDS','600','DEFECT 3350',sysdate,'DEFECT 3350',sysdate);

 grant select on frp.global_parms to venus;


-- Defect 3404 
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('queue_pkg.update_queue_counts', 'INTERVAL', 'SYSDATE + (15/1440)', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
values ('queue_pkg.update_queue_counts', 'REMOVER', 'clean.daemon_pkg.remove(''queue_pkg.update_queue_counts'')', 'SYS', SYSDATE, 
'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
values ('queue_pkg.update_queue_counts', 'SUBMITTER', 'clean.daemon_pkg.submit(''queue_pkg.update_queue_counts'')', 'SYS', 
SYSDATE, 'SYS', SYSDATE);
-- JOB WILL BE SUBMITTED VIA README SCRIPT
-- END Defect 3404



-- Defect 1962
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('expire_pending_orders', 'INTERVAL', 'trunc(SYSDATE + 1) + (20/1440)', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
values ('expire_pending_orders', 'REMOVER', 'scrub.daemon_pkg.remove(''expire_pending_orders'')', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
values ('expire_pending_orders', 'SUBMITTER', 'scrub.daemon_pkg.submit(''expire_pending_orders'')', 'SYS', SYSDATE, 'SYS', 
SYSDATE);
-- JOB WILL BE SUBMITTED VIA README SCRIPT
-- END Defect 1962

-- defect 3525
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_EOD_RUNDATE_OFFSET','-1','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_API_CONNECTION_TIMEOUT','60000','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_TEST_CONNECTION_FLAG','Y','chu',sysdate,'chu',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','PAY_PAL_TEST_TRANSACTION_ID','9485696019232805Y','chu',sysdate,'chu',sysdate);

-- end defect 3525

--------------------------
-- INCOMING FILE HANDLING
--------------------------
CREATE TABLE FRP.FILE_ARCHIVE 
     (FILE_ARCHIVE_ID   NUMBER(22)    NOT NULL,
      CONTEXT           VARCHAR2(100) NOT NULL,
      NAME              VARCHAR2(100) NOT NULL,
      FILE_NAME         VARCHAR2(256) NOT NULL,
      FILE_DATA_TXT     CLOB          NOT NULL,
      CREATED_ON        DATE          NOT NULL,
      CREATED_BY        VARCHAR2(100) NOT NULL,
   CONSTRAINT FILE_ARCHIVE_PK PRIMARY KEY(FILE_ARCHIVE_ID) using index tablespace frp_indx)
   tablespace frp_data
   LOB (file_data_txt) store as file_archive_file_data_lob (tablespace frp_data disable storage in row CACHE READS LOGGING);


CREATE TABLE FRP.FILE_ARCHIVE_PARAMETERS 
     (CONTEXT               VARCHAR2(100) NOT NULL,
      NAME                  VARCHAR2(100) NOT NULL,
      DAYS_TO_HOLD_QTY      NUMBER(22),
      LAST_PURGE_DATE       DATE          NOT NULL,
   CONSTRAINT FILE_ARCHIVE_PARAMETERS_PK PRIMARY KEY(CONTEXT,NAME) using index tablespace frp_indx)
   tablespace frp_data;


CREATE SEQUENCE FRP.FILE_ARCHIVE_SQ
start with 1 INCREMENT BY 1 NOCYCLE CACHE 200;

--------------------------
-- END INCOMING FILE HANDLING
--------------------------



create table pop.email_subscription
     (master_order_number           varchar2(100)  NOT NULL,
      partner_id                    VARCHAR2(10)   NOT NULL,
      email_address                 VARCHAR2(55)   NOT NULL,
      subscription_status_flag      varchar2(1)  DEFAULT 'N'  NOT NULL,
      email_subscr_updated_flag     varchar2(1)  DEFAULT 'N'  NOT NULL,
      created_on                    date           NOT NULL,
      created_by                    VARCHAR2(100)  NOT NULL,
      updated_on                    date           NOT NULL,
      updated_by                    VARCHAR2(100)  NOT NULL,
   constraint email_subscription_pk PRIMARY KEY (master_order_number) USING INDEX TABLESPACE amazon_i,
   constraint email_subscription_ck1 CHECK(subscription_status_flag IN('Y','N')),
   constraint email_subscription_ck2 CHECK(email_subscr_updated_flag IN('Y','N')))
   tablespace amazon;

create bitmap index pop.email_subscription_nb1 on pop.email_subscription (email_subscr_updated_flag) tablespace amazon_i 
compute statistics;

COMMENT ON TABLE  pop.email_subscription is 'Table used to store email subscription choices made at the time of an order';         
COMMENT ON COLUMN pop.email_subscription.master_order_number is 'Master Order Number from the original order';          
COMMENT ON COLUMN pop.email_subscription.partner_id is 'Partner ID from the original order';         
COMMENT ON COLUMN pop.email_subscription.email_address is 'Email address tied to the order.  Kept here for analysis only.';         
COMMENT ON COLUMN pop.email_subscription.subscription_status_flag   is 
'Flag to denote whether the customer has chosen to subscribe to our emails.';
COMMENT ON COLUMN pop.email_subscription.email_subscr_updated_flag   is 
'Flag to denote whether the email subscription has been applied to clean.email yet.';
COMMENT ON COLUMN pop.email_subscription.created_on is 'Date/Time the row was created';               
COMMENT ON COLUMN pop.email_subscription.created_by is 'Username of person or process creating the row';                  
COMMENT ON COLUMN pop.email_subscription.updated_on is   'Date/Time the row was last updated';                
COMMENT ON COLUMN pop.email_subscription.updated_by is 'Username of person or process last updating the row';                  


-- Schedule PopEmailUpdate as a job
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('PopEmailUpdate', 'INTERVAL', 'trunc(SYSDATE + 1) + (270/1440)', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('PopEmailUpdate', 'REMOVER', 'pop.daemon_pkg.remove(''PopEmailUpdate'')', 'SYS', SYSDATE, 'SYS', SYSDATE);
INSERT INTO frp.global_parms (context,name,value,created_by,created_on,updated_by,updated_on) 
	values ('PopEmailUpdate', 'SUBMITTER', 'pop.daemon_pkg.submit(''PopEmailUpdate'')', 'SYS', SYSDATE, 'SYS', SYSDATE);
-- END PopEmailUpdate JOB Setup = Job WILL BE SUBMITTED VIA README SCRIPT

INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('ACCOUNTING_CONFIG','EJB_PROVIDER_URL','opmn:ormi://consumer.ftdi.com:OC4J_ACCOUNTING/accountingreporting','SYS',sysdate,
'SYS',sysdate);
INSERT INTO FRP.GLOBAL_PARMS(CONTEXT, NAME, VALUE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
VALUES('OPS_ADMIN_CONFIG','eodutil_url','http://consumer.ftdi.com/accountingreporting/EODUtil.do','SYS',sysdate,'SYS',sysdate);


create table sitescope.projects 
(PROJECT            VARCHAR2(30)  NOT NULL,
 MAILSERVER_NAME    VARCHAR2(100)  NOT NULL,
  CONSTRAINT projects_pk  PRIMARY KEY(project) USING INDEX TABLESPACE USERS,
  CONSTRAINT projects_ck1 CHECK(mailserver_name IN ('mailserver.ftdi.com','alerts1.ftdalerts.com'))
)
TABLESPACE USERS;


insert into sitescope.projects (project,mailserver_name) values ('SCRUB_ALERTS','alerts1.ftdalerts.com');
insert into sitescope.projects (project,mailserver_name) values ('SCRUB_NOPAGE_ALERTS','alerts1.ftdalerts.com');

-- Get all current projects everything else now
insert into sitescope.projects (project,mailserver_name)
   select distinct project, 'mailserver.ftdi.com' from sitescope.pagers
      where project not in ('SCRUB_ALERTS','SCRUB_NOPAGE_ALERTS');
      
-- MANUALLY COPY CURRENT SCRUB to SCRUB_ALERTS with ftdalerts.com in the pager_number
-- MANUALLY COPY CURRENT SCRUB_NOPAGE to SCRUB_NOPAGE_ALERTS with ftdalerts.com in the pager_number

alter table sitescope.pagers add CONSTRAINT pagers_fk1 FOREIGN KEY(project) REFERENCES sitescope.projects(project);

grant select on sitescope.pagers to ops$oracle;
grant select on sitescope.projects to ops$oracle;
grant execute on utl_smtp to ops$oracle;   -- AS SYS (not necessary in prod)
grant execute on utl_tcp to ops$oracle;    -- AS SYS (not necessary in prod)

--1962 - backout global parm added for PayPal utility screen (only used by IT)
delete from FRP.GLOBAL_PARMS where CONTEXT = 'ACCOUNTING_CONFIG' and NAME = 'EJB_PROVIDER_URL';



--#################################################################################
-- END PRE-DEPLOY ITEMS
--#################################################################################


INSERT INTO CLEAN.MESSAGE_TOKENS (TOKEN_ID,COMPANY_ID,TOKEN_TYPE,TOKEN_VALUE) VALUES 
('web.loyalty.url','ALL','value','/root/web_loyalty_url/text()');
-- End defect 3415

ALTER TABLE POP.PRICE_VARIANCE ADD (
            FTD_DISCOUNT_AMT NUMBER(12,2) DEFAULT 0 NOT NULL, 
            POP_DISCOUNT_AMT NUMBER(12,2) DEFAULT 0 NOT NULL);

-- 1962 Alternate payment - PayPal implementation
ALTER TABLE scrub.payments ADD (
ap_account_txt       VARCHAR2(3000),
ap_auth_txt          VARCHAR2(3000),
orig_auth_amount_txt VARCHAR2(3000));

COMMENT ON COLUMN scrub.payments.ap_account_txt IS 'alternate payment account identifier.  For PayPal this is an email address';
COMMENT ON COLUMN scrub.payments.ap_auth_txt IS 'alternate payment auth string';
COMMENT ON COLUMN scrub.payments.orig_auth_amount_txt IS 'amount authorized for alt payment method';

ALTER TABLE clean.payments ADD (
ap_account_id  NUMBER(22),
ap_auth_txt    VARCHAR2(100),
ap_capture_txt VARCHAR2(100));

ALTER TABLE clean.payments ADD CONSTRAINT payments_ap_accounts_fk FOREIGN KEY (ap_account_id) REFERENCES clean.ap_accounts;

COMMENT ON COLUMN clean.payments.ap_account_id IS 'foreign key to clean.ap_accounts';
COMMENT ON COLUMN clean.payments.ap_auth_txt IS 'alternate payment auth string';
COMMENT ON COLUMN clean.payments.ap_capture_txt IS 'PayPal additional string that''s sent after settlement';

CREATE INDEX clean.payments_n7 ON clean.payments (ap_account_id) TABLESPACE clean_indx compute statistics;

alter table ftd_apps.payment_methods add (over_auth_allowed_pct number);
comment on column ftd_apps.payment_methods.over_auth_allowed_pct is
'percentage we can bill over the authorized amount; for 10% overcharge allowed, enter 10 in this field';

alter table CLEAN.REFUND add (AP_REFUND_TRANS_TXT VARCHAR2(200));

INSERT INTO QUARTZ_SCHEMA.GROUPS (GROUPS.GROUP_ID, GROUPS.DESCRIPTION) VALUES('SQL','SQL');

ALTER TABLE QUARTZ_SCHEMA.PIPELINE ADD (CLASS_CODE VARCHAR2(32) DEFAULT 'SimpleJmsJob' NOT NULL);
alter table quartz_schema.pipeline add CONSTRAINT PIPELINE_FK1 FOREIGN KEY(CLASS_CODE)
    REFERENCES QUARTZ_SCHEMA.APOLLO_JOBS;


INSERT INTO QUARTZ_SCHEMA.PIPELINE (
PIPELINE.PIPELINE_ID, 
PIPELINE.GROUP_ID, 
PIPELINE.DESCRIPTION, 
PIPELINE.TOOL_TIP, 
PIPELINE.DEFAULT_SCHEDULE, 
PIPELINE.DEFAULT_PAYLOAD, 
PIPELINE.FORCE_DEFAULT_PAYLOAD, 
PIPELINE.QUEUE_NAME, 
PIPELINE.CLASS_CODE)
VALUES('SQL','SQL','Misc. SQL jobs','Run a SQL job and optionally send an email notification',null,null,'N',null,'SQLJob');

INSERT INTO QUARTZ_SCHEMA.PIPELINE (
PIPELINE.PIPELINE_ID, 
PIPELINE.GROUP_ID, 
PIPELINE.DESCRIPTION, 
PIPELINE.TOOL_TIP, 
PIPELINE.DEFAULT_SCHEDULE, 
PIPELINE.DEFAULT_PAYLOAD, 
PIPELINE.FORCE_DEFAULT_PAYLOAD, 
PIPELINE.QUEUE_NAME, 
PIPELINE.CLASS_CODE)
VALUES('Monitor','SQL','SQL monitoring jobs','Run a SQL job and optionally send an email notification',null,null,'N',null,
'SQLJob');

INSERT INTO QUARTZ_SCHEMA.PIPELINE (
PIPELINE.PIPELINE_ID, 
PIPELINE.GROUP_ID, 
PIPELINE.DESCRIPTION, 
PIPELINE.TOOL_TIP, 
PIPELINE.DEFAULT_SCHEDULE, 
PIPELINE.DEFAULT_PAYLOAD, 
PIPELINE.FORCE_DEFAULT_PAYLOAD, 
PIPELINE.QUEUE_NAME, 
PIPELINE.CLASS_CODE)
VALUES('Monitor','SHIPPROCESSING','SQL monitoring jobs','Run a SQL job and optionally send an email notification',null,null,'N',
null,'SQLJob');

INSERT INTO QUARTZ_SCHEMA.PIPELINE (
PIPELINE.PIPELINE_ID, 
PIPELINE.GROUP_ID, 
PIPELINE.DESCRIPTION, 
PIPELINE.TOOL_TIP, 
PIPELINE.DEFAULT_SCHEDULE, 
PIPELINE.DEFAULT_PAYLOAD, 
PIPELINE.FORCE_DEFAULT_PAYLOAD, 
PIPELINE.QUEUE_NAME, 
PIPELINE.CLASS_CODE)
VALUES('SQL','SHIPPROCESSING','SQL monitoring jobs','Run a SQL job and optionally send an email notification',null,null,'N',null,
'SQLJob');

--**********************************************************************
--Add a new disposition
--**********************************************************************
INSERT INTO SCRUB.DISPOSITION_CODES
    (disposition_id,disposition_description,pending_flag,remove_flag)
values
    ('L','Locked from Re-Instate','N','Y')
;

--**********************************************************************
--Issue Grants to order_bills and order_details for Scrub
--**********************************************************************
grant select on clean.order_bills to scrub;
grant select on clean.order_details to scrub;
-- END Defect 1692 

alter table clean.stock_email add (use_no_reply_flag char(1) default 'N');
alter table clean.stock_email add constraint stock_email_ck2 check(use_no_reply_flag in ('Y','N'));

ALTER TABLE scrub.reprocess_orders_log 
ADD (reprocessed_flag CHAR(1) DEFAULT 'Y' NOT NULL);
alter table scrub.reprocess_orders_log add constraint reprocess_orders_log_ck1 check (reprocessed_flag IN ('Y','N'));

COMMENT ON COLUMN scrub.reprocess_orders_log.reprocessed_flag IS 
'Y indicates order was submitted for reprocessing, N indicates entry was simply removed from order_exceptions table';

drop trigger pop.price_variance_monitor;

drop  TABLE RPT.MONTHS ;
CREATE TABLE RPT.MONTHS 
    (CALENDAR_year      number(4),
     calendar_month     number(2),
   constraint months_pk primary key (calendar_month,calendar_year) using index,
   constraint months_ck1   CHECK(calendar_year between 1997 and 2100),
   constraint months_ck2   check(calendar_month between 1 and 12)
     ) tablespace rpt_data;

-- LOAD THE RPT.MONTHS TABLE
declare
   y number;
   m number;
begin
   for y in 1997..2020 LOOP
      for m in 1..12 LOOP
         insert into rpt.months select y,m from dual;
      END LOOP;
   END LOOP;
end;
/

--Populate payment method text data
INSERT INTO clean.message_tokens (token_id, company_id, token_type, token_value, default_value)
VALUES('payment.method.text', 'ALL', 'value', '//payment_method_text/text()', null);

INSERT INTO FTD_APPS.PAYMENT_METHODS(payment_method_id,description,payment_type,card_id,
         has_expiration_date,over_auth_allowed_pct)
values('PP','PayPal','P','PP','N',15);


insert into ftd_apps.payment_methods_text VALUES('AX', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('DC', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('DI', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('MC', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('VI', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('CB', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('JP', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('MS', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('GC', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('PC', 'generic', 'Credit Card');
insert into ftd_apps.payment_methods_text VALUES('IN', 'generic', 'Invoice');
insert into ftd_apps.payment_methods_text VALUES('NC', 'generic', 'Account');
insert into ftd_apps.payment_methods_text VALUES('PP', 'generic', 'PayPal Account');$$$$$


--This will update stock emails containing the words 'credit card' with
--the token ~payment.method.text~
--The following stock emails were required in the functional requirements but
--do not exist in Production.  They have been omitted from the update.
--'GCA', 'MDAY', 'VALD', 'XMAS05'
update clean.stock_email se set se.body =
    substr(to_char(se.body), 0, instr(to_char(se.body), 'credit card') - 1)
    || '~payment.method.text~'
    || substr(to_char(se.body), instr(to_char(se.body), 'credit card') + 11)
where instr(to_char(se.body), 'credit card') > 0
and se.title in
(
'CCAN',
'CLP',
'DSCT',
'FSND',
'MBL',
'MBR',
'MCH',
'MOC',
'MRC',
'NOFF',
'NOIF',
'PEND',
'SDEL',
'cx.autocancel',
'cx.autocancelrefund'
);


--This will update stock messages containing the words 'credit card' with
--the xsl code <xsl:value-of select="header/payment_method_text"/>
update ftd_apps.stock_messages sm set sm.content =
    substr(to_char(sm.content), 0, instr(to_char(sm.content), 'credit card') - 1)
    || '<xsl:value-of select="header/payment_method_text"/>'
    || substr(to_char(sm.content), instr(to_char(sm.content), 'credit card') + 11)
where instr(to_char(sm.content), 'credit card') > 0
and sm.stock_message_id in
(
'CCAN',
'CLP',
'FSND',
'GCA',
'MBL',
'MBR',
'MCH',
'MDAY',
'MRC',
'NOFF',
'NOIF',
'PEND',
'VALD',
'WLMTI_CLP',
'WLMTI_FSND',
'WLMTI_MBL',
'WLMTI_MBR',
'WLMTI_MCH',
'WLMTI_MDAY',
'WLMTI_NOFF',
'WLMTI_PEND',
'WLMTI_VALD'
);


--This will update stock messages containing the words 'card' with
--the xsl code <xsl:value-of select="header/payment_method_text"/>
update ftd_apps.stock_messages sm set sm.content =
    substr(to_char(sm.content), 0, instr(to_char(sm.content), 'card') - 1)
    || '<xsl:value-of select="header/payment_method_text"/>'
    || substr(to_char(sm.content), instr(to_char(sm.content), 'card') + 4)
where instr(to_char(sm.content), 'card') > 0
and sm.stock_message_id in
(
'CCAN',
'CLP',
'FSND',
'GCA',
'MBL',
'MBR',
'MCH',
'MDAY',
'MRC',
'NOFF',
'NOIF',
'PEND',
'VALD',
'WLMTI_CLP',
'WLMTI_FSND',
'WLMTI_MBL',
'WLMTI_MBR',
'WLMTI_MCH',
'WLMTI_MDAY',
'WLMTI_NOFF',
'WLMTI_PEND',
'WLMTI_VALD'
);

--------------------------------------------------------
-- ##END## PAYPAL RELATED DATA UPDATES
--------------------------------------------------------

 GRANT EXECUTE ON POP.INSERT_PRICE_VARIANCE TO OSP;
 
 
 ALTER TABLE QUARTZ_SCHEMA.GROUPS
     ADD (ACTIVE_FLAG            CHAR(1) DEFAULT 'Y' NOT NULL,
   CONSTRAINT GROUPS_CK1 CHECK(ACTIVE_FLAG IN('Y','N'))) ;
 
 ALTER TABLE QUARTZ_SCHEMA.PIPELINE
     ADD (ACTIVE_FLAG       CHAR(1) DEFAULT 'Y' NOT NULL,
     CONSTRAINT PIPELINE_CK2 CHECK(ACTIVE_FLAG IN('Y','N'))) ;
 



-- DEFECT 3370

update clean.stock_email
set use_no_reply_flag = 'Y'
where title = 'qc.co.queue';
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('FLORIST','NoReplyFromAddress', 'noreply@florist.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values  ('FTD','NoReplyFromAddress', 'noreply@ftd.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('GIFT','NoReplyFromAddress', 'noreply@giftsense.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('HIGH','NoReplyFromAddress', 'noreply@butterfieldblooms.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('SFMB','NoReplyFromAddress', 'oracle@apollo.ftdi.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('FDIRECT','NoReplyFromAddress', 'noreply@flowersdirect.com');
 
INSERT INTO ftd_apps.email_company_data (COMPANY_ID,NAME, VALUE) 
values ('FUSA','NoReplyFromAddress', 'noreply@flowersusa.com');

-- END DEFECT 3370

grant select on ftd_apps.session_item to clean;
grant select on ftd_apps.session_order_master to clean;
grant select on ftd_apps.payment_methods_text to global;

-- BEGIN DML to fix PROD Data for Source Codes "10021" and "956  7"
-- This was found while coding for Defect 3167. Approved by Mickey and Ops Team (Stephanie)
update ftd_apps.source_master set description = 'AAAEMAIL04 20% OFF'
     where source_code = '10021' and description = chr(5) || 'AAAEMAIL04 20% OFF';

DELETE FROM ftd_apps.source_master where source_code = '956' || chr(1) || chr(1) || '7';

-- END DML to fix PROD Data

insert into ftd_apps.origins values ('CSPI', 'CSP Internet Order', 'internet', 'Y');

grant execute on frp.misc_pkg to ftd_apps;

--------------------------
-- PIF PROCESSING REWRITE
--------------------------

create table ftd_apps.load_product_index
     (INDEX_ID                NUMBER,
      INDEX_FILE_NAME         VARCHAR2(300),
      INDEX_NAME              VARCHAR2(150),
      PARENT_INDEX_FILE_NAME  VARCHAR2(300),
      COUNTRY_ID              VARCHAR2(2),
      DISPLAY_ORDER           NUMBER,
      PARENT_INDEX_ID         NUMBER)
   tablespace ftd_apps_data;

grant select on ftd_apps.load_product_index to ftd_apps_query_role;   
COMMENT ON TABLE ftd_apps.load_product_index IS 'Raw data from the novator product index feed files is parsed into this table';

drop table ftd_apps.load_prod_index_product;   
create table ftd_apps.load_prod_index_product
     (INDEX_FILE_NAME     VARCHAR2(300),
      PRODUCT_ID          VARCHAR2(20),
      DISPLAY_ORDER       NUMBER,
      INDEX_ID            NUMBER,
      LOOKUP_PRODUCT_ID   VARCHAR2(10),
      LOAD_FAILED         CHAR(1) NOT NULL)
   tablespace ftd_apps_data;

create index ftd_apps.load_product_index_product_n1 on ftd_apps.load_prod_index_product (index_id,lookup_product_id) 
tablespace ftd_apps_indx;

grant select on ftd_apps.load_prod_index_product to ftd_apps_query_role, rpt, bi_etl;

COMMENT ON TABLE ftd_apps.load_prod_index_product IS 
'Raw data from the novator product index feed files is parsed into this table';


create table ftd_apps.stage_product_index
     (INDEX_ID                   NUMBER        NOT NULL,
      INDEX_FILE_NAME            VARCHAR2(300) NOT NULL,
      INDEX_NAME                 VARCHAR2(100) NOT NULL,
      PARENT_INDEX_ID            NUMBER,
      INDEX_SUBHEADING           VARCHAR2(50),
      COUNTRY_ID                 VARCHAR2(2),
      DISPLAY_ORDER              NUMBER(3)     NOT NULL,
      OCCASIONS_FLAG             VARCHAR2(1)   NOT NULL,
      PRODUCTS_FLAG              VARCHAR2(1)   NOT NULL,
      DROPSHIP_FLAG              VARCHAR2(1)   NOT NULL,
      WEB_LIVE_FLAG              VARCHAR2(1)   NOT NULL,
      CALL_CENTER_LIVE_FLAG      VARCHAR2(1)   NOT NULL,
      WEB_TEST1_FLAG             VARCHAR2(1)   NOT NULL,
      WEB_TEST2_FLAG             VARCHAR2(1)   NOT NULL,
      WEB_TEST3_FLAG             VARCHAR2(1)   NOT NULL,
      CALL_CENTER_TEST_FLAG      VARCHAR2(1)   NOT NULL,
      SEARCH_INDEX_FLAG          VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_OCCASIONS_FLAG  VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_PRODUCTS_FLAG   VARCHAR2(1)   NOT NULL,
      COMPANY_ID                 VARCHAR2(12),
   constraint stage_product_index_pk PRIMARY KEY (index_id) USING INDEX TABLESPACE ftd_apps_indx,
   constraint stage_product_index_u1 unique (index_file_name) USING INDEX TABLESPACE ftd_apps_indx)
   tablespace ftd_apps_data;

grant select on ftd_apps.stage_product_index to ftd_apps_query_role;
COMMENT ON TABLE ftd_apps.stage_product_index IS 
'Data from the load_product_index table is manipulated and then put into this table';

   
create table ftd_apps.stage_product_index_xref
     (PRODUCT_INDEX_ID           NUMBER(12)    NOT NULL,
      PRODUCT_ID                 VARCHAR2(10)  NOT NULL,
      MAPPED_INDEX_ID            NUMBER(12),
      DISPLAY_ORDER              NUMBER(6)     NOT NULL,
   constraint stage_product_index_xref_pk PRIMARY KEY (product_index_id,product_id) USING INDEX TABLESPACE ftd_apps_indx)
   tablespace ftd_apps_data;

grant select on ftd_apps.stage_product_index_xref to ftd_apps_query_role;
COMMENT ON TABLE ftd_apps.stage_product_index_xref IS 
'Data from the load_prod_index_product table is manipulated and then put into this table';

create table ftd_apps.product_index_bak
     (INDEX_ID                    NUMBER,
      INDEX_FILE_NAME             VARCHAR2(300) NOT NULL,
      INDEX_NAME                  VARCHAR2(100) NOT NULL,
      PARENT_INDEX_ID             NUMBER,
      INDEX_SUBHEADING            vARCHAR2(50),
      COUNTRY_ID                  VARCHAR2(2),
      DISPLAY_ORDER               NUMBER(3)     NOT NULL,
      OCCASIONS_FLAG              VARCHAR2(1)   NOT NULL,
      PRODUCTS_FLAG               VARCHAR2(1)   NOT NULL,
      DROPSHIP_FLAG               VARCHAR2(1)   NOT NULL,
      WEB_LIVE_FLAG               VARCHAR2(1)   NOT NULL,
      CALL_CENTER_LIVE_FLAG       VARCHAR2(1)   NOT NULL,
      WEB_TEST1_FLAG              VARCHAR2(1)   NOT NULL,
      WEB_TEST2_FLAG              VARCHAR2(1)   NOT NULL,
      WEB_TEST3_FLAG              VARCHAR2(1)   NOT NULL,
      CALL_CENTER_TEST_FLAG       VARCHAR2(1)   NOT NULL,
      SEARCH_INDEX_FLAG           VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_OCCASIONS_FLAG   VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_PRODUCTS_FLAG    VARCHAR2(1)   NOT NULL,
      COMPANY_ID                  VARCHAR2(12))
   tablespace ftd_apps_data;
COMMENT ON TABLE ftd_apps.stage_product_index_xref IS 'Backup table for product_index';

create table ftd_apps.intl_product_index_xref
     (PRODUCT_INDEX_ID         NUMBER(12)    NOT NULL,
      PRODUCT_ID               VARCHAR2(10)  NOT NULL,
      MAPPED_INDEX_ID          NUMBER(12),
      DISPLAY_ORDER            NUMBER(6)     NOT NULL)
   tablespace ftd_apps_data;

grant select on ftd_apps.intl_product_index_xref to ftd_apps_query_role;
COMMENT ON TABLE ftd_apps.intl_product_index_xref IS 'Static data for the international catalog product xref';

create table ftd_apps.intl_product_index
     (INDEX_ID                     NUMBER,
      INDEX_FILE_NAME              VARCHAR2(300) NOT NULL,
      INDEX_NAME                   VARCHAR2(100) NOT NULL,
      PARENT_INDEX_ID              NUMBER,
      INDEX_SUBHEADING             VARCHAR2(50),
      COUNTRY_ID                   VARCHAR2(2),
      DISPLAY_ORDER                NUMBER(3)     NOT NULL,
      OCCASIONS_FLAG               VARCHAR2(1)   NOT NULL,
      PRODUCTS_FLAG                VARCHAR2(1)   NOT NULL,
      DROPSHIP_FLAG                VARCHAR2(1)   NOT NULL,
      WEB_LIVE_FLAG                VARCHAR2(1)   NOT NULL,
      CALL_CENTER_LIVE_FLAG        VARCHAR2(1)   NOT NULL,
      WEB_TEST1_FLAG               VARCHAR2(1)   NOT NULL,
      WEB_TEST2_FLAG               VARCHAR2(1)   NOT NULL,
      WEB_TEST3_FLAG               VARCHAR2(1)   NOT NULL,
      CALL_CENTER_TEST_FLAG        VARCHAR2(1)   NOT NULL,
      SEARCH_INDEX_FLAG            VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_OCCASIONS_FLAG    VARCHAR2(1)   NOT NULL,
      ADV_SEARCH_PRODUCTS_FLAG     VARCHAR2(1)   NOT NULL,
      COMPANY_ID                   VARCHAR2(12))
  tablespace ftd_apps_data;
grant select on ftd_apps.intl_product_index to ftd_apps_query_role;
COMMENT ON TABLE ftd_apps.intl_product_index IS 'Static data for the international catalog product indexes';

create table ftd_apps.load_adv_search_product_index
     (INDEX_FILE_NAME   VARCHAR2(300) NOT NULL,
      DISPLAY_ORDER     NUMBER(3)     NOT NULL)
   tablespace ftd_apps_data;
COMMENT ON TABLE ftd_apps.intl_product_index IS 'Static data for the creating advanced search indexes';

create table ftd_apps.product_index_xref_bak
     (PRODUCT_INDEX_ID   NUMBER(12)   NOT NULL,
      PRODUCT_ID         VARCHAR2(10) NOT NULL,
      MAPPED_INDEX_ID    NUMBER(12),
      DISPLAY_ORDER      NUMBER(6)    NOT NULL)
   tablespace ftd_apps_data;
COMMENT ON TABLE ftd_apps.intl_product_index IS 'Backup table for the product_index_xref table';

CREATE TABLE FTD_APPS.PIF_COMPANIES (
  PIF_COMPANY_ID VARCHAR2(12) NOT NULL, 
  FILE_NAME_TXT VARCHAR2(256) NOT NULL,
  FIELD_COUNT NUMBER(10) NOT NULL, 
  PREFIX_USED_FLAG CHAR(1) NOT NULL, 
  PREFIX_TXT VARCHAR2(10) NOT NULL,
  ADV_SEARCH_OCCASION_FLAG CHAR(1) NOT NULL, 
  ACTIVE_FLAG CHAR(1) DEFAULT 'Y' NOT NULL) tablespace ftd_apps_data;

alter table ftd_apps.pif_companies add CONSTRAINT PIF_COMPANIES_PK PRIMARY KEY(PIF_COMPANY_ID)
using index tablespace ftd_apps_indx;
COMMENT ON TABLE ftd_apps.PIF_COMPANIES IS 'Product index feed company configurations.';

alter table ftd_apps.pif_companies add CONSTRAINT PIF_COMPANIES_FK1 FOREIGN KEY(PIF_COMPANY_ID)
REFERENCES FTD_APPS.COMPANY_MASTER(COMPANY_ID);

alter table ftd_apps.pif_companies add CONSTRAINT PIF_COMPANIES_CK1 CHECK(PREFIX_USED_FLAG IN('Y','N'));

alter table ftd_apps.pif_companies add CONSTRAINT PIF_COMPANIES_CK2 CHECK(ADV_SEARCH_OCCASION_FLAG IN('Y','N'));

alter table ftd_apps.pif_companies add CONSTRAINT PIF_COMPANIES_CK3 CHECK(ACTIVE_FLAG IN('Y','N'));
  
CREATE TABLE FTD_APPS.PIF_COMPANY_OCCASIONS (
  PIF_COMPANY_ID VARCHAR2(12) NOT NULL, 
  OCCASION_IDX_FILE_NAME VARCHAR2(300) NOT NULL) tablespace ftd_apps_data;
COMMENT ON TABLE ftd_apps.PIF_COMPANY_OCCASIONS IS 'Product index feed company company occasion indexes to be created.';

alter table ftd_apps.pif_company_occasions add
CONSTRAINT PIF_COMPANY_OCCASIONS_PK PRIMARY KEY(PIF_COMPANY_ID, OCCASION_IDX_FILE_NAME) using index tablespace ftd_apps_indx;

alter table ftd_apps.pif_company_occasions add CONSTRAINT PIF_COMPANY_OCCASIONS_FK1 FOREIGN KEY(PIF_COMPANY_ID)
REFERENCES FTD_APPS.PIF_COMPANIES(PIF_COMPANY_ID);  
  
CREATE TABLE FTD_APPS.PIF_COMPANY_PRODUCTS (
  PIF_COMPANY_ID VARCHAR2(12) NOT NULL, 
  PRODUCT_IDX_FILE_NAME VARCHAR2(300) NOT NULL) tablespace ftd_apps_data;
COMMENT ON TABLE ftd_apps.PIF_COMPANY_PRODUCTS IS 'Product index feed company company product indexes to be created.';

alter table ftd_apps.pif_company_products add CONSTRAINT PIF_COMPANY_PRODUCTS_PK PRIMARY KEY(PIF_COMPANY_ID, PRODUCT_IDX_FILE_NAME) using index tablespace ftd_apps_indx;

alter table ftd_apps.pif_company_products add CONSTRAINT PIF_COMPANY_PRODUCTS_FK1 FOREIGN KEY(PIF_COMPANY_ID)
REFERENCES FTD_APPS.PIF_COMPANIES;



INSERT INTO FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by) 
VALUES('product_database','pif_ftp_server','hawk.ftdi.com', sysdate, 'SYS', sysdate, 'SYS');        
INSERT INTO FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by) 
VALUES('product_database','pif_working_directory','/tmp', sysdate, 'SYS', sysdate, 'SYS');                       
INSERT INTO FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by) 
VALUES('product_database','pif_remote_directory','/PIF', sysdate, 'SYS', sysdate, 'SYS');            
INSERT INTO FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by) 
VALUES('product_database','pif_delete_remote_file','false', sysdate, 'SYS', sysdate, 'SYS');            
INSERT INTO FRP.GLOBAL_PARMS (context, name, value, created_on, created_by, updated_on, updated_by) 
VALUES('product_database','pif_file_pattern','index\-feed\-\w*\.csv', sysdate, 'SYS', sysdate, 'SYS');             

INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('FTD','index-feed-350.csv',5,'N','N','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('SFMB','index-feed-2512.csv',4,'N','musicbox_','Y','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('GIFT','index-feed-8499.csv',4,'Y','gs_','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('HIGH','index-feed-8998.csv',4,'Y','bb_','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('GSC','index-feed-gsc.csv',4,'Y','gsc_','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('FUSA','index-feed-2797.csv',4,'Y','fusa_','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('FLORIST','index-feed-10171.csv',4,'Y','florist_','N','Y');
INSERT INTO FTD_APPS.PIF_COMPANIES c (c.PIF_COMPANY_ID, c.FILE_NAME_TXT, c.FIELD_COUNT, c.PREFIX_USED_FLAG, c.PREFIX_TXT, 
c.ADV_SEARCH_OCCASION_FLAG, c.ACTIVE_FLAG) VALUES('FDIRECT','index-feed-10908.csv',4,'Y','fdirect_','N','Y');

INSERT INTO FTD_APPS.PIF_COMPANY_OCCASIONS o (o.PIF_COMPANY_ID, o.OCCASION_IDX_FILE_NAME) VALUES ('SFMB','musicbox_occasions');
INSERT INTO FTD_APPS.PIF_COMPANY_PRODUCTS p (p.PIF_COMPANY_ID, p.PRODUCT_IDX_FILE_NAME) VALUES ('SFMB',
'musicbox_classiccollectibles');
INSERT INTO FTD_APPS.PIF_COMPANY_PRODUCTS p (p.PIF_COMPANY_ID, p.PRODUCT_IDX_FILE_NAME) VALUES ('SFMB',
'musicbox_uniquecollectibles');
INSERT INTO FTD_APPS.PIF_COMPANY_PRODUCTS p (p.PIF_COMPANY_ID, p.PRODUCT_IDX_FILE_NAME) VALUES ('SFMB',
'musicbox_dragonsfantasy');


--------------------------
-- END PIF PROCESSING REWRITE
--------------------------



grant select on clean.email to pop;
grant execute on clean.customer_maint_pkg to pop;


insert into ftd_apps.shipping_keys (shipping_key_id, shipping_key_description, shipper,
tracking_url, created_by, created_on, updated_by, updated_on) values (
'5','Motorola Shipping Key','Fed Ex','http://www.fedex.com/us/tracking/',user,sysdate,user,sysdate);

insert into ftd_apps.shipping_key_details (shipping_detail_id, shipping_key_id, min_price, max_price, 
created_by, created_on, updated_by, updated_on)
select to_char(to_number(shipping_detail_id)+9), '5', min_price, max_price, user, sysdate, user, sysdate
from   ftd_apps.shipping_key_details
where  shipping_key_id = '1';

insert into ftd_apps.shipping_key_costs (shipping_key_detail_id, shipping_method_id, shipping_cost, 
created_by, created_on, updated_by, updated_on)
select to_char(to_number(shipping_key_detail_id)+9), shipping_method_id, shipping_cost,
user, sysdate, user, sysdate
from   ftd_apps.shipping_key_costs
where  shipping_key_detail_id in ('83','84','85','86','87','88','89','90');

insert into b2b.client_url values ('AN01000000269','Motorola','http://www.ftd.com/motorola/');
insert into b2b.buyer_source_code values ('AN01000000269','Motorola','12752','');
insert into b2b.client_options values ('AN01000000269','1016160000','','');

Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'REPORT_SERVER_URL', 'http://apolloreports.ftdi.com/reports/rwservlet?reportEmail', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'EMAIL_GROUP1', 'kdomenick@ftdi.com,kszubert@ftdi.com,weboperations@ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'EMAIL_GROUP2', 'ssaucier@ftdi.com,sgresowiak@ftdi.com,joreilly@ftdi.com,weboperations@ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AIRCANADA_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AIRCANADA_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'ALASKAAIRLINES_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'ALLANT_OUTBOUND_SERVER', 'ftp.allantgroup.com.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AMERICANAIRLINES_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AMERICANAIRLINES_OUTBOUND_SERVER', '151.193.132.144', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AMERICANAUTOASSOC_OUTBOUND_SERVER', 'ftp.aaa.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'AMERICANEXPRESS_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'ATA_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'ATA_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'BASSHOTELS_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'BASSHOTELS_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'BENEVOLINK_OUTBOUND_SERVER', 'trans.benevolink.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'BESTWESTERN_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'BESTWESTERN_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'CENDANTHOTELS_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'CENDANTHOTELS_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'CONTINENTALAIRLINES_INBOUND_SERVER', 'ftp.insidecoair.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'CONTINENTALAIRLINES_OUTBOUND_SERVER', 'ftp.insidecoair.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'DELTAAIRLINES_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'DELTAAIRLINES_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'DISCOVER_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'FLORISTZIPCODECOUNT_OUTBOUND_SERVER', 'iris01.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'GOLDPOINTS_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'GREENPOINTS_OUTBOUND_SERVER', 'ftp.greenpoints.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'HAWAIIANAIRLINES_OUTBOUND_SERVER', 'ftp.hawaiipremium.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'HILTONHOTELS_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'KOREANAIR_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'KOREANAIR_OUTBOUND_SERVER', '210.105.11.8', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'MARRIOTT_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'MARRIOTT_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'MIDWESTAIRLINES_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'MIDWESTAIRLINES_OUTBOUND_SERVER', '208.241.220.3', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'NORTHWESTAIRLINES_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'NORTHWESTAIRLINES_OUTBOUND_SERVER', 'ftp.nwa.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'PRODUCTFEED_OUTBOUND_SERVER', 'feeds.channelintelligence.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'QUIXTARFLORAGIFT_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'UNITEDAIRLINES_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'UNITEDAIRLINES_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'UPROMISE_OUTBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'USAIRWAYS_INBOUND_SERVER', 'hawk.ftdi.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);
Insert into frp.global_parms values ('PARTNER_REWARD_CONFIG',
  'USAIRWAYS_OUTBOUND_SERVER', 'ftp.americawest.com', 'tschmig',SYSDATE,'tschmig',SYSDATE);

insert into ftd_apps.price_header
(PRICE_HEADER_ID,
DESCRIPTION,
CREATED_BY,
CREATED_ON,
UPDATED_BY,
UPDATED_ON)
Values
(
  'PW','23 Percent off','SYS',SYSDATE,'SYS',SYSDATE
);


insert into ftd_apps.price_header_details
(PRICE_HEADER_ID,
MIN_DOLLAR_AMT,
MAX_DOLLAR_AMT,
DISCOUNT_AMT,
DISCOUNT_TYPE,
CREATED_BY,
CREATED_ON,
UPDATED_BY,
UPDATED_ON)
values ('PW' , 0,9999.99, 23,'P','SYS',sysdate,'SYS',sysdate);



---- END FIX 2.4.0 RELEASE SCRIPT -------------------
