

---- FIX 2.4.1 RELEASE SCRIPT ------------------

--#################################################################################
-- DDL / DML
--#################################################################################


-- CONVERT ANY LOWER CASE ZIPS WITH THE UPPER CASE VERSION
--REPLACED - SEE BELOW
--UPDATE ftd_apps.FLORIST_ZIPS SET ZIP_CODE = UPPER(ZIP_CODE) where zip_code != upper(zip_code);
    -- EXPECTED RESULT is 255 UPDATES


--THE ABOVE UPDATE MAY RESULT IN DUPS - USE THIS INSTEAD:
declare

cursor c1 is
   select rowid,zip_code from ftd_apps.florist_zips
   where zip_code != upper(zip_code);

v_tot_cnt NUMBER := 0;
v_err_cnt NUMBER := 0;
v_delerr_cnt NUMBER := 0;

begin
   for c1_row in c1
   LOOP
      begin
         v_tot_cnt := v_tot_cnt + 1;
         UPDATE ftd_apps.FLORIST_ZIPS
         SET ZIP_CODE = UPPER(ZIP_CODE)
         where rowid = c1_row.rowid;

      exception
         when dup_val_on_index then
            dbms_output.put_line('UPPER ZIP ALREADY THERE - DELETE IT!   Zip Code: ' || c1_row.zip_code);
            v_err_cnt := v_err_cnt + 1;

            begin
               delete from ftd_apps.florist_zips
               where rowid = c1_row.rowid;
            exception
               when others then
                  dbms_output.put_line('UNEXPECTED DELETE ERROR   Zip Code: ' || c1_row.zip_code ||
                                             '     MSG=' || sqlerrm);
                  v_delerr_cnt := v_delerr_cnt + 1;
            end;

         when others then
            dbms_output.put_line('UNEXPECTED ERROR   Zip Code: ' || c1_row.zip_code ||
                                          '     MSG=' || sqlerrm);
            v_err_cnt := v_err_cnt + 1;
      end;



   END LOOP;
dbms_output.put_line('Total Cnt= ' || v_tot_cnt ||
                     '    Deletes=' || v_delerr_cnt ||
                     '    ERRORS=' || v_err_cnt);
end;
/
----- END FLORIST_ZIPS UPDATE





--#################################################################################
-- PACKAGES / PROCEDURES / REPORTS   (FIX_2_4_0 BRANCH!!)
--#################################################################################

ftd_apps_plsql/florist_load_audit_pkg.pkb
ftd_apps_plsql/florist_maint_pkg.pkb
ftd_apps_plsql/oel_update_florist_zip.prc

mercury_plsql/mercury_pkg.pkg
mercury_plsql/mercury_pkg.pkb




---- END FIX 2.4.1 RELEASE SCRIPT -------------------
