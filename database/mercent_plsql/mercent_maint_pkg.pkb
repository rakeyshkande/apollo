
CREATE OR REPLACE
PACKAGE BODY PTN_MERCENT.MERCENT_MAINT_PKG AS

PROCEDURE INSERT_MRCNT_ORDER_REPORT
  (
    IN_ORDER_REPORT IN MRCNT_ORDER_REPORT.REPORT%TYPE,
    OUT_ORDER_REPORT_ID OUT VARCHAR,
    OUT_STATUS OUT VARCHAR,
    OUT_MESSAGE OUT VARCHAR )
AS
BEGIN
  OUT_ORDER_REPORT_ID := MRCNT_ORDER_REPORT_ID_SQ.nextval;
   INSERT
     INTO MRCNT_ORDER_REPORT
    (
      MRCNT_ORDER_REPORT_ID,
      REPORT               ,
      CREATED_ON           ,
      CREATED_BY           ,
      UPDATED_ON           ,
      UPDATED_BY
    )
    VALUES
    (
      OUT_ORDER_REPORT_ID,
      IN_ORDER_REPORT    ,
      SYSDATE            ,
      'SYS'              ,
      SYSDATE            ,
      'SYS'
    );
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
    (
      SQLERRM,1,256
    )
    ;
  END; -- end exception block
END INSERT_MRCNT_ORDER_REPORT;


PROCEDURE INSERT_MRCNT_ORDER
  (
    IN_MRCNT_ORDER_NUMBER    IN MRCNT_ORDER.MRCNT_ORDER_NUMBER%TYPE,
    IN_CHANNEL_NAME          IN MRCNT_ORDER.CHANNEL_NAME%TYPE,
    IN_CHANNEL_ORDER_ID      IN MRCNT_ORDER.CHANNEL_ORDER_ID%TYPE,
    IN_MASTER_ORDER_NUMBER   IN MRCNT_ORDER.MASTER_ORDER_NUMBER%TYPE,
    IN_MRCNT_ORDER_REPORT_ID IN MRCNT_ORDER.MRCNT_ORDER_REPORT_ID%TYPE,
    IN_MRCNT_XML             IN MRCNT_ORDER.MRCNT_XML%TYPE,
    IN_FTD_XML               IN MRCNT_ORDER.FTD_XML%TYPE,
    IN_ORDER_DATE            IN MRCNT_ORDER.ORDER_DATE%TYPE,
    IN_CURRENCY_CODE         IN MRCNT_ORDER.CURRENCY_CODE%TYPE,
    IN_ORDER_STATUS          IN MRCNT_ORDER.ORDER_STATUS%TYPE,
    OUT_STATUS OUT VARCHAR,
    OUT_MESSAGE OUT VARCHAR
  )
AS
BEGIN
  BEGIN
     INSERT
       INTO MRCNT_ORDER
      (
        MRCNT_ORDER_NUMBER   ,
        CHANNEL_NAME         ,
        CHANNEL_ORDER_ID     ,
        MASTER_ORDER_NUMBER  ,
        MRCNT_ORDER_REPORT_ID,
        MRCNT_XML            ,
        FTD_XML              ,
        ORDER_DATE           ,
        CURRENCY_CODE        ,
        ORDER_STATUS         ,
        CREATED_ON           ,
        CREATED_BY           ,
        UPDATED_ON           ,
        UPDATED_BY
      )
      VALUES
      (
        IN_MRCNT_ORDER_NUMBER   ,
        IN_CHANNEL_NAME         ,
        IN_CHANNEL_ORDER_ID     ,
        IN_MASTER_ORDER_NUMBER  ,
        IN_MRCNT_ORDER_REPORT_ID,
        IN_MRCNT_XML            ,
        IN_FTD_XML              ,
        IN_ORDER_DATE           ,
        IN_CURRENCY_CODE        ,
        IN_ORDER_STATUS         ,
        SYSDATE                 ,
        'SYS'                   ,
        SYSDATE                 ,
        'SYS'
      );
    
    OUT_STATUS := 'Y';
  END;
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
    (
      SQLERRM,1,256
    )
    ;
  END; -- end exception block
END INSERT_MRCNT_ORDER;


PROCEDURE SAVE_FTD_ORDER_XML
  (
    IN_MRCNT_ORDER_NUMBER IN MRCNT_ORDER.MRCNT_ORDER_NUMBER%TYPE,
    IN_FTD_XML            IN MRCNT_ORDER.FTD_XML%TYPE,
    IN_ORDER_STATUS       IN MRCNT_ORDER.ORDER_STATUS%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2
  )
AS
BEGIN
   UPDATE MRCNT_ORDER
  SET FTD_XML                = IN_FTD_XML     ,
    ORDER_STATUS             = IN_ORDER_STATUS,
    UPDATED_BY               = 'SYS'          ,
    UPDATED_ON               = sysdate
    WHERE MRCNT_ORDER_NUMBER = IN_MRCNT_ORDER_NUMBER;
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; -- end exception block
END SAVE_FTD_ORDER_XML;


PROCEDURE SAVE_MRCNT_ORDER_DETAILS
  (
    IN_MRCNT_ORDER_ITEM_NUMBER IN MRCNT_ORDER_DETAIL.MRCNT_ORDER_ITEM_NUMBER%TYPE,
    IN_MRCNT_ORDER_NUMBER      IN MRCNT_ORDER_DETAIL.MRCNT_ORDER_NUMBER%TYPE,
	IN_MRCNT_CHANNEL_ORDER_ITEMID	IN MRCNT_ORDER_DETAIL.MRCNT_CHANNEL_ORDER_ITEMID%TYPE,
    IN_CONFIRMATION_NUMBER     IN MRCNT_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
    IN_MERCENT_PRODUCT_ID             IN MRCNT_ORDER_DETAIL.MERCENT_PRODUCT_ID%TYPE,
    IN_PRINCIPAL_AMT    IN MRCNT_ORDER_DETAIL.PRINCIPAL_AMT%TYPE,
    IN_SHIPPING_AMT     IN MRCNT_ORDER_DETAIL.SHIPPING_AMT%TYPE,
    IN_TAX_AMT          IN MRCNT_ORDER_DETAIL.TAX_AMT%TYPE,
    IN_SHIPPING_TAX_AMT IN MRCNT_ORDER_DETAIL.SHIPPING_TAX_AMT%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 )
AS
BEGIN
   INSERT
     INTO MRCNT_ORDER_DETAIL
    (
      MRCNT_ORDER_ITEM_NUMBER,
      MRCNT_ORDER_NUMBER     ,
	  MRCNT_CHANNEL_ORDER_ITEMID,
      CONFIRMATION_NUMBER    ,
      MERCENT_PRODUCT_ID,
      PRINCIPAL_AMT   ,
      SHIPPING_AMT    ,
      TAX_AMT         ,
      SHIPPING_TAX_AMT,
      CREATED_ON      ,
      CREATED_BY      ,
      UPDATED_ON      ,
      UPDATED_BY
    )
    VALUES
    (
      IN_MRCNT_ORDER_ITEM_NUMBER,
      IN_MRCNT_ORDER_NUMBER     ,
	  IN_MRCNT_CHANNEL_ORDER_ITEMID,
      IN_CONFIRMATION_NUMBER    ,
      IN_MERCENT_PRODUCT_ID,
      IN_PRINCIPAL_AMT   ,
      IN_SHIPPING_AMT    ,
      IN_TAX_AMT         ,
      IN_SHIPPING_TAX_AMT,
      SYSDATE            ,
      'SYS'              ,
      SYSDATE            ,
      'SYS'
    );
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
    (
      SQLERRM,1,256
    )
    ;
  END; -- end exception block
END SAVE_MRCNT_ORDER_DETAILS;

PROCEDURE UPDATE_MRCNT_ORDER_STATUS
  (
    IN_MRCNT_ORDER_NUMBER IN MRCNT_ORDER.MRCNT_ORDER_NUMBER%TYPE,
    IN_ORDER_STATUS       IN MRCNT_ORDER.ORDER_STATUS%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 )
AS
BEGIN
   UPDATE MRCNT_ORDER
  SET ORDER_STATUS           = IN_ORDER_STATUS,
    UPDATED_BY               = 'SYS'          ,
    UPDATED_ON               = sysdate
    WHERE MRCNT_ORDER_NUMBER = IN_MRCNT_ORDER_NUMBER;
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; -- end exception block
END UPDATE_MRCNT_ORDER_STATUS;

PROCEDURE INSERT_MERCENT_ACKNOWLEDGEMENT
(
IN_CONFIRMATION_NUMBER           IN VARCHAR2,
IN_STATUS                        IN VARCHAR2,
IN_CANCEL_REASON                 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR exists_cur IS
        SELECT  mrcnt_order_item_number
        FROM    mrcnt_order_detail
        WHERE   confirmation_number = in_confirmation_number;

/*
Only insert a record if one doesn't exist for this item
or if the new status is 'Failure' and the existing status is 'Success'
*/
CURSOR duplicate_cur(in_mrcnt_order_item_number varchar2) IS
        SELECT mrcnt_order_acknowledgement_id
        FROM mrcnt_order_acknowledgement
        WHERE mrcnt_order_item_number = in_mrcnt_order_item_number
--        AND (status_code = 'Failure' OR status_code = IN_STATUS);
        AND NOT(status_code = 'Success' AND IN_STATUS = 'Failure');

v_mrcnt_order_item_number          mrcnt_order_detail.mrcnt_order_item_number%type;
v_mrcnt_order_ack_id   mrcnt_order_acknowledgement.mrcnt_order_acknowledgement_id%type;


BEGIN

    OPEN exists_cur;
    FETCH exists_cur INTO v_mrcnt_order_item_number;
    CLOSE exists_cur;

    IF v_mrcnt_order_item_number IS NOT NULL THEN

        OPEN duplicate_cur(v_mrcnt_order_item_number);
        FETCH duplicate_cur into v_mrcnt_order_ack_id;
        IF duplicate_cur%notfound then
            INSERT INTO MRCNT_ORDER_ACKNOWLEDGEMENT (
                MRCNT_ORDER_ACKNOWLEDGEMENT_ID,
                MRCNT_ORDER_ITEM_NUMBER,
                STATUS_CODE,
                CANCEL_REASON,
                STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY
            ) VALUES (
                MRCNT_ORDER_ACK_ID_SQ.nextval,
                v_MRCNT_order_item_number,
                IN_STATUS,
                IN_CANCEL_REASON,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS'
            );
        END IF;

        OUT_STATUS := 'Y';

    ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'MRCNT_ORDER_DETAIL record not found for confirmation number ' || in_confirmation_number;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_MERCENT_ACKNOWLEDGEMENT;

PROCEDURE SAVE_PRODUCT_FEED_STATUS
  (
    IN_MRCNT_PRODUCT_FEED_ID IN MRCNT_PRODUCT_FEED.MRCNT_PRODUCT_FEED_ID%TYPE,
    IN_PRODUCT_ID            IN MRCNT_PRODUCT_FEED.PRODUCT_ID%TYPE,
    IN_FEED_STATUS           IN MRCNT_PRODUCT_FEED.STATUS%TYPE,
    IN_FEED_ID               IN MRCNT_PRODUCT_FEED.FEED_ID%TYPE,
    IN_UPDATED_BY            IN MRCNT_PRODUCT_FEED.UPDATED_BY%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2
  )
AS
  /*-----------------------------------------------------------------------------
  Description:
  This procedure is responsible for updating the feed status for a given MRCNT_PRODUCT_FEED_ID Input:
  MRCNT_PRODUCT_FEED_ID            number
  product_id              varchar2
  feed_status      varchar2
  feed_id   number
  created_by      date
  updated_by       date
  Output:
  status                       varchar2 (Y or N)
  message                      varchar2 (error message)
  -----------------------------------------------------------------------------*/
BEGIN
   UPDATE MRCNT_PRODUCT_FEED
  SET STATUS                    = IN_FEED_STATUS,
    FEED_ID                     = IN_FEED_ID    ,
    UPDATED_BY                  = IN_UPDATED_BY ,
    UPDATED_ON                  = sysdate
    WHERE MRCNT_PRODUCT_FEED_ID = IN_MRCNT_PRODUCT_FEED_ID
  AND PRODUCT_ID                = IN_PRODUCT_ID;
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; -- end exception block
END SAVE_PRODUCT_FEED_STATUS;

PROCEDURE SAVE_TO_FEED_MASTER
  (
    IN_MRC_XML     IN MRCNT_FEED_MASTER.MRC_XML%TYPE,
    IN_FEED_TYPE   IN MRCNT_FEED_MASTER.FEED_TYPE%TYPE,
    IN_FEED_STATUS IN MRCNT_FEED_MASTER.STATUS%TYPE,
    OUT_FEED_ID OUT MRCNT_FEED_MASTER.FEED_ID%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 )
AS
  /*-----------------------------------------------------------------------------
  Description:
  This procedure is responsible for saving the feed xml to DB:
  MRC_XML             clob
  FEED_TYPE              varchar2
  STATUS      varchar2
  Output:
  feed_id       number
  status                       varchar2 (Y or N)
  message                      varchar2 (error message)
  -----------------------------------------------------------------------------*/
  v_feed_id NUMBER;
BEGIN
  v_feed_id := MRCNT_FEED_MASTER_ID_SQ.nextval;
   INSERT
     INTO PTN_MERCENT.MRCNT_FEED_MASTER
    (
      feed_id  ,
      MRC_XML  ,
      FEED_TYPE,
      STATUS,
      CREATED_BY,
      CREATED_ON,
      UPDATED_BY,
      UPDATED_ON
    )
    VALUES
    (
      v_feed_id   ,
      IN_MRC_XML  ,
      IN_FEED_TYPE,
      IN_FEED_STATUS,
      'SYS',
      sysdate,
      'SYS',
      sysdate
    );
  
  OUT_STATUS  := 'Y';
  OUT_FEED_ID := v_feed_id;
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
    (
      SQLERRM,1,256
    )
    ;
  END; -- end exception block
END SAVE_TO_FEED_MASTER;

PROCEDURE SAVE_MRCNT_ORD_FEED_STATUS (
IN_FEED_ID              IN MRCNT_FEED_MASTER.FEED_ID%TYPE,
IN_FEED_TYPE		IN MRCNT_FEED_MASTER.FEED_TYPE%TYPE,
IN_FEED_STATUS          IN MRCNT_FEED_MASTER.STATUS%TYPE,
IN_SERVER               IN MRCNT_FEED_MASTER.SERVER%TYPE,
IN_FILENAME             IN MRCNT_FEED_MASTER.FILENAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
BEGIN

     UPDATE MRCNT_FEED_MASTER
      SET STATUS                 = IN_FEED_STATUS,
      SERVER                          = IN_SERVER,
      FILENAME                        = IN_FILENAME,
      UPDATED_BY					  = 'SYS',
      UPDATED_ON					  = sysdate
      WHERE FEED_ID = IN_FEED_ID and FEED_TYPE = IN_FEED_TYPE;
    
    OUT_STATUS      := 'Y';
  
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'UNIQUE_CONSTRAINT_EXCEPTION';
END; 
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; -- end exception block
END SAVE_MRCNT_ORD_FEED_STATUS;

PROCEDURE UPDATE_MRCNT_FEED_ID (
IN_FEED_ID              IN MRCNT_FEED_MASTER.FEED_ID%TYPE,
IN_FEED_TYPE            IN MRCNT_FEED_MASTER.FEED_TYPE%TYPE,
IN_FEED_STATUS          IN MRCNT_FEED_MASTER.STATUS%TYPE,
IN_FEED_DATA_ID         IN VARCHAR2,
IN_UPDATED_BY           IN VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
  Description:
  	This procedure is responsible for saving the feed id for a given feed data id.
	feed data Id can be one among 
	order adjustment id, 
	order acknowledgment id, or order fulfillment id
  -----------------------------------------------------------------------------*/
BEGIN

  CASE IN_FEED_TYPE
        WHEN 'ADJUSTMENT_FEED' THEN
 
		UPDATE MRCNT_ORDER_ADJUSTMENT
		  SET FEED_ID                     = IN_FEED_ID,
                  STATUS                          = IN_FEED_STATUS,
		  UPDATED_BY                      = IN_UPDATED_BY,
		  UPDATED_ON					  = sysdate
		  WHERE MRCNT_ORDER_ADJUSTMENT_ID = IN_FEED_DATA_ID;
    
		OUT_STATUS      := 'Y';
    
 
        WHEN 'ACKNOWLEDGMENT_FEED' THEN
        
		UPDATE MRCNT_ORDER_ACKNOWLEDGEMENT
			SET FEED_ID                     = IN_FEED_ID,
                        STATUS                          = IN_FEED_STATUS,
			UPDATED_BY                      = IN_UPDATED_BY,
			UPDATED_ON					  = sysdate
			WHERE MRCNT_ORDER_ACKNOWLEDGEMENT_ID = IN_FEED_DATA_ID;
    
		OUT_STATUS      := 'Y';
     
 
        WHEN 'FULLFILLMENT_FEED' THEN
        
		UPDATE MRCNT_ORDER_FULFILLMENT
			SET FEED_ID                     = IN_FEED_ID,
                        STATUS                          = IN_FEED_STATUS,
			UPDATED_BY                      = IN_UPDATED_BY,
			UPDATED_ON					  = sysdate
			WHERE MRCNT_ORDER_FULFILLMENT_ID = IN_FEED_DATA_ID;
    
		OUT_STATUS      := 'Y';     
  END CASE;
  
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; 
END UPDATE_MRCNT_FEED_ID;


PROCEDURE INSERT_MERCENT_PRICE_VARIANCE
(
  IN_CONFIRMATION_NUMBER        IN MRCNT_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  IN_FTD_PRODUCT_AMT            IN MRCNT_PRICE_VARIANCE.FTD_PRODUCT_AMT%TYPE,
  IN_FTD_SHIPPING_FEE_AMT       IN MRCNT_PRICE_VARIANCE.FTD_SHIPPING_FEE_AMT%TYPE,
  IN_FTD_TAX_AMT                IN MRCNT_PRICE_VARIANCE.FTD_TAX_AMT%TYPE,
  IN_FTD_SHIPPING_TAX_AMT       IN MRCNT_PRICE_VARIANCE.FTD_SHIPPING_TAX_AMT%TYPE,
  IN_MRCNT_PRODUCT_AMT          IN MRCNT_PRICE_VARIANCE.MRCNT_PRODUCT_AMT%TYPE,
  IN_MRCNT_SHIPPING_FEE_AMT     IN MRCNT_PRICE_VARIANCE.MRCNT_SHIPPING_FEE_AMT%TYPE,
  IN_MRCNT_TAX_AMT              IN MRCNT_PRICE_VARIANCE.MRCNT_TAX_AMT%TYPE,
  IN_MRCNT_SHIPPING_TAX_AMT     IN MRCNT_PRICE_VARIANCE.MRCNT_SHIPPING_TAX_AMT%TYPE,
  IN_CREATED_BY         		    IN MRCNT_PRICE_VARIANCE.CREATED_BY%TYPE,
  IN_UPDATED_BY         		    IN MRCNT_PRICE_VARIANCE.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
)
AS

mrcnt_order_item_number varchar2(32);

BEGIN

DECLARE CURSOR detail_cur IS
  SELECT  MRCNT_ORDER_ITEM_NUMBER
    FROM    PTN_MERCENT.MRCNT_ORDER_DETAIL
    WHERE   CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur INTO mrcnt_order_item_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

INSERT
INTO
    PTN_MERCENT.MRCNT_PRICE_VARIANCE
    (
        MRCNT_ORDER_ITEM_NUMBER,
        FTD_PRODUCT_AMT,
        FTD_SHIPPING_FEE_AMT,
        FTD_TAX_AMT,
        FTD_SHIPPING_TAX_AMT,
        MRCNT_PRODUCT_AMT,
        MRCNT_SHIPPING_FEE_AMT,
        MRCNT_TAX_AMT,
        MRCNT_SHIPPING_TAX_AMT,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY
    )
    VALUES
    (
        mrcnt_order_item_number,
        IN_FTD_PRODUCT_AMT,
        IN_FTD_SHIPPING_FEE_AMT,
        IN_FTD_TAX_AMT,
        IN_FTD_SHIPPING_TAX_AMT,
        IN_MRCNT_PRODUCT_AMT,
        IN_MRCNT_SHIPPING_FEE_AMT,
        IN_MRCNT_TAX_AMT,
        IN_MRCNT_SHIPPING_TAX_AMT,
        SYSDATE,
        IN_CREATED_BY,
        SYSDATE,
        IN_UPDATED_BY
    );

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Unable to locate order detail for confirmation number ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'Y';
        OUT_MESSAGE := 'Variance already in database for ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END INSERT_MERCENT_PRICE_VARIANCE;

PROCEDURE INSERT_FULL_MERCENT_ADJUSTMENT
(
  IN_CONFIRMATION_NUMBER IN VARCHAR2,
  IN_ADJUSTMENT_REASON IN VARCHAR2,
  OUT_STATUS OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
) AS

CURSOR get_detail_cur IS
  SELECT mrcnt_order_item_number,
      principal_amt,
      shipping_amt,
      tax_amt,
      shipping_tax_amt
  FROM mrcnt_order_detail
  WHERE confirmation_number = in_confirmation_number;

BEGIN

    FOR mrcnt_rec IN get_detail_cur loop
        INSERT INTO MRCNT_ORDER_ADJUSTMENT (
            MRCNT_ORDER_ADJUSTMENT_ID,
            MRCNT_ORDER_ITEM_NUMBER,
            ADJUSTMENT_REASON,
            PRINCIPAL_AMT,
            SHIPPING_AMT,
            TAX_AMT,
            SHIPPING_TAX_AMT,
            STATUS,
            CREATED_BY,
            CREATED_ON,
            UPDATED_BY,
            UPDATED_ON
        ) VALUES (
            MRCNT_ORDER_ADJUSTMENT_ID_SQ.nextval,
            MRCNT_REC.MRCNT_ORDER_ITEM_NUMBER,
            IN_ADJUSTMENT_REASON,
            MRCNT_REC.PRINCIPAL_AMT,
            MRCNT_REC.SHIPPING_AMT,
            MRCNT_REC.TAX_AMT,
            MRCNT_REC.SHIPPING_TAX_AMT,
            'NEW',
            'OrderValidator',
            sysdate,
            'OrderValidator',
            sysdate
        );
    END LOOP;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_FULL_MERCENT_ADJUSTMENT;

PROCEDURE INSERT_MERCENT_ADJUSTMENT
(
  IN_CONFIRMATION_NUMBER IN VARCHAR2
, IN_ADJUSTMENT_REASON IN VARCHAR2
, IN_PRINCIPAL_AMT IN NUMBER
, IN_SHIPPING_AMT IN NUMBER
, IN_TAX_AMT IN NUMBER
, IN_SHIPPING_TAX_AMT IN NUMBER
, IN_CREATED_BY VARCHAR2
, IN_UPDATED_BY VARCHAR2
, OUT_STATUS OUT VARCHAR2
, OUT_MESSAGE OUT VARCHAR2
) AS

CURSOR exists_cur IS
  SELECT  mrcnt_order_item_number
  FROM    mrcnt_order_detail
  WHERE   confirmation_number = in_confirmation_number;

v_mrcnt_order_item_number          mrcnt_order_detail.mrcnt_order_item_number%type;

BEGIN

    OPEN exists_cur;
    FETCH exists_cur INTO v_mrcnt_order_item_number;
    CLOSE exists_cur;

    IF v_mrcnt_order_item_number IS NOT NULL THEN
        INSERT INTO MRCNT_ORDER_ADJUSTMENT (
            MRCNT_ORDER_ADJUSTMENT_ID,
            MRCNT_ORDER_ITEM_NUMBER,
            ADJUSTMENT_REASON,
            PRINCIPAL_AMT,
            SHIPPING_AMT,
            TAX_AMT,
            SHIPPING_TAX_AMT,
            STATUS,
            CREATED_BY,
            CREATED_ON,
            UPDATED_BY,
            UPDATED_ON
        ) VALUES (
                MRCNT_ORDER_ADJUSTMENT_ID_SQ.nextval,
                v_mrcnt_order_item_number,
                IN_ADJUSTMENT_REASON,
                IN_PRINCIPAL_AMT,
                IN_SHIPPING_AMT,
                IN_TAX_AMT,
                IN_SHIPPING_TAX_AMT,
                'NEW',
                IN_CREATED_BY,
                sysdate,
                IN_UPDATED_BY,
                sysdate
        );
        OUT_STATUS := 'Y';

    ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'MRCNT_ORDER_DETAIL record not found for confirmation number ' || in_confirmation_number;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END INSERT_MERCENT_ADJUSTMENT;

PROCEDURE PROCESS_REFUNDS
(
  IN_USER_NAME                  IN VARCHAR,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

outStatus varchar2(1);
outMessage varchar2(100);
order_count Integer;

cursor refund_cur is
		  SELECT r.refund_id,
                 od.external_order_number,
                 od.order_detail_id,
                 r.refund_disp_code,
                 r.refund_product_amount,
                 r.refund_addon_amount,
                 r.refund_service_fee,
                 r.refund_shipping_fee,
                 r.refund_service_fee_tax,
                 r.refund_shipping_tax,
                 r.refund_discount_amount,
                 r.refund_commission_amount,
                 r.refund_wholesale_amount,
                 r.refund_tax,
                 (select count(*) from clean.refund r2 where r2.order_detail_id = od.order_detail_id) as refund_count
            FROM clean.refund r,
                 clean.order_details od,
                 clean.orders o
           WHERE o.origin_id in (select FTD_ORIGIN_MAPPING from PTN_MERCENT.MRCNT_CHANNEL_MAPPING)
             AND od.order_guid = o.order_guid
             AND od.order_disp_code in ('Processed','Shipped','Printed','Validated')
             AND r.order_detail_id = od.order_detail_id
             AND r.refund_status = 'Unbilled'
             AND TRUNC(r.created_on) <= TRUNC(sysdate);
             
             cursor tax_cur(in_external_order_number varchar2) is
    SELECT meod.tax_amt mercent_tax_amt,
        meod.shipping_tax_amt mercent_shipping_tax_amt,
        nvl((select sum(refund_tax) from clean.refund r
            where r.order_detail_id = od.order_detail_id
            and r.refund_status in ('Billed', 'Settled')), 0) previous_tax
    FROM ptn_mercent.mrcnt_order_detail meod,
        clean.order_details od
    WHERE meod.confirmation_number = in_external_order_number
    AND od.external_order_number = meod.confirmation_number;

    v_mercent_tax_amt             number;
    v_mercent_shipping_tax_amt    number;
    v_previous_tax               number;
    v_tax_total                  number;
    v_diff                       number;
    
BEGIN
  
  FOR refund_rec IN refund_cur loop
  
        OPEN tax_cur(refund_rec.external_order_number);
      FETCH tax_cur INTO v_mercent_tax_amt, v_mercent_shipping_tax_amt, v_previous_tax;
      CLOSE tax_cur;

      v_tax_total := refund_rec.refund_tax + v_previous_tax;
      IF (v_tax_total > v_mercent_tax_amt) THEN
          v_diff := v_tax_total - v_mercent_tax_amt;
          refund_rec.refund_tax := refund_rec.refund_tax - v_diff;
          refund_rec.refund_shipping_tax := v_diff;
      END IF;
      
  -- Update mercent adjustment feed table.
        INSERT_MERCENT_ADJUSTMENT (
                    IN_CONFIRMATION_NUMBER => refund_rec.external_order_number
                  , IN_ADJUSTMENT_REASON => refund_rec.refund_disp_code
                  , IN_PRINCIPAL_AMT => refund_rec.refund_product_amount - refund_rec.refund_discount_amount + refund_rec.refund_addon_amount
                  , IN_SHIPPING_AMT => refund_rec.refund_shipping_fee + refund_rec.refund_service_fee
                  , IN_TAX_AMT => refund_rec.refund_tax
                  , IN_SHIPPING_TAX_AMT => refund_rec.refund_service_fee_tax + refund_rec.refund_shipping_tax
                  , IN_CREATED_BY => IN_USER_NAME
                  , IN_UPDATED_BY => IN_USER_NAME
                  , OUT_STATUS => outStatus
                  , OUT_MESSAGE => outMessage
         );
         
    IF outStatus  = 'Y' THEN 
  
  -- Update refunds table.
      update clean.refund
          set refund_status = 'Billed',
          refund_date = sysdate,
          refund_tax = refund_rec.refund_tax,
          refund_shipping_tax = refund_rec.refund_shipping_tax,
          updated_on = sysdate,
          updated_by = 'PROCESS_REFUNDS'
      where refund_id = refund_rec.refund_id;

  -- Update Payments table.
      update clean.payments
          set bill_status = 'Billed',
          bill_date = sysdate,
          updated_on = sysdate,
          updated_by = 'PROCESS_REFUNDS'
      where refund_id = refund_rec.refund_id;
    
  END IF;

  end loop;
  
  --OUT_STATUS := 'Y';
  
  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
             
END PROCESS_REFUNDS;

PROCEDURE INSERT_FLORIST_FULFILL_ORDERS
(
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting all the florist delivered 
        orders for today that is still live
Output:
        out_status                  varchar2 (Y or N)
        out_message                 varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR GET_ITEM_NUMBERS IS

SELECT  meod.mrcnt_order_item_number
    FROM ptn_mercent.mrcnt_order_detail meod
    JOIN    clean.order_details od ON meod.confirmation_number = od.external_order_number
    JOIN mercury.mercury m on to_char(od.order_detail_id) = m.reference_number and m.mercury_status in ('MC','MN','MM')
    and m.msg_type = 'FTD' and (m.delivery_date) <= trunc(sysdate)
    and not exists (SELECT 'Y' FROM mercury.mercury m2
        where m2.mercury_order_number = m.mercury_order_number
        and m2.msg_type in ('CAN', 'REJ')
        and m2.mercury_status in ('MC','MN','MM'))
        and not exists (SELECT mrcnt_order_item_number FROM ptn_mercent.mrcnt_order_fulfillment mof
        where mof.mrcnt_order_item_number = meod.mrcnt_order_item_number);

BEGIN

  FOR rec in GET_ITEM_NUMBERS LOOP
    insert into ptn_mercent.mrcnt_order_fulfillment
        (mrcnt_order_fulfillment_id,
         mrcnt_order_item_number,
         fulfillment_date,
         shipping_method,
         carrier_name,
         tracking_number,
         status,
         created_on,
         created_by,
         updated_on,
         updated_by)
    values(
        ptn_mercent.mrcnt_order_fulfillment_id_sq.nextval,
        rec.mrcnt_order_item_number,
        sysdate,
        'Standard Delivery',
        'FTD Florist',
         null,
        'NEW',
        sysdate,
        'SYS',
        sysdate,
        'SYS');

   END LOOP;

    OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END INSERT_FLORIST_FULFILL_ORDERS;


PROCEDURE INSERT_DROPSHIP_CONFIRMATION
(
  IN_VENUS_ORDER_NUM            IN VENUS.VENUS.VENUS_ORDER_NUMBER%TYPE,
  IN_SHIP_DATE			IN ptn_mercent.MRCNT_ORDER_FULFILLMENT.FULFILLMENT_DATE%TYPE,
  IN_SHIPPING_METHOD		IN ptn_mercent.MRCNT_ORDER_FULFILLMENT.SHIPPING_METHOD%TYPE,
  IN_CARRIER_NAME		IN ptn_mercent.MRCNT_ORDER_FULFILLMENT.CARRIER_NAME%TYPE,
  IN_TRACKING_NUMBER		IN ptn_mercent.MRCNT_ORDER_FULFILLMENT.TRACKING_NUMBER%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_DROPSHIP_CONFIRMATION
 Type:    Procedure
 Syntax:  INSERT_DROPSHIP_CONFIRMATIONs(venus_order_number, ship_date, shipping_method,carrier_name,tracking_number)

Description:
        This stored procedure inserts a record into table ptn_mercent.mrcnt_order_fulfillment

Input:
	venus_order_number varchar2,
	ship_date	date,
	shipping_method	shipping_method
	carrier_name	carrier_name
	tracking_number	tracking_number

Output:
        status              varchar2
        error message       varchar2
-----------------------------------------------------------------------------*/
v_ship_method  varchar2(20);

CURSOR GET_ITEM_NUMBERS IS
    SELECT distinct meod.mrcnt_order_item_number
    FROM venus.venus v
    JOIN clean.order_details od ON od.order_detail_id = TO_NUMBER(v.REFERENCE_NUMBER)
    JOIN ptn_mercent.mrcnt_order_detail meod ON meod.confirmation_number = od.external_order_number
    WHERE v.venus_order_number = IN_VENUS_ORDER_NUM
    AND v.msg_type = 'FTD'
    AND not exists (SELECT mrcnt_order_item_number FROM ptn_mercent.mrcnt_order_fulfillment mof
        where mof.mrcnt_order_item_number = meod.mrcnt_order_item_number);

BEGIN

select description into v_ship_method from ftd_apps.ship_methods where ship_method_id = in_shipping_method;

IF SQL%NOTFOUND THEN
	v_ship_method := in_shipping_method;
END IF;

FOR rec in GET_ITEM_NUMBERS LOOP
    insert into ptn_mercent.mrcnt_order_fulfillment
        (mrcnt_order_fulfillment_id,
         mrcnt_order_item_number,
         fulfillment_date,
         shipping_method,
         carrier_name,
         tracking_number,
         status,
         created_on,
         created_by,
         updated_on,
         updated_by)
    values(
        ptn_mercent.mrcnt_order_fulfillment_id_sq.nextval,
        rec.mrcnt_order_item_number,
        in_ship_date,
        v_ship_method,
        in_carrier_name,
        in_tracking_number,
        'NEW',
        sysdate,
        'SYS',
        sysdate,
        'SYS');

   END LOOP;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END INSERT_DROPSHIP_CONFIRMATION;

PROCEDURE UPDATE_ORDER_CONF_EMAIL_STATUS (
    IN_STATUS IN VARCHAR2,
    IN_ORDER_ITEM_NUMBER IN VARCHAR2,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 
) AS
BEGIN

   UPDATE MRCNT_ORDER_DETAIL
    SET confirmation_email_sent_flag = IN_STATUS, UPDATED_BY = 'SYS', UPDATED_ON = sysdate
     WHERE  mrcnt_order_item_number = IN_ORDER_ITEM_NUMBER;

  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
END UPDATE_ORDER_CONF_EMAIL_STATUS;


END MERCENT_MAINT_PKG;

.
/
