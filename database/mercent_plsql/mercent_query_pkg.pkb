
CREATE OR REPLACE
PACKAGE BODY PTN_MERCENT.MERCENT_QUERY_PKG AS

PROCEDURE GET_MRCNT_ORDER_XML
(
   IN_MRCNT_ORDER_NUMBER     IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

	OPEN OUT_CUR FOR
	select MRCNT_XML
	from PTN_MERCENT.MRCNT_ORDER
	where MRCNT_ORDER_NUMBER = IN_MRCNT_ORDER_NUMBER;

END GET_MRCNT_ORDER_XML;


PROCEDURE GET_ORDER_CONFIRMATION_NUMBER
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_CUR FOR
select LPAD ( TO_CHAR(ptn_mercent.confirmation_number_sq.nextval), 10, '0') as confirmation_number 
from dual;

END GET_ORDER_CONFIRMATION_NUMBER;


PROCEDURE GET_IOTW_SOURCE_CODE
(
  IN_SOURCE_CODE                IN JOE.IOTW.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                 IN JOE.IOTW.PRODUCT_ID%TYPE,
  IN_TIME_RECEIVED              IN JOE.IOTW.START_DATE%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Returns the IOTW source code that was in effect when the order was received.

Input:
        source_code                      varchar2
        product_id                   varchar2
        time_received                   varchar2

Output:
        out_cur      Cursor containing the row
-----------------------------------------------------------------------------*/
default_code varchar(10);
iotw_code varchar(10);
start_date date;
end_date date;
temp_source_code varchar(10);

BEGIN

DECLARE CURSOR iotw_cur IS
  SELECT  SOURCE_CODE,
          IOTW_SOURCE_CODE,
          TRUNC(START_DATE), --Truncate for price feed calculations
          END_DATE
  FROM JOE.IOTW
    WHERE SOURCE_CODE = IN_SOURCE_CODE and PRODUCT_ID = IN_PRODUCT_ID;

BEGIN
  OPEN iotw_cur;
  FETCH iotw_cur
        INTO    default_code,
                iotw_code,
                start_date,
                end_date;
  IF iotw_cur%NOTFOUND THEN
      temp_source_code := IN_SOURCE_CODE;
  ELSE
      IF (IN_TIME_RECEIVED-start_date>=0) AND (end_date-IN_TIME_RECEIVED>=0) THEN
          temp_source_code := iotw_code;
      ELSE
          temp_source_code := default_code;
      END IF;
  END IF;
  CLOSE iotw_cur;

END;

OPEN OUT_CUR FOR
  SELECT S.SOURCE_CODE as IOTW_SOURCE, D.DISCOUNT_AMT as DISCOUNT, D.DISCOUNT_TYPE as DISCOUNT_TYPE
  FROM  FTD_APPS.PRICE_HEADER_DETAILS D
  JOIN FTD_APPS.SOURCE S ON S.PRICING_CODE = D.PRICE_HEADER_ID
  WHERE S.SOURCE_CODE = temp_source_code;

END GET_IOTW_SOURCE_CODE;


PROCEDURE GET_PDB_DATA
(
  IN_PRODUCT_ID          IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  OUT_CUR                OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Gets the current price and novator id in product database for the passed
    product id.

Input:
        product_id                      varchar2

Output:
        out_cur      Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    SELECT STANDARD_PRICE,
        DELUXE_PRICE,
        PREMIUM_PRICE,
        NOVATOR_ID,
        NVL(SHIP_METHOD_FLORIST, 'N') SHIP_METHOD_FLORIST,
        NVL(SHIP_METHOD_CARRIER, 'N') SHIP_METHOD_CARRIER
    FROM FTD_APPS.PRODUCT_MASTER
    WHERE PRODUCT_ID=IN_PRODUCT_ID;

END GET_PDB_DATA;

PROCEDURE CHECK_MRCNT_ORDER_EXISTS 
(
IN_CHANNEL_NAME          IN MRCNT_ORDER.CHANNEL_NAME%TYPE,
IN_CHANNEL_ORDER_ID		 IN MRCNT_ORDER.CHANNEL_ORDER_ID%TYPE,
OUT_EXISTS				 OUT VARCHAR2,
OUT_STATUS               OUT VARCHAR2,
OUT_MESSAGE              OUT VARCHAR2
)
AS
	v_count integer;
BEGIN
	select count(*) into v_count 
	from ptn_mercent.mrcnt_order 
	where channel_name=IN_CHANNEL_NAME and channel_order_id=IN_CHANNEL_ORDER_ID;
	
	IF v_count > 0 THEN 
		OUT_EXISTS := 'Y';
	ELSE
		OUT_EXISTS := 'N';
	END IF;
	
	OUT_STATUS := 'Y';
	
	EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
	
END CHECK_MRCNT_ORDER_EXISTS;


PROCEDURE CHECK_MRCNT_ORDER_ITEM_EXISTS
(
IN_MRCNT_ORDER_ITEM_NUMBER		IN MRCNT_ORDER_DETAIL.MRCNT_ORDER_ITEM_NUMBER%TYPE,
OUT_EXISTS				 OUT VARCHAR2,
OUT_STATUS               OUT VARCHAR2,
OUT_MESSAGE              OUT VARCHAR2
)
AS
	v_count integer;
  
BEGIN
        
	select count(*) into v_count
  from ptn_mercent.MRCNT_ORDER_DETAIL
	where MRCNT_ORDER_ITEM_NUMBER = IN_MRCNT_ORDER_ITEM_NUMBER;

	IF v_count > 0 THEN
		OUT_EXISTS := 'Y';
	ELSE
		OUT_EXISTS := 'N';
	END IF;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END CHECK_MRCNT_ORDER_ITEM_EXISTS;


PROCEDURE GET_MRCNT_CHANNEL_MAPPING
(
IN_CHANNEL_NAME IN MRCNT_CHANNEL_MAPPING.CHANNEL_NAME%TYPE,
OUT_CUR                     OUT TYPES.REF_CURSOR,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
)

AS

BEGIN

OPEN OUT_CUR FOR

SELECT *
FROM MRCNT_CHANNEL_MAPPING 
WHERE CHANNEL_NAME=IN_CHANNEL_NAME;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;  -- end exception block
END GET_MRCNT_CHANNEL_MAPPING;

PROCEDURE GET_FEED_SENT_TO_MERCENT
(
    OUT_CUR       OUT TYPES.REF_CURSOR
) AS
/*--------------------------------------------------------------------------------------------
 Procedure to get all the feeds sent to MERCENT irrespective of feed type.
----------------------------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
	
	select mrcnt_ORDER_ACKNOWLEDGEMENT_ID as feed_id, mrcnt_order_item_number, 'ORDER_ACK_FEED' as feed_type 
	from ptn_mercent.mrcnt_order_acknowledgement	
	where status='SENT';

END GET_FEED_SENT_TO_MERCENT;

PROCEDURE GET_PRODUCT_FEED
(
    IN_PRODUCTS_PER_FEED    IN NUMBER,
    OUT_CUR                 OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
   select *
from  
( select mpf.product_id,
	mpf.mrcnt_product_feed_id,
	pm.novator_id,
	pm.product_name,
	pm.long_description,
	pm.standard_price,
	pm.deluxe_price,
	pm.premium_price,
	pm.no_tax_flag,
    pm.status,
    pm.exception_start_date,
	pm.exception_end_date    
    FROM ptn_mercent.mrcnt_product_feed mpf,
        ftd_apps.product_master pm
    WHERE mpf.status = 'NEW'
    AND mpf.product_id = pm.product_id
  order by mrcnt_product_feed_id ) 
  where ROWNUM <= IN_PRODUCTS_PER_FEED;

END GET_PRODUCT_FEED;

PROCEDURE GET_ATTRS_BY_PRODUCT
(
  IN_PRODUCT_ID     	        IN  MRCNT_PRODUCT_FEED.PRODUCT_ID%TYPE,
  IN_MRC_PRODUCT_FEED_ID        IN  MRCNT_PRODUCT_FEED.MRCNT_PRODUCT_FEED_ID%TYPE,
  OUT_BULLET_POINTS_CUR       	OUT TYPES.REF_CURSOR,
  OUT_SEARCH_TERMS_CUR          OUT TYPES.REF_CURSOR,
  OUT_INTENDED_USE_CUR		OUT TYPES.REF_CURSOR,
  OUT_TARGET_AUDIENCE_CUR       OUT TYPES.REF_CURSOR,
  OUT_OTHER_ATTRIBUTES_CUR 	OUT TYPES.REF_CURSOR,
  OUT_SUBJECT_MATTER_CUR 	OUT TYPES.REF_CURSOR 
)AS

BEGIN
    OPEN OUT_BULLET_POINTS_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='BULLET_POINTS'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_SEARCH_TERMS_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='SEARCH_TERMS'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;
                
	OPEN OUT_INTENDED_USE_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='INTENDED_USE'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;
		
	OPEN OUT_TARGET_AUDIENCE_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='TARGET_AUDIENCE'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_OTHER_ATTRIBUTES_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='OTHER_ATTRIBUTES'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_SUBJECT_MATTER_CUR FOR
		select mpf.mrcnt_product_feed_id, mpf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_mercent.mrcnt_product_feed mpf 
		left outer join ptn_amazon.az_product_attributes apa on mpf.product_Id=apa.product_id and apa.product_attribute_type='SUBJECT_MATTER'
		where mpf.product_id=IN_PRODUCT_ID and mpf.mrcnt_product_feed_id = in_mrc_product_feed_id
		order by apa.product_attribute_sequence;	                
		
END GET_ATTRS_BY_PRODUCT;

PROCEDURE GET_PRODUCT_COMPANY_MAPPING
(
    IN_PRODUCT_ID      IN   MRCNT_PRODUCT_FEED.PRODUCT_ID%TYPE,
    OUT_MAPPING_EXISTS OUT VARCHAR2
) AS

 v_count    number := 0;
BEGIN

    BEGIN
      select count(*) into v_count from ftd_apps.Product_company_xref pcx
      where pcx.product_id = IN_PRODUCT_ID and pcx.company_id = 'FTD';
              
      EXCEPTION WHEN NO_DATA_FOUND THEN
      v_count := 0;				  
      END;			  
	IF v_count >0 then
            OUT_MAPPING_EXISTS := 'Y';
        ELSE
            OUT_MAPPING_EXISTS := 'N';
        END IF;
        
END GET_PRODUCT_COMPANY_MAPPING;

PROCEDURE GET_PRODUCT_FEED_COUNT
(
    OUT_PRODUCT_COUNT OUT number
) AS

BEGIN
    select count(*) into OUT_PRODUCT_COUNT from ptn_mercent.MRCNT_PRODUCT_FEED where status = 'NEW' order by mrcnt_product_feed_id;
        
END GET_PRODUCT_FEED_COUNT;


PROCEDURE GET_FTD_ORDER_XMLS_BY_STATUS
(
    IN_ORDER_STATUS IN VARCHAR2,
    OUT_CUR        OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
	
    SELECT MRCNT_ORDER_NUMBER, FTD_XML
	FROM  PTN_MERCENT.MRCNT_ORDER
	WHERE ORDER_STATUS = IN_ORDER_STATUS;

END GET_FTD_ORDER_XMLS_BY_STATUS;

PROCEDURE GET_MRCNT_FEEDS_BY_STATUS
(
    IN_FEED_STATUS 		IN MRCNT_FEED_MASTER.STATUS%TYPE,
    IN_FEED_TYPE 		IN MRCNT_FEED_MASTER.FEED_TYPE%TYPE,
    OUT_CUR                     OUT TYPES.REF_CURSOR	
)

AS
/*-----------------------------------------------------------------------------
  Description:
  	This procedure is to get feeds saved for a given feed type and feed status.
  -----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR

	SELECT FEED_ID, MRC_XML as FEED_XML
	FROM MRCNT_FEED_MASTER 
	WHERE FEED_TYPE=IN_FEED_TYPE AND STATUS = IN_FEED_STATUS;

END GET_MRCNT_FEEDS_BY_STATUS;

PROCEDURE GET_ORD_FULFILL_DATA_BY_STATUS
(
    IN_FEED_STATUS IN VARCHAR2,
    OUT_CUR        OUT TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
  Description:
  	This procedure is to get order fulfillment data with a given status.
  -----------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
	
        SELECT MOF.MRCNT_ORDER_FULFILLMENT_ID,
			MOF.SHIPPING_METHOD,
			MOF.CARRIER_NAME,
			MOF.TRACKING_NUMBER,
			TO_CHAR(MOF.FULFILLMENT_DATE,'YYYY-DD-MM HH24:MI:SS') FULFILLMENT_DATE,
			OD.MRCNT_CHANNEL_ORDER_ITEMID,
			O.MRCNT_ORDER_NUMBER,
			O.MASTER_ORDER_NUMBER,
			O.CHANNEL_ORDER_ID,
			O.CHANNEL_NAME,
			OD.MERCENT_PRODUCT_ID,
			OD.CONFIRMATION_NUMBER		
	FROM PTN_MERCENT.MRCNT_ORDER_FULFILLMENT MOF,
		PTN_MERCENT.MRCNT_ORDER_DETAIL OD,
		PTN_MERCENT.MRCNT_ORDER O
	WHERE MOF.MRCNT_ORDER_ITEM_NUMBER = OD.MRCNT_ORDER_ITEM_NUMBER
		AND OD.MRCNT_ORDER_NUMBER = O.MRCNT_ORDER_NUMBER
		AND MOF.STATUS = IN_FEED_STATUS;

END GET_ORD_FULFILL_DATA_BY_STATUS;

PROCEDURE GET_ORD_ADJ_DATA_BY_STATUS
(
    IN_FEED_STATUS     	IN  VARCHAR2,
    OUT_CUR       		OUT TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
  DESCRIPTION:
  	THIS PROCEDURE IS TO GET ORDER ADJUSTMENT DATA WITH A GIVEN STATUS.
-----------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
        SELECT ADJ.MRCNT_ORDER_ADJUSTMENT_ID,
            OD.MRCNT_CHANNEL_ORDER_ITEMID,
            CASE WHEN ADJ.ADJUSTMENT_REASON IS NOT NULL THEN
                NVL((SELECT CRM.MRCNT_CANCEL_REASON
                    FROM PTN_MERCENT.CANCEL_REASON_MAPPING CRM
                    WHERE CRM.APOLLO_CANCEL_CODE = ADJ.ADJUSTMENT_REASON
                    AND CRM.APOLLO_CANCEL_TYPE = 'Adjustment'),
                (SELECT CRM.MRCNT_CANCEL_REASON
                    FROM PTN_MERCENT.CANCEL_REASON_MAPPING CRM
                    WHERE CRM.APOLLO_CANCEL_CODE = 'Default'
                    AND CRM.APOLLO_CANCEL_TYPE = 'Adjustment'))
               ELSE NULL
            END ADJUSTMENT_REASON,
            ADJ.PRINCIPAL_AMT,
            ADJ.SHIPPING_AMT,
            ADJ.TAX_AMT,
            ADJ.SHIPPING_TAX_AMT,
            OD.MRCNT_ORDER_NUMBER,
            OD.CONFIRMATION_NUMBER,
            O.MASTER_ORDER_NUMBER,
            O.CHANNEL_NAME,
            O.CHANNEL_ORDER_ID,
            (SELECT COUNT(ADJ1.MRCNT_ORDER_ITEM_NUMBER) FROM PTN_MERCENT.MRCNT_ORDER_ADJUSTMENT ADJ1 
            WHERE ADJ1.MRCNT_ORDER_ITEM_NUMBER = ADJ.MRCNT_ORDER_ITEM_NUMBER) NO_OF_ADJUSTMENTS, -- NO OF ADJUSTMENTS ON AN ITEM
            CLEAN.REFUND_PKG.IS_FULLY_REFUNDED_ORDER(O.MASTER_ORDER_NUMBER) IS_ORDER_FULL_REFUND, -- IS ORDER FULLY REFUNDED
            CLEAN.REFUND_PKG.IS_ORDER_FULLY_REFUNDED(SOD.ORDER_DETAIL_ID) IS_ITEM_FULLREFUND, --IS ORDER ITEM FULLY REFUNDED
            (SELECT count(ADJ2.MRCNT_ORDER_ADJUSTMENT_ID) FROM PTN_MERCENT.MRCNT_ORDER_ADJUSTMENT ADJ2,
              PTN_MERCENT.MRCNT_ORDER_DETAIL OD1, PTN_MERCENT.MRCNT_ORDER O1 
              WHERE ADJ2.MRCNT_ORDER_ITEM_NUMBER = OD1.MRCNT_ORDER_ITEM_NUMBER 
                  AND OD1.MRCNT_ORDER_NUMBER = O1.MRCNT_ORDER_NUMBER 
                  AND O1.MASTER_ORDER_NUMBER = O.MASTER_ORDER_NUMBER
                  AND ADJ2.STATUS = 'SENT'
            )   IS_PARTIALLY_SENT -- IF  ADJUSTMENTS ON AN ORDER ARE PARTIALLY SENT
        FROM PTN_MERCENT.MRCNT_ORDER_ADJUSTMENT ADJ,
            PTN_MERCENT.MRCNT_ORDER_DETAIL OD,
            SCRUB.ORDER_DETAILS SOD,
            PTN_MERCENT.MRCNT_ORDER O
        WHERE ADJ.MRCNT_ORDER_ITEM_NUMBER=OD.MRCNT_ORDER_ITEM_NUMBER
        AND OD.MRCNT_ORDER_NUMBER=O.MRCNT_ORDER_NUMBER
        AND SOD.EXTERNAL_ORDER_NUMBER = OD.CONFIRMATION_NUMBER
        AND ADJ.STATUS = IN_FEED_STATUS;
        
END GET_ORD_ADJ_DATA_BY_STATUS;

PROCEDURE GET_ORD_ACK_DATA_BY_STATUS
(
    IN_FEED_STATUS     	IN  VARCHAR2,
    OUT_CUR       		OUT TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
  Description:
  	This procedure is to get order acknowledgement data with a given status.
  -----------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
        SELECT 	ACK.MRCNT_ORDER_ACKNOWLEDGEMENT_ID,
		ACK.STATUS_CODE, 
		CASE WHEN ACK.CANCEL_REASON IS NOT NULL THEN
		
                NVL((SELECT CRM.MRCNT_CANCEL_REASON
                    FROM PTN_MERCENT.CANCEL_REASON_MAPPING CRM
                    WHERE CRM.APOLLO_CANCEL_CODE = ACK.CANCEL_REASON
                    AND CRM.APOLLO_CANCEL_TYPE = 'Acknowledgement'),
                (SELECT CRM.MRCNT_CANCEL_REASON
                    FROM PTN_MERCENT.CANCEL_REASON_MAPPING CRM
                    WHERE CRM.APOLLO_CANCEL_CODE = 'DEFAULT'
                    AND CRM.APOLLO_CANCEL_TYPE = 'Acknowledgement'))
			
			ELSE NULL
			
        END CANCEL_REASON,
        O.CHANNEL_ORDER_ID,
	O.CHANNEL_NAME,
        O.MASTER_ORDER_NUMBER,
        OD.CONFIRMATION_NUMBER,
        OD.MRCNT_CHANNEL_ORDER_ITEMID
        FROM PTN_MERCENT.MRCNT_ORDER_ACKNOWLEDGEMENT ACK,
            PTN_MERCENT.MRCNT_ORDER_DETAIL OD,
            PTN_MERCENT.MRCNT_ORDER O
        WHERE ACK.MRCNT_ORDER_ITEM_NUMBER=OD.MRCNT_ORDER_ITEM_NUMBER
        AND OD.MRCNT_ORDER_NUMBER=O.MRCNT_ORDER_NUMBER
        AND ACK.STATUS = IN_FEED_STATUS;

END GET_ORD_ACK_DATA_BY_STATUS;


PROCEDURE GET_MRCNT_PRODUCT_ID_BY_CNF_NO
(
IN_CONF_NO			IN VARCHAR2,
OUT_PRODUCT_ID  OUT VARCHAR2,
OUT_STATUS      OUT VARCHAR2,
OUT_MESSAGE     OUT VARCHAR2
)AS

v_mrcnt_prod_id varchar(512);
 
BEGIN
  select mercent_product_id into v_mrcnt_prod_id from ptn_mercent.mrcnt_order_detail where confirmation_number= in_conf_no;
  OUT_STATUS := 'Y';
  OUT_PRODUCT_ID := v_mrcnt_prod_id;

  EXCEPTION WHEN NO_DATA_FOUND THEN
  BEGIN
  v_mrcnt_prod_id := null;
  END;
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_MRCNT_PRODUCT_ID_BY_CNF_NO;


PROCEDURE GET_MERCENT_ORDER_DETAILS
(
  IN_CONFIRMATION_NUMBER          IN PTN_MERCENT.MRCNT_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Retrieves order details for the given confirmation number.

Input:
        CONFIRMATION_NUMBER                      varchar2

Output:
        out_cur      Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    SELECT MRCNT_ORDER_ITEM_NUMBER,
        MRCNT_ORDER_NUMBER,
        MERCENT_PRODUCT_ID,
        PRINCIPAL_AMT,
        SHIPPING_AMT,
        TAX_AMT,
        SHIPPING_TAX_AMT
    FROM PTN_MERCENT.MRCNT_ORDER_DETAIL
    WHERE CONFIRMATION_NUMBER=IN_CONFIRMATION_NUMBER;

END GET_MERCENT_ORDER_DETAILS;

PROCEDURE GET_MRCNT_CHNL_MAPPING_DETAILS
 (
   OUT_CUR       OUT TYPES.REF_CURSOR,
   OUT_STATUS                  OUT VARCHAR2,
   OUT_MESSAGE                 OUT VARCHAR2
 ) AS
   
 
 
BEGIN
   
    OPEN OUT_CUR FOR
        
        SELECT *        
        FROM MRCNT_CHANNEL_MAPPING;
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
    BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    
END;  -- end exception block
END GET_MRCNT_CHNL_MAPPING_DETAILS;

PROCEDURE GET_MERCENT_ORDER_NUMBER
(
  IN_CHANNEL_ORDER_ID          IN PTN_MERCENT.MRCNT_ORDER.CHANNEL_ORDER_ID%TYPE,
  OUT_MASTER_ORDER_NUMBER      OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    Retrieves order details for the given confirmation number.

Input:
        IN_CHANNEL_ORDER_ID                      varchar2

Output:
        OUT_MASTER_ORDER_NUMBER      Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN


    SELECT MASTER_ORDER_NUMBER      
    INTO OUT_MASTER_ORDER_NUMBER  
    FROM PTN_MERCENT.MRCNT_ORDER
    WHERE CHANNEL_ORDER_ID = IN_CHANNEL_ORDER_ID;
	
	EXCEPTION WHEN OTHERS THEN
    BEGIN
    	OUT_MASTER_ORDER_NUMBER := null;
    END;
END GET_MERCENT_ORDER_NUMBER;

PROCEDURE GET_ITEM_TYPE
(
    IN_PRODUCT_ID      IN   MRCNT_PRODUCT_FEED.PRODUCT_ID%TYPE,
    OUT_ITEM_TYPE      OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
    Retrieves item type for given product Id from Product master table in PTN_AMAZON

Input:
        IN_PRODUCT_ID                      varchar2

Output:
        OUT_ITEM_TYPE      					varchar2
-----------------------------------------------------------------------------*/

BEGIN
      select item_type into OUT_ITEM_TYPE from ptn_amazon.AZ_PRODUCT_MASTER apm
      where apm.product_id = IN_PRODUCT_ID;
      
EXCEPTION WHEN OTHERS THEN
  BEGIN
    OUT_ITEM_TYPE := null;
  END;
        
END GET_ITEM_TYPE;

PROCEDURE GET_ORDERS_DELIVERED_TODAY
(
  OUT_ORDERS_CUR				OUT TYPES.REF_CURSOR, 
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all the florist/ vendor delivered 
       mercent orders that have delivery date as current date and are confirmed.
Output:
		OUT_ORDERS_CUR						cursor
        out_status                  varchar2 (Y or N)
        out_message                 varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_ORDERS_CUR FOR

	--Floral orders delivered today
	SELECT  distinct meod.mrcnt_order_item_number, od.order_guid, od.delivery_date order_delivery_date,
	 m.delivery_date actual_delivery_date, meod.CONFIRMATION_EMAIL_SENT_FLAG
		FROM ptn_mercent.mrcnt_order_detail meod 
		JOIN ptn_mercent.mrcnt_order mo on mo.mrcnt_order_number = meod.mrcnt_order_number 
    		and upper(mo.channel_name) in (select Upper(channel_name) from ptn_mercent.mrcnt_channel_mapping where send_confirmation_email = 'Y')
		JOIN clean.order_details od ON meod.confirmation_number = od.external_order_number 
		JOIN mercury.mercury m on to_char(od.order_detail_id) = m.reference_number 
			and m.mercury_status in ('MC','MN','MM')
			and m.msg_type = 'FTD' 
			and trunc(m.delivery_date) = trunc(sysdate)    
			and not exists (SELECT 'Y' FROM mercury.mercury m2
				where m2.mercury_order_number = m.mercury_order_number
				and m2.msg_type in ('CAN', 'REJ')
				and m2.mercury_status in ('MC','MN','MM'))
		where meod.CONFIRMATION_EMAIL_SENT_FLAG <> 'Y'
	union all
	--Dropship orders delivered today	   
	SELECT  distinct meod.mrcnt_order_item_number, od.order_guid, od.delivery_date order_delivery_date, v.delivery_date actual_delivery_date, meod.CONFIRMATION_EMAIL_SENT_FLAG
		FROM ptn_mercent.mrcnt_order_detail meod 
		JOIN ptn_mercent.mrcnt_order mo on mo.mrcnt_order_number = meod.mrcnt_order_number 
    		and upper(mo.channel_name) in (select Upper(channel_name) from ptn_mercent.mrcnt_channel_mapping where send_confirmation_email = 'Y')
		JOIN clean.order_details od ON meod.confirmation_number = od.external_order_number 
		JOIN venus.venus v on to_char(od.order_detail_id) = v.reference_number 
			and v.venus_status in ('VERIFIED')
			and v.msg_type = 'FTD' 
			and trunc(v.delivery_date) = trunc(sysdate)    
			and not exists (SELECT 'Y' FROM venus.venus v2
				where v2.venus_order_number = v.venus_order_number
				and v2.msg_type in ('CAN', 'REJ')
				and v2.venus_status in ('VERIFIED'))
		where meod.CONFIRMATION_EMAIL_SENT_FLAG <> 'Y'
	order by actual_delivery_date;
       
       OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
	BEGIN
			OUT_STATUS := 'N';
			OUT_MESSAGE := 'ERROR OCCURRED IN GET_ORDERS_DELIVERED_TODAY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;

END GET_ORDERS_DELIVERED_TODAY;

END MERCENT_QUERY_PKG;

.
/