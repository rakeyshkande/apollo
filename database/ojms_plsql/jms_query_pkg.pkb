CREATE OR REPLACE
PACKAGE BODY ojms.JMS_QUERY_PKG AS

PROCEDURE GET_QUEUE_COUNTS
(
OUT_QUEUE_COUNT_CUR             OUT TYPES.REF_CURSOR
)

IS

/*-----------------------------------------------------------------------------
Description:
        This package contains the procedures required to obtain administrative
        information about the Java Messaging System (JMS) queue tables.

Input:
        none

Output:
        cursor result set containing:
        queue_name                      varchar2
        waiting_count                   number
-----------------------------------------------------------------------------*/

   TYPE QUEUE_LIST_TYPE IS TABLE OF QUEUE_COUNTS%ROWTYPE INDEX BY VARCHAR2(30);
   QUEUE_LIST QUEUE_LIST_TYPE;

   CURSOR QUEUE_1_C IS
      SELECT NAME, QUEUE_TABLE
      FROM   USER_QUEUES
      WHERE  QUEUE_TYPE = 'NORMAL_QUEUE';
   QUEUE_1_R QUEUE_1_C%ROWTYPE;

BEGIN

   DELETE FROM QUEUE_COUNTS;
   OPEN QUEUE_1_C;
   LOOP
      FETCH QUEUE_1_C INTO QUEUE_1_R;
      EXIT WHEN QUEUE_1_C%NOTFOUND;
      EXECUTE IMMEDIATE 'SELECT COUNT(*), MIN (ENQ_TIME) FROM '||QUEUE_1_R.QUEUE_TABLE||' WHERE Q_NAME = '''||QUEUE_1_R.NAME||''' AND STATE=0'
      INTO QUEUE_LIST(QUEUE_1_R.NAME).QUEUE_COUNT, QUEUE_LIST(QUEUE_1_R.NAME).MIN_ENQ_TIME;
      INSERT INTO QUEUE_COUNTS (QUEUE_NAME, QUEUE_COUNT, MIN_ENQ_TIME) VALUES (QUEUE_1_R.NAME, QUEUE_LIST(QUEUE_1_R.NAME).QUEUE_COUNT, QUEUE_LIST(QUEUE_1_R.NAME).MIN_ENQ_TIME);
   END LOOP;
   CLOSE QUEUE_1_C;

   INSERT INTO QUEUE_COUNTS (QUEUE_NAME, QUEUE_COUNT, MIN_ENQ_TIME)
   SELECT 'WebOE Orders to Dispatch', COUNT(*), MIN (ENQ_TIME)
   FROM   FTD_EVENTS
   WHERE  Q_NAME = 'FTD_EVENTS'
          AND CORRID = 'WEBOE_DISPATCHER WEBOE_ORDER_TO_DISPATCH';
   COMMIT;

   OPEN OUT_QUEUE_COUNT_CUR FOR
      SELECT QUEUE_NAME, QUEUE_COUNT, TO_CHAR (MIN_ENQ_TIME, 'MM/DD/YYYY HH24:MI:SS') AS MIN_ENQ_TIME
      FROM   QUEUE_COUNTS;

END GET_QUEUE_COUNTS;

END JMS_QUERY_PKG;
.
/
