CREATE OR REPLACE PROCEDURE ojms.delete_mercury_process_poll
(in_corrid ojms.mercury_process_poll.corrid%type,
 out_status out varchar2,
 out_message out varchar2) AS
/*
   Delete from the queue mercury_process_poll using the corrid.
*/
BEGIN
   DELETE FROM ojms.mercury_process_poll
   WHERE corrid = in_corrid;
   COMMIT;
   out_status := 'Y';
   out_message := 'SUCCESS';

   EXCEPTION
      WHEN OTHERS THEN
         out_status := 'N';
         out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;
.
/
