CREATE OR REPLACE PROCEDURE ojms.CLEAR_MERCURY_QUEUE (p_status OUT VARCHAR2) AS
/*
   Delete all rows in the queue MERCURY.
*/
pragma autonomous_transaction;
BEGIN
   DELETE FROM mercury;
   COMMIT;
   p_status := 'Y';
END;
.
/
