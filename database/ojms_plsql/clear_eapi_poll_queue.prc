CREATE OR REPLACE PROCEDURE OJMS.CLEAR_EAPI_POLL_QUEUE (p_status OUT VARCHAR2) AS
/*
   Delete all rows in the queue MERCURY_POLL.
*/
pragma autonomous_transaction;
BEGIN
   DELETE FROM OJMS.mercury_poll;
   COMMIT;
   p_status := 'Y';
END;
.
/