CREATE OR REPLACE
PACKAGE BODY ojms.daemon_pkg AS

   pg_this_package VARCHAR2(61) := 'ojms.daemon_pkg';

FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN IS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Parameter:
        the name of the job, as found in user_jobs.what, including any parameters.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

   v_check    NUMBER;
   v_return   BOOLEAN;

BEGIN

   SELECT COUNT(*)
   INTO   v_check
   FROM   user_jobs
   WHERE  what like p_job_name||'%';

   IF v_check > 0 THEN
      v_return := TRUE;
   ELSE
      v_return := FALSE;
   END IF;

   RETURN V_RETURN;

END CURRENTLY_RUNNING;



PROCEDURE submit(p_context IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        submit a daemon
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;

BEGIN

   v_submit_string := p_context||'('||frp.misc_pkg.get_global_parm_value(p_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (p_context) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(p_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit;



PROCEDURE remove (p_context IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = p_context||'('||frp.misc_pkg.get_global_parm_value(p_context, 'THRESHHOLD')||');';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove;


FUNCTION accounting_gc_feed_ff_interval RETURN DATE IS
/* Implement this schedule: run hourly, from 9:00 until 12:00, Sunday through Friday. 

   8/17/2006 - Per Christy Hu, run on Saturdays also. Commented out Saturday logic. */
   
   v_date DATE;
   v_hour NUMBER;
   v_return DATE;
BEGIN
   v_date := SYSDATE;
--   IF trim(to_char(sysdate,'Day')) = 'Saturday' THEN
--      v_return := trunc(sysdate)+1+(9/24);
--   ELSE
      v_hour := to_number(to_char(sysdate,'hh24'));
      IF v_hour < 9 THEN
         v_return := trunc(sysdate)+(9/24);
      ELSIF v_hour < 12 THEN
         v_return := trunc(sysdate,'hh24')+(1/24);
      ELSE
         v_return := trunc(sysdate)+1+(9/24);
      END IF;
--   END IF;
   RETURN v_return;
END accounting_gc_feed_ff_interval;


END daemon_pkg;
.
/
