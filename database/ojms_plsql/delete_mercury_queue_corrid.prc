CREATE OR REPLACE PROCEDURE ojms.delete_mercury_queue_corrid
(in_corrid ojms.mercury.corrid%type,
 out_status out varchar2) AS
/*
   Delete from the queue MERCURY using the corrid.
*/
BEGIN
   DELETE FROM mercury
   WHERE corrid = in_corrid;
   COMMIT;
   out_status := 'Y';

   EXCEPTION
      WHEN OTHERS THEN
         out_status := 'N';
END;
.
/
