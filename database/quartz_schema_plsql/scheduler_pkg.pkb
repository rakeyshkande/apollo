create or replace package body quartz_schema.SCHEDULER_PKG is

FUNCTION GET_PIPELINE_DETAIL
	(IN_PIPELINE_ID VARCHAR2,
   IN_GROUP_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_PIPELINE_DETAIL
-- Type:    Function
-- Syntax:  GET_PIPELINE_DETAIL(IN_PIPELINE_ID VARCHAR2, IN_GROUP_IN VARCHAR2)
-- Returns: ref_cursor for
--                 PIPELINE_ID,
--                 GROUP_ID,
--                 DESCRIPTION,
--                 TOOL_TIP,
--                 DEFAULT_SCHEDULE,
--                 DEFAULT_PAYLOAD,
--                 FORCE_DFLT_PAYLOAD,
--                 QUEUE_NAME,
--                 CLASS_CODE
--
--
-- Description:   Queries the PIPELINES table for the record who's primary key
--                matches the passed in parameters.
--
--==============================================================================
AS
    pipeline_cursor types.ref_cursor;
BEGIN
     OPEN pipeline_cursor FOR
          SELECT PIPELINE_ID,
                 GROUP_ID,
                 DESCRIPTION,
                 TOOL_TIP,
                 DEFAULT_SCHEDULE,
                 DEFAULT_PAYLOAD,
                 FORCE_DEFAULT_PAYLOAD,
                 QUEUE_NAME,
                 CLASS_CODE,
                 ACTIVE_FLAG
          FROM pipeline
          WHERE 
            (PIPELINE_ID=IN_PIPELINE_ID OR IN_PIPELINE_ID IS NULL) AND 
            (GROUP_ID=IN_GROUP_ID OR IN_GROUP_ID IS NULL);

    RETURN pipeline_cursor;
END GET_PIPELINE_DETAIL;

FUNCTION GET_NEXT_JOB_ID
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------
Description:
   Returns the next job_id sequence number in a varchar format with
   padded leading zeros..
------------------------------------------------------------------------------*/
v_seq_number     NUMBER;
v_job_id      QRTZ_JOB_DETAILS.JOB_NAME%TYPE;

BEGIN
   SELECT job_sq.NEXTVAL
     INTO v_seq_number
     FROM DUAL;

   v_job_id := 'J' || LPAD(TO_CHAR(v_seq_number),9,'0');

   RETURN v_job_id;

END GET_NEXT_JOB_ID;

FUNCTION GET_NEXT_TRIGGER_ID
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------
Description:
   Returns the next job_id sequence number in a varchar format with
   padded leading zeros..
------------------------------------------------------------------------------*/
v_seq_number     NUMBER;
v_trigger_id      QRTZ_JOB_DETAILS.JOB_NAME%TYPE;

BEGIN
   SELECT trigger_sq.NEXTVAL
     INTO v_seq_number
     FROM DUAL;

   v_trigger_id := 'T' || LPAD(TO_CHAR(v_seq_number),9,'0');

   RETURN v_trigger_id;

END GET_NEXT_TRIGGER_ID;

FUNCTION GET_GROUP_PIPELINES
	(IN_GROUP_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_GROUP_PIPELINES
-- Type:    Function
-- Syntax:  GET_GROUP_PIPELINES(IN_GROUP_IN VARCHAR2)
-- Returns: ref_cursor for
--                 PIPELINE_ID
--
--
-- Description:   Queries the PIPELINES table for the record who's group id
--                matches the passed in value.
--
--==============================================================================
AS
    pipeline_cursor types.ref_cursor;
BEGIN
     OPEN pipeline_cursor FOR
          SELECT PIPELINE_ID, DESCRIPTION, ACTIVE_FLAG
          FROM pipeline
          WHERE GROUP_ID=IN_GROUP_ID
          ORDER BY DESCRIPTION;

    RETURN pipeline_cursor;
END GET_GROUP_PIPELINES;

FUNCTION GET_GROUPS
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_GROUPS
-- Type:    Function
-- Syntax:  GET_GROUPS()
-- Returns: ref_cursor for
--                 GROUP_ID VARCHAR2
--                 DESCRIPTION VARCHAR2
--
--
-- Description:   Returns all group records
--
--==============================================================================
AS
    group_cursor types.ref_cursor;
BEGIN
     OPEN group_cursor FOR
          SELECT group_id,description, active_flag
          FROM groups
          ORDER BY description;

    RETURN group_cursor;
END GET_GROUPS;

FUNCTION GET_EMAIL_ADDRESSES
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_EMAIL_ADDRESSES
-- Type:    Function
-- Syntax:  GET_EMAIL_ADDRESSES()
-- Returns: ref_cursor for
--                 EMAIL_ADDRESS VARCHAR2
--
--
-- Description:   Returns all email_addresses records
--
--==============================================================================
AS
    email_cursor types.ref_cursor;
BEGIN
     OPEN email_cursor FOR
       SELECT e.EMAIL_ADDRESS 
          FROM QUARTZ_SCHEMA.EMAIL_ADDRESSES e 
          ORDER BY e.EMAIL_ADDRESS;

    RETURN email_cursor;
END GET_EMAIL_ADDRESSES;

PROCEDURE INSERT_EMAIL_ADDRESS
(
    IN_EMAIL_ADDRESS      IN  EMAIL_ADDRESSES.EMAIL_ADDRESS%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_EMAIL_ADDRESS
 Type:    Procedure
 Syntax:  INSERT_EMAIL_ADDRESS(email_address)

Description:
        This stored procedure inserts a record into table quartz_schema.email_addresses.

Input:
        email_address         varchar2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO EMAIL_ADDRESSES (
    EMAIL_ADDRESS)
  VALUES(
    IN_EMAIL_ADDRESS);

  OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
  --It's already in the database, so report duplicate
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'DUPLICATE'; --Java code is checking for this value
WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_EMAIL_ADDRESS;

FUNCTION GET_CLASS_FOR_CODE (
  IN_CLASS_CODE IN APOLLO_JOBS.CLASS_CODE%TYPE
) RETURN VARCHAR2
--==============================================================================
--
-- Name:    GET_CLASS_FOR_CODE
-- Type:    Function
-- Syntax:  GET_CLASS_FOR_CODE(class_code)
-- Returns: CLASS_PACKAGE VARCHAR2
--
--Input:
--          class_code         varchar2
--
--Output:   class_name         varchar2
        
--
-- Description:   Return class name for the code passed in from 
--                quartz_schema.apollo_jobs
--
--==============================================================================
AS
    v_class_name apollo_jobs.class_package%type;
BEGIN
  BEGIN
    SELECT CLASS_PACKAGE INTO v_class_name 
      FROM QUARTZ_SCHEMA.APOLLO_JOBS 
      WHERE CLASS_CODE = IN_CLASS_CODE;
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
     v_class_name := '';
  END;
  
    RETURN v_class_name;
END GET_CLASS_FOR_CODE;

PROCEDURE GET_CLASS_FOR_CODE (
  IN_CLASS_CODE IN APOLLO_JOBS.CLASS_CODE%TYPE,
  OUT_CLASS_NAME OUT APOLLO_JOBS.CLASS_PACKAGE%TYPE,
  OUT_PARAMETERS OUT types.ref_cursor
)
--==============================================================================
--
-- Name:    GET_CLASS_FOR_CODE
-- Type:    PROCEDURE
-- Syntax:  GET_CLASS_FOR_CODE(class_code)
-- Returns: CLASS_PACKAGE VARCHAR2
--
--Input:
--          class_code         varchar2
--
--Output:   class_name         varchar2
--          properties         ref_cur
--
-- Description:   Return class name for the code passed in from 
--                quartz_schema.apollo_jobs and the records from
--                the class_properties table
--
--==============================================================================
AS
BEGIN
  BEGIN
    SELECT CLASS_PACKAGE INTO OUT_CLASS_NAME 
      FROM QUARTZ_SCHEMA.APOLLO_JOBS 
      WHERE CLASS_CODE = IN_CLASS_CODE;
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
     OUT_CLASS_NAME := '';
  END;
  
  OPEN OUT_PARAMETERS FOR
    SELECT 
  cp.PARAMETER_CODE, 
  cp.REQUIRED_FLAG,
  p.ALWAYS_REQUIRED_FLAG 
FROM QUARTZ_SCHEMA.CLASS_PARAMETERS cp
JOIN QUARTZ_SCHEMA.PARAMETERS p ON cp.PARAMETER_CODE = p.PARAMETER_CODE
WHERE cp.CLASS_CODE = IN_CLASS_CODE;

END GET_CLASS_FOR_CODE;

PROCEDURE DUMMY_PROC_SUCCESS (
  IN_TEXT VARCHAR2,
  OUT_STATUS OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS
BEGIN
     OUT_STATUS := 'Y';
     OUT_MESSAGE := IN_TEXT;
END DUMMY_PROC_SUCCESS;

PROCEDURE DUMMY_PROC_FAILURE (
  IN_TEXT VARCHAR2,
  OUT_STATUS OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS
BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := IN_TEXT;
END DUMMY_PROC_FAILURE;

PROCEDURE INSERT_GROUP
(
    IN_GROUP_ID           IN  GROUPS.GROUP_ID%TYPE,
    IN_DESC               IN  GROUPS.DESCRIPTION%TYPE,
    IN_ACTIVE_FLAG        IN  GROUPS.ACTIVE_FLAG%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_GROUP
 Type:    Procedure
 Syntax:  INSERT_GROUP(group_id, desc, active_flag)

Description:
        This stored procedure inserts/updates a record in table quartz_schema.groups.

Input:
        group_id         varchar2
        desc             varchar2
        active_flag      char

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN
  BEGIN
    INSERT INTO QUARTZ_SCHEMA.GROUPS (
      GROUPS.GROUP_ID, 
      GROUPS.DESCRIPTION, 
      GROUPS.ACTIVE_FLAG 
    ) VALUES (
      IN_GROUP_ID,
      IN_DESC,
      IN_ACTIVE_FLAG
    );
  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    UPDATE QUARTZ_SCHEMA.GROUPS 
      SET 
        GROUPS.DESCRIPTION = IN_DESC, 
        GROUPS.ACTIVE_FLAG = IN_ACTIVE_FLAG 
      WHERE GROUPS.GROUP_ID = IN_GROUP_ID;
  END;
  
  OUT_STATUS := 'Y';
EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_GROUP;

FUNCTION GET_APOLLO_JOBS
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_APOLLO_JOBS
-- Type:    Function
-- Syntax:  GET_APOLLO_JOBS()
-- Returns: ref_cursor for
--                 CLASS_CODE,
--                 CLASS_PATH
--
--
-- Description:   Returns all records in the QUARTZ_SCHEMA.APOLLO_JOBS table.
--
--==============================================================================
AS
    jobs_cursor types.ref_cursor;
BEGIN
     OPEN jobs_cursor FOR
          SELECT a.CLASS_CODE, a.CLASS_PACKAGE FROM QUARTZ_SCHEMA.APOLLO_JOBS a ORDER BY a.CLASS_CODE;

    RETURN jobs_cursor;
END GET_APOLLO_JOBS;

PROCEDURE INSERT_PIPELINE(
  IN_MODULE_ID               IN  PIPELINE.GROUP_ID%TYPE,
  IN_PIPELINE_ID             IN  PIPELINE.PIPELINE_ID%TYPE,
  IN_DESC                    IN  PIPELINE.DESCRIPTION%TYPE,
  IN_ACTIVE_FLAG             IN  PIPELINE.ACTIVE_FLAG%TYPE,
  IN_FORCE_DEFAULT_PAYLOAD   IN  PIPELINE.FORCE_DEFAULT_PAYLOAD%TYPE,
  IN_DEFAULT_SCHEDULE        IN  PIPELINE.DEFAULT_SCHEDULE%TYPE,
  IN_DEFAULT_PAYLOAD         IN  PIPELINE.DEFAULT_PAYLOAD%TYPE,
  IN_CLASS_CODE              IN  PIPELINE.CLASS_CODE%TYPE,
  IN_QUEUE_NAME              IN  PIPELINE.QUEUE_NAME%TYPE,
  IN_TOOL_TIP                IN  PIPELINE.TOOL_TIP%TYPE,
  OUT_STATUS                 OUT VARCHAR2,
  OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_PIPELINE
 Type:    Procedure
 Syntax:  INSERT_PIPELINE(parameters...)

Description:
        This stored procedure inserts/updates a record in table quartz_schema.pipeline.

Input:
        group_id               varchar2
        pipeline_id            varchar2
        desc                   varchar2
        active_flag            char
        force_default_payload  char
        default_schedule       varchar2
        default_payload        varchar2
        class_code             varchar2
        queue_name             varchar2
        tool_tip               varchar2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN
  BEGIN
    INSERT INTO PIPELINE p (
      p.GROUP_ID,
      p.PIPELINE_ID,
      p.DESCRIPTION,
      p.ACTIVE_FLAG,
      p.FORCE_DEFAULT_PAYLOAD,
      p.DEFAULT_SCHEDULE,
      p.DEFAULT_PAYLOAD,
      p.CLASS_CODE,
      p.QUEUE_NAME,
      p.TOOL_TIP               
    ) VALUES (
      IN_MODULE_ID,             
      IN_PIPELINE_ID,           
      IN_DESC,           
      IN_ACTIVE_FLAG,           
      IN_FORCE_DEFAULT_PAYLOAD, 
      IN_DEFAULT_SCHEDULE,      
      IN_DEFAULT_PAYLOAD,       
      IN_CLASS_CODE,            
      IN_QUEUE_NAME,            
      IN_TOOL_TIP               
    );
  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    UPDATE QUARTZ_SCHEMA.PIPELINE p
      SET
        p.DESCRIPTION = IN_DESC,
        p.ACTIVE_FLAG = IN_ACTIVE_FLAG,
        p.FORCE_DEFAULT_PAYLOAD = IN_FORCE_DEFAULT_PAYLOAD,
        p.DEFAULT_SCHEDULE = IN_DEFAULT_SCHEDULE,
        p.DEFAULT_PAYLOAD = IN_DEFAULT_PAYLOAD,
        p.CLASS_CODE = IN_CLASS_CODE,
        p.QUEUE_NAME = IN_QUEUE_NAME,
        p.TOOL_TIP = IN_TOOL_TIP
      WHERE 
        p.GROUP_ID = IN_MODULE_ID AND
        p.PIPELINE_ID = IN_PIPELINE_ID;
  END;
  
  OUT_STATUS := 'Y';
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_PIPELINE;

PROCEDURE DELETE_EMAIL_ADDRESS(
  IN_EMAIL_ADDRESS           IN  EMAIL_ADDRESSES.EMAIL_ADDRESS%TYPE,
  OUT_STATUS                 OUT VARCHAR2,
  OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    DELETE_EMAIL_ADDRESS
 Type:    Procedure
 Syntax:  DELETE_EMAIL_ADDRESS(email_address)

Description:
        This stored procedure deletes a record in table quartz_schema.email_addresses.

Input:
        email_address          varchar2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN
  DELETE FROM QUARTZ_SCHEMA.EMAIL_ADDRESSES e 
    WHERE e.EMAIL_ADDRESS = IN_EMAIL_ADDRESS;
  
  OUT_STATUS := 'Y';
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END DELETE_EMAIL_ADDRESS;

end SCHEDULER_PKG;
/
