create or replace FUNCTION b2b.SHIPTOLOOKUP_FUNC (in_Ship_To_First_Name IN CHAR,
                            in_Ship_To_Last_Name IN CHAR,
                            in_Ship_To_Address_Line1 IN CHAR,
                            in_Ship_To_Address_Line2 IN CHAR,
                            in_Ship_To_City IN CHAR,
                            in_Ship_To_State IN CHAR,
                            in_Ship_To_Zip_Code IN CHAR,
                            in_Ship_to_Country IN CHAR,
                            in_Ship_To_Home_Phone IN CHAR,
                            in_Ship_To_Work_Phone IN CHAR,
                            in_Ship_To_Work_Ext IN CHAR,
                            in_Ship_To_Fax_Number IN CHAR,
                            in_Ship_To_Email IN CHAR,
                            in_User_ID  IN CHAR,
                            in_Address_Type IN CHAR)
RETURN NUMBER
AS
  v_Ship_To_Customer_ID  NUMBER(8);
  v_Ship_To_Count       NUMBER(8);
BEGIN

    SELECT count(*)
    INTO v_Ship_To_Count
    FROM Customer
    WHERE First_Name = in_Ship_To_First_Name
    AND Last_Name = in_Ship_To_Last_Name
    AND Work_Phone = in_Ship_To_Work_Phone;

    /* No Entry exists for First, last and work phone */
    /* Check for first, last, home phone */
    IF v_Ship_To_Count = 0 THEN
      SELECT count(*)
      INTO v_Ship_To_Count
      FROM Customer
      WHERE First_Name = in_Ship_To_First_Name
      AND Last_Name = in_Ship_To_Last_Name
      AND Home_Phone = in_Ship_To_Home_Phone;
    ELSE
      /* Entry exists for First, last and work phone */
      /* Return Customer_ID */
      SELECT Customer_ID
      INTO v_Ship_To_Customer_ID
      FROM Customer
      WHERE First_Name = in_Ship_To_First_Name
      AND Last_Name = in_Ship_To_Last_Name
      AND Work_Phone = in_Ship_To_Work_Phone;

      RETURN v_Ship_To_Customer_ID;
    END IF;

    /* No Entry exists for First, last and home phone */
    /* or first, last and work phone.  New entry */
    IF v_Ship_To_Count = 0 THEN
      SELECT SEQ_1.NEXTVAL
      INTO v_Ship_To_Customer_ID
      FROM DUAL;

      INSERT INTO Customer
      (First_Name, Last_Name, Address_1, Address_2, City, Zip_Code,
       State, Home_Phone, Work_Phone, Fax_Number, Email, Customer_ID,
       Work_Phone_Ext, Last_Update_Date, Last_Update_User, Country_ID,
       Address_Type)
      VALUES
      (in_Ship_To_First_Name, in_Ship_To_Last_Name, in_Ship_To_Address_Line1,
       in_Ship_To_Address_Line2, in_Ship_To_City, in_Ship_To_Zip_Code,
       in_Ship_To_State, in_Ship_To_Home_Phone, in_Ship_To_Work_Phone,
       in_Ship_To_Fax_Number, in_Ship_To_Email, v_Ship_To_Customer_ID,
       in_Ship_To_Work_Ext, SYSDATE, in_USER_ID, in_Ship_to_Country,
       in_Address_Type);
    ELSE
      /* Entry exists for First, Last and Home Phone */
      /* Return Customer ID */
      SELECT Customer_ID
      INTO v_Ship_To_Customer_ID
      FROM Customer
      WHERE First_Name = in_Ship_To_First_Name
      AND Last_Name = in_Ship_To_Last_Name
      AND Home_Phone = in_Ship_To_Home_Phone;
    END IF;

    RETURN v_Ship_To_Customer_ID;
END;
/