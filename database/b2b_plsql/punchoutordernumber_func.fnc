create or replace FUNCTION b2b.PUNCHOUTORDERNUMBER_FUNC
RETURN VARCHAR2
AS
  v_Punchout_Order_Number  VARCHAR2(12);
BEGIN
  SELECT Punchout_Order_Number_Seq.NEXTVAL
  INTO v_Punchout_Order_Number
  FROM DUAL;

  RETURN v_Punchout_Order_Number;
END;
/