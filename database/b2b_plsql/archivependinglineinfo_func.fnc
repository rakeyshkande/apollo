create or replace FUNCTION b2b.ARCHIVEPENDINGLINEINFO_FUNC (in_Buyer_Cookie IN CHAR)
RETURN types.ref_cursor
AS
    v_archive_line_info_cursor   types.ref_cursor;

BEGIN
      OPEN v_archive_line_info_cursor FOR
          SELECT *
          FROM Archive_Pending_LIne_Item
          WHERE buyer_cookie = in_Buyer_Cookie;

    RETURN v_archive_line_info_cursor;


END;
/