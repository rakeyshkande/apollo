create or replace FUNCTION b2b.CLIENTURL_FUNC( in_asn_number IN CHAR)
RETURN types.ref_cursor
AS
    v_client_url_cursor types.ref_cursor;
BEGIN
  OPEN v_client_url_cursor FOR
     SELECT *
     FROM client_url
     WHERE asn_number = in_asn_number;

 RETURN v_client_url_cursor;
END;
/