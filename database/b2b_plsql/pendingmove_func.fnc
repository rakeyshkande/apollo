create or replace FUNCTION b2b.PENDINGMOVE_FUNC (in_Buyer_Cookie  IN CHAR)

RETURN VARCHAR2
AS
    v_Pending_Count   NUMBER;
    v_Pending_Line_Count  NUMBER;
    v_move_cookie VARCHAR2(100);
BEGIN

    SELECT count(*)
    INTO v_Pending_Count
    FROM Pending
    WHERE buyer_cookie = in_Buyer_Cookie;

    SELECT count(*)
    INTO v_Pending_Line_Count
    FROM Pending_Line_Item
    WHERE buyer_cookie = in_Buyer_Cookie;

    IF v_Pending_Count != 0 OR v_Pending_Line_Count != 0 THEN
    
      -- Logic for handling - Same cookie in pending twice but different order numbers.
      --
      -- Instead of deleting existing, change it to something that will never be accessed so we can track if 
      -- website is sending any updated carts with new order numbers. 
      -- This is a kludgey way of doing things, but it's throw-away code so good enough for now.
      --
      SELECT '--MOVE' || to_char(SYSDATE, 'SSSSS') || ':' || in_Buyer_Cookie  INTO v_move_cookie FROM DUAL;
           
      INSERT INTO pending (buyer_cookie, asn_number, payload_id, created_date, updated_on)
             (SELECT v_move_cookie, asn_number, payload_id, created_date, SYSDATE
              FROM pending where buyer_cookie = in_Buyer_Cookie);
    
      UPDATE pending_line_item 
      SET buyer_cookie = v_move_cookie,
          ftd_order_number = substr((ftd_order_number || to_char(SYSDATE, '-SSSSS')), 0, 20),
          updated_on = SYSDATE
      WHERE buyer_cookie = in_Buyer_Cookie;      

      DELETE FROM Pending_Line_Item
      WHERE buyer_cookie = in_Buyer_cookie;

      DELETE FROM Pending
      WHERE buyer_cookie = in_buyer_cookie;

      RETURN 'True';
    ELSE
      RETURN 'False';
    END IF;

END;
/