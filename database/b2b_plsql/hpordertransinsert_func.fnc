create or replace FUNCTION b2b.HPORDERTRANSINSERT_FUNC (in_hp_socket_value  IN  CHAR)
RETURN VARCHAR2
AS
BEGIN

      INSERT INTO HP_Order_Transmission
      (HP_Sequence_Number, HP_Socket_Value, HP_Sent)
      VALUES
      (HP_SEQUENCE_NUMBER.NEXTVAL, in_hp_socket_value, 'N');

      RETURN 'True';

END;
/