create or replace FUNCTION b2b.ORDERDELETETRANSINSERT_FUNC (in_Buyer_Cookie IN CHAR,
                                     in_Send_Date IN DATE)
RETURN VARCHAR2
AS
BEGIN

      INSERT INTO Order_Delete_Transmission
      VALUES
      (in_Buyer_Cookie, in_Send_Date);

      RETURN 'True';

END;
/