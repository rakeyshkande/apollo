create or replace FUNCTION b2b.HPORDERTRANSSELECT_FUNC
RETURN types.ref_cursor
AS
  v_HP_Order_Trans_Cursor   types.ref_cursor;
BEGIN

  OPEN v_HP_Order_Trans_Cursor FOR
    SELECT HP_Sequence_Number, HP_Socket_Value, HP_Sent
    FROM HP_Order_Transmission
    WHERE HP_Sent = 'N';

  RETURN v_HP_Order_Trans_Cursor;
END;
/