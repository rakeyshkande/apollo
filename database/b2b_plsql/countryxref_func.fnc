create or replace FUNCTION b2b.COUNTRYXREF_FUNC  (in_Country_Code  IN CHAR)
RETURN VARCHAR2
AS
  v_HP_Country_Code VARCHAR2(2);
BEGIN

  IF length(in_Country_code) = 3 THEN
    SELECT HP_Country_Code
    INTO v_HP_Country_Code
    FROM Country_Xref
    WHERE Ariba_Country_Code = in_Country_Code;
  ELSE
    SELECT HP_Country_code
    INTO v_HP_Country_Code
    FROM Country_Xref
    WHERE HP_Country_code = in_Country_Code;
  END IF;

  RETURN v_HP_Country_Code;
END;
/