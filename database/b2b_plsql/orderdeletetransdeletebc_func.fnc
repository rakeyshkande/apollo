create or replace FUNCTION b2b.ORDERDELETETRANSDELETEBC_FUNC (in_Buyer_Cookie in CHAR)
RETURN VARCHAR2
AS
BEGIN

    DELETE FROM Order_Delete_Transmission
    WHERE Buyer_Cookie = in_Buyer_Cookie;
    RETURN 'True';

END;
/