create or replace FUNCTION b2b.ORDERDELETETRANSINFO_FUNC(in_Buyer_Cookie  IN CHAR)
RETURN types.ref_cursor
AS
  v_order_delete_trans_cursor   types.ref_cursor;
BEGIN
    OPEN v_order_delete_trans_cursor FOR
      SELECT *
      FROM Order_Delete_Transmission
      WHERE Buyer_Cookie = in_Buyer_Cookie;

    RETURN v_order_delete_trans_cursor;

END;
/