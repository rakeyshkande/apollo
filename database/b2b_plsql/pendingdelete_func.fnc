create or replace FUNCTION b2b.PENDINGDELETE_FUNC (in_Buyer_Cookie  IN CHAR)

RETURN VARCHAR2
AS
    v_Pending_Count   NUMBER;
    v_Pending_Line_Count  NUMBER;
BEGIN

    SELECT count(*)
    INTO v_Pending_Count
    FROM Pending
    WHERE buyer_cookie = in_Buyer_Cookie;

    SELECT count(*)
    INTO v_Pending_Line_Count
    FROM Pending_Line_Item
    WHERE buyer_cookie = in_Buyer_Cookie;

    IF v_Pending_Count != 0 OR v_Pending_Line_Count != 0 THEN
      DELETE FROM Pending_Line_Item
      WHERE buyer_cookie = in_Buyer_cookie;

      DELETE FROM Pending
      WHERE buyer_cookie = in_buyer_cookie;

      RETURN 'True';
    ELSE
      RETURN 'False';
    END IF;

END;
/