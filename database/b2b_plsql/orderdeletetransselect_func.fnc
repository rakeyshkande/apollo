create or replace FUNCTION b2b.ORDERDELETETRANSSELECT_FUNC (in_Hold_Days IN NUMBER)
RETURN types.ref_cursor
AS
    v_Order_Delete_Trans_Cursor   types.ref_cursor;
    v_Delete_Date   DATE;
BEGIN

  v_Delete_Date := (SYSDATE - in_Hold_Days);

  OPEN v_Order_Delete_Trans_Cursor FOR
    SELECT *
    FROM Order_Delete_Transmission
    WHERE Send_Date < v_Delete_Date;

    RETURN v_Order_Delete_Trans_Cursor;
END;
/