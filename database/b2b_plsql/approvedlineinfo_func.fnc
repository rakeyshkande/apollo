create or replace FUNCTION b2b.APPROVEDLINEINFO_FUNC (in_Buyer_Cookie IN CHAR)
RETURN types.ref_cursor
AS
    v_approved_line_info_cursor   types.ref_cursor;

BEGIN
      OPEN v_approved_line_info_cursor FOR
          SELECT *
          FROM Approved_Line_Item
          WHERE buyer_cookie = in_Buyer_Cookie;

    RETURN v_approved_line_info_cursor;


END;
/