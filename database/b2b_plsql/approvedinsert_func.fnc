create or replace FUNCTION B2B.APPROVEDINSERT_FUNC (in_Buyer_Cookie IN CHAR,
                              in_Order_Number IN CHAR,
                              in_Ariba_PO_Number  IN CHAR,
                              in_Ariba_Line_Number  IN NUMBER,
                              in_Tax_Amount  IN FLOAT,
                              in_Order_Total  IN FLOAT,
                              in_Bill_To_First_Name  IN CHAR,
                              in_Bill_To_Last_Name  IN CHAR,
                              in_Bill_To_Address_Line_1  IN CHAR,
                              in_Bill_To_Address_Line_2  IN CHAR,
                              in_Bill_To_City  IN CHAR,
                              in_Bill_To_State  IN CHAR,
                              in_Bill_To_Zip_Code  IN CHAR,
                              in_Bill_To_Country  IN CHAR,
                              in_Bill_To_Home_Phone  IN CHAR,
                              in_Bill_To_Work_Phone  IN CHAR,
                              in_Bill_To_Work_Extension  IN CHAR,
                              in_Bill_To_Fax_Number  IN CHAR,
                              in_Bill_To_Email  IN CHAR,
                              in_Ship_To_First_Name  IN CHAR,
                              in_Ship_To_Last_Name  IN CHAR,
                              in_Ship_To_Address_Line_1  IN CHAR,
                              in_Ship_To_Address_Line_2  IN CHAR,
                              in_Ship_To_City  IN CHAR,
                              in_Ship_To_State  IN CHAR,
                              in_Ship_To_Zip_Code  IN CHAR,
                              in_Ship_To_Country  IN CHAR,
                              in_Ship_To_Home_Phone  IN CHAR,
                              in_Ship_To_Work_Phone  IN CHAR,
                              in_Ship_To_Work_Extension  IN CHAR,
                              in_Ship_To_Fax_Number  IN CHAR,
                              in_Ship_To_Email  IN CHAR,
                              in_Ship_To_Business_Type IN CHAR,
                              in_Ship_To_Business_Name IN CHAR,
                              in_Occasion  IN CHAR,
                              in_Item_number  IN CHAR,
                              in_Item_Description  IN CHAR,
                              in_Item_Price  IN FLOAT,
                              in_AddOn_Count  IN NUMBER,
                              in_AddOn_Detail  IN CHAR,
                              in_Service_Fee  IN FLOAT,
                              in_Delivery_Date  IN DATE,
                              in_Card_Message IN CHAR,
                              in_Special_Instructions IN CHAR,
                              in_Master_Order_Number  IN CHAR,
                              in_Item_UNSPSC_Code  IN CHAR,
                              in_Ariba_Cost_Center  IN CHAR,
                              in_AMS_Project_Code IN CHAR,
                              in_ASN_Number  IN CHAR,
                              in_PayloadID  IN CHAR,
                              in_Created_Date IN DATE,
                              in_Approved_Date in DATE,
                              in_Retail_Price in FLOAT,
                              in_Extra_Shipping_Fee in FLOAT,
                              in_Drop_Ship_Charges in FLOAT)

RETURN VARCHAR2
AS
   v_approved_count   NUMBER(10);
BEGIN

    SELECT count(*) INTO v_approved_count
    FROM Approved
    WHERE buyer_cookie = in_Buyer_Cookie;

    IF v_approved_count = 0 THEN
      /* Records do not exist in Approved Table */
      INSERT INTO Approved
      VALUES
      (in_Buyer_Cookie, in_ASN_Number, in_PayloadID,
      in_Created_Date, in_Approved_Date, SYSDATE);
    END IF;

    INSERT INTO Approved_Line_Item
    (BUYER_COOKIE,
    FTD_ORDER_NUMBER,
    ARIBA_PO_NUMBER,
    ARIBA_LINE_NUMBER,
    ARIBA_LINE_COST_CENTER,
    TAX_AMOUNT,
    ORDER_TOTAL,
    BILL_TO_FIRST_NAME,
    BILL_TO_LAST_NAME,
    BILL_TO_ADDRESS_LINE1,
    BILL_TO_ADDRESS_LINE2,
    BILL_TO_CITY,
    BILL_TO_STATE,
    BILL_TO_ZIP_CODE,
    BILL_TO_COUNTRY,
    BILL_TO_HOME_PHONE,
    BILL_TO_WORK_PHONE,
    BILL_TO_WORK_EXT,
    BILL_TO_FAX_NUMBER,
    BILL_TO_EMAIL,
    SHIP_TO_FIRST_NAME,
    SHIP_TO_LAST_NAME,
    SHIP_TO_ADDRESS_LINE1,
    SHIP_TO_ADDRESS_LINE2,
    SHIP_TO_CITY,
    SHIP_TO_STATE,
    SHIP_TO_ZIP_CODE,
    SHIP_TO_COUNTRY,
    SHIP_TO_HOME_PHONE,
    SHIP_TO_WORK_PHONE,
    SHIP_TO_WORK_EXT,
    SHIP_TO_FAX_NUMBER,
    SHIP_TO_EMAIL,
    SHIP_TO_BUSINESS_TYPE,
    SHIP_TO_BUSINESS_NAME,
    OCCASION,
    ITEM_NUMBER,
    ITEM_DESCRIPTION,
    ITEM_PRICE,
    ADD_ON_COUNT,
    ADD_ON_DETAIL,
    SERVICE_FEE,
    DELIVERY_DATE,
    CARD_MESSAGE,
    SPECIAL_INSTRUCTIONS,
    MASTER_ORDER_NUMBER,
    UNSPSC_CODE,
    AMS_PROJECT_CODE,
    RETAIL_PRICE,
    EXTRA_SHIPPING_FEE,
    DROP_SHIP_CHARGES,
    UPDATED_ON)
    VALUES
    (in_Buyer_Cookie, in_Order_Number, in_Ariba_PO_Number, in_Ariba_Line_Number,
    in_Ariba_Cost_Center, in_Tax_Amount, in_Order_Total, in_Bill_To_First_Name,
    in_Bill_To_Last_Name, in_Bill_To_Address_Line_1, in_Bill_To_Address_Line_2,
    in_Bill_To_City, in_Bill_To_State, in_Bill_To_Zip_Code, in_Bill_To_Country,
    in_Bill_To_Home_Phone, in_Bill_To_Work_Phone, in_Bill_To_Work_Extension,
    in_Bill_To_Fax_Number, in_Bill_To_Email, in_Ship_To_First_Name,
    in_Ship_To_Last_Name, in_Ship_To_Address_Line_1, in_Ship_To_Address_Line_2,
    in_Ship_To_City, in_Ship_To_State, in_Ship_To_Zip_Code, in_Ship_To_Country,
    in_Ship_To_Home_Phone, in_Ship_To_Work_Phone, in_Ship_To_Work_Extension,
    in_Ship_To_Fax_Number, in_Ship_To_Email, in_Ship_To_Business_Type, 
    in_Ship_To_Business_Name, in_Occasion, in_Item_number,
    in_Item_Description, in_Item_Price, in_AddOn_Count, in_AddOn_Detail,
    in_Service_Fee, in_Delivery_Date, in_Card_Message, in_Special_Instructions,
    in_Master_Order_Number, in_Item_UNSPSC_Code, in_AMS_Project_Code,in_Retail_Price,
    in_Extra_Shipping_Fee, in_Drop_Ship_Charges, SYSDATE);

    RETURN 'True';

END;
/