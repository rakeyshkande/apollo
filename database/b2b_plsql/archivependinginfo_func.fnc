create or replace FUNCTION b2b.ARCHIVEPENDINGINFO_FUNC (in_Order_Number IN CHAR)
RETURN types.ref_cursor
AS
    v_archive_pending_info_cursor   types.ref_cursor;

BEGIN
      OPEN v_archive_pending_info_cursor FOR
          SELECT a1.*
          FROM Archive_Pending a1, Archive_Pending_Line_Item b1
          WHERE b1.FTD_Order_Number = in_Order_Number
          AND b1.Buyer_Cookie = a1.Buyer_Cookie;

    RETURN v_archive_pending_info_cursor;


END;
/