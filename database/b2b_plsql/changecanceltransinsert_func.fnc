create or replace FUNCTION b2b.CHANGECANCELTRANSINSERT_FUNC(in_socket_value IN CHAR)
RETURN VARCHAR2
AS
  v_socket_seq_number NUMBER;

BEGIN

  SELECT Socket_Sequence_Number.NEXTVAL
  INTO v_socket_seq_number
  FROM DUAL;

  INSERT INTO Change_Cancel_Transmission
  VALUES
  (v_socket_seq_number, in_socket_value);

  RETURN 'True';
END;
/