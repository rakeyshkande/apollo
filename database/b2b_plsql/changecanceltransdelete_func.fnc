create or replace FUNCTION b2b.CHANGECANCELTRANSDELETE_FUNC(in_socket_seq_number IN NUMBER)
RETURN VARCHAR2
AS
  v_Change_Cancel_Count NUMBER;
BEGIN

  SELECT count(*)
  INTO v_Change_Cancel_Count
  FROM Change_Cancel_Transmission
  WHERE socket_sequence_number = in_socket_seq_number;

  IF v_Change_Cancel_Count > 0 THEN
    DELETE FROM Change_Cancel_Transmission
    WHERE socket_sequence_number = in_socket_seq_number;

    RETURN 'True';
  ELSE
    RETURN 'False';
  END IF;

END;
/