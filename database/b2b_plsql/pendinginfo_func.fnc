create or replace FUNCTION b2b.PENDINGINFO_FUNC (in_FTD_ORDER_NUMBER IN CHAR)
RETURN types.ref_cursor
AS
    v_pending_info_cursor   types.ref_cursor;
    v_move_cookie VARCHAR2(100);
    v_actual_cookie VARCHAR2(100);
    v_pending_line_count   NUMBER;

BEGIN

    -- See if order is in pending
    SELECT count(*)
    INTO   v_pending_line_count
    FROM   pending_line_item 
    WHERE  ftd_order_number = in_FTD_Order_Number;

    -- If order not in pending, also check if it was a moved order    
    IF v_pending_line_count = 0 THEN 
      
       -- Logic for handling - Same cookie in pending twice but different order numbers.
       --
       -- When a pending order is updated from website, the original order is moved instead of deleted
       -- since the website sometimes erroneously sends us a different order number in edited cart.
       -- This is a means to find that original order.
       -- This is a kludgey way of doing things, but it's throw-away code so good enough for now.
       --
       BEGIN
           SELECT DISTINCT buyer_cookie
           INTO   v_move_cookie
           FROM   pending_line_item 
           WHERE  ftd_order_number like in_FTD_Order_Number || '-%'
           AND    buyer_cookie like '--MOVE%';
           IF v_move_cookie IS NOT NULL THEN
              -- Yup, order was moved so use it's cookie to get order that website replaced it with
              SELECT REGEXP_REPLACE(v_move_cookie,'--MOVE\d+:','')
              INTO v_actual_cookie 
              FROM DUAL;
    
              -- Select information for order associated with a buyer cookie   
              OPEN v_pending_info_cursor FOR
                  SELECT *
                  FROM  pending
                  WHERE buyer_cookie = v_actual_cookie;
           END IF;
       EXCEPTION
          WHEN NO_DATA_FOUND THEN
              OPEN v_pending_info_cursor FOR
                 SELECT a1.*
                 FROM Pending a1, Pending_Line_Item b1
                 WHERE b1.FTD_Order_Number = in_FTD_Order_Number
                 and b1.Buyer_Cookie = a1.Buyer_Cookie;              
       END; 
        
    ELSE
       -- Order is in pending, so select information for order associated with its buyer cookie 
      OPEN v_pending_info_cursor FOR
         SELECT a1.*
         FROM Pending a1, Pending_Line_Item b1
         WHERE b1.FTD_Order_Number = in_FTD_Order_Number
         and b1.Buyer_Cookie = a1.Buyer_Cookie;    
    END IF;

    RETURN v_pending_info_cursor;

END;
/