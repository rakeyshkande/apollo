create or replace FUNCTION b2b.SOURCECODELOOKUP_FUNC(in_ASN_Number  IN CHAR)
RETURN VARCHAR2
AS
  v_Source_Code_Count   NUMBER(10);
  v_Buyer_Source_Code   VARCHAR2(10);
BEGIN
    SELECT count(*)
    INTO v_Source_Code_Count
    FROM Buyer_Source_Code
    WHERE ASN_Number = in_ASN_Number;

    IF v_Source_Code_Count > 0 THEN
        /*Retrieve source code for Buyer's ASN Number */
        SELECT source_code
        INTO v_Buyer_Source_Code
        FROM Buyer_Source_Code
        WHERE ASN_Number = in_ASN_Number;

        RETURN v_Buyer_Source_Code;
    ELSE
        RETURN NULL;
    END IF;
END;
/