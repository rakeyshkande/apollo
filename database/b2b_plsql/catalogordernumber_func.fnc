create or replace FUNCTION b2b.CATALOGORDERNUMBER_FUNC
RETURN VARCHAR2
AS
  v_Catalog_Order_Number  VARCHAR2(12);
BEGIN
  SELECT Catalog_Order_Number_Seq.NEXTVAL
  INTO v_CAtalog_Order_Number
  FROM DUAL;

  RETURN v_Catalog_Order_Number;
END;
/