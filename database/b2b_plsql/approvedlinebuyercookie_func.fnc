create or replace FUNCTION b2b.APPROVEDLINEBUYERCOOKIE_FUNC (in_FTD_Order_Number IN char)
RETURN VARCHAR2
AS
  v_Buyer_cookie  VARCHAR2(50);
  v_Buyer_Cookie_Count  Number;
BEGIN
  SELECT count(*)
  INTO v_Buyer_Cookie_Count
  FROM Approved_Line_Item
  WHERE FTD_Order_Number = in_FTD_Order_Number;

  IF v_Buyer_Cookie_Count > 0 
  THEN
    SELECT Buyer_Cookie
    INTO v_Buyer_Cookie
    FROM Approved_Line_Item
    WHERE FTD_Order_Number = in_FTD_Order_Number;
    RETURN v_Buyer_Cookie;
  ELSE
    RETURN 'False';
  END IF;
END;
/
