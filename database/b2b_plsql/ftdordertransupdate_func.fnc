create or replace FUNCTION b2b.FTDORDERTRANSUPDATE_FUNC (in_FTD_Sequence_Number IN NUMBER, in_sent_flag IN CHAR)
RETURN VARCHAR2
AS
BEGIN

  UPDATE FTD_Order_Transmission
  SET FTD_SENT = in_sent_flag,
      SEND_DATE = SYSDATE
  WHERE FTD_Sequence_Number = in_FTD_Sequence_Number;

  RETURN 'True';
END;
/