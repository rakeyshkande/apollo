create or replace FUNCTION b2b.CLIENTOPTIONS_FUNC
RETURN types.ref_cursor
AS
    v_client_options_cursor types.ref_cursor;
BEGIN
  OPEN v_client_options_cursor FOR
     SELECT *
     FROM client_options;

 RETURN v_client_options_cursor;
END;
/