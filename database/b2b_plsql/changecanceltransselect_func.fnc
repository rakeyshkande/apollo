create or replace FUNCTION b2b.CHANGECANCELTRANSSELECT_FUNC
RETURN types.ref_cursor
AS
  v_Change_Cancel_Trans_Cursor   types.ref_cursor;
BEGIN

  OPEN v_Change_Cancel_Trans_Cursor FOR
    SELECT *
    FROM Change_Cancel_Transmission;

  RETURN v_Change_Cancel_Trans_Cursor;

END;
/