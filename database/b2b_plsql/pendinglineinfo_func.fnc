create or replace FUNCTION b2b.PENDINGLINEINFO_FUNC (in_Buyer_Cookie IN CHAR)

RETURN types.ref_cursor
AS
    v_pending_line_info_cursor   types.ref_cursor;
BEGIN

    /* Select information for lines associated with a buyer cookie */

    OPEN v_pending_line_info_cursor FOR

      SELECT *
      FROM Pending_Line_Item
      WHERE Buyer_Cookie = in_Buyer_Cookie;

    RETURN v_pending_line_info_cursor;


END;
/