create or replace FUNCTION b2b.MASTERORDERNUMBER_FUNC
RETURN VARCHAR2
AS
  v_Master_Order_Number  VARCHAR2(12);
BEGIN
  SELECT Master_Order_Number_Seq.NEXTVAL
  INTO v_Master_Order_Number
  FROM DUAL;

  RETURN v_Master_Order_Number;
END;
/