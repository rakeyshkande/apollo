create or replace FUNCTION b2b.ADDONINFO_FUNC (in_addon_id  IN CHAR)
RETURN types.ref_cursor
AS
  v_addon_info_cursor   types.ref_cursor;

BEGIN

  OPEN v_addon_info_cursor FOR
    SELECT * FROM AddOn
    WHERE addon_id = in_addon_id;

  RETURN v_addon_info_cursor;

END;
/