create or replace FUNCTION b2b.FTDORDERTRANSINSERT_FUNC (in_transmission  IN  CLOB, in_sent_flag IN CHAR)
RETURN VARCHAR2
AS
BEGIN

      INSERT INTO FTD_Order_Transmission
      (FTD_Sequence_Number, FTD_Transmission, FTD_Sent)
      VALUES
      (FTD_SEQUENCE_NUMBER.NEXTVAL, in_transmission, in_sent_flag);

      RETURN 'True';
END;
/