create or replace FUNCTION b2b.ARCHIVEPENDINGDELETE_FUNC (in_Buyer_Cookie  IN CHAR)

RETURN VARCHAR2
AS
    v_Archive_Pending_Count   NUMBER;
    v_Archive_Pending_Line_Count  NUMBER;
BEGIN

    SELECT count(*)
    INTO v_Archive_Pending_Count
    FROM Archive_Pending
    WHERE buyer_cookie = in_Buyer_Cookie;

    SELECT count(*)
    INTO v_Archive_Pending_Line_Count
    FROM Archive_Pending_Line_Item
    WHERE buyer_cookie = in_Buyer_Cookie;

    IF v_Archive_Pending_Count != 0 OR v_Archive_Pending_Line_Count != 0 THEN
      DELETE FROM Archive_Pending_Line_Item
      WHERE buyer_cookie = in_Buyer_cookie;

      DELETE FROM Archive_Pending
      WHERE buyer_cookie = in_buyer_cookie;

      RETURN 'True';
    ELSE
      RETURN 'False';
    END IF;

END;
/