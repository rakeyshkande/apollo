create or replace FUNCTION b2b.FTDORDERTRANSSELECT_FUNC
RETURN types.ref_cursor
AS
  v_FTD_Order_Trans_Cursor   types.ref_cursor;
BEGIN

  OPEN v_FTD_Order_Trans_Cursor FOR
    SELECT FTD_Sequence_Number, FTD_TRANSMISSION, FTD_SENT,SEND_DATE
    FROM FTD_Order_Transmission
    WHERE FTD_SENT = 'N' OR FTD_SENT = 'E';

  RETURN v_FTD_Order_Trans_Cursor;
END;
/
