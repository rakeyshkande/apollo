create or replace FUNCTION b2b.APPROVEDDELETE_FUNC (in_Buyer_Cookie  IN CHAR)

RETURN VARCHAR2
AS
    v_Approved_Count   NUMBER;
    v_Approved_Line_Count  NUMBER;
BEGIN

    SELECT count(*)
    INTO v_Approved_Count
    FROM Approved
    WHERE buyer_cookie = in_Buyer_Cookie;

    SELECT count(*)
    INTO v_Approved_Line_Count
    FROM Approved_Line_Item
    WHERE buyer_cookie = in_Buyer_Cookie;

    IF v_Approved_Count != 0 OR v_Approved_Line_Count != 0 THEN
      DELETE FROM Approved_Line_Item
      WHERE buyer_cookie = in_Buyer_cookie;

      DELETE FROM Approved
      WHERE buyer_cookie = in_buyer_cookie;

      RETURN 'True';
    ELSE
      RETURN 'False';
    END IF;

END;
/