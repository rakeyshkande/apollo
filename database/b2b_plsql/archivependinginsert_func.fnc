create or replace FUNCTION b2b.ARCHIVEPENDINGINSERT_FUNC(in_Order_Number   IN CHAR,
                                in_Tax_Amount  IN FLOAT,
                                in_Order_Total IN FLOAT,
                                in_Occasion  IN CHAR,
                                in_Item_Number IN CHAR,
                                in_Item_price  IN FLOAT,
                                in_Addon_Count IN NUMBER,
                                in_Addon_Detail  IN CHAR,
                                in_Service_Fee IN FLOAT,
                                in_Delivery_Date IN DATE,
                                in_Master_Order_Number IN CHAR,
                                in_Item_UNSPSC_Code  IN CHAR,
                                in_Item_Description  IN CHAR,
                                in_PayloadID IN CHAR,
                                in_Buyer_Cookie  IN CHAR,
                                in_ASN_Number  IN CHAR,
                                in_Created_Date IN DATE,
                                in_ship_first_name IN CHAR,
                                in_ship_last_name IN CHAR,
                                in_ship_address1  IN CHAR,
                                in_ship_address2  IN CHAR,
                                in_ship_to_city  IN CHAR,
                                in_ship_to_state  IN CHAR,
                                in_ship_to_zip_code  IN CHAR,
                                in_ship_to_country  IN CHAR,
                                in_ship_to_phone  IN CHAR,
                                in_ship_to_ext  IN CHAR,
                                in_card_message  IN CHAR,
                                in_special_instructions   IN CHAR,
                               in_retail_price IN FLOAT,
                                in_bill_first_name IN CHAR,
                                in_bill_last_name IN CHAR,
                                in_bill_address1  IN CHAR,
                                in_bill_address2  IN CHAR,
                                in_bill_to_city  IN CHAR,
                                in_bill_to_state  IN CHAR,
                                in_bill_to_zip_code  IN CHAR,
                                in_bill_to_country  IN CHAR,
                                in_bill_to_work_phone  IN CHAR,
                                in_bill_to_work_phone_ext  IN CHAR,
                                in_bill_to_email IN CHAR,
                                in_extra_shipping_fee IN FLOAT,
                                in_drop_ship_charges IN FLOAT)
RETURN VARCHAR2
AS
   v_archive_pending_count   NUMBER(10);
BEGIN

    SELECT count(*)
    INTO v_archive_pending_count
    FROM Archive_Pending
    WHERE buyer_cookie = in_Buyer_Cookie
    AND asn_number = in_ASN_Number;

    IF v_archive_pending_count = 0 THEN
      /* Records do not exist in Archive Pending Table */
      INSERT INTO Archive_Pending
      VALUES
      (in_Buyer_Cookie, in_ASN_Number, in_PayloadID,
      in_Created_Date, SYSDATE);
    END IF;

    INSERT INTO Archive_Pending_Line_Item
    ( BUYER_COOKIE,
 FTD_ORDER_NUMBER,
 TAX_AMOUNT,
 ORDER_TOTAL,
 OCCASION,
 ITEM_NUMBER,
 ITEM_DESCRIPTION,
 ITEM_PRICE,
 ADD_ON_COUNT,
 ADD_ON_DETAIL,
 SERVICE_FEE,
 DELIVERY_DATE,
 MASTER_ORDER_NUMBER,
 UNSPSC_CODE,
  SHIP_TO_FIRST_NAME,
 SHIP_TO_LAST_NAME,
  SHIP_TO_ADDRESS_LINE1,
 SHIP_TO_ADDRESS_LINE2,
 SHIP_TO_CITY,
 SHIP_TO_STATE,
 SHIP_TO_ZIP_CODE,
 SHIP_TO_COUNTRY,
 SHIP_TO_PHONE,
 SHIP_TO_EXT,
 CARD_MESSAGE,
 SPECIAL_INSTRUCTIONS,
 RETAIL_PRICE,
 BILL_TO_FIRST_NAME,
 BILL_TO_LAST_NAME,
 BILL_TO_ADDRESS_LINE1,
 BILL_TO_ADDRESS_LINE2,
 BILL_TO_CITY,
 BILL_TO_STATE,
 BILL_TO_ZIP_CODE,
 BILL_TO_COUNTRY,
 BILL_TO_WORK_PHONE,
 BILL_TO_WORK_EXT,
 BILL_TO_EMAIL,
 EXTRA_SHIPPING_FEE,
 DROP_SHIP_CHARGES)
    VALUES
    (in_Buyer_Cookie, in_Order_Number, in_Tax_Amount, in_Order_Total,
    in_Occasion, in_Item_number, in_Item_Description, in_Item_Price,
    in_AddOn_Count, in_AddOn_Detail, in_Service_Fee, in_Delivery_Date,
    in_Master_Order_Number, in_Item_UNSPSC_Code,
   in_ship_first_name,
    in_ship_last_name,
    in_ship_address1,
    in_ship_address2,
    in_ship_to_city,
    in_ship_to_state,
    in_ship_to_zip_code,
    in_ship_to_country,
    in_ship_to_phone,
    in_ship_to_ext,
    in_card_message,
    in_special_instructions,
    in_retail_price,
    in_bill_first_name,
    in_bill_last_name,
    in_bill_address1,
    in_bill_address2,
    in_bill_to_city,
    in_bill_to_state,
    in_bill_to_zip_code,
    in_bill_to_country,
    in_bill_to_work_phone,
    in_bill_to_work_phone_ext,
    in_bill_to_email,
    in_extra_shipping_fee,
    in_drop_ship_charges);


    RETURN 'True';

END;
/