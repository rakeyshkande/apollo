create or replace FUNCTION b2b.ORDERDELETETRANSDELETE_FUNC (in_Hold_Days IN NUMBER)
RETURN VARCHAR2
AS
    v_Delete_Date   DATE;
BEGIN

    v_Delete_Date := (SYSDATE - in_Hold_Days);

    DELETE FROM Order_Delete_Transmission
    WHERE Send_Date < v_Delete_Date;
    RETURN 'True';

END;
/