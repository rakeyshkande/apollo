create or replace FUNCTION b2b.PENDINGSINGLELINEINFO_FUNC (in_FTD_Order_Number  IN CHAR,
                                     in_Item_Number IN CHAR)
RETURN types.ref_cursor
AS
    v_pending_single_info_cursor   types.ref_cursor;
BEGIN
    /* Select information for a singel line base on the FTD Order Number */
    /* and the Item Number values */

    OPEN v_pending_single_info_cursor FOR

      SELECT *
      FROM Pending_Line_Item
      WHERE FTD_Order_Number = in_FTD_Order_Number
      AND Item_Number = in_Item_Number;

    RETURN v_pending_single_info_cursor;
END;
/