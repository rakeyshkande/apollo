create or replace FUNCTION b2b.COUNTRYXREFBYNAME_FUNC  (in_Country_Name  IN CHAR)
RETURN VARCHAR2
AS
  v_HP_Country_Code VARCHAR2(2);
BEGIN


    SELECT HP_Country_Code
    INTO v_HP_Country_Code
    FROM Country_Xref
    WHERE country_name = in_Country_Name;


  RETURN v_HP_Country_Code;
END;
/