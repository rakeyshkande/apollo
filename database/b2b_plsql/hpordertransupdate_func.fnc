create or replace FUNCTION b2b.HPORDERTRANSUPDATE_FUNC (in_HP_Sequence_Number IN NUMBER)
RETURN VARCHAR2
AS
BEGIN

  UPDATE HP_Order_Transmission
  SET HP_Sent = 'Y',
      Send_Date = SYSDATE
  WHERE HP_Sequence_Number = in_HP_Sequence_Number;

  RETURN 'True';
END;
/