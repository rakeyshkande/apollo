package com.ftd.newsletterevents.vo;

public interface INewsletterEvent {
    public static final String STATUS_SUBSCRIBED = "S";
    public static final String STATUS_UNSUBSCRIBED = "U";

    public String getEmailAddress();

    public String getCompanyId();

    public String getOriginAppName();

    public String getStatus();
    
     public Long getTimestamp();
     
     public boolean isRedelivered();
     
     public String getOperatorId();
     
     public Long getOriginTimestamp();
}
