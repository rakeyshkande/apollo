package com.ftd.newsletterevents.vo;

public interface IMetadata {
    public void setTimestamp(String timestamp);
    
    public void setRedelivered(boolean ind);
}
