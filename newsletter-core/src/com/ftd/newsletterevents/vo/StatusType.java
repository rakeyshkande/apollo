package com.ftd.newsletterevents.vo;
import java.io.Serializable;

public final class StatusType implements Serializable {
    static final long serialVersionUID = -6830899719384652622L;
    public static final StatusType SUBSCRIBED = new StatusType("S");
    public static final StatusType UNSUBSCRIBED = new StatusType("U");
    private String type;

    private StatusType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }

    public int hashCode() {
        return type.hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof StatusType) {
            return this.toString().equals(that.toString());
        }

        return false;
    }
}