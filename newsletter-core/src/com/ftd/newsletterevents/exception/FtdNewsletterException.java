package com.ftd.newsletterevents.exception;

public class FtdNewsletterException extends Exception {
    public FtdNewsletterException(String message) {
        super(message);
    }

    public FtdNewsletterException(String message, Throwable t) {
        super(message, t);
    } 
}