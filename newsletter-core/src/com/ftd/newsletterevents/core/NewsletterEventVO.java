package com.ftd.newsletterevents.core;

import com.ftd.newsletterevents.vo.IMetadata;
import com.ftd.newsletterevents.vo.INewsletterEvent;

import java.io.Serializable;


public class NewsletterEventVO implements INewsletterEvent, IMetadata,
    Serializable {
    static final long serialVersionUID = -2270827394448322560L;

    private String emailAddress;
    private String companyId;
    private String originAppName;
    private String status;
    private Long timestamp = new Long(0);
    private boolean isRedelivered;
    Long originTimestamp;
    String operatorId;

    /*
     * The empty constructor used exclusively by the XMLService for unmarshalling.
     */
    NewsletterEventVO() {
    }

//    public NewsletterEventVO(String emailAddress, String companyId,
//        StatusType status, String operatorId, String originAppName) {
//        this(emailAddress, companyId, status, operatorId, originAppName, null);
//    }
    
    public NewsletterEventVO(String emailAddress, String companyId,
        String status, String operatorId, String originAppName, Long originTimestamp) {
        this.emailAddress = emailAddress;
        this.companyId = companyId;
        this.status = status;
        this.originAppName = originAppName;
        this.operatorId = operatorId;
        this.originTimestamp = originTimestamp;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String getOriginAppName() {
        return originAppName;
    }

    void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    void setOriginAppName(String originAppName) {
        this.originAppName = originAppName;
    }

    public String getStatus() {
        return status;
    }

    void setStatus(String status) {
        this.status = status;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public boolean isRedelivered() {
        return isRedelivered;
    }

    public Long getOriginTimestamp() {
        return originTimestamp;
    }

    void setOriginTimestamp(String originTimestamp) {
        this.originTimestamp = new Long(originTimestamp);
    }

    public String getOperatorId() {
        return operatorId;
    }

    void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    // Following methods from IMetadata
    public void setTimestamp(String timestamp) {
        this.timestamp = new Long(timestamp);
    }

    public void setRedelivered(boolean ind) {
        this.isRedelivered = ind;
    }
}
