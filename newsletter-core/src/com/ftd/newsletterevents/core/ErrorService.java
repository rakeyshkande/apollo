package com.ftd.newsletterevents.core;


import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.jms.Session;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;



public class ErrorService {
    public static final String ERROR_TYPE_PUBLISH_FAILURE = "publishFailure";
    public static final String ERROR_TYPE_SUBSCRIBE_FAILURE = "subscribeFailure";
    private static final String DATASOURCE = "NEWSLETTER";
    protected static Logger logger = new Logger(ErrorService.class.getName());

    public static void process(String type, Throwable t, String msg) {
        // Compose the error message.
        StringBuffer sb = new StringBuffer();
        sb.append(ErrorService.getStackTrace(t));

            if (msg != null) {
                sb.append(" MORE-INFO=");
                sb.append(msg);
            }

            // Notify operations of the error.        
            ErrorService.notifyOperations("NewsletterEvents", type,
                sb.toString());
    }

    public static void process(String type, Throwable t) {
        ErrorService.process(type, t, null);
    }

    protected static void notifyOperations(String originName, String type,
        String message) {
        SystemMessenger sm = SystemMessenger.getInstance();
        SystemMessengerVO vo = new SystemMessengerVO();

        vo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
        vo.setSource(originName);
        vo.setType(type);
        vo.setMessage(message);

        Connection dbConn = getDbConnection();

        try {
            sm.send(vo, dbConn, false);
        } catch (Exception e) {
            throw new RuntimeException(
                "notifyOperations: Could not notify operations of error: " +
                message, e);
        } finally {
            try {
                dbConn.close();
            } catch (Exception e) {
                logger.error("notifyOperations: JMS clean-up failed.", e);
            }
        }
    }

    /**
     * Obtain a database connection based based datasource named "NEWSLETTER"
     * which should be defined in a datasource.xml
     * @return
     */
    private static Connection getDbConnection() {
        Connection conn = null;

        try {
        	DataSourceUtil dsUtil = DataSourceUtil.getInstance();
            conn = dsUtil.getConnection(DATASOURCE);
  
        } catch (Exception e) {
            throw new RuntimeException(
                "getDbConnection: Could not obtain connection for " +
                DATASOURCE + " datasource.", e);
        }

        return conn;
    }

    public static boolean isOracleRACException(Exception e, Session session) {

        if (e instanceof JMSException) {
        	return true;
        }
        
        return false;
        /*  This code seems to detect a specific condition where a JMS exception occurred
         * because of a SQL exception.
         * 
         * With JBOSS this condition will change.  If there is still a SQL exception we can't
         * be sure that the SQL exception will even be where we expect it in the stack trace.
         * 
         * 
         * && session instanceof AQjmsSession) {
         *
            SQLException sqlExc = (SQLException) ((JMSException) e).getLinkedException();

            Connection dbConn = null;
            try {
                dbConn = ((AQjmsSession) session).getDBConnection();

                return DbUtil.oracleFatalError(sqlExc, dbConn);
            } catch (JMSException je) {
                logger.error(
                    "isOracleRACException: Could not determine type of JMS error.");
            }
            finally
            {
              //close the connection
              try
              {
                dbConn.close();
              }
              catch(Exception ce)
              {}
            }
        }
		*/
    }

    public static String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();

        return sw.toString();
    }
}
