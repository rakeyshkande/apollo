package com.ftd.newsletterevents.core;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;


public class XmlService {
    protected static Logger logger = new Logger(com.ftd.newsletterevents.core.XmlService.class.getName());

    protected static Document marshall(Class beanClass, Object bean)
        throws ParserConfigurationException, Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);

        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element root = doc.createElement("root");
        Element data = doc.createElement(beanClass.getName());

        // Introspect the 'get' attributes.
        Method[] methods = beanClass.getMethods();

        for (int index = 0; index < methods.length; index++) {
            Method method = methods[index];

            if (!method.getName().equalsIgnoreCase("getClass") &&
                    method.getName().startsWith("get") &&
                    method.invoke(bean, new Object[0]) != null) {
                data.setAttribute(method.getName().substring(3),
                    method.invoke(bean, new Object[0]).toString());
            }
        }

        root.appendChild(data);
        doc.appendChild(root);

        return doc;
    }

    public static Object unmarshall(String xmlString) throws Exception {
        Object result = null;

        if ((xmlString != null) && (xmlString.length() > 0)) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringElementContentWhitespace(true);
            factory.setNamespaceAware(false);

            DocumentBuilder builder = factory.newDocumentBuilder();
            StringReader sr = new StringReader(xmlString);

            try {
                InputSource inputSource = new InputSource(sr);
                result = XmlService.unmarshall(builder.parse(inputSource));
            } finally {
                sr.close();
            }
        }

        return result;
    }

    /*
     * IMPORTANT Restriction: The Value Object must have "set" method
     * signatures that accept only Strings!!!!
     */
    public static Object unmarshall(Document doc) throws Exception {
        Object obj = null;

        if (doc.getFirstChild().getChildNodes().getLength() > 0) {
            Node node = getNode(doc);
            String className = node.getNodeName();

            // Instantiate the class using the empty constructor.
            Class cl = Class.forName(className);
            obj = cl.newInstance();

            NamedNodeMap attrMap = node.getAttributes();

            for (int index = 0; index < attrMap.getLength(); index++) {
                Node nvPair = attrMap.item(index);
                cl.getDeclaredMethod("set" + nvPair.getNodeName(),
                    new Class[] { String.class }).invoke(obj,
                    new Object[] { nvPair.getNodeValue() });
            }
        }

        return obj;
    }
    
    protected static Node getNode(Document doc)
    {
        Node rootNode = doc.getFirstChild();
        NodeList nodeList = rootNode.getChildNodes();
        
        for (int i=0; i < nodeList.getLength();i++)
        {
            Node node = nodeList.item(i);
            String nodeName = node.getNodeName();
            if (nodeName.startsWith("com.ftd"))
            {
                return node;
            }
        }
        return null;
    }

    public static void printXml(Document xmlDocument)
        throws IOException {
        StringWriter sw = new StringWriter(); //string representation of xml document
        //xmlDocument.print(new PrintWriter(sw));
        String xml = "";
        try {
        	xml += DOMUtil.convertToString(xmlDocument);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("printXml(): Data DOM = " + xml);
    }

    public static String marshallToXmlString(Class beanClass, Object bean)
        throws ParserConfigurationException, Exception {
        Document xmlDoc = (Document) XmlService.marshall(beanClass, bean);
        
        String xmlString = JAXPUtil.toStringNoFormat(xmlDoc);
        logger.debug("printXml(): Data DOM = \n" + xmlString);
        return xmlString;
    }
}
