package com.ftd.newsletterevents.core;

public interface IConstants {
    public static final String ALIAS_QUEUE_NAME = "JMSAQ/Queues/NL_SUBSCRIBE_CONTROL";
    public static final String ALIAS_QUEUE_CONNECTION_FACTORY = "JMSAQ/FTDXAQCF";
    
    public static final String SELECTOR_KEY_ORIGIN_APP_NAME = "ORIGIN_APP_NAME_KEY";
    
    //Moved this to a datasource.xml
    //public static final String ALIAS_DATA_SOURCE = "java:jdbc/NEWSLETTERDS";
}
