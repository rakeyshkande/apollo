package com.ftd.op.order.mdb;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.service.DeliveryConfirmationService;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import javax.ejb.MessageDrivenBean;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;

import javax.sql.DataSource;

public class DeliveryConfirmationMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private Logger logger;
  private static String logContext = "com.ftd.op.order.mdb.DeliveryConfirmationMDB";
  private String DCON_QUEUE = "DCON-QUEUE";
  private String DCON_ASK = "DCON-ASK";
  private String DCON_LIFECYCLE = "DCON-LIFECYCLE";
  Connection conn = null;
  private DataSource db;
  
  public void ejbCreate()
  {

  }

  public void onMessage(Message msg) {
      
	String msgData = null;
    try {
      
        //get message and extract order detail_id
        TextMessage textMessage = (TextMessage)msg;
        msgData = textMessage.getText();

        logger.info("msgData: " + msgData);

        db = CommonUtils.getDataSource();
        conn = db.getConnection();
      
        if (msgData != null) {
            DeliveryConfirmationService dcs = new DeliveryConfirmationService(conn);
            if (msgData.equalsIgnoreCase(DCON_ASK)) {
                logger.info("Delivery Confirmation ASK");
                dcs.sendDeliveryConfirmationASK();
            } else if (msgData.equalsIgnoreCase(DCON_QUEUE)) {
                logger.info("Delivery Confirmation QUEUE");
                dcs.populateDeliveryConfirmationQueue();
            } else if (msgData.equalsIgnoreCase(DCON_LIFECYCLE)) {
                logger.info("Order Lifecycle Request");
                dcs.getNewLifecycleUpdates();
            }
        }
        logger.info("Finished");
    } catch (Throwable t) { 
        logger.error(t);
        //Send system message.
        try {
            CommonUtils.sendSystemMessage("DeliveryConfirmationMDB", "DeliveryConfirmationMDB failed:" + msgData + "\n" + t.getMessage());
        } catch (Throwable th) {
            logger.error("onMessage: Could not send system message: ", th);
        }
    } finally {
        try { 
            conn.close();
        } catch (Exception e) {
            logger.error(e);
        }
    }
  }

  public void ejbRemove() {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx) {
      this.context = ctx;
      logger = new Logger(logContext);  
  }
}