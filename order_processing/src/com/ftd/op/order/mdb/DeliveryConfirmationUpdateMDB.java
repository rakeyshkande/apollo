package com.ftd.op.order.mdb;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.service.DeliveryConfirmationService;
import com.ftd.osp.utilities.orderlifecycle.OrderStatus;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.StringTokenizer;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.sql.DataSource;


/**
 * Message Handler for Updating Order Status for the Lifecycle Updates.
 * Receives an OrderStatus XML as the Message Payload and processes the status
 * update.
 */
public class DeliveryConfirmationUpdateMDB implements MessageDrivenBean, MessageListener
{
  private static Logger logger = new Logger("com.ftd.op.order.mdb.DeliveryConfirmationUpdateMDB");
  
 /**
 * Context of the Message Driven Bean.
 */
  private MessageDrivenContext context = null;
  

  /**
   * Invoked by the container on ejb creation.
   */
  public void ejbCreate()
  {
  }
  
  /**  
  * Invoked by the container before it ends the life of the MDB.
  */
  public void ejbRemove()
  {
  }

  /**
  * Invoked by the container to set the message-driven context after instance creation.
  */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
  }  

  /**
  * Invoked by the container to process a message.
  * This bean is registered to handle a message for the LifeCycle Update.
  * It expects a TextMessage contining the OrderStatus XML, which can be converted
  * to an OrderStatus object using {@link com.ftd.osp.utilities.orderlifecycle.OrderStatus#fromXML(String)}
  * 
  * @param msg A {@link javax.jms.TextMessage} containing the OrderStatus XML. 
  *  
  */
  public void onMessage(Message msg)
  {
    Connection conn = null;
    DataSource db = null;
    String msgData = null;

    try
    {
      TextMessage textMessage = (TextMessage) msg;
      msgData = textMessage.getText();

      if(logger.isDebugEnabled())
      {
        logger.debug("msgData: " + msgData);
      }
      int statusId = 0;
      boolean confirmOnly = false;
      if (msgData.contains("|CONFIRM_ONLY")) {
    	  StringTokenizer tokenizer = new StringTokenizer(msgData, "|");
    	  // msg format = status id|CONFIRM_ONLY
    	  statusId = Integer.parseInt(tokenizer.nextToken());
    	  logger.info("msgData: " + msgData);
    	  confirmOnly = true;
    	  db = CommonUtils.getDataSource();
    	  conn = db.getConnection();
    	  DeliveryConfirmationService dcs = new DeliveryConfirmationService(conn);      
    	  dcs.confirmStatusId(statusId);
    	  logger.info("Status Id Successfully Confirmed");
		}
		
      if(!confirmOnly){
	      OrderStatus os = OrderStatus.fromXML(msgData);
	
	      db = CommonUtils.getDataSource();
	      conn = db.getConnection();
	      DeliveryConfirmationService dcs = new DeliveryConfirmationService(conn);      
	      dcs.processLifecycleUpdate(os);
      }
      logger.debug("Finished");
    }
    catch (Throwable t)
    {
      logger.error(t);
      //Send system message.
      try {
          CommonUtils.sendSystemMessage("DeliveryConfirmationUpdateMDB", "DeliveryConfirmationUpdateMDB failed:" + msgData + "\n" + t.getMessage());
      } catch (Throwable th) {
          logger.error("onMessage: Could not send system message: ", th);
      }
    }
    finally
    {
      try
      {
        if(conn != null) 
        {
          conn.close();
        }
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
  }


}
