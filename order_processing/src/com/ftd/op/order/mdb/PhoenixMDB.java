package com.ftd.op.order.mdb;

import java.sql.Connection;
import java.sql.SQLException;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.service.PhoenixService;
import com.ftd.osp.utilities.plugins.Logger;

import javax.ejb.MessageDrivenBean;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;
import javax.sql.DataSource;

@SuppressWarnings("serial")
public class PhoenixMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private Logger logger;
  private static String logContext = "com.ftd.op.order.mdb.PhoenixMDB";
  private Connection conn = null;
  private DataSource db = null;
  
  public void ejbCreate() {
  }

  public void onMessage(Message msg){
      
      try {
	      //get message and extract order detail_id
	      TextMessage textMessage = (TextMessage)msg;
	      String msgData = textMessage.getText();
	      String corrId = textMessage.getJMSCorrelationID();
	      db = CommonUtils.getDataSource();
          conn = db.getConnection();
	      logger.info("corrId: " + corrId + " msgData: " + msgData);
	
	      PhoenixService ps = new PhoenixService();
	      OrderService os = new OrderService(conn);
	      if (corrId.equalsIgnoreCase("processDetail")) {
	          ps.processDetail(msgData);
	      } 
	      else if(msgData.equalsIgnoreCase("BulkPhoenix")){
	    	  logger.info("Bulk processing through Scheduler");
	    	  
	    	  String phoenixStatus = os.doBulkPhoenixProcessing();
	    	  if(phoenixStatus.equals("processing")){
	    		  logger.info("Bulk Phoenix processing has Started. Process might take several minutes to Complete.");
	    	  }else if(phoenixStatus.equals("hold")){
	    		  logger.info("Bulk Phoenixing is already running. Aborting the current run. The scheduler will process in the next run.");
	    	  }
	      }
      } catch (Exception e) {
    	  logger.error(e);
      }finally{
    	  if(conn!=null){
    		  try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	  }
      }

  }

  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
    logger = new Logger(logContext);  
    
  }
}