package com.ftd.op.order.mdb;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.service.OrderHoldService;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;

import java.net.InetAddress;

import java.sql.Connection;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

public class CreditCardAuthMDB implements MessageDrivenBean, MessageListener 
{
  private class LockException extends Exception
  {
    public LockException(String exception) 
    {
      super(exception);
    }
    public LockException(Throwable e) 
    {
      super(e);
    }
    public LockException(String exception, Throwable e) 
    {
      super(exception, e);
    }
  }
  
  private MessageDrivenContext context;
  private Logger logger; 
  private Connection conn;



  public void ejbCreate()
  {
  }

  public void onMessage(Message msg)
  {
    try 
    {   
      
      // get connection from the pool
      conn =  CommonUtils.getDataSource().getConnection();
      // get order detail id from message //
      TextMessage textMessage = (TextMessage)msg;
      String hostname = null;
      
      try {
          hostname = InetAddress.getLocalHost().getHostName();
      } catch (Exception e) {
          logger.error("Couldn't get hostname: " + e);  
          hostname = "" + System.currentTimeMillis();  // Just make it unique
      }
      
      String user = "OP-" + hostname + "-" + Thread.currentThread().getName();
      String msgData = textMessage.getText();
      boolean ignoreCustHold = false;
      String orderDetailId = null;

      if(msgData.length() > 7 && (msgData.subSequence(0,7)).equals("RELEASE")){
            ignoreCustHold = true;
            orderDetailId = msgData.substring(8);
      }
      else
      {
          orderDetailId = msgData;
      }
      
      logger.debug("CreditCardAuthMDB onMessage(Message msg): orderDetailId #"+orderDetailId);
      
      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO detail = dao.getOrderDetail(orderDetailId);
      try {
        if(dao.lockPayment(detail, "SYS-REAUTH-"+user).equalsIgnoreCase("Y")) {
          // authorize order //
          
          OrderHoldService orderHoldService = new OrderHoldService(conn);
          orderHoldService.authorizeOrder(orderDetailId, ignoreCustHold);
          dao.unlockPayment(detail, "SYS-REAUTH-"+user);
        } else 
        {
          throw new LockException("Could not obtain lock");
        }  
      } catch (LockException le) 
      {
        InitialContext initialContext = new InitialContext();
        int delay = 30;
        logger.warn("Could not obtain lock, requeueing.  Delay = " + delay + " s", le);
        MessageToken token = new MessageToken();
        token.setMessage(orderDetailId);
        token.setProperty("JMS_OracleDelay", new Integer(delay).toString(), "int");
        CommonUtils.sendJMSMessage(initialContext, token, "CC_AUTH_LOCATION", "CCAUTH");  
      }
    } catch (Throwable ex) 
    { 
      //log error
      logger.error(ex);
      //send out sytem message
      try{
        CommonUtils.sendSystemMessage(ex.toString());
      }
      catch(Exception e)
      {
        logger.error("Could not send system message:" + e.toString());
      }
    } finally 
    {
      try 
      {
        if (conn != null) conn.close();
      } catch (Exception e) {
        // EMPTY
      }
    }
  }

  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
    logger = new Logger("com.ftd.op.order.mdb.CreditCardAuth");
  }
}