package com.ftd.op.order.mdb;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import javax.ejb.MessageDrivenBean;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;
import javax.sql.DataSource;

public class OrderMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private Logger logger;
  private static String logContext = "com.ftd.op.order.mdb.OrderMDB";
  private DataSource db;
  
  public void ejbCreate()
  {

  }

  public void onMessage(Message msg){
      
    Connection conn = null;
    try {
      String orderDetailId;
      conn = db.getConnection();
      
      //get message and extract order detail_id
      TextMessage textMessage = (TextMessage)msg;
      String msgData = textMessage.getText();

      boolean ignoreCustHold = false;

      if(msgData.length() > 7 && (msgData.subSequence(0,7)).equals("RELEASE")){
            ignoreCustHold = true;
            orderDetailId = msgData.substring(8);
      }
      else
      {
          orderDetailId = msgData;
      }

      logger.debug("processing PROCESS_ORDER JMS MESSAGE: " + orderDetailId);
      
      // get the order detail record
      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO orderDetail = dao.getOrderDetail(orderDetailId);
      /* throw exeption if no order detail record retrieved */
      if(orderDetail == null)
      {
        throw new Exception("No order detail record found");
      }
      
      // process the order
      OrderService service = new OrderService(conn);
      service.processOrder(orderDetail, ignoreCustHold, true);
    } catch (Exception e) { 
      logger.error(e);
      TextMessage message = (TextMessage) msg;
      try {
        CommonUtils.sendSystemMessage("Unrecoverable error processing order: " + message.getText() 
                                        + " Caused exception: " + e.getMessage());
      } catch (Exception e2) 
      {
        logger.error(e2);
      }
    } finally 
    {
      try { 
        conn.close();
      } catch (Exception e) 
      {
        logger.error(e);
      }
    }
  }

  public void ejbRemove()
  {
  }

  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.context = ctx;
    logger = new Logger(logContext);  
    
    try
    {
      db = CommonUtils.getDataSource();
    }
    catch (Exception e)
    {
      logger.error(e);
    }

  }
}