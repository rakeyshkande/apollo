package com.ftd.op.order.service;

import com.ftd.op.common.PGCCUtils;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.CreditCardService;
import com.ftd.op.common.service.MilesPointsService;
import com.ftd.op.common.service.QueueService;
import com.ftd.op.common.to.BaseTO;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.common.vo.IPaymentAuthVO;
import com.ftd.op.common.vo.MilesPointsVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderHoldVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.naming.InitialContext;


/**
 * OrderHoldService
 * 
 */
 
public class OrderHoldService 
{
  private Connection conn;  
  private Logger logger = null;
  private final String VALIDATED = "Validated";
  private final String HOLD = "Held";
  private final String DECLINED = "D";
  private final String NO_ORDER_HOLD_RECORDS_WARNING = "WARNING: No order_hold records found";  // Matches message returned from DELETE_ORDER_HOLD
  
 /**
     * Constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
  public OrderHoldService(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.order.service.OrderHoldService");
  }

  /**
   * Insert GNADD hold into Order Hold table. 
   * @throws com.ftd.op.venus.exception.VenusException
   * @throws java.lang.Exception
   * @param orderDetailId String
   */
  public void insertGNADDHold(String orderDetailId) throws Exception, VenusException
  {
    logger.debug("insertGNADDHold (String orderDetailId ("+orderDetailId+")");
    OrderDAO orderDAO = new OrderDAO(conn);
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
    
    if (orderDetailVO == null)
    {
      throw new VenusException("No order detail record for insertGNADDHold(String orderDetailId ("+orderDetailId+")).");
    }
    long recipientId = orderDetailVO.getRecipientId();
    CustomerVO customerVO = orderDAO.getCustomer(recipientId);
    if (customerVO == null)
    {
      throw new VenusException("No customerVO record for insertGNADDHold: "+recipientId);
    }
    String timezone = orderDAO.getTimezone(customerVO.getState());
    if (timezone == null)
    {
      throw new VenusException("No timezone in customerVO record for insertGNADDHold: "+recipientId);
    }
    OrderHoldVO orderHoldVO = new OrderHoldVO();
    orderHoldVO.setHoldReason("GNADD");
    orderHoldVO.setOrderDetailId(orderDetailId);
    orderHoldVO.setTimezone(new Integer(timezone).intValue());
    orderDAO.insertOrderHold(orderHoldVO);
    // change ORDER_DISP_CODE //
    orderDAO.updateOrderDisposition(new Long(orderDetailId).longValue(), HOLD, OrderConstants.CRS_ID_VL);
  }
  
  /**
   * Insert Product hold into Order Hold table. 
   * @throws com.ftd.op.venus.exception.VenusException
   * @throws java.lang.Exception
   * @param orderDetailId
   */
  
  public void insertProductHold(String orderDetailId)throws Exception, VenusException
  {
    logger.debug("insertProductHold(String orderDetailId ("+orderDetailId+")");
    OrderDAO orderDAO = new OrderDAO(conn);
    OrderHoldVO orderHoldVO = new OrderHoldVO();
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
    if (orderDetailVO == null)
    {
      throw new VenusException("No order detail record for insertProductHold(String orderDetailId ("+orderDetailId+")).");
    }
    // get HOLD_WINDOW and set release date to 14 before the orderDate
    GlobalParameterVO globalParameterVO = orderDAO.getGlobalParameter("HOLD_WINDOW");
    if (globalParameterVO == null)
    {
      new VenusException ("Global Parameter HOLD_WINDOW not found in FRP.GLOBAL_PARMS");
    }
    int holdWindow = new Integer(globalParameterVO.getValue()).intValue();
    Date date = orderDetailVO.getDeliveryDate();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -(holdWindow));
    orderHoldVO.setReleaseDate(calendar.getTime());
    orderHoldVO.setHoldReason("PRODUCT");
    orderHoldVO.setOrderDetailId(orderDetailId);
    orderDAO.insertOrderHold(orderHoldVO);
    // change ORDER_DISP_CODE //
    orderDAO.updateOrderDisposition(new Long(orderDetailId).longValue(), HOLD, OrderConstants.CRS_ID_VL);
  }
  /**
   * Insert Credit Card Auth hold. 
   * @throws java.lang.Exception
   * @param orderDetailId
   */
  public void insertAuthHold(String orderDetailId, boolean orderSent)throws Exception
  {
    logger.debug("insertAuthDHold (String orderDetailId ("+orderDetailId+")");
   OrderDAO orderDAO = new OrderDAO(conn);
    OrderHoldVO orderHoldVO = new OrderHoldVO();
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
    orderHoldVO.setHoldReason("RE-AUTH");
    orderHoldVO.setAuthCount(0);
    if(orderSent) 
    {
      orderHoldVO.setStatus("N");
    } else {
      orderHoldVO.setStatus("P");
    }
    orderHoldVO.setOrderDetailId(orderDetailId);
    orderDAO.insertOrderHold(orderHoldVO);
    // change ORDER_DISP_CODE //
    orderDAO.updateOrderDisposition(new Long(orderDetailId).longValue(), HOLD , OrderConstants.CRS_ID_VL);
  }

 /**
   * Release GNADD holds for today.  
   * @throws java.lang.Exception
   */
  public void releaseGNADDHold()throws Exception
  {
    logger.debug("releaseGNADDHold");
    OrderDAO orderDAO = new OrderDAO(conn);
    ArrayList list = (ArrayList) orderDAO.getGnaddHeldOrders();
    for (int i = 0; i < list.size(); i++)
    {
      //Send JMS message
      OrderHoldVO orderHoldVO = (OrderHoldVO) list.get(i);
      InitialContext initialContext = new InitialContext();
      MessageToken token = new MessageToken();
      token.setMessage(orderHoldVO.getOrderDetailId());
      int timezone = orderHoldVO.getTimezone();
      int delay = 0;
      if (timezone != 0)
      {
        delay = (timezone - 1) * 60 * 60; //seconds
      }
      token.setProperty("JMS_OracleDelay", new Integer(delay).toString(), "int");
      CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "GNADD");
      // update Order Disposition //
      orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
      //  delete Order Hold //
      deleteOrderHold(orderDAO, orderHoldVO.getOrderDetailId());
    }
  }
/**
 * Release GNADD holds for dates before today.
 * @throws java.lang.Exception
 */
  public void releaseEarlyGNADDHold()throws Exception
  {
    logger.debug("releaseEarlyGNADDHold");
    OrderDAO orderDAO = new OrderDAO(conn);
    ArrayList list = (ArrayList) orderDAO.getEarlyGnaddHeldOrders();
    for (int i = 0; i < list.size(); i++)
    {
      //Send JMS message
      OrderHoldVO orderHoldVO = (OrderHoldVO) list.get(i);
      logger.debug("Releasing GNADD Order Hold for order detail id #"+orderHoldVO.getOrderDetailId());
      InitialContext initialContext = new InitialContext();
      MessageToken token = new MessageToken();
      token.setMessage(orderHoldVO.getOrderDetailId());
      CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "GNADD");
      // update Order Disposition //
      orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
      //  delete Order Hold //
      deleteOrderHold(orderDAO, orderHoldVO.getOrderDetailId());
    }
  }
  /**
   * Release Product Hold
   * @throws java.lang.Exception
   */
  public void releaseProductHold()throws Exception
  {
    logger.debug("releaseProductHold");
    OrderDAO orderDAO = new OrderDAO(conn);
    ArrayList list = (ArrayList) orderDAO.getProductHeldOrders();
    for (int i = 0; i < list.size(); i++)
    {
      //Send JMS message
      
      OrderHoldVO orderHoldVO = (OrderHoldVO) list.get(i);
      logger.debug("Releasing Product order hold with order detail id :"+orderHoldVO.getOrderDetailId());
      InitialContext initialContext = new InitialContext();
      MessageToken token = new MessageToken();
      token.setMessage(orderHoldVO.getOrderDetailId());
      CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "PRODUCT");
      // update Order Disposition //
      orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
      //  delete Order Hold //
      deleteOrderHold(orderDAO, orderHoldVO.getOrderDetailId());
    }
  }

/**
 * Release GNADD holds where CSZ is Available.
 * @throws java.lang.Exception
 */
  public void releaseGNADDHoldforCSZAvailable()throws Exception
  {
    logger.debug("releaseGNADDHoldforCSZAvailable");
    OrderDAO orderDAO = new OrderDAO(conn);
    ArrayList list = (ArrayList) orderDAO.getGnaddHeldOrders();
    for (int i = 0; i < list.size(); i++)
    {
      OrderHoldVO orderHoldVO = (OrderHoldVO) list.get(i);
      OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderHoldVO.getOrderDetailId());
      CustomerVO customerVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
      if (orderDAO.isCSZAvailable(customerVO.getZipCode()))
      {
        logger.debug("Releasing GNADD Order Hold because of CSZ availability for order detail id #"+orderHoldVO.getOrderDetailId());
        InitialContext initialContext = new InitialContext();
        MessageToken token = new MessageToken();
        token.setMessage(orderHoldVO.getOrderDetailId());
        CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "GNADD");
        // update Order Disposition //
        orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
        //  delete Order Hold //
        deleteOrderHold(orderDAO, orderHoldVO.getOrderDetailId());
      }
    }
  }


  public void authorizeOrder(String orderDetailId) throws Exception 
  {
    authorizeOrder(orderDetailId, false);
  }
  
/**
 * Authorize Order.
 * @param String orderDetailId
 * @throws java.lang.Exception
 */
   public void authorizeOrder(String orderDetailId, boolean ignoreCustHold) throws Exception
   {
      logger.debug("authorizeOrder(String orderDetailId("+orderDetailId+"))");
      String validationStatus = "";

       // set up JMS Message information
      InitialContext initialContext = new InitialContext();

      // Look up order in order payment table.//
      OrderDAO orderDAO = new OrderDAO(conn);
      CommonDAO commonDAO = new CommonDAO(conn);
      OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
      OrderHoldVO orderHoldVO = orderDAO.getOrderHold(orderDetailId, "RE-AUTH");
      
      // Defect 3737. Regardless of whether an order hold record exists, 
      // if the order has been fully refunded, do not bother to authorize.
      // If the order is not Processed or beyond, update it to Processed.
      String orderFullyRefundIndicator = orderDAO.isOrderFullyRefunded(orderDetailId);
      String orderDispCode = orderDetailVO.getOrderDispCode();
      if(orderFullyRefundIndicator.equalsIgnoreCase(OrderConstants.VL_YES))
      {
            if(orderHoldVO != null) {
                orderDAO.deleteOrderHold(orderDetailId);
            }
            if(!orderDispCode.equals(OrderConstants.ORDER_DISP_CODE_PROCESSED) &&
                !orderDispCode.equals(OrderConstants.ORDER_DISP_CODE_PRINTED) &&
                !orderDispCode.equals(OrderConstants.ORDER_DISP_CODE_SHIPPED) ) {
                orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), OrderConstants.ORDER_DISP_CODE_PROCESSED, OrderConstants.CRS_ID_VL);
            }
            return;
      }
      
      if (orderHoldVO == null)
      {
        logger.warn("No order hold record -- requeueing: " + orderDetailVO.getOrderDetailId());

        GlobalParameterVO delayVO = orderDAO.getGlobalParameter("ORDER_PROCESSING", "AUTH_DELAY");
        if (delayVO == null)
        {
          delayVO = new GlobalParameterVO();
          delayVO.setName("AUTH_DELAY");
          delayVO.setContext("ORDER_PROCESSING");
          delayVO.setValue("15");
          // default to 15 mins
        }
        int delay = new Integer(delayVO.getValue()).intValue() * 60;
        MessageToken token = new MessageToken();
        if(ignoreCustHold) 
        {
          token.setMessage("RELEASE|" + orderDetailId);
        } else {
          token.setMessage(orderDetailId);
        }
        token.setProperty("JMS_OracleDelay", new Integer(delay).toString(), "int");
        CommonUtils.sendJMSMessage(initialContext, token, "CC_AUTH_LOCATION", "CCAUTH");  
        // if the order_hold record does not exists, exit method after handling order
        return;
      }
      PaymentVO paymentVO = orderDAO.getFirstPayment(orderDetailId);
      
      // see if payment if already authorized.  if so and does not require reauth, skip processing //
      if (paymentVO.getAuthNumber() != null && !this.isReauthRequired(orderDetailVO))
      {
        validationStatus = CreditCardService.CREDIT_CARD_APPROVED;
        // remove order hold record //
        orderDAO.deleteOrderHold(orderDetailId);
        if(orderHoldVO.getStatus().equalsIgnoreCase("P")) 
        {
          MessageToken token = new MessageToken();
          if(ignoreCustHold) 
          {
            token.setMessage("RELEASE|" + orderDetailId);
          } else {
            token.setMessage(orderDetailId);
          }
          orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
          CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "PROCESSORDER");                    
          return;
        }
      }
      // if payment is not already authorized, proceed with authorization
      if (!validationStatus.equalsIgnoreCase(CreditCardService.CREDIT_CARD_APPROVED))
      {
        // attempt authorization //
        CommonCreditCardVO outCreditCardVO = null;
        MilesPointsVO outMilesPointsVO = null;
        IPaymentAuthVO ip = this.authorizePayment(orderDetailVO, paymentVO, commonDAO);
        String ccType = null;
        if(ip == null) {
            // Unexpected: payment data does not exists.
            validationStatus = DECLINED;
        } else if(ip instanceof MilesPointsVO) {
            outMilesPointsVO = (MilesPointsVO)ip;
            if (outMilesPointsVO.getValidationStatus() != null) {
                validationStatus = outMilesPointsVO.getValidationStatus();
            }
            //update Miles Verified record
            String milesVerifiedFlag = ((OrderConstants.AUTH_APPROVED).equals(validationStatus))? OrderConstants.VL_YES : OrderConstants.VL_NO;
            orderDAO.updateMilesVerifiedFlag(orderDetailId, milesVerifiedFlag);
        } else {
            outCreditCardVO = (CommonCreditCardVO)ip;
//            logger.info("CreditCardVO response in OrderHoldService: " + outCreditCardVO);
            ccType = (commonDAO.getCreditCardVO(new Long(paymentVO.getCcId()).toString())).getCreditCardType();
            if ((outCreditCardVO.getValidationStatus() != null)) {
               validationStatus = outCreditCardVO.getValidationStatus();
            }
        }
        //  if auth failed because of system error      
        if (validationStatus.equalsIgnoreCase(OrderConstants.AUTH_SYSTEM_ERROR))
        {
          GlobalParameterVO authFlagVO = orderDAO.getGlobalParameter("ORDER_PROCESSING", "AUTH_FLAG");
          if (authFlagVO == null)
          {
            throw new Exception ("Global parameter AUTH_FLAG is null.");
          }
          // if the global authorization flag is Y and order hold status is P, process order and updated order status to N
          if (authFlagVO.getValue().equalsIgnoreCase("Y") && orderHoldVO.getStatus().equalsIgnoreCase("P"))
          {
            MessageToken token = new MessageToken();
            if(ignoreCustHold) 
            {
              token.setMessage("RELEASE|" + orderDetailId);
            } else {
              token.setMessage(orderDetailId);
            }
            orderHoldVO.setStatus("N");
            orderDAO.updateOrderHold(orderHoldVO);
            CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "PROCESSORDER");                    
          }
          // increment auth_count and insert into CCAUTH JMS queue
          GlobalParameterVO retriesVO = orderDAO.getGlobalParameter("ORDER_PROCESSING", "MAX_AUTH_ATTEMPTS");
          if (retriesVO == null)
          {
            throw new Exception ("Global parameter MAX_AUTH_ATTEMPTS is null.");
          }
          int retries = new Integer (retriesVO.getValue()).intValue();
          if (orderHoldVO.getAuthCount() < retries)
          {
            orderHoldVO.setAuthCount(orderHoldVO.getAuthCount()+1);
            orderDAO.updateOrderHold(orderHoldVO);
            GlobalParameterVO delayVO = orderDAO.getGlobalParameter("ORDER_PROCESSING", "AUTH_DELAY");
            if (delayVO == null)
            {
              throw new Exception ("Global paremater AUTH_DELAY is null.");
            }
            int delay = new Integer(delayVO.getValue()).intValue() * 60;
            MessageToken token = new MessageToken();
            if(ignoreCustHold) 
            {
              token.setMessage("RELEASE|" + orderDetailId);
            } else {
              token.setMessage(orderDetailId);
            }
            token.setProperty("JMS_OracleDelay", new Integer(delay).toString(), "int");
            CommonUtils.sendJMSMessage(initialContext, token, "CC_AUTH_LOCATION", "CCAUTH");            
          }
          // if exceeds number of retries, send to queue
          else
          {
            QueueService queueService = new QueueService(conn);
            queueService.sendOrderToCreditQueue(orderDetailVO);
            // Add order comment for orders paid by miles.
            if(orderDetailVO.getMilesPointsRedeemed() > 0) {
                  CommonUtils.createOrderComment(conn, OrderConstants.COMMENT_TEXT_MILES_CHECK_ERROR, orderDetailVO);
            }
          }
        }
        // if card approved //
        if (validationStatus.equalsIgnoreCase(OrderConstants.AUTH_APPROVED))
        {
           // update Order Disposition //
           this.updatePaymentRecord(orderDAO, paymentVO, ip, ccType);

          // if order hasn't been processed, process it
           if (orderHoldVO.getStatus().equalsIgnoreCase("P"))
           {
             MessageToken token = new MessageToken();
             if(ignoreCustHold) 
             {
               token.setMessage("RELEASE|" + orderDetailId);
             } else {
               token.setMessage(orderDetailId);
             }
             orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
             CommonUtils.sendJMSMessage(initialContext, token, "PROCESS_ORDER_LOCATION", "PROCESSORDER");                    
           }
           // remove order hold record //
           orderDAO.deleteOrderHold(orderDetailId);
        }
        // if card declined //
        else if (validationStatus.equalsIgnoreCase(OrderConstants.AUTH_DECLINED))
        {
          // update the payment record
          paymentVO.setAuthResult(DECLINED);
          // Added below statements to populate BAMS related data
          setPaymentExtensionParams(paymentVO, outCreditCardVO);
          
          orderDAO.updatePayment(paymentVO);
          
           // update Order Disposition //
          //orderDAO.updateOrderDisposition(new Long(orderHoldVO.getOrderDetailId()).longValue(), VALIDATED, OrderConstants.CRS_ID_VL);
          
          // do not remove order hold records for declines -- need to give
          // other processes something to see if the order was sent or not.
          //orderDAO.deleteOrderHold(orderDetailId); 

          // send to credit queue //
          QueueService queueService = new QueueService(conn);
          queueService.sendOrderToCreditQueue(orderDetailVO);
          
          // Add order comment for orders paid by miles.
          if(orderDetailVO.getMilesPointsRedeemed() > 0) {
              CommonUtils.createOrderComment(conn, OrderConstants.COMMENT_TEXT_MILES_CHECK_FAILURE, orderDetailVO);
          }
        }
      }

   }

	/**
	 * Populated payment extension parameters and set cc auth provider
	 * @param paymentVO
	 * @param outCreditCardVO
	 */
	private void setPaymentExtensionParams(PaymentVO paymentVO,
			CommonCreditCardVO outCreditCardVO) {	
		if(outCreditCardVO!=null){
		  paymentVO.setCcAuthProvider(outCreditCardVO.getCcAuthProvider());
		  paymentVO.getPaymentExtMap().putAll(outCreditCardVO.getPaymentExtMap());
		  paymentVO.setAuthDate(new Date());
		}
	}
   
   // Private wrapper to delete from order_hold.  If order is not on hold, simply generate system message 
   // (allowing upstream logic to continue).
   //
   private void deleteOrderHold(OrderDAO orderDAO, String orderDetailId) throws Exception 
   {
       try {
           orderDAO.deleteOrderHold(orderDetailId);
       } catch (Exception e) {
           String msg = e.getMessage();
           if ((msg != null) && (msg.indexOf(NO_ORDER_HOLD_RECORDS_WARNING) >= 0)) {
                logger.error("Attempted to release order not found in order_hold: " + orderDetailId);
                CommonUtils.sendSystemMessage("Attempted to release order not found in order_hold: " + orderDetailId + 
                                              " - Please confirm multiple FTD's are not on order (since it may have been processed more than once).");
           } else {
               throw new Exception(e);
           }
       }
   }
   
   private void updatePaymentRecord(OrderDAO orderDAO,  PaymentVO paymentVO, IPaymentAuthVO ip, String creditCardType) throws Exception
   {    
        if(ip != null) {
            if(ip instanceof MilesPointsVO) {
                updateMilesPointsPaymentRecord(orderDAO, paymentVO, ip, creditCardType);
            } else {
                updateCreditCardPaymentRecord(orderDAO, paymentVO, ip, creditCardType);
            }
        }
   
   }
   
    private void updateMilesPointsPaymentRecord(OrderDAO orderDAO, PaymentVO paymentVO, IPaymentAuthVO ip, String creditCardType) throws Exception
    {
        logger.debug("updatePaymentRecord(OrderDAO orderDAO, PaymentVO paymentVO, CommonCreditCardVO outCreditCardVO, String creditCardType)"); 
        MilesPointsVO outMilesPointsVO = (MilesPointsVO)ip;
        paymentVO.setAuthResult("AP"); // approved //
        paymentVO.setAuthNumber(outMilesPointsVO.getAuthNumber());
        orderDAO.updatePayment(paymentVO);
    }
    
   private void updateCreditCardPaymentRecord(OrderDAO orderDAO, PaymentVO paymentVO, IPaymentAuthVO ip, String creditCardType) throws Exception
   {
    logger.debug("updatePaymentRecord(OrderDAO orderDAO, PaymentVO paymentVO, CommonCreditCardVO outCreditCardVO, String creditCardType)"); 
    CommonCreditCardVO outCreditCardVO = (CommonCreditCardVO)ip;
    paymentVO.setPaymentType(creditCardType);
    paymentVO.setAuthResult("AP"); // approved //
    paymentVO.setAuthNumber(outCreditCardVO.getApprovalCode());
    paymentVO.setAuthTransactionId(outCreditCardVO.getAuthorizationTransactionId());
    paymentVO.setMerchantRefId(outCreditCardVO.getMerchantRefId());
    paymentVO.setAvsCode(outCreditCardVO.getAVSIndicator());
    paymentVO.setPaymentIndicator("P"); //paid//
    paymentVO.setPaymentRoute(outCreditCardVO.getRoute());
   // paymentVO.setAuthOverrideFlag(authOverrideFlag);
    // parse acqReferenceData //
    String acqReferenceData = outCreditCardVO.getAcquirerReferenceData();
    paymentVO.setAcqReferenceNumber(acqReferenceData);
    if(paymentVO.getPaymentType().equalsIgnoreCase("MS")) 
    {
      paymentVO.setAafesTicketNumber(acqReferenceData);
      paymentVO.setAuthNumber(outCreditCardVO.getActionCode());
      paymentVO.setAuthDate(new Date());
    } else 
    {
      if (acqReferenceData != null && (!acqReferenceData.equals("")))
      {
        if (acqReferenceData.indexOf("a") != -1)
          paymentVO.setAuthCharIndicator(parseReferenceString(acqReferenceData, "a", getNextIndex(acqReferenceData, "bcde")));
        if (acqReferenceData.indexOf("b") != -1)
          paymentVO.setTransactionId(parseReferenceString(acqReferenceData, "b", getNextIndex(acqReferenceData, "cde")));
        if (acqReferenceData.indexOf("c") != -1)
          paymentVO.setValidationCode(parseReferenceString(acqReferenceData, "c", getNextIndex(acqReferenceData, "de")));
        if (acqReferenceData.indexOf("d") != -1)
          paymentVO.setAuthSourceCode(parseReferenceString(acqReferenceData, "d", getNextIndex(acqReferenceData, "e")));
          if (acqReferenceData.indexOf("e") != -1)
            paymentVO.setResponseCode(parseReferenceString(acqReferenceData, "e", getNextIndex(acqReferenceData, "f")));
        /* The 'f' portion of the data, which is a product identifier, is only used for the credit card settlement file and is parsed out when the settlement file is created */
      }
    }
    // Added below statements to populate BAMS related data
    setPaymentExtensionParams(paymentVO, outCreditCardVO);
    // update payment //
    orderDAO.updatePayment(paymentVO);
  }
  
  private String parseReferenceString (String referenceString, String firstLetter, String secondLetter)
  {
    if (secondLetter == null)
      return referenceString.substring(referenceString.indexOf(firstLetter)+1);
    else
      return referenceString.substring(referenceString.indexOf(firstLetter)+1, referenceString.indexOf(secondLetter));
  }
  
  private String getNextIndex(String referenceData, String characters)
  {
    for (int i = 0; i< characters.length(); i++)
    {
      char test = characters.charAt(i);
      if (referenceData.indexOf(test) != -1)
        return new Character(test).toString();
    }
    return null;
  }
  
    /**
     * Checks if the order needs to be reauthorized before processing.
     * Setting this up as a hook for orders with similar workflow.
     * @param orderDtl
     * @return
     */
    public boolean isReauthRequired(OrderDetailVO orderDtl) throws Exception
    {
        // Order is paid with miles/points. Need reauth.
        return orderDtl.getMilesPointsRedeemed() > 0;   
    }  
    
    public IPaymentAuthVO authorizePayment(OrderDetailVO orderDtl, PaymentVO paymentVO, CommonDAO commonDAO) throws Exception 
    {
        if(orderDtl.getMilesPointsRedeemed() > 0) {
            MilesPointsService mpService = new MilesPointsService(conn);
            MilesPointsVO mpVO = commonDAO.getMilesPointsVO(String.valueOf(orderDtl.getOrderDetailId()));
            mpVO = mpService.validatePaymentMethod(mpVO);            
            return mpVO;
        } else {
            CreditCardService creditCardService = new CreditCardService(conn);
            CommonCreditCardVO inCreditCardVO = commonDAO.getCreditCardVO(new Long(paymentVO.getCcId()).toString());
            inCreditCardVO.setAmount(new Double(paymentVO.getCreditAmount()).toString());
            inCreditCardVO.setCreditCardDnsType(paymentVO.getCompanyId());
            inCreditCardVO.setRoute(paymentVO.getPaymentRoute());
            
            logger.info("***authorizePayment()-->paymentVO.getCardinalVerifiedFlag() :"+paymentVO.getCardinalVerifiedFlag());
            if("Y".equalsIgnoreCase(paymentVO.getCardinalVerifiedFlag())){
	            CommonCreditCardVO cardinalVO = commonDAO.getCardinalData(new Long(paymentVO.getPaymentId()).toString());
	            inCreditCardVO.setEci(cardinalVO.getEci());
	            inCreditCardVO.setCavv(cardinalVO.getCavv());
	            inCreditCardVO.setXid(cardinalVO.getXid());
	            inCreditCardVO.setUcaf(cardinalVO.getUcaf());
            }
            else{
            	inCreditCardVO.setEci("");
                inCreditCardVO.setCavv("");
                inCreditCardVO.setXid("");
                inCreditCardVO.setUcaf("");
            }
            // call the store proc to decide the new route by passing payment id
            inCreditCardVO.getPaymentExtMap().putAll(PGCCUtils.getPaymentExtMapByPaymentId(paymentVO.getPaymentId(), conn));
            PGCCUtils.isNewPlatform(inCreditCardVO, orderDtl.getOrderGuid(), conn);
            CommonCreditCardVO outCreditCardVO = creditCardService.validateCreditCard((CommonCreditCardVO)inCreditCardVO);
            return outCreditCardVO;
        }
    
    }
}