package com.ftd.op.order.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.legacy.LegacySourceCode;
import com.ftd.legacy.dao.LegacySourceCodeDAO;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.MilesPointsService;
import com.ftd.op.common.service.QueueService;
import com.ftd.op.common.vo.MilesPointsVO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.facade.MercuryFacade;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.dao.OscarDAO;
import com.ftd.op.order.dao.PhoenixDAO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderHoldVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.OscarOrderInfoVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.op.order.vo.VendorProductVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.facade.VenusFacade;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ExtendedFloristUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityRequest;
import com.ftd.osp.utilities.vo.ExtendedFloristAvailabilityResponse;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.framework.dispatcher.Dispatcher;

import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * OrderService
 * 
 * This is the BO that handles the business logic for processing orders.
 * 
 * 1. Differentiation of Orders: This process determines if an order must be
 * placed on hold (for a variety of reasons including LP issues, credit card
 * authorization, and product availability). If the order is not to be placed on
 * hold, this process determines whether a florist or a drop ship vendor should
 * fulfill the order. This decision controls the rest of the order flow process.
 * This process will be executed whenever the associated Message Driven Bean
 * receives a new order message (indicating that a new order has been entered
 * into the clean schema). This process will fulfill a single order only,
 * relying on the MDB to indicate the order to be processed.
 * 
 * 2. Florist Selection: Once an order is determined to be a floral order, a
 * florist must be selected to fulfill the order. This section addresses the
 * methodology by which a florist is selected. All of the florist selection
 * logic will be in the appropriate stored procedures to minimize data transfer
 * and maximize performance on the database side. The stored procedures will
 * implement a florist scoring mechanism that can be easily modified to add
 * additional selection criteria (at which point the score bit-field will need
 * to increase in size). The database will only return those florists in the
 * highest scoring tier that meet all of the filtering criteria specified in the
 * requirements doc. At that point, a random lottery will be held to determine
 * which florist is selected for the particular order.
 * 
 * @author Nicole Roberts
 */

public class OrderService {	 
	private OrderConstants orderConstants;
	private Connection conn;	
	private DataSource db;
	private String message; 
	private Logger logger = null;
	// United Mileage Plus
	private static String UA_PAYMENT_TYPE = "UA";

	/**
	 * Constructor
	 * 
	 * @param Connection
	 *            - database connection
	 * @return n/a
	 * @throws none
	 */
	public OrderService(Connection conn) {
		this.conn = conn;
		if (logger == null) {
			logger = new Logger(orderConstants.LOGGER_CATEGORY_ORDERSERVICE);
		}
	}

	/**
	 * Constructor
	 * 
	 * @param none
	 * @return n/a
	 * @throws none
	 */
	public OrderService() {
		if (logger == null) {
			logger = new Logger(orderConstants.LOGGER_CATEGORY_ORDERSERVICE);
		}
	}

	/**
	 * This method calls processOrder with all defaults set properly Returns
	 * true if order is successfully processed, false otherwise.
	 */
	public boolean processOrder(OrderDetailVO orderDtl) {
		return processOrder(orderDtl, false, true, null);
	}

	/**
	 * This method calls processOrder with rwdFlagOverride set to null Returns
	 * true if order is successfully processed, false otherwise.
	 */
	public boolean processOrder(OrderDetailVO orderDtl, boolean ignoreCustHold,
			boolean queueFailures) {
		return processOrder(orderDtl, ignoreCustHold, queueFailures, null);
	}

	/**
	 * This method is the driving method for processing orders. If the order is
	 * valid, it determines whether a florist or vendor should fulfill the
	 * order.
	 * 
	 * @param OrderDetailVO
	 *            orderDtl
	 * @param boolean ignoreCustHold
	 * @param Boolean
	 *            rwdFlagOverride
	 * 
	 * @return none
	 */
	 public boolean processOrder(OrderDetailVO orderDtl, boolean ignoreCustHold,
			boolean queueFailures, Boolean rwdFlagOverride) {
		if (logger.isDebugEnabled()) {
			logger.debug("processOrder (OrderDetailVO orderDtl) :: void");
		}
		logger.info("PROCESSING ORDER: " + orderDtl.getOrderDetailId());

		OrderDAO dao = new OrderDAO(this.conn);
		QueueService oQM = new QueueService(conn);
		CommonDAO commonDAO = new CommonDAO(conn);
		String orderDetailId = String.valueOf(orderDtl.getOrderDetailId());
		String hostname = null;
		String user = null;
		boolean milesPointsLocked = false;

		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (Exception e) {
			logger.error("Couldn't get hostname: " + e);
			hostname = "" + System.currentTimeMillis(); // Just make it unique
		}
		user = "OP-" + hostname + "-" + Thread.currentThread().getName();

		try {
			if (orderDtl.getOrderDetailId() == 0) {
				/* invalid order detail encountered - invalid orderDetailID */
				throw new Exception("Invalid order detail encountered");
			} else {
				/* lock the order */
				try {
					logger.info(orderDtl.getOrderDetailId()
							+ ": Locking order.  Session ID = " + user);

					String lockOrder = dao.lockOrder(orderDtl, user);
					if ((lockOrder == null)
							|| (lockOrder
									.equalsIgnoreCase(orderConstants.VL_NO))) {
						logger.info(orderDtl.getOrderDetailId()
								+ ": Could not obtain lock -- requeueing");
						if (queueFailures)
							oQM.sendOrderToRetryQueue(orderDtl, ignoreCustHold);
						return false;
					}

					// Lock entire cart with our own special lock if paid with
					// miles.
					// This is to prevent deducting multiple times for
					// multi-item order.
					//
					if (isOrderPaidWithMilesPoints(orderDtl)) {
						logger.info("Locking Miles/Points Payment order");
						String lockMilesPoints = dao
								.lockPayment(orderDtl, user);
						if ((lockMilesPoints == null)
								|| (lockMilesPoints
										.equalsIgnoreCase(orderConstants.VL_NO))) {
							logger.info(orderDtl.getOrderGuid()
									+ ": Could not obtain Miles/Points Payment lock -- requeueing");
							if (queueFailures)
								oQM.sendOrderToRetryQueue(orderDtl,
										ignoreCustHold);
							return false;
						}
						milesPointsLocked = true;
					}

				} catch (Exception e) {
					logger.warn(orderDtl.getOrderDetailId()
							+ ": Error trying to obtain lock.  Requeueing: "
							+ orderDtl.getOrderDetailId(), e);
					oQM.sendOrderToRetryQueue(orderDtl, ignoreCustHold);
					return false;
				}
				logger.info(orderDtl.getOrderDetailId() + ": Lock obtained");
				/*
				 * determine if valid order Queue Failures flag bypasses all
				 * validations -- only used for resend of previously submitted
				 * orders
				 */
				logger.info(orderDtl.getOrderDetailId() + ": queueFailures = "
						+ queueFailures);

				if (!queueFailures || isOrderValid(orderDtl, ignoreCustHold)) {
					logger.info(orderDtl.getOrderDetailId()
							+ ": Attempting to fulfill order");

					// Deduct miles if order is paid with miles.
					if (isOrderPaidWithMilesPoints(orderDtl)) {

						// Get order details again from DB since miles points
						// payments may have been deducted in interim
						OrderDetailVO orderDetail = dao.getOrderDetail(String
								.valueOf(orderDtl.getOrderDetailId()));
						if (orderDetail == null) {
							logger.error("Trouble re-reading order details.  Continuing anyhow");
							orderDetail = orderDtl; // This should never happen
													// - but just use one we
													// have if it does
						}

						// Settle cart. If fails, send to CREDIT queue.
						if (!this.settleMilesPointsOrder(orderDetail,
								commonDAO, dao)) {
							oQM.sendOrderToCreditQueue(orderDetail);
							CommonUtils
									.createOrderComment(
											conn,
											OrderConstants.COMMENT_TEXT_MILES_SETTLE_FAILURE,
											orderDetail);
							return false;
						}
					}

					/* get fulfillment method */
					String fulfillmentMethod = getFulfillmentMethod(orderDtl);

					/* determine if order fulfilled by vendor or florist */
					if (fulfillmentMethod
							.equalsIgnoreCase(orderConstants.VL_VENDOR)) /*
																		 * process
																		 * vendor
																		 * delivery
																		 */
					{
						logger.info(orderDtl.getOrderDetailId()
								+ ": Order is vendor fulfilled");

						// Confirm that there are vendors available to ship the
						// product
						String productSubcodeId;
						if (StringUtils.isNotBlank(orderDtl.getSubcode())) {
							productSubcodeId = orderDtl.getSubcode();
						} else {
							productSubcodeId = orderDtl.getProductId();
						}
						List vendors = dao.getAvailableVendors(
								productSubcodeId, orderDtl.getOrderDetailId());
						if (vendors.size() == 0) {
							throw new Exception(
									"There are no vendors available for this product.");
						}

						ProductVO productVO = dao.getProduct(orderDtl
								.getProductId());
						if (!StringUtils.equals(productVO.getShippingSystem(),
								VenusConstants.SHIPPING_SYSTEM_SDS)) {
							/*
							 * On "Venus" and "FTP" products, there will only be
							 * one vendor record
							 */
							VendorProductVO vpVO = (VendorProductVO) vendors
									.get(0);
							orderDtl.setVendorId(vpVO.getVendorId());
							logger.info(orderDtl.getOrderDetailId()
									+ ": Vendor ID = " + vpVO.getVendorId());
						}

						logger.info("orderDetailId/external_order_number "
								+ orderDtl.getOrderDetailId() + "/"
								+ orderDtl.getExternalOrderNumber()
								+ " being saved in venus schema");

						if (orderDtl.isMorningDeliveryOrder()) {
							logger.info("isMorningDeliveryOrder() == true");
							if (orderDtl.getShipMethod() != null
									&& !orderDtl.getShipMethod().equals("")) {
								if (orderDtl.getShipMethod().equalsIgnoreCase(
										"GR")
										|| orderDtl.getShipMethod()
												.equalsIgnoreCase("2F")
										|| orderDtl.getShipMethod()
												.equalsIgnoreCase("SA")) {

									SimpleDateFormat dateFormat = new SimpleDateFormat(
											"yyyy-MM-dd");
									Calendar cal = Calendar.getInstance();
									cal.setTime(orderDtl.getDeliveryDate());
									cal.add(Calendar.DATE, -1);
									String newShipDate = dateFormat.format(cal
											.getTime());
									orderDtl.setShipDate(dateFormat
											.parse(newShipDate));

									if (orderDtl.getShipMethod()
											.equalsIgnoreCase("SA")) {
										// check to see how many days difference
										// there is between the
										int results = daysBetween(
												orderDtl.getShipDate(),
												cal.getTime());
										if (results != 0) {
											orderDtl.setShipDate(dateFormat
													.parse(newShipDate));
										}
									} else {
										orderDtl.setShipMethod("ND");
										orderDtl.setShipDate(dateFormat
												.parse(newShipDate));
									}
								}
							}
						}

						insertVenusRecord(orderDtl);
						logger.info(orderDtl.getOrderDetailId()
								+ ": Venus record inserted");

						orderDtl.setOrderDispCode("Processed");
						orderDtl.setOpStatus("Processed");
						dao.updateOrder(orderDtl);
						logger.info(orderDtl.getOrderDetailId()
								+ ": Updated order status to Processed");
					} else if (fulfillmentMethod
							.equalsIgnoreCase(orderConstants.VL_FLORIST)) /*
																		 * process
																		 * florist
																		 * delivery
																		 */
					{
						String mercuryId = null;

						logger.info(orderDtl.getOrderDetailId()
								+ ": Order is florist fulfilled");

						if (hasFlorist(orderDtl)) {
							logger.info(orderDtl.getOrderDetailId()
									+ ": Using pre-existing florist: "
									+ orderDtl.getFloristId());

							/* insert record into mercury table */
							mercuryId = insertMercuryRecord(orderDtl);
							logger.info(orderDtl.getOrderDetailId()
									+ ": Mercury record inserted");

							dao.insertOrderFloristUsed(orderDtl,
									orderDtl.getFloristId(), "Manual");
							logger.info(orderDtl.getOrderDetailId()
									+ ": Order Florist used record inserted");

							orderDtl.setOrderDispCode("Processed");
							orderDtl.setOpStatus("Processed");
							dao.updateOrder(orderDtl);
							logger.info(orderDtl.getOrderDetailId()
									+ ": Updated order status to Processed");
						} else if (useDailyFlorist(orderDtl)) {
							logger.info(orderDtl.getOrderDetailId()
									+ ": Using Daily Florist: "
									+ orderDtl.getFloristId());

							/* insert record into mercury table */
							mercuryId = insertMercuryRecord(orderDtl);
							logger.info(orderDtl.getOrderDetailId()
									+ ": Mercury record inserted");

							orderDtl.setOrderDispCode("Processed");
							orderDtl.setOpStatus("Processed");
							dao.insertOrderFloristUsed(orderDtl,
									orderDtl.getFloristId(), "DailyFlorist");
							logger.info(orderDtl.getOrderDetailId()
									+ ": Order Florist used record inserted");

							dao.updateOrder(orderDtl);
							logger.info(orderDtl.getOrderDetailId()
									+ ": Updated order status to Processed");
						} else {
							try {
								logger.info(orderDtl.getOrderDetailId()
										+ ": Attempting RWD");
								/* select florist */
								RWDFloristVO rwdFlorist = selectFlorist(
										orderDtl, rwdFlagOverride);

								if (rwdFlorist != null) {

									/* update order detail */
									orderDtl.setFloristId(rwdFlorist
											.getFloristId());
									int priority = rwdFlorist.getPriority();
									if (priority == 1) {
										// Priority 1 (primary) florist found
										logger.info("Primary florist found");
										rwdFlorist.setZipCityFlag("Primary");
										logger.info("rwdFlorist.getZipCityFlag: "
												+ rwdFlorist.getZipCityFlag());
									} else if (priority > 1) {
										// Priority 2 (backup) florist.
										logger.info("Backup florist found");
										rwdFlorist.setZipCityFlag("Backup:"
												+ rwdFlorist.getZipCityFlag());
										logger.info("rwdFlorist.getZipCityFlag: "
												+ rwdFlorist.getZipCityFlag());
									}
									/* insert florist used for order */
									dao.insertOrderFloristUsed(orderDtl,
											rwdFlorist.getFloristId(),
											rwdFlorist.getZipCityFlag());

									logger.info(orderDtl.getOrderDetailId()
											+ ": Florist selected: "
											+ orderDtl.getFloristId()
											+ " "
											+ rwdFlorist
													.getFloristSelectionLogId());
									/* insert record into mercury table */
									mercuryId = insertMercuryRecord(orderDtl);
									logger.info(orderDtl.getOrderDetailId()
											+ ": Mercury record inserted");

									/*
									 * add floristSelectionLogId to the mercury
									 * table
									 */
									MercuryDAO mercuryDAO = new MercuryDAO(conn);
									mercuryDAO
											.updateFloristSelectionLogId(
													mercuryId,
													rwdFlorist
															.getFloristSelectionLogId());

									orderDtl.setOrderDispCode("Processed");
									orderDtl.setOpStatus("Processed");
									dao.updateOrder(orderDtl);
									logger.info(orderDtl.getOrderDetailId()
											+ ": Updated order status to Processed");
								} else {
									throw new Exception(
											"No florist found for order -- sending to Zip Queue");	
								}
							} catch (Exception e) {

								logger.info(orderDtl.getOrderDetailId()
										+ ": No valid florists found");

								logger.error("No valid florists found: "
										+ e.toString());

								//Check to see if there any extended florists available to fulfill this order.  If there are, then assign the Non-FTD FTDM florist.
								boolean isNonFTDFloristAssigned = false;
								if (callEFA(retrieveOrderSourceCode(orderDtl))) {
									//Check the call EFA flag. If it is Y, then call the Extended Florist Availability Service to see 
									//if it's available.  If it is, assign the EFA_FTDM_FLORIST to the order.
									ExtendedFloristAvailabilityRequest extendedFloristAvailabilityRequest = new ExtendedFloristAvailabilityRequest();
									extendedFloristAvailabilityRequest.setProductId(orderDtl.getProductId());
									extendedFloristAvailabilityRequest.setZipCode(orderDtl.getRecipientZipCode());
									extendedFloristAvailabilityRequest.setChannel("apollo");
									//retrieve source code from the order
									extendedFloristAvailabilityRequest.setSourceCode(retrieveOrderSourceCode(orderDtl));
									try{
									ExtendedFloristAvailabilityResponse extendedFloristAvailabilityResponse = ExtendedFloristUtil.isAnyFloristAvailable(extendedFloristAvailabilityRequest);
									if (null != extendedFloristAvailabilityResponse) {
										logger.info("extendedFloristAvailabilityResponse.isAvailable(): " + extendedFloristAvailabilityResponse.getIsAvailable());
										if(extendedFloristAvailabilityResponse.getIsAvailable()){
											// Retrieve EFA FTDM Florist to assign to the order
											ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
											orderDtl.setFloristId(configUtil.getFrpGlobalParm("SERVICE",
													"EFA_FTDM_FLORIST"));
											logger.info("EFA FTDM Florist: " + configUtil.getFrpGlobalParm("SERVICE",
													"EFA_FTDM_FLORIST"));
											
											/* insert record into mercury table */
											mercuryId = insertMercuryRecord(orderDtl);
											logger.info(orderDtl.getOrderDetailId()
													+ ": Mercury record inserted");

											dao.insertOrderFloristUsed(orderDtl,
													orderDtl.getFloristId(), "LDR");
											logger.info(orderDtl.getOrderDetailId()
													+ ": Order Florist used record inserted");

											orderDtl.setOrderDispCode("Processed");
											orderDtl.setOpStatus("Processed");
											dao.updateOrder(orderDtl);
											logger.info(orderDtl.getOrderDetailId()
													+ ": Updated order status to Processed");
											
											isNonFTDFloristAssigned = true;
										}
									}
									} catch (Exception exc){
										logger.error(exc);
										//try to call the service one more time
										try{
											ExtendedFloristAvailabilityResponse extendedFloristAvailabilityResponse = ExtendedFloristUtil.isAnyFloristAvailable(extendedFloristAvailabilityRequest);
											if (null != extendedFloristAvailabilityResponse) {
												logger.info("extendedFloristAvailabilityResponse.isAvailable(): " + extendedFloristAvailabilityResponse.getIsAvailable());
												extendedFloristAvailabilityResponse.getIsAvailable();
												if(extendedFloristAvailabilityResponse.getIsAvailable()){
													// Retrieve EFA FTDM Florist to assign to the order
													ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
													orderDtl.setFloristId(configUtil.getFrpGlobalParm("SERVICE",
															"EFA_FTDM_FLORIST"));
													logger.info("EFA FTDM Florist: " + configUtil.getFrpGlobalParm("SERVICE",
															"EFA_FTDM_FLORIST"));
													
													/* insert record into mercury table */
													mercuryId = insertMercuryRecord(orderDtl);
													logger.info(orderDtl.getOrderDetailId()
															+ ": Mercury record inserted");

													dao.insertOrderFloristUsed(orderDtl,
															orderDtl.getFloristId(), "LDR");
													logger.info(orderDtl.getOrderDetailId()
															+ ": Order Florist used record inserted");

													orderDtl.setOrderDispCode("Processed");
													orderDtl.setOpStatus("Processed");
													dao.updateOrder(orderDtl);
													logger.info(orderDtl.getOrderDetailId()
															+ ": Updated order status to Processed");
													
													isNonFTDFloristAssigned = true;
												}
											}
											} catch (Exception ex){
												logger.error(ex);												
											}														
									}
								}
								
								if(!isNonFTDFloristAssigned){
									return doPhoenixProcessing(orderDtl, dao,
											queueFailures, oQM, true, null, null,
											false);
								}
							}
						}
					} else if (fulfillmentMethod
							.equalsIgnoreCase(orderConstants.VL_SERVICE_FREESHIPPING)) /*
																						 * process
																						 * florist
																						 * delivery
																						 */
					{
						return processServicesOrder_FreeShipping(orderDtl, dao);
					}
					// else the order got stuck in a queue -> punt
				} else {
					// else the order got stuck in a queue -> punt
					return false;
				}
			}
		} catch (Exception e) {
			// If this is free shipping, try again
			try {
				if (isProductServiceTypeFreeShipping(orderDtl.getProductId(),
						dao)) {
					return processServicesOrder_FreeShipping(orderDtl, dao);
				}
			} catch (Exception freeShippingException) {
				logger.error(
						"Failed to handle Exception for Free Shipping Order: "
								+ freeShippingException.getMessage(),
						freeShippingException);
			}

			logger.error("Unrecoverable error on order -- sending to Zip Queue"
					+ orderDtl.getOrderDetailId(), e);
			try {
				if (queueFailures) {
					oQM.sendOrderToZipQueue(orderDtl);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order sent to ZIP CS queue");
				}
				return false;
			} catch (Exception e2) {
				try {
					logger.info(orderDtl.getOrderDetailId() + ": Paging out");

					CommonUtils.sendSystemMessage("Order "
							+ orderDtl.getOrderDetailId()
							+ " dropped... could not insert into Zip queue: "
							+ e2.getMessage());
				} catch (Exception sme) {
					logger.error(e);
				}
				return false;
			}
		} finally {
			if (milesPointsLocked) {
				try {
					dao.unlockPayment(orderDtl, user);
				} catch (Exception eLock) {
					logger.warn(orderDtl.getOrderGuid()
							+ ": MilesPoints unlock failed");
				}
			}
			try {
				dao.unlockOrder(orderDtl, user);
				logger.info(orderDtl.getOrderDetailId()
						+ ": unlocked order for session: " + user);
			} catch (Exception e) {
				// do nothing
				logger.info(orderDtl.getOrderDetailId() + ": Unlock Failed");
			} finally {
				logger.info(orderDtl.getOrderDetailId() + ": END PROCESSING");
			}
		}
		return true;
	}

	public boolean doPhoenixProcessing(OrderDetailVO orderDtl, OrderDAO dao,
			boolean queueFailures, QueueService oQM, boolean sendToQueue,
			String Origin, String mercuryNumber, boolean isBulkProcess)
			throws Exception {

		// sendToQueue will be set to true while normal processing of the order.
		// During Bulk phoenix processint set to false.

		// Check to see if Phoenix is enabled
		GlobalParameterVO globalParam = dao.getGlobalParameter("PHOENIX",
				"PHOENIX_ENABLED");
		/* throw exeption if no global parameter record retrieved */
		if (globalParam == null) {
			throw new Exception(
					"PHOENIX_ENABLED global parameter record not found");
		}

		boolean orderIsPhoenixEligible = false;
		String isOrderPhoenixEligible = null;
		String phoenixProductId = null;
		String bearAddonId = null;
		String chocAddonId = null;
		String orderDeliveryDate = null;
		String recipZipCode = null;
		String origNewProdSame = null;
		List<String> addonList = new ArrayList<String>();
		String[] productList = null;
		int delivery_days_out_max = 1;

		if ((globalParam.getValue() != null)
				&& (globalParam.getValue()
						.equalsIgnoreCase(orderConstants.VL_YES))) {
			logger.info(orderDtl.getOrderDetailId() + ": Phoenix is enabled");

			// If Phoenix is enabled then check to see if the order is Phoenix
			// eligible
			PhoenixDAO phoenixDao = new PhoenixDAO(conn);
			CachedResultSet phoenixEligibilityInfo = phoenixDao
					.getOrderPhoenixEligibility(
							String.valueOf(orderDtl.getOrderDetailId()),
							"ORDDET", mercuryNumber);

			if (phoenixEligibilityInfo.next()) {
				if (phoenixEligibilityInfo
						.getString("v_order_phoenix_eligible") != null)
					isOrderPhoenixEligible = phoenixEligibilityInfo.getString(
							"v_order_phoenix_eligible").trim();
				if (phoenixEligibilityInfo.getString("v_bear_addon_id") != null)
					bearAddonId = phoenixEligibilityInfo.getString(
							"v_bear_addon_id").trim();
				if (phoenixEligibilityInfo.getString("v_choc_addon_id") != null)
					chocAddonId = phoenixEligibilityInfo.getString(
							"v_choc_addon_id").trim();
				if (phoenixEligibilityInfo.getString("v_delivery_date") != null)
					orderDeliveryDate = phoenixEligibilityInfo.getString(
							"v_delivery_date").trim();
				if (phoenixEligibilityInfo.getString("v_recip_zip_code") != null)
					recipZipCode = phoenixEligibilityInfo.getString(
							"v_recip_zip_code").trim();
				if (phoenixEligibilityInfo.getString("v_orig_new_prod_same") != null)
					origNewProdSame = phoenixEligibilityInfo.getString(
							"v_orig_new_prod_same").trim();
				if (phoenixEligibilityInfo.getString("v_phoenix_product_id") != null) {
					phoenixProductId = phoenixEligibilityInfo.getString(
							"v_phoenix_product_id").trim();
				}
			}

			if (isOrderPhoenixEligible.equalsIgnoreCase("Y")) {
				orderIsPhoenixEligible = true;
			} else {
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order is not Phoenix eligible");

			}
		}

		if (orderDtl.getZipQueueCount() > 0) {
			if (queueFailures) {
				if (orderIsPhoenixEligible) {
					// If order is Phoenix eligible then send it to the Phoenix
					// process
					// msg format = order detail id|origin|mercury order
					// number|phoenix product id|bear addon id|chocolate addon
					// id|order delivery date|recip zip code|orig new prod same
					// flag|cancel original FTD message|mercury id|ship
					// method|ship
					// date|csrId|ordercomments|emailbody|emailsubject
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order is Phoenix eligible");
					if (Origin == null || Origin.equalsIgnoreCase("null")) {
						Origin = "FTD";
					}
					StringBuffer sb = new StringBuffer();
					sb.append(orderDtl.getOrderDetailId());
					sb.append("|");
					sb.append(Origin);
					sb.append("|");
					if (mercuryNumber != null) {
						sb.append(mercuryNumber);
					} else {
						sb.append("null");
					}
					sb.append("|");
					sb.append(phoenixProductId);
					sb.append("|");
					sb.append(bearAddonId);
					sb.append("|");
					sb.append(chocAddonId);
					sb.append("|");
					sb.append(orderDeliveryDate);
					sb.append("|");
					sb.append(recipZipCode);
					sb.append("|");
					sb.append(origNewProdSame);
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append("null");
					sb.append("|");
					sb.append(sendToQueue);
					sb.append("|");
					sb.append("null"); // emailTitle
					sb.append("|");
					sb.append("null"); // startOrigin
					sb.append("|");
					if (isBulkProcess) {
						sb.append("BULK"); // bulk
					} else {
						sb.append("null"); // bulk
					}

					String msg = sb.toString();
					Dispatcher dispatcher = Dispatcher.getInstance();
					MessageToken token = new MessageToken();
					token.setMessage(msg);
					token.setJMSCorrelationID("ProcessDetail");
					token.setStatus("PHOENIX");
					if (isBulkProcess) {
						int bulkPriority = 9;
						token.setJMSPriority(bulkPriority);
					}
					dispatcher.dispatchTextMessage(new InitialContext(), token);
					return true;
				} else {
					if (sendToQueue) {
						oQM.sendOrderToFtdQueue(orderDtl);
						logger.info(orderDtl.getOrderDetailId()
								+ ": Order sent to " + Origin + " CS queue");
					}
				}
			}
		} else {
			if (orderIsPhoenixEligible) {
				// If order is Phoenix eligible then send it to the Phoenix
				// process
				// msg format = order detail id|origin|mercury order
				// number|phoenix product id|bear addon id|chocolate addon
				// id|order delivery date|recip zip code|orig new prod same
				// flag|cancel original FTD message|mercury id|ship method|ship
				// date|csrId
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order is Phoenix eligible");
				if (Origin == null || Origin.equalsIgnoreCase("null")) {
					Origin = "ZIP";
				}
				StringBuffer sb = new StringBuffer();
				sb.append(orderDtl.getOrderDetailId());
				sb.append("|");
				sb.append(Origin);
				sb.append("|");
				if (mercuryNumber != null) {
					sb.append(mercuryNumber);
				} else {
					sb.append("null");
				}
				sb.append("|");
				sb.append(phoenixProductId);
				sb.append("|");
				sb.append(bearAddonId);
				sb.append("|");
				sb.append(chocAddonId);
				sb.append("|");
				sb.append(orderDeliveryDate);
				sb.append("|");
				sb.append(recipZipCode);
				sb.append("|");
				sb.append(origNewProdSame);
				sb.append("|");
				sb.append("null");
				sb.append("|");
				sb.append("null");
				sb.append("|");
				sb.append("null");
				sb.append("|");
				sb.append("null");
				sb.append("|");
				sb.append("null");
				sb.append("|");
				sb.append(sendToQueue);
				sb.append("|");
				sb.append("null"); // emailTitle
				sb.append("|");
				sb.append("null"); // startOrigin
				sb.append("|");
				if (isBulkProcess) {
					sb.append("BULK"); // bulk
				} else {
					sb.append("null"); // bulk
				}
				String msg = sb.toString();
				Dispatcher dispatcher = Dispatcher.getInstance();
				MessageToken token = new MessageToken();
				token.setMessage(msg);
				token.setJMSCorrelationID("ProcessDetail");
				token.setStatus("PHOENIX");
				if (isBulkProcess) {
					int bulkPriority = 9;
					token.setJMSPriority(bulkPriority);
				}
				dispatcher.dispatchTextMessage(new InitialContext(), token);
				return true;
			} else {
				if (sendToQueue) {
					orderDtl.setZipQueueCount(orderDtl.getZipQueueCount() + 1);
					dao.updateOrder(orderDtl);
					if (queueFailures) {
						oQM.sendOrderToZipQueue(orderDtl);
						logger.info(orderDtl.getOrderDetailId()
								+ ": Order sent to ZIP CS queue");
					}
				}
			}
		}

		return false;

	}

	/**
	 * This method returns true if the order passes all of the validation
	 * checks.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 */
	private boolean isOrderValid(OrderDetailVO orderDtl, boolean ignoreCustHold)
			throws Exception {
		OrderDAO dao = new OrderDAO(conn);
		OrderHoldService ohs = new OrderHoldService(conn);

		if (logger.isDebugEnabled()) {
			logger.debug("isOrderValid (OrderDetailVO orderDtl) :: boolean");
		}

		QueueService oQM = new QueueService(conn);

		try {

			// Checking whether product Sku is GM Sku or not. If yes skip the
			// delivery date check.
			boolean isGMProdcut = false;
			String gmProductID = (ConfigurationUtil.getInstance()
					.getFrpGlobalParm("SERVICES_FREESHIPPING", "DEFAULT"));
			if (gmProductID != null && gmProductID.trim().length() > 0
					&& orderDtl.getProductId().equalsIgnoreCase(gmProductID)) {
				isGMProdcut = true;
				logger.info(new StringBuffer(gmProductID)
						.append(" Product is GM SKU So Delivery date check will be ignored for Order Number : ")
						.append(orderDtl.getExternalOrderNumber()).toString());
			}

			if (!isGMProdcut) {
				// check delivery date to see if it's before today
				// if it is -- put the order in the zip queue and punt
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

				// remove time information from today for comparison purposes
				Date today = format.parse(format.format(new Date()));

				if (orderDtl.getDeliveryDate().before(today)) {
					logger.info(orderDtl.getOrderDetailId()
							+ ": delivery date is before today");
					oQM.sendOrderToZipQueue(orderDtl);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order sent to ZIP CS queue");
					return false;
				}
			}

			if (orderDtl.getOpStatus().equalsIgnoreCase("Processed")) {
				logger.error("Tried to process an order ("
						+ orderDtl.getExternalOrderNumber() + ":"
						+ orderDtl.getOrderDetailId()
						+ ") that was already in 'Processed' State");
				return false;
			}

			if (!ignoreCustHold) {
				/* check if customer/recipient match hold record */
				String out = getCustomerHold(orderDtl);
				if (out.equalsIgnoreCase("FRAUD")
						|| out.equalsIgnoreCase("PREFERRED")) {
					/* put the unprocessed order in the Hold Queue */
					oQM.sendOrderToHoldQueue(orderDtl);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order sent to LP CS queue");

					orderDtl.setOrderDispCode("Held");
					dao.updateOrder(orderDtl);

					// insert order hold record
					OrderHoldVO holdVO = new OrderHoldVO();
					holdVO.setOrderDetailId(orderDtl.getOrderDetailId() + "");
					holdVO.setHoldReason("CUSTOMER");
					if (out.equalsIgnoreCase("PREFERRED"))
						holdVO.setHoldReasonText("Preferred");
					if (out.equalsIgnoreCase("FRAUD"))
						holdVO.setHoldReasonText("Fraud");

					dao.insertOrderHold(holdVO);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Hold Reason = " + holdVO.getHoldReasonText());
					return false;
				}
			}
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());

			// check fraud flag on the order record
			if (order.getFraudFlag() != null
					&& order.getFraudFlag().equalsIgnoreCase("Y")) {
				/* put the unprocessed order in the Hold Queue */
				oQM.sendOrderToHoldQueue(orderDtl);
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order sent to LP CS queue");

				orderDtl.setOrderDispCode("Held");
				dao.updateOrder(orderDtl);

				// insert order hold record
				OrderHoldVO holdVO = new OrderHoldVO();
				holdVO.setOrderDetailId(orderDtl.getOrderDetailId() + "");
				holdVO.setHoldReason("FRAUD_FLAG");
				holdVO.setHoldReasonText("Scrub Hold");
				dao.insertOrderHold(holdVO);
				logger.info(orderDtl.getOrderDetailId()
						+ ": Hold reason = FRAUD_FLAG");
				return false;
			}

			// Defect 3737. If the order is fully refunded, no need to process
			// the order.
			String orderDetailId = String.valueOf(orderDtl.getOrderDetailId());
			String orderFullyRefundIndicator = dao
					.isOrderFullyRefunded(orderDetailId);
			String orderDispCode = orderDtl.getOrderDispCode();
			OrderHoldVO orderHoldVO = dao
					.getOrderHold(orderDetailId, "RE-AUTH");
			if (orderFullyRefundIndicator
					.equalsIgnoreCase(OrderConstants.VL_YES)) {
				if (orderHoldVO != null) {
					dao.deleteOrderHold(orderDetailId);
				}
				if (!orderDispCode
						.equals(OrderConstants.ORDER_DISP_CODE_PROCESSED)
						&& !orderDispCode
								.equals(OrderConstants.ORDER_DISP_CODE_PRINTED)
						&& !orderDispCode
								.equals(OrderConstants.ORDER_DISP_CODE_SHIPPED)) {
					dao.updateOrderDisposition(
							new Long(orderHoldVO.getOrderDetailId())
									.longValue(),
							OrderConstants.ORDER_DISP_CODE_PROCESSED,
							OrderConstants.CRS_ID_VL);
				}
				return false;
			}

			logger.info(orderDtl.getOrderDetailId()
					+ ": Authorization bypass = " + ignoreCustHold);
			/* check if order authorized */
			if (!isOrderAuthorized(orderDtl, ignoreCustHold)) {
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order is not authorized");
				/* put the unprocessed order in the Auth Queue */
				oQM.sendOrderToAuthQueue(orderDtl, ignoreCustHold);
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order sent to RE-AUTH JMS queue");
				orderDtl.setOrderDispCode("Held");

				// insert order hold record
				OrderHoldVO holdVO = new OrderHoldVO();
				try {
					ohs.insertAuthHold(orderDtl.getOrderDetailId() + "", false);
				} catch (Exception de) {
					// No need to insert RE-AUTH order hold record again if it
					// already exists.
					logger.warn("Order hold record already exists for order detail:"
							+ orderDtl.getOrderDetailId());
				}
				dao.updateOrder(orderDtl);

				return false;
			} else {
				// Defect 5623: Order has authorization. If there's a hold
				// record, delete it.
				if (orderHoldVO != null) {
					dao.deleteOrderHold(orderDetailId);
				}
			}

			/* check if product available */
			if (!isProductAvailable(orderDtl)) {
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order is on product availability hold");
				ohs.insertProductHold(orderDtl.getOrderDetailId() + "");
				return false;
			}

			// Skip GNADD Hold for Free Shipping
			if (!isProductServiceTypeFreeShipping(orderDtl.getProductId(), dao)) {
				/* check GNADD */
				if (isGnaddHold(orderDtl)) {
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order is on GNADD Hold");
					/* put the unprocessed order in the GNADD Hold Queue */
					orderDtl.setOrderDispCode("Held");
					dao.updateOrder(orderDtl);

					ohs.insertGNADDHold(orderDtl.getOrderDetailId() + "");

					return false;
				}
			} else {
				logger.debug("Skipping GnaddHold for Free Shipping Product");
			}

			/*
			 * check loss prevention indicator if customer/recipient are not
			 * preferred
			 */
			if (!getCustomerHold(orderDtl).equalsIgnoreCase("PREFERRED")) {
				if (!ignoreCustHold && isLPHold(orderDtl)) {
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order is on LP Hold");

					/* put the unprocessed order in the LP Queue */
					oQM.sendOrderToLPQueue(orderDtl);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order sent to LP CS queue");
					orderDtl.setOrderDispCode("Held");
					dao.updateOrder(orderDtl);

					// insert order hold record
					OrderHoldVO holdVO = new OrderHoldVO();
					holdVO.setOrderDetailId(orderDtl.getOrderDetailId() + "");
					holdVO.setHoldReasonText("LP Indicator");
					holdVO.setHoldReason("LP_INDICATOR");
					dao.insertOrderHold(holdVO);
					return false;
				}
			}
		} catch (Exception e) {
			logger.error(e);
			return false;
		}

		// order passed all of the validation checks
		return true;
	}

	/**
	 * This method returns true if either the customer or the recipient for an
	 * order is on hold, and false otherwise.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 * @throws java.lang.Exception
	 */
	private boolean isCustomerOnHold(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("isCustomerOnHold (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		boolean result = false;
		try {
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());

			/* determine if customer or recipient is on hold */
			result = dao.isCustomerOnHold(orderDtl.getRecipientId(),
					order.getLossPreventionIndicator())
					|| dao.isCustomerOnHold(order.getCustomerId(),
							order.getLossPreventionIndicator());
		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}

	private String getCustomerHold(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("isCustomerOnHold (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		boolean result = false;
		try {
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());

			/* determine if customer or recipient is on hold */
			String recipient = dao.getCustomerHold(orderDtl.getRecipientId(),
					order.getLossPreventionIndicator()).substring(0, 1);
			if (recipient.equalsIgnoreCase("F"))
				return new String("FRAUD");
			String customer = dao.getCustomerHold(order.getCustomerId(),
					order.getLossPreventionIndicator()).substring(0, 1);
			if (customer.equalsIgnoreCase("F"))
				return new String("FRAUD");
			if (recipient.equalsIgnoreCase("P")
					|| customer.equalsIgnoreCase("P"))
				return new String("PREFERRED");
		} catch (Exception e) {
			logger.error(e);
		}
		return new String("NOHOLD");
	}

	/**
	 * This method returns true if the payment record associated with the order
	 * detail record passed in has a non-null authorization field.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 * @throws java.lang.Exception
	 */
	private boolean isOrderAuthorized(OrderDetailVO orderDtl,
			boolean ignoreCustHold) {
		if (logger.isDebugEnabled()) {
			logger.debug("isOrderAuthorized (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		CommonDAO commonDAO = new CommonDAO(this.conn);
		String orderDetailId = String.valueOf(orderDtl.getOrderDetailId());

		try {
			/*
			 * retrieve the payment record associated with the order detail
			 * record passed in
			 */
			PaymentVO payment = dao.getPayment(orderDtl);
			/* throw exeption if no payment record retrieved */
			if (payment == null) {
				throw new Exception("No payment record found");
			}

			/*
			 * Order is automatically authorized if it's an invoice order, a
			 * "no charge" order, or completely covered by a gift certificate.
			 * PayPal and BillMeLater authorizations are caught during Scrub
			 * processing. The business does not want to check for
			 * PayPal/BillMeLater authorizations within order processing and
			 * move the order to a queue. In the event that end of day fails
			 * because of a missing authorization number you can obtain the
			 * authorization number from Scrub/Order XML.
			 */
			if (payment.getPaymentType().equalsIgnoreCase("IN")
					|| payment.getPaymentType().equalsIgnoreCase("NC")
					|| payment.getPaymentType().equalsIgnoreCase("GC")
					|| payment.getPaymentType().equalsIgnoreCase("GD")
					|| payment.getPaymentType().equalsIgnoreCase("PP")
					|| payment.getPaymentType().equalsIgnoreCase("BM")) {
				return true;
			}

			/*
			 * If order is paid by miles or points, the order needs to go
			 * through miles check regardless of auth status of the cart. The
			 * order has gone through miles check successfully if the
			 * order_detail_extensions contain a info_data of 'Y' with info_name
			 * 'Miles Verified'.
			 */
			if (isOrderPaidWithMilesPoints(orderDtl)) {
				if (orderDtl.isOrigBillBilled()) {
					// Miles have been deducted. No need to check again.
					return true;
				} else if (!dao.isMilesVerified(orderDetailId)) {
					// Miles have not been deducted. Need to check miles.
					return false;
				}
			}

			/*
			 * check if order has credit card authorization; the order is not
			 * authorized if the payment record has a null auth result, and a
			 * null auth number, or a 'D' auth result and a null auth number
			 */

			if ((payment.getAuthResult() == null
					|| payment.getAuthResult().equalsIgnoreCase("D") || payment
					.getAuthResult().equalsIgnoreCase("AP"))
					&& payment.getAuthNumber() == null) {
				/*
				 * retrieve the global parameter record associated with the
				 * authorization flag
				 */
				GlobalParameterVO globalParam = dao.getGlobalParameter(
						orderConstants.OP, orderConstants.AUTH_FLAG);
				/* throw exeption if no global parameter record retrieved */
				if (globalParam == null) {
					throw new Exception(
							"No AUTH_FLAG global parameter record found");
				}

				/* check if order requires credit card authorization */
				if ((globalParam.getValue() != null)
						&& (globalParam.getValue()
								.equalsIgnoreCase(orderConstants.VL_YES))) {

					/*
					 * check to see if order for delivery on current day
					 * (today), so does delivery date equal to today's date
					 */
					Date today = new Date();
					DateFormat df = DateFormat
							.getDateInstance(DateFormat.SHORT);
					String sToday = df.format(today);
					String sDelivery = df.format(orderDtl.getDeliveryDate());
					if (!df.parse(sToday).equals(df.parse(sDelivery))) {
						/*
						 * the order is for future delivery (tomorrow or later),
						 * the order should wait for authorization
						 */
						return false;
					} else {
						// put order in credit queue and update payment record
						// before letting order through
						OrderHoldService ohs = new OrderHoldService(conn);
						ohs.insertAuthHold(orderDtl.getOrderDetailId() + "",
								true);
						payment.setAuthOverrideFlag("Y");
						dao.updatePayment(payment);

						QueueService qs = new QueueService(conn);
						qs.sendOrderToAuthQueue(orderDtl, ignoreCustHold);
					}
				} else {
					// no override -- queueing and holding dealt with by caller
					return false;
				}
			} // else the order is authorized
		} catch (Exception e) {
			logger.error(e);
			// This error handling (no handling) is suspecious. In the case
			// where we could not verify auth we probably should do something
			// probalby.
			// Brought up in 3951 United tech review. Pending research.
		}

		/* order is authorized */
		return true;
	}

	/**
	 * This method returns false if the product_master.hold_until_available flag
	 * is set to Y and the delivery date is outside of the globally defined hold
	 * window.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 * @throws java.lang.Exception
	 */
	private boolean isProductAvailable(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("isProductAvailable (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);

		try {
			/*
			 * retrieve the product record associated with the order detail
			 * record passed in
			 */
			ProductVO product = dao.getProduct(orderDtl.getProductId());
			/* throw exeption if no product record retrieved */
			if (product == null) {
				throw new Exception("No product record found");
			}

			/*
			 * check if product is available by checking the hold until
			 * available flag
			 */
			if ((product.getHoldUntilAvailable() != null)
					&& (product.getHoldUntilAvailable()
							.equalsIgnoreCase(orderConstants.VL_YES))) {
				/*
				 * retrieve the global parameter record associated with the hold
				 * window
				 */
				GlobalParameterVO globalParam = dao.getGlobalParameter(
						orderConstants.OP, orderConstants.HOLD_WINDOW);
				/* throw exeption if no global parameter record retrieved */
				if (globalParam == null) {
					throw new Exception(
							"No HOLD_WINDOW global parameter record found");
				}

				/*
				 * check to see if delivery date is more than HOLD_WINDOW days
				 * out if it is, return false, otherwise return true
				 */

				Date deliveryDate = orderDtl.getDeliveryDate();
				Calendar windowDate = new GregorianCalendar();
				windowDate.setTime(deliveryDate);
				int window = 0 - (new Integer(globalParam.getValue()))
						.intValue();
				windowDate.add(Calendar.DATE, window);

				Date today = new Date();
				if (today.before(windowDate.getTime())) {
					return false;
				}
			}
		} catch (ParseException pe) {
			/* invalid hold window encountered - invalid date format */
			logger.error("Invalid date format for ORDER_PROCESSING HOLD_WINDOW in GLOBAL_PARMS table: "
					+ pe);
		} catch (Exception e) {
			logger.error(e);
		}

		/* product is available */
		return true;
	}

	/**
	 * This method returns true if the order record should get sent to the GNADD
	 * hold queue.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 */
	private boolean isGnaddHold(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("isGnaddHold (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);

		try {
			/* retrieve the gnadd record */
			int gnaddLevel = 0;
			String gnaddDateString = null;
			try {
				gnaddLevel = new Integer(dao.getGlobalParameter(
						"FTDAPPS_PARMS", "GNADD_LEVEL").getValue()).intValue();
				gnaddDateString = new String(dao.getGlobalParameter(
						"FTDAPPS_PARMS", "GNADD_DATE").getValue());
			} catch (Exception e1) {
				throw new Exception("GNADD global parameter not found");
			}

			/* check if GNADD is turned on by checking the GNADD level */
			if (gnaddLevel >= 1) {
				/* check if product within the order is not a drop-ship product */
				if (orderDtl.getShipMethod() == null
						|| orderDtl.getShipMethod().equals("SD")) {
					/* check if delivery date is before GNADD date */
					SimpleDateFormat format = new SimpleDateFormat(
							"MMMMM dd, yyyy");
					Date gnaddDate = format.parse(gnaddDateString);
					if (gnaddDate.compareTo(orderDtl.getDeliveryDate()) <= 0) {
						// check if zip code is in GNADD
						if (!dao.isCSZAvailable(dao.getCustomer(
								orderDtl.getRecipientId()).getZipCode())) {
							return true;
						}
					}
				}
			}
		} catch (ParseException pe) {
			/* invalid gnadd date encountered - invalid date format */
			logger.error("Invalid date format for ORDER_PROCESSING GNADD_DATE in GLOBAL_PARMS table: "
					+ pe);
		} catch (Exception e) {
			logger.error(e);
		}

		/* GNADD is not on hold */
		return false;
	}

	/**
	 * This method returns true if the count of orders with the same
	 * lp_indicator as this one in the globally defined timeperiod exceeds the
	 * limits defined for either the source_code or the company on the order.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 */
	private boolean isLPHold(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("isLPHold (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);

		try {
			/*
			 * retrieve the order record associated with the order detail record
			 * passed in
			 */
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());
			/* throw exeption if no order record retrieved */
			if (order == null) {
				throw new Exception("No order record found");
			}

			/* check if LP indicator on the order turned on */
			if (order.getLossPreventionIndicator() != null) {
				/*
				 * retrieve the company record associated with the order record
				 * retrieved
				 */
				CompanyVO company = dao.getCompany(order.getCompanyId());
				/* throw exeption if no company record retrieved */
				if (company == null) {
					throw new Exception("No company record found");
				}

				/* check if there is a company based ordering limit */
				if ((company.getEnableLpProcessing() != null)
						&& (company.getEnableLpProcessing()
								.equalsIgnoreCase(orderConstants.VL_YES))) {
					/*
					 * retrieve the source record associated with the order
					 * record retrieved
					 */
					SourceVO source = dao.getSource(order.getSourceCode());
					/* throw exeption if no source record retrieved */
					if (source == null) {
						throw new Exception("No source record found");
					}

					/* check if there is a source based ordering limit */
					if ((source.getEnableLpProcessing() != null)
							&& (source.getEnableLpProcessing()
									.equalsIgnoreCase(orderConstants.VL_YES))) {
						if (dao.isOrderOnLPHold(orderDtl))
							return true;
					}
				}
			}
			return false;
		} catch (Exception e) {
			logger.error(e);
		}

		/* no LP hold */
		return false;
	}

	/**
	 * This method returns VENDOR if this order should be fulfilled by a drop
	 * ship vendor, and FLORIST, if a florist should fulfill it.
	 * 
	 * @param OrderDetailVO
	 * @return String
	 */
	private String getFulfillmentMethod(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("getFulfillmentMethod (OrderDetailVO orderDtl) :: String");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		QueueService oQM = new QueueService(conn);
		String result = null;

		try {
			/*
			 * retrieve the product record associated with the order detail
			 * record passed in
			 */
			ProductVO product = dao.getProduct(orderDtl.getProductId());
			/* throw exeption if no product record retrieved */
			if (product == null) {
				throw new Exception("No product record found");
			}

			// check for Product Type Services first
			if (StringUtils.equalsIgnoreCase(product.getProductType(),
					OrderConstants.VL_PRODUCT_TYPE_SERVICES)) {
				if (StringUtils.equalsIgnoreCase(product.getProductSubType(),
						OrderConstants.VL_PRODUCT_SUBTYPE_FREESHIPPING)) {
					result = orderConstants.VL_SERVICE_FREESHIPPING;
				} else {
					throw new Exception("Unknown Services Product Subtype: "
							+ product.getProductSubType());
				}
			}
			/*
			 * determine if the order is Florist or Vendor delivered by checking
			 * if the product has a vendor
			 */
			else if (StringUtils.equalsIgnoreCase(product.getShippingSystem(),
					VenusConstants.SHIPPING_SYSTEM_NONE)) {
				/* order is florist delievered */
				result = orderConstants.VL_FLORIST;
			} else {
				/*
				 * check shipping method to make sure order is not for same day
				 * delivery
				 */
				if (orderDtl.getShipMethod().equalsIgnoreCase("SD")) {
					/*
					 * order must be florist delievered because the order is for
					 * same day delivery
					 */
					result = orderConstants.VL_FLORIST;
				} else {
					/* order is vendor delievered */
					result = orderConstants.VL_VENDOR;
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}

	/**
	 * This method returns true if the order already has a florist assigned to
	 * it.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 */
	private boolean hasFlorist(OrderDetailVO orderDtl) {
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("hasFlorist (OrderDetailVO orderDtl) :: boolean");
			}

			/*
			 * check if order has a florist assigned by checking the florist_id
			 * of the order detail record passed in
			 */
			if ((orderDtl.getFloristId() == null)
					|| (orderDtl.getFloristId().equals(""))) {
				/* the order does not have a florist assigned to it */
				return false;
			} // else the order has a florist assigned to it
		} catch (Exception e) {
			logger.error(e);
		}

		/* order already has a florist assigned to it */
		return true;
	}

	/**
	 * This method returns true if the recipient has a daily florist record and
	 * the florist specified is capable of fulfilling the order based on Sunday
	 * delivery and product codification criteria.
	 * 
	 * @param OrderDetailVO
	 * @return boolean
	 */
	private boolean useDailyFlorist(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("useDailyFlorist (OrderDetailVO orderDtl) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);

		try {
			/*
			 * see if any other orders exist for this recipient on this delivery
			 * date
			 */
			LinkedList floristIds = dao.getDailyFlorist(orderDtl);

			if (floristIds != null) {
				if (floristIds.size() > 0) {
					/*
					 * check if the florist specified is capable of fulfilling
					 * the order based on Sunday delivery and product
					 * codification criteria
					 */
					Iterator iter = floristIds.iterator();
					String floristId = null;
					while (iter.hasNext()) {
						floristId = (String) iter.next();
						if (floristId != null
								&& !floristId.equalsIgnoreCase("")) {
							FloristVO florist = dao.getFlorist(floristId);
							if (florist.getStatus().equalsIgnoreCase("ACTIVE")
									&& canFloristDeliverOrder(orderDtl, florist)) {
								/* update order detail */
								orderDtl.setFloristId(florist.getFloristId());
								dao.updateOrder(orderDtl);

								return true;
							} // else do not use the daily florist because
								// florist is not capable of fulfilling the
								// order
						}
					}
				}
			} // else do not use the daily florist because no record found
		} catch (Exception e) {
			logger.error(e);
		}

		/* do not use the daily florist */
		return false;
	}

	/**
	 * This method returns true if the florist specified is capable of
	 * fulfilling the order based on Sunday delivery and product codification
	 * criteria.
	 * 
	 * @param OrderDetailVO
	 * @param FloristVO
	 * @return boolean
	 */
	private boolean canFloristDeliverOrder(OrderDetailVO orderDtl,
			FloristVO florist) {
		if (logger.isDebugEnabled()) {
			logger.debug("canFloristDeliverOrder(OrderDetailVO orderDtl, FloristVO florist) :: boolean");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		boolean result = true;

		try {
			/* determine if florist can deliver order */
			result = dao.canFloristSupportOrder(orderDtl, florist);
		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}

	/**
	 * This method will take a list of florists (from RWD) and selects one among
	 * them for use on the order. It will either perform a random lottery or
	 * call Oscar to make the decision.
	 * 
	 * @param RWDFloristVOList
	 * @return RWDFloristVO
	 */
	private RWDFloristVO performLotteryOrCallOscar(
			RWDFloristVOList rwdFloristList, String sourceCode,
			Date deliveryDate, String orderDetailId, String pcGroupId) {
		String floristsLogStr = "";
		String oscarReason = "OTHERS";
		boolean performPCGroupIdLogic = false;	
		boolean isPCOrder = false;	 
		OscarOrderInfoVO ooivo = null;
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("performLotteryOrCallOscar(RWDFloristVOList rwdFloristList) :: RWDFloristVO");
			}
			OscarDAO odao = new OscarDAO(this.conn);
			LinkedList floristList = rwdFloristList.getFloristList();

			/*
			 * //TODO Comment START
			 * logger.info("**** Total Available Florist *****");
			 * if(floristList!=null) for (int i = 0; i < floristList.size();
			 * i++) { RWDFloristVO rwdFlorist = (RWDFloristVO)
			 * floristList.get(i);
			 * logger.info(rwdFlorist.getFloristId()+":"+rwdFlorist.getScore());
			 * } //TODO Comment END
			 */

			if (floristList == null) {
				/* no routable florist */
				throw new Exception("No valid florists found");
			}

			if (floristList.size() <= 0) {
				/* no routable florist */
				throw new Exception("No valid florists found");
			} else if (floristList.size() == 1) {
				/* only one routable florist - so just use it */
				logger.info("Only one florist found, so Oscar/Lottery not necessary for orderDetailId: "
						+ orderDetailId);
				return (RWDFloristVO) floristList.get(0);

			} else {

				ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
				boolean anyPriorityFlorists = false;

				try {

					// Call OSCAR with PC_SCENARIO_GROUP if premier circle
					// conditions met

					if (StringUtils.isNotEmpty(pcGroupId)
							&& "Y".equalsIgnoreCase(configUtil
									.getFrpGlobalParm("PREMIER_CIRCLE",
											"PREMIER_CIRCLE_FLAG"))) {
						logger.info("Premier Circle Flag is enabled and group id is not empty : "
								+ pcGroupId);
						if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm(
								"PREMIER_CIRCLE", "PC_GROUP_ID_FLAG"))) {
							logger.info("perform PC Group ID Logic is -----TRUE ------");
							performPCGroupIdLogic = true;
						}

						ooivo = oscarCheckCriteriaForPC(sourceCode,
								deliveryDate, orderDetailId, pcGroupId,
								performPCGroupIdLogic);
						if (ooivo != null
								&& OrderConstants.PREMIER_CIRCLE_SCENARIO_GROUP
										.equalsIgnoreCase(ooivo
												.getScenarioGroup())) {
							oscarReason = OrderConstants.PREMIER_CIRCLE_SCENARIO_GROUP;
						}

					}
					if (ooivo == null) {
						logger.info("Non Premier Circle order, check Oscar criteria");
						ooivo = oscarCheckCriteria(sourceCode, deliveryDate,
								orderDetailId);
					}
					// Multiple routable florists so either let Oscar choose or
					// perform random lottery
					//

					// Call Oscar if selection criteria is met
					//

					if (ooivo != null) {

						OscarSelectorService oss = new OscarSelectorService();

						// First create request object for Oscar
						oss.createFillingRequest(ooivo);

						// Now loop and add all RWD florists to request
						for (int i = 0; i < floristList.size(); i++) {
							RWDFloristVO rwdFlorist = (RWDFloristVO) floristList
									.get(i);
							int priority = rwdFlorist.getPriority();
							if (priority == 1) {
								// Priority 1 (primary) florist, so just return
								// with it, no Oscar or lottery needed
								// since there can be only one primary florist.
								logger.info("Primary florist found, so Oscar/Lottery not necessary for orderDetailId: "
										+ orderDetailId);
								rwdFlorist.setZipCityFlag("Primary");
								logger.info("rwdFlorist.getZipCityFlag: "
										+ rwdFlorist.getZipCityFlag());
								return rwdFlorist;
							} else if (priority > 1) {
								// Priority 2 (backup) florist. Save in request
								// object for Oscar
								anyPriorityFlorists = true;
								oss.addFloristToFillingRequest(rwdFlorist
										.getFloristId());
								floristsLogStr += rwdFlorist.getFloristId()
										+ " ";
							} else {
								// Add this florist to request object for Oscar
								// unless there were priority 2 (backup)
								// florists.
								// Note there may be multiple backup florists,
								// so we want Oscar to choose only among those.
								if (anyPriorityFlorists == false) {
									oss.addFloristToFillingRequest(rwdFlorist
											.getFloristId());
									floristsLogStr += rwdFlorist.getFloristId()
											+ " ";
								}
							}
						}

						// Send request to Oscar to choose florist
						//
						logger.info("Oscar will pick florist for orderDetailId: "
								+ orderDetailId);
						String oscarSelectedFlorist = oss
								.selectFillingFlorist();

						// Map Oscar selection back to appropriate florist VO
						//
						if (oscarSelectedFlorist != null) {
							String logId = odao.logOscarFloristSelection(
									orderDetailId, floristsLogStr,
									oscarSelectedFlorist,
									oss.getOscarRequestId(), oscarReason);
							for (int i = 0; i < floristList.size(); i++) {
								RWDFloristVO rwdFlorist = (RWDFloristVO) floristList
										.get(i);
								if (oscarSelectedFlorist.equals(rwdFlorist
										.getFloristId())) {
									// Found it, so set flag and return
									rwdFlorist.setFloristSelectionLogId(logId);
									rwdFlorist.setZipCityFlag("Oscar");
									return rwdFlorist;
								}
							}
							logger.error("Couldn't map Oscar selected florist for orderDetailId: "
									+ orderDetailId
									+ " back to florist VO, so continuing with RWD lottery instead");
						}
					}
				} catch (Throwable oscarThrowable) {
					// Log any Oscar related exceptions and send system message,
					// then allow lottery to take place
					logger.error("Oscar related error occurred for orderDetailId: "
							+ orderDetailId
							+ " so continuing with RWD lottery instead, but here is exception: "
							+ getStackTrace(oscarThrowable));
					try {
						CommonUtils
								.sendSystemMessage("Oscar florist selection error occurred for orderDetailId: "
										+ orderDetailId
										+ " - "
										+ oscarThrowable.getMessage());
					} catch (Throwable th) {
						logger.error(
								"onMessage: Could not send system message: ",
								th);
					}
				}

				// Perform random lottery if no Oscar selection (or Oscar error)
				//

				Random generator = new Random();
				int number = generator.nextInt(rwdFloristList
						.getWeightSquaredTotal()) + 1;
				int runningWeight = 0;
				logger.info("Apollo lottery (not Oscar) will pick florist for orderDetailId: "
						+ orderDetailId);
				floristsLogStr = "";
				RWDFloristVO returnRwdFlorist = null;

				for (int i = 0; i < floristList.size(); i++) {
					RWDFloristVO rwdFlorist = (RWDFloristVO) floristList.get(i);
					floristsLogStr += rwdFlorist.getFloristId() + " ";
					runningWeight += rwdFlorist.getWeightSquared();
					if ((returnRwdFlorist == null) && (number <= runningWeight)) {
						returnRwdFlorist = rwdFlorist;
					}
				}
				if (returnRwdFlorist != null) {
					String logId = odao.logApolloFloristSelection(
							orderDetailId, floristsLogStr,
							returnRwdFlorist.getFloristId(), oscarReason);
					returnRwdFlorist.setFloristSelectionLogId(logId);
					return returnRwdFlorist;
				}

				/* bad fallthrough throw exception */
				throw new Exception("No valid florists found: bad fallthrough");
			}
		} catch (Exception e) {
			logger.error(e);
		}

		/* return empty RWDFloristVO object */
		return new RWDFloristVO();
	}

	/**
	 * Determines if Oscar is enabled and criteria for its use is matched for
	 * the order. If so, the Oscar specific recipient data is retrieved and
	 * returned. Null returned otherwise.
	 * 
	 * @param sourceCode
	 * @param deliveryDate
	 * @param orderDetailId
	 * @return
	 */
	private OscarOrderInfoVO oscarCheckCriteria(String sourceCode,
			Date deliveryDate, String orderDetailId) throws Exception {
		OscarOrderInfoVO ooivo = null;
		boolean checkSourceCode = false;
		boolean checkZip = false;
		boolean checkCityState = false;

		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm("OSCAR",
					"CHECK_SOURCE_CODE_FLAG"))) {
				checkSourceCode = true;
			}
			if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm("OSCAR",
					"CHECK_ZIP_CODE_FLAG"))) {
				checkZip = true;
			}
			if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm("OSCAR",
					"CHECK_CITY_STATE_FLAG"))) {
				checkCityState = true;
			}

			if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm("OSCAR",
					"FILLERS_SELECTED_BY_OSCAR"))
					&& (checkSourceCode || checkZip || checkCityState)) {

				// Call stored proc to return indication if any criteria matched
				// (along with data needed for Oscar call)
				//
				OscarDAO dao = new OscarDAO(this.conn);
				ooivo = dao.getOscarOrderDetails(sourceCode, deliveryDate,
						orderDetailId);

				// Since one or more flags are on, check if those criteria were
				// matched
				//
				StringBuffer logit = new StringBuffer(
						"Checking Oscar criteria for orderDetailId: "
								+ orderDetailId);
				if (ooivo != null) {
					boolean useOscar = false; // Assume we are not going to use
												// Oscar
					if (!useOscar && checkSourceCode) {
						if (ooivo.isSourceCodeCriteriaMatched()) {
							useOscar = true;
							logit.append(" sourceCodeMatch-Y");
						} else {
							logit.append(" sourceCodeMatch-N");
						}
					}
					if (!useOscar && checkZip == true) {
						if (ooivo.isZipCriteriaMatched()) {
							useOscar = true;
							logit.append(" zipMatch-Y");
						} else {
							logit.append(" zipMatch-N");
						}
					}
					if (!useOscar && checkCityState) {
						if (ooivo.isCityStateCriteriaMatched()) {
							useOscar = true;
							logit.append(" cityStateMatch-Y");
						} else {
							logit.append(" cityStateMatch-N");
						}
					}
					if (!useOscar) {
						ooivo = null;
					} else {
						if (StringUtils.isBlank(ooivo.getScenarioGroup())) {
							ooivo.setScenarioGroup(configUtil.getFrpGlobalParm(
									"OSCAR", "DEFAULT_SCENARIO_GROUP"));
							if (logger.isDebugEnabled()) {
								logger.debug("Using Oscar default scenario group");
							}
						}
					}
					logger.info(logit.toString());
				} else {
					logger.error("No data found when checking Oscar criteria for orderDetailId: "
							+ orderDetailId);
				}
			}
		} catch (Exception e) {
			logger.error("Error when checking Oscar criteria for orderDetailId: "
					+ orderDetailId + " - " + e);
			CommonUtils
					.sendSystemMessage("Error when checking Oscar florist selection criteria for orderDetailId: "
							+ orderDetailId + " - " + e.getMessage());
			ooivo = null;
		}
		return ooivo;
	}

	/**
	 * This method determines if the given Premier Circle group id is eligible
	 * for calling OSCAR or not.
	 * 
	 * @param groupId
	 * @return
	 * @throws Exception
	 */
	private OscarOrderInfoVO oscarCheckCriteriaForPC(String sourceCode,
			Date deliveryDate, String orderDetailId, String pcGroupId,
			boolean isGroupIdLogicEnabled) throws Exception {
		boolean isPCOrder = false;
		OscarOrderInfoVO ooivo = null;
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			if (isGroupIdLogicEnabled) {
				logger.info("Premier Circle Group Id logic is enabled");
				if ("Y".equalsIgnoreCase(configUtil.getFrpGlobalParm(
						"PREMIER_CIRCLE", "PC_GROUP_ID_" + pcGroupId))) {
					logger.info("Premier Circle Group : " + pcGroupId
							+ " is enabled.");
					isPCOrder = true;
				}
			} else {
				logger.info("Premier Circle Group Id logic is disabled, so every pc order goes to OSCAR");
				isPCOrder = true;
			}

			if (isPCOrder) {
				OscarDAO dao = new OscarDAO(this.conn);
				ooivo = dao.getOscarOrderDetails(sourceCode, deliveryDate,
						orderDetailId);
				ooivo.setScenarioGroup(OrderConstants.PREMIER_CIRCLE_SCENARIO_GROUP);
			}

		} catch (Exception e) {
			logger.error("Error when checking Oscar criteria for Premier Circle Group ID : "
					+ pcGroupId);
			throw e;
		}
		return ooivo;
	}

	/**
	 * This method will select a florist to fulfill the order. A list of
	 * routable florists in the highest scoring tier that meet all of the
	 * filtering criteria will be returned and sent through a random lottery.
	 * 
	 * @param OrderDetailVO
	 * @return RWDFloristVO
	 */
	public RWDFloristVO selectFlorist(OrderDetailVO orderDtl,
			Boolean rwdFlagOverride) {
		if (logger.isDebugEnabled()) {
			logger.debug("selectFlorist (OrderDetailVO orderDtl, Boolean rwdFlagOverride) :: RWDFloristVO");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		String orderSourceCode = orderDtl.getSourceCode();
		logger.info("Order Detail Source Code: " + orderSourceCode);
		try {
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());
			orderSourceCode = order.getSourceCode();
			logger.info("Order Source Code: " + orderSourceCode);
		} catch (Exception e2) {
			logger.info("Unable to fetch master order detail using guid: " + orderDtl.getOrderGuid(), e2); 
		}
		RWDFloristVO rwdFlorist = new RWDFloristVO();
		String legacySourceIdCode = null;

		try {

			// LegacyID Order Specific Logger for LG
			if (orderDtl.getLegacyID() != null) {
				legacySourceIdCode = getLegacyIDSourceCode(
						orderDtl.getLegacyID(), this.conn);
				if (legacySourceIdCode != null) {
					orderDtl.setLegacyIDSourceCode(legacySourceIdCode);
				}
				logger.info("LegacyID : " + orderDtl.getLegacyID()
						+ "Source code associated to Legacy ID "
						+ legacySourceIdCode);
			}

			try {
				logger.info("Getting legacy source code for order detail id : "+orderDtl.getOrderDetailId());
				String legacySourceCode = dao.getLegacySourceCode(String.valueOf(orderDtl.getOrderDetailId()));
				if (legacySourceCode != null) {
					logger.info("Legacy Source Code matched : "+legacySourceCode);
					orderDtl.setLegacyIDSourceCode(legacySourceCode);
				} 				
			} catch (Exception e1) {
				logger.info("Error obtaining legacy source code based on address, Defaulting to order source code");
				logger.error(e1);
			}

			/* get list of routable florists */
			RWDFloristVOList list = dao.getRoutableFlorists(orderDtl,
					orderConstants.FALSE, rwdFlagOverride);

			/* perform florist lottery */
			if (orderDtl.getLegacyIDSourceCode() != null
					&& !"".equals(orderDtl.getLegacyIDSourceCode())) {
				rwdFlorist = performLotteryOrCallOscar(list,
						orderDtl.getLegacyIDSourceCode(),
						orderDtl.getDeliveryDate(),
						Long.toString(orderDtl.getOrderDetailId()),
						orderDtl.getPcGroupId());
			} else {
				rwdFlorist = performLotteryOrCallOscar(list,
						orderDtl.getSourceCode(), orderDtl.getDeliveryDate(),
						Long.toString(orderDtl.getOrderDetailId()),
						orderDtl.getPcGroupId());
			}

		} catch (Exception e) {
			logger.error(e);
		}

		return rwdFlorist;
	}

	/**
	 * This method will select a florist to fulfill the order. A list of
	 * routable florists in the highest scoring tier that meet all of the
	 * filtering criteria will be returned and sent through a random lottery.
	 * 
	 * @param RWDTO
	 * @return RWDFloristVO
	 */
	public RWDFloristVO selectFlorist(RWDTO rwd) {
		logger.info("selectFlorist (RWDTO rwd) :: RWDFloristVO");
		
		OrderDAO dao = new OrderDAO(this.conn);
		LegacySourceCodeDAO legacyDAO = new LegacySourceCodeDAO(this.conn);
		RWDFloristVO rwdFlorist = new RWDFloristVO();
		String legacyIDSourceCode = null;
		String legacyID = null;
		

		try {

			legacyID = legacyDAO.getOrderLegacyID(rwd.getOrderDetailId());
			if (legacyID != null) {
				legacyIDSourceCode = getLegacyIDSourceCode(legacyID, this.conn);
			}

			if (legacyIDSourceCode != null) {
				rwd.setSourceCode(legacyIDSourceCode);
			}

			logger.info("Getting legacy source code for order detail id : "+rwd.getOrderDetailId());
					
			String legacySourceCode = dao.getLegacySourceCode(rwd.getOrderDetailId());
			if (legacySourceCode != null) {
				logger.info("Legacy Source Code matched : "+legacySourceCode);
				rwd.setSourceCode(legacySourceCode);
			} 			

			/* get list of routable florists */
			RWDFloristVOList list = dao.getRoutableFlorists(rwd);

			String pcGroupId = dao.getPCGroupIdByOrderDetailId(rwd
					.getOrderDetailId());
			/* perform florist lottery */
			rwdFlorist = performLotteryOrCallOscar(list, rwd.getSourceCode(),
					rwd.getDeliveryDate(), rwd.getOrderDetailId(), pcGroupId);
		} catch (Exception e) {
			logger.error(e);
		}

		return rwdFlorist;
	}

	/**
	 * This method will return a list of routable florists if
	 * returnAllFloristFlag is true; otherwise a list of florist in the highest
	 * scoring tier.
	 * 
	 * @param OrderDetailVO
	 * @return RWDFloristVOList
	 */
	public RWDFloristVOList getFloristList(OrderDetailVO orderDtl,
			boolean returnAllFloristFlag, Boolean rwdFlagOverride) {
		if (logger.isDebugEnabled()) {
			logger.debug("getFloristList (OrderDetailVO orderDtl, boolean returnAllFloristFlag) :: RWDFloristVOList");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		RWDFloristVOList list = new RWDFloristVOList();
		try {
			/* get list of routable florists */
			list = dao.getRoutableFlorists(orderDtl, returnAllFloristFlag,
					rwdFlagOverride);
		} catch (Exception e) {
			logger.error(e);
		}

		return list;
	}

	/**
	 * This method inserts a mercury record.
	 * 
	 * @param OrderDetailVO
	 * @return none
	 */
	public String insertMercuryRecord(OrderDetailVO orderDtl) throws Exception {
		MercuryFacade mf = new MercuryFacade(conn);
		return mf.sendFTDMessage(orderDtl);
	}

	/**
	 * This method inserts a venus record.
	 * 
	 * @param OrderDetailVO
	 * @return none
	 */
	public void insertVenusRecord(OrderDetailVO orderDtl) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("insertVenusRecord (OrderDetailVO orderDtl) :: void");
		}
		OrderDetailKeyTO venusTO = new OrderDetailKeyTO();
		venusTO.setOrderDetailId(new Long(orderDtl.getOrderDetailId())
				.toString());
		venusTO.setCsr("SYS");
		VenusFacade facade = new VenusFacade(conn);
		facade.sendOrder(venusTO);
	}

	/**
	 * This method inserts a FTP record.
	 * 
	 * @param OrderDetailVO
	 * @return none
	 */
	public void insertFTPRecord(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("insertFTPRecord (OrderDetailVO orderDtl) :: void");
		}
	}

	/**
	 * This method inserts an email record.
	 * 
	 * @param orderDtl
	 * @return none
	 */
	public void insertEmailRecord(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("insertEmailRecord (OrderDetailVO orderDtl) :: void");
		}
	}

	/**
	 * If cart is out of scrub and has not been settled, then settle it.
	 * 
	 * @param orderDtl
	 * @param commonDAO
	 * @param orderDAO
	 * @throws Exception
	 */
	public boolean settleMilesPointsOrder(OrderDetailVO orderDtl,
			CommonDAO commonDAO, OrderDAO orderDAO) throws Exception {
		String orderDetailId = String.valueOf(orderDtl.getOrderDetailId());

		// Do not settle if there are other items in scrub.
		if (!orderDAO.isCartOutOfScrub(orderDetailId)) {
			return true;
		}
		// Do not settle if cart has been settled
		logger.info("isOrderBillBilled(" + orderDetailId + "):"
				+ orderDtl.isOrigBillBilled());
		if (orderDtl.isOrigBillBilled()) {
			return true;
		}
		// Update order total if cart has removed items.

		MilesPointsVO mpVO = commonDAO.getMilesPointsVO(orderDetailId);
		MilesPointsService mpService = new MilesPointsService(this.conn);
		boolean success = false;

		success = mpService.deductMilesPoints(mpVO);
		logger.info("decuctMilesPoints(" + orderDetailId + "):" + success);
		// Mile successfully deducted. Update cart accounting status.
		if (success) {
			orderDAO.updateCartBilled(orderDetailId, UA_PAYMENT_TYPE);
		}
		return success;
	}

	private boolean isOrderPaidWithMilesPoints(OrderDetailVO orderDtl) {
		return orderDtl.getMilesPointsRedeemed() > 0;
	}

	/**
	 * Returns true is the Product is a Service with Subtype Free Shipping
	 * Service
	 * 
	 * @param product
	 * @return
	 */
	private boolean isProductServiceTypeFreeShipping(String productId,
			OrderDAO orderDAO) {
		ProductVO product = null;
		try {
			product = orderDAO.getProduct(productId);
		} catch (Exception e) {
			logger.error("Failed to retrieve Product with id: " + productId
					+ ":" + e.getMessage(), e);
			product = null;
		}

		if (product == null) {
			return false;
		}

		if (StringUtils.equalsIgnoreCase(product.getProductType(),
				OrderConstants.VL_PRODUCT_TYPE_SERVICES)
				&& StringUtils.equalsIgnoreCase(product.getProductSubType(),
						OrderConstants.VL_PRODUCT_SUBTYPE_FREESHIPPING)) {
			if (logger.isDebugEnabled()) {
				logger.debug("isProductServiceTypeFreeShipping: "
						+ product.getProductType() + ":"
						+ product.getProductSubType() + "-> true");
			}

			return true;
		}

		if (logger.isDebugEnabled()) {
			logger.debug("isProductServiceTypeFreeShipping: "
					+ product.getProductType() + ":"
					+ product.getProductSubType() + "-> false");
		}

		return false;
	}

	/**
	 * Processes a Free Shipping Order Detail.
	 * 
	 * @param orderDetailVO
	 * @return
	 */
	private boolean processServicesOrder_FreeShipping(OrderDetailVO orderDtl,
			OrderDAO orderDao) throws Exception {

		String externalOrderNo = orderDtl.getExternalOrderNumber();
		String orderGuid = orderDtl.getOrderGuid();
		OrderVO orderVO = orderDao.getOrder(orderGuid);
		String emailId = orderVO.getEmailAddress();

		ProductVO productVO = orderDao.getProduct(orderDtl.getProductId());
		int serviceDuration = productVO.getServiceDuration();
		String emailAddress = null;
		if (emailId != null) {
			emailAddress = orderDao.getEmailAddressByEmailId(Long
					.parseLong(emailId));
		}

		String payLoad = "<accountTask type=\"FS_AUTORENEW_UPDATE\"><fsMembershipVO>"
				+ "<emailAddress><![CDATA["
				+ emailAddress
				+ "]]></emailAddress>"
				+ "<externalOrderNumber><![CDATA["
				+ externalOrderNo
				+ "]]></externalOrderNumber>"
				+ "<duration><![CDATA["
				+ serviceDuration
				+ "]]></duration>"
				+ "</fsMembershipVO></accountTask>";
		logger.debug("Payload :" + payLoad);
		FTDCommonUtils.insertJMSMessage(conn, payLoad, "ojms.ACCOUNT", "", 3);
		logger.info(orderDtl.getOrderDetailId()
				+ ": Processing Free Shipping Item");
		orderDtl.setOrderDispCode("Processed");
		orderDtl.setOpStatus("Processed");
		orderDao.updateOrder(orderDtl);
		logger.info(orderDtl.getOrderDetailId()
				+ ": Updated order status to Processed");

		return true;
	}

	public int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	private String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	/**
	 * @param legacyID
	 * @return
	 * @throws Exception
	 */
	public static String getLegacyIDSourceCode(String legacyID, Connection conn)
			throws Exception {
		String legacySourceCode = null;
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		String lpsfCheck = configUtil.getFrpGlobalParm("PI_CONFIG",
				"PRMY_BCKP_FLORIST_LEGACY_ID_LOOK_UP");

		if ("Y".equalsIgnoreCase(lpsfCheck)) {
			if (legacyID != null && !"".equals(legacyID)) {
				LegacySourceCode lsc = new LegacySourceCode(conn);
				legacySourceCode = lsc.getSourceCode(legacyID);
				return legacySourceCode;
			}
		}
		return null;
	}

	public String doBulkPhoenixProcessing() throws Exception {

		OrderDAO dao = new OrderDAO(this.conn);
		QueueService oQM = new QueueService(this.conn);
		StringTokenizer tokenizer;
		try {

			GlobalParameterVO globalParam = dao.getGlobalParameter("PHOENIX",
					"BULK_PHOENIX_RUNNING_STATUS");

			if ((globalParam.getValue() != null)
					&& (globalParam.getValue().equalsIgnoreCase("N"))) {
				// Setting Bulk Phoenix Running Status to Y so that it wont be
				// triggered again.
				dao.setGlobalParameter("PHOENIX",
						"BULK_PHOENIX_RUNNING_STATUS", "Y", "ORDER_PROCESSING");
				List<String> orderDetailData = dao.getQueueOrderIDs();
				logger.info("Bulk Phoenix order details IDs : "
						+ orderDetailData);
				for (String orderDetailInfo : orderDetailData) {

					tokenizer = new StringTokenizer(orderDetailInfo, "|");
					String orderDetailId = tokenizer.nextToken();
					String origin = tokenizer.nextToken();
					String mercuryNumber = tokenizer.nextToken();

					OrderDetailVO orderDetail = dao
							.getOrderDetail(orderDetailId);
					this.doPhoenixProcessing(orderDetail, dao, true, oQM,
							false, origin, mercuryNumber, true);
				}
				// Setting the Bulk Phoenix Running Status to N so that the
				// process can be triggered again.
				dao.setGlobalParameter("PHOENIX",
						"BULK_PHOENIX_RUNNING_STATUS", "N", "ORDER_PROCESSING");
				// Returning 'processing' since the process will be running in
				// the backend
				return "processing";

			}

		} catch (Exception e) {
			logger.error("Error occured while Bulk phoenixing", e);
			// Returning 'hold' as the some exception occured.
			dao.setGlobalParameter("PHOENIX", "BULK_PHOENIX_RUNNING_STATUS",
					"N", "ORDER_PROCESSING");
			return "error";
		}

		return "hold";
	}
	
	private boolean callEFA(String sourceCode) {
		boolean callEFA = false;
		try {
			ConfigurationUtil configUtil = new ConfigurationUtil(); 
			if ((configUtil.getFrpGlobalParm("SERVICE", "CALL_EFA_SERVICE") != null)
                    && (configUtil.getFrpGlobalParm("SERVICE", "CALL_EFA_SERVICE").equalsIgnoreCase("Y"))) {
				//check to see if efa source code check is set to Y.  If it is, retrieve the list of source codes
				//allowed to call EFA Service
				String efaSourceCodeCheck = configUtil.getFrpGlobalParm("SERVICE","EFA_SOURCE_CODE_CHECK");
				if ("Y".equalsIgnoreCase(efaSourceCodeCheck)) {
					String efaSourceCodesAllowed = configUtil.getFrpGlobalParm("SERVICE","EFA_SOURCE_CODES_ALLOWED");
					if (efaSourceCodesAllowed != null && efaSourceCodesAllowed.length() > 0) {
						String[] efaSourceCodes = efaSourceCodesAllowed.split(",");
						logger.info("Checking if source code is allowed to call EFA");
						for (String efaSourceCode : efaSourceCodes) {
							if(sourceCode.equalsIgnoreCase(efaSourceCode)){
								callEFA = true;
							}
						}
					}
				} else {
			              callEFA = true;
					   }    
             } 
		} catch (Exception e) {
			logger.error("Invalid call efa flag");
		}
		logger.info("Call EFA Service: " + callEFA);
		return callEFA;		
	}
	
	/**
	 * This method will retrieve the source code for the order.
	 * 
	 * @param OrderDetailVO
	 * @return String
	 */
	private String retrieveOrderSourceCode(OrderDetailVO orderDtl) {
		if (logger.isDebugEnabled()) {
			logger.debug("retrieveOrderSourceCode(OrderDetailVO orderDtl) :: String");
		}

		OrderDAO dao = new OrderDAO(this.conn);
		String orderSourceCode = orderDtl.getSourceCode();
		logger.info("Order Detail Source Code: " + orderSourceCode);
		try {
			OrderVO order = dao.getOrder(orderDtl.getOrderGuid());
			orderSourceCode = order.getSourceCode();
			logger.info("Order Source Code: " + orderSourceCode);
		} catch (Exception e2) {
			logger.info("Unable to fetch master order detail using guid: " + orderDtl.getOrderGuid(), e2); 
		}
		return orderSourceCode;
	}
}
