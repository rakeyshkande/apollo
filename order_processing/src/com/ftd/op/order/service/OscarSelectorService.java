package com.ftd.op.order.service;

import com.ftd.op.order.vo.OscarOrderInfoVO;
import com.ftd.oscar.athena.selector.AthenaRequest;
import com.ftd.oscar.athena.selector.AthenaSelector;
import com.ftd.oscar.athena.selector.AthenaSelectorService;
import com.ftd.oscar.athena.selector.AthenaRequestedFillingMember;
import com.ftd.oscar.athena.selector.SelectorResponse;
import com.ftd.osp.utilities.ConfigurationUtil;

import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

/**
 * Class to act as a wrapper for the Oscar Selector Service client library,
 * which makes web service calls to the Athena Filling Member Selector.
 * 
 * Essentially the Oscar web service will select a florist among the list 
 * of florists passed, which Apollo will then use as the fulfilling florist.
 *  
 * All references to Oscar WSDL generated logic and objects should be 
 * constrained to this class.
 */
public class OscarSelectorService {
    private static final String OSCAR_CLIENT_CODE = "Apollo";

    private AthenaRequest athenaRequest = null;
    private String oscarRequestId = null;

    /**
     * Method for populating Oscar request object prior to calling Oscar
     * web service for selection of a florist.  
     * 
     * The companion addFloristToFillingRequest method must also be called
     * before calling Oscar.
     * 
     * @param order_detail_id_str
     * @param sending_florist_id
     * @param scenario_group
     * @param delivery_date
     * @param recip_latitude
     * @param recip_longitude
     */
    public void createFillingRequest(OscarOrderInfoVO ooivo) throws Exception
    {
        athenaRequest = new AthenaRequest();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(ooivo.getDeliveryDate());
        XMLGregorianCalendar xgc_delivery_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);

        athenaRequest.setClientCode(OSCAR_CLIENT_CODE);
        Date currDate = new Date();
        athenaRequest.setClientReference(ooivo.getOrderDetailId() + "_" + currDate);
        athenaRequest.setDeliveryDate(xgc_delivery_date);
        if (ooivo.getLatitude() != null) {
            athenaRequest.setRecipientLatitude(new BigDecimal(ooivo.getLatitude()));
        }
        if (ooivo.getLongitude() != null) {
            athenaRequest.setRecipientLongitude(new BigDecimal(ooivo.getLongitude()));
        }
        athenaRequest.setSendingMemberCode(ooivo.getSendingFlorist());
        athenaRequest.setScenarioGroupCode(ooivo.getScenarioGroup());
    }
    
    
    /**
     * Method for adding to list of florists that Oscar gets to choose from.
     * 
     * The companion createFillingRequest method must be called beforehand.
     * 
     * @param florist_id
     */
    public void addFloristToFillingRequest(String florist_id) throws Exception {
        AthenaRequestedFillingMember arfm = null;
        if (athenaRequest != null) {
            arfm = new AthenaRequestedFillingMember();
            arfm.setMemberCode(florist_id);
            athenaRequest.getFillingMember().add(arfm);            
        } else {
            throw new Exception("Coding issue - No Oscar AthenaRequest object was found to add florist to");
        }
    }
    
    
    /**
     * Method to actually setup/call Oscar web service to pick it's favorite 
     * among the list of wonderful florists we included.
     * 
     * Both createFillingRequest and addFloristToFillingRequest must be called beforehand.
     * 
     * @return
     */
    public String selectFillingFlorist() throws Exception {
        String fillingFlorist = null;

        if (athenaRequest != null) {

            // Get service via URL
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
            URL assUrl = new URL(configUtil.getFrpGlobalParm("SERVICE","OSCAR_SELECTOR_SERVICE_URL"));
            AthenaSelectorService ass = new AthenaSelectorService(assUrl);
            Iterator iter = ass.getPorts();
            while(iter.hasNext()) {
              QName q = (QName)iter.next();
              ass.addPort(q, SOAPBinding.SOAP11HTTP_BINDING, assUrl.toString());
            }
            AthenaSelector as = ass.getAthenaSelectorPort();
            String timeout = configUtil.getFrpGlobalParm("SERVICE","OSCAR_SELECTOR_SERVICE_TIMEOUT");
            ((BindingProvider) as).getRequestContext().put("javax.xml.ws.client.connectionTimeout", timeout);
            ((BindingProvider) as).getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeout);
            
            // Make actual web service call and get response
            SelectorResponse sr = as.selectFillingMember(athenaRequest);
            fillingFlorist = sr.getSelectedMember();
            oscarRequestId = sr.getRequestId();
        } else {
            throw new Exception("Coding issue - No Oscar AthenaRequest object was found when attempting to call Oscar");
        }
        
        return fillingFlorist;
    }

    
    public String getOscarRequestId() {
        return oscarRequestId;
    }
}
