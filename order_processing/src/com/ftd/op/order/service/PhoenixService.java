package com.ftd.op.order.service;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.dao.MessageGeneratorDAO;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.MessagingService;
import com.ftd.op.common.service.QueueService;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.constants.MercuryConstants;
import com.ftd.op.mercury.dao.FloristMaintenanceDAO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.util.FloristMaintenanceLockHelper;
import com.ftd.op.mercury.vo.AutoResponseKeyVO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.dao.PhoenixDAO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.PhoenixVO;
import com.ftd.op.venus.service.VenusService;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.core.domain.ProductAvailVO;

public class PhoenixService {

	private Logger logger = null;
	private DataSource db;
	private static String OUTGOING_MESSAGE = "O";

	Connection conn = null;

	/**
	 * Constructor
	 * 
	 * @param
	 * @return n/a
	 * @throws
	 */
	public PhoenixService() {
		if (logger == null) {
			logger = new Logger("com.ftd.op.order.service.PhoenixService");
		}

	}

	public void processDetail(String msgData) throws ParseException, Exception {
		logger.info("processDetail(" + msgData + ")");
		boolean alreadySentToQueue = false;

		StringTokenizer tokenizer = new StringTokenizer(msgData, "|");
		// msg format = order detail id|origin|mercury order number|phoenix
		// product id|bear addon id|chocolate addon id|order delivery date|recip
		// zip code|orig new prod same flag|cancel original FTD message|mercury
		// id|ship method|ship
		// date|csrId|orderComments|emailBody|emailSubject|emailType|sendToQueue|emailTitle|startOrigin|bulk
		long orderDetailId = Long.parseLong(tokenizer.nextToken());
		String origin = tokenizer.nextToken();
		String mercuryOrderNumber = tokenizer.nextToken();
		String phoenixProductId = tokenizer.nextToken();
		String bearAddonId = tokenizer.nextToken();
		String chocolateAddonId = tokenizer.nextToken();
		String orderDeliveryDate = tokenizer.nextToken();
		String recipZipCode = tokenizer.nextToken();
		String origNewProdSame = tokenizer.nextToken();
		String cancelOrigFTDMsg = tokenizer.nextToken();
		String mercuryId = tokenizer.nextToken();
		String comShipMethod = tokenizer.nextToken();
		String comShipDate = tokenizer.nextToken();
		String csrId = tokenizer.nextToken();
		String orderComments = null;
		String emailBody = null;
		String emailSubject = null;
		String emailType = "";
		String customerPhone = null;
		String sendToQueue = "";
		String emailTitle = null;
		String startOrigin = null;
		String bulk = null;

		if (origin.equalsIgnoreCase("COM")) {
			orderComments = tokenizer.nextToken();
			emailBody = tokenizer.nextToken();
			emailSubject = tokenizer.nextToken();
			emailType = tokenizer.nextToken();
		} else
			emailType = "phoenix_email";

		sendToQueue = tokenizer.nextToken();
		emailTitle = tokenizer.nextToken();
		startOrigin = tokenizer.nextToken();
		bulk = tokenizer.nextToken();

		logger.info("orderDetailId: " + orderDetailId);
		logger.info("origin: " + origin);
		logger.info("mercuryOrderNumber: " + mercuryOrderNumber);
		logger.info("phoenixProductId: " + phoenixProductId);
		logger.info("bearAddonId: " + bearAddonId);
		logger.info("chocolateAddonId: " + chocolateAddonId);
		logger.info("orderDeliveryDate: " + orderDeliveryDate);
		logger.info("recipZipCode: " + recipZipCode);
		logger.info("origNewProdSame: " + origNewProdSame);
		logger.info("cancelOrigFTDMsg: " + cancelOrigFTDMsg);
		logger.info("mercuryId: " + mercuryId);
		logger.info("comShipMethod: " + comShipMethod);
		logger.info("comShipDate: " + comShipDate);
		logger.info("csrId: " + csrId);
		logger.info("orderComments: " + orderComments);
		logger.info("emailBody: " + emailBody);
		logger.info("emailSubject: " + emailSubject);
		logger.info("emailType: " + emailType);
		logger.info("sendToQueue: " + sendToQueue);
		logger.info("emailTitle: " + emailTitle);
		logger.info("startOrigin : " + startOrigin);
		logger.info("bulk : " + bulk);

		Date deliveryDate = null;
		String shipMethod = null;
		Date shipDate = null;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		if (orderDeliveryDate != null) {
			// Convert the string to date
			// Define the format
			deliveryDate = (Date) formatter.parse(orderDeliveryDate);
		}

		try {
			db = CommonUtils.getDataSource();
			conn = db.getConnection();

			// Get order details
			PhoenixDAO dao = new PhoenixDAO(conn);
			PhoenixVO pVO = dao.getPhoenixDetails(
					String.valueOf(orderDetailId), origin, mercuryOrderNumber);
			CustomerVO cusVO = new CustomerVO();
			OrderVO orderVO = new OrderVO();
			OrderDAO orderDao = new OrderDAO(conn);
			CachedResultSet rs = orderDao.getOrderCustomerInfo(String.valueOf(orderDetailId));

			if (rs.next()) {
				cusVO.setCustomerId(Long.parseLong(rs.getString("customer_id")));
				orderVO.setOrderGuid(rs.getString("order_guid"));
				// extOrderNum = rs.getString("external_order_number");
				orderVO.setMasterOrderNumber(rs.getString("master_order_number"));
				cusVO.setFirstName(rs.getString("first_name"));
				cusVO.setLastName(rs.getString("last_name"));
				// customerEmailAddress = rs.getString("email_address");
				// companyId = rs.getString("company_id");
				// originId = rs.getString("origin_id");
				customerPhone = rs.getString("customer_phone_number");
			}

			if (pVO != null && pVO.getOrderDetailId() > 0) {
				DateFormat sdf = new SimpleDateFormat("yyyyMMdd");

				String productId = pVO.getProductId();
				String recipient = pVO.getRecipientName();
				String mercuryAddress = pVO.getMercuryAddress();
				String address1 = pVO.getRecipientAddress1();
				String address2 = pVO.getRecipientAddress2();
				String businessName = pVO.getBusinessName();
				String city = pVO.getRecipientCity();
				String state = pVO.getRecipientState();
				String zipCode = pVO.getRecipientZipCode();
				String country = pVO.getRecipientCountry();
				String phoneNumber = pVO.getRecipientPhoneNumber();
				String cardMessage = pVO.getCardMessage();
				Date orderDate = pVO.getOrderDate();
				String sourceCode = pVO.getSourceCode();

				// Phoenix 178340 changes
				Date orginalDeliveryDate = formatter.parse(formatter.format(pVO.getDeliveryDate()));

				boolean hasDeliveryDateChanged = hasDeliveryDateChanged(orginalDeliveryDate, deliveryDate);

				boolean parseSuccess = true;
				// if (recordSource != null &&
				// recordSource.equalsIgnoreCase("mercury")) {
				if (mercuryAddress != null) {
					int addressLength = mercuryAddress.length();
					if (mercuryAddress.substring(addressLength - 1).equals(",")) {
						mercuryAddress = mercuryAddress.substring(0, addressLength - 1);
					}

					String tempAddress = null;
					if (businessName != null) {
						businessName = businessName.trim();
						if (businessName.equals("")) {
							businessName = null;
						} else {
							tempAddress = businessName;
						}
					}
					if (tempAddress == null) {
						tempAddress = address1;
					} else {
						address1 = address1.trim();
						tempAddress = tempAddress + " " + address1;
					}
					if (address2 != null) {
						address2 = address2.trim();
						tempAddress = tempAddress + " " + address2;
					}

					String[] addressArray = mercuryAddress.split(",");
					if (addressArray.length == 1) {
						if (!tempAddress.equalsIgnoreCase(mercuryAddress)) {
							logger.error("mercuryAddress does not match");
							parseSuccess = false;
						}
					} else {
						for (int i = 0; i < addressArray.length; i++) {
							// Address 1 is always in the first row
							if (i == 0) {
								if (!addressArray[i].equalsIgnoreCase(address1)) {
									logger.error("address1 does not match");
									parseSuccess = false;
									break;
								}
							} else {
								// The second row can either be Address2 or Business Name
								if (i == 1) {
									if (address2 != null
											&& !address2.equals("")) {
										if (!addressArray[i].equalsIgnoreCase(address2)) {
											logger.error("address2 does not match");
											parseSuccess = false;
											break;
										}
									} else {
										if (businessName != null
												&& !businessName.equals("")) {
											if (!addressArray[i].equalsIgnoreCase(businessName)) {
												logger.error("business name does not match (1)");
												parseSuccess = false;
												break;
											}
										} else {
											logger.error("No address2/business name");
											parseSuccess = false;
											break;
										}
									}
								} else {
									// The third row (if it exists) is always
									// the Business Name
									if (businessName != null
											&& !businessName.equals("")) {
										if (!addressArray[i].equalsIgnoreCase(businessName)) {
											logger.error("business name does not match (2)");
											parseSuccess = false;
											break;
										}
									} else {
										logger.error("no business name");
										parseSuccess = false;
										break;
									}
								}
							}
						}

					}
					if (!parseSuccess) {
						logger.error("Could not parse Mercury Address: " + mercuryAddress);
						logger.error("businessName: " + businessName);
						logger.error("address1: " + address1);
						logger.error("address2: " + address2);
						parseSuccess = false;
					} else {
						int cszLength = city.length();
						int zipLength = zipCode.length();
						if (city.substring(cszLength - zipLength).equalsIgnoreCase(zipCode)) {
							city = city.substring(0, cszLength - zipLength - 1);
							cszLength = city.length();
							if (city.substring(cszLength - 4).matches(", [A-Z][A-Z]")) {
								state = city.substring(cszLength - 2, cszLength);
								city = city.substring(0, cszLength - 4);
							} else {
								parseSuccess = false;
								logger.error("Could not parse State: " + mercuryId + " " + city + " / State");
							}
						} else {
							parseSuccess = false;
							logger.error("Could not parse Zip Code: " + mercuryId + " " + city + " / " + zipCode);
						}
					}

					if (phoneNumber == null) {
						phoneNumber = "";
					}
					
					phoneNumber = phoneNumber.replaceAll("/", "");
					phoneNumber = phoneNumber.replaceAll("-", "");
					if (phoneNumber.length() > 10) {
						phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
					} else if (phoneNumber.length() < 10) {
						phoneNumber = "0000000000" + phoneNumber;
						phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
					}

				}

				if (parseSuccess) {
					if (zipCode.length() > 5) {
						if (country.equalsIgnoreCase("US")) {
							zipCode = zipCode.substring(0, 5);
						} else {
							zipCode = zipCode.substring(0, 3);
						}
					}

					logger.info("Order: " + orderDetailId + " productId: "
							+ productId + " to " + phoenixProductId + " zip: " + zipCode + " deliveryDate: "
							+ sdf.format(deliveryDate));
					// Check PAS for availability for origins other than COM
					// COM sends in the ship method and ship date
					//boolean isShipMethodSet = false;
					boolean skipPas = false;
					logger.info("orign: " + origin);
					logger.info("skipPas: " + skipPas);
					if (origin.equalsIgnoreCase("COM")) {
						shipDate = (Date) formatter.parse(comShipDate);
						shipMethod = comShipMethod;
						//isShipMethodSet = true;
						skipPas = true;
						// check if cancel message should be sent
						if (cancelOrigFTDMsg != null && !cancelOrigFTDMsg.equalsIgnoreCase("null")) {
							// get the associated FTD
							MercuryDAO mercuryDao = new MercuryDAO(conn);
							MercuryVO ftdMessage = mercuryDao.getMessageDetailLastFTD(Long.toString(orderDetailId));
							CANMessageTO cancelMsg = this.createCANMessage(orderDetailId, cancelOrigFTDMsg, csrId, ftdMessage);
							MessagingService ms = new MessagingService();
							ms.getMercuryAPI().sendCANMessage(cancelMsg, conn);

							// check for adjustment
							ADJMessageTO adjMsg = this.createADJMessage(pVO.getDeliveryDate(), orderDetailId, csrId, ftdMessage);
							if (adjMsg != null) {
								ms.getMercuryAPI().sendADJMessage(adjMsg, conn);
							}
						}
					}
					if (!skipPas) {
						ArrayList<String> addons = new ArrayList<String>();
						Boolean isPhoenixProductAvailable = false;
						if (bearAddonId != null && !bearAddonId.equalsIgnoreCase("null")) {
							addons.add(bearAddonId);
						}
						if (chocolateAddonId != null && !chocolateAddonId.equalsIgnoreCase("null")) {
							addons.add(chocolateAddonId);
						}

						ProductAvailVO paVO = null;
						logger.info("Phoenix Product List : " + phoenixProductId);

						for (String phoenixProduct : phoenixProductId.split(",")) {
							logger.info("Checking availability for the product : " + phoenixProduct);
							if (bulk != null && bulk.equalsIgnoreCase("BULK")) {
								// Add a delay to avoid numerous jobs processing at same time
								logger.info("Adding a delay before calling PAS for BULK processing ONLY");
								ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
								String delay = configUtil.getFrpGlobalParm("PHOENIX", "PROCESS_DELAY");
								Thread.sleep(Long.valueOf(delay));
								logger.info("Resuming processing....");
							}
							paVO = PASServiceUtil.getProductAvailShipData(phoenixProduct, deliveryDate, zipCode, null, null, null, addons, sourceCode);
							logger.info("available: " + paVO.isIsAvailable() + " " + paVO.isIsAddOnAvailable());
							
							if (paVO != null && paVO.isIsAvailable() && paVO.isIsAddOnAvailable()) {
								phoenixProductId = phoenixProduct;
								shipMethod = paVO.getShipMethod();
								
								if (paVO.getShipDate() != null) {
									SimpleDateFormat shipDatePOFormat = new SimpleDateFormat("MM/dd/yyyy");
									shipDate = shipDatePOFormat.parse(paVO.getShipDate());
								}
								isPhoenixProductAvailable = true;
								break;
							} else {
								logger.info("Product" + phoenixProduct + " is not available in PAS");
							}
						}

						if (!isPhoenixProductAvailable && !sendToQueue.equalsIgnoreCase("false") && !alreadySentToQueue) {
							logger.info("Sending to Queue because sendToQueue is :  " + sendToQueue);
							logger.info("None of the Phoenix Mapped products are available");
							this.sendToAppropriateQueue(orderDetailId, conn, origin, mercuryId);
							alreadySentToQueue = true;

						}

					}// end if(skipPas)
					
					logger.info("shipMethod: " + shipMethod);
					logger.info("shipDate: " + shipDate);

					if (!StringUtils.isEmpty(shipMethod)) {
						// Queue Delete messages
						// The stored procedure will issue a commit so we need to catch subsequent errors
						boolean success = dao.deleteQueueRecords(orderDetailId);

						if (success) {
							boolean postQDSuccess = false;

							try {
								// Update Order Detail record
								if (origin.equalsIgnoreCase("COM")) {
									pVO.setShipMethod(shipMethod);
									pVO.setShipDate(shipDate);
									pVO.setPhoenixProductId(phoenixProductId);
									pVO.setDeliveryDate(deliveryDate);
									dao.updatePhoenixOrder(pVO);
								} else {
									pVO.setShipMethod(shipMethod);
									pVO.setShipDate(shipDate);
									pVO.setPhoenixProductId(phoenixProductId);
									dao.updatePhoenixOrder(pVO);
								}

								// if order has addon(s) update the order add on
								// record(s)
								if (bearAddonId != null && !bearAddonId.equalsIgnoreCase("null")) {
									dao.updateOrderAddOn(orderDetailId, "B", bearAddonId);
									pVO.setBearAddonId("B");
									pVO.setPhoenixBearAddonId(bearAddonId);
								}
								if (chocolateAddonId != null && !chocolateAddonId.equalsIgnoreCase("null")) {
									dao.updateOrderAddOn(orderDetailId, "C", chocolateAddonId);
									pVO.setChocAddonId("C");
									pVO.setPhoenixChocAddonId(chocolateAddonId);
								}

								// Create Argo FTD message
								OrderTO ftdTO = new OrderTO();

								ftdTO.setReferenceNumber(String
										.valueOf(orderDetailId));
								ftdTO.setAddress1(address1);
								ftdTO.setAddress2(address2);
								ftdTO.setBusinessName(businessName);
								// Fix for defect 15995. Remove all the control characters from card message received by mercury system.
								ftdTO.setCardMessage(cardMessage != null ? cardMessage.replaceAll("\\p{Cntrl}", "") : null); 
								ftdTO.setCity(city);
								ftdTO.setDeliveryDate(deliveryDate);
								ftdTO.setMessageType("FTD");
								ftdTO.setOperator("Phoenix");
								ftdTO.setOrderDate(orderDate);
								ftdTO.setOverUnderCharge(new Double(0.0));
								ftdTO.setPhoneNumber(phoneNumber);
								ftdTO.setProductId(phoenixProductId);
								ftdTO.setRecipient(recipient);
								ftdTO.setState(state);
								ftdTO.setShipMethod(shipMethod);
								ftdTO.setShipDate(shipDate);
								ftdTO.setZipCode(zipCode);
								ftdTO.setCountry(country);
								ftdTO.setFromCommunicationScreen(false);
								ftdTO.setCompOrder(null);

								VenusService venusService = new VenusService(conn);
								ResultTO result = venusService.sendOrder(ftdTO, false);

								// check for errors
								if (!result.isSuccess()) {
									logger.error("Cannot create Venus FTD message: " + result.getErrorString());
									throw new Exception("Cannot create Venus FTD message");
								}

								/*
								 * Fix for defect 18979. check if the order has
								 * live FTD. send a cancel message if there is a
								 * live FTD exists.
								 */

								if (StringUtils.isNotEmpty(mercuryId)
										&& !mercuryId.equalsIgnoreCase("null")) {
									logger.info("MercuryId = " + mercuryId);
									MercuryDAO mercuryDAO = new MercuryDAO(conn);
									MercuryVO message = mercuryDAO.getMessage(mercuryId);
									AutoResponseKeyVO autoResponseKeyVO = null;

									// String keyPhrase = null;
									boolean cancelFlag = false;
									boolean softBlockFlag = false;
									String msgType = message.getMessageType();
									String msgText = message.getComments();

									if (message.getQueueMessageType().equalsIgnoreCase("ASKPI")) {
										if (!mercuryDAO.isFTDCancelled(mercuryId)) {
											cancelFlag = true;
										}
									} else {
										autoResponseKeyVO = mercuryDAO.getAutoResponseKey(msgText, msgType);

										if (autoResponseKeyVO == null) {
											cancelFlag = false;
											softBlockFlag = false;

										} else {
											cancelFlag = autoResponseKeyVO.getCancelFlag().equalsIgnoreCase("Y") ? true : false;
											// retryFTDFlag = autoResponseKeyVO.getRetryFTDFlag().equalsIgnoreCase("Y")? true : false;
											// viewQueueFlag = autoResponseKeyVO.getViewQueueFlag().equalsIgnoreCase("Y")? true : false;
											softBlockFlag = autoResponseKeyVO.getFloristSoftBlockFlag().equals("Y") ? true : false;
											// keyPhrase = autoResponseKeyVO.getKeyPhraseTxt();
											// commentText = MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_1 + msgType +
											// 		MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_2 + keyPhrase + MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_3;
										}
									}
									MercuryVO ftdMessage = null;

									if (cancelFlag) {
										CANMessageTO cancelMsg = createCANMessageTO(message);
										MessagingService ms = new MessagingService();
										ms.getMercuryAPI().sendCANMessage(cancelMsg, conn);
										// check for adjustment
										ftdMessage = mercuryDAO.getAssociatedFtd(message);
										ADJMessageTO adjMsg = this.createADJMessage(pVO.getDeliveryDate(), orderDetailId, csrId, ftdMessage);
										if (adjMsg != null) {
											ms.getMercuryAPI().sendADJMessage(adjMsg, conn);
										}
									}

									if (softBlockFlag) {
										if (message.getMessageType().equalsIgnoreCase("FTD")) {
											ftdMessage = message;
										} else {
											if (ftdMessage == null) {
												ftdMessage = mercuryDAO.getAssociatedFtd(message);
											}
										}
										softBlockFlorist(ftdMessage.getFillingFlorist());
									}

								}

								pVO.setEmailBody(emailBody);
								pVO.setEmailSubject(emailSubject);

								// Create Comments
								generateOrderComments(orderDetailId, origin,
										hasDeliveryDateChanged, deliveryDate,
										emailType, origNewProdSame, csrId,
										orderComments, startOrigin);

								// Add to tracking table
								dao.insertPhoenixOrderTracking(pVO);

								// Send email
								if (origin.equalsIgnoreCase("COM")) {
									if (origNewProdSame.equalsIgnoreCase("N")) {
										if (emailType
												.equalsIgnoreCase("apology_email")
												|| emailType
														.equalsIgnoreCase("phoenix_email")
												|| emailType
														.equalsIgnoreCase("stock_email")) {
											if (hasDeliveryDateChanged)
												sendEmailToCustomer(pVO, cusVO,
														orderVO, emailType,
														customerPhone,
														orginalDeliveryDate,
														deliveryDate, csrId,
														emailTitle, startOrigin);
											else
												sendEmailToCustomer(pVO, cusVO,
														orderVO, emailType,
														customerPhone,
														orginalDeliveryDate,
														null, csrId,
														emailTitle, startOrigin);
										}
									}
								} else {
									if (origNewProdSame.equalsIgnoreCase("N"))
										sendEmailToCustomer(pVO, cusVO,
												orderVO, emailType,
												customerPhone,
												orginalDeliveryDate, null,
												csrId, emailTitle, startOrigin);
								}

								postQDSuccess = true;

							} catch (Exception e) {
								logger.error("exception occured after Queue delete: "
										+ e);
							} finally {
								if (!postQDSuccess) {
									logger.error("Process did not complete. Sending System Message.");
									String systemMessage = "An error occurred during Phoenix Processing\n"
											+ " order_detail_id: "
											+ orderDetailId
											+ "\n"
											+ " external_order_number: "
											+ pVO.getExternalOrderNumber();
									CommonUtils
											.sendSystemMessage(systemMessage);
								}
							}

						} else {
							logger.info("Message could not be Queue deleted: "
									+ orderDetailId);
						}
					} else {
						logger.info("Could not find a Ship Method");
						if (!sendToQueue.equalsIgnoreCase("false")
								&& !alreadySentToQueue) {
							this.sendToAppropriateQueue(orderDetailId, conn,
									origin, mercuryId);
							alreadySentToQueue = true;
						}
					}
				} else {
					logger.info("Recipient address on Mercury message is different than recipient address on order");
					if (!sendToQueue.equalsIgnoreCase("false")
							&& !alreadySentToQueue) {
						this.sendToAppropriateQueue(orderDetailId, conn,
								origin, mercuryId);
						alreadySentToQueue = true;
					}
				}
			}

			logger.info("Finished");

		} catch (ParseException pe) {
			logger.error("Invalid date format for delivery date in PhoenixServer: "
					+ pe);
		} catch (Exception e) {
			logger.error(e);
			logger.info("Unexpected error encountered");
			if (!sendToQueue.equalsIgnoreCase("false") && !alreadySentToQueue) {
				this.sendToAppropriateQueue(orderDetailId, conn, origin,
						mercuryId);
				alreadySentToQueue = true;
			}
		} finally {
			// close the connection
			try {
				conn.close();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	private void createComments(long orderDetailId, String commentText,
			String csrId, String startOrigin) throws Exception {
		OrderDAO orderDao = new OrderDAO(conn);
		String user = "Phoenix";
		String commentOrigin = OrderConstants.OPERATOR_OP;
		if (csrId != null && !csrId.equalsIgnoreCase("null")) {
			user = csrId;
			commentOrigin = "CS Call";

		}
		if (startOrigin != null && !startOrigin.equalsIgnoreCase("null")) {
			commentOrigin = startOrigin;
		}
		try {
			CommentsVO commentsVO = new CommentsVO();
			commentsVO.setComment(commentText);
			commentsVO.setCommentOrigin(commentOrigin);
			commentsVO.setCommentType("Order");
			commentsVO.setCreatedBy(user);
			commentsVO.setUpdatedBy(user);
			commentsVO.setOrderDetailId(Long.toString(orderDetailId));
			orderDao.insertComment(commentsVO);
		} catch (Exception e) {
			throw e;
		}
	}

	private void createStockComment(long orderDetailId, String orderGuid,
			Long customerId, String commentText, String csrId,
			String startOrigin) throws Exception {
		OrderDAO orderDao = new OrderDAO(conn);
		String user = "Phoenix";
		String commentOrigin = OrderConstants.OPERATOR_OP;
		if (csrId != null && !csrId.equalsIgnoreCase("null")) {
			user = csrId;
			commentOrigin = "CS Call";
		}

		if (startOrigin != null && !startOrigin.equalsIgnoreCase("null")) {
			commentOrigin = startOrigin;
		}

		// Call insertComment() on OrderDAO passing the specified comment
		// attached to the current order and customer
		CommentsVO commentObj = new CommentsVO();
		commentObj.setComment(commentText);
		commentObj.setCommentType("Order");
		commentObj.setOrderDetailId(Long.toString(orderDetailId));
		commentObj.setOrderGuid(orderGuid);
		commentObj.setCustomerId(Long.toString(customerId));

		commentObj.setCreatedBy(user);
		commentObj.setUpdatedBy(user);

		commentObj.setCommentOrigin(commentOrigin);
		commentObj.setReason("email");
		orderDao.insertComment(commentObj);
	}

	private void sendToAppropriateQueue(long orderDetailId, Connection conn,
			String origin, String mercuryId) throws Exception {
		logger.info("sendToAppropriateQueue: " + orderDetailId + " " + origin);
		OrderDAO orderDao = new OrderDAO(conn);
		OrderService orderService = new OrderService(conn);
		try {
			// lookup the orderDetail record
			OrderDetailVO orderDtl = orderDao.getOrderDetail(Long
					.toString(orderDetailId));
			if (origin.equalsIgnoreCase("REJ")) {
				// since this is a resend, do not perform validations
				logger.debug("Setting order detail #"
						+ orderDtl.getOrderDetailId()
						+ " OP_STATUS to PENDING...");
				orderDtl.setOpStatus("Pending");
				orderDao.updateOrder(orderDtl);
				logger.debug("RETRY COUNT = " + orderDtl.getRejectRetryCount());
				// Clear florist
				orderDao.clearFlorist("" + orderDtl.getOrderDetailId());
				orderDtl.setFloristId(null);

				MercuryDAO dao = new MercuryDAO(conn);
				MercuryVO mercMessage = dao.getMessage(mercuryId);
				// Save associated FTD in orderDetail so we can eventually
				// resend this FTD (in MercuryOutboundService)
				orderDtl.setCurrentFtd(mercMessage);
				String msgType = mercMessage.getMessageType();
				String msgText = mercMessage.getComments();
				AutoResponseKeyVO autoResponseKeyVO = null;
				autoResponseKeyVO = dao.getAutoResponseKey(msgText, msgType);
				String commentText = null;

				if (autoResponseKeyVO == null) {
					// No key phrase found. Send message to queue.
					logger.info(orderDtl.getOrderDetailId()
							+ ": No key phrase found. Send message to queue");
					QueueService queueService = new QueueService(conn);
					queueService.sendMercuryMessageToQueue(mercMessage);
					return;
				} else {
					String keyPhrase = autoResponseKeyVO.getKeyPhraseTxt();
					commentText = MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_1
							+ msgType
							+ MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_2
							+ keyPhrase
							+ MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_3;
				}
				// Clear florist
				orderDao.clearFlorist("" + orderDtl.getOrderDetailId());
				orderDtl.setFloristId(null);
				// Adding a comment explaining what caused the retry.
				createComments(orderDtl.getOrderDetailId(), commentText, null,
						null);

				// try to reroute the order
				boolean succeeded = orderService.processOrder(orderDtl, false,
						false);
				if (!succeeded) {
					QueueService queueService = new QueueService(conn);
					queueService.sendMercuryMessageToQueue(mercMessage);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Could not automatically re-process.  Sending message to queue");
				}
			} else if (origin.equalsIgnoreCase("ZIP")) {
				orderDtl.setZipQueueCount(orderDtl.getZipQueueCount() + 1);
				orderDao.updateOrder(orderDtl);
				QueueService oQM = new QueueService(conn);
				oQM.sendOrderToZipQueue(orderDtl);
				logger.info(orderDtl.getOrderDetailId()
						+ ": Order sent to ZIP CS queue");
			} else if (origin.equalsIgnoreCase("FTD")) {
				if (mercuryId != null) {
					MercuryDAO dao = new MercuryDAO(conn);
					MercuryVO mercMessage = dao.getMessage(mercuryId);

					String msgType = mercMessage.getMessageType();
					String msgText = mercMessage.getSakText();

					AutoResponseKeyVO autoResponseKeyVO = null;
					autoResponseKeyVO = dao
							.getAutoResponseKey(msgText, msgType);
					String commentText = null;
					if (autoResponseKeyVO == null) {
						// No key phrase found. Send message to queue.
						logger.info(orderDtl.getOrderDetailId()
								+ ": No key phrase found. Send message to queue");
						QueueService queueService = new QueueService(conn);
						queueService.sendMercuryMessageToQueue(mercMessage);
						return;
					} else {
						String keyPhrase = autoResponseKeyVO.getKeyPhraseTxt();
						commentText = MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_1
								+ msgType
								+ MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_2
								+ keyPhrase
								+ MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_3;
					}

					orderDao.clearFlorist("" + orderDtl.getOrderDetailId());
					orderDtl.setFloristId(null);
					// Adding a comment explaining what caused the retry.
					createComments(orderDtl.getOrderDetailId(), commentText,
							null, null);

					// try to reroute the order
					boolean succeeded = orderService.processOrder(orderDtl,
							false, false);
					if (!succeeded) {
						// Save associated FTD in orderDetail so we can
						// eventually resend this FTD (in
						// MercuryOutboundService)
						orderDtl.setCurrentFtd(mercMessage);
						QueueService queueService = new QueueService(conn);
						queueService.sendMercuryMessageToQueue(mercMessage);
						logger.info(mercuryId + ": Order sent to FTD CS queue");
					}

				} else {
					QueueService oQM = new QueueService(conn);
					oQM.sendOrderToFtdQueue(orderDtl);
					logger.info(orderDtl.getOrderDetailId()
							+ ": Order sent to FTD CS queue");
				}
			} else if (origin.equalsIgnoreCase("ASK")) {
				if (mercuryId != null) {
					String commentText = null;
					MercuryDAO dao = new MercuryDAO(conn);
					MercuryVO mercMessage = dao.getMessage(mercuryId);
					if (mercMessage != null
							&& "ASKPI".equalsIgnoreCase(mercMessage
									.getQueueMessageType())) {
						commentText = MercuryConstants.ASKP_AUTO_CANCEL_ORDER_COMMENT_TEXT;
						// String msgType = mercMessage.getMessageType();
						// String msgText = mercMessage.getComments();
						// Clear florist
						orderDao.clearFlorist("" + orderDtl.getOrderDetailId());
						orderDtl.setFloristId(null);

						// Adding a comment explaining what caused the retry.
						createComments(orderDtl.getOrderDetailId(),
								commentText, null, null);

						// try to reroute the order
						boolean succeeded = orderService.processOrder(orderDtl,
								false, false);
						if (!succeeded) {
							// Save associated FTD in orderDetail so we can
							// eventually resend this FTD (in
							// MercuryOutboundService)
							orderDtl.setCurrentFtd(mercMessage);
							QueueService queueService = new QueueService(conn);
							queueService.sendMercuryMessageToQueue(mercMessage);
							logger.info(mercuryId
									+ ": Order sent to ASKPI queue");
						} else {
							CANMessageTO cancelMsg = createCANMessageTO(mercMessage);
							MessagingService ms = new MessagingService();
							ms.getMercuryAPI().sendCANMessage(cancelMsg, conn);
						}

					}

				}

			}
			/**
			 * if origin of type not "FTD" not "ZIP" not "REJ" and not "ASKPI"
			 * then send mercury message to Queue.
			 */
			else {
				QueueService queueService = new QueueService(conn);
				MercuryDAO dao = new MercuryDAO(conn);
				MercuryVO mercMessage = dao.getMessage(mercuryId);
				queueService.sendMercuryMessageToQueue(mercMessage);
				logger.info(orderDtl.getOrderDetailId()
						+ ": Could not automatically re-process.  Sending message to queue");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Create CAN message transmission object.
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public CANMessageTO createCANMessage(long orderDetailId,
			String commentText, String csrId, MercuryVO ftdMessage) {
		CANMessageTO canTO = new CANMessageTO();
		if (logger.isDebugEnabled()) {
			logger.debug("Entering createCANMessageTO");
		}

		String comments = commentText;
		String sendingFlorist = ftdMessage.getSendingFlorist();
		String ftdMessageID = ftdMessage.getMercuryId();

		canTO.setMercuryId(ftdMessageID);
		canTO.setComments(comments);
		canTO.setOperator(csrId);
		canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
		canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
		canTO.setFillingFlorist(null);
		canTO.setSendingFlorist(sendingFlorist);

		return canTO;
	}

	/**
	 * Enqueues a JMS message for message-generator to send a Phoenix email to
	 * the customer.
	 * 
	 * @param pVO
	 * @param emailTypeKey
	 *            - Should match email_type_key from
	 *            ftd_apps.phoenix_email_types
	 * @param origDeliveryDate
	 *            - Original delivery date of order
	 * @param newDeliveryDate
	 *            - Should be null if no change to order delivery date,
	 *            otherwise set to new delivery date
	 * @throws Exception
	 */
	private void sendEmailToCustomer(PhoenixVO pVO, CustomerVO cusVO,
			OrderVO orderVO, String emailTypeKey, String customerPhone,
			Date origDeliveryDate, Date newDeliveryDate, String csrId,
			String emailTitle, String startOrigin) throws Exception {
		PointOfContactVO pocObj = new PointOfContactVO();
		StockMessageGenerator stockGen = new StockMessageGenerator(conn);
		MessageGeneratorDAO msgGenDAO = new MessageGeneratorDAO(conn);
		boolean bearAddonFlag = false;
		boolean chocAddonFlag = false;

		// Create Point-Of-Contact object with relevant information for the
		// email
		//
		pocObj.setLetterTitle(emailTypeKey); // Email Type Key is stored in
												// Letter Title
		pocObj.setPointOfContactType(MessageConstants.EMAIL_MESSAGE_TYPE_PHOENIX);
		pocObj.setOrderDetailId(pVO.getOrderDetailId());
		pocObj.setExternalOrderNumber(pVO.getExternalOrderNumber());
		pocObj.setRecipientEmailAddress(pVO.getEmailAddress());
		pocObj.setCompanyId(pVO.getCompanyId());
		Calendar calOrig = Calendar.getInstance();
		calOrig.setTime(origDeliveryDate);
		pocObj.setDeliveryDate(calOrig);
		if (newDeliveryDate != null) {
			Calendar calNew = Calendar.getInstance();
			calNew.setTime(newDeliveryDate);
			pocObj.setNewDeliveryDate(calNew);
		}
		if (emailTypeKey.equals("stock_email")) {
			pocObj.setLetterTitle(emailTitle);
			pocObj.setBody(pVO.getEmailBody());
			pocObj.setEmailSubject(pVO.getEmailSubject());
			String companyEmailAddress = msgGenDAO.loadSenderEmailAddress(
					emailTypeKey, pVO.getCompanyId(), pVO.getOrderDetailId());
			pocObj.setSenderEmailAddress(companyEmailAddress);
			pocObj.setCustomerId(cusVO.getCustomerId());
			pocObj.setOrderGuid(orderVO.getOrderGuid());
			pocObj.setMasterOrderNumber(orderVO.getMasterOrderNumber());
			pocObj.setFirstName(cusVO.getFirstName());
			pocObj.setLastName(cusVO.getLastName());
			pocObj.setDaytimePhoneNumber(customerPhone);
			pocObj.setEmailType(MessageConstants.COM_EMAIL_TYPE);
			pocObj.setPointOfContactType(MessageConstants.EMAIL_MESSAGE_TYPE);
			pocObj.setCommentType("Order");
			pocObj.setSentReceivedIndicator(OUTGOING_MESSAGE);
			// creating stockEmail Order Comments
			createStockComment(pVO.getOrderDetailId(), orderVO.getOrderGuid(),
					cusVO.getCustomerId(),
					"Email Title: " + pocObj.getLetterTitle(), csrId,
					startOrigin);
		} else {
			//
			// Body should contain product ID and any addons in format:
			// <product_id>[:addon[,addon]]
			// e.g., "6022:B,C"
			String prodStr = pVO.getPhoenixProductId();
			if (MessageConstants.PHOENIX_TOKEN_BEAR_ADDON.equals(pVO
					.getBearAddonId())) {
				bearAddonFlag = true;
			}
			if (MessageConstants.PHOENIX_TOKEN_CHOC_ADDON.equals(pVO
					.getChocAddonId())) {
				chocAddonFlag = true;
			}
			if (bearAddonFlag && chocAddonFlag) {
				prodStr += ":" + MessageConstants.PHOENIX_TOKEN_BEAR_ADDON
						+ "," + MessageConstants.PHOENIX_TOKEN_CHOC_ADDON;
			} else if (bearAddonFlag) {
				prodStr += ":" + MessageConstants.PHOENIX_TOKEN_BEAR_ADDON;
			} else if (chocAddonFlag) {
				prodStr += ":" + MessageConstants.PHOENIX_TOKEN_CHOC_ADDON;
			}
			pocObj.setBody(prodStr);
			pocObj.setMailserverCode(MessageConstants.MAILSERVER_CODE_ACXIOM);
		}

		// Insert into Point-Of-Contact table
		//
		stockGen.insertMessage(pocObj);
		if (logger.isDebugEnabled()) {
			logger.debug("Point-Of-Contact email entry (ID:"
					+ pocObj.getPointOfContactId()
					+ ") inserted for Phoenix order: "
					+ pocObj.getExternalOrderNumber());
		}

		// Now trigger email by enqueueing message for MessageGenerator (with
		// pocObj ID as payload)
		StockMessageGenerator.sendMessage(pocObj);
	}

	private ADJMessageTO createADJMessage(Date orderDeliveryDate,
			long orderDetailId, String csrId, MercuryVO ftdMessage)
			throws Exception {
		ADJMessageTO adjTO = null;
		Calendar cOrderDeliveryDate = null;
		boolean adjustmentNeed = false;

		// check if ADJ needs to be sent based on original delivery date
		if (this.checkDeliveryDate(orderDeliveryDate)) {
			adjustmentNeed = true;
			logger.debug("Based on date, ADJ needed on Phoneix Order");
			cOrderDeliveryDate = Calendar.getInstance();
			cOrderDeliveryDate.setTime(orderDeliveryDate);
		}

		if (adjustmentNeed) {
			String adjReasonCode = OrderConstants.MODIFY_ORDER_ADJ_REASON;

			// combinded report number = month + "-1"
			int month = cOrderDeliveryDate.get(Calendar.MONTH) + 1;
			String sMonth = String.valueOf(month);
			// make the month 2 digits
			if (sMonth.length() == 1) {
				sMonth = '0' + sMonth;
			}
			String combinedReportNumber = sMonth + "-1";

			String sendingFlorist = ftdMessage.getSendingFlorist();
			String ftdMessageID = ftdMessage.getMercuryId();

			adjTO = new ADJMessageTO();
			adjTO.setMercuryId(ftdMessageID);
			adjTO.setComments(OrderConstants.ADJ_MSG_COMMENT);
			adjTO.setCombinedReportNumber(combinedReportNumber);
			adjTO.setOperator(csrId);
			adjTO.setMercuryStatus("MO");
			adjTO.setDirection("OUTBOUND");
			adjTO.setOldPrice(new Double(ftdMessage.getPrice()));
			adjTO.setOverUnderCharge(new Double("0.00"));
			adjTO.setAdjustmentReasonCode(adjReasonCode);
			adjTO.setSendingFlorist(sendingFlorist);
			adjTO.setFillingFlorist(ftdMessage.getFillingFlorist());
		}
		return adjTO;
	}

	private boolean checkDeliveryDate(Date deliveryDate) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Entering checkDeliveryDate");
			logger.debug("deliveryDate: " + deliveryDate);
		}
		boolean dateComparision = false;

		try {
			Calendar calCurrent = Calendar.getInstance(TimeZone.getDefault());
			Date currentDate = calCurrent.getTime();

			if (currentDate.after(deliveryDate)) {
				Calendar calDelivery = Calendar.getInstance(TimeZone
						.getDefault());
				calDelivery.setTime(deliveryDate);

				int day = calDelivery.getActualMaximum(Calendar.DAY_OF_MONTH);

				Calendar calAdjustment = Calendar.getInstance(TimeZone
						.getDefault());
				calAdjustment.setTime(deliveryDate);
				calAdjustment.set(calDelivery.get(Calendar.YEAR),
						calDelivery.get(Calendar.MONTH), day);

				Date adjustmentDate = calAdjustment.getTime();

				if (currentDate.after(adjustmentDate)) {
					dateComparision = true;
				}
			}
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting checkDeliveryDate");
			}
		}
		return dateComparision;
	}

	private boolean hasDeliveryDateChanged(Date originalDeliveryDate,
			Date newDeliveryDate) {
		boolean deliveryDatechanged = true;
		if (originalDeliveryDate.equals(newDeliveryDate))
			deliveryDatechanged = false;

		return deliveryDatechanged;
	}

	private void generateOrderComments(long orderDetailId, String origin,
			boolean hasDeliveryDateChanged, Date newDeliveryDate,
			String emailType, String isOrigNewProdSame, String csrId,
			String orderComments, String startOrigin) throws Exception {
		logger.info("Generating Order Comments with : " + orderComments);
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String stringDeliveryDate = formatter.format(newDeliveryDate);

		ConfigurationUtil config = ConfigurationUtil.getInstance();
		if (origin.equalsIgnoreCase("COM")) {
			// Customer Entered comments in Phoenix screen
			if (!StringUtils.isEmpty(orderComments)
					&& !orderComments.equalsIgnoreCase("null")) {
				createComments(orderDetailId, orderComments, csrId, startOrigin);
			}

			if (emailType.equalsIgnoreCase("phoenix_email")) {
				if (hasDeliveryDateChanged) {
					String commentText = (config.getContentWithFilter(conn,
							"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
							"TMPL_PHOENIX_NEWDATE", null)).replace("[date]",
							stringDeliveryDate);
					createComments(orderDetailId, commentText, null, null);
				} else {
					String commentText = config.getContentWithFilter(conn,
							"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
							"TMPL_PHOENIX_DEFAULT", null);
					createComments(orderDetailId, commentText, null, null);
				}
			} else if (emailType.equalsIgnoreCase("apology_email")) {
				String commentText = (config.getContentWithFilter(conn,
						"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
						"TMPL_APOLOGY", null)).replace("[date]",
						stringDeliveryDate);
				createComments(orderDetailId, commentText, null, null);

			} else if (emailType.equalsIgnoreCase("doc_email")) {
				String commentText = (config.getContentWithFilter(conn,
						"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE", "TMPL_DOC",
						null)).replace("[date]", stringDeliveryDate);
				createComments(orderDetailId, commentText, null, null);
			} else if (emailType.equalsIgnoreCase("none")
					|| emailType.equalsIgnoreCase("stock_email")) {
				if (isOrigNewProdSame.equalsIgnoreCase("Y")) {
					String commentText = config.getContentWithFilter(conn,
							"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
							"TMPL_SDFC", null);
					createComments(orderDetailId, commentText, null, null);
				} else {
					String commentText = (config.getContentWithFilter(conn,
							"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
							"TMPL_NONE", null)).replace("[date]",
							stringDeliveryDate);
					createComments(orderDetailId, commentText, null, null);
				}
			}
		} else {
			if (isOrigNewProdSame.equalsIgnoreCase("Y")) {
				String commentText = config.getContentWithFilter(conn,
						"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE", "TMPL_SDFC",
						null);
				createComments(orderDetailId, commentText, null, null);
			} else {
				String commentText = config.getContentWithFilter(conn,
						"PHOENIX_EMAIL", "COMMENT_TEXT_TEMPLATE",
						"TMPL_PHOENIX_DEFAULT", null);
				createComments(orderDetailId, commentText, null, null);
			}
		}
	}

	/**
	 * Create CAN message transmission object.
	 * 
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public CANMessageTO createCANMessageTO(MercuryVO message) throws Exception {

		CANMessageTO canTO = new CANMessageTO();
		if (logger.isDebugEnabled()) {
			logger.debug("Entering createCANMessageTO");
		}

		try {
			MercuryVO ftdMessage = null;
			MercuryDAO mercuryDao = new MercuryDAO(conn);

			// get the associated FTD
			ftdMessage = mercuryDao.getAssociatedFtd(message);
			String operator = "OP";
			String comments = MercuryConstants.AUTO_CANCEL_MESSAGE_COMMENT_TEXT;
			String sendingFlorist = ftdMessage.getSendingFlorist();
			String ftdMessageID = ftdMessage.getMercuryId();

			canTO.setMercuryId(ftdMessageID);
			canTO.setComments(comments);
			canTO.setOperator(operator);
			canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
			canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
			canTO.setFillingFlorist(null);
			canTO.setSendingFlorist(sendingFlorist);
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting createCANMessageTO");
			}
		}
		return canTO;
	}

	/**
	 * Builds a floristVO and calls FloristMaintenace to soft block the florist
	 * Changes for 1791
	 * 
	 * @param floristId
	 */
	public void softBlockFlorist(String floristId) {
		boolean lockAcquired = false;

		FloristVO floristVO = new FloristVO();
		floristVO.setFloristId(floristId);
		floristVO.setBlockedByUserId(MercuryConstants.FLORIST_BLOCK_USER_ID);
		floristVO.setBlockType(MercuryConstants.FLORIST_SOFT_BLOCK_TYPE);

		FloristMaintenanceLockHelper lockHelper = new FloristMaintenanceLockHelper(
				conn);
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();

			if (floristId == null || StringUtils.isEmpty(floristId)) {
				logger.error("Florist Id is null");
				return;
			}

			// get number of days to block from secure context

			String blockDays = configUtil
					.getFrpGlobalParm(
							MercuryConstants.SECURE_CONFIG_CONTEXT,
							MercuryConstants.SECURE_CONFIG_FIELD_FLORIST_SOFT_BLOCK_DAYS);
			int softBlockDays = Integer.parseInt(blockDays);

			if (softBlockDays <= 0) {
				// soft block disabled
				logger.info("Soft Block Disabled. Florist " + floristId
						+ " is active.");
				return;
			}

			// calculate block end date
			// get today calendar object. time set to 6am
			Calendar now = Calendar.getInstance();

			// get today at 6am
			Calendar blockEndDate = new GregorianCalendar(
					now.get(Calendar.YEAR), now.get(Calendar.MONTH),
					now.get(Calendar.DAY_OF_MONTH), 6, 00);

			// calculate end date. today + soft block days = expiry date 6am
			blockEndDate.add(Calendar.DAY_OF_YEAR, softBlockDays);

			floristVO.setBlockEndDate(blockEndDate.getTime());

			// acquire lock
			lockHelper.requestMaintenanceLock(floristVO,
					floristVO.getBlockedByUserId());

			// update florist to database if lock is still present
			if (floristVO.getLockedFlag() != null
					&& floristVO.getLockedFlag().equals("Y")) {
				lockAcquired = true;
				// update florist profile
				FloristMaintenanceDAO fmDAO = new FloristMaintenanceDAO(conn);
				fmDAO.updateFloristBlocks(floristVO, "Y");
				logger.info(" Florist " + floristId + " soft blocked for "
						+ softBlockDays + " days by SYS. Block expires on "
						+ floristVO.getBlockEndDate().toString());
			}
		} catch (Exception e) {
			logger.error("Not able to soft block florist " + floristId);
			logger.error(e);
		} finally {
			// release lock
			try {
				if (lockAcquired == true) {
					lockHelper.releaseMaintenanceLock(floristVO.getFloristId(),
							floristVO.getBlockedByUserId());
				}
			} catch (Exception e) {
				logger.error("Release lock failed " + floristId);
				logger.error(e);
			}
		}
	}

	/**
	 * Queues a Mercury message for Customer Service to work.
	 *
	 * @param message
	 */
	/*
	 * private void queueInboundMessage(MercuryVO message) throws Exception {
	 * QueueService queueService = new QueueService(conn); if
	 * (message.getMessageType().equalsIgnoreCase("GEN")) {
	 * queueService.sendUnassociatedMercuryMessageToQueue(message); } else {
	 * queueService.sendMercuryMessageToQueue(message); } }
	 */
}
