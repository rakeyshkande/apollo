package com.ftd.op.order.service;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.dao.QueueDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.PartnerDCONService;
import com.ftd.op.common.vo.QueueVO;
import com.ftd.op.mercury.constants.MercuryConstants;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.facade.MercuryFacade;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.mercury.vo.SendingFloristVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.EmailVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.LockUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.orderlifecycle.OrderLifecycleException;
import com.ftd.osp.utilities.orderlifecycle.OrderLifecycleUtilities;
import com.ftd.osp.utilities.orderlifecycle.OrderStatus;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPException;
import com.ftd.osp.utilities.xml.JAXPUtil;

public class DeliveryConfirmationService {

    private Logger logger = null;

    private static final String CONFIG_FILE = "order-processing-config.xml";
    private static final String ASK_MSG_OPERATOR = "SYSAR";
    private static final String ASK_MSG_STATUS = "MO";
    private static final String ASK_MSG_DIRECTION = "OUTBOUND";
    private static final String DCON_DC_QUEUE = "DC";
    private static final String DCON_DI_QUEUE = "DI";
    private static final String DCON_SYSTEM = "Merc";
    private static final String PROCESS_LOCK_ID = "LIFECYCLE";
    private static final String CONTENT_CONTEXT_PREFERRED_PARTNER = "PREFERRED_PARTNER";
    private static final String CONTENT_CONTEXT_DELIVERY_CONFIRMATION = "DELIVERY_CONFIRMATION";
    private static final String CONTENT_NAME_DCON_ASK_MESSAGE_PARTNER = "DCON_ASK_MESSAGE_PARTNER";
    private static final String CONTENT_NAME_DCON_ASK_MESSAGE = "DCON_ASK_MESSAGE";
    private static final String CONTENT_NAME_DCON_SIGNATURE = "DCON_SIGNATURE";
    private final static String PARTNER_ORDER_ITEM_NUMBER = "PARTNER_ORDER_ITEM_NUMBER";
    private static final String EMAIL_TYPE_DCON = "DCON";
    private static final String DELIVERY_CONFIRMATION_STATUS = "Delivered";
	private static final String DCON_LIFE_CYCLE_UPDATE_DATETIME_TYPE = "yyyy-MM-dd'T'HH:mm:ss";
    
    Connection conn = null;
  
  /**
     * Constructor
     * 
     * @param
     * @return n/a
     * @throws
     */
  public DeliveryConfirmationService(Connection conn) {
      if(logger == null) {
          logger = new Logger("com.ftd.op.order.service.DeliveryConfirmationService");
      }

      this.conn = conn;

  }
  
  public void sendDeliveryConfirmationASK() throws Throwable {

      logger.debug("sendDeliveryConfirmationASK()");

      OrderDAO dao = new OrderDAO(this.conn);
      MercuryDAO mdao = new MercuryDAO(conn);
      try {

          String checkOrderLifecycle = "";
          String getAllStatusUpdates = "";
          boolean getAll = false;
          GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
          if(globalParamHandler != null) {
              checkOrderLifecycle = globalParamHandler.getFrpGlobalParm(OrderConstants.LIFECYCLE_CONTEXT, OrderConstants.PERFORM_ORDER_LIFECYCLE_UPDATES);
              getAllStatusUpdates = globalParamHandler.getFrpGlobalParm(OrderConstants.LIFECYCLE_CONTEXT, OrderConstants.GET_ALL_STATUS_UPDATES);
          }
          if (getAllStatusUpdates != null && getAllStatusUpdates.equalsIgnoreCase("Y")) getAll = true;
          
          List msgList = dao.getPendingDeliveryConfirmations();
          for (int i=0; i<msgList.size(); i++) {
              MercuryVO vo = (MercuryVO) msgList.get(i);
              logger.debug("Sending DCON ASK: " + vo.getMercuryId() + " " + vo.getMercuryOrderNumber());
              
              boolean foundConfirmation = false;
              if (checkOrderLifecycle != null && checkOrderLifecycle.equalsIgnoreCase("Y")) {
                  logger.debug("Checking Order Lifecycle");
                  List orderStatusList = OrderLifecycleUtilities.GetOrderStatus(vo.getSendingFlorist(), vo.getMercuryOrderNumber(), getAll);
                  logger.debug("size:" + orderStatusList.size());
                  for (int j=0; j<orderStatusList.size(); j++) {
                      OrderStatus os = (OrderStatus) orderStatusList.get(j);
                      logger.info(os.getStatusId() + " " + os.getStatusCode() + " " + os.getEfosNumber() + " " + os.getStatusText() + " " + os.getStatusDate());
                      if (os.getEfosNumber() != null) {
                          logger.debug("Inserting lifecycle record");
                          Map result = mdao.insertLifecycleStatus(os);
                          String mercuryId =  (String) result.get("OUT_MERCURY_ID");
                          String orderDetailId =  (String) result.get("OUT_ORDER_DETAIL_ID");
                          String dconStatus = (String) result.get("OUT_DCON_STATUS");
                          logger.debug("Results: " + mercuryId + " " + orderDetailId + " " + dconStatus);
                          if (os.getStatusCode().equalsIgnoreCase(OrderConstants.LIFECYCLE_CONFIRMED) && dconStatus != null && !dconStatus.equalsIgnoreCase(GeneralConstants.DCON_CONFIRMED)) {
                              boolean success = sendDeliveryConfirmationEmail(orderDetailId);
                              logger.debug("success: " + success);
                              if (success) {
                                  foundConfirmation = true;
                              }
                          }
                      }
                      logger.debug("Confirming " + os.getStatusId());
                      //OrderLifecycleUtilities.ConfirmOrderStatus(os.getStatusId());
                  }
              }
              
              if (!foundConfirmation) {
                  generateConfirmationAskMessage(vo);
                  logger.debug("Updating status");
                  long orderDetailId = Long.parseLong(vo.getReferenceNumber());
                  dao.updateDeliveryConfirmationStatus(orderDetailId, GeneralConstants.DCON_SENT, "DCON-ASK");
              }
          }
      } catch (Exception e) {
          throw e;
      }
  }
  
  public void populateDeliveryConfirmationQueue() throws Throwable {
      logger.debug("populateDeliveryConfirmationQueue()");
      
      OrderDAO dao = new OrderDAO(this.conn);
      QueueDAO qdao = new QueueDAO(this.conn);
      try {
          List msgList = dao.getNonConfirmedOrders();
          for (int i=0; i<msgList.size(); i++) {
              QueueVO vo = (QueueVO) msgList.get(i);
              long orderDetailId = Long.parseLong(vo.getOrderDetailId());
              if(vo.isHasEmail() == true){
                logger.debug("Inserting Queue record: " + vo.getExternalOrderNumber());
                vo.setQueueType(DCON_DC_QUEUE);
                vo.setMessageType(DCON_DC_QUEUE);
                vo.setSystem(DCON_SYSTEM);
                vo.setMessageTimestamp(new Date());
                qdao.insertQueueRecord(vo);
                logger.debug("Updating status");
                dao.updateDeliveryConfirmationStatus(orderDetailId, GeneralConstants.DCON_QUEUED, "DCON-QUEUE");
              }
              else{
                logger.debug("Skipped Queue record: " + vo.getExternalOrderNumber());
                logger.debug("Updating status");
                dao.updateDeliveryConfirmationStatus(orderDetailId, GeneralConstants.DCON_NO_EMAIL, "DCON-QUEUE");
              }
          }
      } catch (Exception e) {
          throw e;
      }

  }
  
  /**
  * Retrieves the lifecycle updates and queues JMS messages to process each update
  */
  public void getNewLifecycleUpdates() throws Throwable {
      logger.debug("getNewLifecycleUpdates()");
      
    LockUtil lockUtil = new LockUtil();
      boolean lockObtained = false;
      String sessionId = "";

      try {

          logger.info("Locking order");

          InetAddress addr = InetAddress.getLocalHost();
          String hostname = addr.getHostName();
          Date now = new Date();
          sessionId = hostname + "-" + now.getTime();
          
          DocumentBuilder documentBuilder = DOMUtil.getDocumentBuilder();
          Dispatcher dispatcher = Dispatcher.getInstance();
          
          boolean lockResult = lockUtil.obtainProcessLock(conn, PROCESS_LOCK_ID, sessionId);
          logger.debug("Obtain lockResult: " + lockResult);
          if (lockResult) {
              lockObtained = true;
              
              MercuryDAO dao = new MercuryDAO(conn); 
              //List sendingFloristList = dao.retrieveCompanySendingFlorists();
              List<String> sendingFloristList = dao.getMercuryEapiOps();
             for (String sendingFloristStr : sendingFloristList) {
            	 logger.debug("Getting order status list for main member code: " + sendingFloristStr);
            	 List<OrderStatus> orderStatusList = OrderLifecycleUtilities.GetNewOrderStatusList(sendingFloristStr);
            	 logger.debug("size:" + orderStatusList.size());
            	 for (OrderStatus orderStatus : orderStatusList) { 
            		 String osXML = orderStatus.toXML(documentBuilder);
            		 if(logger.isDebugEnabled()) {
                       logger.debug("Routing to DCONLIFECYCLEUPDATE JMS Handler: " + osXML);
                     }
            		 
            		// Note: use Dispatcher directly, the CommonUtils.sendJMSMessage sets the Correlation ID to be
                     // The same as the message, which make it too long.
                     // Instead, use the OrderStatus's ID as the Correlation ID.
                     MessageToken token = new MessageToken();
                     token.setMessage(osXML);
                     token.setJMSCorrelationID("STATUSID: " + orderStatus.getStatusId() + ", EFOSNUMBER: " + orderStatus.getEfosNumber());
                     token.setStatus("DCONLIFECYCLEUPDATE");
                     dispatcher.dispatchTextMessage(new InitialContext(), token); 
				}
			 } 
          }

      } catch (OrderLifecycleException ole) {
          logger.error("ole exception: " + ole.getMessage());
          throw ole;
      } catch (Throwable t) {
          throw t;
      } finally {
          try {
              if (lockObtained) {
                  boolean lockResult = lockUtil.releaseProcessLock(conn, PROCESS_LOCK_ID, sessionId);
                  logger.debug("Release lockResult: " + lockResult);
              }
          } catch (Exception e) {
              logger.error("Could not release lock: " + e);
          }
      }
  }

    /**
    * Performs the lifecycle update for the passed in OrderStatus
    *
    * @param OrderStatus The Order Status update to perform
    */
    public void processLifecycleUpdate(OrderStatus os) throws Throwable {

    	Pattern EROS_ID_MATCHER =  Pattern.compile("\\A[A-Z]\\d{4}[A-Z]");
    	
    	try {
          MercuryDAO dao = new MercuryDAO(conn);
          OrderDAO orderDao = new OrderDAO(conn);
          logger.info(os.getStatusId() + " " + os.getStatusCode() + " " + os.getEfosNumber() + " " + os.getStatusText() + " " + os.getStatusDate());
          if (os.getEfosNumber() != null) {
          	  Matcher matcher = EROS_ID_MATCHER.matcher(os.getEfosNumber());
              if( matcher.matches() ) {
                  logger.debug("Inserting lifecycle record");
                  Map result = dao.insertLifecycleStatus(os);
                  String mercuryId =  (String) result.get("OUT_MERCURY_ID");
                  String orderDetailId =  (String) result.get("OUT_ORDER_DETAIL_ID");
                  String dconStatus = (String) result.get("OUT_DCON_STATUS");
                  logger.debug("Results: " + mercuryId + " " + orderDetailId + " " + dconStatus);
                  if (os.getStatusCode().equalsIgnoreCase(OrderConstants.LIFECYCLE_CONFIRMED) &&
                		  orderDetailId != null &&
                	      (dconStatus == null || !dconStatus.equalsIgnoreCase(GeneralConstants.DCON_CONFIRMED))) {
                	  try {
                		Long orderDetailIdL = Long.parseLong(orderDetailId);
                		
                		String deliveryTimestampStr = os.getStatusDate();                		
                		SimpleDateFormat sdf = new SimpleDateFormat(DCON_LIFE_CYCLE_UPDATE_DATETIME_TYPE);
                		Date deliveryTimestamp = sdf.parse(deliveryTimestampStr);
                		
                      	PartnerDCONService.getInstance().persistPartnerDconData(orderDetailIdL, DELIVERY_CONFIRMATION_STATUS, deliveryTimestamp, conn);
                      } catch(Exception e) {
                    	String errorMessage = "Problem occured while persisting partner delivery confirmation data. Order detail id: " + orderDetailId +" Error: " + e;
                      	logger.error(errorMessage, e);
                      	CommonUtils.sendNoPageSystemMessage(errorMessage, "PARTNER_INTEGRATION_NOPAGE", "NOPAGE Partner Integration Message");                      	
                      }
                	  OrderDetailVO odVO = orderDao.getOrderDetail(orderDetailId);
                	  boolean success = checkDeliveryConfMailControl(odVO.getDeliveryDate());
                	  if(success){
                		  success = sendDeliveryConfirmationEmail(orderDetailId);
                	  }
                	  
                      logger.info("success: " + success);
                  }
                  logger.debug("Confirming " + os.getStatusId());
                  OrderLifecycleUtilities.ConfirmOrderStatus(os.getStatusId());
              }
          }
      } catch (OrderLifecycleException ole) {
          logger.error("ole exception: " + ole.getMessage());
          throw ole;
      } catch (Throwable t) {
          throw t;
      } 
    }
  
    /**
    * Builds a "request confirmation" message as an ASK message and sends
    * it to a florist.
    *
    * @param ftdMessage
    */
    private void generateConfirmationAskMessage(MercuryVO ftdMessage) throws Exception {

        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String confMessage = null; 

        PartnerVO partnerMasterVO = FTDCommonUtils.getPreferredPartnerByOrderDetailId(ftdMessage.getReferenceNumber(), this.conn);
        String preferredPartner = null;

        if (partnerMasterVO != null) 
          preferredPartner = partnerMasterVO.getPartnerName();

        logger.debug("preferredPartner = " + preferredPartner);
        if(preferredPartner!=null && !preferredPartner.equalsIgnoreCase("")) 
          confMessage = config.getContentWithFilter(this.conn, CONTENT_CONTEXT_DELIVERY_CONFIRMATION, CONTENT_NAME_DCON_ASK_MESSAGE_PARTNER, partnerMasterVO.getPartnerName(), null);
        else
          confMessage = config.getContentWithFilter(this.conn, CONTENT_CONTEXT_DELIVERY_CONFIRMATION, CONTENT_NAME_DCON_ASK_MESSAGE, null, null);

        //this is a catch all for confMessage - if empty, use the default text from the config file
        if (confMessage == null || confMessage.equalsIgnoreCase(""))
          confMessage = config.getProperty(CONFIG_FILE, "CONFIRMATION_ASK");

        // build ASK message
        ASKMessageTO outputTO = new ASKMessageTO();
        outputTO.setFillingFlorist(ftdMessage.getFillingFlorist());
        outputTO.setMercuryId(ftdMessage.getMercuryId());
        outputTO.setOperator(ASK_MSG_OPERATOR);
        outputTO.setComments(confMessage);
        outputTO.setMercuryStatus(ASK_MSG_STATUS);
        outputTO.setDirection(ASK_MSG_DIRECTION);
        new MercuryFacade(conn).sendASKMessage(outputTO);
    }

    public boolean sendDeliveryConfirmationEmail(String orderDetailId) throws Exception {        
        boolean success = false;
        DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            logger.debug("orderDetailId: " + orderDetailId);
            logger.info("Calling sendDeliveryConfirmationEmail");
            OrderDAO orderDao = new OrderDAO(conn);
            OrderDetailVO odVO = orderDao.getOrderDetail(orderDetailId);
            OrderVO orderVO = orderDao.getOrder(odVO.getOrderGuid());
            
            boolean sendEmail = true;
            
            // If partner order check for the flag send_delivery_conf_email
            String orderConfNumber = odVO.getExternalOrderNumber(); // default to FTD order number
            PartnerMappingVO partnerMappingVO = getPartnerMapping(orderVO.getOriginId(),orderVO.getSourceCode());
            if(partnerMappingVO != null && orderVO.getOriginId().equalsIgnoreCase(partnerMappingVO.getFtdOriginMapping())) {               
                if("Y".equalsIgnoreCase(partnerMappingVO.getSendDelConfEmail())) {
                	orderConfNumber = getPartnerOrderNumber(orderConfNumber, orderVO.getOriginId());
                	if(orderConfNumber == null) {
                		logger.info("Unable to get the partner order number, not sending the delivery confirmation email though flag is set to Y.");
                		sendEmail = false;
                	}
                } else {
                	logger.info("Is partner order - send_delivery_conf_email is, " + partnerMappingVO.getSendDelConfEmail());
                	sendEmail = false;
                }            	
            }  
            
            if(sendEmail) {
            	logger.info("Sending an Delivery confirmation email for the order: " + orderConfNumber);
            	sendMail(orderDetailId, orderDao, odVO, orderVO, orderConfNumber);
            }
            
        } catch (Exception e) {
        	logger.error("Error caught in sendDeliveryConfirmationEmail(..), ", e);
            throw e;
        }        
        return success;
    }

	/** This method should return valid partner order number,
	 * any problem caught retrieving partner order number should not send an email, 
	 * rather log a system message.
	 * @param orderConfNumber
	 * @param orderOrigin
	 * @return
	 */
	private String getPartnerOrderNumber(String orderConfNumber, String orderOrigin) throws Exception {
		try {			
			CachedResultSet result = new PartnerUtility().getPtnOrderDetailByConfNumber(orderConfNumber, orderOrigin, conn);
			if (result != null && result.next()) {
				if (!StringUtils.isEmpty(result.getString(PARTNER_ORDER_ITEM_NUMBER))) {
					return result.getString(PARTNER_ORDER_ITEM_NUMBER);
				}
			}
		} catch(Exception e) {
    		logger.error("Error caught in getPartnerOrderNumber(), unable to get the partner order number: ", e);    		
    		throw e;
    	}
		return null;
	}

	/** Return partner mapping info for a given order origin - (partner origin)
	 * @param orderOrigin
	 * @return
	 */
	private PartnerMappingVO getPartnerMapping(String orderOrigin, String sourceCode) throws Exception {		
		// return FTD external order number, when order origin is invalid
		try {
			if(!StringUtils.isEmpty(orderOrigin)) {
				logger.info("Getting the partner mapping info for origin - " + orderOrigin);
				return new PartnerUtility().getPartnerOriginsInfo(orderOrigin,sourceCode, conn);
			}			
		} catch(Exception e) {
    		logger.error("Error caught in getPartnerSendDCONMapping(), unable to determine if it is partner order : ", e);    		
    		throw e;
    	}
		return null;
	}

	private void sendMail(String orderDetailId, OrderDAO orderDao, OrderDetailVO odVO, OrderVO orderVO, String orderConfNumber) throws Exception, IOException,
			SAXException, ParserConfigurationException, JAXPException {
		
    		CustomerVO buyerVO = orderDao.getCustomer(orderVO.getCustomerId());
    		CustomerVO recipVO = orderDao.getCustomer(odVO.getRecipientId());
    		EmailVO emailVO = orderDao.getCustomerEmailInfo(orderVO.getCustomerId(), orderVO.getCompanyId());
		
			// Only process if buyer has an email address and we have not already sent delivery confirmation
			if (emailVO != null && emailVO.getEmailAddress() != null && !GeneralConstants.DCON_CONFIRMED.equals(odVO.getDeliveryConfirmationStatus())) {
				
			    StockMessageGenerator messageGenerator = new StockMessageGenerator(conn);
			    GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
	
			    PartnerVO pVO = FTDCommonUtils.getPreferredPartnerBySource(odVO.getSourceCode());
			    String partnerName = null;
			    if (pVO != null && pVO.getPartnerName() != null) {
			        partnerName = pVO.getPartnerName();
			    }
			    ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
			    String signature = configurationUtil.getContentWithFilter(conn, CONTENT_CONTEXT_PREFERRED_PARTNER, CONTENT_NAME_DCON_SIGNATURE, partnerName, null);
			    if (signature == null) signature = "";
	
			    //create a point of contact VO
			    PointOfContactVO pocVO = new PointOfContactVO();
			    pocVO.setOrderDetailId(odVO.getOrderDetailId());
			    pocVO.setExternalOrderNumber(orderConfNumber);
			    pocVO.setOrderGuid(odVO.getOrderGuid());
			    pocVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
			    pocVO.setCompanyId(orderVO.getCompanyId());
			    pocVO.setCustomerId(buyerVO.getCustomerId());
	
			    // use company_id to pull SenderEmailAddress from db
	
			    String companyId = orderVO.getCompanyId();
			    logger.debug("Company For Email: " + companyId);
			    if (companyId == null || companyId.equals("")) {
			        companyId = "FTD";
			    }
			    CompanyVO companyVO = orderDao.getCompany(companyId);
			    String url = companyVO.getURL();
			    if (url == null || url.equals("")) {
			        url = "ftd.com";
			    }
			    String companyEmailAddress = "confirmation@" + url;
			    logger.debug("Sender email address: " + companyEmailAddress);
			    pocVO.setSenderEmailAddress(companyEmailAddress);
	
	
	
			    pocVO.setTemplateId(null);
			    pocVO.setPointOfContactType("Email");
			    pocVO.setCommentType("Order");
			    pocVO.setRecipientEmailAddress(emailVO.getEmailAddress());
			    pocVO.setFirstName(buyerVO.getFirstName());
			    pocVO.setLastName(buyerVO.getLastName());
	//		    if (globalParamHandler != null) {
	//		      pocVO.setMailserverCode(globalParamHandler.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_KEY)); 
	//		    }
	
			    //create poc xml
			    Document xml = JAXPUtil.createDocument();
			    Element root = xml.createElement("root");
			    xml.appendChild(root);
			    root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"external_order_number",pocVO.getExternalOrderNumber()));
			    Element csr = xml.createElement("CSR");
			    root.appendChild(csr);
			    Element csrName = xml.createElement("first_name");
			    csr.appendChild(csrName);
			    csrName.appendChild(xml.createTextNode(signature));
			    csrName = xml.createElement("last_name");
			    csr.appendChild(csrName);
			    csrName.appendChild(xml.createTextNode(""));
	
			    Element customersNode = xml.createElement("CUSTOMERS");
			    root.appendChild(customersNode);
			    Element customerNode = xml.createElement("CUSTOMER");
			    customersNode.appendChild(customerNode);
			    Element value = xml.createElement("first_name");
			    customerNode.appendChild(value);
			    value.appendChild(xml.createTextNode(buyerVO.getFirstName()));
			    value = xml.createElement("last_name");
			    customerNode.appendChild(value);
			    value.appendChild(xml.createTextNode(buyerVO.getLastName()));
	
			    Element ordersNode = xml.createElement("ORDERS");
			    root.appendChild(ordersNode);
			    Element orderNode = xml.createElement("ORDER");
			    ordersNode.appendChild(orderNode);
			    value = xml.createElement("first_name");
			    orderNode.appendChild(value);
			    value.appendChild(xml.createTextNode(recipVO.getFirstName()));
			    value = xml.createElement("last_name");
			    orderNode.appendChild(value);
			    value.appendChild(xml.createTextNode(recipVO.getLastName()));
			    value = xml.createElement("language_id");
			    orderNode.appendChild(value);
			    value.appendChild(xml.createTextNode(orderVO.getLanguageId()));
	
			    pocVO.setDataDocument((Document)xml);
			    logger.debug(JAXPUtil.toString(xml));
	
			    String emailTitle = "";
			    if(globalParamHandler != null) {
			        String emailName = OrderConstants.DCON_EMAIL_TITLE;
			        if (partnerName != null) {
			            emailName = partnerName + "_" + emailName;
			        }
			        emailTitle = globalParamHandler.getFrpGlobalParm(OrderConstants.PREFERRED_PARTNER_CONTEXT, emailName);
			        logger.debug("emailName: " + emailName);
			        logger.debug("emailTitle: " + emailTitle);
			    }
	
			    pocVO.setLetterTitle(emailTitle); 
			    pocVO.setEmailSubject("Delivery Confirmation");
			    
			    pocVO.setSourceCode(odVO.getSourceCode());
			    pocVO.setEmailType(EMAIL_TYPE_DCON);
		        if (odVO.getOrderGuid() != null) {
		        	pocVO.setRecordAttributesXML(messageGenerator.generateRecordAttributeXMLFromOrderGuid(odVO.getOrderGuid()));  
		        }
		        else {
		      	  logger.error("Unable to create XML attributes string for orderGuid="+orderVO.getOrderGuid());
		        }
			    
	
			    logger.debug("Email being sent to " + emailVO.getEmailAddress());
			    messageGenerator.processMessage(pocVO);
	
			    logger.debug("Deleting DI/DC queue messages");
			    long orderDetailNum = Long.parseLong(orderDetailId);
			    QueueDAO queueDao = new QueueDAO(conn);
			    queueDao.deleteUntaggedQueueMsgs(orderDetailNum, DCON_DC_QUEUE, "DCON-ASK/ANS");
			    queueDao.deleteUntaggedQueueMsgs(orderDetailNum, DCON_DI_QUEUE, "DCON-ASK/ANS");
	
			    logger.debug("Updating status");
			    orderDao.updateDeliveryConfirmationStatus(orderDetailNum, GeneralConstants.DCON_CONFIRMED, "DCON-ASK/ANS");
	
			    String comment = "Delivery confirmed by florist via Mercury. Automatic Delivery Confirmation email (" + emailTitle + ") sent to " + pocVO.getRecipientEmailAddress();
			    logger.debug("Inserting comment: " + comment);
			    CommentsVO commentsVO = new CommentsVO();
			    commentsVO.setComment(comment);
			    commentsVO.setCommentOrigin(MercuryConstants.OPERATOR_OP);
			    commentsVO.setCommentType(MercuryConstants.COMMENT_TYPE_ORDER);
			    commentsVO.setCreatedBy(MercuryConstants.OPERATOR_OP);
			    commentsVO.setCustomerId(Long.toString(odVO.getRecipientId()));
			    commentsVO.setOrderDetailId(Long.toString(odVO.getOrderDetailId()));
			    commentsVO.setOrderGuid(odVO.getOrderGuid());
			    orderDao.insertComment(commentsVO);
			} 
			else if ((emailVO == null || emailVO.getEmailAddress() == null) && !GeneralConstants.DCON_CONFIRMED.equals(odVO.getDeliveryConfirmationStatus())) 
			{
				/*
				 * DCE-8 : DCON Ask Messages
				 * If buyer email ID is not available and received a Delivery Confirmation response from florist, then
				 * 		1.	Remove the order from DC queue, if tagged already
				 *		2.	Remove the order from DI queue, if tagged already
				 *		3.	Update the delivery confirmation status of the order to “Confirmed”
				 * 		4.	Insert a comment "Delivery has been confirmed by the filling florist, no email on file for customer."  
				 */
				logger.debug("Buyer email ID not available. OrderDetailId:"+orderDetailId);
			    long orderDetailNum = Long.parseLong(orderDetailId);
			    QueueDAO queueDao = new QueueDAO(conn);
			    queueDao.deleteUntaggedQueueMsgs(orderDetailNum, DCON_DC_QUEUE, "DCON-ASK/ANS");
			    queueDao.deleteUntaggedQueueMsgs(orderDetailNum, DCON_DI_QUEUE, "DCON-ASK/ANS");
	
			    logger.debug("Updating order detail status to "+GeneralConstants.DCON_CONFIRMED);
			    orderDao.updateDeliveryConfirmationStatus(orderDetailNum, GeneralConstants.DCON_CONFIRMED, "DCON-ASK/ANS");
	
			    String comment = "Delivery has been confirmed by the filling florist, no email on file for customer.";
			    logger.debug("Inserting comment: " + comment);
			    CommentsVO commentsVO = new CommentsVO();
			    commentsVO.setComment(comment);
			    commentsVO.setCommentOrigin(MercuryConstants.OPERATOR_OP);
			    commentsVO.setCommentType(MercuryConstants.COMMENT_TYPE_ORDER);
			    commentsVO.setCreatedBy(MercuryConstants.OPERATOR_OP);
			    commentsVO.setCustomerId(Long.toString(odVO.getRecipientId()));
			    commentsVO.setOrderDetailId(Long.toString(odVO.getOrderDetailId()));
			    commentsVO.setOrderGuid(odVO.getOrderGuid());
			    orderDao.insertComment(commentsVO);
			}
			else {
			    if (GeneralConstants.DCON_CONFIRMED.equals(odVO.getDeliveryConfirmationStatus())) {
			        logger.debug("Skipping, delivery confirmation email already sent");
			    } else {
			        logger.debug("Skipping, buyer does not have an email address on file");
			    }
			}
		}
	
    /**
    * Performs the lifecycle update for the passed in Status ID
    *
    * @param int statusID The Status ID to confirm in MNAPI
    */
    public void confirmStatusId(int statusId) throws Throwable {
    	
    	try {
    	     logger.debug("Confirming " + statusId);
             OrderLifecycleUtilities.ConfirmOrderStatus(statusId);
      } catch (OrderLifecycleException ole) {
          logger.error("ole exception: " + ole.getMessage());
          throw ole;
      } catch (Throwable t) {
          throw t;
      } 
    }
    public boolean checkDeliveryConfMailControl(Date deliveryDate)throws Exception
    {
        boolean sendEmail = false;
        String dconDays = "5";
        try{
        	dconDays = ConfigurationUtil.getInstance().getFrpGlobalParm(OrderConstants.OP,OrderConstants.DELIVERY_CONFIRMATION_CONTROL_DAYS);
        }catch(Exception ex){
        	logger.error("Error while getting value for DELIVERY_CONFIRMATION_CONTROL_DAYS paramter.And setting Default value to 5");
        }
        Integer ctrlDays = Integer.parseInt(dconDays);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar deliveryDt = Calendar.getInstance();
        deliveryDt.setTime(deliveryDate);
		
		Calendar dateCtrl =Calendar.getInstance();
		dateCtrl.set(Calendar.DATE, dateCtrl.get(Calendar.DATE) - ctrlDays );
		logger.info("Dates :"+deliveryDt.getTime()+" "+dateCtrl.getTime());
		long diffDays = DeliveryDateUTIL.getDateDiff(dateCtrl.getTime(),deliveryDt.getTime());
        if(diffDays >= 0){
        	sendEmail = true;
        }
        logger.info("Diff Days for sending Mail is :"+diffDays+ " "+sendEmail);
        return sendEmail;
    }
    
}
