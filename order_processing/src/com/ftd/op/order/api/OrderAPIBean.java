package com.ftd.op.order.api;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.CreditCardService;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.order.bo.OrderAPIBO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.facade.OrderFacade;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import com.ftd.osp.utilities.plugins.Logger;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import javax.sql.DataSource;
import com.ftd.op.order.to.CreditCardTO;

public class OrderAPIBean implements SessionBean 
{
  Logger logger;
  private static final String LOGGER_CONTEXT = "com.ftd.op.order.api.OrderAPIBean";
  private DataSource db;
  private OrderAPIBO orderAPIBO;
  
  public void ejbCreate() 
  {
  }

  public void ejbActivate()
  {
  }

  public void ejbPassivate()
  {
  }

  public void ejbRemove()
  {
  }

  public void setSessionContext(SessionContext ctx)
  {
    try {
      logger = new Logger(LOGGER_CONTEXT);
      db = CommonUtils.getDataSource();
      orderAPIBO = new OrderAPIBO();
    } catch (Exception e) 
    {
      logger.error(e);
    }
  }
  /**
   * 
   * 
   * @param OrderTO 
   * @return RWDFloristVO 
   * @throws java.lang.Exception
   */
  public RWDFloristTO getFillingFlorist(OrderTO order)  {

    logger.debug("IN getFillingFlorist");
    logger.debug("Order TO has: " + order.getOrderDetailId());
    RWDFloristTO florist = null;
    Connection conn = null;
    try 
    {
      conn = db.getConnection();      
      
      return orderAPIBO.getFillingFlorist(order, conn);
    
    } catch (Exception e) 
    {
        logger.error(e);;
    } finally 
    {
        try {
          if (conn != null) conn.close();
        } catch (Exception e) 
        {
          logger.error(e);
        }
    }
    return florist;
  }
  
    /**
     * 
     * 
     * @param RWDTO
     * @return RWDFloristVO 
     * @throws java.lang.Exception
     */
    public RWDFloristTO getFillingFlorist(RWDTO rwd)  {

      logger.debug("getFillingFlorist(RWDTO rwd) order detail id: " + rwd.getOrderDetailId());
      RWDFloristTO florist = null;
      Connection conn = null;
      try {
        conn = db.getConnection();      
        return orderAPIBO.getFillingFlorist(rwd, conn);
        } catch (Exception e) {
          logger.error(e);;
        } finally 
        {
          try {
            if (conn != null) conn.close();
          } catch (Exception e) 
          {
            logger.error(e);
          }
        }
      return florist;
    }
    
  /**
   * 
   * 
   * @param OrderTO
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristTOList getFullFloristList(OrderTO order)
  {
    Connection conn = null;
    try {
      conn = db.getConnection();
      return orderAPIBO.getFullFloristList(order, conn);
    } catch (Exception e) {    
      logger.error(e);
    } finally 
    {
      try
      {
        if (conn != null) conn.close();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
    return null;
  }
  
  /**
   * 
   * 
   * @param OrderTO
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristTOList getTopFloristList(OrderTO order) 
  {
    Connection conn = null;
    try {
      conn = db.getConnection();
      return orderAPIBO.getTopFloristList(order, conn);
    } catch (Exception e) {
        logger.error(e);
    } finally 
    {
      try
      {
        if (conn != null) conn.close();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
    return null;
  } 

  public String helloWorld(String inString) 
  {
    return orderAPIBO.helloWorld(inString);
  }
  
  public CreditCardTO validateCreditCard(CreditCardTO card)
  {
    Connection conn = null;
    try {
      conn = db.getConnection();
      return orderAPIBO.validateCreditCard(card, conn);
    } catch (Exception e) {
        logger.error(e);
    } finally 
    {
      try
      {
        if (conn != null) conn.close();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
    return null;
  }

  public void processOrder(OrderTO order)
  {
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      orderAPIBO.processOrder(order, conn);
    } catch (Exception e) 
    {
      logger.error(e);
    }
    finally 
    {
      try
      {
        if (conn != null) conn.close();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
  }

  public RWDFloristTOList getFillingFloristList(RWDTO rwd)
  {
    Connection conn = null;
    try {
      conn = db.getConnection();
      return orderAPIBO.getFillingFloristList(rwd, conn);
    } catch (Exception e) {
        logger.error(e);
    } finally 
    {
      try
      {
        if (conn != null) conn.close();
      }
      catch (Exception e)
      {
        logger.error(e);
      }
    }
    return null;
  } 
}
