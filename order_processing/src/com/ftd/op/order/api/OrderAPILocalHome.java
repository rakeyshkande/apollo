package com.ftd.op.order.api;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface OrderAPILocalHome extends EJBLocalHome 
{
  OrderAPILocal create() throws CreateException;
}