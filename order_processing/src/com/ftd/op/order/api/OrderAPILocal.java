package com.ftd.op.order.api;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import java.rmi.RemoteException;
import javax.ejb.EJBLocalObject;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import com.ftd.op.order.to.CreditCardTO;

public interface OrderAPILocal extends EJBLocalObject 
{
  RWDFloristTOList getFullFloristList(OrderTO order);

  RWDFloristTOList getTopFloristList(OrderTO order);

  RWDFloristTO getFillingFlorist(OrderTO order);
  
  RWDFloristTO getFillingFlorist(RWDTO rwd);
  
  String helloWorld(String inString);
  
  CreditCardTO validateCreditCard(CreditCardTO inCreditCard);

  void processOrder(OrderTO order);

  RWDFloristTOList getFillingFloristList(RWDTO rwd);
}