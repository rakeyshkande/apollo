package com.ftd.op.order.api;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface OrderAPIHome extends EJBHome 
{
  OrderAPI create() throws RemoteException, CreateException;
}