package com.ftd.op.order.api;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import com.ftd.op.order.to.CreditCardTO;

public interface OrderAPI extends EJBObject 
{
  RWDFloristTOList getFullFloristList(OrderTO order) throws RemoteException;

  RWDFloristTOList getTopFloristList(OrderTO order) throws RemoteException;

  RWDFloristTO getFillingFlorist(OrderTO order) throws RemoteException;
  
  RWDFloristTO getFillingFlorist(RWDTO rwd) throws RemoteException;

  String helloWorld(String inString) throws RemoteException;
  
  CreditCardTO validateCreditCard(CreditCardTO inCreditCard) throws RemoteException;

  void processOrder(OrderTO order) throws RemoteException;

  RWDFloristTOList getFillingFloristList(RWDTO rwd) throws RemoteException;
} 
