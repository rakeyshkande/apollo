package com.ftd.op.order.dao;


import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.legacy.LegacySourceCode;
import com.ftd.op.common.vo.QueueVO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.AddOnVO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerPhoneVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.EmailVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.GnaddVO;
import com.ftd.op.order.vo.OrderBillVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderHoldVO;
import com.ftd.op.order.vo.OrderTrackingVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.order.vo.ProductSubCodeVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import com.ftd.op.order.vo.SecondChoiceVO;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.op.order.vo.VendorBlockVO;
import com.ftd.op.order.vo.VendorProductVO;
import com.ftd.op.order.vo.VendorVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pg.client.creditcard.authorize.request.BillTo;


/**
 * OrderDAO
 *
 * This is the DAO that handles inserting florist information, updating
 * order information and retrieving information needed for order processing.
 *
 * @author Nicole Roberts
 */
public class

OrderDAO {
    private OrderConstants orderConstants;
    private Connection conn;
    private Logger logger;

    /**
     * Constructor
     *
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
    public OrderDAO(Connection conn) {
        this.conn = conn;
        logger = new Logger(orderConstants.LOGGER_CATEGORY_ORDERDAO);
    }

  /**
   * Defect 3171
   * This is an overloaded method which will pass the default SP id.
   * It updates an order detail record to match the passed in VO.
   *
   * @param OrderDetailVO
   * @return none
   */
    public void updateOrder(OrderDetailVO orderDtl) throws Exception
    {
      //call updateOrder with default update_order_details statement id.
      updateOrder(orderDtl, orderConstants.SP_UPDATE_ORDER_DETAILS);
    }


    /**
   * This method is a wrapper for the SP_UPDATE_ORDER_DETAIL SP.
   * It updates an order detail record to match the passed in VO.
   *
   * @param OrderDetailVO
   * @param String - statementId
   * @return none
   */
    public void updateOrder(OrderDetailVO orderDtl, String statementId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.info("updateOrder (OrderDetailVO orderDtl (" +
                     orderDtl.getOrderDetailId() + ")) :: void" + orderDtl);
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.ORDER_DETAIL_ID,
                        new Long(orderDtl.getOrderDetailId()));
        inputParams.put(orderConstants.DELIVERY_DATE,
                        orderDtl.getDeliveryDate() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
        inputParams.put(orderConstants.RECIPIENT_ID,
                        new Long(orderDtl.getRecipientId()));
        inputParams.put(orderConstants.PRODUCT_ID, orderDtl.getProductId());
        inputParams.put(orderConstants.QUANTITY,
                        new Long(orderDtl.getQuantity()));
        inputParams.put(orderConstants.COLOR_1, orderDtl.getColor1());
        inputParams.put(orderConstants.COLOR_2, orderDtl.getColor2());
        inputParams.put(orderConstants.SUBSTITUTION_INDICATOR,
                        orderDtl.getSubstitutionIndicator());
        inputParams.put(orderConstants.SAME_DAY_GIFT,
                        orderDtl.getSameDayGift());
        inputParams.put(orderConstants.OCCASION, orderDtl.getOccasion());
        inputParams.put(orderConstants.CARD_MESSAGE,
                        orderDtl.getCardMessage());
        inputParams.put(orderConstants.CARD_SIGNATURE,
                        orderDtl.getCardSignature());
        inputParams.put(orderConstants.SPECIAL_INSTRUCTIONS,
                        orderDtl.getSpecialInstructions());
        inputParams.put(orderConstants.RELEASE_INFO_INDICATOR,
                        orderDtl.getReleaseInfoIndicator());
        inputParams.put(orderConstants.FLORIST_ID, orderDtl.getFloristId());
        inputParams.put(orderConstants.SHIP_METHOD, orderDtl.getShipMethod());
        inputParams.put(orderConstants.SHIP_DATE,
                        orderDtl.getShipDate() == null ? null :
                        new java.sql.Date(orderDtl.getShipDate().getTime()));
        inputParams.put(orderConstants.ORDER_DISP_CODE,
                        orderDtl.getOrderDispCode());
        inputParams.put(orderConstants.SECOND_CHOICE_PRODUCT,
                        orderDtl.getSecondChoiceProduct());
        inputParams.put(orderConstants.ZIP_QUEUE_COUNT,
                        new Long(orderDtl.getZipQueueCount()));
        inputParams.put("REJECT_RETRY_COUNT",
                        orderDtl.getRejectRetryCount() + "");
        inputParams.put(orderConstants.UPDATED_BY,
                        orderConstants.UPDATED_BY_VL);
        inputParams.put(orderConstants.DELIVERY_DATE_RANGE_END,
                        orderDtl.getDeliveryDateRangeEnd() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
        inputParams.put(orderConstants.SCRUBBED_ON,
                        orderDtl.getScrubbedOn() == null ? null :
                        new java.sql.Timestamp(orderDtl.getScrubbedOn().getTime()));
        inputParams.put(orderConstants.SCRUBBED_BY, orderDtl.getScrubbedBy());
        inputParams.put(orderConstants.ARIBA_UNSPSC_CODE,
                        orderDtl.getAribaUnspscCode());
        inputParams.put(orderConstants.ARIBA_PO_NUMBER,
                        orderDtl.getAribaPoNumber());
        inputParams.put(orderConstants.ARIBA_AMS_PROJECT_CODE,
                        orderDtl.getAribaAmsProjectCode());
        inputParams.put(orderConstants.ARIBA_COST_CENTER,
                        orderDtl.getAribaCostCenter());
        inputParams.put(orderConstants.SIZE_INDICATOR,
                        orderDtl.getSizeIndicator());
        inputParams.put(orderConstants.MILES_POINTS,
                        null); // never updated by OP
        inputParams.put(orderConstants.SUBCODE, orderDtl.getSubcode());
        inputParams.put(orderConstants.SOURCE_CODE, orderDtl.getSourceCode());
        inputParams.put(orderConstants.OP_STATUS, orderDtl.getOpStatus());
        inputParams.put(orderConstants.VENDOR_ID, orderDtl.getVendorId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(statementId);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the SP_UPDATE_ORDER_DETAIL SP.
   * It updates an order detail record to match the passed in VO.
   *
   * This method varies from updateOrder in that numeric values in the
   * VO are treated as null.  Also this uses a different statement
   * because the one used in updateOrder will not work when you if any
   * of the numbers are null.
   *
   * Note: Passing a null to the stored proc will prevent the stored proc
   * from updating the field.
   *
   * @param OrderDetailVO
   * @return none
   */
    public void updateOrderDetail(OrderDetailVO orderDtl) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.info("updateOrder (OrderDetailVO orderDtl (" +
                     orderDtl.getOrderDetailId() + ")) :: void");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.ORDER_DETAIL_ID,
                        getLong(orderDtl.getOrderDetailId()));
        inputParams.put(orderConstants.DELIVERY_DATE,
                        orderDtl.getDeliveryDate() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
        inputParams.put(orderConstants.RECIPIENT_ID,
                        getLong(orderDtl.getRecipientId()));
        inputParams.put(orderConstants.PRODUCT_ID, orderDtl.getProductId());
        inputParams.put(orderConstants.QUANTITY,
                        getLong(orderDtl.getQuantity()));
        inputParams.put(orderConstants.COLOR_1, orderDtl.getColor1());
        inputParams.put(orderConstants.COLOR_2, orderDtl.getColor2());
        inputParams.put(orderConstants.SUBSTITUTION_INDICATOR,
                        orderDtl.getSubstitutionIndicator());
        inputParams.put(orderConstants.SAME_DAY_GIFT,
                        orderDtl.getSameDayGift());
        inputParams.put(orderConstants.OCCASION, orderDtl.getOccasion());
        inputParams.put(orderConstants.CARD_MESSAGE,
                        orderDtl.getCardMessage());
        inputParams.put(orderConstants.CARD_SIGNATURE,
                        orderDtl.getCardSignature());
        inputParams.put(orderConstants.SPECIAL_INSTRUCTIONS,
                        orderDtl.getSpecialInstructions());
        inputParams.put(orderConstants.RELEASE_INFO_INDICATOR,
                        orderDtl.getReleaseInfoIndicator());
        inputParams.put(orderConstants.FLORIST_ID, orderDtl.getFloristId());
        inputParams.put(orderConstants.SHIP_METHOD, orderDtl.getShipMethod());
        inputParams.put(orderConstants.SHIP_DATE,
                        orderDtl.getShipDate() == null ? null :
                        new java.sql.Date(orderDtl.getShipDate().getTime()));
        inputParams.put(orderConstants.ORDER_DISP_CODE,
                        orderDtl.getOrderDispCode());
        inputParams.put(orderConstants.SECOND_CHOICE_PRODUCT,
                        orderDtl.getSecondChoiceProduct());
        inputParams.put(orderConstants.ZIP_QUEUE_COUNT,
                        getLong(orderDtl.getZipQueueCount()));
        inputParams.put("REJECT_RETRY_COUNT",
                        new Long(orderDtl.getRejectRetryCount()));
        inputParams.put(orderConstants.UPDATED_BY,
                        orderConstants.UPDATED_BY_VL);
        inputParams.put(orderConstants.DELIVERY_DATE_RANGE_END,
                        orderDtl.getDeliveryDateRangeEnd() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
        inputParams.put(orderConstants.SCRUBBED_ON, orderDtl.getScrubbedOn());
        inputParams.put(orderConstants.SCRUBBED_BY, orderDtl.getScrubbedBy());
        inputParams.put(orderConstants.ARIBA_UNSPSC_CODE,
                        orderDtl.getAribaUnspscCode());
        inputParams.put(orderConstants.ARIBA_PO_NUMBER,
                        orderDtl.getAribaPoNumber());
        inputParams.put(orderConstants.ARIBA_AMS_PROJECT_CODE,
                        orderDtl.getAribaAmsProjectCode());
        inputParams.put(orderConstants.ARIBA_COST_CENTER,
                        orderDtl.getAribaCostCenter());
        inputParams.put(orderConstants.SIZE_INDICATOR,
                        orderDtl.getSizeIndicator());
        inputParams.put(orderConstants.MILES_POINTS,
                        getLong(orderDtl.getMilesPoints()));
        inputParams.put(orderConstants.SUBCODE, orderDtl.getSubcode());
        inputParams.put("OP_STATUS", orderDtl.getOpStatus());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_ORDER_DETAILS_STRINGS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /*
   * Get the Long representation of a long.  If numeric value is zero
   * then return null.
   */

    private String getLong(long longValue) {
        return longValue == 0 ? null : Long.toString(longValue);
    }

    /*
   * Get the Long representation of a long.  If numeric value is zero
   * then return null.
   */

    private Double getDouble(Object obj) {
        return obj == null ? null : new Double(obj.toString());
    }


    /**
   * This method is a wrapper for the SP_IS_CUSTOMER_ON_HOLD SP,
   * returning true if the SP returns �Y� and false otherwise.
   *
   * @param long - customerId
   * @return boolean
   */
    public String getCustomerHold(long customerId,
                                  String lpIndicator) throws Exception {

        DataRequest dataRequest = new DataRequest();
        boolean result = false;

        logger.info("isCustomerOnHold (long customerId (" + customerId +
                     "), String lpIndicator) :: boolean");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.CUSTOMER_ID, new Long(customerId));
        inputParams.put(orderConstants.LOSS_PREVENTION_INDICATOR, lpIndicator);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_IS_CUSTOMER_ON_ORDER_HOLD);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String outputs = (String)dataAccessUtil.execute(dataRequest);
        return outputs;
    }

    /**
   * This method is a wrapper for the SP_IS_CUSTOMER_ON_HOLD SP,
   * returning true if the SP returns �Y� and false otherwise.
   *
   * @param long - customerId
   * @return boolean
   */
    public boolean isCustomerOnHold(long customerId,
                                    String lpIndicator) throws Exception {

        DataRequest dataRequest = new DataRequest();
        boolean result = false;

        logger.info("isCustomerOnHold (long customerId (" + customerId +
                     "), String lpIndicator) :: boolean");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.CUSTOMER_ID, new Long(customerId));
        inputParams.put(orderConstants.LOSS_PREVENTION_INDICATOR, lpIndicator);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_IS_CUSTOMER_ON_ORDER_HOLD);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String outputs = (String)dataAccessUtil.execute(dataRequest);
        result = outputs.equalsIgnoreCase(orderConstants.VL_NO) ? false : true;
        return result;
    }

    /**
    * This method is a wrapper for the VALIDATE_SHIPPING_DATES,
    * returning true if the SP returns �Y� and false otherwise.
    *
    * @param product or subcode of the product being validated
    * @param shipDate being validated
    * @param deliveryDate being validated
    * @return boolean
    */

// Arvind Rajoo -- 7/30/2010: This method is unreferenced and the Statement ID is not defined in any of the referenced statements
//    public boolean validateShippingDates(String productSubcodeId, java.util.Date shipDate, java.util.Date deliveryDate) throws Exception {
//
//        DataRequest dataRequest = new DataRequest();
//        boolean result = false;
//
//        logger.info("validateShippingDates (String productSubcodeId (" + productSubcodeId +
//                     ")) :: boolean");
//        /* setup store procedure input parameters */
//        HashMap inputParams = new HashMap();
//        inputParams.put("PRODUCT_SUBCODE_ID", productSubcodeId);
//        inputParams.put("START_DATE", new java.sql.Date(shipDate.getTime()));
//        inputParams.put("DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));
//
//        /* build DataRequest object */
//        dataRequest.setConnection(conn);
//        dataRequest.setStatementID("VALIDATE_SHIPPING_DATES");
//        dataRequest.setInputParams(inputParams);
//
//        /* execute the store prodcedure */
//        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
//        String outputs = (String)dataAccessUtil.execute(dataRequest);
//        result = outputs.equalsIgnoreCase(orderConstants.VL_NO) ? false : true;
//        return result;
//    }


    /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_SHIPPING_KEY_COSTS
   * stored procedure.
   *
   * @param String shippingKey
   * @return Double sameDayDeliveryFee
   */
    public

    Double getShippingKeyCosts(String shippingKeyDetailId,
                               String shippingMethodId) throws Exception {
        Double shippingKeyCosts = new Double(0);
        DataRequest dataRequest = new DataRequest();
        boolean result = false;
        logger.info(" getShippingKeyCosts(String shippingKeyDetailId(" +
                     shippingKeyDetailId + "), String shippingMethodId (" +
                     shippingMethodId + ") ) :: Double");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("SHIPPING_KEY_DETAIL_ID", shippingKeyDetailId);
        inputParams.put("SHIPPING_METHOD_ID", shippingMethodId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_SHIPPING_KEY_COSTS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet outputs = null;
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            shippingKeyCosts = new Double(outputs.getString("SHIPPING_COST"));
        }
        return shippingKeyCosts;
    }

    /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_VENDOR_SHIPPING_RESTRICT
   * stored procedure.
   *
   * @param String vendorId
   * @return VendorBlockVO
   */
    public

    VendorBlockVO getVendorShippingBlock(String vendorId) throws Exception {
        VendorBlockVO vo = new VendorBlockVO();
        DataRequest dataRequest = new DataRequest();
        boolean isEmpty = true;
        logger.info("getVendorShippingBlock(String vendorId (" + vendorId +
                     ")");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("VENDOR_ID", vendorId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_VENDOR_SHIPPING_RESTRICT");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet outputs = null;
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            isEmpty = false;
            vo.setVendorId(vendorId);
            vo.setStartDate(outputs.getDate("START_DATE"));
            vo.setEndDate(outputs.getDate("END_DATE"));
        }
        if (isEmpty)
            return null;
        else
            return vo;
    }

    /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_VENDOR_DELIVERY_RESTRICT
   * stored procedure.
   *
   * @param String vendorId
   * @return VendorBlockVO
   */
    public

    VendorBlockVO getVendorDeliveryBlock(String vendorId) throws Exception {
        VendorBlockVO vo = new VendorBlockVO();
        DataRequest dataRequest = new DataRequest();
        boolean isEmpty = true;
        logger.info("getVendorDeliveryBlock(String vendorId (" + vendorId +
                     ")");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("VENDOR_ID", vendorId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_VENDOR_DELIVERY_RESTRICT_RESULTSET");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet outputs = null;
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            isEmpty = false;
            vo.setVendorId(vendorId);
            vo.setStartDate(outputs.getDate("START_DATE"));
            vo.setEndDate(outputs.getDate("END_DATE"));
        }
        if (isEmpty)
            return null;
        else
            return vo;
    }


    /**
   * This method is a wrapper for the FTD_APPS.FLORIST_QUERY_PKG.VIEW_FLORIST_MASTER
   * stored procedure.
   *
   * @param String floristId
   * @return FloristVO
   */
    public FloristVO getFlorist(String floristId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        FloristVO florist = new FloristVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        logger.info("getFlorist (String floristId (" + floristId +
                     ") ) :: FloristVO");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("FLORIST_ID", floristId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_VIEW_FLORIST_MASTER");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            isEmpty = false;
            florist.setAddress((String)outputs.getString("ADDRESS"));
            florist.setAdjustedWeight(outputs.getInt("ADJUSTED_WEIGHT"));
            florist.setAdjustedWeightEndDate(outputs.getDate("ADJUSTED_WEIGHT_END_DATE"));
            florist.setAdjustedWeightPermenantFlag(outputs.getString("ADJUSTED_WEIGHT_PERMENANT_FLAG"));
            florist.setAdjustedWeightStartDate(outputs.getDate("ADJUSTED_WEIGHT_START_DATE"));
            florist.setAltPhoneNumber(outputs.getString("ALT_PHONE_NUMBER"));
            florist.setCity(outputs.getString("CITY"));
            florist.setCityStateNumber(outputs.getString("CITY_STATE_NUMBER"));
            florist.setDeliveryCity(outputs.getString("DELIVERY_CITY"));
            florist.setDeliveryState(outputs.getString("DELIVERY_STATE"));
            florist.setEmailAddress(outputs.getString("EMAIL_ADDRESS"));
            florist.setFaxNumber(outputs.getString("FAX_NUMBER"));
            florist.setFloristId(outputs.getString("FLORIST_ID"));
            florist.setFloristName(outputs.getString("FLORIST_NAME"));
            florist.setFloristWeight(outputs.getInt("FLORIST_WEIGHT"));
            florist.setInitialWeight(outputs.getInt("INITIAL_WEIGHT"));
            florist.setMercuryFlag(outputs.getString("MERCURY_FLAG"));
            florist.setMinimumOrderAmount(outputs.getDouble("MINIMUM_ORDER_AMOUNT"));
            florist.setOrdersSent(outputs.getInt("ORDERS_SENT"));
            florist.setOwnersName(outputs.getString("OWNERS_NAME"));
            florist.setPhoneNumber(outputs.getString("PHONE_NUMBER"));
            florist.setRecordType(outputs.getString("RECORD_TYPE"));
            florist.setSendingRank(outputs.getInt("SENDING_RANK"));
            florist.setState(outputs.getString("STATE"));
            florist.setStatus(outputs.getString("STATUS"));
            florist.setSundayDeliveryBlockEndDt(outputs.getDate("SUNDAY_DELIVERY_BLOCK_END_DT"));
            florist.setSundayDeliveryBlockStDt(outputs.getDate("SUNDAY_DELIVERY_BLOCK_ST_DT"));
            florist.setSundayDeliveryFlag(outputs.getString("SUNDAY_DELIVERY_FLAG"));
            florist.setSuperFloristFlag(outputs.getString("SUPER_FLORIST_FLAG"));
            florist.setSuspendOverrideFlag(outputs.getString("SUSPEND_OVERRIDE_FLAG"));
            florist.setTerritory(outputs.getString("TERRITORY"));
            florist.setVendorFlag(outputs.getString("VENDOR_FLAG"));
            florist.setZipCode(outputs.getString("ZIP_CODE"));
            florist.setIsFTOFlag(outputs.getString("IS_FTO_FLAG"));
            florist.setAllowMessageForwardingFlag(outputs.getString("ALLOW_MESSAGE_FORWARDING_FLAG"));
            // florist blocks //
            florist.setBlockType(outputs.getString("BLOCK_TYPE"));
            florist.setBlockStartDate(outputs.getDate("BLOCK_START_DATE"));
            florist.setBlockEndDate(outputs.getDate("BLOCK_END_DATE"));
            florist.setBlockedByUserId(outputs.getString("BLOCKED_BY_USER_ID"));
            florist.setSuspendType(outputs.getString("SUSPEND_TYPE"));
            florist.setSuspendStartDate(outputs.getDate("SUSPEND_START_DATE"));
            florist.setSuspendEndDate(outputs.getDate("SUSPEND_END_DATE"));
        }
        if (isEmpty)
            return null;
        else
            return florist;
    }


    /**
   * This method is a wrapper for the SP_GET_DAILY_FLORIST SP,
   * returning a floristId to use or null otherwise.
   *
   * @param OrderDetailVO
   * @return LinkedList of Strings
   */
    public LinkedList getDailyFlorist(OrderDetailVO orderDtl) throws Exception {
        DataRequest dataRequest = new DataRequest();
        boolean isEmpty = true;
        logger.info("getDailyFlorists (OrderDetailVO orderDtl (" +
                     orderDtl.getOrderDetailId() +
                     ")) :: LinkedList (strings)");
        logger.info("customerId: " + orderDtl.getRecipientId());
        logger.info("deliveryDate: " + orderDtl.getDeliveryDate());
        LinkedList florists = new LinkedList();

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.ORDER_DETAIL_ID,
                        new Long(orderDtl.getOrderDetailId()));
        inputParams.put(orderConstants.CUSTOMER_ID,
                        new Long(orderDtl.getRecipientId()));
        inputParams.put(orderConstants.DELIVERY_DATE,
                        orderDtl.getDeliveryDate() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDate().getTime()));

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_DAILY_FLORIST);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet outputs =
            (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (outputs.next()) {
            if (outputs.getObject(1) != null) {
                isEmpty = false;
                florists.add((String)outputs.getObject(1));
            }
        }
        if (isEmpty)
            return null;
        else
            return florists;
    }

    /**
   * This method is a wrapper for the SP_GET_ORDER_DETAIL_PAYMENT SP.
   * It populates a payment VO based on the returned record set.
   * WARNING:  Method does not populate all the payment fields, thus poses a risk,
   * especially for updates.  Use OrderDAO.getFirstPayment instead.
   *
   * @param OrderDetailVO
   * @return PaymentVO
   */
    public PaymentVO getPayment(OrderDetailVO orderDtl) throws Exception {
        DataRequest dataRequest = new DataRequest();
        PaymentVO payment = new PaymentVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        String mpCreditAmt = null;
        String mpDebitAmt = null;

        logger.info("getPayment (OrderDetailVO orderDtl(" +
                     orderDtl.getOrderDetailId() + ")) :: PaymentVO");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID",
                        orderDtl.getOrderDetailId() + "");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_ORDER_DETAIL_PAYMENT);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        /* populate object */

        // always store the first record, unless a credit card is in one of the
        // other records (store the credit card instead)

        while (outputs.next()) {
            if (isEmpty) {
                isEmpty = false;
                payment.setPaymentId(outputs.getLong(orderConstants.PAYMENT_ID));
                payment.setOrderGuid(outputs.getString(orderConstants.ORDER_GUID));
                payment.setAdditionalBillId(outputs.getString(orderConstants.ADDITIONAL_BILL_ID));
                payment.setPaymentType(outputs.getString(orderConstants.PAYMENT_TYPE));
                payment.setCcId(outputs.getLong(orderConstants.CC_ID));
                payment.setCreditAmount(outputs.getDouble(orderConstants.CREDIT_AMOUNT));
                payment.setDebitAmount(outputs.getDouble(orderConstants.DEBIT_AMOUNT));
                payment.setAuthResult(outputs.getString(orderConstants.AUTH_RESULT));
                payment.setAuthNumber(outputs.getString(orderConstants.AUTH_NUMBER));
                payment.setAvsCode(outputs.getString(orderConstants.AVS_CODE));
                payment.setAcqReferenceNumber(outputs.getString(orderConstants.ACQ_REFERENCE_NUMBER));
                payment.setGcCouponNumber(outputs.getString(orderConstants.GC_COUPON_NUMBER));
                payment.setAuthOverrideFlag(outputs.getString(orderConstants.AUTH_OVERRIDE_FLAG) ==
                                            null ? "Y" :
                                            outputs.getString(orderConstants.AUTH_OVERRIDE_FLAG));
                payment.setPaymentIndicator(outputs.getString(orderConstants.PAYMENT_INDICATOR));
                payment.setRefundId(outputs.getString(orderConstants.REFUND_ID));
                mpCreditAmt = outputs.getString(orderConstants.MILES_POINTS_CREDIT_AMT);
                mpDebitAmt = outputs.getString(orderConstants.MILES_POINTS_DEBIT_AMT);
                if(mpCreditAmt != null && mpCreditAmt.length() > 0) {
                    payment.setMilesPointsCreditAmt(Integer.valueOf(mpCreditAmt));
                }
                if(mpDebitAmt != null && mpDebitAmt.length() > 0) {
                    payment.setMilesPointsDebitAmt(Integer.valueOf(mpDebitAmt));
                }
                payment.setCardinalVerifiedFlag(outputs.getString(orderConstants.CARDINAL_VERIFIED_FLAG));
                logger.info("cardinal flag: "+payment.getCardinalVerifiedFlag());
            } else {
                // We removed the logic here since there should have only been one
                // payment record returned anyhow.  If we get here, let's just complain about it.
                logger.error("Uh oh, getPayment found multiple records - should only be one, so continued with first one");
            }
        }
        if (isEmpty)
            return null;
        else
            return payment;
    }


    /**
   * This method is a wrapper for the SP_GET_GNADD SP.
   * It populates a GNADD VO based on the returned record set.
   *
   * @param none
   * @return GnaddVO
   */
    public GnaddVO getGnadd() throws Exception {
        DataRequest dataRequest = new DataRequest();
        GnaddVO gnadd = new GnaddVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        logger.info("getGnadd () :: GnaddVO");

        GlobalParameterVO vo1 =
            this.getGlobalParameter("FTDAPPS_PARMS", "GNADD_DATE");
        GlobalParameterVO vo2 =
            this.getGlobalParameter("ORDER_PROCESSING", "GNADD_HOLD");
        GlobalParameterVO vo3 =
            this.getGlobalParameter("FTDAPPS_PARMS", "GNADD_LEVEL");
        gnadd.setGnaddDate(vo1.getValue());
        gnadd.setGnaddHoldFlag(vo2.getValue());
        gnadd.setGnaddStatus("OFF");
        try {
            String gnaddLevel = vo3.getValue();
            if (new Integer(gnaddLevel).intValue() > 0) {
                gnadd.setGnaddStatus("ON");
            }
        } catch (Exception e) {
            // if anything goes wrong here, default gnadd to off
        }
        return gnadd;
    }

    /**
   * This method is a wrapper for the SP_GET_COMPANY SP.
   * It populates a company VO based on the returned record set.
   *
   * @param String - companyId
   * @return CompanyVO
   */
    public CompanyVO getCompany(String companyId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        CompanyVO company = new CompanyVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        logger.info("getCompany (String companyId (" + companyId +
                     ")) :: CompanyVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.COMPANY_ID, companyId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_COMPANY);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */
        while (outputs.next()) {
            isEmpty = false;
            company.setCompanyId(companyId);
            company.setCompanyName(outputs.getString(orderConstants.COMPANY_NAME));
            company.setInternetOrigin(outputs.getString(orderConstants.INTERNET_ORIGIN));
            company.setDefaultProgramId(outputs.getLong(orderConstants.DEFAULT_PROGRAM_ID));
            company.setPhoneNumber(outputs.getString(orderConstants.PHONE_NUMBER));
            company.setClearingMemberNumber(outputs.getString(orderConstants.CLEARING_MEMBER_NUMBER));
            company.setLogoFileName(outputs.getString(orderConstants.LOGO_FILE_NAME));
            company.setEnableLpProcessing(outputs.getString(orderConstants.ENABLE_LP_PROCESSING));
            company.setURL(outputs.getString("URL"));
        }
        if (isEmpty)
            return null;
        else
            return company;
    }

    /**
   * This method is a wrapper for the SP_GET_SOURCE SP.
   * It populates a company VO based on the returned record set.
   *
   * @param String - sourceCode
   * @return SourceVO
   */
    public SourceVO getSource(String sourceCode) throws Exception {
        DataRequest dataRequest = new DataRequest();
        SourceVO source = new SourceVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        logger.info("getSource (String sourceCode (" + sourceCode +
                     ")) :: SourceVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.SOURCE_CODE, sourceCode);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_SOURCE_CODE);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */
        while (outputs.next()) {
            isEmpty = false;
            source.setBillingInfoLogic(outputs.getString(orderConstants.BILLING_INFO_LOGIC));
            source.setBillingInfoPrompt(outputs.getString(orderConstants.BILLING_INFO_PROMPT));
            source.setBonusPoints(outputs.getString(orderConstants.BONUS_POINTS));
            source.setBonusType(outputs.getString(orderConstants.BONUS_TYPE));
            source.setCompanyId(outputs.getString(orderConstants.COMPANY_ID));
            source.setDefaultSourceCodeFlag(outputs.getString(orderConstants.DEFAULT_SOURCE_CODE_FLAG));
            source.setDepartmentCode(outputs.getString(orderConstants.DEPARTMENT_CODE));
            source.setDescription(outputs.getString(orderConstants.DESCRIPTION));
            source.setDiscountAllowedFlag(outputs.getString(orderConstants.DISCOUNT_ALLOWED_FLAG));
            source.setEnableLpProcessing(outputs.getString(orderConstants.ENABLE_LP_PROCESSING));
            source.setEndDate(outputs.getDate(orderConstants.END_DATE));
            source.setExternalCallCenterFlag(outputs.getString(orderConstants.EXTERNAL_CALL_CENTER_FLAG));
            source.setFraudFlag(outputs.getString(orderConstants.FRAUD_FLAG));
            source.setHighlightDescriptionFlag(outputs.getString(orderConstants.HIGHLIGHT_DESCRIPTION_FLAG));
            source.setJcpenneyFlag(outputs.getString(orderConstants.JCPENNEY_FLAG));
            source.setListCodeFlag(outputs.getString(orderConstants.LIST_CODE_FLAG));
            source.setMarketingGroup(outputs.getString(orderConstants.MARKETING_GROUP));
            source.setMaximumPointsMiles(outputs.getLong(orderConstants.MAXIMUM_POINTS_MILES));
            source.setMileageBonus(outputs.getString(orderConstants.MILEAGE_BONUS));
            source.setMileageCalculationSource(outputs.getString(orderConstants.MILEAGE_CALCULATION_SOURCE));
            source.setOeDefaultFlag(outputs.getString(orderConstants.OE_DEFAULT_FLAG));
            source.setOrderSource(outputs.getString(orderConstants.ORDER_SOURCE));
            source.setPartnerId(outputs.getString(orderConstants.PARTNER_ID));
            source.setPointsMilesPerDollar(outputs.getLong(orderConstants.POINTS_MILES_PER_DOLLAR));
            source.setPricingCode(outputs.getString(orderConstants.PRICING_CODE));
            source.setPromotionCode(outputs.getString(orderConstants.PROMOTION_CODE));
            source.setSendToScrub(outputs.getString(orderConstants.SEND_TO_SCRUB));
            source.setSeparateData(outputs.getString(orderConstants.SEPARATE_DATA));
            source.setShippingCode(outputs.getString(orderConstants.SHIPPING_CODE));
            source.setSourceCode(outputs.getString(orderConstants.SOURCE_CODE));
            source.setSourceType(outputs.getString(orderConstants.SOURCE_TYPE));
            source.setStartDate(outputs.getDate(orderConstants.START_DATE));
            source.setValidPayMethod(outputs.getString(orderConstants.VALID_PAY_METHOD));
            source.setYellowPagesCode(outputs.getString(orderConstants.YELLOW_PAGES_CODE));
            source.setEmergencyTextFlag(outputs.getString(orderConstants.EMERGENCY_TEXT_FLAG));
            source.setRequiresDeliveryConfirmation(outputs.getString(orderConstants.REQUIRES_DELIVERY_CONFIRMATION));
        }
        if (isEmpty)
            return null;
        else
            return source;
    }

    /**
   * This method is a wrapper for the SP_GET_LP_ORDER_COUNT SP.
   * It returns the integer returned by the SP.
   *
   * @param OrderVO
   * @return int
   */
    public int getLPOrderCount(OrderVO order, int hours) throws Exception {
        DataRequest dataRequest = new DataRequest();
        int result = 0;

        logger.info("getLPOrderCount (OrderVO order, int hours) :: int");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.LP_ORDER_INDICATOR,
                        order.getLossPreventionIndicator());
        inputParams.put(orderConstants.HOURS, new Integer(hours));

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_LP_INDICATOR_COUNT);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        result = ((BigDecimal)dataAccessUtil.execute(dataRequest)).intValue();
        return result;
    }

    public boolean isOrderOnLPHold(OrderDetailVO order) throws Exception {
        logger.info("isOrderOnLPHold(OrderDetailVO order (" +
                     order.getOrderDetailId() + "))");
        DataRequest dataRequest = new DataRequest();
        String result = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_GUID", order.getOrderGuid());
        inputParams.put("IN_ORDER_DETAIL_ID", (order.getOrderDetailId() == 0) ? null : Long.toString(order.getOrderDetailId()));

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_IS_ORDER_LP_HOLD");
        dataRequest.setInputParams(inputParams);

        /* execute the stored prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        result = (String)dataAccessUtil.execute(dataRequest);

        return (result != null &&
                result.equalsIgnoreCase(OrderConstants.VL_YES)) ? true : false;
    }

    /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_OCCASION
   * It returns the description for a given occasionId.
   *
   * @param String occasionId
   * @return String description
   */
    public String getOccasionDescription(String occasionId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        String description = "";
        CachedResultSet outputs = null;

        logger.info("getOccasionDescription ( String occasionId :" +
                     occasionId + " ):: String");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("OCCASION_ID", occasionId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_OCCASION");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */
        while (outputs.next()) {
            description = outputs.getString("DESCRIPTION");
        }
        return description;
    }


    /**
   * This method is a wrapper for the FTD_APPS.MISC_PKG.GET_PRODUCT_SECOND_CHOICE
   * It populates a SecondChoiceVO based on the returned record set.
   *
   * @param String - secondChoiceId
   * @return SecondChoiceVO
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
    public SecondChoiceVO getSecondChoice(String secondChoiceId) throws SQLException,
                                                                        Exception {
        DataRequest dataRequest = new DataRequest();
        SecondChoiceVO secondChoiceVO = new SecondChoiceVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;


        logger.info("getSecondChoice ( String secondChoiceId :" +
                     secondChoiceId + " ):: SecondChoiceVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("PRODUCT_SECOND_CHOICE_ID", secondChoiceId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_PRODUCT_SECOND_CHOICE");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */
        while (outputs.next()) {
            isEmpty = false;
            secondChoiceVO.setProductSecondChoiceId(outputs.getString("PRODUCT_SECOND_CHOICE_ID"));
            secondChoiceVO.setMercuryDescription1(outputs.getString("MERCURY_DESCRIPTION1"));
            secondChoiceVO.setMercuryDescription2(outputs.getString("MERCURY_DESCRIPTION2"));
            secondChoiceVO.setOeDescription1(outputs.getString("OE_DESCRIPTION1"));
            secondChoiceVO.setOeDescription2(outputs.getString("OE_DESCRIPTION2"));
        }
        if (isEmpty)
            return null;
        else
            return secondChoiceVO;
    }


    /**
   * This method is a wrapper for the SP_GET_PRODUCT SP.
   * It populates a product VO based on the returned record set.
   *
   * @param String - productId
   * @return ProductVO
   */
    public ProductVO getProduct(String productId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        ProductVO product = new ProductVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        try {
            logger.info("getProduct (String productId (" + productId +
                         ")) :: ProductVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.PRODUCT_ID, productId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_SP_GET_PRODUCT_BY_ID_RESULTSET");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                product.setProductId(outputs.getString(orderConstants.PRODUCT_ID_LC));
                product.setNovatorId(outputs.getString(orderConstants.NOVATOR_ID_LC));
                product.setProductName(outputs.getString(orderConstants.PRODUCT_NAME_LC));
                product.setNovatorName(outputs.getString(orderConstants.NOVATOR_NAME_LC));
                product.setStatus(outputs.getString(orderConstants.STATUS_LC));
                product.setDeliveryType(outputs.getString(orderConstants.DELIVERY_TYPE_LC));
                product.setCategory(outputs.getString(orderConstants.CATEGORY_LC));
                product.setProductType(outputs.getString(orderConstants.PRODUCT_TYPE_LC));
                product.setProductSubType(outputs.getString(orderConstants.PRODUCT_SUB_TYPE_LC));
                product.setColorSizeFlag(outputs.getString(orderConstants.COLOR_SIZE_FLAG_LC));
                product.setStandardPrice(outputs.getDouble(orderConstants.STANDARD_PRICE_LC));
                product.setDeluxePrice(outputs.getDouble(orderConstants.DELUXE_PRICE_LC));
                product.setPremiumPrice(outputs.getDouble(orderConstants.PREMUIM_PRICE_LC));
                product.setPreferredPricePoint(outputs.getDouble(orderConstants.PREFERRED_PRICE_POINT_LC));
                product.setVariablePriceMax(outputs.getDouble(orderConstants.VARIABLE_PRICE_MAX_LC));
                product.setShortDescription(outputs.getString(orderConstants.SHORT_DESCRIPTION_LC));
                product.setLongDescription(outputs.getString(orderConstants.LONG_DESCRIPTION_LC));
                product.setFloristReferenceNumber(outputs.getString(orderConstants.FLORIST_REFERENCE_NUMBER_LC));
                product.setMercuryDescription(outputs.getString(orderConstants.MERCURY_DESCRIPTION_LC));
                product.setItemComments(outputs.getString(orderConstants.ITEM_COMMENTS_LC));
                product.setAddOnBalloonsFlag(outputs.getString(orderConstants.ADD_ON_BALLOONS_FLAG_LC));
                product.setAddOnBearsFlag(outputs.getString(orderConstants.ADD_ON_BEARS_FLAG_LC));
                product.setAddOnCardsFlag(outputs.getString(orderConstants.ADD_ON_CARDS_FLAG_LC));
                product.setAddOnFuneralFlag(outputs.getString(orderConstants.ADD_ON_FUNERAL_FLAG_LC));
                product.setCodifiedFlag(outputs.getString(orderConstants.CODIFIED_FLAG_LC));
                product.setExceptionCode(outputs.getString(orderConstants.EXECPTION_CODE_LC));
                product.setExceptionStartDate(outputs.getDate(orderConstants.EXCEPTION_START_DATE_LC));
                product.setExceptionEndDate(outputs.getDate(orderConstants.EXECPTION_END_DATE_LC));
                product.setExceptionMessage(outputs.getString(orderConstants.EXCEPTION_MESSAGE_LC));
                product.setSecondChoiceCode(outputs.getString(orderConstants.SECOND_CHOICE_CODE_LC));
                product.setHolidaySecondChoiceCode(outputs.getString(orderConstants.HOLIDAY_SECOND_CHOICE_CODE_LC));
                product.setDropshipCode(outputs.getString(orderConstants.DROPSHIP_CODE_LC));
                product.setDiscountAllowedFlag(outputs.getString(orderConstants.DISCOUNT_ALLOWED_FLAG_LC));
                product.setDeliveryIncludedFlag(outputs.getString(orderConstants.DELIVERY_INCLUDED_FLAG_LC));
                product.setTaxFlag(outputs.getString(orderConstants.TAX_FLAG_LC));
                product.setServiceFeeFlag(outputs.getString(orderConstants.SERVICE_FEE_FLAG_LC));
                product.setExoticFlag(outputs.getString(orderConstants.EXOTIC_FLAG_LC));
                product.setEgiftFlag(outputs.getString(orderConstants.EGIFT_FLAG_LC));
                product.setCountryId(outputs.getString(orderConstants.COUNTRY_ID_LC));
                product.setArrangementSize(outputs.getString(orderConstants.ARRANGEMENT_SIZE_LC));
                product.setArrangementColors(outputs.getString(orderConstants.ARRANGEMENT_COLORS_LC));
                product.setDominantFlowers(outputs.getString(orderConstants.DOMINANT_FLOWERS_LC));
                product.setSearchPriority(outputs.getString(orderConstants.SEARCH_PRIORITY_LC));
                product.setRecipe(outputs.getString(orderConstants.RECIPE_LC));
                product.setSendStandardRecipe(outputs.getString(orderConstants.SEND_STANDARD_RECIPE_LC));
                product.setDeluxeRecipe(outputs.getString(orderConstants.DELUXE_RECIPE_LC));
                product.setSendDeluxeRecipe(outputs.getString(orderConstants.SEND_DELUXE_RECIPE_LC));
                product.setPremiumRecipe(outputs.getString(orderConstants.PREMIUM_RECIPE_LC));
                product.setSendPremiumRecipe(outputs.getString(orderConstants.SEND_PREMIUM_RECIPE_LC));
                product.setSubcodeFlag(outputs.getString(orderConstants.SUBCODE_FLAG_LC));
                product.setDimWeight(outputs.getString(orderConstants.DIM_WEIGHT_LC));
                product.setNextDayUpgradeFlag(outputs.getString(orderConstants.NEXT_DAT_UPGRADE_FLAG_LC));
                product.setCorporateSite(outputs.getString(orderConstants.CORPORATE_SITE_LC));
                product.setUnspscCode(outputs.getString(orderConstants.UNSPSC_CODE_LC));
                product.setPriceRank1(outputs.getString(orderConstants.PRICE_RANK1_LC));
                product.setPriceRank2(outputs.getString(orderConstants.PRICE_RANK2_LC));
                product.setPriceRank3(outputs.getString(orderConstants.PRICE_RANK3_LC));
                product.setShipMethodCarrier(outputs.getString(orderConstants.SHIP_METHOD_CARRIER_LC));
                product.setShipMethodFlorist(outputs.getString(orderConstants.SHIP_METHOD_FLORIST_LC));
                product.setShippingKey(outputs.getString(orderConstants.SHIPPING_KEY_LC));
                product.setVariablePriceFlag(outputs.getString(orderConstants.VARIABLE_PRICE_FLAG_LC));
                product.setHolidaySku(outputs.getString(orderConstants.HOLIDAY_SKU_LC));
                product.setHolidayPrice(outputs.getDouble(orderConstants.HOLIDAY_PRICE_LC));
                product.setCatalogFlag(outputs.getString(orderConstants.CATALOG_FLAG_LC));
                product.setHolidayDeluxePrice(outputs.getDouble(orderConstants.HOLIDAY_DELUXE_PRICE_LC));
                product.setHolidayPremiumPrice(outputs.getDouble(orderConstants.HOLIDAY_PREMIUM_PRICE_LC));
                product.setHolidayStartDate(outputs.getDate(orderConstants.HOLIDAY_START_DATE_LC));
                product.setHolidayEndDate(outputs.getDate(orderConstants.HOLIDAY_END_DATE_LC));
                product.setHoldUntilAvailable(outputs.getString(orderConstants.HOLD_UNTIL_AVAILABLE_LC));
                product.setMondayDeliveryFreshcut(outputs.getString(orderConstants.MONDAY_DELIVERY_FRESHCUT_LC));
                product.setTwoDaySatFreshcut(outputs.getString(orderConstants.TWO_DAY_SAT_FRESHCUT_LC));
                product.setShippingSystem(outputs.getString(orderConstants.SHIPPING_SYSTEM_LC));
                product.setPersonalizationTemplateOrder(outputs.getString(orderConstants.PERSONALIZATION_TEMPLATE_ORDER_LC));
                product.setServiceDuration(outputs.getInt(orderConstants.SERVICE_DURATION));
            }
        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }
        if (isEmpty)
            return null;
        else
            return product;
    }

    /** This method determines if two day shipping is available for the passed
     * product id.
     * @param productId
     * @return true if valid else false.
     */
    public boolean isShipMethodAvailable(String productId,
                                         String shipMethod) throws Exception {
        boolean retval = false;
        DataRequest dataRequest = new DataRequest();

        try {
            logger.info("isShipMethodAvailable (String productId (" +
                         productId + ", " + shipMethod + "))");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.PRODUCT_ID, productId);
            inputParams.put(orderConstants.SHIP_METHOD, shipMethod);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_IS_SHIP_METHOD_AVAILABLE");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            String result = (String)dataAccessUtil.execute(dataRequest);

            if ("Y".equals(result)) {
                retval = true;
            }
        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }

        return retval;
    }

    public ProductSubCodeVO getSubcode(String subcodeId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        ProductSubCodeVO productSubCodeVO = null;
        boolean isEmpty = true;
        CachedResultSet rs = null;

        try {
            logger.info("getSubcodde (String subcodeId) :: " + subcodeId);
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", subcodeId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_PRODUCT_SUBCODE");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (rs.next()) {


                productSubCodeVO = new ProductSubCodeVO();
                productSubCodeVO.setProductId(rs.getString(1));
                productSubCodeVO.setProductSubCodeId(rs.getString(2));
                productSubCodeVO.setSubCodeDescription(rs.getString(3));
                productSubCodeVO.setSubCodePrice(getDouble(rs.getString(4)));
                productSubCodeVO.setSubCodeHolidaySKU(rs.getString(5));
                productSubCodeVO.setSubCodeHolidayPrice(getDouble(rs.getString(6)));
                productSubCodeVO.setSubCodeRefNumber(rs.getString(8));
                productSubCodeVO.setDimWeight(rs.getString(11));
            }
        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }

        return productSubCodeVO;
    }


    /**
   * This method is a wrapper for the  CLEAN.ORDER_QUERY_PKG.GET_ORDER_ADD_ON.
   * @param String - orderDetailId
   * @return List
   */
    private ArrayList getAddOns(String orderDetailId) throws SQLException, Exception {
        DataRequest dataRequest = new DataRequest();
        ArrayList addOnList = new ArrayList();

        CachedResultSet outputs = null;

        try {
            logger.info("getAddOns (String " + orderDetailId +
                         ") ::AddOnVO[]");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("ID_STR", orderDetailId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_ORDER_ADD_ON");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            while (outputs.next()) {
                AddOnVO addOn = new AddOnVO();
                addOn.setAddOnCode(outputs.getString("ADD_ON_CODE"));
                addOn.setAddOnId(outputs.getLong("ORDER_ADD_ON_ID"));
                addOn.setAddOnQuantity(outputs.getLong("ADD_ON_QUANTITY"));
                addOn.setOrderDetailId(outputs.getLong("ORDER_DETAIL_ID"));
                addOn.setDesciption(outputs.getString("DESCRIPTION"));
                addOn.setPrice(new Double(outputs.getDouble("PRICE")));
                addOnList.add(addOn);
            }
        } catch (Exception e) {
            logger.error(e);
            throw new Exception(e.toString());
        }
        return (addOnList);
    }


    /**
   * This method is a wrapper for the SP_GET_AVAILABLE_VENDORS.
   * It populates a list of vendor VOs based on the returned record set.
   *
   * @param productId of product you are requesting vendors for
   * @return List of VendorProductVO for which a product is available
   */
    public List getAvailableVendors(String productId, long orderDetailId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        VendorProductVO vendor;
        ArrayList list = new ArrayList();
        CachedResultSet outputs = null;

        try {
            logger.info("getVendors (String productId, long orderDetailId) :: List of VendorProductVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("PRODUCT_ID", productId);
            inputParams.put("ORDER_DETAIL_ID", orderDetailId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_AVAILABLE_VENDORS");
            dataRequest.setInputParams(inputParams);


            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                vendor = new VendorProductVO();
                vendor.setVendorId(outputs.getString("VENDOR_ID"));
                vendor.setProductSkuId(outputs.getString("PRODUCT_SKU_ID"));
                vendor.setVendorCost(outputs.getDouble("VENDOR_COST"));
                vendor.setVendorSku(outputs.getString("VENDOR_SKU"));
                vendor.setAvailable(StringUtils.equals(outputs.getString("AVAILABLE"),"Y"));
                list.add(vendor);
            }
        } catch (Exception e) {
            logger.error(e.toString());
            throw e;
        }
        return list;
    }


    /**
   * This method is a wrapper for the SP_GET_VENDOR_PRODUCT SP.
   * It populates a VendorProductVO for the parameters passed.
   *
   * @param vendorId filter
   * @param productId filter
   * @return VendorProductVO for which a product and vendor passed
   */
    public VendorProductVO getVendorProduct(String vendorId, String productId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        VendorProductVO vendor = null;
        CachedResultSet outputs = null;

        try {
            logger.info("getVendors (String vendorId = " + vendorId + ", String productId = " + productId + ") :: VendorProductVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vendorId);
            inputParams.put("PRODUCT_ID", productId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_VENDOR_PRODUCT");
            dataRequest.setInputParams(inputParams);


            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            if (outputs.next()) {
                vendor = new VendorProductVO();
                vendor.setVendorId(outputs.getString("VENDOR_ID"));
                vendor.setProductSkuId(outputs.getString("PRODUCT_SKU_ID"));
                vendor.setVendorCost(outputs.getDouble("VENDOR_COST"));
                vendor.setVendorSku(outputs.getString("VENDOR_SKU"));
                vendor.setAvailable(StringUtils.equals(outputs.getString("AVAILABLE"),"Y"));
            }
        } catch (Exception e) {
            logger.error(e.toString());
            throw e;
        }
        return vendor;
    }


    /**
   * This method is a wrapper for the SP_GET_VENDOR SP.
   * It populates a vendor VO based on the returned record set.
   *
   * @param String - vendorId
   * @return VendorVO
   */
    public VendorVO getVendor(String vendorId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        VendorVO vendor = new VendorVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        try {
            logger.info("getVendor (String vendorId) :: VendorVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("VENDOR_ID", vendorId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_VENDOR");
            dataRequest.setInputParams(inputParams);


            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;

                vendor.setVendorId(vendorId);
                vendor.setVendorName(outputs.getString("VENDOR_NAME"));
                vendor.setVendorType(outputs.getString("VENDOR_TYPE"));
                vendor.setVendorMemberNumber(outputs.getString("MEMBER_NUMBER"));
            }
        } catch (Exception e) {
            logger.error(e.toString());
            throw e;
        }
        if (isEmpty) {
            return null;
        } else {
            return vendor;
        }
    }

    /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.
   * It populates a global parameter VO based on the returned record set.
   *
   * @param String - context
   * @param String - name
   * @return GlobalParameterVO
   */
    public GlobalParameterVO getGlobalParameter(String context,
                                                String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        GlobalParameterVO param = new GlobalParameterVO();
        boolean isEmpty = true;
        String outputs = null;

        try {
            logger.info("getGlobalParameter (String context (" + context + "), String name (" + name + ")) :: GlobalParameterVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.CONTEXT, context);
            inputParams.put(orderConstants.NAME, name);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_GET_GLOBAL_PARM_VALUE);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (String)dataAccessUtil.execute(dataRequest);
            if (outputs != null)
                isEmpty = false;
            /* populate object */
            param.setName(name);
            param.setValue(outputs);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return param;
    }

    /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.
   * It populates a global parameter VO based on the returned record set.
   *
   * @param String - name
   * @return GlobalParameterVO
   */
    public GlobalParameterVO getGlobalParameter(String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        GlobalParameterVO param = new GlobalParameterVO();
        CachedResultSet outputs = null;
        boolean isEmpty = true;

        try {
            logger.info("getGlobalParameter (String name (" + name + ")) :: GlobalParameterVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.NAME, name);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_GET_GLOBAL_PARMETER);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                param.setName(outputs.getString(orderConstants.NAME_LC));
                param.setValue(outputs.getString(orderConstants.VALUE_LC));
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return param;
    }


    /**
   * This method is a wrapper for the FRP.MISC_PKG.UPDATE_GLOBAL_PARAMETER.
   * It populates a global parameter VO based on the returned record set.
   *
   * @param String - context
   * @param String - name
   * @param String - value
   */
    public void setGlobalParameter(String context, String name, String value,
                                   String updatedBy) throws Exception {
        DataRequest dataRequest = new DataRequest();
        GlobalParameterVO param = new GlobalParameterVO();
        Map outputs = null;

        try {
            logger.info("setGlobalParameter(String context (" + context +
                         "), String name(" + name + "), String value(" +
                         value + "))");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_CONTEXT", context);
            inputParams.put("IN_NAME", name);
            inputParams.put("IN_VALUE", value);
            inputParams.put("IN_UPDATED_BY", updatedBy);
            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_UPDATE_GLOBAL_PARMS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map)dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
       			 * if the procedure executed successfully
       			 */
            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = (String)outputs.get("OUT_MESSAGE");
                throw new Exception(message);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    /**
   * This method is a wrapper for the SP_GET_CUSTOMER SP.
   * It populates a customer VO based on the returned record set.
   *
   * @param String - customerId
   * @return CustomerVO
   */
    public CustomerVO getCustomer(long customerId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        CustomerVO customer = new CustomerVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        CachedResultSet outputs2 = null;

        try {
            logger.info("getCustomer (long customerId (" + customerId +
                         ")) :: CustomerVO");

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.CUSTOMER_ID, new Long(customerId));

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_GET_CUSTOMER);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            customer.setCustomerId(new Long(customerId).longValue());
            while (outputs.next()) {
                isEmpty = false;
                customer.setConcatId(outputs.getString(orderConstants.CONCAT_ID));
                customer.setFirstName(outputs.getString(orderConstants.CUSTOMER_FIRST_NAME));
                customer.setLastName(outputs.getString(orderConstants.CUSTOMER_LAST_NAME));
                customer.setBusinessName(outputs.getString(orderConstants.BUSINESS_NAME));
                customer.setAddress1(outputs.getString(orderConstants.CUSTOMER_ADDRESS_1));
                customer.setAddress2(outputs.getString(orderConstants.CUSTOMER_ADDRESS_2));
                customer.setCity(outputs.getString(orderConstants.CUSTOMER_CITY));
                customer.setState(outputs.getString(orderConstants.CUSTOMER_STATE));

                // fixed to deal with international zip_code of N/A

                String zip_code = outputs.getString(orderConstants.CUSTOMER_ZIP_CODE);
                String country = outputs.getString(orderConstants.CUSTOMER_COUNTRY);

                if (zip_code != null) {

                    /* Added check to pull full zip if country is Canada. */
                    if((zip_code.length() > 5) && (!("CA".equalsIgnoreCase(country))) ) {
                        zip_code = zip_code.substring(0, 5);
                    }

                    customer.setZipCode(zip_code);
                }

                customer.setCountry(country);
                customer.setAddressType(outputs.getString("address_type"));
            }

            /* setup store procedure input parameters */
            HashMap inputParams2 = new HashMap();
            inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());

            /* build DataRequest object */
            DataRequest dataRequest2 = new DataRequest();
            dataRequest2.setConnection(conn);
            dataRequest2.setStatementID("OP_GET_CUSTOMER_PHONES");
            dataRequest2.setInputParams(inputParams2);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil2 = DataAccessUtil.getInstance();
            outputs2 = (CachedResultSet)dataAccessUtil.execute(dataRequest2);

            /* populate object */
            List phoneList = new ArrayList();
            while (outputs2.next()) {
                CustomerPhoneVO phoneVO = new CustomerPhoneVO();
                phoneVO.setCustomerId(customerId);
                phoneVO.setExtension(outputs2.getString("CUSTOMER_EXTENSION"));
                phoneVO.setPhoneId(outputs2.getLong("CUSTOMER_PHONE_ID"));
                phoneVO.setPhoneNumber(outputs2.getString("CUSTOMER_PHONE_NUMBER"));
                phoneVO.setPhoneType(outputs2.getString("CUSTOMER_PHONE_TYPE"));
                phoneList.add(phoneVO);
            }
            customer.setCustomerPhoneVOList(phoneList);

        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return customer;
    }

    /**
   * This method is a wrapper for the SP_CAN_FLORIST_SUPPORT_ORDER SP.
   * It returns true if the SP returns a �Y� and false otherwise.
   *
   * @param OrderDetailVO
   * @param FloristVO
   * @return boolean
   */
    public boolean canFloristSupportOrder(OrderDetailVO orderDtl,
                                          FloristVO florist) throws Exception {
        DataRequest dataRequest = new DataRequest();
        boolean result = true;

        try {
            logger.info("canFloristSupportOrder (OrderDetailVO orderDtl (" +
                         orderDtl.getOrderDetailId() +
                         ", FloristVO florist) :: boolean");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ORDER_DETAIL_ID,
                            new Long(orderDtl.getOrderDetailId()));
            inputParams.put(orderConstants.FLORIST_ID, florist.getFloristId());

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_CAN_FLORIST_SUPPORT_ORDER);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            String outputs = (String)dataAccessUtil.execute(dataRequest);

            /* process results to determine if florist can support order */
            result =
                    outputs.equalsIgnoreCase(orderConstants.VL_YES) ? true : false;
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }

        return result;
    }

    /**
   * This method is a wrapper for the FTD_APPS.MISC_PKG.RETRIEVE_ACTIVE_COUNTRY_NAME.
   *
   * @param String countryId
   * @return String countryName
   */
    public String getActiveCountryName(String countryId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        String result = null;

        try {
            logger.info("getActiveCountryName (String countryId) :: String");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_COUNTRY_ID", countryId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_RETRIEVE_ACTIVE_COUNTRY_NAME");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            result = (String)dataAccessUtil.execute(dataRequest);

            /* process results to determine if florist can support order */
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }

        return result;
    }


    /**
   * This method is a wrapper for the SP_GET_ORDER SP.
   * It populates a Order VO based on the returned record set.
   *
   * @param String - orderGuid
   * @return OrderVO
   */
    public OrderVO getOrder(String orderGuid) throws Exception {
        DataRequest dataRequest = new DataRequest();
        OrderVO order = new OrderVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            logger.info("getOrder (String orderGuid) :: OrderVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ORDER_GUID, orderGuid);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_GET_ORDER_BY_GUID);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                order.setMasterOrderNumber(outputs.getString(orderConstants.MASTER_ORDER_NUMBER));
                order.setCustomerId(outputs.getLong(orderConstants.CUSTOMER_ID));
                order.setMembershipId(outputs.getLong(orderConstants.MEMBERSHIP_ID));
                order.setCompanyId(outputs.getString(orderConstants.COMPANY_ID));
                order.setSourceCode(outputs.getString(orderConstants.SOURCE_CODE));
                order.setOriginId(outputs.getString(orderConstants.ORIGIN_ID));

                if (StringUtils.isNotEmpty(outputs.getString(orderConstants.CHAR_ORDER_DATE)))
                {
                    String orderDate = outputs.getString(orderConstants.CHAR_ORDER_DATE);
                    order.setOrderDate(inputFormat.parse(orderDate));
                    logger.debug("in orderDao orderTimestamp.getTime()" + inputFormat.format(order.getOrderDate()));
                }

                order.setOrderTotal(outputs.getDouble(orderConstants.ORDER_TOTAL));
                order.setProductTotal(outputs.getDouble(orderConstants.PRODUCT_TOTAL));
                order.setAddOnTotal(outputs.getDouble(orderConstants.ADD_ON_TOTAL));
                order.setServiceFeeTotal(outputs.getDouble(orderConstants.SERVICE_FEE_TOTAL));
                order.setShippingFeeTotal(outputs.getDouble(orderConstants.SHIPPING_FEE_TOTAL));
                order.setDiscountTotal(outputs.getDouble(orderConstants.DISCOUNT_TOTAL));
                order.setTaxTotal(outputs.getDouble(orderConstants.TAX_TOTAL));
                order.setLossPreventionIndicator(outputs.getString(orderConstants.LOSS_PREVENTION_INDICATOR));
                order.setFraudFlag(outputs.getString("FRAUD_INDICATOR"));
                order.setLanguageId(outputs.getString("LANGUAGE_ID"));
                order.setEmailAddress(outputs.getString("EMAIL_ID"));
                
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return order;
    }
    
    /**
     * This method is used to get email address from email id
     */
    
    public String getEmailAddressByEmailId(long emailId) throws Exception
    {
    	DataRequest dataRequest = new DataRequest();
    	CachedResultSet outputs = null;
    	String emailAddress = null;
    	try{
    		
    		logger.info("Email ID : " + emailId);
    		HashMap inputParams = new HashMap();
            inputParams.put("IN_EMAIL_ID", emailId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_EMAIL_ADDRESS_BY_EMAIL_ID");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            while(outputs.next()){
            	emailAddress = outputs.getString("EMAIL_ADDRESS");
            }
    		logger.info("Email Address : "+emailAddress);
    	}catch (Exception e) {
    		logger.error(e);
            throw e;
		}
    	
    	return emailAddress;
    }

    /**
   * This method is a wrapper for the SP_GET_ORIGINAL_ORDER_BILL SP.
   * It populates a OrderBill VO based on the returned record set.
   *
   * @param String - orderDetailId
   * @return OrderBillVO
   */
    public OrderBillVO getOrderBill(String orderDetailId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        OrderBillVO orderBill = new OrderBillVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        try {
            logger.info("getOrder (String orderDetailId (" + orderDetailId +
                         ")) :: OrderBillVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ORDER_DETAIL_ID, orderDetailId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_ORIGINAL_ORDER_BILL");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            if (outputs.next()) {
                // only use the original bill, not any additional bills from later on
                orderBill.setOrderBillId(outputs.getLong("order_bill_id"));
                orderBill.setOrderDetailId(outputs.getLong("order_detail_id"));
                orderBill.setServiceFee(outputs.getDouble("service_fee"));
                orderBill.setShippingTax(outputs.getDouble("shipping_tax"));
                orderBill.setAdditionalBillIndicator(outputs.getString("additional_bill_indicator"));
                orderBill.setProductAmount(outputs.getDouble("product_amount"));
                orderBill.setAddOnAmount(outputs.getDouble("add_on_amount"));
                orderBill.setShippingFee(outputs.getDouble("shipping_fee"));
                orderBill.setDiscountAmount(outputs.getDouble("discount_amount"));
                orderBill.setTax(outputs.getDouble("tax"));
                orderBill.setPdbPrice(outputs.getDouble("pdb_price"));
                isEmpty = false;
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return orderBill;
    }

    /**
    * This method is a wrapper for the SP_GET_INTERNATIONAL_FLORIST and
    * SP_GET_ROUTABLE_FLORISTS SP.  It determine whether to return an
    * international florist or a list of top tier florist and populates
    * a RWDFloristVOList based on the returned record set.  If an international
    * florist is returned, it is the only florist in the list.  Otherwise
    * returns a list of top tier florists.  All of the business logic pertaining
    * to florist filtering is contained in the SP.
    *
    * @param RWDTO
    * @return RWDFloristVOList
    */
    public RWDFloristVOList getRoutableFlorists(RWDTO rwd) throws Exception {
        DataRequest dataRequest = new DataRequest();
        CachedResultSet outputs = null;
        RWDFloristVOList list = new RWDFloristVOList();

        logger.info("getRoutableFlorists (RWDTO rwd) :: RWDFloristVOList");
        if (logger.isDebugEnabled()) {
            logger.info("ZIP_CODE : " + rwd.getZipCode());
            logger.info("PRODUCT_ID : " + rwd.getProduct());
            logger.info("DELIVERY_DATE : " + rwd.getDeliveryDate());
            logger.info("DELIVERY_DATE_END: " + rwd.getDeliveryDateEnd());
            logger.info("SOURCE_CODE: " + rwd.getSourceCode());
            logger.info("ORDER_DETAIL_ID: " + rwd.getOrderDetailId());
            logger.info("DELIVERY_CITY: " + rwd.getDeliveryCity());
            logger.info("DELIVERY_STATE: " + rwd.getDeliveryState());
            logger.info("ORDER_VALUE: " + rwd.getOrderValue());
            logger.info("RETURN_ALL: " +
                         (rwd.isReturnAll() == true ? "Y" : "N"));
        }

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.ZIP_CODE, rwd.getZipCode());
        inputParams.put(orderConstants.PRODUCT_ID, rwd.getProduct());
        inputParams.put(orderConstants.DELIVERY_DATE,
                        rwd.getDeliveryDate() == null ? null :
                        new java.sql.Date(rwd.getDeliveryDate().getTime()));
        inputParams.put(orderConstants.DELIVERY_DATE_END,
                        rwd.getDeliveryDateEnd() == null ? null :
                        new java.sql.Date(rwd.getDeliveryDateEnd().getTime()));
        inputParams.put(orderConstants.SOURCE_CODE, rwd.getSourceCode());
        inputParams.put(orderConstants.ORDER_DETAIL_ID,
                        rwd.getOrderDetailId());
        inputParams.put(orderConstants.DELIVERY_CITY, rwd.getDeliveryCity());
        inputParams.put(orderConstants.DELIVERY_STATE, rwd.getDeliveryState());
        inputParams.put(orderConstants.ORDER_VALUE,
                        new Long(new Double(rwd.getOrderValue()).longValue()).toString());
        inputParams.put(orderConstants.RETURN_ALL,
                        rwd.isReturnAll() == true ? "Y" : "N");
        String overrideStr = null;
        if (rwd.getRWDFlagOverride() != null) {
            overrideStr = (rwd.getRWDFlagOverride().booleanValue() == true) ? "Y" : "N";
        }
        inputParams.put(orderConstants.RWD_OVERRIDE_FLAG, overrideStr);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_ROUTABLE_FLORISTS);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */

         while (outputs.next()) {
            RWDFloristVO rwdFlorist = new RWDFloristVO();
            rwdFlorist.setFloristId(outputs.getString(orderConstants.FLORIST_ID));
            rwdFlorist.setScore(outputs.getInt(orderConstants.SCORE));
            rwdFlorist.setFloristWeight(outputs.getInt(orderConstants.WEIGHT));
            rwdFlorist.setFloristName(outputs.getString(orderConstants.FLORIST_NAME));
            rwdFlorist.setPhoneNumber(outputs.getString(orderConstants.PHONE_NUMBER));
            rwdFlorist.setMercuryFlag(outputs.getString(orderConstants.MERCURY_FLAG));
            rwdFlorist.setSuperFloristFlag(outputs.getString(orderConstants.SUPER_FLORIST_FLAG));
            rwdFlorist.setSundayDeliveryFlag(outputs.getString(orderConstants.SUNDAY_DELIVERY_FLAG));
            rwdFlorist.setCutoffTime(outputs.getString(orderConstants.CUTOFF_TIME));
            rwdFlorist.setStatus(outputs.getString(orderConstants.STATUS));
            rwdFlorist.setCodification(outputs.getString(orderConstants.CODIFICATION_ID));
            rwdFlorist.setCodificationBlockFlag(outputs.getString("FLORIST_BLOCKED") ==
                                                null ? false :
                                                outputs.getString("FLORIST_BLOCKED").equalsIgnoreCase("Y"));
            rwdFlorist.setUsedSequence(outputs.getString("SEQUENCES"));
            rwdFlorist.setPriority(outputs.getInt(orderConstants.PRIORITY));
            rwdFlorist.setDisplayStatus(rwdFlorist.getStatus());
            rwdFlorist.setZipCityFlag(outputs.getString("zip_city_flag"));
            rwdFlorist.setFloristSuspended(outputs.getString("FLORIST_SUSPENDED"));
            rwdFlorist.setMinOrderAmt(outputs.getString("MINIMUM_ORDER_AMOUNT"));

            DataRequest blockRequest = new DataRequest();
            CachedResultSet cr = null;
            inputParams = new HashMap();
            inputParams.put("IN_FLORIST_ID", rwdFlorist.getFloristId());
            blockRequest.setConnection(conn);
            blockRequest.setStatementID("VIEW_FLORIST_BLOCKS");
            blockRequest.setInputParams(inputParams);

            DataAccessUtil blockAccessUtil = DataAccessUtil.getInstance();
            cr = (CachedResultSet)blockAccessUtil.execute(blockRequest);

            if (cr.next()) {
            	String blockType = cr.getString("block_type");
            	Date blockStartDate = cr.getDate("block_start_date");
            	Date blockEndDate = cr.getDate("block_end_date");
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                if (blockType != null) {
                	if (blockType.equalsIgnoreCase("O")) {
                		rwdFlorist.setDisplayStatus("Opt Out - " + sdf.format(blockStartDate));
                	} else {
                		rwdFlorist.setDisplayStatus("Blocked - " + sdf.format(blockStartDate) + "-" + sdf.format(blockEndDate));
                	}
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug(rwdFlorist.getFloristId() + " " + rwdFlorist.getPriority() + " " + rwdFlorist.getCodification() + " " +
                        rwdFlorist.getScore() + " " + rwdFlorist.getFloristWeight() + " " + rwdFlorist.getStatus());
        	    logger.debug("displayStatus: " + rwdFlorist.getDisplayStatus());
            }
            list.add(rwdFlorist);
        }

        return list;
    }

    /**
   * This method is a wrapper for the SP_GET_INTERNATIONAL_FLORIST and
   * SP_GET_ROUTABLE_FLORISTS SP.  It determine whether to return an
   * international florist or a list of top tier florist and populates
   * a RWDFloristVOList based on the returned record set.  If an international
   * florist is returned, it is the only florist in the list.  Otherwise
   * returns a list of top tier florists.  All of the business logic pertaining
   * to florist filtering is contained in the SP.
   *
   * @param OrderDetailVO
   * @return RWDFloristVOList
   */
    public RWDFloristVOList getRoutableFlorists(OrderDetailVO orderDtl,
                                                boolean returnAllFloristFlag,
                                                Boolean rwdFlagOverride) throws Exception {
        DataRequest dataRequestIntl = new DataRequest();
        DataRequest dataRequest = new DataRequest();
        RWDFloristVOList list = new RWDFloristVOList();
        Map outputsIntl = null;
        CachedResultSet outputs = null;

        try {
            logger.info("getRoutableFlorists (OrderDetailVO orderDtl (" +
                         orderDtl.getOrderDetailId() +
                         "), boolean returnAllFloristFlag, Boolean rwdFlagOverride) [process International] :: RWDFloristVOList");
            /* setup store procedure input parameters */
            HashMap inputParamsIntl = new HashMap();
            inputParamsIntl.put(orderConstants.ORDER_DETAIL_ID,
                                new Long(orderDtl.getOrderDetailId()));

            /* build DataRequest object */
            dataRequestIntl.setConnection(conn);
            dataRequestIntl.setStatementID(orderConstants.SP_GET_INTERNATIONAL_FLORIST);
            dataRequestIntl.setInputParams(inputParamsIntl);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtilIntl = DataAccessUtil.getInstance();
            outputsIntl = (Map)dataAccessUtilIntl.execute(dataRequestIntl);

            /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
            String status =
                (String)outputsIntl.get(orderConstants.OUT_STATUS_PARAM);

            if (status != null &&
                status.equalsIgnoreCase(orderConstants.VL_NO)) {
                String message =
                    (String)outputsIntl.get(orderConstants.OUT_MESSAGE_PARAM);
                throw new Exception(message);
            }

            /* populate object */
            String floristCode =
                (String)outputsIntl.get(orderConstants.FLORIST_CODE);
            if (floristCode != null) //return an international florist
            {
                RWDFloristVO rwdFloristVO = new RWDFloristVO();
                rwdFloristVO.setFloristId(floristCode);

                list.add(rwdFloristVO);
            } else //return a list of all florists or the top tier florist
            {
                /* retrieve the customer record associated with the order detail record passed in */
                CustomerVO customer = getCustomer(orderDtl.getRecipientId());
                /* throw exeption if no customer record retrieved */
                if (customer == null) {
                    throw new Exception("No customer record found");
                }

                /* only send first three characters for zip code if country code is canada */
                if (customer.getCountry().equalsIgnoreCase("CA")) {
                    customer.setZipCode(new String(customer.getZipCode().substring(0,
                                                                                   3)));
                    logger.info("Truncating zip code for Canada order: " +
                                 customer.getZipCode());
                }

                /* retrieve the customer record associated with the order detail record passed in */
                OrderBillVO orderBill =
                    getOrderBill(new Long(orderDtl.getOrderDetailId()).toString());
                /* throw exeption if no order bill record retrieved */
                if (orderBill == null) {
                    throw new Exception("No order bill record found");
                }
                
                if (logger.isDebugEnabled()) {
                    logger.info("getRoutableFlorists (OrderDetailVO orderDtl, boolean returnAllFloristFlag) [process Routable] :: RWDFloristVOList");
                    logger.info("ZIP_CODE : " + customer.getZipCode());
                    logger.info("PRODUCT_ID : " + orderDtl.getProductId());
                    logger.info("DELIVERY_DATE : " +
                                 orderDtl.getDeliveryDate());
                    logger.info("DELIVERY_DATE_END: " +
                                 orderDtl.getDeliveryDateRangeEnd());
                    logger.info("SOURCE_CODE: " + orderDtl.getSourceCode());
                    logger.info("ORDER_DETAIL_ID: " +
                                 new Long(orderDtl.getOrderDetailId()));
                    logger.info("DELIVERY_CITY: " + customer.getCity());
                    logger.info("DELIVERY_STATE: " + customer.getState());
                    logger.info("ORDER_VALUE: " +
                                 new Long(new Double(orderBill.getProductAmount()).longValue()).toString());
                    logger.info("RETURN_ALL: " +
                                 (returnAllFloristFlag == true ? "Y" : "N"));
                }

                /* setup store procedure input parameters */
                HashMap inputParams = new HashMap();
                inputParams.put(orderConstants.ZIP_CODE,
                                customer.getZipCode());
                inputParams.put(orderConstants.PRODUCT_ID,
                                orderDtl.getProductId());
                inputParams.put(orderConstants.DELIVERY_DATE,
                                orderDtl.getDeliveryDate() == null ? null :
                                new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
                inputParams.put(orderConstants.DELIVERY_DATE_END,
                                orderDtl.getDeliveryDateRangeEnd() == null ?
                                null :
                                new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
                
				if (orderDtl.getLegacyIDSourceCode() != null
						&& !"".equals(orderDtl.getLegacyIDSourceCode())) {
					inputParams.put(orderConstants.SOURCE_CODE,
							orderDtl.getLegacyIDSourceCode());
				} else {
					inputParams.put(orderConstants.SOURCE_CODE,
							orderDtl.getSourceCode());
				}	
                	
                inputParams.put(orderConstants.ORDER_DETAIL_ID,
                                new Long(orderDtl.getOrderDetailId()).toString());
                inputParams.put(orderConstants.DELIVERY_CITY,
                                customer.getCity());
                inputParams.put(orderConstants.DELIVERY_STATE,
                                customer.getState());
                inputParams.put(orderConstants.ORDER_VALUE,
                                new Long(new Double(orderBill.getProductAmount()).longValue()).toString());
                inputParams.put(orderConstants.RETURN_ALL,
                                returnAllFloristFlag == true ? "Y" : "N");
                String overrideStr = null;
                if (rwdFlagOverride != null) {
                    overrideStr = (rwdFlagOverride.booleanValue() == true) ? "Y" : "N";
                }
                inputParams.put(orderConstants.RWD_OVERRIDE_FLAG, overrideStr);
                logger.info("SourceCode being sent is : "+inputParams.get(orderConstants.SOURCE_CODE));
                /* build DataRequest object */
                dataRequest.setConnection(conn);
                dataRequest.setStatementID(orderConstants.SP_GET_ROUTABLE_FLORISTS);
                dataRequest.setInputParams(inputParams);

                /* execute the store prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

                outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

                /* populate object */
                while (outputs.next()) {
                    RWDFloristVO rwdFlorist = new RWDFloristVO();
                    rwdFlorist.setFloristId(outputs.getString(orderConstants.FLORIST_ID));
                    rwdFlorist.setScore(outputs.getInt(orderConstants.SCORE));
                    rwdFlorist.setFloristWeight(outputs.getInt(orderConstants.WEIGHT));
                    rwdFlorist.setFloristName(outputs.getString(orderConstants.FLORIST_NAME));
                    rwdFlorist.setPhoneNumber(outputs.getString(orderConstants.PHONE_NUMBER));
                    rwdFlorist.setMercuryFlag(outputs.getString(orderConstants.MERCURY_FLAG));
                    rwdFlorist.setSuperFloristFlag(outputs.getString(orderConstants.SUPER_FLORIST_FLAG));
                    rwdFlorist.setSundayDeliveryFlag(outputs.getString(orderConstants.SUNDAY_DELIVERY_FLAG));
                    rwdFlorist.setCutoffTime(outputs.getString(orderConstants.CUTOFF_TIME));
                    rwdFlorist.setStatus(outputs.getString(orderConstants.STATUS));
                    rwdFlorist.setCodification(outputs.getString("PRODUCT_CODIFICATION_ID"));
                    rwdFlorist.setCodificationBlockFlag(outputs.getString("FLORIST_BLOCKED") ==
                                                        null ? false :
                                                        outputs.getString("FLORIST_BLOCKED").equalsIgnoreCase("Y"));
                    rwdFlorist.setUsedSequence(outputs.getString("SEQUENCES"));
                    rwdFlorist.setPriority(outputs.getInt(orderConstants.PRIORITY));
                    rwdFlorist.setZipCityFlag(outputs.getString("zip_city_flag"));
                    rwdFlorist.setFloristSuspended(outputs.getString("FLORIST_SUSPENDED"));
                    rwdFlorist.setMinOrderAmt(outputs.getString("MINIMUM_ORDER_AMOUNT"));
                    rwdFlorist.setDisplayStatus(rwdFlorist.getStatus());

                    DataRequest blockRequest = new DataRequest();
                    CachedResultSet cr = null;
                    inputParams = new HashMap();
                    inputParams.put("IN_FLORIST_ID", rwdFlorist.getFloristId());
                    blockRequest.setConnection(conn);
                    blockRequest.setStatementID("VIEW_FLORIST_BLOCKS");
                    blockRequest.setInputParams(inputParams);

                    DataAccessUtil blockAccessUtil = DataAccessUtil.getInstance();
                    cr = (CachedResultSet)blockAccessUtil.execute(blockRequest);

                    if (cr.next()) {
                    	String blockType = cr.getString("block_type");
                    	Date blockStartDate = cr.getDate("block_start_date");
                    	Date blockEndDate = cr.getDate("block_end_date");
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
                        if (blockType != null) {
                        	if (blockType.equalsIgnoreCase("O")) {
                        		rwdFlorist.setDisplayStatus("Opt Out - " + sdf.format(blockStartDate));
                        	} else {
                        		rwdFlorist.setDisplayStatus("Blocked - " + sdf.format(blockStartDate) + "-" + sdf.format(blockEndDate));
                        	}
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug(rwdFlorist.getFloristId() + " " + rwdFlorist.getPriority() + " " + rwdFlorist.getCodification() + " " +
                                rwdFlorist.getScore() + " " + rwdFlorist.getFloristWeight() + " " + rwdFlorist.getStatus());
                	    logger.debug("displayStatus: " + rwdFlorist.getDisplayStatus());
                    }
                    list.add(rwdFlorist);
                }
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return list;
    }



    /**
   * This method is a wrapper for the SP_INSERT_ORDER_FLORIST_USED SP.
   * It is used to insert a record into the order_florist_used table
   * which is used to keep track of the florists that have been used for
   * a particular order.
   *
   * @param OrderDetailVO
   * @param String - floristId
   * @return none
   */
    public void insertOrderFloristUsed(OrderDetailVO orderDtl, String floristId,
    		String selectionData) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        if (floristId == null) {
            logger.info("Tried to insert null into order florist used table -- Ignoring");
            return;
        }
        try {
            logger.info("insertOrderFloristUsed(" +
                         orderDtl.getOrderDetailId() +
                         ", " + floristId +
                         ", " + selectionData + ")");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ORDER_DETAIL_ID,
                            new Long(orderDtl.getOrderDetailId()));
            inputParams.put(orderConstants.FLORIST_ID, floristId);
            inputParams.put("SELECTION_DATA", selectionData);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_INSERT_ORDER_FLORIST_USED);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map)dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
            String status =
                (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
            if (status != null &&
                status.equalsIgnoreCase(orderConstants.VL_NO)) {
                String message =
                    (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
                throw new Exception(message);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    public String lockOrder(OrderDetailVO orderDtl) throws Exception {
        return lockOrder(new Long(orderDtl.getOrderDetailId()).toString(), orderConstants.ENTITY_TYPE_VL, orderConstants.CRS_ID_VL);
    }

    public String lockOrder(OrderDetailVO orderDtl, String user) throws Exception {
        return lockOrder(new Long(orderDtl.getOrderDetailId()).toString(), orderConstants.ENTITY_TYPE_VL, user);
    }

    public String lockPayment(OrderDetailVO orderDtl, String user) throws Exception {
        return lockOrder(orderDtl.getOrderGuid(), orderConstants.ENTITY_TYPE_VL_PAYMENTS, user, orderConstants.ENTITY_LOCK_LEVEL_ORDERS);
    }

    /**
   * This method is a wrapper for the SP_LOCK_ORDER SP.  It is used to
   * prevent CSRs from trying to route an order by hand while the system
   * is already doing so.
   */
    private String lockOrder(String entityId, String entityType, String user) throws Exception {
        return lockOrder(entityId, entityType, user, null);
    }

    private String lockOrder(String entityId, String entityType, String user, String lockLevel) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        String status = null;
        String message = null;
        String obtainedLock = null;

        try {
            logger.info("lockOrder (OrderDetailVO orderDtl(" + entityId + ") :: String");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ENTITY_TYPE, entityType);
            inputParams.put(orderConstants.ENTITY_ID, entityId);
            inputParams.put(orderConstants.SESSION_ID, user);
            inputParams.put(orderConstants.CRS_ID, "SYS");
            inputParams.put(orderConstants.ORDER_LEVEL, lockLevel);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_UPDATE_CSR_LOCKED_ENTITIES);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map)dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
            status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
            obtainedLock = (String)outputs.get(orderConstants.LOCK_OBTAINED);
            //String lockedCSRId = (String)outputs.get(orderConstants.LOCK_CSR);

            if (status == null || status.equalsIgnoreCase(orderConstants.VL_NO)) {
                obtainedLock = orderConstants.VL_NO;
                message =
                        (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
                if (message.substring(0,
                                      5).equalsIgnoreCase(orderConstants.VL_ERROR)) {
                    throw new Exception(message);
                } else {
                    throw new Exception("Cannot obtain lock:" + message);
                }
            } // else - status is 'Y'. Whether lock is obtained depends on value of obtainedLock.
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }

        return obtainedLock;
    }


    public String unlockOrder(OrderDetailVO orderDtl) throws Exception {
        return unlockOrder(new Long(orderDtl.getOrderDetailId()).toString(), orderConstants.ENTITY_TYPE_VL, orderConstants.CRS_ID_VL);
    }

    public String unlockOrder(OrderDetailVO orderDtl, String user) throws Exception {
        return unlockOrder(new Long(orderDtl.getOrderDetailId()).toString(), orderConstants.ENTITY_TYPE_VL, user);
    }

    public String unlockPayment(OrderDetailVO orderDtl, String user) throws Exception {
        return unlockOrder(orderDtl.getOrderGuid(), orderConstants.ENTITY_TYPE_VL_PAYMENTS, user);
    }

    /**
   * This method is a wrapper for the SP_UNLOCK_ORDER SP.  It is used to
   * allow CSRs to access a particular order once the system is done.
   */
    private String unlockOrder(String entityId, String entityType, String user) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        String status = null;
        String message = null;

        try {
            logger.info("unlockOrder (OrderDetailVO orderDtl (" + entityId + ")) :: String");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ENTITY_TYPE, entityType);
            inputParams.put(orderConstants.ENTITY_ID, entityId);
            inputParams.put(orderConstants.SESSION_ID, user);
            inputParams.put(orderConstants.CRS_ID, "SYS");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_DELETE_CSR_LOCKED_ENTITIES);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map)dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
            status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);

            if (status != null &&
                status.equalsIgnoreCase(orderConstants.VL_NO)) {
                message =
                        (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
                if (message.substring(0,
                                      5).equalsIgnoreCase(orderConstants.VL_ERROR)) {
                    throw new Exception(message);
                }
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }

        return status;
    }
    
    
    public CachedResultSet getOrderCustomerInfo(String orderDetailId) throws Exception
    {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_ORDER_CUSTOMER_INFO");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID",orderDetailId);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
          return rs;
    }


    /**
   * This method is a wrapper for the GET_ORDER_DETAILS SP.
   * It populates an Order Detail VO based on the returned record set.
   *
   * @param String - order detail id
   * @return OrderDetailVO
   */
    public OrderDetailVO getOrderDetail(String orderDetailID) throws Exception {
        DataRequest dataRequest = new DataRequest();
        OrderDetailVO orderDetail = new OrderDetailVO();
        boolean isEmpty = true;
        String legacyId = null;
        CachedResultSet outputs = null;
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

        try {
            logger.info("getOrderDetail (String orderDetailID(" +
                         orderDetailID + ")) :: OrderDetailVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(orderConstants.ORDER_DETAIL_ID,new Long(orderDetailID));

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(orderConstants.SP_GET_ORDER_DETAIL);
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                orderDetail.setOrderDetailId(outputs.getLong(orderConstants.ORDER_DETAIL_ID));
                orderDetail.setDeliveryDate(outputs.getString(orderConstants.DELIVERY_DATE) ==
                                            null ? null :
                                            df.parse(outputs.getString(orderConstants.DELIVERY_DATE)));
                orderDetail.setRecipientId(outputs.getLong(orderConstants.RECIPIENT_ID));
                orderDetail.setProductId(outputs.getString(orderConstants.PRODUCT_ID));
                orderDetail.setQuantity(outputs.getLong(orderConstants.QUANTITY));
                orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
                orderDetail.setColor1(outputs.getString(orderConstants.COLOR_1));
                orderDetail.setColor2(outputs.getString(orderConstants.COLOR_2));
                orderDetail.setSubstitutionIndicator(outputs.getString(orderConstants.SUBSTITUTION_INDICATOR));
                orderDetail.setSameDayGift(outputs.getString(orderConstants.SAME_DAY_GIFT));
                orderDetail.setOccasion(outputs.getString(orderConstants.OCCASION));
                orderDetail.setCardMessage(outputs.getString(orderConstants.CARD_MESSAGE));
                orderDetail.setCardSignature(outputs.getString(orderConstants.CARD_SIGNATURE));
                orderDetail.setSpecialInstructions(outputs.getString(orderConstants.SPECIAL_INSTRUCTIONS));
                orderDetail.setReleaseInfoIndicator(outputs.getString(orderConstants.RELEASE_INFO_INDICATOR));
                orderDetail.setFloristId(outputs.getString(orderConstants.FLORIST_ID));
                orderDetail.setShipMethod(outputs.getString(orderConstants.SHIP_METHOD));
                orderDetail.setShipDate(outputs.getDate(orderConstants.SHIP_DATE));
                orderDetail.setOrderDispCode(outputs.getString(orderConstants.ORDER_DISP_CODE));
                orderDetail.setDeliveryDateRangeEnd(outputs.getString(orderConstants.DELIVERY_DATE_RANGE_END) ==
                                                    null ? null :
                                                    df.parse(outputs.getString(orderConstants.DELIVERY_DATE_RANGE_END)));
                orderDetail.setScrubbedOn(outputs.getDate(orderConstants.SCRUBBED_ON_DATE));
                orderDetail.setScrubbedBy(outputs.getString(orderConstants.USER_ID));
                orderDetail.setOrderGuid(outputs.getString(orderConstants.ORDER_GUID));
                orderDetail.setSourceCode(outputs.getString(orderConstants.SOURCE_CODE));
                orderDetail.setSecondChoiceProduct(outputs.getString("SECOND_CHOICE_PRODUCT"));
                orderDetail.setRejectRetryCount(outputs.getLong("REJECT_RETRY_COUNT"));
                orderDetail.setSizeIndicator(outputs.getString("SIZE_INDICATOR"));
                orderDetail.setSubcode(outputs.getString("SUBCODE"));
                orderDetail.setOpStatus(outputs.getString("OP_STATUS"));
                orderDetail.setCarrierDelivery(outputs.getString("CARRIER_DELIVERY"));
                orderDetail.setCarrierId(outputs.getString("CARRIER_ID"));
                orderDetail.setVenusMethodOfPayment(outputs.getString("METHOD_OF_PAYMENT"));
                orderDetail.setVendorId(outputs.getString("VENDOR_ID"));
                //orderDetail.setPersonalizationData(outputs.getString("PERSONALIZATION_DATA"));
                Clob pdataTemp = outputs.getClob("PERSONALIZATION_DATA");
                if(pdataTemp != null) {
                    orderDetail.setPersonalizationData(pdataTemp.getSubString((long)1, (int) pdataTemp.length()));
                }
                orderDetail.setMilesPointsRedeemed(outputs.getLong("MILES_POINTS_AMT"));
                orderDetail.setOrigBillBilled(OrderConstants.VL_NO.equals(outputs.getString("ORIG_BILL_BILLED")) ? false : true);
                orderDetail.setPersonalGreetingId(outputs.getString("PERSONAL_GREETING_ID"));
                orderDetail.setDeliveryConfirmationStatus(outputs.getString("DELIVERY_CONFIRMATION_STATUS"));
                orderDetail.setAddOnList(this.getAddOns(orderDetailID));
                orderDetail.setMorningDeliveryOrder(OrderConstants.VL_NO.equals(outputs.getString("ORDER_HAS_MORNING_DELIVERY")) ? false : true);
                orderDetail.setAVSAddressId(outputs.getString("AVS_ADDRESS_ID"));
                orderDetail.setPcGroupId(outputs.getString("PC_GROUP_ID"));
                //16012 
                orderDetail.setDerived_vip_flag(outputs.getString("DERIVED_VIP_FLAG"));
                logger.info("Derived vip flag --"+ orderDetail.getDerived_vip_flag());
                legacyId=outputs.getString("LEGACY_ID");
                orderDetail.setLegacyID(legacyId);
                if(legacyId!=null){
                	orderDetail.setLegacyIDSourceCode(OrderService.getLegacyIDSourceCode(legacyId,conn));
                }
                orderDetail.setRecipientZipCode(outputs.getString("ZIP_CODE"));
                
                
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return orderDetail;
    }


    private String getLegacyIDSourceCode(String legacyId) {
    	String legacySourceCode =null;
    	try{
		   ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			String lpsfCheck = configUtil.getFrpGlobalParm("PI_CONFIG","PRMY_BCKP_FLORIST_LEGACY_ID_LOOK_UP");
			if ("Y".equalsIgnoreCase(lpsfCheck)) 
			{

				if (legacyId != null 	&& !"".equals(legacyId)) {
					LegacySourceCode lsc = new LegacySourceCode(this.conn);
					legacySourceCode = lsc.getSourceCode(legacyId);
					return 	legacySourceCode;
				}
			}
    	}catch(Exception e){
    		logger.error(e.getMessage());
    	}
			return null;
	}

	/**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_TRACKING.
   * @throws java.lang.Exception
   * @param trackingVO
   */
    public

    void updateTrackingNumber(OrderTrackingVO trackingVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID",
                        new Long(trackingVO.getOrderDetailId()));
        inputParams.put("IN_TRACKING_NUMBER", trackingVO.getTrackingNumber());
        inputParams.put("IN_TRACKING_DESCRIPTION",
                        trackingVO.getTrackingDescription());
        inputParams.put("IN_CARRIER_ID", trackingVO.getCarrierId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_ORDER_TRACKING");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_DISPOSITION.
   * @throws java.lang.Exception
   * @param updatedBy
   * @param newDisposition
   * @param orderDetailId
   */
    public void updateOrderDisposition(long orderDetailId,
                                       String newDisposition,
                                       String updatedBy) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        logger.info("updateOrderDisposition(long orderDetailId (" +
                     orderDetailId +
                     "), String newDisposition, String updatedBy)");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        inputParams.put("IN_DISPOSITION", newDisposition);
        inputParams.put("IN_UPDATED_BY", updatedBy);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_ORDER_DISPOSITION");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }


    /**
   * This is a wrapper for CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS.
   * @throws java.lang.Exception
   * @param commentsVO
   */
    public void insertComment(CommentsVO commentsVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CUSTOMER_ID", commentsVO.getCustomerId());
        inputParams.put("IN_ORDER_GUID", commentsVO.getOrderGuid());
        inputParams.put("IN_ORDER_DETAIL_ID", commentsVO.getOrderDetailId());
        inputParams.put("IN_COMMENT_ORIGIN", commentsVO.getCommentOrigin());
        inputParams.put("IN_REASON", commentsVO.getReason());
        inputParams.put("IN_DNIS_ID", commentsVO.getDnisId());
        inputParams.put("IN_COMMENT_TEXT", commentsVO.getComment());
        inputParams.put("IN_COMMENT_TYPE", commentsVO.getCommentType());
        inputParams.put("IN_CSR_ID", commentsVO.getCreatedBy());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_INSERT_COMMENTS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This is a wrapper for CLEAN.CUSTOMER_QUERY_PKG.GET_CUSTOMER_EMAIL_INFO.
   * @throws java.lang.Exception
   * @return EmailVO
   * @param companyId
   * @param customerId
   */
    public EmailVO getCustomerEmailInfo(long customerId,
                                        String companyId) throws Exception {
        CachedResultSet results = null;
        EmailVO email = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_CUSTOMER_ID", new Long(customerId));
        inParms.put("IN_COMPANY_ID", companyId);

        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_GET_CUSTOMER_EMAIL_INFO");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        if (results.next()) {
            if (results.getObject(3) != null) {
                email = new EmailVO();
                email.setEmailAddress(results.getObject(3).toString());
            }
        }
        return email;
    }

    /**
     * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_ORDER_TRACKING
   *
   * @throws java.lang.Exception
   * @return OrderTrackingVO
   * @param orderDetailId
   */
    public OrderTrackingVO getTrackingInfo(long orderDetailId) throws Exception {
        CachedResultSet results = null;
        OrderTrackingVO trackingVO = null;
        List resultList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_ORDER_DETAIL_ID", Long.toString(orderDetailId));

        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_GET_ORDER_TRACKING");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        if (results.next()) {
            trackingVO = new OrderTrackingVO();
            trackingVO.setOrderDetailId(Long.parseLong(results.getObject(1).toString()));
            trackingVO.setTrackingNumber(results.getObject(2).toString());
            trackingVO.setCarrierId(results.getObject(3).toString());
            trackingVO.setCarrierName(results.getObject(4).toString());
            trackingVO.setCarrierURL(results.getObject(5).toString());
            trackingVO.setCarrierPhone(results.getObject(6).toString());

            resultList.add(trackingVO);
        }
        return trackingVO;
    }

    public void clearFlorist(String orderDetailId) throws Exception {
        logger.info("clearFlorist(String orderDetailId(" + orderDetailId +
                     "))");
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_NULL_ORDER_DETAILS_FLORIST_ID");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message =
                (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }

    }

    /**
   * This method is a wrapper for the FTD_APPS.GET_TIMEZONE
   *
   * @param String state (2 character state code)
   * @return String timezone
   */
    public String getTimezone(String state) throws Exception {
        DataRequest dataRequest = new DataRequest();
        String result = null;

        try {
            logger.info("getTimezone :: String ");

            /* setup stored procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_STATE_MASTER_ID", state);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_STATE_DETAILS");
            dataRequest.setInputParams(inputParams);

            /* execute the stored procedure */
            CachedResultSet results = null;
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            if (results.next()) {
                Object resultObject = results.getObject(3);
                result = resultObject != null ? resultObject.toString() : null;
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return result;
    }

    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_HOLD.
   * @throws java.lang.Exception
   * @param commentsVO
   */
    public void insertOrderHold(OrderHoldVO orderHoldVO) throws Exception {
        logger.info("insertOrderHold(OrderHoldVO orderHoldVO (" +
                     orderHoldVO.getOrderDetailId() + "))");
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderHoldVO.getOrderDetailId());
        inputParams.put("IN_REASON", orderHoldVO.getHoldReason());
        inputParams.put("IN_REASON_TEXT", orderHoldVO.getHoldReasonText());
        inputParams.put("IN_TIMEZONE",
                        new Integer(orderHoldVO.getTimezone()).toString());
        inputParams.put("IN_AUTH_COUNT",
                        new Integer(orderHoldVO.getAuthCount()).toString());
        inputParams.put("IN_RELEASE_DATE",
                        orderHoldVO.getReleaseDate() == null ? null :
                        new java.sql.Date(orderHoldVO.getReleaseDate().getTime()));
        inputParams.put("IN_STATUS", orderHoldVO.getStatus());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_INSERT_ORDER_HOLD");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_ERROR_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_HOLD.
   * @throws java.lang.Exception
   * @param commentsVO
   */
    public void updateOrderHold(OrderHoldVO orderHoldVO) throws Exception {
        logger.info("updateOrderHold(OrderHoldVO orderHoldVO(" +
                     orderHoldVO.getOrderDetailId() + "))");
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderHoldVO.getOrderDetailId());
        inputParams.put("IN_REASON", orderHoldVO.getHoldReason());
        inputParams.put("IN_REASON_TEXT", orderHoldVO.getHoldReasonText());
        inputParams.put("IN_TIMEZONE",
                        new Integer(orderHoldVO.getTimezone()).toString());
        inputParams.put("IN_AUTH_COUNT",
                        new Integer(orderHoldVO.getAuthCount()).toString());
        inputParams.put("IN_RELEASE_DATE",
                        orderHoldVO.getReleaseDate() == null ? null :
                        new java.sql.Date(orderHoldVO.getReleaseDate().getTime()));
        inputParams.put("IN_STATUS", orderHoldVO.getStatus());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_ORDER_HOLD");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_ERROR_MESSAGE");
            throw new Exception(message);
        }
    }


    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.DELETE_ORDER_HOLD.
   * @throws javayeah.lang.Exception
   * @param String orderDetailId
   */
    public void deleteOrderHold(String orderDetailId) throws Exception {
        logger.info(" deleteOrderHold(String orderDetailId(" + orderDetailId +
                     "))");
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_DELETE_ORDER_HOLD");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_ERROR_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_ORDER_HOLD.
   * @throws java.lang.Exception
   * @return OrderHoldVO
   * @param String orderDetailId
   */
    public OrderHoldVO getOrderHold(String orderDetailId,
                                    String holdType) throws Exception {
        logger.info("getOrderHold(String orderDetailId (" + orderDetailId +
                     "), String holdType)");
        CachedResultSet results = null;
        OrderHoldVO orderHoldVO = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_ORDER_DETAIL_ID", orderDetailId);

        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_GET_ORDER_HOLD");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (results.next()) {
            orderHoldVO = new OrderHoldVO();
            orderHoldVO.setOrderDetailId(results.getString("ORDER_DETAIL_ID"));
            orderHoldVO.setHoldReason(results.getString("REASON"));
            orderHoldVO.setTimezone(results.getInt("TIMEZONE"));
            orderHoldVO.setAuthCount(results.getInt("AUTH_COUNT"));
            orderHoldVO.setReleaseDate(results.getDate("RELEASE_DATE"));
            orderHoldVO.setStatus(results.getString("STATUS"));
            orderHoldVO.setHoldReasonText(results.getString("REASON_TEXT"));
            if (orderHoldVO.getHoldReason().equalsIgnoreCase(holdType)) {
                return orderHoldVO;
            }
        }
        return null;
    }


    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_EARLY_GNADD_HELD_ORDERS.
   * @throws java.lang.Exception
   * @return List
   */
    public List getEarlyGnaddHeldOrders() throws Exception {
        DataRequest dataRequest = new DataRequest();
        List list = new ArrayList();
        try {
            logger.info("getEarlyGnaddHeldOrders :: List ");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_EARLY_GNADD_HELD_ORDERS");

            /* execute the stored procedure */
            CachedResultSet results = null;
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            while (results.next()) {
                OrderHoldVO orderHoldVO = new OrderHoldVO();
                orderHoldVO.setAuthCount(new Integer(results.getString("AUTH_COUNT")).intValue());
                orderHoldVO.setHoldReason(results.getString("HOLD_REASON"));
                orderHoldVO.setOrderDetailId(results.getString("ORDER_DETAIL_ID"));
                orderHoldVO.setReleaseDate(results.getDate("RELEASE_DATE"));
                orderHoldVO.setTimezone(new Integer(results.getString("TIMEZONE")).intValue());
                orderHoldVO.setStatus(results.getString("STATUS"));
                list.add(orderHoldVO);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return list;
    }


    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_GNADD_HELD_ORDERS.
   * @throws java.lang.Exception
   * @return
   */
    public List getGnaddHeldOrders() throws Exception {
        DataRequest dataRequest = new DataRequest();
        List list = new ArrayList();
        try {
            logger.info("getGnaddHeldOrders :: List ");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_GNADD_HELD_ORDERS");

            /* execute the stored procedure */
            CachedResultSet results = null;
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            while (results.next()) {
                OrderHoldVO orderHoldVO = new OrderHoldVO();
                orderHoldVO.setAuthCount(new Integer(results.getString("AUTH_COUNT")).intValue());
                orderHoldVO.setHoldReason(results.getString("HOLD_REASON"));
                orderHoldVO.setOrderDetailId(results.getString("ORDER_DETAIL_ID"));
                orderHoldVO.setReleaseDate(results.getDate("RELEASE_DATE"));
                orderHoldVO.setTimezone(new Integer(results.getString("TIMEZONE")).intValue());
                orderHoldVO.setStatus(results.getString("STATUS"));
                list.add(orderHoldVO);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return list;
    }


    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_PRODUCT_HELD_ORDERS.
   * @throws java.lang.Exception
   * @return LIst
   */
    public List getProductHeldOrders() throws Exception {
        DataRequest dataRequest = new DataRequest();
        List list = new ArrayList();
        try {
            logger.info("getProductHeldOrders :: List ");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_PRODUCT_HELD_ORDERS");

            /* execute the stored procedure */
            CachedResultSet results = null;
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            while (results.next()) {
                OrderHoldVO orderHoldVO = new OrderHoldVO();
                orderHoldVO.setAuthCount(new Integer(results.getString("AUTH_COUNT")).intValue());
                orderHoldVO.setHoldReason(results.getString("HOLD_REASON"));
                orderHoldVO.setOrderDetailId(results.getString("ORDER_DETAIL_ID"));
                orderHoldVO.setReleaseDate(results.getDate("RELEASE_DATE"));
                orderHoldVO.setTimezone(new Integer(results.getString("TIMEZONE")).intValue());
                orderHoldVO.setStatus(results.getString("STATUS"));
                list.add(orderHoldVO);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return list;
    }

    public boolean isCSZAvailable(String zipCode) throws Exception {
        // Always return true since CSZ logic was removed Jan 2017
        return true;
    }

    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_PAYMENTS_BY_ORD_DTL_ID.
   * Returns the original cart payment record for a credit card order.
   * @throws java.lang.Exception
   * @return PaymentVO
   * @param String orderDetailId
   */
    public PaymentVO getFirstPayment(String orderDetailId) throws Exception {
        logger.info("getFirstPayment(String orderDetailId(" +
                     orderDetailId + "))");
        CachedResultSet results = null;
        PaymentVO paymentVO = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_ORDER_DETAIL_ID", orderDetailId);

        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_GET_PAYMENTS_BY_ORD_DTL_ID");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        if (results.next()) {
            paymentVO = new PaymentVO();
            paymentVO.setPaymentId(results.getLong("PAYMENT_ID"));
            paymentVO.setOrderGuid(results.getString("ORDER_GUID"));
            paymentVO.setAdditionalBillId(results.getString("ADDITIONAL_BILL_ID"));
            paymentVO.setPaymentType(results.getString("PAYMENT_TYPE"));
            paymentVO.setCcId(results.getLong("CC_ID"));
            paymentVO.setAuthResult(results.getString("AUTH_RESULT"));
            paymentVO.setAuthNumber(results.getString("AUTH_NUMBER"));
            paymentVO.setAvsCode(results.getString("AVS_CODE"));
            paymentVO.setAcqReferenceNumber(results.getString("ACQ_REFERENCE_NUMBER"));
            paymentVO.setGcCouponNumber(results.getString("GC_COUPON_NUMBER"));
            paymentVO.setAuthOverrideFlag(results.getString("AUTH_OVERRIDE_FLAG"));
            paymentVO.setCreditAmount(results.getDouble("CREDIT_AMOUNT"));
            paymentVO.setDebitAmount(results.getDouble("DEBIT_AMOUNT"));
            paymentVO.setPaymentIndicator(results.getString("PAYMENT_INDICATOR"));
            paymentVO.setRefundId(results.getString("REFUND_ID"));
            paymentVO.setAuthDate(results.getDate("AUTH_DATE"));
            paymentVO.setAuthCharIndicator(results.getString("AUTH_CHAR_INDICATOR"));
            paymentVO.setTransactionId(results.getString("TRANSACTION_ID"));
            paymentVO.setValidationCode(results.getString("VALIDATION_CODE"));
            paymentVO.setAuthSourceCode(results.getString("AUTH_SOURCE_CODE"));
            paymentVO.setResponseCode(results.getString("RESPONSE_CODE"));
            paymentVO.setAafesTicketNumber(results.getString("AAFES_TICKET_NUMBER"));
            paymentVO.setMilesPointsCreditAmt(results.getInt("MILES_POINTS_CREDIT_AMT"));
            paymentVO.setCompanyId(results.getString("COMPANY_ID"));
            paymentVO.setCardinalVerifiedFlag(results.getString("CARDINAL_VERIFIED_FLAG"));
            paymentVO.setPaymentRoute(results.getString("route"));
        }
        return paymentVO;
    }

    /**
   *
   * @throws java.lang.Exception
   * @param paymentVO
   */
    public void updatePayment(PaymentVO paymentVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.info("updatePayment(PaymentVO paymentVO)");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_GUID", paymentVO.getOrderGuid());
        inputParams.put("IN_ADDITIONAL_BILL_ID",
                        paymentVO.getAdditionalBillId());
        inputParams.put("IN_PAYMENT_TYPE", paymentVO.getPaymentType());
        if(paymentVO.getCcId() != 0) {
            inputParams.put("IN_CC_ID", new Long(paymentVO.getCcId()).toString());
        }
        inputParams.put("IN_UPDATED_BY", OrderConstants.CRS_ID_VL);
        inputParams.put("IN_AUTH_RESULT", paymentVO.getAuthResult());
        inputParams.put("IN_AUTH_NUMBER", paymentVO.getAuthNumber());
        inputParams.put("IN_AVS_CODE", paymentVO.getAvsCode());
        inputParams.put("IN_ACQ_REFERENCE_NUMBER",
                        paymentVO.getAcqReferenceNumber());
        inputParams.put("IN_GC_COUPON_NUMBER", paymentVO.getGcCouponNumber());
        inputParams.put("IN_AUTH_OVERRIDE_FLAG",
                        paymentVO.getAuthOverrideFlag());
        inputParams.put("IN_CREDIT_AMOUNT",
                        new Double(paymentVO.getCreditAmount()).toString());
        inputParams.put("IN_DEBIT_AMOUNT",
                        new Double(paymentVO.getDebitAmount()).toString());
        inputParams.put("IN_PAYMENT_INDICATOR",
                        paymentVO.getPaymentIndicator());
        inputParams.put("IN_REFUND_ID", paymentVO.getRefundId());
        inputParams.put("IN_AUTH_DATE",
                        paymentVO.getAuthDate() == null ? null :
                        new java.sql.Date(paymentVO.getAuthDate().getTime()));
        ;
        inputParams.put("IN_AUTH_CHAR_INDICATOR",
                        paymentVO.getAuthCharIndicator());
        inputParams.put("IN_TRANSACTION_ID", paymentVO.getTransactionId());
        inputParams.put("IN_VALIDATION_CODE", paymentVO.getValidationCode());
        inputParams.put("IN_AUTH_SOURCE_CODE", paymentVO.getAuthSourceCode());
        inputParams.put("IN_RESPONSE_CODE", paymentVO.getResponseCode());
        inputParams.put("IN_AAFES_TICKET_NUMBER",
                        paymentVO.getAafesTicketNumber());
        inputParams.put("IN_AP_ACCOUNT_ID", null);
        inputParams.put("IN_AP_AUTH_ID", null);
        inputParams.put("IN_NC_APPROVAL_IDENTITY_ID", null);
        inputParams.put("IN_NC_TYPE_CODE", null);
        inputParams.put("IN_NC_REASON_CODE", null);
        inputParams.put("IN_NC_ORDER_DETAIL_ID", null);
        if(paymentVO.getMilesPointsCreditAmt() != 0) {
            inputParams.put("IN_MILES_POINTS_CREDIT_AMT", String.valueOf(paymentVO.getMilesPointsCreditAmt()));
        }
        if(paymentVO.getMilesPointsDebitAmt() != 0) {
            inputParams.put("IN_MILES_POINTS_DEBIT_AMT", String.valueOf(paymentVO.getMilesPointsDebitAmt()));
        }
        inputParams.put("IN_CSC_RESPONSE_CODE", null);
        inputParams.put("IN_CSC_VALIDATED_FLAG", "N");
        inputParams.put("IN_CSC_FAILURE_CNT", new BigDecimal("0"));
        inputParams.put("IN_WALLET_INDICATOR", null);
        logger.info("Credit Card Auth Provider : OP/OrderDAO : "+paymentVO.getCcAuthProvider());
        inputParams.put("IN_ROUTE", paymentVO.getPaymentRoute());
        inputParams.put("IN_CC_AUTH_PROVIDER", paymentVO.getCcAuthProvider());
        inputParams.put("IN_INITIAL_AUTH_AMOUNT", new Double(paymentVO.getCreditAmount()).toString());
        inputParams.put("IN_AUTH_TRANSACTION_ID", paymentVO.getAuthTransactionId());
        inputParams.put("IN_MERCHANT_REF_ID", paymentVO.getMerchantRefId());
        String delimitedPaymentExtFields = null;
        if(paymentVO.getPaymentExtMap()!=null && paymentVO.getPaymentExtMap().size()>0){
        	Map<String,Object> paymentExtInfo = new HashMap(paymentVO.getPaymentExtMap());
        	Map<String,Object> cardSpecificDetailsMap = (Map<String, Object>) paymentExtInfo.remove(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL);
        	if(cardSpecificDetailsMap != null && cardSpecificDetailsMap.size()>0){
        		paymentExtInfo.putAll(cardSpecificDetailsMap);
        	}
        	delimitedPaymentExtFields = FieldUtils.getDelimitedStringFromMap(paymentExtInfo,"###","&&&");
        }                
        inputParams.put("IN_PAYMENT_EXT_INFO",delimitedPaymentExtFields);
//        AUTHORIZATION_TRANSACTION_ID
        inputParams.put("IN_IS_VOICE_AUTH", null);
        inputParams.put("IN_CARDINAL_VERIFIED_FLAG", paymentVO.getCardinalVerifiedFlag());
        inputParams.put("IO_PAYMENT_ID", new Long(paymentVO.getPaymentId()).toString());
        
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_PAYMENTS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_ZIP_QUEUE_ROUTABLE.
   * @throws java.lang.Exception
   * @return LIst
   */
    public List getRoutableZipQueue() throws Exception {
        DataRequest dataRequest = new DataRequest();
        List list = new ArrayList();
        boolean emptyList = true;

        logger.info("getRoutableZipQueue :: List ");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_ZIP_QUEUE_ROUTABLE");

        /* execute the stored procedure */
        CachedResultSet results = null;
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        while (results.next()) {
            String detail = results.getString("ORDER_DETAIL_ID");
            list.add(detail);
            emptyList = false;
        }

        if (emptyList) {
            return null;
        } else {
            return list;
        }
    }

    public String getZipQueueMessageId(OrderDetailVO detail) throws Exception {
        DataRequest dataRequest = new DataRequest();

        logger.info("getQueueMessageId :: List ");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_NUMBER", detail.getOrderDetailId() + "");
        inputParams.put("IN_REQUEST_TYPE", "ZIP");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_QUEUE_RECORD_BY_ORDER");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        CachedResultSet results = null;
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        if (results.next()) {
            return results.getString("MESSAGE_ID");
        } else {
            return null;
        }
    }

    public void deleteZipQueueMessageId(String messageId) throws Exception {
        DataRequest dataRequest = new DataRequest();

        logger.info("deleteZipQueueMessageId :: List ");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_MESSAGE_ID", messageId);
        inputParams.put("IN_CSR_ID", "SYS");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_DELETE_QUEUE_RECORD");
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        HashMap outputs = null;
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (HashMap)dataAccessUtil.execute(dataRequest);
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_ERROR_MESSAGE");
            throw new Exception(message);
        }
    }

    /**
   *
   * @throws java.lang.Exception
   * @param paymentVO
   */
    public void updateFloristForwardFlags() throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.info("updateFloristForwardFlags()");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_FLORIST_MESSAGE_FORWARDING");

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }

  /**
   * findOrderNumber
   *
   * @param a_orderNum
   * @throws java.lang.Exception
   */
    public HashMap findOrderNumber(String a_orderNum) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_FIND_ORDER_NUMBER");
        dataRequest.addInputParam("IN_ORDER_NUMBER", a_orderNum);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        Date startTime = new Date();
        HashMap searchResults = (HashMap)dataAccessUtil.execute(dataRequest);
        long timeInMs = (System.currentTimeMillis() - startTime.getTime());
        logger.info("***STRESS TEST***  - findOrderNumber() - CLEAN.ORDER_QUERY_PKG.FIND_ORDER_NUMBER took " +
                     timeInMs + " milliseconds.");

        return searchResults;
    }

    /**
     * getOriginByGuid
     *
     * @param orderGuid String
     * @return String
     * @throws java.lang.Exception
     */
     public String getOriginByGuid(String orderGuid) throws Exception
    {
          String origin = null;

          Map paramMap = new HashMap();

          paramMap.put("IN_ORDER_GUID", orderGuid);

          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("OP_GET_ORIGIN_BY_GUID");
          dataRequest.setInputParams(paramMap);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

          if(crs.next())
          {
              origin = crs.getString("ORIGIN_ID");
          }

          return origin;
    }

    /**
     * Checks if order is fully refunded
     * @param orderDetailId
     * @return
     * @throws Exception
     */
    public String isOrderFullyRefunded(String orderDetailId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_IS_ORDER_FULLY_REFUNDED");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String searchResults = (String) dataAccessUtil.execute(dataRequest);
      return searchResults;
    }


    /**
     * Checks if all other items in cart is out of scrub.
     * @param orderDetailId
     * @return
     * @throws Exception
     */
    public boolean isCartOutOfScrub(String orderDetailId) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_IS_CART_OUT_OF_SCRUB");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String searchResults = (String) dataAccessUtil.execute(dataRequest);
      logger.info("isCartOutOfScrub(" + orderDetailId + "):" + searchResults);
      return searchResults.equals("Y")? true : false;
    }

    /**
     * Updates Miles Verified flag in order detail extensions table.
     * If the record does not exist, insert it.
     * @param orderDetailId
     * @param milesVerifiedFlag
     * @throws Exception
     */
    public void updateMilesVerifiedFlag(String orderDetailId, String milesVerifiedFlag) throws Exception
    {
      logger.info("updateMilesVerifiedFlag(" + orderDetailId + ", " + milesVerifiedFlag +")");
      Map outputs = null;
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_UPDATE_ORDER_DETAIL_EXTENSIONS");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      dataRequest.addInputParam("IN_INFO_NAME", OrderConstants.INFO_NAME_MILES_VERIFIED);
      dataRequest.addInputParam("IN_INFO_DATA", milesVerifiedFlag);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (HashMap) dataAccessUtil.execute(dataRequest);
      String status = (String)outputs.get("OUT_STATUS");
      if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
      }
    }

    /**
     * Retrieves the order detail extension info data based on id and info name.
     * @param orderDetailId
     * @return boolean
     * @throws Exception
     */
    public String getOrderDetailExtData(String orderDetailId, String infoName) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_ORDER_DETAIL_EXT_DATA");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      dataRequest.addInputParam("IN_INFO_NAME", infoName);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      String searchResults = (String) dataAccessUtil.execute(dataRequest);
      logger.info("getOrderDetailExtData(" + orderDetailId + "):" + searchResults);
      return searchResults;
    }

    /**
     * Checks if the order detail has gone through miles verification.
     * @param orderDetailId
     * @return
     * @throws Exception
     */
    public boolean isMilesVerified(String orderDetailId) throws Exception
    {
        String verifiedFlag = getOrderDetailExtData(orderDetailId, OrderConstants.INFO_NAME_MILES_VERIFIED);
        logger.debug("isMilesVerified(String orderDetailId("+orderDetailId+")): " + verifiedFlag);
        return OrderConstants.VL_YES.equals(verifiedFlag)? true : false;
    }

    /**
     * Updates shopping cart accounting status to the paymentType.
     * @param orderDetailId
     * @param paymentType
     * @throws Exception
     */
    public void updateCartBilled(String orderDetailId, String paymentType) throws Exception
    {

      Map outputs = null;
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_UPDATE_CART_BILLED");
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
      dataRequest.addInputParam("IN_PAYMENT_TYPE", paymentType);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      outputs = (HashMap) dataAccessUtil.execute(dataRequest);
      String status = (String)outputs.get("OUT_STATUS");
      logger.info("updateCartBilled(" + orderDetailId + "):" + status);
      if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message = (String)outputs.get("OUT_MESSAGE");
            throw new Exception(message);
      }
    }

    /**
     * Get a list of orders requiring delivery confirmations
     *
     * @return list of Mercury Messages
     */
    public List getPendingDeliveryConfirmations() throws Exception {
        logger.debug("getPendingDeliveryConfirmations :: List ");

        List list = new ArrayList();
        CachedResultSet output = null;
        DataRequest dataRequest = new DataRequest();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_PENDING_DELIVERY_CONFIRMATIONS");

        /* execute the stored prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (output.next()) {
            MercuryVO vo = new MercuryVO();
            vo.setMercuryId(output.getString("MERCURY_ID"));
            vo.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
            vo.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
            vo.setMercuryStatus(output.getString("MERCURY_STATUS"));
            vo.setMessageType(output.getString("MSG_TYPE"));
            vo.setOutboundId(output.getString("OUTBOUND_ID"));
            vo.setSendingFlorist(output.getString("SENDING_FLORIST"));
            vo.setFillingFlorist(output.getString("FILLING_FLORIST"));
            vo.setReferenceNumber(output.getString("REFERENCE_NUMBER"));

            list.add(vo);
        }
        return list;
    }

    /**
     * Get a list of orders requiring delivery confirmations
     *
     * @return list of Mercury Messages
     */
    public List getNonConfirmedOrders() throws Exception {
        logger.debug("getPendingDeliveryConfirmations :: List ");

        List list = new ArrayList();
        CachedResultSet output = null;
        DataRequest dataRequest = new DataRequest();

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_DCON_QUEUE_ORDERS");

        /* execute the stored prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (output.next()) {
            QueueVO qvo = new QueueVO();
            qvo.setOrderDetailId(output.getString("order_detail_id"));
            qvo.setExternalOrderNumber(output.getString("external_order_number"));
            qvo.setMasterOrderNumber(output.getString("master_order_number"));
            qvo.setOrderGuid(output.getString("order_guid"));
            qvo.setMercuryId(output.getString("fulfill_id"));
            if(output.getString("email_id") != null){
              qvo.setHasEmail(true);
            }
            else
              qvo.setHasEmail(false);
            list.add(qvo);
        }

        return list;

    }

    public void updateDeliveryConfirmationStatus(long orderDetailId, String newStatus, String updatedBy) throws Exception {
        logger.debug("updateDeliveryConfirmationStatus");

        Map output = null;
        DataRequest dataRequest = new DataRequest();

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        inputParams.put("IN_STATUS", newStatus);
        inputParams.put("IN_CSR_ID", updatedBy);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_DCON_STATUS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        output = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)output.get(orderConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
            String message = (String)output.get(orderConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }

    }

    /**
     * This method retrieves default sending florist number from
     * ftd_apps.company_master table for given order detail id
     *
     * @param String orderDetailId
     * @return String default sending florist number
     */
      public String retrieveDefaultFloristId(String orderDetailId) throws Exception {
          DataRequest dataRequest = new DataRequest();
          String defaultSendingFloristNumber = "";
          CachedResultSet outputs = null;

          logger.info("retrieveDefaultFloristId ( String orderDetailId :" +
        		  orderDetailId + " ):: String");
          /* setup store procedure input parameters */
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_SENDING_FLORIST_BY_ID");
          dataRequest.setInputParams(inputParams);

          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

          /* populate object */
          while (outputs.next()) {
              defaultSendingFloristNumber = outputs.getString("SENDING_FLORIST_NUMBER");
          }
          return defaultSendingFloristNumber;
      }
      
      /**
       * This method is used to find out whether the given order number
       * exists in account.account_program_details.
       */
      public boolean isOrderExists(String externalOrdNum) throws Exception{
    	  DataRequest dataRequest = new DataRequest();
          Map outputs = null;
          String flag = "N";
          HashMap inputParams = new HashMap();
          inputParams.put("IN_EXTERNAL_ORDER_NUMBER", externalOrdNum);

          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("IS_ACCOUNT_EXISTS");
          dataRequest.setInputParams(inputParams);

          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (Map)dataAccessUtil.execute(dataRequest);
          flag = (String)outputs.get("OUT_RESULT_FLAG");
          if(flag.equalsIgnoreCase("Y"))
        	  return true;
          else
        	  return false;
      }
      
      public CachedResultSet getProgramByOrderDetailId(long orderDetailId) throws Exception
      {
    	  DataRequest dataRequest = new DataRequest();
          CachedResultSet outputs = null;
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("GET_PROGRAM_BY_ORDER_DETAIL_ID");
          dataRequest.setInputParams(inputParams);

          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
          return outputs;
      }
      
      public String addProgramToAccount(long accountMasterId, String programName, 
    		  String programStatus, String autoRenewFlag, java.sql.Date expDate, 
    		  java.sql.Date startDate, String extOrdNumber, String updatedBy) throws Exception
      {
    	  BigDecimal accountProgramId;
    	  DataRequest dataRequest = new DataRequest();
          Map outputs = null;
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ACCOUNT_MASTER_ID", accountMasterId);
          inputParams.put("IN_PROGRAM_NAME", programName);
          inputParams.put("IN_PROGRAM_STATUS", programStatus);
          inputParams.put("IN_AUTO_RENEW_FLAG", autoRenewFlag);
          inputParams.put("IN_EXPIRATION_DATE", expDate);
          inputParams.put("IN_START_DATE", startDate);
          inputParams.put("IN_EXTERNAL_ORDER_NUMBER", extOrdNumber);
          inputParams.put("IN_UPDATED_BY", updatedBy);
          
          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("ADD_PROGRAM_TO_ACCOUNT");
          dataRequest.setInputParams(inputParams);

          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (Map)dataAccessUtil.execute(dataRequest);
         if(outputs.get("OUT_STATUS") !=null && outputs.get("OUT_STATUS").equals("Y"))
         { 
	          accountProgramId = (BigDecimal)outputs.get("OUT_ACCOUNT_PROGRAM_ID");
	          if(accountProgramId != null)
	          {
	        	  return accountProgramId.toString();
	          }
	          else
	          {
	        	  logger.error("Failed to get account program id");
	        	  throw new Exception("Failed to add program to account.");
	          }  
         }else
         {
        	 logger.error("Failed to add program to account.");
        	 throw new Exception("Failed to add program to account.");
         }
      }
      
	public void doUpdateApCaptureTxt(Connection con, String paymentId,
			String apCaptureTxt) throws Throwable {
		logger.debug("Entering doUpdateApCaptureTxt");

		DataRequest request = new DataRequest();
		String status = "";
		try {
			/* setup store procedure input parameters */
			HashMap inputParams = new HashMap();
			inputParams.put("IN_PAYMENT_ID", paymentId);
			inputParams.put("IN_AP_CAPTURE_TXT", apCaptureTxt);
			inputParams.put("IN_UPDATED_BY", OrderConstants.UPDATED_BY_VL);

			logger.info("in_payment_id=" + paymentId);
			logger.info("in_ap_catpure_txt" + apCaptureTxt);

			// build DataRequest object
			request.setConnection(con);

			request.setInputParams(inputParams);
			request.setStatementID("UPDATE_PMT_AP_CAPTURE_TXT");

			// get data
			DataAccessUtil dau = DataAccessUtil.getInstance();
			Map outputs = (Map) dau.execute(request);
			request.reset();
			status = (String) outputs.get("OUT_STATUS");
			if (status.equals("N")) {
				throw new SQLException((String) outputs.get("OUT_MESSAGE"));
			}
			logger.debug("Exiting doUpdateApCaptureTxt");
		} finally {
		}
	} 
      
    public boolean isFloristBlocked(String floristId, Date deliveryDate) {
        logger.info("isFloristBlocked");
        boolean returnVal = false;
        
        try {
            DataRequest dataRequest = new DataRequest();
            HashMap inParms = new HashMap();

            inParms.put("IN_FLORIST_ID", floristId);
            inParms.put("IN_DELIVERY_DATE", new java.sql.Date(deliveryDate.getTime()));

            dataRequest.setConnection(this.conn);
            dataRequest.setStatementID("IS_FLORIST_BLOCKED");
            dataRequest.setInputParams(inParms);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            String val = (String) dataAccessUtil.execute(dataRequest);
            if (val != null && val.equalsIgnoreCase("Y")) {
            	returnVal = true;
            }
    	
        } catch (Exception e) {
        	logger.error(e);
        }
        
        logger.info("returnVal: " + returnVal);
        return returnVal;
    }
    
    /**
     * This will update ship method and ship date on order detail record.
     * @throws java.lang.Exception
     * @param String orderDetailId
     * @param String shipMethod
     * @param Date shipDate 
     * @param Date deliveryDate
     */
      public void updateOrderShippingInfo(long orderDetailId, String shipMethod, Date shipDate, Date deliveryDate) throws Exception {
          DataRequest dataRequest = new DataRequest();
          Map outputs = null;

          /* setup store procedure input parameters */
          HashMap inputParams = new HashMap();
          inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
          inputParams.put("IN_SHIP_METHOD", shipMethod);
          inputParams.put("IN_SHIP_DATE", new java.sql.Date(shipDate.getTime()));
          inputParams.put("IN_DELIVERY_DATE", deliveryDate == null? null : new java.sql.Date(deliveryDate.getTime()));
          inputParams.put("IN_UPDATED_BY", orderConstants.UPDATED_BY_VL);

          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("UPDATE_ORDER_SHIPPING_INFO");
          dataRequest.setInputParams(inputParams);

          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          outputs = (Map)dataAccessUtil.execute(dataRequest);

          /* read store prodcedure output parameters to determine
         * if the procedure executed successfully */
          String status = (String)outputs.get(orderConstants.OUT_STATUS_PARAM);
          if (status != null && status.equalsIgnoreCase(orderConstants.VL_NO)) {
              String message =
                  (String)outputs.get(orderConstants.OUT_MESSAGE_PARAM);
              throw new Exception(message);
          }
      }
      
      public String getPCGroupIdByOrderDetailId(String orderDetailId) throws Exception{
    	  DataRequest dataRequest = new DataRequest();
          CachedResultSet outputs = null;
          String groupId = null;
          if(orderDetailId == null){
        	  return null;
          }
                    
          /* build DataRequest object */
          dataRequest.setConnection(conn);
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
          dataRequest.setStatementID("GET_PCGROUPID_BY_ORDERDETAILID");
          
          /* execute the store prodcedure */
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          String pcGroupId = (String) dataAccessUtil.execute(dataRequest);

          
          
          return pcGroupId;

      }
      
      /**
       * getSendFloristPdbPriceFlag
       *
       * @param orderGuid String
       * @return String
       * @throws java.lang.Exception
       */
       public String getSendFloristPdbPriceFlag(String orderGuid) throws Exception
      {    	   
    	   logger.info("Getting Flag details to send Pdb price to Florist for order guid -" + orderGuid);
            String sendFloristPdbPrice = "";

            Map paramMap = new HashMap();

            paramMap.put("IN_ORDER_GUID", orderGuid);

            try{
            	DataRequest dataRequest = new DataRequest();
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("OP_GET_SEND_FLORIST_PDB_PRICE_FLAG_BY_GUID");
                dataRequest.setInputParams(paramMap);

                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

                if(crs.next())
                {
                	logger.info("Setting value from DB");
                    sendFloristPdbPrice = crs.getString("SEND_FLORIST_PDB_PRICE");
                }
            } catch (Exception e){
            	logger.error(e);
            } 
            logger.info("Send Florist Pdb Price Flag for order :" + sendFloristPdbPrice);
            
            return sendFloristPdbPrice;
      }
       
       public boolean isFEMOE(String orderDetailId) throws Exception{
    	   logger.info("Checking if the order is a FEMOE or not for order detail id: " + orderDetailId);
    	   DataRequest dataRequest = new DataRequest();
           String status = null;

           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

           /* build DataRequest object */
           dataRequest.setConnection(conn);
           dataRequest.setStatementID("CHECK_FEMOE");
           dataRequest.setInputParams(inputParams);

           /* execute the store prodcedure */
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           status = (String) dataAccessUtil.execute(dataRequest);

           if (status != null && status.equalsIgnoreCase(orderConstants.VL_YES)) {
               return true;
           }
    	   
           logger.info("Order detail Id: " + orderDetailId + " is not a FEMOE");
    	   return false;
       }
       
       public MercuryVO getLastFTD(String orderDetailId) throws Exception{
    	   logger.info("Getting last FTD message for order detail id: " + orderDetailId);
    	   DataRequest dataRequest = new DataRequest();
           Map outputs = null;
           CachedResultSet results = null;
           MercuryVO lastFTD = null;

           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

           /* build DataRequest object */
           dataRequest.setConnection(conn);
           dataRequest.setStatementID("GET_LAST_FTD_FROM_REF_NUM");
           dataRequest.setInputParams(inputParams);

           /* execute the store prodcedure */
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
           
           if (results.next()) {
               lastFTD = new MercuryVO();
               
               lastFTD.setFirstChoice(results.getString("FIRST_CHOICE"));
               lastFTD.setSecondChoice(results.getString("SECOND_CHOICE"));
               lastFTD.setCardMessage(results.getString("CARD_MESSAGE"));
               lastFTD.setOccasion(results.getString("OCCASION"));
               lastFTD.setPrice(Double.valueOf(results.getString("PRICE")));
               lastFTD.setSpecialInstructions(results.getString("SPECIAL_INSTRUCTIONS"));
               lastFTD.setOperator(results.getString("OPERATOR"));
           }
    	   
    	   return lastFTD;
       }
       
       public Calendar getRecipientTime(long orderDetailId, Calendar currentTime) throws Exception {
    	   logger.info("getRecipientTime()");
    	   Calendar returnTime = currentTime;
    	   
    	   DataRequest dataRequest = new DataRequest();
           Map outputs = null;

           /* setup store procedure input parameters */
           HashMap inputParams = new HashMap();
           inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
           inputParams.put("IN_TIME", new java.sql.Timestamp(currentTime.getTime().getTime()));

           /* build DataRequest object */
           dataRequest.setConnection(conn);
           dataRequest.setStatementID("GET_RECIPIENT_TIME");
           dataRequest.setInputParams(inputParams);

           /* execute the store prodcedure */
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           CachedResultSet results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
           if (results.next()) {
        	   try {
        		   returnTime.setTime(results.getDate("return_time"));
        	   } catch (Exception e) {
        		   logger.error("Invalid return time, using current time");
        	   }
           }
           return returnTime;
    	   
       }
       
       
       public List<String> getQueueOrderIDs() {
   		
   		DataRequest dataRequest = new DataRequest();
   		HashMap inputParams = new HashMap();
   	    CachedResultSet rs = null;
   	    List<String> orderDetailsIDs = new ArrayList<String>();
   	    String queuesList = "";
   	    
   	    try {
  	      
   	      /* build DataRequest object */
   	      dataRequest.setConnection(conn);
   	      dataRequest.setStatementID("GET_BULK_PHOENIX_ELGBLE_ORDERS");
   	      dataRequest.setInputParams(inputParams);
   	      
   	      /* execute the store prodcedure */
   	      DataAccessUtil dataAccessUtil;
   		
   			dataAccessUtil = DataAccessUtil.getInstance();
   			rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
   		} catch (Exception e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}    
   	     
   	      while (rs.next())
   	      {     
   	    	StringBuffer sb = new StringBuffer();
       		sb.append(rs.getString("order_detail_id"));
       		sb.append("|");
       		sb.append(rs.getString("queue_type"));
       		sb.append("|");
       		sb.append(rs.getString("mercury_order_number"));
       		orderDetailsIDs.add(sb.toString());
   	      }
   	      
   	      return orderDetailsIDs;
   	}
       
   public String getLatestMercuryNumberForOrder(String orderDetailId) throws Exception
 	  {
 	     // get the recent mercury order number for this order detail Id
 	     DataRequest dataRequest = new DataRequest();
 	     dataRequest.setConnection(conn);
 	     dataRequest.setStatementID("GET_MERCURY_ORDER_NUMBER");
 	     dataRequest.addInputParam("IN_ORDER_DETAIL_ID", String.valueOf(orderDetailId));
 	     String mercuryOrderNumber = null;
 	     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 	        
 	     CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
 	     while (outputs.next())
 	     { 	       
 	       mercuryOrderNumber = outputs.getString("mercury_order_number");
 	     }
 	     return mercuryOrderNumber;
 	  }

public String getLegacySourceCode(String orderDetailId) throws Exception{
	
	 DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
     dataRequest.setStatementID("GET_LEGACY_SOURCE_CODE");
     dataRequest.addInputParam("IN_ORDER_DETAIL_ID", String.valueOf(orderDetailId));
     String legacySourceCode = null;
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
     String sourceCode = (String) dataAccessUtil.execute(dataRequest);
     if(sourceCode != null){
       legacySourceCode = sourceCode;       
     }
     
     return legacySourceCode;

	}

	public BillTo getCcFNameLNameEmail(String customerId) throws Exception
	{
	  CachedResultSet crs = null;
	  BillTo billTo = new BillTo();
	  logger.info(" populateCreditCardFnameLastName(customerId (" + customerId + " )");
	  try
	  {
	    DataRequest dataRequest = new DataRequest();
	    dataRequest.reset();
	    dataRequest.setConnection(conn);
	    dataRequest.setStatementID("GET_CREDITCARD_FNAME_LNAME");
	    dataRequest.addInputParam("IN_CUSTOMER_ID", customerId);
	    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
	
	    if( crs != null && crs.next())
	    {
	    	billTo.setFirstName(crs.getString("FIRST_NAME"));
	    	billTo.setLastName(crs.getString("LAST_NAME"));
	    	billTo.setEmail(crs.getString("EMAIL_ADDRESS"));
	    	billTo.setAddress1(crs.getString("ADDRESS_1"));
	    	billTo.setAddress2(crs.getString("ADDRESS_2"));
	    	billTo.setCity(crs.getString("CITY"));
	    	billTo.setState(crs.getString("STATE"));
	    	billTo.setZipCode(crs.getString("ZIP_CODE"));
	    	billTo.setCountry(crs.getString("COUNTRY"));
	      }
	  }
	  catch (Exception ex)
	  {
	    logger.info(ex);
	    throw ex;
	  }
	  return billTo;
	}
	
	public String getOrdersOrigin(String orderGuid) throws Exception
    {
      CachedResultSet crs = null;
      String origin = "";
      logger.info(" getOrdersOrigin(orderGuid (" + orderGuid + " )");
      try
      {
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDERS_ORIGIN");
        dataRequest.addInputParam("IN_ORDER_GUID", orderGuid);
        crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
    
        if( crs != null && crs.next())
        {
        	origin = crs.getString("ORIGIN_ID");
        }
      }
      catch (Exception ex)
      {
        logger.info(ex);
        throw ex;
      }
      return origin;
  }

	/**
	 * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP. It populates a
	 * global parameter VO based on the returned record set.
	 *
	 * @param String
	 *            - context
	 * @param String
	 *            - name
	 * @return GlobalParameterVO
	 */
	public List<GlobalParameterVO> getLikeGlobalParameter(String context, String nameLike) throws Exception {
		DataRequest dataRequest = new DataRequest();
		List<GlobalParameterVO> params = new ArrayList();
		boolean isEmpty = true;
		CachedResultSet crs;

		try {
			logger.info("getLikeGlobalParameter (String context (" + context + "), String name like (" + nameLike
					+ ")) :: GlobalParameterVO");
			/* setup store procedure input parameters */
			HashMap inputParams = new HashMap();
			inputParams.put(orderConstants.CONTEXT, context);
			inputParams.put(orderConstants.NAME, nameLike);

			/* build DataRequest object */
			dataRequest.setConnection(conn);
			dataRequest.setStatementID(orderConstants.SP_GET_GLOBAL_PARM_LIKE_VALUES);
			dataRequest.setInputParams(inputParams);

			/* execute the store procedure */
			DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

			while (crs != null && crs.next()) {
				GlobalParameterVO param = new GlobalParameterVO();
				param.setName(crs.getString("NAME"));
				param.setValue(crs.getString("VALUE"));
				params.add(param);
			}
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
		if (params.size() == 0) {
			return null;
		}
		return params;
	}

}

