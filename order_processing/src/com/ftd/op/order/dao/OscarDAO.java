package com.ftd.op.order.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Date;

import com.ftd.op.order.vo.OscarOrderInfoVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class OscarDAO {
    private Connection conn;
    private Logger logger;
    private static String DB_FLORIST_SELECTED_BY_OSCAR = "oscar";
    private static String DB_FLORIST_SELECTED_BY_APOLLO = "apollo";

    public OscarDAO(Connection conn)
    {
      this.conn = conn;
      logger = new Logger("com.ftd.op.oscar.dao.OscarDAO");
    }

    public OscarOrderInfoVO getOscarOrderDetails(String sourceCode, Date deliveryDate, String orderDetailId) throws Exception 
    {
      OscarOrderInfoVO ooivo = null;
      CachedResultSet rs = null;

      if (logger.isDebugEnabled()) {
          logger.debug("getOscarOrderDetails for orderDetailId: " + orderDetailId);
      }
      DataRequest dataRequest = new DataRequest();
      dataRequest.reset();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("OP_GET_OSCAR_ORDER_DETAILS");
      dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
      dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

      Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
      rs = (CachedResultSet)outputs.get("OUT_CUR");

      if (rs != null) {
        if (rs.next()) {
          ooivo = new OscarOrderInfoVO();
          ooivo.setOrderDetailId(orderDetailId);
          ooivo.setDeliveryDate(deliveryDate);
          ooivo.setLatitude(rs.getString("LATITUDE"));
          ooivo.setLongitude(rs.getString("LONGITUDE"));
          ooivo.setScenarioGroup(rs.getString("OSCAR_SCENARIO_GROUP_NAME"));
          ooivo.setSendingFlorist(rs.getString("SENDING_FLORIST"));
          if ("Y".equalsIgnoreCase(rs.getString("MATCHED_SOURCE_CODE"))) {
              ooivo.setSourceCodeCriteriaMatched(true);
          }
          long matched_zip = rs.getLong("MATCHED_ZIP");
          if (matched_zip > 0) {
              ooivo.setZipCriteriaMatched(true);
          }
          long matched_city_state = rs.getLong("MATCHED_CITY_STATE");
          if (matched_city_state > 0) {
              ooivo.setCityStateCriteriaMatched(true);
          }
          if (logger.isDebugEnabled()) {
              logger.debug("getOscarOrderDetails dump (lat,long,group,sourcecode,zip,city): " + 
                      rs.getString("LATITUDE") + "," + rs.getString("LONGITUDE") + "," +
                      rs.getString("OSCAR_SCENARIO_GROUP_NAME") + "," + rs.getString("MATCHED_SOURCE_CODE") + "," +
                      rs.getString("MATCHED_ZIP") + "," + rs.getString("MATCHED_CITY_STATE")
                      );
          }
        }
      }
      return ooivo;
    }
    
    
    public String logOscarFloristSelection(String orderDetailId, String floristList, String selectedFlorist, String oscarReqId, String oscarReason) throws Exception 
    {
        return logFloristSelection(orderDetailId, floristList, selectedFlorist, DB_FLORIST_SELECTED_BY_OSCAR, oscarReqId, oscarReason);
    }
    
    public String logApolloFloristSelection(String orderDetailId, String floristList, String selectedFlorist, String oscarReason) throws Exception 
    {
        return logFloristSelection(orderDetailId, floristList, selectedFlorist, DB_FLORIST_SELECTED_BY_APOLLO, null, oscarReason);
    }
    
    
    private String logFloristSelection(String orderDetailId, String floristList, String selectedFlorist, String selector, String oscarReqId, String oscarReason) throws Exception 
    {
        Map outputs = null;
        String logId = null;
        DataRequest dataRequest = new DataRequest();
        if (logger.isDebugEnabled()) {
            logger.debug("Logging florist selection of " + selectedFlorist + " for " + orderDetailId + " selected by " + selector);
        }
                
        dataRequest.reset();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_LOG_FLORIST_SELECTION");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
        dataRequest.addInputParam("IN_FLORIST_LIST", floristList);
        dataRequest.addInputParam("IN_SELECTED_FLORIST", selectedFlorist);
        dataRequest.addInputParam("IN_SELECTOR", selector);
        dataRequest.addInputParam("IN_OSCAR_REQ_ID", oscarReqId);
        dataRequest.addInputParam("IN_OSCAR_REASON", oscarReason);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);
                
        String status = (String)outputs.get("OUT_STATUS");
        logId = (String)outputs.get("OUT_LOG_ID");
        if (status != null && status.equalsIgnoreCase("N")) {
            String message = (String)outputs.get("OUT_MESSAGE");
            logger.error("Error logging Apollo/Oscar florist selection to database: " + message);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Logged florist selection with florist_selection_log_id: " + logId);
        }
        return logId;
    }
}
