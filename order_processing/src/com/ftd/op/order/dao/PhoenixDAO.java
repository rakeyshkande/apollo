package com.ftd.op.order.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Document;

import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.order.vo.PhoenixVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

public class PhoenixDAO {
    private Connection conn;
    private Logger logger;

    public PhoenixDAO(Connection conn)
    {
      this.conn = conn;
      logger = new Logger("com.ftd.op.order.dao.PhoenixDAO");
    }

    public PhoenixVO getPhoenixDetails(String orderDetailId, String origin, String mercuryOrderNumber) {
		logger.info("getPhoenixDetails(" + orderDetailId + " " + origin + " " + mercuryOrderNumber +" )");
    	PhoenixVO pVO = new PhoenixVO();

        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
            inputParams.put("IN_ORIGIN", origin);
            inputParams.put("IN_MERCURY_ORDER_NUMBER", mercuryOrderNumber);
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_PHOENIX_DETAILS");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet crs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            if (crs.next()) {
            	pVO.setRecordSource(crs.getString("record_source"));
            	pVO.setOrderDetailId(crs.getLong("order_detail_id"));
            	pVO.setExternalOrderNumber(crs.getString("external_order_number"));
            	pVO.setEmailAddress(crs.getString("customer_email"));
            	pVO.setProductId(crs.getString("product_id"));
            	pVO.setDeliveryDate(crs.getDate("delivery_date"));
            	pVO.setRecipientName(crs.getString("recipient_name"));
            	pVO.setMercuryAddress(crs.getString("mercury_address"));
            	pVO.setRecipientAddress1(crs.getString("recipient_address1"));
            	pVO.setRecipientAddress2(crs.getString("recipient_address2"));
            	pVO.setBusinessName(crs.getString("business_name"));
            	pVO.setRecipientCity(crs.getString("city"));
            	pVO.setRecipientState(crs.getString("state"));
            	pVO.setRecipientZipCode(crs.getString("zip_code"));
            	pVO.setRecipientCountry(crs.getString("country"));
            	pVO.setRecipientPhoneNumber(crs.getString("phone_number"));
            	pVO.setCardMessage(crs.getString("card_message"));
            	pVO.setOrderDate(crs.getDate("order_date"));
            	pVO.setMercuryId(crs.getString("mercury_id"));
            	pVO.setCompanyId(crs.getString("company_id"));
            	pVO.setSourceCode(crs.getString("source_code"));
            }
        } catch (Exception e) {
        	logger.error(e);
        }
        return pVO;
    }
    
    public boolean deleteQueueRecords(long orderDetailId) throws Exception {
    	boolean result = true;
  	    String queuesList = "";
		logger.info("deleteQueueRecords(" + orderDetailId + ")");
		
		try {
			  //Getting the QueueList from Global Parms
		      String queueListSelected = ConfigurationUtil.getInstance().getFrpGlobalParm(
		    		  OrderProcessingConstants.OP_PHOENIX_CONTEXT, OrderProcessingConstants.OP_PHOENIX_QUEUE_TYPES);
		      StringTokenizer qST = new StringTokenizer(queueListSelected);
			  while (qST.hasMoreElements())
			  {
				  queuesList += "'" + (String)qST.nextElement()+ "',";
			  }        
		      if (queuesList.length() > 0)
		    	  queuesList = queuesList.substring(0, queuesList.length() - 1);
		      else
		    	  queuesList = null;
		    logger.debug("Selected Queue Types: " + queuesList);
	        DataRequest dataRequest = new DataRequest();        
	        Map<String, Object> inputParams = new HashMap<String, Object>();
	        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
	        inputParams.put("IN_QUEUE_TYPES",queuesList);
	        dataRequest.setInputParams(inputParams);
	        dataRequest.setConnection(conn);
	        dataRequest.setStatementID("DELETE_QUEUE_PHOENIX");
	        
	        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
			Map outputs = (Map) dataAccessUtil.execute(dataRequest);
	        
			String status = (String) outputs.get("OUT_STATUS");
			if (status != null && status.equalsIgnoreCase("N")) {				
				logger.error("Error deleting message: " + (String) outputs.get("OUT_MESSAGE"));
				result = false;
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
    	
    	return result;
    }
    
    public void updatePhoenixOrder(PhoenixVO pVO) throws Exception {
    	logger.info("updatePhoenixOrder()");
        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", pVO.getOrderDetailId());
            inputParams.put("IN_PRODUCT_ID", pVO.getProductId());
            inputParams.put("IN_PHOENIX_PRODUCT_ID", pVO.getPhoenixProductId());
            inputParams.put("IN_SHIP_METHOD", pVO.getShipMethod());
            inputParams.put("IN_SHIP_DATE", new java.sql.Date(pVO.getShipDate().getTime()));
            if(pVO.getDeliveryDate() != null )
            	  inputParams.put("IN_DELIVERY_DATE", new java.sql.Date(pVO.getDeliveryDate().getTime()));
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("UPDATE_PHOENIX_ORDER");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);

            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = (String)outputs.get("OUT_MESSAGE");
                throw new Exception(message);
            }

        } catch (Exception e) {
        	logger.error(e);
        }
    }
     
    
    /**
     * This method inserts the phoenix order to phoenix order tracking table
     */
    public void insertPhoenixOrderTracking(PhoenixVO pVO) throws Exception {
    	logger.info("insertPhoenixOrderTracking()");
        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", pVO.getOrderDetailId());
            inputParams.put("IN_NEW_PRODUCT_ID", pVO.getPhoenixProductId());
            inputParams.put("IN_ORIGINAL_PRODUCT_ID",  pVO.getProductId());
            inputParams.put("IN_CREATED_BY","Phoenix");
            inputParams.put("IN_MERCURY_ID", pVO.getMercuryId());
            inputParams.put("IN_ORIG_BEAR_ADDON_ID", pVO.getBearAddonId());
            inputParams.put("IN_NEW_BEAR_ADDON_ID",  pVO.getPhoenixBearAddonId());
            inputParams.put("IN_ORIG_CHOC_ADDON_ID", pVO.getChocAddonId());
            inputParams.put("IN_NEW_CHOC_ADDON_ID",  pVO.getPhoenixChocAddonId());
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("INSERT_PHOENIX_ORDER_TRACKING");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);

            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = (String)outputs.get("OUT_MESSAGE");
                throw new Exception(message);
            }

        } catch (Exception e) {
        	logger.error(e);
        }   
    }
    
 
    /**
     * Retrieve order Phoenix eligibility information.
     *
     * @param orderDetailId
     * @param origin
     * @param mercuryOrderNumber
     * @return CachedResultSet
     * @throws java.lang.Exception
     *
     */
    public CachedResultSet getOrderPhoenixEligibility(String orderDetailId, String origin, String mercuryOrderNumber) throws Exception
    {
    	  logger.info("getOrderPhoenixEligibility(): " + orderDetailId + " " + origin + " " + mercuryOrderNumber);
    	  DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("ORDER_PHOENIX_ELIGIBLE");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
          dataRequest.addInputParam("IN_ORIGIN", origin);
          dataRequest.addInputParam("IN_MERCURY_ORDER_NUMBER", mercuryOrderNumber);

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
   
          CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);

          return rs;
    }
    
    public void updateOrderAddOn(long orderDetailId, String origAddOnId, String phoenixAddOnId) throws Exception {
    	logger.info("updateOrderAddOn()");
        try {
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
            inputParams.put("IN_ORIG_ADD_ON_CODE", origAddOnId);
            inputParams.put("IN_PHOENIX_ADD_ON_CODE", phoenixAddOnId);
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("UPDATE_ORDER_ADD_ONS");
            dataRequest.setInputParams(inputParams);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = (Map)dataAccessUtil.execute(dataRequest);

            String status = (String)outputs.get("OUT_STATUS");
            if (status != null && status.equalsIgnoreCase("N")) {
                String message = (String)outputs.get("OUT_MESSAGE");
                throw new Exception(message);
            }

        } catch (Exception e) {
        	logger.error(e);
        }
    }
    
    public Map getPhoenixEmailTypes() throws Exception{
    	//Map<String, String> map = null;
    	HashMap<String, String> sortedMap = null;
    	try{
    		DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_PHOENIX_EMAIL_TYPES");
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet rs  = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            if(rs != null){
            	sortedMap = new LinkedHashMap<String, String>();
            	while(rs.next()){
            		sortedMap.put(rs.getString("email_type_key"), rs.getString("email_type_name"));
            	}
            }
    	}catch (Exception e) {
    		logger.error(e);
    		throw e;
		}
    	
    	return sortedMap;
    }
    
    public boolean isOrderCotainLiveFTD(String orderDetailId) throws Exception{
    	boolean returnFlag = false;
    	HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);    	
    	DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("CHECK_FOR_LIVE_FTD");
        dataRequest.setInputParams(inputParams);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
    	String outFlag = (String) dataAccessUtil.execute(dataRequest);
    	
    	if(outFlag != null && outFlag.equalsIgnoreCase("Y"))
    		returnFlag = true;
    	else
    		returnFlag = false;
    	
    	return returnFlag;
    }
    
}
