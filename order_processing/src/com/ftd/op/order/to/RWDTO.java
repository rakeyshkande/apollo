package com.ftd.op.order.to;
import java.io.Serializable;
import java.util.Date;

public class RWDTO implements Serializable
{
  private String zipCode;
  private String product;
  private Date deliveryDate;
  private Date deliveryDateEnd;
  private String sourceCode;
  private String orderDetailId;
  private String deliveryCity;
  private String deliveryState;
  private double orderValue;
  private boolean returnAll;
  private Boolean rwdFlagOverride;
          
  public RWDTO()
  {
  }


  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setProduct(String product)
  {
    this.product = product;
  }


  public String getProduct()
  {
    return product;
  }


  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }


  public Date getDeliveryDate()
  {
    return deliveryDate;
  }


  public void setDeliveryDateEnd(Date deliveryDateEnd)
  {
    this.deliveryDateEnd = deliveryDateEnd;
  }


  public Date getDeliveryDateEnd()
  {
    return deliveryDateEnd;
  }


  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = sourceCode;
  }


  public String getSourceCode()
  {
    return sourceCode;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setDeliveryCity(String deliveryCity)
  {
    this.deliveryCity = deliveryCity;
  }


  public String getDeliveryCity()
  {
    return deliveryCity;
  }


  public void setDeliveryState(String deliveryState)
  {
    this.deliveryState = deliveryState;
  }


  public String getDeliveryState()
  {
    return deliveryState;
  }


  public void setOrderValue(double orderValue)
  {
    this.orderValue = orderValue;
  }


  public double getOrderValue()
  {
    return orderValue;
  }


  public void setReturnAll(boolean returnAll)
  {
    this.returnAll = returnAll;
  }


  public boolean isReturnAll()
  {
    return returnAll;
  }

  public void setRWDFlagOverride(Boolean rwdFlagOverride) {
      this.rwdFlagOverride = rwdFlagOverride;
  }

  public Boolean getRWDFlagOverride() {
      return rwdFlagOverride;
  }
}

