package com.ftd.op.order.to;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CreditCardTO implements Serializable 
{
  private String validationStatus; // 'D' - Declined, 'A' - Approved, 'E' - System Error //
  private String creditCardType;
  private String creditCardDnsType;
  private String ccId; 
  private String name;
  private String address2;
  private String city;
  private String country;
  private String customerId;
  private boolean underMinAuthAmt;
  
  private String addressLine;
	private String zipCode;
	private String creditCardNumber;
	private String expirationDate;
	private String userData;
	private String transactionType;
	private String amount;
	private String approvedAmount;
	private String status;
	private String actionCode;
	private String verbiage;
	private String approvalCode;
	private String dateStamp;
	private String timeStamp;
	private String AVSIndicator;
	private String batchNumber;
	private String itemNumber;
	private String acquirerReferenceData;
	private String cscValue;
	private String cscResponseCode;
	private String cscValidatedFlag;	
	private String ccAuthProvider;
	// Added to hold extra payment response elements
	private Map<String, Object> paymentExtMap;
	
	// SP-184
	private String callingSystem;
	
	private String eci;
	private String xid;
	private String cavv;
    private String ucaf;
    private String route;
    private boolean isNewRoute;
    private String requestId;
	private String authorizationTransactionId;
	private String merchantRefId;

	
  public CreditCardTO()
  {
  }


  public void setValidationStatus(String validationStatus)
  {
    this.validationStatus = validationStatus;
  }


  public String getValidationStatus()
  {
    return validationStatus;
  }


  public void setCreditCardType(String creditCardType)
  {
    this.creditCardType = creditCardType;
  }


  public String getCreditCardType()
  {
    return creditCardType;
  }


  public void setCreditCardDnsType(String creditCardDnsType)
  {
    this.creditCardDnsType = creditCardDnsType;
  }


  public String getCreditCardDnsType()
  {
    return creditCardDnsType;
  }


  public void setCcId(String ccId)
  {
    this.ccId = ccId;
  }


  public String getCcId()
  {
    return ccId;
  }


  public void setName(String name)
  {
    this.name = name;
  }


  public String getName()
  {
    return name;
  }


  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setCountry(String country)
  {
    this.country = country;
  }


  public String getCountry()
  {
    return country;
  }


  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }


  public String getCustomerId()
  {
    return customerId;
  }


  public void setAddressLine(String addressLine)
  {
    this.addressLine = addressLine;
  }


  public String getAddressLine()
  {
    return addressLine;
  }


  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setCreditCardNumber(String creditCardNumber)
  {
    this.creditCardNumber = creditCardNumber;
  }


  public String getCreditCardNumber()
  {
    return creditCardNumber;
  }


  public void setExpirationDate(String expirationDate)
  {
    this.expirationDate = expirationDate;
  }


  public String getExpirationDate()
  {
    return expirationDate;
  }


  public void setUserData(String userData)
  {
    this.userData = userData;
  }


  public String getUserData()
  {
    return userData;
  }


  public void setTransactionType(String transactionType)
  {
    this.transactionType = transactionType;
  }


  public String getTransactionType()
  {
    return transactionType;
  }


  public void setAmount(String amount)
  {
    this.amount = amount;
  }


  public String getAmount()
  {
    return amount;
  }


  public void setApprovedAmount(String approvedAmount)
  {
    this.approvedAmount = approvedAmount;
  }


  public String getApprovedAmount()
  {
    return approvedAmount;
  }


  public void setStatus(String status)
  {
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setActionCode(String actionCode)
  {
    this.actionCode = actionCode;
  }


  public String getActionCode()
  {
    return actionCode;
  }


  public void setVerbiage(String verbiage)
  {
    this.verbiage = verbiage;
  }


  public String getVerbiage()
  {
    return verbiage;
  }


  public void setApprovalCode(String approvalCode)
  {
    this.approvalCode = approvalCode;
  }


  public String getApprovalCode()
  {
    return approvalCode;
  }


  public void setDateStamp(String dateStamp)
  {
    this.dateStamp = dateStamp;
  }


  public String getDateStamp()
  {
    return dateStamp;
  }


  public void setTimeStamp(String timeStamp)
  {
    this.timeStamp = timeStamp;
  }


  public String getTimeStamp()
  {
    return timeStamp;
  }


  public void setAVSIndicator(String AVSIndicator)
  {
    this.AVSIndicator = AVSIndicator;
  }


  public String getAVSIndicator()
  {
    return AVSIndicator;
  }


  public void setBatchNumber(String batchNumber)
  {
    this.batchNumber = batchNumber;
  }


  public String getBatchNumber()
  {
    return batchNumber;
  }


  public void setItemNumber(String itemNumber)
  {
    this.itemNumber = itemNumber;
  }


  public String getItemNumber()
  {
    return itemNumber;
  }


  public void setAcquirerReferenceData(String acquirerReferenceData)
  {
    this.acquirerReferenceData = acquirerReferenceData;
  }


  public String getAcquirerReferenceData()
  {
    return acquirerReferenceData;
  }

  public void setUnderMinAuthAmt(boolean underMinAuthAmt)
  {
    this.underMinAuthAmt = underMinAuthAmt;
  }

  public boolean isUnderMinAuthAmt()
  {
    return underMinAuthAmt;
  }

  public void setCscValue(String cscValue)
  {
    this.cscValue = cscValue;
  }

  public String getCscValue()
  {
    return cscValue;
  }

  public void setCscResponseCode(String cscResponseCode)
  {
    this.cscResponseCode = cscResponseCode;
  }

  public String getCscResponseCode()
  {
    return cscResponseCode;
  }

  public void setCscValidatedFlag(String cscValidatedFlag)
  {
    this.cscValidatedFlag = cscValidatedFlag;
  }

  public String getCscValidatedFlag()
  {
    return cscValidatedFlag;
  }


public Map<String, Object> getPaymentExtMap() {
	if(this.paymentExtMap == null) {
		paymentExtMap = new HashMap<String, Object>();
	}
	return paymentExtMap;	
}

public String getCcAuthProvider() {
	return ccAuthProvider;
}


public void setCcAuthProvider(String ccAuthProvider) {
	this.ccAuthProvider = ccAuthProvider;
}


public String getCallingSystem() {
	return callingSystem;
}


public void setCallingSystem(String callingSystem) {
	this.callingSystem = callingSystem;
}


public String getEci() {
	return eci;
}


public void setEci(String eci) {
	this.eci = eci;
}


public String getXid() {
	return xid;
}


public void setXid(String xid) {
	this.xid = xid;
}


public String getCavv() {
	return cavv;
}


public void setCavv(String cavv) {
	this.cavv = cavv;
}

// User Story 4053 - New token for master card
public String getUcaf() {
	return ucaf;
}

public void setUcaf(String ucaf) {
	this.ucaf = ucaf;
}

public String getRoute() {
	return route;
}

public void setRoute(String route) {
	this.route = route;
}

public boolean isNewRoute() {
	return isNewRoute;
}

public void setNewRoute(boolean isNewRoute) {
	this.isNewRoute = isNewRoute;
}

public String getRequestId() {
	return requestId;
}

public void setRequestId(String requestId) {
	this.requestId = requestId;
}

public String getAuthorizationTransactionId() {
	return authorizationTransactionId;
}

public void setAuthorizationTransactionId(String authorizationTransactionId) {
	this.authorizationTransactionId = authorizationTransactionId;
}

public String getMerchantRefId() {
	return merchantRefId;
}

public void setMerchantRefId(String merchantRefId) {
	this.merchantRefId = merchantRefId;
}
}
