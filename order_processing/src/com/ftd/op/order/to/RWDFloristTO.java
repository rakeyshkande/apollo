package com.ftd.op.order.to;

import java.io.Serializable;

public class RWDFloristTO implements Serializable 
{
  private String floristId;
  private int score;
  private int floristWeight;
  private String floristName;
  private String phoneNumber;
  private String sundayDeliveryFlag;
  private String mercuryFlag;
  private String status;
  private String superFloristFlag;
  private String cutoffTime;
  private String codification;
  private boolean codificationBlockFlag;
  private String usedSequence;
  private int priority;
  private String displayStatus;
  private String floristSelectionLogId;
  private String zipCityFlag;
  private String floristSuspended;
  private String minOrderAmt;

  /* 0th and 1st bit are the score block
   * 2nd bit is codification
   * 3rd bit is mercury
   * 4th bit is zip code
   * 5th bit is preferred
   */
   
  private static final int PREFERRED_BIT = 5;
  private static final int ZIP_BIT = 4;
  private static final int MERCURY_BIT = 3;
  private static final int CODIFIED_BIT = 2;

  private static final int SCORE_BLOCK = 4; // use modulus to get low bits
  
  private static final String ACTIVE = "ACTIVE";
  private static final String SOFT_BLOCKED = "SOFT BLOCKED";
  private static final String HARD_BLOCKED = "HARD BLOCKED";
  private static final String SUSPENDED = "SUSPENDED";
  
  public RWDFloristTO()
  {
  }
  
  public void setFloristId(String floristId)
  {
    this.floristId = floristId;
  }


  public String getFloristId()
  {
    return floristId;
  }
  
  public void setScore(int score)
  {
    this.score = score;
  }


  public int getScore()
  {
    return score;
  }
  
  public void setFloristWeight(int floristWeight)
  {
    this.floristWeight = floristWeight;
  }


  public int getFloristWeight()
  {
    return floristWeight;
  }
  
  public void setFloristName(String floristName)
  {

    this.floristName = floristName;
  }


  public String getFloristName()
  {
    return floristName;
  }
  
  public void setPhoneNumber(String phoneNumber)
  {

    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public void setSundayDeliveryFlag(String sundayDeliveryFlag)
  {

    this.sundayDeliveryFlag = sundayDeliveryFlag;
  }


  public String getSundayDeliveryFlag()
  {
    return sundayDeliveryFlag;
  }
  
  public void setMercuryFlag(String mercuryFlag)
  {

    this.mercuryFlag = mercuryFlag;
  }


  public String getMercuryFlag()
  {
    return mercuryFlag;
  }
  
  public void setStatus(String status)
  {

    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }
  
  public void setSuperFloristFlag(String superFloristFlag)
  {

    this.superFloristFlag = superFloristFlag;
  }


  public String getSuperFloristFlag()
  {
    return superFloristFlag;
  }
  
  public void setCutoffTime(String cutoffTime)
  {
    this.cutoffTime = cutoffTime;
  }


  public String getCutoffTime()
  {
    return cutoffTime;
  }
  
  public int getWeightSquared()
  {
    return this.floristWeight * this.floristWeight/*Math.sqrt(this.floristWeight)*/; 
  }
  
  /**
   * Finds the florist's block status based on the lowest 2 bits in 
   * the score bitfield
   * 
   * @return a string representing the block status
   */
  public String getBlockStatus() 
  {
/*
	// get the lowest 2 bits of the score
    int blockCode = score % SCORE_BLOCK;
    String output = new String();
    
    // if score is > 64 then the florist is a primary/backup so status is automatically ACTIVE
    if (score > 64) {
        output = ACTIVE;
    } else {
        //interpret the bitfield
        switch(blockCode) 
        {
          case 0:
            output = HARD_BLOCKED;
            break;
          case 1:
            output = SOFT_BLOCKED;
            break;
          case 2:
            output = SUSPENDED;
            break;
          case 3:
            output = ACTIVE;
            break;
        }
    }
*/
    // don't use old score logic, use displayStatus
    return displayStatus; 
  }

  /**
   * Helper method to test a bit in a bitfield
   * 
   * @return true if the bit is set
   * @param bit bit number (right most bit is 0)
   * @param field 
   */
  private boolean isBitSet(int field, int bit)
  {
    // right shift bit_number
    int test = score >> (bit);
    
    boolean out = false;
    
    // if the lowest order bit is 1 bit is set --> return true
    if (test % 2 == 1) 
    {
      out = true;
    }
    
    return out;
  }
  
/* These functions no longer work with the addition of the Primary/Backup florist logic.
 * They were not used anyway

  public boolean isCodified() 
  {
    return isBitSet(score, CODIFIED_BIT);
  }
  public boolean isMercury() 
  {
    return isBitSet(score, MERCURY_BIT);
  }
  public boolean isZipCode() 
  {
    return isBitSet(score, ZIP_BIT);
  }
  public boolean isPreferred() 
  {
    return isBitSet(score, PREFERRED_BIT);
  }
*/

  public void setCodification(String codification)
  {
    this.codification = codification;
  }


  public String getCodification()
  {
    return codification;
  }


  public void setCodificationBlockFlag(boolean codificationBlockFlag)
  {
    this.codificationBlockFlag = codificationBlockFlag;
  }


  public boolean isCodificationBlockFlag()
  {
    return codificationBlockFlag;
  }


  public void set_codification(String codification)
  {
    this.codification = codification;
  }


  public String get_codification()
  {
    return codification;
  }


  public void set_codificationBlockFlag(boolean codificationBlockFlag)
  {
    this.codificationBlockFlag = codificationBlockFlag;
  }


  public boolean is_codificationBlockFlag()
  {
    return codificationBlockFlag;
  }


  public void setUsedSequence(String usedSequence)
  {
    this.usedSequence = usedSequence;
  }


  public String getUsedSequence()
  {
    return usedSequence;
  }

  public void setPriority(int priority)
  {
    this.priority = priority;
  }

  public int getPriority()
  {
    return priority;
  }

public void setDisplayStatus(String displayStatus) {
	this.displayStatus = displayStatus;
}

public String getDisplayStatus() {
	return displayStatus;
}

  public String getFloristSelectionLogId() {
    return floristSelectionLogId;
  }

  public void setFloristSelectionLogId(String floristSelectionLogId) {
    this.floristSelectionLogId = floristSelectionLogId;
  }

public String getZipCityFlag() {
	return zipCityFlag;
}

public void setZipCityFlag(String zipCityFlag) {
	this.zipCityFlag = zipCityFlag;
}

public String getFloristSuspended() {
	return floristSuspended;
}

public void setFloristSuspended(String floristSuspended) {
	this.floristSuspended = floristSuspended;
}

public String getMinOrderAmt() {
	return minOrderAmt;
}

public void setMinOrderAmt(String minOrderAmt) {
	this.minOrderAmt = minOrderAmt;
}

}
