package com.ftd.op.order.to;

import java.io.Serializable;

public class OrderTO implements Serializable 
{

  private String orderDetailId;
  private Boolean rwdFlagOverride = null;
  
  public OrderTO()
  {
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setRWDFlagOverride(Boolean rwdFlagOverride)
  {
    this.rwdFlagOverride = rwdFlagOverride;
  }

  public Boolean getRWDFlagOverride()
  {
    return rwdFlagOverride;
  }
}
