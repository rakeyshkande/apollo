package com.ftd.op.order.to;
import com.ftd.op.order.vo.RWDFloristVO;

import java.io.Serializable;
import java.util.LinkedList;

public class RWDFloristTOList implements Serializable 
{
  private LinkedList floristList;
    
  public RWDFloristTOList()
  {
    floristList = new LinkedList();
  }

  public void addFloristToList(RWDFloristTO rwdFloristTO)
  {
    this.floristList.add(rwdFloristTO);
  }

  public LinkedList getFloristList()
  {
    return floristList;
  }     
}