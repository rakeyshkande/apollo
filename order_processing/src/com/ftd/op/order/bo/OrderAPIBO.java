package com.ftd.op.order.bo;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.facade.OrderFacade;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.to.CreditCardTO;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.Iterator;
import java.util.LinkedList;

import javax.sql.DataSource;

/**
 * Provides a BO Implementation for utilizing the Order API as a Library.
 */
 
public class OrderAPIBO 
{

  Logger logger = new Logger ("com.ftd.op.order.api.OrderAPIBO");
  
   
  /**
   *  Returns the Filling Florist information for the passed in Order Detail.
   * 
   * @param OrderTO Contains the order detail
   * @return RWDFloristVO 
   * @throws java.lang.Exception
   */
  public RWDFloristTO getFillingFlorist(OrderTO order, Connection conn)  {

    logger.debug("IN getFillingFlorist");
    logger.debug("Order TO has: " + order.getOrderDetailId());
    RWDFloristTO florist = null;
    try {

      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO detail = dao.getOrderDetail(order.getOrderDetailId());
      /* throw exeption if no order detail record retrieved */
      if(detail == null)
      {
        throw new Exception("No order detail record found");
      }
    
      OrderFacade orderFacade = new OrderFacade(conn);
      System.out.println("In the code");
      florist = orderFacade.selectFlorist(detail,order.getRWDFlagOverride()).getRWDFloristTO(); 
    
      if (florist != null) {
        logger.debug("found florist: " + florist.getFloristId());
      } else 
      {
      logger.debug("florist is null");
      }
    
      } catch (Exception e) {
        logger.error(e);;
      } 
      
    return florist;
  }
  
    /**
     * Returns the Filling Florist information for the passed in Order Detail.
     * 
     * @param RWDTO Contains the order detail
     * @return RWDFloristVO 
     * @throws java.lang.Exception
     */
    public RWDFloristTO getFillingFlorist(RWDTO rwd, Connection conn)  {

      logger.debug("getFillingFlorist(RWDTO rwd) order detail id: " + rwd.getOrderDetailId());
      RWDFloristTO florist = null;
      try {     
        OrderDAO dao = new OrderDAO(conn);
        
        OrderFacade orderFacade = new OrderFacade(conn);
        florist = orderFacade.selectFlorist(rwd).getRWDFloristTO();    
      
        if (florist != null) {
          logger.debug("found florist: " + florist.getFloristId());
        } else 
        {
        logger.debug("florist is null");
        }
      
        } catch (Exception e) {
          logger.error(e);;
        } 
        
      return florist;
    }
    
  /**
   * Returns a list of Filling Florists for the passed in Order
   * 
   * @param OrderTO
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristTOList getFullFloristList(OrderTO order, Connection conn)
  {
    try {
      OrderFacade orderFacade = new OrderFacade(conn);
      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO detail = dao.getOrderDetail(order.getOrderDetailId());
      /* throw exeption if no order detail record retrieved */
      if(detail == null)
      {
        throw new Exception("No order detail record found");
      }
      LinkedList list = orderFacade.getFillingFloristList(detail, true, order.getRWDFlagOverride()).getFloristList();
      Iterator iter = list.iterator();
      RWDFloristTOList outlist = new RWDFloristTOList();
      while(iter.hasNext()) 
      {
        outlist.addFloristToList(((RWDFloristVO)iter.next()).getRWDFloristTO());
      }
      return outlist;
    } catch (Exception e) {    
      logger.error(e);
    } 
    
    return null;
  }
  
  /**
   * Returns a list of the top florists for the passed in order.
   * 
   * @param OrderTO
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristTOList getTopFloristList(OrderTO order, Connection conn) 
  {
    try {
      OrderFacade orderFacade = new OrderFacade(conn);
      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO detail = dao.getOrderDetail(order.getOrderDetailId());
      /* throw exeption if no order detail record retrieved */
      if(detail == null)
      {
        throw new Exception("No order detail record found");
      }
      
      // this is confusing... rename the facade method eventually
      LinkedList list = orderFacade.getFillingFloristList(detail, false, order.getRWDFlagOverride()).getFloristList();
      Iterator iter = list.iterator();
      RWDFloristTOList outlist = new RWDFloristTOList();
      while(iter.hasNext()) 
      {
        outlist.addFloristToList(((RWDFloristVO)iter.next()).getRWDFloristTO());
      }
      return outlist;
    } catch (Exception e) {
        logger.error(e);
    } 
    
    return null;
  } 

  /**
   * A test method for the BO. 
   * 
   * @param String
   * @return String
   */
  public String helloWorld(String inString) 
  {
    if (!inString.equals("Exception")) {
      return "Hello World: " + inString;
    } else {
      logger.error("This is an exception");
    }
    return null;
  }
  
    /**
   * Validates the passed in Credit Card.
   * 
   * @param CreditCardTO 
   * @return CreditCardTO Validated Credit Card or <code>null</code> If the card is not valid.
   */
  public CreditCardTO validateCreditCard(CreditCardTO card, Connection conn)
  {
    try {
      OrderFacade of = new OrderFacade(conn);
      CommonCreditCardVO inCreditCard = new CommonCreditCardVO();
      inCreditCard.populateVO(card);
      CommonCreditCardVO outCreditCard = of.validateCreditCard(inCreditCard);
      if(outCreditCard != null) 
      {
        return outCreditCard.getCreditCardTO();
      } else {
        return null;
      }
    } catch (Exception e) {
        logger.error(e);
    } 
    
    return null;
  }

    /**
   * Processes the passed in Order.
   * 
   * @param OrderTO 
   */
  public void processOrder(OrderTO order, Connection conn)
  { 
    try 
    {
      OrderService os = new OrderService(conn);
      OrderDAO dao = new OrderDAO(conn);
      OrderDetailVO detail = dao.getOrderDetail(order.getOrderDetailId());
      os.processOrder(detail,false,true,order.getRWDFlagOverride());
    } catch (Exception e) 
    {
      logger.error(e);
    }
  }

  /**
   * Returns a list of the filling florists for the passed in order.
   * 
   * @param RWDTO
   * @return RWDFloristTOList
   * @throws java.lang.Exception
   */
  public RWDFloristTOList getFillingFloristList(RWDTO rwd, Connection conn)
  {  
    try {
      OrderFacade orderFacade = new OrderFacade(conn);
      
      // this is confusing... rename the facade method eventually
      LinkedList list = orderFacade.getFillingFloristList(rwd).getFloristList();
      Iterator iter = list.iterator();
      RWDFloristTOList outlist = new RWDFloristTOList();
      while(iter.hasNext()) 
      {
        outlist.addFloristToList(((RWDFloristVO)iter.next()).getRWDFloristTO());
      }
      return outlist;
    } catch (Exception e) {
        logger.error(e);
    } 
    
    return null;
  }   
  
  
}
