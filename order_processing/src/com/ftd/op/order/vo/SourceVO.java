package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class SourceVO extends BaseVO
{
  private String   sourceCode;
  private String   sourceType;
  private String   departmentCode;
  private String   description;
  private Date startDate;
  private Date endDate;
  private String   pricingCode;
  private String   shippingCode;
  private String   partnerId;
  private String   validPayMethod;
  private String   listCodeFlag;
  private String   defaultSourceCodeFlag;
  private String   billingInfoPrompt;
  private String   billingInfoLogic;
  private String   marketingGroup;
  private String   externalCallCenterFlag;
  private String   highlightDescriptionFlag;
  private String   discountAllowedFlag;
  private String   orderSource;
  private String   mileageBonus;
  private String   promotionCode;
  private String   mileageCalculationSource;
  private String   bonusPoints;
  private String   bonusType;
  private String   separateData;
  private String   yellowPagesCode;
  private long     pointsMilesPerDollar;
  private long     maximumPointsMiles;
  private String   JcpenneyFlag;
  private String   oeDefaultFlag;
  private String   companyId;
  private String   sendToScrub;
  private String   fraudFlag;
  private String   enableLpProcessing;
  private String emergencyTextFlag;
  private String requiresDeliveryConfirmation;
  
  public SourceVO()
  {
  }
  
  public void setSourceCode(String sourceCode)
  {
    if(valueChanged(this.sourceCode, sourceCode))
    {
      setChanged(true);
    }
    this.sourceCode = trim(sourceCode);
  }

  public String getSourceCode()
  {
    return sourceCode;
  }
  
  public void setSourceType(String sourceType)
  {
    if(valueChanged(this.sourceType, sourceType))
    {
      setChanged(true);
    }
    this.sourceType = trim(sourceType);
  }


  public String getSourceType()
  {
    return sourceType;
  }


  public void setDepartmentCode(String departmentCode)
  {
    if(valueChanged(this.departmentCode, departmentCode))
    {
      setChanged(true);
    }
    this.departmentCode = trim(departmentCode);
  }


  public String getDepartmentCode()
  {
    return departmentCode;
  }


  public void setDescription(String description)
  {
    if(valueChanged(this.description, description))
    {
      setChanged(true);
    }
    this.description = trim(description);
  }


  public String getDescription()
  {
    return description;
  }


  public void setStartDate(Date startDate)
  {
    if(valueChanged(this.startDate, startDate))
    {
      setChanged(true);
    }
    this.startDate = startDate;
  }


  public Date getStartDate()
  {
    return startDate;
  }


  public void setEndDate(Date endDate)
  {
    if(valueChanged(this.endDate, endDate))
    {
      setChanged(true);
    }
    this.endDate = endDate;
  }


  public Date getEndDate()
  {
    return endDate;
  }


  public void setPricingCode(String pricingCode)
  {
    if(valueChanged(this.pricingCode, pricingCode))
    {
      setChanged(true);
    }
    this.pricingCode = trim(pricingCode);
  }


  public String getPricingCode()
  {
    return pricingCode;
  }


  public void setShippingCode(String shippingCode)
  {
    if(valueChanged(this.shippingCode, shippingCode))
    {
      setChanged(true);
    }
    this.shippingCode = trim(shippingCode);
  }


  public String getShippingCode()
  {
    return shippingCode;
  }


  public void setPartnerId(String partnerId)
  {
    if(valueChanged(this.partnerId, partnerId))
    {
      setChanged(true);
    }
    this.partnerId = trim(partnerId);
  }


  public String getPartnerId()
  {
    return partnerId;
  }


  public void setValidPayMethod(String validPayMethod)
  {
    if(valueChanged(this.validPayMethod, validPayMethod))
    {
      setChanged(true);
    }
    this.validPayMethod = trim(validPayMethod);
  }


  public String getValidPayMethod()
  {
    return validPayMethod;
  }


  public void setListCodeFlag(String listCodeFlag)
  {
    if(valueChanged(this.listCodeFlag, listCodeFlag))
    {
      setChanged(true);
    }
    this.listCodeFlag = trim(listCodeFlag);
  }


  public String getListCodeFlag()
  {
    return listCodeFlag;
  }


  public void setDefaultSourceCodeFlag(String defaultSourceCodeFlag)
  {
    if(valueChanged(this.defaultSourceCodeFlag, defaultSourceCodeFlag))
    {
      setChanged(true);
    }
    this.defaultSourceCodeFlag = trim(defaultSourceCodeFlag);
  }


  public String getDefaultSourceCodeFlag()
  {
    return defaultSourceCodeFlag;
  }


  public void setBillingInfoPrompt(String billingInfoPrompt)
  {
    if(valueChanged(this.billingInfoPrompt, billingInfoPrompt))
    {
      setChanged(true);
    }
    this.billingInfoPrompt = trim(billingInfoPrompt);
  }


  public String getBillingInfoPrompt()
  {
    return billingInfoPrompt;
  }


  public void setBillingInfoLogic(String billingInfoLogic)
  {
    if(valueChanged(this.billingInfoLogic, billingInfoLogic))
    {
      setChanged(true);
    }
    this.billingInfoLogic = trim(billingInfoLogic);
  }


  public String getBillingInfoLogic()
  {
    return billingInfoLogic;
  }


  public void setMarketingGroup(String marketingGroup)
  {
    if(valueChanged(this.marketingGroup, marketingGroup))
    {
      setChanged(true);
    }
    this.marketingGroup = trim(marketingGroup);
  }


  public String getMarketingGroup()
  {
    return marketingGroup;
  }


  public void setExternalCallCenterFlag(String externalCallCenterFlag)
  {
    if(valueChanged(this.externalCallCenterFlag, externalCallCenterFlag))
    {
      setChanged(true);
    }
    this.externalCallCenterFlag = trim(externalCallCenterFlag);
  }


  public String getExternalCallCenterFlag()
  {
    return externalCallCenterFlag;
  }


  public void setHighlightDescriptionFlag(String highlightDescriptionFlag)
  {
    if(valueChanged(this.highlightDescriptionFlag, highlightDescriptionFlag))
    {
      setChanged(true);
    }
    this.highlightDescriptionFlag = trim(highlightDescriptionFlag);
  }


  public String getHighlightDescriptionFlag()
  {
    return highlightDescriptionFlag;
  }


  public void setDiscountAllowedFlag(String discountAllowedFlag)
  {
    if(valueChanged(this.discountAllowedFlag, discountAllowedFlag))
    {
      setChanged(true);
    }
    this.discountAllowedFlag = trim(discountAllowedFlag);
  }


  public String getDiscountAllowedFlag()
  {
    return discountAllowedFlag;
  }


  public void setOrderSource(String orderSource)
  {
    if(valueChanged(this.orderSource, orderSource))
    {
      setChanged(true);
    }
    this.orderSource = trim(orderSource);
  }


  public String getOrderSource()
  {
    return orderSource;
  }


  public void setMileageBonus(String mileageBonus)
  {
    if(valueChanged(this.mileageBonus, mileageBonus))
    {
      setChanged(true);
    }
    this.mileageBonus = trim(mileageBonus);
  }


  public String getMileageBonus()
  {
    return mileageBonus;
  }


  public void setPromotionCode(String promotionCode)
  {
    if(valueChanged(this.promotionCode, promotionCode))
    {
      setChanged(true);
    }
    this.promotionCode = trim(promotionCode);
  }


  public String getPromotionCode()
  {
    return promotionCode;
  }


  public void setMileageCalculationSource(String mileageCalculationSource)
  {
    if(valueChanged(this.mileageCalculationSource, mileageCalculationSource))
    {
      setChanged(true);
    }
    this.mileageCalculationSource = trim(mileageCalculationSource);
  }


  public String getMileageCalculationSource()
  {
    return mileageCalculationSource;
  }


  public void setBonusPoints(String bonusPoints)
  {
    if(valueChanged(this.bonusPoints, bonusPoints))
    {
      setChanged(true);
    }
    this.bonusPoints = trim(bonusPoints);
  }


  public String getBonusPoints()
  {
    return bonusPoints;
  }


  public void setBonusType(String bonusType)
  {
    if(valueChanged(this.bonusType, bonusType))
    {
      setChanged(true);
    }
    this.bonusType = trim(bonusType);
  }


  public String getBonusType()
  {
    return bonusType;
  }


  public void setSeparateData(String separateData)
  {
    if(valueChanged(this.separateData, separateData))
    {
      setChanged(true);
    }
    this.separateData = trim(separateData);
  }


  public String getSeparateData()
  {
    return separateData;
  }


  public void setYellowPagesCode(String yellowPagesCode)
  {
    if(valueChanged(this.yellowPagesCode, yellowPagesCode))
    {
      setChanged(true);
    }
    this.yellowPagesCode = trim(yellowPagesCode);
  }


  public String getYellowPagesCode()
  {
    return yellowPagesCode;
  }


  public void setPointsMilesPerDollar(long pointsMilesPerDollar)
  {
    if(valueChanged(this.pointsMilesPerDollar, pointsMilesPerDollar))
    {
      setChanged(true);
    }
    this.pointsMilesPerDollar = pointsMilesPerDollar;
  }


  public long getPointsMilesPerDollar()
  {
    return pointsMilesPerDollar;
  }


  public void setMaximumPointsMiles(long maximumPointsMiles)
  {
    if(valueChanged(this.maximumPointsMiles, maximumPointsMiles))
    {
      setChanged(true);
    }
    this.maximumPointsMiles = maximumPointsMiles;
  }


  public long getMaximumPointsMiles()
  {
    return maximumPointsMiles;
  }


  public void setJcpenneyFlag(String JcpenneyFlag)
  {
    if(valueChanged(this.JcpenneyFlag, JcpenneyFlag))
    {
      setChanged(true);
    }
    this.JcpenneyFlag = trim(JcpenneyFlag);
  }


  public String getJcpenneyFlag()
  {
    return JcpenneyFlag;
  }


  public void setOeDefaultFlag(String oeDefaultFlag)
  {
    if(valueChanged(this.oeDefaultFlag, oeDefaultFlag))
    {
      setChanged(true);
    }
    this.oeDefaultFlag = trim(oeDefaultFlag);
  }


  public String getOeDefaultFlag()
  {
    return oeDefaultFlag;
  }


  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = trim(companyId);
  }


  public String getCompanyId()
  {
    return companyId;
  }


  public void setSendToScrub(String sendToScrub)
  {
    if(valueChanged(this.sendToScrub, sendToScrub))
    {
      setChanged(true);
    }
    this.sendToScrub = trim(sendToScrub);
  }


  public String getSendToScrub()
  {
    return sendToScrub;
  }


  public void setFraudFlag(String fraudFlag)
  {
    if(valueChanged(this.fraudFlag, fraudFlag))
    {
      setChanged(true);
    }
    this.fraudFlag = trim(fraudFlag);
  }


  public String getFraudFlag()
  {
    return fraudFlag;
  }


  public void setEnableLpProcessing(String enableLpProcessing)
  {
    if(valueChanged(this.enableLpProcessing, enableLpProcessing))
    {
      setChanged(true);
    }
    this.enableLpProcessing = trim(enableLpProcessing);
  }


  public String getEnableLpProcessing()
  {
    return enableLpProcessing;
  }
  

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setEmergencyTextFlag(String emergencyTextFlag)
  {
    this.emergencyTextFlag = emergencyTextFlag;
  }


  public String getEmergencyTextFlag()
  {
    return emergencyTextFlag;
  }


  public void setRequiresDeliveryConfirmation(String requiresDeliveryConfirmation)
  {
    this.requiresDeliveryConfirmation = requiresDeliveryConfirmation;
  }


  public String getRequiresDeliveryConfirmation()
  {
    return requiresDeliveryConfirmation;
  }
  
}