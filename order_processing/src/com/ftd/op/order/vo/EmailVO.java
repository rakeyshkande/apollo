package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class EmailVO extends BaseVO
{
  private long      emailId;
  private long      customerId;
  private String    companyId;
  private String    emailAddress;
  private char      activeIndicator;
  private String    subscribeStatus;
  private Date  subscribeDate;
  private Date  createdOn;
  private String    createdBy;
  private Date  updatedOn;
  private String    updatedBy;


  public EmailVO()
  {
  }

  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }

  public long getCustomerId()
  {
    return customerId;
  }

  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = trim(companyId);
  }

  public String getCompanyId()
  {
    return companyId;
  }

  public void setEmailId(long emailId)
  {
    if(valueChanged(this.emailId, emailId))
    {
      setChanged(true);
    }
    this.emailId = emailId;
  }


  public long getEmailId()
  {
    return emailId;
  }


  public void setEmailAddress(String emailAddress)
  {
    if(valueChanged(this.emailAddress, emailAddress))
    {
      setChanged(true);
    }
    this.emailAddress = trim(emailAddress);
  }


  public String getEmailAddress()
  {
    return emailAddress;
  }


  public void setActiveIndicator(char activeIndicator)
  {
    if(valueChanged(this.activeIndicator, activeIndicator))
    {
      setChanged(true);
    }
    this.activeIndicator = activeIndicator;
  }


  public char getActiveIndicator()
  {
    return activeIndicator;
  }


  public void setSubscribeStatus(String subscribeStatus)
  {
    if(valueChanged(this.subscribeStatus, subscribeStatus))
    {
      setChanged(true);
    }
    this.subscribeStatus = trim(subscribeStatus);
  }


  public String getSubscribeStatus()
  {
    return subscribeStatus;
  }


  public void setSubscribeDate(Date subscribeDate)
  {
    if(valueChanged(this.subscribeDate, subscribeDate))
    {
      setChanged(true);
    }
    this.subscribeDate = subscribeDate;
  }


  public Date getSubscribeDate()
  {
    return subscribeDate;
  }

   public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }
  

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}
