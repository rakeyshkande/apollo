package com.ftd.op.order.vo;
import com.ftd.op.common.vo.BaseVO;
import java.util.Date;


public class OrderHoldVO extends BaseVO
{
  
  private String orderDetailId;
  private String holdReason;
  private int timezone;
  private Date releaseDate;
  private int authCount;
  private String status;
  private String holdReasonText;

  public OrderHoldVO()
  {
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setHoldReason(String holdReason)
  {
    this.holdReason = holdReason;
  }


  public String getHoldReason()
  {
    return holdReason;
  }


  public void setTimezone(int timezone)
  {
    this.timezone = timezone;
  }


  public int getTimezone()
  {
    return timezone;
  }


  public void setReleaseDate(Date releaseDate)
  {
    this.releaseDate = releaseDate;
  }


  public Date getReleaseDate()
  {
    return releaseDate;
  }


  public void setAuthCount(int authCount)
  {
    this.authCount = authCount;
  }


  public int getAuthCount()
  {
    return authCount;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setHoldReasonText(String holdReasonText)
  {
    this.holdReasonText = holdReasonText;
  }


  public String getHoldReasonText()
  {
    return holdReasonText;
  }



}
