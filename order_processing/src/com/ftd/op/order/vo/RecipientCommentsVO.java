package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class RecipientCommentsVO extends BaseVO
{
  private long  		commentId;
  private long 		  customerId;
  private String		orderGuid;
  private	long		  orderDetailId;
  private	String		commentOrigin;
  private	String		reason;
  private	long		  dnisId;
  private	String		comment;
  private	String		commentType;
  private Date 	createdOn;
  private String 		createdBy;
  private Date 	updatedOn;
  private String 		updatedBy;

  public RecipientCommentsVO()
  {
  }

  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setCommentId(long commentId)
  {
    if(valueChanged(this.commentId, commentId))
    {
      setChanged(true);
    }
    this.commentId = commentId;
  }


  public long getCommentId()
  {
    return commentId;
  }


  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCommentOrigin(String commentOrigin)
  {
    if(valueChanged(this.commentOrigin, commentOrigin))
    {
      setChanged(true);
    }
    this.commentOrigin = trim(commentOrigin);
  }


  public String getCommentOrigin()
  {
    return commentOrigin;
  }


  public void setReason(String reason)
  {
    if(valueChanged(this.reason, reason))
    {
      setChanged(true);
    }
    this.reason = trim(reason);
  }


  public String getReason()
  {
    return reason;
  }


  public void setDnisId(long dnisId)
  {
    if(valueChanged(this.dnisId, dnisId))
    {
      setChanged(true);
    }
    this.dnisId = dnisId;
  }


  public long getDnisId()
  {
    return dnisId;
  }


  public void setComment(String comment)
  {
    if(valueChanged(this.comment, comment))
    {
      setChanged(true);
    }
    this.comment = trim(comment);
  }


  public String getComment()
  {
    return comment;
  }


  public void setCommentType(String commentType)
  {
    if(valueChanged(this.commentType, commentType))
    {
      setChanged(true);
    }
    this.commentType = trim(commentType);
  }


  public String getCommentType()
  {
    return commentType;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}
