package com.ftd.op.order.vo;

import com.ftd.op.order.to.RWDFloristTO;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.Math;


import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RWDFloristVO extends BaseVO 
{
  private String floristId;
  private int score;
  private int floristWeight;
  private String floristName;
  private String phoneNumber;
  private String sundayDeliveryFlag;
  private String mercuryFlag;
  private String status;
  private String superFloristFlag;
  private String cutoffTime;
  private String codification;
  private boolean codificationBlockFlag;
  private String usedSequence;
  private int priority;
  private String displayStatus;
  private String floristSelectionLogId;
  private String zipCityFlag;
  private String floristSuspended;
  private String minOrderAmt;
  
  public RWDFloristVO()
  {
  }
  
  public void setFloristId(String floristId)
  {
    if(valueChanged(this.floristId, floristId))
    {
      setChanged(true);
    }
    this.floristId = floristId;
  }


  public String getFloristId()
  {
    return floristId;
  }
  
  public void setScore(int score)
  {
    if(valueChanged(this.score, score))
    {
      setChanged(true);
    }
    this.score = score;
  }


  public int getScore()
  {
    return score;
  }
  
  public void setFloristWeight(int floristWeight)
  {
    if(valueChanged(this.floristWeight, floristWeight))
    {
      setChanged(true);
    }
    this.floristWeight = floristWeight;
  }


  public int getFloristWeight()
  {
    return floristWeight;
  }
  
  public void setFloristName(String floristName)
  {
    if(valueChanged(this.floristName, floristName))
    {
      setChanged(true);
    }
    this.floristName = floristName;
  }


  public String getFloristName()
  {
    return floristName;
  }
  
  public void setPhoneNumber(String phoneNumber)
  {
    if(valueChanged(this.phoneNumber, phoneNumber))
    {
      setChanged(true);
    }
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public void setSundayDeliveryFlag(String sundayDeliveryFlag)
  {
    if(valueChanged(this.sundayDeliveryFlag, sundayDeliveryFlag))
    {
      setChanged(true);
    }
    this.sundayDeliveryFlag = sundayDeliveryFlag;
  }


  public String getSundayDeliveryFlag()
  {
    return sundayDeliveryFlag;
  }
  
  public void setMercuryFlag(String mercuryFlag)
  {
    if(valueChanged(this.mercuryFlag, mercuryFlag))
    {
      setChanged(true);
    }
    this.mercuryFlag = mercuryFlag;
  }


  public String getMercuryFlag()
  {
    return mercuryFlag;
  }
  
  public void setStatus(String status)
  {
    if(valueChanged(this.status, status))
    {
      setChanged(true);
    }
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }
  
  public void setSuperFloristFlag(String superFloristFlag)
  {
    if(valueChanged(this.superFloristFlag, superFloristFlag))
    {
      setChanged(true);
    }
    this.superFloristFlag = superFloristFlag;
  }


  public String getSuperFloristFlag()
  {
    return superFloristFlag;
  }
  
  public void setCutoffTime(String cutoffTime)
  {
    if(valueChanged(this.cutoffTime, cutoffTime))
    {
      setChanged(true);
    }
    this.cutoffTime = cutoffTime;
  }


  public String getCutoffTime()
  {
    return cutoffTime;
  }
  
  public int getWeightSquared()
  {
    return this.floristWeight * this.floristWeight/*Math.sqrt(this.floristWeight)*/; 
  }

  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }  
  public RWDFloristTO getRWDFloristTO()
  {
    RWDFloristTO to = new RWDFloristTO();
    to.setCutoffTime(this.getCutoffTime());
    to.setFloristId(this.getFloristId());
    to.setFloristName(this.getFloristName());
    to.setFloristWeight(this.getFloristWeight());
    to.setMercuryFlag(this.getMercuryFlag());
    to.setPhoneNumber(this.getPhoneNumber());
    to.setScore(this.getScore());
    to.setStatus(this.getStatus());
    to.setSundayDeliveryFlag(this.getSundayDeliveryFlag());
    to.setSuperFloristFlag(this.getSuperFloristFlag());
    to.setCodification(this.codification);
    to.setCodificationBlockFlag(this.codificationBlockFlag);
    to.setUsedSequence(this.usedSequence);
    to.setPriority(this.getPriority());
    to.setDisplayStatus(this.getDisplayStatus());
    to.setFloristSelectionLogId(this.getFloristSelectionLogId());
    to.setZipCityFlag(this.getZipCityFlag());
    to.setFloristSuspended(this.getFloristSuspended());
    to.setMinOrderAmt(this.getMinOrderAmt());
    
    return to;
  }


  public void setCodification(String codification)
  {
    this.codification = codification;
  }


  public String getCodification()
  {
    return codification;
  }


  public void setCodificationBlockFlag(boolean codificationBlockFlag)
  {
    this.codificationBlockFlag = codificationBlockFlag;
  }


  public boolean isCodificationBlockFlag()
  {
    return codificationBlockFlag;
  }


  public void setUsedSequence(String usedSequence)
  {
    this.usedSequence = usedSequence;
  }


  public String getUsedSequence()
  {
    return usedSequence;
  }

  public void setPriority(int priority)
  {
    if(valueChanged(this.priority, priority))
    {
      setChanged(true);
    }
    this.priority = priority;
  }

  public int getPriority()
  {
    return priority;
  }

  public void setDisplayStatus(String displayStatus) {
	this.displayStatus = displayStatus;
  }

  public String getDisplayStatus() {
	return displayStatus;
  }
  
  public String getFloristSelectionLogId() {
      return floristSelectionLogId;
  }

  public void setFloristSelectionLogId(String floristSelectionLogId) {
      this.floristSelectionLogId = floristSelectionLogId;
  }

  public String getZipCityFlag() {
	  return zipCityFlag;
  }

  public void setZipCityFlag(String zipCityFlag) {
	  this.zipCityFlag = zipCityFlag;
  }

public String getFloristSuspended() {
	return floristSuspended;
}

public void setFloristSuspended(String floristSuspended) {
	this.floristSuspended = floristSuspended;
}

public String getMinOrderAmt() {
	return minOrderAmt;
}

public void setMinOrderAmt(String minOrderAmt) {
	this.minOrderAmt = minOrderAmt;
}

}
