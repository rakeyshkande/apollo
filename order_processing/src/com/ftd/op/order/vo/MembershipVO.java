package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class MembershipVO extends BaseVO
{

  private long      membershipId;
  private long      customerId;
  private String    membershipNumber;
  private String    membershipType;
  private String    firstName;
  private String    lastName;
  private Date  createdOn;
  private String    createdBy;
  private Date  updatedOn;
  private String    updatedBy;

  public MembershipVO()
  {
  }

    public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }
    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setMembershipId(long membershipId)
  {
    if(valueChanged(this.membershipId, membershipId))
    {
      setChanged(true);
    }
    this.membershipId = membershipId;
  }


  public long getMembershipId()
  {
    return membershipId;
  }


  public void setMembershipNumber(String membershipNumber)
  {
    if(valueChanged(this.membershipNumber, membershipNumber))
    {
      setChanged(true);
    }
    this.membershipNumber = trim(membershipNumber);
  }


  public String getMembershipNumber()
  {
    return membershipNumber;
  }


  public void setMembershipType(String membershipType)
  {
    if(valueChanged(this.membershipType, membershipType))
    {
      setChanged(true);
    }
    this.membershipType = trim(membershipType);
  }


  public String getMembershipType()
  {
    return membershipType;
  }


  public void setFirstName(String firstName)
  {
    if(valueChanged(this.firstName, firstName))
    {
      setChanged(true);
    }
    this.firstName = trim(firstName);
  }


  public String getFirstName()
  {
    return firstName;
  }


  public void setLastName(String lastName)
  {
    if(valueChanged(this.lastName, lastName))
    {
      setChanged(true);
    }
    this.lastName = trim(lastName);
  }


  public String getLastName()
  {
    return lastName;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }







}
