package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class RecipientHistoryVO extends BaseVO
{
  private long customerHistoryId;
  private long customerId;
  private String csrId;
  private String activity;
  private Date createdOn;

  public RecipientHistoryVO()
  {
  }

  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCustomerHistoryId(long customerHistoryId)
  {
    if(valueChanged(this.customerHistoryId, customerHistoryId))
    {
      setChanged(true);
    }
    this.customerHistoryId = customerHistoryId;
  }


  public long getCustomerHistoryId()
  {
    return customerHistoryId;
  }


  public void setCsrId(String csrId)
  {
    if(valueChanged(this.csrId, csrId))
    {
      setChanged(true);
    }
    this.csrId = trim(csrId);
  }


  public String getCsrId()
  {
    return csrId;
  }


  public void setActivity(String activity)
  {
    if(valueChanged(this.activity, activity))
    {
      setChanged(true);
    }
    this.activity = trim(activity);
  }


  public String getActivity()
  {
    return activity;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}
