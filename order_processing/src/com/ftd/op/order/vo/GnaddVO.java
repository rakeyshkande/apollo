package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class GnaddVO extends BaseVO
{
  private String gnaddStatus;
  private String gnaddDate;
  private String gnaddHoldFlag;
  
  public GnaddVO()
  {
  }
  
  public void setGnaddStatus(String gnaddStatus)
  {
    if(valueChanged(this.gnaddStatus, gnaddStatus))
    {
      setChanged(true);
    }
    this.gnaddStatus = trim(gnaddStatus);
  }
  
  public String getGnaddStatus()
  {
    return gnaddStatus;
  }
  
  public void setGnaddDate(String gnaddDate)
  {
    if(valueChanged(this.gnaddDate, gnaddDate))
    {
      setChanged(true);
    }
    this.gnaddDate = gnaddDate;
  }
  
  public String getGnaddDate()
  {
    return gnaddDate;
  }
  
  public void setGnaddHoldFlag(String gnaddHoldFlag)
  {
    if(valueChanged(this.gnaddHoldFlag, gnaddHoldFlag))
    {
      setChanged(true);
    }
    this.gnaddHoldFlag = trim(gnaddHoldFlag);
  }
  
  public String getGnaddHoldFlag()
  {
    return gnaddHoldFlag;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
  
}