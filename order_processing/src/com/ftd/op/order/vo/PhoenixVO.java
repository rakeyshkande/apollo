package com.ftd.op.order.vo;

import java.util.Date;

@SuppressWarnings("serial")
public class PhoenixVO extends BaseVO {

	private long orderDetailId;
	private String externalOrderNumber;
	private String recordSource;
	private String productId;
	private String phoenixProductId;
	private Date deliveryDate;
	private String recipientName;
	private String recipientAddress1;
	private String recipientAddress2;
	private String businessName;
	private String recipientCity;
	private String recipientState;
	private String recipientZipCode;
	private String recipientPhoneNumber;
	private String recipientCountry;
	private String cardMessage;
    private String emailAddress;
    private String emailBody;
    private String emailSubject;
    private Date orderDate;
    private String partnerName;
    private String gcProgramName;
    private String shipMethod;
    private Date shipDate;
    private String mercuryId;
    private String mercuryAddress;
	private String bearAddonId;
	private String phoenixBearAddonId;
	private String chocAddonId;
	private String phoenixChocAddonId;
	private String companyId;
	private String sourceCode;

    public PhoenixVO() {
    }

    
    
    public void setOrderDetailId(long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public long getOrderDetailId() {
		return orderDetailId;
	}

	public void setExternalOrderNumber(String externalOrderNumber) {
	    this.externalOrderNumber = externalOrderNumber;
    }

    public String getExternalOrderNumber() {
	    return externalOrderNumber;
    }

	public void setRecordSource(String recordSource) {
		this.recordSource = recordSource;
	}

	public String getRecordSource() {
		return recordSource;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setPhoenixProductId(String phoenixProductId) {
		this.phoenixProductId = phoenixProductId;
	}

	public String getPhoenixProductId() {
		return phoenixProductId;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientAddress1(String recipientAddress1) {
		this.recipientAddress1 = recipientAddress1;
	}

	public String getRecipientAddress1() {
		return recipientAddress1;
	}

	public void setRecipientAddress2(String recipientAddress2) {
		this.recipientAddress2 = recipientAddress2;
	}

	public String getRecipientAddress2() {
		return recipientAddress2;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientState(String recipientState) {
		this.recipientState = recipientState;
	}

	public String getRecipientState() {
		return recipientState;
	}

	public void setRecipientZipCode(String recipientZipCode) {
		this.recipientZipCode = recipientZipCode;
	}

	public String getRecipientZipCode() {
		return recipientZipCode;
	}

	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}

	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}

	public void setRecipientCountry(String recipientCountry) {
		this.recipientCountry = recipientCountry;
	}

	public String getRecipientCountry() {
		return recipientCountry;
	}

	public void setCardMessage(String cardMessage) {
		this.cardMessage = cardMessage;
	}

	public String getCardMessage() {
		return cardMessage;
	}

	public void setEmailAddress(String emailAddress) {
	    this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
	    return emailAddress;
    }

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setGcProgramName(String gcProgramName) {
		this.gcProgramName = gcProgramName;
	}

	public String getGcProgramName() {
		return gcProgramName;
	}

	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}

	public String getShipMethod() {
		return shipMethod;
	}

	public void setShipDate(Date shipDate) {
		this.shipDate = shipDate;
	}

	public Date getShipDate() {
		return shipDate;
	}

	public String getMercuryId() {
		return mercuryId;
	}
	public void setMercuryId(String mercuryId) {
		this.mercuryId = mercuryId;
	}

	public void setMercuryAddress(String mercuryAddress) {
		this.mercuryAddress = mercuryAddress;
	}

	public String getMercuryAddress() {
		return mercuryAddress;
	}



	public String getBearAddonId() {
		return bearAddonId;
	}



	public void setBearAddonId(String bearAddonId) {
		this.bearAddonId = bearAddonId;
	}



	public String getPhoenixBearAddonId() {
		return phoenixBearAddonId;
	}



	public void setPhoenixBearAddonId(String phoenixBearAddonId) {
		this.phoenixBearAddonId = phoenixBearAddonId;
	}



	public String getChocAddonId() {
		return chocAddonId;
	}



	public void setChocAddonId(String chocAddonId) {
		this.chocAddonId = chocAddonId;
	}



	public String getPhoenixChocAddonId() {
		return phoenixChocAddonId;
	}



	public void setPhoenixChocAddonId(String phoenixChocAddonId) {
		this.phoenixChocAddonId = phoenixChocAddonId;
	}

	public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getSourceCode() {
        return sourceCode;
    }
    
    public String setSourceCode(String sourceCode) {
        return this.sourceCode = sourceCode;
    }



	public String getEmailBody() {
		return emailBody;
	}



	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}



	public String getEmailSubject() {
		return emailSubject;
	}



	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
}
