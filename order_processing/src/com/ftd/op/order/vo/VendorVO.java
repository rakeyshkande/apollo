package com.ftd.op.order.vo;


public class VendorVO extends BaseVO {

    private String vendorId;
    private String vendorName;
    private String vendorType;
    private String memberNumber;

    public VendorVO() {
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        if (valueChanged(this.vendorId, vendorId)) {
            setChanged(true);
        }
        this.vendorId = trim(vendorId);
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        if (valueChanged(this.vendorName, vendorName)) {
            setChanged(true);
        }
        this.vendorName = trim(vendorName);
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        if (valueChanged(this.vendorType, vendorType)) {
            setChanged(true);
        }
        this.vendorType = vendorType;
    }

    private String trim(String str) {
        return (str != null) ? str.trim() : str;
    }


    public void setVendorMemberNumber(String vendorMemberNumber) {
        this.memberNumber = vendorMemberNumber;
    }


    public String getVendorMemberNumber() {
        return memberNumber;
    }
}
