package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class OrderBillVO extends BaseVO
{
  private	long	    orderBillId;
  private	long	    orderDetailId;
  private	double	  productAmount;
  private	double	  addOnAmount;
  private	double	  serviceFee;
  private	double	  shippingFee;
  private	double	  discountAmount;
  private	double	  shippingTax;
  private	double	  tax;
  private	String    additionalBillIndicator;
  private   double    pdbPrice;
  
  public OrderBillVO()
  {
  }

  public void setOrderBillId(long orderBillId)
  {
    if(valueChanged(this.orderBillId, orderBillId))
    {
      setChanged(true);
    }
    this.orderBillId = orderBillId;
  }


  public long getOrderBillId()
  {
    return orderBillId;
  }


  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setProductAmount(double productAmount)
  {
    if(valueChanged(new Double(this.productAmount), new Double(productAmount)))
    {
      setChanged(true);
    }
    this.productAmount = productAmount;
  }


  public double getProductAmount()
  {
    return productAmount;
  }


  public void setAddOnAmount(double addOnAmount)
  {
    if(valueChanged(new Double(this.addOnAmount), new Double(addOnAmount)))
    {
      setChanged(true);
    }
    this.addOnAmount = addOnAmount;
  }


  public double getAddOnAmount()
  {
    return addOnAmount;
  }


  public void setServiceFee(double serviceFee)
  {
    if(valueChanged(new Double(this.serviceFee), new Double(serviceFee)))
    {
      setChanged(true);
    }
    this.serviceFee = serviceFee;
  }


  public double getServiceFee()
  {
    return serviceFee;
  }


  public void setShippingFee(double shippingFee)
  {
    if(valueChanged(new Double(this.shippingFee), new Double(shippingFee)))
    {
      setChanged(true);
    }
    this.shippingFee = shippingFee;
  }


  public double getShippingFee()
  {
    return shippingFee;
  }


  public void setDiscountAmount(double discountAmount)
  {
    if(valueChanged(new Double(this.discountAmount), new Double(discountAmount)))
    {
      setChanged(true);
    }
    this.discountAmount = discountAmount;
  }


  public double getDiscountAmount()
  {
    return discountAmount;
  }


  public void setShippingTax(double shippingTax)
  {
    if(valueChanged(new Double(this.shippingTax), new Double(shippingTax)))
    {
      setChanged(true);
    }
    this.shippingTax = shippingTax;
  }


  public double getShippingTax()
  {
    return shippingTax;
  }


  public void setTax(double tax)
  {
     if(valueChanged(new Double(this.tax), new Double(tax)))
    {
      setChanged(true);
    }
   this.tax = tax;
  }


  public double getTax()
  {
    return tax;
  }


  public void setAdditionalBillIndicator(String additionalBillIndicator)
  {
    this.additionalBillIndicator = additionalBillIndicator;
  }


  public String getAdditionalBillIndicator()
  {
    return additionalBillIndicator;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

  public double getPdbPrice() {
	return pdbPrice;
  }
	
  public void setPdbPrice(double pdbPrice) {
		
	if(valueChanged(new Double(this.pdbPrice), new Double(pdbPrice)))
	{
	  setChanged(true);
	}
	this.pdbPrice = pdbPrice;
	}
  
}
