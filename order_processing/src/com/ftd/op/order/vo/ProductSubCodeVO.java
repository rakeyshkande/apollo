package com.ftd.op.order.vo;

public class ProductSubCodeVO
{
    private String productSubCodeId;
    private String productId;
    private String subCodeDescription;
    private Double subCodePrice;
    private String subCodeHolidaySKU;
    private Double subCodeHolidayPrice;
    private boolean available;
    private String subCodeRefNumber;
    private String dimWeight;
    private int displayOrder;

    public ProductSubCodeVO()
    {
        productSubCodeId = "";
        productId = "";
        subCodeDescription = "";
        subCodeHolidaySKU = "";
        subCodeRefNumber = "";
    }

    public String getProductSubCodeId()
    {
        return productSubCodeId;
    }

    public void setProductSubCodeId(String newProductSubCodeId)
    {
        productSubCodeId = newProductSubCodeId;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String newProductId)
    {
        productId = newProductId;
    }

    public String getSubCodeDescription()
    {
        return subCodeDescription;
    }

    public void setSubCodeDescription(String newSubCodeDescription)
    {
        subCodeDescription = newSubCodeDescription;
    }

    public Double getSubCodePrice()
    {
        return subCodePrice;
    }

    public void setSubCodePrice(Double newSubCodePrice)
    {
        subCodePrice = newSubCodePrice;
    }

    public Double getSubCodeHolidayPrice()
    {
        return subCodeHolidayPrice;
    }

    public void setSubCodeHolidayPrice(Double newSubCodeHolidayPrice)
    {
        subCodeHolidayPrice = newSubCodeHolidayPrice;
    }

    public String getSubCodeHolidaySKU()
    {
        return subCodeHolidaySKU;
    }

    public void setSubCodeHolidaySKU(String newSubCodeHolidaySKU)
    {
        subCodeHolidaySKU = newSubCodeHolidaySKU;
    }

    public boolean isAvailable()
    {
        return available;
    }

    public void setAvailable(boolean newAvailable)
    {
        available = newAvailable;
    }

    public String getSubCodeRefNumber()
    {
        return subCodeRefNumber;
    }

    public void setSubCodeRefNumber(String newSubCodeRefNumber)
    {
        subCodeRefNumber = newSubCodeRefNumber;
    }

    public String getDimWeight()
    {
        return dimWeight;
    }

    public void setDimWeight(String newDimWeight)
    {
        dimWeight = newDimWeight;
    }

    public int getDisplayOrder() 
    {
        return displayOrder;
    }

    public void setDisplayOrder(int newDisplayOrder)
    {
        displayOrder = newDisplayOrder;
    }
}