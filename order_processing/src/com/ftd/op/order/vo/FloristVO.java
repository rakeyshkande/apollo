package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class FloristVO extends BaseVO
{
  private String floristId;
  private String floristName;
  private String address;
  private String city;
  private String state;
  private String zipCode;
  private String phoneNumber;
  private String cityStateNumber;
  private String recordType;
  private String mercuryFlag;
  private int floristWeight;
  private String superFloristFlag;
  private String sundayDeliveryFlag;
  private String deliveryCity;
  private String deliveryState;
  private String ownersName;
  private double minimumOrderAmount;
  private String status;
  private String territory;
  private String faxNumber;
  private String emailAddress;
  private int initialWeight;
  private int adjustedWeight;
  private Date adjustedWeightStartDate;
  private Date adjustedWeightEndDate;
  private String adjustedWeightPermenantFlag;
  private String topLevelFloristId;
  private String parentFloristId;
  private String altPhoneNumber;
  private int ordersSent;
  private int sendingRank;
  private Date sundayDeliveryBlockStDt;
  private Date sundayDeliveryBlockEndDt;
  private String lockedBy;
  private Date lockedDate;
  private String internalLinkNumber;
  private String vendorFlag;
  private String suspendOverrideFlag;
  private String allowMessageForwardingFlag;
  private String isFTOFlag;
  // florist block members//
  private String blockType;
  private Date blockStartDate;
  private Date blockEndDate;
  private String blockedByUserId;
  private String suspendType;
  private Date suspendStartDate;
  private Date suspendEndDate;
  
  
  // florist lock members
   private String lockedFlag;
   private String lockedByUser;
  
  public FloristVO()
  {
  }
  
  public void setFloristId(String floristId)
  {
    if(valueChanged(this.floristId, floristId))
    {
      setChanged(true);
    }
    this.floristId = floristId;
  }


  public String getFloristId()
  {
    return floristId;
  }


  public void setFloristName(String floristName)
  {
    if(valueChanged(this.floristName, floristName))
    {
      setChanged(true);
    }
    this.floristName = floristName;
  }


  public String getFloristName()
  {
    return floristName;
  }


  public void setAddress(String address)
  {
    if(valueChanged(this.address, address))
    {
      setChanged(true);
    }
    this.address = address;
  }


  public String getAddress()
  {
    return address;
  }


  public void setCity(String city)
  {
    if(valueChanged(this.city, city))
    {
      setChanged(true);
    }
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    if(valueChanged(this.state, state))
    {
      setChanged(true);
    }
    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZipCode(String zipCode)
  {
    if(valueChanged(this.zipCode, zipCode))
    {
      setChanged(true);
    }
    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    if(valueChanged(this.phoneNumber, phoneNumber))
    {
      setChanged(true);
    }
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setCityStateNumber(String cityStateNumber)
  {
    if(valueChanged(this.cityStateNumber, cityStateNumber))
    {
      setChanged(true);
    }
    this.cityStateNumber = cityStateNumber;
  }


  public String getCityStateNumber()
  {
    return cityStateNumber;
  }


  public void setRecordType(String recordType)
  {
    if(valueChanged(this.recordType, recordType))
    {
      setChanged(true);
    }
    this.recordType = recordType;
  }


  public String getRecordType()
  {
    return recordType;
  }


  public void setMercuryFlag(String mercuryFlag)
  {
    if(valueChanged(this.mercuryFlag, mercuryFlag))
    {
      setChanged(true);
    }
    this.mercuryFlag = mercuryFlag;
  }


  public String getMercuryFlag()
  {
    return mercuryFlag;
  }


  public void setFloristWeight(int floristWeight)
  {
    if(valueChanged(this.floristWeight, floristWeight))
    {
      setChanged(true);
    }
    this.floristWeight = floristWeight;
  }


  public int getFloristWeight()
  {
    return floristWeight;
  }


  public void setSuperFloristFlag(String superFloristFlag)
  {
    if(valueChanged(this.superFloristFlag, superFloristFlag))
    {
      setChanged(true);
    }
    this.superFloristFlag = superFloristFlag;
  }


  public String getSuperFloristFlag()
  {
    return superFloristFlag;
  }


  public void setSundayDeliveryFlag(String sundayDeliveryFlag)
  {
    if(valueChanged(this.sundayDeliveryFlag, sundayDeliveryFlag))
    {
      setChanged(true);
    }
    this.sundayDeliveryFlag = sundayDeliveryFlag;
  }


  public String getSundayDeliveryFlag()
  {
    return sundayDeliveryFlag;
  }


  public void setDeliveryCity(String deliveryCity)
  {
    if(valueChanged(this.deliveryCity, deliveryCity))
    {
      setChanged(true);
    }
    this.deliveryCity = deliveryCity;
  }


  public String getDeliveryCity()
  {
    return deliveryCity;
  }


  public void setDeliveryState(String deliveryState)
  {
    if(valueChanged(this.deliveryState, deliveryState))
    {
      setChanged(true);
    }
    this.deliveryState = deliveryState;
  }


  public String getDeliveryState()
  {
    return deliveryState;
  }


  public void setOwnersName(String ownersName)
  {
    if(valueChanged(this.ownersName, ownersName))
    {
      setChanged(true);
    }
    this.ownersName = ownersName;
  }


  public String getOwnersName()
  {
    return ownersName;
  }


  public void setMinimumOrderAmount(double minimumOrderAmount)
  {
    this.minimumOrderAmount = minimumOrderAmount;
  }


  public double getMinimumOrderAmount()
  {
    return minimumOrderAmount;
  }


  public void setStatus(String status)
  {
    if(valueChanged(this.status, status))
    {
      setChanged(true);
    }
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setTerritory(String territory)
  {
    if(valueChanged(this.territory, territory))
    {
      setChanged(true);
    }
    this.territory = territory;
  }


  public String getTerritory()
  {
    return territory;
  }


  public void setFaxNumber(String faxNumber)
  {
    if(valueChanged(this.faxNumber, faxNumber))
    {
      setChanged(true);
    }
    this.faxNumber = faxNumber;
  }


  public String getFaxNumber()
  {
    return faxNumber;
  }


  public void setEmailAddress(String emailAddress)
  {
    if(valueChanged(this.emailAddress, emailAddress))
    {
      setChanged(true);
    }
    this.emailAddress = emailAddress;
  }


  public String getEmailAddress()
  {
    return emailAddress;
  }


  public void setInitialWeight(int initialWeight)
  {
    if(valueChanged(this.initialWeight, initialWeight))
    {
      setChanged(true);
    }
    this.initialWeight = initialWeight;
  }


  public int getInitialWeight()
  {
    return initialWeight;
  }


  public void setAdjustedWeight(int adjustedWeight)
  {
    if(valueChanged(this.adjustedWeight, adjustedWeight))
    {
      setChanged(true);
    }
    this.adjustedWeight = adjustedWeight;
  }


  public int getAdjustedWeight()
  {
    return adjustedWeight;
  }


  public void setAdjustedWeightStartDate(Date adjustedWeightStartDate)
  {
    if(valueChanged(this.adjustedWeightStartDate, adjustedWeightStartDate))
    {
      setChanged(true);
    }
    this.adjustedWeightStartDate = adjustedWeightStartDate;
  }


  public Date getAdjustedWeightStartDate()
  {
    return adjustedWeightStartDate;
  }


  public void setAdjustedWeightEndDate(Date adjustedWeightEndDate)
  {
    if(valueChanged(this.adjustedWeightEndDate, adjustedWeightEndDate))
    {
      setChanged(true);
    }
    this.adjustedWeightEndDate = adjustedWeightEndDate;
  }


  public Date getAdjustedWeightEndDate()
  {
    return adjustedWeightEndDate;
  }


  public void setAdjustedWeightPermenantFlag(String adjustedWeightPermenantFlag)
  {
    if(valueChanged(this.adjustedWeightPermenantFlag, adjustedWeightPermenantFlag))
    {
      setChanged(true);
    }
    this.adjustedWeightPermenantFlag = adjustedWeightPermenantFlag;
  }


  public String getAdjustedWeightPermenantFlag()
  {
    return adjustedWeightPermenantFlag;
  }


  public void setTopLevelFloristId(String topLevelFloristId)
  {
    if(valueChanged(this.topLevelFloristId, topLevelFloristId))
    {
      setChanged(true);
    }
    this.topLevelFloristId = topLevelFloristId;
  }


  public String getTopLevelFloristId()
  {
    return topLevelFloristId;
  }


  public void setParentFloristId(String parentFloristId)
  {
    if(valueChanged(this.parentFloristId, parentFloristId))
    {
      setChanged(true);
    }
    this.parentFloristId = parentFloristId;
  }


  public String getParentFloristId()
  {
    return parentFloristId;
  }


  public void setAltPhoneNumber(String altPhoneNumber)
  {
    if(valueChanged(this.altPhoneNumber, altPhoneNumber))
    {
      setChanged(true);
    }
    this.altPhoneNumber = altPhoneNumber;
  }


  public String getAltPhoneNumber()
  {
    return altPhoneNumber;
  }


  public void setOrdersSent(int ordersSent)
  {
    if(valueChanged(this.ordersSent, ordersSent))
    {
      setChanged(true);
    }
    this.ordersSent = ordersSent;
  }


  public int getOrdersSent()
  {
    return ordersSent;
  }


  public void setSendingRank(int sendingRank)
  {
    if(valueChanged(this.sendingRank, sendingRank))
    {
      setChanged(true);
    }
    this.sendingRank = sendingRank;
  }


  public int getSendingRank()
  {
    return sendingRank;
  }


  public void setSundayDeliveryBlockStDt(Date sundayDeliveryBlockStDt)
  {
    if(valueChanged(this.sundayDeliveryBlockStDt, sundayDeliveryBlockStDt))
    {
      setChanged(true);
    }
    this.sundayDeliveryBlockStDt = sundayDeliveryBlockStDt;
  }


  public Date getSundayDeliveryBlockStDt()
  {
    return sundayDeliveryBlockStDt;
  }


  public void setSundayDeliveryBlockEndDt(Date sundayDeliveryBlockEndDt)
  {
    if(valueChanged(this.sundayDeliveryBlockEndDt, sundayDeliveryBlockEndDt))
    {
      setChanged(true);
    }
    this.sundayDeliveryBlockEndDt = sundayDeliveryBlockEndDt;
  }


  public Date getSundayDeliveryBlockEndDt()
  {
    return sundayDeliveryBlockEndDt;
  }


  public void setLockedBy(String lockedBy)
  {
    if(valueChanged(this.lockedBy, lockedBy))
    {
      setChanged(true);
    }
    this.lockedBy = lockedBy;
  }


  public String getLockedBy()
  {
    return lockedBy;
  }


  public void setLockedDate(Date lockedDate)
  {
    if(valueChanged(this.lockedDate, lockedDate))
    {
      setChanged(true);
    }
    this.lockedDate = lockedDate;
  }


  public Date getLockedDate()
  {
    return lockedDate;
  }


  public void setInternalLinkNumber(String internalLinkNumber)
  {
    if(valueChanged(this.internalLinkNumber, internalLinkNumber))
    {
      setChanged(true);
    }
    this.internalLinkNumber = internalLinkNumber;
  }


  public String getInternalLinkNumber()
  {
    return internalLinkNumber;
  }


  public void setVendorFlag(String vendorFlag)
  {
    if(valueChanged(this.vendorFlag, vendorFlag))
    {
      setChanged(true);
    }
    this.vendorFlag = vendorFlag;
  }


  public String getVendorFlag()
  {
    return vendorFlag;
  }


  public void setSuspendOverrideFlag(String suspendOverrideFlag)
  {
    if(valueChanged(this.suspendOverrideFlag, suspendOverrideFlag))
    {
      setChanged(true);
    }
    this.suspendOverrideFlag = suspendOverrideFlag;
  }


  public String getSuspendOverrideFlag()
  {
    return suspendOverrideFlag;
  }
  
  public int getWeightSquared()
  {
    return this.floristWeight * 2;  
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setAllowMessageForwardingFlag(String allowMessageForwardingFlag)
  {
    this.allowMessageForwardingFlag = allowMessageForwardingFlag;
  }


  public String getAllowMessageForwardingFlag()
  {
    return allowMessageForwardingFlag;
  }


  public void setIsFTOFlag(String isFTOFlag)
  {
    this.isFTOFlag = isFTOFlag;
  }


  public String getIsFTOFlag()
  {
    return isFTOFlag;
  }


  public void setBlockType(String blockType)
  {
    this.blockType = blockType;
  }


  public String getBlockType()
  {
    return blockType;
  }


  public void setBlockStartDate(Date blockStartDate)
  {
    this.blockStartDate = blockStartDate;
  }


  public Date getBlockStartDate()
  {
    return blockStartDate;
  }


  public void setBlockEndDate(Date blockEndDate)
  {
    this.blockEndDate = blockEndDate;
  }


  public Date getBlockEndDate()
  {
    return blockEndDate;
  }


  public void setBlockedByUserId(String blockedByUserId)
  {
    this.blockedByUserId = blockedByUserId;
  }


  public String getBlockedByUserId()
  {
    return blockedByUserId;
  }


  public void setSuspendType(String suspendType)
  {
    this.suspendType = suspendType;
  }


  public String getSuspendType()
  {
    return suspendType;
  }


  public void setSuspendStartDate(Date suspendStartDate)
  {
    this.suspendStartDate = suspendStartDate;
  }


  public Date getSuspendStartDate()
  {
    return suspendStartDate;
  }


  public void setSuspendEndDate(Date suspendEndDate)
  {
    this.suspendEndDate = suspendEndDate;
  }


  public Date getSuspendEndDate()
  {
    return suspendEndDate;
  }

    public void setLockedFlag(String lockedFlag) {
        this.lockedFlag = lockedFlag;
    }

    public String getLockedFlag() {
        return lockedFlag;
    }

    public void setLockedByUser(String lockedByUser) {
        this.lockedByUser = lockedByUser;
    }

    public String getLockedByUser() {
        return lockedByUser;
    }
}
