package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class AddOnVO extends BaseVO
{
  private	long	    addOnId;
  private	long	    orderDetailId;
  private	String	  addOnCode;
  private	long	    addOnQuantity;
  private String desciption;
  private Double price;
  private	Date	createdOn;
  private	String 	  createdBy;
  private	Date	updatedOn;
  private	String 	  updatedBy;

  public AddOnVO()
  {
  }

  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setAddOnId(long addOnId)
  {
    if(valueChanged(this.addOnId, addOnId))
    {
      setChanged(true);
    }
    this.addOnId = addOnId;
  }


  public long getAddOnId()
  {
    return addOnId;
  }


  public void setAddOnCode(String addOnCode)
  {
    if(valueChanged(this.addOnCode, addOnCode))
    {
      setChanged(true);
    }
    this.addOnCode = trim(addOnCode);
  }


  public String getAddOnCode()
  {
    return addOnCode;
  }


  public void setAddOnQuantity(long addOnQuantity)
  {
    if(valueChanged(this.addOnQuantity, addOnQuantity))
    {
      setChanged(true);
    }
    this.addOnQuantity = addOnQuantity;
  }


  public long getAddOnQuantity()
  {
    return addOnQuantity;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setDesciption(String desciption)
  {
    this.desciption = desciption;
  }


  public String getDesciption()
  {
    return desciption;
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


}
