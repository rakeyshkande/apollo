package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class OrderDetailHoldVO extends BaseVO
{

  private	long	    orderDetailId;
  private	String	  reason;
  private	char	    holdPostHoliday;
  private	Date	createdOn;
  private	String 	  createdBy;
  private	Date	updatedOn;
  private	String 	  updatedBy;


  public OrderDetailHoldVO()
  {
  }


  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setReason(String reason)
  {
    if(valueChanged(this.reason, reason))
    {
      setChanged(true);
    }
    this.reason = trim(reason);
  }


  public String getReason()
  {
    return reason;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setHoldPostHoliday(char holdPostHoliday)
  {
    if(valueChanged(this.holdPostHoliday, holdPostHoliday))
    {
      setChanged(true);
    }
    this.holdPostHoliday = holdPostHoliday;
  }


  public char getHoldPostHoliday()
  {
    return holdPostHoliday;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

}
