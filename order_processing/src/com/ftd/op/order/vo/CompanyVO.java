package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.List;
import java.util.Date;

public class CompanyVO extends BaseVO
{
  private String  companyId;
  private String  companyName;
  private String  internetOrigin;
  private long    defaultProgramId;
  private String  phoneNumber;
  private String  clearingMemberNumber;
  private String  logoFileName;
  private String  enableLpProcessing;
    private String url;
    
  public CompanyVO()
  {
  }
  
  public void setCompanyId(String companyId)
  {
    if(valueChanged(this.companyId, companyId))
    {
      setChanged(true);
    }
    this.companyId = trim(companyId);
  }
  
  public String getCompanyId()
  {
    return companyId;
  }
 
  public void setCompanyName(String companyName)
  {
    if(valueChanged(this.companyName, companyName))
    {
      setChanged(true);
    }
    this.companyName = trim(companyName);
  }
  
  public String getCompanyName()
  {
    return companyName;
  }
  
  public void setInternetOrigin(String internetOrigin)
  {
    if(valueChanged(this.internetOrigin, internetOrigin))
    {
      setChanged(true);
    }
    this.internetOrigin = trim(internetOrigin);
  }
  
  public String getInternetOrigin()
  {
    return internetOrigin;
  }
  
  public void setDefaultProgramId(long defaultProgramId)
  {
    if(valueChanged(this.defaultProgramId, defaultProgramId))
    {
      setChanged(true);
    }
    this.defaultProgramId = defaultProgramId;
  }
  
  public long getDefaultProgramId()
  {
    return defaultProgramId;
  }
  
  public void setPhoneNumber(String phoneNumber)
  {
    if(valueChanged(this.phoneNumber, phoneNumber))
    {
      setChanged(true);
    }
    this.phoneNumber = phoneNumber;
  }
  
  public String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public void setClearingMemberNumber(String clearingMemberNumber)
  {
    if(valueChanged(this.clearingMemberNumber, clearingMemberNumber))
    {
      setChanged(true);
    }
    this.clearingMemberNumber = clearingMemberNumber;
  }
  
  public String getClearingMemberNumber()
  {
    return clearingMemberNumber;
  }
  
  public void setLogoFileName(String logoFileName)
  {
    if(valueChanged(this.logoFileName, logoFileName))
    {
      setChanged(true);
    }
    this.logoFileName = logoFileName;
  }
  
  public String getLogoFileName()
  {
    return logoFileName;
  }
  
  public void setEnableLpProcessing(String enableLpProcessing)
  {
    if(valueChanged(this.enableLpProcessing, enableLpProcessing))
    {
      setChanged(true);
    }
    this.enableLpProcessing = trim(enableLpProcessing);
  }
  
  public String getEnableLpProcessing()
  {
    return enableLpProcessing;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

    public String getURL()
    {
        return url;
    }

    public void setURL(String newUrl)
    {
        this.url = newUrl;
    }
  
}
