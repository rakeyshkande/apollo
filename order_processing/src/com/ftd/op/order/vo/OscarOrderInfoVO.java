package com.ftd.op.order.vo;

import java.util.Date;

public class OscarOrderInfoVO {
    
    // Needed for Oscar call
    //
    private String orderDetailId;
    private String sendingFlorist;
    private String scenarioGroup;
    private Date deliveryDate;
    private String latitude;
    private String longitude;
    
    // Needed to help make decision if Oscar should be used
    //
    private boolean sourceCodeCriteriaMatched;
    private boolean zipCriteriaMatched;
    private boolean cityStateCriteriaMatched;
    
    
    public String getOrderDetailId() {
        return orderDetailId;
    }
    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }
    public String getSendingFlorist() {
        return sendingFlorist;
    }
    public void setSendingFlorist(String sendingFlorist) {
        this.sendingFlorist = sendingFlorist;
    }
    public String getScenarioGroup() {
        return scenarioGroup;
    }
    public void setScenarioGroup(String scenarioGroup) {
        this.scenarioGroup = scenarioGroup;
    }
    public Date getDeliveryDate() {
        return deliveryDate;
    }
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    public String getLatitude() {
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public String getLongitude() {
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public void setSourceCodeCriteriaMatched(boolean sourceCodeCriteriaMatched) {
        this.sourceCodeCriteriaMatched = sourceCodeCriteriaMatched;
    }
    public boolean isSourceCodeCriteriaMatched() {
        return sourceCodeCriteriaMatched;
    }
    public void setZipCriteriaMatched(boolean zipCriteriaMatched) {
        this.zipCriteriaMatched = zipCriteriaMatched;
    }
    public boolean isZipCriteriaMatched() {
        return zipCriteriaMatched;
    }
    public void setCityStateCriteriaMatched(boolean cityStateCriteriaMatched) {
        this.cityStateCriteriaMatched = cityStateCriteriaMatched;
    }
    public boolean isCityStateCriteriaMatched() {
        return cityStateCriteriaMatched;
    }

}
