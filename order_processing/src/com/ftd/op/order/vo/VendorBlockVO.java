package com.ftd.op.order.vo;

import com.ftd.op.common.vo.BaseVO;
import java.util.Date;

public class VendorBlockVO extends BaseVO
{
  private String vendorId;
  private Date startDate;
  private Date endDate;
  
  public VendorBlockVO()
  {
  }


  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }


  public String getVendorId()
  {
    return vendorId;
  }


  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }


  public Date getStartDate()
  {
    return startDate;
  }


  public void setEndDate(Date endDate)
  {
    this.endDate = endDate;
  }


  public Date getEndDate()
  {
    return endDate;
  }



}