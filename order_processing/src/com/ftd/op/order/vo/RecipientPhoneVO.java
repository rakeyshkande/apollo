package com.ftd.op.order.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class RecipientPhoneVO extends BaseVO
{
  private long      phoneId;
  private long      customerId;
  private String    phoneType;
  private String    phoneNumber;
  private String    extension;
  private Date  createdOn;
  private String    createdBy;
  private Date  updatedOn;
  private String    updatedBy;

  public RecipientPhoneVO()
  {
  }

  public void setCustomerId(long customerId)
  {
    if(valueChanged(this.customerId, customerId ))
    {
      setChanged(true);
    }
    this.customerId = customerId;
  }

  public long getCustomerId()
  {
    return customerId;
  }

  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setPhoneId(long phoneId)
  {
    if(valueChanged(this.phoneId, phoneId))
    {
      setChanged(true);
    }
    this.phoneId = phoneId;
  }


  public long getPhoneId()
  {
    return phoneId;
  }


  public void setPhoneType(String phoneType)
  {
    if(valueChanged(this.phoneType, phoneType))
    {
      setChanged(true);
    }
    this.phoneType = trim(phoneType);
  }


  public String getPhoneType()
  {
    return phoneType;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    if(valueChanged(this.phoneNumber, phoneNumber))
    {
      setChanged(true);
    }
    this.phoneNumber = trim(phoneNumber);
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setExtension(String extension)
  {
    if(valueChanged(this.extension, extension))
    {
      setChanged(true);
    }
    this.extension = trim(extension);
  }


  public String getExtension()
  {
    return extension;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}
