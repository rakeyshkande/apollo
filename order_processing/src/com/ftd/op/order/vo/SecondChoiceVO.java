package com.ftd.op.order.vo;

public class SecondChoiceVO extends BaseVO
{
  public SecondChoiceVO()
  {
  }
  
  private String productSecondChoiceId;
  private String oeDescription1;
  private String oeDescription2;
	private String mercuryDescription1;
  private String mercuryDescription2;


  public void setProductSecondChoiceId(String productSecondChoiceId)
  {
    this.productSecondChoiceId = productSecondChoiceId;
  }


  public String getProductSecondChoiceId()
  {
    return productSecondChoiceId;
  }


  public void setOeDescription1(String oeDescription1)
  {
    this.oeDescription1 = oeDescription1;
  }


  public String getOeDescription1()
  {
    return oeDescription1;
  }


  public void setOeDescription2(String oeDescription2)
  {
    this.oeDescription2 = oeDescription2;
  }


  public String getOeDescription2()
  {
    return oeDescription2;
  }


  public void setMercuryDescription1(String mercuryDescription1)
  {
    this.mercuryDescription1 = mercuryDescription1;
  }


  public String getMercuryDescription1()
  {
    return mercuryDescription1;
  }


  public void setMercuryDescription2(String mercuryDescription2)
  {
    this.mercuryDescription2 = mercuryDescription2;
  }


  public String getMercuryDescription2()
  {
    return mercuryDescription2;
  }
  
}
