package com.ftd.op.order.facade;

import com.ftd.op.common.service.CreditCardService;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDTO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import java.sql.Connection;

public class OrderFacade
{
  Connection conn;
  
  public OrderFacade(Connection conn) 
  {
    this.conn = conn;
  }

  /**
   *  
   * 
   * @param OrderDetailVO
   * @return RWDFloristVO
   * @throws java.lang.Exception
   */
  public RWDFloristVO selectFlorist(OrderDetailVO order) throws Exception
  {
    return selectFlorist(order, null);
  }

  /**
   *  
   * 
   * @param OrderDetailVO
   * @param Boolean
   * @return RWDFloristV
   * @throws java.lang.Exception
   */
  public RWDFloristVO selectFlorist(OrderDetailVO order, Boolean rwdFlagOverride) throws Exception
  {
    OrderService orderService = new OrderService(conn);
    return orderService.selectFlorist(order, rwdFlagOverride);
  }
  
    /**
     *  
     * 
     * @param RWDTO
     * @return RWDFloristVO
     * @throws java.lang.Exception
     */
    public RWDFloristVO selectFlorist(RWDTO rwd) throws Exception
    {
      OrderService orderService = new OrderService(conn);
      return orderService.selectFlorist(rwd);
    }
    
  /**
   * 
   * 
   * @param OrderDetailVO
   * @param boolean - returnAllFloristFlag
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristVOList getFillingFloristList(OrderDetailVO order, boolean returnAllFloristFlag) throws Exception
  {
    return getFillingFloristList(order, returnAllFloristFlag, null);
  }
    
  /**
   * 
   * 
   * @param OrderDetailVO
   * @param boolean - returnAllFloristFlag
   * @param Boolean - rwdFlagOverride
   * @return RWDFloristVOList
   * @throws java.lang.Exception
   */
  public RWDFloristVOList getFillingFloristList(OrderDetailVO order, boolean returnAllFloristFlag, Boolean rwdFlagOverride) throws Exception
  {
    OrderService orderService = new OrderService(conn);
    return orderService.getFloristList(order,returnAllFloristFlag,rwdFlagOverride);
  }

  public CommonCreditCardVO validateCreditCard(CommonCreditCardVO inCreditCard) throws Exception
  {
      CreditCardService creditCardService = new CreditCardService(conn);
      CommonCreditCardVO outCreditCard = creditCardService.validateCreditCard(inCreditCard);
      return outCreditCard;
  }
  public RWDFloristVOList getFillingFloristList(RWDTO rwd) throws Exception
  {
    OrderDAO dao = new OrderDAO(conn);
    return dao.getRoutableFlorists(rwd);
  }
}