package com.ftd.op.order.handler;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.sql.DriverManager;

public class ForwardFlagHandler extends EventHandler 
{
  Logger logger;
  public ForwardFlagHandler()
  {
      this.logger = new Logger("com.ftd.op.order.handler.ForwardFlagHandler");  
  } 
  /** This method is invoked by the event handler framework. 
   */
   public void invoke(Object payload) throws Throwable
  {
      logger.debug("FORWARD_FLAG_HANDLER:invoke(Object payload)");
      Connection conn = CommonUtils.getConnection();   
      OrderDAO dao = new OrderDAO(conn);
      try {
        logger.info("Begin updating florist forward flags...");
        dao.updateFloristForwardFlags();
        logger.info("End updating florist forward flags.");
      } 
      catch(Exception e) 
      {
        logger.error(e);
          // Handle the error.
          try {
              CommonUtils.sendSystemMessage("Order Processing ForwardFlagHandler: Failed." + e.getMessage());
          } catch (Throwable th) {
              logger.error("onMessage: Could not send system message: ", th);
          }
      }
      finally
      {
        //close the connection
        try
        {
          conn.close();
        }
        catch(Exception e)
        {
            logger.error("Failed to close connection:" + e.getMessage());
        }
      }

  }
}