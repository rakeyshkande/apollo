package com.ftd.op.order.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.service.OrderHoldService;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


public class ProductOrderHoldHandler extends com.ftd.eventhandling.events.EventHandler 
{

  Logger logger;

  public ProductOrderHoldHandler()
  {
    this.logger = new Logger("com.ftd.op.order.handler.ProductOrderHoldHandler");  
  }

    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
       logger.debug("invoke(Object payload)");
       Connection conn = CommonUtils.getConnection();
        try
        {
            OrderHoldService orderHoldService = new OrderHoldService(conn);
            orderHoldService.releaseProductHold();
        }
        catch (Throwable t)
        {
          logger.error(t);
          CommonUtils.sendSystemMessage(t.getMessage());
        }
        finally
        {
          conn.close();
        }
    }
  
    
}