package com.ftd.op.order.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;
import javax.naming.InitialContext;

public class ZipQueueHandler extends com.ftd.eventhandling.events.EventHandler
{
  Logger logger;


  public ZipQueueHandler()
  {
    this.logger = new Logger("com.ftd.op.order.handler.GNADDOrderHoldHandler");  
  }
    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
        logger.debug("ZIP_QUEUE_HANDLER:invoke(Object payload)");
        //obtain a database connection
        Connection conn = CommonUtils.getConnection();
        OrderDAO dao = new OrderDAO(conn);
        try
        {
          List list = dao.getRoutableZipQueue();
          if(list != null) 
          {
            Iterator iter = list.iterator();
            while(iter.hasNext()) 
            {
              // for each zip code found, delete from queue and send to OP for processing
              String orderDetailId = (String)iter.next();
              logger.debug("zip_queue_handler: " + orderDetailId);
              OrderDetailVO detail = dao.getOrderDetail(orderDetailId);
              String messageId = dao.getZipQueueMessageId(detail);
              dao.deleteZipQueueMessageId(messageId);
              MessageToken token = new MessageToken();
              token.setMessage(orderDetailId);
              CommonUtils.sendJMSMessage(new InitialContext(), token, "PROCESS_ORDER_LOCATION", "PROCESSORDER");
            }
          }
        }
        catch (Throwable t)
        {
          logger.error(t);
        }
        finally
        {
          conn.close();
        }
    }
}