package com.ftd.op.order.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.service.OrderHoldService;
import com.ftd.op.order.vo.GnaddVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;


public class GNADDOrderHoldHandler extends com.ftd.eventhandling.events.EventHandler 
{

  Logger logger;

  public GNADDOrderHoldHandler()
  {
    this.logger = new Logger("com.ftd.op.order.handler.GNADDOrderHoldHandler");  
  }

    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
        logger.debug("invoke(Object payload)");
        //obtain a database connection
        Connection conn = CommonUtils.getConnection();
        try
        {
          OrderDAO orderDAO = new OrderDAO(conn);
          OrderHoldService orderHoldService = new OrderHoldService(conn);
          GnaddVO gnaddVO = orderDAO.getGnadd();
          if (gnaddVO == null || gnaddVO.getGnaddStatus() == null || gnaddVO.getGnaddDate() == null)
          {
            throw new Exception("FRP.GLOBAL_PARMS GNADD_STATUS or GNADD_DATE is null.");
          }
          SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
          Date date = dateFormat.parse(gnaddVO.getGnaddDate());
          Date today = CommonUtils.clearTime(new Date());
          if (gnaddVO.getGnaddStatus().equalsIgnoreCase("ON"))
          {
            if (date.equals(today))
            {
              // turn off GNADD because it's GNADD day
              logger.info("GNADD handler is turning off GNADD -- it's GNADD day");
              orderDAO.setGlobalParameter("FTDAPPS_PARMS", "GNADD_LEVEL", "0", "ORDER_PROCESSING");
              orderHoldService.releaseGNADDHold();
            }
            else
            {
              orderHoldService.releaseEarlyGNADDHold();
              orderHoldService.releaseGNADDHoldforCSZAvailable();
            }
          }
        }
        catch (Throwable t)
        {
          logger.error(t);
          CommonUtils.sendSystemMessage(t.getMessage());
        }
        finally
        {
          conn.close();
        }

    }

}