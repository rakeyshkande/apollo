package com.ftd.op.venus.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.enterprisedt.net.ftp.FTPClient;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.vo.ServerVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * 
 * This is a class for common utilities used by all aspects of Venus code. 
 * 
 */

public class VenusCommonUtil
{
  private Connection conn;
  private Logger logger;  
    
  public VenusCommonUtil(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.venus.util.VenusCommonUtil");
  }
  
  public String upgradeSAShipping(String productType, Date deliveryDate, CustomerVO customerVO, String productId) throws VenusException, Exception
  {
    String shipMethod = "";
    OrderDAO orderDAO = new OrderDAO(conn);
    GlobalParameterVO cutoffTime = orderDAO.getGlobalParameter("ORDER_PROCESSING", "VENUS_CUTOFF_TIME");
    String cutoffTimeStr = cutoffTime.getValue();
    if (cutoffTimeStr == null)
    {
      throw new VenusException("FRP.GLOBAL_PARMS variable VENUS_CUTOFF_TIME is null."); 
    }
    SimpleDateFormat formatter= new SimpleDateFormat("HHmm");
    Date today = new Date();
    String todayStr = formatter.format(today);
    long daysToDeliver = -1;
    if (new Integer(todayStr).intValue() > new Integer(cutoffTimeStr).intValue())
    {
      daysToDeliver = this.getNumberOfDaysDifference(today, deliveryDate) - 1;
    }
    else
    {
      daysToDeliver = this.getNumberOfDaysDifference(today, deliveryDate);
    }
    if (productType.equalsIgnoreCase("FRECUT")|| productType.equalsIgnoreCase("SDFC"))
    {
     // we still have time to deliver the freshcuts/samedayfreshcut (added SDFC to match deliveryDateUTIL)
      if (daysToDeliver >=1) 
        shipMethod = "PO";
      else
        shipMethod = null;   
    }
    else
    {
      if (customerVO.getState().equalsIgnoreCase("AK") || customerVO.getState().equalsIgnoreCase("HI"))
      {
        shipMethod = "2F";
      }
      else if (daysToDeliver > 1)
      {
        if( orderDAO.isShipMethodAvailable(productId,"2F") ) {
            shipMethod = "2F";
        } else {
            shipMethod = "PO";    
        }
      }
      else if (daysToDeliver == 1)
      {
        shipMethod = "PO";
      }
      else
      {
        shipMethod = null;
      }  
    }
    
    if( logger.isDebugEnabled() ) {
        logger.debug("New Ship Method: "+shipMethod);
        logger.debug("Days To Deliver: "+daysToDeliver);
    }
    
    
    return shipMethod;
  }
  
    /** Gets number of days between the earlier date and the later date.
     * @param Date earlierDate
     * @param Date laterDate
     * @return long days
     */
  private long getNumberOfDaysDifference(Date earlierDate, Date laterDate)
  {
    long daysToDeliver;
    // Do the calc - 86400000 milliseconds in a day //
    daysToDeliver = (CommonUtils.clearTime(laterDate).getTime() - CommonUtils.clearTime(earlierDate).getTime())/86400000;
    return daysToDeliver;
  }
  

    /** This method copies a file from the source to the destination.  Returns
     * destination file if copy was successful and null otherwise.  
     * @param File destination
     * @param File source
     * @return File destination
     */
    public File copyFile(File destination, File source)
    {
        try 
        {
          BufferedReader br = new BufferedReader(new FileReader(source));
          BufferedWriter bw = new BufferedWriter(new FileWriter(destination));
          String line = br.readLine();
          while (line != null)
          {
            bw.write(line);
            line = br.readLine();
            if (line != null)
              bw.newLine();
          }
          bw.close();
          br.close();
          return destination;
        }
        catch (Exception ex)
        {
          logger.debug(ex.toString());
          return null;
        }
    }

  public boolean ftpFile (File file, ConfigurationUtil configUtil, String remoteDir, String loadBalance, String serverLocationField, String serverLogonField, String serverPasswordField)throws Exception
  {
    return ftpFile( file,configUtil,remoteDir,loadBalance,serverLocationField,  serverLogonField,  serverPasswordField, false);
  }


 /** This method ftp's a file to a list of possible servers.
   * @param File destination
   * @param File source
   * @return boolean fileCopied
 */

  public boolean ftpFile (File file, ConfigurationUtil configUtil, String remoteDir, String loadBalance, String serverLocationField, String serverLogonField, String serverPasswordField, boolean useTempName)throws Exception
  { 
    logger.debug("ftpFile (File file, ConfigurationUtil configUtil, String remoteDir ("+remoteDir+"), String loadBalance("+loadBalance+"), String serverLocationField("+serverLocationField+"), String serverLogonField("+serverLogonField+"), String serverPasswordField("+serverPasswordField+"))");
    boolean fileSent = false;
    ArrayList serverList = (ArrayList) this.getFTPServerList(configUtil, loadBalance, serverLocationField, serverLogonField, serverPasswordField);
    int counter = 0;
    ServerVO serverVO = (ServerVO) serverList.get(counter);
    while (fileSent == false)
      {
        if (counter < serverList.size())
        {       
          String remoteLocation = serverVO.getLocation();
          String ftpLogon = serverVO.getLogon();
          String ftpPassword = serverVO.getPassword();
          try
          {
              //get list of files in directory    
        	  FTPClient ftpClient = new FTPClient(remoteLocation);
              ftpClient.login(ftpLogon,ftpPassword);
              
              if(useTempName)
              {
                  String tempName  = remoteDir + file.getName() + ".temp";
                  String endName  = remoteDir + file.getName();
                  logger.debug("start: " + tempName + " from:" + endName);
                  ftpClient.put(file.getPath(), tempName);                   
                  ftpClient.rename(tempName,endName);    
              }
              else
              {
                  ftpClient.put(file.getPath(), remoteDir + file.getName());                                     
              }             
            
              
              fileSent = true;
          }
          catch(Throwable e)
          {
                String msg = "Error occured while ftping "+file.getName()+" to "+remoteLocation+".  Will try to send to alternate FTP server.";
                logger.error(msg);
                logger.error(e);
                CommonUtils.sendSystemMessage(msg + ":" + e.toString()); 
                counter++;
                serverVO = (ServerVO) serverList.get(counter);                
          }
        }
        else
        {
              String msg = "No servers left to try to ftp "+file.getName()+".  File was not ftped.";
              logger.error(msg);
              CommonUtils.sendSystemMessage(msg);
              return false;
       }
      }
    return fileSent;
  }
  
     /**
   * Get FTP Server List for all sequential servers in the config file
   *
   *@return List
   **/
  private List getFTPServerList(ConfigurationUtil configUtil, String loadBalance, String serverLocationField, String serverLogonField, String serverPasswordField)throws Exception
  {
    List serverList = new ArrayList();
    ServerVO serverVO;
    int counter = 1;
    // get first server information from config file. //
    String remoteLocation = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT, serverLocationField+"_"+counter);   
    String ftpLogon = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT, serverLogonField+"_"+counter);   
    String ftpPassword = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT, serverPasswordField+"_"+counter);    
    
    while ((!remoteLocation.equals(""))&&(!ftpLogon.equals(""))&&(!ftpPassword.equals("")))
    {
      // set server VO properties //
      serverVO = new ServerVO();
      serverVO.setLocation(remoteLocation);
      serverVO.setLogon(ftpLogon);
      serverVO.setPassword(ftpPassword);
      serverList.add(serverVO);
      // get next server information //
      counter++;
      remoteLocation = configUtil.getFrpGlobalParmNoNull(VenusConstants.VENUS_CONFIG_CONTEXT, serverLocationField+"_"+counter);   
      ftpLogon = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, serverLogonField+"_"+counter);   
      ftpPassword = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, serverPasswordField+"_"+counter);  
    }
    // shuffle the list, if in load balancing mode //
    if (loadBalance.equalsIgnoreCase("Y"))
    {
      Collections.shuffle(serverList);
    }
    return serverList;
  }

 /** This method takes a newString, checks for nulls and length,
  * and concats it to the parent string.
   * @param String parentString
   * @param String newString
   * @param int maxLength
   * @param boolean addComma
   * @return String 
 */
  public String addCommaDelimitedField(String parentString, String newString, int maxLength, boolean addComma)
    {
      if (newString != null)
      {
        if (newString.length() > maxLength)
        {
          newString = newString.substring(0, maxLength);
        }
        // add double quotes around string
        newString = "\"" + newString + "\"";
        parentString = parentString.concat(newString);
      }
      if (addComma)
      {
        parentString = parentString.concat(",");
      }
      return parentString;
    }
    
   /** Wrapper for addCommaDelimitedField() with boolean addComma defaulted
    * to 'true'.
   */
    public String addCommaDelimitedField(String parentString, String newString, int maxLength)
    {
      return this.addCommaDelimitedField(parentString, newString, maxLength, true);
    }
    
    /**
     * This method takes a String and removes all double 
     * quotes from it (provided the String is not null).
     * @param String parentString
     * @return String
     */
    public String removeDoubleQuotes(String parentString) {   
    	if(parentString != null){
    		parentString = parentString.replaceAll("\"", "");
    	}
    	return parentString;
    }
    
    
	//Use JSCH to sftp the file
	public void uploadFile(File file, String remoteFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;
	

		try{
			JSch jsch = new JSch();
			session = jsch.getSession(username,ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;		
			channelSftp.cd(remoteFilePath);
			channelSftp.put(new FileInputStream(file), file.getName());
			
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
		catch(Exception ex){			
			throw new Exception("Upload of file failed");
		}
	}

}
