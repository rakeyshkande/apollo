package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;
import com.ftd.op.order.vo.CustomerVO;
import java.util.Date;

/**
 * VO for Wine.com Customer File produced in WineCustomerFileHandler.java
 * Did not use the com.ftd.op.order.vo.CustomerVO because it would lead 
 * to confusion because it would be only partially populated.
 */

public class WineCustomerVO extends BaseVO
{
  String venusOrderNumber;
  String recipientName;
  String businessName;
  String address1;
  String address2;
  String city;
  String state;
  String zip;
  String phoneNumber;
  Date birthday;

  public WineCustomerVO()
  {
  }


  public void setVenusOrderNumber(String venusOrderNumber)
  {
    this.venusOrderNumber = venusOrderNumber;
  }


  public String getVenusOrderNumber()
  {
    return venusOrderNumber;
  }


  public void setRecipientName(String recipientName)
  {
    this.recipientName = recipientName;
  }


  public String getRecipientName()
  {
    return recipientName;
  }


  public void setBusinessName(String businessName)
  {
    this.businessName = businessName;
  }


  public String getBusinessName()
  {
    return businessName;
  }


  public void setAddress1(String address1)
  {
    this.address1 = address1;
  }


  public String getAddress1()
  {
    return address1;
  }


  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZip(String zip)
  {
    this.zip = zip;
  }


  public String getZip()
  {
    return zip;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setBirthday(Date birthday)
  {
    this.birthday = birthday;
  }


  public Date getBirthday()
  {
    return birthday;
  }




}