package com.ftd.op.venus.vo;
import java.util.Date;

public class FileItemVO 
{
    private String orderNumber;
    private Date dateReceived;
    private String partnerOrderNumber;
    private Date shipDate;
    private String trackingNumber;

    public FileItemVO()
    {
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public Date getDateReceived()
    {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived)
    {
        this.dateReceived = dateReceived;
    }

    public String getPartnerOrderNumber()
    {
        return partnerOrderNumber;
    }

    public void setPartnerOrderNumber(String partnerOrderNumber)
    {
        this.partnerOrderNumber = partnerOrderNumber;
    }

    public Date getShipDate()
    {
        return shipDate;
    }

    public void setShipDate(Date shipDate)
    {
        this.shipDate = shipDate;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }
}