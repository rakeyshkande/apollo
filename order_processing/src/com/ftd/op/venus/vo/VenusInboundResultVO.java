package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.QueueVO;

public class VenusInboundResultVO 
{
    private VenusMessageVO venusMessage;
    private QueueVO queue;

    public VenusInboundResultVO()
    {
    }

    public VenusMessageVO getVenusMessage()
    {
        return venusMessage;
    }

    public void setVenusMessage(VenusMessageVO venusMessage)
    {
        this.venusMessage = venusMessage;
    }

    public QueueVO getQueue()
    {
        return queue;
    }

    public void setQueue(QueueVO queue)
    {
        this.queue = queue;
    }
}