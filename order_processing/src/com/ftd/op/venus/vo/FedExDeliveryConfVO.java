package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;


public class FedExDeliveryConfVO extends BaseVO
{
  String commitmentIndicator;
  String shipMethod;
  String dayOfWeek;
  String deliveryAvailable;
  int deliveryBusinessDays;
  String deliveryTime;
  String addOneBusinessDayForHi;
  String satHiDeliveryAvailable;

  public FedExDeliveryConfVO()
  {
  }


  public void setCommitmentIndicator(String commitmentIndicator)
  {
    this.commitmentIndicator = commitmentIndicator;
  }


  public String getCommitmentIndicator()
  {
    return commitmentIndicator;
  }


  public void setShipMethod(String shipMethod)
  {
    this.shipMethod = shipMethod;
  }


  public String getShipMethod()
  {
    return shipMethod;
  }


  public void setDayOfWeek(String dayOfWeek)
  {
    this.dayOfWeek = dayOfWeek;
  }


  public String getDayOfWeek()
  {
    return dayOfWeek;
  }


  public void setDeliveryAvailable(String deliveryAvailable)
  {
    this.deliveryAvailable = deliveryAvailable;
  }


  public String getDeliveryAvailable()
  {
    return deliveryAvailable;
  }


  public void setDeliveryBusinessDays(int deliveryBusinessDays)
  {
    this.deliveryBusinessDays = deliveryBusinessDays;
  }


  public int getDeliveryBusinessDays()
  {
    return deliveryBusinessDays;
  }


  public void setDeliveryTime(String deliveryTime)
  {
    this.deliveryTime = deliveryTime;
  }


  public String getDeliveryTime()
  {
    return deliveryTime;
  }


  public void setAddOneBusinessDayForHi(String addOneBusinessDayForHi)
  {
    this.addOneBusinessDayForHi = addOneBusinessDayForHi;
  }


  public String getAddOneBusinessDayForHi()
  {
    return addOneBusinessDayForHi;
  }


  public void setSatHiDeliveryAvailable(String satHiDeliveryAvailable)
  {
    this.satHiDeliveryAvailable = satHiDeliveryAvailable;
  }


  public String getSatHiDeliveryAvailable()
  {
    return satHiDeliveryAvailable;
  }
}