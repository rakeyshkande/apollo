package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;
import java.lang.Double;
import java.util.Date;

/**
 * Record on the TN_MESSAGES table (in Venus system).  These are order-related messages.
 * 
 * @author Dave Ross
 */
public class TNMessageVO extends BaseVO
{

  private long  msgID;
  private String msgType;
  private long msgPOID;
  private String msgPOConfNbr;
  private Double msgAdjAmount;
  private Double msgOrigOrderAmt;
  private String msgText;
  private String msgWhoCreated;
  private Date msgCreatedDate;
  private String msgDateLastModified;
  private String msgOrderVendorID;
  private String orderProcessingFlag;
  private String errorDescription;
  private String msgExportedFlag;
  private String msgReadFlag;
  private String msgProcessedFlag;
  private String cancelReasonCode;
  
  public TNMessageVO()
  {
  }


  public void setMsgID(long msgID)
  {

    this.msgID = msgID;
  }


  public long getMsgID()
  {
    return msgID;
  }


  public void setMsgType(String msgType)
  {
    this.msgType = msgType;
  }


  public String getMsgType()
  {
    return msgType;
  }


  public void setMsgPOID(long msgPOID)
  { 
    this.msgPOID = msgPOID;
  }


  public long getMsgPOID()
  {
    return msgPOID;
  }


  public void setMsgPOConfNbr(String msgPOConfNbr)
  {
    this.msgPOConfNbr = msgPOConfNbr;
  }


  public String getMsgPOConfNbr()
  {
    return msgPOConfNbr;
  }


  public void setMsgAdjAmount(Double msgAdjAmount)
  {
    this.msgAdjAmount = msgAdjAmount;
  }


  public Double getMsgAdjAmount()
  {
    return msgAdjAmount;
  }


  public void setMsgOrigOrderAmt(Double msgOrigOrderAmt)
  {

    this.msgOrigOrderAmt = msgOrigOrderAmt;
  }


  public Double getMsgOrigOrderAmt()
  {
    return msgOrigOrderAmt;
  }


  public void setMsgText(String msgText)
  {

    this.msgText = msgText;
  }


  public String getMsgText()
  {
    return msgText;
  }





  public void setMsgCreatedDate(Date msgCreatedDate)
  {

    this.msgCreatedDate = msgCreatedDate;
  }


  public Date getMsgCreatedDate()
  {
    return msgCreatedDate;
  }


  public void setMsgDateLastModified(String msgDateLastModified)
  {

    this.msgDateLastModified = msgDateLastModified;
  }


  public String getMsgDateLastModified()
  {
    return msgDateLastModified;
  }


  public void setMsgOrderVendorID(String msgOrderVendorID)
  {

    this.msgOrderVendorID = msgOrderVendorID;
  }


  public String getMsgOrderVendorID()
  {
    return msgOrderVendorID;
  }

    public String getOrderProcessingFlag()
    {
        return orderProcessingFlag;
    }

    public void setOrderProcessingFlag(String orderProcessingFlag)
    {
        this.orderProcessingFlag = orderProcessingFlag;
    }

    public String getErrorDescription()
    {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription)
    {
        this.errorDescription = errorDescription;
    }


  public void setMsgWhoCreated(String msgWhoCreated)
  {
    this.msgWhoCreated = msgWhoCreated;
  }


  public String getMsgWhoCreated()
  {
    return msgWhoCreated;
  }


  public void setMsgExportedFlag(String msgExportedFlag)
  {
    this.msgExportedFlag = msgExportedFlag;
  }


  public String getMsgExportedFlag()
  {
    return msgExportedFlag;
  }


  public void setMsgReadFlag(String msgReadFlag)
  {
    this.msgReadFlag = msgReadFlag;
  }


  public String getMsgReadFlag()
  {
    return msgReadFlag;
  }

    public String getCancelReasonCode()
    {
        return cancelReasonCode;
    }

    public void setCancelReasonCode(String cancelReasonCode)
    {
        this.cancelReasonCode = cancelReasonCode;
    }


  public void setMsgProcessedFlag(String msgProcessedFlag)
  {
    this.msgProcessedFlag = msgProcessedFlag;
  }


  public String getMsgProcessedFlag()
  {
    return msgProcessedFlag;
  }
}