package com.ftd.op.venus.vo;

import com.ftd.op.common.vo.BaseVO;

import java.util.Date;


public class VenusMessageVO extends BaseVO
{
  public final String complaintCommTypeIdNotAComplaint = "NC";
  private String venusId;
  private String venusOrderNumber;
  private String venusStatus;
  private String msgType;
  private String sendingVendor;
  private String fillingVendor;
  private Date orderDate;
  private String recipient;
  private String businessName;
  private String address1;
  private String address2;
  private String city;
  private String state;
  private String zip;
  private String phoneNumber;
  private Date deliveryDate;
  private String firstChoice;
  private Double price;
  private String cardMessage;
  private Date shipDate;
  private String operator;
  private String comments;
  private Date transmissionTime;
  private String referenceNumber;
  private String productId;
  private String combinedReportNumber;
  private String rofNumber;
  private String adjReasonCode;
  private Double overUnderCharge;
  private String processedIndicator;
  private String messageText;
  private String sendToVenus;
  private String orderPrinted;
  private String subType;
  private Date createdOn;
  private Date updatedOn;
  private String errorDescription;
  private boolean printed;
  private String country;
  private String cancelReasonCode;
  private String compOrder;
  private Double oldPrice;
  private String shipMethod;
  private String vendorSKU;
  private String productDescription;
  private String productWeight;
  private boolean fromCommunicationScreen = false;
  private String messageDirection;
  private String externalSystemStatus;
  private String vendorId;
  private String shippingSystem;
  private String trackingNumber;
  private Date printedStatusDate;
  private Date shippedStatusDate;
  private Date cancelledStatusDate;
  private String finalCarrier;
  private String finalShipMethod;
  private String sdsStatus;
  private boolean ratedShipment;
  private boolean forcedShipment;
  private Date rejectedStatusDate;
  private boolean zoneJumpFlag;
  private Date zoneJumpLabelDate;
  private String zoneJumpTrailerNumber;
  private String personalGreetingId;
  private String complaintCommOriginTypeId;
  private String complaintCommNotificationTypeId;
  private String newShippingSystem;
    
  public VenusMessageVO()
  {
  }


  public void setVenusId(String venusId)
  {
    this.venusId = venusId;
  }


  public String getVenusId()
  {
    return venusId;
  }


  public void setVenusOrderNumber(String venusOrderNumber)
  {
    this.venusOrderNumber = venusOrderNumber;
  }


  public String getVenusOrderNumber()
  {
    return venusOrderNumber;
  }

     public String getShipMethod()
     {
         return shipMethod;
     }
 
     public void setShipMethod(String shipMethod)
     {
         this.shipMethod = shipMethod;
     }




  public void setVenusStatus(String venusStatus)
  {
    this.venusStatus = venusStatus;
  }


  public String getVenusStatus()
  {
    return venusStatus;
  }


  public void setMsgType(String msgType)
  {
    this.msgType = msgType;
  }


  public String getMsgType()
  {
    return msgType;
  }


  public void setSendingVendor(String sendingVendor)
  {
    this.sendingVendor = sendingVendor;
  }


  public String getSendingVendor()
  {
    return sendingVendor;
  }


  public void setFillingVendor(String fillingVendor)
  {
    this.fillingVendor = fillingVendor;
  }


  public String getFillingVendor()
  {
    return fillingVendor;
  }


  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }

  public Date getOrderDate()
  {
    return orderDate;
  }


  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }


  public String getRecipient()
  {
    return recipient;
  }


  public void setBusinessName(String businessName)
  {
    this.businessName = businessName;
  }


  public String getBusinessName()
  {
    return businessName;
  }


  public void setAddress1(String address1)
  {

    this.address1 = address1;
  }


  public String getAddress1()
  {
    return address1;
  }


  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZip(String zip)
  {
    this.zip = zip;
  }


  public String getZip()
  {
    return zip;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }


  public void setFirstChoice(String firstChoice)
  {
    this.firstChoice = firstChoice;
  }


  public String getFirstChoice()
  {
    return firstChoice;
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }
  
  public Double getPrice()
  {
    return price;
  }


  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = cardMessage;
  }


  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }
  
  public Date getShipDate()
  {
    return shipDate;
  }


  public void setOperator(String operator)
  {
    this.operator = operator;
  }


  public String getOperator()
  {
    return operator;
  }


  public void setComments(String comments)
  {

    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setTransmissionTime(Date transmissionTime)
  {

    this.transmissionTime = transmissionTime;
  }


  public Date getTransmissionTime()
  {
    return transmissionTime;
  }


  public void setReferenceNumber(String referenceNumber)
  {

    this.referenceNumber = referenceNumber;
  }


  public String getReferenceNumber()
  {
    return referenceNumber;
  }


  public void setProductID(String productId)
  {

    this.productId = productId;
  }


  public String getProductId()
  {
    return productId;
  }

  public void setCombinedReportNumber(String combinedReportNumber)
  {
    this.combinedReportNumber = combinedReportNumber;
  }


  public String getCombinedReportNumber()
  {
    return combinedReportNumber;
  }


  public void setRofNumber(String rofNumber)
  {
    this.rofNumber = rofNumber;
  }


  public String getRofNumber()
  {
    return rofNumber;
  }


  public void setAdjReasonCode(String adjReasonCode)
  {

    this.adjReasonCode = adjReasonCode;
  }


  public String getAdjReasonCode()
  {
    return adjReasonCode;
  }


  public void setOverUnderCharge(Double overUnderCharge)
  {
    this.overUnderCharge = overUnderCharge;
  }
  
  public Double getOverUnderCharge()
  {
    return overUnderCharge;
  }


  public void setProcessedIndicator(String processedIndicator)
  {

    this.processedIndicator = processedIndicator;
  }


  public String getProcessedIndicator()
  {
    return processedIndicator;
  }


  public void setMessageText(String messageText)
  {

    this.messageText = messageText;
  }


  public String getMessageText()
  {
    return messageText;
  }


  public void setSendToVenus(String sendToVenus)
  {

    this.sendToVenus = sendToVenus;
  }


  public String getSendToVenus()
  {
    return sendToVenus;
  }


  public void setOrderPrinted(String orderPrinted)
  {

    this.orderPrinted = orderPrinted;
  }


  public String getOrderPrinted()
  {
    return orderPrinted;
  }


  public void setSubType(String subType)
  {

    this.subType = subType;
  }


  public String getSubType()
  {
    return subType;
  }


  public void setCreatedOn(Date createdOn)
  {

    this.createdOn = createdOn;
  }

  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setUpdatedOn(Date updatedOn)
  {

    this.updatedOn = updatedOn;
  }
  
  public Date getUpdatedOn()
  {
    return updatedOn;
  }
  
  public void setErrorDescription(String errorDescription)
  {

    this.errorDescription = errorDescription;
  }


  public String getErrorDescription()
  {
    return errorDescription;
  }  

    public boolean isPrinted()
    {
        return printed;
    }

    public void setPrinted(boolean printed)
    {
        this.printed = printed;
    }


  public void setCountry(String country)
  {
    this.country = country;
  }


  public String getCountry()
  {
    return country;
  }

    public String getCancelReasonCode()
    {
        return cancelReasonCode;
    }

    public void setCancelReasonCode(String cancelReasonCode)
    {
        this.cancelReasonCode = cancelReasonCode;
    }

    public String getCompOrder()
    {
        return compOrder;
    }

    public void setCompOrder(String compOrder)
    {
        this.compOrder = compOrder;
    }


  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }


  public Double getOldPrice()
  {
    return oldPrice;
  }

    public String getVendorSKU()
    {
        return vendorSKU;
    }

    public void setVendorSKU(String vendorSKU)
    {
        this.vendorSKU = vendorSKU;
    }

    public String getProductDescription()
    {
        return productDescription;
    }

    public void setProductDescription(String productDescription)
    {
        this.productDescription = productDescription;
    }

    public String getProductWeight()
    {
        return productWeight;
    }

    public void setProductWeight(String productWeight)
    {
        this.productWeight = productWeight;
    }
    
    
    public void setFromCommunicationScreen(boolean fromCommunicationScreen)
    {
       this.fromCommunicationScreen = fromCommunicationScreen;
    }
    
    public boolean isFromCommunicationScreen()
    {
      return fromCommunicationScreen;
    }
    

    public void setMessageDirection(String messageDirection) 
    {
      this.messageDirection = messageDirection;
    }
    public String getMessageDirection() 
    {
      return this.messageDirection;
    }


    public void setExternalSystemStatus(String externalSystemStatus) 
    {
      this.externalSystemStatus = externalSystemStatus;
    }
    public String getExternalSystemStatus() 
    {
      return this.externalSystemStatus;
    }


    public void setVendorId(String vendorId) 
    {
      this.vendorId = vendorId;
    }
    public String getVendorId() 
    {
      return this.vendorId;
    }


    public void setShippingSystem(String shippingSystem) 
    {
      this.shippingSystem = shippingSystem;
    }
    public String getShippingSystem() 
    {
      return shippingSystem;
    }


    public void setTrackingNumber(String trackingNumber) 
    {
      this.trackingNumber = trackingNumber;
    }
    public String getTrackingNumber() 
    {
      return this.trackingNumber;
    }


    public void setPrintedStatusDate(Date printedStatusDate)
    {
      this.printedStatusDate = printedStatusDate;
    }
    public Date getPrintedStatusDate()
    {
      return this.printedStatusDate;
    }

  
    public void setShippedStatusDate(Date shippedStatusDate)
    {
      this.shippedStatusDate = shippedStatusDate;
    }
    public Date getShippedStatusDate()
    {
      return this.shippedStatusDate;
    }

  
    public void setCancelledStatusDate(Date cancelledStatusDate)
    {
      this.cancelledStatusDate = cancelledStatusDate;
    }
    public Date getCancelledStatusDate()
    {
      return this.cancelledStatusDate;
    }

  
    public void setFinalCarrier(String finalCarrier) 
    {
      this.finalCarrier = finalCarrier;
    }
    public String getFinalCarrier() 
    {
      return this.finalCarrier;
    }


    public void setFinalShipMethod(String finalShipMethod) 
    {
      this.finalShipMethod = finalShipMethod;
    }
    public String getFinalShipMethod() 
    {
      return this.finalShipMethod;
    }


    public void setSdsStatus(String sdsStatus) 
    {
      this.sdsStatus = sdsStatus;
    }
    public String getSdsStatus() 
    {
      return this.sdsStatus;
    }


    public void setProductId(String productId) {
        this.productId = productId;
    }


    public void setRatedShipment(boolean ratedShipment) {
        this.ratedShipment = ratedShipment;
    }

    public boolean isRatedShipment() {
        return ratedShipment;
    }

    public void setForcedShipment(boolean forcedShipment) {
        this.forcedShipment = forcedShipment;
    }

    public boolean isForcedShipment() {
        return forcedShipment;
    }
    
    public Object clone() {
        VenusMessageVO target = new VenusMessageVO();
        
        target.setVenusId(this.venusId);
        target.setVenusOrderNumber(this.venusOrderNumber);
        target.setVenusStatus(this.venusStatus);
        target.setMsgType(this.msgType);
        target.setSendingVendor(this.sendingVendor);
        target.setFillingVendor(this.fillingVendor);
        target.setOrderDate(this.orderDate);
        target.setRecipient(this.recipient);
        target.setBusinessName(this.businessName);
        target.setAddress1(this.address1);
        target.setAddress2(this.address2);
        target.setCity(this.city);
        target.setState(this.state);
        target.setZip(this.zip);
        target.setPhoneNumber(this.phoneNumber);
        target.setDeliveryDate(this.deliveryDate);
        target.setFirstChoice(this.firstChoice);
        target.setPrice(this.price);
        target.setCardMessage(this.cardMessage);
        target.setShipDate(this.shipDate);
        target.setOperator(this.operator);
        target.setComments(this.comments);
        target.setTransmissionTime(this.transmissionTime);
        target.setReferenceNumber(this.referenceNumber);
        target.setProductId(this.productId);
        target.setCombinedReportNumber(this.combinedReportNumber);
        target.setRofNumber(this.rofNumber);
        target.setAdjReasonCode(this.adjReasonCode);
        target.setOverUnderCharge(this.overUnderCharge);
        target.setProcessedIndicator(this.processedIndicator);
        target.setMessageText(this.messageText);
        target.setSendToVenus(this.sendToVenus);
        target.setOrderPrinted(this.orderPrinted);
        target.setSubType(this.subType);
        target.setCreatedOn(this.createdOn);
        target.setUpdatedOn(this.updatedOn);
        target.setErrorDescription(this.errorDescription);
        target.setPrinted(this.printed);
        target.setCountry(this.country);
        target.setCancelReasonCode(this.cancelReasonCode);
        target.setCompOrder(this.compOrder);
        target.setOldPrice(this.oldPrice);
        target.setShipMethod(this.shipMethod);
        target.setVendorSKU(this.vendorSKU);
        target.setProductDescription(this.productDescription);
        target.setProductWeight(this.productWeight);
        target.setMessageDirection(this.messageDirection);
        target.setExternalSystemStatus(this.externalSystemStatus);
        target.setVendorId(this.vendorId);
        target.setShippingSystem(this.shippingSystem);
        target.setTrackingNumber(this.trackingNumber);
        target.setPrintedStatusDate(this.printedStatusDate);
        target.setShippedStatusDate(this.shippedStatusDate);
        target.setCancelledStatusDate(this.cancelledStatusDate);
        target.setFinalCarrier(this.finalCarrier);
        target.setFinalShipMethod(this.finalShipMethod);
        target.setSdsStatus(this.sdsStatus);
        target.setRatedShipment(this.ratedShipment);
        target.setForcedShipment(this.forcedShipment);
        
        return target;
    }

    public void setRejectedStatusDate(Date rejectedStatusDate) {
        this.rejectedStatusDate = rejectedStatusDate;
    }

    public Date getRejectedStatusDate() {
        return rejectedStatusDate;
    }
    
    public boolean isZoneJumpFlag()
    {
        return zoneJumpFlag;
    }

    public void setZoneJumpFlag(boolean zoneJumpFlag)
    {
        this.zoneJumpFlag = zoneJumpFlag;
    }
    
    public void setZoneJumpLabelDate(Date zoneJumpLabelDate) {
        this.zoneJumpLabelDate = zoneJumpLabelDate;
    }

    public Date getZoneJumpLabelDate() {
        return zoneJumpLabelDate;
    }
    
    public void setZoneJumpTrailerNumber(String zoneJumpTrailerNumber)
    {
      this.zoneJumpTrailerNumber = zoneJumpTrailerNumber;
    }


    public String getZoneJumpTrailerNumber()
    {
      return zoneJumpTrailerNumber;
    }

    public void setPersonalGreetingId(String personalGreetingId) {
        this.personalGreetingId = personalGreetingId;
    }

    public String getPersonalGreetingId() {
        return personalGreetingId;
    }

  public String getComplaintCommOriginTypeId()
  {
    return complaintCommOriginTypeId;
  }

  public void setComplaintCommOriginTypeId(String complaintCommOriginTypeId)
  {
    this.complaintCommOriginTypeId = complaintCommOriginTypeId;
  }

  public String getComplaintCommNotificationTypeId()
  {
    return complaintCommNotificationTypeId;
  }

  public void setComplaintCommNotificationTypeId(String complaintCommNotificationTypeId)
  {
    this.complaintCommNotificationTypeId = complaintCommNotificationTypeId;
  }

  public String getNewShippingSystem()
  {
    return newShippingSystem;
  }

  public void setNewShippingSystem(String newShippingSystem)
  {
    this.newShippingSystem = newShippingSystem;
  }

}
