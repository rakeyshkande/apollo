package com.ftd.op.venus.vo;

public class ProductInventoryVO 
{
    private String productId;
    private int inventoryLevel;
    private String threshold;

    public ProductInventoryVO()
    {
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public int getInventoryLevel()
    {
        return inventoryLevel;
    }

    public void setInventoryLevel(int inventoryLevel)
    {
        this.inventoryLevel = inventoryLevel;
    }

    public String getThreshold()
    {
        return threshold;
    }

    public void setThreshold(String threshold)
    {
        this.threshold = threshold;
    }
}