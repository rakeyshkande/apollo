package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;
import com.ftd.op.venus.to.VenusResultTO;


public class UpdateVO extends BaseVO
{
  VenusResultTO venusResultTO;
  boolean updated;
  Object updatedObj;

  public UpdateVO()
  {
    updated = false;
  }


  public void setVenusResultTO(VenusResultTO venusResultTO)
  {
    this.venusResultTO = venusResultTO;
  }


  public VenusResultTO getVenusResultTO()
  {
    return venusResultTO;
  }


  public void setUpdated(boolean updated)
  {
    this.updated = updated;
  }


  public boolean isUpdated()
  {
    return updated;
  }


  public void setUpdatedObj(Object updatedObj)
  {
    this.updatedObj = updatedObj;
  }


  public Object getUpdatedObj()
  {
    return updatedObj;
  }



}