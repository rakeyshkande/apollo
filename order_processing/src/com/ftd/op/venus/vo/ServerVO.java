package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;


public class ServerVO extends BaseVO
{

  private String location;
  private String logon;
  private String password;

  public ServerVO()
  {
  }

  public void setLocation(String location)
  {
    this.location = location;
  }


  public String getLocation()
  {
    return location;
  }


  public void setLogon(String logon)
  {
    this.logon = logon;
  }


  public String getLogon()
  {
    return logon;
  }


  public void setPassword(String password)
  {
    this.password = password;
  }


  public String getPassword()
  {
    return password;
  }




}