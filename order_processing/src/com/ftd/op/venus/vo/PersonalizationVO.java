package com.ftd.op.venus.vo;

public class PersonalizationVO {
    private String personalizationName;
    private String personalizationData;

    public PersonalizationVO() {
    }

    public void setPersonalizationName(String personalizationName) {
        this.personalizationName = personalizationName;
    }

    public String getPersonalizationName() {
        return personalizationName;
    }

    public void setPersonalizationData(String personalizationData) {
        this.personalizationData = personalizationData;
    }

    public String getPersonalizationData() {
        return personalizationData;
    }
}
