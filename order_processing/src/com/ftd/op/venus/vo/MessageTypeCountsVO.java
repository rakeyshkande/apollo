package com.ftd.op.venus.vo;
import com.ftd.op.common.vo.BaseVO;
import com.ftd.op.venus.to.VenusResultTO;


public class MessageTypeCountsVO extends BaseVO
{
  int ftdCount;
  int conCount;
  int rejCount;
  int printedCount;
  int canCount;
 
  public MessageTypeCountsVO()
  {
    ftdCount = 0;
    conCount = 0;
    rejCount = 0;
    printedCount = 0;
    canCount = 0;
  }

  public void setFtdCount(int ftdCount)
  {
    this.ftdCount = ftdCount;
  }


  public int getFtdCount()
  {
    return ftdCount;
  }


  public void setConCount(int conCount)
  {
    this.conCount = conCount;
  }


  public int getConCount()
  {
    return conCount;
  }


  public void setRejCount(int rejCount)
  {
    this.rejCount = rejCount;
  }


  public int getRejCount()
  {
    return rejCount;
  }


  public void setPrintedCount(int printedCount)
  {
    this.printedCount = printedCount;
  }


  public int getPrintedCount()
  {
    return printedCount;
  }


  public void setCanCount(int canCount)
  {
    this.canCount = canCount;
  }


  public int getCanCount()
  {
    return canCount;
  }


 
}