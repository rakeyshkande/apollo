package com.ftd.op.venus.vo;

import org.apache.commons.lang.StringUtils;

public class PersonalCreationsInboundVO {
    private String externalOrderNumber = StringUtils.EMPTY;
    private String trackingNumber = StringUtils.EMPTY;
    private String shipDate = StringUtils.EMPTY;
    private String acknowledgement = StringUtils.EMPTY;
    private int type = 0;
    
    // Type values
    public static final int ACKNOWLEDGEMENT_FILE = 1;
    public static final int TRACKING_FILE = 2;
    
    public PersonalCreationsInboundVO() {
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }
    
    public String toString() {
        return type + " " + externalOrderNumber + " " + trackingNumber + " " + shipDate + " " +  acknowledgement;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = externalOrderNumber;
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }
}
