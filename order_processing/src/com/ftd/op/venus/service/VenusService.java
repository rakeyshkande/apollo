package com.ftd.op.venus.service;

import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.dao.QueueDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.vo.QueueVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.AddOnVO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerPhoneVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.OrderBillVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.order.vo.ProductSubCodeVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.VendorBlockVO;
import com.ftd.op.order.vo.VendorProductVO;
import com.ftd.op.order.vo.VendorVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CancelMessageTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.op.venus.to.VenusBaseMessageTO;
import com.ftd.op.venus.to.VenusResultTO;
import com.ftd.op.venus.util.VenusCommonUtil;
import com.ftd.op.venus.vo.FedExDeliveryConfVO;
import com.ftd.op.venus.vo.MessageTypeCountsVO;
import com.ftd.op.venus.vo.ProductNotificationVO;
import com.ftd.op.venus.vo.TNMessageVO;
import com.ftd.op.venus.vo.UpdateVO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import org.w3c.dom.Element;

import org.xml.sax.SAXException;


//import com.ftd.op.order.vo.SourceVO;


/**
* VenusService
*
* This is a service between the MercuryDAO and VenusFacade
* to build the MercuryVOs.
*
*/
public class VenusService {

    private Connection conn;
    private Logger logger;

    private String WINE_VENDOR_ID;
    private String SAMS_VENDOR_ID;
    private String PM_VENDOR_ID;
    private String PC_VENDOR_ID;
    private String WINE_MEMBER_ID;
    private String SAMS_MEMBER_ID;
    private String PM_MEMBER_ID;
    private String RICHLINE_MEMBER_ID;
    private String PC_MEMBER_ID;
    
    private static final int WINE_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int SAMS_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int PM_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int ESCALATE_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int RICHLINE_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int PC_DIGITS_IN_ORDER_NUMBER = 7;
    private static final int WEST_DIGITS_IN_ORDER_NUMBER = 7;
    private static final String EXTERNAL_STATUS_NOT_VERIFIED = "NOTVERIFIED";

    private static final String DELIMITER = "||";

    /**
  * Constructor
  *
  * Constructor with the database connection passed in as a parameter 
  *
  * @param conn Connection
  */
    public VenusService(Connection conn) throws Exception {
        this.conn = conn;
        logger = new Logger("com.ftd.op.venus.service.VenusService");

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        this.WINE_VENDOR_ID =
                configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "WINE_COM_VENDOR_ID");
        this.SAMS_VENDOR_ID =
                configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "SAMS_COM_VENDOR_ID");
        this.PM_VENDOR_ID =
               configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "PERSONALIZATION_MALL_VENDOR_ID");
        this.PC_VENDOR_ID =
            configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                   "PERSONAL_CREATIONS_VENDOR_ID");
        this.WINE_MEMBER_ID =
                configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "WINE_COM_MEMBER_ID");
        this.SAMS_MEMBER_ID =
                configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "SAMS_COM_MEMBER_ID");
        this.PM_MEMBER_ID =
                configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                       "PERSONALIZATION_MALL_MEMBER_ID");
        this.RICHLINE_MEMBER_ID =
                  configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                                      "RICHLINE_MEMBER_ID");
        this.PC_MEMBER_ID =
            configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                   "PERSONAL_CREATIONS_MEMBER_ID");
                        
    }


    /**
   * This method takes a CancelOrderTO and builds a VenusMessageVO and sends
   * it using sendMessage(VenusMessageVO vo, boolean queueMessage).  It sets the
   * queueMessage to false.
   *
   * @param CancelOrderTO
   * @throws IOException, ParserConfigurationException, SQLException, SAXException, VenusException, Exception
   * @return VenusResultTO
  */
    public VenusResultTO sendCancel(CancelOrderTO to) throws IOException, ParserConfigurationException, SQLException, SAXException, VenusException, Exception {
        VenusDAO dao = new VenusDAO(conn);
        VenusMessageVO ftdVO = dao.getVenusMessageById(to.getVenusId());
        VenusMessageVO vo = new VenusMessageVO();
        vo.setMsgType("CAN");
        vo.setVenusOrderNumber(ftdVO.getVenusOrderNumber());
        vo.setReferenceNumber(ftdVO.getReferenceNumber());
        vo.setComments(to.getComments());
        vo.setOperator(to.getCsr());
        vo.setFillingVendor(ftdVO.getFillingVendor());
        vo.setCancelReasonCode(to.getCancelReasonCode());
        vo.setVenusStatus(to.getVenusStatus());
        
        if (to.getComplaintCommOriginTypeId() == null)
        {
          to.setComplaintCommOriginTypeId(vo.complaintCommTypeIdNotAComplaint);
        }
        if (to.getComplaintCommNotificationTypeId() == null)
        {
          to.setComplaintCommNotificationTypeId(vo.complaintCommTypeIdNotAComplaint);
        }
        vo.setComplaintCommOriginTypeId(to.getComplaintCommOriginTypeId());
        vo.setComplaintCommNotificationTypeId(to.getComplaintCommNotificationTypeId());
        vo.setNewShippingSystem(ftdVO.getNewShippingSystem());

        return this.sendMessage(vo, false, to.isTransmitCancel());
    }


    /**
   * This method takes a OrderDetailKeyTO and builds a VenusMessageVO and sends
   * it using sendOrder(VenusMessageVO vo, boolean queueOrder, OrderDetailVO orderDetailVO, OrderVO orderVO, CustomerVO customerVO).
   * It sets the queueOrder to false.
   *
   * @param to
   * @throws VenusException, Exception
   * @return String
  */
    public VenusResultTO sendOrder(OrderDetailKeyTO to) throws VenusException, Exception {
        logger.info("sendOrder(OrderDetailKeyTO) - OrderDetailID: " + to.getOrderDetailId());
        OrderDAO orderDAO = new OrderDAO(conn);
        OrderDetailVO orderDetailVO =
            orderDAO.getOrderDetail(to.getOrderDetailId());
        if (orderDetailVO == null) {
            throw new VenusException("No order detail record for the given order detail id: " +
                                     to.getOrderDetailId() + DELIMITER);
        }
        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        if (orderVO == null) {
            throw new VenusException("No order record for the given order guid: " +
                                     orderDetailVO.getOrderGuid() + DELIMITER);
        }
        CustomerVO customerVO =
            orderDAO.getCustomer(orderDetailVO.getRecipientId());
        if (customerVO == null) {
            throw new VenusException("No customer record for the given id: " +
                                     orderDetailVO.getRecipientId() +
                                     DELIMITER);
        }
        ProductVO productVO =
            orderDAO.getProduct(orderDetailVO.getProductId());
        if (productVO == null) {
            throw new VenusException("No product record for the given id: " +
                                     orderDetailVO.getProductId() + DELIMITER);
        }

        boolean bVenusOrder = !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS)
                              && !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST);

        //check if the order is for a product with a subtype
        ProductSubCodeVO subcodeVO = null;
        if (orderDetailVO.getSubcode() != null &&
            orderDetailVO.getSubcode().length() > 0) {
            //For products with subtypes the subtype product codes and price should
            //be used.
            subcodeVO = orderDAO.getSubcode(orderDetailVO.getSubcode());
            if (subcodeVO == null) {
                throw new Exception("Subcode does not exist in DB.  Subcode=" +
                                    orderDetailVO.getSubcode());
            }
        }

        /*
         * The VendorProductVO could have possibly been passed in but
         * since the API is public the VendorProductVO is obtained to
         * avoid breaking something and to avoid changing all calls to the
         * public API.
         */
        VendorProductVO vpVO = null;
        if( bVenusOrder ) {
            // Obtain vendor product information
            vpVO = getVendorProduct(orderDetailVO);

            orderDetailVO.setVendorId(vpVO.getVendorId());
        }

        VenusMessageVO vo = new VenusMessageVO();
        // set order number to next number in sequence //
        vo.setVenusStatus("OPEN");
        vo.setMsgType("FTD");
        vo.setShippingSystem(productVO.getShippingSystem());
        vo.setReferenceNumber(new Long(orderDetailVO.getOrderDetailId()).toString());
        
        if(orderDetailVO.isMorningDeliveryOrder()){
            logger.info("isMorningDeliveryOrder() == true");
            if(orderDetailVO.getShipMethod() != null && !orderDetailVO.getShipMethod().equals("")){
                        if(orderDetailVO.getShipMethod().equalsIgnoreCase("GR") || orderDetailVO.getShipMethod().equalsIgnoreCase("2F") ||
                              orderDetailVO.getShipMethod().equalsIgnoreCase("SA") || orderDetailVO.getShipMethod().equalsIgnoreCase("ND") ){
                              
                              SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
                              Calendar cal = Calendar.getInstance();
                        cal.setTime(orderDetailVO.getDeliveryDate());
                              cal.add( Calendar.DATE, -1 );
                        String newShipDate = dateFormat.format(cal.getTime()); 
                                                      
                        String morningDeliveryOrderComment = "Agent Alert this is a Morning Delivery Order - - The Ship Method has been modified to accommodate morning delivery.";

                        if(orderDetailVO.getShipMethod().equalsIgnoreCase("SA")){
                              int results = getDiffInDays(orderDetailVO.getShipDate(),cal.getTime()); 
                              if ( results != 0){
                                    logger.info("SA results == 0 ");
                                    vo.setShipMethod("SA");
                                    vo.setShipDate(dateFormat.parse(newShipDate));
                                    orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                      orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), vo.getShipMethod(), dateFormat.parse(newShipDate), null);
                                    this.insertComment(morningDeliveryOrderComment, orderDetailVO);
                              }
                              else{
                                    vo.setShipMethod(orderDetailVO.getShipMethod());
                                    vo.setShipDate(orderDetailVO.getShipDate());                              
                              }
                        }
                        else if(orderDetailVO.getShipMethod().equalsIgnoreCase("ND")){
                              int results = getDiffInDays(orderDetailVO.getShipDate(),cal.getTime()); 
                              if ( results != 0){                       
                                    vo.setShipMethod("ND");
                                    vo.setShipDate(dateFormat.parse(newShipDate));
                                    orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                      orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), vo.getShipMethod(), dateFormat.parse(newShipDate), null);
                              }
                              else{
                              vo.setShipMethod(orderDetailVO.getShipMethod());
                                vo.setShipDate(orderDetailVO.getShipDate());       
                            }
                        }
                        else{
                              vo.setShipMethod("ND");
                              orderDetailVO.setShipMethod("ND");
                              vo.setShipDate(dateFormat.parse(newShipDate));
                              orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), vo.getShipMethod(), dateFormat.parse(newShipDate), null);
                              this.insertComment(morningDeliveryOrderComment, orderDetailVO);
                        }
                  }
            }
          }
        else{
            vo.setShipMethod(orderDetailVO.getShipMethod());
              vo.setShipDate(orderDetailVO.getShipDate());       
        }
        
        vo.setFillingVendor(orderDetailVO.getFloristId());
        vo.setOrderDate(orderVO.getOrderDate());
        vo.setRecipient(customerVO.getFirstName() + " " + customerVO.getLastName());
        vo.setBusinessName(customerVO.getBusinessName());
        vo.setAddress1(customerVO.getAddress1());
        vo.setAddress2(customerVO.getAddress2());
        vo.setCity(customerVO.getCity());
        vo.setState(customerVO.getState());
        vo.setZip(customerVO.getZipCode());
        ArrayList phoneList = (ArrayList)customerVO.getCustomerPhoneVOList();

        CustomerPhoneVO phoneVO;
        String day = null;
        String evening = null;
        for (int i = 0; i < phoneList.size(); i++) {
            phoneVO = (CustomerPhoneVO)phoneList.get(i);
            if (phoneVO.getPhoneType().equalsIgnoreCase("DAY")) {
                day = phoneVO.getPhoneNumber();
            } else if (phoneVO.getPhoneType().equalsIgnoreCase("EVENING")) {
                evening = phoneVO.getPhoneNumber();
            }
        }
        if (day != null && (!day.equalsIgnoreCase(""))) {
            if(day.length() > 10)
            {
                  day = day.substring(0, 10);
            }
            vo.setPhoneNumber(day);
        } else if ((evening != null) && (!evening.equalsIgnoreCase(""))) {
            if(evening.length() > 10)
            {
                  evening = evening.substring(0, 10);
            }
            vo.setPhoneNumber(evening);
        }
        vo.setDeliveryDate(orderDetailVO.getDeliveryDate());

        if( bVenusOrder ) {
            vo.setPrice(new Double(vpVO.getVendorCost()));
        } else {
            vo.setPrice(new Double("0"));
        }

        String cardMessage = "";
        if (orderDetailVO.getCardMessage() != null &&
            (!orderDetailVO.getCardMessage().equalsIgnoreCase(""))) {
            cardMessage = orderDetailVO.getCardMessage();
        }
        if (orderDetailVO.getCardSignature() != null &&
            (!orderDetailVO.getCardSignature().equalsIgnoreCase(""))) {
            cardMessage = cardMessage + " " + orderDetailVO.getCardSignature();
        }
        vo.setCardMessage(cardMessage);
        vo.setOperator(to.getCsr());
        vo.setTransmissionTime(new Date());
        vo.setProductID(orderDetailVO.getProductId());
        vo.setProcessedIndicator("N");
        vo.setOrderPrinted("N");
        vo.setCountry(customerVO.getCountry());

        if( bVenusOrder ) 
        {
            vo.setFirstChoice(this.buildFirstChoiceString(orderDetailVO,
                                                      productVO, subcodeVO,
                                                      vpVO));
        } 
        else 
        {
          String sdsFirstChoice = buildFirstChoiceStringSDS(orderDetailVO, productVO, subcodeVO); 
          vo.setFirstChoice(sdsFirstChoice);
        }


        //check if this a comp order
        if (isCompOrder(String.valueOf(orderDetailVO.getOrderDetailId()))) {
            vo.setCompOrder("C");
        }

        return this.sendOrder(vo, false, orderDetailVO, orderVO, customerVO,
                              productVO, vpVO);
    }

    /**
   * This method takes a venusBaseMessageTO and transforms it to a venusMessageVO
   * and send it to that sendMessage method.  The boolean queueOrder tells you if
   * you should queue the order on an error.
   *
   * @param queueMessage
   * @param venusBaseMessageTO
   * @return VenusResultTO
   * @throws VenusException, SAXException, SQLException, ParserConfigurationException, IOException, Exception
   */
    public

    VenusResultTO sendMessage(VenusBaseMessageTO venusBaseMessageTO,
                              boolean queueMessage) throws VenusException,
                                                           SAXException,
                                                           SQLException,
                                                           ParserConfigurationException,
                                                           IOException,
                                                           Exception {
        VenusMessageVO venusMessageVO = new VenusMessageVO();
        /* This boolean will be false for CAN orders that have already been "printed".
     * It will add this error to the comments table -
     * "Cancel message will not be sent to Venus because order already shipped. -
     * but not send the CAN message. An ADJ message will be sent afterwards.*/
        boolean transmitCancel = true;
        //TODO:  Should we be getting the original order number and populateing the shipping_system field?

        venusMessageVO.setFillingVendor(venusBaseMessageTO.getFillingVendor());
        venusMessageVO.setMsgType(venusBaseMessageTO.getMessageType());
        venusMessageVO.setOperator(venusBaseMessageTO.getOperator());
        venusMessageVO.setVenusOrderNumber(venusBaseMessageTO.getVenusOrderNumber());
        venusMessageVO.setReferenceNumber(venusBaseMessageTO.getOrderDetailId());
        if (venusBaseMessageTO.getMessageType().equalsIgnoreCase("CAN")) {
            CancelMessageTO cancelMessageTO =
                (CancelMessageTO)venusBaseMessageTO;
            venusMessageVO.setComments(cancelMessageTO.getComments());
            venusMessageVO.setCancelReasonCode(cancelMessageTO.getCancelReasonCode());

            transmitCancel = cancelMessageTO.isTransmitCancel();
        }
        if (venusBaseMessageTO.getMessageType().equalsIgnoreCase("ADJ")) {
            AdjustmentMessageTO adjustmentMessageTO =
                (AdjustmentMessageTO)venusBaseMessageTO;
            venusMessageVO.setComments(adjustmentMessageTO.getComments());
            venusMessageVO.setOldPrice(adjustmentMessageTO.getOldPrice());
            venusMessageVO.setOverUnderCharge(adjustmentMessageTO.getOverUnderCharge());
            venusMessageVO.setPrice(adjustmentMessageTO.getPrice());
            venusMessageVO.setCancelReasonCode(adjustmentMessageTO.getCancelReasonCode());
        }

        return this.sendMessage(venusMessageVO, queueMessage, transmitCancel);
    }

    /**
   * This method takes a VenusMessageVO and a boolean and inserts the vo into the
   * VenusTable. If the vo is not valid and the boolean is true, it inserts it in
   * the queue.  If the vo is valid, it also runs sendMessageToEscalate(VenusMessageVO)
   *
   * @param VenusMessageVO
   * @param boolen queueMessage
   * @param boolean sendMessage - This boolean will be false for CAN orders that
   * have already been "printed". It will add this error to the comments table -
   * "Cancel message will not be sent to Venus because order already shipped.
   * System generated ADJ will go to Venus." Then an ADJ will be sent.
   * @throws VenusException, SAXException, SQLException, ParserConfigurationException, IOException, Exception
   * @return VenusResultTO
  */
    public VenusResultTO sendMessage(VenusMessageVO vo, boolean queueMessage,
                                     boolean transmitCancel) throws VenusException,
                                                                    SAXException,
                                                                    SQLException,
                                                                    ParserConfigurationException,
                                                                    IOException,
                                                                    Exception {
        String key = null;
        VenusResultTO venusResultTO = new VenusResultTO();
        OrderDAO orderDAO = new OrderDAO(conn);
        VenusDAO venusDAO = new VenusDAO(conn);
        logger.info("sendMessage(VenusMessageVO,...) - OrderDetailID: " + vo.getReferenceNumber());

        // set defaults //
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String defaultSendingVendor =
            configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                   "DEFAULT_SENDING_VENDOR");
        vo.setSendingVendor(defaultSendingVendor);
        // get vos//
        OrderDetailVO orderDetailVO =
            orderDAO.getOrderDetail(vo.getReferenceNumber());
        if (orderDetailVO == null) {
            throw new VenusException("No order detail record for the given order detail id: " +
                                     vo.getReferenceNumber() + DELIMITER);
        }
        CustomerVO customerVO =
            orderDAO.getCustomer(orderDetailVO.getRecipientId());
        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        if (orderVO == null) {
            throw new VenusException("No order record for the given order guid: " +
                                     orderDetailVO.getOrderGuid() + DELIMITER);
        }
        if (customerVO == null) {
            throw new VenusException("No customer record for the given id: " +
                                     orderDetailVO.getRecipientId() +
                                     DELIMITER);
        }
        venusResultTO.setCustomerId(new Long(orderVO.getCustomerId()).toString());
        venusResultTO.setOrderGuid(orderVO.getOrderGuid());
        venusResultTO.setOrderDetailId(new Long(orderDetailVO.getOrderDetailId()).toString());
        venusResultTO.setSuccess(true);

        venusResultTO =
                this.processMessage(vo, queueMessage, venusResultTO, orderDetailVO,
                                    transmitCancel);
                                    
        
        boolean bVenusMessage;
        if( StringUtils.equals(vo.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS) ||
            StringUtils.equals(vo.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST)) {
            bVenusMessage = false;
        } else {
            bVenusMessage = true;
        }
        
        if (venusResultTO.isSuccess()) {
            logger.debug("Venus message " + vo.getVenusOrderNumber() +
                         ":VERIFIED (order detail id=" +
                         venusResultTO.getOrderDetailId() + ")");
            
            if( bVenusMessage ) {
                vo.setVenusStatus("VERIFIED");
            } else {
                if( StringUtils.equals(vo.getMsgType(),"CAN") && !StringUtils.equals(vo.getVenusStatus(),"VERIFIED") ) {
                    vo.setVenusStatus("OPEN");
                }
            }

            //NOTE: even though this method is called sendMessage
            //it does not send a message to the ship processing jms queue via the java code.
            //There is actually a trigger on the venus table when a record is inserted
            //it checks to see if the msg type is CAN or REJ if it is then it sends 
            //a jms message to the ship processing or ship ship processing queue.
            //trigger name/location = database\venus_plsql\trg_venus_aiou           
            key = venusDAO.insertVenusMessage(vo, VenusConstants.OUTBOUND_MSG);
                        
            if (venusResultTO.isTreatAsAdjustment()) {
                logger.debug("Venus message " + vo.getVenusId() +
                             ":Will be treated as adjustment");
                vo.setMessageText("ADJ");
            }
            
            if( bVenusMessage ) {
            /**If (!transmitCancel), Cancel message will not be sent to Venus (Escalate)
       * because order already shipped.  System generated ADJ will go to Venus.**/
            if (transmitCancel) {
                logger.debug("Venus message " + vo.getVenusId() +
                             ":Is being transmitted to Escalate");
                    
                this.sendMessageToEscalate(vo);
            } else {
                logger.debug("Venus message " + vo.getVenusId() +
                             ":Is NOT being transmitted to Escalate");
            }
            }
        } else {
            logger.debug("Venus message " + vo.getVenusId() + ":Rejected. " +
                         venusResultTO.getErrorString());
            if (queueMessage) {
                logger.debug("Venus message " + vo.getVenusId() +
                             ":Message queued");
                vo.setVenusStatus("REJECTED");
                key = venusDAO.insertVenusMessage(vo, VenusConstants.OUTBOUND_MSG);
                this.insertIntoQueue(vo, orderDetailVO, orderVO);
                this.insertComment(venusResultTO.getErrorString(),
                                   orderDetailVO);
            } else {
                logger.debug("Venus message " + vo.getVenusId() +
                             ":Message WILL NOT BE queued");
                throw new VenusException(venusResultTO.getErrorString() +
                                         DELIMITER);
            }
        }
        venusResultTO.setKey(key);
        return venusResultTO;
    }

    /*
     * This method checks if the vendor needs to be verified.
     * Any vendor member number in the config file needs to be verified.
     */

    private boolean isVerifyVendor(String memberNumber) throws Exception {
        boolean verifyVendor = true;

        logger.debug("Member number=" + memberNumber);

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String nonVerifyVendors =
            configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                   "NON_VERIFY_VENDORS");


        int pos = -1;
        if (nonVerifyVendors != null) {
            pos = nonVerifyVendors.indexOf(memberNumber);
        }

        logger.debug("isVerify, pos=" + pos);

        if (pos >= 0) {
            verifyVendor = false;
            logger.debug("verifyVendor=false");
        }

        return verifyVendor;

    }

    /**
   * This method takes a VenusMessageVO and a boolean and inserts the vo into the
   * VenusTable. If the vo is not valid and the boolean is true, it inserts it in
   * the queue.
   *
   * @param VenusMessageVO
   * @param boolean queueMessage
   * @param OrderDetailVO orderDetailVO
   * @param OrderVO orderVO
   * @param CustomerVO customerVO
   * @param VendorProductVO vpVO
   * @throws VenusException, SAXException, SQLException, ParserConfigurationException, IOException, Exception
   * @return String
  */
    public VenusResultTO sendOrder(VenusMessageVO vo, boolean queueOrder,
                                   OrderDetailVO orderDetailVO,
                                   OrderVO orderVO, CustomerVO customerVO,
                                   ProductVO productVO,
                                   VendorProductVO vpVO) throws VenusException,
                                                                Exception {

        VenusDAO venusDAO = new VenusDAO(conn);
        QueueDAO queueDAO = new QueueDAO(conn);
        OrderDAO orderDAO = new OrderDAO(conn);
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        VenusResultTO venusResultTO = new VenusResultTO();

        logger.info("sendOrder(VenusMessageVO,...) - OrderDetailID: " + orderDetailVO.getOrderDetailId());

        String key = null;
        // set defaults //
        String defaultSendingVendor =
            configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                   "DEFAULT_SENDING_VENDOR");
        vo.setSendingVendor(defaultSendingVendor);

        //check if the order is for a product with a subtype
        String productSkuId;
        if (orderDetailVO.getSubcode() != null &&
            orderDetailVO.getSubcode().length() > 0) {
            productSkuId = orderDetailVO.getSubcode();
            //For products with subtypes the subtype product codes and price should
            //be used.
            ProductSubCodeVO subcodeVO =
                orderDAO.getSubcode(orderDetailVO.getSubcode());
            if (subcodeVO == null) {
                throw new Exception("Subcode does not exist in DB.  Subcode=" +
                                    orderDetailVO.getSubcode());
            }
            vo.setProductWeight(subcodeVO.getDimWeight());
            vo.setProductDescription(subcodeVO.getSubCodeRefNumber() + "," +
                                     subcodeVO.getSubCodeDescription());
            vo.setProductID(subcodeVO.getProductSubCodeId());
        } else {
            productSkuId = orderDetailVO.getProductId();
            vo.setProductWeight(productVO.getDimWeight());
            vo.setProductDescription(productVO.getMercuryDescription());
        }

        vo.setPersonalGreetingId(orderDetailVO.getPersonalGreetingId());  // Propogate Personal Greeting ID

        boolean bIsFtdwest = StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST);
        boolean bVenusOrder = !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS) 
                              && !bIsFtdwest;

        if( bVenusOrder || bIsFtdwest) {
            // Obtain vendor information if needed
            if( vpVO == null ) {
                try {
                    vpVO = getVendorProduct(orderDetailVO);
                } catch(Exception e) {
                    venusResultTO.setErrorString(e.toString());
                    venusResultTO.setSuccess(false);
                    if (bIsFtdwest) {
                        queueOrder = true;
                    }
                }
            }
            if (vpVO != null) {
                vo.setVendorSKU(vpVO.getVendorSku());
                orderDetailVO.setVendorId(vpVO.getVendorId());
                vo.setVendorId(vpVO.getVendorId());
                vo.setPrice(new Double(vpVO.getVendorCost()));
            }
        }

        // process order //        VenusResultTO venusResultTO = null;
        if (venusResultTO.isSuccess()) {
            venusResultTO =
            this.processOrder(vo, queueOrder, orderDetailVO, orderVO,
                              customerVO, productVO);
        }
        if (venusResultTO.isSuccess()) {
            boolean dispatchToShipProcessing = false;
            if( bVenusOrder ) {
                // check for wine.com
                logger.debug("Filling vendor =" + vo.getFillingVendor());
                if ((vo.getFillingVendor() != null) &&
                    (vo.getFillingVendor().equalsIgnoreCase(this.WINE_MEMBER_ID))) {
                    logger.debug("Wine Order");
                    vo.setVenusStatus("OPEN");
                    vo.setVenusOrderNumber(getWineOrderNumber(conn));
                } else if ((vo.getFillingVendor() != null) &&
                           (vo.getFillingVendor().equalsIgnoreCase(this.SAMS_MEMBER_ID))) {
                    logger.debug("Sams Order");
                    vo.setVenusStatus("OPEN");
                    vo.setVenusOrderNumber(getSAMSOrderNumber(conn));
                } else if ((vo.getFillingVendor() != null) &&
                           (vo.getFillingVendor().equalsIgnoreCase(this.PM_MEMBER_ID))) {
                    logger.debug("Personalization Mall Order");
                    vo.setVenusStatus("OPEN");
                    vo.setVenusOrderNumber(getPMOrderNumber(conn));
                } else if ((vo.getFillingVendor() != null)  && 
                        (vo.getFillingVendor().equalsIgnoreCase(this.RICHLINE_MEMBER_ID))) {
                  logger.debug("Richline Order");
                  vo.setVenusStatus("OPEN");
                  vo.setVenusOrderNumber(getRichlineOrderNumber(conn));
                } else if ((vo.getFillingVendor() != null) &&
                        (vo.getFillingVendor().equalsIgnoreCase(this.PC_MEMBER_ID))) {
                  logger.debug("Personal Creations Order");
                  vo.setVenusStatus("OPEN");
                  vo.setVenusOrderNumber(getPCOrderNumber(conn));
                } else  {
                    dispatchToShipProcessing = true;
                    vo.setVenusStatus("SHIP");
                    if(productVO.getShippingSystem().equalsIgnoreCase("FTD WEST"))
                    {
                        logger.debug("West Order");
                        vo.setVenusOrderNumber(getFtdWestOrderNumber(conn));
                    }
                    else
                    {
                        logger.debug("Argo Order");
                        vo.setVenusOrderNumber(getEscalateOrderNumber(conn));
                    }
                }
            } else {
                logger.debug("SDS Order");
                dispatchToShipProcessing = true;
                vo.setVenusStatus("SHIP");
                if(productVO.getShippingSystem().equalsIgnoreCase("FTD WEST"))
                {
                    logger.debug("West Order");
                    vo.setVenusOrderNumber(getFtdWestOrderNumber(conn));
                }
                else
                {
                    logger.debug("Argo Order");
                    vo.setVenusOrderNumber(getEscalateOrderNumber(conn));
                }
            }

            //check config file to orders for this vendor need to be verified
            if (bVenusOrder && isVerifyVendor(vo.getFillingVendor())) {
                key = venusDAO.insertVenusMessage(vo, VenusConstants.OUTBOUND_MSG, EXTERNAL_STATUS_NOT_VERIFIED);
            } else {
                key = venusDAO.insertVenusMessage(vo, VenusConstants.OUTBOUND_MSG);
            }

            //only dispatch normal escalate orders to ship processing
            if (dispatchToShipProcessing) {
                this.sendShipProcessMessage(key, vo.getProductId());
            }
        } else {
            if (queueOrder) {
                vo.setVenusStatus("REJECTED");

                if( bVenusOrder ) {
                    // check for wine.com
                    logger.debug("Filling vendor=" + vo.getFillingVendor());
                    if ((vo.getFillingVendor() != null) &&
                        (vo.getFillingVendor().equalsIgnoreCase(this.WINE_MEMBER_ID))) {
                        logger.debug("Wine Order");
                        vo.setVenusOrderNumber(getWineOrderNumber(conn));
                    } else if ((vo.getFillingVendor() != null) &&
                               (vo.getFillingVendor().equalsIgnoreCase(this.SAMS_MEMBER_ID))) {
                        logger.debug("Sarms Order");
                        vo.setVenusOrderNumber(getSAMSOrderNumber(conn));
                    } else if ((vo.getFillingVendor() != null) &&
                               (vo.getFillingVendor().equalsIgnoreCase(this.PM_MEMBER_ID))) {
                        logger.debug("Personalization Mall Order");
                        vo.setVenusOrderNumber(getPMOrderNumber(conn));
                    } else if ((vo.getFillingVendor() != null) &&
                                 (vo.getFillingVendor().equalsIgnoreCase(this.RICHLINE_MEMBER_ID))){
                        logger.debug("Richline Order");
                        vo.setVenusOrderNumber(getRichlineOrderNumber(conn));
                    } else if ((vo.getFillingVendor() != null) &&
                            (vo.getFillingVendor().equalsIgnoreCase(this.PC_MEMBER_ID))) {
                        logger.debug("Personal Creationss Order");
                        vo.setVenusOrderNumber(getPCOrderNumber(conn));
                    }
                    else {
                        logger.debug("Escalate Order");
                        vo.setVenusOrderNumber(getEscalateOrderNumber(conn));
                    }
                } else {
                    logger.debug("SDS Order");
                    vo.setVenusOrderNumber(getEscalateOrderNumber(conn));
                }
                key = venusDAO.insertVenusMessage(vo, VenusConstants.OUTBOUND_MSG);
                // insert into queue //
                this.insertIntoQueue(vo, orderDetailVO, orderVO);
                // insert comment //
                this.insertComment(venusResultTO.getErrorString(),
                                   orderDetailVO);
            } else {
                throw new VenusException(venusResultTO.getErrorString() +
                                         DELIMITER);
            }
        }
        venusResultTO.setKey(key);
        return venusResultTO;
    }
    
    private String getRichlineOrderNumber(Connection conn) throws Exception {
      VenusDAO venusDAO = new VenusDAO(conn);
      String seq = 
            "0000000000" + Long.toString(venusDAO.generateVenusOrderNumber());
      return "RICH" + 
            seq.substring(seq.length() - RICHLINE_DIGITS_IN_ORDER_NUMBER);
    }

    private String getWineOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateNextWineOrderNumber());

        return "WINE" +
            seq.substring(seq.length() - WINE_DIGITS_IN_ORDER_NUMBER);
    }

    private String getSAMSOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateNextSamsOrderNumber());

        return "SAMS" +
            seq.substring(seq.length() - SAMS_DIGITS_IN_ORDER_NUMBER);
    }

    private String getPMOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateNextPMOrderNumber());

        return "PM" +
            seq.substring(seq.length() - PM_DIGITS_IN_ORDER_NUMBER);
    }
    
    private String getPCOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateNextPCOrderNumber());

        return "PC" +
            seq.substring(seq.length() - PC_DIGITS_IN_ORDER_NUMBER);
    }

    private String getEscalateOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateVenusOrderNumber());

        return "E" +
            seq.substring(seq.length() - ESCALATE_DIGITS_IN_ORDER_NUMBER);
    }

    /**
   * This method takes a OrderTO and turns it into a venusMessageVO so it can
   * be sent using sendOrder(venusMessageVO, queueOrder, orderDetailVO, orderVO, customerVO).
   * If the vo is not valid and the boolean is true, it inserts it into the queue.
   * @throws Exception, VenusException
   * @return VenusResultTO
   * @param queueOrder
   * @param orderTO
   */
    public

    VenusResultTO sendOrder(OrderTO orderTO,
                            boolean queueOrder) throws VenusException,
                                                       Exception {
        logger.info("sendOrder(OrderTO) - OrderDetailID: " + orderTO.getReferenceNumber());

        // pull information from TO//
        VenusMessageVO venusMessageVO = new VenusMessageVO();
        venusMessageVO.setAddress1(orderTO.getAddress1());
        venusMessageVO.setAddress2(orderTO.getAddress2());
        venusMessageVO.setBusinessName(orderTO.getBusinessName());
        String cardMessage = "";
        if (orderTO.getCardMessage() != null &&
            (!orderTO.getCardMessage().equalsIgnoreCase(""))) {
            cardMessage = orderTO.getCardMessage();
        }
        if (orderTO.getCardSignature() != null &&
            (!orderTO.getCardSignature().equalsIgnoreCase(""))) {
            cardMessage = cardMessage + orderTO.getCardSignature();
        }
        venusMessageVO.setCardMessage(cardMessage);
        venusMessageVO.setCity(orderTO.getCity());
        venusMessageVO.setDeliveryDate(orderTO.getDeliveryDate());
        venusMessageVO.setFillingVendor(orderTO.getFillingVendor());
        venusMessageVO.setMsgType(orderTO.getMessageType());
        venusMessageVO.setOperator(orderTO.getOperator());
        venusMessageVO.setOrderDate(orderTO.getOrderDate());
        venusMessageVO.setOverUnderCharge(orderTO.getOverUnderCharge());
        String phoneNumber = orderTO.getPhoneNumber();
        if (phoneNumber != null && (!phoneNumber.equalsIgnoreCase(""))) {
            if(phoneNumber.length() > 10)
            {
                  phoneNumber = phoneNumber.substring(0, 10);
            }
        } 
        venusMessageVO.setPhoneNumber(phoneNumber);
        venusMessageVO.setPrice(orderTO.getPrice());
        venusMessageVO.setProductID(orderTO.getProductId());
        venusMessageVO.setRecipient(orderTO.getRecipient());
        venusMessageVO.setReferenceNumber(orderTO.getReferenceNumber());        
        venusMessageVO.setState(orderTO.getState());
        venusMessageVO.setSubType(orderTO.getSubType());
        venusMessageVO.setVenusOrderNumber(orderTO.getVenusOrderNumber());
        venusMessageVO.setZip(orderTO.getZipCode());
        venusMessageVO.setCountry(orderTO.getCountry());        
        venusMessageVO.setCompOrder(orderTO.getCompOrder());
        venusMessageVO.setFromCommunicationScreen(orderTO.isFromCommunicationScreen());
      venusMessageVO.setShipMethod(orderTO.getShipMethod());
      venusMessageVO.setShipDate(orderTO.getShipDate());
        // get needed VOs//
        OrderDAO orderDAO = new OrderDAO(conn);
        // get the order detail vo //
        OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderTO.getReferenceNumber());
        if (orderDetailVO == null) 
        {
          throw new VenusException("No order detail record for the given order detail id: " +
                                     orderTO.getReferenceNumber() + DELIMITER);
        }
        
        if(orderDetailVO.isMorningDeliveryOrder()){
            logger.info("isMorningDeliveryOrder() == true");
            if(venusMessageVO.getShipMethod() != null && !venusMessageVO.getShipMethod().equals("")){
                        if(venusMessageVO.getShipMethod().equalsIgnoreCase("GR") || venusMessageVO.getShipMethod().equalsIgnoreCase("2F") ||
                              venusMessageVO.getShipMethod().equalsIgnoreCase("SA") || venusMessageVO.getShipMethod().equalsIgnoreCase("ND")){
                        
                              SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
                              Calendar cal = Calendar.getInstance();
                        cal.setTime(venusMessageVO.getDeliveryDate());
                              cal.add( Calendar.DATE, -1 );
                        String newShipDate = dateFormat.format(cal.getTime()); 
                        String morningDeliveryOrderComment = "Agent Alert this is a Morning Delivery Order - - The Ship Method has been modified to accommodate morning delivery.";
                        
                        if(venusMessageVO.getShipMethod().equalsIgnoreCase("SA")){
                              int results = getDiffInDays(orderDetailVO.getShipDate(),cal.getTime()); 
                              if ( results != 0){     
                                    venusMessageVO.setShipMethod("SA");
                                    orderDetailVO.setShipMethod("SA");
                                    venusMessageVO.setShipDate(dateFormat.parse(newShipDate));
                                    orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                      orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), venusMessageVO.getShipMethod(), dateFormat.parse(newShipDate), venusMessageVO.getDeliveryDate());
                              }
                       }
                        else if(venusMessageVO.getShipMethod().equalsIgnoreCase("ND")){
                              int results = getDiffInDays(orderDetailVO.getShipDate(),cal.getTime()); 
                              if ( results != 0){                       
                                    venusMessageVO.setShipMethod("ND");
                                    venusMessageVO.setShipDate(dateFormat.parse(newShipDate));
                                    orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                      orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), venusMessageVO.getShipMethod(), dateFormat.parse(newShipDate), venusMessageVO.getDeliveryDate());
                              }
                        }
                        else{
                              venusMessageVO.setShipMethod("ND");
                              orderDetailVO.setShipMethod("ND");
                              venusMessageVO.setShipDate(dateFormat.parse(newShipDate));
                              orderDetailVO.setShipDate(dateFormat.parse(newShipDate));
                                orderDAO.updateOrderShippingInfo(orderDetailVO.getOrderDetailId(), venusMessageVO.getShipMethod(), dateFormat.parse(newShipDate), venusMessageVO.getDeliveryDate());
                              this.insertComment(morningDeliveryOrderComment, orderDetailVO);

                        }
                        }
            }
          }
        
        // get the order vo //
        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        if (orderVO == null) 
        {
          throw new VenusException("No order record for the given order guid: " +
                                     orderDetailVO.getOrderGuid() + DELIMITER);
        }

        // get the customer vo //
        CustomerVO customerVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
        if (customerVO == null) 
        {
          throw new VenusException("No customer record for the given id: " +
                                     orderDetailVO.getRecipientId() + DELIMITER);
        }

        // get the product vo //
        ProductVO productVO = orderDAO.getProduct(orderDetailVO.getProductId());
        if (productVO == null) 
        {
          throw new VenusException("No product record for the given id: " +
                                     orderDetailVO.getProductId() + DELIMITER);
        }
        venusMessageVO.setShippingSystem(productVO.getShippingSystem());

        boolean bVenusOrder = !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS)
                              && !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST);

        //check if the order is for a product with a subtype
        ProductSubCodeVO subcodeVO = null;
        String productSkuId;
        if (orderDetailVO.getSubcode() != null &&
            orderDetailVO.getSubcode().length() > 0) 
        {
          productSkuId = orderDetailVO.getSubcode();
          //For products with subtypes the subtype product codes and price should
          //be used.
          subcodeVO = orderDAO.getSubcode(orderDetailVO.getSubcode());
          if (subcodeVO == null) 
          {
            throw new Exception("Subcode does not exist in DB.  Subcode=" +
                                  orderDetailVO.getSubcode());
          }
        } 
        else 
        {
          productSkuId = orderDetailVO.getProductId();
        }

        /*
         * The VendorProductVO could have possibly been passed in but
         * since the API is public the VendorProductVO is obtained to
         * avoid breaking something and to avoid changing all calls to the
         * public API.
         */
        VendorProductVO vpVO = null;
        if( bVenusOrder ) 
        {
            // Obtain vendor id information
            vpVO = getVendorProduct(orderDetailVO);
            orderDetailVO.setVendorId(vpVO.getVendorId());
        }

        //check to see if this comp order originated from the Communication screen
        //if so then call the overloaded buildFirstChoiceString method passing over
        //the new ship method, else call the other buildFirstChoiceString method
        //that pulls the ship method from the order details table
        if( bVenusOrder ) 
        {
          if (orderTO.getCompOrder() != null &&
              orderTO.getCompOrder().equalsIgnoreCase("C") &&
              orderTO.isFromCommunicationScreen()) 
          {
              venusMessageVO.setFirstChoice(this.buildFirstChoiceString(orderDetailVO,
                                                                        productVO,
                                                                        subcodeVO,
                                                                        orderTO.getShipMethod(),
                                                                        vpVO));
          } 
          else 
          {
              venusMessageVO.setFirstChoice(this.buildFirstChoiceString(orderDetailVO,
                                                                        productVO,
                                                                        subcodeVO,
                                                                        vpVO));
          }
        }
        else 
        {
          String sdsFirstChoice = buildFirstChoiceStringSDS(orderDetailVO, productVO, subcodeVO); 
          venusMessageVO.setFirstChoice(sdsFirstChoice);
        }

        // send message //
        VenusResultTO venusResultTO = sendOrder(venusMessageVO, queueOrder, orderDetailVO, 
                                                orderVO, customerVO, productVO, vpVO);

        return venusResultTO;
    }

    private void sendMessageToEscalate(VenusMessageVO venusMessageVO) throws Exception {
        TNMessageVO tnMessageVO = new TNMessageVO();
        tnMessageVO.setMsgType(venusMessageVO.getMsgType());
        tnMessageVO.setMsgPOConfNbr(venusMessageVO.getVenusOrderNumber());
        tnMessageVO.setMsgAdjAmount(venusMessageVO.getOverUnderCharge());
        tnMessageVO.setMsgOrigOrderAmt(venusMessageVO.getPrice());
        tnMessageVO.setMsgText(venusMessageVO.getComments());
        tnMessageVO.setMsgExportedFlag("0");
        tnMessageVO.setMsgReadFlag("0");
        tnMessageVO.setOrderProcessingFlag("0");
        tnMessageVO.setMsgWhoCreated("C/S");
        tnMessageVO.setMsgOrderVendorID(venusMessageVO.getFillingVendor());

        // update price
        if (tnMessageVO.getMsgType().equalsIgnoreCase("CAN")) {
            tnMessageVO.setMsgOrigOrderAmt(venusMessageVO.getPrice());
            tnMessageVO.setMsgAdjAmount(venusMessageVO.getPrice());
        } else if (tnMessageVO.getMsgType().equalsIgnoreCase("ADJ")) {
            tnMessageVO.setMsgOrigOrderAmt(venusMessageVO.getPrice());
            tnMessageVO.setMsgAdjAmount(venusMessageVO.getOverUnderCharge());
        }

        // update message type based on sub type
        if ((venusMessageVO.getSubType() != null) &&
            (!(venusMessageVO.getSubType().equalsIgnoreCase("C")))) {
            // if the subtype is not C, appent the subtype
            tnMessageVO.setMsgType(tnMessageVO.getMsgType() +
                                   venusMessageVO.getSubType());
        }
        // append I to ASK and ANS messages
        if (tnMessageVO.getMsgType().equalsIgnoreCase("ASK") ||
            tnMessageVO.getMsgType().equalsIgnoreCase("ANS")) {
            tnMessageVO.setMsgType(tnMessageVO.getMsgType() + "I");
        }
        // insert TN message //
        VenusDAO venusDAO = new VenusDAO(conn);

        //set flag so that Escalate will process message
        tnMessageVO.setMsgProcessedFlag("0");

        venusDAO.insertTNMessage(tnMessageVO);
    }

    /**
   * This method inserts a comments in the comment table.
   *
   * @param String comment
   * @param OrderDetailVO orderDetailVO
  */
    private void insertComment(String comment,
                               OrderDetailVO orderDetailVO) throws Exception {
        OrderDAO orderDAO = new OrderDAO(conn);
        CommentsVO commentsVO = new CommentsVO();
        commentsVO.setComment(comment);
        commentsVO.setCommentOrigin(VenusConstants.ORDER_PROCESSING_COMMENT_ORIGIN);
        commentsVO.setCommentType(VenusConstants.COMMENT_TYPE_ORDER);
        commentsVO.setCreatedBy(VenusConstants.ORDER_PROCESSING_COMMENT_ORIGIN);
        commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
        commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
        orderDAO.insertComment(commentsVO);
    }

    /**
   * This method inserts a queueVO into the queue.
   *
   * @param VenusMessageVO venusMessageVO
   * @param OrderDetailVO orderDetailVO
   * @param OrderVO orderVO
  */
    private void insertIntoQueue(VenusMessageVO venusMessageVO,
                                 OrderDetailVO orderDetailVO,
                                 OrderVO orderVO) throws VenusException,
                                                         Exception {
        QueueDAO queueDAO = new QueueDAO(conn);
        QueueVO queueVO = new QueueVO();
        queueVO.setQueueType(venusMessageVO.getMsgType());
        queueVO.setMessageType(venusMessageVO.getMsgType());
        queueVO.setSystem(VenusConstants.VENUS_SYSTEM);
        queueVO.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
        queueVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
        queueVO.setOrderGuid(orderDetailVO.getOrderGuid());
        queueVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        queueVO.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
        queueVO.setMessageTimestamp(new java.util.Date());
        queueDAO.insertQueueRecord(queueVO);
    }

    /**
     * This method will determine the type of the passed in message and then do
     * the appropriate processing.  This method will return a boolean flag to indicate
     * whether or not the message was queued.
   *
   */
    private

    VenusResultTO processMessage(VenusMessageVO venusMessageVO,
                                 boolean queueMessage,
                                 VenusResultTO venusResultTO,
                                 OrderDetailVO orderDetailVO,
                                 boolean transmitCancel) throws VenusException,
                                                                SAXException,
                                                                SQLException,
                                                                ParserConfigurationException,
                                                                IOException,
                                                                Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        logger.info("processMessage(VenusMessageVO,...) - VenusID: " + venusMessageVO.getVenusId());

        // Get the FTD message associated with this message
        ArrayList messageList =
            (ArrayList)venusDAO.getVenusMessage(venusMessageVO.getVenusOrderNumber(),
                                                "FTD");
        if (messageList.size() == 0) {
            throw new VenusException("Venus Message with venus order number " +
                                     venusMessageVO.getVenusOrderNumber() +
                                     " and order type FTD was not found." +
                                     DELIMITER);
        }
        VenusMessageVO ftdVenusMessageVO =
            (VenusMessageVO)messageList.get(0); // order numbers can only have one FTD message

        //move some fields over from the outgoing order to the message
        venusMessageVO.setOrderDate(ftdVenusMessageVO.getOrderDate());
        venusMessageVO.setRecipient(ftdVenusMessageVO.getRecipient());
        venusMessageVO.setAddress1(ftdVenusMessageVO.getAddress1());
        venusMessageVO.setAddress2(ftdVenusMessageVO.getAddress2());
        venusMessageVO.setCity(ftdVenusMessageVO.getCity());
        venusMessageVO.setState(ftdVenusMessageVO.getState());
        venusMessageVO.setZip(ftdVenusMessageVO.getZip());
        venusMessageVO.setShippingSystem(ftdVenusMessageVO.getShippingSystem());

        // Get the counts of the various types of message that are on this order
        MessageTypeCountsVO messageTypeCountsVO =
            venusDAO.getMessageTypeCounts(venusMessageVO.getReferenceNumber(),
                                          venusMessageVO.getVenusId());

        boolean bVenusMessage;
        if( StringUtils.equals(venusMessageVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS) ) {
            bVenusMessage = false;
        } else {
            bVenusMessage = true;
        }
        
        // Double check that order exists
        if (orderDetailVO == null) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Order Detail Record not found: " +
                                         venusMessageVO.getReferenceNumber());
        } 
        //can't send a CAN if non-SDS order doesn't have an ftd status of VERIFIED
        else if (bVenusMessage && !(ftdVenusMessageVO.getVenusStatus().equalsIgnoreCase("VERIFIED"))) {

            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Original order " +
                                         venusMessageVO.getVenusOrderNumber() +
                                         " not VERIFIED.");
        }
        //can't send a CAN if SDS order doesn't have an ftd status of VERIFIED or PENDING 
        else if (!bVenusMessage){
            if( !(ftdVenusMessageVO.getVenusStatus().equalsIgnoreCase("VERIFIED")) 
                && !(ftdVenusMessageVO.getVenusStatus().equalsIgnoreCase("PENDING")) ) {

            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Original SDS order " +
                                         venusMessageVO.getVenusOrderNumber() +
                                         " not VERIFIED.");
            }
        }
        // Do CAN processing
        if (venusResultTO.isSuccess() &&
            venusMessageVO.getMsgType().equalsIgnoreCase("CAN")) {
            venusMessageVO.setOldPrice(ftdVenusMessageVO.getPrice());
            venusMessageVO.setOverUnderCharge(ftdVenusMessageVO.getPrice());
            venusMessageVO.setPrice(ftdVenusMessageVO.getPrice());

            venusResultTO =
                    this.processOutboundCAN(venusMessageVO, messageTypeCountsVO,
                                            orderDetailVO, venusResultTO,
                                            transmitCancel, ftdVenusMessageVO);
        }
        //Do ADJ processing
        if (venusResultTO.isSuccess() &&
            venusMessageVO.getMsgType().equalsIgnoreCase("ADJ")) {
            this.processOutboundADJ(venusMessageVO, orderDetailVO,
                                    venusResultTO);
        }

        return venusResultTO;
    }

    /**
   * This method contains all the logic for processing an order.  It contains the
   * validation for the ship method, delivery date, and address information.
   *
   * @param VenusMessageVO vo
   * @param Boolean queueMessage
   * @param OrderDetailVO
   * @param OrderVO
   * @param CustomerVO
   * @return VenusResultTO
   * @throws VenusException, Exception
   */
    private

    VenusResultTO processOrder(VenusMessageVO venusMessageVO,
                               boolean queueMessage,
                               OrderDetailVO orderDetailVO, OrderVO orderVO,
                               CustomerVO customerVO,
                               ProductVO productVO) throws VenusException,
                                                           Exception {

        VenusResultTO venusResultTO = new VenusResultTO();
        logger.info("processOrder(VenusMessageVO,...) - OrderDetailID: " + orderDetailVO.getOrderDetailId());

        OrderDAO orderDAO = new OrderDAO(conn);
        VenusDAO venusDAO = new VenusDAO(conn);
        logger.debug("Get Source VO from Cache");
        com.ftd.op.order.vo.SourceVO sourceVO =
            orderDAO.getSource(orderDetailVO.getSourceCode());
        if (sourceVO == null) {
            throw new VenusException("No source record in cache for the given id: " +
                                     orderVO.getSourceCode() + DELIMITER);
        }
        CompanyVO companyVO = orderDAO.getCompany(sourceVO.getCompanyId());
        if (companyVO == null) {
            throw new VenusException("No company record for the given id: " +
                                     sourceVO.getCompanyId() + DELIMITER);
        }
        venusResultTO.setCustomerId((new Long(orderVO.getCustomerId()).toString()));
        venusResultTO.setOrderGuid(orderVO.getOrderGuid());
        venusResultTO.setOrderDetailId((new Long(orderDetailVO.getOrderDetailId())).toString());

        UpdateVO shipMethodUpdateVO = new UpdateVO();
        UpdateVO vendorUpdateVO = new UpdateVO();
        UpdateVO cardMessageUpdateVO = new UpdateVO();
        UpdateVO fillingFloristUpdateVO = new UpdateVO();

        boolean bVenusOrder = !StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS);
        boolean bWestOrder  = StringUtils.equals(productVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST);
              
        //Defect 2131 fix, if this came from the communication screen, we won't validate
        //the ship dates again.  This is already checked on the front end.
        if (!venusMessageVO.isFromCommunicationScreen()) {
            venusResultTO = this.validateShipDates(orderDetailVO, venusResultTO);
        }

        
        
        if( bVenusOrder ) {
            String origShipMethod = orderDetailVO.getShipMethod();
            Date origShipDate = orderDetailVO.getShipDate();
            if (!bWestOrder && venusResultTO.isSuccess()) {
                shipMethodUpdateVO = this.updateShipMethod(orderDetailVO, productVO, customerVO, venusResultTO, venusMessageVO);
                venusResultTO = shipMethodUpdateVO.getVenusResultTO();
                if (shipMethodUpdateVO.isUpdated()) {
                    orderDetailVO =
                            ((OrderDetailVO)shipMethodUpdateVO.getUpdatedObj());
                    venusMessageVO.setShipMethod(orderDetailVO.getShipMethod());
                    venusMessageVO.setShipDate(orderDetailVO.getShipDate());

                    //Put the original ship method back in the OrderDetailVO,
                    //The upgraded ship method should not be stored with the order: Defect#741
                    orderDetailVO.setShipMethod(origShipMethod);

                    //Issue 2170 - if coming from Communication Screen, preserve the ship date on the order details
                    if (venusMessageVO.isFromCommunicationScreen()) {
                        orderDetailVO.setShipDate(origShipDate);
                    }
                }
            }
            if (venusResultTO.isSuccess()) {
                vendorUpdateVO =
                        this.updateVendor(orderDetailVO, customerVO, productVO,
                                          venusMessageVO, venusResultTO);
                venusResultTO = vendorUpdateVO.getVenusResultTO();
                if (vendorUpdateVO.isUpdated())
                    venusMessageVO =
                            ((VenusMessageVO)vendorUpdateVO.getUpdatedObj());
            }
        }
        if (venusResultTO.isSuccess()) {
            cardMessageUpdateVO =
                    this.updateCardMessage(venusMessageVO, venusResultTO);
            venusResultTO = cardMessageUpdateVO.getVenusResultTO();
            if (cardMessageUpdateVO.isUpdated())
                venusMessageVO =
                        ((VenusMessageVO)cardMessageUpdateVO.getUpdatedObj());
        }

        // if success here from *** per DHL

        if (venusResultTO.isSuccess()) {
            if( bVenusOrder ) {
                if (!(venusMessageVO.getFillingVendor().equalsIgnoreCase(orderDetailVO.getFloristId()))) {
                    fillingFloristUpdateVO = new UpdateVO();
                    orderDetailVO.setFloristId(venusMessageVO.getFillingVendor());
                    orderDetailVO.setVendorId(venusMessageVO.getVendorId());
                    fillingFloristUpdateVO.setUpdated(true);

                    //update table indicating the a vendor was assigned to order
                    orderDAO.insertOrderFloristUsed(orderDetailVO,
                            venusMessageVO.getFillingVendor(), "Venus");

                    //*insert comment *//
                }
                // if wine.com , get the wine price //
                if ((venusMessageVO.getFillingVendor() != null) &&
                    (venusMessageVO.getFillingVendor().equalsIgnoreCase(this.WINE_MEMBER_ID))) {
                    venusMessageVO.setPrice(this.getWinePrice(orderDetailVO,
                                                              productVO));
                } else if ((venusMessageVO.getFillingVendor() != null) &&
                           (venusMessageVO.getFillingVendor().equalsIgnoreCase(this.SAMS_MEMBER_ID))) {
                    venusMessageVO.setPrice(this.getSamsPrice(orderDetailVO,
                                                              productVO));
                }

                if (shipMethodUpdateVO.isUpdated() ||
                    fillingFloristUpdateVO.isUpdated()) {
                  orderDAO.updateOrder(orderDetailVO);
                }
            }

            if (vendorUpdateVO.isUpdated() ||
                cardMessageUpdateVO.isUpdated()) {
                venusDAO.updateVenus(venusMessageVO);
            }

            if( bVenusOrder && !bWestOrder) {
                // *** if success moved up per DHL

                // decrement inventory //
                boolean invLevelHit = false;

                //if order has subcodes use the subcode
                String inventoryProductId = "";
                if (orderDetailVO.getSubcode() != null &&
                    orderDetailVO.getSubcode().length() > 0) {
                    inventoryProductId = orderDetailVO.getSubcode();
                } else {
                    inventoryProductId = orderDetailVO.getProductId();
                }

                invLevelHit =
                        venusDAO.decrementProductInventory(inventoryProductId,
                                                           orderDetailVO.getVendorId(),
                                                           1);

                if (invLevelHit) {
                    sendInventoryNotifications(inventoryProductId,
                                               orderDetailVO.getVendorId());
                }
            }
        }

        return venusResultTO;
    }

    private void sendInventoryNotifications(String productId,
                                            String vendorId) throws Exception {


        logger.debug("Inventory level hit for product " + productId);

        try {

            VenusDAO venusDAO = new VenusDAO(conn);

            //retrieve list of email addresses to send notifcation to
            List notifyList =
                venusDAO.getInventoryNotifications(productId, vendorId);

            //retrieve inventory data for product
            Document xml =
                venusDAO.getInventoryRecordXML(productId, vendorId);

            logger.debug(convertDocToString(xml));

            //if list is not null
            if (notifyList != null) {

                StockMessageGenerator messageGenerator =
                    new StockMessageGenerator();

                //get the sender email address from config file
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                String fromEmailAddress =
                    configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                           "INVENTORY_EMAIL_FROM_ADDRESS");
                String inventoryEmailTemplate =
                    configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,
                                           "INVENTORY_EMAIL_TEMPLATE_ID");
                
               String  myBuysFeedEnabled  = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");
    
                //create a point of contact VO
                PointOfContactVO pocVO = new PointOfContactVO();
                pocVO.setCompanyId("FTD");
                pocVO.setSenderEmailAddress(fromEmailAddress);
                pocVO.setTemplateId(null);
                pocVO.setLetterTitle(inventoryEmailTemplate);
                pocVO.setPointOfContactType("Email");
                pocVO.setCommentType("Order");

                pocVO.setDataDocument(xml);

                //for each email in the list
                Iterator iter = notifyList.iterator();
                while (iter.hasNext()) {
                    ProductNotificationVO notifyVO =
                        (ProductNotificationVO)iter.next();

                    //set email address
                    pocVO.setRecipientEmailAddress(notifyVO.getEmailAddress());

                    logger.debug("Email being sent to " +
                                 notifyVO.getEmailAddress());

                    messageGenerator.processMessage(pocVO);
                }
                
                if  (StringUtils.equalsIgnoreCase(myBuysFeedEnabled,"Y"))
                {
                    logger.debug("Calling sendMyBuys");
                    try{
                      sendMyBuys(productId);
                      logger.debug("Succesfull in sending MyBuys message() for order_processing for product id: "+ productId);
                    } catch (Exception e)
                    {
                       logger.error("Error in sending MyBuys message() for product id"+ e);
                       CommonUtils.sendSystemMessage("Error sending inventory level MyBuys." +  e.getMessage());
                    }
                 }

            } else {
                logger.debug("Nobody is on notification list");
            }
        } catch (Throwable t) {
            //catch the exception, but do not rethrow it.  That way this error will
            //be logged and paged out on, but the processing of venus orders will
            //not be affected.
            logger.error("Error sending inventory level notification email.");
            logger.error(t);
            CommonUtils.sendSystemMessage("Error sending inventory level notification email." +
                                          t.toString());
        }


    }


    /**
   * This is helper method for processOrder.  It validates that the order detail
   * shipping and delivery date do not fall within vendor blocked periods.
   *
   * @param OrderDetailVO
   * @param ProductVO
   * @param VenusResultTO
   * @return VenusResultTO
   */
    private VenusResultTO validateShipDates(OrderDetailVO orderDetailVO,
                                            VenusResultTO venusResultTO) throws Exception {
        logger.debug("Entering validateShipDates method to check ship dates and block dates.");

        OrderDAO orderDAO = new OrderDAO(conn);
        VendorBlockVO shippingBlockVO =
            orderDAO.getVendorShippingBlock(orderDetailVO.getVendorId());

        if (shippingBlockVO != null) {
            if ((shippingBlockVO.getStartDate().before(orderDetailVO.getShipDate()) ||
                 shippingBlockVO.getStartDate().equals(orderDetailVO.getShipDate())) &&
                (shippingBlockVO.getEndDate().after(orderDetailVO.getShipDate()) ||
                 shippingBlockVO.getEndDate().equals(orderDetailVO.getShipDate()))) {
                venusResultTO.setSuccess(false);
                venusResultTO.setErrorString("Invalid ship date. Vendor blocked.");
            }
        }

        if (venusResultTO.isSuccess()) {
            VendorBlockVO deliveryBlockVO =
                orderDAO.getVendorDeliveryBlock(orderDetailVO.getVendorId());
            if (deliveryBlockVO != null) {
                if ((deliveryBlockVO.getStartDate().before(orderDetailVO.getShipDate()) ||
                     deliveryBlockVO.getStartDate().equals(orderDetailVO.getShipDate())) &&
                    (deliveryBlockVO.getEndDate().after(orderDetailVO.getShipDate()) ||
                     deliveryBlockVO.getEndDate().equals(orderDetailVO.getShipDate()))) {
                    venusResultTO.setSuccess(false);
                    venusResultTO.setErrorString("Invalid delivery date. Vendor blocked.");
                }
            }
        }

        if (venusResultTO.isSuccess()) {
            // Fail if ship date has passed
            if (removeTime(new Date()).after(removeTime(orderDetailVO.getShipDate()))) {
                venusResultTO.setSuccess(false);
                venusResultTO.setErrorString("Ship date has passed for the Venus order.");
            }
        }

        return venusResultTO;
    }

    public String buildFirstChoiceString(OrderDetailVO orderDetailVO,
                                         ProductVO productVO,
                                         ProductSubCodeVO subcodeVO,
                                         VendorProductVO vpVO) throws VenusException,
                                                                      Exception {
        String firstChoiceString = "";
        if ((orderDetailVO == null) ||
            (orderDetailVO.getShipMethod() == null)) {
            throw new VenusException("No Ship Method in VenusService.buildFirstChoiceString" +
                                     DELIMITER);
        }

        if (((productVO == null) ||
             (productVO.getMercuryDescription() == null)) &&
            ((subcodeVO == null) ||
             (subcodeVO.getSubCodeRefNumber() == null))) {
            logger.error("No product description or subcode reference number for " +
                         orderDetailVO.getProductId());
            CommonUtils.sendSystemMessage("No product description or subcode reference number for " +
                                          orderDetailVO.getProductId());
        }

        //check if this is a subcode
        if (subcodeVO == null) {
            firstChoiceString =
                    "." + productVO.getFloristReferenceNumber() + orderDetailVO.getShipMethod() +
                    " " + productVO.getMercuryDescription();
        } else {
            firstChoiceString =
                    "." + vpVO.getVendorSku() + orderDetailVO.getShipMethod() +
                    " " + subcodeVO.getSubCodeRefNumber();
        }

        return firstChoiceString;
    }

    public String buildFirstChoiceString(OrderDetailVO orderDetailVO,
                                         ProductVO productVO,
                                         ProductSubCodeVO subcodeVO,
                                         String shipMethod,
                                         VendorProductVO vpVO) throws VenusException,
                                                                      Exception {
        String firstChoiceString = "";
        if ((orderDetailVO == null) ||
            (orderDetailVO.getShipMethod() == null)) {
            throw new VenusException("No Ship Method in VenusService.buildFirstChoiceString" +
                                     DELIMITER);
        }

        if (((productVO == null) ||
             (productVO.getMercuryDescription() == null)) &&
            ((subcodeVO == null) ||
             (subcodeVO.getSubCodeRefNumber() == null))) {
            logger.error("No product description or subcode reference number for " +
                         orderDetailVO.getProductId());
            CommonUtils.sendSystemMessage("No product description or subcode reference number for " +
                                          orderDetailVO.getProductId());
        }

        //check if this is a subcode
        if (subcodeVO == null) {
            firstChoiceString =
                    "." + productVO.getFloristReferenceNumber() + shipMethod +
                    " " + productVO.getMercuryDescription();
        } else {
            firstChoiceString =
                    "." + vpVO.getVendorSku() + shipMethod + " " + subcodeVO.getSubCodeRefNumber();
        }

        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        ArrayList addonList = orderDetailVO.getAddOnList();
        for (int i=0; i<addonList.size(); i++) {
            String description = "";
            AddOnVO addon = (AddOnVO) addonList.get(i);
            if (addon.getAddOnCode() != null && addon.getAddOnCode().startsWith("RC")) {
                description = addon.getAddOnCode() + " - " + addon.getDesciption();
            } else {
                description = addon.getDesciption();
            }
            firstChoiceString = firstChoiceString + "\n" + "PRICE INCLUDES " +
                addon.getAddOnQuantity() + " " + description + "($" +
                format.format(addon.getAddOnQuantity() * addon.getPrice().doubleValue()) + ")";
        }

        return firstChoiceString;
    }

    public String buildCompVendorFirstChoiceString(OrderDetailVO orderDetailVO,
                                                   VendorVO vendorVO,
                                                   ProductVO productVO,
                                                   ProductSubCodeVO subcodeVO,
                                                   VendorProductVO vpVO) throws VenusException,
                                                                                Exception {
        String firstChoiceString = "";
        if ((orderDetailVO == null) ||
            (orderDetailVO.getShipMethod() == null)) {
            throw new VenusException("No Ship Method in VenusService.buildFirstChoiceString" +
                                     DELIMITER);
        }
        if ((vendorVO == null) || (vendorVO.getVendorMemberNumber() == null)) {
            throw new VenusException("No Vendor Member Number in VenusService.buildFirstChoiceString" +
                                     DELIMITER);
        }
        if (((productVO == null) ||
             (productVO.getMercuryDescription() == null)) &&
            ((subcodeVO == null) ||
             (subcodeVO.getSubCodeRefNumber() == null))) {
            logger.error("No product description or subcode reference number for " +
                         orderDetailVO.getProductId());
            CommonUtils.sendSystemMessage("No product description or subcode reference number for " +
                                          orderDetailVO.getProductId());
        }

        //check if this is a subcode
        if (subcodeVO == null) {
            firstChoiceString =
                    "." + productVO.getFloristReferenceNumber() + " " +
                    productVO.getMercuryDescription();
        } else {
            firstChoiceString =
                    "." + vpVO.getVendorSku() + " " + subcodeVO.getSubCodeRefNumber();
        }


        return firstChoiceString;
    }

    public String buildFirstChoiceStringSDS(OrderDetailVO orderDetailVO,
                                                   ProductVO productVO,
                                                   ProductSubCodeVO subcodeVO) throws VenusException,
                                                                                Exception {
        String sdsFirstChoice = "."+orderDetailVO.getShipMethod(); 
        if( subcodeVO==null ) 
        {
          sdsFirstChoice += productVO.getFloristReferenceNumber() + " " + productVO.getMercuryDescription();
        } 
        else 
        {
          sdsFirstChoice += subcodeVO.getProductSubCodeId() + " " + subcodeVO.getSubCodeRefNumber();
        }

        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        ArrayList addonList = orderDetailVO.getAddOnList();
        for (int i=0; i<addonList.size(); i++) {
            String description = "";
            AddOnVO addon = (AddOnVO) addonList.get(i);
            if (addon.getAddOnCode() != null && addon.getAddOnCode().startsWith("RC")) {
                description = addon.getAddOnCode() + " - " + addon.getDesciption();
            } else {
                description = addon.getDesciption();
            }
            sdsFirstChoice = sdsFirstChoice + "\n" + "PRICE INCLUDES " +
                addon.getAddOnQuantity() + " " + description + "($" +
                format.format(addon.getAddOnQuantity() * addon.getPrice().doubleValue()) + ")";
        }

        return sdsFirstChoice;
    }
    
    /**
   * This is a helper method for processOrder. This method updates the shipMethod
   * in the parameter orderDetailVO and passes it back within the UpdateVO.
   *
   * @param OrderDetailVO
   * @param ProductVO
   * @param CustomerVO
   * @param VenusResultTO
   * @return UpdateVO
   * @throw VenusException
   */
    private UpdateVO updateShipMethod(OrderDetailVO orderDetailVO,
                                      ProductVO productVO,
                                      CustomerVO customerVO,
                                      VenusResultTO venusResultTO,
                                      VenusMessageVO vMessageVO) throws VenusException,
                                                                          Exception {
        UpdateVO shipMethodUpdateVO = new UpdateVO();
        String shipMethod = null;
        if (vMessageVO.isFromCommunicationScreen()) {
            shipMethod = vMessageVO.getShipMethod();
        } else {
            shipMethod = orderDetailVO.getShipMethod();
        }

        if ((shipMethod == null) || shipMethod.equalsIgnoreCase("")) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("The order detail record (#" +
                                         orderDetailVO.getOrderDetailId() +
                                         ") is missing the ship method.");
        } else if (shipMethod.equalsIgnoreCase("SA")) {
            /* SA is not a valid FEDEX delivery method.  The ship method has to be converted
       * into something FedEx can handle.  If it can't be converted, the order is
       * placed in a queue.*/
            VenusCommonUtil venusCommonUtil = new VenusCommonUtil(conn);

            String upgradedShipMethod = null;
            if (vMessageVO.isFromCommunicationScreen()) {
                logger.debug("Saturday Delivery Update - using the data from Communication Screen");
                upgradedShipMethod =
                        venusCommonUtil.upgradeSAShipping(productVO.getProductType(),
                                                          vMessageVO.getDeliveryDate(),
                                                          customerVO,
                                                          orderDetailVO.getProductId());
            } else {
                logger.debug("Saturday Delivery Update - using the data from ORDER_DETAILS table");
                upgradedShipMethod =
                venusCommonUtil.upgradeSAShipping(productVO.getProductType(),
                                                  orderDetailVO.getDeliveryDate(),
                                                  customerVO,
                                                  orderDetailVO.getProductId());
          }

            if (upgradedShipMethod != null) {
                // if the upgradeShipMethod is PO, than make sure that PO is a valid ship method //
                if (upgradedShipMethod.equalsIgnoreCase("PO")) {
                    boolean isValidFedExShipMethod =
                        this.isValidFedExShipMethod(customerVO.getZipCode(),
                                                    customerVO.getState(),
                                                    upgradedShipMethod, false);
                    if (!isValidFedExShipMethod) {
                        venusResultTO.setSuccess(false);
                        venusResultTO.setErrorString("Delivery method is not valid.");
                    }
               }

                if (venusResultTO.isSuccess()) {
                    //Now recalculate the ship date
                    Calendar shipDate = Calendar.getInstance();

                    if (vMessageVO.isFromCommunicationScreen()) {
                        shipDate.setTime(vMessageVO.getDeliveryDate());
                    } else {
                    shipDate.setTime(orderDetailVO.getDeliveryDate());
                    }

                    if (upgradedShipMethod.equals("PO")) {
                        shipDate.add(Calendar.DATE, -1);
                    } else { //Only two options are "PO" or "2F"
                        shipDate.add(Calendar.DATE, -2);
                    }

                    if (logger.isDebugEnabled()) {
                        String shipDateStr =
                            "Ship date is " + (shipDate.get(Calendar.MONTH) +
                                               1) + "/" +
                            shipDate.get(Calendar.DAY_OF_MONTH) + "/" +
                            shipDate.get(Calendar.YEAR);
                        logger.debug(shipDateStr);
                    }

                    Calendar today = Calendar.getInstance();
                    today.setTime(CommonUtils.clearTime(new Date()));

                    if (shipDate.before(today)) {
                        venusResultTO.setSuccess(false);
                        venusResultTO.setErrorString("Cannot upgrade SA ship method.  Updated ship date < today");
                    } else {
                        orderDetailVO.setShipMethod(upgradedShipMethod);
                        orderDetailVO.setShipDate(shipDate.getTime());
                        shipMethodUpdateVO.setUpdated(true);
                        shipMethodUpdateVO.setUpdatedObj(orderDetailVO);
                    }
                }
            } else {
                venusResultTO.setSuccess(false);
                venusResultTO.setErrorString("Cannot upgrade SA ship method.");
            }
        } else if (shipMethod.equalsIgnoreCase("ND")) {

            //if next day is not available attempt to upgrade
            boolean isValidFedExShipMethod =
                this.isValidFedExShipMethod(customerVO.getZipCode(),
                                            customerVO.getState(), "ND",
                                            false);
            if (!isValidFedExShipMethod) {
                //check if PO is valid
                isValidFedExShipMethod =
                        this.isValidFedExShipMethod(customerVO.getZipCode(),
                                                    customerVO.getState(),
                                                    "PO", false);
                if (isValidFedExShipMethod) {
                    orderDetailVO.setShipMethod("PO");
                    shipMethodUpdateVO.setUpdated(true);
                    shipMethodUpdateVO.setUpdatedObj(orderDetailVO);
                } else {
                    venusResultTO.setSuccess(false);
                    venusResultTO.setErrorString("Delivery method is not valid");
                }
            }

        }


        shipMethodUpdateVO.setVenusResultTO(venusResultTO);
        return shipMethodUpdateVO;
    }

    /**
   * This is a helper method for processOrder.  It updates the vendor information
   * in the venusMessageVO and passes back the venusMessageVO within the UpdateVO.
   *
   * @param OrderDetailVO
   * @param CustomerVO
   * @param ProductVO
   * @param VenusMessageVO
   * @param VenusResultsTO
   * @return UpdateVO
   */
    private UpdateVO updateVendor(OrderDetailVO orderDetailVO,
                                  CustomerVO customerVO, ProductVO productVO,
                                  VenusMessageVO venusMessageVO,
                                  VenusResultTO venusResultTO) throws VenusException,
                                                                      Exception {
        UpdateVO vendorUpdateVO = new UpdateVO();
        OrderDAO orderDAO = new OrderDAO(conn);
        //TODO:  has the vendor been assigned yet?
        VendorVO vendorVO = orderDAO.getVendor(orderDetailVO.getVendorId());
        if (vendorVO == null) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Invalid Vendor On Product.  Product: " +
                                         productVO.getProductId());
        }
        if (vendorVO.getVendorMemberNumber() == null) {
            throw new VenusException("No Vendor Member Number on Vendor with vendor id " +
                                     vendorVO.getVendorId() + DELIMITER);
        } else if (!vendorVO.getVendorMemberNumber().equalsIgnoreCase(venusMessageVO.getFillingVendor())) {
            venusMessageVO.setFillingVendor(vendorVO.getVendorMemberNumber());
            /* add the insert to the comments table */
            vendorUpdateVO.setUpdated(true);
            vendorUpdateVO.setUpdatedObj(venusMessageVO);
        }
        vendorUpdateVO.setVenusResultTO(venusResultTO);
        return vendorUpdateVO;
    }

    /**
   * This is a helper method for processOrder.It updates the card message information
   * in the venusMessageVO and passes it back within the UpdateVO.
  *
   * @param VenusMessageVO venusMessageVO
   * @param VenusResultTO venusResultTO
   * @return UpdateVO
   */
    private UpdateVO updateCardMessage(VenusMessageVO venusMessageVO,
                                       VenusResultTO venusResultTO) {
        UpdateVO cardMessageUpdateVO = new UpdateVO();
        if ((venusMessageVO.getCardMessage() == null) ||
            (venusMessageVO.getCardMessage().equals(""))) {
            venusMessageVO.setCardMessage("Enjoy your gift!");
            cardMessageUpdateVO.setUpdated(true);
            cardMessageUpdateVO.setUpdatedObj(venusMessageVO);
        }
        cardMessageUpdateVO.setVenusResultTO(venusResultTO);
        return cardMessageUpdateVO;
    }

    /**
   * This is a helper method for processMessage.  It verifies the CAN message.
   *
   * @param VenusMessageVO venusMessageVO
   * @param MessageTypeCountsVO messageTypeCountsVO
   * @param OrderDetailVO orderDetailVO
   * @param VenusResultTO venusResultTO
   * @param 
   * @return VenusResultTO
   * @throws Exception
   */
    private

    VenusResultTO processOutboundCAN(VenusMessageVO venusMessageVO,
                                     MessageTypeCountsVO messageTypeCountsVO,
                                     OrderDetailVO orderDetailVO,
                                     VenusResultTO venusResultTO,
                                     boolean transmitCancel,
                                     VenusMessageVO ftdVenusMessageVO) throws Exception {
        // If an order is cancelled for Zone Jump prior to the vendor printing the label,
        //the system will remove that box from the pallet count so the space can be used for another package.
        //Defect 4389 - Don't remove Zone Jump Orders after Zone Jump label date
        Calendar cTodaysDate = Calendar.getInstance();
        //set today's date time to 00:00:00 too
        cTodaysDate.set(Calendar.HOUR, 0); 
        cTodaysDate.set(Calendar.MINUTE, 0); 
        cTodaysDate.set(Calendar.SECOND, 0); 
        cTodaysDate.set(Calendar.MILLISECOND, 0);
        cTodaysDate.set( Calendar.AM_PM, Calendar.AM );
        
        //Create a Calendar Object
        Calendar zjDate = Calendar.getInstance();
        //Define the format
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String zjLabelDate = null;
        
        if(ftdVenusMessageVO.getZoneJumpLabelDate() != null)
            zjLabelDate = ftdVenusMessageVO.getZoneJumpLabelDate().toString();
        if(zjLabelDate != null)
        {
            zjLabelDate = zjLabelDate.substring(5,7) + "/" +zjLabelDate.substring(8,10) + "/" + zjLabelDate.substring(0,4);      
            zjDate.setTime(sdf.parse(zjLabelDate));
        }
        
        if(ftdVenusMessageVO.isZoneJumpFlag() 
           && !ftdVenusMessageVO.getSdsStatus().equalsIgnoreCase(VenusConstants.PRINTED_DISPOSITION)
           && (getDiffInDays(cTodaysDate.getTime(), zjDate.getTime()) >= 0)) 
        {
            VenusDAO vDAO = new VenusDAO(this.conn);
            vDAO.removeOrderFromVendor(ftdVenusMessageVO.getVenusId());
        }
        // if order contains a cancel and has not been printed
        if ((messageTypeCountsVO.getCanCount() > 0) &&
            (messageTypeCountsVO.getPrintedCount() == 0)) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Cancel request previously sent for this order. Cannot cancel order.");
        } else if (messageTypeCountsVO.getConCount() > 0) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Vendor already confirmed cancel. Cannot cancel order.");
        } else if (messageTypeCountsVO.getRejCount() > 0) {
            venusResultTO.setSuccess(false);
            venusResultTO.setErrorString("Vendor already rejected order. Cannot cancel order.");
        } else if (!transmitCancel) {
            this.insertComment("Cancel message will not be sent to Venus because order already shipped.  System generated ADJ will go to Venus.",
                               orderDetailVO);
        } else if (venusMessageVO.getShipDate() != null) {
            //if the ship date is in the current month
            SimpleDateFormat formatter = new SimpleDateFormat("MM yyyy");
            if (formatter.format(venusMessageVO.getShipDate()).equalsIgnoreCase(formatter.format(new Date()))) {
                this.insertComment("Cancel message will appear as ADJ on Venus.",
                                   orderDetailVO);
                //convert CAN to ADJ message
                venusResultTO.setTreatAsAdjustment(true);
            }
        }
        
        //Only change inventory numbers if the order is not an SDS order.
        //Ship processing handles the inventory for SDS orders.
        if (venusResultTO.isSuccess() && !StringUtils.equals(venusMessageVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS)
                && !StringUtils.equals(venusMessageVO.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST)) 
        {            
            this.insertComment("Order cancelled.", orderDetailVO);
            // increment inventory //
            VenusDAO venusDAO = new VenusDAO(conn);

            //if order has subcodes use the subcode
            if (orderDetailVO.getSubcode() != null &&
                orderDetailVO.getSubcode().length() > 0) {
                venusDAO.incrementProductInventory(orderDetailVO.getSubcode(),
                                                   orderDetailVO.getVendorId(),
                                                   1);
            } else {
                venusDAO.incrementProductInventory(orderDetailVO.getProductId(),
                                                   orderDetailVO.getVendorId(),
                                                   1);
            }

        }
        return venusResultTO;
    }

    /**
   * This is a helper method for processMessage.  It verifies the ADJ message.
   *
   * @param VenusMessageVO venusMessageVO
   * @param OrderDetailVO orderDetailVO
   * @param VenusResultTO venusResultTO
   * @return VenusResultTO
   * @throws Exception
   */
    private

    void processOutboundADJ(VenusMessageVO venusMessageVO,
                            OrderDetailVO orderDetailVO,
                            VenusResultTO venusResultTO) throws Exception {
        if (venusResultTO.isTreatAsAdjustment()) {
            this.insertComment("Adjustment will appear as cancel to customer service.",
                               orderDetailVO);
        } else {
            this.insertComment("Original order amount: " +
                               venusMessageVO.getPrice() +
                               "  Adjusted order amount: " +
                               venusMessageVO.getOverUnderCharge(),
                               orderDetailVO);
        }
    }

    /**
   * This method will be used to determine if the specified FedEx ship method
   * can be used for the given zip code.
   * @throws java.lang.Exception
   * @return boolean isValidFedExShipMethod
   * @param nextDayDeliveryRequired
   * @param String shipMethod
   * @param String state
   * @param String zip
   */
    private boolean isValidFedExShipMethod(String zip, String state, String shipMethod,
                                   boolean nextDayDeliveryRequired) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        // get the current date of the week -MON, TUE, WED, THR, FRI, SAT, SUN//
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");
        String day = dayFormat.format(new Date()).toUpperCase();
        FedExDeliveryConfVO fedExDeliveryConfVO =
            venusDAO.getFedExDeliveryConfVO(zip, shipMethod, day);
        if (fedExDeliveryConfVO != null) {
            if (fedExDeliveryConfVO.getDeliveryAvailable().equalsIgnoreCase("N")) {
                return false;
            } else {
                if ((nextDayDeliveryRequired) &&
                    (fedExDeliveryConfVO.getDeliveryBusinessDays() > 1)) {
                    return false;
                } else if (state.equalsIgnoreCase("HI")) {
                    if (fedExDeliveryConfVO.getAddOneBusinessDayForHi().equalsIgnoreCase("Y")) {
                        return false;
                    } else if (fedExDeliveryConfVO.getDayOfWeek().equalsIgnoreCase("SAT") &&
                               fedExDeliveryConfVO.getSatHiDeliveryAvailable().equalsIgnoreCase("N")) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
     }

   //This method check all the payment records on an order to determine if it's a comp order or not.
   private boolean isCompOrder(String orderDetailId) throws Exception
   {
        VenusDAO venusDAO = new VenusDAO(this.conn);

        boolean compOrder = false;
        List paymentList = venusDAO.getPayment(orderDetailId);
        if(paymentList != null)
        {
            Iterator iter = paymentList.iterator();
            while (iter.hasNext() && !compOrder) {
                PaymentVO payment = (PaymentVO)iter.next();
                if (payment.getAdditionalBillId() == null &&
                    payment.getPaymentType() != null &&
                    payment.getPaymentType().equals("NC")) {
                    compOrder = true;
                } //end if
            } //end while loop
        } //end payment list null

        return compOrder;
    }


    public Double getWinePrice(OrderDetailVO orderDetailVO,
                               ProductVO productVO) throws Exception {
        logger.debug("Wine.com order:" + orderDetailVO.getOrderDetailId());

        OrderDAO orderDAO = new OrderDAO(conn);
        double price = 0;
        // get VOs //
        OrderBillVO orderBillVO =
            orderDAO.getOrderBill(new Long(orderDetailVO.getOrderDetailId()).toString());
        if (orderBillVO == null) {
            throw new VenusException("No order bill record for the given order detail id: " +
                                     orderDetailVO.getOrderDetailId() +
                                     DELIMITER);
        }
        ArrayList orderAddOns = orderDetailVO.getAddOnList();
        // calculate price //
        if ((orderDetailVO.getSizeIndicator() == null) ||
            (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A"))) {
            price = productVO.getStandardPrice();
        } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) {
            price = productVO.getDeluxePrice();
        } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) {
            price = productVO.getPremiumPrice();
        }

        // add ons //
        AddOnVO addOnVO;
       for (int i = 0; i < orderAddOns.size(); i++) {
            addOnVO = (AddOnVO)orderAddOns.get(i);
            price =
                    price + (addOnVO.getPrice().doubleValue() * addOnVO.getAddOnQuantity());
        }
        price =
                price - orderBillVO.getDiscountAmount() + orderBillVO.getServiceFee() +
                orderBillVO.getShippingFee() + orderBillVO.getTax();
        BigDecimal truncatedPrice =
            new BigDecimal(price).setScale(2, BigDecimal.ROUND_HALF_UP);
        return (new Double(truncatedPrice.doubleValue()));
    }

    public Double getSamsPrice(OrderDetailVO orderDetailVO,
                               ProductVO productVO) throws Exception {
        logger.debug("Sams order:" + orderDetailVO.getOrderDetailId());

        OrderDAO orderDAO = new OrderDAO(conn);
        double price = 0;
        // get VOs //
        OrderBillVO orderBillVO =
            orderDAO.getOrderBill(new Long(orderDetailVO.getOrderDetailId()).toString());
        if (orderBillVO == null) {
            throw new VenusException("No order bill record for the given order detail id: " +
                                     orderDetailVO.getOrderDetailId() +
                                     DELIMITER);
        }

        // calculate price //
        if ((orderDetailVO.getSizeIndicator() == null) ||
            (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A"))) {
            price = productVO.getStandardPrice();
        } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) {
            price = productVO.getDeluxePrice();
        } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) {
            price = productVO.getPremiumPrice();
        }


        price = price - orderBillVO.getDiscountAmount();
        BigDecimal truncatedPrice =
            new BigDecimal(price).setScale(2, BigDecimal.ROUND_HALF_UP);
        return (new Double(truncatedPrice.doubleValue()));
    }

    private void addElement(Document xml, String keyName, String value) {
        Element nameElement = (Element)xml.createElement(keyName);

        if (value != null && value.length() > 0) {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);
    }

    //converts XML document to a string

    public static String convertDocToString(Document xml) throws Exception {
        StringWriter sw = new StringWriter();
        DOMUtil.print(xml, new PrintWriter(sw));
        return sw.toString(); //string representation of xml document
    }

    private void sendShipProcessMessage(String venusId, String productId) throws Exception
    {
        logger.debug("Sending venus id:" + venusId + " ship processing...");

        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(venusId);
        //check global parm to see if we have fully converted over to new ship processing instance
        //if fully converted is yes just go to new ship processing
        //if not fully converted over yet then check global parm to see if we need to evaluate which ship processing instance we can send the order to
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String  fullyConvertedToNewShipProcessing  = configUtil.getFrpGlobalParm("SHIPPING_PARMS", "FULLY_CONVERTED");
        logger.debug("fullyConvertedToNewShipProcessing: " + fullyConvertedToNewShipProcessing);
        String shipProcessingQueueName = null;
        if(fullyConvertedToNewShipProcessing.equalsIgnoreCase("Y"))
        {
            shipProcessingQueueName = "SHIPPROCESSSHIP";
            VenusDAO venusDAO = new VenusDAO(conn);
            venusDAO.updateNewShippingSystemFlag(venusId);
        }
        else
        {
             String  newShippingSystem  = configUtil.getFrpGlobalParm("SHIPPING_PARMS", "NEW_SHIPPING_SYSTEM");
             logger.debug("newShippingSystem: " + newShippingSystem);
             if(newShippingSystem.equalsIgnoreCase("Y"))
             {
                 VenusDAO dao = new VenusDAO(conn);   
                 logger.debug("productId: " + productId);
                 String sendToNewShippingSystem = dao.sendToNewShippingSystem(productId);
                 logger.debug("sendToNewShippingSystem: " + sendToNewShippingSystem);
                 if(sendToNewShippingSystem.equalsIgnoreCase("Y"))
                 {
                 shipProcessingQueueName = "SHIPPROCESSSHIP";
                  VenusDAO venusDAO = new VenusDAO(conn);
                  venusDAO.updateNewShippingSystemFlag(venusId);
                 }
                 else
                 {
                   shipProcessingQueueName = "PROCESSSHIP";
                 }
             }
             else
             {
                   shipProcessingQueueName = "PROCESSSHIP";
             }
             
        }   
        token.setStatus(shipProcessingQueueName);
        token.setJMSCorrelationID(venusId);
        //Per Jason Weiss on 9/26/2006, put in a 5 second delay
        token.setProperty("JMS_OracleDelay", String.valueOf(5),"int");
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
    }

    private long getNumberOfDaysDifference(Date earlierDate, Date laterDate) {
        long daysToDeliver;

        // Do the calc - 86400000 milliseconds in a day //
        daysToDeliver =
                (CommonUtils.clearTime(laterDate).getTime() - CommonUtils.clearTime(earlierDate).getTime()) /
                86400000;
        return daysToDeliver;
    }

    /** This function removes the time values from a Date.
    * @param Date input date
    * @returns Date date without time*/
    public static Date removeTime(Date inDate) {
        Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(inDate);

        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.AM_PM, Calendar.AM);

        return cal.getTime();
    }


    public void testUpdateShipMethod(Connection conn) {
        //VenusResultTO validateShipDates(OrderDetailVO orderDetailVO, ProductVO productVO, VenusResultTO venusResultTO)
        try {
            VenusResultTO venusResultTO = new VenusResultTO();
            OrderDAO orderDAO = new OrderDAO(conn);
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail("1501495");
            ProductVO productVO =
                orderDAO.getProduct(orderDetailVO.getProductId());
            OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
            CustomerVO customerVO =
                orderDAO.getCustomer(orderVO.getCustomerId());
            VenusMessageVO vMessageVO = new VenusMessageVO();

            UpdateVO updateVO =
                updateShipMethod(orderDetailVO, productVO, customerVO,
                                 venusResultTO, vMessageVO);

            System.out.println(updateVO.isUpdated());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //VenusResultTO processOrder(VenusMessageVO venusMessageVO, boolean queueMessage, OrderDetailVO orderDetailVO, OrderVO orderVO, CustomerVO customerVO, ProductVO productVO)

    public void testProcessOrder(Connection conn) {
        try {
            OrderDAO orderDAO = new OrderDAO(conn);
            VenusDAO venusDAO = new VenusDAO(conn);
            VenusMessageVO venusMessageVO =
                venusDAO.getVenusMessageById("72015");

            VenusResultTO venusResultTO = new VenusResultTO();
            OrderDetailVO orderDetailVO =
                orderDAO.getOrderDetail(venusMessageVO.getReferenceNumber());
            ProductVO productVO =
                orderDAO.getProduct(orderDetailVO.getProductId());
            OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
            CustomerVO customerVO =
                orderDAO.getCustomer(orderVO.getCustomerId());

            venusResultTO =
                    processOrder(venusMessageVO, false, orderDetailVO, orderVO,
                                 customerVO, productVO);

            System.out.println(venusResultTO.isSuccess());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private VendorProductVO getVendorProduct(OrderDetailVO orderDetailVO)
        throws Exception{
        OrderDAO orderDAO = new OrderDAO(conn);

        // Obtain vendor id information
        String productSubcodeId;
        if( StringUtils.isNotBlank(orderDetailVO.getSubcode()) ) {
            productSubcodeId = orderDetailVO.getSubcode();
        }
        else {
            productSubcodeId = orderDetailVO.getProductId();
        }

        List vendors = orderDAO.getAvailableVendors(productSubcodeId, orderDetailVO.getOrderDetailId());
        if( vendors.size()==0 ) {
          throw new Exception("There are no vendors available for this product.");
        }

        // Non SDS will only have one vendor
        return (VendorProductVO)vendors.get(0);
    }
    
    /**
     * Returns the difference in days between the startDate and endDate.
     * 
     * @param startDate
     * @param endDate
     * @return The difference in days between the startDate and endDate
     * @throws IllegalArgumentException if either startDate or endDate are null
     */
    public int getDiffInDays(Date startDate, Date endDate) {

      // null check inputs
      if ( startDate == null || endDate == null ) {
        throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + startDate + "', endDate='" + endDate + "' were null.");
      }

      long diffInMillis = endDate.getTime() - startDate.getTime();

      int diffInDays = (int)(diffInMillis / DateUtils.MILLIS_IN_DAY);
      return diffInDays;
    }

    /**
     * calls the MDB for sending JMS message to myBuys queue
     * @param String productID of object for whom messages need to be sent
     * @return void
     */
      
    public void sendMyBuys(String productId) throws Exception
    {

         MessageToken messageToken = new MessageToken();
         messageToken.setMessage(productId);
         messageToken.setStatus("MY BUYS");
         Dispatcher dispatcher = Dispatcher.getInstance();
         dispatcher.dispatchTextMessage(new InitialContext(),messageToken);
        
    }
    
    private String getFtdWestOrderNumber(Connection conn) throws Exception {
        VenusDAO venusDAO = new VenusDAO(conn);
        String seq =
            "0000000000" + Long.toString(venusDAO.generateFtdWestOrderNumber());

        return "P" +
            seq.substring(seq.length() - WEST_DIGITS_IN_ORDER_NUMBER);
    }
    
    
}
