package com.ftd.op.venus.bo;

import com.ftd.op.common.to.ResultTO;
import com.ftd.op.venus.facade.VenusFacade;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CalculatedFieldsTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.op.venus.to.VenusResultTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import java.sql.Connection;


/**
 * Provides a BO Implementation for utilizing the Mercury API as a Library.
 */
public class VenusAPIBO
{
  /**
   * Logger instance for the Class
   */
  Logger logger = new Logger ("com.ftd.op.venus.bo.VenusAPIBO");
     
  /**
  * Sends a Adjustment message for the specified Order to the Venus Outbound Queue.
  * 
  * @param to The Message to send, including the order details for the message
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */     
  public ResultTO sendAdjustment(AdjustmentMessageTO to, Connection conn)
  {
    ResultTO result = new ResultTO();

    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      VenusResultTO venusResultTO = vf.sendMessage(to);
      result = (ResultTO) venusResultTO;
    } catch (Throwable e) 
    {
      logger.error(e);

      String msg = "Order Detail ID:" + to.getOrderDetailId() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } 
    
    return result;
  }
  
  /**
  * Sends a Cancel message for the specified Order to the Venus Outbound Queue.
  * 
  * @param to The Message to send, including the order details for the message
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */     
  public ResultTO sendCancel(CancelOrderTO to, Connection conn)
  {
    ResultTO result = new ResultTO();
        
    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      VenusResultTO venusResultTO = vf.sendCancel(to);
      result = (ResultTO) venusResultTO;
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Venus ID:" + to.getVenusId() + " Strack trace:" + e;
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } 
    
    return result;
  }
  
  /**
  * Sends a Order message for the specified Order to the Venus Outbound Queue.
  * 
  * @param to The order details for the order
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */    
  public ResultTO sendOrder(OrderDetailKeyTO to, Connection conn)
  {
    ResultTO result = new ResultTO();

    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      VenusResultTO venusResultTO = vf.sendOrder(to);
      result = (ResultTO) venusResultTO;
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Order Detail ID:" + to.getOrderDetailId() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } 
    
    return result;
  }
  
  /**
  * Sends a Order message for the specified Order to the Venus Outbound Queue.
  * 
  * @param to The order details for the order
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */   
  public ResultTO sendOrder(OrderTO to, Connection conn)
  {
    ResultTO result = new ResultTO();
          
    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      VenusResultTO venusResultTO = vf.sendOrder(to);
      result = (ResultTO) venusResultTO;
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Reference Number:" + to.getReferenceNumber() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } 
    
    return result;
  }
  
  /**
  * Retrievs the Calcilated Fields for the specified order.
  * 
  * @param order The order to retrieve calculated fields for.
  * @param conn Connection to the database
  * @return CalculatedFieldsTO containing the calculated fields for the order or <code>null</code> if there was an exception. 
  */ 
  public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order, Connection conn)
  {
    CalculatedFieldsTO fields = null;
        
    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      fields = vf.getCalculatedFields(order); 
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
      //throw e;
    } 
    
    return fields;
  }
  
  /**
  * Retrieve the calculated fields for a Vendor COMP order.  Difference is ship method 
  * doesn't get stored in the first choice string.
  * 
  * @param order The order to retrieve calculated fields for.
  * @param conn Connection to the database
  * @return CalculatedFieldsTO containing the calculated fields for the order or <code>null</code> if there was an exception. 
  */  
  public CalculatedFieldsTO getCompVendorCalculatedFields(OrderDetailKeyTO order, Connection conn)
  {

    CalculatedFieldsTO fields = null;

    try 
    {
      VenusFacade vf = new VenusFacade(conn);
      fields = vf.getCompVendorCalculatedFields(order); 
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
      //throw e;
    } 
    
    return fields;
  }

  /**
  * Helper/Utility method for formatting the stack trace of an Exception/Throwable as a single string.
  * 
  * @param aThrowable The throwable to format the stack trace for.
  * @return String containing the formatted stack trace.
  */  
  
  private String getStackTrace( Throwable aThrowable ) {
    final Writer result = new StringWriter();
    final PrintWriter printWriter = new PrintWriter( result );
    aThrowable.printStackTrace( printWriter );
    return result.toString();
  }      

}
