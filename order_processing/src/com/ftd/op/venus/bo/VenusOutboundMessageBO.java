package com.ftd.op.venus.bo;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.math.BigDecimal;

import java.net.InetAddress;

import java.sql.Connection;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;


public class VenusOutboundMessageBO
{

  private Logger logger;
  private Connection conn;
  // Constants for Order XML //
  private final String PARTY_TYPE_SHIP_FROM =  "SHIP_FROM";
  private final String PARTY_TYPE_SHIP_TO =  "SHIP_TO";  
  private final String CONTACT_TYPE_PHONE = "PHONE";
  private final String FLEX_FIELD_SYSADMIN = "sysadmin";
  private final String FLEX_FIELD_PROCESS_BATCH = "process_batch";
  private final String FLEX_FIELD_COMPANY_NAME = "companyName";
  private final String PURCHASE_ORDER_LINE_REF_NUMBER= "001";
  private final String PURCHASE_ORDER_LINE_QUANTITY= "1";
  private final String SKU_CLASS = "NORMAL";
  private final String NUMBER_OF_LINE_ITEMS = "1";
  private final String SERVER_LOCATION = "ESCALATE_REMOTE_LOCATION";
  private static String MACHINE_ID = null;
  
  public VenusOutboundMessageBO(Connection conn)
  {
    this.logger = new Logger("com.ftd.op.venus.bo.VenusOutboundMessageBO");
    this.conn = conn;
    initMachineId();
  }
  
  /**
   * This method starts the processing of orders found in the VENUS table.  It will 
   * retrieve all the order that need to be processed and then call a method to 
   * process them.  This method will insure that the order batch is less than or equal
   * to the maximum and is not sent more frequently than allowed.
   */
  
  public void processOrders()throws Exception
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat timeFormatter = new SimpleDateFormat("HHmmss");
    SimpleDateFormat globalParmsFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    OrderDAO orderDAO = new OrderDAO(conn);
    VenusDAO venusDAO = new VenusDAO(conn);
    ArrayList venusOrders = new ArrayList();
    // boolean to tell you if the file is okay to send  - has data, no fatal errors //
    boolean sendFile = false;
    
    boolean resetStatus = false;

    
    // create XML document to be sent to vendor //
    Document xmlDoc = (Document) DOMUtil.getDocument("scltPO", "sclt_po.dtd", null, false);
    try 
    {
      // get global parameter
      GlobalParameterVO lastOrderBatchSent = orderDAO.getGlobalParameter("ORDER_PROCESSING", "VENUS_LAST_BATCH_SENT");
      if (lastOrderBatchSent == null)
      {
        throw new VenusException("VENUS_LAST_BATCH_SENT in FRP.GLOBAL_PARMS is null.");
      }
      GlobalParameterVO batchFrequency = orderDAO.getGlobalParameter("ORDER_PROCESSING", "VENUS_BATCH_FREQUENCY_MINUTES");
      if (batchFrequency == null)
      {
        throw new VenusException("VENUS_BATCH_FREQUENCY_MINUTES in FRP.GLOBAL_PARMS is null.");
      }
      GlobalParameterVO batchMaxSize = orderDAO.getGlobalParameter("ORDER_PROCESSING", "VENUS_BATCH_MAX_SIZE");
      if (batchMaxSize == null)
      {
        throw new VenusException("VENUS_BATCH_MAX_SIZE in FRP.GLOBAL_PARMS is null.");
      }
      
      //convert String to Date //
      Date lastOrderBatchSentDate = globalParmsFormatter.parse(lastOrderBatchSent.getValue(), new ParsePosition(0));
      Date today = new Date();
      // if the differnce in minutes between the current time and the lastOrderBatchSent
      // is less than the batchFrequencyMinutes then exit procedure
      long minutesApart = this.getMinutesApart(lastOrderBatchSentDate, today);
      if (minutesApart < new Long (batchFrequency.getValue()).longValue())
      {
        logger.info("VenusOutboundMessageBO.processOrders: The difference in minutes between the current time and the lastOrderBatchSent ("+minutesApart+") is less than the batchFrequencyMinutes ("+batchFrequency.getValue()+").  Exited processOrders.");
        return;
      }
      venusOrders = (ArrayList) venusDAO.getVenusOrders(new Integer(batchMaxSize.getValue()).intValue(), "ESCALATE",null);
      if (venusOrders.size() == 0)
      {
        logger.info("VenusOutboundMessageBO.processOrders: No Venus Orders.  venusDAO.getVenusOrders( batchSize ("+new Integer(batchMaxSize.getValue()).intValue()+"), vendorType (ESCALATE))");
        return;
      }
   
      // create document id //
      String documentId = venusDAO.generateDocumentSequence();
      
      // create a header for the XML document //
      String scltPOVersion= configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SCLTPO_VERSION");        
      String scltPOType= configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SCLTPO_TYPE");        
      Element scltPO = xmlDoc.createElement("scltPO");
      scltPO.setAttribute("version",scltPOVersion);
      scltPO.setAttribute("type", scltPOType);
      scltPO.appendChild(this.getOrderHeaderXML(xmlDoc, venusDAO, dateFormatter, timeFormatter, documentId));
      for (int i = 0; i < venusOrders.size(); i++)
      {
        VenusMessageVO vo = (VenusMessageVO) venusOrders.get(i);
        logger.info("Processing venus message #"+vo.getVenusId()+"...");
        try
        {
          // add xmlOrder document to xmlFile document //
          scltPO.appendChild(this.getOrderXML(xmlDoc,venusDAO, dateFormatter, vo));
          // set status of venusMessageVO to 'VERIFIED' //
          venusDAO.updateVenusStatus(vo.getVenusId(), "VERIFIED");
          sendFile = true;
         }
         catch (Throwable t)
         {
          logger.error(t);
          CommonUtils.sendSystemMessage("Error processing venus record (Venus id=" + vo.getVenusId() + " " + t.toString());
          // set status of venusMessageVO to 'ERROR' to indicate that the record has an error//
          venusDAO.updateVenusStatus(vo.getVenusId(), "ERROR");
         
         }
      }
      // append trailer //
      scltPO.appendChild(this.getOrderTrailerXML(xmlDoc, documentId));
      // append main node to document //
      xmlDoc.appendChild(scltPO);
    }
    catch (Throwable t)
    {
      sendFile = false;
      logger.error(t);
      CommonUtils.sendSystemMessage("Error getting orders to send to Venus.  Error:" +t.getMessage());

      // set the status of all the order messages that are in the list to "OPEN" so
      // so they will be picked up with the next run 
      for (int i = 0; i < venusOrders.size(); i++)
      {
        VenusMessageVO vo = (VenusMessageVO) venusOrders.get(i);
        venusDAO.updateVenusStatus(vo.getVenusId(), "OPEN");
      }
    }
    // try to create and ftp XML in a file //
    try
    {
      if (sendFile)
      {
        String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"WORKINGDIR");                              
        String archiveDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"ARCHIVEDIR");                              
        String localFileRootName = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ESCALATE_LOCAL_FILENAME");   
        String localFileName = generateFileName(localFileRootName,dateFormatter,timeFormatter);
        
        File file = new File(localDir + localFileName);
        FileWriter out = new FileWriter(file);
        String xmlString = this.xmlDocToString(xmlDoc);
        out.write(xmlString);
        out.close();
        boolean fileSent = this.sendFile(file, configUtil);
        if (fileSent)
        {
          // change last batch date //
          orderDAO.setGlobalParameter("ORDER_PROCESSING", "VENUS_LAST_BATCH_SENT", globalParmsFormatter.format(new Date()), "ORDER_PROCESSING");
          // move file to archive dir //
          file.renameTo(new File(archiveDir + localFileName));
        }
        else
        {
          String msg = "Venus XML Order could not be ftped to Venus.  File must be manually sent to Venus.  File: "+(localDir + localFileName);
          logger.error(msg);
          CommonUtils.sendSystemMessage(msg); 
          // set the status of all the order messages that are in the list to "FTPERROR" so
          // so they will be picked up with the next run 
          for (int i = 0; i < venusOrders.size(); i++)
          {
            VenusMessageVO vo = (VenusMessageVO) venusOrders.get(i);
            venusDAO.updateVenusStatus(vo.getVenusId(), "FTPERROR");
          }
        }
      }
    }
    catch (Throwable t)
    {
       logger.error(t);

      // set the status of all the order messages that are in the list to "FTPERROR" so
      // so they will be picked up with the next run 
      for (int i = 0; i < venusOrders.size(); i++)
      {
        VenusMessageVO vo = (VenusMessageVO) venusOrders.get(i);
        venusDAO.updateVenusStatus(vo.getVenusId(), "FTPERROR");
      }
      CommonUtils.sendSystemMessage("Error tring to FTP file to venus.");

    }
    finally
    {
        //close connection if open
        if(conn != null && !conn.isClosed())
        {
            conn.close();
        }
    }
  }

    /**
     * Send a file to Escalate
     * 
     * @param sourceFile
     * @param configUtil
     * @return
     */
    public boolean sendFile(File sourceFile, ConfigurationUtil configUtil) {
        boolean retval = false;
      
        try {
            String fileName = sourceFile.getName();
            String filePath = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ESCALATE_REMOTE_DIRECTORY")+fileName;
            int ctr = 1;
            String url = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,SERVER_LOCATION+"_"+String.valueOf(ctr));
          
            while (url!=null && url.length()>0) {
                int timeout;
                try {
                    String strTimeout = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"ESCALATE_HTTP_TIMEOUT_"+String.valueOf(ctr));
                    timeout = Integer.parseInt(strTimeout)*1000;
                } catch (Throwable t) {
                    logger.warn("Property "+"ESCALATE_HTTP_TIMEOUT_"+String.valueOf(ctr)+" in file "+VenusConstants.VENUS_CONFIG_FILE+" not properly configured.");
                    timeout = 60000; //60 seconds
                }
                
                PostMethod filePost = new PostMethod(url);
                try {
                Part[] parts = {
                    new FilePart(sourceFile.getCanonicalPath(), sourceFile),
                    new StringPart("fileName",filePath)
                    };
                filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
                HttpClient client = new HttpClient();
                client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout); 
                int status = client.executeMethod(filePost);
                String responseString = filePost.getResponseBodyAsString();
                if (status == HttpStatus.SC_OK && responseString!=null && responseString.equalsIgnoreCase("SUCCESS")) {
                    logger.info("Upload of file "+fileName+" to "+url+": "+responseString);
                } else {
                    throw new Exception("Upload of file "+fileName+" to "+url+" failed: "+ HttpStatus.getStatusText(status)+".  Response: "+responseString);
                }
                } finally {
                    if ( filePost!=null ) {
                        filePost.releaseConnection();
                    }
                }
                
                try {
                    url = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,SERVER_LOCATION+"_"+String.valueOf(++ctr));  
                } catch (Throwable t) {
                    url = null;
                }
            }
    
            retval = true;
        } catch (Throwable t)
        {
            logger.error(t);
        }

        return retval;
    }

   /**
   * Create an XML document header 
   *
   *@return Element
   **/
  private Element getOrderHeaderXML(Document xmlDoc, VenusDAO venusDAO, SimpleDateFormat dateFormatter, SimpleDateFormat timeFormatter, String documentId)throws ParserConfigurationException, IOException, TransformerException, SAXException,  Exception
  {
      Element documentHeader = xmlDoc.createElement("documentHeader");
      //documentHeader.appendChild(xmlDoc.createElement("documentId"));
      documentHeader.appendChild(this.makeElement(xmlDoc, "documentId" , documentId));
      documentHeader.appendChild(this.makeElement(xmlDoc, "generationDate", dateFormatter.format(new Date())));
      documentHeader.appendChild(this.makeElement(xmlDoc, "generationTime", timeFormatter.format(new Date())));
      return documentHeader;
   }
  
    /**
   * Create an XML document summary (trailer) 
   *
   *@return Element
   **/
  
  private Element getOrderTrailerXML(Document xmlDoc, String documentId)throws ParserConfigurationException, IOException, TransformerException, SAXException,  Exception
  {
      Element documentSummary = xmlDoc.createElement("documentSummary");
      documentSummary.appendChild(this.makeElement(xmlDoc, "documentId" , documentId));
      return documentSummary;
   }
  
  /**
   * Create an XML document for the passed in messageVO.  
   * 
   * @param VenusMessageVO
   **/
  private Element getOrderXML (Document xmlDoc, VenusDAO venusDAO, SimpleDateFormat dateFormatter, VenusMessageVO venusMessageVO)throws Exception
  {
      // need this to get information from the config file //
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
      OrderDAO orderDAO = new OrderDAO(conn);

      // lookup VOs //
      OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(venusMessageVO.getReferenceNumber());
      if (orderDetailVO == null)
      {
        throw new VenusException("No order detail record for the given order detail id: "+venusMessageVO.getReferenceNumber());
      }
      OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
      if (orderVO == null)
      {
        throw new Exception ("No order record for given order guid: "+orderDetailVO.getOrderGuid());
      }
      CompanyVO companyVO = orderDAO.getCompany(orderVO.getCompanyId());
      if (companyVO == null)
      {
        throw new Exception ("No company record for given company id: "+orderVO.getCompanyId());
      }
      CustomerVO customerVO = orderDAO.getCustomer(orderVO.getCustomerId());
      if (customerVO == null)
      {
        throw new VenusException("No customer record for the given id: "+venusMessageVO.getRecipient());
      }
      //ProductVO productVO = orderDAO.getProduct(venusMessageVO.getProductId());
      //if (productVO == null)
      //{
      //  throw new VenusException("No product record for the given id: "+venusMessageVO.getProductId());
      //}


      // main element //
      Element purchaseOrder = xmlDoc.createElement("purchaseOrder");
      // order header //
      Element purchaseOrderHeader = xmlDoc.createElement("purchaseOrderHeader");
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc,"purchaseOrderNumber", venusMessageVO.getVenusOrderNumber()));
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc,"purchaseOrderDate", dateFormatter.format(new Date())));
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc,"merchantRefNumber",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"MERCHANT_REF_NUMBER")));
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc, "vendorRefNumber", venusMessageVO.getFillingVendor()));

      // add ship to and ship from information
      purchaseOrderHeader.appendChild(this.getShipFromXML(xmlDoc, configUtil, companyVO));
      purchaseOrderHeader.appendChild(this.getShipToXML(xmlDoc, venusMessageVO, companyVO));
      
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc, "allowBackOrder", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ALLOW_BACK_ORDER")));
      purchaseOrderHeader.appendChild(this.makeElement(xmlDoc, "allowPartialShipment",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ALLOW_PARTIAL_SHIPMENT")));
      
      // making paymentTerm element (within purchaseOrderHeader element) //
      Element paymentTerm = xmlDoc.createElement("paymentTerm");
      paymentTerm.appendChild(this.makeElement(xmlDoc, "displayString",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"DISPLAY_STRING")));
      // append paymentTerm to purchaseOrderHeader //
      purchaseOrderHeader.appendChild(paymentTerm);
      
      // making flexField element (within purchaseOrderHeader element) //
      Element flexField1 = xmlDoc.createElement("flexField");
      flexField1.appendChild(this.makeElement(xmlDoc, "name",FLEX_FIELD_SYSADMIN));
      flexField1.appendChild(this.makeElement(xmlDoc, "value",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SYSADMIN")));
      // append flexField1 to purchaseOrderHeader //
      purchaseOrderHeader.appendChild(flexField1);

      // making flexField element (within purchaseOrderHeader element) //
      Element flexField2 = xmlDoc.createElement("flexField");
      flexField2.appendChild(this.makeElement(xmlDoc, "name", FLEX_FIELD_PROCESS_BATCH));
      flexField2.appendChild(this.makeElement(xmlDoc, "value",venusDAO.generateBatchSequence()));
      // append flexField2 to purchaseOrderHeader //
      purchaseOrderHeader.appendChild(flexField2);

      // only include this tage if the message contains the company name //
      if ((venusMessageVO.getBusinessName() != null) && (!venusMessageVO.getBusinessName().equalsIgnoreCase("")))
      {      
        // making flexField element (within purchaseOrderHeader element) //
        Element flexField3 = xmlDoc.createElement("flexField");
        flexField3.appendChild(this.makeElement(xmlDoc, "name",FLEX_FIELD_COMPANY_NAME));
        flexField3.appendChild(this.makeElement(xmlDoc, "value",formatString(venusMessageVO.getBusinessName())));
        // append flexField2 to purchaseOrderHeader //
        purchaseOrderHeader.appendChild(flexField3);
      }
      
      // add the purchaseOrderHeader to the purchaseOrder //
      purchaseOrder.appendChild(purchaseOrderHeader);

      // making purchaseOrderDetails element (within purchaseOrder element) //      
      Element purchaseOrderDetails = xmlDoc.createElement("purchaseOrderDetails");
      
      // making purchaseOrderLineItem element (within purchaseOrderDetails element) //      
      Element purchaseOrderLineItem = xmlDoc.createElement("purchaseOrderLineItem");
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "purchaseOrderLineRefNumber",PURCHASE_ORDER_LINE_REF_NUMBER));          
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "quantity", PURCHASE_ORDER_LINE_QUANTITY));                
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "wholesaleUnitPrice", venusMessageVO.getPrice() == null ? null : venusMessageVO.getPrice().toString())); 
      
      

      // making skuDetails element (within purchaseOrderLineItem element) //      
      Element skuDetails = xmlDoc.createElement("skuDetails");
      skuDetails.appendChild(this.makeElement(xmlDoc, "skuClass", SKU_CLASS));                
      skuDetails.appendChild(this.makeElement(xmlDoc, "merchantSkuRefNumber",venusMessageVO.getProductId())); 
    
      skuDetails.appendChild(this.makeElement(xmlDoc, "vendorSkuRefNumber",venusMessageVO.getVendorSKU()));  
      skuDetails.appendChild(this.makeElement(xmlDoc, "skuDescription",formatString(venusMessageVO.getProductDescription())));  
      skuDetails.appendChild(this.makeElement(xmlDoc, "weight",venusMessageVO.getProductWeight()));  
    // append skuDetails to purchaseOrderLineItem //
      purchaseOrderLineItem.appendChild(skuDetails);
      
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "customerName",customerVO.getFirstName()+ " " + customerVO.getLastName()));

      // making salesOrderDetails element (within purchaseOrderLineItem element) //      
      Element salesOrderDetails = xmlDoc.createElement("salesOrderDetails");
      salesOrderDetails.appendChild(this.makeElement(xmlDoc, "salesOrderRefNumber",orderDetailVO.getExternalOrderNumber()));  
      salesOrderDetails.appendChild(this.makeElement(xmlDoc, "salesOrderDate", venusMessageVO.getOrderDate() == null ? null : dateFormatter.format(venusMessageVO.getOrderDate())));
     // append salesOrderDetails to purchaseOrderLineItem //
      purchaseOrderLineItem.appendChild(salesOrderDetails);
      
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "allowBackOrder", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ALLOW_BACK_ORDER")));
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "allowPartialShipment",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ALLOW_PARTIAL_SHIPMENT")));
      
      // making shipppingInstruction element (within purchaseOrderLineItem element) //      
      Element shippingInstruction = xmlDoc.createElement("shippingInstruction");
      
      // additional carrier update
      shippingInstruction.appendChild(this.makeElement(xmlDoc, "carrierCode", orderDetailVO.getCarrierId()));  
      shippingInstruction.appendChild(this.makeElement(xmlDoc, "shippingMethodCode",orderDetailVO.getCarrierDelivery()));   
      shippingInstruction.appendChild(this.makeElement(xmlDoc, "methodOfPayment", orderDetailVO.getVenusMethodOfPayment())); 
      
      // append shippingInstruction to purchaseOrderLineItem //
      purchaseOrderLineItem.appendChild(shippingInstruction);

      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "requestedShipDate", venusMessageVO.getShipDate() == null ? null : dateFormatter.format(venusMessageVO.getShipDate()))); 
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "requestedDeliveryDate", venusMessageVO.getDeliveryDate() == null ? null : dateFormatter.format(venusMessageVO.getDeliveryDate())));       
      
     // making packagingInstruction element (within purchaseOrderLineItem element) //      
      Element packagingInstruction = xmlDoc.createElement("packagingInstruction"); 
      packagingInstruction.appendChild(this.makeElement(xmlDoc, "transactionType",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRANSACTION_TYPE"))); 
      packagingInstruction.appendChild(this.makeElement(xmlDoc, "packageWeight", venusMessageVO.getProductWeight() == null ? new Integer(1).toString() : venusMessageVO.getProductWeight())); 
      packagingInstruction.appendChild(this.makeElement(xmlDoc, "packageWeightUnit",configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"PACKAGE_WEIGHT_UNIT"))); 
      // append packagingInstruction to purchaseOrderLineItem //
      purchaseOrderLineItem.appendChild(packagingInstruction);
      
      purchaseOrderLineItem.appendChild(this.makeElement(xmlDoc, "giftMessage", formatString(venusMessageVO.getCardMessage()))); 

      purchaseOrderLineItem.appendChild(this.getShipFromXML(xmlDoc, configUtil, companyVO));
      purchaseOrderLineItem.appendChild(this.getShipToXML(xmlDoc, venusMessageVO, companyVO));

      // append purchaseOrderLineItem to purchaseOrderDetails //
      purchaseOrderDetails.appendChild(purchaseOrderLineItem);


      // append purchaseOrderDetails to purchaseOrder //
      purchaseOrder.appendChild(purchaseOrderDetails);

     // making purchaseOrderSummary element (within purchaseOrder element) //      
      Element purchaseOrderSummary = xmlDoc.createElement("purchaseOrderSummary"); 
      purchaseOrderSummary.appendChild(this.makeElement(xmlDoc, "numberOfLineItems", NUMBER_OF_LINE_ITEMS));     
      purchaseOrder.appendChild(purchaseOrderSummary);
      return purchaseOrder;
  }

 /**
   * Create the ship to party XML 
   *
   *@return Element
   **/
  private Element getShipToXML(Document xmlDoc, VenusMessageVO venusMessageVO, CompanyVO companyVO)throws ParserConfigurationException, IOException, TransformerException, SAXException,  Exception
  {
      Element party = xmlDoc.createElement("party");
      party.appendChild(this.makeElement(xmlDoc,"partyType", PARTY_TYPE_SHIP_TO));
      party.appendChild(this.makeElement(xmlDoc,"partyName", venusMessageVO.getRecipient()));  
      // making address element (within party element) //
      Element address = xmlDoc.createElement("address");
      address.appendChild(this.makeElement(xmlDoc,"addressLine1", venusMessageVO.getAddress1()));
      address.appendChild(this.makeElement(xmlDoc,"addressLine2", venusMessageVO.getAddress2()));
      address.appendChild(this.makeElement(xmlDoc,"city", venusMessageVO.getCity()));
      address.appendChild(this.makeElement(xmlDoc,"stateCode", venusMessageVO.getState()));
      address.appendChild(this.makeElement(xmlDoc,"postalCode", venusMessageVO.getZip()));
      // append address to party //
      party.appendChild(address);
      // make contact element (within party element) //
      Element contact = xmlDoc.createElement("contact");
      contact.appendChild(this.makeElement(xmlDoc, "contactType", CONTACT_TYPE_PHONE));
      contact.appendChild(this.makeElement(xmlDoc, "phoneNumber", this.formatPhoneNumber(venusMessageVO.getPhoneNumber()))); 
      // append contact information to party //
      party.appendChild(contact);
      // append party information to purchaseOrderHeader //
      return party;
   }

 /**
   * Create the ship from party XML 
   *
   *@return Element
   **/
  private Element getShipFromXML(Document xmlDoc, ConfigurationUtil configUtil, CompanyVO companyVO)throws ParserConfigurationException, IOException, TransformerException, SAXException,  Exception
  {
      Element party = xmlDoc.createElement("party");
      party.appendChild(this.makeElement(xmlDoc,"partyType", PARTY_TYPE_SHIP_FROM));
      party.appendChild(this.makeElement(xmlDoc,"partyName", companyVO.getCompanyName())); 
      // making address element (within party element) //
      Element address = xmlDoc.createElement("address");
      address.appendChild(this.makeElement(xmlDoc,"addressLine1", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ADDRESS_1")));
      address.appendChild(this.makeElement(xmlDoc,"addressLine2", ""));
      address.appendChild(this.makeElement(xmlDoc,"city", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"CITY")));
      address.appendChild(this.makeElement(xmlDoc,"stateCode", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"STATE_CODE")));
      address.appendChild(this.makeElement(xmlDoc,"postalCode", configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"POSTAL_CODE")));
      // append address to party //
      party.appendChild(address);
      // make contact element (within party element) //
      Element contact = xmlDoc.createElement("contact");
      contact.appendChild(this.makeElement(xmlDoc, "contactType", CONTACT_TYPE_PHONE));
      contact.appendChild(this.makeElement(xmlDoc, "phoneNumber", this.formatPhoneNumber(companyVO.getPhoneNumber())));
      // append contact information to party //
      party.appendChild(contact);
      return party;
   }


  /**
    * Make a new XML element with a value
   * 
   * @param Document xml
   * @param String name
   * @param String value
   * @return Element
   */
  private String formatPhoneNumber(String phoneNumber) throws VenusException
  {
    String newPhoneNumber = "";
    if((phoneNumber != null) && (phoneNumber.length() > 0 ))
    {
    
      //remove the non numerics
      newPhoneNumber = removeNonNumerics(phoneNumber);

      if (newPhoneNumber.length() != 10) 
      {
        newPhoneNumber = "0000000000" + newPhoneNumber;
        int sLength = newPhoneNumber.length();
        newPhoneNumber = newPhoneNumber.substring(sLength-10, sLength);
      }
    
      if (newPhoneNumber.length() == 10)
      {
        newPhoneNumber = "(" + newPhoneNumber.substring(0,3) + ")"+newPhoneNumber.substring(3,6)+"-"+newPhoneNumber.substring(6,10);
      }
      else 
      {
        throw new VenusException("Phone Number ("+phoneNumber+") is the incorrect length");
      }
    }
    return newPhoneNumber;
  }    
  
    private String removeNonNumerics(String input)
    {
        String output = "";
    
        for (int i=0; i < input.length(); i++)
        {
            char n = input.charAt(i);
            Character nextCharacter = new Character(n);            
            if(nextCharacter.isDigit(n))
            {
                output = output + nextCharacter.toString();
            }
            
        }  
        
        return output;
    }
  

   /**
    * Make a new XML element with a value
   * 
   * @param Document xml
   * @param String name
   * @param String value
   * @return Element
   */
  private Element makeElement(Document xml, String name, String value)
  {
    Element element = xml.createElement(name);
    if(value != null)
    {
      element.appendChild(xml.createTextNode(value));
    }
    return element;
  }    

   /**
    * Make a new element with a value
   * 
   * @param Document xml
   * @param Element parent
   * @param Nodelist
   * @return Element parent
   */
  private Element appendChildren(Document xmlDoc, Element parent, NodeList list)
  {
      for (int i = 0; i < list.getLength(); i++)
      {
        parent.appendChild(list.item(i));
      }
      return parent;
  }
  
      /**
     * Calculate the number of minutes the passed in dates are apart from each
     * other.
     * 
     * @param startTime
     * @param endTime
     * @return long
     */
    private long getMinutesApart(Date startTime, Date endTime)
    {
        long milliApart = endTime.getTime() - startTime.getTime();
        float minutesApart = ((float)milliApart / (float)(1000 * 60));

        BigDecimal bd = new BigDecimal(minutesApart);
        long ret = bd.setScale(0, BigDecimal.ROUND_HALF_DOWN).longValue();        
        
        return ret;
    }

    
    /**
     * Formats a string so that it does not break Escalate.  This means removing
     * non-printable chars and any text that starts with an ampersand.
     * @param input
     * @return 
     */
    private String formatString(String input)
    {
        String output = "";
        
        if(input != null)
        {
            //remove non printable chars
            for (int i=0; i < input.length(); i++)
            {
                char n = input.charAt(i);
                Character nextCharacter = new Character(n);            
    
                //do not include nonprintable chars
                if(!Character.isISOControl(nextCharacter.charValue()))
                {
                        output = output + nextCharacter.toString();                
                }            
            }
            
            //remove xml encoding
            output = FieldUtils.replaceAll(output,"&lt;","<");    
            output = FieldUtils.replaceAll(output,"&gt;",">");        
            output = FieldUtils.replaceAll(output,"&#39;","'");        
            output = FieldUtils.replaceAll(output,"&amp;","and");                
        }
        
        return output;
    }

    private String xmlDocToString(Document xmlDoc) throws Exception
    {
        // Use this to include doctype information etc.
        return DOMUtil.convertToFormattedString(xmlDoc);
    }

    private String generateFileName(String localFileRootName, SimpleDateFormat dateFormatter, SimpleDateFormat timeFormatter ) {  
        Date fileDate = new Date();
        StringBuffer sb = new StringBuffer();
        sb.append(localFileRootName);
        sb.append("_");
        sb.append(dateFormatter.format(fileDate));
        sb.append("_");
        sb.append(timeFormatter.format(fileDate));
        sb.append("_");
        sb.append(MACHINE_ID);
        sb.append(".xml");
        
        return sb.toString();
    }

    /**
     * This method will find the machine machine id and save it in a static
     * variable.  Default value is "UNKNOWN".
     */
    private void initMachineId() {
        if( MACHINE_ID==null ) {
            synchronized (getClass()) {
                if( MACHINE_ID==null ) {
                    String machineId;
                    try
                    {
                      machineId = InetAddress.getLocalHost().getHostName();
                    }
                    catch(Exception e)
                    {
                      e.printStackTrace();
                      machineId = "UNKNOWN";
                    }
                    
                    if(machineId==null || machineId.trim().length()==0 ) {
                        machineId = "UNKNOWN";
                    }
                    
                    MACHINE_ID = machineId.toUpperCase();
                }
            }
        }
    }
  

  void testFileNameGen() {
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat timeFormatter = new SimpleDateFormat("HHmmss");
    System.out.println(this.generateFileName("VenusOrders",dateFormatter,timeFormatter));
  }
}
