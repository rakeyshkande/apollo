package com.ftd.op.venus.to;
import java.io.Serializable;

public class CancelMessageTO extends VenusBaseMessageTO implements Serializable 
{
  
  private String cancelReasonCode;
  /* This boolean will be false when canceling an order that has already been "printed".
   * It will add this error to the comments table - "Cancel message will not be sent 
   * to Venus because order already shipped.  System generated ADJ will go to Venus."
   * Then an ADJ will be sent.  
   * */  
  private boolean transmitCancel;
  

  public CancelMessageTO()
  {
    transmitCancel = true;
  }

  public void setCancelReasonCode(String cancelReasonCode)
  {
    this.cancelReasonCode = cancelReasonCode;
  }


  public String getCancelReasonCode()
  {
    return cancelReasonCode;
  }

  public void setTransmitCancel(boolean transmitCancel)
  {
    this.transmitCancel = transmitCancel;
  }


  public boolean isTransmitCancel()
  {
    return transmitCancel;
  }






}