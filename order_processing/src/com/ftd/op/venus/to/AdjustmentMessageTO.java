package com.ftd.op.venus.to;
import java.io.Serializable;

public class AdjustmentMessageTO extends VenusBaseMessageTO implements Serializable 
{

  private Double price;
  private Double overUnderCharge;
  private Double oldPrice;
  private String cancelReasonCode;

  public AdjustmentMessageTO()
  {
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


  public void setOverUnderCharge(Double overUnderCharge)
  {
    this.overUnderCharge = overUnderCharge;
  }


  public Double getOverUnderCharge()
  {
    return overUnderCharge;
  }


  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }


  public Double getOldPrice()
  {
    return oldPrice;
  }

  public void setCancelReasonCode(String cancelReasonCode) {
    this.cancelReasonCode = cancelReasonCode;
  }

  public String getCancelReasonCode() {
    return cancelReasonCode;
  }
}
