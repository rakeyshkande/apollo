package com.ftd.op.venus.to;
import java.io.Serializable;


public class CancelOrderTO implements Serializable 
{

  private String venusId;
  private String csr;
  private String comments;
  private String cancelReasonCode;
  private String venusStatus;
  boolean transmitCancel = true; 
  private String complaintCommOriginTypeId;
  private String complaintCommNotificationTypeId;
  
  
  public CancelOrderTO()
  {
  }


  public void setVenusId(String venusId)
  {
    this.venusId = venusId;
  }


  public String getVenusId()
  {
    return venusId;
  }

 

  public void setCsr(String csr)
  {
    this.csr = csr;
  }


  public String getCsr()
  {
    return csr;
  }


  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setCancelReasonCode(String cancelReasonCode)
  {
    this.cancelReasonCode = cancelReasonCode;
  }


  public String getCancelReasonCode()
  {
    return cancelReasonCode;
  }


  public void setTransmitCancel(boolean transmitCancel)
  {
    this.transmitCancel = transmitCancel;
  }


  public boolean isTransmitCancel()
  {
    return transmitCancel;
  }

  public void setVenusStatus(String venusStatus)
  {
    this.venusStatus = venusStatus;
  }

  public String getVenusStatus()
  {
    return venusStatus;
  }

  public String getComplaintCommOriginTypeId()
  {
    return complaintCommOriginTypeId;
  }

  public void setComplaintCommOriginTypeId(String complaintCommOriginTypeId)
  {
    this.complaintCommOriginTypeId = complaintCommOriginTypeId;
  }

  public String getComplaintCommNotificationTypeId()
  {
    return complaintCommNotificationTypeId;
  }

  public void setComplaintCommNotificationTypeId(String complaintCommNotificationTypeId)
  {
    this.complaintCommNotificationTypeId = complaintCommNotificationTypeId;
  }
}
