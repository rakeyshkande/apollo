package com.ftd.op.venus.to;
import com.ftd.op.common.to.ResultTO;
import java.io.Serializable;


public class VenusResultTO extends ResultTO implements Serializable 
{
  private String customerId;
  private String orderGuid;
  private String orderDetailId;
  private boolean treatAsAdjustment;

  public VenusResultTO()
  {
      super();
      treatAsAdjustment = false;
  }

  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }


  public String getCustomerId()
  {
    return customerId;
  }


  public void setOrderGuid(String orderGuid)
  {
    this.orderGuid = orderGuid;
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setTreatAsAdjustment(boolean treatAsAdjustment)
  {
    this.treatAsAdjustment = treatAsAdjustment;
  }


  public boolean isTreatAsAdjustment()
  {
    return treatAsAdjustment;
  }

}