package com.ftd.op.venus.to;
import java.io.Serializable;

public class VenusBaseMessageTO implements Serializable 
{

  private String venusOrderNumber;
  private String messageType;
  private String fillingVendor;
  private String operator;
  private String orderDetailId;
  private String comments;
  
  public VenusBaseMessageTO()
  {
  }


  public void setVenusOrderNumber(String venusOrderNumber)
  {
    this.venusOrderNumber = venusOrderNumber;
  }


  public String getVenusOrderNumber()
  {
    return venusOrderNumber;
  }


  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }


  public String getMessageType()
  {
    return messageType;
  }


  public void setFillingVendor(String fillingVendor)
  {
    this.fillingVendor = fillingVendor;
  }


  public String getFillingVendor()
  {
    return fillingVendor;
  }


  public void setOperator(String operator)
  {
    this.operator = operator;
  }


  public String getOperator()
  {
    return operator;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }




}