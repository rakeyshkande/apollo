package com.ftd.op.venus.to;
import java.io.Serializable;

public class CalculatedFieldsTO implements Serializable
{
  String firstChoice;
    private Double price;
  
  public CalculatedFieldsTO()
  {
  }

  public void setFirstChoice(String firstChoice)
  {
    this.firstChoice = firstChoice;
  }


  public String getFirstChoice()
  {
    return firstChoice;
  }

    public Double getPrice()
    {
        return price;
    }

    public void setPrice(Double price)
    {
        this.price = price;
    }

}