package com.ftd.op.venus.to;

import java.io.Serializable;
import java.util.Date;

public class OrderTO implements Serializable 
{
  private Date orderDate;
  private String recipient;
  private String address1;
  private String address2;
  private String city;
  private String state;
  private String zipCode;
  private String country;
  private String phoneNumber;
  private Date deliveryDate;
  private Double price;
  private String cardMessage;
  private Date shipDate;
  private String referenceNumber;
  private String productId;
  private Double overUnderCharge;
  private String subType;
  private String businessName;
  
  private String venusOrderNumber;
  private String messageType;
  private String fillingVendor;
  private String operator;
  private String cardSignature;
  private String compOrder;
  private String shipMethod;
  private boolean fromCommunicationScreen = false;

  public OrderTO()
  {
  }


  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }


  public Date getOrderDate()
  {
    return orderDate;
  }


  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }


  public String getRecipient()
  {
    return recipient;
  }


  public void setAddress1(String address1)
  {
    this.address1 = address1;
  }


  public String getAddress1()
  {
    return address1;
  }


  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }


  public Date getDeliveryDate()
  {
    return deliveryDate;
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = cardMessage;
  }


  public String getCardMessage()
  {
    return cardMessage;
  }


  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }


  public Date getShipDate()
  {
    return shipDate;
  }


  public void setReferenceNumber(String referenceNumber)
  {
    this.referenceNumber = referenceNumber;
  }


  public String getReferenceNumber()
  {
    return referenceNumber;
  }


  public void setProductId(String productId)
  {
    this.productId = productId;
  }


  public String getProductId()
  {
    return productId;
  }


  public void setOverUnderCharge(Double overUnderCharge)
  {
    this.overUnderCharge = overUnderCharge;
  }


  public Double getOverUnderCharge()
  {
    return overUnderCharge;
  }


  public void setSubType(String subType)
  {
    this.subType = subType;
  }


  public String getSubType()
  {
    return subType;
  }


  public void setBusinessName(String businessName)
  {
    this.businessName = businessName;
  }


  public String getBusinessName()
  {
    return businessName;
  }


  public void setVenusOrderNumber(String venusOrderNumber)
  {
    this.venusOrderNumber = venusOrderNumber;
  }


  public String getVenusOrderNumber()
  {
    return venusOrderNumber;
  }


  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }


  public String getMessageType()
  {
    return messageType;
  }


  public void setFillingVendor(String fillingVendor)
  {
    this.fillingVendor = fillingVendor;
  }


  public String getFillingVendor()
  {
    return fillingVendor;
  }


  public void setOperator(String operator)
  {
    this.operator = operator;
  }


  public String getOperator()
  {
    return operator;
  }


  public void setCardSignature(String cardSignature)
  {
    this.cardSignature = cardSignature;
  }


  public String getCardSignature()
  {
    return cardSignature;
  }

    public String getCompOrder()
    {
        return compOrder;
    }

    public void setCompOrder(String compOrder)
    {
        this.compOrder = compOrder;
    }


  public void setShipMethod(String shipMethod)
  {
    this.shipMethod = shipMethod;
  }


  public String getShipMethod()
  {
    return shipMethod;
  }


  public void setCountry(String country)
  {
    this.country = country;
  }


  public String getCountry()
  {
    return country;
  }


  public void setFromCommunicationScreen(boolean fromCommunicationScreen)
  {
    this.fromCommunicationScreen = fromCommunicationScreen;
  }


  public boolean isFromCommunicationScreen()
  {
    return fromCommunicationScreen;
  }



}