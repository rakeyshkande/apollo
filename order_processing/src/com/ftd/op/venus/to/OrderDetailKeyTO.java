package com.ftd.op.venus.to;
import java.io.Serializable;

/**
 * OrderDetailKeyTO contains the orderDetailId for referencing the order detail
 * record.
 */

public class OrderDetailKeyTO implements Serializable
{

  private String orderDetailId;
  private String csr; // operator //
  
  public OrderDetailKeyTO()
  {
  }

  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCsr(String csr)
  {
    this.csr = csr;
  }


  public String getCsr()
  {
    return csr;
  }




}