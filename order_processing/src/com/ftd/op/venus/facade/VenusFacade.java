package com.ftd.op.venus.facade;

import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.ProductSubCodeVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.VendorProductVO;
import com.ftd.op.order.vo.VendorVO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.service.VenusService;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CalculatedFieldsTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.op.venus.to.VenusBaseMessageTO;
import com.ftd.op.venus.to.VenusResultTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


/**
 * VenusFacade
 *
 * This is a facade between the VenusService and VenusAPIBean
 * to create single calls for API use
 *
 * @author Laura Felch
 */
public class

VenusFacade {

    private Connection conn;
    private Logger logger;

    public VenusFacade(Connection conn) {
        this.conn = conn;
        logger = new Logger("com.ftd.op.mercury.facade.VenusFacade");
    }

    /**
   * This method wraps the building and sending of Venus Cancel messages into a single
   * call for internal processing use
   *
   * @param to VenusOrderIdKeyTO
   */
    public

    VenusResultTO sendCancel(CancelOrderTO to) throws Exception {
        logger.debug("Doing a cancel");
        logger.debug("Venus id:" + to.getVenusId());
        logger.debug("Transmist Cancel:" + 
                     (to.isTransmitCancel() ? "true" : "false"));
        VenusService venusService = new VenusService(conn);
        return venusService.sendCancel(to);
    }

    /**
   * This method wraps the building and sending of Venus Adjustment messages into a single
   * call for internal processing use
   *
   * @param to VenusOrderIdKeyTO
   */
    public

    VenusResultTO sendAdjustment(AdjustmentMessageTO to) throws Exception {
        logger.debug("Doing a adjustment");
        VenusService venusService = new VenusService(conn);
        return venusService.sendMessage(to, false);
    }

    /**
   * This method wraps the building and sending of Venus Order messages into a single
   * call for internal processing use
   *
   * @param to OrderDetailKeyTO
   */
    public

    VenusResultTO sendOrder(OrderDetailKeyTO to) throws Exception {
        logger.debug("Doing a order");
        VenusService venusService = new VenusService(conn);
        return venusService.sendOrder(to);
    }

    /**
   * This method wraps the building and sending of Venus Order messages into a single
   * call for internal processing use
   *
   * @param to OrderTO
   */
    public VenusResultTO sendOrder(OrderTO to) throws Exception {
        VenusService venusService = new VenusService(conn);
        return venusService.sendOrder(to, false);
    }
    
    /**
   * This method wraps the building and sending of Venus messages into a single
   * call for internal processing use
   *
   * @param to VenusBaseMessageTO
   */
    public VenusResultTO sendMessage(VenusBaseMessageTO to) throws Exception {
        VenusService venusService = new VenusService(conn);
        return venusService.sendMessage(to, false);
    }

    public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order) throws Exception {
        logger.debug("getCalculatedFields for order detail id " + 
                     order.getOrderDetailId());

        VenusService vs = new VenusService(conn);
        CalculatedFieldsTO fields = new CalculatedFieldsTO();
        OrderDAO dao = new OrderDAO(conn);

        // get needed VOs
        OrderDetailVO orderDetail = 
            dao.getOrderDetail(order.getOrderDetailId());
        if (orderDetail == null) {
            throw new VenusException("No order detail record for given id: " + 
                                     order.getOrderDetailId());
        }
        ProductVO product = dao.getProduct(orderDetail.getProductId());
        if (product == null) {
            throw new VenusException("No product record for given id: " + 
                                     orderDetail.getProductId());
        }

        //check if the order is for a product with a subtype
        ProductSubCodeVO subcodeVO = null;
        String productSkuId;       
        if (orderDetail.getSubcode() != null && 
            orderDetail.getSubcode().length() > 0) {
            productSkuId = orderDetail.getSubcode();
            //For products with subtypes the subtype product codes and price should
            //be used.
            subcodeVO = dao.getSubcode(orderDetail.getSubcode());
            if (subcodeVO == null) {
                throw new Exception("Subcode does not exist in DB.  Subcode=" + 
                                    orderDetail.getSubcode());
            }
        }
        else {
            productSkuId = orderDetail.getProductId();
        }

        VendorProductVO vpVO = dao.getVendorProduct(orderDetail.getVendorId(), productSkuId);
        if (vpVO == null) {
          throw new Exception("Unable to find record for VENDOR_PRODUCT vendor_id/product_id " + 
                              orderDetail.getVendorId() + "/" + 
                              productSkuId);
        }
        
        // calculate the fields
        fields.setFirstChoice(vs.buildFirstChoiceString(orderDetail,  
                                                        product, subcodeVO, vpVO));

        fields.setPrice(new Double(vpVO.getVendorCost()));


        logger.debug("First choice = " + fields.getFirstChoice());
        logger.debug("Price = " + fields.getPrice());

        return fields;
    }

    public CalculatedFieldsTO getCompVendorCalculatedFields(OrderDetailKeyTO order) throws Exception {

        logger.debug("getCompVendorCalculatedFields for order detail id " + 
                     order.getOrderDetailId());

        CalculatedFieldsTO fields = new CalculatedFieldsTO();
        OrderDAO dao = new OrderDAO(conn);

        // get needed VOs
        OrderDetailVO orderDetail = 
            dao.getOrderDetail(order.getOrderDetailId());
        if (orderDetail == null) {
            throw new VenusException("No order detail record for given id: " + 
                                     order.getOrderDetailId());
        }
        ProductVO product = dao.getProduct(orderDetail.getProductId());
        if (product == null) {
            throw new VenusException("No product record for given id: " + 
                                     orderDetail.getProductId());
        }

        //check if the order is for a product with a subtype
        ProductSubCodeVO subcodeVO = null;
        if (orderDetail.getSubcode() != null && 
            orderDetail.getSubcode().length() > 0) {
            //For products with subtypes the subtype product codes and price should
            //be used.
            subcodeVO = dao.getSubcode(orderDetail.getSubcode());
            if (subcodeVO == null) {
                throw new Exception("Subcode does not exist in DB.  Subcode=" + 
                                    orderDetail.getSubcode());
            }
        }

        VendorProductVO vpVO = null;
        String productSkuId;
        if (subcodeVO != null) {
            productSkuId = subcodeVO.getProductSubCodeId();
        } else {
            productSkuId = orderDetail.getProductId();
        }

        vpVO = dao.getVendorProduct(orderDetail.getVendorId(), productSkuId);
        if (vpVO == null) {
            throw new Exception("Unable to find record for VENDOR_PRODUCT vendor_id/product_id " + 
                                orderDetail.getVendorId() + "/" + 
                                productSkuId);
        }

        fields.setPrice(new Double(vpVO.getVendorCost()));


        logger.debug("First choice = " + fields.getFirstChoice());
        logger.debug("Price = " + fields.getPrice());

        return fields;
    }
}
