package com.ftd.op.venus.api;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.venus.bo.VenusAPIBO;
import com.ftd.op.venus.facade.VenusFacade;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CalculatedFieldsTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;
import com.ftd.op.venus.to.VenusResultTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import javax.sql.DataSource;

public class VenusAPIBean implements SessionBean 
{
  private DataSource db;
  private Logger logger;
  private VenusAPIBO venusAPIBO;

  public void ejbCreate()
  {
  }

  public void ejbActivate()
  {
  }

  public void ejbPassivate()
  {
  }

  public void ejbRemove()
  {
  }

  public void setSessionContext(SessionContext ctx)
  {
    logger = new Logger("com.ftd.op.mercury.api.VenusAPIBean");
    try
    {
      db = CommonUtils.getDataSource();
      venusAPIBO = new VenusAPIBO();
    }
    catch(Exception e)
    {
      logger.debug(e.toString());
      
    }    
  }
  
    public ResultTO sendAdjustment(AdjustmentMessageTO to)
  {
      ResultTO result = new ResultTO();
      Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.sendAdjustment(to, conn);
    } catch (Throwable e) 
    {
      logger.error(e);

      String msg = "Order Detail ID:" + to.getOrderDetailId() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }
  
  public ResultTO sendCancel(CancelOrderTO to)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.sendCancel(to, conn);
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Venus ID:" + to.getVenusId() + " Strack trace:" + e;
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }
  
  public ResultTO sendOrder(OrderDetailKeyTO to)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.sendOrder(to, conn);
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Order Detail ID:" + to.getOrderDetailId() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }
  
  public ResultTO sendOrder(OrderTO to)
  {
      ResultTO result = new ResultTO();
      Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.sendOrder(to, conn);
    } catch (Throwable e) 
    {
      logger.error(e);
      String msg = "Reference Number:" + to.getReferenceNumber() + " Strack trace:" + getStackTrace(e);
      logger.error(msg);
      result.setErrorString(msg);
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }
  public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order)
  {

    CalculatedFieldsTO fields = null;
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.getCalculatedFields(order, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
      //throw e;
    } finally 
    {
      try 
      {
        conn.close();
      } catch (Exception ce) 
      {
        // empty
      }
    }
    return fields;
  }
  
   /*
   * Retrieve the calculated fields for a Vendor COMP order.  Difference is ship method 
   * doesn't get stored in the first choice string.
   */
  
  public CalculatedFieldsTO getCompVendorCalculatedFields(OrderDetailKeyTO order)
  {

    CalculatedFieldsTO fields = null;
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return venusAPIBO.getCompVendorCalculatedFields(order, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
      //throw e;
    } finally 
    {
      try 
      {
        conn.close();
      } catch (Exception ce) 
      {
        // empty
      }
    }
    return fields;
  }

  
private String getStackTrace( Throwable aThrowable ) {
    final Writer result = new StringWriter();
    final PrintWriter printWriter = new PrintWriter( result );
    aThrowable.printStackTrace( printWriter );
    return result.toString();
  }
  
  
}