package com.ftd.op.venus.api;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CalculatedFieldsTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;

import java.rmi.RemoteException;

import javax.ejb.EJBObject;

public interface VenusAPI extends EJBObject 
{

  ResultTO sendAdjustment(AdjustmentMessageTO to)throws RemoteException;
  
  ResultTO sendCancel(CancelOrderTO to)throws RemoteException;
  
  ResultTO sendOrder(OrderDetailKeyTO to)throws RemoteException;
  
  ResultTO sendOrder(OrderTO to)throws RemoteException;
  
  CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order) throws RemoteException;
  
  CalculatedFieldsTO getCompVendorCalculatedFields(OrderDetailKeyTO order) throws RemoteException;

}