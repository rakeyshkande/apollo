package com.ftd.op.venus.api;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.to.CalculatedFieldsTO;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.OrderDetailKeyTO;
import com.ftd.op.venus.to.OrderTO;

import javax.ejb.EJBLocalObject;

public interface VenusAPILocal extends EJBLocalObject 
{

  ResultTO sendAdjustment(AdjustmentMessageTO to);

  ResultTO sendCancel(CancelOrderTO to);
  
  ResultTO sendOrder(OrderDetailKeyTO to);
  
  ResultTO sendOrder(OrderTO to);
  
  CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order);
  
  CalculatedFieldsTO getCompVendorCalculatedFields(OrderDetailKeyTO order);

  
}