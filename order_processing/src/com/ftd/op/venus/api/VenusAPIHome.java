package com.ftd.op.venus.api;
import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;

public interface VenusAPIHome extends EJBHome 
{
  VenusAPI create() throws RemoteException, CreateException;
}