package com.ftd.op.venus.api;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface VenusAPILocalHome extends EJBLocalHome 
{
  VenusAPILocal create() throws CreateException;
}