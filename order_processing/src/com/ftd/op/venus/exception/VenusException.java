package com.ftd.op.venus.exception;

/**
 * VenusException.java
 * 
 * This class seperates exceptions that are manually thrown by the Venus API. 
 * For example, the API would throw this error if an order is not found for 
 * a given order number.  
 */

public class VenusException extends Exception
{
  public VenusException (String message) 
  {
    super (message);
  }
    
  public VenusException () 
  {
    super ();
  }

}