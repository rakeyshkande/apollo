package com.ftd.op.venus.handler;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Iterator;
import java.util.List;

/**
 * This object is responsible for checking if Escalate has received orders(FTDs)
 * that we sent to them.  This object is written in what that it can easily
 * be modified to check the status of other message types or other systems(wine, sams..)
 * if needed.  This would most likely only require a stored procedure change.
 */
public class VenusVerificationHandler extends com.ftd.eventhandling.events.EventHandler  
{
    private static final String LOGGER_CATEGORY = "com.ftd.op.venus.handler.VenusVerificationHandler";
    private Logger logger;
    private static final String ORDERPROCESSING_CONTEXT = "ORDER_PROCESSING";

    public VenusVerificationHandler()
    {
        //Build a logger
        this.logger =  new Logger(LOGGER_CATEGORY);   
    }

    /**
     * This method is invoked by event handler framework.
     */
    public void invoke(Object payload) throws Throwable
    {
        logger.info("Starting VenusVerificationHandler");

        //connection
        Connection conn = null;
    
        try
        {
            //obtain a database connection
            conn = CommonUtils.getConnection();   
            verifiyMessages(conn);
        }
        catch(Throwable t)
        {
            logger.error(t);
            CommonUtils.sendSystemMessage(t.toString());
        }
        finally
        {
            if(conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }   

    }
    
    /*
     * This method contains the main logic for VenusVerification.
     */
    private void verifiyMessages(Connection conn) throws Exception
    {
        //get the number of minutes to wait before sending out a page
        OrderDAO orderDAO = new OrderDAO(conn);
        GlobalParameterVO globalVO = orderDAO.getGlobalParameter(ORDERPROCESSING_CONTEXT,"VENUS_VERIFY_MSG_WAIT");
        String minuteString = globalVO.getValue();
        if(minuteString == null)
        {
            throw new Exception("VENUS_VERIFY_MSG_WAIT global paramater does not exist in DB.");
        }
        int minutes = Integer.parseInt(minuteString);
        
        //call proc to check for orders that do not exist on the remote system (e.g. Escalate)
        VenusDAO venusDAO = new VenusDAO(conn);
        List notVerifiedList = venusDAO.verifyMessages(minutes);
        
        //if any records returned
        if(notVerifiedList != null && notVerifiedList.size() > 0)
        {
            String notFoundOrders = "";
            
            logger.info(notVerifiedList.size() + " order(s) do not exist on Escalate.");
        
            //create string of all the order numbers that do not exist on remote system
            Iterator iter = notVerifiedList.iterator();
            while(iter.hasNext())
            {
                String orderNumber = (String)iter.next();
                if(notFoundOrders.length() > 0)
                {
                    notFoundOrders = notFoundOrders + "," + orderNumber;
                }
                else
                {
                    notFoundOrders = orderNumber;
                }
            }

            //send out system notification       
            String msg = "The following venus order number(s) do not exist on Escalate: " + notFoundOrders;
            CommonUtils.sendSystemMessage(msg);
        }
        else
        {
            logger.info("All venus records have been verified.");
        }
    }
    
    
}