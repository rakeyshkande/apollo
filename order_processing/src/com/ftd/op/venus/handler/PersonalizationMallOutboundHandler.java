package com.ftd.op.venus.handler;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.util.VenusCommonUtil;
import com.ftd.op.venus.vo.PersonalizationVO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.File;
import java.io.FileWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.w3c.dom.Document;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;


/**
  * This class handles outbound files sent to Personalization Mall.
  *
  */
public class PersonalizationMallOutboundHandler extends com.ftd.eventhandling.events.EventHandler {
    private final String SERVER_LOCATION = "REMOTE_LOCATION";
    private final String SERVER_LOGON = "FTP_LOGON";
    private final String SERVER_PASSWORD = "FTP_PASSWORD";

    private static final String BILL_TO_FIRST_NAME = "FTD.";
    private static final String BILL_TO_LAST_NAME = "COM";
    private static final String BILL_TO_ADDR1 = "3113 Woodcreek Drive";
    private static final String BILL_TO_ADDR2 = StringUtils.EMPTY;
    private static final String BILL_TO_CITY = "Downers Grove";
    private static final String BILL_TO_STATE = "IL";
    private static final String BILL_TO_ZIP = "60515";
    private static final String BILL_TO_PHONE = StringUtils.EMPTY;
    private static final String ORDER_QUANTITY = "1";
    
    private Logger logger;
    private Connection conn;
     
    public PersonalizationMallOutboundHandler() throws Exception {
        this.logger = new Logger("com.ftd.op.venus.handler.PersonalizationMallOutboundHandler");  
    }


    /** 
     * This method is invoked by the event handler framework. 
     */
    public void invoke(Object payload) throws Throwable {
        MessageToken messageToken = (MessageToken) payload;
        String vendorId = (String)messageToken.getMessage();
        
        conn = CommonUtils.getConnection();
        
        try {
            if(logger.isInfoEnabled())
                logger.info("Executing vendor " + vendorId);
            processOrders(vendorId);
        }
        catch (Throwable t) {
            String msg = "Personalization Mall outbound file was not created.  " + t.getMessage();
            logger.error(msg, t);
            CommonUtils.sendSystemMessage(msg);
        }
        finally {
            conn.close();
            conn = null;
        }
    }
  
  
    /**
     * Creates and FTPs Personalization Mall outbound file 
     * @param vendorId
     * @throws Exception
     */
    public void processOrders(String vendorId)throws Exception {
        StringBuffer errorDetail = null;
        VenusDAO venusDAO = new VenusDAO(conn);
        VenusCommonUtil venusCommonUtil = new VenusCommonUtil(conn);
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

        int batchMaxSize = 0; 
        try {
            batchMaxSize = Integer.parseInt(configUtil.getFrpGlobalParm("ORDER_PROCESSING", "VENUS_BATCH_MAX_SIZE_" + vendorId));
        } catch(NumberFormatException nfe) { 
            // Set to max if size cannot be obtained
            batchMaxSize = Integer.MAX_VALUE;
            logger.warn("Max batch size set to maximum value!");
        }
        ArrayList venusOrders = (ArrayList)venusDAO.getVenusOrders(batchMaxSize, "FTP", vendorId);

        String filePrefix = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "FTP_FILE_PREFIX_" + vendorId);      

        int ftpCount = venusDAO.getDailyFTPCount(vendorId, "ORDER");

        String ftpCountString = this.getFtpCountString(ftpCount);
        String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT, "WORKINGDIR");  
        String archiveDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT, "ARCHIVEDIR");  
        String localFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + "_" + vendorId + ".xml";
        String remoteFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + ".xml";

        File file = new File(localDir + localFileName);
        FileWriter out = new FileWriter(file);
        
        // Create order xml
        Document orderDocument = DOMUtil.getDefaultDocument();
        List errors = createOrderXML(orderDocument, venusOrders);
        // If errors exist only process the valid orders
        if(errors.size() > 0) {
            errorDetail = new StringBuffer();
            String orderDetailId = null;
            
            // Iterate through errors
            for(Iterator<String> it = errors.iterator(); it.hasNext();)
            {
                orderDetailId = it.next();
                errorDetail.append(orderDetailId);
                errorDetail.append(" ");
                // Iterate through orders
                for (Iterator<VenusMessageVO> it2 = venusOrders.iterator(); it2.hasNext();) {
                    VenusMessageVO vmVO = it2.next();
                    // Remove problem orders and set venus status back to open
                    if(vmVO.getReferenceNumber().equals(orderDetailId)) {
                        venusDAO.updateVenusStatus(vmVO.getVenusId(), "OPEN");
                        it2.remove();  
                    }
                }  
            }
            
            // Process valid orders
            orderDocument = DOMUtil.getDefaultDocument();
            List moreErrors = createOrderXML(orderDocument, venusOrders);
            // If additional errors are encountered cut bait and send out a system message
            if(moreErrors.size() > 0) {
                for(Iterator<String> it = moreErrors.iterator(); it.hasNext();) {
                  errorDetail.append(orderDetailId);
                  errorDetail.append(" ");
                }
                String msg = "Personalization Mall outbound file was not created.  Please investigate.  Orders which encountered errors:\n" + errorDetail;
                logger.error(msg);
                CommonUtils.sendSystemMessage(msg);
                return;
            }
            // If no more errors are encountered send out a message detailing the problem orders
            else {
                String msg = "Personalization Mall outbound file was created but the following orders were not sent to FTP Server:\n" + errorDetail;
                logger.error(msg);
                CommonUtils.sendSystemMessage(msg); 
            }
        }
        
        DOMUtil.print(orderDocument, out);
        out.close();
        
        // Update status
        errorDetail = new StringBuffer();
        for (int i = 0; i < venusOrders.size(); i++) {
            VenusMessageVO venusMessageVO = (VenusMessageVO)venusOrders.get(i);
            
            try {
                venusDAO.updateVenusStatus(venusMessageVO.getVenusId(), "VERIFIED");
                VenusMessageVO newVenusMessageVO = this.getVenusMessageForInsert(venusMessageVO);
                venusDAO.insertVenusMessage(newVenusMessageVO, VenusConstants.OUTBOUND_MSG);
            }
            catch(Exception ex) {
                logger.error("Error updating venus id " + venusMessageVO.getVenusId() + " to VERIFIED.", ex);
                errorDetail.append(venusMessageVO.getVenusId());
                errorDetail.append(" ");
                try {
                    venusDAO.updateVenusStatus(venusMessageVO.getVenusId(), "ERROR");
                }
                catch(Exception ex1) {
                    logger.warn("Error updating venus id " + venusMessageVO.getVenusId() + " to ERROR.", ex1);
                }
            }
        }
        if(errorDetail.length() > 0)
        {
            String msg = "The following venus ids were not set to VERIFIED when creating the Personalization Mall outbound file:\n" + errorDetail;
            CommonUtils.sendSystemMessage(msg);
        }

        File remoteFile = null;
        try {
            boolean fileSent = false;
            // The name of the file that is FTPed to the remote server should not contain the vendor //

            remoteFile = venusCommonUtil.copyFile(new File(localDir + remoteFileName), file);
            
            if(remoteFile == null) {
                throw new Exception (file.getName() + " could not be copied.");
            }
            else {
                fileSent = this.ftpFile(remoteFile, configUtil, venusCommonUtil, vendorId);
            }
        
            /* Insert FTP transmission history regardless of whether the ftp failed.
             * If a transmission history entry is not inserted the current
             * file will be overwritten during the next execution.
             * That results in lost orders.  This isn't the best design
             * but a refactor is not warranted at this time.
             */
            venusDAO.insertTransmissionHistory(vendorId, "ORDER");

            // move local file to archive dir //
            file.renameTo(new File(archiveDir + localFileName));

            if(fileSent) {
                remoteFile.delete();
            }
            else {
                String msg = remoteFile.getName() + " could not be FTPed.  File must be manually sent to vendor " + vendorId + ".  File: " + (localDir + localFileName);
                logger.error(msg);
                CommonUtils.sendSystemMessage(msg); 
            }
        }
        catch (Exception ex) {
            String msg = remoteFile.getName() + " could not be FTPed.  File must be manually sent to vendor " + vendorId + ".  File: " + (localDir + localFileName) + ": " + ex.toString();
            logger.error(msg, ex);
            CommonUtils.sendSystemMessage(msg); 
        }
    }
    
    
    /**
     * Create XML document detailing Personalization Mall orders 
     * @param List Venus orders
     * @return List List of failed orders
     */  
    private List createOrderXML(Document orderDocument, List venusOrders) throws Exception{
        List errors = new ArrayList();
        Element root = orderDocument.createElement("orders");
  
        for (int i = 0; i < venusOrders.size(); i++) {
            VenusMessageVO venusMessageVO = (VenusMessageVO)venusOrders.get(i);
            
            try {
                addOrderToXML(orderDocument, root, venusMessageVO);
            }
            catch(Exception ex) {
                errors.add(venusMessageVO.getReferenceNumber());
                String msg = "Error creating Personalization Mall outbound XML for order "
                  + venusMessageVO.getReferenceNumber() 
                  + ".  Please investigate:";
                logger.error(msg + ex.toString(), ex);
            }
        }
        orderDocument.appendChild(root);
        return errors;
    }


    /**
     * Vendor specific ftpFile method- sets parameters and calls the 
     * VenusCommonUtil.java ftpFile method 
     * @param File file
     * @param ConfigurationUtil configUtil
     * @param VenusCommonUtil venusCommonUtil
     * @param String vendorId
     * @return boolean fileSent
     */  
    private boolean ftpFile(File file, ConfigurationUtil configUtil, VenusCommonUtil venusCommonUtil, String vendorId) {
        boolean fileSent = false;
        
        try {
            String remoteDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT, "VENDOR_" + vendorId + "_REMOTE_DIRECTORY");    
            String loadBalance = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "LOAD_BALANCE");                      
            fileSent = venusCommonUtil.ftpFile(file, configUtil, remoteDir, loadBalance, "VENDOR_" + vendorId + "_" + SERVER_LOCATION, "VENDOR_" + vendorId + "_" + SERVER_LOGON, "VENDOR_" + vendorId + "_" + SERVER_PASSWORD);    
        }
        catch (Throwable t) {
            logger.error(t);
            return false;
        }
        
        return fileSent;
    }


    /** 
     * The following method will create a new VenusMessageVO to the specification 
     * of the VenusMessageVO section
     * @param VenusMessageVO venusMessageVO
     * @returns VenusMessageVO
     */    
    private VenusMessageVO getVenusMessageForInsert(VenusMessageVO venusMessageVO) {
      SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy h:mma");
      VenusMessageVO newVenusMessageVO = new VenusMessageVO();
      
      // copied fields //
      newVenusMessageVO.setVenusOrderNumber(venusMessageVO.getVenusOrderNumber());
      newVenusMessageVO.setOrderDate(venusMessageVO.getOrderDate());
      newVenusMessageVO.setFillingVendor(venusMessageVO.getFillingVendor());
      newVenusMessageVO.setRecipient(venusMessageVO.getRecipient());
      newVenusMessageVO.setAddress1(venusMessageVO.getAddress1());
      newVenusMessageVO.setAddress2(venusMessageVO.getAddress2());
      newVenusMessageVO.setCity(venusMessageVO.getCity());
      newVenusMessageVO.setState(venusMessageVO.getState());
      newVenusMessageVO.setZip(venusMessageVO.getZip());
      newVenusMessageVO.setPhoneNumber(venusMessageVO.getPhoneNumber());
      newVenusMessageVO.setDeliveryDate(venusMessageVO.getDeliveryDate());
      newVenusMessageVO.setPrice(venusMessageVO.getPrice());
      newVenusMessageVO.setCardMessage(venusMessageVO.getCardMessage());
      newVenusMessageVO.setReferenceNumber(venusMessageVO.getReferenceNumber());
      newVenusMessageVO.setCountry(venusMessageVO.getCountry());
      newVenusMessageVO.setProductID(venusMessageVO.getProductId());
      newVenusMessageVO.setShippingSystem(venusMessageVO.getShippingSystem());
      
      // different fields //
      newVenusMessageVO.setOperator("SYS");
      newVenusMessageVO.setMessageText("Order printed on " + dateFormatter.format(new Date()));
      newVenusMessageVO.setMsgType("ANS");
      newVenusMessageVO.setVenusStatus("VERIFIED");
      newVenusMessageVO.setOrderPrinted("Y");
      newVenusMessageVO.setTransmissionTime(new Date()); 
      
      return newVenusMessageVO;
    }


    /** 
     * The following method will add an order to the XML order file.
     * @param Document Order document
     * @param Element Order document root element
     * @param VenusMessageVO venusMessageVO
     * @throws Exception
     */
    private void addOrderToXML(Document orderDocument, Element root, VenusMessageVO venusMessageVO) throws Exception { 
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

        OrderDAO orderDAO = new OrderDAO(conn);
        OrderDetailVO orderDetail = orderDAO.getOrderDetail(venusMessageVO.getReferenceNumber());
        String externalOrderNumber = orderDetail.getExternalOrderNumber();
        CustomerVO recipient = orderDAO.getCustomer(orderDetail.getRecipientId());
        List personalizations = getPersonalizationList(orderDetail.getPersonalizationData());

        Element orderElement = orderDocument.createElement("order");
        root.appendChild(orderElement);
        
        Element orderDetailElement = orderDocument.createElement("order-details");
        orderElement.appendChild(orderDetailElement);
        
        Element detailElement = orderDocument.createElement("po-number");
        Text detailData = orderDocument.createTextNode(externalOrderNumber);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("order-date");
        detailData = orderDocument.createTextNode(dateFormatter.format(venusMessageVO.getOrderDate()));
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-first-name");
        detailData = orderDocument.createTextNode(BILL_TO_FIRST_NAME);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-last-name");
        detailData = orderDocument.createTextNode(BILL_TO_LAST_NAME);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-addr1");
        detailData = orderDocument.createTextNode(BILL_TO_ADDR1);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-addr2");
        detailData = orderDocument.createTextNode(BILL_TO_ADDR2);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-city");
        detailData = orderDocument.createTextNode(BILL_TO_CITY);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-state");
        detailData = orderDocument.createTextNode(BILL_TO_STATE);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-zip");
        detailData = orderDocument.createTextNode(BILL_TO_ZIP);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("bill-to-phone");
        detailData = orderDocument.createTextNode(BILL_TO_PHONE);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
                    
        detailElement = orderDocument.createElement("ship-to-first-name");
        detailData = orderDocument.createCDATASection(recipient.getFirstName());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-last-name");
        detailData = orderDocument.createCDATASection(recipient.getLastName());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-addr1");
        detailData = orderDocument.createCDATASection(venusMessageVO.getAddress1());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-addr2");
        detailData = orderDocument.createCDATASection(venusMessageVO.getAddress2());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-city");
        detailData = orderDocument.createCDATASection(venusMessageVO.getCity());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-state");
        detailData = orderDocument.createCDATASection(venusMessageVO.getState());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-zip");
        detailData = orderDocument.createCDATASection(venusMessageVO.getZip());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("ship-to-phone");
        detailData = orderDocument.createCDATASection(venusMessageVO.getPhoneNumber());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("order-quantity");
        detailData = orderDocument.createTextNode(ORDER_QUANTITY);
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("pc-product-number");
        detailData = orderDocument.createTextNode(venusMessageVO.getVendorSKU());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
        
        detailElement = orderDocument.createElement("description");
        detailData = orderDocument.createCDATASection(venusMessageVO.getProductDescription());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
      
        Element personalizationRootElement = orderDocument.createElement("personalizations");
      
        String token = null;
        PersonalizationVO pvo = null;
        ProductVO productVO = orderDAO.getProduct(orderDetail.getProductId());
        StringTokenizer st = new StringTokenizer(productVO.getPersonalizationTemplateOrder(), ",");
        int tokenCnt = 0;
        int persCnt = 0;
        while(st.hasMoreTokens()) {
          token = st.nextToken();
          tokenCnt++;

          for(int i = 0; i < personalizations.size(); i++) {
            pvo = (PersonalizationVO)personalizations.get(i);
            
            if(pvo.getPersonalizationName().equals(token)) {
              persCnt++;
              detailElement = orderDocument.createElement("personalization");
              
              Element personalizationElement = orderDocument.createElement("personalization-name");
              Text personalizationData = orderDocument.createTextNode(pvo.getPersonalizationName());
              personalizationElement.appendChild(personalizationData);
              detailElement.appendChild(personalizationElement);
              
              personalizationElement = orderDocument.createElement("personalization-data");
              personalizationData = orderDocument.createCDATASection(pvo.getPersonalizationData());
              personalizationElement.appendChild(personalizationData);
              detailElement.appendChild(personalizationElement);          
              
              personalizationRootElement.appendChild(detailElement);
            }
          }
        }

        /* Test that the number of personalization fields from the template
         * match the number of personalization nodes created for the order.
         */ 
        if(tokenCnt != persCnt) {
          throw new Exception("Personalization template fields and number of personalizations should match.\n" 
            + "Personalization Template Fields:" + tokenCnt + "\n"
            + "Personalization nodes created:" + persCnt + "\n" 
            + "Order:" + orderDetail.getOrderDetailId());
        } 
        /* Test that the number of personalization fields from the template
         * match the number of personalizations.
         */ 
        else if(personalizations.size() != tokenCnt) {
          throw new Exception("Personalization template fields and number of personalizations should match.\n" 
            + "Personalization Template Fields:" + tokenCnt + "\n"
            + "Personalizations:" + personalizations.size() + "\n" 
            + "Order:" + orderDetail.getOrderDetailId()); 
        }

        orderDetailElement.appendChild(personalizationRootElement);
        
        detailElement = orderDocument.createElement("gift-card-message");
        detailData = orderDocument.createCDATASection(venusMessageVO.getCardMessage());
        detailElement.appendChild(detailData);
        orderDetailElement.appendChild(detailElement);
    }


    /** 
     * This method creates the string signifying what number file transmission
     * this is for the day.  If this is the first batch of the day, the string
     * will be blank, 2nd - "A", 3rd -"B", etc...
     * @param ftpCount Number of transmissions
     * @returns String Letter correlating to number of transmissions
     */
    private String getFtpCountString(int ftpCount) { 
        String ftpCountString = StringUtils.EMPTY;
        
        if(ftpCount != 0) {
            // get the Unicode value for the letter we need//
            int characterInt = (ftpCount % 26) + 64; 
            ftpCountString = new Character((char)characterInt).toString();
        }
        
        return ftpCountString;
    }
   
   
    /**
     * This method returns a list of PersonalizationVO objects based on the
     * personalization XML stored within the database.
     * @param personalizations Personalization XML
     * @return An ArrayList of PersonalizationVO objects
     * @throws Exception
     */
    private List getPersonalizationList(String personalizations) throws Exception {
        List personalizationList = new ArrayList();
        PersonalizationVO pvo = null;
        
        if(personalizations != null && !personalizations.equals("")) {      
            Document doc = (Document)DOMUtil.getDocument(personalizations);
            Node itemNode =  DOMUtil.selectSingleNode(doc, "personalizations/personalization/name/text()");
            if(itemNode != null) {
                NodeList names = DOMUtil.selectNodes(doc, "personalizations/personalization/name/text()");
                NodeList data = DOMUtil.selectNodes(doc, "personalizations/personalization/data/text()");
                
                for(int i = 0; i < names.getLength(); i++) {
                    pvo = new PersonalizationVO();
                    pvo.setPersonalizationName(names.item(i).getNodeValue());
                    pvo.setPersonalizationData(data.item(i).getNodeValue());
                    personalizationList.add(pvo);
                }
            }
        }
        
        return personalizationList;
    }
}