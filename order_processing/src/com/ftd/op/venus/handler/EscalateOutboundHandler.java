package com.ftd.op.venus.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.venus.bo.VenusOutboundMessageBO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


public class EscalateOutboundHandler extends com.ftd.eventhandling.events.EventHandler 
{

  Logger logger;

  public EscalateOutboundHandler()
  {
    this.logger = new Logger("com.ftd.op.venus.handler.EscalateOutboundHandler");  
  }

    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
        //obtain a database connection
        Connection conn = CommonUtils.getConnection();
        try
        {
          VenusOutboundMessageBO venusOutboundMessageBO = new VenusOutboundMessageBO(conn);
          venusOutboundMessageBO.processOrders();
        }
        catch (Throwable t)
        {
          logger.error(t);
          CommonUtils.sendSystemMessage(t.getMessage());
        }
        finally
        {
          conn.close();
        }

    }
  

}