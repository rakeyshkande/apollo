
package com.ftd.op.venus.handler;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.op.common.dao.QueueDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.vo.QueueVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.EmailVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderTrackingVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.service.VenusService;
import com.ftd.op.venus.to.CancelOrderTO;
import com.ftd.op.venus.to.VenusResultTO;
import com.ftd.op.venus.vo.FileItemVO;
import com.ftd.op.venus.vo.TNMessageVO;
import com.ftd.op.venus.vo.VenusInboundResultVO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;


import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**  This object will be invoked by the Event Handler Framework.
 *   After receiving a message this object will query the TN_MESSAGES table
 *   for any records that have not yet been processed.  Each message will
 *   then be processed.
 */
public class VenusInboundHandler extends com.ftd.eventhandling.events.EventHandler 
{

    private Logger logger;
    private static final String LOGGER_CATEGORY = "com.ftd.op.venus.handler.VenusInboundHandler";
    private static final String POC_EMAIL_TYPE = "Email";
    private static final String SHIPPING_METHOD_NEXTDAY = "ND";
    private static final String VENUS_MESSAGE_TYPE_ORDER = "FTD";
    private static final String VENUS_MESSAGE_TYPE_ANSWER = "ANS";
    private static final String VENUS_SHIP_METHOD_PRIORITY_OVERNIGHT = "PO";
    private static final String ESCLATE_PROCESSING = "ESCALATE";
    private static final String FTP_PROCESSING = "FTP";
    private static final String SYSTEM_CSR = "SYS";
 
    private String RICHLINE_VENDOR_ID;
    
    //List of emails that need to be sent out
    private ArrayList emailList = new ArrayList();


    public VenusInboundHandler() throws Exception
    {
        this.logger =  new Logger(LOGGER_CATEGORY);  
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance(); 
        this.RICHLINE_VENDOR_ID = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "RICHLINE_VENDOR_ID");
    }

     public void invoke(Object payload) throws Throwable
    {
      Connection conn = null;
      try {
        //obtain a database connection
        conn = CommonUtils.getConnection("VENUS_INBOUND_DATABASE_CONNECTION");

        // COMMENTED OUT BELOW IN MERGE ON 1/13/06
        // conn = CommonUtils.getConnection();

        //get text that is part of jms message
        MessageToken messageToken = (MessageToken)payload;
        
        StringTokenizer tokenizer = new StringTokenizer(messageToken.getMessage().toString(),",");

        String messageFlag = tokenizer.nextToken();
        
        //execute the correct process
        if(messageFlag.equals(ESCLATE_PROCESSING))
        {
            executeEscalate(conn);            
        }
        else if(messageFlag.equals(FTP_PROCESSING))
        {
            //get the vendor id, if one exists
            String vendorId = null;            
            if(tokenizer.hasMoreTokens())
            {
                vendorId = tokenizer.nextToken();            
            }
            else
            {
                throw new Exception("Vendor id does not exist in token.");
            }

           executeFTP(conn,vendorId); 
        }
      } catch (Throwable t) {
    	  logger.error(t);
    	  CommonUtils.sendSystemMessage("VenusInboundHandler", t.getMessage());
    	  
      } finally 
      {
        try {
          conn.close();
        } catch(Exception e) 
        {
          logger.warn(e);
        }
      }

    }
    
    
 
    
    
    
    /** This method contains the main flow of logic for processing an inbound
     * message. 
     */
    public void executeEscalate(Connection conn) throws Exception
    {
        
        try{
        
            logger.info("Executing venus inbound.");
        
            //create Venus DAO            
            VenusDAO venusDAO = new VenusDAO(conn);
            QueueDAO queueDAO = new QueueDAO(conn);           
       
            //get list of messages that need to be processed
            List messageList =  venusDAO.getUnprocessedRecords();
       
            //For each record returned
            if(messageList != null)
            {
                TNMessageVO tnMessageVO = null;
            
                logger.info("About to process " + messageList.size() + " messages.");
            
                Iterator iter = messageList.iterator();
                int i = 0;
                while(iter.hasNext())
                {
                    i++;
                    
                    //if an error occurs processing a message set that message
                    //status to error, page support, and continue processing 
                    //the rest of the messages.
                    try
                    {
                
                        //get the message object
                        tnMessageVO = (TNMessageVO)iter.next();
                        
                        VenusMessageVO inboundMessage = createInboundMessage(conn,tnMessageVO);

                        //insert the venus message into the database  
                        String venusMessageId = venusDAO.insertVenusMessage(inboundMessage,VenusConstants.INBOUND_MSG);
                        
                        //process the message
                        logger.debug("Processing TN Message ("+i+"):" + tnMessageVO.getMsgID());
                        VenusInboundResultVO inboundResultVO = processMessage(conn,tnMessageVO,inboundMessage);
                        VenusMessageVO venusMessageVO = inboundResultVO.getVenusMessage();
                        venusMessageVO.setTransmissionTime(new java.util.Date());
                        
                        //update the status of the message
                        venusDAO.updateTNMessageStatus(tnMessageVO.getMsgID(),VenusConstants.TN_MESSAGE_PROCESSED,null);                    
                        venusMessageVO.setVenusStatus(VenusConstants.VENUS_VERIFIED_STATUS);
                        
                        //The communication page requires the comments field in the Venus
                        //table table to contain the message text.
                        venusMessageVO.setComments(venusMessageVO.getMessageText());

                        //update the venus message into the database  
                        venusDAO.updateVenus(venusMessageVO);
                        
                        //if something needs to be queued
                        if(inboundResultVO.getQueue() != null)
                        {
                            QueueVO queueVO = inboundResultVO.getQueue();
                            queueVO.setMercuryId(venusMessageId);
                            queueDAO.insertQueueRecord(queueVO);                            
                        }

                        
                    }//end try  
                    catch(Throwable t)
                    {
                        logger.error(t);
                        
                        String errorMsg = "Error processing an inbound message table from the TN_Messages table.  Message ID:" + tnMessageVO.getMsgID() + " Error:" + t.toString();
                        
                        CommonUtils.sendSystemMessage(errorMsg);

                        //update the status of the message
                        venusDAO.updateTNMessageStatus(tnMessageVO.getMsgID(),VenusConstants.TN_MESSAGE_ERROR,errorMsg);                    
                        
                    }//end catch block                    
                }//end while loop
                
                  //If any emails need to be sent out then send them.
                  //This is done in batch to reduce the number of connections used.
                  if(emailList != null && emailList.size() > 0)
                  {
                      StockMessageGenerator messageGenerator = new StockMessageGenerator();                      
                      messageGenerator.processMessages(emailList);                      
                  }                
                
            }//end if mesage list null
        }//end try block
        catch(Throwable t)
        {
            logger.error(t);            
            String errorMsg = "Error retrieving inbound messages from TN_Messages table.  Error:" + t.toString();            
            CommonUtils.sendSystemMessage(errorMsg); 
        }//end catch
        finally
        {
            logger.info("Inbound processing complete.");
        
            if(conn != null && !conn.isClosed()){
                conn.close();
            }
        }
    }//end execute method
    
    
    private VenusMessageVO createInboundMessage(Connection conn,TNMessageVO tnMessageVO)
    throws Exception
    {
    
         //remove carriage returns from message
        String msg = "";
        StringTokenizer tokenizer1 = new StringTokenizer(tnMessageVO.getMsgText(), "\r\n");
        while (tokenizer1.hasMoreTokens()) {    
           msg = msg + tokenizer1.nextToken();    // process the line
        }
        tnMessageVO.setMsgText(msg);
       
        //Convert the TNmessageVO into a VenusMessageVO
        VenusMessageVO inboundMessage = getVenusMessage(tnMessageVO);

        //Retrieve venus FTD order message related to this message 
        VenusMessageVO venusOrderMessage = getAssociatedOrder(conn,inboundMessage);   
        inboundMessage.setVenusStatus(VenusConstants.VENUS_VERIFIED_STATUS);
        inboundMessage.setReferenceNumber(venusOrderMessage.getReferenceNumber());
        inboundMessage.setOperator(SYSTEM_CSR);
        inboundMessage.setTransmissionTime(new java.util.Date());
        inboundMessage.setFillingVendor(venusOrderMessage.getFillingVendor());
        inboundMessage.setAddress1(venusOrderMessage.getAddress1());
        inboundMessage.setAddress2(venusOrderMessage.getAddress2());
        inboundMessage.setCity(venusOrderMessage.getCity());
        inboundMessage.setState(venusOrderMessage.getState());
        inboundMessage.setOrderDate(venusOrderMessage.getOrderDate());
        inboundMessage.setRecipient(venusOrderMessage.getRecipient());
        inboundMessage.setZip(venusOrderMessage.getZip());
        inboundMessage.setPhoneNumber(venusOrderMessage.getPhoneNumber());
        inboundMessage.setSendingVendor(venusOrderMessage.getFillingVendor());  
        inboundMessage.setComments(msg);
        inboundMessage.setShippingSystem(venusOrderMessage.getShippingSystem());
        inboundMessage.setPersonalGreetingId(venusOrderMessage.getPersonalGreetingId());
        return inboundMessage;
    }    
 
 
    /* This method contains the main flow of logic for processing an 
     * inbound Venus message.  We receive inbound Venus messages in 
     * response to orders set to the Venus application.
     * 
     * The message id of the inbound venus message will be passed into 
     * this method.  This method will then retrieve the inbound venus message 
     * (referred to as TNMessage), the order detail record, and the recipient 
     * record associated with the inbound venus message id.
     *     
     * Each TNMessage contains some message text.  That text determines what 
     * processing will be done.  This method will parse the text and call 
     * the appropriate method to do the processing. (* Note, use regular 
     * expression to do the text parsing)
     * 
     * After the processing of the message is complete, the Venus message 
     * will be inserted into the venus messages table.
     */ 
    private VenusInboundResultVO processMessage(Connection conn,TNMessageVO tnMessageVO, VenusMessageVO venusMessage) throws Exception
    {
 
         //remove carriage returns from message
        String msg = "";
        StringTokenizer tokenizer1 = new StringTokenizer(tnMessageVO.getMsgText(), "\r\n");
        while (tokenizer1.hasMoreTokens()) {    
           msg = msg + tokenizer1.nextToken();    // process the line
        }
        tnMessageVO.setMsgText(msg);
       
        //Retrieve venus FTD order message related to this message 
        VenusMessageVO venusOrderMessage = getAssociatedOrder(conn,venusMessage);   

        //Get the order detail record associated to the TNMessage       
        OrderDAO orderDAO = new OrderDAO(conn);
        OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(venusOrderMessage.getReferenceNumber());
        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());

        //Get the customer record associated with the order
        CustomerVO recipientVO = orderDAO.getCustomer(orderDetailVO.getRecipientId());
        
        // Get the product record associated with the order
        ProductVO productVO = orderDAO.getProduct(orderDetailVO.getProductId());

        
        //Get the message text associated with each of the message types
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String trackingMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRACKING_MESSAGE");
        String printedMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"PRINTED_MESSAGE");
        String shippedMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SHIPPED_MESSAGE");
        String statusMessageStart =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"STATUS_MESSAGE_START");
        String statusMesageEnd = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"STATUS_MESSAGE_END");
        String venusNotProcessed =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_NOT_PROCESSED_MESSAGE");
        String cancelProposed =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"CANCEL_PROPOSED_MESSAGE");
        String venusInvalidService =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_INVALID_SERVICE_MESSAGE");
        String transactionIdError =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRANSACTION_ID_ERROR_MESSAGE");
        String serviceNotAvailableFedEx =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SERVICE_NOT_AVAIL_FEDEX_MSG");        
        String verifyShipMessage =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VERIFY_SHIP_MESSAGE");
        String insertErrorMessage =configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"INSERT_ERROR_MESSAGE");

        QueueVO queueVO = null; 

        //Detemine what the message type is
        if (Pattern.matches(".*" + trackingMessage + ".*", tnMessageVO.getMsgText())){
            logger.debug("Processing tracking number message.");
            queueVO = processTrackingMessage(conn, venusMessage, orderDetailVO, orderVO);            
        }
        else if (Pattern.matches(printedMessage + ".*", tnMessageVO.getMsgText())){
            logger.debug("Processing printed message.");
            queueVO = processOrderPrintedMessage(conn,venusMessage,orderDetailVO);
        }
        else if (Pattern.matches(shippedMessage + ".*", tnMessageVO.getMsgText())){
            logger.debug("Processing shipped message.");
            queueVO = processItemShippedMessage(conn,shippedMessage, orderDetailVO
                ,venusMessage);
        }
        else if (Pattern.matches(statusMessageStart + ".*", tnMessageVO.getMsgText()) &&  Pattern.matches(".*" + statusMesageEnd, tnMessageVO.getMsgText())) {
            logger.debug("Processing status update message.");
            queueVO = processOrderStatusUpdateMessage(conn, statusMessageStart, statusMesageEnd,
                    venusMessage, orderDetailVO);
        }
        else if (Pattern.matches(cancelProposed + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing cancel proposed message.");
            queueVO = processCancelProposedMessage(conn,venusMessage, orderDetailVO);
        }
        else if (Pattern.matches(serviceNotAvailableFedEx + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing unavailable dest message.");
            queueVO = processUnavailableDestinationMessage(conn,
                    venusMessage,orderDetailVO,orderVO, venusOrderMessage,recipientVO);
        }
        else if (Pattern.matches(venusNotProcessed + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing not processed message.");
            queueVO = processOrderNotProcessedMessage(conn, venusMessage, orderDetailVO,orderVO,venusOrderMessage,recipientVO, productVO);
        }
        else if (Pattern.matches(venusInvalidService + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing invalid service message.");
            queueVO = processInvalidServiceType(conn, venusMessage, orderVO, orderDetailVO);
        }
        else if (Pattern.matches(transactionIdError + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing inconsistent transaction ids message.");
            queueVO = processInconsistentTransactionIDs(conn, venusMessage, orderDetailVO,orderVO,venusOrderMessage,recipientVO, productVO);
        }
        else if (Pattern.matches(verifyShipMessage + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing verify ship status message.");
            queueVO = processVerifyShipStatus(conn, venusMessage, venusOrderMessage,orderDetailVO, orderVO,recipientVO, productVO);
        }
        else if (Pattern.matches(insertErrorMessage + ".*", tnMessageVO.getMsgText())) {
            logger.debug("Processing insert error message.");
            queueVO = processInsertErrorMessage(conn, venusMessage, orderDetailVO, orderVO,venusOrderMessage,recipientVO, productVO,recipientVO);
        }
        else if (Pattern.matches("\\d\\d\\d\\d:.*", tnMessageVO.getMsgText()))  {
            logger.debug("Processing fedex message.");
            queueVO = processFedExMessage(conn, tnMessageVO.getMsgText(), venusMessage, orderVO, orderDetailVO);
        }
        else {
            logger.debug("Processing some other message.");        
            queueVO = processOtherMessage(conn, venusMessage, orderVO, orderDetailVO); 
        }
               
        VenusInboundResultVO inboundResultVO = new VenusInboundResultVO();   
        inboundResultVO.setVenusMessage(venusMessage);
        inboundResultVO.setQueue(queueVO);
                   
        return inboundResultVO;               
               
    }
    
    
    /* 
     * This method will parse a tracking number and delivery date out of the 
     * message text.  The order in the database will then be updated with the 
     * tracking number and ship date.  
     * 
     * If the delivery date on the message is greater then the delivery date 
     * on the order then a message will be inserted into a queue.
     */
    private QueueVO processTrackingMessage(Connection conn,VenusMessageVO venusMessageVO, 
                        OrderDetailVO orderDetailVO,OrderVO orderVO)
        throws Exception
    {
            QueueVO retQueue = null;
            
            //var used for string parsing
            int pos = 0;
            
            //create order daos
            OrderDAO orderDAO = new OrderDAO(conn);
            
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
            String trackingMessageDateFormat = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRACKING_MESSAGE_DATE_FORMAT");            
            String trackingMessageDeliveryDateMsg = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_TRACKING_MESSAGE_DEL_DATE");            
            String trackingMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRACKING_MESSAGE");
            String trackingMessageCarrier = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRACKING_MESSAGE_CARRIER");
            String trackingMessageDelivery = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"TRACKING_MESSAGE_DELIVERY_DATE");            

            // Parse tracking number and delivery date  from message
            String carrierId = venusMessageVO.getMessageText().substring(trackingMessageCarrier.length(), venusMessageVO.getMessageText().indexOf(trackingMessage) - 1); 
            pos = venusMessageVO.getMessageText().indexOf(".");
            String trackingNumber = venusMessageVO.getMessageText().substring(venusMessageVO.getMessageText().indexOf(trackingMessage) + trackingMessage.length() + 1,pos);
            pos = venusMessageVO.getMessageText().indexOf(trackingMessageDelivery);
            String deliveryDateUnformatted = venusMessageVO.getMessageText().substring(pos + trackingMessageDelivery.length() + 1);
            
            //format the date into a date object
            SimpleDateFormat sdfSDF = new SimpleDateFormat(trackingMessageDateFormat);
            Date deliveryDateOnMessage = sdfSDF.parse(deliveryDateUnformatted);            
                         
            //Update order with trackingNumber that was parsed from message text.
            OrderTrackingVO trackingVO = new OrderTrackingVO();
            trackingVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
            trackingVO.setTrackingDescription("");
            trackingVO.setTrackingNumber(trackingNumber);
            trackingVO.setCarrierId(carrierId);
            orderDAO.updateTrackingNumber(trackingVO);           
                            
            //Put the message that was received from Venus into the DB
            createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
            
            //create order daos
            VenusDAO venusDAO = new VenusDAO(conn);
            
            String hasCompOrder = venusDAO.checkVenusMessagesForComp(venusMessageVO.getVenusOrderNumber());
            
            // Get the FTD delivery date for the specified venus order number
            Date ftdDeliveryDate = venusDAO.getFTDDeliveryDate(venusMessageVO.getVenusOrderNumber());
        
            //Only do this check if there is not a comp order associated with
            //this venus order number.  Delivery date is not updated on the order
            //details table for vendor comp orders; Defect #2100.
            
            //If the delivery date is later then expected notify the customer
            if( hasCompOrder != null && hasCompOrder.equalsIgnoreCase("N") &&
                deliveryDateOnMessage.after(ftdDeliveryDate))
            {
                createComment(conn,trackingMessageDeliveryDateMsg,orderDetailVO);

                //insert into queue
                retQueue = new QueueVO();
                retQueue.setQueueType(venusMessageVO.getMsgType());
                retQueue.setMessageTimestamp(new java.util.Date());
                retQueue.setMessageType(venusMessageVO.getMsgType());
                retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            }        
            
            return retQueue;
    }
    

    /*
     * This method will update the order detail record to have a disposition of �printed�.
     */
    private QueueVO processOrderPrintedMessage(Connection conn,VenusMessageVO venusMessageVO,OrderDetailVO orderDetailVO)
        throws Exception
    {
        
        //update the order detail disposition
        OrderDAO orderDAO = new OrderDAO(conn);        
        orderDAO.updateOrderDisposition(orderDetailVO.getOrderDetailId(),VenusConstants.PRINTED_DISPOSITION, VenusConstants.VENUS_SYSTEM);

        //set the printed flag on the Venus VO        
        venusMessageVO.setPrinted(true);
        
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
        
        return null;
        
    }
    
    /* 
     * This method will parse out a ship date from the passed in message text.  
     * The order detail record will then be updated with this ship date and 
     * disposition of the order detail will be set to �shipped�.  
     * An Fed Ex confirmation will also be sent out.
     */
    private QueueVO processItemShippedMessage(Connection conn,String shippedMessageKey, OrderDetailVO orderDetailVO
                ,VenusMessageVO venusMessageVO) throws Exception
    {


        // Parse ship date from the message
        String date = venusMessageVO.getMessageText().substring((shippedMessageKey.length()), venusMessageVO.getMessageText().length());
        String inDateFormat = "MM/dd/yyyy";
        SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);                
        Date formattedDate = sdfInput.parse(date);

		//Update order details with ship date
        orderDetailVO.setOrderDispCode(VenusConstants.SHIPPED_DISPOSITION);
        orderDetailVO.setShipDate(formattedDate);
        OrderDAO orderDAO = new OrderDAO(conn);
		orderDAO.updateOrder(orderDetailVO);

		//Send the email confirmation
        sendDropShipEmail(conn,orderDetailVO);

        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
        
        return null;
        
    }
    
    /* This method will extract the status from the text message and then 
     * update the order detail record with that status.
     */
    private QueueVO processOrderStatusUpdateMessage(Connection conn, String statusMessageStart, String statusMessageEnd,
                    VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO) throws Exception
    {
        //Extract status from message.  Message will be the following format:
        //�Order status updated to XXXXXX by end of day process�.  XXXXXX = the status.
        int endPos = venusMessageVO.getMessageText().indexOf(statusMessageEnd);
        String status = venusMessageVO.getMessageText().substring(statusMessageStart.length(), endPos);

        //The first letter of the status should be in caps and all other lettes in lower case
        if(status != null && status.length() > 1)
        {
            status = status.substring(0,1).toUpperCase() + status.substring(1).toLowerCase();
        }
            
        //Update OrderDetailVO with the status found in message
        //update the order detail disposition
        OrderDAO orderDAO = new OrderDAO(conn);      
        try{
            logger.debug("Updating status to " + status);
            orderDAO.updateOrderDisposition(orderDetailVO.getOrderDetailId(),status, VenusConstants.VENUS_SYSTEM);
        }
        catch(SQLException sqle)
        {
            //This is exception is being caught so that it will be logged
            //what status the proc was attempting to put in the order. This
            //could come in handy if we start receving stange status messages
            //that are not accounted for in the DB.
            logger.error("Error setting order status to " + status);
            throw sqle;
        }
               
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
        
        return null;
        
    }
    
    /* When a cancel proposed message is received we just place put the 
     * message into the order comment.
     */
    private QueueVO processCancelProposedMessage(Connection conn, VenusMessageVO
                venusMessageVO, OrderDetailVO orderDetailVO) throws Exception
    {
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);  
        
        return null;
    }
    
    /*
     * Based on product availability this method will either resend the 
     * order with a ship method of �priority overnight� or it will place 
     * a message into the FTD Queue.
     */
    private QueueVO processUnavailableDestinationMessage(Connection conn,
            VenusMessageVO venusMessageVO,OrderDetailVO orderDetailVO,OrderVO orderVO,VenusMessageVO venusOrderMessage, CustomerVO recipientVO)
                throws Exception
    {
        //create daos
        OrderDAO orderDAO = new OrderDAO(conn);
        
        QueueVO retQueue = null;
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         

        boolean canStillDeliver = true;

        //if ship method is next day check if can be shipped out again based on delivery date
        if(orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
            canStillDeliver = canResend(recipientVO.getCountry(),orderDetailVO.getProductId(), recipientVO.getZipCode(), orderDetailVO.getOccasion(),recipientVO.getState() ,orderDetailVO.getDeliveryDate(),orderDetailVO.getShipMethod(), conn);    
        }    
    
        //if the order is being shipped next day
        if(canStillDeliver && orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
        
            logger.debug("Next day order.");
        
            //retrieve the product information            
            ProductVO product = orderDAO.getProduct(orderDetailVO.getProductId());

            //If a next day upgrade available for this product
            if(product.getNextDayUpgradeFlag() != null && product.getNextDayUpgradeFlag().equals("Y")){            
                
                logger.debug("Upgrade available.");
                
                
                
                //Set the ship method on the VenusMessageVO to priority overnight (�PO�).
                venusOrderMessage.setShipMethod(VENUS_SHIP_METHOD_PRIORITY_OVERNIGHT);
                
                //get fedex error message
                String fedexErrorMsg = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_FEDEX_INVALID_SHIP_METHOD");            
                createComment(conn,fedexErrorMsg,orderDetailVO);
                String venusCancelCode = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_CANCEL_CODE");            

                VenusService venusService = new VenusService(conn);

                //update first choice string
                String currentFirstchoice = venusOrderMessage.getFirstChoice();
                int indexOf = currentFirstchoice.indexOf("ND");
                String newFirstChoice = currentFirstchoice.substring(0,indexOf) + "PO" + currentFirstchoice.substring(indexOf + 2,currentFirstchoice.length());
                venusOrderMessage.setFirstChoice(newFirstChoice);

                //cancel the existing order if the inbound is not a reject
                VenusResultTO resultsTO = new VenusResultTO();
                resultsTO.setSuccess(true);
                if(!venusMessageVO.getMsgType().equals("REJ"))
                {
                    CancelOrderTO cancelTO = new CancelOrderTO();
                    cancelTO.setComments("Order automatically cancelled by Venus processing.");
                    cancelTO.setCsr(SYSTEM_CSR);
                    cancelTO.setCancelReasonCode(venusCancelCode);
                    cancelTO.setVenusId(venusOrderMessage.getVenusId());
                    cancelTO.setTransmitCancel(true);
                    resultsTO = venusService.sendCancel(cancelTO);                                           
                }

                
                //if cancelled resend the order
                if(resultsTO.isSuccess())
                {                    
                    //call api to resend the order
                    VenusResultTO venusResultTO = venusService.sendOrder(venusOrderMessage,true,orderDetailVO,orderVO,recipientVO, product,null);
                    String newOrderNumber = venusResultTO.getKey();
                    
                    //add comment indicating the order was resent
                    String message = "Order " + venusMessageVO.getVenusOrderNumber() + " resent as " + newOrderNumber;
                    createComment(conn,message,orderDetailVO);        
                }
                else
                {
                    //add comment indicating the order could not be resent
                    String message = "Venus system could not automatically can and resend order.  " + resultsTO.getErrorString();
                    createComment(conn,message,orderDetailVO);                                    
                
                    //queue it
                    retQueue = new QueueVO();
                    retQueue.setQueueType(venusMessageVO.getMsgType());
                    retQueue.setMessageType(venusMessageVO.getMsgType());
                    retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                    retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                    retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                    retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                    retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                    retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                    retQueue.setMessageTimestamp(new java.util.Date());            

                }

               
            }//end if next day upgrade available
            else { // next day flag not �Y�
            

                //insert into queue
                retQueue = new QueueVO();
                retQueue.setQueueType(venusMessageVO.getMsgType());
                retQueue.setMessageType(venusMessageVO.getMsgType());
                retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                retQueue.setMessageTimestamp(new java.util.Date());
                
                //add comment to order
                createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
            
            }//end next day flag not "Y"
        
        
        } // end if ship method next day                    
        else { //the shipping method is not next day
        

                //insert into queue
                retQueue = new QueueVO();
                retQueue.setQueueType(venusMessageVO.getMsgType());
                retQueue.setMessageType(venusMessageVO.getMsgType());
                retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                retQueue.setMessageTimestamp(new java.util.Date());
                
                //add comment to order
                createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
        
        
        }//end shipping method not next day
        
        return retQueue;
                
    }

    /** Determines what the underlying product id is for the passed
     * product id/sub-code
     * @param productId
     * @param conn
     * @return the product id from ftd_apps.product_master table.
     */
    private String getProductMasterId(String productId, Connection conn) {
        String productMasterId = null;
        VenusDAO venusDAO = new VenusDAO(conn);
        try {
            productMasterId = venusDAO.getProductMasterId(productId);
        } catch (Exception e) {
            productMasterId = productId;
        }
        
        return productMasterId;
    }
    
    private boolean canResend(String country,String product, String zip, String occasion,String state,Date deliveryDate, String deliveryMethod,Connection conn)
    throws Exception
    {
        boolean canResend = false;
    
        //check if the deliery date on the order is still valid
        DeliveryDateUTIL util = new DeliveryDateUTIL();

        //get number of days before delivery date
        long diffMillis = deliveryDate.getTime() - (new Date()).getTime();
        int diffDays = new Long(diffMillis/(24*60*60*1000)).intValue() + 1;  

        OEDeliveryDateParm oeParms = util.getCOMParameterData(country,getProductMasterId(product,conn),zip,"CA", "US", "1", occasion,conn, state, "FTD", false, false,diffDays);        
        if(util.validDelivery(oeParms,deliveryMethod,deliveryDate))
        {
            canResend = true;
        }        
        
        return canResend;
    }
    
    
    /*
     * This method will resend a venus order.
     */
    private QueueVO processOrderNotProcessedMessage(Connection conn, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO, OrderVO orderVO, VenusMessageVO venusOrderMessage, CustomerVO recipientVO, ProductVO productVO) throws Exception
    {                
    
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String venusCancelCode = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_CANCEL_CODE");            
        
        QueueVO retQueue = null;
        
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);

        VenusService venusService = new VenusService(conn);

        //if ship method is next day check if can be shipped out again based on delivery date
        boolean canStillDeliver = true;
        if(orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
            canStillDeliver = canResend(recipientVO.getCountry(),orderDetailVO.getProductId(), recipientVO.getZipCode(), orderDetailVO.getOccasion(),recipientVO.getState() ,orderDetailVO.getDeliveryDate(),orderDetailVO.getShipMethod(), conn);    
        }    

        //cancel the existing order if the inbound is not a reject
        VenusResultTO resultsTO = new VenusResultTO();
        resultsTO.setSuccess(true);
        if(canStillDeliver && !venusMessageVO.getMsgType().equals("REJ"))
        {
            //cancel the existing order
            CancelOrderTO cancelTO = new CancelOrderTO();
            cancelTO.setComments("Order automatically cancelled by Venus processing.");
            cancelTO.setCsr(SYSTEM_CSR);
            cancelTO.setCancelReasonCode(venusCancelCode);
            cancelTO.setVenusId(venusOrderMessage.getVenusId());
            cancelTO.setTransmitCancel(true);
            resultsTO = venusService.sendCancel(cancelTO);                       
        }        
        
        
        if(canStillDeliver && resultsTO.isSuccess())
        {
            //call api to resend the order
            VenusResultTO venusResultTO = venusService.sendOrder(venusOrderMessage,true,orderDetailVO,orderVO,recipientVO, productVO, null);
            String newOrderNumber = venusResultTO.getKey();
                
            //add comment indicating the order was resent
            String message = "Order " + venusMessageVO.getVenusOrderNumber() + " resent as " + newOrderNumber;
            createComment(conn,message,orderDetailVO);        
        }
        else
        {
            //add comment indicating the order could not be resent
            String message = "Venus system could not automatically can and resend order.  " + resultsTO.getErrorString();
            createComment(conn,message,orderDetailVO);                                    
        
            //queue it
            retQueue = new QueueVO();
            retQueue.setQueueType(venusMessageVO.getMsgType());
            retQueue.setMessageType(venusMessageVO.getMsgType());
            retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
            retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
            retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
            retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            retQueue.setMessageTimestamp(new java.util.Date());            
        }

        return retQueue;
    }
    
    /*
     * This method will insert a message into the FTD Queue.
     */
    private QueueVO processInvalidServiceType(Connection conn, VenusMessageVO venusMessageVO, OrderVO orderVO, OrderDetailVO orderDetailVO) throws Exception
    {

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String invalidServiceMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_INVALID_SERVICE_MESSAGE");                      
    
         QueueVO retQueue = null;
    
        //insert into queue
        retQueue = new QueueVO();
        retQueue.setQueueType(venusMessageVO.getMsgType());
        retQueue.setMessageType(venusMessageVO.getMsgType());
        retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
        retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
        retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
        retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
        retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
        retQueue.setMessageTimestamp(new java.util.Date());

        //change the venus message text
        venusMessageVO.setMessageText(invalidServiceMessage);
                
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);
        return retQueue;
    }
    
    /*
     * This method will resend a venus order.
     */
    private QueueVO processInconsistentTransactionIDs(Connection conn, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO, OrderVO orderVO,VenusMessageVO venusOrderMessage, CustomerVO recipientVO, ProductVO productVO)
        throws Exception
    {
    
        QueueVO retQueue = null;
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String venusCancelCode = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_CANCEL_CODE");                            
        
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO); 

        VenusService venusService = new VenusService(conn);

        //if ship method is next day check if can be shipped out again based on delivery date
        boolean canStillDeliver = true;
        if(orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
            canStillDeliver = canResend(recipientVO.getCountry(),orderDetailVO.getProductId(), recipientVO.getZipCode(), orderDetailVO.getOccasion(),recipientVO.getState() ,orderDetailVO.getDeliveryDate(),orderDetailVO.getShipMethod(), conn);    
        }    

        //cancel the existing order if the inbound is not a reject
        VenusResultTO resultsTO = new VenusResultTO();
        resultsTO.setSuccess(true);
        if(canStillDeliver && !venusMessageVO.getMsgType().equals("REJ"))
        {
            //cancel the existing order
            CancelOrderTO cancelTO = new CancelOrderTO();
            cancelTO.setComments("Order automatically cancelled by Venus processing.");
            cancelTO.setCsr(SYSTEM_CSR);
            cancelTO.setCancelReasonCode(venusCancelCode);
            cancelTO.setVenusId(venusOrderMessage.getVenusId());
            cancelTO.setTransmitCancel(true);
            resultsTO = venusService.sendCancel(cancelTO);                       
        }                
        
        
        if(canStillDeliver && resultsTO.isSuccess())
        {
            //call api to resend the order
            VenusResultTO venusResultTO = venusService.sendOrder(venusOrderMessage,true,orderDetailVO,orderVO,recipientVO, productVO, null);
            String newOrderNumber = venusResultTO.getKey();
                
            //add comment indicating the order was resent
            String message = "Order " + venusMessageVO.getVenusOrderNumber() + " resent as " + newOrderNumber;
            createComment(conn,message,orderDetailVO);        
        }
        else
        {
            //add comment indicating the order could not be resent
            String message = "Venus system could not automatically can and resend order.  " + resultsTO.getErrorString();
            createComment(conn,message,orderDetailVO);                                    
        
            //queue it
            retQueue = new QueueVO();
            retQueue.setQueueType(venusMessageVO.getMsgType());
            retQueue.setMessageType(venusMessageVO.getMsgType());
            retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
            retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
            retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
            retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            retQueue.setMessageTimestamp(new java.util.Date());            

        }                 
        

        return retQueue;

    }
    
    /*
     * This method will find out how many times an order was sent to Venus.  
     * If the number of times it was sent out is less then a global setting 
     * the order will be sent again.  Otherwise a message will be 
     * placed in the FTD Queue.
     */
    private QueueVO processVerifyShipStatus(Connection conn, VenusMessageVO venusMessageVO, VenusMessageVO venusOrderMessage,OrderDetailVO orderDetailVO, OrderVO orderVO, CustomerVO recipientVO, ProductVO productVO) throws Exception
    {
        //needed daos
        VenusDAO venusDAO = new VenusDAO(conn);
        OrderDAO orderDAO = new OrderDAO(conn);
        
        QueueVO retQueue = null;
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String verifyShipComment = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_VERIFY_SHIP_STATUS");                      
        
        //Find out how many times this order has been sent out.
        int timesSent = venusDAO.getNumberOfTimesSent(venusMessageVO.getReferenceNumber(),VENUS_MESSAGE_TYPE_ORDER);
        
        //Get the number of times a message should be resent.
        GlobalParameterVO globalVO = orderDAO.getGlobalParameter("ORDER_PROCESSING_VENUS","VENUS_FTD_RETRY_AMOUNT");
        int retryMax = Integer.parseInt(globalVO.getValue());                     
        
        //add comment to order
        createComment(conn,verifyShipComment,orderDetailVO);        

        //if ship method is next day check if can be shipped out again based on delivery date
        boolean canStillDeliver = true;
        if(orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
            canStillDeliver = canResend(recipientVO.getCountry(),orderDetailVO.getProductId(), recipientVO.getZipCode(), orderDetailVO.getOccasion(),recipientVO.getState() ,orderDetailVO.getDeliveryDate(),orderDetailVO.getShipMethod(), conn);    
        }    

        //If we tried the max amount of times
        if(!canStillDeliver || timesSent >= retryMax)
        {
           logger.info("Order cannot be resent, queueing. timesSent=" + timesSent + "retryMax=" + retryMax);
        
            //queue it
            //insert into queue
            retQueue = new QueueVO();
            retQueue.setQueueType(venusMessageVO.getMsgType());
            retQueue.setMessageType(venusMessageVO.getMsgType());
            retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
            retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
            retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
            retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            retQueue.setMessageTimestamp(new java.util.Date());            

        }
        else
        {
            //resend the order
            
            String venusCancelCode = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_CANCEL_CODE");                            
            
            logger.info("Resending Order. timesSent=" + timesSent + "retryMax=" + retryMax);

            //call api to resend the order
            VenusService venusService = new VenusService(conn);

            //cancel the existing order if the inbound is not a reject
            VenusResultTO resultsTO = new VenusResultTO();
            resultsTO.setSuccess(true);
            if(!venusMessageVO.getMsgType().equals("REJ"))
            {
                //cancel the existing order
                CancelOrderTO cancelTO = new CancelOrderTO();
                cancelTO.setComments("Order automatically cancelled by Venus processing.");
                cancelTO.setCsr(SYSTEM_CSR);
                cancelTO.setCancelReasonCode(venusCancelCode);
                cancelTO.setVenusId(venusOrderMessage.getVenusId());
                cancelTO.setTransmitCancel(true);
                resultsTO = venusService.sendCancel(cancelTO);  
            }                

            //if the order could be cancelled then resend it
            if(resultsTO.isSuccess())
            {
                VenusResultTO venusResultTO = venusService.sendOrder(venusOrderMessage,true,orderDetailVO,orderVO,recipientVO, productVO, null);
                String newOrderNumber = venusResultTO.getKey();
                
                //add comment indicating the order was resent
                String message = "Order " + venusMessageVO.getVenusOrderNumber() + " resent as " + newOrderNumber;
                createComment(conn,message,orderDetailVO);                        
            }
            else
            {
                //add comment indicating the order was resent
                String message = "Venus system could not automatically can and resend order.  " + resultsTO.getErrorString();
                createComment(conn,message,orderDetailVO);                                    
            
                //queue it
                retQueue = new QueueVO();
                retQueue.setQueueType(venusMessageVO.getMsgType());
                retQueue.setMessageType(venusMessageVO.getMsgType());
                retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                retQueue.setMessageTimestamp(new java.util.Date());            

            }
           



        }//end if retry attempts done

        return retQueue;
        
    }
    

    /*
     * This method will find out how many times an order was sent to Venus.  
     * If the number of times it was sent out is less then a global setting 
     * the order will be sent again.  Otherwise a message will be 
     * placed in the FTD Queue.
     */
    private QueueVO processInsertErrorMessage(Connection conn, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO, OrderVO orderVO, VenusMessageVO venusOrderMessage, CustomerVO buyerVO, ProductVO productVO,CustomerVO recipientVO) throws Exception
    {
        //needed daos
        VenusDAO venusDAO = new VenusDAO(conn);
        OrderDAO orderDAO = new OrderDAO(conn);
        
        QueueVO retQueue = null;
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String verifyShipComment = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_VERIFY_SHIP_STATUS");                      
        
        //Find out how many times this order has been sent out.
        int timesSent = venusDAO.getNumberOfTimesSent(venusMessageVO.getReferenceNumber(),VENUS_MESSAGE_TYPE_ORDER);
        
        //Get the number of times a message should be resent.
        GlobalParameterVO globalVO = orderDAO.getGlobalParameter("ORDER_PROCESSING_VENUS","VENUS_FTD_RETRY_AMOUNT");
        int retryMax = Integer.parseInt(globalVO.getValue());        
        
        //add comment to order
        createComment(conn,verifyShipComment,orderDetailVO);        

        //If we tried the max amount of times
        if(timesSent >= retryMax)
        {
            logger.info("Order cannot be resent, queueing. timesSent=" + timesSent + "retryMax=" + retryMax);        
        
            //queue it
            //insert into queue
            retQueue = new QueueVO();
            retQueue.setQueueType(venusMessageVO.getMsgType());
            retQueue.setMessageType(venusMessageVO.getMsgType());
            retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
            retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
            retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
            retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
            retQueue.setMessageTimestamp(new java.util.Date());            

        }
        else
        {
            //resend the order
            
            String venusCancelCode = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_CANCEL_CODE");                            
            
            logger.info("Resending Order. timesSent=" + timesSent + "retryMax=" + retryMax);            
            
            //call api to resend the order
            VenusService venusService = new VenusService(conn);
            
            //if ship method is next day check if can be shipped out again based on delivery date
            boolean canStillDeliver = true;
            if(orderDetailVO.getShipMethod().equals(SHIPPING_METHOD_NEXTDAY)){
                canStillDeliver = canResend(recipientVO.getCountry(),orderDetailVO.getProductId(), recipientVO.getZipCode(), orderDetailVO.getOccasion(),recipientVO.getState() ,orderDetailVO.getDeliveryDate(),orderDetailVO.getShipMethod(), conn);    
            }                
            
            //cancel the existing order if the inbound is not a reject
            VenusResultTO resultsTO = new VenusResultTO();
            resultsTO.setSuccess(true);
            if(canStillDeliver && !venusMessageVO.getMsgType().equals("REJ"))
            {
                //cancel the existing order
                CancelOrderTO cancelTO = new CancelOrderTO();
                cancelTO.setComments("Order automatically cancelled by Venus processing.");
                cancelTO.setCsr(SYSTEM_CSR);
                cancelTO.setCancelReasonCode(venusCancelCode);
                cancelTO.setVenusId(venusOrderMessage.getVenusId());
                cancelTO.setTransmitCancel(true);
                resultsTO = venusService.sendCancel(cancelTO);  
            }                


            //if the order could be cancelled then resend it
            if(canStillDeliver && resultsTO.isSuccess())
            {
                VenusResultTO venusResultTO = venusService.sendOrder(venusOrderMessage,true,orderDetailVO,orderVO,buyerVO, productVO, null);
                String newOrderNumber = venusResultTO.getKey();
                
                //add comment indicating the order was resent
                String message = "Order " + venusMessageVO.getVenusOrderNumber() + " resent as " + newOrderNumber;
                createComment(conn,message,orderDetailVO);                        
            }
            else
            {
                //add comment indicating the order was resent
                String message = "Venus system could not automatically can and resend order.  " + resultsTO.getErrorString();
                createComment(conn,message,orderDetailVO);                                    
            
                //queue it
                retQueue = new QueueVO();
                retQueue.setQueueType(venusMessageVO.getMsgType());
                retQueue.setMessageType(venusMessageVO.getMsgType());
                retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
                retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
                retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                retQueue.setMessageTimestamp(new java.util.Date());            

            }

        }//end if retry attempts done

        return retQueue;
    }
    
    /*
     * This method will check if the passed in message is located on 
     * the FedEx do not page table.  If the number is on the list then a 
     * message will be placed into the FTD Queue and nothing else is done.  
     * If the number is not on the list then FTD support will be paged and 
     * the message will be placed in the FTD queue.
     */
    private QueueVO processFedExMessage(Connection conn, String message, VenusMessageVO venusMessageVO, OrderVO orderVO, OrderDetailVO orderDetailVO)
        throws Exception
    {
        //daos
        VenusDAO venusDAO = new VenusDAO(conn);

        QueueVO retQueue = null;

        //Parse out the first 4 chars from the messages text.	
        //This will be the message Fed Ex message number.		
        String fedExCode = message.substring(0,4);

        //create queue and order comment text
        String errorMessage =  "Fedex error on order " + venusMessageVO.getVenusOrderNumber() + ":" + venusMessageVO.getMessageText();
        String commentMessage = errorMessage;
      
        //If message is not part of do not pagelist
        if(!venusDAO.existsOnDoNotPageList(fedExCode))
        {       
            //get message from config file
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
            String itPagedMessage = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"COMMENT_FEDEX_IT_NOTIFIED");                      

            //append paged text to message
            commentMessage = commentMessage + " " + itPagedMessage;

        	//Send out system notification with text of supportMessage
             CommonUtils.sendSystemMessage(errorMessage);
			
        }

        //add comment to order
        createComment(conn,commentMessage,orderDetailVO);       
        
        //insert into queue
        retQueue = new QueueVO();
        retQueue.setQueueType(venusMessageVO.getMsgType());
        retQueue.setMessageType(venusMessageVO.getMsgType());
        retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
        retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
        retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
        retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
        retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
        retQueue.setMessageTimestamp(new java.util.Date());        
        
        return retQueue;
    }
    
    /*
     * Any message text that we do not recognize should cause a message to 
     * be placed into the Queue.
     */
    private QueueVO processOtherMessage(Connection conn, VenusMessageVO venusMessageVO, OrderVO orderVO, OrderDetailVO orderDetailVO)
            throws Exception
    {
        //add comment to order
        createComment(conn,venusMessageVO.getMessageText(),orderDetailVO);       
        
        QueueVO retQueue = null;
        
        //insert into queue
        retQueue = new QueueVO();
        retQueue.setQueueType(venusMessageVO.getMsgType());
        retQueue.setMessageType(venusMessageVO.getMsgType());
        retQueue.setSystem(VenusConstants.VENUS_SYSTEM);
        retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
        retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
        retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
        retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
        retQueue.setMessageTimestamp(new java.util.Date());        
        
        return retQueue;
    }
    
    /*
     * This method process inbound messages for FTP clients.  Currenltly the only
     * FTP partner is Wine.com.  This method is written with wine.com in mind,
     * but some parts are made generic so that may be a reduced amount of work
     * if another FTP partner is added.  This proc is wine.com specfic though.
     */
    public void executeFTP(Connection conn,String vendorId) throws Exception
    {
    
        try{
    
            logger.info("Inbound FTP processing. Vendor=" + vendorId);
    
            //get ftp config data
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
            String remoteLocation = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_REMOTE_LOCATION_" + vendorId);                      
            String remoteFileName = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"INBOUND_REMOTE_FILENAME_" + vendorId);
            String ftpLogon = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT,"INBOUND_FTP_LOGON_" + vendorId);
            String ftpPassword = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT,"INBOUND_FTP_PASSWORD_" + vendorId);
            String remoteDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_REMOTE_DIRECTORY_" + vendorId);
            String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_LOCAL_DIRECTORY_" + vendorId);
            String localFileName = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_LOCAL_FILENAME_" + vendorId);
                            
            //get list of files in directory       
            FTPClient ftpClient = new FTPClient(remoteLocation);
            ftpClient.login(ftpLogon,ftpPassword);
            
            String[] files = null;
            try{
                //When files do not exist in the specified dir this object throws
                //an exception.  We do not consider this an error because this
                //dir will not always contain files.
                files = ftpClient.dir(remoteDir + remoteFileName); 
            }
            catch(FTPException ftpe)
            {
                logger.info("Files do not exist in directory.");
                logger.info(ftpe);
            }
            
            //if nothing was found exit
            if(files == null || files.length == 0)
            {
                return;
            }
            
            //insert a record to indicate that we got a file
            VenusDAO venusDAO = new VenusDAO(conn);
            venusDAO.insertTransmissionHistory(vendorId,"TRACKING_FILE");            
            
            //get current date and time to be used in file name
            SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHMMSS");
            String dateString = sdf.format(new java.util.Date());
            
            //process each file
            for(int i = 0;i < files.length;i++)
            {
                //get the file from the remote location
                String localFile = localDir + localFileName + "_" + dateString + "_" + i+1 + ".csv";
                
                logger.info("Local File:"+localFile);
                
                ftpClient.get(localFile,files[i]); 
                
                //remove the file from the remote directory
                ftpClient.delete(files[i]);
                
                //process file
                processFTPFile(conn,localFile,vendorId);
            }
        }
        catch(Throwable e)
        {
            String msg = "Error occured while processing inbound vendor file.";
            logger.error(msg);
            logger.error(e);
            CommonUtils.sendSystemMessage(msg + ":" + e.toString());            
        }
        finally
        {
            //close connection if open
            if(conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        
    }
    
    /*
     * Process a file that received from an FTP vendor
     */
    private void processFTPFile(Connection conn,String fileName,String vendorId) throws Exception
    {
    	//get an input stream for the file
    	BufferedReader br = new BufferedReader(new FileReader(fileName));

    	VenusDAO venusDAO = new VenusDAO(conn);
    	OrderDAO orderDAO = new OrderDAO(conn);
    	FileItemVO fileItemVO = null;

    	//read the file line by line
    	String line = "";
    	while( (line = br.readLine()) != null)
    	{       

    		try{
    			//parse the file line
    			fileItemVO = parseFTPline(line, vendorId);

    			//Fix for 17939 
    			if(!validateFileItemVO(fileItemVO))
    			{
    				String msg = "Processing inbound vendor("+vendorId+") tracking file.  Invalid order in file: Order details ----- " + line + " ----- Processing will continue.";
    				logger.error(msg);           
    				CommonUtils.sendSystemMessage(msg);  
    			}
    			else
    			{
    				//if a valid line was found
    				if(fileItemVO != null)
    				{           

    					//obtain order detail id from external order number
    					Map orderNumberMap = orderDAO.findOrderNumber(fileItemVO.getOrderNumber());
    					String externalOrderNumber = orderNumberMap.get("OUT_ORDER_DETAIL_ID").toString();                

    					//get the associated venus order message
    					VenusMessageVO venusMessageVO = getAssociatedOrder(conn,externalOrderNumber);

    					//check if the order was found
    					if(venusMessageVO == null)
    					{
    						String msg = "Processing inbound vendor("+vendorId+") tracking file.  Invalid order in file: Order details ----- " + line + " ----- Processing will continue.";
    						logger.error(msg);           
    						CommonUtils.sendSystemMessage(msg);  
    					}             
    					else
    					{                
    						//update the order details table with the ship date
    						OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(venusMessageVO.getReferenceNumber());
    						orderDetailVO.setOrderDetailId(Long.parseLong(venusMessageVO.getReferenceNumber()));
    						orderDetailVO.setShipDate(fileItemVO.getShipDate());
    						orderDetailVO.setOrderDispCode(VenusConstants.SHIPPED_DISPOSITION);
    						orderDAO.updateOrder(orderDetailVO);

    						//update the tracking number in the DB
    						OrderTrackingVO trackingVO = new OrderTrackingVO();
    						trackingVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
    						trackingVO.setTrackingNumber(fileItemVO.getTrackingNumber());
    						orderDAO.updateTrackingNumber(trackingVO);

    						//make tracking message
    						SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMMMM dd, yyyy");            
    						String shipDate = sdf.format(fileItemVO.getShipDate());  
    						sdf = new SimpleDateFormat("EEE, ddMMMyy");
    						// Q2SP17-18
    						//String deliveryDate = sdf.format(fileItemVO.getShipDate());
    						String deliveryDate = sdf.format(orderDetailVO.getDeliveryDate());
    						String trackingMsg = "Your FedEx tracking number is "+trackingVO.getTrackingNumber()+". Your order is scheduled to ship on "+shipDate+"  for delivery on " + deliveryDate;

    						//update fields in the venus VO
    						venusMessageVO.setShipDate(fileItemVO.getShipDate());
    						venusMessageVO.setMsgType(VENUS_MESSAGE_TYPE_ANSWER);
    						venusMessageVO.setVenusStatus(VenusConstants.VENUS_VERIFIED_STATUS);
    						venusMessageVO.setOperator(VenusConstants.VENUS_SYSTEM);
    						venusMessageVO.setMessageText(trackingMsg);            

    						//insert record inoto venus table

    						//The communication page requires the comments field in the Venus
    						//table table to contain the message text.
    						venusMessageVO.setComments(venusMessageVO.getMessageText());

    						venusDAO.insertVenusMessage(venusMessageVO,VenusConstants.INBOUND_MSG);
    					}//end order found
    				}//end file contains valid line
    			}
    		}
    		catch(Exception e)
    		{
    			String msg = "Processing inbound vendor("+vendorId+") tracking file.  Invalid order in file: Order details ----- " + line + " ----- Processing will continue.";
    			logger.error(msg);           
    			CommonUtils.sendSystemMessage(msg);  
    		}
    	}//end while loop
    }//end proc

    /*
     * Parse a line from a wine.com file into a VO
     */
    private FileItemVO parseFTPline(String line, String vendorId) throws Exception
    {
        FileItemVO fileItemVO = new FileItemVO();
        
        //if the line does not contain data then exit
        if(line == null || line.trim().length() == 0)
        {
            return null;
        }
        
        //tokenize the string based on commas
        StringTokenizer tokenizer = new StringTokenizer(line,",");
        SimpleDateFormat sdfForNormalVendors = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat sdfForRichline = new SimpleDateFormat("MMddyyyy");
        
        //get the items from the line
        //if it is a richline vendor, parse the dates differently
        if(RICHLINE_VENDOR_ID.equalsIgnoreCase(vendorId)){
            fileItemVO.setOrderNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
            fileItemVO.setDateReceived(sdfForRichline.parse(FieldUtils.replaceAll(tokenizer.nextToken(),"\"","")));
            fileItemVO.setPartnerOrderNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
            fileItemVO.setShipDate(sdfForRichline.parse(FieldUtils.replaceAll(tokenizer.nextToken(),"\"","")));
            fileItemVO.setTrackingNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
        }
        else{
	        fileItemVO.setOrderNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
	        fileItemVO.setDateReceived(sdfForNormalVendors.parse(FieldUtils.replaceAll(tokenizer.nextToken(),"\"","")));
	        fileItemVO.setPartnerOrderNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
	        fileItemVO.setShipDate(sdfForNormalVendors.parse(FieldUtils.replaceAll(tokenizer.nextToken(),"\"","")));
	        fileItemVO.setTrackingNumber(FieldUtils.replaceAll(tokenizer.nextToken(),"\"",""));
        }        
       
        return fileItemVO;
    }
    
    /**
     * This method is used to check if the inboound file has required fields
     */
    private boolean validateFileItemVO(FileItemVO fileItemVo)
    {
    	boolean validFileItem = false;
    	if (fileItemVo != null)
    	{
    		if ( fileItemVo.getOrderNumber() != null && !fileItemVo.getOrderNumber().trim().equals("")
    				&&  fileItemVo.getShipDate() != null 
    				&&  fileItemVo.getTrackingNumber() != null && !fileItemVo.getTrackingNumber().trim().equals("")
    				&&  fileItemVo.getPartnerOrderNumber() != null && !fileItemVo.getPartnerOrderNumber().trim().equals("")
    		)
    		{
    			validFileItem = true;
    		}
    	}
    	return validFileItem;
    }
    
    private void sendDropShipEmail(Connection conn,OrderDetailVO orderDetailVO) throws Exception
    {
     
        //get the sender email address from config file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String fromEmailAddress = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"FEDEX_EMAIL_FROM_ADDRESS");                 
        String fedExEmailTemplate = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"FEDEX_EMAIL_TEMPLATE_ID");                 

        //Get data needed for email
        OrderDAO orderDAO = new OrderDAO(conn);
        OrderVO orderVO = orderDAO.getOrder(orderDetailVO.getOrderGuid());
        CustomerVO buyerVO = orderDAO.getCustomer(orderVO.getCustomerId());
        EmailVO emailVO = orderDAO.getCustomerEmailInfo(buyerVO.getCustomerId(),orderVO.getCompanyId());
    
        //if email info was not found then do not send out email
        if(emailVO == null)
        {
            logger.info("Email infomration was not found for customer " + buyerVO.getCustomerId() + " for company " + orderVO.getCompanyId() + ".  Tracking number will not be emailed.");
            return;
        }

        CompanyVO companyVO = orderDAO.getCompany(orderVO.getCompanyId());


        //get  tracking vo from order
        OrderTrackingVO trackingVO = orderDAO.getTrackingInfo(orderDetailVO.getOrderDetailId());
                   
        //if tracking number does not exist on order exit
        if(trackingVO == null)
        {
            String errorMsg = "Tracking number was not found for order detail id " + orderDetailVO.getOrderDetailId();
            throw new Exception(errorMsg);
        }
                   
        //create poc xml
        Document xml = DOMUtil.getDocument();
        addElement(xml,"carrier_name",trackingVO.getCarrierName());
        addElement(xml,"carrier_url",trackingVO.getCarrierURL());
        addElement(xml,"carrier_phone",trackingVO.getCarrierPhone());
        addElement(xml,"tracking_number",trackingVO.getTrackingNumber());
        addElement(xml,"external_order_number",orderDetailVO.getExternalOrderNumber());
        addElement(xml,"company_name",companyVO.getCompanyName());
        addElement(xml,"company_url",companyVO.getURL());

        Element root = xml.createElement("root");
        xml.appendChild(root);
        Element ordersNode = xml.createElement("ORDERS");
        root.appendChild(ordersNode);
        Element orderNode = xml.createElement("ORDER");
        ordersNode.appendChild(orderNode);
        Element value = xml.createElement("language_id");
        orderNode.appendChild(value);
        value.appendChild(xml.createTextNode(orderVO.getLanguageId()));

        //dump out xml for debugging
        logger.debug(convertDocToString(xml));

        //create a point of contact VO
        PointOfContactVO pocVO = new PointOfContactVO();
        pocVO.setCompanyId(orderVO.getCompanyId());
        pocVO.setSenderEmailAddress(fromEmailAddress + "@" + companyVO.getURL());
        pocVO.setRecipientEmailAddress(emailVO.getEmailAddress());
        pocVO.setTemplateId(null);
        pocVO.setLetterTitle(fedExEmailTemplate);
        pocVO.setPointOfContactType(POC_EMAIL_TYPE);
        pocVO.setCommentType("Order");

        pocVO.setDataDocument(xml);     
     
        emailList.add(pocVO);
     
    }
    
    
    private void addElement(Document xml, String keyName, String value)
    {
        Element nameElement = xml.createElement(keyName);
        
        if(value != null && value.length() > 0 )
        {
            nameElement.appendChild(xml.createTextNode(value));
        }
        xml.getDocumentElement().appendChild(nameElement);    
    }    
    
    /*
     * Insert a comment into the DB
     */
    private void createComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
        throws Exception
    {
            OrderDAO orderDAO = new OrderDAO(conn);    
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(VenusConstants.ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCommentType(VenusConstants.COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(VenusConstants.ORDER_PROCESSING_COMMENT_ORIGIN);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(commentsVO);
    }

    /*
     * Get the order message assocated with the past in venus messageVO
     */
    private VenusMessageVO getAssociatedOrder(Connection conn,VenusMessageVO venusMessageVO) throws Exception
    {
        VenusDAO venusDAO = new VenusDAO(conn);
    
        //Retrieve venus FTD order message related to this message 
        List venusList = venusDAO.getVenusMessage(venusMessageVO.getVenusOrderNumber(), VENUS_MESSAGE_TYPE_ORDER);
        if(venusList == null || venusList.size() == 0)
        {
            throw new Exception("Corresponding order messge could not be found for Venus order " + venusMessageVO.getVenusOrderNumber());
        }
        else
        {
            if(venusList.size() > 1)
            {
                throw new Exception("Multiple FTDs were found for the for Venus order " + venusMessageVO.getVenusOrderNumber());                        
            }
        }
        VenusMessageVO foundOrder = (VenusMessageVO)venusList.get(0);    
        
        return foundOrder;
    }

    /*
     * Get the order message assocated with the past in venus messageVO
     */
    private VenusMessageVO getAssociatedOrder(Connection conn,String externalOrderNumber) throws Exception
    {
        VenusDAO venusDAO = new VenusDAO(conn);
    
        VenusMessageVO foundOrder = null;
    
        //Retrieve venus FTD order message related to this message 
        List venusList = venusDAO.getVenusMessageByDetailId(externalOrderNumber, VENUS_MESSAGE_TYPE_ORDER);

        if(venusList == null || venusList.size() == 0)
        {
            //return null            
        }
        else
        {
            if(venusList.size() > 1)
            {
                throw new Exception("Multiple FTDs were found for the for external order number " + externalOrderNumber);                        
            }
            
            foundOrder = (VenusMessageVO)venusList.get(0);    
        }
        
        return foundOrder;
    }

    
    
    /*
     * Create a Venus message using the past in TNMesageVO
     */
     private VenusMessageVO getVenusMessage(TNMessageVO tnMessageVO)
     {
        VenusMessageVO venusMessageVO = new VenusMessageVO();
        venusMessageVO.setVenusOrderNumber(tnMessageVO.getMsgPOConfNbr());
        venusMessageVO.setMsgType(tnMessageVO.getMsgType());
        venusMessageVO.setPrice(tnMessageVO.getMsgOrigOrderAmt());
        venusMessageVO.setOverUnderCharge(tnMessageVO.getMsgAdjAmount());
        venusMessageVO.setMessageText(tnMessageVO.getMsgText());
        return venusMessageVO;
     }
 
 
     /** This method converts an XML document to a String. 
     * @param xmlDoc document to convert 
     * @return String version of XML */
    private String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";      
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }      
        
    public static void main(String[] args)
    {
    try
    {
//      String driver_ = "oracle.jdbc.driver.OracleDriver";
//      String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1522:QA6";
//      String user_ = "jweiss";
//      String password_ = "jweiss";
//      
//      String testa = "A";      
//      String testb = null;
//      
//      if(!testa.equalsIgnoreCase(testb))
//      {
// //         System.out.println("not equal");
//      }
//
//     Class.forName(driver_);                         
//     Connection conn = DriverManager.getConnection(database_, user_, password_);     
    
     
     VenusInboundHandler v = new VenusInboundHandler();
     Connection conn = v.testConnection();
     GregorianCalendar cal = new GregorianCalendar();
     cal.set(Calendar.DAY_OF_MONTH,9);
     cal.set(Calendar.HOUR,0);
     cal.set(Calendar.MINUTE,0);
     cal.set(Calendar.SECOND,0);
     cal.set(Calendar.MILLISECOND,0);
     cal.set(Calendar.AM_PM,Calendar.AM);
     SimpleDateFormat format = new SimpleDateFormat();
     System.out.println(format.format(cal.getTime()));
     //VenusDAO venusDAO = new VenusDAO(conn);
     //String productid = venusDAO.getProductMasterId("F313");
     //System.out.println(v.canResend("US","335F", "864016811", "3","AZ",cal.getTime(), "ND",conn)); //false
     //System.out.println(v.canResend("US","X538", "136403152", "3","NY",cal.getTime(), "ND",conn)); //true
     //System.out.println(v.canResend("US","335F", "34736", "3","FL",cal.getTime(), "ND",conn)); //false
     System.out.println(v.canResend("US","F313", "22485", "3","FL",cal.getTime(), "ND",conn)); //false
/*
     v.executeEscalate(conn);
*/
/*
        OrderDAO orderDAO = new OrderDAO(conn);
        String orderDetailId = "1458935";
        OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(orderDetailId);
        orderDetailVO.setOrderDetailId(Long.parseLong(orderDetailId));
        orderDetailVO.setShipDate(new java.util.Date());
        orderDAO.updateOrder(orderDetailVO);
        */
     
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
    }
    
    private Connection testConnection() throws Exception
    {
        String driver_ = "oracle.jdbc.driver.OracleDriver";
        String database_ = "jdbc:oracle:thin:@ithaca.ftdi.com:1521:zeus1";
        String user_ = "osp";
        String password_ = "osp";
        
        String testa = "A";      
        String testb = null;
        
        if(!testa.equalsIgnoreCase(testb))
        {
        //         System.out.println("not equal");
        }
        
        Class.forName(driver_);               
        
        return DriverManager.getConnection(database_, user_, password_);     
    }
    
}
