package com.ftd.op.venus.handler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderTrackingVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.vo.PersonalCreationsInboundVO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


/**
 * This class handles inbound files sent from Personal Creations and updates
 * the proper order tables and venus tables.  Order receipt acknowledgements
 * and tracking number/ship date are received as separate records.
 *
 * @author Andy Liakas updated by Mike Kruger
 */
public class PersonalCreationsInboundHandler extends com.ftd.eventhandling.events.EventHandler {
    private static final String VENUS_MESSAGE_TYPE_ORDER = "FTD";
    
    private Connection conn;        // Database connection
    private Logger logger;          // Log4J logger

     
    public PersonalCreationsInboundHandler() throws Exception {
        this.logger = new Logger("com.ftd.op.venus.handler.PersonalCreationsInboundHandler");  
    }


    /** 
     * This method is invoked by the event handler framework. 
     */
    public void invoke(Object payload) throws Throwable {
        MessageToken messageToken = (MessageToken) payload;
        String message = (String)messageToken.getMessage();

        conn = CommonUtils.getConnection();
        try {
            process(message);
        }
        catch (Throwable t) {
            String msg = "Error occurred while processing Personal Creations inbound files:" + t.getMessage();
            logger.error(msg, t);
            CommonUtils.sendSystemMessage(msg);
        }
        finally {
            if(conn != null) {
                conn.close();
                conn = null;
            }
        }
    }
    
    
    /**
     * This method handles the invocation to decide which type of record is
     * incoming, acknowledgement or delivery confirmation (tracking).  The 
     * method then calls the proper sub-methods to process the incoming file.
     * @param message
     * @throws Throwable
     */
    private void process(String message) throws Throwable {
        // Get a list of all records
        VenusDAO vdao = new VenusDAO(conn);
        List fileList = getFilesFromFTP(message);
        
        if(fileList == null || fileList.size() == 0) {
            if(logger.isInfoEnabled())
                logger.info("There are no new files for vendor " + message);
        }
        else {
            File inputFile = null;
            PersonalCreationsInboundVO pcivo = null;
            List fileInfo = null;
            for(int j = 0; j < fileList.size(); j++) {
                try {
                    inputFile = (File)fileList.get(j);
                    fileInfo = readPCFile(inputFile);
                    
                    // Process each record
                    for(int i = 0; i < fileInfo.size(); i++) {
                        try {
                            // Build inbound vo
                            pcivo = buildPCIVO((String)fileInfo.get(i));
      
                            // Obtain associated order
                            VenusMessageVO venusMessageVO = getAssociatedOrder(vdao, pcivo.getExternalOrderNumber());
                            
                            // Call proper method depending on record type
                            if(pcivo.getType() == PersonalCreationsInboundVO.ACKNOWLEDGEMENT_FILE) {
                                createAcknowledgementANS(vdao, venusMessageVO);    
                            }
                            else if(pcivo.getType() == PersonalCreationsInboundVO.TRACKING_FILE) {
                                createTrackingANS(vdao, venusMessageVO, pcivo);    
                            }
                            else {
                                throw new Exception("Unknown type");
                            }
                        }
                        catch(Exception e)
                        {
                            String msg = "Error processing line:" + fileInfo.get(i) + "\nFrom inbound file " + inputFile.getName() + ".  Investigate:" + e;
                            logger.error(msg, e);
                            CommonUtils.sendSystemMessage(msg);
                        }
                    } 
                }
                catch(Exception e)
                {
                    String msg = "Error processing info from Personal Creations inbound file " + inputFile.getName() + ".  Investigate:" + e;
                    logger.error(msg, e);
                    CommonUtils.sendSystemMessage(msg);
                }
            }
        }
    }
    
    
    /** 
     * This record retrieves the inbound file from the FTP server.
     * @param vendorId - The vendor id to retrieve
     * @return List - List of files
     * @throws Exception
     */
    private List getFilesFromFTP(String vendorId) throws Exception {
        List fileList = new ArrayList();
    
        if(logger.isInfoEnabled())
            logger.info("Inbound FTP processing. Vendor=" + vendorId);
        
        //get ftp config data
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
        String sftpServer = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_REMOTE_LOCATION_" + vendorId);                      
        String remoteFileName = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"INBOUND_REMOTE_FILENAME_" + vendorId);
        String sftpLogon = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT,"INBOUND_FTP_LOGON_" + vendorId);
        String sftpPassword = configUtil.getSecureProperty(VenusConstants.SECURE_CONFIG_CONTEXT,"INBOUND_FTP_PASSWORD_" + vendorId);
        String remoteDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_REMOTE_DIRECTORY_" + vendorId);
        String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_LOCAL_DIRECTORY_" + vendorId);
        String localFileName = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"INBOUND_LOCAL_FILENAME_" + vendorId);
        String sourcePath = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"INBOUND_REMOTE_SOURCE_PATH_" + vendorId);
        
        //get list of files in directory       
        Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
        String[] files = null;
        ArrayList<String> ftpFileList = new ArrayList<String>();
        try{
            //When files do not exist in the specified dir this object throws
            //an exception.  We do not consider this an error because this
            //dir will not always contain files.
			JSch jsch = new JSch();
			session = jsch.getSession(sftpLogon, sftpServer);
			session.setPassword(sftpPassword);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteDir);
            Vector totalFiles = channelSftp.ls(sourcePath);
            for(int i = 0; i < totalFiles.size(); i++){
                ChannelSftp.LsEntry ls = (ChannelSftp.LsEntry) totalFiles.get(i);
                logger.info("File Name: " + ls.getFilename());
                if(ls.getFilename().endsWith(".txt") || ls.getFilename().endsWith(".TXT"))
                	ftpFileList.add(ls.getFilename());
            }
            
            files = new String[ftpFileList.size()];
            files = ftpFileList.toArray(files);
                    	
        }
        catch(Exception e)
        {
            logger.info("Files do not exist in directory.");
            logger.info(e);
        }
        
        //if nothing was found exit
        if(files == null || files.length == 0)
        {
            return null;
        }
        
        //insert a record to indicate that we got a file
        VenusDAO venusDAO = new VenusDAO(conn);
        venusDAO.insertTransmissionHistory(vendorId,"TRACKING_FILE");   
    
        //get current date and time to be used in file name
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHmmss");
        String dateString = sdf.format(new java.util.Date());
        
        //process each file
        for(int i = 0;i < files.length;i++)
        {
            //get the file from the remote location
            String localFile = localDir + localFileName + "_" + dateString + "_" + i+1 + ".txt";
            
            if(logger.isInfoEnabled()) {
                logger.info("Local File:" + localFile);
                logger.info("Remote File:" + files[i]);
            }
            
            try {
            	BufferedInputStream bis = new BufferedInputStream(channelSftp.get(files[i]));
    			File file = new File(files[i]);
    			OutputStream os = new FileOutputStream(file);
    			BufferedOutputStream bos = new BufferedOutputStream(os);
    			byte[] buffer = new byte[1024];
    			int readCount;
    			while ((readCount = bis.read(buffer)) > 0) {
    				bos.write(buffer, 0, readCount);
    			}
    			bis.close();
    			bos.close();	
                //archive the file and remove the file from the remote directory
    			channelSftp.cd("Processed");
    			channelSftp.put(new FileInputStream(file), files[i]);
    			channelSftp.cd("..");
    			channelSftp.rm(files[i]);
    			//channelSftp.rm(files[i]);
            
                //File file = new File(localFile);
                fileList.add(file);
                
            } catch(Exception e) {
                String msg = "Error FTP'ing Personal Creations file: " + files[i] + " so skipping and moving on.  Exception: " + e;
                logger.error(msg, e);
    			channelSftp.disconnect();
    			channel.disconnect();
    			session.disconnect();
                CommonUtils.sendSystemMessage(msg);
            }
        }
		channelSftp.disconnect();
		channel.disconnect();
		session.disconnect();
        return fileList;
    }
    
    
    /**
     * This method will update the VENUS table and add an ANS message as well as
     * order comment when an order acknowledgement is received from Personal 
     * Creations.
     * @param vdao Data access object used for database interaction.
     * @param venusMessageVO Venus record returned for the order id received
     * from the Personal Creations file.
     * @throws Exception
     */
    private void createAcknowledgementANS(VenusDAO vdao, VenusMessageVO venusMessageVO) throws Exception {
        String trackingMsg = "Personal Creations has received order";
         
        // Update VenusMessageVO
        venusMessageVO.setMsgType(VenusConstants.VENUS_ANS_MESSAGE);
        venusMessageVO.setVenusStatus(VenusConstants.VENUS_VERIFIED_STATUS);
        venusMessageVO.setOperator(VenusConstants.VENUS_SYSTEM);
        venusMessageVO.setMessageText(trackingMsg);            
        
        // The communication page requires the comments field in the Venus
        // table table to contain the message text
        venusMessageVO.setComments(venusMessageVO.getMessageText());
        vdao.insertVenusMessage(venusMessageVO,VenusConstants.INBOUND_MSG);
    }
        
        
    /**
     * This method will update the CLEAN.ORDER_DETAILS table, VENUS table, and
     * add an ANS message as well as order comment when an order tracking number
     * is received from Personal Creations.
     * @param vdao Data access object used for database interaction.
     * @param venusMessageVO Venus record returned for the order id received
     * from the Personal Creations file.
     * @param pciVO Value object containing fields retrieved from the inbound
     * file.
     * @throws Exception
     */
    private void createTrackingANS(VenusDAO vdao, VenusMessageVO venusMessageVO, PersonalCreationsInboundVO pciVO) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat(VenusConstants.PERSONAL_CREATIONS_INBOUND_DATE_MASK);

        // Parse ship date
        Date shipDate = dateFormat.parse(pciVO.getShipDate());

        // Update the order details table with the ship date and status
        OrderDAO orderDAO = new OrderDAO(conn);
        OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(venusMessageVO.getReferenceNumber());
        orderDetailVO.setShipDate(shipDate);
        orderDetailVO.setOrderDispCode(VenusConstants.SHIPPED_DISPOSITION);
        orderDAO.updateOrder(orderDetailVO, OrderConstants.SP_UPDATE_ORDER_DETAILS_PERSONAL_CREATIONS);
        
        // Update the order tracking number
        OrderTrackingVO orderTrackingVO = new OrderTrackingVO();
        orderTrackingVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
        orderTrackingVO.setTrackingNumber(pciVO.getTrackingNumber());
        orderTrackingVO.setCarrierName("UPS");
                
        orderDAO.updateTrackingNumber(orderTrackingVO);
        
        // Create ANS tracking message
        dateFormat = new SimpleDateFormat("EEE, MMMMM dd, yyyy");            
        String shipDateFormatted = dateFormat.format(shipDate);  
        dateFormat = new SimpleDateFormat("EEE, ddMMMyy");
        String trackingMsg = "Your UPS tracking number is " + pciVO.getTrackingNumber() + 
                             ". Your order is scheduled to ship on " + shipDateFormatted + "."; 
        
        // Update VenusMessageVO with values from inbound file
        venusMessageVO.setShipDate(shipDate);
        venusMessageVO.setMsgType(VenusConstants.VENUS_ANS_MESSAGE);
        venusMessageVO.setVenusStatus(VenusConstants.VENUS_VERIFIED_STATUS);
        venusMessageVO.setOperator(VenusConstants.VENUS_SYSTEM);
        venusMessageVO.setMessageText(trackingMsg);            
        
        // The communication page requires the comments field in the Venus
        // table to contain the message text
        venusMessageVO.setComments(venusMessageVO.getMessageText());
        vdao.insertVenusMessage(venusMessageVO,VenusConstants.INBOUND_MSG);
    }
    
    
  /**
   * This method reads and stores each line from the PC acknowledgement or 
   * tracking files.
   * 
   * @param input PC inbound file
   * @return List Containing file info
   * @throws Exception
   */
   private List readPCFile(File input) throws Exception {
       String info;
       ArrayList fileInfo = new ArrayList();
       FileReader reader = new FileReader(input);
       BufferedReader bufReader = new BufferedReader(reader);        

       // Read each line in the input file
       while(bufReader.ready()) {
           info = bufReader.readLine();
           if(info == null || info.trim().equals(""))
              logger.warn("Input from Personal Creations inbound file is blank.  Input:" + info);
           else
              fileInfo.add(info);
       }

       // Close input file readers
       bufReader.close();
       reader.close();

       return fileInfo;
   }      


    /**
     * This method transforms the incoming file info into a value object that
     * contains the fields from the | (pipe) delimited file.
     * 
     * | (pipe) delimited text file format has replaced the XML file.
     * @param input | (pipe) delimited text to transform into a 
     * PersonalCreationsInboundVO object.
     * @return PersonalCreationsInboundVO PersonalCreationsInboundVO object
     * containing fields from the input String.
     * @throws Exception
     */
     private PersonalCreationsInboundVO buildPCIVO(String input) throws Exception {
         StringTokenizer st = null;
         String token = null; 
         PersonalCreationsInboundVO pcivo = new PersonalCreationsInboundVO();

         // Tokenize input
         st = new StringTokenizer(input, "|");
         
         // First field is the type
         if(st.hasMoreTokens())
            token = st.nextToken();
         else
            throw new Exception("No type field exists.  Attempting to parse the following line:" + input);
          pcivo.setType(Integer.parseInt(token));
         
          // Second field is the external order number
          if(st.hasMoreTokens())
             token = st.nextToken();
          else
             throw new Exception("No external order number exists.  Attempting to parse the following line:" + input);
          pcivo.setExternalOrderNumber(token);
         
         // If type is acknowledgement
         if(pcivo.getType() == PersonalCreationsInboundVO.ACKNOWLEDGEMENT_FILE) {
              // Set acknowledgement message.  We don't use the message for anything so just print out a warn if it is missing
              token = "";
              if(st.hasMoreTokens())
                 token = st.nextToken();
              else
                 logger.warn("No acknowledgement message exists.  Attempting to parse the following line:" + input);
              pcivo.setAcknowledgement(token);
         }
         
         // If type is acknowledgement
         else if(pcivo.getType() == PersonalCreationsInboundVO.TRACKING_FILE) {
              // Third field is the ship date
              if(st.hasMoreTokens())
                 token = st.nextToken();
              else
                 throw new Exception("No ship date exists.  Attempting to parse the following line:" + input);
              pcivo.setShipDate(token);
             
              // Last field is the tracking number
              if(st.hasMoreTokens())
                 token = st.nextToken();
              else
                 throw new Exception("No tracking number exists.  Attempting to parse the following line:" + input);
              pcivo.setTrackingNumber(token);
         }
         else {
            throw new Exception("Invalid type.  Attempting to parse the following line:" + input);
         }

         if(logger.isDebugEnabled())
            logger.debug("PCIVO is " + pcivo.toString());
         
         return pcivo;
     }      
    
    
    /**
     * This method will return a VenusMessageVO representing an order based on
     * the external order number value passed in.
     * @param vdao Data access object to pull information from database.
     * @param externalOrderNumber External order number to return order for.
     * @return VenusMessageVO containing order information.
     * @throws Exception
     */
    private VenusMessageVO getAssociatedOrder(VenusDAO vdao, String externalOrderNumber) throws Exception {
        VenusMessageVO foundOrder = null;
    
        if(logger.isDebugEnabled())
            logger.debug("External order number is " + externalOrderNumber);

        // Obtain order detail id
        OrderDAO orderDAO = new OrderDAO(conn);
        Map orderNumberMap = orderDAO.findOrderNumber(externalOrderNumber);
        String orderDetailId = orderNumberMap.get("OUT_ORDER_DETAIL_ID").toString();
        
        if(logger.isDebugEnabled())
            logger.debug("Order detail id is " + orderDetailId);

        if(orderDetailId == null || orderDetailId.equals(""))
            throw new Exception("Could not find order detail id for external order number:" + externalOrderNumber);

        //Retrieve venus FTD order message related to this message 
        List venusList = vdao.getVenusMessageByDetailId(orderDetailId, VENUS_MESSAGE_TYPE_ORDER);
        
        if(venusList != null && venusList.size() > 0) {
            if(venusList.size() > 1) {
                throw new Exception("Multiple FTDs were found for order detail id " + orderDetailId);                        
            }
            
            foundOrder = (VenusMessageVO)venusList.get(0);    
        }
        else {
            throw new Exception("No FTD found for order detail id " + orderDetailId);                        
        }
        
        return foundOrder;
    }

}