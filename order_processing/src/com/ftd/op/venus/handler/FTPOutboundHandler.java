package com.ftd.op.venus.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.util.VenusCommonUtil;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;
import java.io.File;
import java.io.FileWriter;

import java.sql.Connection;
import java.sql.DriverManager;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class FTPOutboundHandler extends com.ftd.eventhandling.events.EventHandler
{

  Logger logger;
  private final String SERVER_LOCATION = "REMOTE_LOCATION";
  private final String SERVER_LOGON = "FTP_LOGON";
  private final String SERVER_PASSWORD = "FTP_PASSWORD";
  
  private String WINE_VENDOR_ID;
  private String SAMS_VENDOR_ID;
  private String RICHLINE_VENDOR_ID;
  
  private final String PRIORITY_OVERNIGHT = "PO";

  public FTPOutboundHandler() throws Exception
  {
    this.logger = new Logger("com.ftd.op.venus.handler.FTPOutboundHandler");  
    
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
      this.WINE_VENDOR_ID = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"WINE_COM_VENDOR_ID");                            
      this.SAMS_VENDOR_ID = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"SAMS_COM_VENDOR_ID"); 
      this.RICHLINE_VENDOR_ID = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "RICHLINE_VENDOR_ID");
  }

    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
        MessageToken messageToken = (MessageToken) payload;
        String vendorId = (String)messageToken.getMessage();
        //obtain a database connection
        Connection conn = CommonUtils.getConnection();
        try
        {
           logger.info("Executing vendor " + vendorId);
           processOrders(conn, vendorId);
        }
        catch (Throwable t)
        {
          logger.error(t);
          CommonUtils.sendSystemMessage(t.getMessage());
        }
        finally
        {
          conn.close();
        }
    }
  
    public void processOrders(Connection conn, String vendorId)throws Exception
    {
      VenusDAO venusDAO = new VenusDAO(conn);
      OrderDAO orderDAO = new OrderDAO(conn);
      VenusCommonUtil venusCommonUtil = new VenusCommonUtil(conn);
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
      SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

      String context = "ORDER_PROCESSING";
      String name = "VENUS_BATCH_MAX_SIZE_"+vendorId;
      GlobalParameterVO batchMaxSize = orderDAO.getGlobalParameter(context, name);
      
      //get the file prefix to use
      String filePrefix = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"FTP_FILE_PREFIX_" + vendorId);      
      if (batchMaxSize == null)
      {
        // if value not found in table, set value to max int //
        batchMaxSize = new GlobalParameterVO();
        batchMaxSize.setContext(context);
        batchMaxSize.setName(name);
        batchMaxSize.setValue(new Integer(Integer.MAX_VALUE).toString());
        
        logger.debug("Max batch size set to default of max int.");
      }
      ArrayList venusOrders = (ArrayList) venusDAO.getVenusOrders(new Integer(batchMaxSize.getValue()).intValue(), "FTP",vendorId);
      // if no orders, exit procedure //
      if (  (venusOrders.size() == 0) && 
            !(vendorId.equalsIgnoreCase(this.WINE_VENDOR_ID) || vendorId.equalsIgnoreCase(this.SAMS_VENDOR_ID) || vendorId.equalsIgnoreCase(this.RICHLINE_VENDOR_ID)))
      {
        logger.info("FTPOutboundHandler.processOrders exited because there were no orders to be procesed.");
        return;
      }
      int ftpCount = venusDAO.getDailyFTPCount(vendorId, "ORDER");
      String ftpCountString = this.getFtpCountString(ftpCount);
      String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"WORKINGDIR");       
      String archiveDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"ARCHIVEDIR");         
      String localFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + "_" + vendorId + ".txt";
      String remoteFileName = "";

      /* To make those object able to work with order vendors this logic should
       * be put in the configuration file.  This is be put here as part of a production
       * fix and for deployment reasons I do not want to make updates to the venus
       * configuration file. */
         if(vendorId != null && vendorId.equals("00096"))
         {
            remoteFileName = filePrefix + ftpCountString + ".txt";             
            logger.info("00096 file name = " + remoteFileName);
         }
         else if(vendorId != null && vendorId.equals("00119"))
         {
            remoteFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + ".TXT";                          
            logger.info("00119 file name = " + remoteFileName);
         }
         else if (vendorId != null && vendorId.equals("00179"))
         {
        	remoteFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + ".TXT";
        	logger.info("00179 file name = " + remoteFileName);
         }
         else
         {
            remoteFileName = filePrefix + dateFormatter.format(new Date()) + ftpCountString + ".TXT";                                       
            logger.info("OTHER file name (vendor=" + vendorId + ")= " + remoteFileName);
         }

      File file = new File(localDir + localFileName);
      FileWriter out = new FileWriter(file);
      for (int i = 0; i < venusOrders.size(); i++)
      {
        VenusMessageVO venusMessageVO = (VenusMessageVO)venusOrders.get(i);
        try
        {
          out.write(this.getOrderString(conn,vendorId,configUtil, venusCommonUtil, venusMessageVO));
          venusDAO.updateVenusStatus(venusMessageVO.getVenusId(), "VERIFIED");
          VenusMessageVO newVenusMessageVO = this.getVenusMessageForInsert(venusMessageVO);
          venusDAO.insertVenusMessage(newVenusMessageVO,VenusConstants.OUTBOUND_MSG);
        }
        catch(Exception ex)
        {
          logger.error("Error creating file to FTP to vendor");
          logger.error(ex);
          venusDAO.updateVenusStatus(venusMessageVO.getVenusId(), "ERROR");
        }
      }
      out.close();
      try
      {
    	boolean fileSent = false;
        // The name of the file that is FTPed to the remote server should not contain the vendor //

        File remoteFile = venusCommonUtil.copyFile(new File(localDir + remoteFileName), file);
        if (remoteFile == null)   
        {
          throw new VenusException (file.getName()+" could not be copied.");
        }
        else
        {
          fileSent = this.ftpFile(conn, remoteFile, configUtil, venusCommonUtil, vendorId);
        }
        if (fileSent)
        {
          // move file to archive dir //
          file.renameTo(new File(archiveDir + localFileName));
          // insert FTP transmission //
          venusDAO.insertTransmissionHistory(vendorId, "ORDER");
          remoteFile.delete();
        }
        else
        {
          throw new Exception ("Ftp failed.");
        }
      }
      catch (Exception ex)
      {
        String msg = file.getName()+" could not be ftped to Venus.  File must be manually sent to Venus.  File: "+(localDir + localFileName);
        logger.error(msg+ ": "+ex.toString());
        CommonUtils.sendSystemMessage(msg+ ": "+ex.toString()); 
      }
    }
    
   /**
   * Vendor specific ftpFile method- sets parameters and calls the 
   * VenusCommonUtil.java ftpFile method 
   * @param Connection conn
   * @param File file
   * @param ConfigurationUtil configUtil
   * @param VenusCommonUtil venusCommonUtil
   * @return boolean fileSent
   **/
  
  private boolean ftpFile(Connection conn, File file, ConfigurationUtil configUtil, VenusCommonUtil venusCommonUtil, String vendorId)
  {
    boolean fileSent = false;
    try
    {
      String remoteDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"VENDOR_"+vendorId+"_REMOTE_DIRECTORY");  
      String loadBalance = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "LOAD_BALANCE");  
      fileSent = venusCommonUtil.ftpFile(file, configUtil, remoteDir, loadBalance, "VENDOR_"+vendorId+"_"+SERVER_LOCATION, "VENDOR_"+vendorId+"_"+SERVER_LOGON, "VENDOR_"+vendorId+"_"+SERVER_PASSWORD);    
    }
    catch (Throwable t)
    {
      logger.error(t);
      return false;
    }
    return fileSent;
  }


   /** The following method will create a new VenusMessageVO to the specification 
    * of the VenusMessageVO section
     * @param Connection conn
     * @param VenusMessageVO venusMessageVO
     * @throws Exception
     * @returns VenusMessageVO
     */    
    
    private VenusMessageVO getVenusMessageForInsert(VenusMessageVO venusMessageVO)
    {
      SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy h:mma");
      VenusMessageVO newVenusMessageVO = new VenusMessageVO();
      // copied fields //
      newVenusMessageVO.setVenusOrderNumber(venusMessageVO.getVenusOrderNumber());
      newVenusMessageVO.setOrderDate(venusMessageVO.getOrderDate());
      newVenusMessageVO.setFillingVendor(venusMessageVO.getFillingVendor());
      newVenusMessageVO.setRecipient(venusMessageVO.getRecipient());
      newVenusMessageVO.setAddress1(venusMessageVO.getAddress1());
      newVenusMessageVO.setAddress2(venusMessageVO.getAddress2());
      newVenusMessageVO.setCity(venusMessageVO.getCity());
      newVenusMessageVO.setState(venusMessageVO.getState());
      newVenusMessageVO.setZip(venusMessageVO.getZip());
      newVenusMessageVO.setPhoneNumber(venusMessageVO.getPhoneNumber());
      newVenusMessageVO.setDeliveryDate(venusMessageVO.getDeliveryDate());
      newVenusMessageVO.setPrice(venusMessageVO.getPrice());
      newVenusMessageVO.setCardMessage(venusMessageVO.getCardMessage());
      newVenusMessageVO.setReferenceNumber(venusMessageVO.getReferenceNumber());
      newVenusMessageVO.setCountry(venusMessageVO.getCountry());
      newVenusMessageVO.setProductID(venusMessageVO.getProductId());
      newVenusMessageVO.setShippingSystem(venusMessageVO.getShippingSystem());
      // different fields //
      newVenusMessageVO.setOperator("SYS");
      newVenusMessageVO.setMessageText("Order printed on "+dateFormatter.format(new Date()));
      newVenusMessageVO.setMsgType("ANS");
      newVenusMessageVO.setVenusStatus("VERIFIED");
      newVenusMessageVO.setOrderPrinted("Y");
      newVenusMessageVO.setTransmissionTime(new Date()); 
      
      return newVenusMessageVO;

    }

    /** The following method will create a comma-delimited string for an order.
     * This string should be added to the otubound file.
     * @param Connection conn
     * @param VenusMessageVO venusMessageVO
     * @throws Exception
     * @returns String orderString
     */
    private String getOrderString(Connection conn,String vendorId, ConfigurationUtil configUtil, VenusCommonUtil venusCommonUtil, VenusMessageVO venusMessageVO) throws Exception
    { 
     
     //get the age verify flag
      String ageVerifyFlag = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"AGE_VERIFY_FLAG_" + vendorId);      

      String orderString = "";

      //obtain external order number from order detai lid
      OrderDAO orderDAO = new OrderDAO(conn);
      Map orderNumberMap = orderDAO.findOrderNumber(venusMessageVO.getReferenceNumber());
      String externalOrderNumber = orderNumberMap.get("OUT_EXTERNAL_ORDER_NUMBER").toString();
      
      SimpleDateFormat dateFormatter = new SimpleDateFormat("MMddyy");
      // Order Number - Max Length 15 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, externalOrderNumber, 15);
      // Recipient Name - Max Length 30 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, venusMessageVO.getRecipient(), 30);
      // Company  - Max Length 30 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getBusinessName()), 30);
      // Address 1  - Max Length 30 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getAddress1()), 30);
      // Address 2  - Max Length 30 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getAddress2()), 30);
      // City - Max Length 16 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getCity()), 16);
      // State - Max Length 2 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getState()), 2);
      // Zip - Max Length 10 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getZip()), 10);
      // Phone Number - Max Length 10 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, formatString(venusMessageVO.getPhoneNumber()), 10);
      // Gift Item #- Max Length 10 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, venusMessageVO.getProductId(), 10);
      // Item Qty - Max Length 10 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, "1", 10);

      // Ship Method - Max Length 2//
      String shipMethod = venusMessageVO.getShipMethod();

      //check for special wine.com ship method conversion
      if(vendorId.equals(WINE_VENDOR_ID))
      {
          shipMethod = convertWineShipMethod(shipMethod,venusMessageVO.getState());
      }

      //update ship method if vendor requires special methods
      shipMethod = getCustomShipMethod(conn,vendorId,shipMethod);
      
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, shipMethod, 2);

      // Ship Date - Max Length 6 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, (venusMessageVO.getShipDate() == null ? null : dateFormatter.format(venusMessageVO.getShipDate())), 6);
      // Gift Message Line  - Max Length 40 (each) EXCEPT for Richline vendor who can handle full 4000//
      if(!vendorId.equalsIgnoreCase(RICHLINE_VENDOR_ID)){
	      ArrayList cardMessageList = (ArrayList) this.splitCardMessageIntoLines(venusMessageVO.getCardMessage());      
	      for (int i = 0; i < cardMessageList.size(); i++)
	      {
	        orderString = venusCommonUtil.addCommaDelimitedField(orderString, ((String) cardMessageList.get(i)), 40);
	      }
      }
      else if(vendorId.equalsIgnoreCase(RICHLINE_VENDOR_ID)){
    	// Removing double quotes from card message for Richline vendor orders
    	  String cardMessage = venusMessageVO.getCardMessage();
    	  if(cardMessage != null){
    		  cardMessage = venusCommonUtil.removeDoubleQuotes(cardMessage);
    	  }
    	  else{
    		  cardMessage = "";
    	  }
    	  orderString = venusCommonUtil.addCommaDelimitedField(orderString, cardMessage, 4000);  
      }
      // Price - Max Length 10 //
      orderString = venusCommonUtil.addCommaDelimitedField(orderString, venusMessageVO.getPrice().toString(), 10);
      // AgeVerify - Max Length 1 //
      if(!vendorId.equalsIgnoreCase(RICHLINE_VENDOR_ID)){
          orderString = venusCommonUtil.addCommaDelimitedField(orderString, configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"AGE_VERIFY_FLAG_" + vendorId), 1, false); 
      }
      // Product description
      else if(vendorId.equalsIgnoreCase(RICHLINE_VENDOR_ID)){
    	  orderString = venusCommonUtil.addCommaDelimitedField(orderString, venusMessageVO.getVendorSKU(), 20, false);
      }
      orderString = orderString.concat("\n");
      return orderString;
    }


    private String formatString(String inString)
    {
        return inString == null ? "" : inString;
    }



    /**
     * This method is used to convert ftd ship method types to wine.com ship method types.
     */
     private String convertWineShipMethod(String shipMethod,String state)
     {         
         if(state.equalsIgnoreCase("HI") || state.equalsIgnoreCase("AK")){
            shipMethod = PRIORITY_OVERNIGHT;
         }

        return shipMethod;

     }
     
     /**
      * Check DB to see if ship method needs to be converted.
      */
     private String getCustomShipMethod(Connection conn,String vendorId,String shipMethod)
        throws Exception
     {
         VenusDAO venusDAO = new VenusDAO(conn);
         String dbShipMethod = venusDAO.getVendorShipMethod(vendorId,shipMethod);
         
         return dbShipMethod == null ? shipMethod : dbShipMethod;
     }


    /** This method creates the string signifying what number file transmission
     * this is for the day.  If this is the first batch of the day, the string
     * will be blank, 2nd - "A", 3rd -"B", etc...
     */
    private String getFtpCountString(int ftpCount)
    { 
      String ftpCountString = "";
      if (ftpCount != 0)
      {
        // get the Unicode value for the letter we need//
        int characterInt = (ftpCount%26)+64; 
        ftpCountString = new Character((char) characterInt).toString();
      }
      return ftpCountString;
    }
/*    
    private String addCommaDelimitedField(String parentString, String newString, int maxLength, boolean addComma)
    {
      if (newString != null)
      {
        if (newString.length() > maxLength)
        {
          newString = newString.substring(0, maxLength);
        }
        parentString = parentString.concat(newString);
      }
      if (addComma)
      {
        parentString = parentString.concat(",");
      }
      return parentString;
    }

    private String addCommaDelimitedField(String parentString, String newString, int maxLength)
    {
      return this.addCommaDelimitedField(parentString, newString, maxLength, true);
    }
  */    
   private List splitCardMessageIntoLines(String cardMessage)
   throws Exception
   {
      List lines = new ArrayList ();
      while (cardMessage != null && cardMessage.length() > 0)
      {
        if(lines.size() < 6)
        {
            if (cardMessage.length() < 40)
            {
              lines.add (cardMessage);       
              cardMessage = null;
            }
            else
            {
              lines.add(cardMessage.substring (0,40));  
              cardMessage = cardMessage.substring(40);
            }
        }
        else
        {
            String msg = "Card message length is to long for Venus order. Message:" + cardMessage;
            logger.error(msg);
            CommonUtils.sendSystemMessage(msg);
        }
      }
      
      //there must be exactly 6 lines
      while(lines.size() < 6)
      {
          lines.add("");
      }
      
      return lines;           
   }

   
}