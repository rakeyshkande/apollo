package com.ftd.op.venus.handler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.vo.CustomerPhoneVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.op.venus.exception.VenusException;
import com.ftd.op.venus.util.VenusCommonUtil;
import com.ftd.op.venus.vo.FTPCustomerVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class FTPCustomerFileHandler extends com.ftd.eventhandling.events.EventHandler
{

  Logger logger;
  private final String SERVER_LOCATION = "REMOTE_LOCATION";
  private final String SERVER_LOGON = "FTP_LOGON";
  private final String SERVER_PASSWORD = "FTP_PASSWORD";

  public FTPCustomerFileHandler()
  {
    this.logger = new Logger("com.ftd.op.venus.handler.FTPCustomerFileHandler");  
  }

    /** This method is invoked by the event handler framework. 
     */
     public void invoke(Object payload) throws Throwable
    {
        Connection conn = CommonUtils.getConnection();
        try
        {
            MessageToken messageToken = (MessageToken)payload;
            String vendorId = messageToken.getMessage().toString();
        
            logger.error("FTPCustomerFileHandler invoked. Vendor="+vendorId);           
            processCustomers(conn,vendorId);           
        }
        catch (Throwable t)
        {
          logger.error(t);
          CommonUtils.sendSystemMessage("Error occured trying to process customer file." + t.getMessage());
        }
        finally
        {
          conn.close();
        }
    }

  /**
   * This method will get a list of the all the customers that 
   * placed orders on Wine.com last month.  The customer information
   * will be put into a file and FTPed to a vendor.
   * @throws java.lang.Exception
   * @param conn
   */

  public void processCustomers(Connection conn,String vendorId)throws Exception  
  {
    logger.info("processCustomers(Connection conn)");
    VenusDAO venusDAO = new VenusDAO(conn);  
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();  
    
    String custFilePrefix = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"CUSTOMER_FILE_PREFIX_" + vendorId);
    String custFileExtension = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"CUSTOMER_FILE_EXTENSION_" + vendorId);
        
    VenusCommonUtil venusCommonUtil = new VenusCommonUtil(conn);
    SimpleDateFormat yyyymmddFormat = new SimpleDateFormat("yyyyMMdd");

    // get previous month and year //
    SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");    
    Date today = new java.util.Date();
    int month = new Integer(monthFormat.format(today)).intValue();
    int year = new Integer(yearFormat.format(today)).intValue();;
    // get previous month //
    if (month == 1)
    {
      month = 12;
      year = year - 1;
    }
    else 
    {
      month = month - 1;
    }
    ArrayList customerRecords = (ArrayList) venusDAO.getCustomers(vendorId, new Integer(month).toString(), new Integer(year).toString());
    // if no records exist, exit procedure //
    if (customerRecords.size() == 0)
    {
      logger.info("No customer records exist for month "+month+" and year "+year+".");
      return;
    }
    String localDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"WORKINGDIR");                              
    String archiveDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"ARCHIVEDIR");                              
      //    String localFileName = custFilePrefix + yyyymmddFormat.format(new Date());
      // GaneshM. Changes for Issue 555 & 556. Should not append date to the ftp file names.
          String localFileName = custFilePrefix ;

    File file = new File(localDir + localFileName + custFileExtension);
    FileWriter out = new FileWriter(file);
    for (int i = 0; i < customerRecords.size(); i++)
    {
      // the customerVO in the FTPCustomerVO only contains selected fields //
       FTPCustomerVO ftpCustomerVO = (FTPCustomerVO) customerRecords.get(i);
       try
       {
          out.write(this.getCustomerString(venusCommonUtil, ftpCustomerVO));
       }
       catch(Exception ex)
       {
          String msg = "Error adding customer record "+ftpCustomerVO.getRecipientName()+" for venus order number "+ftpCustomerVO.getVenusOrderNumber()+" to file "+file.getName()+".";
          logger.info(msg + ": "+ex.toString());
          CommonUtils.sendSystemMessage(msg+ ": "+ex.toString()); 
       }
    }
    out.close();
    try
    {
      boolean fileSent = false;
      fileSent = this.ftpFile(conn, file, configUtil, venusCommonUtil, vendorId);
      if (fileSent)
      {
        // if file already exists in archive dir, appent a number//
          // GaneshM. Issue 555 & 556. Keep the archive filename the old way (to append date to file names)
           String archiveFileName = localFileName + yyyymmddFormat.format(new Date());
          File archivedFile = new File(archiveDir + archiveFileName + custFileExtension);
        int counter = 2;
        while (archivedFile.isFile())
        {
          archivedFile = new File(archiveDir + localFileName + "_"+ counter + custFileExtension);
          counter ++;
        }
        // move file to archive dir //
        file.renameTo(archivedFile);
        // insert FTP transmission //
        venusDAO.insertTransmissionHistory(vendorId, "CUSTOMER");
      }
      else
      {
        throw new Exception ("Ftp failed.");
      }
    }
    catch (Exception ex)
    {
      String msg = file.getName()+" could not be ftped to Venus.  File must be manually sent to Venus.  File: "+(localDir + localFileName + custFileExtension);
      logger.error(msg+ ": "+ex.toString());
      CommonUtils.sendSystemMessage(msg+ ": "+ex.toString()); 
    }
  }
 
   private String getCustomerString(VenusCommonUtil venusCommonUtil, FTPCustomerVO ftpCustomerVO) throws Exception
    { 
      String customerString = "";
      SimpleDateFormat mmddyyyyFormat = new SimpleDateFormat("MM/dd/yyyy");
      // order number - max length 15 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getVenusOrderNumber(), 15);;
      // recipient name - max length 30 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getRecipientName(), 30);
      // company - max length 30 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getBusinessName(), 30);
      // address 1 - max length 30 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getAddress1(), 30);
      // address 2 - max length 30 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getAddress2(), 30);
      // city - max length 16 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getCity(), 16);
      // state - max length 2 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getState(), 2);
      // zip  - max legnth 10 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getZip(), 10);
      // phone number - max length 10 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getPhoneNumber(), 10);
      // date of birth - max length 10 //
      customerString = venusCommonUtil.addCommaDelimitedField(customerString, ftpCustomerVO.getBirthday() == null ? null : mmddyyyyFormat.format(ftpCustomerVO.getBirthday()), 10, false);
      customerString = customerString.concat("\n");
      return customerString;
    }

 
    /**
   * Vendor specific ftpFile method- sets parameters and calls the 
   * VenusCommonUtil.java ftpFile method 
   * @param File file
   * @param ConfigurationUtil configUtil
   * @param Connection conn
   * @param VenusCommonUtil venusCommonUtil
   * @return boolean fileSent
   **/
  
  private boolean ftpFile(Connection conn, File file, ConfigurationUtil configUtil, VenusCommonUtil venusCommonUtil, String vendorId)
  {
    boolean fileSent = false;
    try
    {
      String remoteDir = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"VENDOR_"+vendorId+"_REMOTE_DIRECTORY");                      
      String loadBalance = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE, "LOAD_BALANCE");                      
      fileSent = venusCommonUtil.ftpFile(file, configUtil, remoteDir, loadBalance, "VENDOR_"+vendorId+"_"+SERVER_LOCATION, "VENDOR_"+vendorId+"_"+SERVER_LOGON, "VENDOR_"+vendorId+"_"+SERVER_PASSWORD);    
    }
    catch (Throwable t)
    {
      logger.error(t);
      return false;
    }
    return fileSent;
  }
  //processCustomers

    
    
}

