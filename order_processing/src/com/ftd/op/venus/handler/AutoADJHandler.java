package com.ftd.op.venus.handler;

import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.facade.VenusFacade;
import com.ftd.op.venus.to.AdjustmentMessageTO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.venus.dao.VenusDAO;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * This handler gets an Excel file from a URL, parses it, and then sends out
 * ADJ messages using the data from the file.
 */
public class AutoADJHandler extends com.ftd.eventhandling.events.EventHandler   
{

    private static final String LOGGER_CATEGORY = "com.ftd.op.venus.handler.AutoADJHandler";
    private static final String INVALID_REPORT_LINE = "Oracle XDK Java";
    private static final String VENUS_MESSAGE_TYPE_ORDER = "FTD";
    private static final String VENUS_MESSAGE_TYPE_ADJUSTMENT = "ADJ";
    private static final String ADJ_COMMENT = "Order in cancel proposed state.  Cancelled by end of month process.";
    private Logger logger;

    public AutoADJHandler()
    {
        //Build a logger
        this.logger =  new Logger(LOGGER_CATEGORY);                
    }
    
    /**
     * This method is invoked by event handler framework.
     */
    public void invoke(Object payload) throws Throwable
    {
        //connection
        Connection conn = null;
    
        try
        {
            //obtain a database connection
            conn = CommonUtils.getConnection();   
            sendADJMessage(conn);
        }
        catch(Throwable t)
        {
            logger.error(t);
            CommonUtils.sendSystemMessage(t.toString());
        }
        finally
        {
            if(conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }   
    }    
    
    /*
     * This method contains the main logic for the class
     */
    private void sendADJMessage(Connection conn) throws Exception
    {

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String adjustMonthString = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ADJUST_MONTH");
        int adjustAmount = 0;
        if(adjustMonthString != null && adjustMonthString.length() > 0)
        {
            adjustAmount = Integer.parseInt(adjustMonthString);
        }        



        //get the first and last day of previous month
        Calendar lastMonth = Calendar.getInstance();

        lastMonth.add(Calendar.MONTH, adjustAmount);

        String startDate = (lastMonth.get(Calendar.MONTH) + 1) + "/" + lastMonth.getMinimum(Calendar.DAY_OF_MONTH) + "/" + lastMonth.get(Calendar.YEAR);
        String endDate = (lastMonth.get(Calendar.MONTH) + 1) + "/" + lastMonth.getActualMaximum(Calendar.DAY_OF_MONTH) + "/" + lastMonth.get(Calendar.YEAR);        

        //get list server urls
        List urlList = getURLs(startDate,endDate);        
        
        //get and parse the excel file 
        List orderList = getReport(urlList);
        
        //Venus DAO
        VenusDAO venusDAO = new VenusDAO(conn);
        
        //Object used to call Adjustment code in order processing
        VenusFacade facade = new VenusFacade(conn);
        
        //save the list of orders numbers that ADJs were sent out for.  This is used for logging.
        String adjsSent = "";
        
        //list of orders that an adustment could not be sent for because associated order not found
        String adjsCannotBeFound = "";
        
        //for each record 
        Iterator iter = orderList.iterator();
        while(iter.hasNext())
        {
            String venusOrderNumber = (String)iter.next();
        
            //get original venus order record
            VenusMessageVO venusMessageVO = getAssociatedOrder(conn,venusOrderNumber);
            
            //only send out the ADJ if the associated could be found
            if(venusMessageVO == null)
            {
                adjsCannotBeFound = adjsCannotBeFound + ":" + venusOrderNumber;                
            }
            else
            {

                //create adjustment record
                AdjustmentMessageTO adjTO = new AdjustmentMessageTO();
                adjTO.setComments(ADJ_COMMENT);
                adjTO.setFillingVendor(venusMessageVO.getFillingVendor());
                adjTO.setMessageType(VENUS_MESSAGE_TYPE_ADJUSTMENT);
                adjTO.setOldPrice(venusMessageVO.getPrice());
                adjTO.setOperator("SYSTEM");
                adjTO.setOrderDetailId(venusMessageVO.getReferenceNumber());
                adjTO.setOverUnderCharge(new Double("0.00"));
                adjTO.setPrice(venusMessageVO.getPrice());
                adjTO.setVenusOrderNumber(venusOrderNumber);
                
                //send out adjustment   
                try
                {
                    facade.sendAdjustment(adjTO);                                    
                    adjsSent = adjsSent + ":" + venusOrderNumber;                
                }
                catch(Exception e)
                {
                    //if error occurs log it and continue processing the rest
                    logger.error(e);
                    CommonUtils.sendSystemMessage("Error sending automated adjustment.  See order processing log. Venus order=" + venusOrderNumber);                    
                }

            }

        }       

        //if some order's could not be found send out page
        if(adjsCannotBeFound.length() > 0)
        {
            CommonUtils.sendSystemMessage("Automated ADJ messages could not be sent for the following orders because the order could be found in the DB" + adjsCannotBeFound);
        }
            
        //log the order numbers that were sent out   
        logger.info("ADJ messages wer sent for the following venus order numbers" + adjsSent);
    }
    
    /*
     * Get the order message assocated with the past in venus messageVO
     */
    private VenusMessageVO getAssociatedOrder(Connection conn,String vendorOrderNumber) throws Exception
    {
        VenusDAO venusDAO = new VenusDAO(conn);
    
        //Retrieve venus FTD order message related to this message 
        List venusList = venusDAO.getVenusMessage(vendorOrderNumber, VENUS_MESSAGE_TYPE_ORDER);
        if(venusList == null || venusList.size() == 0)
        {
            return null;
        }
        else
        {
            if(venusList.size() > 1)
            {
                return null;
            }
        }
        VenusMessageVO foundOrder = (VenusMessageVO)venusList.get(0);    
        
        return foundOrder;
    }    
    
    /*
     * Obtain report and return list of E Numbers
     */
     private List getReport(List urls) throws Exception
     {
        
        //list of venus order numbers
        List orderNumberList = new ArrayList();               
        
        //indicates if a file was read in
        boolean fileRead = false;
        
        //loop through all the URLs and try and get the report
        Iterator iter = urls.iterator();
        while(!fileRead && iter.hasNext())
        {
            try
            {
                //Get site url
                String site = (String)iter.next();
                logger.info("Connection to site " + site);                
                
                //Connect to URL                
                URL url = new URL(site);
                Object content = url.getContent();                
                InputStream istream = url.openConnection().getInputStream();        
                InputStreamReader ireader = new InputStreamReader(istream);
                BufferedReader breader = new BufferedReader(ireader);
        
                //read first line of file
                String textLine = breader.readLine();
                //if the line of text contains a specfic text string, then that indicates 
                //that this URL is not running the application to display the report
                if(textLine != null && textLine.indexOf(INVALID_REPORT_LINE) >= 0)
                {
                    throw new Exception("ADJ Application is not running.  Will try next site.  Site=:" + site);
                }                
                
                //loop through file and get all the venus order (E numbers) out
                while(textLine != null && textLine.length() > 0)
                {
                    String eNumber = getENumber(textLine);
                    
                    if(eNumber != null)
                    {
                        orderNumberList.add(eNumber);                          
                    }

                    textLine = breader.readLine();                    

                    //flag to indicate a file was read in
                    fileRead = true;
                }
             
            }
            catch(Exception e)
            {
                //set file read flag to false so the next url will be tried
                fileRead = false;
                
                //log error, but keep processing and try next url
                logger.error(e);
            }            
        }//end URL loop
        
        //if a file was not able to be read throw an exception
        if(!fileRead)
        {
            throw new Exception("The Automatic ADJ process was unable to connection to a server.  Process was NOT run.");
        }
        
        return orderNumberList;

     }


    /*
     * Parse out the E number from the line of text
     */
    private String getENumber(String textLine)  throws Exception
    {
        String orderNumber = "";
        boolean firstISOFound = false;
        boolean secondISOFound = false;    
        
        try{
    
            //Loop through the line and parse out the E number
            //E number comes after the first ISO char and ends at the second ISO char
            int i = 0;
            while(!secondISOFound && i < textLine.length())
            {
                char testchar = textLine.charAt(i);
    
                //if the first ISO was found save char to string
                if(firstISOFound)
                {
                    //check if we find the second ISO...the end of E number
                    if(Character.isISOControl(testchar))
                    {
                        secondISOFound = true;
                    }
                    else
                    {
                        orderNumber = orderNumber + textLine.charAt(i);                    
                    }
                }
                else
                {
                    //are at the start of E number?
                    if(Character.isISOControl(testchar))
                    {
                        firstISOFound = true;
                    }
    
                }
                
                //next char
                i++;
                
            }//end of for loop
        }
        catch(Throwable t)
        {
            logger.error("Error parsing line:" + textLine);
            logger.error("Processing of file will continue");
            orderNumber = null;
        }
        
        //if the order number does not start with an E then throw an exception.
        //this indicates tha the format of the file is not as expected
        if(!orderNumber.startsWith("E"))
        {
            logger.error("Error obtaining ENumber:" + textLine);
            logger.error("Processing of file will continue");
            orderNumber = null;
        }               

        return orderNumber;
    }
    
    
    /*
     * Get a list of URLS from the config file.
     * The start and end date will be added to each URL.
     */
     private List getURLs(String startDate, String endDate) throws Exception
     {
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         String url = null;
         
         List urlList = new ArrayList();
         
             //get first url
             int i = 1;
             url = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ADJ_FILE_URL1");
             
             //loop through all the URLS in the config file
             while(url != null && url.length() > 0)
             {
                 //append dates to URL and then add it to list
                 url = url + "&startDate=" + startDate + "&endDate=" + endDate;                     
                 urlList.add(url);
                 
                 //get next URL
                 i++;
                 url = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"ADJ_FILE_URL" + i);
             }                     

         
         return urlList;         
     }
     
    
}