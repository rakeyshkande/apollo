package com.ftd.op.venus.constants;

public class VenusConstants 
{

    public static final String VENUS_CONFIG_FILE = "venus_config.xml";
    public static final String VENUS_CONFIG_CONTEXT = "ORDER_PROCESSING_CONFIG";

    // Secure Configuration Context
    public static final String SECURE_CONFIG_CONTEXT = "order_processing";

    public static final String ORDER_PROCESSING_PARMS = "ORDER_PROCESSING";
    public static final String LAST_VENUS_BATCH_SENT = "venus_last_batch_sent";
    public static final String LAST_VENUS_BATCH_FREQ = "venus_batch_frequency_minutes";
    public static final String VENUS_VERIFIED_STATUS = "VERIFIED";
    public static final String VENUS_REJECTED_STATUS = "REJECTED";
    
    public static final String TN_MESSAGE_PROCESSED = "PROCESSED";
    public static final String TN_MESSAGE_ERROR = "ERROR";
    
    public static final String OUTBOUND_MSG = "OUTBOUND";
    public static final String INBOUND_MSG = "INBOUND";    
    
    public static final String VENUS_SYSTEM = "Venus";
    
    //This is the configuration file used by all of order processing
    public static final String PROPERTY_FILE = "somefile.xml";
    
    public static final String ORDER_PROCESSING_COMMENT_ORIGIN = "OrderProc";
    public static final String COMMENT_TYPE_ORDER = "Order";
    
    public static final String PRINTED_DISPOSITION = "Printed";  
    public static final String SHIPPED_DISPOSITION = "Shipped";  
    
    public static final String VENUS_ANS_MESSAGE = "ANS";
    public static final String PERSONALIZATION_MALL_INBOUND_DATE_MASK = "yyyyMMdd";
    public static final String PERSONAL_CREATIONS_INBOUND_DATE_MASK = "yyyyMMdd";
    
    //SP_GET_ORDER_DETAIL_PAYMENT
    public final static String PAYMENT_ID = "PAYMENT_ID";
    public final static String ADDITIONAL_BILL_ID = "ADDITIONAL_BILL_ID";
    public final static String PAYMENT_TYPE = "PAYMENT_TYPE";
    public final static String CC_ID = "CC_ID";
    public final static String CREDIT_AMOUNT = "CREDIT_AMOUNT";
    public final static String DEBIT_AMOUNT = "DEBIT_AMOUNT";
    public final static String AUTH_RESULT = "AUTH_RESULT";
    public final static String AUTH_NUMBER = "AUTH_NUMBER";
    public final static String AVS_CODE = "AVS_CODE";
    public final static String ACQ_REFERENCE_NUMBER = "ACQ_REFERENCE_NUMBER";
    public final static String GC_COUPON_NUMBER = "GC_COUPON_NUMBER";
    public final static String AUTH_OVERRIDE_FLAG = "AUTH_OVERRIDE_FLAG";
    public final static String AUTH_FLAG = "AUTH_FLAG";
    public final static String PAYMENT_INDICATOR = "PAYMENT_INDICATOR";
    public final static String REFUND_ID = "REFUND_ID";
    public final static String ORDER_GUID = "ORDER_GUID";
  
  /* Shipping system types */
    public static final String SHIPPING_SYSTEM_ESCALATE = "ESCALATE";
    public static final String SHIPPING_SYSTEM_FTP = "FTP";
    public static final String SHIPPING_SYSTEM_SDS = "SDS";
    public static final String SHIPPING_SYSTEM_NONE = "NONE";
    public static final String SHIPPING_SYSTEM_FTD_WEST = "FTD WEST";
    
    public VenusConstants()
    {
    }
}