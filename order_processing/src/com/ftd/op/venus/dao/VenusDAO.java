package com.ftd.op.venus.dao;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.venus.constants.VenusConstants;
import com.ftd.op.venus.vo.FTPCustomerVO;
import com.ftd.op.venus.vo.FedExDeliveryConfVO;
import com.ftd.op.venus.vo.MessageTypeCountsVO;
import com.ftd.op.venus.vo.ProductNotificationVO;
import com.ftd.op.venus.vo.TNMessageVO;
import com.ftd.op.venus.vo.VenusMessageVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


/**
 *
 *
 * @author Dave Ross
 */
public class VenusDAO
{

  Connection conn;

    private Logger logger;
    private static final int TN_MESSAGE_ERROR_LENGTH = 300;
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_ERROR_MESSAGE";


  public VenusDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.venus.dao.VenusDAO");
  }

  /**
     This method gets a list of records from the TNMessages table that have
     not been processed.
     *
     * @return
     * @throws java.lang.Exception
     */
  public List getUnprocessedRecords() throws Exception
  {
    CachedResultSet results = null;

    ArrayList unprocessedList = new ArrayList();

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_VENUS.GET_UNPROCESSED_VENUS_RECORDS");

    Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);

    String status = (String)outputs.get(STATUS_PARAM);

    if(status.equals("N") )
    {
     String message = (String)outputs.get(MESSAGE_PARAM);
     throw new Exception(message.toString());
    }

    results = (CachedResultSet)outputs.get("OUT_CUR");

    if ( results != null )
    {

      while ( results.next() )
      {
        TNMessageVO tnMessageVO = new TNMessageVO();

      tnMessageVO.setMsgID(results.getLong("MSG_ID"));
      tnMessageVO.setMsgType(results.getString("MSG_TYPE"));
      tnMessageVO.setMsgPOID(results.getLong("MSG_PO_ID"));
      tnMessageVO.setMsgPOConfNbr(results.getString("MSG_PO_CONF_NBR"));
      tnMessageVO.setMsgAdjAmount(new Double(results.getDouble("MSG_ADJ_AMOUNT")));
      tnMessageVO.setMsgOrigOrderAmt(new Double(results.getDouble("MSG_ORIG_ORDER_AMT")));
      tnMessageVO.setMsgText(results.getString("MSG_TEXT"));
      tnMessageVO.setMsgOrderVendorID(results.getString("MSG_ORDER_VENDOR_ID"));
      unprocessedList.add(tnMessageVO);

      }
    }
    return unprocessedList;
  }



  public List getInventoryNotifications(String productId, String vendorId) throws Exception
  {
    CachedResultSet results = null;

    ArrayList invList = new ArrayList();

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_INV_CTRL_NOTIFICATIONS");

    HashMap inputParams = new HashMap();
    inputParams.put("IN_PRODUCT_ID", productId);
    inputParams.put("IN_VENDOR_ID", vendorId);
    dataRequest.setInputParams(inputParams);

    Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);


    results = (CachedResultSet)outputs.get("OUT_INV_NOTIFICATIONS_CURSOR");

    if ( results != null )
    {

      while ( results.next() )
      {
        ProductNotificationVO notificationVO = new ProductNotificationVO();

        notificationVO.setEmailAddress(results.getString("email_address"));
        invList.add(notificationVO);
      }
    }
    return invList;
  }




    /**
     * @return
     * @throws java.lang.Exception
     */
    public Document getInventoryRecordXML(String productId, String vendorId) throws Exception
    {
        Document data = DOMUtil.getDefaultDocument();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_GET_INVENTORY_DATA");

        HashMap inputParams = new HashMap();
        inputParams.put("IN_PRODUCT_ID", productId);
        inputParams.put("IN_VENDOR_ID", vendorId);
        dataRequest.setInputParams(inputParams);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        HashMap map = (HashMap)dataAccessUtil.execute(dataRequest);

        Document mainDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element mainRoot = (Element) mainDoc.createElement("inventorydata");
        mainDoc.appendChild(mainRoot);

        Document inventoryDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element invRoot = (Element) inventoryDoc.createElement("inventory");
        inventoryDoc.appendChild(invRoot);

        Document emailDoc = DOMUtil.getDocumentBuilder().newDocument();
        Element emailRoot = (Element) emailDoc.createElement("notfications");
        emailDoc.appendChild(emailRoot);

        DOMUtil.addSection(inventoryDoc,((Document)map.get("OUT_INVENTORY_CURSOR")).getChildNodes());
        DOMUtil.addSection(emailDoc,((Document)map.get("OUT_INV_NOTIFICATIONS_CURSOR")).getChildNodes());
        DOMUtil.addSection(mainDoc,inventoryDoc.getChildNodes());
        DOMUtil.addSection(mainDoc,emailDoc.getChildNodes());

        return (Document)mainDoc;
    }



  /**
   * This method serves as a wrapper for the VENUS.TN_MESSAGE_PKG.GET_TN_MESSAGE
   * stored procedure.
   *
   * @param String messageID
   * @return TNMessageVO
   * @throws IOException, ParserConfigurationException, SQLException, SAXException, Exception
   */

  public TNMessageVO getTNMessage(String messageID) throws IOException, ParserConfigurationException, SQLException, SAXException, Exception
  {
    TNMessageVO msgVO = null;

    CachedResultSet rs = null;

    msgVO = new TNMessageVO();

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("OP_GET_TN_MESSAGE");
    dataRequest.addInputParam("IN_MESSAGE_ID", messageID);

    Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
    String status = (String)outputs.get("OUT_STATUS");

    if(status.equals("N") )
    {
     String message = (String)outputs.get("OUT_ERROR_MESSAGE");
     throw new Exception(message.toString());
    }
    rs = (CachedResultSet)outputs.get("OUT_CUR");

    if ( rs != null )
    {
      if ( rs.next() )
      {
        msgVO = new TNMessageVO();
        msgVO.setMsgID(new Long(messageID).longValue());
        msgVO.setMsgType(rs.getString("MSG_TYPE"));
        msgVO.setMsgPOID(rs.getLong("MSG_PO_ID"));
        msgVO.setMsgPOConfNbr(rs.getString("MSG_PO_CONF_NBR"));
        msgVO.setMsgAdjAmount(new Double(rs.getDouble("MSG_ADJ_AMOUNT")));
        msgVO.setMsgOrigOrderAmt(new Double(rs.getDouble("MSG_ORIG_ORDER_AMT")));
        msgVO.setMsgText(rs.getString("MSG_TEXT"));
        msgVO.setMsgOrderVendorID(rs.getString("MSG_ORDER_VENDOR_ID"));
        msgVO.setMsgExportedFlag(rs.getString("MSG_EXPORTED_FLAG"));
        msgVO.setMsgReadFlag(rs.getString("MSG_READ_FLAG"));
        msgVO.setMsgProcessedFlag(rs.getString("MSG_PROCESSED_FLAG"));
        msgVO.setOrderProcessingFlag(rs.getString("ORD_PROCESSING_PROCESSED_FLAG"));
        msgVO.setErrorDescription(rs.getString("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode(rs.getString("CANCEL_REASON_CODE"));
      }
    }
    return msgVO;
  }

  public boolean existsOnDoNotPageList(String message) throws Exception
  {


    boolean exists = false;
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_VENUS.GET_FEDEX_ERROR");
    dataRequest.addInputParam("IN_ERROR_CODE", message);
    String description = (String) DataAccessUtil.getInstance().execute(dataRequest);

    return description == null ? false : true;

  }

  public void updateTNMessageStatus(long messageID, String status, String errorDesc) throws Exception
  {

    logger.info("updateTNMessageStatus(" + messageID + "," + status + "," + errorDesc);

    //If error is over 300 chars truncate it
    if(errorDesc != null && errorDesc.length() > TN_MESSAGE_ERROR_LENGTH)
    {
        errorDesc = errorDesc.substring(TN_MESSAGE_ERROR_LENGTH);
    }


    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_VENUS.UPDATE_TN_MESSAGE_STATUS");
    dataRequest.addInputParam("IN_MESSAGE_ID", new Long(messageID));
    dataRequest.addInputParam("IN_STATUS", status);
    dataRequest.addInputParam("IN_ERROR_DESCRIPTION", errorDesc);
    HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
    if ( results != null )
    {
        String queryStatus = (String) results.get("OUT_STATUS");
        if ( queryStatus.equals("N") )
        {
          String errorMessage = (String) results.get("OUT_ERROR_MESSAGE");
          throw new Exception(errorMessage);
        }

       logger.info("updateTNMessageStatus - end");

    }
    else throw new Exception("Call to VENUS.UPDATE_TN_MESSAGE_STATUS did not return a valid HashMap");
  }
  /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE
   * stored procedure.
   *
   * @param String venusOrderNumber
   * @param String messsageType
   * @return List messages
   * @throws IOException, ParserConfigurationException, SQLException, SAXException
   */

  public List getVenusMessage(String venusOrderNumber, String messageType) throws IOException, ParserConfigurationException, SQLException, SAXException
  {
    CachedResultSet crs = null;
    ArrayList messages = null;

    VenusMessageVO msgVO = null;

    logger.info("VenusDAO.getVenusMessage(String venusOrderNumber ("+ venusOrderNumber +"), String messageType ("+ messageType +"))");
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_VENUS_MESSAGE");
    dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusOrderNumber);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      messages = new ArrayList();

      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus((String)crs.getObject("VENUS_STATUS"));
        msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductID((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
        msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
        msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
        msgVO.setShippingSystem(crs.getString("SHIPPING_SYSTEM"));
        msgVO.setTrackingNumber(crs.getString("TRACKING_NUMBER"));
        msgVO.setPrintedStatusDate(getUtilDate(crs.getObject("PRINTED")));
        msgVO.setShippedStatusDate(getUtilDate(crs.getObject("SHIPPED")));
        msgVO.setCancelledStatusDate(getUtilDate(crs.getObject("CANCELLED")));
        msgVO.setFinalCarrier(crs.getString("FINAL_CARRIER"));
        msgVO.setFinalShipMethod(crs.getString("FINAL_SHIP_METHOD"));
        msgVO.setSdsStatus(crs.getString("SDS_STATUS"));
        msgVO.setZoneJumpFlag(crs.getString("ZONE_JUMP_FLAG") == null ? false : true);
        msgVO.setZoneJumpLabelDate(getUtilDate(crs.getObject("ZONE_JUMP_LABEL_DATE")));
        msgVO.setZoneJumpTrailerNumber(crs.getString("ZONE_JUMP_TRAILER_NUMBER"));
        msgVO.setPersonalGreetingId(crs.getString("PERSONAL_GREETING_ID"));
         
        messages.add(msgVO);
      }
    }
    return messages;
  }

   /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE_BY_VENUS_ID
   * stored procedure.
   *
   * @param String String venusId
   * @return VenusMessageVO
   * @throws IOException, ParserConfigurationException, SQLException, SAXException
   */

  public VenusMessageVO getVenusMessageById(String venusId) throws IOException, ParserConfigurationException, SQLException, SAXException
  {

    CachedResultSet crs = null;

    VenusMessageVO msgVO = null;

    logger.info("VenusDAO.getVenusMessage(String venusId (" +venusId+ " )");
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_VENUS_MESSAGE_BY_VENUS_ID");
    dataRequest.addInputParam("IN_VENUS_ID", venusId);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus((String)crs.getObject("VENUS_STATUS"));
        msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductID((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setShippingSystem(crs.getString("SHIPPING_SYSTEM"));
        msgVO.setTrackingNumber(crs.getString("TRACKING_NUMBER"));
        msgVO.setPrintedStatusDate(getUtilDate(crs.getObject("PRINTED")));
        msgVO.setShippedStatusDate(getUtilDate(crs.getObject("SHIPPED")));
        msgVO.setCancelledStatusDate(getUtilDate(crs.getObject("CANCELLED")));
        msgVO.setFinalCarrier(crs.getString("FINAL_CARRIER"));
        msgVO.setFinalShipMethod(crs.getString("FINAL_SHIP_METHOD"));
        msgVO.setSdsStatus(crs.getString("SDS_STATUS"));
        msgVO.setPersonalGreetingId(crs.getString("PERSONAL_GREETING_ID"));
        msgVO.setNewShippingSystem(crs.getString("NEW_SHIPPING_SYSTEM"));
      }
    }
    return msgVO;
  }


  public void insertTransmissionHistory(String vendorId, String transmissionType)
    throws Exception
  {
    logger.info("insertTransmissionHistory(String vendorId ("+vendorId+"), String transmissionType("+transmissionType+")");
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_INSERT_FTP_TRANSMISSION_HIST");
    dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
    dataRequest.addInputParam("IN_TRANSMISSION_TYPE", transmissionType);
    Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }



  /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_MESSAGE
   * stored procedure.
   *
   * @param String venusOrderNumber
   * @param String messsageType
   * @return List messages
   * @throws IOException, ParserConfigurationException, SQLException, SAXException
   */

  public List getVenusMessageByDetailId(String orderDetailId, String messageType) throws IOException, ParserConfigurationException, SQLException, SAXException
  {
    logger.info("getVenusMessageByDetailId(String orderDetailId ("+orderDetailId+"), String messageType ("+messageType+"))");
    CachedResultSet crs = null;
    VenusMessageVO msgVO = null;
    ArrayList messages = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_VENUS_MESS_BY_ORDER_DETAIL");
    dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      messages = new ArrayList();

      while ( crs.next() )
      {
        msgVO = new VenusMessageVO();
        msgVO.setVenusId((String)crs.getObject("VENUS_ID"));
        msgVO.setVenusOrderNumber((String)crs.getObject("VENUS_ORDER_NUMBER"));
        msgVO.setVenusStatus((String)crs.getObject("VENUS_STATUS"));
        msgVO.setMsgType((String)crs.getObject("MSG_TYPE"));
        msgVO.setSendingVendor((String)crs.getObject("SENDING_VENDOR"));
        msgVO.setFillingVendor((String)crs.getObject("FILLING_VENDOR"));
        msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
        msgVO.setRecipient((String)crs.getObject("RECIPIENT"));
        msgVO.setBusinessName((String)crs.getObject("BUSINESS_NAME"));
        msgVO.setAddress1((String)crs.getObject("ADDRESS_1"));
        msgVO.setAddress2((String)crs.getObject("ADDRESS_2"));
        msgVO.setCity((String)crs.getObject("CITY"));
        msgVO.setState((String)crs.getObject("STATE"));
        msgVO.setZip((String)crs.getObject("ZIP"));
        msgVO.setCountry((String)crs.getObject("COUNTRY"));
        msgVO.setPhoneNumber((String)crs.getObject("PHONE_NUMBER"));
        msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
        msgVO.setFirstChoice((String)crs.getObject("FIRST_CHOICE"));
        msgVO.setPrice(new Double(crs.getDouble("PRICE")));
        msgVO.setCardMessage((String)crs.getObject("CARD_MESSAGE"));
        msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
        msgVO.setOperator((String)crs.getObject("OPERATOR"));
        msgVO.setComments((String)crs.getObject("COMMENTS"));
        msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
        msgVO.setReferenceNumber((String)crs.getObject("REFERENCE_NUMBER"));
        msgVO.setProductID((String)crs.getObject("PRODUCT_ID"));
        msgVO.setCombinedReportNumber((String)crs.getObject("COMBINED_REPORT_NUMBER"));
        msgVO.setRofNumber((String)crs.getObject("ROF_NUMBER"));
        msgVO.setAdjReasonCode((String)crs.getObject("ADJ_REASON_CODE"));
        msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
        msgVO.setProcessedIndicator((String)crs.getObject("PROCESSED_INDICATOR"));
        msgVO.setMessageText((String)crs.getObject("MESSAGE_TEXT"));
        msgVO.setSendToVenus((String)crs.getObject("SEND_TO_VENUS"));
        msgVO.setOrderPrinted((String)crs.getObject("ORDER_PRINTED"));
        msgVO.setSubType((String)crs.getObject("SUB_TYPE"));
        msgVO.setErrorDescription((String)crs.getObject("ERROR_DESCRIPTION"));
        msgVO.setCancelReasonCode((String)crs.getObject("CANCEL_REASON_CODE"));
        msgVO.setCompOrder((String)crs.getObject("COMP_ORDER"));
        msgVO.setOldPrice(new Double(crs.getDouble("OLD_PRICE")));
        msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
        msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
        msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
        msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
        msgVO.setShippingSystem(crs.getString("SHIPPING_SYSTEM"));
        msgVO.setPersonalGreetingId(crs.getString("PERSONAL_GREETING_ID"));

        messages.add(msgVO);
      }
    }
    return messages;
  }


  /**
     This method overrides insertVenusMessage()

     * @param venusMessage
     * @param direction
     * @return
     * @throws java.lang.Exception
     */
  public String insertVenusMessage(VenusMessageVO venusMessage, String direction) throws Exception
  {
      return insertVenusMessage(venusMessage,direction,null);
  }


    /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.INSERT_VENUS_MESSAGE
   * stored procedure.
   *
   * @param String venusMessage
   * @throws Exception
   */

  public String insertVenusMessage(VenusMessageVO venusMessage, String direction,String externalSystemStatus) throws Exception
  {
    logger.info( "VenusDAO - " + venusMessage.getReferenceNumber() +
    							" - insertVenusMessage(VenusMessageVO venusMessage) :: String ");
    logger.info("externalSystemStatus=" + externalSystemStatus);
    DataRequest dataRequest = new DataRequest();

    // There is no vendor for venus messages associated with the SDS shipping system
    if(!StringUtils.equals(venusMessage.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS)
       && !StringUtils.equals(venusMessage.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST))
      venusMessage = this.switchVendorForTesting(venusMessage);

    dataRequest.setStatementID("OP_INSERT_VENUS_MESSAGE");
    dataRequest.setConnection(conn);

    dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusMessage.getVenusOrderNumber());
    dataRequest.addInputParam("IN_VENUS_STATUS", venusMessage.getVenusStatus());
    dataRequest.addInputParam("IN_MSG_TYPE", venusMessage.getMsgType());
    dataRequest.addInputParam("IN_SENDING_VENDOR", venusMessage.getSendingVendor());
    dataRequest.addInputParam("IN_FILLING_VENDOR", venusMessage.getFillingVendor());
    dataRequest.addInputParam("IN_ORDER_DATE", venusMessage.getOrderDate() == null ? null : new java.sql.Date(venusMessage.getOrderDate().getTime()));
    dataRequest.addInputParam("IN_RECIPIENT", venusMessage.getRecipient());
    dataRequest.addInputParam("IN_BUSINESS_NAME", venusMessage.getBusinessName());
    dataRequest.addInputParam("IN_ADDRESS_1", venusMessage.getAddress1());
    dataRequest.addInputParam("IN_ADDRESS_2", venusMessage.getAddress2());
    dataRequest.addInputParam("IN_CITY", venusMessage.getCity());
    dataRequest.addInputParam("IN_STATE", venusMessage.getState());
    dataRequest.addInputParam("IN_ZIP", venusMessage.getZip());
    dataRequest.addInputParam("IN_PHONE_NUMBER", venusMessage.getPhoneNumber());
    dataRequest.addInputParam("IN_DELIVERY_DATE", venusMessage.getDeliveryDate() == null ? null : new java.sql.Date(venusMessage.getDeliveryDate().getTime()));
    dataRequest.addInputParam("IN_FIRST_CHOICE", venusMessage.getFirstChoice());
    dataRequest.addInputParam("IN_PRICE", venusMessage.getPrice() == null? null: venusMessage.getPrice().toString());
    dataRequest.addInputParam("IN_CARD_MESSAGE", venusMessage.getCardMessage());
    dataRequest.addInputParam("IN_SHIP_DATE", venusMessage.getShipDate()== null ? null :new java.sql.Date(venusMessage.getShipDate().getTime()));
    dataRequest.addInputParam("IN_OPERATOR", venusMessage.getOperator());
    dataRequest.addInputParam("IN_COMMENTS", venusMessage.getComments());
    dataRequest.addInputParam("IN_TRANSMISSION_TIME", venusMessage.getTransmissionTime() == null ? null : new java.sql.Timestamp(venusMessage.getTransmissionTime().getTime()));
    dataRequest.addInputParam("IN_REFERENCE_NUMBER", venusMessage.getReferenceNumber());
    dataRequest.addInputParam("IN_PRODUCT_ID", venusMessage.getProductId());
    dataRequest.addInputParam("IN_COMBINED_REPORT_NUMBER", venusMessage.getCombinedReportNumber());
    dataRequest.addInputParam("IN_ROF_NUMBER", venusMessage.getRofNumber());
    dataRequest.addInputParam("IN_ADJ_REASON_CODE", venusMessage.getAdjReasonCode());
    dataRequest.addInputParam("IN_OVER_UNDER_CHARGE", venusMessage.getOverUnderCharge() == null? null: venusMessage.getOverUnderCharge().toString());
    dataRequest.addInputParam("IN_PROCESSED_INDICATOR", venusMessage.getProcessedIndicator());
    dataRequest.addInputParam("IN_MESSAGE_TEXT", venusMessage.getMessageText());
    dataRequest.addInputParam("IN_SEND_TO_VENUS", venusMessage.getSendToVenus());
    dataRequest.addInputParam("IN_ORDER_PRINTED", getBoolean(venusMessage.isPrinted()));
    dataRequest.addInputParam("IN_SUB_TYPE", venusMessage.getSubType());
    dataRequest.addInputParam("IN_ERROR_DESCRIPTION", venusMessage.getErrorDescription());
    dataRequest.addInputParam("IN_COUNTRY", venusMessage.getCountry());
    dataRequest.addInputParam("IN_CANCEL_REASON_CODE", venusMessage.getCancelReasonCode());
    dataRequest.addInputParam("IN_COMP_ORDER", venusMessage.getCompOrder());
    dataRequest.addInputParam("IN_OLD_PRICE", venusMessage.getOldPrice() == null ? null : venusMessage.getOldPrice().toString());
    dataRequest.addInputParam("IN_SHIP_METHOD", venusMessage.getShipMethod());
    dataRequest.addInputParam("IN_VENDOR_SKU", venusMessage.getVendorSKU());
    dataRequest.addInputParam("IN_PRODUCT_DESCRIPTION", venusMessage.getProductDescription());
    dataRequest.addInputParam("IN_PRODUCT_WEIGHT", venusMessage.getProductWeight());
    dataRequest.addInputParam("IN_MESSAGE_DIRECTION", direction);
    dataRequest.addInputParam("IN_EXTERNAL_SYSTEM_STATUS", externalSystemStatus);
    dataRequest.addInputParam("IN_SHIPPING_SYSTEM", venusMessage.getShippingSystem());
    dataRequest.addInputParam("IN_TRACKING_NUMBER", venusMessage.getTrackingNumber());
    dataRequest.addInputParam("IN_PRINTED", venusMessage.getPrintedStatusDate() == null ? null : new java.sql.Date(venusMessage.getPrintedStatusDate().getTime()));
    dataRequest.addInputParam("IN_SHIPPED", venusMessage.getShippedStatusDate() == null ? null : new java.sql.Date(venusMessage.getShippedStatusDate().getTime()));
    dataRequest.addInputParam("IN_CANCELLED", venusMessage.getCancelledStatusDate() == null ? null : new java.sql.Date(venusMessage.getCancelledStatusDate().getTime()));
    dataRequest.addInputParam("IN_FINAL_CARRIER", venusMessage.getFinalCarrier());
    dataRequest.addInputParam("IN_FINAL_SHIP_METHOD", venusMessage.getFinalShipMethod());
    dataRequest.addInputParam("IN_REJECTED", venusMessage.getRejectedStatusDate() == null ? null : new java.sql.Date(venusMessage.getRejectedStatusDate().getTime()));
    dataRequest.addInputParam("IN_VENDOR_ID", venusMessage.getVendorId());
    dataRequest.addInputParam("IN_PERSONAL_GREETING_ID", venusMessage.getPersonalGreetingId());
    dataRequest.addInputParam("IN_ORIG_COMPLAINT_COMM_TYPE_ID", venusMessage.getComplaintCommOriginTypeId());
    dataRequest.addInputParam("IN_NOTI_COMPLAINT_COMM_TYPE_ID", venusMessage.getComplaintCommNotificationTypeId());
    dataRequest.addInputParam("IN_NEW_SHIPPING_SYSTEM", venusMessage.getNewShippingSystem());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }

    return ((String)outputs.get("OUT_VENUS_ID"));
  }
  private VenusMessageVO switchVendorForTesting(VenusMessageVO venusMessage)throws Exception
  {
     // check to see if in test mode //
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    String venusTestMode = configUtil.getFrpGlobalParm(VenusConstants.VENUS_CONFIG_CONTEXT,"VENUS_TEST_MODE");
    String doNotUpdateCodes = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_TEST_DO_NOT_UPDATE");
    if (venusTestMode.equalsIgnoreCase("Y") && !StringUtils.equals(venusMessage.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_SDS)
    		&& !StringUtils.equals(venusMessage.getShippingSystem(),VenusConstants.SHIPPING_SYSTEM_FTD_WEST))
    {
      logger.info("Venus is running in test mode.  Vendor numbers will be changed on outgoing orders.  This will not affect SDS or FTD WEST orders.");

      String vendorId = configUtil.getProperty(VenusConstants.VENUS_CONFIG_FILE,"VENUS_TEST_VENDOR_ID");
      if ((vendorId == null) || (vendorId.equalsIgnoreCase("")))
      {
        logger.info("Cannot switch vendor because config file constant VENUS_TEST_VENDOR_ID is blank or null (VENUS_TEST_MODE = 'Y' and VENUS_TEST_VENDOR_ID = "+vendorId+".");
      }
      else if (vendorId.equalsIgnoreCase(venusMessage.getFillingVendor()))
      {
        logger.info("Cannot switch vendor because config file constant VENUS_TEST_VENDOR_ID = venusMessage.getFillingVendor()");
      }
      else if (doNotUpdateCodes != null && doNotUpdateCodes.indexOf(venusMessage.getFillingVendor())  >= 0 )
      {
        logger.info("Vendor is not being switched because it is in the skip list.  See config file.");
      }
      else
      {
        // if switch message already there, remove it.
        if ((venusMessage.getAddress2() != null) && (venusMessage.getAddress2().indexOf("|SWITCHED FROM") != -1 ))
        {
          int index = venusMessage.getAddress2().indexOf("|SWITCHED FROM");
          if(index > 0)
          {
              venusMessage.setAddress2(venusMessage.getAddress2().substring(0,index-1));
          }

        }
        logger.info("Switching vendor from "+venusMessage.getFillingVendor()+" to test vendor "+vendorId +".(VENUS_TEST_MODE = 'Y' and VENUS_TEST_VENDOR_ID = "+vendorId+".");
        String msg = "|SWITCHED FROM "+venusMessage.getFillingVendor()+" TO TEST VENDOR "+vendorId +"|";
        venusMessage.setFillingVendor(vendorId);
        if (venusMessage.getAddress2() == null)
        {
          venusMessage.setAddress2(msg);
        }
        else
        {
          venusMessage.setAddress2(venusMessage.getAddress2()+" "+msg);
        }
      }
    }
    return venusMessage;
  }

  private String getBoolean(boolean bool)
  {
      return bool ? "Y" : "N";
  }


  /**
   * This method serves as a wrapper for the VENUS.TN_MESSAGE_PKG.INSERT_TN_MESSAGE
   * stored procedure.
   *
   * @param TNMessageVO
   * @throws Exception
   */

  public void insertTNMessage(TNMessageVO tnMessage) throws Exception
  {


    logger.info("insertTNMessage(TNMessageVO tnMessage) :: String ");
    DataRequest dataRequest = new DataRequest();
    dataRequest.setStatementID("OP_INSERT_TN_MESSAGE");
    dataRequest.setConnection(conn);
    dataRequest.addInputParam("IN_MSG_TYPE", tnMessage.getMsgType());
    dataRequest.addInputParam("IN_MSG_PO_ID", new Long(tnMessage.getMsgPOID()).toString());
    dataRequest.addInputParam("IN_MSG_PO_CONF_NBR", tnMessage.getMsgPOConfNbr());
    dataRequest.addInputParam("IN_MSG_ADJ_AMOUNT", tnMessage.getMsgAdjAmount() == null ? null : tnMessage.getMsgAdjAmount().toString());
    dataRequest.addInputParam("IN_MSG_ORIG_ORDER_AMT", tnMessage.getMsgOrigOrderAmt() == null ? null : tnMessage.getMsgOrigOrderAmt().toString());
    dataRequest.addInputParam("IN_MSG_TEXT", tnMessage.getMsgText());
    dataRequest.addInputParam("IN_MSG_EXPORTED_FLAG", tnMessage.getMsgExportedFlag());
    dataRequest.addInputParam("IN_MSG_READ_FLAG", tnMessage.getMsgReadFlag());
    dataRequest.addInputParam("IN_MSG_PROCESSED_FLAG", tnMessage.getMsgProcessedFlag());
    dataRequest.addInputParam("IN_MSG_WHO_CREATED", tnMessage.getMsgWhoCreated());
    dataRequest.addInputParam("IN_MSG_ORDER_VENDOR_ID", tnMessage.getMsgOrderVendorID());
    dataRequest.addInputParam("IN_ORD_PRCSSING_PRCSSED_FLAG", tnMessage.getOrderProcessingFlag());
    dataRequest.addInputParam("IN_ERROR_DESCRIPTION", tnMessage.getErrorDescription());
    dataRequest.addInputParam("IN_CANCEL_REASON_CODE", tnMessage.getCancelReasonCode());
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    Map outputs = (Map) dataAccessUtil.execute(dataRequest);

    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_ERROR_MESSAGE");
      throw new Exception(message);
    }
  }

    /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_VENUS_ORDER_NUMBER_SQ
   * stored procedure.
   *
   * @throws Exception
   */

  public long generateVenusOrderNumber() throws Exception{


    DataRequest dataRequest = new DataRequest();
    long idNum = 0;
    try
    {
      logger.info("generateVenusOrderNumber :: String ");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_NEXT_VENUS_ORDER_NUMBER_SQ");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      idNum = output.longValue();

    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return idNum;
  }


  /**
   * This method checks if the vendor ship method needs to be converted from the
   * ftd ship method to a vendor specific ship method.
   *
   * @throws Exception
   */

  public String getVendorShipMethod(String vendorId, String shipMethod) throws Exception{


    DataRequest dataRequest = new DataRequest();
    String vendorShipMethod = null;
    Object obj = null;
    try
    {
      logger.info("getVendorShipMethod :: String ");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_VENDOR_SHIP_METHOD");
      dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
      dataRequest.addInputParam("IN_SHIP_METHOD", shipMethod);

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      vendorShipMethod =(String) dataAccessUtil.execute(dataRequest);


    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return vendorShipMethod;
  }


     /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_DOCUMENT_SEQUENCE
   * stored procedure.
   *
   * @throws Exception
   */

  public String generateDocumentSequence() throws Exception{


    DataRequest dataRequest = new DataRequest();
    long seq = 0;
    try
    {
      logger.info("String generateDocumentSequence()");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_DOCUMENT_SEQUENCE");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      seq = output.longValue();

    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return (new Long(seq).toString());
  }

      /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_BATCH_SEQUENCE
   * stored procedure.
   *
   * @throws Exception
   */

  public String generateBatchSequence() throws Exception{

    DataRequest dataRequest = new DataRequest();
    long seq = 0;
    try
    {
      logger.info("String generateBatchSequence()");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_BATCH_SEQUENCE");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      seq = output.longValue();

    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return (new Long(seq).toString());
  }

     /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_WINE_ORDER_NUMBER_SQ
   * stored procedure.
   *
   * @throws Exception
   */

  public long generateNextWineOrderNumber() throws Exception{


    DataRequest dataRequest = new DataRequest();
    long seq = 0;
    try
    {
      logger.info("String generateNextWineOrderNumber()");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_NEXT_WINE_ORDER_NUMBER_SQ");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      seq = output.longValue();

    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return seq;
  }

       /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_WINE_ORDER_NUMBER_SQ
   * stored procedure.
   *
   * @throws Exception
   */

  public long generateNextSamsOrderNumber() throws Exception{


    DataRequest dataRequest = new DataRequest();
    long seq = 0;
    try
    {
      logger.info("String generateNextWineOrderNumber()");
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_NEXT_SAMS_ORDER_NUMBER_SQ");

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      seq = output.longValue();

    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return seq;
  }

    /**
    * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_PM_ORDER_NUMBER_SQ
    * stored procedure.
    *
    * @throws Exception
    */

    public long generateNextPMOrderNumber() throws Exception{
        DataRequest dataRequest = new DataRequest();
        long seq = 0;
        try
        {
            logger.info("String generateNextPMOrderNumber()");
            /* setup stored procedure input parameters */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("OP_GET_NEXT_PM_ORDER_NUMBER_SQ");
        
            /* execute the stored procedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
            seq = output.longValue();
        
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return seq;
    }

    /**
   * This method serves as a wrapper for the VENUS.VENUS_PKG.GET_MESSAGE_TYPE_COUNTS
   * stored procedure.
   *
   * @param String externalOrderNumber
   * @param  String venusId
   * @throws SAXException, ParserConfigurationException, IOException, SQLException
   *
   */

  public MessageTypeCountsVO getMessageTypeCounts(String externalOrderNumber, String venusId)throws SAXException, ParserConfigurationException, IOException, SQLException
  {

    logger.info("getMessageTypeCounts(String externalOrderNumber ("+externalOrderNumber+"), String venusId ("+venusId+"))");
    MessageTypeCountsVO countsVO = new MessageTypeCountsVO();
    boolean isEmpty = true;
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_MESSAGE_TYPE_COUNTS");
    dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
    dataRequest.addInputParam("IN_VENUS_ID", venusId);
    HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
    if ( results != null )
    {
        isEmpty = false;
        countsVO.setCanCount(((BigDecimal) results.get("OUT_CAN_COUNT")).intValue());
        countsVO.setConCount(((BigDecimal) results.get("OUT_CON_COUNT")).intValue());
        countsVO.setFtdCount(((BigDecimal) results.get("OUT_FTD_COUNT")).intValue());
        countsVO.setPrintedCount(((BigDecimal) results.get("OUT_PRINTED_COUNT")).intValue());
        countsVO.setRejCount(((BigDecimal) results.get("OUT_REJ_COUNT")).intValue());
    }
    if (isEmpty)
      return null;
    else
      return countsVO;
  }

  public int getNumberOfTimesSent(String externalOrderNumber, String messageType) throws Exception
  {
    logger.info("getNumberOfTimesSent(String externalOrderNumber ("+externalOrderNumber+"), String messageType("+messageType+"))");
    DataRequest dataRequest = null;
    int retVal = 0;

    dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(this.conn);
    dataRequest.setStatementID("OP_VENUS.GET_NUM_OF_TIMES_MESSAGE_SENT");
    dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", externalOrderNumber);
    dataRequest.addInputParam("IN_MESSAGE_TYPE", messageType);

    retVal = Integer.parseInt(DataAccessUtil.getInstance().execute(dataRequest).toString());

    return retVal;
  }

    /**
     *  This method serves as a wrapper for the VENUS.VENUS_PKG.GET_VENUS_FTD_ORDERS
     *  stored procedure. Get a list of venus orders that need to be processed.
     *
     * @param int batchSize
     * @return List
     */
     public List getVenusOrders(int batchSize, String vendorType, String vendorId) throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
     {

        CachedResultSet crs = null;

        List messages = new ArrayList();

        VenusMessageVO msgVO = null;
        logger.info(" getVenusOrders(int batchSize "+batchSize+",  String vendorType ("+vendorType+"))");
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_VENUS_FTD_ORDERS");
        dataRequest.addInputParam("IN_ROWNUM", new Integer(batchSize).toString());
        dataRequest.addInputParam("IN_VENDOR_TYPE", vendorType);
        dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
        String status = (String)outputs.get("OUT_STATUS");
        if(status.equals("N") )
        {
         String message = (String)outputs.get("OUT_ERROR_MESSAGE");
         throw new Exception(message.toString());
        }

        crs = (CachedResultSet)outputs.get("OUT_CURSOR");

        if( crs != null )
        {
          while ( crs.next() )
          {
            msgVO = new VenusMessageVO();
            msgVO.setVenusId(crs.getString("VENUS_ID"));
            msgVO.setVenusOrderNumber(crs.getString("VENUS_ORDER_NUMBER"));
            msgVO.setVenusStatus(crs.getString("VENUS_STATUS"));
            msgVO.setSendingVendor(crs.getString("SENDING_VENDOR"));
            msgVO.setFillingVendor(crs.getString("FILLING_VENDOR"));
            msgVO.setOrderDate(getUtilDate(crs.getObject("ORDER_DATE")));
            msgVO.setRecipient(crs.getString("RECIPIENT"));
            msgVO.setBusinessName(crs.getString("BUSINESS_NAME"));
            msgVO.setAddress1(crs.getString("ADDRESS_1"));
            msgVO.setAddress2(crs.getString("ADDRESS_2"));
            msgVO.setCity(crs.getString("CITY"));
            msgVO.setState(crs.getString("STATE"));
            msgVO.setZip(crs.getString("ZIP"));
            msgVO.setCountry(crs.getString("COUNTRY"));
            msgVO.setPhoneNumber(crs.getString("PHONE_NUMBER"));
            msgVO.setDeliveryDate(getUtilDate(crs.getObject("DELIVERY_DATE")));
            msgVO.setFirstChoice(crs.getString("FIRST_CHOICE"));
            msgVO.setPrice(new Double(crs.getDouble("PRICE")));
            msgVO.setCardMessage(crs.getString("CARD_MESSAGE"));
            msgVO.setShipDate(getUtilDate(crs.getObject("SHIP_DATE")));
            msgVO.setShipMethod(crs.getString("SHIP_METHOD"));
            msgVO.setOperator(crs.getString("OPERATOR"));
            msgVO.setComments(crs.getString("COMMENTS"));
            msgVO.setTransmissionTime(getUtilDate(crs.getObject("TRANSMISSION_TIME")));
            msgVO.setReferenceNumber(crs.getString("REFERENCE_NUMBER"));
            msgVO.setProductID(crs.getString("PRODUCT_ID"));
            msgVO.setCombinedReportNumber(crs.getString("COMBINED_REPORT_NUMBER"));
            msgVO.setRofNumber(crs.getString("ROF_NUMBER"));
            msgVO.setAdjReasonCode(crs.getString("ADJ_REASON_CODE"));
            msgVO.setOverUnderCharge(new Double(crs.getDouble("OVER_UNDER_CHARGE")));
            msgVO.setProcessedIndicator(crs.getString("PROCESSED_INDICATOR"));
            msgVO.setMessageText(crs.getString("MESSAGE_TEXT"));
            msgVO.setSendToVenus(crs.getString("SEND_TO_VENUS"));
            msgVO.setOrderPrinted(crs.getString("SUB_TYPE"));
            msgVO.setSubType(crs.getString("SUB_TYPE"));
            msgVO.setErrorDescription(crs.getString("ERROR_DESCRIPTION"));
            msgVO.setCancelReasonCode(crs.getString("CANCEL_REASON_CODE"));
            msgVO.setCompOrder(crs.getString("COMP_ORDER"));
            msgVO.setVendorSKU(crs.getString("VENDOR_SKU"));
            msgVO.setProductDescription(crs.getString("PRODUCT_DESCRIPTION"));
            msgVO.setProductWeight(crs.getString("PRODUCT_WEIGHT"));
            msgVO.setMessageDirection(crs.getString("MESSAGE_DIRECTION"));
            msgVO.setExternalSystemStatus(crs.getString("EXTERNAL_SYSTEM_STATUS"));
            msgVO.setVendorId(crs.getString("VENDOR_ID"));
            msgVO.setShippingSystem(crs.getString("SHIPPING_SYSTEM"));
            msgVO.setTrackingNumber(crs.getString("TRACKING_NUMBER"));
            msgVO.setPrintedStatusDate(getUtilDate(crs.getObject("PRINTED")));
            msgVO.setShippedStatusDate(getUtilDate(crs.getObject("SHIPPED")));
            msgVO.setCancelledStatusDate(getUtilDate(crs.getObject("CANCELLED")));
            msgVO.setFinalCarrier(crs.getString("FINAL_CARRIER"));
            msgVO.setFinalShipMethod(crs.getString("FINAL_SHIP_METHOD"));
            msgVO.setSdsStatus(crs.getString("SDS_STATUS"));
            msgVO.setPersonalGreetingId(crs.getString("PERSONAL_GREETING_ID"));

            messages.add(msgVO);
          }
        }
        return messages;
    }

      /**
     *  This method serves as a wrapper for the VENUS.VENUS_PKG.UPDATE_VENUS
     *  stored procedure. Updates the venus record.
     *
     * @param VenusMessageVO venusMessageVO
     * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
     */

     public void updateVenus(VenusMessageVO venusMessageVO)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
     {
        logger.info("updateVenus(VenusMessageVO venusMessgeVO)");
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_VENUS");
        dataRequest.addInputParam("IN_VENUS_ID", venusMessageVO.getVenusId());
        dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusMessageVO.getVenusOrderNumber());
        dataRequest.addInputParam("IN_VENUS_STATUS", venusMessageVO.getVenusStatus());
        dataRequest.addInputParam("IN_MSG_TYPE", venusMessageVO.getMsgType());
        dataRequest.addInputParam("IN_SENDING_VENDOR", venusMessageVO.getSendingVendor());
        dataRequest.addInputParam("IN_FILLING_VENDOR", venusMessageVO.getFillingVendor());
        dataRequest.addInputParam("IN_ORDER_DATE", venusMessageVO.getOrderDate() == null ? null : new java.sql.Date(venusMessageVO.getOrderDate().getTime()));
        dataRequest.addInputParam("IN_RECIPIENT", venusMessageVO.getRecipient());
        dataRequest.addInputParam("IN_BUSINESS_NAME", venusMessageVO.getBusinessName());
        dataRequest.addInputParam("IN_ADDRESS_1", venusMessageVO.getAddress1());
        dataRequest.addInputParam("IN_ADDRESS_2", venusMessageVO.getAddress2());
        dataRequest.addInputParam("IN_CITY", venusMessageVO.getCity());
        dataRequest.addInputParam("IN_STATE", venusMessageVO.getState());
        dataRequest.addInputParam("IN_ZIP", venusMessageVO.getZip());
        dataRequest.addInputParam("IN_PHONE_NUMBER", venusMessageVO.getPhoneNumber());
        dataRequest.addInputParam("IN_DELIVERY_DATE", venusMessageVO.getDeliveryDate() == null ? null : new java.sql.Date(venusMessageVO.getDeliveryDate().getTime()));
        dataRequest.addInputParam("IN_FIRST_CHOICE", venusMessageVO.getFirstChoice());
        dataRequest.addInputParam("IN_PRICE", venusMessageVO.getPrice() == null ? null : venusMessageVO.getPrice().toString());
        dataRequest.addInputParam("IN_CARD_MESSAGE", venusMessageVO.getCardMessage());
        dataRequest.addInputParam("IN_SHIP_DATE", venusMessageVO.getShipDate() == null ? null : new java.sql.Date(venusMessageVO.getShipDate().getTime()));
        dataRequest.addInputParam("IN_OPERATOR", venusMessageVO.getOperator());
        dataRequest.addInputParam("IN_COMMENTS", venusMessageVO.getComments());
        dataRequest.addInputParam("IN_TRANSMISSION_TIME", venusMessageVO.getTransmissionTime() == null ? null : new java.sql.Date(venusMessageVO.getTransmissionTime().getTime()));
        dataRequest.addInputParam("IN_REFERENCE_NUMBER", venusMessageVO.getReferenceNumber());
        dataRequest.addInputParam("IN_PRODUCT_ID", venusMessageVO.getProductId());
        dataRequest.addInputParam("IN_COMBINED_REPORT_NUMBER", venusMessageVO.getCombinedReportNumber());
        dataRequest.addInputParam("IN_ROF_NUMBER", venusMessageVO.getRofNumber());
        dataRequest.addInputParam("IN_ADJ_REASON_CODE", venusMessageVO.getAdjReasonCode());
        dataRequest.addInputParam("IN_OVER_UNDER_CHARGE", venusMessageVO.getOverUnderCharge() == null ? null : venusMessageVO.getOverUnderCharge().toString());
        dataRequest.addInputParam("IN_PROCESSED_INDICATOR", venusMessageVO.getProcessedIndicator());
        dataRequest.addInputParam("IN_MESSAGE_TEXT", venusMessageVO.getMessageText());
        dataRequest.addInputParam("IN_SEND_TO_VENUS", venusMessageVO.getSendToVenus());
        dataRequest.addInputParam("IN_ORDER_PRINTED", venusMessageVO.getOrderPrinted());
        dataRequest.addInputParam("IN_SUB_TYPE", venusMessageVO.getSubType());
        dataRequest.addInputParam("IN_ERROR_DESCRIPTION", venusMessageVO.getErrorDescription());
        dataRequest.addInputParam("IN_COUNTRY", venusMessageVO.getCountry());
        dataRequest.addInputParam("IN_CANCEL_REASON_CODE", venusMessageVO.getCancelReasonCode());
        dataRequest.addInputParam("IN_COMP_ORDER", venusMessageVO.getCompOrder());
        dataRequest.addInputParam("IN_OLD_PRICE", venusMessageVO.getOldPrice() == null ? null : venusMessageVO.getOldPrice().toString());
        dataRequest.addInputParam("IN_SHIP_METHOD", venusMessageVO.getShipMethod());
        dataRequest.addInputParam("IN_VENDOR_SKU", venusMessageVO.getVendorSKU());
        dataRequest.addInputParam("IN_PRODUCT_DESCRIPTION", venusMessageVO.getProductDescription());
        dataRequest.addInputParam("IN_PRODUCT_WEIGHT", venusMessageVO.getProductWeight());
        dataRequest.addInputParam("IN_SHIPPING_SYSTEM", venusMessageVO.getShippingSystem());
        dataRequest.addInputParam("IN_TRACKING_NUMBER", venusMessageVO.getTrackingNumber());
        dataRequest.addInputParam("IN_PRINTED", venusMessageVO.getPrintedStatusDate() == null ? null : new java.sql.Date(venusMessageVO.getPrintedStatusDate().getTime()));
        dataRequest.addInputParam("IN_SHIPPED", venusMessageVO.getShippedStatusDate() == null ? null : new java.sql.Date(venusMessageVO.getShippedStatusDate().getTime()));
        dataRequest.addInputParam("IN_CANCELLED", venusMessageVO.getCancelledStatusDate() == null ? null : new java.sql.Date(venusMessageVO.getCancelledStatusDate().getTime()));
        dataRequest.addInputParam("IN_FINAL_CARRIER", venusMessageVO.getFinalCarrier());
        dataRequest.addInputParam("IN_FINAL_SHIP_METHOD", venusMessageVO.getFinalShipMethod());
        dataRequest.addInputParam("IN_PERSONAL_GREETING_ID", venusMessageVO.getPersonalGreetingId());


        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
        String status = (String)outputs.get("OUT_STATUS");

        if(status.equals("N") )
        {
         String message = (String)outputs.get("OUT_ERROR_MESSAGE");
         throw new Exception(message.toString());
        }
     }

     /**
     *  This method serves as a wrapper for the VENUS.VENUS_PKG.UPDATE_VENUS_STATUS
     *  stored procedure. Updates the venus status.
     *
     * @param String venusId
     * @param String venusStatus
     * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
     */

     public void updateVenusStatus(String venusId, String venusStatus)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
     {
        logger.info("updateVenusStatus(String venusId ("+venusId+"), String venusStatus ("+venusStatus+"))");
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_UPDATE_VENUS_STATUS");
        dataRequest.addInputParam("IN_VENUS_ID", venusId);
        dataRequest.addInputParam("IN_VENUS_STATUS", venusStatus);

        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
        String status = (String)outputs.get("OUT_STATUS");

        if(status.equals("N") )
        {
         String message = (String)outputs.get("OUT_MESSAGE");
         throw new Exception(message.toString());
        }
     }


    /**
   * This method serves as a wrapper for the GLOBAL.INVENTORY_MAINT_PKG.INCREMENT_PRODUCT_INVENTORY
   * stored procedure.
   *
   * @param String productId
   * @param String vendorId
   * @param  String increaseAmount
   * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
   *
   */

  public void incrementProductInventory(String productId, String vendorId, int increaseAmount)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
  {

    logger.info("incrementProductInventory(String productId ("+productId+"), int increaseAmount ("+increaseAmount+"), vendor id "+vendorId);
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_INCREMENT_PRODUCT_INVENTORY");
    dataRequest.addInputParam("IN_PRODUCT_ID", productId);
    dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
    dataRequest.addInputParam("IN_INCREASE_AMOUNT", new Integer(increaseAmount).toString());
    HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
    if ( results != null )
    {
        if (((String) results.get("OUT_STATUS")).equalsIgnoreCase("N"))
        {
          throw new Exception((String) results.get("OUT_ERROR_MESSAGE"));
        }
    }
    else throw new Exception("INCREMENT_PRODUCT_INVENTORY did not return any rows");


  }

    /**
   * This method serves as a wrapper for the GLOBAL.INVENTORY_MAINT_PKG.DECREMENT_PRODUCT_INVENTORY
   * stored procedure.
   *
   * @param String productId
   * @param String vendorId
   * @param  String decreaseAmount
   * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
   *
   */

  public boolean decrementProductInventory(String productId, String vendorId,  int decreaseAmount)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
  {
    logger.info("decrementProductInventory(String productId ("+productId+"), String vendorId (" +vendorId+ ") int decreaseAmount ("+decreaseAmount+"))");

    boolean invLevelReached = false;

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_DECREMENT_PRODUCT_INVENTORY");
    dataRequest.addInputParam("IN_PRODUCT_ID", productId);
    dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
    dataRequest.addInputParam("IN_DECREASE_AMOUNT", new Integer(decreaseAmount).toString());
    HashMap results = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);
    if ( results != null )
    {

        String levelFlag = (String) results.get("OUT_INVENTORY_LEVEL_REACHED");
        if(levelFlag != null && levelFlag.equalsIgnoreCase("Y"))
        {
            invLevelReached = true;
        }

        if (((String) results.get("OUT_STATUS")).equalsIgnoreCase("N"))
        {
          throw new Exception((String) results.get("OUT_ERROR_MESSAGE"));
        }
    }
    else throw new Exception("DECREMENT_PRODUCT_INVENTORY did not return any rows");

    return invLevelReached;

  }
      /**
   * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_DAILY_FTP_COUNT
   * stored procedure.
   * @param String vendorId
   * @param String ftpType
   * @return int count
   * @throws Exception
   */

  public int getDailyFTPCount(String vendorId, String ftpType) throws Exception{

    logger.info("getDailyFTPCount(String vendorId ("+vendorId+"), String ftpType ("+ftpType+"))");
    DataRequest dataRequest = new DataRequest();
    int count = 0;
    try
    {
      /* setup stored procedure input parameters */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_DAILY_FTP_COUNT");
      dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
      dataRequest.addInputParam("IN_TRANSMISSION_TYPE", ftpType);

      /* execute the stored prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
      count = output.intValue();
    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
    return count;
  }
/**
   * This method is a wrapper for the VENUS.VENUS_PKG.GET_CUSTOMERS_LAST_MONTH
   * stored procedures.  It only populates the CustomerVO with select customer
   * fields.  Do not expect all of the customerVO fields to be populated.
   * @throws Exception
   * @return List
   * @param String year
   * @param String month
   * @param Stromg vendorId
   */
  public List getCustomers(String vendorId, String month, String year)throws Exception
  {
    logger.info("getCustomers(String vendorId("+vendorId+"), String month("+month+"), String year("+year+"))");
    CachedResultSet results = null;
    ArrayList customerList = new ArrayList();

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_CUSTOMERS_LAST_MONTH");
    dataRequest.addInputParam("IN_VENDOR_ID", vendorId);
    dataRequest.addInputParam("IN_MONTH", month);
    dataRequest.addInputParam("IN_YEAR", year);

    results = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if ( results != null )
    {

      while ( results.next() )
      {
         FTPCustomerVO ftpCustomerVO = new FTPCustomerVO();
         ftpCustomerVO.setVenusOrderNumber(results.getString("VENUS_ORDER_NUMBER"));
         ftpCustomerVO.setRecipientName(results.getString("FIRST_NAME")+ " " + results.getString("LAST_NAME"));
         ftpCustomerVO.setBusinessName(results.getString("BUSINESS_NAME"));
         ftpCustomerVO.setAddress1(results.getString("ADDRESS_1"));
         ftpCustomerVO.setAddress2(results.getString("ADDRESS_2"));
         ftpCustomerVO.setCity(results.getString("CITY"));
         ftpCustomerVO.setState(results.getString("STATE"));
         ftpCustomerVO.setZip(results.getString("ZIP_CODE"));
         ftpCustomerVO.setPhoneNumber(results.getString("PHONE_NUMBER"));
         String dateString  = results.getString("BIRTHDAY");
         Date birthDate= FieldUtils.formatStringToUtilDate(dateString);
         ftpCustomerVO.setBirthday(birthDate);
         customerList.add(ftpCustomerVO);
      }
    }
    return customerList;
  }

  /*
   * This method will return a list of all the mesages that have not
   * been verifed by an external system.  (Currently this only applies to
   * FTD messages that are sent to Escalate).
   */
    public List verifyMessages(int minutes) throws Exception
    {
    CachedResultSet results = null;
    ArrayList orderList = new ArrayList();

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_UNVERIFIED_MESSAGES");
    dataRequest.addInputParam("IN_MINUTES", Integer.toString(minutes));

    Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
    String status = (String)outputs.get("OUT_STATUS");

    if(status.equals("N") )
    {
     String message = (String)outputs.get("OUT_ERROR_MESSAGE");
     throw new Exception(message.toString());
    }


    results = (CachedResultSet)outputs.get("OUT_CUR");

    if ( results != null )
    {

      while ( results.next() )
      {
         String orderNumber = results.getString("VENUS_ORDER_NUMBER");
         orderList.add(orderNumber);
      }
    }
    return orderList;
    }


  public FedExDeliveryConfVO getFedExDeliveryConfVO(String zipCode, String shipMethod, String dayOfWeek) throws Exception
  {
    logger.info("VenusDAO.getFedExDeliveryConfVO(String zipCode (" +zipCode+ " ), String shipMethod (" +shipMethod+ " ),  String dayOfWeek (" +dayOfWeek +")");
    FedExDeliveryConfVO fedExDeliveryConfVO = null;
    CachedResultSet crs = null;
    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_FEDEX_COMMITMENT_DETAILS");
    dataRequest.addInputParam("IN_ZIPCODE", zipCode);
    dataRequest.addInputParam("IN_SHIP_METHOD", shipMethod);
    dataRequest.addInputParam("IN_DAY_OF_WEEK", dayOfWeek);
    crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

    if( crs != null )
    {
      while ( crs.next() )
      {
        fedExDeliveryConfVO = new FedExDeliveryConfVO();
        fedExDeliveryConfVO.setCommitmentIndicator(crs.getString("COMMITMENT_INDICATOR"));
        fedExDeliveryConfVO.setShipMethod(crs.getString("SHIP_METHOD"));
        fedExDeliveryConfVO.setDayOfWeek(crs.getString("DAY_OF_WEEK"));
        fedExDeliveryConfVO.setDeliveryAvailable(crs.getString("DELIVERY_AVAILABLE"));
        fedExDeliveryConfVO.setDeliveryTime(crs.getString("DELIVERY_TIME"));
        fedExDeliveryConfVO.setDeliveryBusinessDays( crs.getObject("DELIVERY_BUSINESS_DAYS") == null ? 0 : new Integer(crs.getString("DELIVERY_BUSINESS_DAYS")).intValue());
        fedExDeliveryConfVO.setAddOneBusinessDayForHi(crs.getString("ADD_ONE_BUSINESS_DAY_FOR_HI"));
        fedExDeliveryConfVO.setSatHiDeliveryAvailable(crs.getString("SAT_HI_DELIVERY_AVAILABLE"));
      }
    }
    return fedExDeliveryConfVO;
  }


  private long getLong(Object obj)
  {
      return obj == null ? 0 : Integer.parseInt(obj.toString());
  }

  private double getDouble(Object obj)
  {
      return obj == null ? 0.0 : Double.parseDouble(obj.toString());
  }

     /*
     * Converts a database timestamp to a java.util.Date.
     */
    private java.util.Date getUtilDate(Object time)
    {


      java.util.Date theDate = null;
      boolean test  = false;
      if(time != null){
              if(test || time instanceof Timestamp)
              {
                Timestamp timestamp = (Timestamp)time;
                theDate = new java.util.Date(timestamp.getTime());
              }
              else
              {
                theDate = (java.util.Date)time;
                //theDate = new java.util.Date(timestamp.getTime());
               }
      }


      return theDate;
    }

  /**
     This method the master product id for the passed in product id/sub code.
     *
     * @return
     * @throws java.lang.Exception
     */
  public String getProductMasterId(String productId) throws Exception
  {
    String masterProductId = productId;
    CachedResultSet results = null;

    DataRequest dataRequest = new DataRequest();
    dataRequest.reset();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_PRODUCT_BY_ANY_ID");
    dataRequest.addInputParam("IN_PRODUCT_ID", productId);

    //Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);

    //results = (CachedResultSet)outputs.get("OUT_CUR");
    results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

    if ( results != null && results.next() ) {
        masterProductId = results.getString("productId");
    }

    return masterProductId;
  }

  /**
   * This method is a wrapper for the SP_GET_ORDER_DETAIL_PAYMENT SP.
   * It populates a payment VO based on the returned record set.
   * WARNING:  Method does not populate all the payment fields, thus poses a risk,
   * especially for updates.  Use OrderDAO.getCreditCardPayment instead.
   *
   * @param OrderDetailVO
   * @return java.util.List
   */
  public List getPayment(String orderDetailId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    List paymentList = new ArrayList();
    boolean isEmpty = true;
    CachedResultSet outputs = null;

    logger.info("getPayment for Venus order(String orderDetailId ("+orderDetailId+")) :: List");

    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);

    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_ORDER_DETAIL_PAYMENT");
    dataRequest.setInputParams(inputParams);

    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    /* populate object */
    while (outputs.next())
    {
      PaymentVO payment = new PaymentVO();
      payment.setPaymentId(outputs.getLong(VenusConstants.PAYMENT_ID));
      payment.setOrderGuid(outputs.getString(VenusConstants.ORDER_GUID));
      payment.setAdditionalBillId(outputs.getString(VenusConstants.ADDITIONAL_BILL_ID));
      payment.setPaymentType(outputs.getString(VenusConstants.PAYMENT_TYPE));
      payment.setCcId(outputs.getLong(VenusConstants.CC_ID));
      payment.setCreditAmount(outputs.getDouble(VenusConstants.CREDIT_AMOUNT));
      payment.setDebitAmount(outputs.getDouble(VenusConstants.DEBIT_AMOUNT));
      payment.setAuthResult(outputs.getString(VenusConstants.AUTH_RESULT));
      payment.setAuthNumber(outputs.getString(VenusConstants.AUTH_NUMBER));
      payment.setAvsCode(outputs.getString(VenusConstants.AVS_CODE));
      payment.setAcqReferenceNumber(outputs.getString(VenusConstants.ACQ_REFERENCE_NUMBER));
      payment.setGcCouponNumber(outputs.getString(VenusConstants.GC_COUPON_NUMBER));
      payment.setAuthOverrideFlag(outputs.getString(VenusConstants.AUTH_OVERRIDE_FLAG)==null?"Y":outputs.getString(VenusConstants.AUTH_OVERRIDE_FLAG));
      payment.setPaymentIndicator(outputs.getString(VenusConstants.PAYMENT_INDICATOR));
      payment.setRefundId(outputs.getString(VenusConstants.REFUND_ID));
      paymentList.add(payment);
    }

      return paymentList;
  }

  /**
   * This method checks the VENUS.VENUS table to see if there is a comp order
   * associated with the venus order number passed in.
   * @param String venusOrderNumber
   * @return String
   */
    public String checkVenusMessagesForComp(String venusOrderNumber) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(this.conn);
      dataRequest.setStatementID("OP_IS_VENDOR_COMP_ORDER");
      dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusOrderNumber);

      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String hasCompOrder = (String) dataAccessUtil.execute(dataRequest);

      return hasCompOrder;

    }

    /**
     * This method returns the FTD delivery date corresponding to the venus
     * order number passed in.
     * @param venusOrderNumber The venus order number to return the delivery
     * date for.
     * @return Delivery date corresponding to the venus order number.
     * @throws Exception
     * @author Andy Liakas, 09/11/2006
     */
    public Date getFTDDeliveryDate(String venusOrderNumber) throws Exception {
        CachedResultSet results = null;

        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_FTD_DELIVERY_DATE");
        dataRequest.addInputParam("IN_VENUS_ORDER_NUMBER", venusOrderNumber);

        results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        Date ftdDeliveryDate = null;

        if ( results != null && results.next() ) {
            ftdDeliveryDate = results.getDate("delivery_date");
        }

        return ftdDeliveryDate;
    }
    
    /**
    * This method will remove a previously assigned Zone Jump order from the vendor it was assigned to and  
    * remove that box from the vendor's pallet count so the space can be used for another package.
    *
    * @param String venusId
    * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    */

    public void removeOrderFromVendor(String venusId)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    {
       logger.info("removeOrderFromVendor(String venusId ("+venusId+"))");
       DataRequest dataRequest = new DataRequest();
       dataRequest.reset();
       dataRequest.setConnection(conn);
       dataRequest.setStatementID("OP_REMOVE_ORDER_FROM_VENDOR");
       dataRequest.addInputParam("IN_VENUS_ID", venusId);
       dataRequest.addInputParam("IN_UPDATED_BY", "ORDER_PROCESSING");

       Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
       String status = (String)outputs.get("OUT_STATUS");

       if(status.equals("N") )
       {
        String message = (String)outputs.get("OUT_MESSAGE");
        throw new Exception(message.toString());
       }
    }
    
    /**
     * This method checks to see if all vendors associated to product on the order are
     * can use the new shipping system 
     * @param String productId
     * @return String
     */
      public String sendToNewShippingSystem(String productId) throws Exception
      {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(this.conn);
        dataRequest.setStatementID("OP_SEND_TO_NEW_SHIP_SYSTEM");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        String sendToNewShipSystem = (String) dataAccessUtil.execute(dataRequest);

        return sendToNewShipSystem ;

      }
      
      /**
       *  This method updates the new shipping system flag on the venus record
       *
       * @param String venusId
       * @throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
       */

       public void updateNewShippingSystemFlag(String venusId)throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
       {
          logger.info("updateNewShippingSystemFlag(String venusId ("+venusId+"))");
          DataRequest dataRequest = new DataRequest();
          dataRequest.reset();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("UPDATE_NEW_SHIPPING_SYSTEM");
          dataRequest.addInputParam("IN_VENUS_ID", venusId);

          Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
          String status = (String)outputs.get("OUT_STATUS");

          if(status.equals("N") )
          {
           String message = (String)outputs.get("OUT_MESSAGE");
           throw new Exception(message.toString());
          }
       }
       
       /**
        * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_PC_ORDER_NUMBER_SQ
        * stored procedure.
        *
        * @throws Exception
        */

        public long generateNextPCOrderNumber() throws Exception{
            DataRequest dataRequest = new DataRequest();
            long seq = 0;
            try
            {
                logger.info("String generateNextPCOrderNumber()");
                /* setup stored procedure input parameters */
                dataRequest.setConnection(conn);
                dataRequest.setStatementID("OP_GET_NEXT_PC_ORDER_NUMBER_SQ");
            
                /* execute the stored prodcedure */
                DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
                seq = output.longValue();
            
            } catch (Exception e) {
                logger.error(e);
                throw e;
            }
            return seq;
        }
        
        /**
         * This method serves as a wrapper for the VENUS.UTIL_PKG.GET_NEXT_WEST_ORDER_NUMBER_SQ
         * stored procedure.
         *
         * @throws Exception
         */

         public long generateFtdWestOrderNumber() throws Exception{


           DataRequest dataRequest = new DataRequest();
           long idNum = 0;
           try
           {
             logger.info("generateFtdWestOrderNumber :: String ");
             /* setup stored procedure input parameters */
             dataRequest.setConnection(conn);
             dataRequest.setStatementID("OP_GET_NEXT_WEST_ORDER_NUMBER_SQ");

             /* execute the stored prodcedure */
             DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
             BigDecimal output =(BigDecimal) dataAccessUtil.execute(dataRequest);
             idNum = output.longValue();
       } catch (Exception e) {
             logger.error(e);
             throw e;
           }
           return idNum;
         }


}
