package com.ftd.op.test;

import com.ftd.op.order.vo.CompanyVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderBillVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.SecondChoiceVO;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.op.order.vo.VendorVO;
import java.text.DateFormat;
import java.util.Date;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;

import com.ftd.op.test.JdbcTestFixture;
import com.ftd.op.test.RetrieveOrderDetailTestFixture;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import java.util.LinkedList;
import java.util.Iterator;

/* test methods returning VO objects in the OrderDAO */
public class OrderDAOTestCase extends TestCase 
{
  JdbcTestFixture fixture = new JdbcTestFixture();
  Connection conn;

  public OrderDAOTestCase(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture.setUp();
    this.conn = fixture.getConnection();
  }

  public void tearDown() throws Exception
  {
    fixture.tearDown();
  }
 
  /**
   * CompanyVO getCompany(String companyId)
   * Test by sending an invalid companyId
   * Expecting the returned VO to be null
   */
  public void testGetCompany1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      CompanyVO vo = dao.getCompany(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * CompanyVO getCompany(String companyId)
   * Test by sending a valid companyId
   * Expecting the returned VO to be populated
   */
  public void testGetCompany2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      CompanyVO vo = dao.getCompany("FTD");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * CustomerVO getCustomer(long customerId)
   * Test by sending an invalid customerId
   * Expecting the returned VO to be null
   */
  public void testGetCustomer1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      CustomerVO vo = dao.getCustomer(0);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * CustomerVO getCustomer(long customerId)
   * Test by sending a valid customerId
   * Expecting the returned VO to be populated
   */
  public void testGetCustomer2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      CustomerVO vo = dao.getCustomer(999001);     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * FloristVO getDailyFlorist(OrderVO order, OrderDetailVO orderDtl)
   * Test by sending an invalid customerId in the order object
   * Expecting the returned VO to be null
   */
  public void testGetDailyFlorist1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderVO order = new OrderVO();
      order.setCustomerId(0);
      OrderDetailVO detail = new OrderDetailVO();
      detail.setDeliveryDate(new Date());
      LinkedList vo = dao.getDailyFlorist(detail);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * FloristVO getDailyFlorist(OrderVO order, OrderDetailVO orderDtl)
   * Test by sending a valid customerId in the order object and a the 
   * same delivery date in the database for orderDetailId: 998008
   * Expecting the returned VO to be populated
   */
  public void testGetDailyFlorist2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderVO order = new OrderVO();
      order.setCustomerId(102581);
      OrderDetailVO detail = new OrderDetailVO();
      DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
      detail.setDeliveryDate(df.parse("05/22/2005"));
      LinkedList vo = dao.getDailyFlorist(detail);     
      assertTrue(vo!=null && vo.size() > 0);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * FloristVO getFlorist(String floristId)
   * Test by sending an invalid floristId
   * Expecting the returned VO to be null
   */
  public void testGetFlorist1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      FloristVO vo = dao.getFlorist(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * FloristVO getFlorist(String floristId)
   * Test by sending a valid floristId
   * Expecting the returned VO to be populated
   */
  public void testGetFlorist2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      FloristVO vo = dao.getFlorist("52-5568AA");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * GlobalParameterVO getGlobalParameter(String name)
   * Test by sending an invalid name
   * Expecting the returned VO to be null
   */
  public void testGetGlobalParameter1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      GlobalParameterVO vo = dao.getGlobalParameter(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * GlobalParameterVO getGlobalParameter(String name)
   * Test by sending a valid name
   * Expecting the returned VO to be populated
   */
  public void testGetGlobalParameter2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      GlobalParameterVO vo = dao.getGlobalParameter("AUTH_FLAG");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * GlobalParameterVO getGlobalParameter(String context, String name)
   * Test by sending an invalid name
   * Expecting the returned VO to be null
   */
  public void testGetGlobalParameter3()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      GlobalParameterVO vo = dao.getGlobalParameter("ORDER_PROCESSING",null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * GlobalParameterVO getGlobalParameter(String context, String name)
   * Test by sending an invalid context
   * Expecting the returned VO to be null
   */
  public void testGetGlobalParameter4()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      GlobalParameterVO vo = dao.getGlobalParameter(null,"AUTH_FLAG");     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * GlobalParameterVO getGlobalParameter(String context, String name)
   * Test by sending a valid name
   * Expecting the returned VO to be populated
   */
  public void testGetGlobalParameter5()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      GlobalParameterVO vo = dao.getGlobalParameter("ORDER_PROCESSING","AUTH_FLAG");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * OrderVO getOrder(String orderGuid)
   * Test by sending an invalid orderGuid
   * Expecting the returned VO to be null
   */
  public void testGetOrder1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderVO vo = dao.getOrder(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * OrderVO getOrder(String orderGuid)
   * Test by sending a valid orderGuid
   * Expecting the returned VO to be populated
   */
  public void testGetOrder2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderVO vo = dao.getOrder("N998008");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * OrderBillVO getOrderBill(String orderDetailId)
   * Test by sending an invalid orderDetailId
   * Expecting the returned VO to be null
   */
  public void testGetOrderBill1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderBillVO vo = dao.getOrderBill(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * OrderBillVO getOrderBill(String orderDetailId)
   * Test by sending a valid orderDetailId
   * Expecting the returned VO to be populated
   */
  public void testGetOrderBill2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderBillVO vo = dao.getOrderBill("998004");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * OrderDetailVO getOrderDetail(String orderDetailID)
   * Test by sending an invalid orderDetailID
   * Expecting the returned VO to be null
   */
  public void testGetOrderDetail1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO vo = dao.getOrderDetail(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * OrderDetailVO getOrderDetail(String orderDetailID)
   * Test by sending a valid orderDetailID
   * Expecting the returned VO to be populated
   */
  public void testGetOrderDetail2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO vo = dao.getOrderDetail("998001");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * PaymentVO getPayment(OrderDetailVO orderDtl)
   * Test by sending an invalid orderGuid in the OrderDetailVO object
   * Expecting the returned VO to be null
   */
  public void testGetPayment1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO detail = new OrderDetailVO();
      detail.setOrderGuid(null);
      PaymentVO vo = dao.getPayment(detail);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * PaymentVO getPayment(OrderDetailVO orderDtl)
   * Test by sending a valid orderGuid in the OrderDetailVO object
   * Expecting the returned VO to be populated
   */
  public void testGetPayment2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO detail = new OrderDetailVO();
      detail.setOrderGuid("N998004");
      PaymentVO vo = dao.getPayment(detail);     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * ProductVO getProduct(String productId)
   * Test by sending an invalid productId
   * Expecting the returned VO to be null
   */
  public void testGetProduct1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      ProductVO vo = dao.getProduct(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * ProductVO getProduct(String productId)
   * Test by sending a valid productId
   * Expecting the returned VO to be populated
   */
  public void testGetProduct2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      ProductVO vo = dao.getProduct("7038");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * SecondChoiceVO getSecondChoice(String secondChoiceId)
   * Test by sending an invalid secondChoiceId
   * Expecting the returned VO to be null
   */
  public void testGetSecondChoice1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      SecondChoiceVO vo = dao.getSecondChoice(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * SecondChoiceVO getSecondChoice(String secondChoiceId)
   * Test by sending a valid secondChoiceId
   * Expecting the returned VO to be populated
   */
  public void testGetSecondChoice2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      SecondChoiceVO vo = dao.getSecondChoice("99");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * SourceVO getSource(String sourceCode)
   * Test by sending an invalid sourceCode
   * Expecting the returned VO to be null
   */
  public void testGetSource1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      SourceVO vo = dao.getSource(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  
  /**
   * SourceVO getSource(String sourceCode)
   * Test by sending a valid sourceCode
   * Expecting the returned VO to be populated
   */
  public void testGetSource2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      SourceVO vo = dao.getSource("7536");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * VendorVO getVendor(String vendorId)
   * Test by sending an invalid vendorId
   * Expecting the returned VO to be null
   */
  public void testGetVendor1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      VendorVO vo = dao.getVendor(null);     
      assertTrue(vo==null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }  
  }
  public void testIsOrderExists()
  {
	  try{
		 CallableStatement proc = this.conn.prepareCall("call ACCOUNT.ACCOUNT_QUERY_PKG.IS_ACCOUNT_EXISTS(?,?,?,?)");
		 proc.setString("IN_EXTERNAL_ORDER_NUMBER", "C2503603162");
		 proc.registerOutParameter("OUT_RESULT_FLAG", Types.VARCHAR);
		 proc.registerOutParameter("OUT_STATUS", Types.VARCHAR);
		 proc.registerOutParameter("OUT_MESSAGE", Types.VARCHAR);
		 proc.execute();
		 System.out.println("OUT_RESULT_FLAG : "+proc.getString("OUT_RESULT_FLAG"));
		 System.out.println("OUT_STATUS : "+proc.getString("OUT_STATUS"));
		 System.out.println("OUT_MESSAGE : "+proc.getString("OUT_MESSAGE"));
	  }
	  catch (Exception e)
	  {
		  e.printStackTrace();
	  }
  }
  /**
   * VendorVO getVendor(String vendorId)
   * Test by sending a valid vendorId
   * Expecting the returned VO to be populated
   */
  /*public void testGetVendor2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      VendorVO vo = dao.getVendor("");     
      assertTrue(vo!=null);      
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }*/
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderDAOTestCase.class);        
    } 
    catch (Exception e) 
    {
      e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}