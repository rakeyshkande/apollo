package com.ftd.op.test;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import java.sql.Connection;
import com.ftd.op.test.JdbcTestFixture;
import com.ftd.op.test.RetrieveOrderDetailTestFixture;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.OrderDetailVO;

/**
 * Test cases are written for specific data.  
 * Run the following SQL statements:
 * sql_for_OrderServiceDataSpecific3TestCase
 */
public class OrderServiceDataSpecific3TestCase extends TestCase 
{
  JdbcTestFixture fixture = new JdbcTestFixture();
  Connection conn;

  public OrderServiceDataSpecific3TestCase(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture.setUp();
    this.conn = fixture.getConnection();
  }

  public void tearDown() throws Exception
  {
    fixture.tearDown();
  }
  
  /**
   * boolean isGnaddHold(OrderDetailVO)
   * Test by sending an order that has a gnadd hold
   * Expecting order to be sent to sendOrderToGnaddHoldQueue
   */
  public void testIsGnaddHold2() throws Exception
  {
    long orderDetailId = 998007;
    RetrieveOrderDetailTestFixture retrieveOrderDetail = new RetrieveOrderDetailTestFixture(orderDetailId, this.conn);
    retrieveOrderDetail.setUp();
      
    OrderService service = new OrderService(this.conn);
    service.processOrder(retrieveOrderDetail.getOrderDetailVO());  
  }
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderServiceDataSpecific3TestCase.class);        
    } 
    catch (Exception e) 
    {
      System.out.println(e.toString());
      e.printStackTrace();
    }

    return suite;
  } 

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );
  }
}