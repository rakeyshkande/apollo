package com.ftd.op.test;

import com.ftd.op.venus.bo.VenusOutboundMessageBO;

import com.ftd.osp.utilities.ConfigurationUtil;

import java.io.File;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class OrderUploadTestCase extends TestCase {

    public OrderUploadTestCase(String sTestName) {
        super(sTestName);
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run( suite() ); 
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
    
        try 
        {
            suite = new TestSuite();        
            suite.addTest(new OrderUploadTestCase("testFileUpload"));
        } 
        catch (Exception e) 
        {
            //System.out.println(e.toString());
            e.printStackTrace();
        }

        return suite;
    }
    
    public void testFileUpload() throws Exception{
        VenusOutboundMessageBO bo = new VenusOutboundMessageBO(null);
        ConfigurationUtil configutil = ConfigurationUtil.getInstance();
        
        File srcFile = new File("H:\\EMail\\Sent 2004\\Sent 2004_1.zip");
        //File srcFile = new File("C:\\temp\\pdbappconfig.xml");
        bo.sendFile(srcFile,configutil);
    }
}
