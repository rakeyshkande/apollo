package com.ftd.op.test;

import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import java.sql.Connection;

public class RetrieveOrderDetailTestFixture 
{
  OrderDetailVO orderDetail = new OrderDetailVO();
  Connection conn;

  public RetrieveOrderDetailTestFixture(long orderDetailId, Connection conn)
  {
    orderDetail.setOrderDetailId(orderDetailId);
    this.conn = conn;
  }

  public void setUp() throws Exception
  {
    OrderDAO dao = new OrderDAO(this.conn);
    orderDetail = dao.getOrderDetail(new Long(orderDetail.getOrderDetailId()).toString());
  }

  public void tearDown() throws Exception
  {
  }

  public OrderDetailVO getOrderDetailVO()
  {
    return orderDetail;
  } 
}