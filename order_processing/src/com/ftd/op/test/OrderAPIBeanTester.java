package com.ftd.op.test;
import com.ftd.op.order.to.OrderTO;
import com.ftd.op.order.to.RWDFloristTO;
import com.ftd.op.order.to.RWDFloristTOList;
import java.rmi.RemoteException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.ftd.op.order.api.OrderAPIBean;
import com.ftd.op.test.EJBTestFixture;
import junit.framework.Assert;

public class OrderAPIBeanTester extends TestCase 
{
  private EJBTestFixture fixture1 = new EJBTestFixture();
  private OrderTO orderTO;
  
  public OrderAPIBeanTester(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture1.setUp();
    orderTO = new OrderTO();
    orderTO.setOrderDetailId("998004");
  }

  public void tearDown() throws Exception
  {
    fixture1.tearDown();
  }

  /**
   * RWDFloristTO getFillingFlorist(OrderTO)
   */
  public void testgetFillingFlorist() throws Exception
  {
    RWDFloristTO florist = fixture1.order.getFillingFlorist(orderTO);
    assertNotNull("Couldn't find a florist", florist);
  }

  /**
   * RWDFloristTOList getFullFloristList(OrderTO)
   */
  public void testgetFullFloristList() throws Exception
  {
    RWDFloristTOList list = fixture1.order.getFullFloristList(orderTO);
    assertNotNull("Couldn't get full florist list", list);
  }

  /**
   * RWDFloristTOList getTopFloristList(OrderTO)
   */
  public void testgetTopFloristList() throws Exception
  {
    RWDFloristTOList list = fixture1.order.getTopFloristList(orderTO);
    
    assertNotNull("Couldn't get top florist list", list);
  }

  /**
   * String helloWorld(String)
   */
  public void testhelloWorld() throws Exception
  {
    try {
      String output = fixture1.order.helloWorld("Output");
      assertEquals("HelloWorld failed", new String("Hello World: " + "Output" ), output);
    } catch (RemoteException e) 
    {
      assertTrue("Threw a remote exception: " + e.toString(), false);
    }
  }
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderAPIBeanTester.class);        
    } 
    catch (Exception e) 
    {
      System.out.println(e.toString());
      e.printStackTrace();
    }


    return suite;
  } 
}