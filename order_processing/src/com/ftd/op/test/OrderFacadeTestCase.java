package com.ftd.op.test;

import com.ftd.op.order.dao.OrderDAO;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import java.sql.Connection;
import com.ftd.op.test.JdbcTestFixture;
import com.ftd.op.test.RetrieveOrderDetailTestFixture;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;
import java.util.LinkedList;
import java.util.Iterator;

public class OrderFacadeTestCase extends TestCase 
{
  JdbcTestFixture fixture = new JdbcTestFixture();
  Connection conn;

  public OrderFacadeTestCase(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture.setUp();
    this.conn = fixture.getConnection();
  }

  public void tearDown() throws Exception
  {
    fixture.tearDown();
  }
 
  public void testGetFloristList1()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO vo = dao.getOrderDetail("102589");     
      
      OrderService service = new OrderService(this.conn);
      RWDFloristVOList list = service.getFloristList(vo, true, null);
      LinkedList output =  list.getFloristList();
      Iterator iter = output.iterator();
      System.out.println("----GetFloristList returnAll START----");
      while(iter.hasNext())
      {
        RWDFloristVO florist = (RWDFloristVO)iter.next();
        System.out.println(florist.getFloristId());
      }
      System.out.println("----GetFloristList returnAll DONE----");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void testGetFloristList2()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO vo = dao.getOrderDetail("102589");     
      
      OrderService service = new OrderService(this.conn);
      RWDFloristVOList list = service.getFloristList(vo, false, null);
      LinkedList output =  list.getFloristList();
      Iterator iter = output.iterator();
      System.out.println("----GetFloristList returnTop START----");
      while(iter.hasNext())
      {
        RWDFloristVO florist = (RWDFloristVO)iter.next();
        System.out.println(florist.getFloristId());
      }
      System.out.println("----GetFloristList returnTop DONE----");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public void testSelectFlorist()
  { 
    try
    {  
      OrderDAO dao = new OrderDAO(this.conn);
      OrderDetailVO vo = dao.getOrderDetail("102589");     
      
      OrderService service = new OrderService(this.conn);
      RWDFloristVO florist = service.selectFlorist(vo, null);
      System.out.println("----SelectFlorist START----");
      System.out.println(florist.getFloristId());
      System.out.println("----SelectFlorist DONE----");
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderFacadeTestCase.class);        
    } 
    catch (Exception e) 
    {
      e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}