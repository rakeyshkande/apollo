package com.ftd.op.test;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import java.sql.Connection;
import com.ftd.op.test.JdbcTestFixture;
import com.ftd.op.test.RetrieveOrderDetailTestFixture;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.OrderDetailVO;

/**
 * Test cases are written for specific data.  
 * Run the following SQL statements:
 * sql_for_OrderServiceDataSpecific1TestCase
 */
public class OrderServiceDataSpecific1TestCase extends TestCase 
{
  JdbcTestFixture fixture = new JdbcTestFixture();
  Connection conn;

  public OrderServiceDataSpecific1TestCase(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture.setUp();
    this.conn = fixture.getConnection();
  }

  public void tearDown() throws Exception
  {
    fixture.tearDown();
  }
  
  /**
   * boolean isCustomerOnHold(OrderDetailVO)
   * Test by sending an order with a customerId that is on hold
   * Expecting order to be sent to sendOrderToHoldQueue
   */
  public void testIsCustomerOnHold() throws Exception
  {       
      long orderDetailId = 998001;
      RetrieveOrderDetailTestFixture retrieveOrderDetail = new RetrieveOrderDetailTestFixture(orderDetailId, this.conn);
      retrieveOrderDetail.setUp();
      
      OrderService service = new OrderService(this.conn);
      service.processOrder(retrieveOrderDetail.getOrderDetailVO());
  }
  
  /**
   * boolean isOrderAuthorized(OrderDetailVO)
   * Test by sending an order that needs authorization
   * Expecting order to be sent to sendOrderToAuthQueue
   */
  public void testIsOrderAuthorized() throws Exception
  {
    long orderDetailId = 998002;
    RetrieveOrderDetailTestFixture retrieveOrderDetail = new RetrieveOrderDetailTestFixture(orderDetailId, this.conn);
    retrieveOrderDetail.setUp();
      
    OrderService service = new OrderService(this.conn);
    service.processOrder(retrieveOrderDetail.getOrderDetailVO());  
  }
  
  /**
   * boolean isProductAvailable(OrderDetailVO)
   * Test by sending an order that has a product that is not available
   * Expecting order to be sent to sendOrderToHoldQueue
   */
  public void testIsProductAvailable() throws Exception
  {
    long orderDetailId = 998006;
    RetrieveOrderDetailTestFixture retrieveOrderDetail = new RetrieveOrderDetailTestFixture(orderDetailId, this.conn);
    retrieveOrderDetail.setUp();
      
    OrderService service = new OrderService(this.conn);
    service.processOrder(retrieveOrderDetail.getOrderDetailVO());  
  }
  
  /**
   * boolean hasFlorist(OrderDetailVO)
   * Test by sending an order that has a florist assigned
   * Expecting order to insert mercury record.
   */
  public void testHasFlorist() throws Exception
  {
    long orderDetailId = 998008;
    RetrieveOrderDetailTestFixture retrieveOrderDetail = new RetrieveOrderDetailTestFixture(orderDetailId, this.conn);
    retrieveOrderDetail.setUp();
      
    OrderService service = new OrderService(this.conn);
    service.processOrder(retrieveOrderDetail.getOrderDetailVO());        
  }
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderServiceDataSpecific1TestCase.class);        
    } 
    catch (Exception e) 
    {
      //System.out.println(e.toString());
      //e.printStackTrace();
    }

    return suite;
  } 

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );
  }
}