package com.ftd.op.test;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.vo.MercuryVO;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.ftd.op.mercury.api.MercuryAPIBean;

public class MercuryAPIBeanTester extends TestCase 
{
  EJBTestFixture ejbFixture = new EJBTestFixture();
  JdbcTestFixture jdbcFixture = new JdbcTestFixture();
  Connection conn;
  
  HashMap testMap;
  
  public MercuryAPIBeanTester(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    ejbFixture.setUp();
    jdbcFixture.setUp();
    conn = jdbcFixture.getConnection();
          //Make test hash map//
      
      testMap = new HashMap();
      testMap.put("MERCURY_STATUS", "MO");
      testMap.put("OUTBOUND_ID", "AB");
      testMap.put("SENDING_FLORIST","90-8400");
      testMap.put("FILLING_FLORIST", "14-0624AA");
      testMap.put("ORDER_DATE", new Date());
      testMap.put("RECIPIENT", "Ketchup");
      testMap.put("ADDRESS", "Wrigley Field, 1060 West Addison Chicago");
      testMap.put("CITY_STATE_ZIP", "CHICAGO, IL, 60613");
      testMap.put("PHONE_NUMBER", "866/652-2827");
      testMap.put("DELIVERY_DATE", new Date());
      testMap.put("DELIVERY_DATE_TEXT", "OCT 13 - WED");
      testMap.put("FIRST_CHOICE", "C13-3068 My One & Only, 04-05 Wkbk p.63");
      testMap.put("SECOND_CHOICE", "A SIMILAR ARRANGEMENT IN A SIMILAR CONTAINER.");
      testMap.put("PRICE", new Double("64.99"));
      testMap.put("CARD_MESSAGE", "You really toast my buns.  Love, Mustard");
      testMap.put("OCCASION", "ANNIVERSARY OCCASION");
      testMap.put("SPECIAL_INSTRUCTIONS", "NO SPECIAL INSTRUCTIONS");
      testMap.put("PRIORITY", "FTO/N");
      testMap.put("OPERATOR", "WEISS");
      testMap.put("COMMENTS", "COMMENTS");
      testMap.put("SAK_TEXT", "VERIFIED SAT OCT 09 04  5:56P     ORDER #P9230X-4206");
      testMap.put("CTSEQ", new Integer(2462));
      testMap.put("CRSEQ", new Integer(4630));
      testMap.put("TRANSMISSION_TIME", new Date());
      testMap.put("REFERENCE_NUMBER", "16179130*1");
      testMap.put("PRODUCT_ID", "3068");
      testMap.put("ZIP_CODE", "47406-7501");
      testMap.put("ASK_ANSWER_CODE", null);//**
      testMap.put("SORT_VALUE", "20041013120041009175247");
      testMap.put("RETRIEVAL_FLAG", null);//**
      testMap.put("COMBINED_REPORT_NUMBER", null);//**
      testMap.put("ROF_NUMBER", null);//**
      testMap.put("ADJ_REASON_CODE", null);//**
      testMap.put("OVER_UNDER_CHARGE", new Double(2.0));
      testMap.put("FROM_MESSAGE_NUMBER", null);//**
      testMap.put("FROM_MESSAGE_DATE", null);//**
      testMap.put("TO_MESSAGE_NUMBER", null);//**
      testMap.put("TO_MESSAGE_DATE", null);//**
      testMap.put("VIEW_QUEUE", "N");
      testMap.put("MESSAGE_DIRECTION", "OUTBOUND");
      testMap.put("COMP_ORDER",null);//**
      testMap.put("SPOKE_TO", null);//**
      testMap.put("REQUIRE_CONFIRMATION", "N");
      testMap.put("SUFFIX", null);//**
      testMap.put("ORDER_SEQ", null);//**
      testMap.put("ADMIN_SEQ", null);//**
      testMap.put("OLD_PRICE",  new Double(1.0));
      testMap.put("MERCURY_ORDER_NUMBER", "FTD0000086148");
     
  }

  public void tearDown() throws Exception
  {
    ejbFixture.tearDown();
    jdbcFixture.tearDown();
  }

  /**
   * void sendFTDMessage(OrderDetailKeyTO)
   */
  public void testsendFTDMessage()
  {
    assertTrue(true);
  }

  /**
   * FTDMessageTO buildFTDMessageTO() - Helper Method
   */
  private FTDMessageTO buildFTDMessageTO()
  {
      FTDMessageTO to = new FTDMessageTO();
      to.setAddress((String)testMap.get("ADDRESS"));
      to.setCardMessage((String)testMap.get("CARD_MESSAGE"));
      to.setCityStateZip((String)testMap.get("CITY_STATE_ZIP"));
      to.setDeliveryDate((Date)testMap.get("DELIVERY_DATE"));
      to.setDeliveryDateText((String)testMap.get("DELIVERY_DATE_TEXT"));
      to.setDirection((String)testMap.get("MESSAGE_DIRECTION"));
      to.setFillingFlorist((String)testMap.get("FILLING_FLORIST"));
      to.setFirstChoice((String)testMap.get("FIRST_CHOICE"));
      to.setIsCompOrder((String)testMap.get("COMP_ORDER"));
      to.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
      to.setOccasion((String)testMap.get("OCCASION"));
      to.setOperator((String)testMap.get("OPERATOR"));
      to.setOrderDate((Date)testMap.get("ORDER_DATE"));
      to.setOrderDetailId((String)testMap.get("REFERENCE_NUMBER"));
      to.setPhoneNumber((String)testMap.get("PHONE_NUMBER"));
      to.setPrice((Double)testMap.get("PRICE"));
      to.setProductId((String)testMap.get("PRODUCT_ID"));
      to.setRecipient((String)testMap.get("RECIPIENT"));
      to.setRequestConfirmation((String)testMap.get("REQUIRE_CONFIRMATION"));
      to.setSecondChoice((String)testMap.get("SECOND_CHOICE"));
      to.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));
      to.setSpecialInstructions((String)testMap.get("SPECIAL_INSTRUCTIONS"));
      to.setZipCode((String)testMap.get("ZIP_CODE"));
      return to;
  }



  /**
   * void sendFTDMessage(FTDMessageTO)
   */
  public void testsendFTDMessage1()
  {
      FTDMessageTO to = this.buildFTDMessageTO(); 
      ResultTO result = null;
      
      try { 
        result = ejbFixture.mercury.sendFTDMessage(to);
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO mercuryVO = dao.getMessage(result.getKey());
        testEquals(to, "getAddress", mercuryVO, "getAddress");
        testEquals(to, "getCardMessage", mercuryVO, "getCardMessage");
        testEquals(to, "getCityStateZip", mercuryVO, "getCityStateZip");
        testEquals(to, "getDeliveryDate", mercuryVO, "getDeliveryDate");
        testEquals(to, "getDeliveryDateText", mercuryVO, "getDeliveryDateText");
        testEquals(to, "getFirstChoice", mercuryVO, "getFirstChoice");
        testEquals(to, "getOccasion", mercuryVO, "getOccasion");
        testEquals(to, "getOrderDate", mercuryVO, "getOrderDate");
        testEquals(to, "getOrderDetailId", mercuryVO, "getReferenceNumber");        
        testEquals(to, "getPhoneNumber", mercuryVO, "getPhoneNumber");        
        testEquals(to, "getPrice", mercuryVO, "getPrice");        
        testEquals(to, "getProductId", mercuryVO, "getProductId");        
        testEquals(to, "getRecipient", mercuryVO, "getRecipient");        
        testEquals(to, "isRequestConfirmation", mercuryVO, "getRequireConfirmation");        
        testEquals(to, "getSecondChoice", mercuryVO, "getSecondChoice");        
        testEquals(to, "getSendingFlorist", mercuryVO, "getSendingFlorist");        
        testEquals(to, "getSpecialInstructions", mercuryVO, "getSpecialInstructions");        
        testEquals(to, "getZipCode", mercuryVO, "getZipCode");        
      } catch (Exception e)
      {
        e.printStackTrace();
        assertTrue(e.getMessage(), false);
      }
      assertNotNull("Null result", result);
      assertTrue(result.getErrorString(), result.isSuccess());
    System.out.println(result.getKey());
  }
  
 
  
  /**
   * void sendADJMessage(ADJMessageTO)
   */
  public void testsendADJMessage()
  {
    ResultTO ftdResult;    
    ResultTO adjResult;
    FTDMessageTO ftdTo = this.buildFTDMessageTO();
    try
    {
      ftdResult = ejbFixture.mercury.sendFTDMessage(ftdTo);
      assertNotNull("FTD Null result", ftdResult);
      assertTrue(ftdResult.getErrorString(), ftdResult.isSuccess());
      String mercuryMessageId = ftdResult.getKey();
      ADJMessageTO adjTo = new ADJMessageTO();
      adjTo.setAdjustmentReasonCode((String)testMap.get("ADJ_REASON_CODE"));
      adjTo.setCombinedReportNumber((String)testMap.get("COMBINED_REPORT_NUMBER"));
      adjTo.setComments((String)testMap.get("COMMENTS"));
      adjTo.setDirection((String)testMap.get("MESSAGE_DIRECTION"));
      adjTo.setFillingFlorist((String)testMap.get("FILLING_FLORIST"));
      //adjTo.setMercuryOrderNumber(mercuryMessageId);
      adjTo.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
      adjTo.setOldPrice((Double)testMap.get("OLD_PRICE"));
      adjTo.setOperator((String)testMap.get("OPERATOR"));
      adjTo.setPrice((Double)testMap.get("PRICE"));
      adjTo.setRofNumber((String)testMap.get("ROF_NUMBER"));
      adjTo.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));    
      adjResult = ejbFixture.mercury.sendADJMessage(adjTo);
      assertNotNull("Null result", adjResult);
      assertTrue(adjResult.getErrorString(), adjResult.isSuccess());
      // checking that data was entered correctly //
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryVO mercuryVO = dao.getMessage(adjResult.getKey());
       testEquals(ftdResult, "getKey", mercuryVO, "getMercuryOrderNumber");
       testEquals(adjTo, "getMercuryStatus", mercuryVO, "getMercuryStatus");
       testEquals(adjTo, "getMessageType", mercuryVO, "getMessageType");
       testEquals(ftdTo, "getDirection", mercuryVO, "getDirection");
       testEquals(adjTo, "getSendingFlorist", mercuryVO, "getSendingFlorist");
       testEquals(adjTo, "getFillingFlorist", mercuryVO, "getFillingFlorist");
       testEquals(ftdTo, "getOrderDate", mercuryVO, "getOrderDate");
       testEquals(ftdTo, "getRecipient", mercuryVO, "getRecipient");
       testEquals(ftdTo, "getAddress", mercuryVO, "getAddress");
       testEquals(ftdTo, "getCityStateZip", mercuryVO, "getCityStateZip");
       testEquals(ftdTo, "getPhoneNumber", mercuryVO, "getPhoneNumber");
       testEquals(ftdTo, "getDeliveryDate", mercuryVO, "getDeliveryDate");
       testEquals(ftdTo, "getDeliveryDateText", mercuryVO, "getDeliveryDateText");
       testEquals(ftdTo, "getFirstChoice", mercuryVO, "getFirstChoice");
       testEquals(ftdTo, "getSecondChoice", mercuryVO, "getSecondChoice");
       testEquals(adjTo, "getOldPrice", mercuryVO, "getOldPrice");
       testEquals(adjTo, "getPrice", mercuryVO, "getPrice");
       testEquals(ftdTo, "getCardMessage", mercuryVO, "getCardMessage");
       testEquals(ftdTo, "getOccasion", mercuryVO, "getOccasion");
       testEquals(ftdTo, "getSpecialInstructions", mercuryVO, "getSpecialInstructions");
       // PRIORTY IS CALCULATED
       // OPERATOR IS CALCULATED
       testEquals(adjTo, "getComments", mercuryVO, "getComments");
       testEquals(ftdTo, "getOrderDetailId", mercuryVO, "getReferenceNumber");
       testEquals(ftdTo, "getProductId", mercuryVO, "getProductId");
       testEquals(ftdTo, "getZipCode", mercuryVO, "getZipCode");
       testEquals(adjTo, "getCombinedReportNumber", mercuryVO, "getCombinedReportNumber");
       testEquals(adjTo, "getRofNumber", mercuryVO, "getRofNumber");
       testEquals(adjTo, "getAdjustmentReasonCode", mercuryVO, "getAdjReasonCode");
       testEquals(adjTo, "getDirection", mercuryVO, "getDirection");
      } catch (Exception e)
    {
      e.printStackTrace();

      assertTrue(e.getMessage(), false);
    }
  }
  /**
   * void sendANSMessage(ANSMessageTO)
   */
  public void testsendANSMessage()
  {
    ResultTO ftdResult;    
    ResultTO ansResult;
    FTDMessageTO ftdTo = this.buildFTDMessageTO();
    try
    {
      ftdResult = ejbFixture.mercury.sendFTDMessage(ftdTo);
      assertNotNull("FTD Null result", ftdResult);
      assertTrue(ftdResult.getErrorString(), ftdResult.isSuccess());
      String mercuryMessageId = ftdResult.getKey();
      ANSMessageTO ansTo = new ANSMessageTO();
      ansTo.setAskAnswerCode((String)testMap.get("ASK_ANSWER_CODE"));
      ansTo.setComments((String)testMap.get("COMMENTS"));
      ansTo.setDirection(((String)testMap.get("MESSAGE_DIRECTION")));
      ansTo.setFillingFlorist((String)testMap.get("FILLING_FLORIST"));
      //ansTo.setMercuryOrderNumber(mercuryMessageId);
      ansTo.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
      ansTo.setOldPrice((Double)testMap.get("OLD_PRICE"));
      ansTo.setOperator((String)testMap.get("OPERATOR"));
      ansTo.setPrice((Double)testMap.get("PRICE"));
      ansTo.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));
      ansResult = ejbFixture.mercury.sendANSMessage(ansTo);
      assertNotNull("Null result", ansResult);
      assertTrue(ansResult.getErrorString(), ansResult.isSuccess());

      // checking that data was entered correctly //
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryVO mercuryVO = dao.getMessage(ansResult.getKey());

       testEquals(ftdResult, "getKey", mercuryVO, "getMercuryOrderNumber");
       testEquals(ansTo, "getMercuryStatus", mercuryVO, "getMercuryStatus");
       testEquals(ansTo, "getMessageType", mercuryVO, "getMessageType");
       testEquals(ansTo, "getSendingFlorist", mercuryVO, "getSendingFlorist");
       testEquals(ansTo, "getFillingFlorist", mercuryVO, "getFillingFlorist");
       testEquals(ftdTo, "getOrderDate", mercuryVO, "getOrderDate");
       testEquals(ftdTo, "getRecipient", mercuryVO, "getRecipient");
       testEquals(ftdTo, "getAddress", mercuryVO, "getAddress");
       testEquals(ftdTo, "getCityStateZip", mercuryVO, "getCityStateZip");
       testEquals(ftdTo, "getPhoneNumber", mercuryVO, "getPhoneNumber");
       testEquals(ftdTo, "getDeliveryDate", mercuryVO, "getDeliveryDate");
       testEquals(ftdTo, "getDeliveryDateText", mercuryVO, "getDeliveryDateText");
       testEquals(ftdTo, "getFirstChoice", mercuryVO, "getFirstChoice");
       testEquals(ftdTo, "getSecondChoice", mercuryVO, "getSecondChoice");
       testEquals(ansTo, "getOldPrice", mercuryVO, "getOldPrice");
       testEquals(ansTo, "getPrice", mercuryVO, "getPrice");
       testEquals(ftdTo, "getCardMessage", mercuryVO, "getCardMessage");
       testEquals(ftdTo, "getOccasion", mercuryVO, "getOccasion");
       testEquals(ftdTo, "getSpecialInstructions", mercuryVO, "getSpecialInstructions");
       // PRIORTY IS CALCULATED
       // OPERATOR IS CALCULATED
       testEquals(ansTo, "getComments", mercuryVO, "getComments");
       testEquals(ftdTo, "getOrderDetailId", mercuryVO, "getReferenceNumber");
       testEquals(ftdTo, "getProductId", mercuryVO, "getProductId");
       testEquals(ftdTo, "getZipCode", mercuryVO, "getZipCode");
       testEquals(ansTo, "getAskAnswerCode", mercuryVO, "getAskAnswerCode");
       testEquals(ansTo, "getDirection", mercuryVO, "getDirection");
      
      } catch (Exception e)
    {
      e.printStackTrace();

      assertTrue(e.getMessage(), false);
    }
  }
  /**
   * void sendASKMessage(ASKMessageTO)
   */
  public void testsendASKMessage()
  {
    ResultTO ftdResult;    
    ResultTO askResult;
    FTDMessageTO ftdTo = this.buildFTDMessageTO();
    try
    {
      ftdResult = ejbFixture.mercury.sendFTDMessage(ftdTo);
      assertNotNull("FTD Null result", ftdResult);
      assertTrue(ftdResult.getErrorString(), ftdResult.isSuccess());
      String mercuryMessageId = ftdResult.getKey();
      ASKMessageTO askTo = new ASKMessageTO();
      askTo.setAskAnswerCode((String)testMap.get("ASK_ANSWER_CODE"));
      askTo.setComments((String)testMap.get("COMMENTS"));
      askTo.setDirection((String)testMap.get("MESSAGE_DIRECTION"));
      askTo.setFillingFlorist((String) testMap.get("FILLING_FLORIST"));
      //askTo.setMercuryOrderNumber(mercuryMessageId);
      askTo.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
      askTo.setOldPrice((Double)testMap.get("OLD_PRICE"));
      askTo.setOperator((String)testMap.get("OPERATOR"));
      askTo.setPrice((Double)testMap.get("PRICE"));
      askTo.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));
      askResult = ejbFixture.mercury.sendASKMessage(askTo);
      assertNotNull("Null result", askResult);
      assertTrue(askResult.getErrorString(), askResult.isSuccess());
       // checking that data was entered correctly //
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryVO mercuryVO = dao.getMessage(askResult.getKey());
       testEquals(ftdResult, "getKey", mercuryVO, "getMercuryOrderNumber");
       testEquals(askTo, "getMercuryStatus", mercuryVO, "getMercuryStatus");
       testEquals(askTo, "getMessageType", mercuryVO, "getMessageType");
       testEquals(ftdTo, "getSendingFlorist", mercuryVO, "getSendingFlorist");
       testEquals(ftdTo, "getFillingFlorist", mercuryVO, "getFillingFlorist");
       testEquals(ftdTo, "getOrderDate", mercuryVO, "getOrderDate");
       testEquals(ftdTo, "getRecipient", mercuryVO, "getRecipient");
       testEquals(ftdTo, "getAddress", mercuryVO, "getAddress");
       testEquals(ftdTo, "getCityStateZip", mercuryVO, "getCityStateZip");
       testEquals(ftdTo, "getPhoneNumber", mercuryVO, "getPhoneNumber");
       testEquals(ftdTo, "getDeliveryDate", mercuryVO, "getDeliveryDate");
       testEquals(ftdTo, "getDeliveryDateText", mercuryVO, "getDeliveryDateText");
       testEquals(ftdTo, "getFirstChoice", mercuryVO, "getFirstChoice");
       testEquals(ftdTo, "getSecondChoice", mercuryVO, "getSecondChoice");
       testEquals(askTo, "getOldPrice", mercuryVO, "getOldPrice");
       testEquals(askTo, "getPrice", mercuryVO, "getPrice");
       testEquals(ftdTo, "getCardMessage", mercuryVO, "getCardMessage");
       testEquals(ftdTo, "getOccasion", mercuryVO, "getOccasion");
       testEquals(ftdTo, "getSpecialInstructions", mercuryVO, "getSpecialInstructions");
       // PRIORTY IS CALCULATED
       // OPERATOR IS CALCULATED
       testEquals(askTo, "getComments", mercuryVO, "getComments");
       testEquals(ftdTo, "getOrderDetailId", mercuryVO, "getReferenceNumber");
       testEquals(ftdTo, "getProductId", mercuryVO, "getProductId");
       testEquals(ftdTo, "getZipCode", mercuryVO, "getZipCode");
       testEquals(askTo, "getAskAnswerCode", mercuryVO, "getAskAnswerCode");
       testEquals(askTo, "getDirection", mercuryVO, "getDirection");
      
      } catch (Exception e)
    {
      e.printStackTrace();
      assertTrue(e.getMessage(), false);
    }
  }
  /**
   * void sendCANMessage(CANMessageTO)
   */
  public void testsendCANMessage()
  {  
    ResultTO ftdResult;    
    ResultTO canResult;
    FTDMessageTO ftdTo = this.buildFTDMessageTO();
    try
    {
      ftdResult = ejbFixture.mercury.sendFTDMessage(ftdTo);
      assertNotNull("FTD Null result", ftdResult);
      assertTrue(ftdResult.getErrorString(), ftdResult.isSuccess());
      String mercuryMessageId = ftdResult.getKey();
      CANMessageTO canTo= new CANMessageTO();
      canTo.setComments((String)testMap.get("COMMENTS"));
      canTo.setDirection((String)testMap.get("MESSAGE_DIRECTION"));
      canTo.setFillingFlorist((String)testMap.get("FILLING_FLORIST"));
      //canTo.setMercuryOrderNumber(mercuryMessageId);
      canTo.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
      canTo.setOperator((String)testMap.get("OPERATOR"));
      canTo.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));
      canResult = ejbFixture.mercury.sendCANMessage(canTo);
      assertNotNull("Null result", canResult);
      assertTrue(canResult.getErrorString(), canResult.isSuccess());
   // checking that data was entered correctly //
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryVO mercuryVO = dao.getMessage(canResult.getKey());
       testEquals(ftdResult, "getKey", mercuryVO, "getMercuryOrderNumber");
       testEquals(canTo, "getMercuryStatus", mercuryVO, "getMercuryStatus");
       testEquals(canTo, "getMessageType", mercuryVO, "getMessageType");
       testEquals(canTo, "getSendingFlorist", mercuryVO, "getSendingFlorist");
       testEquals(canTo, "getFillingFlorist", mercuryVO, "getFillingFlorist");
       testEquals(ftdTo, "getOrderDate", mercuryVO, "getOrderDate");
       testEquals(ftdTo, "getRecipient", mercuryVO, "getRecipient");
       testEquals(ftdTo, "getAddress", mercuryVO, "getAddress");
       testEquals(ftdTo, "getCityStateZip", mercuryVO, "getCityStateZip");
       testEquals(ftdTo, "getPhoneNumber", mercuryVO, "getPhoneNumber");
       testEquals(ftdTo, "getDeliveryDate", mercuryVO, "getDeliveryDate");
       testEquals(ftdTo, "getDeliveryDateText", mercuryVO, "getDeliveryDateText");
       testEquals(ftdTo, "getFirstChoice", mercuryVO, "getFirstChoice");
       testEquals(ftdTo, "getSecondChoice", mercuryVO, "getSecondChoice");
       testEquals(ftdTo, "getPrice", mercuryVO, "getPrice");
       testEquals(ftdTo, "getCardMessage", mercuryVO, "getCardMessage");
       testEquals(ftdTo, "getOccasion", mercuryVO, "getOccasion");
       testEquals(ftdTo, "getSpecialInstructions", mercuryVO, "getSpecialInstructions");
       // PRIORTY IS CALCULATED
       // OPERATOR IS CALCULATED
       testEquals(canTo, "getComments", mercuryVO, "getComments");
       testEquals(ftdTo, "getOrderDetailId", mercuryVO, "getReferenceNumber");
       testEquals(ftdTo, "getProductId", mercuryVO, "getProductId");
       testEquals(ftdTo, "getZipCode", mercuryVO, "getZipCode");
       testEquals(canTo, "getDirection", mercuryVO, "getDirection");
     
 
    } catch (Exception e)
    {
      e.printStackTrace();
      assertTrue(e.getMessage(), false);
    }
  }

  /**
   * void sendGENMessage(GENMessageTO)
   */
  public void testsendGENMessage()
  {
    GENMessageTO genTo = new GENMessageTO();
    genTo.setComments((String)testMap.get("COMMENTS"));
    genTo.setDeliveryDate((Date)testMap.get("DELIVERY_DATE"));
    genTo.setDirection((String)testMap.get("MESSAGE_DIRECTION"));
    genTo.setFillingFlorist((String)testMap.get("FILLING_FLORIST"));
    genTo.setMercuryStatus((String)testMap.get("MERCURY_STATUS"));
    genTo.setOperator((String)testMap.get("OPERATOR"));
    genTo.setPriority((String)testMap.get("PRIORITY"));
    genTo.setRetrievalFlag((String)testMap.get("RETRIEVAL_FLAG"));
    genTo.setSendingFlorist((String)testMap.get("SENDING_FLORIST"));
    
    ResultTO result = null;;
    try { 
      result = ejbFixture.mercury.sendGENMessage(genTo);
         // checking that data was entered correctly //
       MercuryDAO dao = new MercuryDAO(conn);
       MercuryVO mercuryVO = dao.getMessage(result.getKey());
       testEquals(genTo, "getMercuryStatus", mercuryVO, "getMercuryStatus");
       testEquals(genTo, "getMessageType", mercuryVO, "getMessageType");
       testEquals(genTo, "getSendingFlorist", mercuryVO, "getSendingFlorist");
       testEquals(genTo, "getFillingFlorist", mercuryVO, "getFillingFlorist");
       testEquals(genTo, "getDeliveryDate", mercuryVO, "getDeliveryDate");
       // PRIORTY IS CALCULATED
       // OPERATOR IS CALCULATED
       testEquals(genTo, "getComments", mercuryVO, "getComments");
       testEquals(genTo, "getDirection", mercuryVO, "getDirection");

    } catch (Exception e)
    {
      e.printStackTrace();
      assertTrue(e.getMessage(), false);
    }
    assertNotNull("Null result", result);
    assertTrue(result.getErrorString(), result.isSuccess());
  }
  
    public void testEquals (Object obj1, String methodName1, Object obj2, String methodName2) throws Exception 
  {
    Method method1 = obj1.getClass().getMethod(methodName1, null);
    Method method2 = obj2.getClass().getMethod(methodName2, null);

    if ((method1.invoke(obj1, null) == null) || (method2.invoke(obj2, null) == null))
    {
         assertTrue(methodName1+" equals " +methodName2, ((method1.invoke(obj1, null) == null) && (method2.invoke(obj2, null) == null)));
    }
    else
    {
        try{
          if (method1.getReturnType().equals(method1.getReturnType()))
          {
            if (method1.getReturnType().isPrimitive())
            {
              assertTrue( methodName1+" == "+methodName2, (method1.invoke(obj1, null) == method2.invoke(obj2, null)));
            }
            else if (method1.getReturnType().getName().equalsIgnoreCase("java.util.Date"))
            {
              SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
              String date1 = formatter.format((Date)method1.invoke(obj1, null));
              String date2 = formatter.format((Date)method2.invoke(obj2, null));
              assertTrue( methodName1+" equals "+methodName2, date1.equals(date2));
            }
            else
            {
              assertTrue( methodName1+" equals "+methodName2, (method1.invoke(obj1, null).equals(method2.invoke(obj2, null))));
            }
          }
          else
          {
            assertTrue(methodName1+" and "+methodName2 +" did not have the same return types.", false);
          }
        }
       catch ( Exception e) {
            e.printStackTrace ();
        }
    }
  }
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(MercuryAPIBeanTester.class);        
      //suite.addTest(new MercuryAPIBeanTester("testsendADJMessage"));
    } 
    catch (Exception e) 
    {
      System.out.println(e.toString());
      e.printStackTrace();
    }


    return suite;
  } 
  
}
