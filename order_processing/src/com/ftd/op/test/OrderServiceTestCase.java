package com.ftd.op.test;

import java.util.Date;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import java.sql.Connection;
import com.ftd.op.test.JdbcTestFixture;
import com.ftd.op.test.RetrieveOrderDetailTestFixture;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.op.order.vo.RWDFloristVOList;

public class OrderServiceTestCase extends TestCase 
{
  JdbcTestFixture fixture = new JdbcTestFixture();
  Connection conn;

  public OrderServiceTestCase(String sTestName)
  {
    super(sTestName);
  }

  public void setUp() throws Exception
  {
    fixture.setUp();
    this.conn = fixture.getConnection();
  }

  public void tearDown() throws Exception
  {
    fixture.tearDown();
  }
  
  /**
   * void processOrder(OrderDetailVO)
   * Test by sending an invalid orderDetailId
   * Expecting the Invalid order detail encountered Exception to be caught and logged
   */
  /*public void testProcessOrder1() throws Exception
  { 
    try
    {  
      OrderDetailVO orderDtl = new OrderDetailVO();
      orderDtl.setOrderDetailId(0); //invalid orderDetailId
      orderDtl.setOrderGuid("N998004"); 
      orderDtl.setDeliveryDate(new Date());
      orderDtl.setDeliveryDateEnd(new Date());
      orderDtl.setSourceCode("7536");
      orderDtl.setShipDate(new Date());
      orderDtl.setRecipientId(102581);
      orderDtl.setProductId("3121");
      orderDtl.setSameDayGift("N");
      orderDtl.setFloristId("111");
        
      OrderService service = new OrderService(this.conn);
      service.processOrder(orderDtl);
    }
    catch (Exception e)
    {
      //do nothing
    }  
  }*/
  
  /**
   * void processOrder(OrderDetailVO)
   * Test by sending an invalid orderGuid
   * Expecting the Invalid order encountered Exception to be caught and logged
   */
  public void testProcessOrder2() throws Exception
  { 
    try
    {  
      OrderDetailVO orderDtl = new OrderDetailVO();
      orderDtl.setOrderDetailId(998001);
      orderDtl.setOrderGuid(null); //invalid orderGuid
      orderDtl.setDeliveryDate(new Date());
      orderDtl.setSourceCode("7536");
      orderDtl.setShipDate(new Date());
      orderDtl.setRecipientId(101347);
      orderDtl.setProductId("3121");
      orderDtl.setSameDayGift("N");
      orderDtl.setFloristId("111");
        
      OrderService service = new OrderService(this.conn);
      service.processOrder(orderDtl);
    }
    catch (Exception e)
    {
      //do nothing
    }  
  }
    
  /**
   * void insertMercuryRecord(OrderDetailVO)
   */
  //public void testInsertMercuryRecord() throws Exception
  //{
  //}

  /**
   * void insertVenusRecord(OrderDetailVO)
   */
  //public void testInsertVenusRecord() throws Exception
  //{
  //}

  /**
   * void insertFTPRecord(OrderDetailVO)
   */
  //public void testInsertFTPRecord() throws Exception
  //{
  //}

  /**
   * void insertEmailRecord(OrderDetailVO)
   */
  //public void testInsertEmailRecord() throws Exception
  //{
  //}
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(OrderServiceTestCase.class);        
    } 
    catch (Exception e) 
    {
      //System.out.println(e.toString());
      //e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}