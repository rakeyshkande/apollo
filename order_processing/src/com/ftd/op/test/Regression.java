package com.ftd.op.test;
import junit.framework.Test;
import junit.framework.TestSuite;

public class Regression 
{
  public static Test suite()
  {
    TestSuite suite;
    suite = new TestSuite("Regression");

    suite.addTest(com.ftd.op.test.OrderAPIBeanTester.suite());
    suite.addTest(com.ftd.op.test.MercuryAPIBeanTester.suite());
    return suite;
  }

  public static void main(String args[])
  {
    String args2[] = {"-noloading", "com.ftd.op.test.Regression"};

    junit.textui.TestRunner.main(args2);
  }
}