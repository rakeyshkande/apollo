package com.ftd.op.test;
import com.ftd.op.common.framework.util.ServiceLocator;
import com.ftd.op.mercury.api.MercuryAPI;
import com.ftd.op.order.api.OrderAPI;

public class EJBTestFixture 
{
  private ServiceLocator locator;
  public OrderAPI order;
  public MercuryAPI mercury;
  
  public EJBTestFixture()
  {
    try {
      locator = ServiceLocator.getInstance();
    } catch (Exception e) 
    {
      System.out.println("Failed to create service locator");
    }
  }

  public void setUp()
  {
    try {
      order = (OrderAPI)locator.getRemoteBean("ejb/OrderAPI", "com.ftd.op.order.api.OrderAPIHome");
      mercury = (MercuryAPI)locator.getRemoteBean("ejb/MercuryAPI", "com.ftd.op.mercury.api.MercuryAPIHome");
    } catch (Exception e) 
    {
      System.out.println("Failed to create remote reference to mercury and order api beans");
    }
  }

  public void tearDown()
  {
  }
}