package com.ftd.op.test;

import java.sql.DriverManager;
import java.sql.Connection;

public class JdbcTestFixture 
{
  String _connectionURL = "jdbc:oracle:thin:@adonis.ftdi.com:1521:fozzie";
  Connection _connection;
  String _password = "osp";
  String _userName = "osp";

  public JdbcTestFixture()
  {
  }

  public void setUp() throws Exception
  {
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
    _connection = DriverManager.getConnection(getConnectionURL(), getUserName(), getPassword());
  }

  public void tearDown() throws Exception
  {
    _connection.close();
  }

  public Connection getConnection()
  {
    return _connection;
  }

  String getUserName()
  {
    return _userName;
  }

  String getPassword()
  {
    return _password;
  }

  public String getConnectionURL()
  {
    return _connectionURL;
  }
}