package com.ftd.op.mercury.api;
import javax.ejb.EJBLocalHome;
import javax.ejb.CreateException;

public interface MercuryAPILocalHome extends EJBLocalHome 
{
  MercuryAPILocal create() throws CreateException;
}