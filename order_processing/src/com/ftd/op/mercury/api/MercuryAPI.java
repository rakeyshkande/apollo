package com.ftd.op.mercury.api;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.to.OrderDetailKeyTO;
import javax.ejb.EJBObject;
import java.rmi.RemoteException;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.CalculatedFieldsTO;

public interface MercuryAPI extends EJBObject 
{
  ResultTO sendFTDMessage(OrderDetailKeyTO order) throws RemoteException;

  ResultTO sendFTDMessage(OrderDetailKeyTO order, String floristId) throws RemoteException;

  ResultTO sendFTDMessage(FTDMessageTO message) throws RemoteException;

  ResultTO sendADJMessage(ADJMessageTO message) throws RemoteException;

  ResultTO sendANSMessage(ANSMessageTO message) throws RemoteException;

  ResultTO sendASKMessage(ASKMessageTO message) throws RemoteException;

  ResultTO sendCANMessage(CANMessageTO message) throws RemoteException;

  ResultTO sendGENMessage(GENMessageTO message) throws RemoteException;

  CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order) throws RemoteException;
}