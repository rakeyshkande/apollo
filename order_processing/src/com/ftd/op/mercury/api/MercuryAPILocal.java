package com.ftd.op.mercury.api;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.to.OrderDetailKeyTO;
import javax.ejb.EJBLocalObject;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.to.CalculatedFieldsTO;

public interface MercuryAPILocal extends EJBLocalObject 
{
  ResultTO sendFTDMessage(OrderDetailKeyTO order);

  ResultTO sendFTDMessage(OrderDetailKeyTO order, String floristId);

  ResultTO sendFTDMessage(FTDMessageTO message);

  ResultTO sendADJMessage(ADJMessageTO message);

  ResultTO sendANSMessage(ANSMessageTO message);

  ResultTO sendASKMessage(ASKMessageTO message);

  ResultTO sendCANMessage(CANMessageTO message);

  ResultTO sendGENMessage(GENMessageTO message);

  CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order);
}