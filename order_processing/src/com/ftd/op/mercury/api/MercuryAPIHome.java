package com.ftd.op.mercury.api;
import javax.ejb.EJBHome;
import java.rmi.RemoteException;
import javax.ejb.CreateException;

public interface MercuryAPIHome extends EJBHome 
{
  MercuryAPI create() throws RemoteException, CreateException;
}