package com.ftd.op.mercury.api;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.mercury.facade.MercuryFacade;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.to.OrderDetailKeyTO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.plugins.Logger;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.op.mercury.to.CalculatedFieldsTO;

public class MercuryAPIBean implements SessionBean 
{
  private DataSource db;
  private Logger logger;
  private MercuryAPIBO mercuryAPIBO;   

  public void ejbCreate()
  {
  }

  public void ejbActivate()
  {
  }

  public void ejbPassivate()
  {
  }

  public void ejbRemove()
  {
  }

  public void setSessionContext(SessionContext ctx)
  {
    logger = new Logger("com.ftd.op.mercury.api.MercuryAPIBean");
    try
    {
      db = CommonUtils.getDataSource();
      mercuryAPIBO = new MercuryAPIBO();
    }
    catch(Exception e)
    {
      logger.debug(e.toString());
      
    }    
  }
  

  public ResultTO sendFTDMessage(OrderDetailKeyTO order)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return mercuryAPIBO.sendFTDMessage(order, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }


  public ResultTO sendFTDMessage(OrderDetailKeyTO order, String floristId)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;    
    try 
    {
      conn = db.getConnection();
      return mercuryAPIBO.sendFTDMessage(order, floristId, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }


  public static void main(String[] args)
  {
    System.out.println( new String("90-8500AA").matches("[0-9][0-9]-[0-9]+"));
    
  }
  public ResultTO sendFTDMessage(FTDMessageTO message)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;    
    try 
    {
      conn = db.getConnection();      
      return mercuryAPIBO.sendFTDMessage(message, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  }

  public ResultTO sendADJMessage(ADJMessageTO message)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
      try
      {
        conn = db.getConnection();
        return mercuryAPIBO.sendADJMessage(message, conn);
      }
      catch (Exception e)
      {
        logger.error(e);        
        result.setErrorString(e.getMessage());
        result.setSuccess(false);
      } finally 
      {
        try 
        {
          if (conn != null) conn.close();
        } catch (Exception e) 
        {
          //EMPTY
        }
      }
      return result;
  }

  public ResultTO sendANSMessage(ANSMessageTO message)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
    try {
      conn = db.getConnection();
      return mercuryAPIBO.sendANSMessage(message, conn);
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try 
      {
        if (conn != null) conn.close();
      } catch (Exception e) 
      {
        //EMPTY
      }
    }
    return result;
  }

  public ResultTO sendASKMessage(ASKMessageTO message)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;    
    try {
      conn = db.getConnection();
      return mercuryAPIBO.sendASKMessage(message, conn);
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try 
      {
        if (conn != null) conn.close();
      } catch (Exception e) 
      {
        //EMPTY
      }
    }
    return result;
  }

  public ResultTO sendCANMessage(CANMessageTO message) 
  {
    ResultTO result = new ResultTO();
    Connection conn = null; 
    try {
      conn = db.getConnection();
      return mercuryAPIBO.sendCANMessage(message, conn); 
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try 
      {
        if (conn != null) conn.close();
      } catch (Exception e) 
      {
        //EMPTY
      }
    }
    return result;
  }

  public ResultTO sendGENMessage(GENMessageTO message)
  {
    ResultTO result = new ResultTO();
    Connection conn = null;
    try {
      conn = db.getConnection();
      return  mercuryAPIBO.sendGENMessage(message, conn);
    } catch (Exception e) {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } finally 
    {
      try {
        if (conn != null) conn.close();
      } catch (Exception e) {
        //EMPTY
      }
    }
    return result;
  } 

  public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order)
  {

    CalculatedFieldsTO fields = null;
    Connection conn = null;
    try 
    {
      conn = db.getConnection();
      return mercuryAPIBO.getCalculatedFields(order, conn);
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
    } finally 
    {
      try 
      {
        conn.close();
      } catch (Exception ce) 
      {
        // empty
      }
    }
    return fields;
  }
}