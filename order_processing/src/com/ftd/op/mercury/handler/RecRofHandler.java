package com.ftd.op.mercury.handler;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;

public class RecRofHandler extends EventHandler 
{
  private Connection conn;
  private MercuryDAO mercuryDao;
  private OrderDAO orderDao;
  
  // trailer record constants
  private static final int TRAILER_BUFFER_LENGTH = 68;
  private static final int TRAILER_BILLING = 0;
  private static final int TRAILER_START_DATE = 9;
  private static final int TRAILER_END_DATE = 19;
  private static final int TRAILER_TOTAL_LABEL = 29;
  private static final int TRAILER_COUNT = 59;
  
  // detail record constants
  private static final int DETAIL_BUFFER_LENGTH = 150;
  private static final int DETAIL_SENDING_MEMBER_NUM = 2; 
  private static final int DETAIL_DELIVERY_DATE = 8;
  private static final int DETAIL_RECIPIENT = 12;
  private static final int DETAIL_DOLLAR_AMT = 21;
  private static final int DETAIL_FILLING_MEMBER_NUM = 35;
  private static final int DETAIL_OPERATOR = 41;
  private static final int DETAIL_OCCASION = 80;
  private static final int DETAIL_ORDER_NUMBER = 81;  

  //private static final String CONFIG_FILE = "order-processing-config.xml";
  private static final String RECROF_CONFIG_FILE = "recrof-config.xml";

  // Secure Configuration Context
  private static final String SECURE_CONFIG_CONTEXT = "order_processing";
  
  private Logger logger;
  private static final String LOG_CONTEXT = "com.ftd.op.mercury.handler.RecRofHandler";
  
  public RecRofHandler(Connection conn)
  {
    mercuryDao = new MercuryDAO(conn);
    orderDao = new OrderDAO(conn);
    logger = new Logger(LOG_CONTEXT);
  }
  private void generateFile(Date inDate) throws Exception
  { 

    ConfigurationUtil config = ConfigurationUtil.getInstance();            

    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
    String localDir = config.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_DIRECTORY");                              
    String localFileName = config.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_FILENAME");                            


    File outFile = new File(localDir + File.separator + localFileName);
    outFile.createNewFile();
    OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(outFile));
    int count = 0;
    LinkedList mercuryList = mercuryDao.getManualReconcileList(inDate);    
    if(mercuryList != null) {
      Iterator iter = mercuryList.iterator();
      while(iter.hasNext()) 
      {
        String mercId = (String)iter.next();
        output.write(this.buildDetailLine(mercId));
        output.write(System.getProperty("line.separator"));
        count++;

      }
    }
    
    Calendar cal = Calendar.getInstance();    
    output.write(this.buildTrailerLine(cal.getTime(), inDate, count));
    output.write(System.getProperty("line.separator"));
    output.flush();
    
    // only mark the records as reconciled once the file is created successfully
    if(mercuryList != null) {
      Iterator iter2 = mercuryList.iterator();
      while(iter2.hasNext()) 
      {
        String mercId = (String)iter2.next();
        mercuryDao.markReconciled(mercId);
      }
    }
  }
  public void invoke(Object payload) throws Throwable {
      //obtain a database connection
      conn = CommonUtils.getConnection();
      
      try
      {
        RecRofHandler handler = new RecRofHandler(conn);
        handler.generateFile(new Date());
        handler.sendRecRofFile();
      }
      catch(Exception e) {
    	  logger.error(e);
    	  CommonUtils.sendSystemMessage("RecRofHandler", e.getMessage());
      }
      finally
      {
        //close the connection
        try
        {
        	conn.close();
        }
        catch(Exception e)
        { 
        	logger.error(e);
        }
      }
  }

  private String calcOccasion(OrderDetailVO orderDetailVO, CustomerVO customerVO) 
  {
      MercuryDAO dao = new MercuryDAO(conn);
      String occasionString = "";
      if ((orderDetailVO.getOccasion()== null) || orderDetailVO.getOccasion().equals("8") || (new Integer(orderDetailVO.getOccasion()).intValue() <= 0))
      {
        if ((customerVO.getAddressType() != null) && 
            (customerVO.getAddressType().trim().equalsIgnoreCase("FUNERAL HOME") || customerVO.getAddressType().trim().equalsIgnoreCase("CEMETERY")))
        {
          occasionString = new String("1"); // FUNERAL/SYMPATHY
        }
        else 
        {
          occasionString = new String("8"); // OTHER
        }
      }
      else
      {
        int occasionInt = new Integer(orderDetailVO.getOccasion()).intValue();
        if (occasionInt > 13)
        {
          occasionString = new String("5"); // HOLIDAY
        }
        else if ((occasionInt <= 13)&&(occasionInt >= 8))
        {
          occasionString = new String("8"); // OTHER
        }
        else if (occasionInt < 8)
        {
          occasionString = new Integer(occasionInt).toString(); // HOLIDAY
        }
      } 
      return occasionString;
  }
  private String buildTrailerLine(Date startDate, Date endDate, int numRecords) 
  {
    StringBuffer buffer = new StringBuffer();
    buffer.setLength(TRAILER_BUFFER_LENGTH);
    for(int i = 0; i < TRAILER_BUFFER_LENGTH; i++) 
    {
      buffer.setCharAt(i, ' ');
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    DecimalFormat decimalFormat = new DecimalFormat();
    decimalFormat.setGroupingUsed(false);
    decimalFormat.setMinimumIntegerDigits(10);
    
    buffer.insert(TRAILER_BILLING, "Billing");
    buffer.insert(TRAILER_START_DATE, dateFormat.format(startDate));
    buffer.insert(TRAILER_END_DATE, dateFormat.format(endDate));
    buffer.insert(TRAILER_TOTAL_LABEL, "Total Records");
    buffer.insert(TRAILER_COUNT, decimalFormat.format(numRecords));
    return buffer.toString();
  }
  private String buildDetailLine(String mercuryId) throws Exception
  {
    MercuryVO mercury = mercuryDao.getMessage(mercuryId);
    
    OrderDetailVO detail = orderDao.getOrderDetail(mercury.getReferenceNumber());
    CustomerVO recipient = orderDao.getCustomer(detail.getRecipientId());
    
    
    StringBuffer buffer = new StringBuffer();
    
    // init string to all blanks
    buffer.setLength(DETAIL_BUFFER_LENGTH);
    for(int i = 0; i < DETAIL_BUFFER_LENGTH; i++) {
      buffer.setCharAt(i, ' ');
    }
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMdd");
    
    //System.out.println("Sending florist: " + mercury.getSendingFlorist());
    buffer.insert(DETAIL_SENDING_MEMBER_NUM, mercury.getSendingFlorist().substring(0, 2) + mercury.getSendingFlorist().substring(3,7));
    buffer.insert(DETAIL_DELIVERY_DATE, dateFormat.format(mercury.getDeliveryDate()));
    buffer.insert(DETAIL_RECIPIENT, new String((recipient.getLastName() + "   ")).substring(0,3).toUpperCase());
    buffer.insert(DETAIL_DOLLAR_AMT, transformPrice(mercury.getPrice().toString()));
    //System.out.println("Filling florist: " + mercury.getFillingFlorist());
    buffer.insert(DETAIL_FILLING_MEMBER_NUM, mercury.getFillingFlorist().substring(0,2) + mercury.getFillingFlorist().substring(3,7));
    buffer.insert(DETAIL_OPERATOR, "##");
    buffer.insert(DETAIL_OCCASION, calcOccasion(detail, recipient));
    buffer.insert(DETAIL_ORDER_NUMBER, mercury.getMercuryMessageNumber());
    return buffer.toString();
    
  }
  
  public void sendRecRofFile() throws Exception
  {
    //get ftp config data
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String remoteLocation = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_REMOTE_LOCATION");                      
    String remoteFileName = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_REMOTE_FILENAME");                      
    String remoteDir = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_REMOTE_DIRECTORY");                      
    String localDir = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_DIRECTORY");                              
    String localFileName = configUtil.getProperty(RECROF_CONFIG_FILE,"RECROF_LOCAL_FILENAME");                            

    // Obtain Secure Configuration properties
    String ftpLogon = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"RECROF_FTP_LOGON");                      
    String ftpPassword = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"RECROF_FTP_PASSWORD");                              
                    
    //get list of files in directory       
    FTPClient ftpClient = new FTPClient(remoteLocation);
    ftpClient.login(ftpLogon,ftpPassword);
        
    //get the file from the remote location
    
     String localFile = localDir + File.separator + localFileName;
     String remoteFile = remoteDir + "/" + remoteFileName;

     System.out.println(remoteFile);
     System.out.println(localFile);
     ftpClient.setType(FTPTransferType.ASCII);
     try {
       // make the remote directory if it doesn't already exist
       ftpClient.mkdir(remoteDir);
     } catch(FTPException e) 
     {
       // do nothing
     }
     ftpClient.put(localFile, remoteFile); 
  }
  private String transformPrice(String inPrice) 
  {
    BigDecimal decPrice = new BigDecimal(inPrice);
    
    String out = decPrice.movePointRight(2).toString();
    while(out.length() < 6)
    {
      out = "0" + out;
    }
    return out;
  }
}