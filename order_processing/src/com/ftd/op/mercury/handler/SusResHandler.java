package com.ftd.op.mercury.handler;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.mercury.dao.SusResDAO;
import com.ftd.op.mercury.vo.GotoShutdownVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;

public class SusResHandler extends EventHandler 
{
  private static final String SUSRES_CONFIG_FILE = "susres-config.xml";
  private Logger logger;  
  private Connection conn;

  // Secure Configuration Context
  private static final String SECURE_CONFIG_CONTEXT = "order_processing";

  public SusResHandler()
  {
    logger = new Logger("com.ftd.op.mercury.handler.SusResHandler");
  }
  public SusResHandler(Connection conn) 
  {
    logger = new Logger("com.ftd.op.mercury.handler.SusResHandler");
    this.conn = conn;    
  }
  public void invoke(Object payload) throws Throwable {
      //obtain a database connection
      conn = CommonUtils.getConnection();
            
  }
  public void loadSusResFile() throws Exception
  {
    //get ftp config data
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
    String remoteLocation = configUtil.getProperty(SUSRES_CONFIG_FILE,"SUSRES_REMOTE_LOCATION");                      
    String remoteFileName = configUtil.getProperty(SUSRES_CONFIG_FILE,"SUSRES_REMOTE_FILENAME");                      

    // Obtain Secure Configuration properties
    String ftpLogon = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"SUSRES_FTP_LOGON");                      
    String ftpPassword = configUtil.getSecureProperty(SECURE_CONFIG_CONTEXT,"SUSRES_FTP_PASSWORD");                              

    String remoteDir = configUtil.getProperty(SUSRES_CONFIG_FILE,"SUSRES_REMOTE_DIRECTORY");                      
    String localDir = configUtil.getProperty(SUSRES_CONFIG_FILE,"SUSRES_LOCAL_DIRECTORY");                              
    String localFileName = configUtil.getProperty(SUSRES_CONFIG_FILE,"SUSRES_LOCAL_FILENAME");                            
                    
                    
    //get list of files in directory       
    FTPClient ftpClient = new FTPClient(remoteLocation);
    ftpClient.login(ftpLogon,ftpPassword);
    
    String[] files = null;
    try{
        //When files do not exist in the specified dir this object throws
        //an exception.  We do not consider this an error because this
        //dir will not always contain files.
        files = ftpClient.dir(remoteDir + remoteFileName); 
    }
    catch(FTPException ftpe)
    {
        logger.debug("Files do not exist in directory.");
        logger.debug(ftpe);
    }
    
    //if nothing was found exit
    if(files == null || files.length == 0)
    {
        return;
    }
    //get current date and time to be used in file name
    SimpleDateFormat sdf = new SimpleDateFormat("MMddyy_HHMMSS");
    String dateString = sdf.format(new java.util.Date());
    
    //process each file
    for(int i = 0;i < files.length;i++)
    {
        //get the file from the remote location
        String localFile = localDir + localFileName + "_" + dateString + "_" + i+1 + ".csv";
        ftpClient.get(localFile,files[i]); 
        
        //remove the file from the remote directory
        ftpClient.delete(files[i]);
        
        //process file
        processSusresFile(localFile);
    }
  }
  public void processSusresFile(String file)
  {
    try {
      //get an input stream for the file
      BufferedReader br = new BufferedReader(new FileReader(file));
      String in;
      while((in = br.readLine()) != null)
      {
        String [] fields = in.split(" ");
        System.out.println(in);
        GotoShutdownVO newVO = new GotoShutdownVO();
        newVO.setFloristId(fields[0]);
        for(int i = 0; i < fields.length; i++)
        {
          // implementation assumes that "null" means space in the file
          // it could mean ASCII(0) in which case, we should change 
          // the rexexp to include both space and Ascii(0)
          
          if(fields[i].equalsIgnoreCase("P")) newVO.setPStatus("P");
          if(fields[i].equalsIgnoreCase("F")) newVO.setFStatus("F");
          if(fields[i].equalsIgnoreCase("Q")) newVO.setQStatus("Q");
          if(fields[i].equalsIgnoreCase("X")) newVO.setXStatus("X");
          if(fields[i].equalsIgnoreCase("M") ||
             fields[i].equalsIgnoreCase("B") ||
             fields[i].equalsIgnoreCase("S") ||
             fields[i].equalsIgnoreCase("C")) newVO.setMercInfo(fields[i]);
        }
        SusResDAO dao = new SusResDAO(conn);
        GotoShutdownVO oldVO = dao.getGotoShutdown(newVO.getFloristId());  
        if(oldVO != null) 
        {
          // if the new VO has all status fields == null then delete from table
          if(newVO.isClear()) 
          {
            dao.deleteGotoShutdown(oldVO);
          } else {
            dao.updateGotoShutdown(newVO);
          }
        } else 
        {
          // if the VO has all status fields == null then do nothing
          // otherwise, insert record
          if(!newVO.isClear()) dao.insertGotoShutdown(newVO);
        }
      }
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
  }
  /**
   * 
   * @param args
   */
  public static void main(String[] args)
  {      
    try {
      String driver_ = "oracle.jdbc.driver.OracleDriver";
      String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1522:dev5";
      String user_ = "osp";
      String password_ = "osp";
      
      Class.forName(driver_);
      Connection connection = DriverManager.getConnection(database_, user_, password_);
      SusResHandler susResHandler = new SusResHandler(connection);
      susResHandler.processSusresFile("c:/logs/testfile/gotorpt.txt");
      
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
    //susResHandler.invoke(new String("susres"));
  }
}