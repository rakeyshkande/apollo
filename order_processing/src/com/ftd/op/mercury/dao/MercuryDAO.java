package com.ftd.op.mercury.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.ftd.op.mercury.vo.AutoResponseKeyVO;
import com.ftd.op.mercury.vo.MercuryOpsVO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.orderlifecycle.OrderStatus;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * 
 * @author Jason Weiss / Laura Felch
 */
 
public class MercuryDAO 
{

  private static final String YES = "Y";
  private static final String NO = "N";
  
  // special constant for bucket query (returns one of two cursors with non constant name)
  
  private static final int SEQUENCE_NUMBER = 2;
  
  private Connection conn;
  private String status;
  private String message;
  private Logger logger;  

  /**
     * Constructor
     * 
     * Constructor with the database connection passed in as a parameter
     * 
     * @param conn Connection
     */
  public MercuryDAO(Connection conn)
  {
      this.conn = conn;
      logger = new Logger("com.ftd.op.mercury.dao.MercuryDAO");
  }

  /**
   * Looks up a Mercury OPS record by suffix
   * 
   * @param suffix
   * @return Mercury Ops record
   */
  public MercuryOpsVO getMercuryOps(String suffix) throws Exception{
    MercuryOpsVO opsVO = new MercuryOpsVO();
    boolean isEmpty = true;
    DataRequest dataRequest = new DataRequest();
    CachedResultSet output = null;
    if(logger.isDebugEnabled())
    {        
      logger.debug("getInboundMercuryMessageList :: MercuryOpsVO ");
    }
       
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SUFFIX", suffix);
     
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_MERCURY_OPS");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (output.next())
    {
      isEmpty = false;
      opsVO.setAvailable(output.getString("AVAILABLE_FLAG").equals(YES));
      opsVO.setInboundAdminSequence(output.getInt("INBOUND_ADMIN_SEQUENCE"));
      opsVO.setInboundOrderSequence(output.getInt("INBOUND_ORDER_SEQUENCE"));
      opsVO.setInUse(output.getString("IN_USE_FLAG").equals(YES));
      opsVO.setLastActivity(output.getDate("LAST_MESSAGE_ACTIVITY"));
      opsVO.setLastTransmission(output.getDate("LAST_TRANSMISSION"));
      opsVO.setMaxBatchSize(output.getInt("BATCH_SIZE"));
      opsVO.setOutboundAdminSequence(output.getInt("OUTBOUND_ADMIN_SEQUENCE"));
      opsVO.setOutboundOrderSequence(output.getInt("OUTBOUND_ORDER_SEQUENCE"));
      opsVO.setSendNewOrders(output.getString("SEND_NEW_ORDERS").equals(YES));
      opsVO.setSleepTime(output.getInt("SLEEP_TIME"));
      opsVO.setSequenceLock(output.getString("SEQUENCE_CHECK_LOCK").equals(YES));
      opsVO.setSuffix(output.getString("SUFFIX"));
    }
  
    if (isEmpty)
      return null;
    else
      return opsVO;
  }
  
  /**
   * Updates an ops record to match an ops VO
   * 
   * Used to update sequence numbers, sequence lock, and in_use_flag
   * 
   * @param ops record to update
   */
  public void updateMercuryOps(MercuryOpsVO ops) throws Exception{
    DataRequest dataRequest = new DataRequest();    
   if(logger.isDebugEnabled())
    {        
      logger.debug("getInboundMercuryMessageList :: void ");
    }
     
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SUFFIX", ops.getSuffix());
    inputParams.put("IN_IN_USE_FLAG", ops.isInUse() ? YES : NO);

    // convert java.util.Date to java.sql.Date
    inputParams.put("IN_LAST_TRANSMISSION", 
      ops.getLastTransmission() == null ? 
        null : 
        new java.sql.Timestamp(ops.getLastTransmission().getTime()));
    
    // convert java.util.Date to java.sql.Date
    inputParams.put("IN_LAST_MESSAGE_ACTIVITY", 
      ops.getLastActivity() == null ? 
        null:
        new java.sql.Timestamp(ops.getLastActivity().getTime()));

    inputParams.put("IN_AVAILABLE_FLAG", ops.isAvailable() ? YES : NO);
    inputParams.put("IN_SEND_NEW_ORDERS", ops.isSendNewOrders() ? YES : NO);
    inputParams.put("IN_SLEEP_TIME", new Integer(ops.getSleepTime()));
    inputParams.put("IN_BATCH_SIZE", new Integer(ops.getMaxBatchSize()));
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_UPDATE_MERCURY_OPS");
    dataRequest.setInputParams(inputParams);
    
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    HashMap outputs = (HashMap)dataAccessUtil.execute(dataRequest);
    if (!outputs.get("OUT_STATUS").equals(YES))
    {
        throw new Exception((String)outputs.get("OUT_MESSAGE"));
    }
  }
  public void updateMercuryOpsSequence(MercuryOpsVO ops) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    logger.debug("updateMercuryOpsSequence :: void ");
     
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SUFFIX", ops.getSuffix());
    inputParams.put("IN_INBOUND_ADMIN_SEQUENCE", new Integer(ops.getInboundAdminSequence()));
    inputParams.put("IN_OUTBOUND_ADMIN_SEQUENCE", new Integer(ops.getOutboundAdminSequence()));
    inputParams.put("IN_INBOUND_ORDER_SEQUENCE", new Integer(ops.getInboundOrderSequence()));
    inputParams.put("IN_OUTBOUND_ORDER_SEQUENCE", new Integer(ops.getOutboundOrderSequence()));
    inputParams.put("IN_SEQUENCE_CHECK_LOCK", ops.isSequenceLock() ? YES : NO);
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_UPDATE_MERCURY_OPS_SEQUENCES");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
    HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
    if (!outputs.get("OUT_STATUS").equals(YES))
    {
        throw new Exception((String)outputs.get("OUT_MESSAGE"));
    }
  }
  /**
   * This method returns the FTD associated with a particular mercury
   * message
   * 
   * @param message
   * @return a MercuryVO populated with the associated FTD message
   */
  public MercuryVO getAssociatedFtd(MercuryVO message) throws Exception{
    MercuryVO vo = new MercuryVO();
    boolean isEmpty = true;
    DataRequest dataRequest = new DataRequest();
    CachedResultSet output = null;
    if(logger.isDebugEnabled())
    {        
      logger.debug("getAssociatedFtd :: MercuryVO ");
    }
       
      /* setup stored procedure input parameters */
    
    HashMap inputParams = new HashMap();
    inputParams.put("IN_MERCURY_MESSAGE_NUMBER", message.getMercuryOrderNumber());
      
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_MERCURY_BY_MESSAGE_NUMBER");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (output.next())
    {
      isEmpty = false;
      vo.setMercuryId(output.getString("MERCURY_ID"));
      vo.setMercuryMessageNumber(output.getString("MERCURY_MESSAGE_NUMBER"));
      vo.setMercuryOrderNumber(output.getString("MERCURY_ORDER_NUMBER"));
      vo.setMercuryStatus(output.getString("MERCURY_STATUS"));
      vo.setMessageType(output.getString("MSG_TYPE"));
      vo.setOutboundId(output.getString("OUTBOUND_ID"));
      vo.setSendingFlorist(output.getString("SENDING_FLORIST"));
      vo.setFillingFlorist(output.getString("FILLING_FLORIST"));
      vo.setOrderDate(output.getDate("ORDER_DATE"));
      vo.setRecipient(output.getString("RECIPIENT"));
      vo.setAddress(output.getString("ADDRESS"));
      vo.setCityStateZip(output.getString("CITY_STATE_ZIP"));
      vo.setPhoneNumber(output.getString("PHONE_NUMBER"));
      vo.setDeliveryDate(output.getDate("DELIVERY_DATE"));
      vo.setDeliveryDateText(output.getString("DELIVERY_DATE_TEXT"));
      vo.setFirstChoice(output.getString("FIRST_CHOICE"));
      vo.setSecondChoice(output.getString("SECOND_CHOICE"));
      vo.setPrice(new Double(output.getDouble("PRICE")));
      vo.setCardMessage(output.getString("CARD_MESSAGE"));
      vo.setOccasion(output.getString("OCCASION"));
      vo.setSpecialInstructions(output.getString("SPECIAL_INSTRUCTIONS"));
      vo.setPriority(output.getString("PRIORITY"));
      vo.setOperator(output.getString("OPERATOR"));
      vo.setComments(output.getString("COMMENTS"));
      vo.setSakText(output.getString("SAK_TEXT"));
      vo.setCtseq(output.getInt("CTSEQ"));
      vo.setCrseq(output.getInt("CRSEQ"));
      vo.setTransmissionDate(output.getDate("TRANSMISSION_TIME"));
      vo.setReferenceNumber(output.getString("REFERENCE_NUMBER"));
      vo.setProductId(output.getString("PRODUCT_ID"));
      vo.setZipCode(output.getString("ZIP_CODE"));
      vo.setAskAnswerCode(output.getString("ASK_ANSWER_CODE"));
      vo.setSortValue(output.getString("SORT_VALUE"));
      vo.setRetrievalFlag(output.getString("RETRIEVAL_FLAG"));
      vo.setFromMessageNumber(output.getString("FROM_MESSAGE_NUMBER"));
      vo.setToMessageNumber(output.getString("TO_MESSAGE_NUMBER"));
      vo.setFromMessageDate(output.getDate("FROM_MESSAGE_DATE"));
      vo.setToMessageDate(output.getDate("TO_MESSAGE_DATE"));
      vo.setViewQueue(output.getString("VIEW_QUEUE"));
      vo.setSuffix(output.getString("SUFFIX"));
      vo.setDirection(output.getString("MESSAGE_DIRECTION"));
      vo.setOrderSequence(output.getString("ORDER_SEQ"));
      vo.setAdminSequence(output.getString("ADMIN_SEQ"));
      vo.setCombinedReportNumber(output.getString("COMBINED_REPORT_NUMBER"));
      vo.setRofNumber(output.getString("ROF_NUMBER"));
      vo.setOverUnderCharge(new Double(output.getDouble("OVER_UNDER_CHARGE")));
      vo.setAdjReasonCode(output.getString("ADJ_REASON_CODE"));
      vo.setCompOrder(output.getString("COMP_ORDER"));
      vo.setRequireConfirmation(output.getString("REQUIRE_CONFIRMATION"));
      vo.setOldPrice(new Double(output.getDouble("OLD_PRICE")));
    }
    if (isEmpty)
      return null;
    else
      return vo;
  }
  
  /**
   * This method serves as a wrapper for the SP_UPDATE_MERCURY_MESSAGE stored
   * procedure. It never actually gets called and is implemented here only for 
   * completeness. 
   * 
   * @param vo MercuryVO
   */
  public void updateMessage (MercuryVO vo)
  {
  }
  
  public String getMercuryManualId() throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    long idNum = 0;

    if(logger.isDebugEnabled())
    {        
      logger.debug("generateMercuryId :: String ");
    }
     
    /* setup stored procedure input parameters */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_NEXT_MERCURY_MANUAL_SEQ");

    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    BigDecimal output =(BigDecimal)dataAccessUtil.execute(dataRequest);
    idNum = output.longValue();

    DecimalFormat numFormat = new DecimalFormat();
    numFormat.setMinimumIntegerDigits(4);
    numFormat.setGroupingUsed(false);;
    
    Date today = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
    String outputString = formatter.format(today) + "-" + numFormat.format(idNum);
    return outputString;
  }
    /**
   * This method serves as a wrapper for the SP_INSERT_MERCURY_MESSAGE stored
   * procedure. 
   * 
   * @param vo MercuryVO
   */

  public String insertMessage(MercuryVO vo) throws Exception
  {
    String output = null;
    DataRequest dataRequest = new DataRequest();
    logger.debug("insertMessage:: MercuryVO");
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    
    String mercuryMessageId = this.generateMercuryId(vo.getMessageType());
    if(vo.getMercuryStatus().equals("MN") || vo.getMercuryStatus().equals("MM"))
    {
      if(vo.getMessageType().equals("FTD")) 
      {
        String id = getMercuryManualId();
        vo.setMercuryMessageNumber(id);
        vo.setMercuryOrderNumber(id);
      }
    }
    output = mercuryMessageId;
    inputParams.put("IN_MERCURY_ID", mercuryMessageId);
    inputParams.put("IN_MERCURY_MESSAGE_NUMBER", vo.getMercuryMessageNumber());
    inputParams.put("IN_MERCURY_ORDER_NUMBER", vo.getMercuryOrderNumber());
    inputParams.put("IN_MERCURY_STATUS", vo.getMercuryStatus());
    inputParams.put("IN_MSG_TYPE", vo.getMessageType());
    inputParams.put("IN_OUTBOUND_ID", vo.getOutboundId());
    inputParams.put("IN_SENDING_FLORIST", vo.getSendingFlorist());
    inputParams.put("IN_FILLING_FLORIST", vo.getFillingFlorist());
    inputParams.put("IN_ORDER_DATE", vo.getOrderDate() == null ? null : new java.sql.Date(vo.getOrderDate().getTime()));
    inputParams.put("IN_RECIPIENT", vo.getRecipient());
    inputParams.put("IN_ADDRESS", vo.getAddress());
    inputParams.put("IN_CITY_STATE_ZIP", vo.getCityStateZip());
    inputParams.put("IN_PHONE_NUMBER", vo.getPhoneNumber());
    inputParams.put("IN_DELIVERY_DATE",  vo.getDeliveryDate() == null ? null : new java.sql.Date(vo.getDeliveryDate().getTime()));
    inputParams.put("IN_DELIVERY_DATE_TEXT", vo.getDeliveryDateText());
    inputParams.put("IN_FIRST_CHOICE", vo.getFirstChoice());
    inputParams.put("IN_SECOND_CHOICE", vo.getSecondChoice());
    inputParams.put("IN_PRICE", vo.getPrice() == null? null: vo.getPrice().toString()); ///
    inputParams.put("IN_CARD_MESSAGE", vo.getCardMessage());
    inputParams.put("IN_OCCASION", vo.getOccasion());
    inputParams.put("IN_SPECIAL_INSTRUCTIONS", vo.getSpecialInstructions());
    inputParams.put("IN_PRIORITY", vo.getPriority());
    if (vo.getOperator() == null)
      inputParams.put("IN_OPERATOR", "<"+mercuryMessageId+">SYS");
    else
      inputParams.put("IN_OPERATOR", "<"+mercuryMessageId+">" + vo.getOperator());
    inputParams.put("IN_COMMENTS", vo.getComments());
    inputParams.put("IN_SAK_TEXT", vo.getSakText());
    inputParams.put("IN_CTSEQ", (new Integer (vo.getCtseq ()).toString ()));
    inputParams.put("IN_CRSEQ", (new Integer(vo.getCrseq()).toString()));
    inputParams.put("IN_TRANSMISSION_TIME",  vo.getTransmissionDate() == null ? null : new java.sql.Timestamp(vo.getTransmissionDate().getTime()));
    inputParams.put("IN_REFERENCE_NUMBER", vo.getReferenceNumber());
    inputParams.put("IN_PRODUCT_ID", vo.getProductId());
    inputParams.put("IN_ZIP_CODE", vo.getZipCode());
    inputParams.put("IN_ASK_ANSWER_CODE", vo.getAskAnswerCode());
    inputParams.put("IN_SORT_VALUE", vo.getSortValue());
    inputParams.put("IN_RETRIEVAL_FLAG", vo.getRetrievalFlag());
    inputParams.put("IN_COMBINED_REPORT_NUMBER", vo.getCombinedReportNumber());
    inputParams.put("IN_ROF_NUMBER", vo.getRofNumber());
    inputParams.put("IN_ADJ_REASON_CODE", vo.getAdjReasonCode());
    inputParams.put("IN_OVER_UNDER_CHARGE", vo.getOverUnderCharge() == null ? null : vo.getOverUnderCharge().toString());
    inputParams.put("IN_FROM_MESSAGE_NUMBER", vo.getFromMessageNumber());
    inputParams.put("IN_FROM_MESSAGE_DATE",  vo.getFromMessageDate() == null ? null : new java.sql.Date(vo.getFromMessageDate().getTime()));
    inputParams.put("IN_TO_MESSAGE_NUMBER", vo.getToMessageNumber());
    inputParams.put("IN_TO_MESSAGE_DATE",  vo.getToMessageDate() == null ? null : new java.sql.Date(vo.getToMessageDate().getTime()));
    inputParams.put("IN_VIEW_QUEUE", vo.getViewQueue() == null ? "N" : vo.getViewQueue());
    inputParams.put("IN_MESSAGE_DIRECTION", vo.getDirection());
    inputParams.put("IN_COMP_ORDER", vo.getCompOrder());
    inputParams.put("IN_REQUIRE_CONFIRMATION", vo.getRequireConfirmation() == null ? "N" :  vo.getRequireConfirmation());
    inputParams.put("IN_SUFFIX", vo.getSuffix());
    inputParams.put("IN_ORDER_SEQ", vo.getOrderSequence());
    inputParams.put("IN_ADMIN_SEQ", vo.getAdminSequence());
    inputParams.put("IN_OLD_PRICE",  vo.getOldPrice() == null ? null : vo.getOldPrice().toString());/////
    inputParams.put("IN_ORIG_COMPLAINT_COMM_TYPE_ID", vo.getComplaintCommOriginTypeId());
    inputParams.put("IN_NOTI_COMPLAINT_COMM_TYPE_ID", vo.getComplaintCommNotificationTypeId());
    inputParams.put("IN_APPROVAL_IDENTITY_ID", vo.getApproval_identity_id());

    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_INSERT_MERCURY");
    dataRequest.setInputParams(inputParams);
  
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
    if (!outputs.get("OUT_STATUS").equals(YES))
    {
        throw new Exception((String)outputs.get("OUT_MESSAGE"));
    }
    return output;
  }
  
    /**
   * This method serves as a wrapper for the SP_GET_MERCURY_MESSAGE stored
   * procedure. 
   * 
   * @param messageId String 
   * @return vo MercuryVO
   */

  public MercuryVO getMessage(String messageId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    MercuryVO vo = new MercuryVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    logger.debug("getMessage:: MercuryVO");
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_MERCURY_ID", messageId);
         
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_MERCURY_BY_ID");
    dataRequest.setInputParams(inputParams);
  
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();       
    outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (outputs.next())
    {
      isEmpty = false;
      vo.setMercuryId(messageId);
      vo.setMercuryMessageNumber(outputs.getString("MERCURY_MESSAGE_NUMBER"));
      vo.setMercuryOrderNumber(outputs.getString("MERCURY_ORDER_NUMBER"));
      vo.setMercuryStatus(outputs.getString("MERCURY_STATUS"));
      vo.setMessageType(outputs.getString("MSG_TYPE"));
      vo.setOutboundId(outputs.getString("OUTBOUND_ID"));
      vo.setSendingFlorist(outputs.getString("SENDING_FLORIST"));
      vo.setFillingFlorist(outputs.getString("FILLING_FLORIST"));
      vo.setOrderDate(outputs.getDate("ORDER_DATE"));
      vo.setRecipient(outputs.getString("RECIPIENT"));
      vo.setAddress(outputs.getString("ADDRESS"));
      vo.setCityStateZip(outputs.getString("CITY_STATE_ZIP"));
      vo.setPhoneNumber(outputs.getString("PHONE_NUMBER"));
      vo.setDeliveryDate(outputs.getDate("DELIVERY_DATE"));
      vo.setDeliveryDateText(outputs.getString("DELIVERY_DATE_TEXT"));
      vo.setFirstChoice(outputs.getString("FIRST_CHOICE"));
      vo.setSecondChoice(outputs.getString("SECOND_CHOICE"));
      vo.setPrice(new Double(outputs.getDouble("PRICE")));
      vo.setCardMessage(outputs.getString("CARD_MESSAGE"));
      vo.setOccasion(outputs.getString("OCCASION"));
      vo.setSpecialInstructions(outputs.getString("SPECIAL_INSTRUCTIONS"));
      vo.setPriority(outputs.getString("PRIORITY"));
      vo.setOperator(outputs.getString("OPERATOR"));
      vo.setComments(outputs.getString("COMMENTS"));
      vo.setSakText(outputs.getString("SAK_TEXT"));
      vo.setCtseq(outputs.getInt("CTSEQ"));
      vo.setCrseq(outputs.getInt("CRSEQ"));
      vo.setTransmissionDate(outputs.getDate("TRANSMISSION_TIME"));
      vo.setReferenceNumber(outputs.getString("REFERENCE_NUMBER"));
      vo.setProductId(outputs.getString("PRODUCT_ID"));
      vo.setZipCode(outputs.getString("ZIP_CODE"));
      vo.setAskAnswerCode(outputs.getString("ASK_ANSWER_CODE"));
      vo.setSortValue(outputs.getString("SORT_VALUE"));
      vo.setRetrievalFlag(outputs.getString("RETRIEVAL_FLAG"));
      vo.setFromMessageNumber(outputs.getString("FROM_MESSAGE_NUMBER"));
      vo.setToMessageNumber(outputs.getString("TO_MESSAGE_NUMBER"));
      vo.setFromMessageDate(outputs.getDate("FROM_MESSAGE_DATE"));
      vo.setToMessageDate(outputs.getDate("TO_MESSAGE_DATE"));
      vo.setViewQueue(outputs.getString("VIEW_QUEUE"));
      vo.setSuffix(outputs.getString("SUFFIX"));
      vo.setDirection(outputs.getString("MESSAGE_DIRECTION"));
      vo.setOrderSequence(outputs.getString("ORDER_SEQ"));
      vo.setAdminSequence(outputs.getString("ADMIN_SEQ"));
      vo.setCombinedReportNumber(outputs.getString("COMBINED_REPORT_NUMBER"));
      vo.setRofNumber(outputs.getString("ROF_NUMBER"));
      vo.setOverUnderCharge(new Double(outputs.getDouble("OVER_UNDER_CHARGE")));
      vo.setAdjReasonCode(outputs.getString("ADJ_REASON_CODE"));
      vo.setCompOrder(outputs.getString("COMP_ORDER"));
      vo.setRequireConfirmation(outputs.getString("REQUIRE_CONFIRMATION"));
      vo.setOldPrice(new Double(outputs.getDouble("OLD_PRICE")));
    }
    if (isEmpty)
      return null;
    else
      return vo;
  }

  /**
   * This method serves as a wrapper for the SP_DELETE_MERCURY_MESSAGE stored
   * procedure. It never actually gets called and is implemented here only for 
   * completeness. 
   * 
   * @param vo MercuryVO
   */ 
  public void deleteMessage (MercuryVO vo)
  {
  }
  public String generateMercuryId(String messageType) throws Exception{
    DataRequest dataRequest = new DataRequest();
    long idNum = 0;

    if(logger.isDebugEnabled())
    {        
      logger.debug("generateMercuryId :: String ");
    }
     
    /* setup stored procedure input parameters */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_NEXT_MERCURY_ID_SEQUENCE");

    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    BigDecimal output =(BigDecimal)dataAccessUtil.execute(dataRequest);
    idNum = output.longValue();
    
    String numString = new String(idNum + "");
    // zero pad short ids
    while(numString.length() < 10) 
    {
      numString = "0" + numString;
    }
    
    return new String(messageType + numString);
  }
  public void clearMercuryInboundQueue() throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_DELETE_MESSAGES");
      Map paramMap = new HashMap();
      paramMap.put("IN_QUEUE_NAME", "OJMS.MERCURY_INBOUND");
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
      if (outputs == null || !outputs.get("OUT_STATUS").equals("Y"))
      {
          throw new Exception((String)outputs.get("OUT_MESSAGE"));
      }
  }
  /**
   * Looks up a Mercury OPS record by suffix
   * 
   * @param suffix
   * @return Mercury Ops record
   */
  public List getAvailableSuffixes() throws Exception{
    List suffixList = new LinkedList();
    boolean isEmpty = true;
    DataRequest dataRequest = new DataRequest();

    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put("IN_SUFFIX", null);
    inputParams.put("IN_AVAILABLE_FLAG", "Y");
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_VIEW_MERCURY_OPS");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    CachedResultSet output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (output.next())
    {
      isEmpty = false;
      suffixList.add((String)output.getString("SUFFIX"));
    }
    if (isEmpty)
      return null;
    else
      return suffixList;
  }
  
  
  public String getColorById(String colorId) throws Exception 
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
     dataRequest.setStatementID("OP_GET_COLOR_BY_ID");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_COLOR_ID",colorId);

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     String output = (String) dataAccessUtil.execute(dataRequest);
    
     return output;
  }
  public String getBoxXOccasion(String occasionId) throws Exception 
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
     dataRequest.setStatementID("OP_GET_OCCASION_BOX_X_DESC");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_OCCASION_ID", occasionId);


     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
     String output = null;
     if (outputs.next()){
      output = (String)outputs.getObject(1);
     }
     return output;
  }
  public void markReconciled(String mercuryID) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
     dataRequest.setStatementID("OP_MARK_RECONCILED");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_MERCURY_ID",mercuryID);

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
     String status = (String) outputs.get("OUT_STATUS");
     if(status.equals("N"))
     {
         String message = (String) outputs.get("OUT_MESSAGE");
         throw new Exception(message);
     }
  }
  public LinkedList getManualReconcileList(Date deliveryDate) throws Exception 
  {
     LinkedList output = new LinkedList();
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
     
     dataRequest.setStatementID("OP_GET_MERCURY_RECONCILES");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_DELIVERY_DATE", new Timestamp(deliveryDate.getTime()));

     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
     boolean hasOutput = false;
     while (outputs.next()){
      output.add((String)outputs.getObject(1));
      hasOutput = true;
     }
     if (hasOutput) {
      return output;
     } else 
     {
       return null;
     }
  } 

  public void insertMercuryFOL(String mercuryID) throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
    
     dataRequest.setStatementID("OP_INSERT_MERCURY_FOL");
     Map paramMap = new HashMap();   
     
     paramMap.put("IN_MERCURY_ID",mercuryID);
     
     dataRequest.setInputParams(paramMap);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
     String status = (String) outputs.get("OUT_STATUS");
     if(status.equals("N"))
     {
         String message = (String) outputs.get("OUT_MESSAGE");
         throw new Exception(message);
     }
  }
    
      /**
     * This method serves as a wrapper for the order_mesg_pkg.get_matching_auto_response_key stored
     * procedure. 
     * 
     * @param messageText String 
     * @param msgType String
     * @return vo AutoResponseKeyVO
     */
    public AutoResponseKeyVO getAutoResponseKey (String messageText, String msgType) throws Exception
    {
      DataRequest dataRequest = new DataRequest();
      boolean isEmpty = true;
      CachedResultSet outputs = null;
      AutoResponseKeyVO vo = null;
      logger.debug("getAutoResponseKey:: AutoResponseKeyVO");
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_MESSAGE_TEXT", messageText);
      inputParams.put("IN_MSG_TYPE", msgType);
           
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_GET_MATCHING_AUTO_RESPONSE_KEY");
      dataRequest.setInputParams(inputParams);
    
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();       
      outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
      if (outputs!= null && outputs.next())
      {
        isEmpty = false;
        vo = new AutoResponseKeyVO();
        vo.setKeyPhraseId(outputs.getString("KEY_PHRASE_ID"));
        vo.setMsgType(outputs.getString("MSG_TYPE"));
        vo.setKeyPhraseTxt(outputs.getString("KEY_PHRASE_TXT"));
        vo.setCancelFlag(outputs.getString("RESPOND_WITH_CAN_FLAG"));
        vo.setRetryFTDFlag(outputs.getString("RETRY_FTD_FLAG"));
        vo.setFloristSoftBlockFlag(outputs.getString("FLORIST_SOFT_BLOCK_FLAG"));
        vo.setViewQueueFlag(outputs.getString("VIEW_QUEUE_FLAG"));
      }
      if (isEmpty)
        return null;
      else
        return vo;
    }

    /**
     * This method serves as a wrapper for the mercury_pkg.update_view_queue_flag stored
     * procedure.
     * @param mercuryID
     * @param viewQueue
     * @throws Exception
     */
      public void updateViewQueueFlag(String mercuryID, String viewQueue) throws Exception
      {
         DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
        
         dataRequest.setStatementID("OP_UPDATE_VIEW_QUEUE_FLAG");
         Map paramMap = new HashMap();   
         
         paramMap.put("IN_MERCURY_ID",mercuryID);
          paramMap.put("IN_VIEW_QUEUE",viewQueue);
         
         dataRequest.setInputParams(paramMap);
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
         String status = (String) outputs.get("OUT_STATUS");
         if(status.equals("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
      }  
      
    /**
    * This method serves as a wrapper for the order_mesg_pkg.get_ask_mercury_status stored
    * procedure. Returns true if the FTD related to the message has been cancelled
    * or rejected.
    * 
    * The DB function being called return the following: 
    * inbound ask/ans message flag | rej/can message flag | live ftd flag
    *
    * @param message MercuryVO
    * @return boolean
    */
    public boolean isOrderCancelledOrRejected (MercuryVO message) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        boolean flag = false;
        Object outputs = null;
        String status = null;
        String delimiter = "|";
        logger.debug("isOrderCancelledOrRejected:: boolean");
        
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_MERCURY_ID", message.getMercuryId());
        inputParams.put("IN_CREATED_ON", message.getTransmissionDate());
             
        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_ASK_MERCURY_STATUS");
        dataRequest.setInputParams(inputParams);
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();       
        outputs = dataAccessUtil.execute(dataRequest);
        if (outputs!= null)
        {
            try {
                // Outputs in the format of example Y|N|Y. Tokenizing for the piece between '|'.
                StringTokenizer st = new StringTokenizer((String)outputs);
                status = st.nextToken(delimiter);
                status = st.nextToken(delimiter);
                if ("Y".equalsIgnoreCase(status)) {
                    flag = true;
                }
            } catch (Exception ee) {
                flag = false;
            }
        }
        return flag;
    }        

    /**
     * This method serves as a wrapper for order_mesg_pkg.is_delivery_confirmation_text
     * @param msgText
     * @throws Exception
     */
    public boolean isDeliveryConfirmationText(String msgText) throws Exception {
        boolean result = false;

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        dataRequest.setStatementID("OP_IS_DELIVERY_CONFIRMATION_TEXT");
        Map paramMap = new HashMap();   
         
        paramMap.put("IN_MESSAGE_TEXT",msgText);
         
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if (status != null && status.equalsIgnoreCase("Y")) {
            result = true;
        }

        return result;
    }
    
    public Map insertLifecycleStatus(OrderStatus os) throws Exception {
        
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        Date statusTimestamp = new Date();
        try {
            String statusDate = os.getStatusDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            statusTimestamp = sdf.parse(statusDate);
        } catch (Exception e) {
            logger.error("parse exception: " + e);
        }
        
        //if status text contains a url, split it out into the status url variable
        os.setStatusUrl(null);
        String text = os.getStatusText();
        String url = null;
        int httpStart = text.indexOf("http://");
        if (httpStart < 0) {
        	httpStart = text.indexOf("https://");
        }
        if (httpStart >= 0) {
        	int httpEnd = text.indexOf(" ", httpStart);
            if (httpEnd < 0) {
            	url = text.substring(httpStart);
            	text = text.substring(0, httpStart-1);
            } else {
                url = text.substring(httpStart, httpEnd);
                text = text.substring(0, httpStart-1) + text.substring(httpEnd);
            }
            logger.info("<" + text + ">");
            logger.info("<" + url + ">");
            os.setStatusText(text);
            os.setStatusUrl(url);
        }

        dataRequest.setStatementID("OP_INSERT_LIFECYCLE_STATUS");
        Map paramMap = new HashMap();   
         
        paramMap.put("IN_EFOS_ORDER_NUMBER", os.getEfosNumber());
        paramMap.put("IN_STATUS_CODE", os.getStatusCode());
        paramMap.put("IN_STATUS_TIMESTAMP", new java.sql.Timestamp(statusTimestamp.getTime()));
        paramMap.put("IN_STATUS_TEXT", os.getStatusText());
        paramMap.put("IN_STATUS_URL", os.getStatusUrl());
         
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

        return outputs;
    }
    
    /**
     * Get a list of default sending florist numbers 
     * and their associated available suffixes 
     * 
     * @return list of sending florist vo's
     */
 /*   public List<SendingFloristVO> retrieveCompanySendingFlorists() throws Exception {
        logger.debug("retrieveCompanySendingFlorists :: List<SendingFloristVO> ");

        List list = new ArrayList();
        CachedResultSet output = null;
        DataRequest dataRequest = new DataRequest();
         
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_COMPANY_SENDING_FLORISTS");
        
         execute the stored procedure 
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
        output = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        while (output.next()) {
            SendingFloristVO sfVO = new SendingFloristVO();
            sfVO.setSendingFloristNumber(output.getString("SENDING_FLORIST_NUMBER")); 
            sfVO.setAvailableSuffixes(output.getString("AVAILABLE_SUFFIXES"));
            list.add(sfVO);
        }
        
        return list;
    }*/
    
	@SuppressWarnings("unchecked")
	public List<String> getMercuryEapiOps() throws Exception {
		logger.debug("getMercuryEapiOps :: List<ACTIVE MAIN_MEMBER_CODE> ");

		 DataRequest dataRequest = new DataRequest();
		    dataRequest.setConnection(conn);

		    Map paramMap = new HashMap();
		    paramMap.put("IN_AVAILABLE", "Y"); 

		    dataRequest.setStatementID("EAPI_GET_MAIN_MEMBER_CODES");

		    dataRequest.setInputParams(paramMap);
		    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
		    logger.info("Fetch available main member codes for READING DCONLIFE CYCYE UPDATES process");

		    Map<String, Object> outputs = (Map<String, Object>) dataAccessUtil.execute(dataRequest);
		    String status = (String) outputs.get("OUT_STATUS");
		    if (status.equals("N")) {
		      String message = (String) outputs.get("OUT_MESSAGE");
		      throw new Exception(message);
		    }

		    CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
		    List mainMemberCodes = new ArrayList<String>();
		    while (rs.next()) {
		      mainMemberCodes.add((String) rs.getObject("main_member_code"));
		    }
		    return mainMemberCodes;
		}
    
    /**
     * This method updates the florist selection log id on the mercury.mercury table
     * @param mercuryID
     * @param floristSelectionLogId
     * @throws Exception
     */
      public void updateFloristSelectionLogId(String mercuryId, String floristSelectionLogId) throws Exception
      {         
    	 DataRequest dataRequest = new DataRequest();
         dataRequest.setConnection(conn);
        
         dataRequest.setStatementID("UPDATE_FLORIST_SELECTION_ID");
         Map paramMap = new HashMap();   
         
         paramMap.put("IN_MERCURY_ID",mercuryId);
         paramMap.put("IN_FLORIST_SELECTION_LOG_ID", floristSelectionLogId);
         
         dataRequest.setInputParams(paramMap);
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
         String status = (String) outputs.get("OUT_STATUS");
         if(status.equals("N"))
         {
             String message = (String) outputs.get("OUT_MESSAGE");
             throw new Exception(message);
         }
      } 
      
      /**
       * Overloaded method to call stored procedure to return Message Value Object based 
       * on order detail id and message type.
       * 
       * Populates the following fields in the MessagseVO
       * -sending florist
       * -filling florist
       * -message id
       * 
       */
      public MercuryVO getMessageDetailLastFTD(String orderDetailID)
          throws Exception
      {            
      
          DataRequest request = new DataRequest();
          MercuryVO messageVO = new MercuryVO();
          CachedResultSet messageRS=null;
          String messageType = "Mercury";
          String MESSAGE_DATE_FORMAT =  "yyyy-MM-dd";
          String MESSAGE_END_DELIVERY_DATE_FORMAT = "MMM dd yyyy";
          
          try {
          
              /* setup store procedure input parameters */
              HashMap inputParams = new HashMap();
              inputParams.put("IN_ORDER_DETAIL_ID",orderDetailID);
              inputParams.put("IN_MESSAGE_TYPE",messageType);
              inputParams.put("IN_COMP_ORDER","Y");            
              
              /* build DataRequest object */
              request.setConnection(conn);
              request.reset();
              request.setInputParams(inputParams);
              request.setStatementID("OP_GET_MESG_DETAIL_FROM_LAST_FTD");

          
             /* execute the store procedure */
              DataAccessUtil dau = DataAccessUtil.getInstance();
              messageRS = (CachedResultSet) dau.execute(request);
                           
              if(messageRS.next())
              {  
                  messageVO.setFillingFlorist(messageRS.getString("filling_florist"));
                  messageVO.setSendingFlorist(messageRS.getString("sending_florist"));                    
                  messageVO.setPrice(getDouble(messageRS.getObject("price")));
                  messageVO.setMercuryOrderNumber(messageRS.getString("message_order_number"));
                  messageVO.setDeliveryDate(messageRS.getDate("delivery_date"));
                  messageVO.setMercuryId(messageRS.getString("ftd_message_id"));
              }

          }finally {

              if(logger.isDebugEnabled()){
                 logger.debug("Exiting getMessageDetailNewFTD");
              } 
          }
          
          return messageVO;

      
      }
      
      public boolean isFTDCancelled(String mercuryId) throws Exception
      {
		boolean flag = false;
		HashMap inputParams = new HashMap();
		inputParams.put("IN_MERCURY_ID", mercuryId);
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(conn);
		dataRequest.setStatementID("IS_FTD_CANCELLED");
		dataRequest.setInputParams(inputParams);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

		String outFlag = (String) dataAccessUtil.execute(dataRequest);

		if (outFlag != null && outFlag.equalsIgnoreCase("Y"))
			flag = true;
		else
			flag = false;

		return flag;
    	  
      }
      
      private double getDouble(Object obj)
      {
          double value = 0.00;
          if(obj != null)
          {
              value = Double.parseDouble(obj.toString());
          }
          
          return value;
      }
        
}