package com.ftd.op.mercury.dao;

import com.ftd.op.order.vo.FloristVO;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * Florist Mainetenance DAO.
 *
 * Ganesh M.
 */
public class FloristMaintenanceDAO {

    private Connection connection;
    private Logger logger;

    // additional comment for soft block
    private static final String BLOCK_ADDITIONAL_COMMENT = "BLOCKED BY SYS";
    
    // constants
    private static final String YES = "Y";
    private static final String NO = "N";
    
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE"; 

    public FloristMaintenanceDAO(Connection connection)
    {
        this.connection = connection;
        this.logger = new Logger("com.ftd.op.mercury.dao.FloristMaintenanceDAO");
    }
    
    /*
     * Update florist block information
     */
    public void updateFloristBlocks(FloristVO floristVO, String blockIndicator) throws Exception
    {
    
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        List resultList = new ArrayList();

        dataRequest.setStatementID("OP_UPDATE_FLORIST_BLOCKS");
        Map paramMap = new HashMap();
        paramMap.put("IN_FLORIST_ID",floristVO.getFloristId()); 
        paramMap.put("IN_BLOCK_TYPE",floristVO.getBlockType()); 
        paramMap.put("IN_BLOCK_INDICATOR",blockIndicator); 
        paramMap.put("IN_BLOCK_START_DATE", convertDate(floristVO.getBlockStartDate()));
        paramMap.put("IN_BLOCK_END_DATE",convertDate(floristVO.getBlockEndDate())); 
        paramMap.put("IN_BLOCKED_BY_USER_ID",floristVO.getBlockedByUserId());
        if(blockIndicator.equalsIgnoreCase(YES)) {
            paramMap.put("IN_ADDITIONAL_COMMENT",BLOCK_ADDITIONAL_COMMENT); 
        } else {
            paramMap.put("IN_ADDITIONAL_COMMENT",null);
        }
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
    }
    
    /*
    * 
    */
    private java.sql.Date convertDate(java.util.Date value)
    {
    
    
       java.sql.Date sqlDate = null;
       
       if(value != null)
       {
           sqlDate = new java.sql.Date(value.getTime());
       }//end if
       return sqlDate;
       
    }//end method    
}
