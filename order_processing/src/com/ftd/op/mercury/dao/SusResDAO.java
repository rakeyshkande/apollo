package com.ftd.op.mercury.dao;
import com.ftd.op.mercury.vo.GotoShutdownVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;

public class SusResDAO 
{
  private Connection conn;
  private Logger logger;
  public SusResDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.mercury.SusResDAO");
  } 
  public void insertGotoShutdown(GotoShutdownVO vo) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    logger.debug("insertGotoShutdown:: GotoShutdownVO");
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_FLORIST_ID", vo.getFloristId());
    inputParams.put("IN_P_STATUS", vo.getPStatus());
    inputParams.put("IN_F_STATUS", vo.getFStatus());
    inputParams.put("IN_Q_STATUS", vo.getQStatus());
    inputParams.put("IN_X_STATUS", vo.getXStatus());
    inputParams.put("IN_MERC_INFO", vo.getMercInfo());
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_INSERT_GOTO_FLORIST_SHUTDOWN");
    dataRequest.setInputParams(inputParams);
  
    /* execute the stored procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
    String test = (String)outputs.get("OUT_STATUS");
    if (!test.equalsIgnoreCase("Y"))
    {
        throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
    }
  }
  public void updateGotoShutdown(GotoShutdownVO vo) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    logger.debug("updateGotoShutdown:: GotoShutdownVO");
    
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_FLORIST_ID", vo.getFloristId());
    inputParams.put("IN_X_STATUS", vo.getXStatus());
    inputParams.put("IN_P_STATUS", vo.getPStatus());
    inputParams.put("IN_F_STATUS", vo.getFStatus());
    inputParams.put("IN_Q_STATUS", vo.getQStatus());
    inputParams.put("IN_MERC_INFO", vo.getMercInfo());
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_UPDATE_GOTO_FLORIST_SHUTDOWN");
    dataRequest.setInputParams(inputParams);
  
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
    if (!((String)outputs.get("OUT_STATUS")).equalsIgnoreCase("Y"))
    {
        throw new Exception((String)outputs.get("OUT_MESSAGE"));
    }
  }
  public void deleteGotoShutdown(GotoShutdownVO vo) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    logger.debug("deleteGotoShutdown:: GotoShutdownVO");
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_FLORIST_ID", vo.getFloristId());
    
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_DELETE_GOTO_FLORIST_SHUTDOWN");
    dataRequest.setInputParams(inputParams);
  
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    HashMap outputs = (HashMap) dataAccessUtil.execute(dataRequest);
    if (!((String)outputs.get("OUT_STATUS")).equalsIgnoreCase("Y"))
    {
        throw new Exception((String)outputs.get("OUT_MESSAGE"));
    }
    
  }
  public GotoShutdownVO getGotoShutdown(String floristId) throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    GotoShutdownVO vo = new GotoShutdownVO();
    boolean isEmpty = true;
    CachedResultSet outputs = null;
    logger.debug("getGotoShutdown:: floristId");
    /* setup stored procedure input parameters */
    
    HashMap inputParams = new HashMap();
    inputParams.put("IN_FLORIST_ID", floristId);
         
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("OP_GET_GOTO_FLORIST_SHUTDOWN");
    dataRequest.setInputParams(inputParams);
  
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();       
    outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    if (outputs != null && outputs.next())
    {
      isEmpty = false;
      vo.setFloristId(outputs.getString("FLORIST_ID"));
      vo.setFStatus(outputs.getString("F_STATUS"));
      vo.setPStatus(outputs.getString("P_STATUS"));
      vo.setQStatus(outputs.getString("Q_STATUS"));
      vo.setXStatus(outputs.getString("X_STATUS"));
      vo.setMercInfo(outputs.getString("MERC_INFO"));
    } else {
      return null;
    } 
    return vo;
  }
}