package com.ftd.op.mercury.vo;  

import java.util.Date;

public class MercuryOpsVO 
{
  private String Suffix;
  private boolean inUse;
  private Date LastTransmission;
  private Date LastActivity;
  private boolean Available;
  private boolean SendNewOrders;
  private int SleepTime;
  private int maxBatchSize;
  private int inboundAdminSequence;
  private int inboundOrderSequence;
  private int outboundAdminSequence;
  private int outboundOrderSequence;
  private boolean sequenceLock;
  
  public MercuryOpsVO()
  {
  }

  public int getMaxBatchSize()
  {
    return maxBatchSize;
  }

  public void setMaxBatchSize(int value)
  {
    maxBatchSize = value;
  }

  public String getSuffix()
  {
    return Suffix;
  }

  public void setSuffix(String newSuffix)
  {
    Suffix = newSuffix;
  }

  public boolean isInUse()
  {
    return inUse;
  }

  public void setInUse(boolean newInUse)
  {
    inUse = newInUse;
  }

  public Date getLastTransmission()
  {
    return LastTransmission;
  }

  public void setLastTransmission(Date newLastTransmission)
  {
    LastTransmission = newLastTransmission;
  }

  public Date getLastActivity()
  {
    return LastActivity;
  }

  public void setLastActivity(Date newLastActivity)
  {
    LastActivity = newLastActivity;
  }

  public boolean isAvailable()
  {
    return Available;
  }

  public void setAvailable(boolean newAvailable)
  {
    Available = newAvailable;
  }

  public boolean isSendNewOrders()
  {
    return SendNewOrders;
  }

  public void setSendNewOrders(boolean newSendNewOrders)
  {
    SendNewOrders = newSendNewOrders;
  }

  public int getSleepTime()
  {
    return SleepTime;
  }

  public void setSleepTime(int newSleepTime)
  {
    SleepTime = newSleepTime;
  }


  public void setInboundAdminSequence(int inboundAdminSequence)
  {
    this.inboundAdminSequence = inboundAdminSequence;
  }


  public int getInboundAdminSequence()
  {
    return inboundAdminSequence;
  }


  public void setInboundOrderSequence(int inboundOrderSequence)
  {
    this.inboundOrderSequence = inboundOrderSequence;
  }


  public int getInboundOrderSequence()
  {
    return inboundOrderSequence;
  }


  public void setOutboundAdminSequence(int outboundAdminSequence)
  {
    this.outboundAdminSequence = outboundAdminSequence;
  }


  public int getOutboundAdminSequence()
  {
    return outboundAdminSequence;
  }


  public void setOutboundOrderSequence(int outboundOrderSequence)
  {
    this.outboundOrderSequence = outboundOrderSequence;
  }


  public int getOutboundOrderSequence()
  {
    return outboundOrderSequence;
  }


  public void setSequenceLock(boolean sequenceLock)
  {
    this.sequenceLock = sequenceLock;
  }


  public boolean isSequenceLock()
  {
    return sequenceLock;
  }
}
