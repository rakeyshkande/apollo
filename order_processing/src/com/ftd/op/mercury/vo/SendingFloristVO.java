package com.ftd.op.mercury.vo;

public class SendingFloristVO {
	
	private String sendingFloristNumber;
	private String availableSuffixes;
	 

	public SendingFloristVO()
	{
	}


	public String getSendingFloristNumber() {
		return sendingFloristNumber;
	}


	public void setSendingFloristNumber(String sendingFloristNumber) {
		this.sendingFloristNumber = sendingFloristNumber;
	}


	public String getAvailableSuffixes() {
		return availableSuffixes;
	}


	public void setAvailableSuffixes(String availableSuffixes) {
		this.availableSuffixes = availableSuffixes;
	}


	

	
}
