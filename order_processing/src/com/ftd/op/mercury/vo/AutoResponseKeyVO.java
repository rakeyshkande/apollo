package com.ftd.op.mercury.vo;


public class AutoResponseKeyVO
{
    private String keyPhraseId;
    private String msgType;
    private String keyPhraseTxt;
    private String cancelFlag;
    private String retryFTDFlag;
    private String floristSoftBlockFlag;
    private String viewQueueFlag;
    
    public AutoResponseKeyVO()
    {
    }

    public void setKeyPhraseId(String keyPhraseId) {
        this.keyPhraseId = keyPhraseId;
    }

    public String getKeyPhraseId() {
        return keyPhraseId;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setKeyPhraseTxt(String keyPhraseTxt) {
        this.keyPhraseTxt = keyPhraseTxt;
    }

    public String getKeyPhraseTxt() {
        return keyPhraseTxt;
    }

    public void setCancelFlag(String cancelFlag) {
        this.cancelFlag = cancelFlag;
    }

    public String getCancelFlag() {
        return cancelFlag;
    }

    public void setRetryFTDFlag(String retryFTDFlag) {
        this.retryFTDFlag = retryFTDFlag;
    }

    public String getRetryFTDFlag() {
        return retryFTDFlag;
    }

    public void setFloristSoftBlockFlag(String floristSoftBlockFlag) {
        this.floristSoftBlockFlag = floristSoftBlockFlag;
    }

    public String getFloristSoftBlockFlag() {
        return floristSoftBlockFlag;
    }

    public void setViewQueueFlag(String viewQueueFlag) {
        this.viewQueueFlag = viewQueueFlag;
    }

    public String getViewQueueFlag() {
        return viewQueueFlag;
    }
}
