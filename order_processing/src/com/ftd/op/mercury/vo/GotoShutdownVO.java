package com.ftd.op.mercury.vo;


public class GotoShutdownVO
{

    public GotoShutdownVO()
    {
        floristId = null;
        pStatus = null;
        fStatus = null;
        qStatus = null;
        xStatus = null;
        mercInfo = null;
    }

    public boolean isClear()
    {
        return pStatus == null && qStatus == null && fStatus == null && xStatus == null;
    }

    public void setFloristId(String floristId)
    {
        this.floristId = floristId;
    }

    public String getFloristId()
    {
        return floristId;
    }

    public void setPStatus(String pStatus)
    {
        this.pStatus = pStatus;
    }

    public String getPStatus()
    {
        return pStatus;
    }

    public void setFStatus(String fStatus)
    {
        this.fStatus = fStatus;
    }

    public String getFStatus()
    {
        return fStatus;
    }

    public void setQStatus(String qStatus)
    {
        this.qStatus = qStatus;
    }

    public String getQStatus()
    {
        return qStatus;
    }

    public void setXStatus(String xStatus)
    {
        this.xStatus = xStatus;
    }

    public String getXStatus()
    {
        return xStatus;
    }

    public void setMercInfo(String mercInfo)
    {
        this.mercInfo = mercInfo;
    }

    public String getMercInfo()
    {
        return mercInfo;
    }

    private String floristId;
    private String pStatus;
    private String fStatus;
    private String qStatus;
    private String xStatus;
    private String mercInfo;
}