package com.ftd.op.mercury.to;
import java.io.Serializable;

public class CalculatedFieldsTO implements Serializable
{
  String firstChoice;
  String secondChoice;
  Double price;
  String specialInstructions;
  String cardMessage;
  String occasion;
  
  public CalculatedFieldsTO()
  {
  }

  public void setFirstChoice(String firstChoice)
  {
    this.firstChoice = firstChoice;
  }


  public String getFirstChoice()
  {
    return firstChoice;
  }


  public void setSecondChoice(String secondChoice)
  {
    this.secondChoice = secondChoice;
  }


  public String getSecondChoice()
  {
    return secondChoice;
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }

  public void setSpecialInstructions(String specialInstructions)
  {
    this.specialInstructions = specialInstructions;
  }


  public String getSpecialInstructions()
  {
    return specialInstructions;
  }


  public String getCardMessage() {
	return cardMessage;
  }


  public String getOccasion() {
	return occasion;
  }


  public void setCardMessage(String cardMessage) {
	this.cardMessage = cardMessage;
  }


  public void setOccasion(String occasion) {
	this.occasion = occasion;
  }

}