package com.ftd.op.mercury.to;

import java.io.Serializable;

public class OrderDetailKeyTO implements Serializable 
{

  private String orderDetailId;
  
  public OrderDetailKeyTO()
  {
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }
   
}