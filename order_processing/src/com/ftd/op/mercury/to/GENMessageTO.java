package com.ftd.op.mercury.to;

import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import java.io.Serializable;
import java.util.Date;

public class GENMessageTO extends BaseMercuryMessageTO implements Serializable
{
  private Date deliveryDate;
  private String priority;
  private String comments;
  private String retrievalFlag;
  private static final String type = "GEN";
  
 public GENMessageTO()
  {
    this.setMessageType(type);
  }


  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }


  public Date getDeliveryDate()
  {
    return deliveryDate;
  }


  public void setPriority(String priority)
  {
    this.priority = priority;
  }


  public String getPriority()
  {
    return priority;
  }


  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setRetrievalFlag(String retrievalFlag)
  {
    this.retrievalFlag = retrievalFlag;
  }


  public String getRetrievalFlag()
  {
    return retrievalFlag;
  }
}
 