package com.ftd.op.mercury.to;

import com.ftd.op.mercury.to.BaseMercuryMessageTO;

public class ADJMessageTO extends BaseMercuryMessageTO 
{
  private String mercuryId;
  private Double price;
  private String comments;
  private String combinedReportNumber;
  private String rofNumber;
  private String adjustmentReasonCode;
  private Double oldPrice;
  private Double overUnderCharge;
  private static final String type = "ADJ";
  
  public ADJMessageTO()
  {
    this.setMessageType(type);
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setCombinedReportNumber(String combinedReportNumber)
  {
    this.combinedReportNumber = combinedReportNumber;
  }


  public String getCombinedReportNumber()
  {
    return combinedReportNumber;
  }


  public void setRofNumber(String rofNumber)
  {
    this.rofNumber = rofNumber;
  }


  public String getRofNumber()
  {
    return rofNumber;
  }


  public void setAdjustmentReasonCode(String adjustmentReasonCode)
  {
    this.adjustmentReasonCode = adjustmentReasonCode;
  }


  public String getAdjustmentReasonCode()
  {
    return adjustmentReasonCode;
  }


  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }


  public Double getOldPrice()
  {
    return oldPrice;
  }


  public void setOverUnderCharge(Double overUnderCharge)
  {
    this.overUnderCharge = overUnderCharge;
  }


  public Double getOverUnderCharge()
  {
    return overUnderCharge;
  }


  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }


  public String getMercuryId()
  {
    return mercuryId;
  }

    
}