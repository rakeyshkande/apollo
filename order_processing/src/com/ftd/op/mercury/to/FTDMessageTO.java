package com.ftd.op.mercury.to;

import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import java.io.Serializable;
import java.util.Date;

public class FTDMessageTO extends BaseMercuryMessageTO implements Serializable
{
  private Date orderDate;
  private String recipient;
  private String address;
  private String cityStateZip;
  private String phoneNumber;
  private Date deliveryDate;
  private String deliveryDateText;
  private String firstChoice;
  private String secondChoice;
  private Double price;
  private String cardMessage;
  private String occasion;
  private String specialInstructions;
  private String productId;
  private String zipCode;
  private String requestConfirmation; /*boolean containing Y or N*/
  private String orderDetailId;
  private String isCompOrder; /*boolean containing Y or N*/
  private Double oldPrice;
  
  private static final String type = "FTD";
  
 public FTDMessageTO()
  {
    this.setMessageType(type);
  }

  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }


  public Date getOrderDate()
  {
    return orderDate;
  }


  public void setRecipient(String recipient)
  {
    this.recipient = recipient;
  }


  public String getRecipient()
  {
    return recipient;
  }


  public void setAddress(String address)
  {
    this.address = address;
  }


  public String getAddress()
  {
    return address;
  }

  public void setCityStateZip(String cityStateZip)
  {
    this.cityStateZip = cityStateZip;
  }


  public String getCityStateZip()
  {
    return cityStateZip;
  }


 public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }


  public Date getDeliveryDate()
  {
    return deliveryDate;
  }


  public void setDeliveryDateText(String deliveryDateText)
  {
    this.deliveryDateText = deliveryDateText;
  }


  public String getDeliveryDateText()
  {
    return deliveryDateText;
  }


  public void setFirstChoice(String firstChoice)
  {
    this.firstChoice = firstChoice;
  }


  public String getFirstChoice()
  {
    return firstChoice;
  }


  public void setSecondChoice(String secondChoice)
  {
    this.secondChoice = secondChoice;
  }


  public String getSecondChoice()
  {
    return secondChoice;
  }


  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = cardMessage;
  }


  public String getCardMessage()
  {
    return cardMessage;
  }


  public void setOccasion(String occasion)
  {
    this.occasion = occasion;
  }


  public String getOccasion()
  {
    return occasion;
  }


  public void setSpecialInstructions(String specialInstructions)
  {
    this.specialInstructions = specialInstructions;
  }


  public String getSpecialInstructions()
  {
    return specialInstructions;
  }


  public void setProductId(String productId)
  {
    this.productId = productId;
  }


  public String getProductId()
  {
    return productId;
  }


  public void setZipCode(String zipCode)
  {
    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setRequestConfirmation(String requestConfirmation)
  {
    this.requestConfirmation = requestConfirmation;
  }


  public String isRequestConfirmation()
  {
    return requestConfirmation;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setIsCompOrder(String isCompOrder)
  {
    this.isCompOrder = isCompOrder;
  }


  public String isIsCompOrder()
  {
    return isCompOrder;
  }


  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }


  public Double getOldPrice()
  {
    return oldPrice;
  }


 








}
 