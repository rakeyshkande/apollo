package com.ftd.op.mercury.to;
import java.io.Serializable;

public class BaseMercuryMessageTO implements Serializable
{
  private String messageType;
  private String mercuryStatus;
  private String operator;
  private String direction;
  private String sendingFlorist;
  private String fillingFlorist;
  public static final String INBOUND = "INBOUND";
  public static final String OUTBOUND = "OUTBOUND";
  public static final String MERCURY_OPEN = "MO";
  public static final String MERCURY_MANUAL = "MM";
  public static final String MERCURY_NOSEND = "MN";
  
  public BaseMercuryMessageTO()
  {
  }


  public void setMessageType(String messageType)
  {
    this.messageType = messageType;
  }


  public String getMessageType()
  {
    return messageType;
  }


  public void setMercuryStatus(String mercuryStatus)
  {
    this.mercuryStatus = mercuryStatus;
  }


  public String getMercuryStatus()
  {
    return mercuryStatus;
  }


  public void setOperator(String operator)
  {
    this.operator = operator;
  }


  public String getOperator()
  {
    return operator;
  }


  public void setDirection(String direction)
  {
    this.direction = direction;
  }


  public String getDirection()
  {
    return direction;
  }


  public void setSendingFlorist(String sendingFlorist)
  {
    this.sendingFlorist = sendingFlorist;
  }


  public String getSendingFlorist()
  {
    return sendingFlorist;
  }


  public void setFillingFlorist(String fillingFlorist)
  {
    this.fillingFlorist = fillingFlorist;
  }


  public String getFillingFlorist()
  {
    return fillingFlorist;
  }
  
 
}