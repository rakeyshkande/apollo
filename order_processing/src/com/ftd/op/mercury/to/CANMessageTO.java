package com.ftd.op.mercury.to;

import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import java.io.Serializable;

public class CANMessageTO extends BaseMercuryMessageTO implements Serializable
{
  private String mercuryId;
  private String comments;
  private static final String type = "CAN";
  private String complaintCommOriginTypeId;
  private String complaintCommNotificationTypeId;
  
  public CANMessageTO()
  {
    this.setMessageType(type);
  }

  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }


  public String getMercuryId()
  {
    return mercuryId;
  }


  public String getComplaintCommOriginTypeId()
  {
    return complaintCommOriginTypeId;
  }

  public void setComplaintCommOriginTypeId(String complaintCommOriginTypeId)
  {
    this.complaintCommOriginTypeId = complaintCommOriginTypeId;
  }

  public String getComplaintCommNotificationTypeId()
  {
    return complaintCommNotificationTypeId;
  }

  public void setComplaintCommNotificationTypeId(String complaintCommNotificationTypeId)
  {
    this.complaintCommNotificationTypeId = complaintCommNotificationTypeId;
  }
}
