package com.ftd.op.mercury.to;

import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import java.io.Serializable;

public class ASKMessageTO extends BaseMercuryMessageTO implements Serializable
{
  private String mercuryId;
  private Double price;
  private String comments;
  private String askAnswerCode;
  private Double oldPrice;
  private static final String type = "ASK";
  private String approval_identity_id;
  

  public ASKMessageTO()
  {
    this.setMessageType(type);
  }

  public void setPrice(Double price)
  {
    this.price = price;
  }


  public Double getPrice()
  {
    return price;
  }


  public void setAskAnswerCode(String askAnswerCode)
  {
    this.askAnswerCode = askAnswerCode;
  }


  public String getAskAnswerCode()
  {
    return askAnswerCode;
  }


  public void setOldPrice(Double oldPrice)
  {
    this.oldPrice = oldPrice;
  }


  public Double getOldPrice()
  {
    return oldPrice;
  }


  public void setComments(String comments)
  {
    this.comments = comments;
  }


  public String getComments()
  {
    return comments;
  }


  public void setMercuryId(String mercuryId)
  {
    this.mercuryId = mercuryId;
  }


  public String getMercuryId()
  {
    return mercuryId;
  }

  public String getApproval_identity_id() {
	  return approval_identity_id;
  }

  public void setApproval_identity_id(String approval_identity_id) {
	  this.approval_identity_id = approval_identity_id;
  }  
}