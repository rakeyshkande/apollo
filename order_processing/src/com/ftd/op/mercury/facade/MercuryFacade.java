package com.ftd.op.mercury.facade;

import com.ftd.op.mercury.service.MercuryOutboundService;
import com.ftd.op.mercury.to.*;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderBillVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.SecondChoiceVO;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;
import java.util.ArrayList; 

/**
 * MercuryFacade
 * 
 * This is a facade between the MercuryService and MercuryAPIBean
 * to create single calls for API use
 * 
 * @author Laura Felch
 */

public class MercuryFacade
{
 
  private Connection conn;
  private Logger logger;  
    
  public MercuryFacade(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.op.mercury.facade.MercuryFacade");
  }

/**
   * This method gets an OrderDetailVO that matches the passed in key and calls 
   * sendFTDMessage(OrderDetailVO)
   * 
   * @param to OrderDetailKeyTO
   */
  public String sendFTDMessage(OrderDetailKeyTO to) throws Exception
  {
    OrderDAO orderDAO = new OrderDAO(conn);
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(to.getOrderDetailId());
    return this.sendFTDMessage(orderDetailVO);
  }
  
/**
   * This method gets an OrderDetailVO that matches the passed in key and calls 
   * sendFTDMessage(OrderDetailVO)
   * 
   * @param to OrderDetailKeyTO
   */
  public String sendFTDMessage(OrderDetailKeyTO to, String floristId) throws Exception
  {
    OrderDAO orderDAO = new OrderDAO(conn);
    OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(to.getOrderDetailId());
    orderDetailVO.setFloristId(floristId);
    return this.sendFTDMessage(orderDetailVO);
  }
  
/**
   * This method wraps the building and sending of FTD messages into a single 
   * call for internal processing use
   * 
   * @param vo OrderDetailVO
   */

  public String sendFTDMessage(OrderDetailVO vo) throws Exception
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildFTDMessage(vo));
  }
  

/**
   * This method wraps the building and sending of FTD messages into a single 
   * call for API use
   * 
   * @param to FTDMessageTO
   */

  public String sendFTDMessage(FTDMessageTO to) throws Exception
  {
      MercuryOutboundService ms = new MercuryOutboundService(conn);
      return ms.sendMercuryMessage(ms.buildFTDMessage(to));
  }
  
/**
   * This method wraps the building and sending of ADJ messages into a single 
   * call for API use
   * 
   * @param to ADJMessageTO
   */
  public String sendADJMessage(ADJMessageTO to)throws Exception 
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildADJMessage(to));
  }

/**
   * This method wraps the building and sending of ASK messages into a single 
   * call for API use
   * 
   * @param to ASKMessageTO
   */
  public String sendASKMessage(ASKMessageTO to) throws Exception
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildASKMessage(to));
  }

/**
   * This method wraps the building and sending of ANS messages into a single 
   * call for API use
   * 
   * @param to ANSMessageTO
   */
   public String sendANSMessage(ANSMessageTO to) throws Exception
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildANSMessage(to));
  }

/**
   * This method wraps the building and sending of CAN messages into a single 
   * call for API use
   * 
   * @param to CANMessageTO
   */
   public String sendCANMessage(CANMessageTO to) throws Exception
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildCANMessage(to));
  }

/**
   * This method wraps the building and sending of GEN messages into a single 
   * call for API use
   * 
   * @param to GENMessageTO
   */
   public String sendGENMessage(GENMessageTO to) throws Exception
  {
     MercuryOutboundService ms = new MercuryOutboundService(conn);
     return ms.sendMercuryMessage(ms.buildGENMessage(to));
  }
  
  public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order) throws Exception 
  {
    
    MercuryOutboundService ms = new MercuryOutboundService(conn);
    CalculatedFieldsTO fields = new CalculatedFieldsTO();
    OrderDAO dao = new OrderDAO(conn);
    
    
    
    // get needed VOs
    OrderDetailVO orderDetail = dao.getOrderDetail(order.getOrderDetailId());
    ProductVO product = dao.getProduct(orderDetail.getProductId());
    ArrayList addOns = orderDetail.getAddOnList();
    SecondChoiceVO secondChoice = dao.getSecondChoice(product.getSecondChoiceCode());
    CustomerVO customer = dao.getCustomer(orderDetail.getRecipientId());
    OrderBillVO orderBill = dao.getOrderBill(new Long(orderDetail.getOrderDetailId()).toString());
    SourceVO source = dao.getSource(orderDetail.getSourceCode());

    String sizeIndicator = orderDetail.getSizeIndicator();
    String lookupId = (sizeIndicator == null ? orderDetail.getProductId() : orderDetail.getProductId() + sizeIndicator);
    
    GlobalParameterVO mercPriceOverrideVO = dao.getGlobalParameter("ORDER_PROCESSING",
            "MERC-PRICE-OVERRIDE-" + lookupId);
    if (mercPriceOverrideVO != null ) {
        String newPrice = mercPriceOverrideVO.getValue();
        try {
            orderBill.setProductAmount(Double.parseDouble(newPrice));
            logger.info("Mercury price changed to " + newPrice);
        } catch (NumberFormatException nfe) {
            logger.error("New price is not numeric: " + newPrice + " - Using PDB price");
        }
    } else {
    	logger.info("##### Price Override is not set up. Fetching flag from FTD_APPS.ORIGINS table");
    	logger.info("##### Pdb price in order bill -" + orderBill.getPdbPrice());
    	if(dao.getSendFloristPdbPriceFlag(orderDetail.getOrderGuid()).equals("Y")){
    		logger.info("##### Price Override flag is set to 'Y'");
    		orderBill.setProductAmount(orderBill.getPdbPrice());
    		logger.info("##### Product Amount changed to new Pdb price - " + orderBill.getProductAmount());
    	}
    }

    GlobalParameterVO emergencyMercTextFlagVO = dao.getGlobalParameter("ORDER_PROCESSING",
            "EMERGENCY_MERC_TEXT_FLAG");
    String emergencyMercTextFlag = null;
    String emergencyMercTextDate = null;
    String emergencyMercTextEndDate = null;
    String emergencyMercText = null;

    if (emergencyMercTextFlagVO != null) {
        emergencyMercTextFlag = emergencyMercTextFlagVO.getValue();
    } else {
        emergencyMercTextFlag = "N";
        logger.info("ORDER_PROCESSING:EMERGENCY_MERC_TEXT_FLAG global parm not set. Defaulting to 'N'.");
    }

    if (emergencyMercTextFlag != null && emergencyMercTextFlag.equalsIgnoreCase("Y")) {

        GlobalParameterVO emergencyMercTextDateVO = dao.getGlobalParameter("ORDER_PROCESSING", "EMERGENCY_MERC_TEXT_DATE");
        if (emergencyMercTextDateVO != null) {
            emergencyMercTextDate = emergencyMercTextDateVO.getValue();
        }

        GlobalParameterVO emergencyMercTextEndDateVO = dao.getGlobalParameter("ORDER_PROCESSING", "EMERGENCY_MERC_TEXT_END_DATE");
        if (emergencyMercTextEndDateVO != null) {
            emergencyMercTextEndDate = emergencyMercTextEndDateVO.getValue();
        }

        GlobalParameterVO emergencyMercTextVO = dao.getGlobalParameter("ORDER_PROCESSING", "EMERGENCY_MERC_TEXT");
        if (emergencyMercTextVO != null) {
            emergencyMercText = emergencyMercTextVO.getValue();
        }
    }
    
    try {
	    // SP-68 : New FTD for FEMOE should be retrieved from Old FTD.
	    boolean isFEMOE = dao.isFEMOE(String.valueOf(orderDetail.getOrderDetailId()));
	
	    if (isFEMOE) {
	    	MercuryVO lastFTD = dao.getLastFTD(String.valueOf(orderDetail.getOrderDetailId()));
	    	if (lastFTD != null && lastFTD.getOperator() != null && lastFTD.getOperator().contains("FEMOE")) {
	    		fields.setFirstChoice(lastFTD.getFirstChoice());
	    		fields.setSecondChoice(lastFTD.getSecondChoice());
	    		fields.setCardMessage(lastFTD.getCardMessage());
	    		if (mercPriceOverrideVO != null ) {
	    			String newPrice = mercPriceOverrideVO.getValue();
	    			try {
	    				fields.setPrice(Double.parseDouble(newPrice));
	    				logger.info("Mercury price changed to " + newPrice);
	    			} catch (NumberFormatException nfe) {
	    				logger.error("New price is not numeric: " + newPrice + " - Using PDB price");
	    			}
	    		}
	    		else {
	    			fields.setPrice(lastFTD.getPrice());
	    		}
	    		fields.setOccasion(lastFTD.getOccasion());
	    		fields.setSpecialInstructions(ms.buildSpecialInstructionString(
	                    orderDetail, source, emergencyMercTextFlag, emergencyMercTextDate,
	                    emergencyMercText, emergencyMercTextEndDate, customer, product, "Facade"));
	    		return fields;
	    	}
	    }
    }
    catch (Exception e) {
    	logger.error("Unable to check whether the order is FEMOE or not for orderDetailId: " + orderDetail.getOrderDetailId(), e);
    }
    
    
    
    // calculate the fields
    fields.setFirstChoice(ms.buildFirstChoiceString(orderDetail, product, addOns));
    fields.setSecondChoice(ms.buildSecondChoiceString(orderDetail, product, secondChoice, addOns));
    fields.setPrice(ms.buildPrice(orderDetail, customer, orderBill, addOns));
    fields.setSpecialInstructions(ms.buildSpecialInstructionString(
                    orderDetail, source, emergencyMercTextFlag, emergencyMercTextDate,
                    emergencyMercText, emergencyMercTextEndDate, customer, product, "Facade"));
    
    return fields;   
  }
}