package com.ftd.op.mercury.service;

import com.ftd.op.common.service.QueueService;
import com.ftd.op.common.to.BaseTO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.util.ProxyFlorist;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.AddOnVO;
import com.ftd.op.order.vo.CustomerPhoneVO;
import com.ftd.op.order.vo.CustomerVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderBillVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.PaymentVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.op.order.vo.SecondChoiceVO;
import com.ftd.op.order.vo.SourceVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.FTDCommonUtils;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


/**
 * MercuryService
 *
 * This is a service between the MercuryDAO and MercuryFacade
 * to build the MercuryVOs.
 *
 * @author Laura Felch
 */
public class MercuryOutboundService {
    // mercury status constants
    private static final String MERCURY_MANUAL = "MM";
    private static final String MERCURY_NOSEND = "MN";
    private static final String MERCURY_OPEN = "MO";
    
    private static final int LINE_LENGTH = 54;
    
    private Connection conn;
    private Logger logger;

    /**
    * Constructor
    *
    * Constructor with the database connection passed in as a parameter
    *
    * @param conn Connection
    */
    public MercuryOutboundService(Connection conn) {
        this.conn = conn;
        logger = new Logger("com.ftd.op.mercury.service.MercuryOutboundService");
    }

    /**
       * This method populate a MercuryVO
       *
       * @param vo OrderDetailVO
       * @return MercuryVO
       *
       */
    public MercuryVO buildFTDMessage(OrderDetailVO vo)
        throws Exception {
        MercuryVO mercuryVO = new MercuryVO();

        try {
            // Get Needed stuff from DAO // 
            OrderDAO orderDAO = new OrderDAO(conn);
            CustomerVO customerVO = orderDAO.getCustomer(vo.getRecipientId());

            if (customerVO == null) {
                throw new Exception("Cannot locate customer record.");
            }

            OrderBillVO orderBill = orderDAO.getOrderBill(new Long(
                        vo.getOrderDetailId()).toString());

            if (orderBill == null) {
                throw new Exception("Cannot locate billing record.");
            }

            ProductVO productVO = orderDAO.getProduct(vo.getProductId());

            if (productVO == null) {
                throw new Exception("Cannot locate product record.");
            }

            FloristVO floristVO = orderDAO.getFlorist(vo.getFloristId());

            if (floristVO == null) {
                throw new Exception("Cannot locate florist record.");
            }

            OrderVO orderVO = orderDAO.getOrder(vo.getOrderGuid());

            if (orderVO == null) {
                throw new Exception("Cannot locate order record.");
            }

            SourceVO sourceVO = orderDAO.getSource(vo.getSourceCode());

            if (sourceVO == null) {
                throw new Exception("Cannot locate source record.");
            }

            ArrayList addOnVOList = vo.getAddOnList();

            SecondChoiceVO secondChoiceVO = orderDAO.getSecondChoice(productVO.getSecondChoiceCode());
            PaymentVO payment = orderDAO.getPayment(vo);

            // Values //
            Double sameDayDeliveryFee = new Double(0);

            if ((vo.getShipMethod() != null) &&
                    (vo.getShipMethod().equalsIgnoreCase("SD"))) {
                sameDayDeliveryFee = orderDAO.getShippingKeyCosts(productVO.getShippingKey(),
                        "SD");
            }

            String allowMessageForwarding = floristVO.getAllowMessageForwardingFlag();
            // default to N if not defined
            if(allowMessageForwarding == null) allowMessageForwarding = "N";
            
            GlobalParameterVO emergencyMercTextFlagVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                    "EMERGENCY_MERC_TEXT_FLAG");
            String emergencyMercTextFlag = null;

            if (emergencyMercTextFlagVO != null) {
                emergencyMercTextFlag = emergencyMercTextFlagVO.getValue();
            } else {
                emergencyMercTextFlag = "N";
                logger.info(
                    "ORDER_PROCESSING:EMERGENCY_MERC_TEXT_FLAG global parm not set.  Defaulting to 'N'.");
            }

            GlobalParameterVO emergencyMercTextDateVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                    "EMERGENCY_MERC_TEXT_DATE");
            String emergencyMercTextDate = null;

            if (emergencyMercTextDateVO != null) {
                emergencyMercTextDate = emergencyMercTextDateVO.getValue();
            }

            GlobalParameterVO emergencyMercTextEndDateVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                    "EMERGENCY_MERC_TEXT_END_DATE");
            String emergencyMercTextEndDate = null;

            if (emergencyMercTextEndDateVO != null) {
                emergencyMercTextEndDate = emergencyMercTextEndDateVO.getValue();
            }

            GlobalParameterVO emergencyMercTextVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                    "EMERGENCY_MERC_TEXT");
            String emergencyMercText = null;

            if (emergencyMercTextVO != null) {
                emergencyMercText = emergencyMercTextVO.getValue();
            }

            logger.info("****Processing order product amount for sending to Florist****");
            String sizeIndicator = vo.getSizeIndicator();
            String lookupId = (sizeIndicator == null ? vo.getProductId() : vo.getProductId() + sizeIndicator);
            GlobalParameterVO mercPriceOverrideVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                    "MERC-PRICE-OVERRIDE-" + lookupId);
            if (mercPriceOverrideVO != null ) {
                String newPrice = mercPriceOverrideVO.getValue();
                try {
                    orderBill.setProductAmount(Double.parseDouble(newPrice));
                    logger.info("Mercury price changed to " + newPrice);
                } catch (NumberFormatException nfe) {
                    logger.error("New price is not numeric: " + newPrice + " - Using PDB price");
                }
            } else {
            	logger.info("##### Price Override is not set up. Fetching flag from FTD_APPS.ORIGINS table for origin, " + (orderVO != null ? orderVO.getOriginId() : null) + "####");
            	logger.info("##### Pdb price in order bill -" + orderBill.getPdbPrice());
            	if(orderDAO.getSendFloristPdbPriceFlag(vo.getOrderGuid()).equals("Y")){
            		logger.info("##### Price Override flag is set to 'Y'");
            		orderBill.setProductAmount(orderBill.getPdbPrice());
            		logger.info("##### Product Amount changed to new Pdb price - " + orderBill.getProductAmount());
            	}
            }
            logger.info("#### Price sent to Florist - orderBill.getProductAmount()" + orderBill.getProductAmount());

            // Set Mercury Status //
            if (floristVO.getMercuryFlag() != null) {
                if (floristVO.getMercuryFlag().equalsIgnoreCase("Y")) {
                    mercuryVO.setMercuryStatus(MERCURY_OPEN);
                } else {
                    mercuryVO.setMercuryStatus(MERCURY_MANUAL);
                }
            } else {
                mercuryVO.setMercuryStatus(MERCURY_MANUAL);
            }

            // Set Message Type = 'FTD' //
            mercuryVO.setMessageType("FTD");

            // Set Recipient //
            mercuryVO.setRecipient(customerVO.getFirstName() + " " +
                customerVO.getLastName());

            // Set Address//
            String address = new String();

            address = address.concat(customerVO.getAddress1());

            if (customerVO.getAddress2() != null) {
                //PLEASE NOTE: DO NOT CHANGE \n BELOW TO ANYTHING ELSE.  IT MESSES UP HOW FLORISTS SEE ADDRESS IN THEIR POS SYSTEMS!!!
            	//PLEASE REFER TO QE-222 FOR DETAILS.
            	address = address.concat("\n" + customerVO.getAddress2());
            }

            if (customerVO.getBusinessName() != null) {
            	//PLEASE NOTE: DO NOT CHANGE \n BELOW TO ANYTHING ELSE.  IT MESSES UP HOW FLORISTS SEE ADDRESS IN THEIR POS SYSTEMS!!!
            	//PLEASE REFER TO QE-222 FOR DETAILS.
            	address = address.concat("\n" + customerVO.getBusinessName());
            }

            mercuryVO.setAddress(address);

            // Set CityStateZip //
            mercuryVO.setCityStateZip(this.buildCityStateZipString(customerVO,
                    orderDAO));

            // Set Delivery Date// 
            mercuryVO.setDeliveryDate(vo.getDeliveryDate());

            // Set Delivery Text// 
            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd");            
            String deliveryDateText = formatter.format(vo.getDeliveryDate());
            mercuryVO.setDeliveryDateText(deliveryDateText);

            // Set First Choice //
            mercuryVO.setFirstChoice(this.buildFirstChoiceString(vo, productVO,
                    addOnVOList));

            // Set Second Choice //
            mercuryVO.setSecondChoice(this.buildSecondChoiceString(vo,
                    productVO, secondChoiceVO, addOnVOList));

            // Set Price //
            mercuryVO.setPrice(this.buildPrice(vo, customerVO, orderBill, addOnVOList));
            logger.info("#### Price sent to Florist - mercuryVO.getPrice()" + mercuryVO.getPrice());
            // Set Occasion //
            mercuryVO.setOccasion(this.buildOccasionString(vo, customerVO));

            // Set Special Instructions //
            mercuryVO.setSpecialInstructions(this.buildSpecialInstructionString(
                    vo, sourceVO, emergencyMercTextFlag, emergencyMercTextDate,
                    emergencyMercText, emergencyMercTextEndDate, customerVO, productVO, null));

            // Set Card Message //
            String cardMessage = "";

            if ((vo.getCardMessage() == null) &&
                    (vo.getCardSignature() == null)) {
                cardMessage = "NO MESSAGE ENTERED";
            } else {
                if (vo.getCardMessage() != null) {
                    cardMessage = cardMessage.concat(vo.getCardMessage()) +
                        " ";
                }

                if (vo.getCardSignature() != null) {
                    cardMessage = cardMessage.concat(vo.getCardSignature());
                }
            }

            mercuryVO.setCardMessage(breakString(cardMessage, LINE_LENGTH));

            // Set Order Date //
            mercuryVO.setOrderDate(orderVO.getOrderDate());

            /*  if we're using a proxy, replace the normally used florist
             *  with the proxy... otherwise, just use it
             */
            ProxyFlorist proxy = ProxyFlorist.getInstance();
            String proxyFloristString = null;

            if (!proxy.isProxyFlorist()) {
                // not using proxy -- just set the florist to what's in the VO
                mercuryVO.setFillingFlorist(vo.getFloristId());
            } else {
                /*  Use the proxy.  Append the florist we would have chosen to the
                 *  comments field.
                 */
                mercuryVO.setFillingFlorist(proxy.getRandomProxyFlorist());

                proxyFloristString = "***Florist " + vo.getFloristId() + " replaced by proxy***";
                mercuryVO.setComments(proxyFloristString);
                mercuryVO.setSpecialInstructions(breakString(mercuryVO.getSpecialInstructions() +
                    " " + proxyFloristString, LINE_LENGTH));
            }

            // The Operator is null until the MercuryDAO//
            //  Set Priority //
            mercuryVO.setPriority(this.buildPriorityString(floristVO,
                    allowMessageForwarding, customerVO));

            //  Set Product Id //
            mercuryVO.setProductId(vo.getProductId());

            // Set Zip Code //
            mercuryVO.setZipCode(customerVO.getZipCode());

            // Set Phone Number //
            ArrayList phoneList = (ArrayList) customerVO.getCustomerPhoneVOList();
            CustomerPhoneVO phoneVO;

            for (int i = 0; i < phoneList.size(); i++) {
                phoneVO = (CustomerPhoneVO) phoneList.get(i);

                if (phoneVO.getPhoneType().equalsIgnoreCase("DAY") &&
                        (phoneVO.getPhoneNumber() != null)) {
                    if (customerVO.getCountry().equalsIgnoreCase("US") ||
                            customerVO.getCountry().equalsIgnoreCase("USA") ||
                            customerVO.getCountry().equalsIgnoreCase("CA")) {
                        // format phone number for us and canada
                        String number = phoneVO.getPhoneNumber();
                        mercuryVO.setPhoneNumber(number.substring(0, 3) + "/" +
                            number.substring(3, 6) + "-" +
                            number.substring(6, 10));
                    } else {
                        mercuryVO.setPhoneNumber(phoneVO.getPhoneNumber());
                    }

                    break;
                } else if (phoneVO.getPhoneType().equalsIgnoreCase("EVENING") &&
                        (phoneVO.getPhoneNumber() != null)) {
                    if (customerVO.getCountry().equalsIgnoreCase("US") ||
                            customerVO.getCountry().equalsIgnoreCase("USA") ||
                            customerVO.getCountry().equalsIgnoreCase("CA")) {
                        // format phone number for us and canada     
                        String number = phoneVO.getPhoneNumber();
                        mercuryVO.setPhoneNumber(number.substring(0, 3) + "/" +
                            number.substring(3, 6) + "-" +
                            number.substring(6, 10));
                    } else {
                        mercuryVO.setPhoneNumber(phoneVO.getPhoneNumber());
                    }

                    // no break here ... use DAY phone if we have it
                }
            }

            // if we couldn't find a phone number for the recipient, default to all zeros.
            if (mercuryVO.getPhoneNumber() == null) {
                mercuryVO.setPhoneNumber("000/000-0000");
            }

            // Set Reference Number //
            mercuryVO.setReferenceNumber(new Long(vo.getOrderDetailId()).toString());

            // Set Message Direction //
            mercuryVO.setDirection("OUTBOUND");

            // Set Sending Florist //
            String assignedSendingFlorist = orderDAO.retrieveDefaultFloristId(new Long(vo.getOrderDetailId()).toString());
            
            
            // Set Sort Value
            String state = customerVO.getState();
            mercuryVO.setSortValue(buildSortValue(vo.getDeliveryDate(),
                    orderVO.getOrderDate(), state));
            mercuryVO.setTransmissionDate(buildTransmissionTime());

            if (payment.getPaymentType().equalsIgnoreCase("NC")) {
                mercuryVO.setCompOrder("C");
            }

            if (sourceVO.getRequiresDeliveryConfirmation().equalsIgnoreCase("Y")) {
                mercuryVO.setRequireConfirmation("Y");
            } else {
                mercuryVO.setRequireConfirmation("N");
            }
            
           
			String assignOutboundId = (orderDAO.getGlobalParameter("MERCURY_INTERFACE_CONFIG", "ASSIGN_OUTBOUND_ID")).getValue();
			if (assignOutboundId != null && assignOutboundId.equalsIgnoreCase("Y")) {
				setSendingFloristWithOutbound(orderVO.getCompanyId(), assignedSendingFlorist, mercuryVO); 
			}

            // If there was an FTD message in the VO - use it since it's more recent.  
            // We currently save the FTD during REJ handling - both florist and mercury (MR) rejects. 
            // Note we don't take delivery date from FTD since VO should have latest.
            if (vo.getCurrentFtd() != null) {
                MercuryVO curFtd = vo.getCurrentFtd();
                logger.info("Re-using FTD (Mercury Order Number: " + curFtd.getMercuryOrderNumber() + 
                             ") for new FTD on order: " + vo.getExternalOrderNumber());
                if (curFtd.getRecipient() != null) {
                    mercuryVO.setRecipient(curFtd.getRecipient());
                }
                if (curFtd.getAddress() != null && curFtd.getCityStateZip() != null) {
                    mercuryVO.setAddress(curFtd.getAddress());
                    mercuryVO.setCityStateZip(curFtd.getCityStateZip());
                }
                if (curFtd.getPhoneNumber() != null) {
                    mercuryVO.setPhoneNumber(curFtd.getPhoneNumber());
                }
                if (curFtd.getOccasion() != null) {
                    mercuryVO.setOccasion(curFtd.getOccasion());
                }
                if (curFtd.getCardMessage() != null) {
                    mercuryVO.setCardMessage(curFtd.getCardMessage());
                }
                if (curFtd.getSpecialInstructions() != null) {
                    if (proxyFloristString == null) {
                        mercuryVO.setSpecialInstructions(curFtd.getSpecialInstructions());
                    } else {
                        mercuryVO.setSpecialInstructions(breakString(curFtd.getSpecialInstructions() +
                            " " + proxyFloristString, LINE_LENGTH));
                    }
                }
            }

            // If a date range, check for florist blocks
            // Don't check if the MercuryVO delivery date has been manually changed to a
            //         new date that is outside of the date range
            if (vo.getDeliveryDateRangeEnd() != null &&
            		!mercuryVO.getDeliveryDate().before(vo.getDeliveryDate()) &&
            		!mercuryVO.getDeliveryDate().after(vo.getDeliveryDateRangeEnd())) {
            	logger.info("Checking florist blocks for date range");
                SimpleDateFormat sdfRange = new SimpleDateFormat("MM/dd/yyyy");
            	Calendar start = Calendar.getInstance();
            	start.setTime(vo.getDeliveryDate());
            	Calendar end = Calendar.getInstance();
            	end.setTime(vo.getDeliveryDateRangeEnd());
            	for (; !start.after(end); start.add(Calendar.DATE, 1)) {
            		Date current = start.getTime();
            		logger.info(vo.getFloristId() + " " + sdfRange.format(current));
            		if (!orderDAO.isFloristBlocked(vo.getFloristId(), current)) {
            			if (current.compareTo(mercuryVO.getDeliveryDate()) == 0) {
            				// Don't change if the selected date is not blocked
            				logger.info("Date is the same, not changing");
            			} else {
                    		logger.info("New Delivery Date " + sdfRange.format(current));
                            mercuryVO.setDeliveryDate(current);
                            formatter = new SimpleDateFormat("MMM dd");
                            deliveryDateText = formatter.format(current);
                            mercuryVO.setDeliveryDateText(deliveryDateText);
            			}
        		    	break;
            		}
            	}
            }
            
            try {
        	    // SP-85 : New FTD for FEMOE should be retrieved from Old FTD.
        	    boolean isFEMOE = orderDAO.isFEMOE(String.valueOf(vo.getOrderDetailId()));
        	
        	    if (isFEMOE) {
        	    	logger.info("FEMOE order");
        	    	MercuryVO lastFTD = orderDAO.getLastFTD(String.valueOf(vo.getOrderDetailId()));
        	    	if (lastFTD != null && lastFTD.getOperator() != null && lastFTD.getOperator().contains("FEMOE")) {
        	    		mercuryVO.setFirstChoice(lastFTD.getFirstChoice());
        	    		mercuryVO.setSecondChoice(lastFTD.getSecondChoice());
        	    		mercuryVO.setCardMessage(lastFTD.getCardMessage());
        	    		mercuryVO.setOccasion(lastFTD.getOccasion());
        	    		if (mercPriceOverrideVO != null ) {
        	    			String newPrice = mercPriceOverrideVO.getValue();
        	    			try {
        	    				mercuryVO.setPrice(Double.parseDouble(newPrice));
        	    				logger.info("Mercury price changed to " + newPrice);
        	    			} catch (NumberFormatException nfe) {
        	    				logger.error("New price is not numeric: " + newPrice + " - Using PDB price");
        	    			}
        	    		}
        	    		else {
        	    			mercuryVO.setPrice(lastFTD.getPrice());
        	    		}
        	    	}
        	    }
            }
            catch (Exception e) {
            	logger.error("Unable to check whether the order is FEMOE or not for orderDetailId: " + vo.getOrderDetailId(), e);
            }

        } catch (Exception ex) {
            logger.error(ex);
            throw (ex);
        }

        return mercuryVO;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to FTDMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildFTDMessage(FTDMessageTO to) throws Exception {
        MercuryVO vo = new MercuryVO();
        vo.setMessageType(to.getMessageType());
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setOperator(to.getOperator());
        vo.setDirection(to.getDirection());
        vo.setSendingFlorist(to.getSendingFlorist());
        OrderDAO dao = new OrderDAO(conn);
        
        OrderDetailVO orderDetailVO = dao.getOrderDetail(to.getOrderDetailId());
        OrderVO orderVO = dao.getOrder(orderDetailVO.getOrderGuid());
        CustomerVO recipient = dao.getCustomer(orderDetailVO.getRecipientId());
        
        String assignOutboundId = (dao.getGlobalParameter("MERCURY_INTERFACE_CONFIG", "ASSIGN_OUTBOUND_ID")).getValue();
		if (assignOutboundId != null && assignOutboundId.equalsIgnoreCase("Y")) {
			setSendingFloristWithOutbound(orderVO.getCompanyId(), to.getSendingFlorist(), vo); 
		}
        
        FloristVO florist = dao.getFlorist(to.getFillingFlorist());

        // priority calculation
        GlobalParameterVO allowMessageForwardingVO = dao.getGlobalParameter("ORDER_PROCESSING",
                "ALLOW_MESSAGE_FORWARDING");
        String allowMessageForwarding = "N";

        if (allowMessageForwardingVO != null) {
            allowMessageForwarding = allowMessageForwardingVO.getValue();
        }

        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
            vo.setSpecialInstructions(breakString(to.getSpecialInstructions(), LINE_LENGTH));
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field and the special instructions field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());

            String floristProxyString = new String("***Florist " +
                    to.getFillingFlorist() + " replaced by proxy***");
            vo.setComments(floristProxyString);
            vo.setSpecialInstructions(breakString(floristProxyString + " " +
                to.getSpecialInstructions(), LINE_LENGTH));
        }

        vo.setOrderDate(to.getOrderDate());
        vo.setRecipient(to.getRecipient());
        vo.setAddress(to.getAddress());
        vo.setCityStateZip(to.getCityStateZip());
        vo.setPhoneNumber(to.getPhoneNumber());
        vo.setDeliveryDate(to.getDeliveryDate());
        vo.setDeliveryDateText(to.getDeliveryDateText());
        vo.setFirstChoice(to.getFirstChoice());
        vo.setSecondChoice(to.getSecondChoice());
        vo.setPrice(to.getPrice());
        vo.setOldPrice(to.getOldPrice());

        vo.setCardMessage(breakString(to.getCardMessage(), LINE_LENGTH));
        vo.setOccasion(to.getOccasion());

        vo.setProductId(to.getProductId());
        vo.setZipCode(to.getZipCode());
        vo.setRequireConfirmation(to.isRequestConfirmation());
        vo.setReferenceNumber(to.getOrderDetailId());
        vo.setCompOrder(to.isIsCompOrder());
        vo.setSortValue(buildSortValue(to.getDeliveryDate(), to.getOrderDate(),
                parseStateFromCSZ(to.getCityStateZip())));
        vo.setTransmissionDate(buildTransmissionTime());

       
        vo.setPriority(this.buildPriorityString(florist,
                allowMessageForwarding, recipient));

        return vo;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to FTDMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildADJMessage(ADJMessageTO to) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO originalVo = dao.getMessage(to.getMercuryId());

        if (originalVo == null) {
            throw new Exception("Cannot find Mercury Message.");
        }

        MercuryVO vo = new MercuryVO();
        vo.setMercuryOrderNumber(originalVo.getMercuryMessageNumber());
        vo.setMessageType(to.getMessageType());
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setOutboundId(originalVo.getOutboundId());
        vo.setSendingFlorist(to.getSendingFlorist());
        vo.setComments(breakString(to.getComments(), LINE_LENGTH));

        // ROF number is always the first set of digits from the FTD's MM number
        // (A0367Z-3354 => ROF number of A0367Z)
        String[] rof = originalVo.getMercuryMessageNumber().split("-");
        vo.setRofNumber(rof[0]);

        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());
            vo.setComments(breakString(vo.getComments() + "  Florist " +
                to.getFillingFlorist() + " replaced by proxy", LINE_LENGTH));
        }

        vo.setOrderDate(originalVo.getOrderDate());
        String recipName = originalVo.getRecipient();
        int startPos = recipName.indexOf(' ');
        int endPos = 0;
        if (startPos == -1)
        {
          startPos = 0;
          endPos = 3;
          if (endPos > recipName.length()) endPos = recipName.length();
        } else 
        {
          startPos = startPos + 1;
          endPos = startPos + 3;
          if (endPos > recipName.length()) endPos = recipName.length();
        }
        vo.setRecipient(recipName.substring(startPos,endPos).toUpperCase());
        vo.setAddress(originalVo.getAddress());
        vo.setCityStateZip(originalVo.getCityStateZip());
        vo.setPhoneNumber(originalVo.getPhoneNumber());
        vo.setDeliveryDate(originalVo.getDeliveryDate());

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        vo.setDeliveryDateText(formatter.format(originalVo.getDeliveryDate())
                                        .toUpperCase());

        vo.setFirstChoice(originalVo.getFirstChoice());
        vo.setSecondChoice(originalVo.getSecondChoice());
//        vo.setOldPrice(to.getOldPrice());
        vo.setPrice(to.getOldPrice());
        vo.setOverUnderCharge(to.getOverUnderCharge());
        vo.setCardMessage(breakString(originalVo.getCardMessage(), LINE_LENGTH));
        vo.setOccasion(originalVo.getOccasion());
        vo.setSpecialInstructions(breakString(originalVo.getSpecialInstructions(), LINE_LENGTH));
        vo.setPriority(originalVo.getPriority());
        vo.setOperator(to.getOperator());
        vo.setReferenceNumber(originalVo.getReferenceNumber());
        vo.setProductId(originalVo.getProductId());
        vo.setZipCode(originalVo.getZipCode());
        vo.setCombinedReportNumber(to.getCombinedReportNumber());
        vo.setAdjReasonCode(to.getAdjustmentReasonCode());
        vo.setDirection(to.getDirection());

        String state = parseStateFromCSZ(originalVo.getCityStateZip());
        vo.setSortValue(buildSortValue(originalVo.getDeliveryDate(),
                originalVo.getOrderDate(), state));
        vo.setTransmissionDate(buildTransmissionTime());

        return vo;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to ANSMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildANSMessage(ANSMessageTO to) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO originalVo = dao.getMessage(to.getMercuryId());
        OrderDAO odao = new OrderDAO(conn);

        if (originalVo == null) {
            throw new Exception("Cannot find given Mercury Message.");
        }

        MercuryVO vo = new MercuryVO();
        vo.setMercuryOrderNumber(originalVo.getMercuryMessageNumber());
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setMessageType(to.getMessageType());
        vo.setComments(breakString(to.getComments(), LINE_LENGTH));
        vo.setSendingFlorist(to.getSendingFlorist());

        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());
            vo.setComments(breakString(vo.getComments() + "  Florist " +
                to.getFillingFlorist() + " replaced by proxy", LINE_LENGTH));
        }

        vo.setOrderDate(originalVo.getOrderDate());
        vo.setRecipient(originalVo.getRecipient());
        vo.setAddress(originalVo.getAddress());
        vo.setCityStateZip(originalVo.getCityStateZip());
        vo.setPhoneNumber(originalVo.getPhoneNumber());

        // Delivery date is now taken from order_details (instead of associated Mercury Message)
        Date ordDeliveryDate = FTDCommonUtils.getOrderDeliveryDate(conn, originalVo.getReferenceNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        if (ordDeliveryDate != null) {
            vo.setDeliveryDate(ordDeliveryDate);            
            vo.setDeliveryDateText(formatter.format(ordDeliveryDate));
        } else {
            vo.setDeliveryDate(originalVo.getDeliveryDate());
            vo.setDeliveryDateText(formatter.format(originalVo.getDeliveryDate()));
        }

        vo.setFirstChoice(originalVo.getFirstChoice());
        vo.setSecondChoice(originalVo.getSecondChoice());
        vo.setOldPrice(to.getOldPrice());
        vo.setPrice(to.getPrice());
        vo.setCardMessage(breakString(originalVo.getCardMessage(), LINE_LENGTH));
        vo.setOccasion(originalVo.getOccasion());
        vo.setSpecialInstructions(breakString(originalVo.getSpecialInstructions(), LINE_LENGTH));
        vo.setPriority(originalVo.getPriority());
        vo.setOperator(to.getOperator());

        vo.setReferenceNumber(originalVo.getReferenceNumber());
        vo.setProductId(originalVo.getProductId());
        vo.setZipCode(originalVo.getZipCode());
        vo.setAskAnswerCode(to.getAskAnswerCode());
        vo.setDirection(to.getDirection());

        String state = parseStateFromCSZ(originalVo.getCityStateZip());
        vo.setSortValue(buildSortValue(originalVo.getDeliveryDate(),
                originalVo.getOrderDate(), state));
        vo.setTransmissionDate(buildTransmissionTime());
        vo.setApproval_identity_id(to.getApproval_identity_id());

        return vo;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to ASKMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildASKMessage(ASKMessageTO to) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO originalVo = dao.getMessage(to.getMercuryId());

        if (originalVo == null) {
            throw new Exception("Cannot find given Mercury Message.");
        }

        MercuryVO vo = new MercuryVO();
        vo.setMercuryOrderNumber(originalVo.getMercuryMessageNumber());
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setMessageType(to.getMessageType());
        vo.setOutboundId(originalVo.getOutboundId());
        vo.setSendingFlorist(originalVo.getSendingFlorist());
        vo.setComments(breakString(to.getComments(), LINE_LENGTH));

        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());
            vo.setComments(breakString(vo.getComments() + "  Florist " +
                to.getFillingFlorist() + " replaced by proxy", LINE_LENGTH));
        }

        vo.setOrderDate(originalVo.getOrderDate());
        vo.setRecipient(originalVo.getRecipient());
        vo.setAddress(originalVo.getAddress());
        vo.setCityStateZip(originalVo.getCityStateZip());
        vo.setPhoneNumber(originalVo.getPhoneNumber());

        // Delivery date is now taken from order_details (instead of associated Mercury Message)
        Date ordDeliveryDate = FTDCommonUtils.getOrderDeliveryDate(conn, originalVo.getReferenceNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        if (ordDeliveryDate != null) {
            vo.setDeliveryDate(ordDeliveryDate);            
            vo.setDeliveryDateText(formatter.format(ordDeliveryDate));
        } else {
            vo.setDeliveryDate(originalVo.getDeliveryDate());
            vo.setDeliveryDateText(formatter.format(originalVo.getDeliveryDate()));
        }

        vo.setFirstChoice(originalVo.getFirstChoice());
        vo.setSecondChoice(originalVo.getSecondChoice());
        vo.setOldPrice(to.getOldPrice());
        vo.setPrice(to.getPrice());
        vo.setCardMessage(breakString(originalVo.getCardMessage(), LINE_LENGTH));
        vo.setOccasion(originalVo.getOccasion());
        vo.setSpecialInstructions(breakString(originalVo.getSpecialInstructions(), LINE_LENGTH));
        vo.setPriority(originalVo.getPriority());
        vo.setOperator(to.getOperator());

        vo.setReferenceNumber(originalVo.getReferenceNumber());
        vo.setProductId(originalVo.getProductId());
        vo.setZipCode(originalVo.getZipCode());
        vo.setAskAnswerCode(to.getAskAnswerCode());
        vo.setDirection(to.getDirection());

        String state = parseStateFromCSZ(originalVo.getCityStateZip());
        vo.setSortValue(buildSortValue(originalVo.getDeliveryDate(),
                originalVo.getOrderDate(), state));
        vo.setTransmissionDate(buildTransmissionTime());
        vo.setApproval_identity_id(to.getApproval_identity_id());

        return vo;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to CANMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildCANMessage(CANMessageTO to) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO originalVo = dao.getMessage(to.getMercuryId());

        if (originalVo == null) {
            throw new Exception("Cannot find given Mercury Message.");
        }

        MercuryVO vo = new MercuryVO();
        vo.setMercuryOrderNumber(originalVo.getMercuryMessageNumber());
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setMessageType(to.getMessageType());
        vo.setOutboundId(originalVo.getOutboundId());
        vo.setSendingFlorist(to.getSendingFlorist());
        vo.setComments(breakString(to.getComments(), LINE_LENGTH));
        
        if (to.getComplaintCommOriginTypeId() == null)
        {
            to.setComplaintCommOriginTypeId(vo.complaintCommTypeIdNotAComplaint);
        }
        if (to.getComplaintCommNotificationTypeId() == null)
        {
            to.setComplaintCommNotificationTypeId(vo.complaintCommTypeIdNotAComplaint);
        }
        
        vo.setComplaintCommOriginTypeId(to.getComplaintCommOriginTypeId());
        vo.setComplaintCommNotificationTypeId(to.getComplaintCommNotificationTypeId());


        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());
            vo.setComments(breakString(vo.getComments() + "  Florist " +
                to.getFillingFlorist() + " replaced by proxy", LINE_LENGTH));
        }

        vo.setOrderDate(originalVo.getOrderDate());
        vo.setRecipient(originalVo.getRecipient());
        vo.setAddress(originalVo.getAddress());
        vo.setCityStateZip(originalVo.getCityStateZip());
        vo.setPhoneNumber(originalVo.getPhoneNumber());

        // Delivery date is now taken from order_details (instead of associated Mercury Message)
        Date ordDeliveryDate = FTDCommonUtils.getOrderDeliveryDate(conn, originalVo.getReferenceNumber());
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        if (ordDeliveryDate != null) {
            vo.setDeliveryDate(ordDeliveryDate);            
            vo.setDeliveryDateText(formatter.format(ordDeliveryDate));
        } else {
            vo.setDeliveryDate(originalVo.getDeliveryDate());
            vo.setDeliveryDateText(formatter.format(originalVo.getDeliveryDate()));
        }
        
        vo.setFirstChoice(originalVo.getFirstChoice());
        vo.setSecondChoice(originalVo.getSecondChoice());
        vo.setOldPrice(originalVo.getOldPrice());
        vo.setPrice(new Double("0")); // cancels always have a price of 0
        vo.setCardMessage(breakString(originalVo.getCardMessage(), LINE_LENGTH));
        vo.setOccasion(originalVo.getOccasion());
        vo.setSpecialInstructions(breakString(originalVo.getSpecialInstructions(), LINE_LENGTH));
        vo.setPriority(originalVo.getPriority());
        vo.setOperator(to.getOperator());
        vo.setReferenceNumber(originalVo.getReferenceNumber());
        vo.setProductId(originalVo.getProductId());
        vo.setZipCode(originalVo.getZipCode());
        vo.setDirection(to.getDirection());

        String state = parseStateFromCSZ(originalVo.getCityStateZip());
        vo.setSortValue(buildSortValue(originalVo.getDeliveryDate(),
                originalVo.getOrderDate(), state));
        vo.setTransmissionDate(buildTransmissionTime());

        return vo;
    }

    /**
       * This method populate a MercuryVO
       *
       * @param to GENMessageTO
       * @return MercuryVO
       */
    public MercuryVO buildGENMessage(GENMessageTO to) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        MercuryVO vo = new MercuryVO();
        vo.setMercuryStatus(to.getMercuryStatus());
        vo.setMessageType(to.getMessageType());
        vo.setSendingFlorist(to.getSendingFlorist());
        vo.setComments(breakString(to.getComments(), LINE_LENGTH));

        /*  if we're using a proxy, replace the normally used florist
         *  with the proxy... otherwise, just use it
         */
        ProxyFlorist proxy = ProxyFlorist.getInstance();

        if (!proxy.isProxyFlorist()) {
            // not using proxy -- just set the florist to what's in the VO
            vo.setFillingFlorist(to.getFillingFlorist());
        } else {
            /*  Use the proxy.  Append the florist we would have chosen to the
             *  comments field.
             */
            vo.setFillingFlorist(proxy.getRandomProxyFlorist());
            vo.setComments(breakString(vo.getComments() + "  Florist " +
                to.getFillingFlorist() + " replaced by proxy", LINE_LENGTH));
        }

        vo.setDeliveryDate(to.getDeliveryDate());
        vo.setPriority(to.getPriority());
        vo.setOperator(to.getOperator());

        vo.setDirection(to.getDirection());
        vo.setRetrievalFlag(to.getRetrievalFlag());
        vo.setTransmissionDate(this.buildTransmissionTime());

        return vo;
    }

    /**
       * This method inserts a MercuryVO into the Mercury table using the
       * SP_INSERT_MERCURY produre in the insertMessage() method in the MercuryDAO
       *
       * @param vo MercuryVO
       */
    public String sendMercuryMessage(MercuryVO vo) throws Exception {
        String output = null;

        if (vo.getMercuryStatus().equalsIgnoreCase(MERCURY_MANUAL)) {
            logger.info("sending manual message");
            QueueService service = new QueueService(conn);
            MercuryDAO dao = new MercuryDAO(conn);
            output = dao.insertMessage(vo);
            logger.info("message inserted");

            vo.setMercuryId(output);
            service.sendMercuryMessageToQueue(vo);
            logger.info("message sent to queue");
        } else if (vo.getMercuryStatus().equalsIgnoreCase(MERCURY_OPEN) ||
                vo.getMercuryStatus().equalsIgnoreCase(MERCURY_NOSEND)) {
            logger.info("sending mercury message");
            MercuryDAO dao = new MercuryDAO(conn);
            output = dao.insertMessage(vo);
            logger.info("MercuryOutboundService - message inserted");
        }

        return output;
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
     * It builds the cityStateZip string.
     *
     * @param vo CustomerVO
     * **/
    private String buildCityStateZipString(CustomerVO customerVO,
        OrderDAO orderDAO) throws Exception {
        String cityStateZip = "";
        String country = customerVO.getCountry();
        String city = customerVO.getCity();

        String state = customerVO.getState();
        String zipCode = customerVO.getZipCode();

        if (country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA") ||
                country.equalsIgnoreCase("")) {
            cityStateZip = city + ", " + state + " " + zipCode;
        } else if (country.equalsIgnoreCase("CA")) {
            cityStateZip = city + ", " + state + " " + zipCode;
        } else {
            cityStateZip = city + ", ";

            if (state == null) {
                cityStateZip = cityStateZip.concat("NA ");
            } else {
                cityStateZip = cityStateZip.concat(state + " ");
            }

            if ((zipCode != null) && (!zipCode.equalsIgnoreCase("99999"))) {
                cityStateZip = cityStateZip.concat(zipCode);
            }

            String fullCountryName = orderDAO.getActiveCountryName(country);

            if (fullCountryName != null) {
                cityStateZip = cityStateZip.concat(" " + fullCountryName);
            }
        }

        return cityStateZip;
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
    * It builds the first choice string.
    *
    * @param OrderDetailVO orderDetailVO
    * @param ProductVO productVO
    * @param List addOnVOList
    * @return String firstChoiceString
    * **/
    public String buildFirstChoiceString(OrderDetailVO orderDetailVO,
        ProductVO productVO, List addOnVOList) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);

        OrderDAO orderDAO = new OrderDAO(conn);
        GlobalParameterVO floristReferenceSuffixVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                "FLORIST_REFERENCE_NUMBER_SUFFIX_DELUXE");
        String deluxeSuffix = "";
        if (floristReferenceSuffixVO != null) {
            deluxeSuffix = floristReferenceSuffixVO.getValue();
        }
        floristReferenceSuffixVO = orderDAO.getGlobalParameter("ORDER_PROCESSING",
                "FLORIST_REFERENCE_NUMBER_SUFFIX_PREMIUM");
        String premiumSuffix = "";
        if (floristReferenceSuffixVO != null) {
            premiumSuffix = floristReferenceSuffixVO.getValue();
        }

        String firstChoiceString = productVO.getFloristReferenceNumber();

        if (orderDetailVO.getColor1() != null) {
            firstChoiceString = firstChoiceString + " " + dao.getColorById(orderDetailVO.getColor1());
        }

        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        if (orderDetailVO.getSizeIndicator() != null && productVO.getDeliveryType().equals("D")) {
            if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A")) { //Standard size
                firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) { // Deluxe size //
                if (productVO.getSendDeluxeRecipe() != null && productVO.getSendDeluxeRecipe().equalsIgnoreCase("Y")) {
                    firstChoiceString = firstChoiceString + deluxeSuffix;
                    firstChoiceString = firstChoiceString + " " + productVO.getProductName() + " Deluxe\n";
                    firstChoiceString = firstChoiceString + "See Special Instructions for Deluxe recipe\n";
                } else {
                    firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
                }
                firstChoiceString = firstChoiceString + "Include $" +
                    format.format(productVO.getDeluxePrice() - productVO.getStandardPrice()) +
                    " for DELUXE for 1st and 2nd choice\n";
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) { // Premium size //
                if (productVO.getSendPremiumRecipe() != null && productVO.getSendPremiumRecipe().equalsIgnoreCase("Y")) {
                    firstChoiceString = firstChoiceString + premiumSuffix;
                    firstChoiceString = firstChoiceString + " " + productVO.getProductName() + " Premium\n";
                    firstChoiceString = firstChoiceString + "See Special Instructions for Premium recipe\n";
                } else {
                    firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
                }
                firstChoiceString = firstChoiceString + "Include $" +
                    format.format(productVO.getPremiumPrice() - productVO.getStandardPrice()) +
                    " for PREMIUM for 1st and 2nd choice\n";
            }
        } else {
            firstChoiceString = firstChoiceString + " " + productVO.getMercuryDescription() + "\n";
        }

        // add comments field in product_master
        if (productVO.getItemComments() != null) {
            firstChoiceString = firstChoiceString +
                productVO.getItemComments() + "\n";
        }

        // Need to Add Add-Ons//
        for (int i = 0; i < addOnVOList.size(); i++) {
            AddOnVO addOn = (AddOnVO) addOnVOList.get(i);
            
            // modification for Ren cards
            String description = null;
            if(addOn.getAddOnCode().startsWith("RC")) 
            {
              description = addOn.getAddOnCode() + " - " + addOn.getDesciption();
            } else 
            {
              description = addOn.getDesciption();
            }
            
            firstChoiceString = firstChoiceString + "PRICE INCLUDES " +
                addOn.getAddOnQuantity() + " " + description + "($" +
                format.format(addOn.getAddOnQuantity() * addOn.getPrice()
                                                              .doubleValue()) +
                ")" + (((addOnVOList.size() - 1) == i) ? "" : "\n");           
       }

        return firstChoiceString;
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
    * It builds the second choice string.
    *
    * @param OrderDetailVO orderDetailVO
    * @param ProductVO productVO
    * @param SecondChoiceVO secondChoiceVO
    * @return String secondChoiceString
    * **/
    public String buildSecondChoiceString(OrderDetailVO orderDetailVO,
        ProductVO productVO, SecondChoiceVO secondChoiceVO, List addOnVOList)
        throws Exception { 
        MercuryDAO dao = new MercuryDAO(conn);

        String secondChoiceString = "";
        String substitutionIndicator = orderDetailVO.getSubstitutionIndicator();

        // if there are no substitutions, return "NONE"
        if ((substitutionIndicator != null) &&
                (substitutionIndicator.equalsIgnoreCase("N") &&
                (orderDetailVO.getColor2() == null))) {
            secondChoiceString = "NONE. PLEASE FILL BY ITEM# TO FULL VALUE";
        } else {
            /*
             * if there is no second choice defined on the order and
             * the second choice on the product is either undefined,
             * or it's defined as None, but allows second choice color
             * to be found, then use the second choice color if it
             * exists on the order
             */
            if ((orderDetailVO.getSecondChoiceProduct() == null) &&
                    ((secondChoiceVO.getProductSecondChoiceId() == null) ||
                    (secondChoiceVO.getProductSecondChoiceId().equals("0") &&
                    productVO.getColorSizeFlag().equalsIgnoreCase("C")))) {
                if (orderDetailVO.getColor2() == null) {
                    secondChoiceString = "NONE. PLEASE FILL BY ITEM# TO FULL VALUE";
                } else {
                    secondChoiceString = productVO.getFloristReferenceNumber() +
                        " " + dao.getColorById(orderDetailVO.getColor2()) +
                        "-" + productVO.getMercuryDescription() + "\n";
                }
            } else {
                secondChoiceString = secondChoiceVO.getMercuryDescription1() +
                    " " +
                    ((secondChoiceVO.getMercuryDescription2() == null) ? ""
                                                                       : secondChoiceVO.getMercuryDescription2()) +
                    "\n";
            }
        }

        // add product comments
        if (productVO.getItemComments() != null) {
            secondChoiceString += (productVO.getItemComments() + "\n");
        }

        // add addons
        NumberFormat format = NumberFormat.getInstance();
        format.setMinimumFractionDigits(2);

        for (int i = 0; i < addOnVOList.size(); i++) {
            AddOnVO addOn = (AddOnVO) addOnVOList.get(i);

            if (addOn.getAddOnCode().startsWith("RC")) {
                secondChoiceString += ("SIMILAR GREETING CARD APPROPRIATE FOR THE OCCASION" +
                (((addOnVOList.size() - 1) == i) ? "" : "\n"));
            } else {
                secondChoiceString = secondChoiceString + "PRICE INCLUDES " +
                    addOn.getAddOnQuantity() + " " + addOn.getDesciption() +
                    "($" +
                    format.format(addOn.getAddOnQuantity() * addOn.getPrice()
                                                                  .doubleValue()) +
                    ")" + (((addOnVOList.size() - 1) == i) ? "" : "\n");
            }
        }

        return secondChoiceString; 
    }

    /**
     * 
     * Return the price that includes Product, Addons and Shipping fee
     * 
     * @param orderDetailVO
     * @param customer
     * @param bill
     * @param addOnVOList
     * @return
     * @throws Exception
     */
    public Double buildPrice(OrderDetailVO orderDetailVO, CustomerVO customer,
        OrderBillVO bill, 
        List<AddOnVO> addOnVOList) throws Exception {
        double price = 0;
        double addOnPrice = 0;
        price = bill.getProductAmount() + bill.getShippingFee();
        
        if (addOnVOList != null && !addOnVOList.isEmpty()) {
        	for (AddOnVO addOnVO : addOnVOList) {
        		if (addOnVO != null && addOnVO.getPrice() != null) {
        			addOnPrice = addOnPrice + (addOnVO.getPrice().doubleValue() * addOnVO.getAddOnQuantity());
        		}
        	}
        }
        
        price = price + addOnPrice;

        OrderDAO orderDao = new OrderDAO(conn);
        OrderVO orderVO = orderDao.getOrder(orderDetailVO.getOrderGuid());
        String companyId = orderVO.getCompanyId();
        // if recipient is in Canada and company is not equal to FTDCA, then ADD in CANADA GST tax //
        if (customer.getCountry().equalsIgnoreCase("CA") && companyId != null 
        		&& !companyId.equals("") && !companyId.equalsIgnoreCase("FTDCA") ) {
            price = price * (1 + (getGST().doubleValue() * 0.01));
        }

        return new Double(price);
    }

    public Double getGST() throws Exception {
        OrderDAO orderDAO = new OrderDAO(conn);
    	String gst;
	GlobalParameterVO gstVO = 
		orderDAO.getGlobalParameter("ORDER_PROCESSING",
                                            "CANADA_GST_TAX");
	return new Double(gstVO.getValue());
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
    * It builds the occasion string.
    *
    * @param OrderDetailVO orderDetailVO
    * @param CustomerVO customerVO
    * @param OrderDAO orderDAO
    * @return String occasionString
    * **/
    private String buildOccasionString(OrderDetailVO orderDetailVO,
        CustomerVO customerVO) throws Exception {
        MercuryDAO dao = new MercuryDAO(conn);
        String occasionString = "";

        if ((orderDetailVO.getOccasion() == null) ||
                orderDetailVO.getOccasion().equals("8") ||
                (new Integer(orderDetailVO.getOccasion()).intValue() <= 0)) {
            if ((customerVO.getAddressType() != null) &&
                    (customerVO.getAddressType().trim().equalsIgnoreCase("FUNERAL HOME") ||
                     customerVO.getAddressType().trim().equalsIgnoreCase("CEMETERY")
                    )) {
                occasionString = new String("1"); // FUNERAL/SYMPATHY
            } else {
                occasionString = new String("8"); // OTHER
            }
        } else {
            int occasionInt = new Integer(orderDetailVO.getOccasion()).intValue();

            if (occasionInt > 13) {
                occasionString = new String("5"); // HOLIDAY
            } else if ((occasionInt <= 13) && (occasionInt >= 8)) {
                occasionString = new String("8"); // OTHER
            } else if (occasionInt < 8) {
                occasionString = new Integer(occasionInt).toString(); // HOLIDAY
            }
        }

        return dao.getBoxXOccasion(occasionString);
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
    * It builds the priority string.
    *
    * @param FloristVO floristVO
    * @param String allowMessageForwarding
    * @return String priorityString
    * **/
    private String buildPriorityString(FloristVO floristVO,
        String allowMessageForwarding, CustomerVO recipient) {
        String priorityString = "";

        if (recipient.getCountry().equalsIgnoreCase("US") ||
                recipient.getCountry().equalsIgnoreCase("USA") ||
                recipient.getCountry().equalsIgnoreCase("CA")) {
            if (floristVO.getIsFTOFlag().equalsIgnoreCase("Y")) {
                priorityString = "FTO";
            } else {
                priorityString = "X";
            }

            if (allowMessageForwarding.equalsIgnoreCase("Y")) {
                if (floristVO.getAllowMessageForwardingFlag().equalsIgnoreCase("Y")) {
                    //it's a goto (super) florist
                    if ((floristVO.getSuperFloristFlag() != null) &&
                            floristVO.getSuperFloristFlag().equals("Y")) {
                        priorityString = priorityString + "/Y";
                    }
                    // all other cases
                    else {
                        priorityString = priorityString + "/M";
                    }
                } else {
                    //it's a goto (super) florist
                    if ((floristVO.getSuperFloristFlag() != null) &&
                            floristVO.getSuperFloristFlag().equals("Y")) {
                        priorityString = priorityString + "/X";
                    }
                    // all other cases
                    else {
                        priorityString = priorityString + "/N";
                    }
                }
            } else {
                //it's a goto (super) florist
                if ((floristVO.getSuperFloristFlag() != null) &&
                        floristVO.getSuperFloristFlag().equals("Y")) {
                    priorityString = priorityString + "/X";
                }
                // all other cases
                else {
                    priorityString = priorityString + "/N";
                }
            }
        } else {
            // priority string for international orders = A/M
            priorityString = "A/M";
        }

        return priorityString;
    }

    /**
     * This method determines if the test range intersects the base range
     *
     */
    private boolean isDateRangeIntersecting(Date testRangeStart,
        Date testRangeEnd, Date baseRangeStart, Date baseRangeEnd) {
        if (baseRangeEnd == null) {
            if (testRangeEnd == null) {
                return testRangeStart.compareTo(baseRangeStart) >= 0;
            } else {
                return testRangeEnd.compareTo(baseRangeStart) >= 0;
            }
        } else {
            if (testRangeEnd == null) {
                return ((testRangeStart.compareTo(baseRangeStart) >= 0) &&
                (testRangeStart.compareTo(baseRangeEnd) <= 0));
            } else {
                return ((testRangeStart.compareTo(baseRangeStart) >= 0) &&
                (testRangeStart.compareTo(baseRangeEnd) <= 0)) ||
                ((testRangeEnd.compareTo(baseRangeStart) >= 0) &&
                (testRangeEnd.compareTo(baseRangeEnd) <= 0)) ||
                ((testRangeStart.compareTo(baseRangeStart) <= 0) &&
                (testRangeEnd.compareTo(baseRangeEnd) >= 0));
            }
        }
    }

    /** This method is a helper function for buildFTDMessage(OrderDetailVO vo).
     * It builds the special instructions string.
     *
     * @param
     * @param
     * @return String specialInstructions
     * **/
    public String buildSpecialInstructionString(OrderDetailVO orderDetailVO,
        SourceVO sourceVO, String emergencyMercTextFlag,
        String emergencyMercTextDate, String emergencyMercText,
        String emergencyMercTextEndDate, CustomerVO customer,
        ProductVO productVO, String callingSource) throws Exception {

        String specialInstructionString = "";

        if ((emergencyMercTextFlag.equalsIgnoreCase("Y")) &&
                (sourceVO != null) &&
                (sourceVO.getEmergencyTextFlag() != null) &&
                (sourceVO.getEmergencyTextFlag().equalsIgnoreCase("Y"))) {
            SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
            Date date = formatter1.parse(emergencyMercTextDate);
            Date endDate = (emergencyMercTextEndDate != null)
                ? formatter1.parse(emergencyMercTextEndDate) : null;

            // Get today's date with time = midnight
            Date delivDate = orderDetailVO.getDeliveryDate();
            Date delivDateEnd = orderDetailVO.getDeliveryDateRangeEnd();

            if (isDateRangeIntersecting(delivDate, delivDateEnd, date, endDate)) {
                specialInstructionString = emergencyMercText + " ";
            }
        }

        //12529 add the derived vip flag
        if(orderDetailVO.getDerived_vip_flag() == null && orderDetailVO.getSpecialInstructions() != null)
        {
       		specialInstructionString = specialInstructionString + orderDetailVO.getSpecialInstructions();
        }

        Iterator iter = customer.getCustomerPhoneVOList().iterator();

        // Don't build date range string if calling from COM / Mercury Facade
        if (callingSource == null) {
            if (orderDetailVO.getDeliveryDateRangeEnd() != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("MMM dd - EEE");
                String deliveryDateText = formatter.format(orderDetailVO.getDeliveryDate());
                deliveryDateText = deliveryDateText.concat(" or " + formatter.format(orderDetailVO.getDeliveryDateRangeEnd()));
                specialInstructionString += " Delivery " + deliveryDateText + " OK ";
            }
        }

        while(iter.hasNext()) 
        {
          CustomerPhoneVO phone = (CustomerPhoneVO)iter.next();
          if (phone.getPhoneType().equalsIgnoreCase("DAY"))
          {
            if(phone.getExtension() != null)  {
              specialInstructionString += "  Recipient Phone Extension: " + phone.getExtension();
            }
          }
        }

        if (orderDetailVO.getSizeIndicator() != null && productVO.getDeliveryType().equals("D")) {
            if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("A")) {
                if (productVO.getSendStandardRecipe() != null && productVO.getSendStandardRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getRecipe() != null ? productVO.getRecipe() : "");
                }
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("B")) {
                if (productVO.getSendDeluxeRecipe() != null && productVO.getSendDeluxeRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getDeluxeRecipe() != null ? productVO.getDeluxeRecipe() : "");
                }
            } else if (orderDetailVO.getSizeIndicator().equalsIgnoreCase("C")) {
                if (productVO.getSendPremiumRecipe() != null && productVO.getSendPremiumRecipe().equalsIgnoreCase("Y")) {
                    specialInstructionString += " RECIPE: " + (productVO.getPremiumRecipe() != null ? productVO.getPremiumRecipe() : "");
                }
            }
        }
        if(orderDetailVO.getSpecialInstructions() != null)
        {
        	if(orderDetailVO.getSpecialInstructions().trim().length() > 0)
        	{
        		if (orderDetailVO.getSpecialInstructions().contains("INST:"))        	
        		{
        			if (orderDetailVO.getDerived_vip_flag() != null)  
        				specialInstructionString = orderDetailVO.getSpecialInstructions() + " " + specialInstructionString;
        		}
        		else
        		{
        			if (orderDetailVO.getDerived_vip_flag() != null) 
        			{
        				if (specialInstructionString != null && specialInstructionString.trim().length() > 0)
        					specialInstructionString = orderDetailVO.getSpecialInstructions() + " INST:" + specialInstructionString;
        				else
        					specialInstructionString = orderDetailVO.getSpecialInstructions();
        			}
        		}
        	}
        }

        if(specialInstructionString == null || specialInstructionString.trim().length() == 0) { 
          // always default to None, because special Instructions is a required field
          specialInstructionString = "None";
        }
        
        return breakString(specialInstructionString, LINE_LENGTH);
    }

    /**
     * Builds the sort value field for an outbound mercury message
     *
     * @return sort value field
     * @param recipientId used to get the timezone (secondary sort criterion)
     * @param orderDate used as the tertiary sort criterion
     * @param deliveryDate used as the primary sort criterion
     */
    private String buildSortValue(Date deliveryDate, Date orderDate,
        String state) throws Exception {
        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyyMMdd");

        OrderDAO dao = new OrderDAO(conn);

        String formattedDelivDate = simpleFormat.format(deliveryDate);
        String formattedOrderDate = simpleFormat.format(orderDate);
        String timezone;

        timezone = dao.getTimezone(state);

        if (timezone == null) {
            // default to eastern time zone for international (non-canada) orders
            timezone = "1";
        }

        // calculate current time (from midnight) in seconds
        Calendar cal = Calendar.getInstance();
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
        long currentTime = (hours * 3600) + (minutes * 60) + seconds;

        // build sortvalue string: delivery date, timezone, order date, message time
        String sortValue = formattedDelivDate + timezone + formattedOrderDate +
            currentTime;

        return sortValue;
    }

    /**
     * Returns the current date/time as the transmission time field
     * @return current time
     */
    private Date buildTransmissionTime() {
        return new Date();
    }

    /* This method parses the 2 character state code from a given CSZ of the
     * format city\w*,\w*state\w+zip
     */
    public String parseStateFromCSZ(String csz) {
        String output;

        try {
            String[] array = csz.split(",");
            output = array[1].trim().split(" ")[0].trim();
        } catch (Exception e) {
            e.printStackTrace();
            output = null;
            logger.info(
                "parseStateFromCSZ -- Bad format, assuming international");
        }

        return output;
    }
    private String breakString(String inString, int lineLength) 
    {
      if(inString == null) return null;
      inString = inString.replaceAll("\n", " ");
      String outputString = new String();
      if(inString.length() == 0) return inString;
      
      // append a blank space so the substring method can always find a space at the end
      String remainingString = inString;
   
      try {   
        while(lineLength < remainingString.length()) {
          outputString += remainingString.substring(0, remainingString.indexOf(" ", lineLength)) + "\n";
          remainingString = remainingString.substring(remainingString.indexOf(" ", lineLength) + 1, remainingString.length()).trim();
        }
      } catch(Exception e) 
      {
        // fall out of loop on exception
      }
      return outputString + remainingString;
    }

    public static void testGetters(Object obj1, String methodName1,
        Object obj2, String methodName2) throws Exception {
        if ((obj1 == null) || (obj2 == null)) {
            if ((obj1 == null) && (obj2 == null)) {
                System.out.println("assert true that as true");
            }
        } else {
            try {
                Method method1 = obj1.getClass().getMethod(methodName1, null);
                Method method2 = obj2.getClass().getMethod(methodName2, null);

                if (method1.getReturnType().equals(method1.getReturnType())) {
                    if (method1.getReturnType().isPrimitive()) {
                        System.out.println("Assert True return = return");
                    } else if (method1.getReturnType().getName()
                                          .equalsIgnoreCase("java.util.Date")) {
                        System.out.println("AssertTrue as Date");
                    } else {
                        System.out.println("Assert True as object");
                    }
                } else {
                    System.out.println("Return types can't be equal");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
	public void setSendingFloristWithOutbound(String companyId, String assignedSendingFlorist, MercuryVO mercuryVO) {
		String sendingFloristNumber = null;
		try {
			OrderDAO orderDAO = new OrderDAO(conn);
			List<GlobalParameterVO> params = orderDAO.getLikeGlobalParameter("MERCURY_INTERFACE_CONFIG",
					"EAPI_OUTBOUND_ID_LIST_%");
			GlobalParameterVO outboundIdVO = null;
			GlobalParameterVO defaultOutboundIdVO = null;
			
			for (GlobalParameterVO globalParameterVO : params) {
				String[] toProcess = globalParameterVO.getName().replace("EAPI_OUTBOUND_ID_LIST_", "").split("_");  
				 
				if (toProcess[1].equalsIgnoreCase(companyId)) {
					logger.info("Company: " + companyId + ", globalParamName: " + globalParameterVO.getName());
					sendingFloristNumber = toProcess[0];
					outboundIdVO = globalParameterVO;
					break;
				} else if (toProcess[1].equalsIgnoreCase("DEFAULT")) {
					logger.info("Company: " + companyId + ", globalParamName: " + globalParameterVO.getName());
					sendingFloristNumber = toProcess[0];
					defaultOutboundIdVO = globalParameterVO;
				}
			}

			if (outboundIdVO == null) {
				logger.info("Invalid Outbound. Assigning DEFAULT OutBound Id for company: " + companyId);
				outboundIdVO = defaultOutboundIdVO;
			}
			
			mercuryVO.setSendingFlorist(assignedSendingFlorist); // from original FTD or order detail default florist ID
			if (sendingFloristNumber != null && !assignedSendingFlorist.equals(sendingFloristNumber)) {
				mercuryVO.setSendingFlorist(sendingFloristNumber);
				logger.info("The sending florist number on original order is: " + assignedSendingFlorist
						+ ", different from new sending florist number" + sendingFloristNumber); 
			}
			
			String idString = outboundIdVO.getValue();
			if (idString != null && !idString.equals("")) {
				String patternStr = " ";
				// wrap this in a new try loop to not interfere with the creation of a message
				try {
					String[] fields = idString.split(patternStr);
					int listSize = fields.length;
					Random generator = new Random();
					int seq = generator.nextInt(listSize);
					if (seq >= 0 && fields[seq] != null && fields[seq].length() == 2) {
						logger.info("Selected suffix: " + fields[seq]);
						mercuryVO.setOutboundId(fields[seq]);
					}
				} catch (Exception e) {
					logger.error("Error assigning suffix: " + e);
				}
			}

		} catch (Exception e) {
			// TODO
		} 
	}
    
}
