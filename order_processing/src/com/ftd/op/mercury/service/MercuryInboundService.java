package com.ftd.op.mercury.service;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.service.MessagingService;
import com.ftd.op.common.service.PartnerDCONService;
import com.ftd.op.common.service.QueueService;
import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.constants.MercuryConstants;
import com.ftd.op.mercury.dao.FloristMaintenanceDAO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.util.FloristMaintenanceLockHelper;
import com.ftd.op.mercury.util.ProxyFlorist;
import com.ftd.op.mercury.vo.AutoResponseKeyVO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.dao.PhoenixDAO;
import com.ftd.op.order.service.DeliveryConfirmationService;
import com.ftd.op.order.service.OrderService;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.FloristVO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;

import java.sql.Connection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import javax.naming.InitialContext;
import javax.sql.DataSource;


/**
 * Contains all of the business logic involved in processing inbound Mercury
 * messages
 *
 * @author Jason Weiss
 */
public class MercuryInboundService {

    private static final int sequenceRollover = 10000;
    private static final int DEFAULT_CUTOFF_TIME = 1400;

    private static final String OUTBOUND = "OUTBOUND";
    private static final String INBOUND = "INBOUND";
    private static final String ADMIN = "Y";
    private static final String ORDER = "N";

    // message types
    private static final String REJECT = "REJ";
    private static final String FORWARD = "FOR";
    private static final String ASKPI = "ASKPI";
    private static final String ASK = "ASK";
    private static final String ANS = "ANS";
    private static final String FTD_ORDER = "FTD";

    // Mercury status
    private static final String MERCURY_COMPLETE = "MC";

    // global parms
    private static final String GLOBAL_REJECT_RETRIES = "REJECT_RETRIES";
    private static final String GLOBAL_MONDAY_CUTOFF = "MONDAY_CUTOFF";
    private static final String GLOBAL_TUESDAY_CUTOFF = "TUESDAY_CUTOFF";
    private static final String GLOBAL_WEDNESDAY_CUTOFF = "WEDNESDAY_CUTOFF";
    private static final String GLOBAL_THURSDAY_CUTOFF = "THURSDAY_CUTOFF";
    private static final String GLOBAL_FRIDAY_CUTOFF = "FRIDAY_CUTOFF";
    private static final String GLOBAL_SATURDAY_CUTOFF = "SATURDAY_CUTOFF";
    private static final String GLOBAL_SUNDAY_CUTOFF = "SUNDAY_CUTOFF";

    //FOL
    private static final String FOL_STRING = "90-8418";

    private static String LOGGER_CATEGORY = "com.ftd.op.mercury.service.MercuryInboundService";

    private static Logger logger = null;
    private Connection conn;
	private OrderConstants orderConstants;
	
	//To allow order comments if message has cancled.
    private boolean isMessageCancled=false;
    /**
   * Constructor for inbound mercury message handling service
   */
    public MercuryInboundService(Connection c) {
        conn = c;
        if (logger == null) {
            logger = new Logger(LOGGER_CATEGORY);
        }
    }

    /**
   * This method dispatches an inbound mercury message to the appropriate
   * helper method
   *
   * @param message
   */
    private void processMercuryMessage(MercuryVO message) throws Exception {

        if ((message.getFillingFlorist() != null && 
             message.getFillingFlorist().startsWith(FOL_STRING)) || 
            (message.getSendingFlorist() != null && 
             message.getSendingFlorist().startsWith(FOL_STRING))) {
            MercuryDAO mercuryDAO = new MercuryDAO(conn);
            mercuryDAO.insertMercuryFOL(message.getMercuryId());
        } else {
            if (message.getDirection().equals(INBOUND)) {
                    if (message.getMessageType().equals(REJECT)) {
                        handleRejectMessage(message);
                    } else if (message.getMessageType().equals(FORWARD)) {
                        handleForwardMessage(message);
                    } else if (message.getQueueMessageType().equals(ASKPI)) {
                        // Defect 3531
                        handleASKPIMessage(message);
                    } else if (message.getMessageType().equals(ASK) || message.getMessageType().equals(ANS)) {
                        handleASKANSMessage(message);
                    } else {
                        queueInboundMessage(message);
                }
            } else if (message.getDirection().equals(OUTBOUND)) {
                handleVerifyMessage(message);
            } else {
                //punt... direction should be INBOUND or OUTBOUND only
            }
        }
    }

    /**
   * Get the inbound Mercury message by its Id and dispatch to its appropriate handler.
   *
   * @param mercuryId
   */
    public void processMercuryInboundMessage(String mercuryId) {
        try {
            
        	logger.info("MercuryId = "+mercuryId);
        	MercuryDAO dao = new MercuryDAO(conn);
            MercuryVO mercMessage = dao.getMessage(mercuryId);
            
            if(mercMessage == null)
            {
            	logger.info("Mercury Inbound Message is null. Putting the message in ORDER_EXCEPTIONS table.");
				HashMap outputMap = this.insertOrderExceptions(conn, mercuryId, "ORDER DISPATCHER/MERCURYINBOUND", "Mercury Inbound Message is null.");
				String status = (String) outputMap.get("OUT_STATUS");
                String message = (String) outputMap.get("OUT_MESSAGE");
                logger.debug("status: " + status + " - " + message);
            }
            else
            {
            	logger.info("Begin processing for inbound mercury message: " + mercMessage.getMercuryMessageNumber()
                    + " " + mercMessage.getMessageType() + " " + mercMessage.getDirection());
            	this.processMercuryMessage(mercMessage);
            }
            
        } catch (Exception e) {
            logger.error("Mercury inbound Message processing failed", e);
            e.printStackTrace();
        }
    }

    /**
   * This method tests whether any numbers are missed in a list of integers
   *
   * @param integerList
   * @param lastInteger
   * @return
   */
    private int checkSequence(LinkedList integerList, int lastUsed) {
        int counter = lastUsed;
        Iterator iter = integerList.iterator();
        int sequenceNumber = lastUsed;
        while (iter.hasNext()) {
            counter = (counter + 1) % sequenceRollover;
            sequenceNumber = ((Integer)iter.next()).intValue();
            if (sequenceNumber != counter) {
                // send message to admin -- missing sequence number
                //Jason, I commented this out
                //        System.out.println("invalid sequence");
            }
        }
        return sequenceNumber;
    }

    /**
     * This method attempts to automactically respond to these messages:
     * 1. Inbound florist REJ message with matching text comments.
     * 2. Outbound FTD verify messages with matching sak messages.
     * 3. Inbound florist ASKP messages (that are over thredshold).
     * 4. Inbound florist ASK messages with matching text comments.
     * 
     * It attempts to resend order for processing. Queue message if retry count has been reached.
     * ASKPI's will cancel and resend FTD. Other messages go through the following logic:
     * For the given message type and message text, find a matching key phrase from mercury.auto_response_key_phrase.
     * 
     * If a match is found and a retry is required, then retry FTD; If retry is not required, put message in queue;
     * If a match is found and cancel flag is on, cancel original;
     * If the view queue flag is on, then put message to view queue.
     * 
     * If no matching key phrase is found, queue message. 
     * Do not auto response if the global switch is off or too close to cutoff.
     * 
     * This method is enhanced by defect 3707.
     * 
     * @param orderDetail
     * @param message
     * @throws Exception
     */
    private void attemptAutomaticResponse(OrderDetailVO orderDetail, 
                                          MercuryVO message) throws Exception {
        logger.info(orderDetail.getOrderDetailId() + 
                    ": Attempting automatic response to message: " + 
                    message.getMercuryId() + "," + 
                    orderDetail.getOrderDetailId());
        OrderDAO orderDao = new OrderDAO(conn);
        MercuryDAO mercuryDao = new MercuryDAO(conn);
        CommonDAO commonDao = new CommonDAO(conn);
        DateFormat df = null;
        String sToday = null;
        String sDelivery = null;
        String queueMsgType = null;
        String msgType = null;
        String msgText = null;
        String keyPhrase = null;
        AutoResponseKeyVO autoResponseKeyVO = null;
        boolean cancelFlag = false;
        boolean retryFTDFlag = false;
        boolean viewQueueFlag = false;     
        boolean softBlockFlag = false;
        String commentText = null;

        boolean phoenixEnabled = false; 
        GlobalParameterVO globalParam = orderDao.getGlobalParameter(
                "PHOENIX", "PHOENIX_ENABLED");
        if ((globalParam.getValue() != null)
                    && (globalParam.getValue()
                                .equalsIgnoreCase(orderConstants.VL_YES))) {
              logger.info(orderDetail.getOrderDetailId() + ": Phoenix is enabled");
              phoenixEnabled = true;              
        }        	
        
        int rejectRetryLimit = 
            (new Integer(orderDao.getGlobalParameter(GLOBAL_REJECT_RETRIES).getValue())).intValue();
        
        // test retry limit
        long numFailures = orderDetail.getRejectRetryCount();
        
        if (rejectRetryLimit <= numFailures) {
            logger.info(orderDetail.getOrderDetailId() + 
                        ": Could not retry -- too many attempts");
            queueInboundMessage(message);
            return;
        }
        
        if(phoenixEnabled){        	
        	int phoenixRejectThreshold = (new Integer(orderDao.getGlobalParameter("PHOENIX_REJECT_THRESHOLD").getValue())).intValue();
        	logger.info("phoenixRejectThreshold: " + phoenixRejectThreshold + "| numFailures: " + numFailures + "| rejectRetryLimit: " + rejectRetryLimit);
        	logger.info("Message Type : "+message.getMessageType());
        	        	
        	if ((phoenixRejectThreshold <= numFailures && phoenixRejectThreshold <= rejectRetryLimit 
        			&& (message.getMessageType().equalsIgnoreCase("FTD") || message.getMessageType().equalsIgnoreCase("REJ") || (message.getMessageType().equalsIgnoreCase("ASK") 
        					&& "P".equalsIgnoreCase(message.getAskAnswerCode())))) ) {
        		      		
        		//If Phoenix is enabled then check to see if the order is Phoenix eligible
        		PhoenixDAO phoenixDao = new PhoenixDAO(conn);
        		String mercOrderNum = null;
        		if (message.getMercuryOrderNumber() != null){
        			mercOrderNum = message.getMercuryOrderNumber();
        		}

        		CachedResultSet phoenixEligibilityInfo = phoenixDao.getOrderPhoenixEligibility(String.valueOf(orderDetail.getOrderDetailId()), "REJ", mercOrderNum);
        		
        		String isOrderPhoenixEligible = null;
                String phoenixProductId = null;
                String bearAddonId = null;
                String chocAddonId = null;
                String orderDeliveryDate = null;
                String recipZipCode = null;
                String origNewProdSame = null;

                if(phoenixEligibilityInfo.next())
                {
                   if(phoenixEligibilityInfo.getString("v_order_phoenix_eligible") != null)
                	   isOrderPhoenixEligible = phoenixEligibilityInfo.getString("v_order_phoenix_eligible").trim();
                   if(phoenixEligibilityInfo.getString("v_phoenix_product_id") != null)
                	   phoenixProductId = phoenixEligibilityInfo.getString("v_phoenix_product_id").trim();
                   if(phoenixEligibilityInfo.getString("v_bear_addon_id") != null)
                	   bearAddonId = phoenixEligibilityInfo.getString("v_bear_addon_id").trim();
                   if(phoenixEligibilityInfo.getString("v_choc_addon_id") != null)
                	   chocAddonId = phoenixEligibilityInfo.getString("v_choc_addon_id").trim();
                   if(phoenixEligibilityInfo.getString("v_delivery_date") != null)
                	   orderDeliveryDate = phoenixEligibilityInfo.getString("v_delivery_date").trim();                   
                   if(phoenixEligibilityInfo.getString("v_recip_zip_code") != null)
                	   recipZipCode = phoenixEligibilityInfo.getString("v_recip_zip_code").trim();
                   if(phoenixEligibilityInfo.getString("v_orig_new_prod_same") != null)
                	   origNewProdSame = phoenixEligibilityInfo.getString("v_orig_new_prod_same").trim();
                 }
               
                 if(isOrderPhoenixEligible.equalsIgnoreCase("Y")){
	                logger.info(orderDetail.getOrderDetailId() + ": Order is Phoenix eligible");
	                logger.info("msgType: " + message.getMessageType());
					//msg format = order detail id|origin|mercury order number|phoenix product id|bear addon id|chocolate addon id|order delivery date|recip zip code|orig new prod same flag|cancel original FTD message|mercury id|ship method|ship date|csrId
	                orderDetail.setRejectRetryCount(numFailures + 1);
	                orderDao.updateOrder(orderDetail);
	                StringBuffer sb = new StringBuffer();
  	        		sb.append(orderDetail.getOrderDetailId());
  	        		sb.append("|");
  	        		sb.append(message.getMessageType());
  	        		sb.append("|");
  	        		if (message.getMercuryOrderNumber() != null){
  	        			sb.append(message.getMercuryOrderNumber());
  	        		}
  	        		else{
  	        			sb.append("null");
  	        		}  	        		
  	        		sb.append("|");
  	        		sb.append(phoenixProductId);
  	        		sb.append("|");
  	        		sb.append(bearAddonId);
  	        		sb.append("|");
  	        		sb.append(chocAddonId);
  	        		sb.append("|");
  	        		sb.append(orderDeliveryDate);
  	        		sb.append("|");
  	        		sb.append(recipZipCode);
  	        		sb.append("|");
  	        		sb.append(origNewProdSame);
  	        		sb.append("|");
  	        		sb.append("null");
  	        		sb.append("|");
  	        		sb.append(message.getMercuryId());
  	        		sb.append("|");
  	        		sb.append("null");
  	        		sb.append("|");
  	        		sb.append("null");
  	        		sb.append("|");
  	        		sb.append("null");
  	        		sb.append("|");
  	        		sb.append("null");
  	        		sb.append("|");
  	                sb.append("null"); //emailTitle
  	                sb.append("|");
	                sb.append("null"); //startOrigin
	                sb.append("|");
	                sb.append("null"); //bulk
  	        		String msg = sb.toString();	     
	                logger.info("msg: " + msg);
	                Dispatcher dispatcher = Dispatcher.getInstance();
	                MessageToken token = new MessageToken();
	                token.setMessage(msg);
	                token.setJMSCorrelationID("ProcessDetail");
	                token.setStatus("PHOENIX");
	                dispatcher.dispatchTextMessage(new InitialContext(), token); 
	                return;
                 }
        	}

        }	
    
        
        df = DateFormat.getDateInstance(DateFormat.SHORT);
        sToday = df.format(new Date());
        sDelivery = df.format(orderDetail.getDeliveryDate());  

        if (df.parse(sDelivery).before(df.parse(sToday))) {
            //The delivery date has passed, so, don't attempt to resend  
            logger.info(orderDetail.getOrderDetailId() + 
                        ": Could not retry -- delivery date has passed");
            queueInboundMessage(message);
            return;
        }
        // Cutoff stop does not apply to ASKPI's
        else if (isCloseToCutoff(orderDetail, !ASKPI.equalsIgnoreCase(queueMsgType), orderDao)) {
            logger.info(orderDetail.getOrderDetailId() + 
                            ": Could not retry -- past cutoff");
            queueInboundMessage(message);
            // once the message is queued automated processing is done  
            return;      
        } 

        queueMsgType = message.getQueueMessageType();
        msgType = message.getMessageType();
        msgText = message.getComments();        
        
        if(FTD_ORDER.equalsIgnoreCase(msgType)) {
            msgText = message.getSakText();
        }

        if (ASKPI.equalsIgnoreCase(queueMsgType)) {
            cancelFlag = true;
            retryFTDFlag = true;
            commentText = MercuryConstants.ASKP_AUTO_CANCEL_ORDER_COMMENT_TEXT;
        } 
        else {
            autoResponseKeyVO = mercuryDao.getAutoResponseKey(msgText, msgType);
            
            if(autoResponseKeyVO == null) {
                // No key phrase found. Send message to queue.
                queueInboundMessage(message);
                return;
            } 
            else {
                cancelFlag = autoResponseKeyVO.getCancelFlag().equalsIgnoreCase("Y")? true : false;
                retryFTDFlag = autoResponseKeyVO.getRetryFTDFlag().equalsIgnoreCase("Y")? true : false;
                viewQueueFlag = autoResponseKeyVO.getViewQueueFlag().equalsIgnoreCase("Y")? true : false;
                softBlockFlag = autoResponseKeyVO.getFloristSoftBlockFlag().equals("Y")? true : false;
                keyPhrase = autoResponseKeyVO.getKeyPhraseTxt();
                commentText = MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_1 + msgType +
                              MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_2 + keyPhrase + 
                              MercuryConstants.AUTO_RETRY_ORDER_COMMENT_TEXT_3;
            }
        }
        
        if(retryFTDFlag) {
            orderDetail.setRejectRetryCount(numFailures + 1);
            logger.debug("Setting order detail #" + 
                         orderDetail.getOrderDetailId() + 
                         " OP_STATUS to PENDING...");
            orderDetail.setOpStatus("Pending");
            orderDao.updateOrder(orderDetail);
            logger.debug("RETRY COUNT = " + orderDetail.getRejectRetryCount());

            orderDao.clearFlorist("" + orderDetail.getOrderDetailId());
            orderDetail.setFloristId(null);
            
             
            // try to reroute the order
            OrderService orderService = new OrderService(conn);

            // since this is a resend, do not perform validations
            boolean succeeded = 
                orderService.processOrder(orderDetail, false, false);
            if (!succeeded) {
                queueInboundMessage(message);
                logger.info(orderDetail.getOrderDetailId() + 
                            ": Could not automatically re-process.  Sending message to queue");
            } else {
                // Defect 4864: Send cancel only if new FTD is successful.
                if(cancelFlag) {
                    this.sendCancelMessage(message);
                }
            }
            // Adding a comment explaining what caused the retry.
            if(this.isMessageCancled)
                 this.createOrderComment(commentText, orderDetail);
        }
        else {
            queueInboundMessage(message);
        }
        
        if(softBlockFlag) {
        
            MercuryVO ftdMessage = null;
            
            if(message.getMessageType().equalsIgnoreCase("FTD")) {
                ftdMessage = message;
            } else {
                ftdMessage = mercuryDao.getAssociatedFtd(message);
            }
            softBlockFlorist(ftdMessage.getFillingFlorist());
        }
        
        if(commonDao.isViewQueueFlagOn() && viewQueueFlag) {
            mercuryDao.updateViewQueueFlag(message.getMercuryId(), "Y");
        }
    }

    /**
   * Resends an FTD if the message meets the automated response criteria.  Put
   * the message in the REJ queue if the criteria are met or the resend fails.
   *
   * @param message
   */
    private void handleRejectMessage(MercuryVO rejMessage) {
        try {
            OrderDAO orderDao = new OrderDAO(conn);
            MercuryDAO mercuryDao = new MercuryDAO(conn);

            MercuryVO ftdMessage = null;
            // get the associated FTD
            ftdMessage = mercuryDao.getAssociatedFtd(rejMessage);

            // lookup the orderDetail record    
            OrderDetailVO orderDetail = 
                orderDao.getOrderDetail(ftdMessage.getReferenceNumber());
            if (orderDetail == null) {
                throw new Exception("Cannot locate order detail record");
            }
            
            // Save associated FTD in orderDetail so we can eventually resend this FTD (in MercuryOutboundService)
            orderDetail.setCurrentFtd(ftdMessage);
            
            // attempt automatic response
            try {
                attemptAutomaticResponse(orderDetail, rejMessage);
            } catch (Exception er){
                logger.error("Failed automated response. Queuing message id:" + rejMessage.getMercuryId() + er);
                this.queueInboundMessage(rejMessage);
            }

        } catch (Exception e) {
            logger.error("Failed processing REJ message", e);
        }
    }
    
    /**
    * Compare price on FTD and the ASKP message. If price increase within threshold, 
    * send ANS to approve price change. If price increase is greater than threshold,
    * creates CAN message and resends an FTD if the message is within retry count.  
    * Puts the message in the ASK queue if order has been tried too many times.
    *
    * @param message
    */
    private void handleASKPIMessage(MercuryVO message) throws Exception {
        try {
            OrderDAO orderDao = new OrderDAO(conn);
            MercuryDAO mercuryDao = new MercuryDAO(conn);

            MercuryVO ftdMessage = null;
            // get the associated FTD
            ftdMessage = mercuryDao.getAssociatedFtd(message);

            // lookup the orderDetail record    
            OrderDetailVO orderDetail = 
                orderDao.getOrderDetail(ftdMessage.getReferenceNumber());
            if (orderDetail == null) {
                throw new Exception("Cannot locate order detail record");
            }
            
            // Jira Id : SP-36
            // Save associated FTD in orderDetail so we can eventually resend this FTD (in MercuryOutboundService)
            orderDetail.setCurrentFtd(ftdMessage);
            
            // Retrieve price increase threshold
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            String priceIncreaseThreshold =  config.getFrpGlobalParmNoNull(MercuryConstants.ASK_MESSAGE_CONTEXT, MercuryConstants.AUTO_ANSP_LIMIT);
            BigDecimal priceLimit = new BigDecimal(priceIncreaseThreshold);
            BigDecimal newPrice = new BigDecimal(String.valueOf(message.getPrice()));
            BigDecimal priceDelta = new BigDecimal(String.valueOf(message.getPrice() - ftdMessage.getPrice()));
            priceDelta.setScale(2, RoundingMode.HALF_UP);
            
            if (newPrice.compareTo(new BigDecimal("0.00")) == 0) {
                //Price invalid. Queue message.
                this.queueInboundMessage(message);
            } else if (mercuryDao.isOrderCancelledOrRejected(message)) {
                // order has been rejected or cancelled.
                this.queueInboundMessage(message);
            } else if(priceDelta.compareTo(priceLimit) > 0) {
                // If price increase over threshold, CAN and retry.
                try {
                    this.attemptAutomaticResponse(orderDetail, message);
                } catch (Exception er) {
                    logger.error("Failed automated response. Queuing message id:" + message.getMercuryId() + er);
                    this.queueInboundMessage(message);
                }
            } else {
                // Price delta is less or equal to threshold. Approve request by sending ANS.
                try {
                    this.sendAnswerMessage(message);
                    this.createOrderComment(MercuryConstants.ASKP_AUTO_ANSWER_ORDER_COMMENT_TEXT, orderDetail);
                } catch (Exception ea) {
                    logger.error("Failed sending answer message. Queuing message id:" + message.getMercuryId() + ea);
                    this.queueInboundMessage(message);
                }
            }
        } catch (Exception e) {
            logger.error("Failed processing ASPI message", e);
            throw e;
        }
    }    
    
    /**
    * Match the message with key phrases defined in mercury.auto_response_key_phrase.
    * If the matched key phrase requires CAN, cancel FTD and attempt to resend. 
    *
    * @param message
    */
    private void handleASKANSMessage(MercuryVO message) throws Exception {
        try {
            logger.debug("handleASKANSMessage");
            OrderDAO orderDao = new OrderDAO(conn);
            MercuryDAO mercuryDao = new MercuryDAO(conn);

            MercuryVO ftdMessage = null;
            // get the associated FTD
            ftdMessage = mercuryDao.getAssociatedFtd(message);

            // lookup the orderDetail record    
            OrderDetailVO orderDetail = orderDao.getOrderDetail(ftdMessage.getReferenceNumber());
            if (orderDetail == null) {
                throw new Exception("Cannot locate order detail record");
            }

            if (isDeliveryConfirmation(message.getComments(), mercuryDao) || (message != null && message.getAskAnswerCode() != null && MercuryConstants.ASK_ANS_CODE_C.equalsIgnoreCase(message.getAskAnswerCode()))) {
                logger.debug("Message is a Delivery Confirmation");
                                
                String orderDetailIdStr = ftdMessage.getReferenceNumber();                
                try {
                	Long orderDetailId = Long.parseLong(orderDetailIdStr);
                	PartnerDCONService.getInstance().persistPartnerDconData(orderDetailId, "Delivered", new Date(), conn);
                } catch(Exception e) {
                	String errorMessage = "Problem occured while persisting partner delivery confirmation data. Order detail id: " + orderDetailIdStr +" Error: " + e;
                  	logger.error(errorMessage, e);
                  	CommonUtils.sendNoPageSystemMessage(errorMessage, "PARTNER_INTEGRATION_NOPAGE", "NOPAGE Partner Integration Message");
                }
                
                DeliveryConfirmationService dcs = new DeliveryConfirmationService(conn);
                boolean success = dcs.checkDeliveryConfMailControl(ftdMessage.getDeliveryDate());
                if(success){
                	success = dcs.sendDeliveryConfirmationEmail(ftdMessage.getReferenceNumber());
                }
                
                logger.info("result: " + success);

            } else {
                ConfigurationUtil config = ConfigurationUtil.getInstance();    
                String autoResponseOnAsk = config.getFrpGlobalParmNoNull(MercuryConstants.CONFIG_CONTEXT, MercuryConstants.AUTO_RESPONSE_ON_ASK);
                logger.debug("AUTO_RESPONSE_ON_ASK : "+autoResponseOnAsk);

                if(mercuryDao.isOrderCancelledOrRejected(message)) {
                    // order has been rejected or cancelled.
                    this.queueInboundMessage(message);
                } else if(autoResponseOnAsk != null && autoResponseOnAsk.equalsIgnoreCase("Y")) {
                    // attempt automatic response
                    try {
                        attemptAutomaticResponse(orderDetail, message);
                    } catch (Exception er){
                        logger.error("Failed automated response. Queuing message id:" + message.getMercuryId() + er);
                        this.queueInboundMessage(message);
                    }
                } else {
                    // Global switch is off. Send message to queue.
                    queueInboundMessage(message);
                }
            }
            
        } catch (Exception e) {
            logger.error("Failed processing ASK message", e);
            throw e;
        }
    }        

    /**
   *
   * @param date
   * @return
   */
    private int getCutoffTime(Calendar date) throws Exception {
        int cutoffTime = DEFAULT_CUTOFF_TIME;
        try {
            OrderDAO dao = new OrderDAO(conn);

            GlobalParameterVO global;
            switch (date.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                global = dao.getGlobalParameter(GLOBAL_MONDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_MONDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.TUESDAY:
                global = dao.getGlobalParameter(GLOBAL_TUESDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_TUESDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.WEDNESDAY:
                global = dao.getGlobalParameter(GLOBAL_WEDNESDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_WEDNESDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.THURSDAY:
                global = dao.getGlobalParameter(GLOBAL_THURSDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_THURSDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.FRIDAY:
                global = dao.getGlobalParameter(GLOBAL_FRIDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_FRIDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.SATURDAY:
                global = dao.getGlobalParameter(GLOBAL_SATURDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_SATURDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            case Calendar.SUNDAY:
                global = dao.getGlobalParameter(GLOBAL_SUNDAY_CUTOFF);
                if (global == null) {
                    throw new Exception("Could not find GLOBAL_SUNDAY_CUTOFF in global parms");
                }
                cutoffTime = (new Integer(global.getValue())).intValue();
                break;

            default:
                logger.error("Bad day of week");
            }
        } catch (Exception e) {
            throw e;
        }
        return cutoffTime;
    }
    
    /**
     * Returns true if current time has passed the global cutoff and false otherwise,
     * taking into consideration of cutoff stop (a configurable number of hours
     * before cutoff).
     * @return
     */
    private boolean isCloseToCutoff(OrderDetailVO orderDetail, boolean applyCutoffStop, OrderDAO orderDao) throws Exception
    {
        // Retrieve price increase threshold
        ConfigurationUtil config = null;
        String autoResponseCutoffStop = null;
        int cutoffStop = 0;
     
        if(applyCutoffStop) {
            try {
                config = ConfigurationUtil.getInstance();
                autoResponseCutoffStop = config.getFrpGlobalParmNoNull(MercuryConstants.CONFIG_CONTEXT, MercuryConstants.AUTO_RESPONSE_OFF_BEFORE_CUTOFF_MINUTES);
                cutoffStop = Integer.parseInt(autoResponseCutoffStop);
            } catch (Exception ne) {
                cutoffStop = 0;
            }
        }
        
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String sToday = df.format(new Date());
        String sDelivery = df.format(orderDetail.getDeliveryDate());

        Calendar rightNow = Calendar.getInstance();
        rightNow = orderDao.getRecipientTime(orderDetail.getOrderDetailId(), rightNow);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
        logger.info("rightNow: " + sdf.format(rightNow.getTime()));


        if (df.parse(sToday).equals(df.parse(sDelivery))) {
            if (addTime(rightNow.get(Calendar.HOUR_OF_DAY), rightNow.get(Calendar.MINUTE), cutoffStop) > getCutoffTime(rightNow)) {
                return true;
            }
        }   
        return false;
    }

    /**
   * Updates an order to point to a new florist as specified by a FOR message.
   *
   * Put the old florist in the used florist list for this order (so it won't
   * get picked out of RWD if this order comes back to us) and update the order.
   *
   * @param message
   */
    private void handleForwardMessage(MercuryVO forMessage) {
        try {
            OrderDAO orderDao = new OrderDAO(conn);
            MercuryDAO mercuryDao = new MercuryDAO(conn);

            // get the associated FTD
            MercuryVO ftdMessage = mercuryDao.getAssociatedFtd(forMessage);


            // lookup the orderDetail record    
            OrderDetailVO orderDetail = 
                orderDao.getOrderDetail(ftdMessage.getReferenceNumber());
            if (orderDetail == null) {
                throw new Exception("Cannot locate order detail record");
            }

            // update order record to use new florist
            orderDetail.setFloristId(forMessage.getFillingFlorist());
            orderDao.updateOrder(orderDetail);

            // set current florist as used for the order
            orderDao.insertOrderFloristUsed(orderDetail, 
            		orderDetail.getFloristId(), "Forward");

            // TODO: put message in FOR queue -- I don't think we need to do this
        } catch (Exception e) {
            logger.error("Trouble with FOR message", e);
        }
    }

    /**
   * Queues a Mercury message for Customer Service to work.
   *
   * @param message
   */
    private void queueInboundMessage(MercuryVO message) throws Exception {
        QueueService queueService = new QueueService(conn);
        if (message.getMessageType().equalsIgnoreCase("GEN")) {
            queueService.sendUnassociatedMercuryMessageToQueue(message);
        } else {
            queueService.sendMercuryMessageToQueue(message);
        }
    }

    /**
   * Processes verification responses for Mercury outbound messages.
   *
   * Resend an FTD if it rejects and the reject automated response criteria.
   * Generate a confirm delivery ASK message if the message is an FTD and it is
   * verified.
   *
   * @param message
   */
    private void handleVerifyMessage(MercuryVO verMessage) {
        try {
            // if we tried to send an admin message and it failed, queue it
            // if it succeeded, we're done
            OrderDAO orderDao = new OrderDAO(conn);

            if (!verMessage.getMessageType().equals(FTD_ORDER)) {
                if (!verMessage.getMercuryStatus().equals(MERCURY_COMPLETE)) {
                    queueInboundMessage(verMessage);
                    return;
                }

            } else {
                if (verMessage.getMercuryStatus().equals(MERCURY_COMPLETE)) {
                    if (verMessage.getRequireConfirmation().equals("Y")) {
                        //generateConfirmationAskMessage(verMessage);
                        logger.debug("Updating status");
                        long orderDetailId = Long.parseLong(verMessage.getReferenceNumber());
                        orderDao.updateDeliveryConfirmationStatus(orderDetailId, GeneralConstants.DCON_PENDING, "FTD-OP");
                    }
                } else {
                    OrderDetailVO orderDetail = 
                        orderDao.getOrderDetail(verMessage.getReferenceNumber());
                    if (orderDetail == null) {
                        throw new Exception("Cannot locate order detail record");
                    }
                    
                    // Save associated FTD in orderDetail so we can eventually resend this FTD (in MercuryOutboundService)
                    orderDetail.setCurrentFtd(verMessage);

                    // Try to automate the response.
                    attemptAutomaticResponse(orderDetail, verMessage);
                }
            }

            // create random responses to fill up the queues, if autoRespond is
            // turned on
            ProxyFlorist proxy = ProxyFlorist.getInstance();
            if (proxy.isAutoRespond()) {
                proxy.respondToMercury(conn, verMessage);
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    /**
     * Builds a floristVO and calls FloristMaintenace to soft block the florist
     * Changes for 1791
     * @param floristId
     */
    public void softBlockFlorist(String floristId) {
        boolean lockAcquired = false;
        
        FloristVO floristVO = new FloristVO();
        floristVO.setFloristId(floristId);
        floristVO.setBlockedByUserId(MercuryConstants.FLORIST_BLOCK_USER_ID);
        floristVO.setBlockType(MercuryConstants.FLORIST_SOFT_BLOCK_TYPE);
        
        FloristMaintenanceLockHelper lockHelper = new FloristMaintenanceLockHelper(conn);
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            
            if(floristId == null || StringUtils.isEmpty(floristId)) {
                logger.error("Florist Id is null");
                return;
            }
    
            // get number of days to block from secure context
            
            String blockDays = configUtil.getFrpGlobalParm(MercuryConstants.SECURE_CONFIG_CONTEXT,MercuryConstants.SECURE_CONFIG_FIELD_FLORIST_SOFT_BLOCK_DAYS);
            int softBlockDays = Integer.parseInt(blockDays);
            
            if(softBlockDays <= 0) {
                // soft block disabled
                logger.info("Soft Block Disabled. Florist " + floristId + " is active.");
                return;
            }

            // calculate block end date
            // get today calendar object. time set to 6am
            Calendar now = Calendar.getInstance();
            
            // get today at 6am
            Calendar blockEndDate = new GregorianCalendar(now.get(Calendar.YEAR),now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH),6,00 );
    
            // calculate end date. today + soft block days = expiry date 6am
            blockEndDate.add(Calendar.DAY_OF_YEAR, softBlockDays);
    
            floristVO.setBlockEndDate(blockEndDate.getTime());
            
            // acquire lock
            lockHelper.requestMaintenanceLock(floristVO, floristVO.getBlockedByUserId());
    
            // update florist to database if lock is still present
            if (floristVO.getLockedFlag() != null && 
                floristVO.getLockedFlag().equals("Y")) {
                lockAcquired = true;
                // update florist profile
                FloristMaintenanceDAO fmDAO = new FloristMaintenanceDAO(conn);
                fmDAO.updateFloristBlocks(floristVO, "Y");
                logger.info(" Florist " + floristId + " soft blocked for " + softBlockDays + " days by SYS. Block expires on " + floristVO.getBlockEndDate().toString());
            }
        }catch(Exception e) {
            logger.error("Not able to soft block florist " + floristId);
            logger.error(e);
        }
        finally {
            // release lock
            try {
                if(lockAcquired == true) {
                    lockHelper.releaseMaintenanceLock(floristVO.getFloristId(), floristVO.getBlockedByUserId());
                }
            }catch(Exception e) {
                logger.error("Release lock failed " + floristId);
                logger.error(e);
            }
        }
    }
    
    /**
     * Send CAN message.
     * @param message
     * @throws Exception
     */
    public void sendCancelMessage(MercuryVO message) throws Exception {
    	this.isMessageCancled=false;
        CANMessageTO cancelMsg = createCANMessageTO(message);
        MessagingService ms = new MessagingService();
        ResultTO result=ms.getMercuryAPI().sendCANMessage(cancelMsg, conn);
        if(result != null && result.isSuccess()){
        	this.isMessageCancled=true;
        }
    }
    
    /**
     * Send ANS message.
     * @param message
     * @throws Exception
     */
    public void sendAnswerMessage(MercuryVO message) throws Exception {
        ANSMessageTO answerMsg = createANSMessageTO(message);
        MessagingService ms = new MessagingService();
        ResultTO result=ms.getMercuryAPI().sendANSMessage(answerMsg, conn);
    }    
    
    /**
     * Create CAN message transmission object.
     * @param message
     * @return
     * @throws Exception
     */
    public CANMessageTO createCANMessageTO(MercuryVO message) throws Exception {

        CANMessageTO canTO = new CANMessageTO();
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createCANMessageTO");
        }

        try {
            MercuryVO ftdMessage = null;
            MercuryDAO mercuryDao = new MercuryDAO(conn);
            
            // get the associated FTD
            ftdMessage = mercuryDao.getAssociatedFtd(message);
            String operator = "OP";
            String comments = MercuryConstants.AUTO_CANCEL_MESSAGE_COMMENT_TEXT;
            String sendingFlorist = ftdMessage.getSendingFlorist();
            String ftdMessageID = ftdMessage.getMercuryId();

            canTO.setMercuryId(ftdMessageID);
            canTO.setComments(comments);
            canTO.setOperator(operator);
            canTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            canTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            canTO.setFillingFlorist(null);
            canTO.setSendingFlorist(sendingFlorist);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createCANMessageTO");
            }
        }
        return canTO;
    }
    
    /**
     * Create ANS message transmission object for ASPI within price increase limit.
     * @param message
     * @return
     * @throws Exception
     */
    public ANSMessageTO createANSMessageTO(MercuryVO message) throws Exception {

        ANSMessageTO ansTO = new ANSMessageTO();
        if (logger.isDebugEnabled()) {
            logger.debug("Entering createANSMessageTO");
        }

        try {
            MercuryVO ftdMessage = null;
            MercuryDAO mercuryDao = new MercuryDAO(conn);
            
            // get the associated FTD
            ftdMessage = mercuryDao.getAssociatedFtd(message);
            String operator = "OP";
            String comments = MercuryConstants.AUTO_ANSWER_MESSAGE_COMMENT_TEXT + 
                                " PRICE WILL CHANGE FROM " + ftdMessage.getPrice() + " TO " + message.getPrice() + ".";
            String sendingFlorist = ftdMessage.getSendingFlorist();
            String ftdMessageID = ftdMessage.getMercuryId();

            ansTO.setMercuryId(ftdMessageID);
            ansTO.setComments(comments);
            ansTO.setOperator(operator);
            ansTO.setMercuryStatus(BaseMercuryMessageTO.MERCURY_OPEN);
            ansTO.setOldPrice(ftdMessage.getPrice());
            ansTO.setPrice(message.getPrice());
            ansTO.setAskAnswerCode("P");
            ansTO.setDirection(BaseMercuryMessageTO.OUTBOUND);
            ansTO.setFillingFlorist(message.getSendingFlorist());
            ansTO.setSendingFlorist(sendingFlorist);
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting createANSMessageTO");
            }
        }
        return ansTO;
    }    
    
    /**
     * Creates an order comment.
     * @param comment
     * @param orderDetailVO
     * @throws Exception
     */
    private void createOrderComment(String comment, OrderDetailVO orderDetailVO)
        throws Exception
    {
            OrderDAO orderDAO = new OrderDAO(conn);    
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(MercuryConstants.OPERATOR_OP);
            commentsVO.setCommentType(MercuryConstants.COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(MercuryConstants.OPERATOR_OP);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(commentsVO);
    }    
    
    /**
     * Add hour, minute and addMinute. 
     * hour: integer between 0 and 23
     * minute: integer between 0 and 59
     * addMinute: integer
     *
     * returns time in the format of xxxx, where the first 2 digits are current hour (between 0 and 23); the last 2 digits
     * are current minutes (between 0 and 59).
     * @param hour
     * @param minute
     * @param addMinute
     * @return
     * @throws Exception
     */
     private static int addTime(int hour, int minute, int addMinute) throws Exception
     {
         int mTotal = minute + addMinute;
         int cHour = 0;
         int cMinute = 0;
         int tHour = 0;
         int retTime = 0;
         
         if(mTotal >= 60) {
             cHour = mTotal/60;
             cMinute = mTotal%60;
         } else {
             cMinute = mTotal;
         }
         
         tHour = hour + cHour;
         retTime = tHour * 100 + cMinute;
         
         logger.info("addTime returning:" + retTime);
         return retTime;
         
     }
    
    private static boolean isDeliveryConfirmation(String comments, MercuryDAO mercuryDAO) throws Exception {
        boolean response = mercuryDAO.isDeliveryConfirmationText(comments);
        logger.debug("isDeliveryConfirmation: " + response + " " + comments);
        return response;
    }
    
    public HashMap insertOrderExceptions(Connection qConn, String masterOrderNumber, String context,
            String errorMessage) throws Exception {

            logger.debug("insertOrderExceptions(" + masterOrderNumber + ")");
            
            /* build DataRequest object */
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(qConn);
            dataRequest.setStatementID("INSERT_ORDER_EXCEPTIONS");
            Map inputParams = new HashMap();
            inputParams.put("IN_CONTEXT", context);
            inputParams.put("IN_MESSAGE", masterOrderNumber);
            inputParams.put("IN_ERROR", errorMessage);
            dataRequest.setInputParams(inputParams);

            /* execute the stored procedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
            HashMap outputMap = (HashMap) dataAccessUtil.execute(dataRequest);

            return outputMap;

   }
}
