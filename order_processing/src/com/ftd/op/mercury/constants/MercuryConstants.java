package com.ftd.op.mercury.constants;

public class MercuryConstants {

    public static final String CONFIG_FILE = "order-processing-config.xml";
    
    
    // Secure Configuration Context
    public static final String SECURE_CONFIG_CONTEXT = "ORDER_PROCESSING";
    public static final String CONFIG_CONTEXT = "ORDER_PROCESSING";
    public static final String SECURE_CONFIG_FIELD_FLORIST_SOFT_BLOCK_DAYS = "FLORIST_SOFT_BLOCK_DAYS";
    
    // florist block constants
    public static final String FLORIST_BLOCK_USER_ID = "SYS";
    public static final String FLORIST_SOFT_BLOCK_TYPE = "S";
    
    // Auto response to ASPI
    public static final String OPERATOR_OP = "OrderProc";
    public static final String COMMENT_TYPE_ORDER = "Order";
    public static final String AUTO_CANCEL_MESSAGE_COMMENT_TEXT = "PLEASE CANCEL ORDER.";
    public static final String AUTO_ANSWER_MESSAGE_COMMENT_TEXT = "YOUR REQUEST HAS BEEN APPROVED.";
    public static final String ASKP_AUTO_CANCEL_ORDER_COMMENT_TEXT = "System responded automatically to ASKP with CAN. Order is cancelled and will attempt to send new FTD";
    public static final String ASKP_AUTO_CANCEL_ORDER_NO_NEW_FTD_COMMENT_TEXT = "System responded automatically to ASKP with CAN. Order is cancelled";
    public static final String ASKP_AUTO_ANSWER_ORDER_COMMENT_TEXT = "System responded automatically to ASKP with ANS. Price change request has been approved.";
    public static final String AUTO_RETRY_ORDER_COMMENT_TEXT_1 = "System responded automatically to message type ";
    public static final String AUTO_RETRY_ORDER_COMMENT_TEXT_2 = " containing key phrase \"";
    public static final String AUTO_RETRY_ORDER_COMMENT_TEXT_3 = "\". System will attempt to send another FTD message.";
    
    // Global parm for auth ANSP threshold
    public static final String ASK_MESSAGE_CONTEXT = "ASK_MESSAGE_EVENT";
    public static final String AUTO_ANSP_LIMIT = "AUTO_ANSP_LIMIT";
    
    public static final String AUTO_RESPONSE_ON_REJ = "AUTO_RESPONSE_ON_REJ";
    public static final String AUTO_RESPONSE_ON_ASK = "AUTO_RESPONSE_ON_ASK";
    public static final String AUTO_RESPONSE_OFF_BEFORE_CUTOFF_MINUTES = "AUTO_RESPONSE_OFF_BEFORE_CUTOFF_MINUTES";
    
    public static final String ASK_ANS_CODE_C = "C";
    
}
