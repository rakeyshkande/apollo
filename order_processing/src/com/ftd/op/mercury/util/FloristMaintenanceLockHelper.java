package com.ftd.op.mercury.util;

import com.ftd.op.order.vo.FloristVO;

import java.sql.Connection;

import java.util.Map;


import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

public class FloristMaintenanceLockHelper {
    private Connection connection;
    private Logger logger;
    
    public FloristMaintenanceLockHelper(Connection connection)
    {
        this.connection = connection;
        this.logger = new Logger("com.ftd.op.mercury.util.FloristMaintenanceLockHelper");
    }
    
    public void requestMaintenanceLock(FloristVO florist, String userId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("OP_UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", florist.getFloristId());
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "Y");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equals("Y"))
        {
            // Record updated and available
            florist.setLockedByUser(userId);
            florist.setLockedFlag("Y");
        }
        else
        {
            // Record already locked by another user
            String lockedByUser = (String) outputs.get("OUT_LOCKED_BY");
            florist.setLockedByUser(lockedByUser);
            florist.setLockedFlag("N");
        }
    }
    
    public void releaseMaintenanceLock(String floristId, String userId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        dataRequest.setStatementID("OP_UPDATE_FLORIST_LOCK");
        dataRequest.addInputParam("IN_FLORIST_ID", floristId);
        dataRequest.addInputParam("IN_LOCK_INDICATOR", "N");
        dataRequest.addInputParam("IN_LOCKED_BY", userId);
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    }
}