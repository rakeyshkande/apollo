package com.ftd.op.mercury.util;
import com.ftd.op.common.service.QueueService;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.service.MercuryOutboundService;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.vo.RWDFloristVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.util.DataAccessEntityResolver;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;



import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Caches a list of proxy florists from the file system, allowing callers to 
 * request a random proxy.
 * 
 * Implements the singleton interface
 */
public class ProxyFlorist 
{
  private class Response 
  {
    String response;
    String type;
    int frequency;
    public Response()
    {
    }
    public void setResponse(String responseString)
    {
      response = responseString;
    }
    public void setType(String typeString) 
    {
      type = typeString;
    }
    public void setFrequency(String frequencyString)
    {
      frequency = new Integer(frequencyString).intValue();
    }
    public String getType()
    {
      return type;
    }
    public String getResponse()
    {
      return response;
    }
    public int getFrequency()
    {
      return frequency;
    }
  }
  private class Responder 
  {
    private String type;
    private List responses;
    public Responder()
    {
      responses = new LinkedList();
    }
    public void setType(String typeString)
    {
      type = typeString;
    }
    public void addResponse(Response resp)
    {
      responses.add(resp);
    }
    public List getResponses()
    {
      return responses;
    }
    public String getType()
    {
      return type;
    }
  }
  private static final String CONFIG_FILE = "order-processing-config.xml";
  private static final String PROXY_FLORIST_FILE = "proxy-florists.xml";

  private LinkedList proxyFloristList;
  private boolean proxyFlorist;
  
  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.op.mercury.util.ProxyFlorist";
  private Hashtable autoResponders;
  // Implements singleton pattern
  private static ProxyFlorist instance = null;
  
  /**
   * Accessor for a public property which toggles the proxy_florist "feature"
   * 
   * @throws java.lang.Exception
   * @return true if proxyFlorist is on
   */
  public boolean isProxyFlorist() throws Exception
  {
    ConfigurationUtil config = ConfigurationUtil.getInstance();            
    return config.getFrpGlobalParm(OrderConstants.OP_CONFIG_CONTEXT,"PROXY_FLORIST").equals("true");         
  }
  /**
   * Accessor for a public property which toggles the auto-responder "feature"
   * 
   * @throws java.lang.Exception
   * @return true if autoRespond is on
   */
  public boolean isAutoRespond() throws Exception {
    ConfigurationUtil config = ConfigurationUtil.getInstance();
    return config.getFrpGlobalParm(OrderConstants.OP_CONFIG_CONTEXT,"AUTO_RESPOND").equals("true");
  }
  
  
  /**
   * Returns the florist Id of a random florist proxy.  Selection is weighted
   * (linearly) based on the weights specified in the florist-proxy.xml file.
   * 
   * @return a random floristId
   */
  public String getRandomProxyFlorist()
  {
    // short circuit in the typical case
    if (proxyFloristList.size() == 1) 
    {
      return ((RWDFloristVO)proxyFloristList.get(0)).getFloristId();
    }
    
    int totalweight = 0;
    for(int i=0; i < proxyFloristList.size(); i++)
    {
      totalweight += ((RWDFloristVO)proxyFloristList.get(i)).getFloristWeight();
    }
    Random generator = new Random();
    int number = generator.nextInt(totalweight) + 1;
    int runningWeight = 0;
    int i;
    for(i = 0; i < proxyFloristList.size(); i++)
    {
      
      runningWeight += ((RWDFloristVO)proxyFloristList.get(i)).getFloristWeight();

      if(runningWeight >= number)
      {
        break;
      }
    }
    return ((RWDFloristVO)proxyFloristList.get(i)).getFloristId();
  }
  
  /**
   * This method is called by the local construtor to initialize the florist 
   * proxy cache from the configuration file on disk.
   * 
   * @throws java.lang.Exception
   * @return a linked list of proxy florists (RWDFloristVO)
   */
  private LinkedList initProxyFlorists() throws Exception
  {
    NodeList florists;
    DocumentBuilder builder = DOMUtil.getDocumentBuilder(false, true, true, true, false, false);
    // set the entity resolver
    builder.setEntityResolver(new DataAccessEntityResolver());

    Document doc = DOMUtil.getDocument(ResourceUtil.getInstance().getResourceFileAsStream(PROXY_FLORIST_FILE));

    // get a list of all proxy florists
    florists = doc.getElementsByTagName("florist");

    LinkedList list = new LinkedList();
    
    // read all florists into cache
    for (int i = 0; i < florists.getLength(); i++) 
    {
      List respList = new LinkedList();
      Element e = (Element)florists.item(i);
      NodeList responders = e.getElementsByTagName("responder");
      for(int j=0; j<responders.getLength(); j++) 
      {
        Responder resp = new Responder();
        Element e1 = (Element) responders.item(j);
        resp.setType(e1.getAttribute("msgType"));
        NodeList responses = e1.getElementsByTagName("response");
        int frequencyTotal = 0;
        for(int k=0; k<responses.getLength(); k++)
        {
          Element e2 = (Element) responses.item(k);
          Response r = new Response();
          r.setType(e2.getAttribute("msgType"));
          r.setResponse(e2.getAttribute("comments"));
          r.setFrequency(e2.getAttribute("frequency"));
          frequencyTotal += r.getFrequency();
          resp.addResponse(r);
        }
        
        if (frequencyTotal > 100 || frequencyTotal < 0) 
        {
          throw new Exception("Bad frequency total for responses");
        }
        respList.add(resp);
      }
      RWDFloristVO proxy = new RWDFloristVO();
      proxy.setFloristId(e.getAttribute("floristId"));
      proxy.setFloristWeight(new Integer(e.getAttribute("weight")).intValue());
      this.autoResponders.put(proxy.getFloristId(), respList);
      list.add(proxy);
    }
    return list;    
  }
  
  
  /**
   * Public interface for the singleton pattern
   * @return the singleton instance of this class
   */
  public static ProxyFlorist getInstance() 
  {
    if (com.ftd.op.mercury.util.ProxyFlorist.instance == null) 
    {
      instance = new ProxyFlorist();
    }
    return instance;
  }
  
  private ProxyFlorist() 
  {
    logger = new Logger(LOGGER_CATEGORY);
    try {
      autoResponders = new Hashtable();
      proxyFlorist = isProxyFlorist();
      if (proxyFlorist) {
        proxyFloristList = initProxyFlorists();
        if (proxyFloristList == null) throw new Exception("Null Proxy list");
        if (proxyFloristList.size() <= 0) 
        {
          throw new Exception("Empty florist Proxy List");
        }
      }
      
    } catch (Exception e) {
      proxyFlorist = false;
      logger.warn("Couldn't set up proxy florists -- defaulting to no proxy", e);
    }
  }
  private Response pickRandomResponse(List list)
  {
    Random generator = new Random();
    int number = generator.nextInt(100) + 1;
    int runningWeight = 0;
    int i;
    boolean found = false;
    for(i = 0; i < list.size(); i++)
    {
      Response r = (Response) list.get(i);
      runningWeight += r.getFrequency();

      if(runningWeight >= number)
      {
        return r;
      }
    }
    return null;
  }
  public Response determineResponse(String florist, String messageType) 
  {
      List responders = (List)autoResponders.get(florist);
      // no auto responders -- just return null
      if(responders == null) return null;
      
      Iterator iter = responders.iterator();
      while(iter.hasNext())
      {
        Responder responder = (Responder)iter.next();
        if(responder.getType().equalsIgnoreCase(messageType))
        {
          return pickRandomResponse(responder.getResponses());
        }
      }
      return null;
  }
  public void respondToMercury(Connection conn, MercuryVO mercury) throws Exception
  {
    MercuryOutboundService ms = new MercuryOutboundService(conn);
    MercuryDAO dao = new MercuryDAO(conn);
    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    QueueService qs = new QueueService(conn);
    MercuryVO originalFtd = dao.getAssociatedFtd(mercury);
    
    Response resp = determineResponse(mercury.getFillingFlorist(), mercury.getMessageType());
    if(resp != null) {
      if(resp.getType().equalsIgnoreCase("ASK"))
      {
        ASKMessageTO ask = new ASKMessageTO();
        ask.setAskAnswerCode(mercury.getAskAnswerCode());
        ask.setComments(resp.getResponse());
        ask.setMercuryId(originalFtd.getMercuryId());
        ask.setOldPrice(mercury.getOldPrice());
        ask.setDirection("INBOUND");
        ask.setFillingFlorist(mercury.getSendingFlorist() + mercury.getOutboundId() == null? "" : mercury.getOutboundId());
        ask.setMercuryStatus("MC");
        ask.setOperator("PROXY");
        ask.setPrice(mercury.getPrice());
        ask.setSendingFlorist(mercury.getFillingFlorist());
        MercuryVO outboundMerc = ms.buildASKMessage(ask);
        outboundMerc.setComments(resp.getResponse());
        outboundMerc.setFillingFlorist(mercury.getSendingFlorist() + mercury.getOutboundId() == null? "" : mercury.getOutboundId());
        outboundMerc.setMercuryOrderNumber(mercury.getMercuryOrderNumber());
        outboundMerc.setMercuryMessageNumber(mercury.getMercuryMessageNumber());
        String mercId = dao.insertMessage(outboundMerc);
        qs.sendMercuryMessageToQueue(outboundMerc);
        
      } else if (resp.getType().equalsIgnoreCase("ANS"))
      {
        ANSMessageTO ans = new ANSMessageTO();
        ans.setAskAnswerCode(mercury.getAskAnswerCode());
        ans.setComments(resp.getResponse());
        ans.setMercuryId(originalFtd.getMercuryId());
        ans.setOldPrice(mercury.getOldPrice());
        ans.setDirection("INBOUND");
        ans.setFillingFlorist(mercury.getSendingFlorist() + (mercury.getOutboundId() == null ? "" : mercury.getOutboundId()));
        ans.setMercuryStatus("MC");
        ans.setOperator("PROXY");
        ans.setPrice(mercury.getPrice());
        ans.setSendingFlorist(mercury.getFillingFlorist());
        
        MercuryVO outboundMerc = ms.buildANSMessage(ans);
        outboundMerc.setComments(resp.getResponse());
        outboundMerc.setFillingFlorist(mercury.getSendingFlorist() + (mercury.getOutboundId() == null ? "" : mercury.getOutboundId()));
        outboundMerc.setMercuryOrderNumber(mercury.getMercuryOrderNumber());
        outboundMerc.setMercuryMessageNumber(mercury.getMercuryMessageNumber());
        String mercId = dao.insertMessage(outboundMerc);
        qs.sendMercuryMessageToQueue(outboundMerc);
        
      } else if (resp.getType().equalsIgnoreCase("REJ"))
      {
        MercuryVO outboundMerc = new MercuryVO();
        outboundMerc.setMercuryOrderNumber(mercury.getMercuryOrderNumber());
        outboundMerc.setMercuryMessageNumber(mercury.getMercuryMessageNumber());
        outboundMerc.setMessageType("REJ");
        outboundMerc.setMercuryStatus("MC");
        outboundMerc.setSendingFlorist(mercury.getFillingFlorist().substring(0,7));
        outboundMerc.setFillingFlorist(mercury.getSendingFlorist() + (mercury.getOutboundId() == null ? "" : mercury.getOutboundId()));
        outboundMerc.setRecipient(mercury.getRecipient());
        outboundMerc.setAddress(mercury.getAddress());
        outboundMerc.setCityStateZip(mercury.getCityStateZip());
        outboundMerc.setDeliveryDateText(formatter.format(mercury.getDeliveryDate()));
        outboundMerc.setOperator("PROXY " + new Timestamp(System.currentTimeMillis()) + " MERC ID" + outboundMerc.getFillingFlorist());
        outboundMerc.setComments(resp.getResponse());
        outboundMerc.setRequireConfirmation("N");
        outboundMerc.setSuffix(mercury.getSuffix());
        outboundMerc.setDirection("INBOUND");
        
        String mercId = dao.insertMessage(outboundMerc);
        outboundMerc.setMercuryId(mercId);
        qs.sendMercuryMessageToQueue(outboundMerc);
      }
    }      
  }
  public static void main(String[] args)
  {
    ProxyFlorist fl = ProxyFlorist.getInstance();
    Response r = fl.determineResponse("90-8590AA", "ASK");
    if(r != null) {
      System.out.println(r.getType() + " : " + r.getResponse());
    } else 
    {
      System.out.println("Null response");
    }
  }
}