package com.ftd.op.mercury.mdb;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.mercury.service.MercuryInboundService;
import com.ftd.osp.utilities.plugins.Logger;
import java.sql.Connection;
import javax.ejb.MessageDrivenBean;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import javax.jms.TextMessage;
import javax.sql.DataSource;

public class InboundMDB implements MessageDrivenBean, MessageListener 
{
  private static final long serialVersionUID = 1L;
  private MessageDrivenContext context;
  private static String LOGGER_CATEGORY = "com.ftd.op.mercury.mdb.InboundMDB";
  private Logger logger;
  private DataSource db;
  
	public void ejbCreate() {
	}

	public void ejbRemove() {
	}

	public void setMessageDrivenContext(MessageDrivenContext ctx) 
	{
		this.context = ctx;
		logger = new Logger(LOGGER_CATEGORY);
		try {
			db = CommonUtils.getDataSource();
		} catch (Throwable t) {
			String message = "Error starting OP Mercury inbound.";
			logger.error(message, t);
			try {
				CommonUtils.sendSystemMessage(message + " : " + t.toString());
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	public void onMessage(Message msg) 
	{
		// get message and extract MercuryId
		TextMessage textMessage = (TextMessage) msg;
		try {
			String mercuryId = textMessage.getText();
			logger.info("Message received:" + mercuryId);
			this.processMercuryMessage(mercuryId);
		} catch (JMSException e) {
			logger.error(e);
		}
	}

  private void processMercuryMessage(String mercuryId)
  {
		Connection conn = null;
	    try 
	    {   
	     conn = db.getConnection();
	     logger.info("Processing Message Id:" + mercuryId);      
	     new MercuryInboundService(conn).processMercuryInboundMessage(mercuryId);
	     logger.info("Message sent for processing.");
	    } 
	    catch (Throwable ex) 
	    { 
	      logger.error(ex);
	      String message = "Error processing MercuryId("+mercuryId+")  :" + ex.toString();

	      try{
	        CommonUtils.sendSystemMessage(message);
	      }
	      catch(Exception e)
	      {
	        logger.error("Could not send system message:" + e.toString());
	      }
	    } finally 
	    {
	      try 
	      {
	        if (conn != null) conn.close();
	      } catch (Exception e) {
	        // EMPTY
	      }
	    }
	}
}