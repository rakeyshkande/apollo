package com.ftd.op.mercury.bo;

import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.BaseMercuryMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.osp.mercuryinterface.constants.MercuryConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;

import javax.rmi.PortableRemoteObject;


public class MercuryHandler {
    private static Logger logger = new Logger(MercuryHandler.class.getName());

    public static BaseMercuryMessageTO getTOInstance(String messageType) {
        // Determine which transfer object to instantiate.
        BaseMercuryMessageTO messageTO = null;

        if (messageType.equalsIgnoreCase(MercuryConstants.ADJ_MESSAGE)) {
            return new ADJMessageTO();
        } else if (messageType.equalsIgnoreCase(MercuryConstants.ANS_MESSAGE)) {
            return new ANSMessageTO();
        } else if (messageType.equalsIgnoreCase(MercuryConstants.ASK_MESSAGE)) {
            return new ASKMessageTO();
        } else if (messageType.equalsIgnoreCase(MercuryConstants.CAN_MESSAGE)) {
            return new CANMessageTO();
        } else if (messageType.equalsIgnoreCase(MercuryConstants.FTD_MESSAGE)) {
            return new FTDMessageTO();
        } else if (messageType.equalsIgnoreCase(MercuryConstants.GEN_MESSAGE)) {
            return new GENMessageTO();
        } else {
            throw new RuntimeException(
                "getTOInstance: Message type is unrecognized: " + messageType);
        }
    }

    public static ResultTO process(BaseMercuryMessageTO messageTO, Connection connection)
        throws Exception {
        InitialContext jndiContext = new InitialContext();

        MercuryAPIBO api = new MercuryAPIBO();

        ResultTO result = null;
        
        logger.info("Received Message: " + messageTO.getMessageType());

        if (messageTO instanceof com.ftd.op.mercury.to.ADJMessageTO) {
            result = api.sendADJMessage((ADJMessageTO) messageTO, connection);
        } else if (messageTO instanceof com.ftd.op.mercury.to.ANSMessageTO) {
            result = api.sendANSMessage((ANSMessageTO) messageTO, connection);
        } else if (messageTO instanceof com.ftd.op.mercury.to.ASKMessageTO) {
            result = api.sendASKMessage((ASKMessageTO) messageTO, connection);
        } else if (messageTO instanceof com.ftd.op.mercury.to.CANMessageTO) {
            result = api.sendCANMessage((CANMessageTO) messageTO, connection);
        } else if (messageTO instanceof com.ftd.op.mercury.to.FTDMessageTO) {
            result = api.sendFTDMessage((FTDMessageTO) messageTO, connection);
        } else if (messageTO instanceof com.ftd.op.mercury.to.GENMessageTO) {
            result = api.sendGENMessage((GENMessageTO) messageTO, connection);
        } else {
            throw new RuntimeException(
                "process: Message class is unrecognized: " +
                messageTO.getClass().getName());
        }

        return result;
    }

    public static Map composePayloadMap(String message) {
        String[] tokens = message.split("|");
        Map result = new HashMap(tokens.length);

        for (int x = 0; x < tokens.length; x++) {
            // Populate each name-value pair within a Map.
            result.put(tokens[x], tokens[x++]);
        }

        return result;
    }
}
