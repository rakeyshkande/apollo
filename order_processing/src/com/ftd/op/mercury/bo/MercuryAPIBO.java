package com.ftd.op.mercury.bo;

import com.ftd.op.common.to.ResultTO;
import com.ftd.op.mercury.facade.MercuryFacade;
import com.ftd.op.mercury.to.ADJMessageTO;
import com.ftd.op.mercury.to.ANSMessageTO;
import com.ftd.op.mercury.to.ASKMessageTO;
import com.ftd.op.mercury.to.CANMessageTO;
import com.ftd.op.mercury.to.CalculatedFieldsTO;
import com.ftd.op.mercury.to.FTDMessageTO;
import com.ftd.op.mercury.to.GENMessageTO;
import com.ftd.op.mercury.to.OrderDetailKeyTO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;


/**
 * Provides a BO Implementation for utilizing the Mercury API as a Library.
 */
public class MercuryAPIBO
{
  /**
   * Logger instance for the Class
   */
  Logger logger = new Logger ("com.ftd.op.mercury.bo.MercuryAPIBO");
  
  
   
  /**
  * Sends a FTD message for the specified order to the Mercury Outbound Queue.
  * 
  * @param order The order to send the message for
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendFTDMessage(OrderDetailKeyTO order, Connection conn)
  {
    ResultTO result = new ResultTO();

    try 
    {
      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendFTDMessage(order);
      result.setKey(messageKey);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }

  /**
  * Sends a FTD message for the specified order and florist to the Mercury Outbound Queue.
  * 
  * @param order The order to send the message for
  * @param floristId the ID of the florist to send the message for
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendFTDMessage(OrderDetailKeyTO order, String floristId, Connection conn)
  {
    ResultTO result = new ResultTO();
    
    try 
    {
      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendFTDMessage(order, floristId);
      result.setKey(messageKey);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }


  /**
  * Sends a FTD message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendFTDMessage(FTDMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();

    try 
    {

      if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
      {
        throw new Exception("Sending Florist does not match input pattern (99-9999)");
      }
      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendFTDMessage(message);
      result.setKey(messageKey);
    } catch (Exception e) 
    {
      logger.error(e);
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }

  /**
  * Sends a ADJ message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendADJMessage(ADJMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();
    
      try
      {
        if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
        {
          throw new Exception("Sending Florist does not match input pattern (99-9999)");
        }
        MercuryFacade mf = new MercuryFacade(conn);
        String messageKey = mf.sendADJMessage(message);
        result.setKey(messageKey);
      }
      catch (Exception e)
      {
        logger.error(e);        
        result.setErrorString(e.getMessage());
        result.setSuccess(false);
      } 
      
      return result;
  }

  /**
  * Sends a ANS message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendANSMessage(ANSMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();
    
    try {
      if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
      {
        throw new Exception("Sending Florist does not match input pattern (99-9999)");
      }

      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendANSMessage(message);
      result.setKey(messageKey);
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }

  /**
  * Sends a ASK message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendASKMessage(ASKMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();
    
    try {
      if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
      {
        throw new Exception("Sending Florist does not match input pattern (99-9999)");
      }

      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendASKMessage(message);
      result.setKey(messageKey);
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }

  /**
  * Sends a CAN message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendCANMessage(CANMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();
    
    try {
      if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
      {
        throw new Exception("Sending Florist does not match input pattern (99-9999)");
      }

      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendCANMessage(message);
      result.setKey(messageKey);
    }
    catch (Exception e)
    {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  }

  /**
  * Sends a GEN message to the Mercury Outbound Queue.
  * 
  * @param message The message to send.
  * @param conn Connection to the database
  * @return ResultTO Contains the key for the message that was queued. Otherwise, contain an error string.
  */
  public ResultTO sendGENMessage(GENMessageTO message, Connection conn)
  {
    ResultTO result = new ResultTO();
    
    try {
      if(!message.getSendingFlorist().matches("[0-9][0-9]-[0-9]+"))
      {
        throw new Exception("Sending Florist does not match input pattern (99-9999)");
      }
      MercuryFacade mf = new MercuryFacade(conn);
      String messageKey = mf.sendGENMessage(message);
      result.setKey(messageKey);
    } catch (Exception e) {
      logger.error(e);        
      result.setErrorString(e.getMessage());
      result.setSuccess(false);
    } 
    
    return result;
  } 

  /**
  * Retrievs the Calcilated Fields for the specified order.
  * 
  * @param order The order to retrieve calculated fields for.
  * @param conn Connection to the database
  * @return CalculatedFieldsTO containing the calculated fields for the order or <code>null</code> if there was an exception. 
  */
  public CalculatedFieldsTO getCalculatedFields(OrderDetailKeyTO order, Connection conn)
  {

    CalculatedFieldsTO fields = null;
    
    try 
    {
      MercuryFacade mf = new MercuryFacade(conn);
      fields = mf.getCalculatedFields(order); 
    } catch (Exception e) 
    {
      logger.error(e);
      fields = null;
    } 
    return fields;
  }


}
