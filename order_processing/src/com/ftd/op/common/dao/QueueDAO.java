package com.ftd.op.common.dao;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.op.common.vo.QueueVO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;

/**
 * 
 * @author Jason Weiss
 */
public class QueueDAO 
{
  private Connection conn;
  private Logger logger;
  
  private static final String LOGGING_CONTEXT = "com.ftd.op.order.dao.QueueDAO";
  
  // stored procedure strings
  private static final String QUEUE_TYPE = "IN_QUEUE_TYPE";
  private static final String MESSAGE_TYPE = "IN_MESSAGE_TYPE";
  private static final String MESSAGE_TIMESTAMP = "IN_MESSAGE_TIMESTAMP";
  private static final String SYSTEM = "IN_SYSTEM";
  private static final String MERCURY_NUMBER = "IN_MERCURY_NUMBER";
  private static final String MASTER_ORDER_NUMBER = "IN_MASTER_ORDER_NUMBER";
  private static final String ORDER_GUID = "IN_ORDER_GUID";
  private static final String ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
  private static final String EXTERNAL_ORDER_NUMBER = "IN_EXTERNAL_ORDER_NUMBER";
  private static final String POC_ID = "IN_POINT_OF_CONTACT_ID";
  private static final String EMAIL_ADDRESS = "IN_EMAIL_ADDRESS";
  private static final String MERCURY_ID = "IN_MERCURY_ID";
  
  private static final String INSERT_QUEUE_RECORD = "OP_INSERT_QUEUE_RECORD";
  private static final String STATUS = "OUT_STATUS";
  
  // SP output checking strings
  private static final String NO = "N";
  private static final String ERROR_MESSAGE = "OUT_ERROR_MESSAGE";
  
  public QueueDAO(Connection c)
  {
    conn = c;
    logger = new Logger(LOGGING_CONTEXT);
  }
  public void insertQueueRecord(QueueVO queueRecord) throws Exception{
    DataRequest dataRequest = new DataRequest();
    HashMap outputs = null;
    
   
      
      /* setup stored procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(QUEUE_TYPE, new String(queueRecord.getQueueType()));
      inputParams.put(MESSAGE_TYPE, new String(queueRecord.getMessageType()));      
      java.sql.Timestamp sqlTimeStamp = new java.sql.Timestamp(queueRecord.getMessageTimestamp().getTime());      
      inputParams.put(MESSAGE_TIMESTAMP, sqlTimeStamp);
      inputParams.put(SYSTEM, queueRecord.getSystem() != null ? queueRecord.getSystem() : null);
      if(queueRecord.getMercuryNumber() != null) {
        inputParams.put(MERCURY_NUMBER, new String(queueRecord.getMercuryNumber()));
      } else 
      {
        inputParams.put(MERCURY_NUMBER, null);
      }
      inputParams.put(MASTER_ORDER_NUMBER, queueRecord.getMasterOrderNumber() != null ? queueRecord.getMasterOrderNumber() : null);
      inputParams.put(ORDER_GUID, queueRecord.getOrderGuid() != null ? queueRecord.getOrderGuid() : null);
      inputParams.put(ORDER_DETAIL_ID, queueRecord.getOrderDetailId() != null ? queueRecord.getOrderDetailId() : null);
      inputParams.put(EXTERNAL_ORDER_NUMBER, queueRecord.getExternalOrderNumber() != null ? queueRecord.getExternalOrderNumber() : null);
      inputParams.put(POC_ID, null);
      inputParams.put(EMAIL_ADDRESS, null);
      inputParams.put(MERCURY_ID, queueRecord.getMercuryId());

      logger.info("QueueDAO - calling " + INSERT_QUEUE_RECORD 
                                        + " -   QUEUE_TYPE>>"+inputParams.get(QUEUE_TYPE)
                                        + "<<   MESSAGE_TYPE>>"+inputParams.get(MESSAGE_TYPE)
                                        + "<<   SYSTEM>>"+inputParams.get(SYSTEM)
                                        + "<<   MERCURY_NUMBER>>"+inputParams.get(MERCURY_NUMBER)
                                        + "<<   MASTER_ORDER_NUMBER>>"+inputParams.get(MASTER_ORDER_NUMBER)
                                        + "<<   ORDER_GUID>>"+inputParams.get(ORDER_GUID)
                                        + "<<   ORDER_DETAIL_ID>>"+inputParams.get(ORDER_DETAIL_ID)
                                        + "<<   EXTERNAL_ORDER_NUMBER>>"+inputParams.get(EXTERNAL_ORDER_NUMBER)
                                        + "<<   POC_ID>>"+inputParams.get(POC_ID)
                                        + "<<   EMAIL_ADDRESS>>"+inputParams.get(EMAIL_ADDRESS)
                                        + "<<   MERCURY_ID>>"+inputParams.get(MERCURY_ID)      
                                        + "<<"
      );
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(INSERT_QUEUE_RECORD);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (HashMap) dataAccessUtil.execute(dataRequest);
      
      /* read stored procedure output parameters to determine 
       * if the procedure executed successfully */
      if (!outputs.get("OUT_STATUS").equals("Y"))
      {
          throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
      }

     
    }

  public void deleteUntaggedQueueMsgs(long orderDetailId, String queueType, String updatedBy) throws Exception {

      DataRequest dataRequest = new DataRequest();
      HashMap outputs = null;
        
      /* setup stored procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
      inputParams.put("IN_QUEUE_TYPE", queueType);      
      inputParams.put("IN_CSR_ID", updatedBy);
        
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("OP_DELETE_ALL_UNTAGGED_Q_RECS");
      dataRequest.setInputParams(inputParams);
        
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (HashMap) dataAccessUtil.execute(dataRequest);
        
      if (!outputs.get("OUT_STATUS").equals("Y")) {
          throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
      }
      
  }

}