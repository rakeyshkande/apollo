package com.ftd.op.common.dao;

import com.ftd.op.common.vo.AuthResponseStatsVo;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.common.vo.MilesPointsVO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


public class CommonDAO 
{

    private Connection conn;
    private Logger logger;
    private static final String INSERT_AUTH_RESP_STATS_STATEMENT = "INSERT_AUTH_RESPONSE_STATS";
    private static final String STATUS_PARAM = "OUT_STATUS";
    private static final String MESSAGE_PARAM = "OUT_MESSAGE"; 

    public CommonDAO(Connection c)
    {
      conn = c;
      logger = new Logger("com.ftd.op.common.dao.CommonDAO");
    }
    
    /*
    public String getGlobalParam(String context, String paramName)
    {
        return null;
    }
    */
    
    /**
   * Wrapper for  CLEAN.BILLING_END_OF_DAY_PKG.GET_CREDIT_CARD_INFO
   * @throws java.lang.Exception
   * @return CommonCreditCardVO
   * @param creditCardId
   */
    public CommonCreditCardVO getCreditCardVO(String creditCardId) throws Exception
    {
      CommonCreditCardVO creditCardVO = null;
      CachedResultSet crs = null;    
      logger.info(" getCreditCardVO(String creditCardId (" +creditCardId+ " )");
      try
      {
        DataRequest dataRequest = new DataRequest();
        dataRequest.reset();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("OP_GET_CREDIT_CARD_INFO");
        dataRequest.addInputParam("IN_CC_ID", creditCardId);
        crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
    
        if( crs != null )
        {
          while ( crs.next() )
          {
            creditCardVO = new CommonCreditCardVO();
            // parent class credit card settings - CreditCardVO //
            creditCardVO.setCreditCardNumber(crs.getString("CC_NUMBER"));
            creditCardVO.setAddressLine(crs.getString("ADDRESS_LINE_1"));
            // save date a yyMM which is what credit card validater is expecting //
            String tempDate = crs.getString("CC_EXPIRATION");
            if (tempDate != null) {
                SimpleDateFormat dateFormatMMDDYYY = new SimpleDateFormat("MM/yy");
                Date expirationDate = dateFormatMMDDYYY.parse(tempDate);
                SimpleDateFormat dateFormatYYMM = new SimpleDateFormat("yyMM");
                creditCardVO.setExpirationDate(dateFormatYYMM.format(expirationDate));
            }
            creditCardVO.setZipCode(crs.getString("ZIP_CODE"));
            // child class credit card settings - CommonCreditCardVO //
            creditCardVO.setCcId(crs.getString("CC_ID"));
            creditCardVO.setCreditCardType(crs.getString("CC_TYPE"));
            creditCardVO.setName(crs.getString("NAME"));
            creditCardVO.setAddress2(crs.getString("ADDRESS_LINE_2"));
            creditCardVO.setCity(crs.getString("CITY"));
            creditCardVO.setStatus(crs.getString("STATE"));
            creditCardVO.setCountry(crs.getString("COUNTRY"));
            creditCardVO.setCustomerId(crs.getString("CUSTOMER_ID"));
           
          }
        }
      }
      catch (Exception ex)
      {
        logger.info(ex);
        throw ex;
      }
      return creditCardVO;
  }
  

  /**
   * Returns the minimum authorization amount for a given payment method 
   * 
   * @throws java.lang.Exception
   * @return BigDecimal - Minimum authorization amount
   * @param paymentMethodId - Payment method id
   */
  public BigDecimal getPmtMthdMinAuthAmt(String paymentMethodId) throws Exception
  {
    logger.info(" getCCMinAuthAmt(String paymentMethodId (" +paymentMethodId+ ")");

    CachedResultSet crs = null;  
    BigDecimal minAuthAmt = null;

    try
    {
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID_SET");
      dataRequest.addInputParam("payment_method_id", paymentMethodId);
      crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

      if(crs.next())
      {
        minAuthAmt = crs.getBigDecimal("min_auth_amt");
      }
    }
    catch (Exception ex)
    {
      logger.info(ex);
      throw ex;
    }
    return minAuthAmt;
  }
  
    /*
     * Checks the cache to determine if view queue flagging is turned on
     */
    public static boolean isViewQueueFlagOn() throws Exception
    {
      GlobalParmHandler globalParamHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
      String viewQueueString = null;
      if(globalParamHandler != null)
      {
          viewQueueString = globalParamHandler.getFrpGlobalParm("LOAD_MEMBER_DATA","VIEW_QUEUE_SWITCH");
      }
      else
      {
          throw new Exception("GlobalParamHandler is null. Method=isViewQueueFlagOn");
      }

      return viewQueueString == null || viewQueueString.equals("Y") ? true : false;

    }  
    
    /**
     * Returns the next value from sequence clean.mp_internal_auth_sq
     * @return
     * @throws Exception
     */
    public String getNextInternalAuthNumber() throws Exception
    {
        logger.info("getNextInternalAuthNumber...");
        String authNumber = null;
        
        try
        {
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("OP_GET_NEXT_MP_INTERNAL_AUTH_VAL");
          authNumber = (String) DataAccessUtil.getInstance().execute(dataRequest);

          if(authNumber == null)
          {
            throw new SQLException("Fatal Error: unable to obtain internal auth.");
          }
        }
        catch (Exception ex)
        {
          logger.info(ex);
          throw ex;
        }
        return authNumber;
    }
    
    
    /**
     * Retrieves miles points payment info.
     * @param paymentId
     * @return MilesPointsVO
     * @throws Exception
     */
    public MilesPointsVO getMilesPointsVO(String orderDetailId) throws Exception
    {
        MilesPointsVO mpVO = null;
        CachedResultSet crs = null;    
        logger.info(" getMilesPointsVO(String orderDetailId (" +orderDetailId+ " )");
        try
        {
          DataRequest dataRequest = new DataRequest();
          dataRequest.reset();
          dataRequest.setConnection(conn);
          dataRequest.setStatementID("OP_GET_MILES_POINTS_PMT_INFO");
          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
          crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
        
          if( crs != null )
          {
            while ( crs.next() )
            {
              mpVO = new MilesPointsVO();
              
              mpVO.setAccountId(crs.getString("AP_ACCOUNT_TXT"));
              mpVO.setAuthNumber(crs.getString("AUTH_NUMBER"));
              mpVO.setMilesAmt(crs.getInt("MILES_AMT"));
              mpVO.setPaymentId(crs.getString("PAYMENT_ID"));
              mpVO.setPaymentType(crs.getString("PAYMENT_TYPE"));
             
            }
          }
        }
        catch (Exception ex)
        {
          logger.info(ex);
          throw ex;
        }
        return mpVO;
    } 
    
    /**
     * Util class to insert auth response statistics
     * 
     * @param authRespStatsVo
     * @param connection
     * @return
     * @throws Exception
     */
    public boolean insertAuthResponseStats(AuthResponseStatsVo authRespStatsVo, Connection connection) throws Exception {
		 if(connection == null) {
	            throw new Exception("Connection is not set");
	        }	
	        DataAccessUtil dau = DataAccessUtil.getInstance();
	        DataRequest dataRequest = new DataRequest();
	        
	        try {
	            dataRequest.setConnection(connection);
	            dataRequest.addInputParam("IN_REFERENCE_NUMBER", authRespStatsVo.getReferenceNumber());
	            dataRequest.addInputParam("IN_CREDIT_CARD_TYPE", authRespStatsVo.getCreditCardType());
	            dataRequest.addInputParam("IN_VALIDATION_STATUS_CODE", authRespStatsVo.getValidationStatusCode());
	            dataRequest.addInputParam("IN_REQUEST_XML", authRespStatsVo.getRequestXml());
	            dataRequest.addInputParam("IN_RESPONSE_XML", authRespStatsVo.getResponseXml());
	            
	            java.sql.Timestamp timestamp = new java.sql.Timestamp(authRespStatsVo.getTimeStamp().getTime());
	            dataRequest.addInputParam("IN_TIMESTAMP", timestamp);
	            
	            dataRequest.setStatementID(INSERT_AUTH_RESP_STATS_STATEMENT);
	            Map outputs = (Map) dau.execute(dataRequest);
	            dataRequest.reset();
	      
	            String status = (String) outputs.get(STATUS_PARAM);
	            String outMsg = null;
	            if(status.equals("N")) {
	                outMsg = (String) outputs.get(MESSAGE_PARAM);
	                throw new Exception(outMsg);
	            }
	        } catch(Exception ex) {
	        	logger.error("Error occurred: " + ex.getMessage());
	            throw ex;
	        }
		return true;
	}

	public CommonCreditCardVO getCardinalData(String paymentId) throws Exception {
		CommonCreditCardVO cardinalVO = null;
	      CachedResultSet crs = null;    
	      logger.info(" getCardinalData(String paymentId (" +paymentId+ " )");
	      try
	      {
	        DataRequest dataRequest = new DataRequest();
	        dataRequest.reset();
	        dataRequest.setConnection(conn);
	        dataRequest.setStatementID("OP_GET_CARDINAL_COMMERCE_DATA");
	        dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
	        crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
	    
	        if( crs != null )
	        {
	          while (crs.next() )
	          {
	        	cardinalVO = new CommonCreditCardVO();
	        	cardinalVO.setEci(crs.getString("ECI"));
	        	cardinalVO.setCavv(crs.getString("CAVV"));
	        	cardinalVO.setXid(crs.getString("XID"));
	        	cardinalVO.setUcaf(crs.getString("UCAF"));
	          }
	        }
	      }
	      catch (Exception ex)
	      {
	        logger.info(ex);
	        throw ex;
	      }
	      return cardinalVO;
	}
    
	  /**
	   * Returns the payment ext values for a given payment method 
	   * 
	   * @throws java.lang.Exception
	   * @return CachedResultSet - payment ext values
	   * @param paymentMethodId - Payment method id
	   */
	public CachedResultSet getPaymentExtByPaymentId(long paymentId) throws Exception
    {
		logger.info(" doGetPaymentExtByPaymentId(long paymentId (" +paymentId+ " )");
    	CachedResultSet rs;
        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(this.conn);
            dataRequest.addInputParam("IN_PAYMENT_ID", paymentId);
            dataRequest.setStatementID("GET_PAYMENT_EXT_BY_ID");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            
            logger.debug("Calling GET_PAYMENT_EXT_BY_ID: " + paymentId);
            rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting doGetPaymentExtByPaymentId");
            } 
        }
    	return rs;
    }
	
	    
}