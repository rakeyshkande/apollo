package com.ftd.op.common.service;

import com.ftd.op.common.framework.util.ServiceLocator;
import com.ftd.op.mercury.bo.MercuryAPIBO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * MessagingService
 * 
 * This class looks up for the service for messages transmission.
 * 
 */
public class MessagingService 
{
    private MercuryAPIBO mercuryAPIBO;
    private Logger logger;
    
    public MessagingService()
    {
      try {
        mercuryAPIBO = new MercuryAPIBO();
        logger = new Logger("com.ftd.op.mercury.dao.MercuryDAO");
      } catch (Exception e) 
      {
        System.out.println("Failed to create service locator");
      }
    }

    /**
    * @return A reference to the MercuryAPIBO
    * 
    */
    public MercuryAPIBO getMercuryAPI() {
        return mercuryAPIBO;
    }
}
