/**
 * 
 */
package com.ftd.op.common.service;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * @author kvasant
 *
 */
public class PartnerDCONService {
	
	private static final String IN_ORDER_DETAIL_ID = "IN_ORDER_DETAIL_ID";
	private static final String IN_DELIVERY_STATUS = "IN_DELIVERY_STATUS";
	private static final String IN_DELIVERY_STATUS_DATETIME = "IN_DELIVERY_STATUS_DATETIME";
	
	private static final String INSERT_PARTNER_FLORIST_DCON_DATA_STMT = "INSERT_PTN_FLORIST_DCON_DATA";
	
	private static final PartnerDCONService partnerDconService = new PartnerDCONService();
	
	public static PartnerDCONService getInstance() {
		return partnerDconService;
	}
			
	private Logger logger = new Logger(PartnerDCONService.class.getName());
	
	public void persistPartnerDconData(Long orderDetailId,String deliveryStatus, Date statusDateTime, Connection conn) throws Exception {
		
		if(logger.isDebugEnabled()) {
			logger.debug("********** Save Partner Florist Delivery confirmation data **************");
		}
						
		Map<String,Object> inputParams = new HashMap<String,Object>();
		
		inputParams.put(IN_ORDER_DETAIL_ID, orderDetailId);
		inputParams.put(IN_DELIVERY_STATUS, deliveryStatus);
		inputParams.put(IN_DELIVERY_STATUS_DATETIME, statusDateTime == null ? null : new Timestamp(statusDateTime.getTime()));
		

		DataRequest request = new DataRequest();
		request.reset();
		request.setConnection(conn);
		request.setInputParams(inputParams);
		request.setStatementID(INSERT_PARTNER_FLORIST_DCON_DATA_STMT);
		
		Map<String,Object> result;
		
		DataAccessUtil dau = DataAccessUtil.getInstance();
		result = (Map<String,Object>) dau.execute(request);
		String status = (String) result.get("OUT_STATUS");
		String message = (String) result.get("OUT_MESSAGE");
		
		if(logger.isDebugEnabled()) {
			logger.debug(message);
		}
		
		if(!"Y".equalsIgnoreCase(status)) {				
			throw new Exception(message);			
		}	
					
	}

}
