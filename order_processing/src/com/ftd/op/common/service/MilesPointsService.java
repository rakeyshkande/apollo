package com.ftd.op.common.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.milespoints.webservice.MilesPointsRequest;
import com.ftd.milespoints.webservice.PartnerResponse;
import com.ftd.milespoints.webservice.UpdateMilesResp;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.framework.util.WebServiceClientFactory;
import com.ftd.op.common.vo.MilesPointsVO;
import com.ftd.op.order.dao.OrderDAO;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.BillingRecordUtility;
import com.ftd.osp.utilities.vo.MembershipVO;


public class MilesPointsService
{
  private Logger logger;
  private Connection conn;
  // credit card validationStatus //
  public static final String MILES_APPROVED = "A";
  public static final String MILES_DECLINED= "D";
  public static final String MILES_SYSTEM_ERROR = "E";
  private static final String CONFIG_FILE = "order-processing-config.xml";
  public static final String SERVICE_CONTEXT = "SERVICE";
  
  protected WebServiceClientFactory wscf;
  protected OrderDAO orderDAO;
  
  public MilesPointsService(Connection _conn)
  {
    conn = _conn;
    wscf = new WebServiceClientFactory();
    orderDAO = new OrderDAO(conn);
    logger = new Logger("com.ftd.op.common.service.MilesPointsService");
  }
  
  /**
   * Validates a given MilesPointsVO by sending a request to miles points services to
   * check if the account has sufficient miles points. 
   * If so, it sets validationStatus to "A"; if authNumber is null then it generates a new auth
   * If not, it sets validationStatus to "D"; 
   * If it encounters an error, it sets validationStatus to "E".
   * 
   * Required fields for validating a miles points account are:
   *    accountId
   *    paymentType
   *    milesAmt
   *    authNumber
   * 
   * @throws java.lang.Exception
   * @return CommonCreditCardVO
   * @param mpVO
   */

   public MilesPointsVO validatePaymentMethod(MilesPointsVO mpVO) throws Exception
    {
        logger.info("validatePaymentMethod(MilesPointsVO mpVO)");
        try
        {
            // Utilize miles points service client to check miles.
            MilesPointsRequest mpReq = getAuthenticatedMilesPointsRequest();
            String authNumber = null;
            CommonDAO commonDAO = null;

            mpReq.setMembershipNumber(mpVO.getAccountId());
            mpReq.setMembershipType(mpVO.getPaymentType());
            mpReq.setMilesPointsRequested(mpVO.getMilesAmt());
            PartnerResponse checkMilesResult = wscf.getMilesPointsService().checkMiles(mpReq);
            
            String validationStatus = parsePartnerResponse(checkMilesResult);
            logger.info("Miles requested: " + mpReq.getMilesPointsRequested());
            logger.info("payment id: " + mpVO.getPaymentId() + ". validationStatus=" + validationStatus);
            
            
            mpVO.setValidationStatus(validationStatus);
            if (MILES_APPROVED.equals(validationStatus)) {
                // Approved. If no auth then add auth.
                authNumber = mpVO.getAuthNumber();
                if(authNumber == null || authNumber == "" ) {
                    commonDAO = new CommonDAO(conn);
                    authNumber = commonDAO.getNextInternalAuthNumber();
                    mpVO.setAuthNumber(authNumber);
                }
            } else if (MILES_DECLINED.equals(validationStatus)){
                // Declined. If has auth, then remove auth.
                if(mpVO.getAuthNumber() != null) {
                    mpVO.setAuthNumber(null);
                }
            } else {
                // MILES_SYSTEM_ERROR
                if(mpVO.getAuthNumber() != null) {
                    mpVO.setAuthNumber(null);
                }
            }
        }
        catch (Throwable ex)
        {
            logger.error(ex);
            mpVO.setValidationStatus(MILES_SYSTEM_ERROR);
            // Auth cannot be verified. Remove auth.
            if(mpVO.getAuthNumber() != null) {
                mpVO.setAuthNumber(null);
            }
        }
        return mpVO;
    }
    
    /**
     * Deduct miles by sending a request to miles points services.
     * 
     * Required fields for validating a miles points account are:
     *    accountId
     *    paymentType
     *    milesAmt
     * 
     * @param mpVO
     * @return
     * @throws Exception
     */
    public boolean deductMilesPoints(MilesPointsVO mpVO) throws Exception {
        try {
            // Utilize miles points service client to update miles.
        	MilesPointsRequest mpReq = getAuthenticatedMilesPointsRequest();
            mpReq.setMembershipNumber(mpVO.getAccountId());
            mpReq.setMembershipType(mpVO.getPaymentType());
            mpReq.setMilesPointsRequested(mpVO.getMilesAmt());
            mpReq.setPaymentId(mpVO.getPaymentId());
            UpdateMilesResp resp = wscf.getMilesPointsService().updateMiles(mpReq);
            
            logger.info("deductMiles Miles requested: " + mpReq.getMilesPointsRequested());
            logger.info("payment id: " + mpVO.getPaymentId() + ". confirmation:=" + resp.getExternalConfirmationNumber());

            MembershipVO mvo = new MembershipVO();
            
            mvo.setMembershipNumber(mpVO.getAccountId());
            mvo.setMembershipType(mpVO.getPaymentType()); 
            mvo.setPaymentId(mpVO.getPaymentId());
            mvo.setMilesPointsRequested(mpVO.getMilesAmt());
            mvo.setReturnBooleanResult(resp.getReturnBooleanResult().equals("Y")? true : false);
            
            // Create billing info record
            BillingRecordUtility billingRecordUtil = new BillingRecordUtility();
            billingRecordUtil.createBillingRecord(conn, mvo);
            
            //update payment record
            orderDAO.doUpdateApCaptureTxt(conn, mpVO.getPaymentId(), resp.getExternalConfirmationNumber());
            
            if (resp.getReturnBooleanResult().equals("Y")) {        
            	return true;
            } else {
	        	logger.error("failed to deduct miles for paymentID: " + mpVO.getPaymentId());
	        	return false;
	        }
        } catch (Throwable t) {
        	//Any errors from the United side should result in an exception here.
        	// A system message should already have been sent, so we just swallow it and move on.
            logger.error(t);
            return false;
        }
    }
    
    /**
     * Check miles by sending a request to miles points services.
     * 
     * Required fields for validating a miles points account are:
     *    accountId
     *    paymentType
     *    milesAmt
     * 
     * @param mpVO
     * @return
     * @throws Exception
     */
    public boolean checkMilesPoints(MilesPointsVO mpVO) throws Exception {
        String validationStatus = null;
        try {
            // Utilize miles points service client to check miles.

        	MilesPointsRequest mpReq = getAuthenticatedMilesPointsRequest();
            mpReq.setMembershipNumber(mpVO.getAccountId());
            mpReq.setMembershipType(mpVO.getPaymentType());
            mpReq.setMilesPointsRequested(mpVO.getMilesAmt());
            PartnerResponse resp = wscf.getMilesPointsService().checkMiles(mpReq);
            validationStatus = parsePartnerResponse(resp);

        } catch (Throwable t) {
            logger.error(t);
        }
        return MILES_APPROVED.equals(validationStatus);
    } 
    
    
    private MilesPointsRequest getAuthenticatedMilesPointsRequest() throws IOException, ParserConfigurationException, SAXException, SQLException, Exception {
    	MilesPointsRequest req = new MilesPointsRequest();
    	ConfigurationUtil util = new ConfigurationUtil();
    	req.setClientUserName(util.getSecureProperty(SERVICE_CONTEXT, "SVS_CLIENT"));
    	req.setClientPassword(util.getSecureProperty(SERVICE_CONTEXT, "SVS_HASHCODE"));
    	return req;
    }
    
    protected String parsePartnerResponse(PartnerResponse prvo) throws Throwable
    {
        String returnVal = "";
        if (prvo != null) {

            boolean conSuccess = "Y".equals(prvo.getConSuccess());
            boolean milesVerify = "Y".equals(prvo.getMilesVerify());
            String errorCode = prvo.getAuthErrorCode();
            int ec = 0;
            
            if(milesVerify) {
                returnVal = MILES_APPROVED;
            } else {
                if(errorCode != null) {
                    try {
                        ec = Integer.parseInt(errorCode);
                    } catch (Exception e) {
                        // error code is not numeric. Consider it a system failure.
                        conSuccess = false;
                    }
                    
                    if (ec < 0) {
                        // Negative error code. Consider it a system failure.
                        conSuccess = false;
                    }
                }
                
                if(conSuccess) {
                    returnVal = MILES_DECLINED;
                } else {
                    returnVal = MILES_SYSTEM_ERROR;
                }
            }
        }
        return returnVal;
    }
}  
