package com.ftd.op.common.service;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Caches automated response criteria for inbound mercury messages
 * 
 * This class implements the singleton pattern.  Its purpose is to cache the 
 * criteria for automated responses to mercury messages, and to provide methods 
 * to test whether a particular message matches any of the specified criteria.  
 * The criteria will be stored in an XML configuration file and read upon 
 * initialization.  The criteria will be discarded and reread upon any call of 
 * the flushCache method.
 * 
 * @author Jason Weiss
 */
public class AutomatedResponse {

  static private AutomatedResponse instance = null;
  private LinkedList ftdCriteria;
  private LinkedList rejCriteria;
  static private String configurationFilename = "automated_response_config.xml";
  private Logger logger;
  static private String loggingContext = "com.ftd.op.common.service.AutomatedResponse";
  /**
   *  AutomatedRespoonse constructor.  
   *  
   *  This is private since we're implementing 
   *  the singleton pattern.
   */
  private AutomatedResponse(){
    ftdCriteria = new LinkedList();
    rejCriteria = new LinkedList();
    logger = new Logger(loggingContext);
  }
  
  /**
   * Returns the singleton AutomatedResponse instance, creating a new one if 
   * necessary.
   * @return 
   */
  public static AutomatedResponse getInstance(){
    if(instance == null){
      instance = new AutomatedResponse();
      instance.init();
    }
    return instance;
  }
  
  /**
   * Reads the configuration file and sets up the FTD and REJ response criteria 
   * caches
   */
  private void init(){
    try {               
      Document config = (Document) DOMUtil.getDocument(ResourceUtil.getInstance().getResourceFileAsStream(configurationFilename));
      NodeList nodes = (NodeList) DOMUtil.selectNodes(config, 
                              "/MercuryAutomatedResponseConfiguration/Item");
      
      for(int i = 0; i < nodes.getLength(); i++){
        Element e = (Element) nodes.item(i);
        if (e.getAttribute("type").equals("REJ")){
          rejCriteria.add(e.getAttribute("string").toUpperCase());
        } else if (e.getAttribute("type").equals("FTD")){
          ftdCriteria.add(e.getAttribute("string").toUpperCase());
        }
      }
    }
    catch (Exception e) {
      logger.error(e);
    }
  }
  
  /**
   * Tests a given string to see if it matches any of the FTD response criteria
   * 
   * @param message 
   * @return true if string matches any of the cached criteria, false otherwise
   */
  public boolean matchesFTDCriteria(String message){
  
    Iterator iter = ftdCriteria.iterator();
    try {
      while(message.toUpperCase().indexOf((String)iter.next()) < 0) {
        // empty loop
      }
      // fallout means we've found a matching string
      return true;
    } 
    catch (NullPointerException npe) {
      return false;
    }
    catch (NoSuchElementException nsee){
      return false;
    }
  }
  
  /**
   * Tests a given string to see if it matches any of the REJ response criteria
   * 
   * @param message
   * @return true if string matches any of the cached criteria, false otherwise
   */
  public boolean matchesREJCriteria(String message){
    
    Iterator iter = rejCriteria.iterator();
    try {
      while(message.toUpperCase().indexOf((String)iter.next()) < 0) {
        // empty loop
      }
      // fallout means we've found a matching string
      return true;
    } 
    catch (NullPointerException npe) {
      return false;
    }
    catch (NoSuchElementException nsee){
      return false;
    }
  }
  
  /**
   * Deletes both sets of response criteria caches causing them to be reloaded 
   * the next time they are needed.
   */
  public void flushCache(){
  
    /* TODO: validate that garbage collection works ok here */
 
    ftdCriteria = new LinkedList();
    rejCriteria = new LinkedList();
    init();
  }
  public static void main(String args[]){
    AutomatedResponse ar = com.ftd.op.common.service.AutomatedResponse.getInstance();
    
    String testString = "$";
    
    if (args.length > 0) {
      testString = args[0];
    }
    
    if(ar.matchesREJCriteria(testString)) 
    {
      System.out.println("REJ Match Found");
    }
    if(ar.matchesFTDCriteria(testString))
    {
      System.out.println("FTD Match Found");
    }
    ar.flushCache();
  }
  
}
