package com.ftd.op.common.service;

import com.ftd.op.common.dao.QueueDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.vo.QueueVO;
import com.ftd.op.mercury.dao.MercuryDAO;
import com.ftd.op.mercury.vo.MercuryVO;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.op.order.vo.ProductVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.utilities.vo.MessageToken;
import java.sql.Connection;
import java.sql.DriverManager;

import java.util.Calendar;
import java.util.Date;
import javax.naming.InitialContext;

/**
 * QueueHandler
 * 
 * This class handles the transmission of orders 
 * to various queues for additional processing.
 * 
 * @author Nicole Roberts
 */
 
public class QueueService 
{
  private OrderConstants orderConstants;
  private Logger logger;
  
  // queue names
  private static final String LP = "LP";
  private static final String FTD = "FTD";
  private static final String ZIP = "ZIP";
  private static final String HOLD = "LP";
  private static final String CREDIT = "CREDIT";

  private static final String MERCURY = "Merc";
  private static final String VENUS = "Venus";
  
  private Connection conn;
  
  /**
     * Constructor
     * 
     * @param none
     * @return n/a
     * @throws none
     */
  public QueueService(Connection c)
  {
    conn = c;
    logger = new Logger(orderConstants.LOGGER_CATEGORY_QUEUESERVICE);
  }
  
  /**
   * This method places the order in the FTD Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToFtdQueue(OrderDetailVO order) throws Exception
  {
    sendOrderToCSQueue(order, FTD);
  }
  
  /**
   * This method places the order in the ZIP Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToZipQueue(OrderDetailVO order) throws Exception
  {
    sendOrderToCSQueue(order, ZIP);
  }
  
  
  /**
   * This method places the order in the authorization Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToAuthQueue(OrderDetailVO order, boolean ignoreCustHold) throws Exception
  {
      MessageToken token = new MessageToken();
      if(ignoreCustHold) 
      {
        token.setMessage("RELEASE|" + order.getOrderDetailId());
      } else {
        token.setMessage(new String(order.getOrderDetailId() + ""));
      }
      
      // insert 30 second delay on CC_AUTH processing to prevent order_hold
      // record race condition
      token.setProperty("JMS_OracleDelay", new Integer(30).toString(), "int");
      CommonUtils.sendJMSMessage(new InitialContext(), token, "CC_AUTH_LOCATION", "CCAUTH");
  }
  
  /**
   * This method places the order in the hold Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToHoldQueue(OrderDetailVO order) throws Exception
  {
    sendOrderToCSQueue(order, HOLD);
  }
  
   /**
   * This method places the order in the credit Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToCreditQueue(OrderDetailVO order) throws Exception
  {
    sendOrderToCSQueue(order, CREDIT);
  }
  
  
  /**
   * This method places the order in the LP Queue.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToLPQueue(OrderDetailVO orderDetail) throws Exception {
    sendOrderToCSQueue(orderDetail, LP);
  }
  
  /**
   * This method places the order in the Retry Queue
   * to be retried once the order lock is freed.
   * 
   * @param order - OrderDetailVO object
   * @return none
   * @throws java.lang.Exception
   */
  public void	sendOrderToRetryQueue(OrderDetailVO order, boolean release) throws Exception
  {
      MessageToken token = new MessageToken();
      token.setMessage(new String((release ? "RELEASE|" : "") + order.getOrderDetailId() + ""));
      // put a 30 second delay on all re-queues
      token.setProperty("JMS_OracleDelay", "30", "int");

      CommonUtils.sendJMSMessage(new InitialContext(), token, "PROCESS_ORDER_LOCATION", "PROCESSORDER");
  }
  
  /**
   * Place a given order in the passed in customer service queue
   * 
   * @param orderDetail order to enqueue
   * @param queue customer service queue
   */
  private void sendOrderToCSQueue(OrderDetailVO orderDetail, String queue) throws Exception {
    try{
      OrderDAO orderDao = new OrderDAO(conn);
      QueueDAO queueDao = new QueueDAO(conn);

      QueueVO queueItem = new QueueVO();
      OrderVO order = orderDao.getOrder(orderDetail.getOrderGuid());      
      /* throw exeption if no order record retrieved */
      if(order == null)
      {
        throw new Exception("No order record found");
      }
            
      queueItem.setQueueType(queue);
      queueItem.setMessageType(queue);
      
      // set message timestamp to now
      Calendar cal = Calendar.getInstance();
      queueItem.setMessageTimestamp(cal.getTime());
      if(orderDetail.getShipMethod() == null || orderDetail.getShipMethod().equals("SD")) {
        queueItem.setSystem(MERCURY);
      } 
      else 
      {
        //DI-144 FTDW orders going to a Queue have system of Venus, should be FTD WEST
    	ProductVO productVO = orderDao.getProduct(orderDetail.getProductId());
    	if(productVO.getShippingSystem().equalsIgnoreCase("FTD WEST"))
        {
    		queueItem.setSystem("FTD WEST");
        }
    	else
    	{
    		queueItem.setSystem(VENUS);
    	}
      }
      queueItem.setMercuryNumber(null);
      queueItem.setMasterOrderNumber(order.getMasterOrderNumber());
      queueItem.setOrderGuid(orderDetail.getOrderGuid());
      queueItem.setOrderDetailId(new Long(orderDetail.getOrderDetailId()).toString());
      queueItem.setExternalOrderNumber(orderDetail.getExternalOrderNumber());
      queueItem.setMercuryId(null);
      
      queueDao.insertQueueRecord(queueItem);
    } catch (Exception e) {
      logger.error(e);
      throw e;
    }
  }
  
  
  /**
   * Place a given order in the passed in customer service queue
   * 
   * @param message Mercury message to place in the queue
   */
  public void sendMercuryMessageToQueue(MercuryVO message) throws Exception {
    try {

      QueueVO queueItem = new QueueVO();
      OrderDAO orderDao = new OrderDAO(conn);
      QueueDAO queueDao = new QueueDAO(conn);
      MercuryDAO mercDao = new MercuryDAO(conn);
      
      MercuryVO associatedFtd = mercDao.getAssociatedFtd(message);
      if (associatedFtd == null) 
      {
        throw new Exception("No FTD associated with inbound message");
      }
      System.out.println(associatedFtd.getMercuryOrderNumber());
      OrderDetailVO orderDetail = orderDao.getOrderDetail(associatedFtd.getReferenceNumber());
      /* throw exeption if no order detail record retrieved */
      if(orderDetail == null)
      {
        throw new Exception("No order detail record found");
      }
      
      OrderVO order = orderDao.getOrder(orderDetail.getOrderGuid());
      /* throw exeption if no order record retrieved */
      if(order == null)
      {
        throw new Exception("No order record found");
      }
      
      queueItem.setQueueType(message.getMessageType());
      queueItem.setMessageType(message.getQueueMessageType());
      queueItem.setMessageTimestamp(new Date());
      queueItem.setSystem(MERCURY);
      queueItem.setMercuryNumber(message.getMercuryMessageNumber());
      queueItem.setMasterOrderNumber(order.getMasterOrderNumber());
      queueItem.setOrderGuid(orderDetail.getOrderGuid());
      queueItem.setOrderDetailId(new String(associatedFtd.getReferenceNumber()));
      queueItem.setExternalOrderNumber(orderDetail.getExternalOrderNumber());
      queueItem.setMercuryId(message.getMercuryId());
      queueDao.insertQueueRecord(queueItem);
      
    } catch (Exception e) {
      this.sendUnassociatedMercuryMessageToQueue(message);
      logger.error("Couldn't associate message to an order.", e);
    }
  }
  
  /** 
   * Used to deal with GEN messages and other messages for which there seems to be no associated order
   * 
   */
  public void sendUnassociatedMercuryMessageToQueue(MercuryVO message) throws Exception {

    QueueVO queueItem = new QueueVO();
    QueueDAO queueDao = new QueueDAO(conn);
    
    queueItem.setQueueType(message.getMessageType());
    queueItem.setMessageType(message.getQueueMessageType());
    queueItem.setMessageTimestamp(new Date());
    queueItem.setSystem(MERCURY);
    queueItem.setMercuryId(message.getMercuryId());
    queueItem.setMercuryNumber(message.getMercuryMessageNumber());
    
    queueDao.insertQueueRecord(queueItem);
  }

}