package com.ftd.op.common.service;

import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;

import com.ftd.pg.client.CreditCardClient;
import com.ftd.pg.client.creditcard.authorize.request.AuthorizeRequest;
import com.ftd.pg.client.creditcard.authorize.response.AuthorizeResponse;
import com.ftd.op.common.CreditCardApprovalUtility;
import com.ftd.op.common.JccasClient;
import com.ftd.op.common.JccasUtils;
import com.ftd.op.common.PGCCUtils;
import com.ftd.op.common.ValidateMembership;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.framework.util.CommonUtils;
import com.ftd.op.common.vo.AuthResponseStatsVo;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.common.vo.CreditCardVO;
import com.ftd.op.common.vo.jccasreq.AuthorizeCCRequest;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse.Errors;
import com.ftd.op.common.vo.jccasres.CardGrpType;
import com.ftd.op.common.vo.jccasres.RespGrp;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.GlobalParameterVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.service.util.SequenceUtils;


public class CreditCardService
{
  private Logger logger;
  private Connection conn;
  // credit card validationStatus //
  public static final String CREDIT_CARD_APPROVED = "A";
  public static final String CREDIT_CARD_DECLINED= "D";
  public static final String CREDIT_CARD_SYSTEM_ERROR = "E";
  private static final String CONFIG_FILE = "order-processing-config.xml";
  private static final String AUTH_PROVIDER = "cc.auth.provider";
  private static final String BAMS_CC_LIST = "bams.cc.list";
  private static final String JCCAS_SEQUENCE_NAME = "jccas_requestid";
  private static final String CC_MASK_VALUE = "************";
  private static final String CVV_MASK_VALUE = "***";
  private static final String AMEX_CC_ID = "AX";
  // Below constant is used to set credit card validation status on any error during jccas service call
  private static final String JCCAS_ERROR_STATUS = "ERROR";
  private static final String JCCAS_AUTH_SUCCESS_RESP_CODE = "000";  
  
  
  public CreditCardService(Connection c)
  {
    conn = c;
    logger = new Logger("com.ftd.op.common.service.CreditCardService");
  }
  /**
   * Validates a given CommonCreditCardVO.  Duplicates code in  
   * com.ftd.applications.oe.services.ValidateSVC.  Ideally, both classes will 
   * eventually use this service so the code is centralized.
   * 
   * Required fields for validating a credit card are expiration date, credit card number,
   * credit card type, amount, zip code, address line, and possibly dns type.
   * 
   * @throws java.lang.Exception
   * @return CommonCreditCardVO
   * @param creditCardVO
   */

   public CommonCreditCardVO validateCreditCard(CommonCreditCardVO creditCardVO) throws Exception
    {
        logger.debug("validateCreditCard(CommonCreditCardVO creditCardVO)");
        try
        {
            CreditCardApprovalUtility ccas = new CreditCardApprovalUtility();
            String creditCardNumber = creditCardVO.getCreditCardNumber();
            String creditCardType = creditCardVO.getCreditCardType();
            String creditCardExpDate = creditCardVO.getExpirationDate();
            String dnisType = creditCardVO.getCreditCardDnsType() == null ? "" : creditCardVO.getCreditCardDnsType();
            
            // WE CURRENTLY DO NOT CARE ABOUT ADDRESS VALIDATION 
            // DUMMY UP THE ADDRESS FIELDS IF NECESSARY
            /*if(creditCardVO.getZipCode() == null) creditCardVO.setZipCode("99999");
            if(creditCardVO.getAddressLine() == null) creditCardVO.setAddressLine(" ");
            if(creditCardVO.getCity() == null) creditCardVO.setCity(" ");
            if(creditCardVO.getCountry() == null) creditCardVO.setCountry(" ");*/
            
            logger.debug("Attempting " + creditCardVO.getCreditCardType() + " authorization for $" + creditCardVO.getAmount());
            // if it's a test credit card, mark as valid/accepted  //
            
            if (this.isTestCreditCard(creditCardVO))
            {
              creditCardVO.setStatus("S");
              creditCardVO.setActionCode("000");
              creditCardVO.setCscResponseCode((creditCardVO.getCscValue()!=null && !"".equals(creditCardVO.getCscValue())) ? "M" : "P");
              creditCardVO.setValidationStatus(CREDIT_CARD_APPROVED);
              creditCardVO.setApprovalCode("FAKE AUTH");
              logger.debug("This is a test credit card.  Skipping credit card validation...");
              return creditCardVO;
            }
            else
            {
              
              // Call CCAS if credit card number passes check digit routine
              if ( new ValidateMembership().checkCreditCard(creditCardType, creditCardNumber) )
              {
                  Date curDate = new Date();
                  Calendar expDate = Calendar.getInstance();
                  if(dnisType.equals("JP"))
                  {
                      expDate.set(Calendar.YEAR, 9999);
                  } 
                  else
                  {
                      if (creditCardVO.getCreditCardType().equals("MS"))
                      {
                          expDate.set(Calendar.YEAR, 9999);
                      }
                      else
                      {
                          if (creditCardExpDate != null)
                          {
                              // parse date string yyMM//
                              SimpleDateFormat dateFormat = new SimpleDateFormat("yyMM");
                              expDate.setTime(dateFormat.parse(creditCardExpDate));
                              // set date to the last day of the month // 
                              expDate.add(Calendar.MONTH, 1);
                              expDate.add(Calendar.DATE, -1);
                          }
                      }
                  }
                  // Check expiration date
                  if(expDate.getTime().after(curDate))
                  {
                      // Obtain minimum authorization amount for payment type
                      CommonDAO commonDAO = new CommonDAO(conn);
                      BigDecimal minAuthAmt = commonDAO.getPmtMthdMinAuthAmt(creditCardVO.getCreditCardType());
                      
                      if(logger.isDebugEnabled())
                      {
                        logger.debug("Minimum payment amount for " + creditCardVO.getCreditCardType() + " is " + minAuthAmt);
                        logger.debug("Authorization amount is " + creditCardVO.getAmount());
                      }
                      
                      // For JCPenney DNIS type, check whether credit card validation is enabled
                      if ( dnisType.equals("JP") && ! this.doJcPenneyValidation() )
                      {
                          // If credit card validation not enabled, just return 'A'
                          creditCardVO.setValidationStatus(CREDIT_CARD_APPROVED);
                          return creditCardVO;
                      }
                      /* 9/30/07 Mike Kruger - If authorization amount is below the minimum
                       * authorization amount for the payment type do not authorize, 
                       * set credit card status to approved, and set the 
                       * commonCreditCardVO.underMinAuthAmt equal to true.
                       */ 
                      else if(minAuthAmt.compareTo(new BigDecimal(creditCardVO.getAmount())) == 1)
                      {
                          if(logger.isDebugEnabled())
                            logger.debug("Authorization amount is below the minimum authorization amount.");

                          creditCardVO.setUnderMinAuthAmt(true);
                          creditCardVO.setValidationStatus(CREDIT_CARD_APPROVED);          
                          return creditCardVO;
                      }
                      else
                      {
                          // Call Florist.com credit card authorization
                          if(creditCardType.equals("MS"))
                          {
                              creditCardVO = (CommonCreditCardVO) ccas.aafesPurchaseRequest(creditCardVO);
                          }   
                          else
                          {
                        	  ConfigurationUtil cu = ConfigurationUtil.getInstance();
                        	  String bamsCCList = cu.getFrpGlobalParm(OrderProcessingConstants.AUTH_CONFIG, BAMS_CC_LIST);
                        	  String authProvider = cu.getFrpGlobalParm(OrderProcessingConstants.AUTH_CONFIG, AUTH_PROVIDER);
                        	  
                        	  if (creditCardVO.isNewRoute()) {
                        		  AuthorizeResponse response = null;
                        		  String serviceTimeout = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_SVC_TIMEOUT);
                        		  
                        		  AuthorizeRequest request = PGCCUtils.getCreditCardRequestBean((CommonCreditCardVO) creditCardVO, conn);
                        		  logger.info("PaymentGateway request:" + request); 
                       			  response = callPGAuthorizeCreditCard(creditCardVO, serviceTimeout, request, conn);
                        		  logger.info("Response from PaymentGateway:" + response);
                        		  creditCardVO.setTransactionType(OrderProcessingConstants.OE_CCAS_PURCHASE_REQUEST);
                        		  //  creditCardVO.setCcAuthProvider(response.getRoute());
								} else if (JccasUtils.checkValidBamsCC(creditCardType, bamsCCList) && "BAMS".equals(authProvider)) {
									logger.info("Calling JCCAS service for authentication through BAMS. Credit card Type: " + creditCardType);
									creditCardVO.setCcAuthProvider(authProvider);
									callJccasService(creditCardVO);
								} else {
									logger.info("Calling CCAS service for authentication. Credit card Type: " + creditCardType);
									creditCardVO.setCcAuthProvider(null);
									creditCardVO = (CommonCreditCardVO) ccas.purchaseRequest(creditCardVO, dnisType);
								}
                          }
                          /* CREDIT CARD APPROVAL - 
                           * Status of S and action code one of '000','0NC','0NE','094' 
                           * indicates credit card approval. */
                          if (  creditCardVO.getStatus().equals("S") &&
                               (creditCardVO.getActionCode().equals("000") ||
                                creditCardVO.getActionCode().equals("0NC") ||
                                creditCardVO.getActionCode().equals("0NE") ||
                                creditCardVO.getActionCode().equals("094")) )
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_APPROVED);
                              return creditCardVO;
                          }
                          /* CREDIT CARD APPROVAL - 
                           * Status of A indicates credit card approval for AAFES */
                          else if (creditCardVO.getStatus().equals("A"))
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_APPROVED);
                              creditCardVO.setVerbiage("AP");
                              return creditCardVO;
                          }
                          /* CREDIT CARD DECLINE - 
                           * Action code one of '999','800','700','600','500'
                           * or Status of 'D' (AAFES)
                           * */
                          else if ((creditCardVO.getActionCode() != null && (creditCardVO.getActionCode().equals("999") ||
                                creditCardVO.getActionCode().equals("800") ||
                                creditCardVO.getActionCode().equals("700") ||
                                creditCardVO.getActionCode().equals("600") ||
                                creditCardVO.getActionCode().equals("500"))) ||
                                creditCardVO.getStatus().equals("D"))
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_DECLINED);
                              return creditCardVO;
                          }
                          /* SYSTEM ERROR - 
                          /* Status other than 'S' when type is 'MS' indicates a system error. */
                          else if ( !creditCardVO.getStatus().equals("S") && !creditCardType.equals("MS"))
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_SYSTEM_ERROR);
                              return creditCardVO;
                          }
                          /* SYSTEM ERROR - 
                          /* Status of 'X' for AAFES. */
                          else if (creditCardVO.getStatus().equals("X"))
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_SYSTEM_ERROR);
                              return creditCardVO;
                          } 
                          /* SYSTEM ERROR for JCCAS Service */
                          else if (creditCardVO.getStatus().equals(JCCAS_ERROR_STATUS))
                          {
                        	  creditCardVO.setValidationStatus(CREDIT_CARD_SYSTEM_ERROR);
                              return creditCardVO;
                          }
                          else
                          {
                              creditCardVO.setValidationStatus(CREDIT_CARD_SYSTEM_ERROR);
                              return creditCardVO;
                          }
                      }
                  }
                  // Card has Expired // 
                  else {
                     creditCardVO.setValidationStatus(CREDIT_CARD_DECLINED);
                     creditCardVO.setActionCode("600"); // Action Code for Expired Date //
                     return creditCardVO;
                  }
                }
                // Card has an invalid number //
                else {
                     
                     creditCardVO.setValidationStatus(CREDIT_CARD_DECLINED);
                     logger.debug("Credit Card has invalid number.");
                     return creditCardVO;
                }
            }
        }
        catch (Exception ex)
        {
          logger.error(ex);
          creditCardVO.setValidationStatus(CREDIT_CARD_SYSTEM_ERROR);
        }
        return creditCardVO;
    }
   
/**
    * Call jccas client for credit card authorisation for BAMS related credit cards
    * 
    * @param creditCardVO
    * @throws Exception
    */
	private void callJccasService(CommonCreditCardVO creditCardVO) throws Exception {
		Connection jccasStatsConn = null;
		try {			
			jccasStatsConn = CommonUtils.getConnection();
			String requestId = getNextJccasRequestId();
			AuthorizeCCRequest request = JccasUtils.getJccasRequestBean(
					creditCardVO, requestId);
			String systemMessage;
			
			String tempCardnum = request.getCardNumber();
			String tempCVV = request.getCVV();
			request.setCardNumber(CC_MASK_VALUE + tempCardnum.substring(tempCardnum.length()-4, tempCardnum.length()));
			request.setCVV(CVV_MASK_VALUE);
			massageCardinalTokens(request);
			String requestXml = JccasUtils.covertToXmlString(request, AuthorizeCCRequest.class);
			logger.info("JCCAS Request: " + requestXml);
			request.setCardNumber(tempCardnum);
			request.setCVV(tempCVV);
			AuthorizeCCResponse response = null;
			
			try {
				response = JccasClient.getInstance().callJCCASApi(request, jccasStatsConn, creditCardVO.getCallingSystem());
			} catch(Exception e) {
				creditCardVO.setStatus(JCCAS_ERROR_STATUS);	
				systemMessage = "Exception occured while calling JCCAS service for request id:" + requestId + ". Setting credit card validation status as error."  + e.toString(); 
				logger.error(systemMessage , e);
				CommonUtils.sendPageSystemMessage(systemMessage);
				logJccasResponseStatistics(creditCardVO, requestId, e.toString(), requestXml, null, jccasStatsConn);
				return;
			}
			
			if (response == null) {			
				creditCardVO.setStatus(JCCAS_ERROR_STATUS);
				systemMessage = "Null response received from JCCAS service for the request id: " + requestId + ". Setting credit card validation status as error";
				logger.error(systemMessage);
				CommonUtils.sendPageSystemMessage(systemMessage);							
			} else {
				RespGrp respGroup = response.getRespGrp();
				Errors errors = response.getErrors();
				if (respGroup != null) {
					String respCode = respGroup.getRespCode(); 
					// Auth Aproved
					if (respCode.equals(JCCAS_AUTH_SUCCESS_RESP_CODE)) {
						logger.info("Response received from BAMS. Status:Auth Approved, Response code: " + respCode);
						creditCardVO.setStatus("S");
						creditCardVO.setVerbiage("AP");
						JccasUtils.populateJccasRespToCCVo(creditCardVO, response,
								requestId);
					}
					// Auth Declined				
					else {
						logger.error("Response received from BAMS. Status:Auth Declined, Response code: " + respCode);
						creditCardVO.setStatus("D");
						JccasUtils.populateJccasRespToCCVo(creditCardVO, response, requestId);
					}
				}
				else if (errors != null && errors.getError() != null) {
					com.ftd.op.common.vo.jccasres.AuthorizeCCResponse.Errors.Error error = errors
							.getError();
					creditCardVO.setStatus(JCCAS_ERROR_STATUS);
					creditCardVO.setActionCode(String.valueOf(error.getCode()));
					
					systemMessage = "Error Response received from jccas service. Setting credit card status ERROR. JCCAS Error detials: "
							+ error.getValue()
							+ ", code: "
							+ error.getCode()						
							+ ", Error Message: "
							+ error.getMessage()
							+ ", Severity: " + error.getSeverity();
					logger.error(systemMessage);
					CommonUtils.sendPageSystemMessage(systemMessage);
				}
			}			
			
			logJccasResponseStatistics(creditCardVO, requestId, null, requestXml, response, jccasStatsConn);	
			
		} catch(Exception e) {
			throw e;
		} finally {
			try {
				if(jccasStatsConn != null && !jccasStatsConn.isClosed()) {
					jccasStatsConn.close();
				}
			} catch (SQLException e) {
				logger.error("Exception/Error occured while closing the jccas stats connection: " + e.getMessage(), e);
			}
		}		

	}
	
	private AuthorizeResponse callPGAuthorizeCreditCard(CommonCreditCardVO creditCardVO, String serviceTimeout, AuthorizeRequest request, Connection conn) {
		AuthorizeResponse response = null;
		//400/404/406/415/451
		List<String> pgErrorCodes = Arrays.asList("400","404","406","415","451");
		
		try {
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			CreditCardClient creditCardClient = new CreditCardClient(serviceTimeout);
			String pgAuthURL = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE,OrderProcessingConstants.PG_SVC_URL);
			if(pgAuthURL!=null && !StringUtils.isEmpty(pgAuthURL))
			{
				pgAuthURL = pgAuthURL+ "creditcard/authorize";
			}	
   		    response = creditCardClient.authorizeCC(request,pgAuthURL);
   		    
		} catch(Exception e) {
			String systemMessage = "Exception occured while calling Payment gateway for MerchantRef id:" + request.getMerchantReferenceId() + ". Setting credit card validation status as error."  + e.getMessage(); 
			logger.error(systemMessage , e);
			//CommonUtils.sendPGPageSystemMessage(systemMessage);
			PGCCUtils.logCreditCardResponseStatistics(creditCardVO, creditCardVO.getRequestId(), e.toString(), request, null, conn);
			if (pgErrorCodes.contains(e.getCause().getMessage())) {
				creditCardVO.setStatus("D");//Declined Status
				creditCardVO.setActionCode("600"); //Declined
			}
		}
		
		if(response==null)
		{
			String systemMessage = "Received Null response form Payment gateway for MerchantRef id:" + request.getMerchantReferenceId() + ". Setting credit card validation status as error."; 
			logger.error(systemMessage);
			CommonUtils.sendPGPageSystemMessage(systemMessage);
		}else
		{	
			//Successful
			PGCCUtils.populateCreditCardRespToCCVo((CommonCreditCardVO) creditCardVO, response);
		}	
		
		PGCCUtils.logCreditCardResponseStatistics(creditCardVO, creditCardVO.getRequestId(), null, request, response, conn);
		return response;
	}
	
	/**
	 * This method massages the cardinal tags to set as empty when null
	 * 
	 */
	private void massageCardinalTokens(AuthorizeCCRequest request) {
	
	if(request.getEci() == null)
	{
		request.setEci("");
	}
	if(request.getXid() == null)
	{
		request.setXid("");
	}
	if(request.getCavv() == null)
	{
		request.setCavv("");
	}
	if(request.getUcaf() == null)
	{
		request.setUcaf("");
	}
}
	/**
	 * This method persists jccas request and response statistics
	 * 
	 * @param creditCardVO
	 * @param requestId
	 * @param errorMessage
	 * @param tempCardnum
	 * @param requestXml
	 * @param response
	 * @param startTime
	 */
	private void logJccasResponseStatistics(CommonCreditCardVO creditCardVO,
			String requestId, String errorMessage, String requestXml,
			AuthorizeCCResponse response, Connection jccasStatsConn) {
		try {
			// log request and response stats
			String responseXml = null;
			if (response != null) {
				CardGrpType cardGroup = response.getCardGrp();
				if (cardGroup != null && cardGroup.getAcctNum() != null) {
					// Auth Success or Declined case 
					String tempCardNum = cardGroup.getAcctNum();
					cardGroup.setAcctNum(CC_MASK_VALUE
							+ tempCardNum.substring(tempCardNum.length() - 4,
									tempCardNum.length()));
					responseXml = JccasUtils.covertToXmlString(response,
							AuthorizeCCResponse.class);					
					cardGroup.setAcctNum(tempCardNum);
				} else {
					// Auth error case
					responseXml = JccasUtils.covertToXmlString(response,
							AuthorizeCCResponse.class);
				}
				logger.info("JCCAS Response: " + responseXml);
			} else {
				responseXml = errorMessage;
			}

			AuthResponseStatsVo authRespStatsVo = new AuthResponseStatsVo();
			authRespStatsVo.setCreditCardType(creditCardVO.getCreditCardType());
			authRespStatsVo.setReferenceNumber(requestId);
			authRespStatsVo.setRequestXml(requestXml);
			authRespStatsVo.setResponseXml(responseXml);
			authRespStatsVo.setTimeStamp(new Date());
			authRespStatsVo.setValidationStatusCode(creditCardVO.getStatus()
					.substring(0, 1));
			CommonDAO commonDao = new CommonDAO(jccasStatsConn);
			commonDao.insertAuthResponseStats(authRespStatsVo, jccasStatsConn);
			logger.debug("Auth respose statistics are saved to database");
		} catch (Exception e) {
			logger.error(
					"Exception/Error occured while inserting auth response statistics: "
							+ e.getMessage(), e);
		} 
	}
	
	/**
	 * gets the next sequece id
	 * 
	 * @return
	 * @throws Exception
	 */
    private String getNextJccasRequestId() throws Exception {
    	SequenceUtils su = SequenceUtils.getInstance();
    	return su.getNextSequence(conn, JCCAS_SEQUENCE_NAME);    	
    }

	/**
   * This method checks the FRP.GLOBAL_PARMS for the CHECK_JC_PENNEY.  If this value is 
   * 'Y', it returns true.  Then, the method does the JcPenny validations.  
   * @throws java.lang.Exception
   * @return boolean checkJcPenny
   */
    private boolean doJcPenneyValidation() throws Exception
    {
      logger.debug("doJcPenneyValidation()");
      OrderDAO orderDAO = new OrderDAO(conn);
      GlobalParameterVO checkJcPenny = orderDAO.getGlobalParameter("FTDAPPS_PARMS", "CHECK_JC_PENNEY");
      if (checkJcPenny != null && checkJcPenny.getValue() != null && checkJcPenny.getValue().equals("Y"))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
   
   private boolean isTestCreditCard(CreditCardVO creditCardVO) throws Exception
   {
     logger.debug("isTestCreditCard(CreditCardVO creditCardVO)");
     boolean isTestCard = false;
     ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
     String flag = configUtil.getFrpGlobalParm(OrderConstants.OP_CONFIG_CONTEXT,"BIPASS_CREDIT_CARD_VALIDATION");
     ArrayList list = (ArrayList) this.getTestCreditCardList(configUtil);
     if ((flag != null) && flag.equalsIgnoreCase("Y") && (list.size() != 0))
     {
        for (int i = 0; i <list.size(); i++)
        {
          if (((String)list.get(i)).equalsIgnoreCase(creditCardVO.getCreditCardNumber()))
          {
            isTestCard = true;
          }
        }
     }
     return isTestCard;
   }
   
   private List getTestCreditCardList(ConfigurationUtil configUtil)throws Exception
   {
     logger.debug("getTestCreditCardList(ConfigurationUtil configUtil)");
     ArrayList list = new ArrayList();
     int counter = 1;
     String testCreditCard = configUtil.getProperty(CONFIG_FILE, "TEST_CREDIT_CARD_"+counter);
     while (!testCreditCard.equals(""))
     {
        list.add(testCreditCard);
        counter++;
        testCreditCard = configUtil.getProperty(CONFIG_FILE, "TEST_CREDIT_CARD_"+counter);
     }
     return list;
   }
   
}  
