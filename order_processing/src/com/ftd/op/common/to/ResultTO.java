package com.ftd.op.common.to;
import java.io.Serializable;

public class ResultTO implements Serializable
{
  private boolean success;
  private String errorString;
  private String key;
  
  public ResultTO()
  {
    success = true;
  }


  public void setSuccess(boolean success)
  {
    this.success = success;
  }


  public boolean isSuccess()
  {
    return success;
  }


  public void setErrorString(String errorString)
  {
    this.errorString = errorString;
  }


  public String getErrorString()
  {
    return errorString;
  }


  public void setKey(String key)
  {
    this.key = key;
  }


  public String getKey()
  {
    return key;
  }
  
}