/**
 * 
 */
package com.ftd.op.common;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.ftd.op.common.exception.CreditCardValidationException;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.common.vo.jccasreq.AuthorizeCCRequest;
import com.ftd.op.common.vo.jccasreq.CardTypeType;
import com.ftd.op.common.vo.jccasreq.ChannelType;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse.CardSpcGrp;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse.CardSpcGrp.CardSpcDtl;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse.CommonGrp;
import com.ftd.op.common.vo.jccasres.CardGrpType;
import com.ftd.op.common.vo.jccasres.RespGrp;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

/**
 * Utility class for JCCAS client
 * 
 * @author kvasant
 *
 */
public class JccasUtils {
	
	
	private static final Logger logger = new Logger(JccasUtils.class.getName());
	
	/**
	 * Populates jccas request bean from credicardvo bean
	 * 
	 * @param creditCardVO
	 * @param requestId
	 * @return AuthorizeCCRequest
	 */
	public static AuthorizeCCRequest getJccasRequestBean(CommonCreditCardVO creditCardVO, String requestId) {
		AuthorizeCCRequest request = new AuthorizeCCRequest();
		request.setCardNumber(creditCardVO.getCreditCardNumber());
		request.setCardType(getJccasCreditCardType(creditCardVO.getCreditCardType()));
		request.setCVV(creditCardVO.getCscValue());
		request.setBillingAddress(creditCardVO.getAddressLine());
		request.setBillingAmount(String.valueOf((int)(Double.parseDouble(creditCardVO.getAmount()) * 100)));
		request.setBillingZipCode(creditCardVO.getZipCode());
		String expiryDate = creditCardVO.getExpirationDate(); 
		if(expiryDate !=null) {
			if(expiryDate.length() == 4) {
				try {
					SimpleDateFormat sdf4 = new SimpleDateFormat("yyMM");
					Date expiryDate4 = sdf4.parse(expiryDate);
					SimpleDateFormat sdf6 = new SimpleDateFormat("yyyyMM");
					expiryDate = sdf6.format(expiryDate4);
				} catch (ParseException e) {
					logger.warn("Unable to parse the expirydate: " + expiryDate , e);				
				}
			} else if(expiryDate.length() == 6) {
				try {
					SimpleDateFormat sdf6 = new SimpleDateFormat("yyMMdd");
					Date expiryDate4 = sdf6.parse(expiryDate);
					SimpleDateFormat sdf8 = new SimpleDateFormat("yyyyMMdd");
					expiryDate = sdf8.format(expiryDate4);
				} catch (ParseException e) {
					logger.warn("Unable to parse the expirydate: " + expiryDate , e);				
				}
			}			
		}	
		request.setCardExpiryDate(expiryDate);
		request.setCompany(creditCardVO.getCreditCardDnsType());	
		request.setRequestId(requestId);
		request.setChannel(ChannelType.APOLLO);
		request.setEci(creditCardVO.getEci());
		request.setCavv(creditCardVO.getCavv());
		request.setXid(creditCardVO.getXid());
		request.setUcaf(creditCardVO.getUcaf());
		
		return request;
	}
	
	/**
	 * It validates the jccas request and throws exception in case of any validation error
	 * 
	 * @param request
	 * @throws CreditCardValidationException
	 */
	public static void validateRequest(AuthorizeCCRequest request) throws CreditCardValidationException {
		StringBuilder errorMessage = new StringBuilder();
		
		String tempCCNumber = request.getCardNumber();
        if (tempCCNumber.length() > 24)
        {
        	errorMessage.append("Invalid Credicard number;");            
        }
        
        String tempExpDate = request.getCardExpiryDate();
        if(tempExpDate == null || !(tempExpDate.length() == 6 || tempExpDate.length() == 8))
        {
        	errorMessage.append("Invalid credit card expiry date;");        	
        }
        
        String tempAmount = request.getBillingAmount();
        if (tempAmount.length() > 13)
        {
        	errorMessage.append("Invalid credit card amount;");            
        }
        
        if(errorMessage.length() > 0){
        	throw new CreditCardValidationException(errorMessage.toString());
        }
	}

	/**
	 * Credit card type to jccas card type
	 * 
	 * @param creditCardType
	 * @return
	 */
	private static CardTypeType getJccasCreditCardType(String creditCardType) {
		if(creditCardType.equals("DC")) {
			return CardTypeType.DINERS;
		} else if(creditCardType.equals("DI")) {
			return CardTypeType.DISCOVER;
		} else if(creditCardType.equals("MC")) {
			return CardTypeType.MASTER_CARD;
		} else if(creditCardType.equals("VI")) {
			return CardTypeType.VISA;
		}		
		return null;
	}

	/**
	 * Populates jccas response bean into credit card vo bean
	 * 
	 * @param creditCardVO
	 * @param response
	 * @param requestId
	 * @return
	 */
	public static CommonCreditCardVO populateJccasRespToCCVo(CommonCreditCardVO creditCardVO, AuthorizeCCResponse response, String requestId) {		
		
	
		Map<String, Object> paymentExtensionMap = creditCardVO.getPaymentExtMap();
		paymentExtensionMap.put(PaymentExtensionConstants.REQUEST_ID, requestId);
		
		RespGrp responseGroup = response.getRespGrp();
		
		if (responseGroup != null) {
			String respCode = responseGroup.getRespCode();
			creditCardVO.setActionCode(respCode);
			paymentExtensionMap.put(PaymentExtensionConstants.RESPONSE_CODE, respCode);
			creditCardVO.setApprovalCode(responseGroup.getAuthID());	
			paymentExtensionMap.put(PaymentExtensionConstants.ADDITIONAL_RESP_DATA, responseGroup.getAddtlRespData());
		}
		
		CardGrpType cardGroup = response.getCardGrp();
		
		if (cardGroup != null) {
			creditCardVO.setAVSIndicator(cardGroup.getAVSResultCode()!=null?cardGroup.getAVSResultCode().value():null);
			creditCardVO.setCscResponseCode(cardGroup.getCCVResultCode()!=null?cardGroup.getCCVResultCode().value():null);
		}
		
		CommonGrp commonGroup = response.getCommonGrp();		
		
		if(commonGroup != null) {
			creditCardVO.setTimeStamp(commonGroup.getLocalDateTime());		
			creditCardVO.setApprovedAmount(commonGroup.getTxnAmt());			
			paymentExtensionMap.put(PaymentExtensionConstants.TRANSACTION_AMOUNT, formatAmount(commonGroup.getTxnAmt()));
			paymentExtensionMap.put(PaymentExtensionConstants.TRANSMISSION_DATE_TIME, commonGroup.getTrnmsnDateTime());
			paymentExtensionMap.put(PaymentExtensionConstants.STAN, commonGroup.getSTAN());
			paymentExtensionMap.put(PaymentExtensionConstants.REFERENCE_NUMBER, commonGroup.getRefNum());			
			paymentExtensionMap.put(PaymentExtensionConstants.TERMINAL_CATEGORY_CODE, commonGroup.getTermCatCode());
			paymentExtensionMap.put(PaymentExtensionConstants.POS_CONDITION_CODE, commonGroup.getPOSCondCode());
			paymentExtensionMap.put(PaymentExtensionConstants.TRANSACTION_CURRENCY, commonGroup.getTxnCrncy());					
			
		}
		
		CardSpcGrp cardSpecialGroup = response.getCardSpcGrp();
		
		if(cardSpecialGroup != null) {
			List<CardSpcDtl> cardSpecialDetails =  cardSpecialGroup.getCardSpcDtl();
			Map<String, String> cardSpecialDetailMap = new HashMap<String, String>();
			// Populating card special details into the map
			for(CardSpcDtl cardSpecialDetail : cardSpecialDetails) {
				cardSpecialDetailMap.put(cardSpecialDetail.getName(), cardSpecialDetail.getValue());
			}		
			paymentExtensionMap.put(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL, cardSpecialDetailMap);
		}
		
		return creditCardVO;
	}

	/**
	 * Formats the amoount into decival value
	 * Example: 
	 * inputs:  1234,123,4,23 
	 * outputs: 12.34,1.23, 0.04,0.23 resp.
	 * 
	 * @param txnAmt
	 * @return
	 */
	private static String formatAmount(String txnAmt) {
		if (txnAmt != null && txnAmt.trim().length() > 0) {
			int length = txnAmt.length();

			if (length == 1) {
				txnAmt = "0.0" + txnAmt;
			} else if (length == 2) {
				txnAmt = "0." + txnAmt;
			} else {
				txnAmt = txnAmt.substring(0, length - 2) + "." + txnAmt.substring(length - 2, length);
			}
		}
		return txnAmt;
	}

	/**
	 * Checks the given ccType present in comma separated list of credit cards. Return true if present other wise false
	 * 
	 * @param ccType
	 * @param csCcList
	 * @return
	 */
	public static boolean checkValidBamsCC(String ccType, String csCcList) {
		String[] ccList = csCcList.split(",");
		for(String cardType : ccList) {
			if(ccType.equals(cardType)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Converts given jaxb object into xml string
	 * 
	 * @param reqObject
	 * @param reqClass
	 * @return
	 * @throws JAXBException
	 */
	public static String covertToXmlString(Object reqObject, Class reqClass) throws JAXBException {
		StringWriter w = new StringWriter();
		JAXBContext reqcontext = JAXBContext.newInstance(reqClass);
		Marshaller m = reqcontext.createMarshaller();			
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.setProperty(CharacterEscapeHandler.class.getName(),
                new CharacterEscapeHandler() {
                    @Override
                    public void escape(char[] ac, int i, int j, boolean flag,
                            Writer writer) throws IOException {
                        writer.write(ac, i, j);
                    }
                });
        m.marshal(reqObject, w);
		return w.toString();
	}
	
	public static int getPort(String baseURL){
		int port;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			port = uri.getPort();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			port = 443;
		}
		if(port <= 0)
			port = 443;
		return port;

	}

	public static String getHost(String baseURL){
		String host = null;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			host = uri.getHost();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			logger.error("This is an invalid URL" + baseURL);
		}
		return host;
	}
	
	public static String getPath(String baseURL){
		String path = null;
		URI uri = null;
		try
		{
			uri = new URI(baseURL);
			path = uri.getPath();
		}
		catch(URISyntaxException e)
		{
			e.printStackTrace();
			logger.error("This is an invalid URL" + baseURL);
		}
		return path;
	}	
}
