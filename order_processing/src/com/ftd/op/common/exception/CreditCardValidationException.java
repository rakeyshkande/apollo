/**
 * 
 */
package com.ftd.op.common.exception;

/**
 * @author kvasant
 *
 */
public class CreditCardValidationException extends Exception {

	/**
	 * 
	 */
	public CreditCardValidationException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CreditCardValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public CreditCardValidationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CreditCardValidationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
