/**
 * 
 */
package com.ftd.op.common;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;

import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.common.vo.jccasreq.AuthorizeCCRequest;
import com.ftd.op.common.vo.jccasres.AuthorizeCCResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;


/**
 * Client class used to make http call jccas service using apache commons
 * http-client framework
 * 
 * @author kvasant
 * 
 */
public class JccasClient {

	private static final String JCCAS_WS_URL = "jccas.ws.url";
	private static final String JCCAS_WS_USERNAME = "jccas.ws.username";
	private static final String JCCAS_WS_PASSWORD = "jccas.ws.password";
	private static final String JCCAS_WS_SOCKET_TIMEOUT = "jccas.ws.socket.timeout";
	private Logger logger = new Logger(JccasClient.class.getName());

	/**
	 * The unique instance of this class.
	 */
	private static JccasClient jccasClient = new JccasClient();

	private JccasClient() {

	}

	/**
	 * Returns singleton instance of class
	 * 
	 * @return JccasClient
	 */
	public static JccasClient getInstance() {
		return jccasClient;
	}

	
	/**
	 * This method has been introduced to fix Jira Id: SP-184.
	 * A custom timeout value will be used based on the calling system.
	 * Ex: If the calling system is Account Management for FS Auto Renew,  
	 * then the JCCAS Socket timeout value configured for Auto renew process
	 * will be used. 
	 * 
	 * 
	 * @param request
	 * @param jccasStatsConn
	 * @return
	 * @throws Exception
	 */
	public AuthorizeCCResponse callJCCASApi(AuthorizeCCRequest request, Connection jccasStatsConn)
			throws Exception {
		return callJCCASApi(request, jccasStatsConn, null);
	}
	
	/**
	 * Makes http call to JCCAS service
	 * 
	 * @param AuthorizeCCRequest
	 *            request
	 * @return AuthorizeCCResponse
	 * @throws Exception
	 */
	public AuthorizeCCResponse callJCCASApi(AuthorizeCCRequest request, Connection jccasStatsConn, String callingSystem)
			throws Exception {

		JccasUtils.validateRequest(request);

		ConfigurationUtil cu = ConfigurationUtil.getInstance();

		String jccasWsUrl = cu.getFrpGlobalParm(
				OrderProcessingConstants.SERVICE, JCCAS_WS_URL);		

		if (jccasWsUrl == null || jccasWsUrl.isEmpty()) {
			logger.error("Empty/null jccas webservice url retrieved");
			throw new Exception("Empty/null jccas webservice url retrieved");
		}

		String jccasWsUsername = cu.getSecureProperty(
				OrderProcessingConstants.SERVICE, JCCAS_WS_USERNAME);
		String jccasWsPassword = cu.getSecureProperty(
				OrderProcessingConstants.SERVICE, JCCAS_WS_PASSWORD);
		
		if (jccasWsUsername == null || jccasWsUsername.isEmpty()) {
			logger.error("Empty/null jccas webservice username retrieved");
			throw new Exception("Empty/null jccas webservice username retrieved");
		}

		if (jccasWsPassword == null || jccasWsPassword.isEmpty()) {
			logger.error("Empty/null jccas webservice password retrieved");
			throw new Exception("Empty/null jccas webservice password retrieved");
		}

		String socketTimeout = cu.getFrpGlobalParm(
				OrderProcessingConstants.AUTH_CONFIG, JCCAS_WS_SOCKET_TIMEOUT);

		String requestXmlString = JccasUtils.covertToXmlString(request, AuthorizeCCRequest.class);
		
		PostMethod post = new PostMethod(jccasWsUrl);		
		post.setRequestHeader("Content-Type", "application/xml");
		post.setRequestHeader("x-user-name", jccasWsUsername);
		post.setRequestHeader("x-password", jccasWsPassword);
		
		// Fix for SP-184
		if (callingSystem != null && !callingSystem.isEmpty()) {
			String newTimeout = cu.getFrpGlobalParm(
					OrderProcessingConstants.AUTH_CONFIG, CallingSystemGlobalParamMapper.getGlobalParamNameForCallingSystem(callingSystem));
			socketTimeout = (newTimeout != null && !newTimeout.isEmpty()) ? newTimeout : socketTimeout;  
		}
		
		if (socketTimeout != null) {
			post.setRequestHeader("conn-timeout", socketTimeout);
		}
		
		RequestEntity entity = new StringRequestEntity(requestXmlString, "application/xml", null);
        post.setRequestEntity(entity);
		
		HttpClientParams clientParams = new HttpClientParams();
		try {
			clientParams.setSoTimeout(Integer.valueOf(socketTimeout));
		} catch(NumberFormatException e) {
			// Setting default value in case any exception
			clientParams.setSoTimeout(10000);
			logger.error("Invalid socketTimeout: " + socketTimeout +  " read from global params. Setting to default value" , e);
		} 
		
		HttpClient client = new HttpClient(clientParams);	 
		int result = 0;		
		logger.info("Sending request to JCCAS service. Request Id: " + request.getRequestId());
		long startTime = (new Date()).getTime();
		AuthorizeCCResponse response = null;
		
		try {
			 result = client.executeMethod(post);
			
			 if (result == 200) {
				logger.info("Response received from Jccas service for requestId:" + request.getRequestId());
				InputStream is = post.getResponseBodyAsStream();
				JAXBContext rescontext = JAXBContext.newInstance(AuthorizeCCResponse.class);
				Unmarshaller um = rescontext.createUnmarshaller();
				response = (AuthorizeCCResponse) um.unmarshal(is);
			} else {
				logger.info("Error response received from the jccas service. For the request id: "
					+ request.getRequestId() + ", Response code: " + result);
			}
		} catch(Exception e) {			
			throw e;
		} finally {
			logServiceResponseTracking(request.getRequestId(), startTime, jccasStatsConn);	
			releaseClientConnection(post);
		}
		
		return response;
	}
	
	/** Releases the client resource
	 * @param post
	 */
	private void releaseClientConnection(PostMethod post) {
		try {			
			if(post != null) {
				post.releaseConnection();
			}
		} catch(Exception e) {
			logger.error("Error caught releasing Client Connection: ", e);
		}		
	}

	/**
	 * Service response tracking
	 * 
	 * @param requestId
	 * @param startTime
	 * @param jccasStatsConn
	 * @throws Exception
	 */
	private void logServiceResponseTracking(String requestId, long startTime,
			Connection jccasStatsConn) {
		try {
			long responseTime = System.currentTimeMillis() - startTime;
			logger.debug("responseTime: " + responseTime);
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName("JCCAS Service");
			srtVO.setServiceMethod("Authorize");
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new Date());
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insert(jccasStatsConn, srtVO);
		} catch (Exception e) {
			logger.error("Exception occured while persisting JCCAS response times.");
		}
		
	}
}
