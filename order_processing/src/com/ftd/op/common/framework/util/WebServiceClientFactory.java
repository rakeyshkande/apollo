/**
 * 
 */
package com.ftd.op.common.framework.util;

import java.net.URL;

import com.ftd.milespoints.webservice.MilesPointsService;
import com.ftd.milespoints.webservice.MilesPointsServiceImplService;
import com.ftd.milespoints.webservice.MilesPointsService_MilesPointsServiceImplPort_Client;
import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.osp.mercuryinterface.util.CommonUtilites;
import com.ftd.osp.utilities.ConfigurationUtil;

/**
 * @author cjohnson
 *
 */
public class WebServiceClientFactory {
	
	private static String SERVICE_CONTEXT = "SERVICE";
	private static String MILESPOINTS_SERVICE_URL_CONFIG = "MILESPOINTS_SERVICE_URL";
	
	protected URL milesPointsWsdlUrl;
	protected MilesPointsService mps;
	protected ConfigurationUtil util;
	
	public WebServiceClientFactory() {
		util = new ConfigurationUtil();
	}
	
	public WebServiceClientFactory(ConfigurationUtil util) {
		this.util = util;
	}

	public MilesPointsService getMilesPointsService() throws Exception {
		
		String mpsURL = util.getFrpGlobalParm(SERVICE_CONTEXT, MILESPOINTS_SERVICE_URL_CONFIG);
		
		if (mps == null ||
				(milesPointsWsdlUrl != null && !milesPointsWsdlUrl.toString().equals(mpsURL))) {
			
			milesPointsWsdlUrl = new URL(mpsURL);
			MilesPointsServiceImplService milesPointsService = new MilesPointsServiceImplService(milesPointsWsdlUrl);			
			this.mps = milesPointsService.getMilesPointsServiceImplPort();
		}
		
		return mps;
	}

}
