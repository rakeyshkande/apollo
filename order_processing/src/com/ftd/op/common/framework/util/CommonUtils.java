package com.ftd.op.common.framework.util;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.order.constants.OrderConstants;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.CommentsVO;
import com.ftd.op.order.vo.OrderDetailVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;

/**
 * 
 * @author Jason Weiss
 */
public class CommonUtils 
{
  private static final String CONFIG_FILE = "order-processing-config.xml";
  private static Logger logger = new Logger("com.ftd.op.common.service.CreditCardService");;
  public CommonUtils(){
    
  }
  private static String LOGGER_CATEGORY = "com.ftd.op.common.framework.util.CommonUtilites";

  /* 
   * Get database connection
   */
  static public Connection getConnection() throws Exception
  {
    return getConnection("DATABASE_CONNECTION");
  }  
  
  static public Connection getConnection(String connectionName) throws Exception
  {
    
    ConfigurationUtil config = ConfigurationUtil.getInstance();            
    String dbConnection = config.getProperty(CONFIG_FILE,connectionName);         
   
    //get DB connection
    DataSource dataSource = (DataSource)lookupResource(dbConnection);
    return dataSource.getConnection();  
  }    
  
  
  static public DataSource getDataSource() throws Exception 
  {
    ConfigurationUtil config = ConfigurationUtil.getInstance();            
    String dbConnection = config.getProperty(CONFIG_FILE,"DATABASE_CONNECTION");         
   
    //get DB connection
    DataSource dataSource = (DataSource)lookupResource(dbConnection);
    return dataSource;
    
  }
    
  /**
   * This method sends a message to the System Messenger.
   * 
   * @param String message
   * @returns String message id
   */
  static public String sendSystemMessage(String message) throws Exception
  {      
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
    String messageSource = configUtil.getProperty(CONFIG_FILE,"MESSAGE_SOURCE");
    
    return sendSystemMessage(messageSource, message);
  }     
  
  /**
   * This method sends a message to the System Messenger, taking source as input
   * parameter so system message can be easily turned off.
   * 
   * @param String message
   * @returns String message id
   */
  static public String sendSystemMessage(String source, String message) throws Exception
  {
    Logger logger =  new Logger(LOGGER_CATEGORY);
    
    logger.error("Sending System Message:" + message);
          
    String messageID = "";
  
    //build system vo
    SystemMessengerVO sysMessage = new SystemMessengerVO();
    sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
    sysMessage.setSource(source);
    sysMessage.setType("ERROR");
    sysMessage.setMessage(message);
  
    SystemMessenger sysMessenger = SystemMessenger.getInstance();
    Connection conn = getConnection();
    try
    {
      messageID = sysMessenger.send(sysMessage,conn);
    
      if(messageID == null) {
        String msg = "Error occured while attempting to send out a system message. Msg not sent: " + message;
        logger.error(msg);
        System.out.println(msg);
      }
    }
    finally
    {
      conn.close();
    }
  
    return messageID;  
  }       
 
  /**
   * Returns a transactional resource from the EJB container.
   * 
   * @param jndiName
   * @return 
   * @throws javax.naming.NamingException
   */
  public static Object lookupResource(String jndiName)
              throws NamingException
  {
    InitialContext initContext = null;
    try
    {
      initContext = new InitialContext();
//      Context myenv = (Context) initContext.lookup("java:comp/env");
//      System.out.println("Resource lookup env:" + myenv.getEnvironment());
      
      return initContext.lookup(jndiName);      
    }finally  {
      try  {
        initContext.close();
      } catch (Exception ex)  {

      } finally  {
      }
    }
  }  
  /**
 * Sends out a JMS message.
 * 
 * @param context Initial Context
 * @param messageToken JMS Message
 * @throws java.lang.Exception
 */
  public static void sendJMSMessage(InitialContext context,MessageToken messageToken)
      throws Exception {

     
     ConfigurationUtil config = ConfigurationUtil.getInstance();            
     String connectionFactoryLocation = config.getProperty(CONFIG_FILE,"CONNECTION_FACTORY_LOCATION");     
     String destinationLocation = config.getProperty(CONFIG_FILE,"MERCURY_DESTINATION_LOCATION");     
     
     //make the correlation id the same as the message
     messageToken.setJMSCorrelationID((String)messageToken.getMessage());

     Dispatcher dispatcher = Dispatcher.getInstance();
     messageToken.setStatus("OPSUFFIX");
     dispatcher.dispatchTextMessage(context, messageToken);  
  }

/**
   * Sends out a JMS message.
   * 
   * @throws java.lang.Exception
   * @param status
   * @param destinationLocationPropertyName
   * @param messageToken
   * @param context
   */
  public static void sendJMSMessage(InitialContext context,MessageToken messageToken, String destinationLocationPropertyName, String status)
      throws Exception {

     
     ConfigurationUtil config = ConfigurationUtil.getInstance();            
     String connectionFactoryLocation = config.getProperty(CONFIG_FILE,"CONNECTION_FACTORY_LOCATION");     
     String destinationLocation = config.getProperty(CONFIG_FILE, destinationLocationPropertyName);     
     
     //make the correlation id the same as the message
     messageToken.setJMSCorrelationID((String)messageToken.getMessage());

     Dispatcher dispatcher = Dispatcher.getInstance();
     messageToken.setStatus(status);
     dispatcher.dispatchTextMessage(context, messageToken);  
  }

  /**
   * Clears the time from a date object.
   * @return Date
   * @param Date
   */
  public static Date clearTime (Date date)
  {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.clear(Calendar.HOUR_OF_DAY);    
    calendar.clear(Calendar.HOUR);
    calendar.clear(Calendar.MINUTE);    
    calendar.clear(Calendar.SECOND);    
    calendar.clear(Calendar.MILLISECOND); 
    calendar.set(Calendar.AM_PM,Calendar.AM);
    return new Date(calendar.getTimeInMillis());
  }
  
    /**
     * Creates an order comment.
     * @param comment
     * @param orderDetailVO
     * @throws Exception
     */
    public static void createOrderComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
        throws Exception
    {
            OrderDAO orderDAO = new OrderDAO(conn);    
            CommentsVO commentsVO = new CommentsVO();
            commentsVO.setComment(comment);
            commentsVO.setCommentOrigin(OrderConstants.OPERATOR_OP);
            commentsVO.setCommentType(OrderConstants.COMMENT_TYPE_ORDER);
            commentsVO.setCreatedBy(OrderConstants.OPERATOR_OP);
            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
            orderDAO.insertComment(commentsVO);
    }     
    
    /**
     * Utility method to send page system message
     * 
     * @param logMessage
     */
    public static void sendPageSystemMessage(String logMessage) {
		Connection conn = null;
		try {
			conn = getConnection();
			String appSource = OrderProcessingConstants.SM_PAGE_SOURCE;
			String errorType = OrderProcessingConstants.SM_TYPE;
			String subject = OrderProcessingConstants.SM_PAGE_SUBJECT;
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(appSource);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			// Do not attempt to send system message it requires obtaining a
			// connection
			// and may end up in an infinite loop.
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

	}

    /**
     * Utility method to send nopage system message
     * @param logMessage
     */
	public static void sendNoPageSystemMessage(String logMessage) {
		Connection conn = null;
		try {
			conn = getConnection();
			String appSource = OrderProcessingConstants.SM_NOPAGE_SOURCE;
			String errorType = OrderProcessingConstants.SM_TYPE;
			String subject = OrderProcessingConstants.SM_NOPAGE_SUBJECT;
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(appSource);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			// Do not attempt to send system message it requires obtaining a
			// connection
			// and may end up in an infinite loop.
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

	}
	
	public static void sendNoPageSystemMessage(String logMessage,String source, String subject) {
		Connection conn = null;
		try {
			conn = getConnection();			
			String errorType = OrderProcessingConstants.SM_TYPE;			
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;
			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(source);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			// Do not attempt to send system message it requires obtaining a
			// connection
			// and may end up in an infinite loop.
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

		
	}
	 /**
     * Utility method to send Payment Gateway page system message
     * 
     * @param logMessage
     */
    public static void sendPGPageSystemMessage(String logMessage) {
		Connection conn = null;
		try {
			conn = getConnection();
			String appSource = OrderProcessingConstants.SM_PG_PAGE_SOURCE;
			String errorType = OrderProcessingConstants.SM_TYPE;
			String subject = OrderProcessingConstants.SM_PG_PAGE_SUBJECT;
			int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

			SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
			systemMessengerVO.setLevel(pageLevel);
			systemMessengerVO.setSource(appSource);
			systemMessengerVO.setType(errorType);
			systemMessengerVO.setSubject(subject);
			systemMessengerVO.setMessage(logMessage);
			String result = SystemMessenger.getInstance().send(systemMessengerVO, conn, false);
			logger.debug(result);
		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					logger.error("Unable to close connection: " + e);
				}
			}
		}

	}
}