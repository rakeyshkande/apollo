package com.ftd.op.common.framework.service;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

/**
 * Base Class for all the session beans used in FTD.COM order processoing.
 * 
 * This code is relatively simple for now, implementing the barest minimum of 
 * stateless session-bean functionality, doing nothing tricky in any of the
 * methods.
 *
 * @ejb.bean generate = "false"
 *
 * @author  Jason Weiss
 */
public class BaseSessionBean implements SessionBean {

    /** Object to store the SessionContext */
    protected SessionContext mCtx;
    
    /**
     * Initlaize the session bean.
     */
    protected void initialize() {
	    // do nothing
    }

    /**
     * Sets the session context.
     *
     * @param context SessionContext for the session
     *
     * @throws EJBException This gets thrown for system level errors.
     */
    public void setSessionContext(SessionContext context)
        throws EJBException {
        mCtx = context;
    }

    /**
     * The EJB Specification requires this method
     *
     * @throws EJBException This gets thrown for system level errors.
     */
    public void ejbRemove()
        throws EJBException {
	
	// do nothing
    }

    /**
     * The EJB Specification requires this method
     *
     * @throws EJBException This gets thrown for system level errors.
     */
    public void ejbActivate()
        throws EJBException {
	
	// do nothing
    }

    /**
     * The EJB Specification requires this method
     *
     * @throws EJBException This gets thrown for system level errors.
     */
    public void ejbPassivate()
        throws EJBException {
		
	// do nothing
    }

}