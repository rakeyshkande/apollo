package com.ftd.op.common;

/**
 * An utility class to hold the global param names
 * for the custom JCCAS timeout for each calling system
 * 
 * Ex:
 * Currently, the common JCCAS timeout is re-tried from 
 * Global param "jccas.ws.socket.timeout". This will be
 * common for all the calling system. But if the calling
 * system wants to use/override with a custom value then
 * this class will be used to fetch the global param name 
 * which will hold the custom timeout value.
 * 
 * 
 * @author kdatchan
 *
 */
public class CallingSystemGlobalParamMapper {
	
	public static final String FS_AUTO_RENEW = "FS_AUTO_RENEW";
	public static final String GP_FS_AUTO_RENEW = "jccas.ws.socket.timeout.fsautorenew";
	
	public static String getGlobalParamNameForCallingSystem(String callingSystem) {
		
		if (callingSystem != null && !callingSystem.isEmpty()) {
			if (FS_AUTO_RENEW.equals(callingSystem)) {
				return GP_FS_AUTO_RENEW;
			}
		}
		return null;
	}

}
