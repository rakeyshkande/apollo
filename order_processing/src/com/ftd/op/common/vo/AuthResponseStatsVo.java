/**
 * 
 */
package com.ftd.op.common.vo;

import java.util.Date;

/**
 * VO for auth response stats
 * 
 * @author kvasant
 *
 */
public class AuthResponseStatsVo {
	
	private String referenceNumber;
	private String creditCardType;
	private String validationStatusCode;
	private String requestXml;
	private String responseXml;
	private Date timeStamp;

	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getCreditCardType() {
		return creditCardType;
	}
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	public String getValidationStatusCode() {
		return validationStatusCode;
	}
	public void setValidationStatusCode(String validationStatusCode) {
		this.validationStatusCode = validationStatusCode;
	}
	public String getRequestXml() {
		return requestXml;
	}
	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}
	public String getResponseXml() {
		return responseXml;
	}
	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

}
