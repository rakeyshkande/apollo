//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.09.09 at 02:54:39 PM IST 
//


package com.ftd.op.common.vo.jccasres;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CardTypeType">
 *   &lt;restriction base="{}Max10AN">
 *     &lt;enumeration value="Diners"/>
 *     &lt;enumeration value="Discover"/>
 *     &lt;enumeration value="MasterCard"/>
 *     &lt;enumeration value="Visa"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CardTypeType")
@XmlEnum
public enum CardTypeType {

    @XmlEnumValue("Diners")
    DINERS("Diners"),
    @XmlEnumValue("Discover")
    DISCOVER("Discover"),
    @XmlEnumValue("MasterCard")
    MASTER_CARD("MasterCard"),
    @XmlEnumValue("Visa")
    VISA("Visa");
    private final String value;

    CardTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CardTypeType fromValue(String v) {
        for (CardTypeType c: CardTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
