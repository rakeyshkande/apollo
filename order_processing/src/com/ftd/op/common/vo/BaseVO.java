package com.ftd.op.common.vo;

import com.ftd.op.common.to.BaseTO;
import java.io.Serializable;
import java.lang.reflect.Method;

public class BaseVO implements Serializable
{

  public BaseVO()
  {
  }

 /** 
  * This method prints all the getters for a given object using System.out.println.
  * 
  * @param Object obj
  * **/
 public static void printGetters (Object obj) throws Exception 
  {
    BaseTO.printGetters(obj);
  }

}
