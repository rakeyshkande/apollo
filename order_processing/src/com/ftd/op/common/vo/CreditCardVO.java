package com.ftd.op.common.vo;

/**
 * CreditCardVO contains CC information.
 * 
 * @author 	Mike McCormack
 */

import java.io.Serializable;

public class CreditCardVO implements Serializable {

	private String requestId;
	private String addressLine;
	private String zipCode;
	private String creditCardNumber;
	private String expirationDate;
	private String userData;
	private String transactionType;
	private String amount;
	private String approvedAmount;
	private String status;
	private String actionCode;
	private String verbiage;
	private String approvalCode;
	private String authorizationTransactionId;
	private String merchantRefId;
	private String dateStamp;
	private String timeStamp;
	private String AVSIndicator;
	private String batchNumber;
	private String itemNumber;
	private String acquirerReferenceData;
	private String cscValue;
	private String cscResponseCode;
	private boolean isNewRoute;
	private String origin;
	private boolean underMinAuthAmt;

	
	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	/**
	 * Get addressLine1
	 *
	 * @returns string : addressLine1 of address
	 */
	public String getAddressLine() {
		return addressLine;
	}

	/**
	 * Set addressLine1
	 *
	 * @param string
	 *            : addressLine of address
	 */
	public void setAddressLine(String newAddressLine) {
		addressLine = newAddressLine;
	}

	/**
	 * Get zipCode
	 *
	 * @returns string : zipCode of address
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Set zipCode
	 *
	 * @param string
	 *            : zipCode of address
	 */
	public void setZipCode(String newZipCode) {
		zipCode = newZipCode;
	}

	/**
	 * Get creditCardNumber
	 *
	 * @returns string : creditCardNumber of address
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	/**
	 * Set creditCardNumber
	 *
	 * @param string
	 *            : creditCardNumber of address
	 */
	public void setCreditCardNumber(String newCreditCardNumber) {
		creditCardNumber = newCreditCardNumber;
	}

	/**
	 * Get expirationDate
	 *
	 * @returns string : expirationDate of address
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Set expirationDate
	 *
	 * @param string
	 *            : expirationDate of address
	 */
	public void setExpirationDate(String newExpirationDate) {
		expirationDate = newExpirationDate;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getUserData() {
		return userData;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setUserData(String newUserData) {
		userData = newUserData;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setTransactionType(String newTransactionType) {
		transactionType = newTransactionType;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setAmount(String newAmount) {
		amount = newAmount;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getApprovedAmount() {
		return approvedAmount;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setApprovedAmount(String newApprovedAmount) {
		approvedAmount = newApprovedAmount;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setStatus(String newStatus) {
		status = newStatus;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setActionCode(String newActionCode) {
		actionCode = newActionCode;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getVerbiage() {
		return verbiage;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setVerbiage(String newVerbiage) {
		verbiage = newVerbiage;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String newApprovalCode) {
		approvalCode = newApprovalCode;
	}
	
	public String getAuthorizationTransactionId() {
		return authorizationTransactionId;
	}

	public void setAuthorizationTransactionId(String authorizationTransactionId) {
		this.authorizationTransactionId = authorizationTransactionId;
	}

	
	public String getMerchantRefId() {
		return merchantRefId;
	}

	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getDateStamp() {
		return dateStamp;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setDateStamp(String newDateStamp) {
		dateStamp = newDateStamp;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setTimeStamp(String newTimeStamp) {
		timeStamp = newTimeStamp;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getAVSIndicator() {
		return AVSIndicator;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setAVSIndicator(String newAVSIndicator) {
		AVSIndicator = newAVSIndicator;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setBatchNumber(String newBatchNumber) {
		batchNumber = newBatchNumber;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getItemNumber() {
		return itemNumber;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setItemNumber(String newItemNumber) {
		itemNumber = newItemNumber;
	}

	/**
	 * Get userData
	 *
	 * @returns string : userData of address
	 */
	public String getAcquirerReferenceData() {
		return acquirerReferenceData;
	}

	/**
	 * Set userData
	 *
	 * @param string
	 *            : userData of address
	 */
	public void setAcquirerReferenceData(String newAcquirerReferenceData) {
		acquirerReferenceData = newAcquirerReferenceData;
	}

	public void setUnderMinAuthAmt(boolean underMinAuthAmt) {
		this.underMinAuthAmt = underMinAuthAmt;
	}

	public boolean isUnderMinAuthAmt() {
		return underMinAuthAmt;
	}

	public void setCscValue(String cscValue) {
		this.cscValue = cscValue;
	}

	public String getCscValue() {
		return cscValue;
	}

	public void setCscResponseCode(String cscResponseCode) {
		this.cscResponseCode = cscResponseCode;
	}

	public String getCscResponseCode() {
		return cscResponseCode;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public boolean isNewRoute() {
		return isNewRoute;
	}

	public void setNewRoute(boolean isNewRoute) {
		this.isNewRoute = isNewRoute;
	}

	@Override
	public String toString() {
		return "CreditCardVO [requestId=" + requestId + ", addressLine="
				+ addressLine + ", zipCode=" + zipCode + ", creditCardNumber="
				+ creditCardNumber + ", expirationDate=" + expirationDate
				+ ", userData=" + userData + ", transactionType="
				+ transactionType + ", amount=" + amount + ", approvedAmount="
				+ approvedAmount + ", status=" + status + ", actionCode="
				+ actionCode + ", verbiage=" + verbiage + ", approvalCode="
				+ approvalCode + ", dateStamp=" + dateStamp + ", timeStamp="
				+ timeStamp + ", AVSIndicator=" + AVSIndicator
				+ ", batchNumber=" + batchNumber + ", itemNumber=" + itemNumber
				+ ", acquirerReferenceData=" + acquirerReferenceData
				+ ", cscValue=" + cscValue + ", cscResponseCode="
				+ cscResponseCode + ", isNewRoute=" + isNewRoute + ", origin="
				+ origin + ", underMinAuthAmt=" + underMinAuthAmt + "]";
	}

}
