package com.ftd.op.common.vo;
import java.util.HashMap;
import java.util.Map;

import com.ftd.op.order.to.CreditCardTO;

public class CommonCreditCardVO extends com.ftd.op.common.vo.CreditCardVO implements IPaymentAuthVO{


  private String validationStatus; // 'D' - Declined, 'A' - Approved, 'E' - System Error //
  private String creditCardType;
  private String creditCardDnsType;
  private String ccId; 
  private String name;
  private String address2;
  private String city;
  private String country;
  private String customerId;
  private String ccAuthProvider;

  private Map<String, Object> paymentExtMap;

  // SP-184
  private String callingSystem;

  //User Story 2893
  private String eci;
  private String xid;
  private String cavv;
  private String ucaf;
  private String route;
  
  public CommonCreditCardVO()
  {
  }

  public void setValidationStatus(String validationStatus)
  {
    this.validationStatus = validationStatus;
  }


  public String getValidationStatus()
  {
    return validationStatus;
  }


  public void setCreditCardType(String creditCardType)
  {
    this.creditCardType = creditCardType;
  }


  public String getCreditCardType()
  {
    return creditCardType;
  }


  public void setCreditCardDnsType(String creditCardDnsType)
  {
    this.creditCardDnsType = creditCardDnsType;
  }


  public String getCreditCardDnsType()
  {
    return creditCardDnsType;
  }


  public void setAddress2(String address2)
  {
    this.address2 = address2;
  }


  public String getAddress2()
  {
    return address2;
  }


  public void setCcId(String ccId)
  {
    this.ccId = ccId;
  }


  public String getCcId()
  {
    return ccId;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setCountry(String country)
  {
    this.country = country;
  }


  public String getCountry()
  {
    return country;
  }


  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }


  public String getCustomerId()
  {
    return customerId;
  }


  public void setName(String name)
  {
    this.name = name;
  }


  public String getName()
  {
    return name;
  }

  public void populateVO(CreditCardTO card) 
  {
    this.setAcquirerReferenceData(card.getAcquirerReferenceData());
    this.setActionCode(card.getActionCode());
    this.setAddress2(card.getAddress2());
    this.setAddressLine(card.getAddressLine());
    this.setAmount(card.getAmount());
    this.setApprovalCode(card.getApprovalCode());
    this.setApprovedAmount(card.getApprovedAmount());
    this.setAVSIndicator(card.getAVSIndicator());
    this.setBatchNumber(card.getBatchNumber());
    this.setCcId(card.getCcId());
    this.setCity(card.getCity());
    this.setCountry(card.getCountry());
    this.setCreditCardDnsType(card.getCreditCardDnsType());
    this.setCreditCardNumber(card.getCreditCardNumber());
    this.setCreditCardType(card.getCreditCardType());
    this.setCscResponseCode(card.getCscResponseCode());
    this.setCscValue(card.getCscValue());
    this.setCustomerId(card.getCustomerId());
    this.setDateStamp(card.getDateStamp());
    this.setExpirationDate(card.getExpirationDate());
    this.setItemNumber(card.getItemNumber());
    this.setName(card.getName());
    this.setStatus(card.getStatus());
    this.setTimeStamp(card.getTimeStamp());
    this.setTransactionType(card.getTransactionType());
    this.setUserData(card.getUserData());
    this.setValidationStatus(card.getValidationStatus());
    this.setVerbiage(card.getVerbiage());
    this.setZipCode(card.getZipCode());
    this.setCcAuthProvider(card.getCcAuthProvider());
    this.setCallingSystem(card.getCallingSystem());
    this.getPaymentExtMap().putAll(card.getPaymentExtMap());
    this.setEci(card.getEci());
    this.setCavv(card.getCavv());
    this.setXid(card.getXid());
    this.setUcaf(card.getUcaf());
    this.setRoute(card.getRoute());
    this.setNewRoute(card.isNewRoute());
    this.setRequestId(card.getRequestId());
    this.setMerchantRefId(card.getMerchantRefId());
  }
  public CreditCardTO getCreditCardTO() 
  {
    CreditCardTO card = new CreditCardTO();
    
    // special translation for AAFES cards... aafes puts the auth_number 
    // in the action code
    if(this.getCreditCardType().startsWith("MS")) 
    {
      card.setApprovalCode(getActionCode());      
    }
    else 
    {
      card.setApprovalCode(getApprovalCode());
    }
    
    card.setAcquirerReferenceData(getAcquirerReferenceData());
    card.setActionCode(getActionCode());
    card.setAddress2(getAddress2());
    card.setAddressLine(getAddressLine());
    card.setAmount(getAmount());
    card.setApprovedAmount(getApprovedAmount());
    card.setAVSIndicator(getAVSIndicator());
    card.setBatchNumber(getBatchNumber());
    card.setCcId(getCcId());
    card.setCity(getCity());
    card.setCountry(getCountry());
    card.setCreditCardDnsType(getCreditCardDnsType());
    card.setCreditCardNumber(getCreditCardNumber());
    card.setCreditCardType(getCreditCardType());
    card.setCustomerId(getCustomerId());
    card.setDateStamp(getDateStamp());
    card.setExpirationDate(getExpirationDate());
    card.setItemNumber(getItemNumber());
    card.setName(getName());
    card.setStatus(getStatus());
    card.setTimeStamp(getTimeStamp());
    card.setTransactionType(getTransactionType());
    card.setUserData(getUserData());
    card.setValidationStatus(getValidationStatus());
    card.setVerbiage(getVerbiage());
    card.setZipCode(getZipCode());
    card.setUnderMinAuthAmt(isUnderMinAuthAmt());
    card.setCscResponseCode(getCscResponseCode());
    card.setCscValue(getCscValue());
    card.setCscValidatedFlag((getCscValue()!=null && !"".equals(getCscValue())) ? "Y" : "N");
    card.setCcAuthProvider(getCcAuthProvider());
    card.setCallingSystem(getCallingSystem());
	card.getPaymentExtMap().putAll(getPaymentExtMap());
	card.setEci(getEci());
	card.setCavv(getCavv());
	card.setXid(getXid());
	card.setUcaf(getUcaf());
	card.setRoute(getRoute());
	card.setNewRoute(isNewRoute());
	card.setRequestId(getRequestId());
	card.setAuthorizationTransactionId(getAuthorizationTransactionId());
	card.setMerchantRefId(getMerchantRefId());
    return card;
  }

	public Map<String, Object> getPaymentExtMap() {	
		if(this.paymentExtMap == null) {
			paymentExtMap = new HashMap<String, Object>();
		}
		return paymentExtMap;
	}

	public String getCcAuthProvider() {
		return ccAuthProvider;
	}

	public void setCcAuthProvider(String ccAuthProvider) {
		this.ccAuthProvider = ccAuthProvider;
	}

	public String getCallingSystem() {
		return callingSystem;
	}


	public void setCallingSystem(String callingSystem) {
		this.callingSystem = callingSystem;
	}
	
		
	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getXid() {
		return xid;
	}

	public void setXid(String xid) {
		this.xid = xid;
	}

	public String getCavv() {
		return cavv;
	}

	public void setCavv(String cavv) {
		this.cavv = cavv;
	}
	
	// User Story 4053 - New token for master card
	public String getUcaf() {
		return ucaf;
	}

	public void setUcaf(String ucaf) {
		this.ucaf = ucaf;
	}
	
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "CommonCreditCardVO [creditCardType=" + creditCardType
				+ ", creditCardDnsType=" + creditCardDnsType + ", ccId=" + ccId
				+ ", name=" + name + ", address2=" + address2 + ", city="
				+ city + ", country=" + country + ", customerId=" + customerId
				+ ", ccAuthProvider=" + ccAuthProvider + ", paymentExtMap="
				+ paymentExtMap + ", callingSystem=" + callingSystem + "]"
						+ "  CreditCardVO [requestId=" + getRequestId() + ", addressLine="
				+ getAddressLine() + ", zipCode=" + getZipCode() + ", creditCardNumber="
				+ getCreditCardNumber() + ", expirationDate=" + getExpirationDate()
				+ ", userData=" + getUserData() + ", transactionType="
				+ getTransactionType() + ", amount=" + getAmount() + ", approvedAmount="
				+ getApprovedAmount() + ", status=" + getStatus() + ", actionCode="
				+ getActionCode() + ", verbiage=" + getVerbiage() + ", approvalCode="
				+ getApprovalCode() + ", dateStamp=" + getDateStamp() + ", timeStamp="
				+ getTimeStamp() + ", AVSIndicator=" + getAVSIndicator()
				+ ", batchNumber=" + getBatchNumber() + ", itemNumber=" + getItemNumber()
				+ ", acquirerReferenceData=" + getAcquirerReferenceData()
				+ ", cscValue=" + getCscValue() + ", cscResponseCode="
				+ getCscResponseCode() + ", isNewRoute=" + isNewRoute() + ", origin="
				+ getOrigin() + ", underMinAuthAmt=" + isUnderMinAuthAmt() 
				+ ", route=" + route + ", merchantRefId=" + getMerchantRefId() + "]";
	}
}
