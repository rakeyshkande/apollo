package com.ftd.op.common.vo;
import com.ftd.op.order.to.CreditCardTO;
import java.util.Date;

public class MilesPointsVO implements IPaymentAuthVO{


    private String validationStatus; // 'D' - Declined, 'A' - Approved, 'E' - System Error //
    private String paymentType;
    private String paymentId;
    private String authNumber; 
    private int milesAmt;
    private String accountId;


    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setAuthNumber(String authNumber) {
        this.authNumber = authNumber;
    }

    public String getAuthNumber() {
        return authNumber;
    }

    public void setMilesAmt(int milesAmt) {
        this.milesAmt = milesAmt;
    }

    public int getMilesAmt() {
        return milesAmt;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }
    
}
