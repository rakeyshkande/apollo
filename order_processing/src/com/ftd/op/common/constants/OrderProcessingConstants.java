package com.ftd.op.common.constants;

public class OrderProcessingConstants 
{

    public static final String OE_CCAS_PURCHASE_REQUEST = "PR";
    public static final String OP_PHOENIX_CONTEXT = "PHOENIX";
    public static final String OP_PHOENIX_QUEUE_TYPES = "QUEUE_TYPES";
    public static final String OP_PHOENIX_BULK_QUEUE_TYPES = "PHOENIX_ELIGIBLE_QUEUES";
    public static final String AUTH_CONFIG = "AUTH_CONFIG";
    public static final String SERVICE = "SERVICE";
    
    public static final String SM_PAGE_SOURCE = "JCCAS_PAGE";
	public static final String SM_NOPAGE_SOURCE = "JCCAS_NOPAGE";
	public static final String SM_PAGE_SUBJECT = "JCCAS Message";
	public static final String SM_NOPAGE_SUBJECT = "NOPAGE JCCAS Message";
	public static final String SM_TYPE = "System Exception";
	
	//Added to fix SP-87.cc number validations same as jccas based on BAMS on or off.
	public static final String AUTH_PROVIDER = "cc.auth.provider";
	public static final String BAMS_CC_LIST = "bams.cc.list";
	public static final String BAMS_AUTH_PROVIDER="BAMS";
	
	
	public static final String SM_PG_PAGE_SOURCE = "PG_PAGE";
	public static final String SM_PG_NOPAGE_SOURCE = "PG_NOPAGE";
	public static final String SM_PG_PAGE_SUBJECT = "Payment Gateway Message";
	public static final String SM_PG_NOPAGE_SUBJECT = "NOPAGE Payment Gateway Message";
	
	public static final String PG_SVC_ENABLED = "PAYMENT_GATEWAY_SVC_ENABLED";
	public static final String PG_ROUTES = "PAYMENT_GATEWAY_ROUTES";
	public static final String PG_ROUTE_ORIGINS = "PAYMENT_GATEWAY_ROUTE_ORIGINS";
	public static final String PG_SVC_TIMEOUT = "PAYMENT_GATEWAY_SVC_TIMEOUT";
	public static final String PG_SVC_URL = "PAYMENT_GATEWAY_SVC_URL";
	
}