/**
 * 
 */
package com.ftd.op.common;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Custom jaxb CDATA binding adapter
 * 
 * @author kvasant
 *
 */
public class CustomCDATAAdapter extends XmlAdapter<String, String> {
	
	@Override
    public String marshal(String arg0) throws Exception {
        return "<![CDATA[" + arg0 + "]]>";
    }
    @Override
    public String unmarshal(String arg0) throws Exception {
        return arg0;
    }

}
