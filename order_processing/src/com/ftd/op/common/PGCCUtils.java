package com.ftd.op.common;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ftd.op.common.constants.OrderProcessingConstants;
import com.ftd.op.common.dao.CommonDAO;
import com.ftd.op.common.vo.AuthResponseStatsVo;
import com.ftd.op.common.vo.CommonCreditCardVO;
import com.ftd.op.common.vo.jccasreq.CardTypeType;
import com.ftd.op.order.dao.OrderDAO;
import com.ftd.op.order.vo.OrderVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.constants.PaymentExtensionConstants;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pg.client.PaymentGatewayUtils;
import com.ftd.pg.client.creditcard.authorize.request.AuthorizeRequest;
import com.ftd.pg.client.creditcard.authorize.request.BillTo;
import com.ftd.pg.client.creditcard.authorize.request.CardinalCommerce;
import com.ftd.pg.client.creditcard.authorize.request.CreditCard;
import com.ftd.pg.client.creditcard.authorize.response.AuthorizeResponse;
import com.ftd.pg.client.creditcard.authorize.response.CardSpcDtl;
import com.ftd.pg.client.creditcard.authorize.response.CardSpcGrp;
import com.ftd.pg.client.creditcard.authorize.response.CreditCardResponse;
import com.ftd.service.util.SequenceUtils;

/**
 * Utility class for CreditCardClient
 * 
 * @author nkatle
 *
 */
public class PGCCUtils {
	
	private static final Logger logger = new Logger(PGCCUtils.class.getName());
	private static final String JCCAS_SEQUENCE_NAME = "jccas_requestid";

	/**
	 * Populates creditCard request bean from credicardvo bean
	 * 
	 * @param creditCardVO
	 * @param requestId
	 * @return AuthorizeCCRequest
	 */
	public static AuthorizeRequest getCreditCardRequestBean(CommonCreditCardVO creditCardVO, Connection conn) throws Exception {
		AuthorizeRequest authorizeRequest = new AuthorizeRequest();
		authorizeRequest.setPaymentType(getCreditCardType(creditCardVO.getCreditCardType()).value().toUpperCase());
		//From Apollo, first time we will pass Master order number as MerchantRefId
		/*if(isBamsCreditCardType(creditCardVO.getCreditCardType())) {
			authorizeRequest.setMerchantReferenceId(getNextJccasRequestId(conn));
			creditCardVO.setRequestId(authorizeRequest.getMerchantReferenceId());
		} else {
			authorizeRequest.setMerchantReferenceId(creditCardVO.getRequestId());
		}*/
		authorizeRequest.setMerchantReferenceId(creditCardVO.getRequestId());
		authorizeRequest.setCurrency("usd");
		authorizeRequest.setRetry(true);
		authorizeRequest.setAmount(String.valueOf((Double.parseDouble(creditCardVO.getAmount()))));
		
		CreditCard creditCard = new CreditCard();
		creditCard.setCardNumber(creditCardVO.getCreditCardNumber());
		String ccType = getCreditCardType(creditCardVO.getCreditCardType()).value();
		creditCard.setCardType(ccType.toUpperCase());
		creditCard.setCvvNumber(creditCardVO.getCscValue());
        CardinalCommerce cardinalCommerce = new CardinalCommerce();
		cardinalCommerce.setEci(creditCardVO.getEci());
		cardinalCommerce.setCavv(creditCardVO.getCavv());
		cardinalCommerce.setXid(creditCardVO.getXid());
		cardinalCommerce.setUcafCollectInd(creditCardVO.getUcaf());
		creditCard.setCardinalCommerce(cardinalCommerce);

		String expiryDate = creditCardVO.getExpirationDate();
		Date expiryDate4 = null; 
		Calendar c = Calendar.getInstance();
		if(expiryDate !=null) {
			if(expiryDate.length() == 4) {
				try {
					SimpleDateFormat sdf4 = new SimpleDateFormat("yyMM");
					expiryDate4 = sdf4.parse(expiryDate);
				} catch (ParseException e) {
					logger.error("Unable to parse the expirydate: " + expiryDate + ": " + e);
					throw e;
				}
			} else if(expiryDate.length() == 6) {
				try {
					SimpleDateFormat sdf6 = new SimpleDateFormat("yyMMdd");
					expiryDate4 = sdf6.parse(expiryDate);
				} catch (ParseException e) {
					logger.error("Unable to parse the expirydate: " + expiryDate + ": " + e);
					throw e;
				}
			}
		}
		if(expiryDate4 != null) {
			c.setTime(expiryDate4);
			creditCard.setExpirationMonth(Integer.toString(c.get(Calendar.MONTH)+ 1));
			creditCard.setExpirationYear(Integer.toString(c.get(Calendar.YEAR)));	
		}
		authorizeRequest.setCreditCard(creditCard);
		authorizeRequest.setBillTo(populateBillToDetails(creditCardVO, conn));
		Map<String, Object> paymentExtParams = creditCardVO.getPaymentExtMap();
		if(paymentExtParams != null) {
			if(paymentExtParams.get(PaymentExtensionConstants.MERCHANT_REF_ID) != null) {
				authorizeRequest.setMerchantReferenceId((String) paymentExtParams.get(PaymentExtensionConstants.MERCHANT_REF_ID));	
			}
			
		}
		return authorizeRequest;
	}
	
	private static BillTo populateBillToDetails(CommonCreditCardVO creditCardVO, Connection conn) {
		OrderDAO orderDAO = new OrderDAO(conn);
		BillTo billTo = new BillTo();
		
		BillTo tempBillTo = null;
		try {
			tempBillTo = orderDAO.getCcFNameLNameEmail(creditCardVO.getCustomerId());
			if(tempBillTo!= null) {
				
				if(StringUtils.isBlank(billTo.getFirstName()) || StringUtils.isBlank(billTo.getLastName())) {
					billTo.setFirstName(tempBillTo.getFirstName());
					billTo.setLastName(tempBillTo.getLastName());
					billTo.setEmail(tempBillTo.getEmail());
				}
		
				if(!StringUtils.isBlank(tempBillTo.getEmail())) {
					billTo.setEmail(tempBillTo.getEmail());
				}
				
				if(StringUtils.isBlank(creditCardVO.getAddressLine())) {
					billTo.setAddress1(tempBillTo.getAddress1());
					billTo.setAddress2(tempBillTo.getAddress2());
				} else {
					billTo.setAddress1(creditCardVO.getAddressLine());
				}
				
				if(StringUtils.isBlank(creditCardVO.getCity())) {
					billTo.setCity(tempBillTo.getCity());
				} else {
					billTo.setCity(creditCardVO.getCity());
				}
				
				if(StringUtils.isBlank(creditCardVO.getStatus())) {
					billTo.setState(tempBillTo.getState());
				} else {
					billTo.setState(creditCardVO.getStatus());
				}
				
				if(StringUtils.isBlank(creditCardVO.getZipCode())) {
					billTo.setZipCode(tempBillTo.getZipCode());
				} else {
					billTo.setZipCode(creditCardVO.getZipCode());
				}
	
				if(StringUtils.isBlank(creditCardVO.getCountry())) {
					billTo.setCountry(tempBillTo.getCountry());
				} else {
					billTo.setCountry(creditCardVO.getCountry());
				}
			
			}
		} catch (Exception e) {
			logger.error("Error occured while fetching Customer card details" + e.getMessage());
		}
		
		return billTo;
	}
	/**
	 * Populates creditCard response bean into credit card vo bean
	 * 
	 * @param creditCardVO
	 * @param response
	 * @param requestId
	 * @return
	 */
	public static CommonCreditCardVO populateCreditCardRespToCCVo(CommonCreditCardVO creditCardVO, AuthorizeResponse response) {		
		
		Map<String, Object> paymentExtensionMap = new HashMap<String, Object>();
		creditCardVO.setMerchantRefId(response.getMerchantReferenceId());
		
		CreditCardResponse responseGroup = response.getCreditCardResponse();
		if (responseGroup != null) {
			String reasonCode = responseGroup.getReasonCode();
			if(StringUtils.isBlank(reasonCode)){
				creditCardVO.setStatus("ERROR");
			} else if("100".equals(reasonCode.trim())) {
				creditCardVO.setStatus("S");
				creditCardVO.setActionCode("000");
			} else {
				creditCardVO.setActionCode(reasonCode);
				creditCardVO.setStatus("D");
			}
			//paymentExtensionMap.put(PaymentExtensionConstants.RESPONSE_CODE, reasonCode);
			creditCardVO.setApprovalCode(responseGroup.getAuthCode());
			creditCardVO.setAuthorizationTransactionId(responseGroup.getAuthorizationTransactionId());
			
			//paymentExtensionMap.put(PaymentExtensionConstants.ADDITIONAL_RESP_DATA, responseGroup.getCcvResultCode());
		}
		creditCardVO.setAVSIndicator(responseGroup.getAvsResultCode());
		creditCardVO.setCscResponseCode(responseGroup.getCcvResultCode());
		
		creditCardVO.setTimeStamp(responseGroup.getTransmissionDateTime());		
		creditCardVO.setApprovedAmount(responseGroup.getAmount());
		creditCardVO.setRoute(response.getRoute());
		/*paymentExtensionMap.put(PaymentExtensionConstants.TRANSMISSION_DATE_TIME, responseGroup.getTransmissionDateTime());
		paymentExtensionMap.put(PaymentExtensionConstants.STAN, responseGroup.getStan());
		paymentExtensionMap.put(PaymentExtensionConstants.REFERENCE_NUMBER, responseGroup.getRefNum());			
		paymentExtensionMap.put(PaymentExtensionConstants.TERMINAL_CATEGORY_CODE, responseGroup.getTermCatCode());
		paymentExtensionMap.put(PaymentExtensionConstants.POS_CONDITION_CODE, responseGroup.getPosCondCode());
		paymentExtensionMap.put(PaymentExtensionConstants.TRANSACTION_CURRENCY, responseGroup.getCurrency());
		paymentExtensionMap.put(PaymentExtensionConstants.TRANSACTION_AMOUNT, responseGroup.getAmount());*/
		
		/*CardSpcGrp cardSpecialGroup = responseGroup.getCardSpcGrp();
		
		if(cardSpecialGroup != null) {
			List<CardSpcDtl> cardSpecialDetails =  cardSpecialGroup.getCardSpcDtl();
			Map<String, String> cardSpecialDetailMap = new HashMap<String, String>();
			// Populating card special details into the map
			for(CardSpcDtl cardSpecialDetail : cardSpecialDetails) {
				cardSpecialDetailMap.put(cardSpecialDetail.getName(), cardSpecialDetail.getValue());
			}		
			paymentExtensionMap.put(PaymentExtensionConstants.CARD_SPECIFIC_DETAIL, cardSpecialDetailMap);
		}
		*/
		creditCardVO.getPaymentExtMap().putAll(paymentExtensionMap);
		return creditCardVO;
	}
	/**
	 * Credit card type to creditCard card type
	 * 
	 * @param creditCardType
	 * @return
	 */
	private static CardTypeType getCreditCardType(String creditCardType) {
//		VISA, MASTERCARD, DISCOVER,DINERS,AMERICANEXPRESS
		if(creditCardType.equals("DC")) {
			return CardTypeType.DINERS;
		} else if(creditCardType.equals("DI")) {
			return CardTypeType.DISCOVER;
		} else if(creditCardType.equals("MC")) {
			return CardTypeType.MASTER_CARD;
		} else if(creditCardType.equals("VI")) {
			return CardTypeType.VISA;
		} else if(creditCardType.equals("AX")) {
			return CardTypeType.AMERICAN_EXPRESS;
		}
		return null;
	}
	
	private static boolean isBamsCreditCardType(String creditCardType) {
	    String jccasCardTypes;
		try {
			ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
			jccasCardTypes = configUtil.getFrpGlobalParm("AUTH_CONFIG","bams.cc.list");
			if (jccasCardTypes != null && jccasCardTypes.contains(creditCardType))
			{	
				return true;
			}
		} catch (Exception e) {
			logger.error("Error occured while fetching jccasCardTypes: " + e.getMessage());
		}
		return false;
	}
	
	public static String getNextJccasRequestId(Connection conn) throws Exception {
    	SequenceUtils su = SequenceUtils.getInstance();
		return su.getNextSequence(conn, JCCAS_SEQUENCE_NAME);
    }
	
	/**
	 * This method massages the cardinal tags to set as empty when null
	 * 
	 */
	public static void massageCardinalTokens(CardinalCommerce request) {
	
		if(request.getEci() == null)
		{
			request.setEci("");
		}
		if(request.getXid() == null)
		{
			request.setXid("");
		}
		if(request.getCavv() == null)
		{
			request.setCavv("");
		}
		if(request.getUcafCollectInd() == null)
		{
			request.setUcafCollectInd("");
		}
	}
	
	public static boolean isNewPlatform(CommonCreditCardVO creditCardVo, String orderGuidId, Connection conn){
		try {
			OrderDAO orderDAO = new OrderDAO(conn);
			OrderVO orderVO = orderDAO.getOrder(orderGuidId);
			creditCardVo.setOrigin(orderDAO.getOrdersOrigin(orderGuidId));
			creditCardVo.setRequestId(orderVO.getMasterOrderNumber());
			String orderRoute = creditCardVo.getRoute();
			orderRoute = StringUtils.isEmpty(orderRoute)?"":orderRoute;
			// should check 3 conditions
			// is global flag enabled - PAYMENT_GATEWAY_SVC_ENABLED - Y/N
			// is new origin - PAYMENT_GATEWAY_ROUTE_ORIGINS - MOBILE,TABLET,WEB
			// is credit card type PG-PS/PG-JCCAS specific card - PAYMENT_GATEWAY_ROUTES
			ConfigurationUtil cu = ConfigurationUtil.getInstance();
			String pgSvcEnabled = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_SVC_ENABLED);
			String pgRoutes = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTES);
			String pgRouteOrigins = cu.getFrpGlobalParm(OrderProcessingConstants.SERVICE, OrderProcessingConstants.PG_ROUTE_ORIGINS);
			logger.info("SvcEnabled:" + pgSvcEnabled + " :: Routes:" + pgRoutes + " :: RouteOrigins:" + pgRouteOrigins + " :: creditCardVo.Origin:" + creditCardVo.getOrigin() + " :: creditCardVo.CreditCardType:" + creditCardVo.getCreditCardType());
			if("Y".equalsIgnoreCase(pgSvcEnabled)) {
				if(pgRoutes != null) {
					String[] origins = pgRouteOrigins.split(",");
					if(origins != null && origins.length > 0) {
						List<String> originList = Arrays.asList(origins);
						if(originList.contains(creditCardVo.getOrigin())) {
							String[] routes = pgRoutes.split(",");
							if(routes != null && routes.length > 0) {
								List<String> routeList = Arrays.asList(routes);
								//PG-PS
								if(routeList.contains(orderRoute)) {
											creditCardVo.setNewRoute(true);
											return true;
								}
							}
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("Error while validation the platform. Treating as old platform", e);
		}
		return false;
	}
	
	public static void logCreditCardResponseStatistics(CommonCreditCardVO creditCardVO,
			String requestId, String errorMessage, AuthorizeRequest request,
			AuthorizeResponse response, Connection creditCardStatsConn) {
		try {
			// log request and response stats
			String responseJson = null;
			String requestJson = null;
			if (response != null) {
					responseJson = PaymentGatewayUtils.convertToJsonString(response);
					requestJson = PaymentGatewayUtils.convertToJsonString(request);
			} else {
				responseJson = errorMessage;
			}

			AuthResponseStatsVo authRespStatsVo = new AuthResponseStatsVo();
			authRespStatsVo.setCreditCardType(creditCardVO.getCreditCardType());
			authRespStatsVo.setReferenceNumber(requestId);
			authRespStatsVo.setRequestXml(requestJson);
			authRespStatsVo.setResponseXml(responseJson);
			authRespStatsVo.setTimeStamp(new Date());
			authRespStatsVo.setValidationStatusCode(creditCardVO.getStatus()
					.substring(0, 1));
			CommonDAO commonDao = new CommonDAO(creditCardStatsConn);
			commonDao.insertAuthResponseStats(authRespStatsVo, creditCardStatsConn);
			logger.debug("Auth respose statistics are saved to database");
		} catch (Exception e) {
			logger.error(
					"Exception/Error occured while inserting auth response statistics: "
							+ e.getMessage(), e);
		} 
	}
	
	public static Map<String, String> getPaymentExtMapByPaymentId(long paymentId, Connection conn) throws Exception
    {
    	Map<String, String> paymentExtMap = new HashMap<String, String>();
    	try{
    		//To-Do logic to fetch attributes from payemnt_ext
    		CommonDAO commonDAO = new CommonDAO(conn);
    		CachedResultSet rs = commonDAO.getPaymentExtByPaymentId(paymentId);
    		if(rs != null){
    			while (rs.next()){
    				String key = rs.getString("AUTH_PROPERTY_NAME");
    				String value = rs.getString("AUTH_PROPERTY_VALUE");
    				paymentExtMap.put(key, value);
    			}
    		}
    	}catch(Exception e){
    		logger.error("Error occured while fetching payment ext table values for a payment id."+paymentId);
    		throw e;
    	}
    	return paymentExtMap;
    }
	
	enum PG_PS {
		AX,PP,DC,DI,MC,VI
	}
	
	/*enum PG_JCCAS {
		DC,DI,MC,VI
	}*/
	
}


