package com.ftd.oputil;
import com.ftd.oputil.common.CommonUtils;
import com.ftd.oputil.dao.OrderDAO;
import com.ftd.oputil.vo.OrderDetailVO;
import com.ftd.oputil.vo.OrderVO;
import com.ftd.osp.utilities.vo.MessageToken;
import java.sql.Connection;
import javax.naming.InitialContext;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;

public class OrderToolsServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    response.setHeader("pragma", "no-cache");
    PrintWriter out = response.getWriter();
    out.print("<HTML><HEAD><TITLE>Order Tools</TITLE></HEAD>");
    out.print("<BODY><h1>Order Tools</h1>");
    out.print("<FORM METHOD=POST>");
    out.print("Order Detail ID: <INPUT TYPE=TEXT NAME=orderDetail>");
    out.print("   <INPUT TYPE=SUBMIT NAME=action VALUE=Reset>");
    out.print("   <INPUT TYPE=SUBMIT NAME=action VALUE=Process>");
    out.print("   <INPUT TYPE=SUBMIT NAME=action VALUE=Customer>");
    out.print("</FORM>");
    out.print("</BODY></HTML>");
    out.close();
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    String orderDetailId = request.getParameter("orderDetail");
    String msg = null;
    if(orderDetailId == null)
    {
      response.sendError(response.SC_BAD_REQUEST, "No order detail record specified");      return;
    }
    if(request.getParameter("action").equals("Reset"))
    {
      Connection conn = null;
      try {
        conn = CommonUtils.getDataSource().getConnection();
        OrderDAO dao = new OrderDAO(conn);
        //dao.clearFlorist(orderDetailId);
        
        out.println("<html>");
        out.println("<head><title>Order Process</title></head>");
        out.println("<body>");
        out.println("<h1> Order " + orderDetailId + " Reset</h1>");
        out.println("</body></html>");
        out.close();
      } catch (Exception e) 
      {
        e.printStackTrace(out);
        out.close();
      }
      finally
      {
        //close the connection
        try
        {
          conn.close();
        }
        catch(Exception e)
        {}
      }

    } else if(request.getParameter("action").equals("Process")) 
    {
      try {
        MessageToken token = new MessageToken();
        token.setMessage(orderDetailId);
        CommonUtils.sendJMSMessage(new InitialContext(), token, "PROCESS ORDER", "PROCESSORDER");
        out.println("<html>");
        out.println("<head><title>Order Process</title></head>");
        out.println("<body>");
        out.println("<h1> Order " + orderDetailId + " processed</h1>");
        out.println("</body></html>");
        out.close();
      } catch (Exception e) 
      {
        e.printStackTrace(out);
        out.close();
      }
    } else if(request.getParameter("action").equals("Customer")) 
    {
      Connection conn = null;
      try 
      {
        conn = CommonUtils.getDataSource().getConnection();
        OrderDAO dao = new OrderDAO(conn);
        OrderDetailVO detail = dao.getOrderDetail(orderDetailId);
        if(detail == null) throw new Exception("Order Detail Id not found");
        OrderVO order = dao.getOrder(detail.getOrderGuid());
        String recipient = dao.getCustomerHold(detail.getRecipientId(), order.getLossPreventionIndicator());
        String customer = dao.getCustomerHold(order.getCustomerId(), order.getLossPreventionIndicator());
        out.println("<html>");
        out.println("<head><title>Order Process</title></head>");
        out.println("<body>");
        out.println("<h1> Customer Hold for order:" + orderDetailId + " </h1>");
        out.println("<h2> Customer: " + customer + "</h2>");
        out.println("<h2> Recipient: " + recipient + "</h2>");
        out.println("</body></html>");
        out.close();
        
      } catch(Exception e) 
      {
        e.printStackTrace(out);
        out.close();
      }
      finally
      {
        //close the connection
        try
        {
          conn.close();
        }
        catch(Exception e)
        {}
      }
    }
  }
}