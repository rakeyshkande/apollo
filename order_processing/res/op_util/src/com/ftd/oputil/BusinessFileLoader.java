package com.ftd.oputil;
import com.ftd.oputil.dao.BusinessFileDAO;
import com.ftd.oputil.vo.BusinessFileVO;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;

public class BusinessFileLoader 
{
  private Connection conn;
  public void loadBusinessFile(String file)
  {
    String in = null;
    try {
      BusinessFileDAO dao  = new BusinessFileDAO(conn);
      //get an input stream for the file
      BufferedReader br = new BufferedReader(new FileReader(file));
      while((in = br.readLine()) != null)
      {
        String typeChar = in.substring(0,1);
        String number = in.substring(1,6);
        String name = in.substring(6,36);
        String address = in.substring(37,67);
        String city = in.substring(68,98);
        String state = in.substring(98,100);
        String zip = in.substring(100,110);
        String phone = in.substring(110);

        BusinessFileVO line = new BusinessFileVO();
        line.setId(number != null ? number.trim().toUpperCase() : null);
        line.setName(name != null ? name.trim().toUpperCase() : null);
        line.setAddress(address != null ? address.trim().toUpperCase() : null);
        line.setCity(city != null ? city.trim().toUpperCase() : null);
        line.setState(state != null ? state.trim().toUpperCase() : null);
        line.setZipcode(zip != null ? zip.trim().toUpperCase() : null);
        line.setPhone(phone != null ? phone.trim().toUpperCase() : null);
        
        if(typeChar.equalsIgnoreCase("F")) {
            line.setType("FUNERAL HOME");
        } else if(typeChar.equalsIgnoreCase("H")) 
        {
            line.setType("HOSPITAL");
        } else if(typeChar.equalsIgnoreCase("N")) 
        {
            line.setType("NURSING HOME");
        } else {
            line.setType("UNKNOWN");
        }        
        dao.insertBusinessFileRecord(line);
      }
    } catch (Exception e) 
    {
      System.out.println(in);
      e.printStackTrace();
    } 
  }
  public void clearBusinessFile() 
  {
    try {
      new BusinessFileDAO(conn).deleteBusinessFile();
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
  }
  public BusinessFileLoader(Connection conn)
  {
    this.conn = conn;
  }

  /**
   * 
   * @param args
   */
  public static void main(String[] args)
  {
    if (args.length != 4) 
    {
      System.out.println("Parameter 1 is the connection string");
      System.out.println("Parameter 2 is the full path to the file");
      System.out.println("Parameter 3 is the database user name");
      System.out.println("Parameter 4 is the database password");
      System.out.println("Bad Parameter list -- FILE NOT LOADED");
      return;
    }
    
    String databaseConnectionString = args[0];
    String file = args[1];
    String user = args[2];
    String password = args[3];
    System.out.println("Connection String: " + databaseConnectionString);
    System.out.println("File to load: " + file);
    Connection connection = null;
    try {
      String driver_ = "oracle.jdbc.driver.OracleDriver";
      String database_ = databaseConnectionString;
      //String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1522:dev5";
      String user_ = user;
      //String user_ = "osp";
      String password_ = password;
      //String password_ = "osp";
      
      Class.forName(driver_);
      connection = DriverManager.getConnection(database_, user_, password_);
      BusinessFileLoader businessFileLoader = new BusinessFileLoader(connection);    
      businessFileLoader.clearBusinessFile();
      System.out.println("Started at: " + new Date().toString());
      businessFileLoader.loadBusinessFile(file);
      System.out.println("Ended at: " + new Date().toString());
      
    } catch (Exception e) 
    {
      e.printStackTrace();
    } finally 
    {
      try {
        connection.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace();
      }
    }
  }
}