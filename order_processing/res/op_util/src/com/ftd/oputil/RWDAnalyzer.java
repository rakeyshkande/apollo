package com.ftd.oputil;
import com.ftd.oputil.common.CommonUtils;
import com.ftd.oputil.dao.OrderDAO;
import com.ftd.oputil.vo.CustomerVO;
import com.ftd.oputil.vo.FloristVO;
import com.ftd.oputil.vo.OrderBillVO;
import com.ftd.oputil.vo.OrderDetailVO;
import com.ftd.oputil.vo.ProductVO;
import com.ftd.oputil.vo.RWDFloristVO;
import com.ftd.oputil.vo.RWDFloristVOList;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import javax.sql.DataSource;

public class RWDAnalyzer 
{
  private DataSource db;
  private Connection conn; 
  private OrderDetailVO orderDetail;
  private ProductVO product;
  private CustomerVO recipient;
  private String codification;
  private boolean productCodified = false;
  private boolean addBreak = false;;
  private RWDFloristVOList routableFlorists = new RWDFloristVOList();
  PrintWriter out;
  
  public RWDAnalyzer(PrintWriter output, boolean web)
  {
    try {
      db = CommonUtils.getDataSource();
      out = output;
      addBreak = web;
    } catch (Exception e) 
    {
      e.printStackTrace();
      e.printStackTrace(out);
    }
  }
  public void analyzeRWD() 
  {
    RWDFloristVOList topTier = new RWDFloristVOList();
    int topScore = 0;

    // find top tier florists
    Iterator iter = routableFlorists.getFloristList().iterator();
    while(iter.hasNext()) 
    {
      RWDFloristVO florist = (RWDFloristVO)iter.next();
      if((florist.getScore() >= topScore)) 
      {
        topScore = florist.getScore();
      } 
    }
    iter = routableFlorists.getFloristList().iterator();
    while(iter.hasNext())
    {
      RWDFloristVO florist = (RWDFloristVO)iter.next();
      if(florist.getScore() == topScore)
      {
        topTier.add(florist);
      }
    }
    int totalWeightSquared = topTier.getWeightSquaredTotal();
    if(addBreak) {
      out.println("<h1> Top Tier Florist Analysis</h1>");
      out.println("<b> Top score: " + topScore + "</b><br>");
      out.println("<table border=2><tr><th>#</th><th>Florist Id</th><th>Percentage Chosen</th></tr>");
      
    } else {
      out.println();
      out.println("------------------------------");
      out.println("Top Tier Florist Analysis");
      out.println("\tTop Score: " + topScore);
      out.println();
      out.println("#\tFlorist Id \tPercentage Chosen");
      out.println("-------------------------------------------------------");
    }
    Iterator iter2 = topTier.getFloristList().iterator();
    int i = 1;
    while(iter2.hasNext())
    {
      RWDFloristVO topFlorist = (RWDFloristVO)iter2.next();
      double floristPct = 100 * ((double)topFlorist.getWeightSquared() / topTier.getWeightSquaredTotal());
      
      if(addBreak)  
      {
        out.println("<tr><td>" + i + "</td><td>" + topFlorist.getFloristId() + "</td><td>" + floristPct + "</td></tr>");

      } else 
      {
        out.println(i + "\t" + topFlorist.getFloristId() + "\t\t" + floristPct); 
      }
      i++;
    }
    if(addBreak) 
    {
      out.println("</table>");
    }
  }
  public void analyzeFlorists(){
    OrderDAO dao = new OrderDAO(conn);
    LinkedList list = null;
    int weightSquared = 0;
    try 
    {
      RWDFloristVOList rwdList = dao.getRoutableFlorists(orderDetail, true);
      weightSquared = rwdList.getWeightSquaredTotal();
      list = rwdList.getFloristList();
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    }
    if(addBreak) 
    {
      out.println("<h1>Florist analysis:</h1>");
    } else 
    {
      out.println("Florist analysis:");
    }
    Iterator iter = list.iterator();
    while(iter.hasNext())
    {
      RWDFloristVO florist = (RWDFloristVO)iter.next();
      if(addBreak) {
        out.println("<b>Analysis for " + florist.getFloristName() + "</b><br>");
      } else 
      {
        out.println("Analysis for " + florist.getFloristName());
      }
      analyzeFlorist(florist);
      if(addBreak) out.println("<strike>--------------------------------------------</strike><br>");
    }
    if(!addBreak) {
      out.println("---------------------------------------------");
    }
  }
  private boolean isFloristActive(RWDFloristVO florist)
  {
    if(florist.getStatus().equalsIgnoreCase("Active") ||
        (florist.getStatus().equalsIgnoreCase("Blocked") &&
         florist.getScore() > 32)) 
    {
      return true;
    }
    return false;
  }
  private boolean isFloristZipBlocked(RWDFloristVO florist)
  {
    String queryString = 
      "select block_start_date, block_end_date " + 
      " from ftd_apps.florist_zips " + 
      " where florist_id = '" + florist.getFloristId() + "' and " + 
      "       zip_code = '" + recipient.getZipCode() + "'";

    Statement s = null; 
    Date startDate = null;
    Date endDate = null;
    try {
      s = conn.createStatement();
      ResultSet r = s.executeQuery(queryString);
      if(r.next()) 
      {
        startDate = r.getDate("block_start_date");
        endDate = r.getDate("block_end_date");
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    if(startDate == null && endDate == null) 
    {
      return false;
    }
    if(isDateRangeIntersecting(orderDetail.getDeliveryDate(), orderDetail.getDeliveryDateEnd(), startDate, endDate))
    {
      return true;
    }
    return false;
  }
  private boolean isFloristAlreadyUsed(RWDFloristVO florist) 
  {
    boolean output = false;
    Statement s = null;
    try {
      s = conn.createStatement();
      String queryString = 
        "select florist_id " +
        " from clean.order_florist_used " + 
        " where florist_id = '" + florist.getFloristId() + "' and" +
        "       order_detail_id = '" + orderDetail.getOrderDetailId() + "'";
        
      ResultSet r = s.executeQuery(queryString);
      if(r.next()) 
      {
        if (r.getString("florist_id") != null) 
        {
          output = true;
        } else {
          output = false;
        }
      } else 
      {
        output = false;
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try 
      {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    return output;
  }
  private boolean canDeliverSunday(RWDFloristVO florist) 
  {
    Date deliveryDate = orderDetail.getDeliveryDate();
    SimpleDateFormat formatter = new SimpleDateFormat("EEE");
    String dayOfWeek = formatter.format(deliveryDate);

    boolean output = true;

    if(dayOfWeek.equalsIgnoreCase("SUN") && 
       orderDetail.getDeliveryDateEnd() == null) { 
      if(florist.getSundayDeliveryFlag().equalsIgnoreCase("N"))
      {
        output = false;
      }
    }
    return output;
  }
  private boolean isFloristCodified(RWDFloristVO florist)
  {    
    Statement s = null;
    boolean output = false;
    try {
      s = conn.createStatement();
      ResultSet r = s.executeQuery(
                      "select codification_id " +
                      " from ftd_apps.florist_codifications" +
                      " where florist_id = '" + florist.getFloristId() + "' and " +
                      "       codification_id = '" + codification + "'");

      String codification_id = null;
      if(r.next())
      {
        codification_id = r.getString("codification_id");
      }
  
      if(codification_id != null) 
      {
        output = true;
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    return output;
  }
   /** 
    * This method determines if the test range intersects the base range
    * 
    */
  private boolean isDateRangeIntersecting(Date testRangeStart, Date testRangeEnd, Date baseRangeStart, Date baseRangeEnd) 
  {

    if (baseRangeEnd == null) 
    {
      if(testRangeEnd == null) {
        return testRangeStart.compareTo(baseRangeStart) >= 0;
      } else 
      {
        return testRangeEnd.compareTo(baseRangeStart) >= 0;
      }
    } else 
    {
      if(testRangeEnd == null) 
      {
        return (testRangeStart.compareTo(baseRangeStart) >= 0 && testRangeStart.compareTo(baseRangeEnd) <=0);
      } else 
      {
        return  (testRangeStart.compareTo(baseRangeStart) >= 0 && testRangeStart.compareTo(baseRangeEnd) <=0) ||
                (testRangeEnd.compareTo(baseRangeStart) >= 0 && testRangeEnd.compareTo(baseRangeEnd) <=0) ||
                (testRangeStart.compareTo(baseRangeStart) <= 0 && testRangeEnd.compareTo(baseRangeEnd) >= 0); 
      }
    }
  }
  private boolean isCodificationBlocked(RWDFloristVO florist) 
  {
    boolean output = false;
    if(!productCodified) {
      output = false;
    }
    String queryString = 
      "select block_start_date, block_end_date " + 
      " from ftd_apps.florist_codifications " + 
      " where florist_id = '" + florist.getFloristId() + "' and " + 
      "       codification_id = '" + codification + "'";

    Statement s = null; 
    Date startDate = null;
    Date endDate = null;
    try {
      s = conn.createStatement();
      ResultSet r = s.executeQuery(queryString);
      if(r.next()) 
      {
        startDate = r.getDate("block_start_date");
        endDate = r.getDate("block_end_date");
      } else 
      {
        // florist not codified
        return false;
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    if(startDate == null && endDate == null) 
    {
      output = false;
    } else 
    {

      
      if(isDateRangeIntersecting(orderDetail.getDeliveryDate(),orderDetail.getDeliveryDateEnd(), startDate, endDate))
      {
        output = true;
      }
    }
    return output;
  }
  private boolean isFloristPastCutoff(FloristVO florist) 
  {
    SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    
    String orderDate = dateFormat.format(orderDetail.getDeliveryDate());
    String today = dateFormat.format(new Date());
    String currentTime = timeFormat.format(new Date());
    
    // if order date is later than today
    if(orderDate.compareTo(today) > 0) 
    {
      return false;
    } else 
    {
      String cutoffTime = getCutoffTime(florist);
      if (cutoffTime == null) cutoffTime = new String("1400");
      if(currentTime.compareTo(cutoffTime) > 0) 
      {
        return true;
      }
    }
    return false;
  }
  private String getCutoffTime(FloristVO florist) 
  {
    // get florist cutoff time adjusted for timezone if it exists, otherwise get the default  
    String output = null;
    String queryString  = 
      "select NVL(ftd_apps.MISC_PKG.CHANGE_TIMES_TIMEZONE(fz.cutoff_time,sm.time_zone,sm.state_master_id)," +
                 "ftd_apps.MISC_PKG.CHANGE_TIMES_TIMEZONE('" + getDefaultCutoffTime(orderDetail.getDeliveryDate()) +"', sm.time_zone, sm.state_master_id)) " +
      " from ftd_apps.florist_zips fz, ftd_apps.state_master sm" +
      " where sm.state_master_id = '" + florist.getState() + "' and " + 
      "       fz.florist_id = '" + florist.getFloristId() + "' and " +
      "       fz.zip_code = '" + recipient.getZipCode() + "'";
      
    Statement s = null;
    try {
      s = conn.createStatement();
      ResultSet r = s.executeQuery(queryString);
      
      if(r.next())
      {
        output = r.getString(1);
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    return output;
  }
  private String getDefaultCutoffTime(Date deliveryDate)
  {
    SimpleDateFormat formatter = new SimpleDateFormat("EEE");
    String dayOfWeek = formatter.format(deliveryDate);

    String output = null;

    Statement s = null;
    try {
      s = conn.createStatement();
      String queryString =         
        "SELECT  monday_cutoff, tuesday_cutoff, " + 
        "        wednesday_cutoff, thursday_cutoff," +
        "        friday_cutoff, saturday_cutoff," +
        "        sunday_cutoff " +
        "  FROM ftd_apps.global_parms " +
        "  WHERE global_parms_key = 1";

      ResultSet r = s.executeQuery(queryString);
      if(r.next()){
        if(dayOfWeek.equalsIgnoreCase("MON")){
          output = r.getString("monday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("TUE")) 
        {
            output = r.getString("tuesday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("WED")) 
        {
            output = r.getString("wednesday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("THU")) 
        {
            output = r.getString("thursday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("FRI")) 
        {
            output = r.getString("friday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("SAT")) 
        {
            output = r.getString("saturday_cutoff");
        } else if (dayOfWeek.equalsIgnoreCase("SUN")) 
        {
            output = r.getString("sunday_cutoff");
        } else {
            output = null;
        }
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    return output;
  }
  private boolean isPriceTooLow(FloristVO florist) 
  {
    // if florist is GOTO florist, ignore check
    if(florist.getSuperFloristFlag().equalsIgnoreCase("Y")) return false;
    OrderDAO dao = new OrderDAO(conn);
    double floristPrice = florist.getMinimumOrderAmount();
    OrderBillVO orderBill = dao.getOrderBill(new String("" + orderDetail.getOrderDetailId()));
    double orderPrice = orderBill.getProductAmount();
    if(orderPrice >= floristPrice) 
    {
      return false;
    }
    return true;
  }
  
  public void analyzeFlorist(RWDFloristVO florist)
  {
    OrderDAO dao = new OrderDAO(conn);
    FloristVO floristMaster = dao.getFlorist(florist.getFloristId());
    boolean floristCodified = false;
    boolean eliminated = false;
    if(addBreak) 
    {
      out.println("<b>Florist Id: " + florist.getFloristId() + "</b><br>");
      out.println("<b>Base Score: " + florist.getScore() + "</b><br>");
  
      // true score eliminates status bits
      out.println("<b>True Score: " + (florist.getScore() / 4) + "</b><br>");
      out.println("<b>Weight: " + florist.getFloristWeight() + "</b><br>");
      out.println("<b>Min order amount: " + floristMaster.getMinimumOrderAmount() + "</b><br>");
      out.println("<b>EFOS city state number: " + floristMaster.getCityStateNumber() + "</b><br>");
      
    } else {
      out.println("\tFlorist Id: " + florist.getFloristId());
      out.println("\tBase Score: " + florist.getScore());

      // true score eliminates status bits
      out.println("\tTrue Score: " + (florist.getScore() / 4));
      out.println("\tWeight: " + florist.getFloristWeight());
      out.println("\tMin order amount: " + floristMaster.getMinimumOrderAmount());
      out.println("\tEFOS city state number: " + floristMaster.getCityStateNumber());
    }
    if(florist.getSuperFloristFlag() != null &&
       florist.getSuperFloristFlag().equals("Y"))
    {
      if(addBreak) {
        out.println("<b>GOTO Florist</b><br>");
      } else 
      {
        out.println("\tGOTO Florist");
      }
    } else 
    {
      if(addBreak) {
        out.println("<b>Not a GOTO Florist</b><br>");
      } else {
        out.println("\tNot a GOTO Florist");
      }
    }
    if(florist.getSundayDeliveryFlag() == null) 
    {
      if(addBreak) {
        out.println("<b>Sunday delivery Not OK</b><br>");
      } else {
        out.println("\tSunday delivery Not OK");
      }
    }
    else if(florist.getSundayDeliveryFlag().equals("Y"))
    {
      if(addBreak) {
        out.println("<b>Sunday delivery OK</b><br>");
      } else {
        out.println("\tSunday delivery OK");
      }
    } else 
    {
      if(addBreak) {
        out.println("<b>Sunday delivery Not OK</b><br>");
      } else {
        out.println("\tSunday delivery Not OK");
      }
    }
    if(productCodified) {
      if (isFloristCodified(florist)) 
      {
        floristCodified = true;
        if(addBreak) 
        {
          out.println("<b> Florist is codified for product</b><br>");
        }
        else {
          out.println("\tFlorist is codified for product");
        }
      } else 
      {
        floristCodified = false;
        if(addBreak) 
        {
          out.println("<b>Florist is not codified for product</b><br>");
        
        } else 
        {
          out.println("\tFlorist is not codified for product");
        }
      }
    } else 
    {
      // this is so we don't skip the low price check later
      floristCodified = false;
      if(addBreak) 
      {
        out.println("<b>Product is not codified</b><br>");
      } else {
        out.println("\tProduct is not codified");
      }
    }
    if(florist.getMercuryFlag() == null || florist.getMercuryFlag().equals("N"))
    {
      if(addBreak) 
      {
        out.println("<b>Manual Florist</b><br?");
      } else 
      {
        out.println("\tManual Florist");
      }
    } else 
    {
      if(addBreak)
      {
        out.println("<b>Florist has Mercury Machine</b><br>");
      } else 
      {
        out.println("\tFlorist has Mercury Machine");
      }
    }
    if(addBreak) {
      out.println("<b>Elimination Reasons:</b><br>");
    } else 
    {
      out.println("\tElimination Reasons: ");
    }
    if(addBreak) out.println("<UL>");
    if(!isFloristActive(florist))
    {
      if(addBreak) {
        out.println("<LI>Florist is not active</LI>"); 
      } else 
      {
        out.println("\t\tFlorist is not active");
      }
      eliminated = true;
    }
    if(isFloristZipBlocked(florist))
    {
      if(addBreak) {
        out.println("<LI>Florist is blocked for this zip code</LI>");
      } else 
      {
        out.println("\t\tFlorist is blocked for this zip code");
      }
      eliminated = true;
    }
    if(isFloristPastCutoff(floristMaster))
    {
      if(addBreak) 
      {
        out.println("<li>Past cutoff time for this florist </li>"); 
      } else {
        out.println("\t\tPast cutoff time for this florist");
      }
      eliminated = true;
    } 
    if(isCodificationBlocked(florist)) 
    {
      if(addBreak) 
      {
        out.println("<li>Florist has blocked this codification</li>");
      } else {
        out.println("\t\tFlorist has blocked this codification");
      }
      eliminated = true;
    }
    if(isPriceTooLow(floristMaster)) 
    {
      if(addBreak) {
        out.println("<li>Order price is too low for this florist</li>");
      } else 
      {
        out.println("\t\tOrder price is too low for this florist");
      }
      eliminated = true;
    }
    if(isFloristAlreadyUsed(florist))
    {
      if(addBreak) {
        out.println("<li>Florist is in used florist list for this order</li>");
      } else 
      {
        out.println("\t\tFlorist is in used florist list for this order");
      }
      eliminated = true;
    }
    if(!canDeliverSunday(florist)) 
    {
      if(addBreak) 
      {
        out.println("<li>Florist cannot deliver Sunday order</li>");
      } else 
      {
        out.println("\t\tFlorist cannot deliver Sunday order");
      }
      eliminated = true;
    }
    if(!eliminated) 
    {
      routableFlorists.add(florist);
      if(addBreak) {
        out.println("<li>None</li>");
      } else 
      {
        out.println("\t\tNone");
      }
    }
    out.println();
    if(addBreak) out.println("</UL>");
  }
  public void analyzeProduct()
  {
    Statement s = null;
    try {
      s = conn.createStatement();
      ResultSet r = s.executeQuery(
                      "select codification_id " +
                      " from ftd_apps.codified_products" +
                      " where product_id = '" + orderDetail.getProductId() + "'");
      
      String codification_id = null;
      if(r.next())
      {
        codification_id = r.getString("codification_id");
      }
      
      if(addBreak) {
        out.println("<h1>Product Analysis:</h1>");
        out.println("<b>Product Id: " + product.getProductId() + "</b><br>");
        out.println("<b>Product Name: " + product.getProductName() + "</b><br>");
        out.println("<b>Product Type: " + product.getProductType() + "</b><br>");
      } else 
      {
        out.println("Product Analysis:");
        out.println("\tProduct Id: " + product.getProductId());
        out.println("\tProduct Name: " + product.getProductName());
        out.println("\tProduct Type: " + product.getProductType());
      }
      if(codification_id != null) 
      {
        productCodified = true;
        codification = codification_id;
        if (addBreak) 
        {
          out.println("<b>Product is codified as: " + codification_id + "</b><br>");
          
        } else {
          out.println("\tProduct is codified as: " + codification_id);
        }
      } else 
      {
        productCodified = false;
        codification = null;
        if (addBreak) {
          out.println("<b>Product is not codified</b><br>");
        } else 
        {
          out.println("\tProduct is not codified");
        }
      }
      if (!addBreak) out.println("---------------------------------------------");
      
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try {
        s.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
  }
  public void analyzeOrder()
  {
    try {
      OrderDAO dao = new OrderDAO(conn);
      LinkedList dailyFloristList = dao.getDailyFlorist(orderDetail);
      String dailyFloristString = new String();
      Iterator iter = dailyFloristList.iterator();
      while(iter.hasNext()) 
      {
        dailyFloristString += (String)iter.next() + " : ";
      }
      if(addBreak) {
        out.println("<h1>Order Analysis:</h1>");
        out.println("<b>Order Detail Id: " + orderDetail.getOrderDetailId() + "</b><br>");
        out.println("<b>Zip Code:" + recipient.getZipCode() + "</b><br>");
        out.println("<b>Product: " + orderDetail.getProductId() + "</b><br>");
        out.println("<b>Delivery Date: " + orderDetail.getDeliveryDate() + "</b><br>");
        out.println("<b>Delivery Date End: " + orderDetail.getDeliveryDateEnd() + "</b><br>");
        if(dailyFloristList != null) {
          out.println("<b>Daily Florist Exists: " + dailyFloristString + "</b><br>");
        } else 
        {
          out.println("<b>No Daily Florist</b><br>");
        }
      } else 
      {
        out.println("Order Analysis:");
        out.println("\tOrder Detail Id: " + orderDetail.getOrderDetailId());
        out.println("\tZip Code:" + recipient.getZipCode());
        out.println("\tProduct: " + orderDetail.getProductId());
        out.println("\tDelivery Date: " + orderDetail.getDeliveryDate());
        out.println("\tDelivery Date End: " + orderDetail.getDeliveryDateEnd());
        if(dailyFloristList != null) {
          out.println("\tDaily Florist Exists: " + dailyFloristString);
        } else 
        {
          out.println("\tNo Daily Florist");
        }
        out.println("---------------------------------------------");
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    }
  }  
  public String getOrderDetail(String externalOrderNumber)
  {
    String output = null;
    Statement s = null;
    try 
    {
      conn = db.getConnection();
      String query = "select order_detail_id " +
                     " from clean.order_details " +
                     " where external_order_number = '" +
                          externalOrderNumber + "'";
      s = conn.createStatement();
      ResultSet r = s.executeQuery(query);
      while(r.next())
      {
        output = r.getString(1);
      }
    } catch (Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    } finally 
    {
      try 
      {
        s.close();
        conn.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    return output;
  }
  public void analyze(String orderDetailId) 
  {
    try {
      conn = db.getConnection();
      OrderDAO dao = new OrderDAO(conn);
      orderDetail = dao.getOrderDetail(orderDetailId);
      product = dao.getProduct(orderDetail.getProductId());
      recipient = dao.getCustomer(orderDetail.getRecipientId());
      // modify recipient for canada lookups
      if(recipient.getCountry().equalsIgnoreCase("CA"))
      {
        recipient.setZipCode(new String(recipient.getZipCode().substring(0,3)));
      }
      
      analyzeOrder();
      analyzeProduct();
      analyzeFlorists();
      analyzeRWD();
    } catch(Exception e) 
    {
      e.printStackTrace(out);
      e.printStackTrace();
    }finally 
    {
      try {
        conn.close();
      } catch (Exception e1) 
      {
        e1.printStackTrace(out);
        e1.printStackTrace();
      }
    }
    
  }
  /**
   * 
   */
  public static void main(String[] args)
  {
    RWDAnalyzer rwdAnalyzer = new RWDAnalyzer(new PrintWriter(System.out), false);
    rwdAnalyzer.analyze("1459753");
  }
}