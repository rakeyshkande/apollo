package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class GlobalParameterVO
{
  private String context;
  private String name;
  private String value;
  
  public GlobalParameterVO()
  {
  }
  
  public void setName(String name)
  {
    this.name = trim(name);
  }

  public String getName()
  {
    return name;
  }
  
  public void setContext(String context)
  {
    this.context = trim(context);
  }

  public String getContext()
  {
    return context;
  }
  
  public void setValue(String value)
  {
    this.value = trim(value);
  }

  public String getValue()
  {
    return value;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
 
}
