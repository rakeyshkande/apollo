package com.ftd.oputil.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.Math;


import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RWDFloristVO
{
  private String floristId;
  private int score;
  private int floristWeight;
  private String floristName;
  private String phoneNumber;
  private String sundayDeliveryFlag;
  private String mercuryFlag;
  private String status;
  private String superFloristFlag;
  private String cutoffTime;
  
  public RWDFloristVO()
  {
  }
  
  public void setFloristId(String floristId)
  {
    this.floristId = floristId;
  }


  public String getFloristId()
  {
    return floristId;
  }
  
  public void setScore(int score)
  {
    this.score = score;
  }


  public int getScore()
  {
    return score;
  }
  
  public void setFloristWeight(int floristWeight)
  {
    this.floristWeight = floristWeight;
  }


  public int getFloristWeight()
  {
    return floristWeight;
  }
  
  public void setFloristName(String floristName)
  {
    this.floristName = floristName;
  }


  public String getFloristName()
  {
    return floristName;
  }
  
  public void setPhoneNumber(String phoneNumber)
  {
    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }
  
  public void setSundayDeliveryFlag(String sundayDeliveryFlag)
  {
    this.sundayDeliveryFlag = sundayDeliveryFlag;
  }


  public String getSundayDeliveryFlag()
  {
    return sundayDeliveryFlag;
  }
  
  public void setMercuryFlag(String mercuryFlag)
  {
    this.mercuryFlag = mercuryFlag;
  }


  public String getMercuryFlag()
  {
    return mercuryFlag;
  }
  
  public void setStatus(String status)
  {
    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }
  
  public void setSuperFloristFlag(String superFloristFlag)
  {
    this.superFloristFlag = superFloristFlag;
  }


  public String getSuperFloristFlag()
  {
    return superFloristFlag;
  }
  
  public void setCutoffTime(String cutoffTime)
  {

    this.cutoffTime = cutoffTime;
  }


  public String getCutoffTime()
  {
    return cutoffTime;
  }
  
  public int getWeightSquared()
  {
    return this.floristWeight * this.floristWeight/*Math.sqrt(this.floristWeight)*/; 
  }

  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }  
}
