package com.ftd.oputil.vo;

public class BusinessFileVO 
{
  private String id;
  private String name;
  private String type;
  private String address;
  private String city;
  private String state;
  private String zipcode;
  private String phone;
  
  public BusinessFileVO()
  {
  }


  public void setId(String id)
  {
    this.id = id;
  }


  public String getId()
  {
    return id;
  }


  public void setName(String name)
  {
    this.name = name;
  }


  public String getName()
  {
    return name;
  }


  public void setType(String type)
  {
    this.type = type;
  }


  public String getType()
  {
    return type;
  }


  public void setAddress(String address)
  {
    this.address = address;
  }


  public String getAddress()
  {
    return address;
  }


  public void setCity(String city)
  {
    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {
    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZipcode(String zipcode)
  {
    this.zipcode = zipcode;
  }


  public String getZipcode()
  {
    return zipcode;
  }


  public void setPhone(String phone)
  {
    this.phone = phone;
  }


  public String getPhone()
  {
    return phone;
  }
}