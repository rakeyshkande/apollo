package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class FloristVO
{
  private String floristId;
  private String floristName;
  private String address;
  private String city;
  private String state;
  private String zipCode;
  private String phoneNumber;
  private String cityStateNumber;
  private String recordType;
  private String mercuryFlag;
  private int floristWeight;
  private String superFloristFlag;
  private String sundayDeliveryFlag;
  private String deliveryCity;
  private String deliveryState;
  private String ownersName;
  private double minimumOrderAmount;
  private String status;
  private String territory;
  private String faxNumber;
  private String emailAddress;
  private int initialWeight;
  private int adjustedWeight;
  private Date adjustedWeightStartDate;
  private Date adjustedWeightEndDate;
  private String adjustedWeightPermenantFlag;
  private String topLevelFloristId;
  private String parentFloristId;
  private String altPhoneNumber;
  private int ordersSent;
  private int sendingRank;
  private Date sundayDeliveryBlockStDt;
  private Date sundayDeliveryBlockEndDt;
  private String lockedBy;
  private Date lockedDate;
  private String internalLinkNumber;
  private String vendorFlag;
  private String suspendOverrideFlag;
  private String allowMessageForwardingFlag;
  private String isFTOFlag;
  
  public FloristVO()
  {
  }
  
  public void setFloristId(String floristId)
  {

    this.floristId = floristId;
  }


  public String getFloristId()
  {
    return floristId;
  }


  public void setFloristName(String floristName)
  {

    this.floristName = floristName;
  }


  public String getFloristName()
  {
    return floristName;
  }


  public void setAddress(String address)
  {

    this.address = address;
  }


  public String getAddress()
  {
    return address;
  }


  public void setCity(String city)
  {

    this.city = city;
  }


  public String getCity()
  {
    return city;
  }


  public void setState(String state)
  {

    this.state = state;
  }


  public String getState()
  {
    return state;
  }


  public void setZipCode(String zipCode)
  {

    this.zipCode = zipCode;
  }


  public String getZipCode()
  {
    return zipCode;
  }


  public void setPhoneNumber(String phoneNumber)
  {

    this.phoneNumber = phoneNumber;
  }


  public String getPhoneNumber()
  {
    return phoneNumber;
  }


  public void setCityStateNumber(String cityStateNumber)
  {

    this.cityStateNumber = cityStateNumber;
  }


  public String getCityStateNumber()
  {
    return cityStateNumber;
  }


  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }


  public String getRecordType()
  {
    return recordType;
  }


  public void setMercuryFlag(String mercuryFlag)
  {

    this.mercuryFlag = mercuryFlag;
  }


  public String getMercuryFlag()
  {
    return mercuryFlag;
  }


  public void setFloristWeight(int floristWeight)
  {

    this.floristWeight = floristWeight;
  }


  public int getFloristWeight()
  {
    return floristWeight;
  }


  public void setSuperFloristFlag(String superFloristFlag)
  {

    this.superFloristFlag = superFloristFlag;
  }


  public String getSuperFloristFlag()
  {
    return superFloristFlag;
  }


  public void setSundayDeliveryFlag(String sundayDeliveryFlag)
  {

    this.sundayDeliveryFlag = sundayDeliveryFlag;
  }


  public String getSundayDeliveryFlag()
  {
    return sundayDeliveryFlag;
  }


  public void setDeliveryCity(String deliveryCity)
  {

    this.deliveryCity = deliveryCity;
  }


  public String getDeliveryCity()
  {
    return deliveryCity;
  }


  public void setDeliveryState(String deliveryState)
  {

    this.deliveryState = deliveryState;
  }


  public String getDeliveryState()
  {
    return deliveryState;
  }


  public void setOwnersName(String ownersName)
  {

    this.ownersName = ownersName;
  }


  public String getOwnersName()
  {
    return ownersName;
  }


  public void setMinimumOrderAmount(double minimumOrderAmount)
  {
    this.minimumOrderAmount = minimumOrderAmount;
  }


  public double getMinimumOrderAmount()
  {
    return minimumOrderAmount;
  }


  public void setStatus(String status)
  {

    this.status = status;
  }


  public String getStatus()
  {
    return status;
  }


  public void setTerritory(String territory)
  {
    this.territory = territory;
  }


  public String getTerritory()
  {
    return territory;
  }


  public void setFaxNumber(String faxNumber)
  {

    this.faxNumber = faxNumber;
  }


  public String getFaxNumber()
  {
    return faxNumber;
  }


  public void setEmailAddress(String emailAddress)
  {

    this.emailAddress = emailAddress;
  }


  public String getEmailAddress()
  {
    return emailAddress;
  }


  public void setInitialWeight(int initialWeight)
  {

    this.initialWeight = initialWeight;
  }


  public int getInitialWeight()
  {
    return initialWeight;
  }


  public void setAdjustedWeight(int adjustedWeight)
  {

    this.adjustedWeight = adjustedWeight;
  }


  public int getAdjustedWeight()
  {
    return adjustedWeight;
  }


  public void setAdjustedWeightStartDate(Date adjustedWeightStartDate)
  {

    this.adjustedWeightStartDate = adjustedWeightStartDate;
  }


  public Date getAdjustedWeightStartDate()
  {
    return adjustedWeightStartDate;
  }


  public void setAdjustedWeightEndDate(Date adjustedWeightEndDate)
  {

    this.adjustedWeightEndDate = adjustedWeightEndDate;
  }


  public Date getAdjustedWeightEndDate()
  {
    return adjustedWeightEndDate;
  }


  public void setAdjustedWeightPermenantFlag(String adjustedWeightPermenantFlag)
  {

    this.adjustedWeightPermenantFlag = adjustedWeightPermenantFlag;
  }


  public String getAdjustedWeightPermenantFlag()
  {
    return adjustedWeightPermenantFlag;
  }


  public void setTopLevelFloristId(String topLevelFloristId)
  {
    this.topLevelFloristId = topLevelFloristId;
  }


  public String getTopLevelFloristId()
  {
    return topLevelFloristId;
  }


  public void setParentFloristId(String parentFloristId)
  {
    this.parentFloristId = parentFloristId;
  }


  public String getParentFloristId()
  {
    return parentFloristId;
  }


  public void setAltPhoneNumber(String altPhoneNumber)
  {
    this.altPhoneNumber = altPhoneNumber;
  }


  public String getAltPhoneNumber()
  {
    return altPhoneNumber;
  }


  public void setOrdersSent(int ordersSent)
  {
    this.ordersSent = ordersSent;
  }


  public int getOrdersSent()
  {
    return ordersSent;
  }


  public void setSendingRank(int sendingRank)
  {
    this.sendingRank = sendingRank;
  }


  public int getSendingRank()
  {
    return sendingRank;
  }


  public void setSundayDeliveryBlockStDt(Date sundayDeliveryBlockStDt)
  {
    this.sundayDeliveryBlockStDt = sundayDeliveryBlockStDt;
  }


  public Date getSundayDeliveryBlockStDt()
  {
    return sundayDeliveryBlockStDt;
  }


  public void setSundayDeliveryBlockEndDt(Date sundayDeliveryBlockEndDt)
  {
    this.sundayDeliveryBlockEndDt = sundayDeliveryBlockEndDt;
  }


  public Date getSundayDeliveryBlockEndDt()
  {
    return sundayDeliveryBlockEndDt;
  }


  public void setLockedBy(String lockedBy)
  {
    this.lockedBy = lockedBy;
  }


  public String getLockedBy()
  {
    return lockedBy;
  }


  public void setLockedDate(Date lockedDate)
  {
    this.lockedDate = lockedDate;
  }


  public Date getLockedDate()
  {
    return lockedDate;
  }


  public void setInternalLinkNumber(String internalLinkNumber)
  {
    this.internalLinkNumber = internalLinkNumber;
  }


  public String getInternalLinkNumber()
  {
    return internalLinkNumber;
  }


  public void setVendorFlag(String vendorFlag)
  {
    this.vendorFlag = vendorFlag;
  }


  public String getVendorFlag()
  {
    return vendorFlag;
  }


  public void setSuspendOverrideFlag(String suspendOverrideFlag)
  {
    this.suspendOverrideFlag = suspendOverrideFlag;
  }


  public String getSuspendOverrideFlag()
  {
    return suspendOverrideFlag;
  }
  
  public int getWeightSquared()
  {
    return this.floristWeight * 2;  
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


  public void setAllowMessageForwardingFlag(String allowMessageForwardingFlag)
  {
    this.allowMessageForwardingFlag = allowMessageForwardingFlag;
  }


  public String getAllowMessageForwardingFlag()
  {
    return allowMessageForwardingFlag;
  }


  public void setIsFTOFlag(String isFTOFlag)
  {
    this.isFTOFlag = isFTOFlag;
  }


  public String getIsFTOFlag()
  {
    return isFTOFlag;
  }
}
