package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class OrderDetailVO
{
  private	long	    orderDetailId;
  private	String	  orderGuid;
  private	String	  externalOrderNumber;
  private	String	  hpOrderNumber;
  private	String	  sourceCode;
  private	Date      deliveryDate;
  private	Date      deliveryDateEnd;
  private	long	    recipientId;
  private	String	  productId;
  private	long	    quantity;
  private	String	  color1;
  private	String	  color2;
  private	String	  substitutionIndicator;
  private	String	  sameDayGift;
  private	String	  occasion;
  private	String	  cardMessage;
  private	String	  cardSignature;
  private	String	  specialInstructions;
  private	String	  releaseInfoIndicator;
  private	String	  floristId;
  private	String	  shipMethod;
  private	Date      shipDate;
  private	String	  orderDispCode;
  private String    secondChoiceProduct;
  private long      zipQueueCount;
  private long      randomWeightDistFailures;
  private String    lpOrderIndicator;
  private Date      deliveryDateRangeEnd;
  private Date      scrubbedOn;
  private String    scrubbedBy;
  private String    aribaUnspscCode;
  private String    aribaPoNumber;
  private String    aribaAmsProjectCode;
  private String    aribaCostCenter;
  private String    sizeIndicator;
  private long      milesPoints;
  private String    subcode;
 
  public OrderDetailVO()
  {
    
  }

  public void setOrderDetailId(long orderDetailId)
  {

    this.orderDetailId = orderDetailId;
  }

  public long getOrderDetailId()
  {
    return orderDetailId;
  }

  public void setOrderGuid(String orderGuid)
  {

    this.orderGuid = trim(orderGuid);
  }

  public String getOrderGuid()
  {
    return orderGuid;
  }
  
  public void setExternalOrderNumber(String externalOrderNumber)
  {
    this.externalOrderNumber = trim(externalOrderNumber);
  }

  public String getExternalOrderNumber()
  {
    return externalOrderNumber;
  }

  public void setHpOrderNumber(String hpOrderNumber)
  {
    this.hpOrderNumber = trim(hpOrderNumber);
  }

  public String getHpOrderNumber()
  {
    return hpOrderNumber;
  }

  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = trim(sourceCode);
  }

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setDeliveryDate(Date deliveryDate)
  {
    this.deliveryDate = deliveryDate;
  }

  public Date getDeliveryDate()
  {
    return deliveryDate;
  }
  
  public void setDeliveryDateEnd(Date deliveryDateEnd)
  {
    this.deliveryDateEnd = deliveryDateEnd;
  }

  public Date getDeliveryDateEnd()
  {
    return deliveryDateEnd;
  }
  
  public void setRecipientId(long recipientId)
  {
    this.recipientId = recipientId;
  }

  public long getRecipientId()
  {
    return recipientId;
  }

  public void setProductId(String productId)
  {
    this.productId = trim(productId);
  }

  public String getProductId()
  {
    return productId;
  }

  public void setQuantity(long quantity)
  {
   this.quantity = quantity;
  }

  public long getQuantity()
  {
    return quantity;
  }

  public void setColor1(String color1)
  {
    this.color1 = trim(color1);
  }

  public String getColor1()
  {
    return color1;
  }

  public void setColor2(String color2)
  {
    this.color2 = trim(color2);
  }

  public String getColor2()
  {
    return color2;
  }

  public void setSubstitutionIndicator(String substitutionIndicator)
  {
    this.substitutionIndicator = trim(substitutionIndicator);
  }

  public String getSubstitutionIndicator()
  {
    return substitutionIndicator;
  }

  public void setSameDayGift(String sameDayGift)
  {
    this.sameDayGift = trim(sameDayGift);
  }

  public String getSameDayGift()
  {
    return sameDayGift;
  }

  public void setOccasion(String occasion)
  {
    this.occasion = trim(occasion);
  }

  public String getOccasion()
  {
    return occasion;
  }

  public void setCardMessage(String cardMessage)
  {
    this.cardMessage = trim(cardMessage);
  }

  public String getCardMessage()
  {
    return cardMessage;
  }

  public void setCardSignature(String cardSignature)
  {
    this.cardSignature = trim(cardSignature);
  }

  public String getCardSignature()
  {
    return cardSignature;
  }

  public void setSpecialInstructions(String specialInstructions)
  {
    this.specialInstructions = trim(specialInstructions);
  }

  public String getSpecialInstructions()
  {
    return specialInstructions;
  }

  public void setReleaseInfoIndicator(String releaseInfoIndicator)
  {
    this.releaseInfoIndicator = trim(releaseInfoIndicator);
  }

  public String getReleaseInfoIndicator()
  {
    return releaseInfoIndicator;
  }

  public void setFloristId(String floristId)
  {
    this.floristId = trim(floristId);
  }

  public String getFloristId()
  {
    return floristId;
  }

  public void setShipMethod(String shipMethod)
  {
    this.shipMethod = trim(shipMethod);
  }

  public String getShipMethod()
  {
    return shipMethod;
  }

  public void setShipDate(Date shipDate)
  {
    this.shipDate = shipDate;
  }

  public Date getShipDate()
  {
    return shipDate;
  }

  public void setOrderDispCode(String orderDispCode)
  {
    this.orderDispCode = trim(orderDispCode);
  }

  public String getOrderDispCode()
  {
    return orderDispCode;
  }
  
  public void setSecondChoiceProduct(String secondChoiceProduct)
  {
    this.secondChoiceProduct = trim(secondChoiceProduct);
  }

  public String getSecondChoiceProduct()
  {
    return secondChoiceProduct;
  }
  
  public void setZipQueueCount(long zipQueueCount)
  {
    this.zipQueueCount = zipQueueCount;
  }

  public long getZipQueueCount()
  {
    return zipQueueCount;
  }
  
  public void setRandomWeightDistFailures(long randomWeightDistFailures)
  {
    this.randomWeightDistFailures = randomWeightDistFailures;
  }

  public long getRandomWeightDistFailures()
  {
    return randomWeightDistFailures;
  }
   
  public void setLpOrderIndicator(String lpOrderIndicator)
  {
    this.lpOrderIndicator = trim(lpOrderIndicator);
  }

  public String getLpOrderIndicator()
  {
    return lpOrderIndicator;
  }
  
  public void setDeliveryDateRangeEnd(Date deliveryDateRangeEnd)
  {
    this.deliveryDateRangeEnd = deliveryDateRangeEnd;
  }


  public Date getDeliveryDateRangeEnd()
  {
    return deliveryDateRangeEnd;
  }


  public void setScrubbedOn(Date scrubbedOn)
  {
    this.scrubbedOn = scrubbedOn;
  }


  public Date getScrubbedOn()
  {
    return scrubbedOn;
  }


  public void setScrubbedBy(String scrubbedBy)
  {
    this.scrubbedBy = trim(scrubbedBy);
  }


  public String getScrubbedBy()
  {
    return scrubbedBy;
  }


  public void setAribaUnspscCode(String aribaUnspscCode)
  {
    this.aribaUnspscCode = trim(aribaUnspscCode);
  }


  public String getAribaUnspscCode()
  {
    return aribaUnspscCode;
  }


  public void setAribaPoNumber(String aribaPoNumber)
  {
    this.aribaPoNumber = trim(aribaPoNumber);
  }


  public String getAribaPoNumber()
  {
    return aribaPoNumber;
  }


  public void setAribaAmsProjectCode(String aribaAmsProjectCode)
  {
    this.aribaAmsProjectCode = trim(aribaAmsProjectCode);
  }


  public String getAribaAmsProjectCode()
  {
    return aribaAmsProjectCode;
  }


  public void setAribaCostCenter(String aribaCostCenter)
  {
    this.aribaCostCenter = trim(aribaCostCenter);
  }


  public String getAribaCostCenter()
  {
    return aribaCostCenter;
  }


  public void setSizeIndicator(String sizeIndicator)
  {
    this.sizeIndicator = trim(sizeIndicator);
  }


  public String getSizeIndicator()
  {
    return sizeIndicator;
  }


  public void setMilesPoints(long milesPoints)
  {
    this.milesPoints = milesPoints;
  }


  public long getMilesPoints()
  {
    return milesPoints;
  }


  public void setSubcode(String subcode)
  {
    this.subcode = trim(subcode);
  }


  public String getSubcode()
  {
    return subcode;
  }
 
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
}
