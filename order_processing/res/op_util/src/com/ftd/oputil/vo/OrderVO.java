package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;



public class OrderVO
{
  private	String	  orderGuid;
  private	String	  masterOrderNumber;
  private	long 	    customerId;
  private	long	    membershipId;
  private	String	  companyId;
  private	String	  sourceCode;
  private	String	  originId;
  private	Date    	orderDate;
  private	double	  orderTotal;
  private	double	  productTotal;
  private	double	  addOnTotal;
  private	double	  serviceFeeTotal;
  private	double	  shippingFeeTotal;
  private	double	  discountTotal;
  private	double	  taxTotal;
  private	String	  lossPreventionIndicator;
  

  public OrderVO()
  {
  }

  public void setOrderGuid(String orderGuid)
  {
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setCustomerId(long customerId)
  {
    this.customerId = customerId;
  }


  public long getCustomerId()
  {
    return customerId;
  }


  public void setCompanyId(String companyId)
  {
    this.companyId = trim(companyId);
  }


  public String getCompanyId()
  {
    return companyId;
  }

  public void setMasterOrderNumber(String masterOrderNumber)
  {
    this.masterOrderNumber = trim(masterOrderNumber);
  }


  public String getMasterOrderNumber()
  {
    return masterOrderNumber;
  }


  public void setMembershipId(long membershipId)
  {
    this.membershipId = membershipId;
  }


  public long getMembershipId()
  {
    return membershipId;
  }


  public void setSourceCode(String sourceCode)
  {
    this.sourceCode = trim(sourceCode);
  }


  public String getSourceCode()
  {
    return sourceCode;
  }


  public void setOriginId(String originId)
  {
    this.originId = trim(originId);
  }


  public String getOriginId()
  {
    return originId;
  }


  public void setOrderDate(Date orderDate)
  {
    this.orderDate = orderDate;
  }


  public Date getOrderDate()
  {
    return orderDate;
  }


  public void setOrderTotal(double orderTotal)
  {
    this.orderTotal = orderTotal;
  }


  public double getOrderTotal()
  {
    return orderTotal;
  }


  public void setProductTotal(double productTotal)
  {
    this.productTotal = productTotal;
  }


  public double getProductTotal()
  {
    return productTotal;
  }


  public void setAddOnTotal(double addOnTotal)
  {
    this.addOnTotal = addOnTotal;
  }


  public double getAddOnTotal()
  {
    return addOnTotal;
  }


  public void setServiceFeeTotal(double serviceFeeTotal)
  {
    this.serviceFeeTotal = serviceFeeTotal;
  }


  public double getServiceFeeTotal()
  {
    return serviceFeeTotal;
  }


  public void setShippingFeeTotal(double shippingFeeTotal)
  {
    this.shippingFeeTotal = shippingFeeTotal;
  }


  public double getShippingFeeTotal()
  {
    return shippingFeeTotal;
  }


  public void setDiscountTotal(double discountTotal)
  {
    this.discountTotal = discountTotal;
  }


  public double getDiscountTotal()
  {
    return discountTotal;
  }


  public void setTaxTotal(double taxTotal)
  {
    this.taxTotal = taxTotal;
  }


  public double getTaxTotal()
  {
    return taxTotal;
  }


  public void setLossPreventionIndicator(String lossPreventionIndicator)
  {
    this.lossPreventionIndicator = trim(lossPreventionIndicator);
  }


  public String getLossPreventionIndicator()
  {
    return lossPreventionIndicator;
  }


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }


}
