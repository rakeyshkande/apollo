package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class GnaddVO
{
  private String gnaddStatus;
  private String gnaddDate;
  private String gnaddHoldFlag;
  
  public GnaddVO()
  {
  }
  
  public void setGnaddStatus(String gnaddStatus)
  {
    this.gnaddStatus = trim(gnaddStatus);
  }
  
  public String getGnaddStatus()
  {
    return gnaddStatus;
  }
  
  public void setGnaddDate(String gnaddDate)
  {

    this.gnaddDate = gnaddDate;
  }
  
  public String getGnaddDate()
  {
    return gnaddDate;
  }
  
  public void setGnaddHoldFlag(String gnaddHoldFlag)
  {

    this.gnaddHoldFlag = trim(gnaddHoldFlag);
  }
  
  public String getGnaddHoldFlag()
  {
    return gnaddHoldFlag;
  }
  
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
  
}
