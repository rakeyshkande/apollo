package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;



public class CustomerVO
{
  private long 		  customerId;
  private String 		concatId;
  private String 		firstName;
  private String 		lastName;
  private String 		businessName;
  private String 		address1;
  private String 		address2;
  private String 		city;
  private String 		state;
  private String 		zipCode;
  private String 		country;
  private String 		county;
  private String 		addressType;
  private char 		  preferredCustomer;
  private char 		  buyerIndicator;
  private char 		  recipientIndicator;
  private String 		origin;
  private Date 	firstOrderDate;
  private Date 	birthday;
  

  public CustomerVO()
  {
  }

  public void setCustomerId(long customerId)
  {

    this.customerId = customerId;
  }

  public long getCustomerId()
  {
    return customerId;
  }

  public void setConcatId(String concatId)
  {

    this.concatId = trim(concatId);
  }

  public String getConcatId()
  {
    return concatId;
  }

  public void setFirstName(String firstName)
  {

    this.firstName = trim(firstName);
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setLastName(String lastName)
  {

    this.lastName = trim(lastName);
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setBusinessName(String businessName)
  {

    this.businessName = trim(businessName);
  }

  public String getBusinessName()
  {
    return businessName;
  }

  public void setAddress1(String address1)
  {

    this.address1 = trim(address1);
  }

  public String getAddress1()
  {
    return address1;
  }

  public void setAddress2(String address2)
  {

    this.address2 = trim(address2);
  }

  public String getAddress2()
  {
    return address2;
  }

  public void setCity(String city)
  {

    this.city = trim(city);
  }

  public String getCity()
  {
    return city;
  }

  public void setState(String state)
  {

    this.state = trim(state);
  }

  public String getState()
  {
    return state;
  }

  public void setZipCode(String zipCode)
  {

    this.zipCode = trim(zipCode);
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setCountry(String country)
  {

    this.country = trim(country);
  }

  public String getCountry()
  {
    return country;
  }
  
  public void setCounty(String county)
  {

    this.county = trim(county);
  }

  public String getCounty()
  {
    return county;
  }

  public void setAddressType(String addressType)
  {

    this.addressType = trim(addressType);
  }

  public String getAddressType()
  {
    return addressType;
  }

  public void setPreferredCustomer(char preferredCustomer)
  {

    this.preferredCustomer = preferredCustomer;
  }

  public char getPreferredCustomer()
  {
    return preferredCustomer;
  }

  public void setBuyerIndicator(char buyerIndicator)
  {

    this.buyerIndicator = buyerIndicator;
  }

  public char getBuyerIndicator()
  {
    return buyerIndicator;
  }

  public void setRecipientIndicator(char recipientIndicator)
  {

    this.recipientIndicator = recipientIndicator;
  }

  public char getRecipientIndicator()
  {
    return recipientIndicator;
  }

  public void setOrigin(String origin)
  {

    this.origin = trim(origin);
  }

  public String getOrigin()
  {
    return origin;
  }

  public void setFirstOrderDate(Date firstOrderDate)
  {

    this.firstOrderDate = firstOrderDate;
  }

  public Date getFirstOrderDate()
  {
    return firstOrderDate;
  }
  
  public void setBirthday(Date birthday)
  {

    this.birthday = birthday;
  }

  public Date getBirthday()
  {
    return birthday;
  }
  


  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }

}
