package com.ftd.oputil.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class OrderBillVO
{
  private	long	    orderBillId;
  private	long	    orderDetailId;
  private	double	  productAmount;
  private	double	  addOnAmount;
  private	double	  serviceFee;
  private	double	  shippingFee;
  private	double	  discountAmount;
  private	double	  shippingTax;
  private	double	  tax;
  private	char	    additionalBillIndicator;
  
  public OrderBillVO()
  {
  }

  public void setOrderBillId(long orderBillId)
  {
    this.orderBillId = orderBillId;
  }


  public long getOrderBillId()
  {
    return orderBillId;
  }


  public void setOrderDetailId(long orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setProductAmount(double productAmount)
  {
    this.productAmount = productAmount;
  }


  public double getProductAmount()
  {
    return productAmount;
  }


  public void setAddOnAmount(double addOnAmount)
  {
    this.addOnAmount = addOnAmount;
  }


  public double getAddOnAmount()
  {
    return addOnAmount;
  }


  public void setServiceFee(double serviceFee)
  {
    this.serviceFee = serviceFee;
  }


  public double getServiceFee()
  {
    return serviceFee;
  }


  public void setShippingFee(double shippingFee)
  {
    this.shippingFee = shippingFee;
  }


  public double getShippingFee()
  {
    return shippingFee;
  }


  public void setDiscountAmount(double discountAmount)
  {
    this.discountAmount = discountAmount;
  }


  public double getDiscountAmount()
  {
    return discountAmount;
  }


  public void setShippingTax(double shippingTax)
  {
    this.shippingTax = shippingTax;
  }


  public double getShippingTax()
  {
    return shippingTax;
  }


  public void setTax(double tax)
  {
   this.tax = tax;
  }


  public double getTax()
  {
    return tax;
  }


  public void setAdditionalBillIndicator(char additionalBillIndicator)
  {
    this.additionalBillIndicator = additionalBillIndicator;
  }


  public char getAdditionalBillIndicator()
  {
    return additionalBillIndicator;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
}
