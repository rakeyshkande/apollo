package com.ftd.oputil.vo;

import com.ftd.oputil.vo.RWDFloristVO;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.LinkedList;
import java.util.Date;
import java.util.List;

public class RWDFloristVOList
{
  private int weightSquaredTotal;
  private LinkedList floristList;
    
  public RWDFloristVOList()
  {
    weightSquaredTotal = 0;
    floristList = new LinkedList();
  }
  
  public void setWeightSquaredTotal(int weightSquaredTotal)
  {

    this.weightSquaredTotal = weightSquaredTotal;
  }

  public int getWeightSquaredTotal()
  {
    return weightSquaredTotal;
  }
  

  public LinkedList getFloristList()
  {
    return floristList;
  }
  
  public void add(RWDFloristVO florist) 
  {
    weightSquaredTotal += florist.getWeightSquared();
    floristList.add(florist);
  }
 
  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }
}
