package com.ftd.oputil;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.PrintWriter;
import java.io.IOException;

public class OrderUtilitiesServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    response.setHeader("pragma", "no-cache");
    PrintWriter out = response.getWriter();
    out.print("<HTML><HEAD><TITLE>RWD Analyzer</TITLE></HEAD>");
    out.print("<BODY><h1>RWD Analyzer</h1>");
    out.print("<FORM METHOD=POST>");
    out.print("Order Detail ID or External Order Number: <INPUT TYPE=TEXT NAME=orderDetail>");
    out.print("   <INPUT TYPE=SUBMIT NAME=action VALUE=Analyze>");
    out.print("</FORM>");
//    out.print("<FORM METHOD=POST>");
//    out.print("<hr>");
//    out.print("<h1>Inject Orders</h1>");
//    out.print("<INPUT TYPE=SUBMIT NAME=action VALUE=InjectOrders>");
//    out.print("</FORM>");
    out.print("</BODY></HTML>");
    out.close();
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    String orderDetailId = request.getParameter("orderDetail");
    String msg = null;
    if(request.getParameter("action").equals("Analyze") && orderDetailId == null)
    {
      response.sendError(response.SC_BAD_REQUEST, "No order detail record specified");
      return;
    }
    if(request.getParameter("action").equals("Analyze"))
    {
      out.println("<html>");
      out.println("<head><title>Random Weighted Distribution -- Florist Analysis</title></head>");
      out.println("<body>");
      RWDAnalyzer analyzer = new RWDAnalyzer(out, true);
      String value = analyzer.getOrderDetail(orderDetailId);
      if (value == null) value = orderDetailId;
      analyzer.analyze(value);
      out.println("</body></html>");
      out.close();
    } else if(request.getParameter("action").equals("InjectOrders")) 
    {
      out.println("<html>");
      out.println("<head><title>Order Injector</title></head>");
      out.println("<body>");
      OrderInjector oi = new OrderInjector(out);
      oi.run();
      out.println("</body></html>");
      out.close();
    }
  }
}