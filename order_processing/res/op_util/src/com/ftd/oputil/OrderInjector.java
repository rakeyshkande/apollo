package com.ftd.oputil;

import com.ftd.osp.utilities.xml.DOMUtil;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class OrderInjector 
{
  PrintWriter out;
  Connection conn;
  int runNumber;
  private static final String orderGathererURL = "http://apollo.ftdi.com:7778/OG/servlet/OrderGatherer";
  public OrderInjector(PrintWriter output)
  {
  
    String driver_ = "oracle.jdbc.driver.OracleDriver";
    String database_ = "jdbc:oracle:thin:@stheno-dev.ftdi.com:1521:dev5";
    String user_ = "jweiss";
    String password_ = "jweiss";
    try {
      Class.forName(driver_);
      conn = DriverManager.getConnection(database_, user_, password_);
      this.getRunNumber();
      out = output;
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
    finally
    {
      //close the connection
      try
      {
        conn.close();
      }
      catch(Exception e)
      {}
    }
  }
  public void parseClob(String clob) 
  {
    try {
      NodeList orders;
      DocumentBuilder builder = DOMUtil.getDocumentBuilder(false, true, true, true, false, false);
      // set the entity resolver
      
      
      Document doc = DOMUtil.getDocument(clob);
      
      NodeList list = doc.getElementsByTagName("order");
      for(int i = 0; i < list.getLength(); i++) 
      {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
          document = builder.newDocument(); 
        } catch (Exception e) 
        {
          e.printStackTrace();
        }
        Node newNode = document.importNode(list.item(i), true);
        document.appendChild(newNode);

        
        sendDocument(document);
      }
      
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
  }
  private String generateXMLString(Document doc) 
  {
    String output = null;
    try {
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformer = tFactory.newTransformer();
              
      StringWriter sw = new StringWriter();
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(sw);
      transformer.transform(source, result);
      output = sw.getBuffer().toString();
    } catch (Exception e) 
    {
      e.printStackTrace();
    }
    return output;
  }
  void sendDocument(Document document) throws java.net.MalformedURLException, java.io.IOException
  {
    // set url
    URL url = new URL(orderGathererURL);
    URLConnection urlConn;
    DataOutputStream printout;
    
    // set up connection
    urlConn = url.openConnection();
    urlConn.setDoInput(true);
    urlConn.setDoOutput(true);
    urlConn.setUseCaches(false);
    urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    
    fixTestData(document);
    out.println(generateXMLString(document));
    String content = "Order=" + generateXMLString(document);
    OutputStream output = urlConn.getOutputStream();
    output.write(content.getBytes());
    output.flush();
    output.close();
    BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
    String line;
    while (null != (line = in.readLine()))
    {
      out.println(line);
    }
    urlConn = null;
  }
  public void fixTestData(Document document) 
  {
    String CC_NUM = "4222222222222";
    SimpleDateFormat ccDateFormat = new SimpleDateFormat("MM/yy");
    SimpleDateFormat delivDateFormat = new SimpleDateFormat("MM/dd/yy");
    Calendar cal = new GregorianCalendar();
    cal.add(Calendar.DATE, 1);
    
    Date tomorrow = new Date(cal.getTimeInMillis());

    // fix credit card number
    NodeList list = document.getElementsByTagName("cc-number");
    for(int i=0; i < list.getLength(); i++)
    {
      list.item(i).getFirstChild().setNodeValue(CC_NUM);
    }
    
    // fix credit card month
    list = document.getElementsByTagName("cc-exp-date");
    for(int i=0; i < list.getLength(); i++)
    {
      list.item(i).getFirstChild().setNodeValue(ccDateFormat.format(tomorrow));
    }
    
    // fix delivery date
    list = document.getElementsByTagName("delivery-date");
    for(int i=0; i < list.getLength(); i++)
    {
      list.item(i).getFirstChild().setNodeValue(delivDateFormat.format(tomorrow));
    }
    
    list = document.getElementsByTagName("order-number");
    for(int i = 0; i < list.getLength(); i++)
    {
      String output = list.item(i).getFirstChild().getNodeValue();
      list.item(i).getFirstChild().setNodeValue(output + "-" + runNumber);
      out.println("<b>EXTERNAL_ORDER_NUMBER: " + list.item(i).getFirstChild().getNodeValue() + "</b><br>");
    }
    list = document.getElementsByTagName("master-order-number");
    for(int i=0; i < list.getLength(); i++) 
    {
      String output = list.item(i).getFirstChild().getNodeValue();
      list.item(i).getFirstChild().setNodeValue(output + "-" + runNumber);
      out.println("MASTER_ORDER_NUMBER: " + list.item(i).getFirstChild().getNodeValue() + "</b><br>");
    }
  }
  public void getRunNumber() 
  {
    String queryString = 
      "select jweiss.order_injector_sequence.NEXTVAL" + 
      " from dual ";

    Statement s = null; 

    try {
      s = conn.createStatement();
      
      ResultSet r = s.executeQuery(queryString);
      while (r.next())
      {
        runNumber = r.getInt(1);
      }
    } catch (Exception e) 
    {
      e.printStackTrace();
    } finally 
    {
      try
      {
        s.close();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }
  public void run() 
  {
    String queryString = 
      "select order_xml" + 
      " from jweiss.order_injector";

    Statement s = null; 
    String clob  = null;

    try {
      s = conn.createStatement();
      
      ResultSet r = s.executeQuery(queryString);
      while (r.next())
      {
        clob = r.getString(1);
        parseClob(clob);
      }
    } catch (Exception e) 
    {
      e.printStackTrace();
    } finally 
    {
      try
      {
        s.close();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }
  /**
   * 
   * @param args
   */
  public static void main(String[] args)
  {
    OrderInjector orderInjector = new OrderInjector(new PrintWriter(System.out));
    orderInjector.run();
  }
}