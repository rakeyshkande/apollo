package com.ftd.oputil.dao;

import com.ftd.oputil.constants.OrderUtilConstants;


import com.ftd.oputil.vo.OrderBillVO;
import com.ftd.oputil.vo.CustomerVO;
import com.ftd.oputil.vo.FloristVO;
import com.ftd.oputil.vo.GlobalParameterVO;
import com.ftd.oputil.vo.GnaddVO;
import com.ftd.oputil.vo.OrderDetailVO;
import com.ftd.oputil.vo.OrderVO;
import com.ftd.oputil.vo.ProductVO;
import com.ftd.oputil.vo.RWDFloristVO;
import com.ftd.oputil.vo.RWDFloristVOList;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * OrderDAO
 * 
 * This is the DAO that handles inserting florist information, updating 
 * order information and retrieving information needed for order processing.
 * 
 * @author Nicole Roberts
 */

public class OrderDAO 
{
  private OrderUtilConstants orderConstants;  
  private Connection conn;  
  
  /**
     * Constructor
     * 
     * @param Connection - database connection
     * @return n/a
     * @throws none
     */
  public OrderDAO(Connection conn)
  {
    this.conn = conn;
  }
  
  /**
   * Constructor
   * 
   * @param none
   * @return n/a
   * @throws ClassNotFoundException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @throws XSLException
   * @throws Exception
   */
/*  public OrderDAO() throws IOException, ParserConfigurationException, SAXException, TransformerException, 
               XSLException, Exception
  {
    try
    {
      this.conn = DataSourceUtil.getInstance().getConnection(
             ConfigurationUtil.getInstance().getProperty(orderConstants.PROPERTY_FILE,
                                   orderConstants.DATASOURCE_NAME));
    }
    catch (Exception e) 
    {            
      throw new Exception(e.toString());
    }
  }
  */  
  /**
   * This method is a wrapper for the SP_UPDATE_ORDER_DETAIL SP.
   * It updates an order detail record to match the passed in VO.
   * 
   * @param OrderDetailVO
   * @return none
   */
  public void updateOrder(OrderDetailVO orderDtl)
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()));
      inputParams.put(orderConstants.DELIVERY_DATE, orderDtl.getDeliveryDate()==null?null:new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
      inputParams.put(orderConstants.RECIPIENT_ID, new Long(orderDtl.getRecipientId()));
      inputParams.put(orderConstants.PRODUCT_ID, orderDtl.getProductId());
      inputParams.put(orderConstants.QUANTITY, new Long(orderDtl.getQuantity()));
      inputParams.put(orderConstants.COLOR_1, orderDtl.getColor1());
      inputParams.put(orderConstants.COLOR_2, orderDtl.getColor2());
      inputParams.put(orderConstants.SUBSTITUTION_INDICATOR, orderDtl.getSubstitutionIndicator());
      inputParams.put(orderConstants.SAME_DAY_GIFT, orderDtl.getSameDayGift());
      inputParams.put(orderConstants.OCCASION, orderDtl.getOccasion());
      inputParams.put(orderConstants.CARD_MESSAGE, orderDtl.getCardMessage());
      inputParams.put(orderConstants.CARD_SIGNATURE, orderDtl.getCardSignature());
      inputParams.put(orderConstants.SPECIAL_INSTRUCTIONS, orderDtl.getSpecialInstructions());
      inputParams.put(orderConstants.RELEASE_INFO_INDICATOR,orderDtl.getReleaseInfoIndicator());
      inputParams.put(orderConstants.FLORIST_ID, orderDtl.getFloristId());
      inputParams.put(orderConstants.SHIP_METHOD, orderDtl.getShipMethod());
      inputParams.put(orderConstants.SHIP_DATE, orderDtl.getShipDate());
      inputParams.put(orderConstants.ORDER_DISP_CODE, orderDtl.getOrderDispCode());
      inputParams.put(orderConstants.SECOND_CHOICE_PRODUCT, orderDtl.getSecondChoiceProduct());
      inputParams.put(orderConstants.ZIP_QUEUE_COUNT, new Long(orderDtl.getZipQueueCount()));
      inputParams.put(orderConstants.RANDOM_WEIGHT_DIST_FAILURES, new Long(orderDtl.getRandomWeightDistFailures()));
      inputParams.put(orderConstants.UPDATED_BY, orderConstants.UPDATED_BY_VL);
      inputParams.put(orderConstants.DELIVERY_DATE_RANGE_END, orderDtl.getDeliveryDateRangeEnd()==null?null:new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
      inputParams.put(orderConstants.SCRUBBED_ON, orderDtl.getScrubbedOn());
      inputParams.put(orderConstants.SCRUBBED_BY, orderDtl.getScrubbedBy());
      inputParams.put(orderConstants.ARIBA_UNSPSC_CODE, orderDtl.getAribaUnspscCode());
      inputParams.put(orderConstants.ARIBA_PO_NUMBER, orderDtl.getAribaPoNumber());
      inputParams.put(orderConstants.ARIBA_AMS_PROJECT_CODE, orderDtl.getAribaAmsProjectCode());
      inputParams.put(orderConstants.ARIBA_COST_CENTER, orderDtl.getAribaCostCenter());
      inputParams.put(orderConstants.SIZE_INDICATOR, orderDtl.getSizeIndicator());
      inputParams.put(orderConstants.MILES_POINTS, new Long(orderDtl.getMilesPoints()));
      inputParams.put(orderConstants.SUBCODE, orderDtl.getSubcode());
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_UPDATE_ORDER_DETAILS);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputs.get(orderConstants.OUT_STATUS_PARAM);
      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        String message = (String) outputs.get(orderConstants.OUT_MESSAGE_PARAM);
        throw new Exception(message);
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    }     
  }

public String getCustomerHold(long customerId, String LPI) 
{
    DataRequest dataRequest = new DataRequest();
    String outputs = null;
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.CUSTOMER_ID, customerId + "");
      inputParams.put(orderConstants.LOSS_PREVENTION_INDICATOR, LPI);
            
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_IS_CUSTOMER_ON_ORDER_HOLD);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
      outputs = (String) dataAccessUtil.execute(dataRequest);
      
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    
    return outputs;
  
}
  /**
   * This method is a wrapper for the SP_IS_CUSTOMER_ON_HOLD SP, 
   * returning true if the SP returns �Y� and false otherwise.
   * 
   * @param long - customerId
   * @return boolean
   */
  public boolean isCustomerOnHold(long customerId)
  {    
    DataRequest dataRequest = new DataRequest();
    boolean result = false;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.CUSTOMER_ID, new Long(customerId));
            
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_IS_CUSTOMER_ON_ORDER_HOLD);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
      String outputs = (String) dataAccessUtil.execute(dataRequest);
      
      /* process results to determine if customer is on hold */
      result = outputs.equalsIgnoreCase(orderConstants.VL_YES) ? true : false;
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    
    return result;
  }
  
  /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_VENDOR_SHIPPING_RESTRICT
   * stored procedure.
   * 
   * @param String shippingKey
   * @return  sameDayDeliveryFee
   */


  /**
   * This method is a wrapper for the GLOBAL.GLOBAL_PKG.GET_SHIPPING_KEY_COSTS
   * stored procedure.
   * 
   * @param String shippingKey
   * @return Double sameDayDeliveryFee
   */

   public Double getShippingKeyCosts(String shippingKeyDetailId, String shippingMethodId)
   {
    Double shippingKeyCosts = new Double(0);
    DataRequest dataRequest = new DataRequest();
    boolean result = false;
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("SHIPPING_KEY_DETAIL_ID", shippingKeyDetailId);
      inputParams.put("SHIPPING_METHOD_ID", shippingMethodId);
            
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("GET_SHIPPING_KEY_COSTS");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
      ResultSet outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      while (outputs.next())
      {
        shippingKeyCosts = new Double(outputs.getString("SHIPPING_COST"));        
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
     return shippingKeyCosts;    
   }
  
  
  
  /**
   * This method is a wrapper for the FTD_APPS.FLORIST_QUERY_PKG.VIEW_FLORIST_MASTER
   * stored procedure.
   * 
   * @param String floristId
   * @return FloristVO
   */
  public FloristVO getFlorist(String floristId)
  {   
    DataRequest dataRequest = new DataRequest();
    FloristVO florist = new FloristVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("FLORIST_ID", floristId);
            
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("VIEW_FLORIST_MASTER");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      while (outputs.next())
      {
          isEmpty = false;
          florist.setAddress((String) outputs.getString("ADDRESS"));
          florist.setAdjustedWeight(outputs.getInt("ADJUSTED_WEIGHT"));
          florist.setAdjustedWeightEndDate(outputs.getDate("ADJUSTED_WEIGHT_END_DATE"));
          florist.setAdjustedWeightPermenantFlag(outputs.getString("ADJUSTED_WEIGHT_PERMENANT_FLAG"));
          florist.setAdjustedWeightStartDate(outputs.getDate("ADJUSTED_WEIGHT_START_DATE"));
          florist.setAltPhoneNumber(outputs.getString("ALT_PHONE_NUMBER"));
          florist.setCity(outputs.getString("CITY"));
          florist.setCityStateNumber(outputs.getString("CITY_STATE_NUMBER"));
          florist.setDeliveryCity(outputs.getString("DELIVERY_CITY"));
          florist.setDeliveryState(outputs.getString("DELIVERY_STATE"));
          florist.setEmailAddress(outputs.getString("EMAIL_ADDRESS"));
          florist.setFaxNumber(outputs.getString("FAX_NUMBER"));
          florist.setFloristId(outputs.getString("FLORIST_ID"));
          florist.setFloristName(outputs.getString("FLORIST_NAME"));
          florist.setFloristWeight(outputs.getInt("FLORIST_WEIGHT"));
          florist.setInitialWeight(outputs.getInt("INITIAL_WEIGHT"));
          florist.setMercuryFlag(outputs.getString("MERCURY_FLAG"));
          florist.setMinimumOrderAmount(outputs.getDouble("MINIMUM_ORDER_AMOUNT"));
          florist.setOrdersSent(outputs.getInt("ORDERS_SENT"));
          florist.setOwnersName(outputs.getString("OWNERS_NAME"));
          florist.setPhoneNumber(outputs.getString("PHONE_NUMBER"));
          florist.setRecordType(outputs.getString("RECORD_TYPE"));
          florist.setSendingRank(outputs.getInt("SENDING_RANK"));
          florist.setState(outputs.getString("STATE"));
          florist.setStatus(outputs.getString("STATUS"));
          florist.setSundayDeliveryBlockEndDt(outputs.getDate("SUNDAY_DELIVERY_BLOCK_END_DT"));
          florist.setSundayDeliveryBlockStDt(outputs.getDate("SUNDAY_DELIVERY_BLOCK_ST_DT"));
          florist.setSundayDeliveryFlag(outputs.getString("SUNDAY_DELIVERY_FLAG"));
          florist.setSuperFloristFlag(outputs.getString("SUPER_FLORIST_FLAG"));
          florist.setSuspendOverrideFlag(outputs.getString("SUSPEND_OVERRIDE_FLAG"));
          florist.setTerritory(outputs.getString("TERRITORY"));
          florist.setVendorFlag(outputs.getString("VENDOR_FLAG"));
          florist.setZipCode(outputs.getString("ZIP_CODE"));
          florist.setIsFTOFlag(outputs.getString("IS_FTO_FLAG"));
          florist.setAllowMessageForwardingFlag(outputs.getString("ALLOW_MESSAGE_FORWARDING_FLAG"));
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    }    
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    }
     if (isEmpty)
      return null;
    else
      return florist;
  }
  
  
  /**
   * This method is a wrapper for the SP_GET_DAILY_FLORIST SP,
   * returning a floristId to use or null otherwise.
   * 
   * @param OrderDetailVO 
   * @return LinkedList of Strings 
   */
  public LinkedList getDailyFlorist(OrderDetailVO orderDtl) throws Exception
  {    
    DataRequest dataRequest = new DataRequest();
    boolean isEmpty = true;
    LinkedList florists = new LinkedList();
    
    /* setup store procedure input parameters */
    HashMap inputParams = new HashMap();
    inputParams.put(orderConstants.CUSTOMER_ID, new Long(orderDtl.getRecipientId()));
    inputParams.put(orderConstants.DELIVERY_DATE, orderDtl.getDeliveryDate()==null?null:new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
          
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(orderConstants.SP_GET_DAILY_FLORIST);
    dataRequest.setInputParams(inputParams);
    
    /* execute the store prodcedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
    CachedResultSet outputs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    while (outputs.next())
    {
      isEmpty = false;
      florists.add((String)outputs.getObject(1));  
    }
    if (isEmpty)
      return null;
    else
      return florists;
  }
    

  /**
   * This method is a wrapper for the SP_GET_GNADD SP.  
   * It populates a GNADD VO based on the returned record set.
   * 
   * @param none
   * @return GnaddVO 
   */
  public GnaddVO getGnadd()
  {
    DataRequest dataRequest = new DataRequest();
    GnaddVO gnadd = new GnaddVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_GNADD_STATUS);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        gnadd.setGnaddStatus(outputs.getString(2));     
        gnadd.setGnaddDate(outputs.getString(4));
        gnadd.setGnaddHoldFlag(outputs.getString(6));
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    }  
    if (isEmpty)
     return null;
    else
      return gnadd;      
  }
  

  /**
   * This method is a wrapper for the SP_GET_PRODUCT SP.  
   * It populates a product VO based on the returned record set.
   * 
   * @param String - productId
   * @return ProductVO 
   */
   public ProductVO getProduct(String productId)
  {    
    DataRequest dataRequest = new DataRequest();
    ProductVO product = new ProductVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.PRODUCT_ID, productId);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_PRODUCT_BY_ID);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        product.setProductId(outputs.getString(orderConstants.PRODUCT_ID_LC));     
        product.setNovatorId(outputs.getString(orderConstants.NOVATOR_ID_LC));     
        product.setProductName(outputs.getString(orderConstants.PRODUCT_NAME_LC));
        product.setNovatorName(outputs.getString(orderConstants.NOVATOR_NAME_LC));
        product.setStatus(outputs.getString(orderConstants.STATUS_LC));     
        product.setDeliveryType(outputs.getString(orderConstants.DELIVERY_TYPE_LC));
        product.setCategory(outputs.getString(orderConstants.CATEGORY_LC));
        product.setProductType(outputs.getString(orderConstants.PRODUCT_TYPE_LC));     
        product.setProductSubType(outputs.getString(orderConstants.PRODUCT_SUB_TYPE_LC));
        product.setColorSizeFlag(outputs.getString(orderConstants.COLOR_SIZE_FLAG_LC));
        product.setStandardPrice(outputs.getDouble(orderConstants.STANDARD_PRICE_LC));
        product.setDeluxePrice(outputs.getDouble(orderConstants.DELUXE_PRICE_LC));
        product.setPremiumPrice(outputs.getDouble(orderConstants.PREMUIM_PRICE_LC));
        product.setPreferredPricePoint(outputs.getDouble(orderConstants.PREFERRED_PRICE_POINT_LC));
        product.setVariablePriceMax(outputs.getDouble(orderConstants.VARIABLE_PRICE_MAX_LC));
        product.setShortDescription(outputs.getString(orderConstants.SHORT_DESCRIPTION_LC));
        product.setLongDescription(outputs.getString(orderConstants.LONG_DESCRIPTION_LC));     
        product.setFloristReferenceNumber(outputs.getString(orderConstants.FLORIST_REFERENCE_NUMBER_LC));
        product.setMercuryDescription(outputs.getString(orderConstants.MERCURY_DESCRIPTION_LC));
        product.setItemComments(outputs.getString(orderConstants.ITEM_COMMENTS_LC));     
        product.setAddOnBalloonsFlag(outputs.getString(orderConstants.ADD_ON_BALLOONS_FLAG_LC));
        product.setAddOnBearsFlag(outputs.getString(orderConstants.ADD_ON_BEARS_FLAG_LC));
        product.setAddOnCardsFlag(outputs.getString(orderConstants.ADD_ON_CARDS_FLAG_LC));     
        product.setAddOnFuneralFlag(outputs.getString(orderConstants.ADD_ON_FUNERAL_FLAG_LC));
        product.setCodifiedFlag(outputs.getString(orderConstants.CODIFIED_FLAG_LC));
        product.setExceptionCode(outputs.getString(orderConstants.EXECPTION_CODE_LC));     
        product.setExceptionStartDate(outputs.getDate(orderConstants.EXCEPTION_START_DATE_LC));
        product.setExceptionEndDate(outputs.getDate(orderConstants.EXECPTION_END_DATE_LC));
        product.setExceptionMessage(outputs.getString(orderConstants.EXCEPTION_MESSAGE_LC));     
        product.setVendorId(outputs.getString(orderConstants.VENDOR_ID_LC));
        product.setVendorCost(outputs.getDouble(orderConstants.VENDOR_COST_LC));
        product.setVendorSku(outputs.getString(orderConstants.VENDOR_SKU_LC));     
        product.setSecondChoiceCode(outputs.getString(orderConstants.SECOND_CHOICE_CODE_LC));
        product.setHolidaySecondChoiceCode(outputs.getString(orderConstants.HOLIDAY_SECOND_CHOICE_CODE_LC));
        product.setDropshipCode(outputs.getString(orderConstants.DROPSHIP_CODE_LC));
        product.setDiscountAllowedFlag(outputs.getString(orderConstants.DISCOUNT_ALLOWED_FLAG_LC));     
        product.setDeliveryIncludedFlag(outputs.getString(orderConstants.DELIVERY_INCLUDED_FLAG_LC));
        product.setTaxFlag(outputs.getString(orderConstants.TAX_FLAG_LC));
        product.setServiceFeeFlag(outputs.getString(orderConstants.SERVICE_FEE_FLAG_LC));
        product.setExoticFlag(outputs.getString(orderConstants.EXOTIC_FLAG_LC));     
        product.setEgiftFlag(outputs.getString(orderConstants.EGIFT_FLAG_LC));
        product.setCountryId(outputs.getString(orderConstants.COUNTRY_ID_LC));
        product.setArrangementSize(outputs.getString(orderConstants.ARRANGEMENT_SIZE_LC));
        product.setArrangementColors(outputs.getString(orderConstants.ARRANGEMENT_COLORS_LC));     
        product.setDominantFlowers(outputs.getString(orderConstants.DOMINANT_FLOWERS_LC));
        product.setSearchPriority(outputs.getString(orderConstants.SEARCH_PRIORITY_LC));
        product.setRecipe(outputs.getString(orderConstants.RECIPE_LC));     
        product.setSubcodeFlag(outputs.getString(orderConstants.SUBCODE_FLAG_LC));
        product.setDimWeight(outputs.getString(orderConstants.DIM_WEIGHT_LC));
        product.setNextDayUpgradeFlag(outputs.getString(orderConstants.NEXT_DAT_UPGRADE_FLAG_LC));     
        product.setCorporateSite(outputs.getString(orderConstants.CORPORATE_SITE_LC));
        product.setUnspscCode(outputs.getString(orderConstants.UNSPSC_CODE_LC));
        product.setPriceRank1(outputs.getString(orderConstants.PRICE_RANK1_LC));     
        product.setPriceRank2(outputs.getString(orderConstants.PRICE_RANK2_LC));
        product.setPriceRank3(outputs.getString(orderConstants.PRICE_RANK3_LC));
        product.setShipMethodCarrier(outputs.getString(orderConstants.SHIP_METHOD_CARRIER_LC));     
        product.setShipMethodFlorist(outputs.getString(orderConstants.SHIP_METHOD_FLORIST_LC));
        product.setShippingKey(outputs.getString(orderConstants.SHIPPING_KEY_LC));
        product.setVariablePriceFlag(outputs.getString(orderConstants.VARIABLE_PRICE_FLAG_LC));
        product.setHolidaySku(outputs.getString(orderConstants.HOLIDAY_SKU_LC));
        product.setHolidayPrice(outputs.getDouble(orderConstants.HOLIDAY_PRICE_LC));
        product.setCatalogFlag(outputs.getString(orderConstants.CATALOG_FLAG_LC));
        product.setHolidayDeluxePrice(outputs.getDouble(orderConstants.HOLIDAY_DELUXE_PRICE_LC));
        product.setHolidayPremiumPrice(outputs.getDouble(orderConstants.HOLIDAY_PREMIUM_PRICE_LC));
        product.setHolidayStartDate(outputs.getDate(orderConstants.HOLIDAY_START_DATE_LC));
        product.setHolidayEndDate(outputs.getDate(orderConstants.HOLIDAY_END_DATE_LC));
        product.setHoldUntilAvailable(outputs.getString(orderConstants.HOLD_UNTIL_AVAILABLE_LC));
        product.setMondayDeliveryFreshcut(outputs.getString(orderConstants.MONDAY_DELIVERY_FRESHCUT_LC));
        product.setTwoDaySatFreshcut(outputs.getString(orderConstants.TWO_DAY_SAT_FRESHCUT_LC));
        product.setDefaultCarrier(outputs.getString(orderConstants.DEFAULT_CARRIER_LC));
      }
    }
    catch (Exception e) 
    {            
        e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    } 
    if (isEmpty)
      return null;
    else
      return product;
  }
  
  /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.  
   * It populates a global parameter VO based on the returned record set.
   * 
   * @param String - context
   * @param String - name
   * @return GlobalParameterVO 
   */
  public GlobalParameterVO getGlobalParameter(String context, String name) 
  {    
    DataRequest dataRequest = new DataRequest();
    GlobalParameterVO param = new GlobalParameterVO();
    boolean isEmpty = true;
    String outputs = null;
    
    try
    {
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.CONTEXT, context);
      inputParams.put(orderConstants.NAME, name);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_GLOBAL_PARM_VALUE);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (String) dataAccessUtil.execute(dataRequest);
      if (outputs != null)
        isEmpty = false;
      /* populate object */
      param.setName(name);     
      param.setValue(outputs);      
    }
    catch (Exception e) 
    {            
        e.printStackTrace();
    } 
    if (isEmpty)
      return null;
    else
      return param;
  }
  
  /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.  
   * It populates a global parameter VO based on the returned record set.
   * 
   * @param String - name
   * @return GlobalParameterVO 
   */
  public GlobalParameterVO getGlobalParameter(String name) 
  {    
    DataRequest dataRequest = new DataRequest();
    GlobalParameterVO param = new GlobalParameterVO();
    ResultSet outputs = null;
    boolean isEmpty = true;
    
    try
    {

      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.NAME, name);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_GLOBAL_PARMETER);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        param.setName(outputs.getString(orderConstants.NAME_LC));     
        param.setValue(outputs.getString(orderConstants.VALUE_LC));
      }
    }
    catch (Exception e) 
    {            
        e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    }  
    if (isEmpty)
      return null;
    else
      return param;
  }
  
  /**
   * This method is a wrapper for the SP_GET_CUSTOMER SP.   
   * It populates a customer VO based on the returned record set.
   * 
   * @param String - customerId
   * @return CustomerVO
   */
  public CustomerVO getCustomer(long customerId) 
  {
    DataRequest dataRequest = new DataRequest();
    CustomerVO customer = new CustomerVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.CUSTOMER_ID, new Long(customerId));
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_CUSTOMER);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      customer.setCustomerId(new Long(customerId).longValue());     
      while (outputs.next())
      {
        isEmpty = false;
        customer.setConcatId(outputs.getString(orderConstants.CONCAT_ID));
        customer.setFirstName(outputs.getString(orderConstants.CUSTOMER_FIRST_NAME));
        customer.setLastName(outputs.getString(orderConstants.CUSTOMER_LAST_NAME));
        customer.setBusinessName(outputs.getString(orderConstants.BUSINESS_NAME));
        customer.setAddress1(outputs.getString(orderConstants.CUSTOMER_ADDRESS_1));
        customer.setAddress2(outputs.getString(orderConstants.CUSTOMER_ADDRESS_2));
        customer.setCity(outputs.getString(orderConstants.CUSTOMER_CITY));
        customer.setState(outputs.getString(orderConstants.CUSTOMER_STATE));
        customer.setZipCode(outputs.getString(orderConstants.CUSTOMER_ZIP_CODE).substring(0,5));
        customer.setCountry(outputs.getString(orderConstants.CUSTOMER_COUNTRY));       
      }
      
      /* setup store procedure input parameters */
      HashMap inputParams2 = new HashMap();
      inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());
      
      /* build DataRequest object */
      DataRequest dataRequest2 = new DataRequest();
      dataRequest2.setConnection(conn);
      dataRequest2.setStatementID("GET_CUSTOMER_PHONES");
      dataRequest2.setInputParams(inputParams2);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil2 = DataAccessUtil.getInstance();    
      ResultSet outputs2 = (ResultSet) dataAccessUtil.execute(dataRequest2);

      
    }
    catch (Exception e) 
    {            
        e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    }  
    if (isEmpty)
      return null;
    else
      return customer;
  }
  
  /**
   * This method is a wrapper for the SP_CAN_FLORIST_SUPPORT_ORDER SP.  
   * It returns true if the SP returns a �Y� and false otherwise.
   * 
   * @param OrderDetailVO 
   * @param FloristVO 
   * @return boolean
   */
  public boolean canFloristSupportOrder(OrderDetailVO orderDtl, FloristVO florist) 
  {
    DataRequest dataRequest = new DataRequest();
    boolean result = true;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()));
      inputParams.put(orderConstants.FLORIST_ID, florist.getFloristId());

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_CAN_FLORIST_SUPPORT_ORDER);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    
      String outputs = (String) dataAccessUtil.execute(dataRequest);

      /* process results to determine if florist can support order */
      result = outputs.equalsIgnoreCase(orderConstants.VL_YES) ? true : false;
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    
    return result;
  }
  
    /**
   * This method is a wrapper for the FTD_APPS.MISC_PKG.RETRIEVE_ACTIVE_COUNTRY_NAME.  
   * 
   * @param String countryId
   * @return String countryName 
   */
  public String getActiveCountryName(String countryId) 
  {
    DataRequest dataRequest = new DataRequest();
    String result = null;
       
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_COUNTRY_ID", countryId);

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("RETRIEVE_ACTIVE_COUNTRY_NAME");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      result = (String) dataAccessUtil.execute(dataRequest);

      /* process results to determine if florist can support order */
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    
    return result;
  }

  
  /**
   * This method is a wrapper for the SP_GET_ORDER SP.  
   * It populates a Order VO based on the returned record set.
   * 
   * @param String - orderGuid
   * @return OrderVO 
   */
  public OrderVO getOrder(String orderGuid) 
  {
    DataRequest dataRequest = new DataRequest();
    OrderVO order = new OrderVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_GUID, orderGuid);

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_ORDER_BY_GUID);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        order.setMasterOrderNumber(outputs.getString(orderConstants.MASTER_ORDER_NUMBER));     
        order.setCustomerId(outputs.getLong(orderConstants.CUSTOMER_ID));
        order.setMembershipId(outputs.getLong(orderConstants.MEMBERSHIP_ID));
        order.setCompanyId(outputs.getString(orderConstants.COMPANY_ID));        
        order.setSourceCode(outputs.getString(orderConstants.SOURCE_CODE));
        order.setOriginId(outputs.getString(orderConstants.ORIGIN_ID));
        order.setOrderDate(outputs.getDate(orderConstants.ORDER_DATE));
        order.setOrderTotal(outputs.getDouble(orderConstants.ORDER_TOTAL));
        order.setProductTotal(outputs.getDouble(orderConstants.PRODUCT_TOTAL));
        order.setAddOnTotal(outputs.getDouble(orderConstants.ADD_ON_TOTAL));
        order.setServiceFeeTotal(outputs.getDouble(orderConstants.SERVICE_FEE_TOTAL));
        order.setShippingFeeTotal(outputs.getDouble(orderConstants.SHIPPING_FEE_TOTAL));
        order.setDiscountTotal(outputs.getDouble(orderConstants.DISCOUNT_TOTAL));
        order.setTaxTotal(outputs.getDouble(orderConstants.TAX_TOTAL));
        order.setLossPreventionIndicator(outputs.getString(orderConstants.LOSS_PREVENTION_INDICATOR));
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    } 
    if (isEmpty)
      return null;
    else 
      return order;
  }
  

  /**
   * This method is a wrapper for the SP_GET_INTERNATIONAL_FLORIST and 
   * SP_GET_ROUTABLE_FLORISTS SP.  It determine whether to return an 
   * international florist or a list of top tier florist and populates 
   * a RWDFloristVOList based on the returned record set.  If an international
   * florist is returned, it is the only florist in the list.  Otherwise
   * returns a list of top tier florists.  All of the business logic pertaining 
   * to florist filtering is contained in the SP.
   * 
   * @param OrderDetailVO 
   * @return RWDFloristVOList 
   */
  public RWDFloristVOList getRoutableFlorists(OrderDetailVO orderDtl, boolean returnAllFloristFlag)  
  {
    DataRequest dataRequestIntl = new DataRequest();
    DataRequest dataRequest = new DataRequest();
    RWDFloristVOList list = new RWDFloristVOList();
    Map outputsIntl = null;
    ResultSet outputs = null;
    
    try
    {

      
      /* setup store procedure input parameters */
      HashMap inputParamsIntl = new HashMap();
      inputParamsIntl.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()));

      /* build DataRequest object */      
      dataRequestIntl.setConnection(conn);
      dataRequestIntl.setStatementID(orderConstants.SP_GET_INTERNATIONAL_FLORIST);
      dataRequestIntl.setInputParams(inputParamsIntl);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtilIntl = DataAccessUtil.getInstance();    
      outputsIntl = (Map) dataAccessUtilIntl.execute(dataRequestIntl);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputsIntl.get(orderConstants.OUT_STATUS_PARAM);
     
      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        String message = (String) outputsIntl.get(orderConstants.OUT_MESSAGE_PARAM);
        throw new Exception(message);
      }
      
      /* populate object */
      String floristCode = (String) outputsIntl.get(orderConstants.FLORIST_CODE);
      if(floristCode != null) //return an international florist
      {
        RWDFloristVO rwdFloristVO = new RWDFloristVO();
        rwdFloristVO.setFloristId(floristCode);
        
        list.add(rwdFloristVO);
      }
      else //return a list of all florists or the top tier florist
      {          
        /* retrieve the customer record associated with the order detail record passed in */
        CustomerVO customer = getCustomer(orderDtl.getRecipientId());
        /* throw exeption if no customer record retrieved */
        if(customer == null)
        {
          throw new Exception("No customer record found");
        }

        OrderBillVO orderBill = getOrderBill(new Long(orderDtl.getOrderDetailId()).toString());
        if(customer.getCountry().equalsIgnoreCase("CA")) 
        {
          customer.setZipCode(new String(customer.getZipCode().substring(0,3)));
        }
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(orderConstants.ZIP_CODE, customer.getZipCode());
        inputParams.put(orderConstants.PRODUCT_ID, orderDtl.getProductId());
        inputParams.put(orderConstants.DELIVERY_DATE, orderDtl.getDeliveryDate()==null?null:new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
        inputParams.put(orderConstants.DELIVERY_DATE_END, orderDtl.getDeliveryDateEnd()==null?null:new java.sql.Date(orderDtl.getDeliveryDateEnd().getTime()));
        inputParams.put(orderConstants.SOURCE_CODE, orderDtl.getSourceCode());
        inputParams.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()).toString());
        inputParams.put(orderConstants.DELIVERY_CITY, customer.getCity());
        inputParams.put(orderConstants.DELIVERY_STATE, customer.getState());
        inputParams.put(orderConstants.ORDER_VALUE, new Long(new Double(orderBill.getProductAmount()).longValue()).toString());
        inputParams.put(orderConstants.RETURN_ALL, returnAllFloristFlag==true?"Y":"N");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(orderConstants.SP_GET_ROUTABLE_FLORISTS);
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();  
 
        outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
        
        /* populate object */
        while (outputs.next())
        {
          RWDFloristVO rwdFlorist = new RWDFloristVO();
          rwdFlorist.setFloristId(outputs.getString(orderConstants.FLORIST_ID));
          rwdFlorist.setScore(outputs.getInt(orderConstants.SCORE));
          rwdFlorist.setFloristWeight(outputs.getInt(orderConstants.WEIGHT));
          rwdFlorist.setFloristName(outputs.getString(orderConstants.FLORIST_NAME));
          rwdFlorist.setPhoneNumber(outputs.getString(orderConstants.PHONE_NUMBER));
          rwdFlorist.setMercuryFlag(outputs.getString(orderConstants.MERCURY_FLAG));
          rwdFlorist.setSuperFloristFlag(outputs.getString(orderConstants.SUPER_FLORIST_FLAG));
          rwdFlorist.setSundayDeliveryFlag(outputs.getString(orderConstants.SUNDAY_DELIVERY_FLAG));
          rwdFlorist.setCutoffTime(outputs.getString(orderConstants.CUTOFF_TIME));
          rwdFlorist.setStatus(outputs.getString(orderConstants.STATUS));
          
          list.add(rwdFlorist);
        }  
      }      
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null) outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    } 
   
    return list; 
  }
  
  /**
   * This method is a wrapper for the SP_INSERT_ORDER_FLORIST_USED SP.
   * It is used to insert a record into the order_florist_used table  
   * which is used to keep track of the florists that have been used for  
   * a particular order. 
   * 
   * @param OrderDetailVO 
   * @param String - floristId
   * @return none
   */
  public void insertOrderFloristUsed(OrderDetailVO orderDtl, String floristId)  
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
    
    try
    {      
        
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDtl.getOrderDetailId()));
      inputParams.put(orderConstants.FLORIST_ID, floristId);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_INSERT_ORDER_FLORIST_USED);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputs.get(orderConstants.OUT_STATUS_PARAM);
      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        String message = (String) outputs.get(orderConstants.OUT_MESSAGE_PARAM);
        throw new Exception(message);
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    }      
  }
  
  /**
   * This method is a wrapper for the SP_LOCK_ORDER SP.  It is used to
   * prevent CSRs from trying to route an order by hand while the system 
   * is already doing so.
   * 
   * @param OrderDetailVO 
   * @return int
   */
  public String lockOrder(OrderDetailVO orderDtl)  
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
    String status = null;
    String message = null;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ENTITY_TYPE, orderConstants.ENTITY_TYPE_VL);
      inputParams.put(orderConstants.ENTITY_ID, new Long(orderDtl.getOrderDetailId()).toString());
      inputParams.put(orderConstants.SESSION_ID, new Integer(orderDtl.hashCode()).toString());
      inputParams.put(orderConstants.CRS_ID, orderConstants.CRS_ID_VL);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_UPDATE_CSR_LOCKED_ENTITIES);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
      outputs = (Map) dataAccessUtil.execute(dataRequest);

      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      status = (String) outputs.get(orderConstants.OUT_STATUS_PARAM);

      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        message = (String) outputs.get(orderConstants.OUT_MESSAGE_PARAM);
        if(message.substring(0, 5).equalsIgnoreCase(orderConstants.VL_ERROR))
        {
          throw new Exception(message);
        }  
      }
    }
    catch (Exception e) 
    { 
      e.printStackTrace();
    } 
    
    return status;
  }
  
  /**
   * This method is a wrapper for the SP_UNLOCK_ORDER SP.  It is used to
   * allow CSRs to access a particular order once the system is done.
   * 
   * @param OrderDetailVO 
   * @return int
   */
  public String unlockOrder(OrderDetailVO orderDtl) 
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
    String status = null;
    String message = null;
    
    try
    {      
      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ENTITY_TYPE, orderConstants.ENTITY_TYPE_VL);
      inputParams.put(orderConstants.ENTITY_ID, new Long(orderDtl.getOrderDetailId()).toString());
      inputParams.put(orderConstants.SESSION_ID, new Integer(orderDtl.hashCode()).toString());
      inputParams.put(orderConstants.CRS_ID, orderConstants.CRS_ID_VL);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_DELETE_CSR_LOCKED_ENTITIES);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);

      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      status = (String) outputs.get(orderConstants.OUT_STATUS_PARAM);

      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        message = (String) outputs.get(orderConstants.OUT_MESSAGE_PARAM);
        if(message.substring(0, 5).equalsIgnoreCase(orderConstants.VL_ERROR))
        {
          throw new Exception(message);
        }  
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    
    return status;
  }
  
  /**
   * This method is a wrapper for the GET_ORDER_DETAILS SP.  
   * It populates an Order Detail VO based on the returned record set.
   * 
   * @param String - order detail id
   * @return OrderDetailVO 
   */
  public OrderDetailVO getOrderDetail(String orderDetailID)  
  {
    DataRequest dataRequest = new DataRequest();
    OrderDetailVO orderDetail = new OrderDetailVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
    
    try
    {
        
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_DETAIL_ID, new Long(orderDetailID));

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_ORDER_DETAIL);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {        
        isEmpty = false;
        orderDetail.setOrderDetailId(outputs.getLong(orderConstants.ORDER_DETAIL_ID));
        orderDetail.setDeliveryDate(outputs.getString(orderConstants.DELIVERY_DATE)==null?null:df.parse(outputs.getString(orderConstants.DELIVERY_DATE)));        
        orderDetail.setRecipientId(outputs.getLong(orderConstants.RECIPIENT_ID));
        orderDetail.setProductId(outputs.getString(orderConstants.PRODUCT_ID));
        orderDetail.setQuantity(outputs.getLong(orderConstants.QUANTITY));
	orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
        orderDetail.setColor1(outputs.getString(orderConstants.COLOR_1));
        orderDetail.setColor2(outputs.getString(orderConstants.COLOR_2));
        orderDetail.setSubstitutionIndicator(outputs.getString(orderConstants.SUBSTITUTION_INDICATOR));
        orderDetail.setSameDayGift(outputs.getString(orderConstants.SAME_DAY_GIFT));
        orderDetail.setOccasion(outputs.getString(orderConstants.OCCASION));
        orderDetail.setCardMessage(outputs.getString(orderConstants.CARD_MESSAGE));
        orderDetail.setCardSignature(outputs.getString(orderConstants.CARD_SIGNATURE));
        orderDetail.setSpecialInstructions(outputs.getString(orderConstants.SPECIAL_INSTRUCTIONS));
        orderDetail.setReleaseInfoIndicator(outputs.getString(orderConstants.RELEASE_INFO_INDICATOR));
        orderDetail.setFloristId(outputs.getString(orderConstants.FLORIST_ID));
        orderDetail.setShipMethod(outputs.getString(orderConstants.SHIP_METHOD));
        orderDetail.setShipDate(outputs.getDate(orderConstants.SHIP_DATE));                        
        orderDetail.setOrderDispCode(outputs.getString(orderConstants.ORDER_DISP_CODE));
        orderDetail.setDeliveryDateEnd(outputs.getString(orderConstants.DELIVERY_DATE_RANGE_END)==null?null:df.parse(outputs.getString(orderConstants.DELIVERY_DATE_RANGE_END)));
        orderDetail.setScrubbedOn(outputs.getDate(orderConstants.SCRUBBED_ON_DATE));
        orderDetail.setScrubbedBy(outputs.getString(orderConstants.USER_ID));
        orderDetail.setOrderGuid(outputs.getString(orderConstants.ORDER_GUID));                       
        orderDetail.setSourceCode(outputs.getString(orderConstants.SOURCE_CODE));
      }        
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    finally 
    {
       try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    } 
    if (isEmpty)
      return null;
    else
      return orderDetail;
  }
  /**
   * This method is a wrapper for the SP_GET_ORDER_BILLS SP.  
   * It populates a OrderBill VO based on the returned record set.
   * 
   * @param String - orderDetailId
   * @return OrderBillVO 
   */
  public OrderBillVO getOrderBill(String orderDetailId) 
  {
    DataRequest dataRequest = new DataRequest();
    OrderBillVO orderBill = new OrderBillVO();
    boolean isEmpty = true;
    ResultSet outputs = null;
    
    try
    {      
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put(orderConstants.ORDER_DETAIL_ID, orderDetailId);

      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID(orderConstants.SP_GET_ORDER_BILLS);
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (ResultSet) dataAccessUtil.execute(dataRequest);
      
      /* populate object */
      while (outputs.next())
      {
        isEmpty = false;
        orderBill.setProductAmount(outputs.getDouble(orderConstants.PRODUCT_AMOUNT));
        orderBill.setAddOnAmount(outputs.getDouble(orderConstants.ADD_ON_AMOUNT));
      }
    }
    catch (Exception e) 
    {            
      e.printStackTrace();
    } 
    finally 
    {
      try
      {
        /* close ResultSet */
        if(outputs != null)
          outputs.close();
      }
      catch (Exception e) 
      {            
        e.printStackTrace();
      }
    } 
    if (isEmpty)
      return null;
    else
      return orderBill;
  }
  
  // new 
  public void clearFlorist(String orderDetailId) throws Exception
  {
      DataRequest dataRequest = new DataRequest();
      Map outputs = null;   
       
      /* setup store procedure input parameters */
      HashMap inputParams = new HashMap();
      inputParams.put("IN_ORDER_DETAIL_ID", orderDetailId);
      
      /* build DataRequest object */
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("NULL_ORDER_DETAILS_FLORIST_ID");
      dataRequest.setInputParams(inputParams);
      
      /* execute the store prodcedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      outputs = (Map) dataAccessUtil.execute(dataRequest);
      
      /* read store prodcedure output parameters to determine 
       * if the procedure executed successfully */
      String status = (String) outputs.get(orderConstants.OUT_STATUS_PARAM);
      if(status != null && status.equalsIgnoreCase(orderConstants.VL_NO))
      {
        String message = (String) outputs.get(orderConstants.OUT_MESSAGE_PARAM);
        throw new Exception(message);
      }
    
  }

}