package com.ftd.oputil.dao;
import com.ftd.oputil.vo.BusinessFileVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class BusinessFileDAO 
{
  Connection conn;
  public BusinessFileDAO(Connection conn)
  {
    this.conn = conn;
  }

  /**
   * This method is a wrapper for the INSERT_BUSINESS SP.
   * It is used to insert a record into the business table  
   * which is a reference table with funeral home, and hospital information 
   * 
   * @param BusinessFileVO record
   */
  public void insertBusinessFileRecord(BusinessFileVO record)  throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
    
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    
    inputParams.put("IN_BUSINESS", record.getId());
    inputParams.put("IN_TYPE", record.getType());
    inputParams.put("IN_NAME", record.getName());
    inputParams.put("IN_ADDRESS", record.getAddress());
    inputParams.put("IN_CITY", record.getCity());
    inputParams.put("IN_STATE", record.getState());
    inputParams.put("IN_ZIPCODE", record.getZipcode());
    inputParams.put("IN_PHONE", record.getPhone());
    
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("INSERT_BUSINESS");
    dataRequest.setInputParams(inputParams);
    
    /* execute the stored procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      System.out.println(record.getId());
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    } 

  }
  /**
   * This method is a wrapper for the DELETE_BUSINESS SP.
   * It is used to delete all records from the business table in 
   * preparation for a new Business File to be loaded.
   * 
   */
  public void deleteBusinessFile()  throws Exception
  {
    DataRequest dataRequest = new DataRequest();
    Map outputs = null;
  
    /* setup stored procedure input parameters */
    HashMap inputParams = new HashMap();
    
    
    /* build DataRequest object */
    dataRequest.setConnection(conn);
    dataRequest.setStatementID("CLEAR_BUSINESS");
    dataRequest.setInputParams(inputParams);
    
    /* execute the stored procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
    outputs = (Map) dataAccessUtil.execute(dataRequest);
    String status = (String) outputs.get("OUT_STATUS");
    if(status != null && status.equalsIgnoreCase("N"))
    {
      String message = (String) outputs.get("OUT_MESSAGE");
      throw new Exception(message);
    }
  }
}