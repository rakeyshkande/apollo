package com.ftd.op.common.framework.util;


import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ftd.milespoints.webservice.MilesPointsRequest;
import com.ftd.milespoints.webservice.PartnerResponse;
import com.ftd.osp.utilities.ConfigurationUtil;

public class WebServiceConnectivityTests {
	
	ConfigurationUtil util;

	@Before
	public void setUp() throws Exception {
		util = createMock(ConfigurationUtil.class);
		expect(util.getFrpGlobalParm("SERVICE", "MILESPOINTS_SERVICE_URL"))
			.andReturn("http://localhost/ps/services/MPS?wsdl").anyTimes();
		EasyMock.replay(util);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testCheckMiles() throws Exception {
		
		WebServiceClientFactory wscf = new WebServiceClientFactory(util);
		
		MilesPointsRequest req = new MilesPointsRequest();
		req.setClientUserName("BEARS");
		req.setClientPassword("RULES");
		req.setMembershipNumber("03129325380");
		req.setMembershipType("UA");
		req.setMilesPointsRequested(20);
		
		PartnerResponse resp = wscf.getMilesPointsService().checkMiles(req);
		
		assertTrue(resp != null);
		
	}

}
