package com.ftd.op.order;

import java.math.BigDecimal;
import java.util.Date;

import com.ftd.op.order.service.OscarSelectorService;
import com.ftd.op.order.vo.OscarOrderInfoVO;
import junit.framework.TestCase;

public class OscarTests extends TestCase {
    public void testOscarService() {
        OscarSelectorService oss = new OscarSelectorService();
        OscarOrderInfoVO ooivo = new OscarOrderInfoVO();
        
        Date currDate = new Date();
        // Lat/long of FTD in Downers Grove
        String lat = "41.828595";
        String lon = "-88.034279";
        
        try {
            ooivo.setDeliveryDate(currDate);
            ooivo.setLatitude(lat);
            ooivo.setLongitude(lon);
            ooivo.setOrderDetailId("12345");
            ooivo.setScenarioGroup("Quality");
            ooivo.setSendingFlorist("90-8400AA");
            
            oss.createFillingRequest(ooivo);
            oss.addFloristToFillingRequest("13-3215BQ");
            oss.addFloristToFillingRequest("12-3430AA");
            String fillingFlorist = oss.selectFillingFlorist();
            System.out.println("And the winner is...: " + fillingFlorist);
        } catch (Exception e) {
            System.out.println("Oscar Tests Exception: " + e);
            e.printStackTrace();
        }
    }
}
