package com.ftd.op.order;

import com.ftd.oscar.athena.selector.AthenaRequest;
import com.ftd.oscar.athena.selector.AthenaRequestedFillingMember;
import com.ftd.oscar.athena.selector.AthenaSelector;
import com.ftd.oscar.athena.selector.AthenaSelectorService;
import com.ftd.oscar.athena.selector.SelectorResponse;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

import junit.framework.TestCase;

/*
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
*/

public class OscarRawTests extends TestCase {
    public void testGary() {
        try {
            BigDecimal lat = BigDecimal.valueOf(41.828595);
            BigDecimal lon = BigDecimal.valueOf(-88.034279);
            
            Date currDate = new Date();
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(currDate);
            XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            
            AthenaRequest ar = new AthenaRequest();
            AthenaRequestedFillingMember arfm1 = new AthenaRequestedFillingMember();
            AthenaRequestedFillingMember arfm2 = new AthenaRequestedFillingMember();
            ar.setClientCode("Apollo");
            ar.setClientReference("1239");
            ar.setDeliveryDate(xgc);
            ar.setRecipientLatitude(lat);
            ar.setRecipientLongitude(lon);
            ar.setSendingMemberCode("90-8400AA");
            ar.setScenarioGroupCode("Default");
            arfm1.setMemberCode("12-3430AA");
            arfm2.setMemberCode("13-3215BQ");
            ar.getFillingMember().add(arfm1);
            ar.getFillingMember().add(arfm2);

            // This logic works (while both methods below fail)
            //
            URL assUrl = new URL("http://oscar.ftdi.com/AthenaEAR-AthenaSelector-1.0/AthenaSelector?wsdl");
            //URL assUrl = new URL("http://oscarpapp01v1.ftdi.com:8080/AthenaEAR-AthenaSelector-1.0/AthenaSelector");
            AthenaSelectorService ass = new AthenaSelectorService(assUrl);
            Iterator iter = ass.getPorts();
            while(iter.hasNext())
            {
              QName testQ = (QName)iter.next();
              ass.addPort(testQ, SOAPBinding.SOAP11HTTP_BINDING, assUrl.toString());
            }
            AthenaSelector as = ass.getAthenaSelectorPort();
            ((BindingProvider) as).getRequestContext().put("javax.xml.ws.client.connectionTimeout", "2000");
            ((BindingProvider) as).getRequestContext().put("javax.xml.ws.client.receiveTimeout", "2000");
            SelectorResponse sr = as.selectFillingMember(ar);
            if (sr != null) {
                System.out.println("requestId: " + sr.getRequestId() + " Selected member: " + sr.getSelectedMember());
            } else {
                System.out.println("No response object returned");                
            }
                        
            // This logic works but requires cxf-2.4.2.jar which conflicts with CAMS in COM
            //
            /*
            JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
            //factory.getInInterceptors().add(new LoggingInInterceptor());
            //factory.getOutInterceptors().add(new LoggingOutInterceptor());
            factory.setServiceClass(AthenaSelector.class);
            factory.setAddress("http://oscar.ftdi.com/AthenaEAR-AthenaSelector-1.0/AthenaSelector?wsdl");
            AthenaSelector client = (AthenaSelector) factory.create();
            // Set timeout on client call first
            Client proxy = ClientProxy.getClient(client);                 
            long timeout = Long.parseLong("2000");  // Timeout in milliseconds
            HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
            HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();                 
            httpClientPolicy.setConnectionTimeout(timeout);
            httpClientPolicy.setReceiveTimeout(timeout);
            conduit.setClient(httpClientPolicy);
            // Now make call
            SelectorResponse sr0 = client.selectFillingMember(ar);
            if (sr0 != null) {
                System.out.println("requestId: " + sr0.getRequestId() + " Selected member: " + sr0.getSelectedMember());
            } else {
                System.out.println("No response object returned");                
            }
            */

            // This original logic doesn't work since wsdl is auto-generated by web service and includes url
            // with port number that prevents passage through F5.  The getAthenaSelectorPort method uses that url.
            //
            /*
            //URL assUrl = new URL("http://erosdapp02:8180/AthenaEAR-AthenaSelector-1.0/AthenaSelector?wsdl");
            //URL assUrl = new URL("http://erosdapp03:8180/AthenaEAR-AthenaSelector-1.0/AthenaSelector?wsdl");
            URL assUrl = new URL("http://oscar.ftdi.com/AthenaEAR-AthenaSelector-1.0/AthenaSelector?wsdl");
            AthenaSelectorService ass = new AthenaSelectorService(assUrl);
            AthenaSelector as = ass.getAthenaSelectorPort();
            SelectorResponse sr = as.selectFillingMember(ar);
            if (sr != null) {
                System.out.println("requestId: " + sr.getRequestId() + " Selected member: " + sr.getSelectedMember());
            } else {
                System.out.println("No response object returned");                
            }
            */
            
        } catch (Exception e) {
            System.out.println("GPS Exception: " + e);
            e.printStackTrace();
        }
    }
}