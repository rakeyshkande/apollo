/**
 * 
 */
package com.ftd.op.order;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.IdGeneratorUtil;
import com.ftd.osp.utilities.id.vo.IdRequestVO;

/**
 * This class will build a simple order XML
 * @author cjohnson
 *
 */
public class OrderBuilder {
	
	public OrderBuilder() throws Exception {
		mockServices();
	}

	ConfigurationUtil configUtil = null;
	IdGeneratorUtil idUtil = null;
	Connection conn = null;
	
	//define order attributes here
	String baseMasterOrderNumber = "MME";
	String baseOrderNumber = "FME";
	String customerPhoneNumber = "8019686900";
	String GDNumber = "6006492147013900456";
	String GDPin = "6807";
	boolean useCreditCard = false;
	boolean useGiftCard = false;
	String GDTransactionID = null;  //we need to authorize this gift card to get a TX id
	String sourceCode = "14983";
	String masterOrderNumber = baseMasterOrderNumber + RandomStringUtils.random(6,false, true);
	String OrderNumber = baseOrderNumber +RandomStringUtils.random(6,false,true);
	
	final List products = Arrays.asList(
		new TestItem() {{
			productId = "6026";
			price = 5.00;
			serviceFee = 0.00;
			orderNumber = OrderNumber;
			shippingMethod = "SD";
			taxAmount = 0.25;
			
		}}//,
//		new TestItem() {{
//			productId = "GC01";
//			price = 75.00;
//			shippingFee = 10.99;
//			orderNumber = OrderNumber + "1";
//			shippingMethod = "GR";
//		}}
	);
	
	double GDAmount = 50;///(double)Math.round((useGiftCard? getOrderTotal() : 0.0) * 100) / 100;
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		OrderBuilder builder = new OrderBuilder();
		
		String xml = builder.buildXML();
		System.out.print(xml);
		System.out.println();
		URL url = new URL("http://brass2.ftdi.com:8712/OG/servlet/OrderGatherer");
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter os = new OutputStreamWriter(conn.getOutputStream());
		os.write("Order="+URLEncoder.encode(xml, "UTF-8"));
		os.flush();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		os.close();
		br.close();
		
		System.out.println("submitted to OG: " + builder.masterOrderNumber);
		
	}
	
	
	
	private String buildXML() {
		

		
		String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" +
					"<order key-name=\"\">" +
					"	<header>" +

					"		<buyer-signed-in>Y</buyer-signed-in>" +
					"		<buyer-first-name><![CDATA[Rose]]></buyer-first-name>" +
					"		<socket-timestamp />" +
					"		<alt-pay-method>" +
					"			<account-number>03129325380</account-number>" +
					"			<miles-points>600</miles-points>" +
					"			<order-number />" +
					"			<auth-id />" +
					"			<payment-type>UA</payment-type>" +
					"			<redemption-rate>0.009333</redemption-rate>" +
					"			<approval-amount />" +
					"		</alt-pay-method>" +
					"		<buyer-email-address><![CDATA[rlazuk@ftdi.com]]></buyer-email-address>" +
					"		<buyer-city><![CDATA[Sandy]]></buyer-city>" +
					"		<buyer-postal-code><![CDATA[84093]]></buyer-postal-code>" +
					"		<order-extensions />" +
					"		<language-id>ENUS</language-id>" +
					"		<ariba-asn-buyer-number />" +
					"		<buyer-daytime-phone><![CDATA["+customerPhoneNumber+"]]></buyer-daytime-phone>";
				if (useCreditCard) {
					xml+="		<cc-approval-amt>"+(getOrderTotal()-GDAmount)+"</cc-approval-amt> " +
					"		<cc-number>4444333322221111</cc-number>" +
					"		<cc-type>Visa</cc-type>" +
					"		<cc-avs-result />" +
					"		<cc-exp-date>11/14</cc-exp-date>" +
					"		<cc-approval-action-code>000</cc-approval-action-code>" +
					"		<cc-approval-code />" +
					"		<cc-acq-data>aWb161332488045973cQR5Pd5e00fC</cc-acq-data>" +
					"		<csc-response-code >M</csc-response-code>" +
					"		<csc-validated-flag>Y</csc-validated-flag>" +
					"       <cc-approval-verbage>AP</cc-approval-verbage> ";
				} else {
					xml+= "<cc-type /><cc-number /><cc-approval-amt /><cc-approval-verbage /><csc-validated-flag>N</csc-validated-flag><cc-approval-code />";
				}
					xml+="		<buyer-fax />" +
					"		<buyer-state><![CDATA[UT]]></buyer-state>" +
					"		<origin>FOX</origin>" +
					"		<ariba-buyer-cookie />" +
					"		<aafes-ticket-number />" +
					"		<buyer-evening-phone><![CDATA["+customerPhoneNumber+"]]></buyer-evening-phone>" +
					"		<loss-prevention-indicator>1319291648310185900069911815</loss-prevention-indicator>" +
					"		<fraud-codes />" +
					"		<buyer-last-name><![CDATA[Lazuk]]></buyer-last-name>" +
					"		<ariba-payload />" +
					"		<transaction-date>Tue Mar 13 11:51:11 CST 2012</transaction-date>" +
					"		<order-count>"+products.size()+"</order-count>";
					
				if (useGiftCard) {
					xml+="	<gift-certificates>" +
					"			<gift-certificate>" +
					"				<payment-type>GD</payment-type>" +
					"				<transaction-id><![CDATA["+GDTransactionID+"]]></transaction-id>" +
					"				<amount><![CDATA["+GDAmount+"]]></amount>" +
					"				<pin><![CDATA["+GDPin+"]]></pin>" +
					"				<number><![CDATA["+GDNumber+"]]></number>" +
					"			</gift-certificate>" +
					"		</gift-certificates>";
				}
					
					xml+="	<buyer-work-ext />" +
					"		<source-code>"+sourceCode+"</source-code>" +
					"		<news-letter-flag>Y</news-letter-flag>" +
					"		<order-amount>"+getOrderTotal()+"</order-amount>" +
					"		<co-brand-credit-card-code />" +
					"		<co-brands />" +
					"		<buyer-country><![CDATA[US]]></buyer-country>" +
					"		<buyer-address1><![CDATA[8993 Ida Lane]]></buyer-address1>" +
					"		<buyer-address2 />" +
					"                <master-order-number>"+masterOrderNumber+"</master-order-number>" +
					"	</header>" +
					"	<items>";
			for (TestItem item : (List<TestItem>)products) {
				xml+="		<item>" +
					"			<order-number>"+item.orderNumber+"</order-number>" +
					"			<productid>"+item.productId+"</productid>" +
					"           <product-price>"+item.price+"</product-price>" +
					"			<retail-variable-price>"+item.price+"</retail-variable-price>" +
					"			<discounted-product-price>"+item.price+"</discounted-product-price>" +
					"			<discount-amount>0</discount-amount>" +
					"			<service-fee>"+item.serviceFee+"</service-fee>" +
					"			<drop-ship-charges>"+item.shippingFee+"</drop-ship-charges>	" +
					"           <order-total>"+getOrderTotal()+"</order-total>" +
					"			<taxes>" +
					"				<tax>" +
					"					<amount>"+item.taxAmount+"</amount>" +
					"					<name>il-tax</name>" +
					"				</tax>" +
					"" +
					"			</taxes>		" +
					"" +
					"			<recip-city><![CDATA[Enterprise]]></recip-city>" +
					"			<second-color-choice />" +
					"			<fol-indicator>FTD</fol-indicator>" +
					"			<lmg-email-signature />" +
					"			<qms-usps-range-record-type />" +
					"			<lmg-email-address />" +
					"			<qms-address1><![CDATA[100 Regal]]></qms-address1>" +
					"			<qms-address2 />" +
					"			<size-indicator>S</size-indicator>" +
					"			<item-of-the-week-flag>Y</item-of-the-week-flag>" +
					"			<qms-state><![CDATA[AL]]></qms-state>" +
					"			<ariba-ams-project-code />" +
					"			<recip-international />" +
					"			<qms-result-code />" +
					"			<auto-renew />" +
					"			<qms-firm-name />" +
					"			<add-ons />" +
					"			<recip-phone><![CDATA[3140000000]]></recip-phone>" +
					"			<qms-override-flag />" +
					"			<qms-latitude />" +
					"			<item-extensions />" +
					"			<recip-address1><![CDATA[100 Regal]]></recip-address1>" +
					"			<sunday-delivery-flag />" +
					"			<recip-address2 />" +
					"			<taxes />" +
					"			<shipping-method>"+item.shippingMethod+"</shipping-method>" +
					"			<product-substitution-acknowledgement />" +
					"			<sender-release-flag />" +
					"			<extra-shipping-fee />" +
					"			<lmg-flag />" +
					"			<ship-to-type-name />" +
					"			<personal-greeting-id />" +
					"" +
					"			<item-source-code>"+sourceCode+"</item-source-code>" +
					"			<ariba-po-number />" +
					"			<ariba-cost-center />" +
					"			<occassion>13</occassion>" +
					"			<first-color-choice />" +
					"			<recip-country><![CDATA[US]]></recip-country>" +
					"			<ariba-unspsc-code>empty</ariba-unspsc-code>" +
					"			<card-message><![CDATA[Happy Halloween!  We love you and thought you might like a sweet treat without any kind of trick :)  Love Jeff, Cheri, Jack, Ella and Evan]]></card-message>" +
					"			<second-delivery-date />" +
					"			<ship-to-type-info />" +
					"			" +
					"			<recip-last-name><![CDATA[Clark]]></recip-last-name>" +
					"			<recip-phone-ext />" +
					"			<recip-postal-code><![CDATA[36330]]></recip-postal-code>" +
					"			<qms-city><![CDATA[Enterprise]]></qms-city>" +
					"			<qms-longtitude />" +
					"			<delivery-date>03/20/2012</delivery-date>" +
					"			<ship-to-type><![CDATA[HOME]]></ship-to-type>" +
					"			<recip-state><![CDATA[AL]]></recip-state>" +
					"			<special-instructions />" +
					"			<qms-postal-code><![CDATA[36330]]></qms-postal-code>" +
					"			<tax-amount>0.00</tax-amount>" +
					"			<card-signature />" +
					"			<recip-first-name><![CDATA[Buddy]]></recip-first-name>" +
					"		</item>";
			}
			
			xml+=   "	</items>" +
					"	<guid />" +
					"</order>";
		
		
		return xml;
	}
	
	private void mockServices() throws Exception {
		configUtil = createMock(ConfigurationUtil.class);
		expect(configUtil.getFrpGlobalParm("SERVICE", "PAYMENT_SERVICE_URL"))
			.andReturn("http://localhost/ps/services/PS?wsdl").anyTimes();
		expect(configUtil.getSecureProperty("SERVICE", "SVS_CLIENT")).andReturn("BEARS").anyTimes();
		expect(configUtil.getSecureProperty("SERVICE", "SVS_HASHCODE")).andReturn("RULES").anyTimes();
		expect(configUtil.getProperty(isA(String.class), isA(String.class))).andReturn("mocked out message").anyTimes();
		replay(configUtil);
		
		conn = createMock(Connection.class);
		expect(conn.isClosed()).andReturn(true);
		replay(conn);
	}
	
	private double getOrderTotal() {
		
		double total = 0;
		for (TestItem item : (List<TestItem>)products) {
			total = total + item.price + item.serviceFee + item.shippingFee + item.taxAmount;
		}
		return total;
	}
	
	class TestItem {
		public String productId;
		public double price;
		public double serviceFee = 0.0;
		public double shippingFee = 0.0;
		public String orderNumber = "";
		public String shippingMethod = "";
		public double taxAmount = 0.0;
	}

}
