package com.ftd.osp.test.ordervalidator.util;

import junit.framework.*;
import java.util.*;

import com.ftd.osp.ordervalidator.util.StringReplacer;

public class StringReplacerTest extends TestCase 
{
    public StringReplacerTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("StringReplacerTest");
        
        suite.addTest(new StringReplacerTest("testReplaceNull"));
        suite.addTest(new StringReplacerTest("testReplace"));
        suite.addTest(new StringReplacerTest("testReplaceWordsNotFound"));

        return suite;
    }

    public void testReplaceWordsNotFound()
    {
        try
        {
            Map map = new HashMap();
            String inStr = "This is a test string {1}";
            
            String outStr = StringReplacer.replace(inStr, map);

            this.assertNotNull(inStr);
            this.assertNotNull(outStr);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testReplaceNull()
    {
        try
        {
            Map map = new HashMap();
            map.put("1", null);
            String inStr = "This is a test string {1}";
            
            String outStr = StringReplacer.replace(inStr, map);

            this.assertNotNull(inStr);
            this.assertNotNull(outStr);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testReplace()
    {
        try
        {
            Map map = new HashMap();
            map.put("1", "with replacement values");
            String inStr = "This is a test string {1}";
            
            String outStr = StringReplacer.replace(inStr, map);

            this.assertNotNull(inStr);
            this.assertNotNull(outStr);
            this.assertTrue(outStr.indexOf("{1}") == -1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            StringReplacerTest stringReplacerTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                stringReplacerTest = new StringReplacerTest(args[i]);
                stringReplacerTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}

