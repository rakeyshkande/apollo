/*
 * @(#) ValidateOrderPriceTest.java      1.0     2004/08/31
 * 
 * SameDayFreshCut_ServiceFee_Less_Than
 */


package com.ftd.osp.test.ordervalidator.util;

import com.ftd.osp.ordervalidator.util.ValidateOrderPrice;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;

import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * <code>ValidateOrderPriceTest</code> is the JUnit test class for 
 * <code>ValidateOrderPrice</code>.
 * 
 * The NoCeiling test case cannot be run with the other test cases.  This test
 * requires the ceiling record in the datastore to be null.
 * 
 * The ZeroCeiling test case should not be run with the other test case.  This
 * test requires the ceiling record in the database to be 0.
 * 
 * The INSERT_PRICE_VARIANCE stored procedure should not be called more than
 * once for the same line item.  An exception will be thrown.  The datastore
 * exception message indicates 
 * "Unable to locate order detail for confirmation number ... "
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/08/31  Initial Release.(RFL)
 * 
 */
public class ValidateOrderPriceTest extends TestCase  
{



    public ValidateOrderPriceTest(String name) 
    {
        super(name);
    }//end method ValidateOrderPrice
    

    /**
     * 
     * @param args
     */
    public static void main(String[] args) 
    {
        int iCount = args.length;
        
        if(iCount > 0)
        {
            ValidateOrderPriceTest validateOrderPrice = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateOrderPrice = new ValidateOrderPriceTest(args[i]);
                validateOrderPrice.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }//end method main

    
    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateOrderPriceTest");
        
//        suite.addTest(new ValidateOrderPriceTest("NoCeiling"));
//        suite.addTest(new ValidateOrderPriceTest("ZeroCeiling"));
        suite.addTest(new ValidateOrderPriceTest("SameDayFreshCut_ServiceFee_Greater_Than"));
        suite.addTest(new ValidateOrderPriceTest("SameDayFreshCut_ServiceFee_Less_Than"));
        suite.addTest(new ValidateOrderPriceTest("SameDayFreshCut_ServiceFee_Equal"));
        
//        suite.addTest(new ValidateOrderPriceTest("Freshcut_ServiceFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("Freshcut_ServiceFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("Freshcut_ServiceFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("Floral_ServiceFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("Floral_ServiceFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("Floral_ServiceFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("SpecialtyGift_ShippingFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SpecialtyGift_ShippingFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SpecialtyGift_ShippingFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("None_ServiceFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ServiceFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ServiceFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("None_ShippingFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ShippingFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ShippingFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ServiceFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ServiceFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ServiceFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ShippingFee_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ShippingFee_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("SameDayGift_ShippingFee_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("None_ProductPrice_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ProductPrice_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ProductPrice_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("None_Tax_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_Tax_Less_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_Tax_Equal"));

//        suite.addTest(new ValidateOrderPriceTest("None_ShippingTax_Greater_Than"));
//        suite.addTest(new ValidateOrderPriceTest("None_ShippingTax_Less_Than"));
//       suite.addTest(new ValidateOrderPriceTest("None_ShippingTax_Equal"));

        return suite;
        
    }//end method suite()
    
    
    public void NoCeiling()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            OrderVO order = this.getOrder(1, 1);
            
            
            boolean response = ValidateOrderPrice.validate(order,
                                (OrderDetailsVO)order.getOrderDetail().get(0),
                                con);

            this.assertTrue(!response);
            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }//end method NoCeiling
    
    
    public void ZeroCeiling()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            OrderVO order = this.getOrder(1, 1);
            
            
            boolean response = ValidateOrderPrice.validate(order,
                                (OrderDetailsVO)order.getOrderDetail().get(0),
                                con);

            this.assertTrue(response);
            
        }
        catch(Exception e)
        {
            this.fail(e.toString());
        }
    }//end method ZeroCeiling
    
    
    /*
     * 
     */
    public void SameDayFreshCut_ServiceFee_Greater_Than() 
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            OrderVO order = this.getOrder(1, 2);
            
            boolean response = ValidateOrderPrice.validate(order,
                                (OrderDetailsVO)order.getOrderDetail().get(0),
                                con);

            this.assertTrue(response);
            
        }//end try
        catch(Exception e)
        {
            this.fail(e.toString());
        }//end catch ()
    }//end method SameDayFreshCut_ServiceFee_Greater_Than
    
    
    /*
     * 
     */
    public void SameDayFreshCut_ServiceFee_Less_Than() 
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            OrderVO order = this.getOrder(1, 3);
            
            boolean response = ValidateOrderPrice.validate(order,
                                (OrderDetailsVO)order.getOrderDetail().get(0),
                                con);

            this.assertTrue(response);
            
        }//end try
        catch(Exception e)
        {
            this.fail(e.toString());
        }//end catch ()
    }//end method SameDayFreshCut_ServiceFee_Less_Than
    
    
    /*
     * 
     */
    public void SameDayFreshCut_ServiceFee_Equal() 
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            OrderVO order = this.getOrder(1, 4);
            
            boolean response = ValidateOrderPrice.validate(order,
                                (OrderDetailsVO)order.getOrderDetail().get(0),
                                con);

            this.assertTrue(!response);
            
        }//end try
        catch(Exception e)
        {
            this.fail(e.toString());
        }//end catch ()
    }//end method SameDayFreshCut_ServiceFee_Equal
    
    
    /*
     * 
     */
    private OrderDetailsVO getLineItem(int line) 
    {
        RecipientAddressesVO recipAddress = new RecipientAddressesVO();
        List Addresses = new ArrayList();
        RecipientsVO recipient = new RecipientsVO();
        List recipPhones = new ArrayList();
        List recipients = new ArrayList();
        OrderDetailsVO lineItem = new OrderDetailsVO();
        
        recipAddress.setPostalCode("60532");
        recipAddress.setStateProvince("IL");
        recipAddress.setCountry("US");
        Addresses.add(recipAddress);
        recipient.setRecipientAddresses(Addresses);
        
        recipient.setRecipientPhones(recipPhones);
        
        recipients.add(recipient);
        
        lineItem.setRecipients(recipients);
        
        switch (line) 
        {
            case 1:
                lineItem.setSourceCode("4029");
                lineItem.setProductId("FFCC");
                lineItem.setSizeChoice("A");
                lineItem.setExternalOrderNumber("Z0000000042");
                lineItem.setProductsAmount("39.99");
                return lineItem;
                
            case 2:
                lineItem.setSourceCode("4029");
                lineItem.setProductId("F039");
                lineItem.setSizeChoice("A");
                lineItem.setExternalOrderNumber("Z0000000085");
                lineItem.setProductsAmount("52.99");
                lineItem.setServiceFeeAmount("100.99");
                lineItem.setTaxAmount("0.00");
                lineItem.setShippingTax("2.82");
                return lineItem;
                
            case 3:
                lineItem.setSourceCode("4029");
                lineItem.setProductId("F039");
                lineItem.setSizeChoice("A");
                lineItem.setExternalOrderNumber("Z0000000051");
                lineItem.setProductsAmount("38.99");
                lineItem.setServiceFeeAmount("0.01");
                lineItem.setTaxAmount("2.82");
                lineItem.setShippingTax("0.00");
                return lineItem;
                
            case 4:
                lineItem.setSourceCode("4029");
                lineItem.setProductId("F039");
                lineItem.setSizeChoice("A");
                lineItem.setExternalOrderNumber("Z0000000054");
                lineItem.setProductsAmount("38.99");
                lineItem.setServiceFeeAmount("9.99");
                lineItem.setTaxAmount("2.82");
                lineItem.setShippingTax("0.00");
                return lineItem;
                
            default:
                return null;
                
        }//end switch
        
    }//end method getLineItem


        
    /*
     * 
     */
    private OrderVO getOrder(int code, int line) 
    {
        OrderVO order = new OrderVO();

        List lineItems = new ArrayList();
        
        switch (code) 
        {
            case 1:
                lineItems.add(this.getLineItem(line));
                order.setOrderDetail(lineItems);
                return order;
                
            case 2:
                return order;
                
            default:
                return null;
                
        }//end switch
        
    }//end method getOrder
        

}//end class