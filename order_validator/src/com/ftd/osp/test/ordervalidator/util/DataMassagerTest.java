package com.ftd.osp.test.ordervalidator.util;

import com.ftd.osp.utilities.GUID.GUIDGenerator;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import junit.framework.*;
import java.util.*;
import java.sql.Connection;

import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.order.*;

   /*
    There are two xml configuration files that may need to be changed when 
    testig the DataMassager. test_data_config.xml and metadata_config.xml
    Both of these files are used by the DeliveryDateUTIL to test wether or not
    we wish to use the system time or properties set in the test_data_config.xml file.
    You may turn on/off test mode, but to do so you must change it in both files.
   */
public class DataMassagerTest extends TestCase 
{   
    private static final String TWO_DAY_DELIVERY = "2F";
    private static final String  SAME_DAY_DELIVERY = "SD";
    private static final String NEXT_DAY_DELIVERY = "ND";
    private static final String FLORIST_DELIVERY = "";
    
    public DataMassagerTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("DataMassagerTest");
        //suite.addTest(new DataMassagerTest("testDataMassagerEmptySourceCode"));
        //suite.addTest(new DataMassagerTest("testDataMassagerPhoneNumberFormat"));
        //suite.addTest(new DataMassagerTest("testDataMassagerPhoneExtensions"));
       // suite.addTest(new DataMassagerTest("testDataMassagerItemColors"));
       // suite.addTest(new DataMassagerTest("testDataMassagerOccasionNameToId"));
       // suite.addTest(new DataMassagerTest("testDataMassagerLMGEmail"));
        //suite.addTest(new DataMassagerTest("testDataMassagerJCPbuyerEmail"));
        //suite.addTest(new DataMassagerTest("testDataMassagerJCPINT"));
       // suite.addTest(new DataMassagerTest("testDataMassagerBreakAddresses"));
       // suite.addTest(new DataMassagerTest("testDataMassagerNoCCInfo"));
        //suite.addTest(new DataMassagerTest("testDataMassagerCCNumberFormat"));
       // suite.addTest(new DataMassagerTest("testDataMassagerCCExpFormat"));
       // suite.addTest(new DataMassagerTest("testDataMassagerBuyerEmailFormat"));
       // suite.addTest(new DataMassagerTest("testDataMassagerShipToTypeRules"));
       // suite.addTest(new DataMassagerTest("testDataMassagerCountryConversion"));
        //suite.addTest(new DataMassagerTest("testDataMassagerStateConversion"));
        //suite.addTest(new DataMassagerTest("testDataMassagerTruncateShipToName"));
        //suite.addTest(new DataMassagerTest("testDataMassagerTestOrigin"));
        //suite.addTest(new DataMassagerTest("testDataMassagerProductSubcode"));
        //suite.addTest(new DataMassagerTest("testDataMassagerBulkRules"));
        //suite.addTest(new DataMassagerTest("testDataMassagerCoBrandInfoInsert"));
       //suite.addTest(new DataMassagerTest("testDataMassagerSetShippingFee"));
       // suite.addTest(new DataMassagerTest("testDataMassagerRetainServiceFee"));
         //suite.addTest(new DataMassagerTest("testDataMassagerAmazonDropShip"));
        //suite.addTest(new DataMassagerTest("testDataMassagerAmazonFloral"));
          suite.addTest(new DataMassagerTest("testDataMassagerAmazonSameDayFlorist"));
         //suite.addTest(new DataMassagerTest("testDataMassagerAmazonNextDayCarrier"));
          // suite.addTest(new DataMassagerTest("testDataMassagerDelvieryDate"));
        return suite;
    }

public void testDataMassagerAmazonDropShip()
    {
      try
      {//Results the shipMethod on the OrderDetailsVO should be set to 2F(Two day)
        //to test this we must use an order from  Product_Master table
        //and find the Product_type where the value of this column is SPEGFT and
        //use this rows product id to populate the item objects ProductID field.
        //
        //Also test this by checking that the delivery date falls into the approperiate two day range
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(22);
            
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getShipMethod(),TWO_DAY_DELIVERY);
             //Change the date string to meet your testing needs
            this.assertEquals( ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate(),"09/17/2004" );
      }catch(Exception e)
      {
        this.fail();
      }

    }


  public void testDataMassagerAmazonFloral()
    {
      try
      {//Results the shipMethod on the OrderDetailsVO should be set to ""
        //to test this we must use an order from  Product_Master table
        //and find the Product_type where the value of this column is FLORAL and
        //use this rows product id to populate the item objects ProductID field.
        
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(23);
            GUIDGenerator g = GUIDGenerator.getInstance();
            order.setGUID(g.getGUID());
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getShipMethod(),FLORIST_DELIVERY);
            //Change the date string to meet your testing needs
            System.out.println("Delivery Date::" + ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate() );
            this.assertEquals( ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate(),"09/15/2004" );
          
     

      }catch(Exception e)
      {
        this.fail();
      }

    }


 public void testDataMassagerAmazonSameDayFlorist()
    {
      try
      {//Results the shipMethod on the OrderDetailsVO should be set to "SD"
        //to test this we must use an order from  Product_Master table
        //and find the Product_type where the value of this column is SDFC and
        //and ship_method_florist = "Y"
        //use this rows product id to populate the item objects ProductID field.
         Connection con = TestHelper.getConnection();
        //ScrubMapperDAO dao = new ScrubMapperDAO(con);
         OrderVO order = getOrder(20);
       //OrderVO order = dao.mapOrderFromDB("FTD_GUID_56863878401038925523072903303010952737840-6026877950-39275037502121333590195578404715543260630-11679841850-18640977106959548255-34968979410113331831772311248907413175154750287072");
         
         order = DataMassager.massageOrderData(order, con);
         this.assertNotNull(order);
         System.out.println( ((OrderDetailsVO)order.getOrderDetail().get(0)).getShipMethod() );
        //this.assertEquals( ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate(), "09/15/2004" );
        //Change the date string to meet your testing needs
        this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getShipMethod(),SAME_DAY_DELIVERY);
      

      }catch(Exception e)
      {
        this.fail();
      }

    }
    
    
    public void testDataMassagerAmazonNextDayCarrier()
    {
      try
      {//Results the shipMethod on the OrderDetailsVO should be set to "ND"
        //to test this we must use an order from  Product_Master table
        //and find the Product_type where the value of this column is SDFC and
        //and ship_method_carrier = "Y" and ship_method_florist = "N"
        //use this rows product id to populate the item objects ProductID field.
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(24);

            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);
            System.out.println( ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate() );
             //Change the date string to meet your testing needs
            this.assertEquals( ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate(), "09/27/2004" );
            
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getShipMethod(),NEXT_DAY_DELIVERY);


      }catch(Exception e)
      {
        this.fail();
      }

    }



    public  void testDataMassagerCoBrandInfoInsert()
    {
      try 
      {
        Connection con = TestHelper.getConnection();
        ScrubMapperDAO dao = new ScrubMapperDAO(con);
        // Advo
       // OrderVO order = dao.mapOrderFromDB("FTD_GUID_-11391242670-1298526789024783104602016261820016190690410-15118951530223677150-78309312314698819200-104720683305415223240-618043203251891561686172-2021254141252-7833495371831131261429194");
      //OrderVO order = dao.mapOrderFromDB("FTD_GUID_844311319015142356480-8728099390211412596002123705320-4807626420-13647782910-1894344991-1109165671014656292110-92250415301152980801251-1868646079684645168201731806701945741324412835214");
         //FOX ORDER
         //OrderVO order = dao.mapOrderFromDB("FTD_GUID_8581849130-1077844520123282392501935173640-6395178820-1137641640-388813401267914325910220015710170-70313108903807268550-1035558537251-361075194173140796870793433857124193-68580302973");
        // Target
        //OrderVO order = dao.mapOrderFromDB("FTD_GUID_-21363624510-110729247104694142640-5996050280-12989670650-10228912405027111730-145675493811038306810041170617207223113950-18796244982512114978899193-114547120910-1029214952210170532789121");
        //AMAZON ORDER
        OrderVO order = dao.mapOrderFromDB("FTD_GUID_17803591770-4014119702062432854015068771530-1964048091016232305750-1296296902014860744371-8455343840-717074726013079579330281433038251-9987093568423791346173-10477461637444696102214");
        
        order = DataMassager.massageOrderData(order, con);
        this.assertNotNull(order);
        
      } catch (Exception ex) 
      {
         ex.printStackTrace();
      } 
      
    }
    
    
    
    public  void testDataMassagerDelvieryDate()
    {
      try 
      {
        Connection con = TestHelper.getConnection();
        ScrubMapperDAO dao = new ScrubMapperDAO(con);
       
        //OrderVO order = dao.mapOrderFromDB("FTD_GUID_17803591770-4014119702062432854015068771530-1964048091016232305750-1296296902014860744371-8455343840-717074726013079579330281433038251-9987093568423791346173-10477461637444696102214");
        OrderVO order = dao.mapOrderFromDB("FTD_GUID_17803591770-4014119702062432854015068771530-1964048091016232305750-1296296902014860744371-8455343840-717074726013079579330281433038251-9987093568423791346173-10477461637444696102214");
        order = DataMassager.massageOrderData(order, con);
        this.assertNotNull(order);
        System.out.println("Delivery Date - Product 3067 " + 
                    ((OrderDetailsVO)order.getOrderDetail().get(0)).getDeliveryDate() );
      } catch (Exception ex) 
      {
         ex.printStackTrace();
      } 
      
    }
  
    public void testDataMassagerTestOrigin()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(17);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);
            this.assertEquals(order.getOrderOrigin(), "AMZNI");

            // Now test changing to the test origin
            List payments = new ArrayList();
            List creditCards = new ArrayList();
            PaymentsVO payment = new PaymentsVO();            
            CreditCardsVO creditCard = new CreditCardsVO();            
            creditCard.setCCNumber("4111111111111111");
            creditCards.add(creditCard);
            payment.setCreditCards(creditCards);
            payment.setPaymentMethodType("C");
            payments.add(payment);
            order.setPayments(payments);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(order.getOrderOrigin(), "test");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }    
    
    public void testDataMassagerSetShippingFee()
    {
      try
      {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(18);

            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);
            this.assertEquals(  ((OrderDetailsVO)order.getOrderDetail().get(0)).getServiceFeeAmount(),"0" );
            this.assertEquals( ((OrderDetailsVO)order.getOrderDetail().get(0)).getShippingFeeAmount(), "18.00");


          
      }catch(Exception e)
      {
        this.fail();
      }
      
    }
    
    public void testDataMassagerRetainServiceFee()
    {
      try
      {
          Connection con = TestHelper.getConnection();
          OrderVO order = getOrder(19);
          
          order = DataMassager.massageOrderData(order, con);
          this.assertNotNull(order);
          this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getServiceFeeAmount(),"9.00" );
          this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getShippingFeeAmount(),"3.00" );          
          
      }catch(Exception e)
      {
        this.fail();
      }
    }

    public void testDataMassagerTruncateShipToName()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(16);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // check for a length of 40
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getName().length(), 40);

            // test a length of less than 40
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName("test");            
            order = DataMassager.massageOrderData(order, con);

            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getName().length(), 4);
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerStateConversion()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(15);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for US/IL
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getStateProv(), "IL");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getStateProvince(), "IL");

            // Check for CA/AB
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setCountry("Canada");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("Canada");            
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setStateProv("Alberta");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setStateProvince("Alberta");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getStateProv(), "AB");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getStateProvince(), "AB");

            // Check for DOM/empty
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setStateProv("Not a state");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setStateProvince("Not a state");            
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getStateProv(), "NA");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getStateProvince(), "NA");

            // Check for INT/empty
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setCountry("France");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("France");
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setStateProv("");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setStateProvince("");            
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getStateProv(), "NA");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getStateProvince(), "NA");                        
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }    

    public void testDataMassagerCountryConversion()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(14);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for country code US
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getCountry(), "US");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getCountry(), "US");

            // Check for country code FR
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setCountry("France");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("France");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getCountry(), "FR");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getCountry(), "FR");

            // Check for country code empty
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setCountry("Not a country");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("Not a country");            
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getCountry(), "");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getCountry(), "");            
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }    

    public void testDataMassagerShipToTypeRules()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(13);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for address type default          
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "HOME");

            // Check that if the ship to name is N/A the address type is HOME
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName("n/a");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressType("HOME");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "HOME");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getName(), null);
            
            // Check that if ship to name contains HOSP and the ship to type is
            // R the type should be changed to H
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName("Christ Hospital");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressType("HOME");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "HOSPITAL");
            
            // Check that is this to name contains Funeral or Mortuary and the 
            // type is R set type to F
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName("Marks Funeral Home");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressType("HOME");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "FUNERAL HOME");
            
            // If the ship to name does not contain HOSP, funeral or mortuary and
            // the type is R set it to B
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName("Hal's Subway");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressType("HOME");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "BUSINESS");
            
            // If the ship to name is null and type is B change the type to R
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setName(null);
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressType("BUSINESS");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressType(), "HOME");            
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerBuyerEmailFormat()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(12);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for lower case buyer email address          
            this.assertEquals(((BuyerEmailsVO)((BuyerVO)order.getBuyer().get(0)).getBuyerEmails().get(0)).getEmail(), "jpenney@ftdi.com");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerCCExpFormat()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(11);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for default CC exp date format  MM/yy          
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCExpiration(), "01/04");

            // Check for MM/dd/yyyy
            ((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).setCCExpiration("01/12/04");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCExpiration(), "01/04");

            // Check for MM/yyyy
            ((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).setCCExpiration("01/2004");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCExpiration(), "01/2004");            
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerCCNumberFormat()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(10);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for default CC number format            
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCNumber(), "4111111111111111");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerNoCCInfo()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(9);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for default CC info
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCType(), "AX");            
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCNumber(), "4444444444444444");
            this.assertEquals(((CreditCardsVO)((PaymentsVO)order.getPayments().get(0)).getCreditCards().get(0)).getCCExpiration(), "10/04");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }
    
    public void testDataMassagerBreakAddresses()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(8);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check both address lines
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getAddressLine1(), "12345678901234567890");
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getAddressLine2(), "12345678901234567890");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressLine1(), "12345678901234567890");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressLine2(), "12345678901234567890");

            // Check for the case when they can't be broken up
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setAddressLine1("123 456789012345678901234567890123456789012345678901");
            ((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).setAddressLine2(null);
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressLine1("123 456789012345678901234567890123456789012345678901");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressLine2(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getAddressLine1(),"123 456789012345678901234567890123456789012345678901" );
            this.assertEquals(((BuyerAddressesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerAddresses().get(0)).getAddressLine2(), "");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressLine1(), "123 456789012345678901234567890123456789012345678901");
            this.assertEquals(((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).getAddressLine2(), "");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }    

    public void testDataMassagerJCPINT()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(7);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Change to JCP source code
            this.assertEquals(order.getSourceCode(), "7588");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }    

    public void testDataMassagerJCPbuyerEmail()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(6);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Change to JCP source code
            this.assertEquals(order.getSourceCode(), "7731");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }
    
    public void testDataMassagerLMGEmail()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(5);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check simple conversion
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getLastMinuteGiftEmail(), "jpenney@ftdi.com");

            // Null should return null
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setLastMinuteGiftEmail(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getLastMinuteGiftEmail(), null);
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerOccasionNameToId()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(4);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check simple conversion
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getOccassionId(), "3");

            // Make sure that if the code exists it is not erased
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getOccassionId(), "3");

            // Blank occasion should be changed to 8
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setOccassionId(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getOccassionId(), "8");
        }
        catch(Exception e)
        {
            this.fail();
        }    
    }

    public void testDataMassagerItemColors()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(3);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check simple conversion
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorFirstChoice(), "A");
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorSecondChoice(), "B");

            // Check that color IDs are not changed
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorFirstChoice(), "A");
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorSecondChoice(), "B");            

            // Check that if no colors are sent in that none come out
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setColorFirstChoice(null);
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setColorSecondChoice(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorFirstChoice(), null);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorSecondChoice(), null);

            // Check that is the color name cannot be found the code is set to X
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setColorFirstChoice("Not a color");
            ((OrderDetailsVO)order.getOrderDetail().get(0)).setColorSecondChoice("No color");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorFirstChoice(), "X");
            this.assertEquals(((OrderDetailsVO)order.getOrderDetail().get(0)).getColorSecondChoice(), "X");            
        }
        catch(Exception e)
        {
            this.fail();
        }        
    }    

    public void testDataMassagerPhoneExtensions()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(2);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check extension parsing
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "7086521290");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "1234");

            // Check that if an extension is already parsed it does not get erased
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "1234");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "7086521290");
            
            // Check that an extension value of 0, NA and NONE is set to null
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension("NA");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "7086521290");

            // Check that an extension has dashes removed
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension("123-456");
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "123456");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "7086521290");

            // Check that ext. is parsed correctly
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("123456789 extension 4321");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "4321");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "123456789");

            // Check for invalid phone number
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("john1234");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "john1234");

            // Check for null extension
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("847 555-1212 extension");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "8475551212");

            // Check for some other extensions
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("847 555-1212 next 12334");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "8475551212next12334");

            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("x12345");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "12345");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "");

            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber("3214DJFHDH(*&(*%^dkjhdsh");
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setExtension(null);
            order = DataMassager.massageOrderData(order, con);
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getExtension(), "");
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "3214DJFHDHdkjhdsh");            
        }    
        catch(Exception e)
        {
            this.fail();
        }        
    }    

    public void testDataMassagerPhoneNumberFormat()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(1);
    
            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check removal of (, ), -, " "       
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "7086521290");
            this.assertEquals(((RecipientPhonesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientPhones().get(0)).getPhoneNumber(), "9999999999");

            // Check a null phone number
            ((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).setPhoneNumber(null);
            ((RecipientPhonesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientPhones().get(0)).setPhoneNumber(null);

            order = DataMassager.massageOrderData(order, con);
            this.assertNotNull(order);

            // Check for empty phone numbers
            this.assertEquals(((BuyerPhonesVO)((BuyerVO)order.getBuyer().get(0)).getBuyerPhones().get(0)).getPhoneNumber(), "");
            this.assertEquals(((RecipientPhonesVO)((RecipientsVO)((OrderDetailsVO)order.getOrderDetail().get(0)).getRecipients().get(0)).getRecipientPhones().get(0)).getPhoneNumber(), "");
            
        }
        catch(Exception e)
        {
            this.fail();
        }        
    }

    public void testDataMassagerEmptySourceCode()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(0);
    
            order = DataMassager.massageOrderData(order, con);

            this.assertNotNull(order);

            this.assertEquals(order.getSourceCode(), "350");
        }
        catch(Exception e)
        {
            this.fail();
        }        
    }

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List payments = new ArrayList();
        List creditCards = new ArrayList();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List recipPhones = new ArrayList();
        List buyerPhones = new ArrayList();
        List recipAddresses = new ArrayList();
        List buyers = new ArrayList();
        List buyerEmails = new ArrayList();
        List buyerAddresses = new ArrayList();
        List memberships = new ArrayList();
        List qmsAddresses = new ArrayList();
        
        OrderDetailsVO item = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        RecipientPhonesVO recipPhone = null;
        BuyerPhonesVO buyerPhone = null;
        BuyerVO buyer = null;
        BuyerEmailsVO buyerEmail = null;
        BuyerAddressesVO buyerAddress = null;
        MembershipsVO membership = null;
        
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0:  // default source code
                order.setSourceCode("");
                item = new OrderDetailsVO();
                break;
            case 1: // Phone number formats
                // Buyer
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");

                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                buyerPhone = new BuyerPhonesVO();
                buyerPhone.setPhoneNumber("(708) 652-1290");
                
                buyerPhones.add(buyerPhone);
                buyer.setBuyerPhones(buyerPhones);
                buyers.add(buyer);
                order.setBuyer(buyers);

                // Item
                item = new OrderDetailsVO();
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("(999) 999-9999");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);                
                item.setRecipients(recipients);

                recipAddress = new RecipientAddressesVO();                
                recipAddress.setCountry("US");                
                
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                recipients.add(recipient);
                break;                
            case 2: // extension parsing
                // Buyer
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");

                buyerPhone = new BuyerPhonesVO();
                buyerPhone.setPhoneNumber("(708) 652-1290 x1234");
                
                buyerPhones.add(buyerPhone);
                buyer.setBuyerPhones(buyerPhones);
                buyers.add(buyer);
                order.setBuyer(buyers);

                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);
                
                break;
            case 3: // Color choices
                item = new OrderDetailsVO();
                item.setColorFirstChoice("RED");
                item.setColorSecondChoice("YELLOW");
                break;
            case 4: // Occasion
                item = new OrderDetailsVO();
                item.setOccassionId("Birthday");
                break;
            case 5: // LMG Email
                item = new OrderDetailsVO();
                item.setLastMinuteGiftEmail("JPENNEY@FTDI.COM");
                break;    
            case 6: // JCP employee source code
                order.setSourceCode("196h");
                
                // Buyer
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");

                buyerEmail = new BuyerEmailsVO();
                buyerEmail.setEmail("jpenney@jcpenney.com");

                buyerEmails.add(buyerEmail);
                buyer.setBuyerEmails(buyerEmails);
                buyers.add(buyer);
                order.setBuyer(buyers);

                creditCard = new CreditCardsVO();
                creditCard.setCCExpiration("01/04");
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setCreditCards(creditCards);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payments.add(payment);

                order.setPayments(payments);                
                break;
            case 7: // JCP INT source code
                order.setSourceCode("196h");

                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();                
                recipAddress.setAddressLine1("123 Wine Street");
                recipAddress.setCity("Paris");
                recipAddress.setStateProvince("");
                recipAddress.setCountry("France");
                recipAddress.setPostalCode("NA");                
                
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                break;
            case 8:
                // Buyer
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("12345678901234567890");
                buyerAddress.setAddressLine2("12345678901234567890");
                buyerAddresses.add(buyerAddress);
                
                buyer = new BuyerVO();
                buyer.setBuyerAddresses(buyerAddresses);
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyers.add(buyer);
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();                
                recipAddress.setAddressLine1("12345678901234567890 12345678901234567890");                
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                order.setBuyer(buyers);
                break;
            case 9: // CPC info
                order.setSourceCode("4500");
                
                creditCard = new CreditCardsVO();
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setPaymentMethodType("I");
                payment.setCreditCards(creditCards);
                payments.add(payment);

                order.setPayments(payments);                
                break;
            case 10:
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111-1111111 11111");
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setPaymentMethodType("C");
                payment.setPaymentsType("VI");
                payment.setCreditCards(creditCards);
                payments.add(payment);

                order.setPayments(payments);                
                break;
            case 11: // CC exp date format
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setPaymentMethodType("C");
                payment.setPaymentsType("VI");
                payment.setCreditCards(creditCards);
                payments.add(payment);

                order.setPayments(payments);                
                break; 
            case 12: // buyer email address format
                // Buyer
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");

                buyerEmail = new BuyerEmailsVO();
                buyerEmail.setEmail("JPENNEY@FTDI.COM");
                buyerEmails.add(buyerEmail);
                buyer.setBuyerEmails(buyerEmails);
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;
            case 13: // Ship to type rules 
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                break;
            case 14: // Country to code conversion
                // Buyer
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("United States");
                buyerAddresses.add(buyerAddress);
                
                buyer = new BuyerVO();
                buyer.setBuyerAddresses(buyerAddresses);
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyers.add(buyer);
                order.setBuyer(buyers);
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                break;
            case 15: // state to code conversion
                // Buyer
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("United States");
                buyerAddress.setStateProv("Illinois");
                buyerAddresses.add(buyerAddress);
                
                buyer = new BuyerVO();
                buyer.setBuyerAddresses(buyerAddresses);
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyers.add(buyer);
                order.setBuyer(buyers);
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                break;
            case 16: // ship to name truncate test                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddress.setName("a very long ship to nameaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setRecipients(recipients);
                break;
            case 17: // Origin test
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("2512");
                break;
           case 18: //Test shipping fee and zero out service fee
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("2512");
                
                recipient = new RecipientsVO();
                recipient.setLastName("Jenkins");
                recipient.setFirstName("Carl");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setProductId("G043");
                item.setLineNumber("1");
                item.setRecipients(recipients);
                item.setServiceFeeAmount("18.00");
                item.setShippingFeeAmount("5.00");              
                break;
                
            case 19:
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("2512");
                
                recipient = new RecipientsVO();
                recipient.setLastName("Jenkins");
                recipient.setFirstName("Carl");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                
                item = new OrderDetailsVO();
                item.setProductId("7040");
                item.setRecipients(recipients);
                item.setServiceFeeAmount("9.00");
                item.setShippingFeeAmount("3.00");          
                break;
            case 20://test ship method for same day fresh cuts
                 //delivered by florist (Same day delivery)
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");

                recipient = new RecipientsVO();
                recipient.setLastName("Jenkins");
                recipient.setFirstName("Carl");
                recipients.add(recipient);

                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddress.setPostalCode("60616");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                item = new OrderDetailsVO();
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDFC
                item.setProductId("6103");
                item.setShipMethodCarrierFlag("Y");
                item.setShipMethodFloristFlag("Y");
                item.setLineNumber("1");
                item.setRecipients(recipients);
                item.setServiceFeeAmount("9.00");
                item.setShippingFeeAmount("3.00");          
                break;
            case 21://test orders that will have delivery dates calculated
                    order.setOrderOrigin("AMZNI");
                    order.setSourceCode("2512");
                    
                    recipient = new RecipientsVO();
                    recipient.setLastName("Jenkins");
                    recipient.setFirstName("Carl");
                    recipients.add(recipient);
                    
                    recipAddress = new RecipientAddressesVO();
                    recipAddress.setCountry("United States");
                    recipAddress.setStateProvince("Illinois");
                    recipAddress.setPostalCode("60616");
                    recipAddresses.add(recipAddress);
                    recipient.setRecipientAddresses(recipAddresses);
                    
                    item = new OrderDetailsVO();
                    item.setProductId("G043");
                    item.setLineNumber("1");
                    
                    item.setServiceFeeAmount("25.00");
                    item.setShippingFeeAmount("5.00"); 
                    item.setRecipients(recipients);
               
                    break;
                case 22://test ship method for drop ship only product
                    order.setOrderOrigin("AMZNI");
                    order.setSourceCode("7800");
                    
    
                    recipient = new RecipientsVO();
                    recipient.setLastName("Jenkins");
                    recipient.setFirstName("Carl");
                    recipients.add(recipient);
    
                    recipAddress = new RecipientAddressesVO();
                    recipAddress.setCountry("United States");
                    recipAddress.setStateProvince("Illinois");
                    recipAddress.setPostalCode("60616");
                    recipAddresses.add(recipAddress);
                    recipient.setRecipientAddresses(recipAddresses);
    
                    item = new OrderDetailsVO();
                    //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                    //table as a value of SPEGFT
                    item.setProductId("G043");
                    item.setLineNumber("1");
                    item.setRecipients(recipients);
                    item.setServiceFeeAmount("9.00");
                    item.setShippingFeeAmount("3.00");
    
                    break;
            case 23://test ship method for floral product
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                order.setMasterOrderNumber("A004CARL");
                order.setOrderDate("09/02/2004");
                recipient = new RecipientsVO();
                recipient.setRecipientId(999999);
                recipient.setLastName("Jenkins");
                recipient.setFirstName("Carl");
                recipients.add(recipient);

                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddress.setPostalCode("60616");
                recipAddress.setRecipientAddressId(99999999);
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                item = new OrderDetailsVO();
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of FLORAL
                item.setProductId("3072");
                
                
                item.setLineNumber("1");
                item.setSourceCode("7800");
                item.setExternalOrderNumber("Z1085198");
                item.setRecipients(recipients);
                item.setServiceFeeAmount("9.00");
                item.setShippingFeeAmount("3.00");

                break;
           
             case 24://test ship method for same day fresh cuts
                 //delivered by carrier
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");

                recipient = new RecipientsVO();
                recipient.setLastName("Jenkins");
                recipient.setFirstName("Carl");
                recipients.add(recipient);

                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("United States");
                recipAddress.setStateProvince("Illinois");
                recipAddress.setPostalCode("60616");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                item = new OrderDetailsVO();
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDFC
                item.setProductId("3067");
                item.setLineNumber("1");
                item.setRecipients(recipients);
                item.setServiceFeeAmount("9.00");
                item.setShippingFeeAmount("3.00");
                item.setSourceCode("7800");

                break;

            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }
    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            DataMassagerTest dataMassagerTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                dataMassagerTest = new DataMassagerTest(args[i]);
                dataMassagerTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }

}