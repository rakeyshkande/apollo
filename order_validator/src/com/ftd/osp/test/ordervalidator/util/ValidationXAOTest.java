package com.ftd.osp.test.ordervalidator.util;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import org.w3c.dom.Document;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.*;
import com.ftd.osp.ordervalidator.vo.*;

public class ValidationXAOTest extends TestCase 
{
    public ValidationXAOTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidationXAOTest");
        
        suite.addTest(new ValidationXAOTest("testValidationXAO"));
        suite.addTest(new ValidationXAOTest("testValidationXAONull"));


        return suite;
    }

    public void testValidationXAO()
    {
        try
        {
            ValidationXAO vXAO = new ValidationXAO();
            Connection conn = TestHelper.getConnection();
            OrderVO order = TestHelper.getScrubOrder(conn);
            OrderValidator validator = new OrderValidator();

            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, conn);
            
            Document xml = vXAO.generateValidationXML(vOrder);

            this.assertNotNull(xml);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidationXAONull()
    {
        try
        {
            ValidationXAO vXAO = new ValidationXAO();
            Document xml = vXAO.generateValidationXML(null);

            this.assertNotNull(xml);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // 
                item.setLineNumber("1");
                                
                break;
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidationXAOTest validationXAOTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validationXAOTest = new ValidationXAOTest(args[i]);
                validationXAOTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}