package com.ftd.osp.test.ordervalidator;

import java.sql.Connection;

import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.dataaccess.util.OSPConnectionPool;

//import com.ftd.osp.ordergatherer.*;

public class TestHelper 
{
  private static final String DUPLICATE_RECORD_ERROR_MESSAGE = "com.ftd.osp.ordergatherer.OrderMappingException: java.lang.Exception: DUPLICATE MASTER ORDER NUMBER";
  private static final String TEST_ORDER_FOUND_ERROR = "com.ftd.osp.ordergatherer.OrderValidationException: TEST ORDER FOUND ERROR";

    public TestHelper()
    {
    }

    public static Connection getConnection()
    {
        Connection conn = null;
        try
        {
            OSPConnectionPool pool = OSPConnectionPool.getInstance();        
            //conn = TestConnectionGenerator.getConnection();
            conn = pool.getConnection("jdbc:oracle:thin:@adonis.ftdi.com:1521:dev49j", "osp", "osp");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return conn;        
    }

    public static OrderVO getScrubOrder(Connection conn)
    {
        OrderVO order = null;
        try
        {
            ScrubMapperDAO dao = new ScrubMapperDAO(conn);
            //order = dao.mapOrderFromDB("FTD_GUID_14272413680-112546977006299033100-87873324406396385950-13276422600-13862144880164888222719937586550-18093090200140613884501220529874248-6103238721631027492037217-38444408037-333976590115");
            order = dao.mapOrderFromDB("FTD_GUID_1957767536062054889704552716470-18350784490-16178541360-106807952009633358980-6479526631-20259552270-17401444280-6067284180-437658130248-6764007178-394510545197-17768062327129575587456");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return order;        
    }

    public static OrderVO getFRPOrder(Connection conn)
    {
        OrderVO order = null;
        try
        {
            FRPMapperDAO dao = new FRPMapperDAO(conn);
            order = dao.mapOrderFromDB("FTD_GUID_-1619097151011862845250210007592010783688790-1734887569018850007610-111234720301017216749120405754600120669538306321985900-433396882247-569941308146114831935190-337291438553004164082");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return order;        
    }
/*
    public static String saveOrderXML(String orderXML)
    {
        Connection connection = null;
        OrderVO order = new OrderVO();
        try
        {
            OrderPreprocessor orderPreprocessor = new OrderPreprocessor(orderXML);
            order = orderPreprocessor.createOrder();
            OrderValidator orderValidator = new OrderValidator();
            orderValidator.validate(order);

            // get database connection
            connection =  getConnection();
            // set autocommit to false
            connection.setAutoCommit(false);
        
            OrderDBMapper orderDBMapper = new OrderDBMapper(connection);
            orderDBMapper.map(order);

            // commit the JDBC connection
            connection.commit();
        }
        catch(OrderValidationException ove)
        {
            ove.printStackTrace();
            try  
            {
            if (connection != null)  
            {
                connection.rollback();
            }                  
            } 
            catch (Exception ex)  
            {
                ex.printStackTrace();
            }

            if(ove.toString().equals(TEST_ORDER_FOUND_ERROR))
            {
                // If test credit card number and test email address
                //was used, the order needs to be thrown out
                //so no further processing is done on it.  Return a 200
                //response back.
            }
            else
            {

            }
        }
        catch(OrderMappingException ome)
        {
            ome.printStackTrace();
            try  
            {
                if (connection != null)  
                {
                    connection.rollback();
                }                  
            } 
            catch (Exception ex)  
            {
                ex.printStackTrace();
            }

            if(ome.getMessage().toString().equals(DUPLICATE_RECORD_ERROR_MESSAGE))
            {
                //if duplicate master order number, throw order
                //out and return 200 response back.
            }
            else
            {

            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            try  
            {
                if (connection != null)  
                {
                    connection.rollback();
                }                  
            } 
            catch (Exception ex1)  
            {
                ex1.printStackTrace();
            }
        } 
        finally 
        {
            try  
            {
                if (connection != null)  
                {
                    connection.setAutoCommit(true);
                    connection.close();
                }                  
            } 
            catch (Exception ex)  
            {
                ex.printStackTrace();
            }
        }

        return order.getGUID();
    }
*/
}