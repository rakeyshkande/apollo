package com.ftd.osp.test.ordervalidator;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.OrderValidatorBO;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.GUID.*;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.framework.dispatcher.*;
import com.ftd.osp.utilities.plugins.Logger;
import java.net.*;
import java.io.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.*;

import com.ftd.osp.utilities.xml.*;

public class OrderValidatorBOTest
{
    private String guid = null;
    private static final String TEST_SETUP_FILE_NAME = "test_setup.xml";
    private static Logger logger= new Logger("com.ftd.osp.ordervalidator.OrderValidatorBO");

    public OrderValidatorBOTest()
    {
        testExecuteFromSetup(); 
        //System.out.println(loadConfig().size());
        
    }

    public static void main(String[] args)
    {
        new OrderValidatorBOTest();
    }

    public void testExecuteFromSetup()
    {
        ArrayList testThreads = new ArrayList();
        TestThread testThread = null;
        boolean check = true;
        long start = 0;
        long end = 0;
    
        System.out.print("Loading cache ...");
        CacheManager.getInstance();
        System.out.println("OK");

        System.out.print("Loading setup ...");
        ArrayList list = loadConfig();
        System.out.println("OK");

        System.out.println("Starting test with " + list.size() + " threads");
        
        ConfigItem configItem = null;
        start = System.currentTimeMillis();
        for(int i = 0; i < list.size(); i ++)
        {
          configItem = (ConfigItem)list.get(i);
          testThread = new TestThread(configItem.guidList, configItem.name);
          testThreads.add(testThread);
          testThread.start();
        }

        while(true)
        {
          check = true;
          for(int j = 0; j < testThreads.size(); j++)
          {
            check = check && ((TestThread)testThreads.get(j)).isFinished();
          }
          if(check)
          break;
        }
        end = System.currentTimeMillis();
        System.out.println("TOTAL PROCESSING TIME = " + (end - start));
    }

    private ArrayList loadConfig()
    {      
        ArrayList list = new ArrayList();
      
        try
        {
          URL url = this.getClass().getClassLoader().getResource(TEST_SETUP_FILE_NAME);
          if (url == null) 
          {
             System.out.println("Setup sile not found. Exiting ...");
             System.exit(0);
          }
        
          File configurationFile = new File(url.getFile());
          Document configDoc = DOMUtil.getDocument(configurationFile);

          Element e = null;
          ArrayList guids = null;
          ConfigItem ci = null;
          Node node = null;
          NodeList tmpNL = null;
          NodeList threadList = DOMUtil.selectNodes(configDoc, "/test/thread");
          for(int i =0; i < threadList.getLength(); i ++)
          {
              ci = new ConfigItem();
              e = (Element)threadList.item(i);
              ci.name = e.getAttribute("id");

              guids = new ArrayList();
              node = threadList.item(i);
              tmpNL = DOMUtil.selectNodes(node, "order");
              
              for(int j = 0; j < tmpNL.getLength(); j++)
              {
                guids.add(((Element)(tmpNL.item(j))).getAttribute("guid"));
              }
              ci.guidList = guids;
              list.add(ci);
          }
        }catch(Exception e)
        {
            System.out.println(e.toString());
        }
        return list;   
    }
}

class ConfigItem
{
  public ArrayList guidList = null;
  public String name = null; 
}