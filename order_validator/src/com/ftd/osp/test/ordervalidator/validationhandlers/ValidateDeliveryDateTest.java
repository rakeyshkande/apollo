package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateDeliveryDate;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateDeliveryDateTest extends TestCase 
{
    public ValidateDeliveryDateTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateDeliveryDateTest");
        
        suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDatePassed"));
        suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDateExceedsMax"));
        suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDateInvalid"));
        suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDateMissing"));
        suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDateSunINT"));
        //suite.addTest(new ValidateDeliveryDateTest("testValidateDeliveryDateSunDropShip"));
    
        return suite;
    }

/*
    public void testValidateDeliveryDateSunDropShip()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_SUNDAY))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
*/

    public void testValidateDeliveryDateSunINT()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_INVALID))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateDeliveryDateMissing()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_MISSING))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }


    public void testValidateDeliveryDateInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_HOLIDAY))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateDeliveryDateExceedsMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_TOO_FAR_OFF))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateDeliveryDatePassed()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateDeliveryDate.validate(request);

            this.assertNotNull(response);

            Iterator it = response.keySet().iterator();
            boolean foundError = false;
            String error = null;
            while(it.hasNext())
            {
                error = (String)it.next();

                if(error.startsWith(ValidationConstants.RESPONSE_DELIVERY_DATE_PASSED))
                {
                    foundError = true;
                    break;
                }
            }

            this.assertTrue(foundError);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        item = new OrderDetailsVO();
        List recipients = new ArrayList();
        List addresses = new ArrayList();
        RecipientsVO recipient = null;
        RecipientAddressesVO address = null;

        switch (code)
        {
            case 0: // Delivery date passed
                item.setDeliveryDate("01/20/2003");
                break;
            case 1: // Delivery date exceeds max
                item.setDeliveryDate("01/20/2006");
                break;
            case 2: // Delivery date invalid
                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setAddressLine1("1745 DANBURY LANE");
                address.setAddressLine2("");
                address.setCity("ROCHESTER HILLS");
                address.setStateProvince("MI");
                address.setPostalCode("48307-3310");
                address.setCountry("US");
                item.setProductId("8212");
                item.setDeliveryDate("02/14/2004");
                item.setLineNumber("1");
                break;
            case 3: // Delivery date missing
                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setAddressLine1("1745 DANBURY LANE");
                address.setAddressLine2("");
                address.setCity("ROCHESTER HILLS");
                address.setStateProvince("MI");
                address.setPostalCode("48307-3310");
                item.setProductId("2001");
                item.setDeliveryDate("");
                item.setLineNumber("1");
                break;
            case 4: // SUN INT date
                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setAddressLine1("1745 DANBURY LANE");
                address.setAddressLine2("");
                address.setCity("Pairs");
                address.setStateProvince("Paris");
                address.setPostalCode("");
                address.setCountry("FR");
                address.setInternational("Y");
                item.setProductId("8212");
                item.setDeliveryDate("03/07/2004");
                item.setLineNumber("1");
                break;
            case 5: // SUN drop ship date
                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setAddressLine1("1745 DANBURY LANE");
                address.setAddressLine2("");
                address.setCity("ROCHESTER HILLS");
                address.setStateProvince("MI");
                address.setPostalCode("48307");
                item.setProductId("2001");
                item.setShipMethod("GR");
                item.setDeliveryDate("03/07/2004");
                item.setLineNumber("1");
                break;                                                
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateDeliveryDateTest validateDeliveryDateTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateDeliveryDateTest = new ValidateDeliveryDateTest(args[i]);
                validateDeliveryDateTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}