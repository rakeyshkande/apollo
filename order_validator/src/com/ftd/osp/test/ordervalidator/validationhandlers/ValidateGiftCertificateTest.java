package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateGiftCertificate;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;

public class ValidateGiftCertificateTest extends TestCase 
{
    public ValidateGiftCertificateTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateGiftCertificateTest");
        
        suite.addTest(new ValidateGiftCertificateTest("testValidateGiftCertificate"));


        return suite;
    }

    public void testValidateGiftCertificate()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateGiftCertificate.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // Find keyword
                item.setLineNumber("1");
                                
                break;
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateGiftCertificateTest validateGiftCertificateTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateGiftCertificateTest = new ValidateGiftCertificateTest(args[i]);
                validateGiftCertificateTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}