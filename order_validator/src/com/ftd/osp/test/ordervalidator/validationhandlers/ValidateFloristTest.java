package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateFlorist;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateFloristTest extends TestCase 
{
    public ValidateFloristTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateFloristTest");
        
        //suite.addTest(new ValidateFloristTest("testValidateFloristInvalid"));
        //suite.addTest(new ValidateFloristTest("testValidateFloristValid"));
       // suite.addTest(new ValidateFloristTest("testValidateFloristCodifiedProduct"));
        //suite.addTest(new ValidateFloristTest("testValidateFloristSundayDelivery"));
        
        //suite.addTest(new ValidateFloristTest("testValidateFloristSDFCFloristAvailableFloristSelected"));
       // suite.addTest(new ValidateFloristTest("testValidateFloristSDFCFloristNotAvailableFloristSelected"));
       // suite.addTest(new ValidateFloristTest("testValidateFloristSDFCFloristAvailableCarrierSelected"));
        suite.addTest(new ValidateFloristTest("testValidateFloristSDFCFloristNotAvailableCarrierSelected"));
        //suite.addTest(new ValidateFloristTest("testValidateFloristSDGFloristNotAvailableFloristSelected"));

        return suite;
    }
    
    public void testValidateFloristSDGFloristNotAvailableFloristSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(8));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE + "1"), ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    public void testValidateFloristSDFCFloristNotAvailableCarrierSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidateFlorist.validate(request);
this.assertEquals(response.get(ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED + "1"), ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED);
            //this.assertNotNull(response);
           // this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateFloristSDFCFloristAvailableCarrierSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED + "1"), ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateFloristSDFCFloristNotAvailableFloristSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE + "1"), ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateFloristSDFCFloristAvailableFloristSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateFloristCodifiedProduct()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_FLORIST_NO_PRODUCT + "1"), ValidationConstants.RESPONSE_FLORIST_NO_PRODUCT);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    
    

    public void testValidateFloristSundayDelivery()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);
            // First test when a florist is selected
            this.assertEquals(response.get(ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY + "1"), ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY);

            // Next test for when a florist is not selected
            ((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).setFloristNumber("");
            response = ValidateFlorist.validate(request);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY + "1"), ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    


    public void testValidateFloristInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE + "1"), ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateFloristValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateFlorist.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

        
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List coBrand = new ArrayList();
        List membership = new ArrayList();
        List payments = new ArrayList();
        List buyers = new ArrayList();
        List buyerAddresses = new ArrayList();
        List buyerPhones = new ArrayList();
        List buyerEmails = new ArrayList();
        List orderContactInfo = new ArrayList();
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        
        List recipients = new ArrayList();
        List recipAddresses = new ArrayList();
        
        List recipPhones = new ArrayList();
        
        List items = new ArrayList();
        List addons = new ArrayList();
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // Valid Florist
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setFloristNumber("24-6173AA");
                
                item.setAddOns(addons);
                break;
            case 1: // Invalid Florist
                order.setOrderOrigin("amzni");
                order.setCoBrand(coBrand);
                order.setMemberships(membership);
                order.setPayments(payments);
                buyer = new BuyerVO();
                buyers.add(buyer);
                buyerAddress = new BuyerAddressesVO();
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);
                buyer.setBuyerPhones(buyerPhones);
                buyer.setBuyerEmails(buyerEmails);
                order.setBuyer(buyers);
                order.setOrderContactInfo(orderContactInfo);
                
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                recipient.setRecipientPhones(recipPhones);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("60532");
                recipAddress.setStateProvince("IL");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setFloristNumber("1234567");
                
                item.setAddOns(addons);
                item.setSourceCode("4029");
                item.setProductId("FFCC");
                item.setSizeChoice("A");
                item.setExternalOrderNumber("23456");
                item.setProductsAmount("39.99");
                break;
            case 2: // Sunday delivery
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setFloristNumber("24-6173AA");
                item.setDeliveryDate("11/30/2003");
                break;
            case 3: // Codified product
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setFloristNumber("24-6173AA");
                item.setProductId("01DI");
                item.setDeliveryDate("11/28/2003");
                break;
            case 4: // SDFC florist delivery selected florist available
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("60515");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                //item.setFloristNumber("24-6173AA");
                item.setShipMethod("SD");
                item.setProductId("6103");
                break;
            case 5: // SDFC florist delivery selected florist not available
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("29222");
                recipAddress.setCountry("US");
                item.setFloristNumber("XXXX");
                item.setLineNumber("1");
                item.setShipMethod("SD");
                item.setProductId("6103");
                break;                                
            case 6: // SDFC carrier delivery selected florist available
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("60515");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setShipMethod("ND");
                item.setProductId("6103");
                break;
            case 7: // SDFC carrier delivery selected florist not available
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("29222");
                recipAddress.setCountry("US");

                item.setLineNumber("1");
                item.setShipMethod("ND");
                //item.setProductId("6103");
                item.setShipMethodCarrierFlag("Y");
                item.setShipMethodFloristFlag("N");
                item.setProductId("PDB4");
                break;
            case 8: // SDG florist delivery selected florist not available
                order.setOrderOrigin("amzni");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("29222");
                recipAddress.setCountry("US");
                item.setFloristNumber("XXXX");
                item.setLineNumber("1");
                item.setShipMethod("SD");
                item.setProductId("6104");
                break;                                                                                
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateFloristTest validateFloristTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateFloristTest = new ValidateFloristTest(args[i]);
                validateFloristTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}