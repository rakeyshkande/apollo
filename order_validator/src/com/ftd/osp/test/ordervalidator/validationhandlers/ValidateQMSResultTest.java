package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateQMSResult;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.*;

public class ValidateQMSResultTest extends TestCase 
{
    public ValidateQMSResultTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateQMSResultTest");
        
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultAddressInvalid"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultCityStateInvalid"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultZipInvalid"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultCityInvalid"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultQMSError"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultZipChanged"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultAddressChanged"));
        suite.addTest(new ValidateQMSResultTest("testValidateQMSResultValid"));

        return suite;
    }

    public void testValidateQMSResultValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateQMSResultAddressChanged()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            ////this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_ADDRESS_CHANGED + "1"), ValidationConstants.RESPONSE_QMS_ADDRESS_CHANGED);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateQMSResultZipChanged()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            ////this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_ZIP_CHANGED + "1"), ValidationConstants.RESPONSE_QMS_ZIP_CHANGED);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateQMSResultQMSError()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            ////this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_ERROR + "1"), ValidationConstants.RESPONSE_QMS_ERROR);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateQMSResultCityInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_CITY_INVALID + "1"), ValidationConstants.RESPONSE_QMS_CITY_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    
    
    public void testValidateQMSResultZipInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_ZIP_INVALID + "1"), ValidationConstants.RESPONSE_QMS_ZIP_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateQMSResultCityStateInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_CITY_STATE_INVALID + "1"), ValidationConstants.RESPONSE_QMS_CITY_STATE_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateQMSResultAddressInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateQMSResult.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_QMS_ADDRESS_INVALID + "1"), ValidationConstants.RESPONSE_QMS_ADDRESS_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List addresses = new ArrayList();
        
        OrderDetailsVO item = null;
        AVSAddressVO avsAddress = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO address = null;

        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // Address invalid
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;
            case 1: // City/State invalid
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;
            case 2: // Zip invalid
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;                
            case 3: // City invalid
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;                
            case 4: // QMS error
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;                
            case 5: // Zip changed
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;
            case 6: // Address changed
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;
            case 7: // Address valid
                item.setLineNumber("1");

                avsAddress = new AVSAddressVO();

                item.setAvsAddress(avsAddress);

                recipient = new RecipientsVO();
                recipients.add(recipient);
                address = new RecipientAddressesVO();
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                item.setRecipients(recipients);            
                address.setCountry("US");
                break;                                
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateQMSResultTest validateQMSResultTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateQMSResultTest = new ValidateQMSResultTest(args[i]);
                validateQMSResultTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}