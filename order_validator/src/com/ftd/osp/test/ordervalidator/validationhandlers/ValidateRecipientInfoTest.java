package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateRecipientInfo;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.*;

public class ValidateRecipientInfoTest extends TestCase 
{
    public ValidateRecipientInfoTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateRecipientInfoTest");
        
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoLength"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoMissing"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoInvalid"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoInvalidProdForINT"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoInvalidProdForDOM"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoSurcharge"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoInvalidCountry"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientInfoAddressTooLong"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientJCPSourceCode"));
        suite.addTest(new ValidateRecipientInfoTest("testValidateRecipientPOBox"));

        return suite;
    }

    public void testValidateRecipientPOBox()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(9));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            // Check to make sure no errors exist to start
            this.assertTrue(response.size() == 2);

            // Test a normal address
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressLine2("");            
            response = ValidateRecipientInfo.validate(request);
            this.assertTrue(response.size() == 1);
            
            // Test floral delivery
            ((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).setShipMethod("");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setAddressLine2("P.O. Box 9000");
            response = ValidateRecipientInfo.validate(request);
            this.assertTrue(response.size() == 1);

            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientJCPSourceCode()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(8));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            // Check to make sure no errors exist to start
            this.assertTrue(response.size() == 1);

            // Check for an error with the zip missing on a domestic order
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("US");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setInternational("N");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setPostalCode("");
            response = ValidateRecipientInfo.validate(request);
            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_ZIP_MISSING + "1"));
            
            // Check for an error with the zip missing on an international order
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setCountry("FR");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setInternational("Y");
            ((RecipientAddressesVO)((RecipientsVO)((OrderDetailsVO)((OrderVO)request.get(ValidationConstants.ORDER)).getOrderDetail().get(0)).getRecipients().get(0)).getRecipientAddresses().get(0)).setPostalCode("");
            response = ValidateRecipientInfo.validate(request);
            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_ZIP_MISSING + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoAddressTooLong()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateRecipientInfoInvalidCountry()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_COUNTRY_INVALID + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoSurcharge()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_STATE_AK_HI + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoInvalidProdForDOM()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_DOMESTIC_PROD_INT + "1"), ValidationConstants.RESPONSE_RECIP_DOMESTIC_PROD_INT);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoInvalidProdForINT()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_INT_DOMESTIC_PROD + "1"), ValidationConstants.RESPONSE_RECIP_INT_DOMESTIC_PROD);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }        

    public void testValidateRecipientInfoInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 5);

            //this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_ONE + "1"), ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_ONE);
            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_RECIP_STATE_INVALID + "1"));
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_ZIP_INVALID + "1"), ValidationConstants.RESPONSE_RECIP_ZIP_INVALID);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_DEST_NOT_VALID + "1"), ValidationConstants.RESPONSE_RECIP_DEST_NOT_VALID);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID + "1"), ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoMissing()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 4);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING + "1"), ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_DEST_MISSING + "1"), ValidationConstants.RESPONSE_RECIP_DEST_MISSING);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateRecipientInfoLength()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateRecipientInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 11);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_CITY_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_CITY_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_STATE_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_STATE_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_FIRST_NAME_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_FIRST_NAME_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_LAST_NAME_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_LAST_NAME_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_DEST_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_DEST_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG + "1"), ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List addresses = new ArrayList();
        List recipPhones = new ArrayList();
        //List destinations = new ArrayList();
        
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO address = null;
        RecipientPhonesVO recipPhone = null;
        //DestinationsVO dest = null;

        switch (code)
        {           
            case 0: // Length checks
                item = new OrderDetailsVO();
                item.setLineNumber("1");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfdslkjfsldkfjlsdkfjsldkfjlsdkfjlsdkjflsdkjfsdlkjfsldk");
                recipient.setLastName("ldkfjdslkfjdslkfjldskfjlsdkjfldskjflsdkjflkdsjflkdjflskdjflsdkjflsdk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("lksdjflksdjflksdjflksdjflsdkjflskdjflskdjflskdjflskdjflskdjflkdsjflksdjflkdsjflskdjsdkljfslddslj");
                address.setCity("kjflsdkjfldskjfdslkjfsldkjflsdkfjldskjflsdkjfldskjflsdkjfdslkjdsflk");
                address.setStateProvince("kjsfdldkjfsldkfjlsdkfjlsdkfjlsdkjflsdkjflskdjflsdkjflsdkjflsdkjflsdkjflskj");
                address.setPostalCode("dkljfdsfgfdgfdgdflkfjdslkjflsdkjflsdkjflsdkfjlskdjfdslkj");
                address.setCountry("US");
                address.setAddressType("HOME");
                address.setName("lkfjdslfkjsdlkfjsldkfjlsdkjflskdjflsdkjflksdjflksdjlkfsjdlkjfsdlkfjslkj");
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);
                
                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("12345678901");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);

                recipients.add(recipient);
                item.setRecipients(recipients);
                break;
            case 1: // Missing data checks
                item = new OrderDetailsVO();
                item.setLineNumber("1");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setCity("kjflsdkjfldskjfdslk");
                address.setStateProvince("MI");
                address.setPostalCode("48307");
                address.setCountry("US");
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            case 2: // Invalid checks
                item = new OrderDetailsVO();
                item.setLineNumber("1");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("P.O. Box 123");
                address.setCity("kjflsdkjfldskjfdslk");
                address.setStateProvince("Mich");
                address.setPostalCode("XOXOX");
                address.setCountry("US");
                address.setAddressType("H");
                address.setName("dlkfdlskjdslj");                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("gggggggggg");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            case 3: // DOM prod INT recip
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("8212");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("123 test st");
                address.setCity("Paris");
                address.setStateProvince("Paris");
                address.setPostalCode("12345");
                address.setCountry("FR");
                address.setAddressType("HOME");
                address.setName("");                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("123456789000");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            case 4: // INT prod DOM recip
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("7920");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("123 test st");
                address.setCity("Chicago");
                address.setStateProvince("IL");
                address.setPostalCode("60606");
                address.setCountry("US");
                address.setAddressType("HOME");
                address.setName("");                                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);
                break;
            case 5: // State surcharge
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("9153");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("123 test st");
                address.setCity("Blah");
                address.setStateProvince("HI");
                address.setPostalCode("86818");
                address.setCountry("US");
                address.setAddressType("HOME");
                address.setName("");                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);
                break;
            case 6: // Invalid country
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("8212");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("123 test st");
                address.setCity("Chicago");
                address.setStateProvince("IL");
                address.setPostalCode("12345");
                address.setCountry("USA");
                address.setInternational("N");
                address.setAddressType("HOME");
                address.setName("");                                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("123456789000");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            case 7: // Address too long
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("8212");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("1234567890123456789012345678901234567890123456789012345678901234567890");
                address.setCity("Chicago");
                address.setStateProvince("IL");
                address.setPostalCode("12345");
                address.setCountry("US");
                address.setInternational("N");
                address.setAddressType("HOME");
                address.setName("");                                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            case 8: // JCP source code
                order.setSourceCode("196h");
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("8212");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("345678901234567890");
                address.setCity("Chicago");
                address.setStateProvince("IL");
                address.setPostalCode("12345");
                address.setCountry("US");
                address.setInternational("N");
                address.setAddressType("HOME");
                address.setName("");                                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;                
            case 9: // PO Box
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductId("1234");
                item.setShipMethod("ND");

                recipient = new RecipientsVO();
                recipient.setFirstName("kfjsdlkfjdslkjfds");
                recipient.setLastName("ldkfjdslkfjdslk");
                
                address = new RecipientAddressesVO();
                address.setAddressLine1("123 Test Street");
                address.setAddressLine2("P.O. Box 9000");
                address.setCity("Chicago");
                address.setStateProvince("IL");
                address.setPostalCode("60641");
                address.setCountry("US");
                address.setInternational("N");
                address.setAddressType("HOME");
                address.setName("");                                
                addresses.add(address);
                recipient.setRecipientAddresses(addresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("1234567890");
                recipPhone.setPhoneType("Home");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                recipients.add(recipient);
                item.setRecipients(recipients);                                
                break;
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateRecipientInfoTest validateRecipientInfoTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateRecipientInfoTest = new ValidateRecipientInfoTest(args[i]);
                validateRecipientInfoTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}