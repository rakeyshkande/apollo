package com.ftd.osp.test.ordervalidator.validationhandlers;

import com.ftd.osp.utilities.ConfigurationUtil;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateOtherItemFields;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.*;
import org.xml.sax.SAXException;

public class ValidateOtherItemFieldsTest extends TestCase 
{
    private final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    public ValidateOtherItemFieldsTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateOtherItemFieldsTest");
        
        //suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsGiftMsgMax"));
       // suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsSpecialInstMax"));
        //suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsSignatureMax"));
       // suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsOccasionMissing"));
       // suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsAddonInvalid"));
        suite.addTest(new ValidateOtherItemFieldsTest("testValidateOtherItemFieldsMaxQuantity"));

        return suite;
    }

    public void testValidateOtherItemFieldsAddonInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateOtherItemFields.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_ADDON_INVALID + "1"), ValidationConstants.RESPONSE_ADDON_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateOtherItemFieldsOccasionMissing()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateOtherItemFields.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_OCCASION_MISSING + "1"), ValidationConstants.RESPONSE_OCCASION_MISSING);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateOtherItemFieldsGiftMsgMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateOtherItemFields.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN + "1"), ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateOtherItemFieldsSignatureMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateOtherItemFields.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SIGNATURE_EXCEEDS_MAX_LEN + "1"), ValidationConstants.RESPONSE_SIGNATURE_EXCEEDS_MAX_LEN);
        }
        catch(Exception e)
        {
            this.fail();
        }
    } 

    public void testValidateOtherItemFieldsSpecialInstMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateOtherItemFields.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SPEC_INSTR_EXCEEDS_MAX_LEN + "1"), ValidationConstants.RESPONSE_SPEC_INSTR_EXCEEDS_MAX_LEN);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    


public void testValidateOtherItemFieldsMaxQuantity() throws ParserConfigurationException, SAXException, IOException
{
       
  try
  {
    ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
    ValidationRequest request = new ValidationRequest();
    Connection con = TestHelper.getConnection();
    ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
    
    request.put(ValidationConstants.VALIDATION_DAU, vdau);
    request.put(ValidationConstants.CONNECTION, con);
    request.put(ValidationConstants.ORDER, getOrder(5));
    
    ValidationResponse response = ValidateOtherItemFields.validate(request);
    
    this.assertNotNull(response);
    this.assertEquals(response.get(ValidationConstants.RESPONSE_MAX_QUANTITY_EXCEEDED +  
                        configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "MAX_QUANTITY") ),
                                      ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_INVALID);
    
  }
  catch(Exception e)
  {
    this.fail();
  }
  
}

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        List addons = new ArrayList();

        AddOnsVO addon = null;
        OrderDetailsVO item = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // Gift message max length
                item.setLineNumber("1");
                item.setCardMessage("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
                item.setOccassionId("8");
                break;
            case 1: // Special instrucitons max length
                item.setLineNumber("1");
                item.setSpecialInstructions("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
                item.setOccassionId("1");
                break;
            case 2: // Signature max length
                item.setLineNumber("1");
                item.setCardSignature("123456789012345678901234567890123456789012345678");
                item.setOccassionId("8");
                break;
            case 3: // Occasion missing
                item.setLineNumber("1");                                
                break;
            case 4: // Add-on invalid
                item.setLineNumber("1");
                item.setOccassionId("8");
                addon = new AddOnsVO();
                addon.setAddOnCode("XXX");

                addons.add(addon);

                item.setAddOns(addons);
                
                break;
            case 5://order created to test 
                  //that the response sends back an error due to 
                  //item quantity > MAX_QUANTITY
              item.setLineNumber("1");
              item.setOccassionId("8");
              item.setCardSignature("123456789012345678901234567890123456789012345678");
              item.setCardMessage("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
              item.setSpecialInstructions("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
              order.setOrderOrigin("AMZNI");
              //set the quantity above the value contained in 
              //the metadata_config.xmlto ensrue a value is returned
              //in the response map.
              item.setQuantity("5");
              order.setSourceCode("2512");
            default:            
            break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateOtherItemFieldsTest validateOtherItemFieldsTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateOtherItemFieldsTest = new ValidateOtherItemFieldsTest(args[i]);
                validateOtherItemFieldsTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}