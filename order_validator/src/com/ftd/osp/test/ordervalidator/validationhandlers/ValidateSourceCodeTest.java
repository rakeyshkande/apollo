package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateSourceCode;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.*;
import com.ftd.osp.utilities.order.*;

public class ValidateSourceCodeTest extends TestCase 
{
    public ValidateSourceCodeTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateSourceCodeTest");
        suite.addTest(new ValidateSourceCodeTest("testValidateSourceCodeInvalid"));
        suite.addTest(new ValidateSourceCodeTest("testValidateSourceCodeValid"));
        suite.addTest(new ValidateSourceCodeTest("testValidateSourceCodeInactive"));
        suite.addTest(new ValidateSourceCodeTest("testValidateSourceCodeForCorrectPaymentType"));
        suite.addTest(new ValidateSourceCodeTest("testValidateSourceCodeForIncorrectPaymentType"));

        return suite;
    }

    public void testValidateSourceCodeForIncorrectPaymentType()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            this.assertTrue((response.size() == 2));

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD), ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateSourceCodeForCorrectPaymentType()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);            
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            // Check normal source code
            this.assertTrue((response.size() == 1));

            // Check an EDS source code with payment type of PC
            ((OrderVO)request.get(ValidationConstants.ORDER)).setSourceCode("4500");
            response = ValidateSourceCode.validate(request);
            this.assertTrue((response.size() == 1));
            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateSourceCodeInactive()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            this.assertTrue((response.size() == 3));

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SOURCE_CODE_EXPIRED), ValidationConstants.RESPONSE_SOURCE_CODE_EXPIRED);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateSourceCodeValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            this.assertTrue((response.size() == 1));                    
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateSourceCodeInvalid()
    {
        try
        {        
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SOURCE_CODE_INVALID), ValidationConstants.RESPONSE_SOURCE_CODE_INVALID);                    
        }
        catch(Exception e)
        {
            this.fail();
        }
    }


    public void testValidateCostCenterLookup()
    {
        try
        {        
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            ScrubMapperDAO dao = new ScrubMapperDAO(con);
            // Advo
            //OrderVO order = dao.mapOrderFromDB("FTD_GUID_-11391242670-1298526789024783104602016261820016190690410-15118951530223677150-78309312314698819200-104720683305415223240-618043203251891561686172-2021254141252-7833495371831131261429194");
            // Target
            OrderVO order = dao.mapOrderFromDB("FTD_GUID_-21363624510-110729247104694142640-5996050280-12989670650-10228912405027111730-145675493811038306810041170617207223113950-18796244982512114978899193-114547120910-1029214952210170532789121");
            request.put(ValidationConstants.ORDER, order);
            ValidationResponse response = ValidateSourceCode.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SOURCE_CODE_INVALID), ValidationConstants.RESPONSE_SOURCE_CODE_INVALID);                    
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
      
    }

    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List payments = new ArrayList();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        PaymentsVO payment = null;
        
        item = new OrderDetailsVO();


        switch (code)
        {
            case 0: // Invalid source code
                order.setSourceCode("JMP1");
                break;
            case 1: // Valid source code
                order.setSourceCode("350");
                break;          
            case 2: // Source code expired
                order.setSourceCode("5539");
                break;                
            case 3: // Validate for correct payment type
                order.setSourceCode("1012");
                payment = new PaymentsVO();
                payment.setPaymentsType("AX");
                payments.add(payment);
                order.setPayments(payments);
                break;     
            case 4: // Validate for incorrect payment type
                order.setSourceCode("1012");
                payment = new PaymentsVO();
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payments.add(payment);
                order.setPayments(payments);
                break;
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateSourceCodeTest validateSourceCodeTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateSourceCodeTest = new ValidateSourceCodeTest(args[i]);
                validateSourceCodeTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}