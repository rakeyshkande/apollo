package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateOrderAmount;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateOrderAmountTest extends TestCase 
{
    public ValidateOrderAmountTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateOrderAmountTest");
        
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountExceedsMax"));
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountIsZero"));
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountItemTotalZero"));
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountItemExceedsMax"));
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountItemExceedsMaxFuneral"));
        suite.addTest(new ValidateOrderAmountTest("testValidateOrderAmountItemExceedsMaxPrice"));

        return suite;
    }

    public void testValidateOrderAmountItemExceedsMaxPrice()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_ITEM_TOTAL_EXCEEDS_MAX));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderAmountItemExceedsMaxFuneral()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            // Should get fraud warning
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderAmountItemExceedsMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_ORDER_ITEMS_EXCEEDS_MAX), ValidationConstants.RESPONSE_ORDER_ITEMS_EXCEEDS_MAX);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderAmountItemTotalZero()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_ITEM_TOTAL_ZERO + "1"), ValidationConstants.RESPONSE_ITEM_TOTAL_ZERO);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderAmountIsZero()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_ORDER_TOTAL_ZERO), ValidationConstants.RESPONSE_ORDER_TOTAL_ZERO);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderAmountExceedsMax()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateOrderAmount.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertNotNull(response.get(ValidationConstants.RESPONSE_ORDER_TOTAL_EXCEEDS_MAX));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        

        switch (code)
        {           
            case 0: // Order amount exceeds max
                order.setOrderTotal("1000.00");            
                break;
            case 1: // Order amount is zero
                order.setOrderTotal("0.00");            
                break;                
            case 2: // Item total price zero
                order.setOrderTotal("100.00");
            
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductsAmount("0.00");

                items.add(item);                        
                break;
            case 3: // Item total exceeds max
                order.setOrderTotal("100.00");

                for (int i = 1; i <= 50; i++) 
                {
                    item = new OrderDetailsVO();
                    item.setLineNumber(String.valueOf(i));
                    item.setProductsAmount("1.00");

                    items.add(item);
                }
                break;
            case 4: // Item total exceeds max funeral occasion
                order.setOrderTotal("100.00");

                for (int i = 1; i <= 50; i++) 
                {
                    item = new OrderDetailsVO();
                    item.setLineNumber(String.valueOf(i));
                    item.setProductsAmount("1.00");
                    item.setOccassionId("1");

                    items.add(item);
                }
                break;
            case 5: // Item total exceeds max funeral occasion
                order.setOrderTotal("200.00");

                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setProductsAmount("200.00");
                item.setOccassionId("1");
                items.add(item);
                break;                                                
            default:            
                break;
        }

        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateOrderAmountTest validateOrderAmountTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateOrderAmountTest = new ValidateOrderAmountTest(args[i]);
                validateOrderAmountTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}