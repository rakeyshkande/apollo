package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateCustomerInfo;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateCustomerInfoTest extends TestCase 
{
    public ValidateCustomerInfoTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateCustomerInfoTest");
        
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoUSCAN"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoINT"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoLengths"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoPhonesInvalid"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoZipInvalid"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoCountryInvalid"));
        suite.addTest(new ValidateCustomerInfoTest("testValidateCustomerInfoEmail"));
    
        return suite;
    }

    public void testValidateCustomerInfoEmail()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_EMAIL_INVALID), ValidationConstants.RESPONSE_BUYER_EMAIL_INVALID);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateCustomerInfoCountryInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_COUNTRY_INVALID), ValidationConstants.RESPONSE_BUYER_COUNTRY_INVALID);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCustomerInfoZipInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);

            // US Zip code
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_ZIP_INVALID), ValidationConstants.RESPONSE_BUYER_ZIP_INVALID);
            this.assertTrue(response.size() == 2);

            // CA Zip code
            request.put(ValidationConstants.ORDER, getOrder(5));
            response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_ZIP_INVALID), ValidationConstants.RESPONSE_BUYER_ZIP_INVALID);
            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCustomerInfoPhonesInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID), ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID), ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID);
            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCustomerInfoUSCAN()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 9);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCustomerInfoINT()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 8);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCustomerInfoLengths()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateCustomerInfo.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG), ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_FIRST_NAME_TOO_LONG), ValidationConstants.RESPONSE_BUYER_FIRST_NAME_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_LAST_NAME_TOO_LONG), ValidationConstants.RESPONSE_BUYER_LAST_NAME_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_CITY_TOO_LONG), ValidationConstants.RESPONSE_BUYER_CITY_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_STATE_TOO_LONG), ValidationConstants.RESPONSE_BUYER_STATE_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_COUNTY_TOO_LONG), ValidationConstants.RESPONSE_BUYER_COUNTY_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_EMAIL_TOO_LONG), ValidationConstants.RESPONSE_BUYER_EMAIL_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_TOO_LONG), ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_TOO_LONG);
            this.assertEquals(response.get(ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG), ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }        

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List buyers = new ArrayList();
        List buyerPhones = new ArrayList();
        List buyerEmails = new ArrayList();
        List buyerAddresses = new ArrayList();
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        BuyerPhonesVO phoneNumber = null;
        BuyerPhonesVO homePhoneNumber = null;
        BuyerPhonesVO workPhoneNumber = null;
        BuyerEmailsVO email = null;

        switch (code)
        {
            case 0: // Buyer US/CAN
                buyer = new BuyerVO();
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;         
            case 1: // Buyer INT
                buyer = new BuyerVO();
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setCountry("AL");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;                
            case 2: // Buyer lengths
                buyer = new BuyerVO();
                buyer.setFirstName("Averylongfirstnamethatdoesnotpassthetest");
                buyer.setLastName("Averylonglastnamethatdoesnotpassthetest");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("The Number One Apartments Number 1008");
                buyerAddress.setCity("Averylongcitynamethatshouldnotfit");
                buyerAddress.setStateProv("Averylongstatenamethatshouldnotfit");
                buyerAddress.setCounty("Averylongcountynamethatdoesnotfit");
                buyerAddress.setPostalCode("1111122222");
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                email = new BuyerEmailsVO();
                email.setEmail("averylongemailaddress@averylongdomainthatdoesnotexist.com");
                buyerEmails.add(email);
                buyer.setBuyerEmails(buyerEmails);

                phoneNumber = new BuyerPhonesVO();
                phoneNumber.setPhoneNumber("12345678900987654321");
                phoneNumber.setPhoneType("Home");
                buyerPhones.add(phoneNumber);
                buyer.setBuyerPhones(buyerPhones);
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;
            case 3: // Buyer phones invalid
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("");
                buyerAddress.setCity("Chicago");
                buyerAddress.setStateProv("IL");
                buyerAddress.setCounty("test county");
                buyerAddress.setPostalCode("60606");
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                homePhoneNumber = new BuyerPhonesVO();
                homePhoneNumber.setPhoneNumber("12345qwert");
                homePhoneNumber.setPhoneType("Home");
                buyerPhones.add(homePhoneNumber);

                workPhoneNumber = new BuyerPhonesVO();
                workPhoneNumber.setPhoneNumber("12345qwert");
                workPhoneNumber.setPhoneType("Work");
                buyerPhones.add(workPhoneNumber);
                
                buyer.setBuyerPhones(buyerPhones);
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;                 
            case 4: // Buyer zip invalid (US)
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("");
                buyerAddress.setCity("Denver");
                buyerAddress.setStateProv("CO");
                buyerAddress.setCounty("test county");
                buyerAddress.setPostalCode("AAAAA");
                buyerAddress.setCountry("US");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                phoneNumber = new BuyerPhonesVO();
                phoneNumber.setPhoneNumber("1234567890");
                phoneNumber.setPhoneType("Home");
                buyerPhones.add(phoneNumber);
                buyer.setBuyerPhones(buyerPhones);                
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;
            case 5: // Buyer zip invalid (CA)
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("");
                buyerAddress.setCity("Toronto");
                buyerAddress.setStateProv("ON");
                buyerAddress.setCounty("test county");
                buyerAddress.setPostalCode("123LLL");
                buyerAddress.setCountry("CA");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                phoneNumber = new BuyerPhonesVO();
                phoneNumber.setPhoneNumber("1234567890");
                phoneNumber.setPhoneType("Home");
                buyerPhones.add(phoneNumber);
                buyer.setBuyerPhones(buyerPhones);                
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;
            case 6: // Buyer country invalid
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("");
                buyerAddress.setCity("Toronto");
                buyerAddress.setStateProv("ON");
                buyerAddress.setCounty("test county");
                buyerAddress.setPostalCode("60606");
                buyerAddress.setCountry("II");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                phoneNumber = new BuyerPhonesVO();
                phoneNumber.setPhoneNumber("1234567890");
                phoneNumber.setPhoneType("Home");
                buyerPhones.add(phoneNumber);
                buyer.setBuyerPhones(buyerPhones);                
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;                                                
            case 7: // Buyer email invalid
                buyer = new BuyerVO();
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1234 Rocky Mountain Avenue");
                buyerAddress.setAddressLine2("");
                buyerAddress.setCity("Toronto");
                buyerAddress.setStateProv("ON");
                buyerAddress.setCounty("test county");
                buyerAddress.setPostalCode("L5P1C4");
                buyerAddress.setCountry("CA");
                buyerAddresses.add(buyerAddress);
                buyer.setBuyerAddresses(buyerAddresses);

                phoneNumber = new BuyerPhonesVO();
                phoneNumber.setPhoneNumber("1234567890");
                phoneNumber.setPhoneType("Home");
                buyerPhones.add(phoneNumber);
                buyer.setBuyerPhones(buyerPhones);   

                email = new BuyerEmailsVO();
                email.setEmail("mymailatftdi.com");
                buyerEmails.add(email);
                buyer.setBuyerEmails(buyerEmails);                
                
                buyers.add(buyer);
                order.setBuyer(buyers);
                break;                
            default:            
                break;
        }

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateCustomerInfoTest validateCustomerInfoTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateCustomerInfoTest = new ValidateCustomerInfoTest(args[i]);
                validateCustomerInfoTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}