package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateMembershipId;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateMembershipIdTest extends TestCase 
{
    public ValidateMembershipIdTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateMembershipIdTest");
        
        suite.addTest(new ValidateMembershipIdTest("testValidateMembershipIdAAAValid"));
        suite.addTest(new ValidateMembershipIdTest("testValidateMembershipIdAAAInvalid"));
        suite.addTest(new ValidateMembershipIdTest("testValidateMembershipIdDELTValid"));
        suite.addTest(new ValidateMembershipIdTest("testValidateMembershipIdDELTInvalid"));
        suite.addTest(new ValidateMembershipIdTest("testValidateMembershipIdMissing"));

        return suite;
    }

    public void testValidateMembershipIdMissing()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateMembershipId.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateMembershipIdAAAValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateMembershipId.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    
    
    public void testValidateMembershipIdAAAInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateMembershipId.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID), ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateMembershipIdDELTValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateMembershipId.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateMembershipIdDELTInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateMembershipId.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID), ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    
    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List memberships = new ArrayList();
        
        MembershipsVO membership = null;

        switch (code)
        {           
            case 0: // AAA Valid
                order.setSourceCode("6520");

                membership = new MembershipsVO();
                membership.setMembershipIdNumber("415047");
                memberships.add(membership);

                order.setMemberships(memberships);                                
                break;
            case 1: // AAA Invalid
                order.setSourceCode("4517");

                membership = new MembershipsVO();
                membership.setMembershipIdNumber("111111");
                memberships.add(membership);

                order.setMemberships(memberships);                                
                break;
            case 2: // DELT Valid
                order.setSourceCode("2136");

                membership = new MembershipsVO();
                membership.setMembershipIdNumber("2228366353");
                memberships.add(membership);

                order.setMemberships(memberships);                                
                break;                
            case 3: // DELT Invalid
                order.setSourceCode("2136");

                membership = new MembershipsVO();
                membership.setMembershipIdNumber("1111111111");
                memberships.add(membership);

                order.setMemberships(memberships);                                
                break;
            case 4: // Missing
                order.setSourceCode("2136");

                membership = new MembershipsVO();
                membership.setMembershipIdNumber("");
                memberships.add(membership);

                order.setMemberships(memberships);                                
                break;                                                
            default:
                break;
        }

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateMembershipIdTest validateMembershipIdTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateMembershipIdTest = new ValidateMembershipIdTest(args[i]);
                validateMembershipIdTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}