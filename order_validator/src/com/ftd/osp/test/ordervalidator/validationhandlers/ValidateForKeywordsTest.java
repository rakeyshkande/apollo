package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateForKeywords;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;

public class ValidateForKeywordsTest extends TestCase 
{
    public ValidateForKeywordsTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateForKeywordsTest");
        
        suite.addTest(new ValidateForKeywordsTest("testValidateForKeywordsFound"));
        suite.addTest(new ValidateForKeywordsTest("testValidateForKeywordsNotFound"));

        return suite;
    }

    public void testValidateForKeywordsFound()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateForKeywords.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_SPEC_INSTR_CONTAINS_KEYWORDS + "1"), ValidationConstants.RESPONSE_SPEC_INSTR_CONTAINS_KEYWORDS);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateForKeywordsNotFound()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateForKeywords.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0: // Find keyword
                item.setLineNumber("1");
                item.setOccassionId("1");
                item.setSpecialInstructions("$12.34");
                                
                break;
            case 1: // Keyword not found
                item.setLineNumber("1");
                item.setOccassionId("8");
                item.setSpecialInstructions("12.34");
                                
                break;                
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateForKeywordsTest validateForKeywordsTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateForKeywordsTest = new ValidateForKeywordsTest(args[i]);
                validateForKeywordsTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}