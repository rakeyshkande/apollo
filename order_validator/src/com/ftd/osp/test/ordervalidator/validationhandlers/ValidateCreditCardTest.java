package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidatePayment;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;


public class ValidateCreditCardTest extends TestCase 
{
    public ValidateCreditCardTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateCreditCardTest");
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardInfoRequired"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardNumberValid"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardNumberNotValid"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardTypeValid"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardTypeNotValid"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardTotal"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardDateInvalid"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardDateInvalid4DigitYear"));
        suite.addTest(new ValidateCreditCardTest("testValidateCreditCardNoCharge"));
        

        return suite;
    }

    public void testValidateCreditCardNoCharge()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(8));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCreditCardDateInvalid4DigitYear()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_CC_DATE_INVALID));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateCreditCardDateInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_CC_DATE_INVALID));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateCreditCardTotal()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_CC_INFO_MISSING), ValidationConstants.RESPONSE_CC_INFO_MISSING);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    
    
    public void testValidateCreditCardTypeNotValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_CC_INVALID_TYPE), ValidationConstants.RESPONSE_CC_INVALID_TYPE);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateCreditCardTypeValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue((response.size() == 1));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }        

    public void testValidateCreditCardNumberNotValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_CC_INVALID_NUMBER), ValidationConstants.RESPONSE_CC_INVALID_NUMBER);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateCreditCardNumberValid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertTrue((response.size() == 1));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateCreditCardInfoRequired()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidatePayment.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_CC_INFO_MISSING), ValidationConstants.RESPONSE_CC_INFO_MISSING);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List payments = new ArrayList();
        List creditCards = new ArrayList();
        List items = new ArrayList();
        OrderDetailsVO item = null;
        PaymentsVO payment = null;
        PaymentsVO gc = null;
        CreditCardsVO creditCard = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
            case 0:                
                break;     
            case 1: // Validate CC number valid
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;
            case 2: // Validate CC number invalid
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("1111111111111111");
                creditCard.setCCType("VI");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;                
            case 3: // Validate CC type valid
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;
            case 4: // Validate CC type invalid
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VX");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);
                payment.setPaymentsType("VX");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;                
            case 5: // Test that the credit card is present when a GC is used 
                    // and does not cover the total
                // GC
                gc = new PaymentsVO();
                gc.setPaymentMethodType("G");
                gc.setAmount("25.00");
                payments.add(gc);
                
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;
            case 6: // Validate CC date invalid
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCard.setCCExpiration("0104");
                creditCards.add(creditCard);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;
            case 7: // Validate CC date invalid four digit year
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCard.setCCExpiration("10/2003");
                creditCards.add(creditCard);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;
            case 8: // No charge
                payment = new PaymentsVO();
                creditCard = new CreditCardsVO();
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("NC");
                creditCard.setCCExpiration("01/04");
                creditCards.add(creditCard);
                payment.setPaymentsType("NC");
                payment.setPaymentMethodType("C");
                payment.setCreditCards(creditCards);
                payments.add(payment);
                order.setPayments(payments);
                order.setOrderTotal("69.99");
                break;                
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateCreditCardTest validateCreditCardTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateCreditCardTest = new ValidateCreditCardTest(args[i]);
                validateCreditCardTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}