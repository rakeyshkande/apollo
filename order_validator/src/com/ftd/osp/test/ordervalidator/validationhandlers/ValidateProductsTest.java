package com.ftd.osp.test.ordervalidator.validationhandlers;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.validationhandlers.ValidateProducts;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.*;

public class ValidateProductsTest extends TestCase 
{
    public ValidateProductsTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("ValidateProductsTest");
        
        /*
        suite.addTest(new ValidateProductsTest("testValidateProductsIdMissing"));
        suite.addTest(new ValidateProductsTest("testValidateProductsProductUnavail"));
        suite.addTest(new ValidateProductsTest("testValidateProductsProductInvalid")); // Not in DB
        suite.addTest(new ValidateProductsTest("testValidateProductsNoDropShipCA"));
        suite.addTest(new ValidateProductsTest("testValidateProductsNoSatDelivery"));
        suite.addTest(new ValidateProductsTest("testValidateProductsNoColorSelected"));
        suite.addTest(new ValidateProductsTest("testValidateProductsNoPriceSelected"));
        suite.addTest(new ValidateProductsTest("testValidateProductsColorInvalid"));
        suite.addTest(new ValidateProductsTest("testValidateProductsPriceInvalid"));
        suite.addTest(new ValidateProductsTest("testValidateProductsWithSubCodeAvail"));
        suite.addTest(new ValidateProductsTest("testValidateProductsWithSubCodeNotAvail"));
        */
        //suite.addTest(new ValidateProductsTest("testValidateProductsVariablePrice"));
        
        //following 4 test cases added for Amazon.com orders
       // suite.addTest(new ValidateProductsTest("testValidateProductsNoPriceVariance"));
       // suite.addTest(new ValidateProductsTest("testValidateProductsPriceVariance01"));
        suite.addTest(new ValidateProductsTest("testValidateProductsPriceVariance"));
  /*      suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodCarrierSameDay"));
        suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodCarrierFreshCut"));
        suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodFloristSameDay"));
        suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodFloristFreshcut"));
        suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodDropShipOnly"));
        suite.addTest(new ValidateProductsTest("testValidateProductsValidShipMethodFlorist"));
        suite.addTest(new ValidateProductsTest("testValidateProductsInvalidShipMethodCarrier"));
        suite.addTest(new ValidateProductsTest("testValidateProductsInvalidShipMethodFlorist"));
*/
        return suite;
    }

    public void testValidateProductsVariablePrice()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(11));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_PROD_PRICE_VARIABLE_INVALID + "1"));

        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateProductsWithSubCodeNotAvail()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(10));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_PROD_UNAVAILABLE + "1"));

        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateProductsWithSubCodeAvail()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(9));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateProductsPriceInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(8));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_PRICE_POINT_INVALID + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateProductsColorInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(7));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertTrue(response.containsKey(ValidationConstants.RESPONSE_COLOR_INVALID + "1"));
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateProductsNoPriceSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(6));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_PRICE_SELECTED + "1"), ValidationConstants.RESPONSE_NO_PRICE_SELECTED);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    


    public void testValidateProductsNoColorSelected()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(5));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 4);

            this.assertTrue(response.get(ValidationConstants.RESPONSE_NO_COLOR_SELECTED + "1") != null);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateProductsNoSatDelivery()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(4));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_SATURDAY_DELIVERY + "1"), ValidationConstants.RESPONSE_NO_SATURDAY_DELIVERY);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateProductsNoDropShipCA()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(3));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_NO_DROP_SHIP_CANADA + "1"), ValidationConstants.RESPONSE_NO_DROP_SHIP_CANADA);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateProductsProductInvalid()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(2));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 4);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_PROD_INVALID + "1"), ValidationConstants.RESPONSE_PROD_INVALID);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateProductsProductUnavail()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(1));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 3);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_PROD_UNAVAILABLE + "1"), ValidationConstants.RESPONSE_PROD_UNAVAILABLE);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }    

    public void testValidateProductsIdMissing()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(0));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 5);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_PROD_INFO_MISSING + "1"), ValidationConstants.RESPONSE_PROD_INFO_MISSING);
        }
        catch(Exception e)
        {
            this.fail();
        }
    }   
    
    public void testValidateProductsNoPriceVariance()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(12));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsPriceVariance01()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(13));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsPriceVariance()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(14));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_EXCEEDS_PRICE_VARIANCE_MAX + "1"), ValidationConstants.RESPONSE_EXCEEDS_PRICE_VARIANCE_MAX);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsInvalidShipMethodFlorist()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(21));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_INVALID_SHIP_METHOD + "1"), ValidationConstants.RESPONSE_INVALID_SHIP_METHOD);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    public void testValidateProductsInvalidShipMethodCarrier()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(22));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertEquals(response.get(ValidationConstants.RESPONSE_INVALID_SHIP_METHOD + "1"), ValidationConstants.RESPONSE_INVALID_SHIP_METHOD);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodFlorist()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(15));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodDropShipOnly()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(20));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodFloristFreshcut()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(16));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodFloristSameDay()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(17));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodCarrierFreshCut()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(18));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 2);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateProductsValidShipMethodCarrierSameDay()
    {
        try
        {
            ValidationRequest request = new ValidationRequest();
            Connection con = TestHelper.getConnection();
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
            request.put(ValidationConstants.VALIDATION_DAU, vdau);
            request.put(ValidationConstants.CONNECTION, con);
            request.put(ValidationConstants.ORDER, getOrder(19));
            ValidationResponse response = ValidateProducts.validate(request);

            this.assertNotNull(response);

            this.assertTrue(response.size() == 1);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List recipAddresses = new ArrayList();
        
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        item = new OrderDetailsVO();

        switch (code)
        {           
           case 0: // Product ID missing
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");                                                
                break;
            case 1: // Product unavailable
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("9113");
                item.setProductsAmount("49.99");                                
                break;
            case 2: // Product invalid
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("AAAA");
                item.setProductsAmount("49.99");                                
                break;
            case 3: // No drop ship to Canada
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("2001");
                item.setProductsAmount("49.99");
                item.setShipMethod("GR");

                recipient = new RecipientsVO();

                recipAddress = new RecipientAddressesVO();
                recipAddress.setCountry("CA");
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                recipients.add(recipient);

                item.setRecipients(recipients);
                break;                
            case 4: // No Saturday delivery
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("2049");
                item.setProductsAmount("34.99");
                item.setShipMethod("SA");
                break;
            case 5: // No color selected
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("8212");
                item.setProductsAmount("59.99");
                break;
            case 6: // No price selected
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("8212");
                item.setColorFirstChoice("A");
                item.setColorSecondChoice("B");
                item.setSizeChoice("A");
                break;
            case 7: // Color invalid
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("8212");
                item.setColorFirstChoice("A");
                item.setColorSecondChoice("RED");
                item.setSizeChoice("A");
                item.setProductsAmount("59.99");
                break;
            case 8: // Price invalid
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("8212");
                item.setColorFirstChoice("A");
                item.setColorSecondChoice("B");
                item.setSizeChoice("A");
                item.setProductsAmount("29.99");
                break;
            case 9: // Subcode available
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("FF11");
                item.setProductSubCodeId("F111");
                item.setSizeChoice("A");
                item.setProductsAmount("64.99");
                break;                                                                                
            case 10: // Subcode not available
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("9200");
                item.setProductSubCodeId("9201");
                item.setSizeChoice("A");
                item.setProductsAmount("85.99");
                break;                                                                                                
            case 11: // Subcode not available
                order.setOrderOrigin("FOX");
                item.setLineNumber("1");
                item.setProductId("7031");
                item.setSizeChoice("A");
                item.setProductsAmount("52.99");
                break; 
            case 12: // No Price Variance
                order.setOrderOrigin("AMZNI");
                item.setLineNumber("1");
                item.setProductId("7031");
                item.setSizeChoice("A");
                item.setProductsAmount("59.99");
                item.setPdbPrice("59.99");
                item.setShipMethod("2F");
                item.setSourceCode("7800");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                break; 
           case 13: // Price Variance of $0.01
                order.setOrderOrigin("AMZNI");
                item.setLineNumber("1");
                item.setProductId("7031");
                item.setSizeChoice("A");
                item.setProductsAmount("60.00");
                item.setPdbPrice("59.99");
                item.setShipMethod("2F");
                item.setSourceCode("7800");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                break; 
            case 14: // Price Variance > $0.01
                order.setOrderOrigin("AMZNI");
                item.setLineNumber("1");
                item.setProductId("3068"); //7031
                item.setSizeChoice("A");
                item.setProductsAmount("78.00");
                item.setPdbPrice("46.99");
                item.setShipMethod("");
                item.setSourceCode("7800");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z0000000042");
                break; 
            case 15: // Florist only delivered product
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of FLORAL
                item.setLineNumber("1");
                item.setProductId("3443");
                item.setSizeChoice("A");
                item.setProductsAmount("119.99");
                item.setPdbPrice("119.99");
                item.setShipMethod("");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 16: // Same day freshcut delivered by florist
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDFC
                item.setLineNumber("1");
                item.setProductId("F039");
                item.setSizeChoice("A");
                item.setProductsAmount("38.99");
                item.setPdbPrice("38.99");
                item.setShipMethod("SD");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 17: // Same day gift delivered by Florist
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                item.setLineNumber("1");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDG
                item.setProductId("0BGG");
                item.setSizeChoice("A");
                item.setProductsAmount("32.99");
                item.setPdbPrice("32.99");
                item.setShipMethod("SD");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 18: // Same day freshcut delivered by carrier
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDFC
                item.setProductId("PDB4");
                item.setLineNumber("1");
                item.setSizeChoice("A");
                item.setProductsAmount("49.99");
                item.setPdbPrice("49.99");
                item.setShipMethod("2F");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 19: // Same day gift delivered by carrier
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                item.setLineNumber("1");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SDG
                item.setProductId("0BGB");
                item.setSizeChoice("A");
                item.setProductsAmount("32.99");
                item.setPdbPrice("32.99");
                item.setShipMethod("2F");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 20: // Drop Ship only product
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                item.setLineNumber("1");
                //used becuase PRODUCT_TYPE in the PRODUCT_MASTER
                //table as a value of SPEGFT
                item.setProductId("G043");
                item.setSizeChoice("A");
                item.setProductsAmount("41.99");
                item.setPdbPrice("41.99");
                item.setShipMethod("2F");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 21: // Invalid ship method for Florist delivered order
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                item.setLineNumber("1");
                item.setProductId("0BGG");
                item.setSizeChoice("A");
                item.setProductsAmount("32.99");
                item.setPdbPrice("32.99");
                item.setShipMethod("SA");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
              case 22: // Invalid ship method for Carrier delivered order
                order.setOrderOrigin("AMZNI");
                order.setSourceCode("7800");
                item.setLineNumber("1");
                item.setProductId("MK16");
                item.setSizeChoice("A");
                item.setProductsAmount("29.99");
                item.setPdbPrice("29.99");
                item.setShipMethod("SA");
                recipient = new RecipientsVO();
                recipients.add(recipient);
                recipAddress = new RecipientAddressesVO();
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);
                item.setRecipients(recipients);            
                recipAddress.setPostalCode("48307");
                recipAddress.setStateProvince("MI");
                recipAddress.setCountry("US");
                item.setExternalOrderNumber("Z00001");
                item.setSourceCode("7800");
                break; 
            default:            
                break;
        }

        items.add(item);
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            ValidateProductsTest validateProductsTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                validateProductsTest = new ValidateProductsTest(args[i]);
                validateProductsTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}