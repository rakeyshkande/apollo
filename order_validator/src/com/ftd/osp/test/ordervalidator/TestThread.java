package com.ftd.osp.test.ordervalidator;
import java.util.*;
import java.sql.Connection;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.OrderValidatorBO;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.GUID.*;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.framework.dispatcher.*;


public class TestThread extends Thread
{
  ArrayList guidList = null;
  String name = null;
  boolean done = false;

  public TestThread(ArrayList guidList, String name)
  {
      this.guidList = guidList;
      this.name = name;
  }

  public void run()
  {
      Iterator it = null;
      MessageToken mt = null;
      Connection con = null;
      OrderValidatorBO bo = null;
        BusinessConfig config = null;
        MessageToken token = null;
      try
        {
            long start = 0;
            long end = 0;

            for(int i = 0; i < this.guidList.size(); i++)
            {
              config = new BusinessConfig();
              
              con = TestHelper.getConnection();
              config.setConnection(con);

              token = new MessageToken();
              token.setMessage((String)this.guidList.get(i));
              config.setInboundMessageToken(token);

              bo = new OrderValidatorBO();
      
              start = System.currentTimeMillis();
              bo.execute(config);
              end = System.currentTimeMillis();
              System.out.println("VALIDATION thread_name=" + this.name + ", start=" + start + ", end=" + end + ", total validation processign time=" + (end - start));
            }
        }
        catch(Exception e)
        {
           System.out.println(e.toString());
           done = true;
        }

        done = true;
  }

  public boolean isFinished()
  {
    return this.done;
  }

}