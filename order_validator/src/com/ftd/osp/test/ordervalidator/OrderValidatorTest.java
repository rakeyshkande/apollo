package com.ftd.osp.test.ordervalidator;

import junit.framework.*;
import java.sql.Connection;
import java.util.*;

import org.w3c.dom.Document;

import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.ordervalidator.OrderValidator;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.utilities.order.*;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.*;

public class OrderValidatorTest extends TestCase 
{
    private String guid = null;
    private static final String TEST_SETUP_FILE_NAME = "test_setup.xml"; 
    
    public OrderValidatorTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("OrderValidatorTest");
        suite.addTest(new OrderValidatorTest("testValidateOrderAuto"));
        //suite.addTest(new OrderValidatorTest("testValidateOrderScrubGUID"));
        //suite.addTest(new OrderValidatorTest("testValidateOrderScrub"));
       // suite.addTest(new OrderValidatorTest("testExecuteFromSetup"));
       // suite.addTest(new OrderValidatorTest("testValidateOrderAmazon"));

        return suite;
    }

    public void testExecuteFromSetup()
    {
        try
        {            
            this.assertNotNull(this.guid);
            Connection con = TestHelper.getConnection();
            ScrubMapperDAO dao = new ScrubMapperDAO(con);
            OrderVO order = dao.mapOrderFromDB(guid);

            OrderValidator validator = new OrderValidator();
            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_AUTO, con);

            this.assertNotNull(vOrder);

            ValidationXAO xao = new ValidationXAO();
            Document doc = xao.generateValidationXML(vOrder);
            DOMUtil.print(doc, System.out);            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderScrubGUID()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = TestHelper.getScrubOrder(con);

            OrderValidator validator = new OrderValidator();
            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, con);

            this.assertNotNull(vOrder);

            ValidationXAO xao = new ValidationXAO();
            Document doc = xao.generateValidationXML(vOrder);
            DOMUtil.print(doc,System.out);            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }


    public void testValidateOrderAuto()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(1);

            OrderValidator validator = new OrderValidator();
            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_AUTO, con);

            this.assertNotNull(vOrder);

            ValidationXAO xao = new ValidationXAO();
            Document doc = xao.generateValidationXML(vOrder);
            DOMUtil.print(doc, System.out);            
        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public void testValidateOrderScrub()
    {
        try
        {
            Connection con = TestHelper.getConnection();
            OrderVO order = getOrder(1);

            OrderValidator validator = new OrderValidator();
            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, con);

            this.assertNotNull(vOrder);

            ValidationXAO xao = new ValidationXAO();
            Document doc = xao.generateValidationXML(vOrder);
            DOMUtil.print(doc, System.out);
        
        }
        catch(Exception e)
        {
            this.fail();
        }
    }
    
    public void testValidateOrderAmazon() 
    {
        try
        {
            this.assertNotNull(this.guid);
            Connection con = TestHelper.getConnection();
            ScrubMapperDAO dao = new ScrubMapperDAO(con);
            OrderVO order = dao.mapOrderFromDB(guid);

            OrderValidator validator = new OrderValidator();
            OrderValidationNode vOrder = validator.validateOrder(order, OrderValidator.VALIDATION_AMAZON, con);

            this.assertNotNull(vOrder);
            
            ValidationXAO xao = new ValidationXAO();
            Document doc = xao.generateValidationXML(vOrder);
            DOMUtil.print(doc, System.out);
          
          
        }
        catch(Exception e)
        {
          
        }
    }
    
    private OrderVO getOrder(int code)
    {
        OrderVO order = new OrderVO();
        List payments = new ArrayList();
        List creditCards = new ArrayList();
        List items = new ArrayList();
        List recipients = new ArrayList();
        List recipPhones = new ArrayList();
        List recipAddresses = new ArrayList();
        List buyers = new ArrayList();
        List buyerAddresses = new ArrayList();
        List buyerPhones = new ArrayList();
        List memberships = new ArrayList();
        List qmsAddresses = new ArrayList();
        
        OrderDetailsVO item = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        RecipientPhonesVO recipPhone = null;
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        BuyerPhonesVO buyerPhone = null;
        MembershipsVO membership = null;
        AVSAddressVO avsAddress = null;
        
        item = new OrderDetailsVO();


        switch (code)
        {
            case 0: // 
                order.setSourceCode("JMP1");
                break;
            case 1: // 
                //order.setSourceCode("1012");
                order.setSourceCode("9999");
                order.setProductsTotal("100.00");
                order.setOrderTotal("100.00");
                order.setOrderOrigin("novator");
                
                item = new OrderDetailsVO();
                item.setLineNumber("1");
                item.setDeliveryDate("12/10/2003");
                item.setProductId("6104");
                item.setFloristNumber("12-2648AA");
                item.setProductsAmount("30.99");

                //item.setProductId("8212");
                //item.setProductsAmount("59.99");

                item.setSizeChoice("A");
                item.setShipMethod("ND");
                item.setOccassionId("8");
                //item.setSpecialInstructions("This is an ass test");
                //item.setColorFirstChoice("A");
                //item.setColorSecondChoice("A");
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();                
                recipAddress.setAddressLine1("5424 W. Waveland Ave.");
                //recipAddress.setAddressLine1("P.O. Box 07");
                recipAddress.setCity("Chicago");
                recipAddress.setStateProvince("IL");
                recipAddress.setCountry("US");
                recipAddress.setPostalCode("60515");  
                recipAddress.setAddressType("HOME");
                recipAddress.setInternational("N");
                
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                avsAddress = new AVSAddressVO();
                avsAddress.setAddress("1234 STATE ST");
                avsAddress.setCity("CHICAGO");
                avsAddress.setStateProvince("IL");                
                avsAddress.setPostalCode("60605-2430");
                avsAddress.setLatitude(" 41630237");
                avsAddress.setLongitude(null);

                item.setAvsAddress(avsAddress);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("9999999999");
                recipPhone.setPhoneType("HOME");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                item.setRecipients(recipients);
                items.add(item);
/*
                item = new OrderDetailsVO();
                item.setLineNumber("2");
                item.setDeliveryDate("12/04/2003");
                item.setProductId("8212");
                item.setProductsAmount("59.99");
                //item.setProductsAmount("59.99");
                item.setSizeChoice("A");
                //item.setShipMethod("GR");
                item.setOccassionId("8");
                //item.setSpecialInstructions("This is an ass test");
                item.setColorFirstChoice("A");
                item.setColorSecondChoice("B");
                
                // Recipient
                recipient = new RecipientsVO();
                recipient.setLastName("Penney");
                recipient.setFirstName("A");
                recipients.add(recipient);
                
                recipAddress = new RecipientAddressesVO();                
                recipAddress.setAddressLine1("5424 W. Waveland Ave.");
                //recipAddress.setAddressLine1("P.O. Box 07");
                recipAddress.setCity("Chicago");
                recipAddress.setStateProvince("IL");
                recipAddress.setCountry("US");
                recipAddress.setPostalCode("60606");  
                recipAddress.setAddressType("HOME");
                recipAddress.setInternational("N");
                
                recipAddresses.add(recipAddress);
                recipient.setRecipientAddresses(recipAddresses);

                qmsAddress = new QmsAddressesVO();
                qmsAddress.setFirmName("HOME");
                qmsAddress.setAddressLine1("1234 STATE ST");
                qmsAddress.setCity("CHICAGO");
                qmsAddress.setStateProvince("IL");                
                qmsAddress.setPostalCode("60605-2430");
                qmsAddress.setGeofindMatchCode("T19");
                qmsAddress.setLatitude(" 41630237");
                qmsAddress.setLongitude(null);
                qmsAddress.setRangeRecordType("S");

                qmsAddresses.add(qmsAddress);
                item.setQmsAddresses(qmsAddresses);

                recipPhone = new RecipientPhonesVO();
                recipPhone.setPhoneNumber("9999999999");
                recipPhone.setPhoneType("HOME");
                recipPhones.add(recipPhone);
                recipient.setRecipientPhones(recipPhones);
                
                item.setRecipients(recipients);

                items.add(item);
*/                

                // Payments
                creditCard = new CreditCardsVO();
                creditCard.setCCExpiration("01/04");
                creditCard.setCCNumber("4111111111111111");
                creditCard.setCCType("VI");
                creditCards.add(creditCard);

                payment = new PaymentsVO();
                payment.setCreditCards(creditCards);
                payment.setPaymentsType("VI");
                payment.setPaymentMethodType("C");
                payments.add(payment);

                payment = new PaymentsVO();
                payment.setPaymentsType("GC");
                payment.setPaymentMethodType("G");
                payment.setAmount("10.00");
                payment.setGiftCertificateId("1111111");
                payments.add(payment);                

                order.setPayments(payments);

                // Buyer
                buyerAddress = new BuyerAddressesVO();
                buyerAddress.setAddressLine1("1745 Danbury Lane");
                buyerAddress.setCity("Blah");
                buyerAddress.setCountry("US");
                buyerAddress.setPostalCode("L5PM1");
                buyerAddress.setStateProv("ON");
                buyerAddresses.add(buyerAddress);                

                buyerPhone = new BuyerPhonesVO();
                buyerPhone.setPhoneType("Home");
                buyerPhone.setPhoneNumber("1234567890");

                buyerPhones.add(buyerPhone);                
                
                buyer = new BuyerVO();
                buyer.setBuyerAddresses(buyerAddresses);
                buyer.setBuyerPhones(buyerPhones);
                buyer.setFirstName("Jeff");
                buyer.setLastName("Penney");
                buyers.add(buyer);

                
                // Membership
                //membership = new MembershipsVO();
                //membership.setFirstName("Jeff");
                //membership.setLastName("Penney");
                //membership.setMembershipIdNumber("1234567890");

                //memberships.add(membership);

                //order.setMemberships(memberships);

                order.setBuyer(buyers);
                
                break;                
            default:            
                break;
        }
        
        order.setOrderDetail(items);

        return order;        
    }    

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            OrderValidatorTest orderValidatorTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                orderValidatorTest = new OrderValidatorTest(args[i]);
                orderValidatorTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }

    public void setUp()
    {
        try
        {
            // Load GUID for this test
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            guid = configUtil.getProperty(TEST_SETUP_FILE_NAME, "GUID");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }    
}