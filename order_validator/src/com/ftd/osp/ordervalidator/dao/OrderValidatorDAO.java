package com.ftd.osp.ordervalidator.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

/**
 *
 *
 * @author Michael Giovenco
 */
public class OrderValidatorDAO
{
  private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
  private static final String GET_PROGRAM_MASTER_BY_NAME = "GET_PROGRAM_MASTER_BY_NAME";
    
  Connection conn;
  private Logger logger;

  public OrderValidatorDAO(Connection conn)
  {
    this.conn = conn;
    logger = new Logger("com.ftd.osp.ordervalidator.dao.OrderValidatorDAO");
  }

  public ProductVO getProductDetails(String productId)
    throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
  {

    logger.info("getProductDetails(" + productId + ")");

    /* build DataRequest object */
    DataRequest dataRequest = new DataRequest();
    dataRequest.setConnection(conn);
    dataRequest.setStatementID(GET_PRODUCT_DETAILS);
    dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, productId);

    /* execute the stored procedure */
    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);
    ProductVO productVO = null;

    while (cr.next()){
      productVO = new ProductVO();
      productVO.setProductId(cr.getString("productId"));
      productVO.setNovatorId(cr.getString("novatorId"));
      productVO.setProductType(cr.getString("productType"));
      productVO.setProductSubType(cr.getString("productSubType"));
      productVO.setDeliveryType(cr.getString("deliveryType"));
      productVO.setMorningDeliveryFlag(cr.getString("morningDeliveryFlag"));
      productVO.setShippingSystem(cr.getString("shippingSystem"));
      productVO.setPersonalizationTemplate(cr.getString("personalizationTemplate"));
    }

    return productVO;
  }
  
  
  
  
  /**
   * Retrieves a the display name from the ACCOUNT.PROGRAM_MASTER table
   * @param connection
   * @param programName
   * @return The display name or an empty string if it is not found
   */
  public String getProgramDisplayName(String programName)
  throws Exception
  {
     DataRequest dataRequest = new DataRequest();
     dataRequest.setConnection(conn);
     dataRequest.setStatementID(GET_PROGRAM_MASTER_BY_NAME);
     
     dataRequest.addInputParam(ValidationConstants.IN_PROGRAM_NAME, programName);
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     Map outputs = (Map) dataAccessUtil.execute(dataRequest);
     String status = (String) outputs.get("OUT_STATUS");
     if (status == null || !status.equals("Y")) {
      String errorMessage = (String) outputs.get("OUT_MESSAGE");
      throw new Exception("Error in GET_PROGRAM_MASTER_BY_NAME: " + errorMessage);
     }
     
     CachedResultSet rs = (CachedResultSet) outputs.get("OUT_CUR");
     String programDisplayName = "";
     if (rs.next()) {
      programDisplayName = rs.getString("display_name");
     }
      
     logger.info("ProgramDisplayName: " + programName + "=" + programDisplayName);
    
    return programDisplayName;    
  }
  
  public String getStateByZipcodeAndCityAjax(Connection conn, String zipCode, String city) throws Exception{
  	logger.debug("getStateByZipcodeAndCity( ZipCode :" + zipCode + ")");
  	logger.debug("getStateByZipcodeAndCity( City : " + city + ")");

      /* build DataRequest object */
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      dataRequest.setStatementID("AJAX_GET_STATE_BY_ZIPCODE_CITY");
      Map inputParams = new HashMap();
      inputParams.put("IN_ZIP_CODE", zipCode);
      inputParams.put("IN_CITY", city);
      dataRequest.setInputParams(inputParams);
      
      
      /* execute the stored procedure */
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      CachedResultSet resultSet = (CachedResultSet)outputs.get("OUT_CUR");
      String outState = null;
      if(resultSet.next()){
    	  outState = resultSet.getString("state_id");  
      }
      
      return outState;
  	
  }    
  
}
