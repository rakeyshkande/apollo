package com.ftd.osp.ordervalidator.vo;
import java.util.*;

/**
 * This class represents a response returned from a validation handler class'
 * validate method.
 * 
 * @author Jeff Penney
 *
 */
public class ValidationResponse extends HashMap implements IValueObject
{
    public ValidationResponse()
    {
    }

    /**
     * This method follows the same matching rules as containsKey and returns
     * the matching string
     * @param key a key to search for
     * @author Jeff Penney
     *
     */
    public List getPartialMatch(String searchKey)
    {
        List retList = new ArrayList();
        Set keys = this.keySet();
        Iterator it = keys.iterator();
        String key = null;
        while(it.hasNext())
        {
            key = (String)it.next();

            if(key.startsWith(searchKey))
            {
                retList.add(key);                
            }
        }

        return retList;        
    }

    /**
     * This method checks the parameter key against the keys in teh Map.  If a 
     * Map key starts with the parameter key then true is returned.
     * @param key a key to search for
     * @author Jeff Penney
     *
     */
    public boolean containsKey(String searchKey)
    {
        boolean ret = false;
        Set keys = this.keySet();
        Iterator it = keys.iterator();
        String key = null;
        while(it.hasNext())
        {
            key = (String)it.next();

            if(key.startsWith(searchKey))
            {
                ret = true;
                break;
            }
        }

        return ret;
    }
}