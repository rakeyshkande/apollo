package com.ftd.osp.ordervalidator.vo;
import java.util.Map;

/**
 * This class is the object representation of a validation Node in the 
 * validation XML configuration file.  The Node contains a step which is a class
 * name that performs a validation, an execution order which is a number and result
 * actions which defile the actions that will be taken if certain errors are returned
 * from the validate method.
 * 
 * @author Jeff Penney
 *
 */
public class ValidationVO 
{
    private String step;
    private String executionOrder;
    private Map resultActions;
    boolean autoFix;

    public ValidationVO()
    {
    }

    public String getStep()
    {
        return step;
    }

    public void setStep(String newStep)
    {
        step = newStep;
    }

    public String getExecutionOrder()
    {
        return executionOrder;
    }

    public void setExecutionOrder(String newExecutionOrder)
    {
        executionOrder = newExecutionOrder;
    }

    public Map getResultActions()
    {
        return resultActions;
    }

    public void setResultActions(Map newResultActions)
    {
        resultActions = newResultActions;
    }

    public boolean isAutoFix()
    {
        return autoFix;
    }

    public void setAutoFix(boolean newAutoFix)
    {
        autoFix = newAutoFix;
    }
}