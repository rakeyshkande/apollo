package com.ftd.osp.ordervalidator.vo;

/**
 * This class extends ValidationNode and is used to represent gift certificate object's
 * validation errors.  It has a field for every field that is validated.
 * 
 * @author Jeff Penney
 *
 */
public class GiftCertValidationNode extends ValidationNode 
{
    public static final String GIFT_CERT_NUMBER = "giftCertificateNumber";
    public static final String GIFT_CERT_AMOUNT = "giftCertificateAmount";
    
    public GiftCertValidationNode()
    {
        super();
    }
}