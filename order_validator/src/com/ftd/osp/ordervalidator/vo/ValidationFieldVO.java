package com.ftd.osp.ordervalidator.vo;

/**
 * This class if used to represent the field elements of the validation 
 * configuration file.
 * @author Jeff Penney
 *
 */
public class ValidationFieldVO 
{
    private String name;
    private String type;

    public ValidationFieldVO()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String newType)
    {
        type = newType;
    }
}