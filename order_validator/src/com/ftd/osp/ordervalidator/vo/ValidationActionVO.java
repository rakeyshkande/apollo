package com.ftd.osp.ordervalidator.vo;
import java.util.List;

/**
 * This class represents a handler defined in the validation configuration file.
 * It will contain an error code, send to scrub flag and auto fix flag.
 * 
 * @author Jeff Penney
 *
 */
public class ValidationActionVO
{
    private String code;
    private boolean sendToScrub;
    private List fields;
    private String type;
    private String status;
    private boolean sendToClean;

    public ValidationActionVO()
    {
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String newCode)
    {
        code = newCode;
    }

    public boolean isSendToScrub()
    {
        return sendToScrub;
    }

    public void setSendToScrub(boolean newSendToScrub)
    {
        sendToScrub = newSendToScrub;
    }

    public boolean isSendToClean()
    {
        return sendToClean;
    }

    public void setSendToClean(boolean newSendToClean)
    {
        sendToClean = newSendToClean;
    }


    public List getFields()
    {
        return fields;
    }

    public void setFields(List newFields)
    {
        fields = newFields;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String newType)
    {
        type = newType;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String newStatus)
    {
        status = newStatus;
    }



}