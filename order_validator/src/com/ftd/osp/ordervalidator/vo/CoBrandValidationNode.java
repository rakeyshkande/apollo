package com.ftd.osp.ordervalidator.vo;

/**
 * This class extends ValidationNode and is used to represent co-brand object's
 * validation errors.  It has a field for every field that is validated.
 * 
 * @author Jeff Penney
 *
 */
public class CoBrandValidationNode extends ValidationNode 
{
    public static final String CO_BRAND_NAME = "coBrandName";
    public static final String CO_BRAND_DATA = "coBrandDate";

    public CoBrandValidationNode()
    {
        super();
    }
}