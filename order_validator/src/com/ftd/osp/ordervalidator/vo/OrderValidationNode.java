package com.ftd.osp.ordervalidator.vo;

/**
 * This class extends ValidationNode and is used to represent an order object's
 * validation errors.  It has a field for every field that is validated.
 * 
 * @author Jeff Penney
 *
 */
public class OrderValidationNode extends ValidationNode
{
    public static final String SOURCE_CODE = "source_code";
    public static final String BUYER_FIRST_NAME = "buyer_first_name";
    public static final String BUYER_LAST_NAME = "buyer_last_name";
    public static final String BUYER_ADDRESS_1 = "buyer_address1";
    public static final String BUYER_ADDRESS_2 = "buyer_address2";
    public static final String BUYER_CITY = "buyer_city";
    public static final String BUYER_STATE = "buyer_state";
    public static final String BUYER_POSTAL_CODE = "buyer_postal_code";
    public static final String BUYER_COUNTRY = "buyer_country";
    public static final String BUYER_DAYTIME_PHONE = "buyer_daytime_phone";
    public static final String BUYER_EVENING_PHONE = "buyer_evening_phone";
    public static final String BUYER_WORK_EXT = "buyer_work_ext";
    public static final String BUYER_FAX = "buyer_fax";
    public static final String BUYER_EMAIL_ADDRESS = "buyer_email_address";
    public static final String CC_NUMBER = "cc_number";
    public static final String CC_EXP_DATE = "cc_exp_date";
    public static final String CC_TYPE = "cc_type";
    public static final String ORDER_AMOUNT = "order_amount";
    public static final String MEMBER_ID = "membership_id";
    public static final String MEMBER_FIRST_NAME = "membership_first_name";
    public static final String MEMBER_LAST_NAME = "membership_first_name";

    public static final String GIFT_CERTIFICATES = "GIFT_CERTIFICATES";
    public static final String CO_BRAND_INFO = "CO_BRAND_INFO";
    public static final String ITEMS = "ITEMS";

    private boolean sendToClean;

    public OrderValidationNode()
    {
        super();
    }
    
    public boolean isSendToClean()
    {
        return sendToClean;
    }

    public void setSendToClean(boolean newSendToClean)
    {
        sendToClean = newSendToClean;
    }
    
}