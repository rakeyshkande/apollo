package com.ftd.osp.ordervalidator.vo;

import java.util.HashMap;

/**
 * This class represents a request that is passed as a parameter to the 
 * validate method of a validation handler class.
 * 
 * @author Jeff Penney
 *
 */
public class ValidationRequest extends HashMap implements IValueObject 
{	
	public ValidationRequest()
	{}
}
