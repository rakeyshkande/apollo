package com.ftd.osp.ordervalidator.vo;

/**
 * This class extends ValidationNode and is used to represent an item object's
 * validation errors.  It has a field for every field that is validated.
 * 
 * @author Jeff Penney
 *
 */
public class ItemValidationNode extends ValidationNode
{
    public static final String ORDER_TOTAL = "order_total";
    public static final String TAX_AMOUNT = "tax_amount";
    public static final String PRODUCT_ID = "productid";
    public static final String PRODUCT_ADDON = "product_addon";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String SIZE = "size";
    public static final String RECIP_ADDRESS1 = "recip_address1";
    public static final String RECIP_ADDRESS2 = "recip_address2";
    public static final String RECIP_CITY = "recip_city";
    public static final String RECIP_STATE = "recip_state";
    public static final String RECIP_POSTAL_CODE = "recip_postal_code";
    public static final String RECIP_COUNTRY = "recip_country";
    public static final String RECIP_INTERNATIONAL = "recip_international";
    public static final String RECIP_PHONE = "recip_phone";
    public static final String RECIP_PHONE_EXT = "recip_phone_ext";
    public static final String SHIP_TO_TYPE = "ship_to_type";
    public static final String SHIP_TO_TYPE_NAME = "ship_to_type_name";
    public static final String SHIP_TO_TYPE_INFO = "ship_to_type_info";
    public static final String DELIVERY_DATE = "delivery_date";
    public static final String SECOND_DELIVERY_DATE = "second_delivery_date";
    public static final String SPECIAL_INSTRUCTIONS = "special_instructions";
    public static final String SHIPPING_METHOD = "shipping_method";
    public static final String SUNDAY_DELIVERY_FLAG = "sunday_delivery_flag";
    public static final String SENDER_RELEASE_FLAG = "sunday_release_flag";
    public static final String QMS_RESULT_CODE = "qms_result_code";
    public static final String FIRST_COLOR_CHOICE = "first_color_choice";
    public static final String SECOND_COLOR_CHOICE = "second_color_choice";
    public static final String LINE_ITEM_NUMBER = "line_item_number";

    public static final String QMS_ADDRESS1 = "qms_address1";
    public static final String QMS_CITY = "qms_city";
    public static final String QMS_STATE = "qms_state";
    public static final String QMS_POSTAL_CODE = "qms_postal_code";
    
    
    

    public ItemValidationNode()
    {
        super();
    }
}