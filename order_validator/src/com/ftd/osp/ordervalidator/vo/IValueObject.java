package com.ftd.osp.ordervalidator.vo;

/**
 * This interface is for value objects to implement.
 * 
 * @author Jeff Penney
 *
 */
public interface IValueObject
{
}

