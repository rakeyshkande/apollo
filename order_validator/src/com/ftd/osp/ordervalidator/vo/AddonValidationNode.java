package com.ftd.osp.ordervalidator.vo;

/**
 * This class extends ValidationNode and is used to represent add on object's
 * validation errors.  It has a field for every field that is validated.
 * 
 * @author Jeff Penney
 *
 */
public class AddonValidationNode extends ValidationNode
{
    public static final String ID = "id";
    public static final String QUANTITY = "quantity";

    public AddonValidationNode()
    {
        super();
    }
}