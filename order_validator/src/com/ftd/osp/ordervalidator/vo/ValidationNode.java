package com.ftd.osp.ordervalidator.vo;

import java.util.*;

/**
 * This class represents a base class for a validation response value object.
 * This object will be used by the OrderValidator to build a representaion of
 * an order that has been validated.  It will contain a name and status as well
 * as child nodes.
 * 
 * @author Jeff Penney
 *
 */
public class ValidationNode
{
    public static final String NAME = "name";
    public static final String STATUS = "status";
    public static final String VALUE = "value";

    public static final String VALID = "valid";
    public static final String ERROR = "error";
    public static final String WARNING = "warning";

    public static final String ERROR_TYPE = "type";

    private Map attributes;
    private List nodes;
    private String text;

    public ValidationNode()
    {
        attributes = new HashMap();
        nodes = new ArrayList();
        text = null;
    }

    public String getAttribute(String field)
    {
        return (String)attributes.get(field);
    }

    public void setAttribute(String field, String value)
    {
        attributes.put(field, value);
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return text;
    }

    public List getChildNodes()
    {
        return nodes;
    }

    public List getChildNodes(String nodeName)
    {
        List myNodes = new ArrayList();
        ValidationNode node = null;

        for (int i = 0; i < nodes.size(); i++) 
        {
            node = (ValidationNode)nodes.get(i);
            if(node.getAttribute(ValidationNode.NAME).equals(nodeName))
            {
                myNodes.add(node);
            }            
        }
        
        return myNodes;
    }

    public void addChildNode(ValidationNode node)
    {
        nodes.add(node);
    }    
    
}