package com.ftd.osp.ordervalidator;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.CustProfileTimeoutException;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.ordervalidator.util.DispatchHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.ItemValidationNode;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.ordervalidator.vo.ValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.PaymentServiceRequest;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.OrderDataMapper;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.OrderDetailStates;
import com.ftd.osp.utilities.stats.StatsUtil;
import com.ftd.osp.utilities.stats.ValidationStatsUtil;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.MessageToken.MessageProperty;
import com.ftd.osp.utilities.xml.DOMUtil;


//import com.ftd.osp.utilities.*;


/**
 * This class is used by the scrub framework to automatically validate an order.
 * @author Jeff Penney
 *
 */
public class OrderValidatorBO implements IBusinessObject
{
    private static Logger logger= new Logger("com.ftd.osp.ordervalidator.OrderValidatorBO");
    private static final String MSG_TYPE_EXCEPTION = "EXCEPTION";
    private static final String MSG_SOURCE = "ORDER VALIDATOR SERVICE";

    
    private static final String FULL_ORDER = "ORDER";


    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    private static String msg_status_scrub = "0000";
    private static String msg_status_dispatch = "ES1004";
    private static String ROLLBACK = "ROLLBACK";
    private static String SYSTEM_USER = "_SYSTEM__";


    // Used to determine if the order is an OE order
    private static String regex = "M\\d";
    private static Pattern p = Pattern.compile(regex);

    /**
     * Default constructor for the OrderValidatorBO
     *
     */
    public OrderValidatorBO()
    {
    }

    /**
     * Implements the execute method of the IBusinessObject interface
     * @param config The BusinessConfig object passed from the message
     *
     */
    public void execute(BusinessConfig config)
    {
    	String MasterOrderNumber="";
        try
        {
            if(logger.isDebugEnabled())
                logger.debug("inside OrderValidatorBO");

            // Assemble order
            OrderDataMapper orderDao = new ScrubMapperDAO(config.getConnection());
            MessageToken token = config.getInboundMessageToken();

            if(logger.isDebugEnabled())
                logger.debug("guid: " + (String)token.getMessage());

            OrderVO scrubOrder = orderDao.mapOrderFromDB((String)token.getMessage());

            logger.info("Master Order Number: " + scrubOrder.getMasterOrderNumber());
            //SP-101 setting InvokedAsynchronously as true in order to re-process this message in MDB level in case of CAMSTimeout Exception.
            //scrubOrder.setInvokedAsynchronously(true); - SP-164 - Do not retry for Order validator asynchronous flow
            logger.debug("Setting InvokedAsynchronously as true");
            MasterOrderNumber = scrubOrder.getMasterOrderNumber();

            OrderValidationNode orderVNode = null;

            // Check the master order number because it should not be null or empty
            if(scrubOrder.getMasterOrderNumber() == null || scrubOrder.getMasterOrderNumber().length() < 1)
            {
                throw new ValidationException("Master order number is null or empty");
            }
            
            // Get metadata
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String paymentTypeCC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_CREDIT_CARD");

            String originBulk = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_BULK");

            String originAmazon = configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"ORDER_ORIGIN_AMAZON");

            String originRoses = configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"ORDER_ORIGIN_ROSES");

            Collection payments = scrubOrder.getPayments();
            Iterator it = null;
            if(payments != null && !payments.isEmpty()) {
                it = payments.iterator();
                boolean hasTokenId = false;
                boolean isCCEncrypted = false;
                while(it.hasNext()) {
                    PaymentsVO payment = (PaymentsVO) it.next();

                    if (payment.getTokenId() != null && !payment.getTokenId().isEmpty()) {
                        logger.info("tokenId: " + payment.getTokenId());
                        hasTokenId = true;
                        boolean successFlag = false;

                        try {
                        	logger.info("Call the token service:");
                        	// scrubOrder.getMasterOrderNumber()
                        	CreditCardsVO creditCard = PaymentServiceRequest.getCreditCardsFromToken(payment.getTokenId(), scrubOrder.getMasterOrderNumber());
                        	creditCard.setBuyerId(scrubOrder.getBuyerId());
                        	List<CreditCardsVO> creditCards = new ArrayList<CreditCardsVO>();
                    		creditCards.add(creditCard);
                    		payment.setCreditCards(creditCards);
                        	payment.setCreditCardId(creditCard.getCreditCardId());
                        	payment.setPaymentsType(creditCard.getCCType());
                        	payment.setPaymentMethodType(creditCard.getCCType());

                        	logger.info("Payment method type:" + payment.getPaymentMethodType());

                        	successFlag = true;
                        } catch (Exception ex) {
                        	logger.error("Error while calling Detokenization: " + ex.getMessage());
                        }

                        // Re-queue if the token service call was not successful
                        if (!successFlag) {                       	
                            String errorMessage = "Detokenization failed, re-queueing " +
                                    "Master Order Number: " + scrubOrder.getMasterOrderNumber() + " / " +
                                    "Payment Id: " + payment.getPaymentId();
                            logger.info(errorMessage);

                            SystemMessengerVO systemMessageVO = new SystemMessengerVO();
                            systemMessageVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                            systemMessageVO.setSource(MSG_SOURCE);
                            systemMessageVO.setType(MSG_TYPE_EXCEPTION);
                            systemMessageVO.setMessage(errorMessage);
                            systemMessageVO.setSubject("Tokenization Service Error");

                            SystemMessenger sysMessenger = SystemMessenger.getInstance();
                            sysMessenger.send(systemMessageVO, config.getConnection(), false);

                            int retryCount = 0;
                            MessageProperty retryProperty = token.getProperty("retryCount");
                            if (retryProperty != null) {
                                try {
                                    retryCount = Integer.parseInt( (String) retryProperty.value);
                                } catch (Exception e) {
                                    logger.error("Invalid retryCount: " + retryProperty);
                                }
                            }
                            String maxRetries = configUtil.getFrpGlobalParm("SERVICE", "DETOKENIZE_MAX_ATTEMPTS");
                            int maxRetryCount = 99;
                            try {
                                maxRetryCount = Integer.parseInt(maxRetries);
                            } catch (Exception e) {
                                logger.error(maxRetries + " is not a number");
                            }
                            retryCount += 1;
                            logger.info("retryCount: " + retryCount + " max: " + maxRetryCount);
                            if (retryCount <= maxRetryCount) {
                                MessageToken mt = new MessageToken();
                                mt.setStatus("ES1002");
                                mt.setMessage(scrubOrder.getGUID());
                                mt.setProperty("retryCount", Integer.toString(retryCount), "counter");
                                config.setOutboundMessageToken(mt);
                                return;
                            }
                        }
                    } else {
                    	logger.info("TokenId doesn't exist");

                        String paymentType = payment.getPaymentMethodType();
                        if(paymentType != null && paymentType.equals(paymentTypeCC) &&
                                payment.getCreditCards() != null && !payment.getCreditCards().isEmpty()) {
                            CreditCardsVO creditCard = (CreditCardsVO) payment.getCreditCards().get(0);
                            String ccEncrypted = creditCard.getCcEncrypted();
                            logger.info("ccEncrypted: " + ccEncrypted);
                            if (ccEncrypted != null && ccEncrypted.equals("Y")) {
                                isCCEncrypted = true;
                            }
                        }
                    }
                }
            }


            /* Determine if this is an OE order
             * Master order numbers starting with "M + a digit" are also created
             * by WebOE and Modify Order but are not routed through Order Gatherer.
             */
            if(logger.isDebugEnabled())
            {
                logger.debug("Regular expression: " + regex);
                logger.debug("Master order number: " + scrubOrder.getMasterOrderNumber());
            }

            Matcher m = p.matcher(scrubOrder.getMasterOrderNumber());
            scrubOrder.setOeOrder(m.lookingAt());

            if(logger.isDebugEnabled())
                logger.debug(scrubOrder.getMasterOrderNumber() + " is an OE order: " + scrubOrder.isOeOrder());

            // Set the order user to SYSTEM if this is not an OE order
            if(!scrubOrder.isOeOrder())
                scrubOrder.setCsrId(SYSTEM_USER);

            // Record entry.
            recordStats(scrubOrder, null, OrderDetailStates.OV_IN, config.getConnection());

            // Use the ValidationDataAccessUtil to reduce the number of database calls
            ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();

            // Massage any order data that does not fit into spec
            scrubOrder = DataMassager.massageOrderData(scrubOrder, config.getConnection(), vdau);

            //ObjectCopyUtil ocu = new ObjectCopyUtil();
            //OrderVO frpOrder = (OrderVO)ocu.deepCopy(scrubOrder);
            MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(config.getConnection());
            boolean isPartnerOrder = false;
            if(scrubOrder.getOrderOrigin() != null && new PartnerUtility().isPartnerOrder(scrubOrder.getOrderOrigin(),scrubOrder.getSourceCode(), config.getConnection())) {
            	isPartnerOrder = true;
            }
            // This section contains the checks for skipping order validation
            if(!skipValidation(scrubOrder))
            {
                // Validate order
                OrderValidator orderValidator = new OrderValidator();
                //For Amazon Order do validation
                if(logger.isDebugEnabled())
                    logger.debug("OVBO order origin: " + scrubOrder.getOrderOrigin());

                if( scrubOrder.getOrderOrigin() != null && scrubOrder.getOrderOrigin().equalsIgnoreCase( originAmazon ) )
                {
                  if(logger.isDebugEnabled())
                      logger.debug("OVBO before setting orderVNode");

                  orderVNode =
                    orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_AMAZON, config.getConnection(), vdau);

                  if(logger.isDebugEnabled())
                      logger.debug("OVBO after setting orderVNode");
                }
                else if( scrubOrder.getOrderOrigin() != null &&
                				mercentOrderPrefixes.isMercentOrder(scrubOrder.getOrderOrigin()))
                {
                  if(logger.isDebugEnabled())
                      logger.debug("OVBO before setting orderVNode - mercent order");

                  orderVNode =
                    orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_MERCENT, config.getConnection(), vdau);

                  if(logger.isDebugEnabled())
                      logger.debug("OVBO after setting orderVNode - mercent order");
                }
                else if (isPartnerOrder) {
					if (logger.isDebugEnabled())
						logger.debug("OVBO before setting orderVNode - partner order");
						orderVNode = orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_PARTNER, config.getConnection(), vdau);

					if (logger.isDebugEnabled())
						logger.debug("OVBO after setting orderVNode - partner order");
				}
                else if ( scrubOrder.getOrderOrigin() != null && scrubOrder.getOrderOrigin().equalsIgnoreCase( originRoses )) {
                    if (logger.isDebugEnabled())
                        logger.debug("OVBO before setting orderVNode - roses.com order");
                        orderVNode = orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_ROSES, config.getConnection(), vdau);

                    if (logger.isDebugEnabled())
                        logger.debug("OVBO after setting orderVNode - roses.com order");
                }
                // For OE orders
                else if( scrubOrder.isOeOrder() )
                {
                  orderVNode =
                    orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_OE, config.getConnection(), vdau);
                }
                // For everything else use auto validation
                else if(scrubOrder.getOrderOrigin() != null)
                {
                    orderVNode = orderValidator.validateOrder(scrubOrder, OrderValidator.VALIDATION_AUTO, config.getConnection(), vdau);
                }
            }

            // Use the fixer before attempting to save to FRP schema
            //FRPDataFixer fixer = new FRPDataFixer(config.getConnection());
            //frpOrder = fixer.fixOrder(frpOrder);

            //If this is an Amazon, Walmart or Target order and the entire order has been cancelled
            //because it failed one of the four pre-validation checks, then
            //the order has already been dispatched as a Removed Order, so the
            //following logic should not be executed.
            if( scrubOrder.getOrderOrigin() != null)
            {
                if ((scrubOrder.getOrderOrigin().equalsIgnoreCase( originAmazon ) ||
                		mercentOrderPrefixes.isMercentOrder(scrubOrder.getOrderOrigin()) ||
                		isPartnerOrder)
                    && scrubOrder.getStatus().equals(String.valueOf(OrderStatus.REMOVED)))
                {
                    // Do Nothing
                }
                else
                {
                    // If order is invalid save to FRP adapter (set default fields)
                    if(orderVNode != null && orderVNode.getAttribute(ValidationNode.STATUS) != null && orderVNode.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                    {
                      logger.info("Order " + scrubOrder.getMasterOrderNumber() + " is not valid.  Saving to SCRUB...");

                      if(logger.isDebugEnabled())
                          logger.debug("Here is the validation XML");

                      ValidationXAO vXML = new ValidationXAO();
                      Document doc = vXML.generateValidationXML(orderVNode);

                      //check for the "cc_number" field and mask it out
                      Node xmlNode = DOMUtil.selectSingleNode(doc, "/validation/order/header/data[@field_name='cc_number']");
                      if( xmlNode!=null )
                      {
                        NamedNodeMap map = xmlNode.getAttributes();
                        Attr attr = (Attr)map.getNamedItem("field_value");
                        if( attr!=null )
                        {
                          String strValue = attr.getValue();
                          if( strValue!=null )
                          {
                            int strLength = strValue.length();
                            String newValue = "XXXXXXXXXXXXXXXXXXXXXXXX";
                            if( strLength>=4 ) {
                              newValue += strValue.substring(strLength-4);
                              attr.setValue(newValue.substring(newValue.length()-strLength));
                            }
                          }
                        }
                      }

                      StringWriter sw = new StringWriter();
                      PrintWriter pw = new PrintWriter(sw);
                      DOMUtil.print(doc, pw);

                      if(logger.isDebugEnabled())
                          logger.debug(sw.toString());

                      HashSet validItems = new HashSet();

                      boolean headerValid = true;
                      boolean itemsValid = true;

                      ValidationNode vHeader = (ValidationNode)orderVNode.getChildNodes("header").get(0);
                      ValidationNode vItems = (ValidationNode)orderVNode.getChildNodes("items").get(0);

                      // Determine what part of the order is invalid
                      if(vHeader.getAttribute(ValidationNode.STATUS) != null && vHeader.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                      {
                          headerValid = false;
                      }

                      if(vItems.getAttribute(ValidationNode.STATUS) != null && vItems.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                      {
                          itemsValid = false;
                      }

                      // header valid with items that are invalid
                      if(headerValid && !itemsValid)
                      {
                          setStatusItemInValid(scrubOrder, orderVNode, config, validItems);
                          //setStatusItemInValid(frpOrder, orderVNode, null);
                      }
                      // header invalid
                      else if(!headerValid)
                      {
                          setStatusHeaderInValid(scrubOrder, orderVNode, config);
                          //setStatusHeaderInValid(frpOrder, orderVNode, null);
                      }

                      //if there is enough valid info on the order then send to the clean schema
                      if(orderVNode.isSendToClean()){

                            logger.info("Invalid order is being dispatched to Clean:" + scrubOrder.getGUID());

                            //dispatch each order detail record
                            List orderDetails = scrubOrder.getOrderDetail();
                            Iterator iter = orderDetails.iterator();
                            while(iter.hasNext()){

                                OrderDetailsVO orderDetailVO = (OrderDetailsVO)iter.next();

                                //dispatch record (but only invalid ones since calling method will do valid ones)
                                if (!validItems.contains(orderDetailVO.getLineNumber())) {
                                  logger.info("Dispatching invalid line item #" + orderDetailVO.getLineNumber());
                                  DispatchHelper.dispatchOrder(scrubOrder.getGUID(),orderDetailVO.getLineNumber(),this.msg_status_dispatch);
                                }
                            }

                        }
                        else
                        {
                            logger.info("Invalid order is not being dispatched to Clean:" + scrubOrder.getGUID());
                        }



                    }
                    // If order is valid save to FRP tables
                    else
                    {
                      logger.info("Order " + scrubOrder.getMasterOrderNumber() + " is valid.  Saving...");

                      setStatusOrderValid(scrubOrder, config);
                      //setStatusOrderValid(frpOrder, null);
                    }

                    // Save to Scrub schema
                    orderDao.updateOrder(scrubOrder);

                    }//end if this is an Amazon order and the entire order has been cancelled check

              // Save to FRP schema
              //frpDAO.mapOrderToDB(frpOrder);
              }
        }
        //handling CAMSTimeoutException .So MDB re-process the message
        catch(CustProfileTimeoutException camstimeoutEx)
        {
        	logger.error(camstimeoutEx.getMessage());
        	logException(camstimeoutEx, config,"NOPAGE Profile Service TimeoutException for MasterOrderNumber = "+MasterOrderNumber +". Please Re-Instate the order");
        }
        catch(SQLException sqle)
        {
            logException(sqle, config);
        }
        catch(ParserConfigurationException pce)
        {
            logException(pce, config);
        }
        catch(IOException ioe)
        {
            logException(ioe, config);
        }
        catch(TransformerException te)
        {
            logException(te, config);
        }
        catch(SAXException saxe)
        {
            logException(saxe, config);
        }
        catch(NoSuchFieldException nsfe)
        {
            logException(nsfe, config);
        }
        catch(InvocationTargetException ite)
        {
            logException(ite, config);
        }
        catch(IllegalAccessException iae)
        {
            logException(iae, config);
        }
        catch(NoSuchMethodException nsme)
        {
            logException(nsme, config);
        }
        catch(ClassNotFoundException cnfe)
        {
            logException(cnfe, config);
        }
        catch(Exception e)
        {
            logException(e, config);
        }
        catch(Throwable t)
        {
            logException(t, config);
        }
        finally
        {

        }
    }


    public boolean validateStateByZipcode(String zipCode, String stateCode, String city) throws Exception{
    	boolean returnVal = true;
    	Connection conn = null;
		 String outState = null;
	     try {
	         conn = getDBConnection();
	         if (conn == null) {
	             throw new Exception("Unable to connect to database");
	         }
	         if(city == null || city.trim().equals("")){
	         	city = "";
	         }
	         OrderValidatorDAO orderDAO = new OrderValidatorDAO(conn);
	         outState = orderDAO.getStateByZipcodeAndCityAjax(conn, zipCode, city);
	         if(outState != null){
	        	 //String stateCd = (String)resultMap.get("state_id");
	        	 if(!stateCode.equalsIgnoreCase(outState)){
	        		 returnVal = false;
	        	 }
	         }else{
	        	 returnVal = false;
	         }
	     } catch (Throwable t) {
	         logger.error("Error in validateStateByZipcodeAndCity()",t);
	         throw new Exception("Could not validate state code.");
	     }finally{
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error("Error while closing connection");
				}
			}
		}
    	return returnVal;
    }

    private void recordStats(OrderVO order, String relator, int state, Connection connection) throws Exception
    {
        OrderDetailsVO item = null;
        Iterator it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            recordStats(order, item, relator, state, connection);
        }
    }

    private void recordStats(OrderVO order, OrderDetailsVO item, String relator, int state, Connection connection) throws Exception
    {
        StatsUtil statsUtil = new StatsUtil();
        statsUtil.setOrder(order);
        statsUtil.setState(state);
        statsUtil.setRelator(relator);
        statsUtil.setItem(item);
        statsUtil.insert(connection);

    }

    private void setBuyerStatus(OrderVO order, String status)
    {
        BuyerVO buyer = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer != null)
            {
                buyer.setStatus(status);
            }
        }
    }

    private void setRecipientStatus(OrderDetailsVO item, String status)
    {
        RecipientsVO recipient = null;

        if(item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO)item.getRecipients().get(0);

            if(recipient != null)
            {
                recipient.setStatus(status);
            }
        }
    }
    private void logException(Throwable e, BusinessConfig config)
    {
    	logException(e,config, null);
    }
    /*
     * adding subject to System Error Email Notification in case of CAMSTimeoutException
     */
    private void logException(Throwable e, BusinessConfig config, String subject)
    {
        logger.error(e);

        try
        {
            SystemMessengerVO sysVo = new SystemMessengerVO();
            sysVo.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            sysVo.setMessage(stringWriter.toString());
            sysVo.setSource(MSG_SOURCE);
            sysVo.setType(MSG_TYPE_EXCEPTION);
            if(subject != null){
            	sysVo.setSubject(subject);
            }
            SystemMessenger.getInstance().send(sysVo, getDBConnection());
        }
        catch(Exception ee)
        {
            logger.error("Could not log exception to SystemMessenger");
        }
        MessageToken mt = new MessageToken();
        mt.setStatus(ROLLBACK);
        config.setOutboundMessageToken(mt);
    }


    private ValidationNode getValidationItem(String lineNumber, ValidationNode vItems)
    {
        ValidationNode vItem = null;
        ValidationNode vItemTemp = null;
        Collection items = vItems.getChildNodes();
        Iterator it = items.iterator();

        while(it.hasNext())
        {
            vItemTemp = (ValidationNode)it.next();

            String nodeNumber = ((ValidationNode)vItemTemp.getChildNodes(ItemValidationNode.LINE_ITEM_NUMBER).get(0)).getText();

            if(nodeNumber.equals(lineNumber))
            {
                vItem = vItemTemp;
            }
        }

        return vItem;
    }

    private boolean skipValidation(OrderVO order) throws IOException, TransformerException,
                    SAXException, ParserConfigurationException, TransformerException, SQLException,
                    ParseException
    {
        boolean foundCC = false;
        boolean foundEmail = false;
        boolean result = false;

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String paymentTypeCC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_CREDIT_CARD");

        // Check email address unix2@novator.com
        BuyerVO buyer = null;
        String email = null;

        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            // Get email address

            if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
            {
                email = ((BuyerEmailsVO)buyer.getBuyerEmails().get(0)).getEmail();
            }

            if(email == null)
            {
                email = "";
            }

            if(email.equals("unix2@novator.com"))
            {
                foundEmail = true;
            }
        }

        // Check credit card number 4111111111111111
        Collection payments = order.getPayments();
        Iterator it = null;
        if(payments != null && payments.size() > 0)
        {
            // Get order credit cards
            it = payments.iterator();
            PaymentsVO payment = null;
            CreditCardsVO creditCard = null;
            String ccNumber = null;
            String paymentType = null;
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                paymentType = payment.getPaymentMethodType();
                if(paymentType == null) paymentType = "";
                if(paymentType.equals(paymentTypeCC))
                {
                    if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                        ccNumber = creditCard.getCCNumber();
                        if(ccNumber == null) ccNumber = "";

                        if(ccNumber.equals("4111111111111111"))
                        {
                            foundCC = true;
                            break;
                        }
                    }
                }
            }
        }

        if(foundCC && foundEmail)
        {
            result = true;
        }

        return result;
    }

    /**
     * This method sets the status of order valid
     * valid
     * @author Jeff Penney
     *
     */
    private void setStatusOrderValid(OrderVO order, BusinessConfig config) throws Exception
    {
        // Set order status to Valid
        order.setStatus(String.valueOf(OrderStatus.VALID));
        setBuyerStatus(order, String.valueOf(OrderStatus.VALID));

        // Set Item status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            //Check to see if status of item is set to 2004 - Removed.
            //If the item status = 2004, then this item
            //has already been cancelled and dispatched to the hp in the
            //cancelled order format, so skip the logic below.
            if(!item.getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
            {
              item.setStatus(String.valueOf(OrderStatus.VALID_ITEM));
              setRecipientStatus(item, String.valueOf(OrderStatus.VALID_ITEM));

              if(config != null)
              {
                  // Set response in the message token
                  mt = new MessageToken();
                  mt.setStatus(this.msg_status_dispatch);
                  mt.setMessage(item.getGuid() + "/" + item.getLineNumber());
                  config.setOutboundMessageToken(mt);

                  // record exit
                  this.recordStats(order, null, OrderDetailStates.OV_OUT, config.getConnection());
              }

            }
        }
    }

    /**
     * This method sets the status of header invalid
     * @author Jeff Penney
     *
     */
    private void setStatusHeaderInValid(OrderVO order, OrderValidationNode vOrder, BusinessConfig config)
                 throws SAXException, ParserConfigurationException, IOException,
                 SQLException, SystemMessengerException, Exception
    {
        ValidationNode vItems = (ValidationNode)vOrder.getChildNodes("items").get(0);
        ValidationNode vItem = null;

        // Set header status
        order.setStatus(String.valueOf(OrderStatus.INVALID_HEADER));
        setBuyerStatus(order, String.valueOf(OrderStatus.INVALID_HEADER));

        // Loop through items to set status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        ValidationStatsUtil vStats = new ValidationStatsUtil();
        String reason = null;
        String itemNumber = null;
        String fieldValue = null;
        String fieldName = null;

        // Get the XML representation of the order validation object
        ValidationXAO vXML = new ValidationXAO();
        Document doc = vXML.generateValidationXML(vOrder);
        // Look for errors
        NodeList headerErrorNodes = DOMUtil.selectNodes(doc, "/validation/order/header/data[@status='error']");

        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            itemNumber = item.getLineNumber();

            vItem = getValidationItem(itemNumber, vItems);

            // Item is invalid
            if(vItem.getAttribute(ValidationNode.STATUS) != null && vItem.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
            {
                item.setStatus(String.valueOf(OrderStatus.INVALID_ITEM));
                setRecipientStatus(item, String.valueOf(OrderStatus.INVALID_ITEM));
            }
            // Item is valid but header is not
            else
            {
                item.setStatus(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER));
                setRecipientStatus(item, String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER));
            }

            if(config != null)
            {
                // Set response in the message token
                mt = new MessageToken();
                mt.setStatus(this.msg_status_scrub);
                mt.setMessage(item.getGuid() + "/" + item.getLineNumber());
                config.setOutboundMessageToken(mt);

                NodeList itemErrorNodes = DOMUtil.selectNodes(doc, "/validation/order/items/item/data[@status='error']");
                Node dataNode = null;
                Node messageNode = null;
                NodeList messages = null;
                // Record the errors to the stats tables
                for(int i = 0; i < headerErrorNodes.getLength(); i++)
                {
                    dataNode = (Node)headerErrorNodes.item(i);
                    fieldValue = dataNode.getAttributes().getNamedItem("field_value").getNodeValue();
                    fieldName = dataNode.getAttributes().getNamedItem("field_name").getNodeValue();
                    messages = DOMUtil.selectNodes(doc, "/validation/order/header/data[@field_name='" + fieldName + "']/messages/message/description");
                    for(int j = 0; j < messages.getLength(); j++)
                    {
                        messageNode = messages.item(j);
                        reason = messageNode.getFirstChild().getNodeValue();
                        vStats.insertReseaon(item.getExternalOrderNumber(), order.getGUID(), reason, fieldValue, config.getConnection());
                    }
                }

                for(int i = 0; i < itemErrorNodes.getLength(); i++)
                {
                    dataNode = (Node)itemErrorNodes.item(i);
                    fieldValue = dataNode.getAttributes().getNamedItem("field_value").getNodeValue();
                    fieldName = dataNode.getAttributes().getNamedItem("field_name").getNodeValue();
                    messages = DOMUtil.selectNodes(doc, "/validation/order/items/item[@line_number='" + item.getLineNumber() + "']/data[@field_name='" + fieldName + "']/messages/message/description");
                    for(int j = 0; j < messages.getLength(); j++)
                    {
                        messageNode = messages.item(j);
                        reason = messageNode.getFirstChild().getNodeValue();
                        vStats.insertReseaon(item.getExternalOrderNumber(), order.getGUID(), reason, fieldValue, config.getConnection());
                    }
                }

                // record exit
                this.recordStats(order, item, order.getGUID(), OrderDetailStates.OV_OUT, config.getConnection());
            }
        }
    }

    /**
     * This method sets the status of items invalid if the header is
     * valid
     * @author Jeff Penney
     *
     */
    private void setStatusItemInValid(OrderVO order, OrderValidationNode vOrder, BusinessConfig config, HashSet validItems)
                 throws SAXException, ParserConfigurationException, IOException,
                 SQLException, SystemMessengerException, Exception
    {
        ValidationNode vItems = (ValidationNode)vOrder.getChildNodes("items").get(0);
        ValidationNode vItem = null;

        boolean validHeaderInvalidItemSet = false;

        // Loop through items to set status
        List items = order.getOrderDetail();
        MessageToken mt = null;
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        String itemNumber = null;
        ValidationStatsUtil vStats = new ValidationStatsUtil();
        String reason = null;
        String fieldValue = null;
        String fieldName = null;

        // Get the XML representation of the order validation object
        ValidationXAO vXML = new ValidationXAO();
        Document doc = vXML.generateValidationXML(vOrder);
        // Look for errors
        NodeList headerErrorNodes = DOMUtil.selectNodes(doc, "/validation/order/header/data[@status='error']");

        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            itemNumber = item.getLineNumber();

            vItem = getValidationItem(itemNumber, vItems);

            //Check to see if status of item is set to 2004 - Removed.
            //If the item status = 2004, then this item
            //has already been cancelled and dispatched to the hp in the
            //cancelled order format, so skip the logic below.
            if(!item.getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
            {
              // Set header status.  Set it only once per order.
              if(!validHeaderInvalidItemSet)
              {
                order.setStatus(String.valueOf(OrderStatus.VALID_HEADER_INVALID_ITEM));
                setBuyerStatus(order, String.valueOf(OrderStatus.VALID_HEADER_INVALID_ITEM));
                validHeaderInvalidItemSet = true;
              }

              mt = new MessageToken();
              mt.setMessage(item.getGuid() + "/" + item.getLineNumber());



              // Item is invalid
              if(vItem.getAttribute(ValidationNode.STATUS) != null && vItem.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
              {

                  item.setStatus(String.valueOf(OrderStatus.INVALID_ITEM));
                  setRecipientStatus(item, String.valueOf(OrderStatus.INVALID_ITEM));

                  mt.setStatus(this.msg_status_scrub);
                  if(config != null)
                  {
                      NodeList itemErrorNodes = DOMUtil.selectNodes(doc, "/validation/order/items/item/data[@status='error']");
                      Node dataNode = null;
                      Node messageNode = null;
                      NodeList messages = null;
                      // Record the errors to the stats tables
                      for(int i = 0; i < headerErrorNodes.getLength(); i++)
                      {
                          dataNode = (Node)headerErrorNodes.item(i);
                          fieldValue = dataNode.getAttributes().getNamedItem("field_value").getNodeValue();
                          fieldName = dataNode.getAttributes().getNamedItem("field_name").getNodeValue();
                          messages = DOMUtil.selectNodes(doc, "/validation/order/header/data[@field_name='" + fieldName + "']/messages/message/description");
                          for(int j = 0; j < messages.getLength(); j++)
                          {
                              messageNode = messages.item(j);
                              reason = messageNode.getFirstChild().getNodeValue();
                              vStats.insertReseaon(item.getExternalOrderNumber(), order.getGUID(), reason, fieldValue, config.getConnection());
                          }
                      }

                      for(int i = 0; i < itemErrorNodes.getLength(); i++)
                      {
                          dataNode = (Node)itemErrorNodes.item(i);
                          fieldValue = dataNode.getAttributes().getNamedItem("field_value").getNodeValue();
                          fieldName = dataNode.getAttributes().getNamedItem("field_name").getNodeValue();
                          messages = DOMUtil.selectNodes(doc, "/validation/order/items/item/data[@field_name='" + fieldName + "']/messages/message/description");
                          for(int j = 0; j < messages.getLength(); j++)
                          {
                              messageNode = messages.item(j);
                              reason = messageNode.getFirstChild().getNodeValue();
                              vStats.insertReseaon(item.getExternalOrderNumber(), order.getGUID(), reason, fieldValue, config.getConnection());
                          }
                      }

                      // record exit
                      this.recordStats(order, item, order.getGUID(), OrderDetailStates.OV_OUT, config.getConnection());
                  }
              }
              // Item is valid
              else
              {
                  item.setStatus(String.valueOf(OrderStatus.VALID_ITEM));
                  setRecipientStatus(item, String.valueOf(OrderStatus.VALID_ITEM));
                  validItems.add(itemNumber);

                  mt.setStatus(this.msg_status_dispatch);

                  if(config != null)
                  {
                      // record exit
                      this.recordStats(order, item, null, OrderDetailStates.OV_OUT, config.getConnection());
                  }
              }

              // Only set the token if the config is not null since we call this method
              // twice in a row.
              if(config != null)
              {
                  config.setOutboundMessageToken(mt);
              }
          }//end isAmazonOrder check
        }
    }

    // ****** THIS HAS NOT BEEN UNIT TESTED USE AT YOUR OWN RISK ****************
    private String cleanupAlphaNumString(String inString, boolean keepAlpha)
    {
        StringBuffer outString = new StringBuffer();
        String keepString = null;
        String curChar;

        if(keepAlpha)
        {
            keepString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        }
        else
        {
            keepString = "1234567890";
        }

        for(int i = 0; i < inString.length(); i++)
        {
            curChar = inString.substring(i, i+1);

            if(keepString.indexOf(curChar) != -1)
            {
                outString.append(curChar);
            }
        }

        return outString.toString();
    }

    private Connection getDBConnection()
	throws IOException, ParserConfigurationException, SAXException, TransformerException,
           Exception
    {
	    Connection conn = null;
		conn = DataSourceUtil.getInstance().getConnection(ValidationConstants.ORDER_SCRUB_DATASOURCE_NAME);
		return conn;
    }
    // ****** THIS HAS NOT BEEN UNIT TESTED USE AT YOUR OWN RISK ****************
}
