package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;

/**
 * This class Validates fields in Item fields such as special instructions,
 * gift message and signature.
 * 
 * @author Jeff Penney
 *
 */
public class ValidateOtherItemFields 
{
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    private static final String UPDATE_ORDER_ADDONS_PRICE = "UPDATE_ORDER_ADDONS_PRICE";
    private static final String GET_ADDONS_BY_ID_FOR_VENDOR_PROD = "GET_ADDONS_BY_ID_FOR_VENDOR_PROD";
    private static final String SAME_DAY_DELIVERY = "SD";
    private static final String SHIP_METHOD_NEXT_DAY =  "ND";
    private static final String SHIP_METHOD_TWO_DAY =   "2F";
    private static final String SHIP_METHOD_SATURDAY =  "SA";
    private static final String SHIP_METHOD_GROUND =    "GR";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateOtherItemFields");
        
    /**
     * Validates the following Item fields: gift message, special instructions, signature
     * @param request Contains an Order object
     * @exception Exception if a database error occurs.
     * @return Returns any validation errors.
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, Exception
    {    
        logger.info("Starting other item field validation");

        OrderVO order = null;
        Connection conn = null;
        Map outputParams = null;

        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }

        ValidationResponse response = new ValidationResponse();

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String funeralCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "OCCASION_TYPE_FUNERAL");  
        String amazonOrigin = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_AMAZON");
        
        // Loop through the order's items
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        String specialInstructions = null;
        String giftMessage = null;
        String signature = null;
        String occasionId = null;
        String detailSourceCode = null;
        String orderSourceCode = order.getSourceCode();

        java.util.Date startDate = null;
        java.util.Date endDate = null;
        String sourceCode = null;

        if(orderSourceCode == null)
        {
          orderSourceCode = "";
        }

        DataRequest dataRequest = new DataRequest();                    
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            
            if (order.getOrderOrigin().equalsIgnoreCase(amazonOrigin) || 
            		mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin()) ||
            		new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), conn)
            	)

            { 
              try {            
                    if( Integer.parseInt(item.getQuantity()) > 
                              Integer.parseInt(configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"MAX_QUANTITY" ))  )
                    {
                        response.put( ValidationConstants.RESPONSE_MAX_QUANTITY_EXCEEDED  + 
                                     item.getLineNumber(), 
                                     FieldUtils.replaceAll(ValidationConstants.RESPONSE_MAX_QUANTITY_EXCEEDED, "max_quantity", String.valueOf(Integer.parseInt(configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"MAX_QUANTITY" ))) ) );              
                        logger.debug("quantity error: " + FieldUtils.replaceAll(ValidationConstants.RESPONSE_MAX_QUANTITY_EXCEEDED, "max_quantity", String.valueOf(Integer.parseInt(configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"MAX_QUANTITY")))) );

                    }
              }
              catch(NumberFormatException e) {
                throw new ValidationException("Error parsing quantity: " + e.toString() );
              }
              
            }

            specialInstructions = item.getSpecialInstructions();
            giftMessage = item.getCardMessage();
            signature = item.getCardSignature();
            occasionId = item.getOccassionId();

            detailSourceCode=item.getSourceCode();
            if(detailSourceCode == null)
            {
              detailSourceCode = "";
            }
        
            //Validation detail level source code. emueller, 3/22/04-Item Of the Week change
            //We only need to validate if the source is not the same as the order
            //level source code.
            if(!order.getSourceCode().equals(detailSourceCode)){

                if(detailSourceCode.length() == 0)
                {
                  //error, must have detail level source code
                   response.put(ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_INVALID);              
                }
                else
                {
                    //Get source code info from cache or DB
                    if(sourceHandler != null)
                    {
                        SourceMasterVO sourceVo = sourceHandler.getSourceCodeById(detailSourceCode);
                        if(sourceVo != null)
                        {
                            sourceCode = sourceVo.getSourceCode();
                            startDate = sourceVo.getStartDate();
                            endDate = sourceVo.getEndDate();
                        }
                    }
                    // Get the source code data from the database
                    else
                    {
                        dataRequest.addInputParam("SOURCE_CODE", detailSourceCode);
                        dataRequest.setConnection(conn);
                        dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
                        CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
                        rs.reset();        
                        if(rs.next())
                        {
                            sourceCode = (String)rs.getObject(1);
                            startDate = (java.util.Date)rs.getObject(5);
                            endDate = (java.util.Date)rs.getObject(6);
                        }//end record set contains data
                    }//end get data from cache or DB


                    if(sourceCode != null)
                    {
                        // If the source code exits but is before today then send back expired
                        if(endDate != null && endDate.before(new java.util.Date()))
                        {
                            response.put(ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_EXPIRED + item.getLineNumber(), ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_EXPIRED);
                        }
                    }
                    else
                    {
                        // if the source code does not exist in the SOURCE table
                        // then it is invalid
                        response.put(ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_DETAIL_SOURCE_CODE_INVALID);
                    }                  

  
                }//end, else source code exists on detail
            }//end if source codes on detail is same as header

            
            // Validate gift message length
            if (giftMessage != null) 
            {
                if (giftMessage.length() > ValidationConstants.GIFT_MESSAGE_MAX_LEN)  
                {
                    response.put(ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN + item.getLineNumber(), ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN);
                } else {
                    String pcData = item.getPersonalizationData();
                    if (pcData != null && !pcData.trim().isEmpty() && giftMessage.length() > ValidationConstants.GIFT_MESSAGE_MAX_LEN_PC) 
                    {
                        response.put(ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN + item.getLineNumber(), ValidationConstants.RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN);
                    }                    
                }
                
    			if (!StringUtils.isEmpty(giftMessage) && FieldUtils.numberRangeRegCheck(giftMessage, 13, 20) ) 
    			{    				
    				response.put(
    						ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_GIFT_MSG
    								+ item.getLineNumber(),
    						ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_GIFT_MSG);
    			}
            }

            // Validate special instrucitons length
            // Only check for Ariba and Funeral orders
            String occasion = item.getOccassionId();
            if(occasion == null) occasion = "";
            String origin = order.getOrderOrigin();
            if(origin == null) origin = "";
         
            //Validation special instructins based on order origin
            boolean instructionsError = false;
            if(specialInstructions != null && specialInstructions.length() > 300)
            {
                response.put(ValidationConstants.RESPONSE_SPEC_INSTR_EXCEEDS_MAX_LEN + item.getLineNumber(), ValidationConstants.RESPONSE_SPEC_INSTR_EXCEEDS_MAX_LEN);
                instructionsError = true;
            }

            // Validate signature length
            if(signature != null && signature.length() > 230)
            {
                response.put(ValidationConstants.RESPONSE_SIGNATURE_EXCEEDS_MAX_LEN + item.getLineNumber(), ValidationConstants.RESPONSE_SIGNATURE_EXCEEDS_MAX_LEN);
            }

            // Begin Check for an occasion
            int occasionIdInt = 0;
            if(occasionId == null || occasionId.length() < 1)
            {
                response.put(ValidationConstants.RESPONSE_OCCASION_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_OCCASION_MISSING);
            } else {
                try {
                    occasionIdInt = Integer.parseInt(occasionId);
                } catch (NumberFormatException nfe) {
                    response.put(ValidationConstants.RESPONSE_OCCASION_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_OCCASION_MISSING);
                    occasionIdInt = 0;
                }
            }
            // End Check for an occasion


            // Make sure add-ons are valid
            AddOnsVO addon = null;
            List addons = item.getAddOns();
            Iterator addonIt = null;
            dataRequest = new DataRequest();
            ArrayList<String> addonIdList = new ArrayList<String>();
            boolean addonCheckFailed = false;
            boolean addonMaxExceeded = false; 
            boolean addonPriceInvalid = false;
            boolean addonInvalid = false;
            if(addons != null && addons.size() > 0)
            {  
            	Map<String, AddOnVO> allowedAddonsHash = new HashMap<String, AddOnVO>();
            	
                String  isFloristAvailable = "N" ;
                String  isDropshipAvailable = "N" ;
                 
                //#18729
                // fetching the delivery type of the product.
                // if ShipMethod is null or SD then it is florist delivered.
                // if ShipMethod is ND , 2F , GR , SA then it is drop-ship delivered.
                
                String shipMethod = item.getShipMethod();
                if (shipMethod == null || ("").equalsIgnoreCase(shipMethod) || (SAME_DAY_DELIVERY).equalsIgnoreCase(shipMethod) ){
                	isFloristAvailable = ValidationConstants.YES ;
                }
                if ((SHIP_METHOD_NEXT_DAY).equalsIgnoreCase(shipMethod) || (SHIP_METHOD_TWO_DAY).equalsIgnoreCase(shipMethod) 
                		|| (SHIP_METHOD_GROUND).equalsIgnoreCase(shipMethod) || (SHIP_METHOD_SATURDAY).equalsIgnoreCase(shipMethod)){
                	isDropshipAvailable = ValidationConstants.YES;
                }
                
                logger.info("Florist available: " + isFloristAvailable);
    			logger.info("Dropship available:" + isDropshipAvailable);

                // First get list of valid add-ons for this product/occasion/source code/DeliveryType  combo
                // and create hash (so we don't have to loop multiple times)
                AddOnUtility addonUtil = new AddOnUtility();
                if (logger.isDebugEnabled()) {
                    logger.debug("Addon validation for product/occasion/source: " + item.getProductId() + "/" + occasionIdInt + "/" + item.getSourceCode()); 
                }
                
                boolean returnAllAddOnFlag = false;
                if(new PartnerUtility().isPartnerOrder(origin,sourceCode,conn)){
                	returnAllAddOnFlag = true;
                }
                Map<String, ArrayList<AddOnVO>> aaHash = addonUtil.getActiveAddonListByProductIdAndOccasionAndSourceCodeAndDeliveryType(
                                                          item.getProductId(), occasionIdInt, item.getSourceCode(), returnAllAddOnFlag, isFloristAvailable , isDropshipAvailable ,conn);
                if (aaHash != null) {
                    Iterator aaIt = aaHash.keySet().iterator();
                    while (aaIt.hasNext()) {
                      String key = (String) aaIt.next();
                      ArrayList aavoList = aaHash.get(key);
                      for(int i = 0; i < aavoList.size(); i++) {
                          AddOnVO aavo = (AddOnVO)aavoList.get(i);
                          if (logger.isDebugEnabled()) {
                            logger.debug("Addon ID/price/maxQuantity " + aavo.getAddOnId() + "/" + aavo.getAddOnPrice() + "/" + aavo.getMaxQuantity()); 
                          }
                          allowedAddonsHash.put(aavo.getAddOnId(), aavo);
                      }
                    }
                } else {
                    // Nothing returned from addonUtility so treat as if no addons allowed
                    response.put(ValidationConstants.RESPONSE_ADDON_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_ADDON_INVALID);
                    addonCheckFailed = true;
                }
                
                // Now loop over order addons and confirm each is valid
                if (addonCheckFailed == false) {
                    addonIt = addons.iterator();
                    while(addonIt.hasNext()) {
                        addon = (AddOnsVO)addonIt.next();
                        if (logger.isDebugEnabled()) {
                            logger.debug("Addon for order: " + addon.getAddOnId() + " " + addon.getPrice()); 
                        }
                        AddOnVO allowedAddon = allowedAddonsHash.get(addon.getAddOnCode());
                        if (allowedAddon != null) {
                            // Save list of addons ids for vender check later
                            addonIdList.add(addon.getAddOnCode());

                            // Check if quantity exceeds max for this addon
                            try {
                                if (Integer.parseInt(addon.getAddOnQuantity()) > Integer.parseInt(allowedAddon.getMaxQuantity())) {
                                    addonCheckFailed = true;
                                    addonMaxExceeded = true;
                                }
                            } catch (NumberFormatException nfe) {
                                logger.error("validateOtherItemFields: One of these addon quantities is non-numeric: '" + 
                                             addon.getAddOnQuantity() + "' '" + allowedAddon.getMaxQuantity() + "'");
                                addonCheckFailed = true;
                                addonMaxExceeded = true;
                            }
                            
                            // See if addon price is current or historically valid and save matching addon_history_id in scrub.add_ons
                            java.util.Date priceDateTime = null;
                            // Use the order header order creation date
                            priceDateTime = order.getOrderDateTime();
                            if (priceDateTime != null) {
                                dataRequest.addInputParam("IN_ADDON_ID", addon.getAddOnId());
                                dataRequest.addInputParam("IN_ORDER_DATE", new java.sql.Timestamp(priceDateTime.getTime())); 
                                dataRequest.setConnection(conn);
                                dataRequest.setStatementID(UPDATE_ORDER_ADDONS_PRICE);
                                outputParams = (Map) vdau.execute(dataRequest);
                                dataRequest.reset();
                                boolean priceMatchFound = false;
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Addon validation - output from UPDATE_ORDER_ADDONS_PRICE: " + outputParams);  
                                }
                                if (outputParams != null) {
                                    if ("Y".equals((String) outputParams.get("OUT_STATUS"))) {
                                        priceMatchFound = true;
                                        BigDecimal addonHistId = (BigDecimal) outputParams.get("OUT_ADDON_HIST_ID");
                                        addon.setAddOnHistoryId(addonHistId.longValue());
                                    }
                                }
                                if (priceMatchFound == false) {
                                    addonCheckFailed = true;
                                    addonPriceInvalid = true;
                                }
                            } else {
                                logger.error("validateOtherItemFields: Couldn't verify addon historical price since order date was not populated");
                            }
                        } else {

                            // Addon not found in list of valid addons
                            addonCheckFailed = true;
                            addonInvalid = true;
                        }
                    }  // end while

                    if (addonInvalid) {
                        response.put(ValidationConstants.RESPONSE_ADDON_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_ADDON_INVALID);
                    }
                    if (addonMaxExceeded) {
                        response.put(ValidationConstants.RESPONSE_ADDON_MAX_QTY_EXCEEDED + item.getLineNumber(), ValidationConstants.RESPONSE_ADDON_MAX_QTY_EXCEEDED);
                    }
                    if (addonPriceInvalid) {
                        response.put(ValidationConstants.RESPONSE_ADDON_PRICE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_ADDON_PRICE_INVALID);
                    }
                    
                    if (addonCheckFailed == false) {
                    
                        // For vendor delivery make sure there's a vendor that has all the addons
                        if ((item.getShipMethod() != null) && (!SAME_DAY_DELIVERY.equals(item.getShipMethod()))) {
                            ArrayList vendorList = addonUtil.getVendorListByAddonListAndProduct(item.getProductId(), addonIdList, conn);
                            if (logger.isDebugEnabled()) {
                                int totalVendors = 0;
                                if (vendorList != null) totalVendors = vendorList.size();
                                logger.debug("Addon for vendor delivered order.  Total vendors supporting this product/addon(s) combo: " + totalVendors);
                            }
                            if (vendorList == null || vendorList.size() < 1) {
                                response.put(ValidationConstants.RESPONSE_ADDON_NO_VENDORS + item.getLineNumber(), ValidationConstants.RESPONSE_ADDON_NO_VENDORS);
                                break;
                            }
                        }
                    }
                }
            }  // End make sure add-ons are valid
        }


        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending other item field validation");

        return response;
    }
    
}
