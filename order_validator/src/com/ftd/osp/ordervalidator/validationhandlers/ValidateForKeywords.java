package com.ftd.osp.ordervalidator.validationhandlers;

import java.util.*;
import java.sql.*;
import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerException;

import com.ftd.osp.ordervalidator.util.*;
import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.KeywordHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.*;
import com.ftd.osp.utilities.ConfigurationUtil;


import org.apache.oro.text.perl.Perl5Util;

/**
 * This class validates the order special instructions field for forbidden words
 * from the database.
 * @author Jeff Penney
 *
 */
public class ValidateForKeywords
{   
    private static final String GET_KEYWORDS = "GET KEYWORDS";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateForKeywords");
    
    /**
    * 
    * @description Checks the special instructions section of each item in an order for
    * keywords that are not allowed
    * @param request A ValidationRequest (Map) containing an Order with the name ORDER
    * @exception throws a general Exception
    * @return Returns a ValidationResponse (Map) containing the order (ORDER) and a response code (RESPONSE_CODE)
    *  
    */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException
    {
        logger.info("Starting keyword validation");
    
        OrderVO order = null;
        Connection conn = null;
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String aribaOrigin1 = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_ARIBA1");        
        String aribaOrigin2 = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_ARIBA2");
        String funeralCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "OCCASION_TYPE_FUNERAL");        
        
        ValidationResponse response = new ValidationResponse();
        List wordList = new ArrayList();
        // Try source code cache
        KeywordHandler keywordHandler = (KeywordHandler)CacheManager.getInstance().getHandler("CACHE_NAME_KEYWORDS");
        if(keywordHandler != null)
        {
            wordList = keywordHandler.getKeywords();
        }
        // Get the source code data from the database
        else
        {     
            DataRequest dataRequest = new DataRequest();
            // Get the keywords from the database
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(GET_KEYWORDS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
        
            while(rs.next())
            {
                wordList.add(rs.getObject(1));
            }
        }
        
        // Loop through  items
        OrderDetailsVO item = null;
        boolean breakOut = false;
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        String specialInstructions = null;
        Perl5Util regexp = new Perl5Util();
        String keyWord = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            // check special instructions for each word

            // Only check for Ariba and Funeral orders
            String occasion = item.getOccassionId();
            if(occasion == null) occasion = "";
            String origin = order.getOrderOrigin();
            if(origin == null) origin = "";
            // JMP 12/23/2003 - Removed Ariba/Funeral check per Tim
            //if(origin.equalsIgnoreCase(aribaOrigin1) || origin.equalsIgnoreCase(aribaOrigin2) || occasion.equals(funeralCode))
            //{
                boolean match = false;
                for(int i = 0;i < wordList.size(); i++) 
                {
                    specialInstructions = item.getSpecialInstructions();
                    if(specialInstructions == null)
                    {
                        specialInstructions = "";
                    }

                    keyWord = (String)wordList.get(i);
                    // Replace $ with \$
                    keyWord = FieldUtils.replaceAll(keyWord, "$", "\\$");
                
                    match = regexp.match(("/" + keyWord + "/i"), specialInstructions);

                    if(match)
                    {
                        response.put(ValidationConstants.RESPONSE_SPEC_INSTR_CONTAINS_KEYWORDS + item.getLineNumber(), ValidationConstants.RESPONSE_SPEC_INSTR_CONTAINS_KEYWORDS);
                        breakOut = true;
                        break;
                    }
                }

                // break out of the loop if one of the items failed the test
                //if(breakOut)
                //{
                //    break;
                //}
            //}
        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending keyword validation");
        
        return response;        
    }
}