package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.ordervalidator.util.DispatchCancelledOrder;
import com.ftd.osp.ordervalidator.util.ValidateOrderPrice;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.client.util.PASParamUtil;
import com.ftd.pas.core.domain.ProductAvailVO;

/**
 * This class validates items in an order for a florist that can delivery to the 
 * given zip codes
 * @author Jeff Penney
 *
 */
public class ValidateFlorist 
{
    private static final String GET_GLOBAL_GNADD_SETTINGS = "GET GLOBAL GNADD SETTINGS";
    private static final String GET_GOTO_FLORIST_FLAG = "GET_GOTO_FLORIST_FLAG";    
    private static final String CHECK_ZIP_CODE_AVAIL = "CHECK ZIP CODE AVAIL";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";


    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static final String ZIP_CODE = "ZIP_CODE";
    private static final String PRODUCT_ID = "PRODUCT_ID";

    private static final String ORDER_ORIGIN_AMAZON = "ORDER_ORIGIN_AMAZON";
    private static final String ORDER_VALIDATOR = "ORDER_VALIDATOR";
    private static final String USER_NAME = "auto";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateFlorist");
    
    /**
     * Validates that a florist is available to deliver this order 
     * @param request Contains an Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing any errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, ParseException, Exception
    {
        logger.info("Starting florist validation");
        
        ValidationResponse response = new ValidationResponse();
        OrderVO order = null;
        Connection conn = null;
        
        //the following variables are for Amazon.com orders only
        boolean hasCancelledItems = false;
        boolean carrierDelivered = false;
        boolean changeToCarrierDelivered = false;
        boolean orderAlreadyBeenRemoved = false;
        boolean isAmazonOrder = false;
        boolean isMercentOrder = false;

        boolean isAmazonAutoCancel = false;
        boolean isMercentAutoCancel = false;
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
            
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }                

        DataRequest dataRequest = new DataRequest();

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String sdShipMethod = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "SAME_DAY_SHIP_METHOD");          
        String floristShipMethod = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "FLORIST_DELIVERED_METHOD");          
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");      

        // Get the globa GNADD setting
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_GLOBAL_GNADD_SETTINGS);
        CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
        rs.reset();
        String gnaddLevel = null;
        java.util.Date gnaddDate = null;
        while(rs.next())
        {
            gnaddLevel = String.valueOf(rs.getObject(1));
            
            if(rs.getObject(2) != null)
            {
                gnaddDate = FieldUtils.formatStringToUtilDate(String.valueOf(rs.getObject(2)));
            }
        }
        
        //default to off
        if(gnaddLevel == null)
        {   
            gnaddLevel = "0";
        }
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
        //check to see if this is an Amazon order
        if(order.getOrderOrigin().equalsIgnoreCase( configUtil.getProperty(METADATA_CONFIG_FILE_NAME, ORDER_ORIGIN_AMAZON)))
        {
          isAmazonOrder = true;
        }
        isAmazonAutoCancel = configUtil.getFrpGlobalParm(ValidationConstants.AMZ_GLOBAL_CONTEXT, ValidationConstants.AMZ_AUTO_CANCEL).equalsIgnoreCase("Y");
        
        if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	isMercentOrder = true;
        }
        isMercentAutoCancel = configUtil.getFrpGlobalParm(ValidationConstants.MERCENT_GLOBAL_CONTEXT, ValidationConstants.MERCENT_AUTO_CANCEL).equalsIgnoreCase("Y");
        
        if(isAmazonOrder || isMercentOrder)
        {
            //need to add a check that if this order has already been removed
            //do not send cancellation email again
            if(order.getStatus().equals(String.valueOf(OrderStatus.REMOVED)))
            {
            	orderAlreadyBeenRemoved = true;
            }
        }

        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        String specialInstructions = null;
        OrderDetailsVO item = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        String florstId = null;
        String productType = null;
        OEDeliveryDateParm parms = null;
        
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            
            if(isAmazonOrder || isMercentOrder)
            {
                //need to add a check that if this item has already been removed
                //do not send cancellation email again
                if(item.getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
                {
                	orderAlreadyBeenRemoved = true;
                }  
            }

            //for Amazon/Mercent orders only: 
            //need to add a check for same day freshcuts that are florist delivered, 
            //if there is not a florist available in the recipients zip code, then we need
            //to check to see if this product can be carrier delivered.  If it can be, then we
            //need to change this item to a carrier delivered product.  If there is not a florist
            //available in the recipient's zip code, and this product is not carrier delivered, 
            //then this order will be cancelled.
            //This check only applies to products with product type of Same Day Freshcut (SDFC).
            
            if(isAmazonOrder || isMercentOrder)
            {
                // Get product details
                dataRequest.setConnection(conn);
                dataRequest.setStatementID(GET_PRODUCT_DETAILS);
                dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductId());
                rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
                dataRequest.reset();
        
                String shipMethodCarrier = null;
                String prodType = null;
                //if rs < 1 it's possible that the productId that is being used as the input parameter
                //is an upsell product and does not have information itself in product master thus causing an excpetion.
                if (rs.getRowCount() < 1 )
                {
                   throw new ValidationException(ValidationConstants.UPSELL_ERROR);
                }
              
                while(rs.next())
                {
                    prodType = (String)rs.getObject(8);
                    shipMethodCarrier = (String)rs.getObject(56);
                }
    
                if(shipMethodCarrier == null) shipMethodCarrier = "N";
                
                if(prodType.equalsIgnoreCase(ValidationConstants.SAME_DAY_FRESH_CUT) )
                {
                  if( shipMethodCarrier.equalsIgnoreCase("Y") )
                  {
                    carrierDelivered = true;
                  }
                }
                
            }//end isAmazonOrder
            
            // Get the zip code availability flags
            // Only check for florist delivered items
            String shipMethod = item.getShipMethod();
            boolean checkFlorist = false;
            
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    String countryId = null;
                    countryId = recipAddress.getCountry();
                    if(countryId == null) countryId = "";

                    String zipCode = null;
                    zipCode = recipAddress.getPostalCode();
                    if(zipCode == null) zipCode = "";
                    
                    String productId = null;
                    productId = item.getProductId();
                    if(productId == null) productId = "";


                    parms = DeliveryDateUTIL.getParameterData(countryId, productId, zipCode, canadaCountryCode, usCountryCode, item.getLineNumber(), item.getOccassionId(), conn,recipAddress.getStateProvince());
                    productType = parms.getProductType();
                    if(productType == null) productType = "";


                    if(shipMethod == null || shipMethod.equals(sdShipMethod) || shipMethod.equals(floristShipMethod) || productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) || productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG))
                    {
                        checkFlorist = true;
                    }

                    // only run these check for florist delivered products
                    if(checkFlorist)
                    {

                        String zip = zipCode;
                        boolean floristAvailable = false;
                        if(countryId.equals(usCountryCode) || countryId.equals(canadaCountryCode))
                        {
                            if(countryId.equals(usCountryCode) && zipCode.length() > 5)
                            {
                                zip = zipCode.substring(0, 5);
                            }
                            else if(countryId.equals(canadaCountryCode)  && zipCode.length() > 3)
                            {
                                zip = zipCode.substring(0, 3);
                            }
                        }
                    
                        dataRequest.setStatementID(CHECK_ZIP_CODE_AVAIL);
                        dataRequest.addInputParam(ValidationConstants.ZIP_CODE, zip);
                        rs = (CachedResultSet)vdau.execute(dataRequest);
                        rs.reset();
                        String floristZipCode = null;
                        String zipGNADDFlag = null;
                        String sundayDeliveryFlag = null;
                        while(rs.next())
                        {
                            floristZipCode = (String)rs.getObject(1);
                            zipGNADDFlag = (String)rs.getObject(2);
                            sundayDeliveryFlag = (String)rs.getObject(3);
                        }

                        if(sundayDeliveryFlag == null) sundayDeliveryFlag = "N"; 
/*
                        // if the zip code exists and GNADD level is zero then a florist is
                        // available
                        if(floristZipCode != null && (zipGNADDFlag == null || !zipGNADDFlag.equals(ValidationConstants.YES)) &&
                           gnaddLevel.equals("0"))
                        {
                            // A florist is available
                            floristAvailable = true;                        
                        }
                        else if(shipMethod == null || shipMethod.equals(GeneralConstants.DELIVERY_SAME_DAY_CODE))
                        {
                            // No florist available
                            response.put(ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE + item.getLineNumber(), ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE);
                        }
*/

                        String floristId = item.getFloristNumber();
                        if(floristId == null) floristId = "";

                        // if the country is canada then only use the first three characters of
                        // the zip code - Defect#402
                        String trunatedZip = zipCode;
                        if(countryId != null && countryId.equals(canadaCountryCode) && zipCode.length() > 3)
                        {
                            trunatedZip = zipCode.substring(0, 3);
                        }
                        else if(countryId != null && countryId.equals("US") && zipCode.length() > 5)
                        {
                            trunatedZip = zipCode.substring(0, 5);
                        }

                        // Get florist data
                        dataRequest.setStatementID(GET_GOTO_FLORIST_FLAG);
                        dataRequest.addInputParam(ZIP_CODE, trunatedZip);
                        dataRequest.addInputParam(PRODUCT_ID, productId);
                        dataRequest.addInputParam("CITY", "");
                        dataRequest.addInputParam("STATE", "");
                        dataRequest.addInputParam("PHONE", "");
                        dataRequest.addInputParam("ADDRESS", "");
                        dataRequest.addInputParam("FLORIST_NAME", "");
                        dataRequest.setConnection(conn);
        
                        rs = (CachedResultSet)vdau.execute(dataRequest);
                        rs.reset();
                        dataRequest.reset();

                        String id = null;
                        String sundayFlag = null;
                        String products = null;
                        String currentBlock = null;
                        // String block = null;
                        boolean foundFlorist = false;
                        
                        boolean hasActiveFlorist = false;
                        boolean hasSuspendedFlorist = false;
                        boolean hasSundayDelivery = false;
                        
                        // If no zips are found for the florist we assume international and let it through
                        if(rs.getRowCount() == 0)
                        {
                            hasActiveFlorist = true;
                        }
                        
                        while(rs.next())
                        {
                            id = (String)rs.getObject(1);
                            sundayFlag = (String)rs.getObject(10);
                            currentBlock = (String)rs.getObject(12);
                            products = (String)rs.getObject(13);
                            
                            // If florist id is specified for item
                            if(floristId.length() > 0)
                            {
                                if( floristId.equals(id) && !isAmazonOrder && !isMercentOrder)
                                {
                                    foundFlorist = true;
                                    
                                    if(currentBlock == null || currentBlock.equals("") || currentBlock.equals("P"))
                                    {
                                        hasActiveFlorist = true;
                                        
                                        if(sundayFlag != null && !sundayFlag.equals("N"))
                                        {
                                            hasSundayDelivery = true;
                                        }
                                    }
                                    
                                    if(currentBlock != null && currentBlock.equals("M"))
                                    {
                                        hasSuspendedFlorist = true;
                                    }
                                    
                                    break;
                                }
                            }
                            // No florist specified for item
                            else
                            {
                                
                                //for Amazon/Mercent orders, since florist numbers
                                //aren't passed over, we just need to check and
                                //see if there are any florists available in this
                                //zip code.  If florist is found, set foundFlorist
                                //flag to true.  HP will figure out which florist
                                //to send it to.
                                if(isAmazonOrder || isMercentOrder)
                                {
                                    foundFlorist = true;
                                    // break;
                                }
                                
                                if(currentBlock == null || currentBlock.equals("") || currentBlock.equals("P"))
                                {
                                    hasActiveFlorist = true;
                                    
                                    if(sundayFlag != null && !sundayFlag.equals("N"))
                                    {
                                        hasSundayDelivery = true;
                                    }
                                }
                                
                                if(currentBlock != null && currentBlock.equals("M"))
                                {
                                    hasSuspendedFlorist = true;
                                }
                            }
                        }
                        
                        logger.info("found florist: " + foundFlorist);
                        logger.info("hasCancelledItems: " + hasCancelledItems);
                        logger.info("order origin: " + order.getOrderOrigin());
                        
                        logger.info("has active florists: " + hasActiveFlorist);
                        logger.info("has suspended florists: " + hasSuspendedFlorist);
                        
                        // if(sundayFlag == null) sundayFlag = "N";
                        if(products == null) products = "";
                        // if(block == null) block = "";
                        // if(block.equalsIgnoreCase("P")) block = "";

                        //For Amazon.com/Mercent orders
                        //If recipient zip code not serviced by a Florist and
                        //product can not be delivered by a carrier, then
                        //cancel the order and create an adjustment feed record.
                        //But only cancel the order if this is the first time
                        //through validation and the order has not already been
                        //cancelled.
                        //
                        //If recipient zip code not serviced by a Florist and
                        //product can be delivered by a carrier, then this item needs to be changed
                        //from florist delivered order to carrier delivered order.
                        if (!hasCancelledItems && !foundFlorist && (isAmazonOrder || isMercentOrder)) 
                        {
                            if(!carrierDelivered  && !orderAlreadyBeenRemoved)
                            {
                            	logger.info("product not service by a Florist");
                             	if (isAmazonOrder && isAmazonAutoCancel) {
                                    cancelOrder(order, item, conn, carrierDelivered);
                                    hasCancelledItems = true;
                                    logger.info("hasCancelledItems: " + hasCancelledItems);                                    
                                    item.setAmazonStatus("CANCELLED");
                             	}
                             	else if (isMercentOrder && isMercentAutoCancel) {
                                    cancelOrder(order, item, conn, carrierDelivered);
                                    hasCancelledItems = true;
                                    logger.info("hasCancelledItems: " + hasCancelledItems);                                    
                                    item.setMercentStatus("CANCELLED");
                             	}
                            }
                            else if(carrierDelivered)
                            {
                              changeToCarrierDelivered = true; 
                            }
                        }//end if ()
                        
                        // To determine if a florist is available check the block flag
                        if((!foundFlorist && floristId.length() > 0) 
                            || (!hasActiveFlorist && gnaddLevel.equals("0"))
                            || (!hasActiveFlorist && !gnaddLevel.equals("0") && !hasSuspendedFlorist))
                        {
                            response.put(ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE + item.getLineNumber(), ValidationConstants.RESPONSE_NO_FLORIST_AVAILABLE);
                        }//end if

                        // Get codification details
                        parms = new OEDeliveryDateParm();
                        DeliveryDateUTIL.getCodificationDetails(parms, countryId, productId, zipCode, canadaCountryCode, conn);
                    
                        // Check to make sure the selected florist can deliver this 
                        // product if it is codified
                        if(parms.getCodifiedProduct() != null)
                        {
                        logger.info("codified product: " + parms.getCodifiedProduct());
                        logger.info("ship Method: " + shipMethod);
                        logger.info("codified available: " + parms.getCodifiedAvailable());
                        }
                         //if we have an Amazon/Mercent order and the shipMethod is Florist Only and the zip code has a block
                        //the order is cancelled.
                        //But only cancel the order if this is the first time through validation and the order has not already been
                        //cancelled.
                         if( (isAmazonOrder || isMercentOrder) && !orderAlreadyBeenRemoved && ( shipMethod == null || shipMethod.equals(ValidationConstants.FLORIST_ONLY) )&& !hasActiveFlorist && !carrierDelivered )
                        {
                        	logger.info("Florist Only delivery - zipcode blocked");
                          	if (isAmazonOrder && isAmazonAutoCancel) {
                              cancelOrder(order, item, conn, carrierDelivered);
                              hasCancelledItems = true;                              
                              item.setAmazonStatus("CANCELLED");
                          	}
                          	else if (isAmazonOrder && isMercentAutoCancel) {
                              cancelOrder(order, item, conn, carrierDelivered);
                              hasCancelledItems = true;                             
                              item.setMercentStatus("CANCELLED");
                            	} 
                          		
                        }
        
                        if(parms.getCodifiedProduct() != null && parms.getCodifiedProduct().equals("Y") && (shipMethod == null || shipMethod.equals(GeneralConstants.DELIVERY_SAME_DAY_CODE) || shipMethod.equals(ValidationConstants.FLORIST_ONLY)))
                        {
                            // If a florist is selected then check product string.
                            // Otherwise use the CSZ_PRODUCTS table 
                            if((foundFlorist && products.indexOf(productId) == -1))
                            {
                                // Cancel Amazon orders for invalid florist
                                // Do not put the order into scrub
                                if (!hasCancelledItems && (isAmazonOrder || isMercentOrder)) 
                                {
                                   //check to see if this item can be carrier delivered
                                   //if it can't be delivered by a carrier, then cancel this order.
                                   //If it can be delivered by a carrier, then this item needs to be changed
                                   //from florist delivered order to carrier delivered order.
                                   //if(!carrierDelivered).
                                   //But only cancel the order if this is the first time
                                   //through validation and the order has not already been
                                   //cancelled.
                                   if(!carrierDelivered && !orderAlreadyBeenRemoved && (parms.getCodifiedAvailable() == null || parms.getCodifiedAvailable().equals("N")) )
                                   {
                                	  logger.info("The selected florist does not have this product");
                                     if (isAmazonAutoCancel) {
                                         // Amazon order cancellation
                                         cancelOrder(order, item, conn, carrierDelivered);
                                         hasCancelledItems = true;                                         
                                         item.setAmazonStatus("CANCELLED");
                                     }
                                     else if (isMercentAutoCancel) {
                                         // Amazon/Mercent order cancellation
                                         cancelOrder(order, item, conn, carrierDelivered);
                                         hasCancelledItems = true;                                         
                                         item.setMercentStatus("CANCELLED");
                                     }
                                     continue;
                                   }
                                   else if(carrierDelivered)
                                   {
                                     changeToCarrierDelivered = true; 
                                   }
                                }//end if
                                else
                                  response.put(ValidationConstants.RESPONSE_FLORIST_NO_PRODUCT + item.getLineNumber(), ValidationConstants.RESPONSE_FLORIST_NO_PRODUCT);
                            }
                            else if(floristId.length() == 0 && parms.getCodifiedAvailable() != null && parms.getCodifiedAvailable().equals("N"))
                            {
                                // Cancel Amazon/Mercent orders for invalid florist
                                // Do not put the order into scrub
                                if (!hasCancelledItems && (isAmazonOrder || isMercentOrder)) 
                                {
                                   //check to see if this item can be carrier delivered
                                   //if it can't be delivered by a carrier, then cancel this order.
                                   //If it can be delivered by a carrier, then this item needs to be changed
                                   //from florist delivered order to carrier delivered order.
                                   //But only cancel the order if this is the first time
                                   //through validation and the order has not already been
                                   //cancelled.
                                   if(!carrierDelivered && !orderAlreadyBeenRemoved)
                                   {
                                      if (isAmazonAutoCancel) {
                                         // Amazon order cancellation
                                         cancelOrder(order, item, conn, carrierDelivered);
                                         hasCancelledItems = true;
                                         logger.info("Floral product not available for recipient zip code");
                                         item.setAmazonStatus("CANCELLED");
                                      }
                                      else if (isMercentAutoCancel) {
                                          // Mercent order cancellation
                                          cancelOrder(order, item, conn, carrierDelivered);
                                          hasCancelledItems = true;
                                          logger.info("Floral product not available for recipient zip code");
                                          item.setMercentStatus("CANCELLED");
                                       }
                                      continue;
                                   }
                                   else if(carrierDelivered)
                                   {
                                     changeToCarrierDelivered = true; 
                                   }
                                }//end if
                                else
                                {
                                    // 3/9/04 -emueller, I changed the message that this sets. Defect#486
                                    response.put(ValidationConstants.RESPONSE_PRODUCT_NOT_AVAIL_IN_ZIP + item.getLineNumber(), ValidationConstants.RESPONSE_PRODUCT_NOT_AVAIL_IN_ZIP);
                                    
                                }//end else ()
                            }
                        }

                        //For Amazon Orders and Mercent orders only, if this item has a product type of Same Day Freshcut,
                        //and a florist is not available, but this item can be carrier delivered
                        //then the item needs to be updated from a florist delivered item
                        //to carrier delivered item.
                        if(changeToCarrierDelivered)
                        {
                          ProductAvailVO paVO = null;
                          
                          //update ship method
                          item.setShipMethod(ValidationConstants.SAME_DAY_CARRIER);
                                        
                          //move service fee to shipping fee
                          if( item.getServiceFeeAmount() != null && !productType.equals(ValidationConstants.SAME_DAY_FRESH_CUT))
                          {
                            item.setShippingFeeAmount(item.getServiceFeeAmount());
                            item.setServiceFeeAmount("0");
                          }
                          
                          //recalculate the delivery date
                          parms = DeliveryDateUTIL.getParameterData(recipAddress.getCountry(), item.getProductId(), 
                                                  recipAddress.getPostalCode(), canadaCountryCode, usCountryCode, item.getLineNumber(),
                                                  item.getOccassionId(), conn, recipAddress.getStateProvince());
              
                          parms.setOrder(order);
                         
                          //DDUTIL to PAS.
                          //#PAS instead of DDUTIL (fetch the delivery date from ship method using PAS instead of DDUTIL)
                          List<ProductAvailVO> productAvailVOs = new ArrayList<ProductAvailVO>();
                          SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                                                   
                          productAvailVOs = PASServiceUtil.getProductAvailableDates(item.getProductId(), null, 
                        		  recipAddress.getPostalCode(), recipAddress.getCountry(), order.getSourceCode(), DataMassager.getDeliveryDaysOutMAX(vdau, conn));
                        		 
                          //For Orders that need the delivery date calcualted, like Amazon.com orders,
                          //need to loop through dates
                          //returned and set first date that matches ship method
                          //stored in OrderDetailsVO for this item
                          int j = productAvailVOs.size();
                          for( int i = 0; i < j ; i++)
                          {
                              boolean found = false;
                              paVO = productAvailVOs.get(i);
                              String deliveryDate = sdf.format(paVO.getDeliveryDate().getTime());
                             
                              
                              List sms = PASParamUtil.getShippingMethods(paVO);
                              
                              String sm = null;
                              for(int m = 0; m < sms.size(); m++)
                              { 
                                sm = (String) sms.get(m);
                                
                                 if ( sm.equalsIgnoreCase( item.getShipMethod()))
                                  {        
                                      found = true;
                                      item.setDeliveryDate(deliveryDate);
                                      logger.info("Delivery date by PAS: " + deliveryDate );
                                      
                                      break;    
                                  }
                                                                  
                              }
                              if (found)
                              {
                                break;
                              }
            
                          }
                          
                        }
                        
                        // If the product type is SDFC and a florist is available 
                        // then the ship method must be florist delivery
                        String available = parms.getCodifiedAvailable();
                        if(available == null) available = "N";
                        if(productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC) && 
                           (shipMethod == null || !shipMethod.equals(GeneralConstants.DELIVERY_SAME_DAY_CODE)) &&
                           !available.equals("N") && !available.equalsIgnoreCase("NA"))
                        {
                            response.put(ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED + item.getLineNumber(), ValidationConstants.RESPONSE_FLORIST_DELIVERY_REQUIRED);
                        }
                        
                        // Use the florist_master table to determine if a florist
                        // is available for sunday delivery.  If a floirst is not selected
                        // then use the generic csz_avail table to determine if any 
                        // florist can deliver to this zip code.
                        // Convert delivery date to a Calendar
                        String deliveryDateStr = item.getDeliveryDate();
                        if(deliveryDateStr == null) deliveryDateStr = "";
                        java.util.Date deliveryDate = null;
                        Calendar calDeliveryDate = null;
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                        try
                        {
                            deliveryDate = sdf.parse(deliveryDateStr);
                            calDeliveryDate = Calendar.getInstance();
                            calDeliveryDate.setTime(deliveryDate);
                        }
                        catch(ParseException pe)
                        {
                            logger.error("ValidateFlorist: Could not parse delivery date...");
                        }
            
                        // Check to see if the selected florist can delivery on 
                        // Sunday if the delivery date is Sunday
                        if(floristId.length() == 0 && calDeliveryDate != null && sundayDeliveryFlag.equals("N") && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                        {
                            response.put(ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY + item.getLineNumber(), ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY);
                        }
                        else if(foundFlorist && calDeliveryDate != null && !hasSundayDelivery && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                        {
                            response.put(ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY + item.getLineNumber(), ValidationConstants.RESPONSE_FLORIST_NO_SUNDAY);
                        }
                    }                    
                }
            }
            
          hasCancelledItems = false;
        }//end while (item iterator hasNext())
        
        
        
        // if all the line items have been marked removed, then the order should
        // also be marked removed
        if (isAllLineItemsRemoved(order))
        {
            logger.info("all line items have been removed");
            logger.info("Old status: " + order.getStatus());
            //removeOrder(order, conn);
        }//end if (isAllLineItemsRemoved())                    
     
        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending florist validation");
        
        return response;
    }


    /*
     * This method is VERY similar to the method of the same name within
     * com.ftd.osp.ordervalidator.validationhandlers.ValidateDeliveryDate.java.
     */
    private static void cancelOrder(OrderVO order, OrderDetailsVO item, Connection connection, boolean isCarrierDelivered)
    					throws ValidationException
    {
        // update order item statuses
        item.setStatus(String.valueOf(OrderStatus.REMOVED_ITEM));
        
        // update order in scrub
        updateOrderScrub(order, item, connection);
        
        // validate prices
        try
        {
            boolean value = ValidateOrderPrice.validate(order, item, connection);
        }
        catch (Exception e) 
        {
            logger.error("exception thrown during order price validation.", e);
            throw new ValidationException("Unable to validate order price.");
        }//end catch
        
        // dispatch remover order to HP
        DispatchCancelledOrder dispatcher = new DispatchCancelledOrder();
        try{
          logger.info("dispatching cancelled order number: " + item.getExternalOrderNumber());
          dispatcher.dispatchOrder(order, item, ValidationConstants.MSG_STATUS_DIPSATCHER, connection);
        }
        catch(Exception e)
        {
          logger.error("exception thrown when dispatching cancelled order. " + e.toString());
        }
        
    }//end cancelAmazonOrder
    
    
    /*
     * 
     */
    private static boolean isAllLineItemsRemoved(OrderVO order) 
                                                    throws ValidationException 
    {
        Iterator i = order.getOrderDetail().iterator();
        OrderDetailsVO line = null;
        
        while (i.hasNext()) 
        {
            line = (OrderDetailsVO) i.next();
            if (!line.getStatus().equals(
                                    String.valueOf(OrderStatus.REMOVED_ITEM)))
            {
                return false;
            }//end if
            
        }//end while
        
        return true;
        
    }//end method isAllLineItemsRemoved
    
    
    /*
     * 
     */
    private static void removeOrder(OrderVO order, Connection connection)
                                                    throws ValidationException
    {
        order.setStatus(String.valueOf(OrderStatus.REMOVED));
        updateOrderScrub(order, null, connection);
        
    }//end method removeOrder()
    

    /*
     * 
     */
    private static void updateOrderScrub(OrderVO order, 
                                         OrderDetailsVO line, 
                                         Connection connection) 
                                                    throws ValidationException
    {
        OrderVO clone = null;
        
        // Update order with system name
        order.setCsrId(ORDER_VALIDATOR);
    
        // Create a working copy of the order.  This copy is necessary so that
        // in the subsequent code, a single item can be set on the order.  This
        // will prevent the ScrubMapperDAO.updateOrder(order) from resaving the
        // entire order.  Instead it will only update the header and the single
        // line item in question.
        try
        {
            clone = (OrderVO) new ObjectCopyUtil().deepCopy(order);
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to update order in scrub, due to exception "
                         + "thrown during deepCopy of order.", e);
            throw new ValidationException("Unable to update order.");
        }//end catch

        // Update order with individual item
        if(line != null)
        {
            List singleItemList = new ArrayList();
            try
            {
                singleItemList.add(
                        (OrderDetailsVO) new ObjectCopyUtil().deepCopy(line));
            }//end try
            catch (Exception e) 
            {
                logger.error("Unable to update order in scrub, due to exception"
                             + " thrown during deepCopy of line item.", e);
                throw new ValidationException("Unable to update order.");
            }//end catch
        
            clone.setOrderDetail(singleItemList);
            
        }//end if (line != null)

        // Update scrub database
        try 
        {
            new ScrubMapperDAO(connection).updateOrder(clone);
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to update order in scrub, due to exception "
                         + "thrown during DAO.updateOrder().", e);
            throw new ValidationException("Unable to update order.");
        }//end catch
        
        
    }//end method updateOrderScrub
    
    
}//end class