package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

public class ValidateQMSResult 
{
    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateQMSResult");

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    /**
     * Validates the result of a QMS query on an address
     * @param request Contains an Order object
     * @exception Exception if a database error occurs.
     * @return Returns any validation errors.
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
           SQLException, Exception
  {    
        logger.info("Starting QMS Result validation");

        OrderVO order = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        
        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }
        ValidationResponse response = new ValidationResponse();
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        String avsResult = null;
        OrderDetailsVO item = null;
        ProductVO productVO = null;
        OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
        
        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();        
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");

        // Loop through the order's items
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        AVSAddressVO avsAddress = null;
        String country = null;

        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            productVO = orderValidatorDAO.getProductDetails(item.getProductId());

            if(productVO == null)
                continue;

            // Check for AVS enabled/disabled flags for florist and dropship.
            // If AVS is disabled for this type of order, skip Address validation for this order
            String orderType = "";
            GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
            if (item.getShipMethodCarrierFlag() != null && item.getShipMethodCarrierFlag().equals("Y")) {
            	//dropship item
            	if (!isAddressVerificationEnabledDropship()) {
            		logger.info("global parm AVS_CONFIG.verify_address_dropship_orders if off.  skipping address validation");
            		continue;
            	}
            } else {
            	//florist item
            	if (!isAddressVerificationEnabledFlorist()) {
            		logger.info("global parm AVS_CONFIG.verify_address_floral_orders if off.  skipping address validation");
            		continue;
            	}
            }
            if(!(item.getProductType().equals(ValidationConstants.PRODUCT_TYPE_SERVICES) && productVO.getProductSubType().equals(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP)))
            {

            	
                // Get country code
                if(item.getRecipients() != null && item.getRecipients().size() > 0)
                {
                    recipient = (RecipientsVO)item.getRecipients().get(0);

                    if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                    {
                        recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                        country = recipAddress.getCountry();
                    }
                }

                // Validate for US orders only
                if (!usCountryCode.equals(country)){
                	logger.info("skipping address validation for non-US country");
                	continue;
                }

                avsAddress = item.getAvsAddress();

                if (avsAddress == null) {
                	logger.info("found null AVS address.  This should never happen but there is nothing invalid about it");
                    continue;
                }

                logger.info("Validating address ID=" + avsAddress.getAvsAddressId());
                
                /**
                 * Use Case 23175.  Skip address verification if Address verification was performed on the website, regardless of the result
                 */
                if (avsAddress.getAvsPerformed() != null
                		&& avsAddress.getAvsPerformed().equals(GeneralConstants.AVS_YES)
                		&& avsAddress.getAvsPerformedOrigin() != null
                		&& avsAddress.getAvsPerformedOrigin().equals(GeneralConstants.AVS_PERFORMED_WEBSITE)) {
                	logger.info("AVS was already performed on the website.  skipping address validation regardless of if it's result was PASS or FAIL");
                	continue;
                }

                avsResult = avsAddress.getResult();
                // A result of pass or an override constitutes a clean address
                if(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS.equals(avsResult) 
                        || GeneralConstants.AVS_YES.equals(avsAddress.getOverrideflag())) {
                    logger.info("skipping address validation because avsResult = " + avsResult + " and avsOverrideFlag = " + avsAddress.getOverrideflag());
                	continue;
                }

                // iterate through the scores to evaluate/look for specific errors. If none found then use the generic error.
                boolean errorFound = false;
                Map <String, String>reasonToError = new HashMap<String,String>();
                reasonToError.put(GeneralConstants.AVS_SCORE_ADDRESS_LINE_DIFFERENT, ValidationConstants.RESPONSE_QMS_ADDRESS_INVALID);
                reasonToError.put(GeneralConstants.AVS_SCORE_CITY_DIFFERENT, ValidationConstants.RESPONSE_QMS_CITY_INVALID);
                reasonToError.put(GeneralConstants.AVS_SCORE_STATE_DIFFERENT, ValidationConstants.RESPONSE_QMS_CITY_STATE_INVALID);
                reasonToError.put(GeneralConstants.AVS_SCORE_ZIP_DIFFERENT, ValidationConstants.RESPONSE_QMS_ZIP_INVALID);
                // no specific error for zip+4

                if (avsAddress.getScores() != null)
                {
                    for (AVSAddressScoreVO score : avsAddress.getScores())
                    {
                        int val = Integer.valueOf(score.getScore());
                        if (val > 0)
                        {
                            logger.info("Address has error " + score.getReason());

                            String error = reasonToError.get(score.getReason());
                            if (error != null)
                            {
                                errorFound = true;
                                response.put(error + item.getLineNumber(), error);
                            }
                        }
                    }
                }

                // if no specific error was found issue the generic error
                if (!errorFound)
                    response.put(ValidationConstants.RESPONSE_QMS_GENERIC_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_QMS_GENERIC_INVALID);
            }                
        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending QMS Result validation");
       
        return response;
    }
    
    private static boolean isAddressVerificationEnabledDropship() {
    	GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
    	String dropshipActive = gph.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_DROPSHIP_ORDERS);
    	return (dropshipActive != null && dropshipActive.equalsIgnoreCase("on") ? true : false );
    	
    }
    
    private static boolean isAddressVerificationEnabledFlorist() {
    	GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
    	String floristActive = gph.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_FLORIST_ORDERS);
    	return (floristActive != null && floristActive.equalsIgnoreCase("on") ? true : false );
    	
    }
}