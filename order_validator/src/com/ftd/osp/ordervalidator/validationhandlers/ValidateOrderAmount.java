/*
 * 
 */
package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.util.ValidationHandlerUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

/**
 * This class validates various order and item dollar amount fields.  If the order 
 * total is over a certain amount determined by a database row, then an error is
 * returned.  If the product amount is zero an error is returned.  If the order total
 * is zero then an error is returned.
 * @author Jeff Penney
 *
 */
public class ValidateOrderAmount 
{
    private static final String GET_GLOBAL_PARAM = "GET_GLOBAL_PARAM_VALIDATION";
    
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateOrderAmount");
    
    //PRODUCT TYPES
    private static final String PRODUCT_TYPE_FRESHCUT = "FRECUT";
    private static final String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SDFC";
    private static final String PRODUCT_TYPE_FLORAL = "FLORAL";
    private static final String PRODUCT_TYPE_SAMEDAY_GIFT = "SDG";
    private static final String PRODUCT_TYPE_SPECIALTY_GIFT = "SPEGFT";
    
    /**
     * Validates against the following rules.  If the order total exceeds a preset max fail.
     * If the order total is zero fail.  If the total of all items in the order
     * is zer fail.
     * @param request Contains the Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing any errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {    
        logger.info("Starting order amount validation");
    
        OrderVO order = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }    

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String bulkOrderOrigin = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_BULK");        
        String occasionTypeFuneral = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "OCCASION_TYPE_FUNERAL");

        ValidationResponse response = new ValidationResponse();

        String orderTotalStr = order.getOrderTotal();
        if(orderTotalStr == null) orderTotalStr = "0";
        
        // defect 1962. Validate alternate payment type orders
        ValidationHandlerUtil vhu = new ValidationHandlerUtil();
        boolean isAltPayOrder = vhu.isAltPayOrder(order);
        boolean isOverAllowedAuth = false;
        String paymentTypeDesc = null;

        if(isAltPayOrder) {
            // check if order total exceeds allowed total
            isOverAllowedAuth = FTDCommonUtils.isOrderOverAllowedAuth(order, conn);
            paymentTypeDesc = vhu.getAltPayPaymentTypeDesc(order);
            if (isOverAllowedAuth) {
                response.put(ValidationConstants.RESPONSE_AP_OVER_ALLOWED_TOTAL, 
                             paymentTypeDesc + ValidationConstants.RESPONSE_AP_OVER_ALLOWED_TOTAL);
            }
        }

        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        items = order.getOrderDetail();
        it = items.iterator();
        boolean isFuneral = false;
        boolean isPremier = false;
        int itemCount = 0;
        String itemOccasion = null;
        String preferredPartner = null;
        OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
        
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            itemCount++;

            // determine if the order contains a product from the premier collection
            if (!isPremier && item.getPremierCollectionFlag() != null && item.getPremierCollectionFlag().equalsIgnoreCase("Y"))
            {
                isPremier = true;
            }
            
            // Determine if the order is associated with a Preferred Partner (e.g., USAA)
            if (preferredPartner == null && item.getPreferredProcessingPartner() != null) {
                preferredPartner = item.getPreferredProcessingPartner();
            }

            // JMP 01/27/04 - Removed for QA defect 197
            //itemOccasion = item.getOccassionId();
            //if(itemOccasion == null) itemOccasion = "";
            //if(itemOccasion.equals(occasionTypeFuneral))
            //{
            //    isFuneral = true;
            //}
        }

        String maxItemTotal = null;
        String maxTotal = null;
        BigDecimal maxItemAmount = null;
        BigDecimal maxAmount = null;
        
        boolean byPassCardinalCommerceChecks = false;
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {            	
            if(parmHandler.getGlobalParm("VALIDATION_SERVICE", "BYPASS_CARDINAL_COMMERCE_MAX_CHECKS").equalsIgnoreCase("Y"))
            {
            	byPassCardinalCommerceChecks = true; 
            }
        }
    	logger.info("###byPassCardinalCommerceChecks: "+ byPassCardinalCommerceChecks);
    	
    	Collection payments = order.getPayments();
    	Iterator paymentsIt = null;
        boolean isOrderCardinalVerified = false;
        if(payments != null && payments.size() > 0)
        {
            // Get payments
            it = payments.iterator();
            String cardinalVerifiedFlag = null;
            
            PaymentsVO payment = null;
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                cardinalVerifiedFlag = payment.getCardinalVerifiedFlag();
                
                if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y"))
                {
                	isOrderCardinalVerified = true;
                }
            }
        }
        logger.info("###isOrderCardinalVerified: "+ isOrderCardinalVerified);
        
        // If the order is associated with a Preferred Partner, obtain the appropriate thresholds
        if (preferredPartner != null)
        {
            // Get the max order total
            maxItemTotal = getFrpGlobalParm(preferredPartner + "_MAX_ITEM_TOTAL", vdau, conn);

            if(maxItemTotal == null)
            {
                throw new ValidationException("Missing " + preferredPartner + "_MAX_ITEM_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxItemAmount = new BigDecimal(maxItemTotal);

            // Get the max order total
            maxTotal = getFrpGlobalParm(preferredPartner + "_MAX_ORDER_TOTAL", vdau, conn);

            if(maxTotal == null)
            {
                throw new ValidationException("Missing " + preferredPartner + "_MAX_ORDER_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxAmount = new BigDecimal(maxTotal);
            
        // if the order contains a premier product obtain the lux total thresholds, otherwise, obtain the standard total thresholds
        } else if(isPremier)
        {
            // Get the max order total
            maxItemTotal = getFrpGlobalParm("LUX_MAX_ITEM_TOTAL", vdau, conn);

            if(maxItemTotal == null)
            {
                throw new ValidationException("Missing LUX_MAX_ITEM_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxItemAmount = new BigDecimal(maxItemTotal);

            // Get the max order total
            maxTotal = getFrpGlobalParm("LUX_MAX_ORDER_TOTAL", vdau, conn);

            if(maxTotal == null)
            {
                throw new ValidationException("Missing LUX_MAX_ORDER_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxAmount = new BigDecimal(maxTotal);
        }
        else if(isOrderCardinalVerified) 
        {
            // Get the max order total
            maxItemTotal = getFrpGlobalParm("CARDINAL_COMMERCE_MAX_ITEM_TOTAL", vdau, conn);

            if(maxItemTotal == null)
            {
                throw new ValidationException("Missing CARDINAL_COMMERCE_MAX_ITEM_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxItemAmount = new BigDecimal(maxItemTotal);

            // Get the max order total
            maxTotal = getFrpGlobalParm("CARDINAL_COMMERCE_MAX_ORDER_TOTAL", vdau, conn);

            if(maxTotal == null)
            {
                throw new ValidationException("Missing CARDINAL_COMMERCE_MAX_ORDER_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxAmount = new BigDecimal(maxTotal);
        }
        else 
        {
            // Get the max order total
            maxItemTotal = getFrpGlobalParm("MAX_ITEM_TOTAL", vdau, conn);

            if(maxItemTotal == null)
            {
                throw new ValidationException("Missing MAX_ITEM_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxItemAmount = new BigDecimal(maxItemTotal);

            // Get the max order total
            maxTotal = getFrpGlobalParm("MAX_ORDER_TOTAL", vdau, conn);

            if(maxTotal == null)
            {
                throw new ValidationException("Missing MAX_ORDER_TOTAL in FRP.GLOBAL_PARMS table");
            }

            maxAmount = new BigDecimal(maxTotal);
        }

        String origin = order.getOrderOrigin();
        if(origin == null) origin = "";
        
        // if the order total exceeds a preset amount
        BigDecimal orderTotal = new BigDecimal(orderTotalStr);

        if(!origin.equals(bulkOrderOrigin) && !isFuneral && orderTotal.compareTo(maxAmount) == 1)
        {
        	if(isOrderCardinalVerified && byPassCardinalCommerceChecks)
            {  
            	//bypass error message 
        		logger.info("bypass order total exceeds max error message");
            }
            else{
            	//show error message
            	response.put(ValidationConstants.RESPONSE_ORDER_TOTAL_EXCEEDS_MAX, FieldUtils.replaceAll(ValidationConstants.RESPONSE_ORDER_TOTAL_EXCEEDS_MAX, "max", maxAmount.toString()));
            }
        }
        // end order total amount check

        // if the order total is zero
        if(orderTotal.compareTo(new BigDecimal(0)) == 0)
        {
            response.put(ValidationConstants.RESPONSE_ORDER_TOTAL_ZERO, ValidationConstants.RESPONSE_ORDER_TOTAL_ZERO);
        }
        // end order total check

        // if the sum of all products is zero
        items = order.getOrderDetail();
        it = items.iterator();
        BigDecimal itemTotal = new BigDecimal(0);
        item = null;
        BigDecimal itemPrice = new BigDecimal(0);
        BigDecimal itemTax = new BigDecimal(0);
        BigDecimal itemServiceFee = new BigDecimal(0);
        BigDecimal itemShippingFee = new BigDecimal(0);
        BigDecimal itemDropShipFee = new BigDecimal(0);
        BigDecimal totalItemCharges = new BigDecimal(0);
        BigDecimal originalItemCharges = new BigDecimal(0);

        ProductVO productVO = null;

        String itemNumber = "";

        String itemPriceStr = null;
        String itemTaxStr = null;
        String itemServiceFeeStr = null;
        String itemDropShipFeeStr = null;
        String originalItemChargesStr = null;
        
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            productVO = orderValidatorDAO.getProductDetails(item.getProductId());
            
            itemPriceStr = item.getProductsAmount();
            if(itemPriceStr == null) itemPriceStr = "0";
            itemTaxStr = item.getTaxAmount();
            if(itemTaxStr == null) itemTaxStr = "0";
            itemServiceFeeStr = item.getServiceFeeAmount();
            if(itemServiceFeeStr == null) itemServiceFeeStr = "0";
            itemDropShipFeeStr = item.getShippingFeeAmount();
            if(itemDropShipFeeStr == null) itemDropShipFeeStr = "0";
            originalItemChargesStr = item.getOrigTotalFeeAmount();
            //We do not set originalItemChargesStr to 0 if it is null
            //that would cause incorrect behavior in check where it it used            
            
            itemPrice = new BigDecimal(itemPriceStr);
            itemTax = new BigDecimal(itemTaxStr);
            itemServiceFee = new BigDecimal(itemServiceFeeStr);
            itemDropShipFee = new BigDecimal(itemDropShipFeeStr);
            itemNumber = item.getLineNumber();
            
            itemTotal = itemTotal.add(itemPrice).add(itemTax).add(itemServiceFee).add(itemDropShipFee);
          
            if (originalItemChargesStr == null)
            {
                logger.info("Cannot check if fees are greater after recalculate. " +
                  "Original charges were null for order detail " + item.getOrderDetailId());
            }
            else if (item.getProductType() == null)
            {
                logger.info("Cannot check if fees are greater after recalculate.  " +
                  "Product type is null for order detail " + item.getOrderDetailId());
            }
            else
            {   
                String productType = item.getProductType();
                if ((item.getProductType().equals(PRODUCT_TYPE_FRESHCUT)
                         || productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT)
                         || productType.equals(PRODUCT_TYPE_FLORAL) || productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT) || productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT))
                      && originalItemChargesStr != null)
                {
                    totalItemCharges = itemServiceFee.add(itemDropShipFee);
                    originalItemCharges = new BigDecimal(originalItemChargesStr);
                    if(productVO != null){
                      if(!(item.getProductType().equals(ValidationConstants.PRODUCT_TYPE_SERVICES) && productVO.getProductSubType().equals(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP)))           
                      {
                        //Validate that there isn't a service charge discrepency
                        if (totalItemCharges.compareTo(originalItemCharges) > 0)
                        {
                            //The charges are greater than the original charges
                            String displayMessage = ValidationConstants.RESPONSE_SERVICE_FEE_INCREASE;
                            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);  
                            //Avoid regex errors if the string is malformed
                            if (displayMessage.contains("{originalPrice}") && displayMessage.contains("{newPrice}"))
                            {
                                displayMessage = FieldUtils.replaceAll(displayMessage, "originalPrice",currencyFormat.format(originalItemCharges.doubleValue()));
                                displayMessage = FieldUtils.replaceAll(displayMessage, "newPrice",currencyFormat.format(totalItemCharges.doubleValue()));
                            }
                            logger.info(displayMessage);
                            response.put(ValidationConstants.RESPONSE_SERVICE_FEE_INCREASE + itemNumber, displayMessage);
                        }
                      }
                    }
                }
            }

            if(itemPrice.compareTo(maxItemAmount) > 0)
            {
            	if(isOrderCardinalVerified && byPassCardinalCommerceChecks)
                {  
                	//bypass error message 
            		logger.info("bypass item total exceeds max");
                }
                else{
                	//show error message
                	response.put(ValidationConstants.RESPONSE_ITEM_TOTAL_EXCEEDS_MAX + itemNumber, FieldUtils.replaceAll(ValidationConstants.RESPONSE_ITEM_TOTAL_EXCEEDS_MAX,"amount", maxItemTotal));
                }
            }      
        }
        
        if(items != null && items.size() > 0 && itemTotal.compareTo(new BigDecimal(0)) == 0)
        {
            response.put(ValidationConstants.RESPONSE_ITEM_TOTAL_ZERO + itemNumber, ValidationConstants.RESPONSE_ITEM_TOTAL_ZERO);
        }
        // end sum of products check


        // Total number of line items exceeds predetermined maximum


        // if the order contains a premier product obtain the lux line item threshold, otherwise, obtain the standard item threshold
        String maxItems = null;
        if(isPremier)
        {
            // Get lux max items
            maxItems = getFrpGlobalParm("LUX_MAX_LINE_ITEMS", vdau, conn);

            if(maxItems == null)
            {
                throw new ValidationException("Missing LUX_MAX_LINE_ITEMS in FRP.GLOBAL_PARMS table");
            }
        }
        else if(isOrderCardinalVerified)
        {
            // Get cardinal commerce max items
            maxItems = getFrpGlobalParm("CARDINAL_COMMERCE_MAX_LINE_ITEMS", vdau, conn);
            logger.info("maxItems: " + maxItems);

            if(maxItems == null)
            {
                throw new ValidationException("Missing CARDINAL_COMMERCE_MAX_LINE_ITEMS in FRP.GLOBAL_PARMS table");
            }
        }
        else
        {
            // Get max items
            maxItems = getFrpGlobalParm("MAX_LINE_ITEMS", vdau, conn);

            if(maxItems == null)
            {
                throw new ValidationException("Missing MAX_LINE_ITEMS in FRP.GLOBAL_PARMS table");
            }
        }
        logger.info("itemCount: " + itemCount);
        logger.info("maxItems: " + maxItems);
        logger.info("isOrderCardinalVerified: "+ isOrderCardinalVerified);
        // Exclude funeral and bulk orders
        if(!origin.equals(bulkOrderOrigin) /*&& !isFuneral*/ && itemCount > Integer.parseInt(maxItems))
        {
        	if(isOrderCardinalVerified && byPassCardinalCommerceChecks)
            {  
            	//bypass error message
        		logger.info("bypass order items exceeds max error message");
            }
            else{
            	//show error message
            	response.put(ValidationConstants.RESPONSE_ORDER_ITEMS_EXCEEDS_MAX, ValidationConstants.RESPONSE_ORDER_ITEMS_EXCEEDS_MAX);
            }
        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending order amount validation");
        
        return response;
    }

    private static String getFrpGlobalParm(String key, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        String value = null;
        
        // Try cache first
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {            
            value = parmHandler.getGlobalParm("VALIDATION_SERVICE", key);
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.addInputParam("IN_PARAM", key);
            dataRequest.setStatementID(GET_GLOBAL_PARAM);
            String rs = (String)vdau.execute(dataRequest);
            value = rs;
            dataRequest.reset();            
        }

        return value;
    }
}