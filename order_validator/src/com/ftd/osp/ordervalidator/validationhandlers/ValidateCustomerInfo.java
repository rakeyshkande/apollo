package com.ftd.osp.ordervalidator.validationhandlers;

import com.ftd.ftdutilities.FieldUtils;

import com.ftd.ftdutilities.FTDCAMSUtils;
import org.apache.commons.lang.StringUtils;
import com.ftd.osp.ordervalidator.OrderValidatorBO;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PostalCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;


import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * This class Validates fields in the BUYERXXX fields of the order object.
 * Checks for missing first name, last name, Address 1, city, state, zip on
 * US and CAN orders.  Checks for last name, Address 1, city, zip on international orders.
 * Check that the sold-to state is in the STATE_MASTER table of the FTD_APPS database
 * schema.  If the evening or daytime phone numbers contain any alpha characters besides
 * "ext" and "x" then an error is returned.
 *
 *
 * @author Jeff Penney
 *
 */
public class ValidateCustomerInfo
{
    private static final String GET_STATE_LIST = "GET_STATE_LIST";
    private static final String GET_ZIPCODE_LIST = "GET_ZIPCODE_LIST";
    private static final String GET_COUNTRY_DETAILS = "GET_COUNTRY_DETAILS";
    private static final String STATE_ID = "STATE_ID";
    private static final String ZIP_CODE = "ZIP_CODE";
    private static final String COUNTRY_CODE = "COUNTRY_ID";
    private static final String PHONE_TYPE_HOME = "Home";
    private static final String PHONE_TYPE_WORK = "Work";
    private static final String PHONE_TYPE_FAX = "Fax";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateCustomerInfo");

    /**
     * Validates the following Bill-to customer information:
     * Checks for missing first name, last name, Address 1, city, state, zip on
     * US and CAN orders.  Checks for last name, Address 1, city, zip on international orders.
     * Check that the sold-to state is valid.
     * @param request Contains an Order object
     * @exception Exception if a database error occurs.
     * @return Returns any validation errors.
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {
        logger.info("Starting buyer validation");

        OrderVO order = null;
        Connection conn = null;

        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }

        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");

        // Check for missing customer information
        /* This includes:
         * Last name (US, CAN, INT)
         * First name (US & CAN)
         * Address1 (US, CAN, INT)
         * City (US, CAN, INT)
         * State (US & CAN)
         * Zip (US, CAN, INT)
         */

        // remove any null values
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;

        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);

                String country = (buyerAddress.getCountry() == null)?"":buyerAddress.getCountry();

                String lastName = buyer.getLastName();
                if(lastName == null)
                {
                    lastName = "";
                }
                String firstName = buyer.getFirstName();
                if(firstName == null) firstName = "";
                String city = buyerAddress.getCity();
                if(city == null) city = "";
                String county = buyerAddress.getCounty();
                if(county == null) county = "";
                String state = buyerAddress.getStateProv();
                if(state == null) state = "";
                String zip = buyerAddress.getPostalCode();
                if(zip == null) zip = "";
                String address1 = buyerAddress.getAddressLine1();
                if(address1 == null) address1 = "";
                String address2 = buyerAddress.getAddressLine2();
                if(address2 == null) address2 = "";

                boolean militaryAddress = false;
                if(city.equals("APO") || city.equals("FPO"))
                {
                  militaryAddress = true;
                }


                // Get email address
                String email = null;
                if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
                {
                    email = ((BuyerEmailsVO)buyer.getBuyerEmails().get(0)).getEmail();
                }
                if(email == null)
                {
                    email = "";
                }

                // BEGIN Check field lengths
                // First name max length 20
                if(firstName.length() > 20)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_FIRST_NAME_TOO_LONG, ValidationConstants.RESPONSE_BUYER_FIRST_NAME_TOO_LONG);
                }
                
                // check for long digit number in the first name
                if(!StringUtils.isEmpty(firstName) && FieldUtils.numberRangeRegCheck(firstName, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_FIRST_NAME , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_FIRST_NAME);
                }
                
                // Last name max length 20
                if(lastName.length() > 20)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_LAST_NAME_TOO_LONG, ValidationConstants.RESPONSE_BUYER_LAST_NAME_TOO_LONG);
                }
                
                // check for long digit number in the field
                if(!StringUtils.isEmpty(lastName) && FieldUtils.numberRangeRegCheck(lastName, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_LAST_NAME , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_LAST_NAME);
                }
                
                // Zip code max length CA=6, US=9, INT=12
                if(country.equals(usCountryCode) && zip.length() > 9 && !militaryAddress)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG, ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG);
                }
                else if(country.equals(canadaCountryCode) && zip.length() > 6 && !militaryAddress)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG, ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG);
                }
                else if(zip.length() > 12 && !militaryAddress)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG, ValidationConstants.RESPONSE_BUYER_ZIP_TOO_LONG);
                }

                // City name max length 30
                if(city.length() > 30)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_CITY_TOO_LONG, ValidationConstants.RESPONSE_BUYER_CITY_TOO_LONG);
                }

                // County name max length 20
                if(county.length() > 20)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_COUNTY_TOO_LONG, ValidationConstants.RESPONSE_BUYER_COUNTY_TOO_LONG);
                }

                // State name max length 10
                if(state.length() > 10)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_STATE_TOO_LONG, ValidationConstants.RESPONSE_BUYER_STATE_TOO_LONG);
                }

                // Email address length 55
                if(email.length() > 55)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_EMAIL_TOO_LONG, ValidationConstants.RESPONSE_BUYER_EMAIL_TOO_LONG);
                }

                // Country length 20
                if(country.length() > 20)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_COUNTRY_TOO_LONG, ValidationConstants.RESPONSE_BUYER_COUNTRY_TOO_LONG);
                }

                // Check for address 1 and 2 length
                if(address1.length() > 45)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG, ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG);
                }
                else if(address2.length() > 45)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG, ValidationConstants.RESPONSE_BUYER_ADDRESS_TOO_LONG);
                }
                
                // checking for any long digit number in adddress1
                if(!StringUtils.isEmpty(address1) && FieldUtils.numberRangeRegCheck(address1, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS1 , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS1);
                }
                
                if(!StringUtils.isEmpty(address2) && FieldUtils.numberRangeRegCheck(address2, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS2 , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS2);
                }
                // **** Phone number lengths are checked below ****

                // END Check field lengths

                if((country.equals(usCountryCode) || country.equals(canadaCountryCode)) && firstName.length() < 1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_FIRST_NAME_MISSING, ValidationConstants.RESPONSE_BUYER_FIRST_NAME_MISSING);
                }

                if(lastName.length() < 1 )
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_LAST_NAME_MISSING, ValidationConstants.RESPONSE_BUYER_LAST_NAME_MISSING);
                }

                if(city.length() < 1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_CITY_MISSING, ValidationConstants.RESPONSE_BUYER_CITY_MISSING);
                }
                
                // checking for any long digit number in city
                if(!StringUtils.isEmpty(city) && FieldUtils.numberRangeRegCheck(city, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_CITY , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_CITY);
                }
                if(state.length() < 1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_STATE_MISSING, ValidationConstants.RESPONSE_BUYER_STATE_MISSING);
                }

                if(zip.length() < 1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ZIP_MISSING, ValidationConstants.RESPONSE_BUYER_ZIP_MISSING);
                }

                if(address1.length() < 1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_ADDRESS1_MISSING, ValidationConstants.RESPONSE_BUYER_ADDRESS1_MISSING);
                }
                //Zip/state validation
                if(country.equalsIgnoreCase("US")){
	                OrderValidatorBO orderValidatorBO = new OrderValidatorBO();
	                if(zip != null && zip.length() >= 5 && !orderValidatorBO.validateStateByZipcode(zip.substring(0, 5), state, "")){
	                	response.put(ValidationConstants.RESPONSE_INVALID_BUYER_STATE_FOR_ZIP, ValidationConstants.RESPONSE_INVALID_BUYER_STATE_FOR_ZIP);
	                }
                }


                CachedResultSet rs = null;

                // Check that the state is valid
                if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                {
                    // Get the state list since more than just US and CA states exist in the DB
                    Map stateMap = getStateByIdMap(vdau, conn);

                    if(!stateMap.containsKey(state))
                    {
                        response.put(ValidationConstants.RESPONSE_BUYER_STATE_INVALID, FieldUtils.replaceAll(ValidationConstants.RESPONSE_BUYER_STATE_INVALID, "state", state));
                    }
                    else
                    {
                        // only check zip code if the state is valid
                        // Defect 129 QA
                        // Check for valid zip code US/CA only
                        // Ony check the DB for US country codes, emueller, 3/26/04
                        // Do not check zip for military addresses, 4/2/04 - emueller
                        if(country.equals(usCountryCode) && !militaryAddress)
                        {
                            String zipCode = null;
                            if(country.equals(usCountryCode) && zip.length() > 5)
                            {
                                // Truncate to 5 for US
                                zipCode = zip.substring(0,5);
                            }
                            else
                            {
                                zipCode = zip;
                            }

                            boolean zipExists = false;
                            // Check cache for zip code
                            PostalCodeHandler postalCodeHandler = (PostalCodeHandler)CacheManager.getInstance().getHandler("CACHE_NAME_POSTAL_CODE");
                            if(postalCodeHandler != null)
                            {
                                zipExists = postalCodeHandler.postalCodeExists(zipCode);
                            }
                            // Get the zip code data from the database if the
                            // cache handler is null or if the zip code does not exist
                            // in cache
                            if(postalCodeHandler == null || zipExists == false)
                            {
                                dataRequest.setStatementID(GET_ZIPCODE_LIST);
                                dataRequest.addInputParam(ZIP_CODE, zipCode);
                                dataRequest.addInputParam("CITY", null);
                                dataRequest.addInputParam("STATE", state);
                                dataRequest.setConnection(conn);
                                rs = (CachedResultSet)vdau.execute(dataRequest);
                                rs.reset();
                                dataRequest.reset();

                                String zipOut = null;

                                while(rs.next())
                                {
                                    zipOut = (String)rs.getObject(1);
                                }

                                if(zipOut != null)
                                {
                                    zipExists = true;
                                }
                            }

                            if(!zipExists)
                            {
                                response.put(ValidationConstants.RESPONSE_BUYER_ZIP_INVALID, ValidationConstants.RESPONSE_BUYER_ZIP_INVALID);
                            }
                        }
                    }
                }

                // Check for valid country
                // Try cache first
                CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");
                boolean countryExists = false;
                if(countryHandler != null)
                {
                    countryExists = countryHandler.countryExists(country);
                }
                else
                {
                    dataRequest.setStatementID(GET_COUNTRY_DETAILS);
                    dataRequest.addInputParam(COUNTRY_CODE, country);
                    dataRequest.setConnection(conn);
                    rs = (CachedResultSet)vdau.execute(dataRequest);
                    rs.reset();
                    dataRequest.reset();
                    String checkCountry = null;

                    while(rs.next())
                    {
                        checkCountry = (String)rs.getObject(1);
                    }

                    if(checkCountry != null)
                    {
                        countryExists = true;
                    }
                }

                if(!countryExists)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_COUNTRY_INVALID, ValidationConstants.RESPONSE_BUYER_COUNTRY_INVALID);
                }

                // Begin email address validation
                if(email.length() > 0 && email.indexOf("@") == -1)
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_EMAIL_INVALID, ValidationConstants.RESPONSE_BUYER_EMAIL_INVALID);
                } 
                
                // check for long digit in email
                if(!StringUtils.isEmpty(email) && FieldUtils.numberRangeRegCheck(email, 13, 20))
                {                	
                	response.put(ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_EMAIL , ValidationConstants.RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_EMAIL);
                }
                // End email address validation           
                
                // Get the buyer phone numbers
                BuyerPhonesVO buyerHomePhone = null;
                BuyerPhonesVO buyerWorkPhone = null;
                BuyerPhonesVO buyerPhone = null;
                String phoneNumber = null;
                StringCharacterIterator sci = null;

                if(buyer.getBuyerPhones() != null)
                {
                    for(int i = 0; i < buyer.getBuyerPhones().size(); i++)
                    {
                        buyerPhone = (BuyerPhonesVO)buyer.getBuyerPhones().get(i);

                        if(buyerPhone.getPhoneType().equalsIgnoreCase(PHONE_TYPE_HOME))
                        {
                            buyerHomePhone = buyerPhone;
                        }
                        else if(buyerPhone.getPhoneType().equalsIgnoreCase(PHONE_TYPE_WORK))
                        {
                            buyerWorkPhone = buyerPhone;
                        }
                    }

                    if(buyerWorkPhone != null)
                    {
                        // Check that the phone number does not contain alpha chars except ext or x
                        // Daytime Phone
                        phoneNumber = buyerWorkPhone.getPhoneNumber();
                        if(phoneNumber == null) phoneNumber = "";
                        
                        // Daytime phone number min length is 7  max length is 20
                        if(phoneNumber.length() < 7 || phoneNumber.length() > 20)
                        {
                            response.put(ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID);
                        }
                        /*else if((!country.equals(usCountryCode) && !country.equals(canadaCountryCode))  && phoneNumber.length() > 20)
                        {
                            response.put(ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_TOO_LONG, ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_TOO_LONG);
                        }*/

                        if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                        {
                            if(phoneNumber == null || phoneNumber.length() < 1)
                            {
                                // This will make the phone number invalid because it is required
                                phoneNumber = "@@";
                            }

                            // remove ext, x and extension
                            phoneNumber = phoneNumber.toLowerCase();
                            // remove ext, x and extension
                            phoneNumber = phoneNumber.toLowerCase();
                            //This was changed to use string replaces for defect#46
                            //(indexOf is also being used so string replaces can be avoided if possible)
                            if(phoneNumber.indexOf("extension") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"extension","0");
                            }else if(phoneNumber.indexOf("ext") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"ext","0");
                            }else if(phoneNumber.indexOf("x") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"x","0");
                            }
                            // Loop through phone numbers
                            sci = new StringCharacterIterator(phoneNumber);
                            for(char c = sci.first(); c != CharacterIterator.DONE; c = sci.next())
                            {
                                if(!Character.isDigit(c))
                                {
                                    response.put(ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID);
                                    break;
                                }
                            }

                            // If phone number is present it must be 10 digits for US and CA
                           /* if(phoneNumber.length() > 0 && phoneNumber.length() < 10)
                            {
                                response.put(ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_DAYTIME_PHONE_INVALID);
                            }*/
                        }
                    }
                    else
                    {
                        // No work phone number
                    }

                    if(buyerHomePhone != null)
                    {
                        // Evening Phone
                        phoneNumber = buyerHomePhone.getPhoneNumber();
                        if(phoneNumber == null)
                        {
                            phoneNumber = "";
                        }
                        
                        // Evening phone number max length is US/CA=10, INT=20
                        if(phoneNumber.length()>0 && (phoneNumber.length() < 7 || phoneNumber.length() > 20))
                        {
                            response.put(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID);
                        }
                       /* else if((!country.equals(usCountryCode) && !country.equals(canadaCountryCode))  && phoneNumber.length() > 20)
                        {
                            response.put(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_TOO_LONG, ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_TOO_LONG);
                        }*/

                        if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                        {
                            // remove ext, x and extension
                            //This was changed to use string replaces for defect#46
                            //(indexOf is also being used so string replaces can be avoided if possible)
                            if(phoneNumber.indexOf("extension") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"extension","0");
                            }else if(phoneNumber.indexOf("ext") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"ext","0");
                            }else if(phoneNumber.indexOf("x") > -1)
                            {
                                phoneNumber = phoneNumber.replace(' ', '0');
                                phoneNumber = FieldUtils.replaceOnce(phoneNumber,"x","0");
                            }
                            // Loop through phone numbers
                            sci = new StringCharacterIterator(phoneNumber);
                            for(char c = sci.first(); c != CharacterIterator.DONE; c = sci.next())
                            {
                                if(!Character.isDigit(c))
                                {
                                    response.put(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID);
                                    break;
                                }
                            }

                            // If phone number is present it must be 10 digits for US and CA
                           /* if(phoneNumber.length() > 0 && phoneNumber.length() > 20)
                            {
                                response.put(ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID, ValidationConstants.RESPONSE_BUYER_EVENING_PHONE_INVALID);
                            }*/
                        }
                    }
                    else
                    {
                        // No home phone
                    }
                }
                else
                {
                    response.put(ValidationConstants.RESPONSE_BUYER_PHONE_MISSING, ValidationConstants.RESPONSE_BUYER_PHONE_MISSING);
                }
            }
            else
            {
                response.put(ValidationConstants.RESPONSE_BUYER_INFO_MISSING, ValidationConstants.RESPONSE_BUYER_INFO_MISSING);
            }
        }
        else
        {
            response.put(ValidationConstants.RESPONSE_BUYER_INFO_MISSING, ValidationConstants.RESPONSE_BUYER_INFO_MISSING);
        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending buyer validation");

        return response;
    }

    private static Map getStateByIdMap(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");

        Map stateMap = new HashMap();
        if(stateHandler != null)
        {
            stateMap = stateHandler.getStatesByIdMap();
        }
        // If the states handler is not found then lookup the states from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_STATE_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            String stateId = null;
            String countryCode = null;

            while(rs.next())
            {
                stateId = (String)rs.getObject(1);
                countryCode = (String)rs.getObject(3);

                if(countryCode == null || countryCode.equals("CAN"))
                {
                    stateMap.put(stateId, countryCode);
                }
            }
        }
        return stateMap;
    }
}