package com.ftd.osp.ordervalidator.validationhandlers;

import java.sql.*;
import java.math.BigDecimal;
import java.util.*;
import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerException;

import com.ftd.osp.ordervalidator.util.*;
import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.BillingInfoHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.BillingInfoVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;

import org.apache.oro.text.perl.Perl5Util;

/**
 * This class validates the billing info formats.  These fields are dynamic and 
 * this validation was built for a maximum of 6 prompts (easy to increase this 
 * number).  The validation is done via regular expressions which are stored 
 * in the database for each prompt.
 * @author Jeff Penney
 *
 */
public class ValidateBillingInfo
{
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateOrderAmount");
    
    /**
     * Validates against the following rules.  If order contains invalid formats it 
     * adds them as errors.
     * @param request Contains the Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing any errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {    
        logger.info("Starting billing info field validation");
    
        OrderVO order = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }    

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();
        BillingInfoHandler billingInfoHandler = (BillingInfoHandler)CacheManager.getInstance().getHandler("CACHE_NAME_BILLING_INFO");

        if(billingInfoHandler != null)
        {
            // if order contains cobrand data
            if(order.getCoBrand() != null 
               && order.getCoBrand().size() > 0
               && billingInfoHandler.hasBillingInfo(order.getSourceCode()))
            {   
                Perl5Util regExp = new Perl5Util();

                int promptCount = 1;
                BillingInfoVO billingInfoVO = null;
                String promptFormat = null;
                CoBrandVO coBrand = null;
                
                Iterator coBrandIterator = order.getCoBrand().iterator();
                while(coBrandIterator.hasNext())
                {
                    coBrand = (CoBrandVO) coBrandIterator.next();

                    // check value against prompt format
                    billingInfoVO = billingInfoHandler.getBillingInfoBySourceAndPrompt(order.getSourceCode(), coBrand.getInfoName());
                    if(validatePrompt(billingInfoVO, coBrand, response, promptCount, regExp))
                    {
                        promptCount++;
                    }
                }            
            }
        }
        else
        {
            logger.error("Cache could not be found for Billing Info.");
        }

        logger.info("Ending billing info field validation");
        
        return response;
    }

    private static boolean validatePrompt(BillingInfoVO billingInfoVO, CoBrandVO coBrand, ValidationResponse response, int promptCount, Perl5Util regExp)
    {
        boolean foundError = false;
        
        if(billingInfoVO != null)
        {
            // set data to empty string because regexp does not like null
            if(coBrand.getInfoData() == null)
            {
                coBrand.setInfoData("");
            }

            // validate format against regexp
            if(!regExp.match(billingInfoVO.getInfoFormat(), coBrand.getInfoData()))
            {
                switch (promptCount) 
                {
                    case 1:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_1, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_1);
                            foundError = true;
                        }
                        break;
                    case 2:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_2, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_2);
                            foundError = true;
                        }
                        break;
                    case 3:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_3, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_3);
                            foundError = true;
                        }
                        break;
                    case 4:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_4, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_4);
                            foundError = true;
                        }
                        break;
                    case 5:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_5, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_5);
                            foundError = true;
                        }
                        break;
                    case 6:
                        {
                            response.put(ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_6, coBrand.getInfoName() + "~" + ValidationConstants.RESPONSE_BILLING_INFO_FORMAT_INVALID_6);
                            foundError = true;
                        }
                        break;
                    default:
                        {
                            // Nothing
                        }
                        break;
                }                
            }
        }

        return foundError;
    }
}