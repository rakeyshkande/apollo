package com.ftd.osp.ordervalidator.validationhandlers;

import java.sql.*;
import java.math.BigDecimal;
import java.util.*;
import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerException;

import com.ftd.osp.ordervalidator.util.*;
import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.FraudCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.ftdutilities.*;

/**
 * This class validates various order and item dollar amount fields.  If the order 
 * total is over a certain amount determined by a database row, then an error is
 * returned.  If the product amount is zero an error is returned.  If the order total
 * is zero then an error is returned.
 * @author Jeff Penney
 *
 */
public class ValidateOtherHeaderFields
{
    private static final String GET_GLOBAL_PARAM = "GET_GLOBAL_PARAM_VALIDATION";
    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateOtherHeaderFields");
    
    /**
     * Validates against the following rules.  If order contains novator fraud messages
     * add them as warnings.
     * @param request Contains the Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing any errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {    
        logger.info("Starting other header field validation");
    
        OrderVO order = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }    

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();

        String fraudFlag = null;

        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        if(sourceHandler != null)
        {
            SourceMasterVO sourceVo = sourceHandler.getSourceCodeById(order.getSourceCode());
            if(sourceVo != null)
            {
                fraudFlag = sourceVo.getFraudFlag();
            }
        }
        // Get the source code data from the database
        else
        {
            dataRequest.addInputParam("SOURCE_CODE", order.getSourceCode());
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
        
            while(rs.next())
            {
                fraudFlag = (String)rs.getObject(34);
            }
        }

    	Collection payments = order.getPayments();
    	Iterator it = null;
        boolean isOrderCardinalVerified = false;
        if(payments != null && payments.size() > 0) {
            // Get payments
            it = payments.iterator();
            String cardinalVerifiedFlag = null;
            
            PaymentsVO payment = null;
            while(it.hasNext()) {
                payment = (PaymentsVO)it.next();
                cardinalVerifiedFlag = payment.getCardinalVerifiedFlag();
                
                if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y"))
                {
                	isOrderCardinalVerified = true;
                }
            }
        }
        logger.info("isOrderCardinalVerified: "+ isOrderCardinalVerified);
        logger.info("fraudFlag: "+ fraudFlag);

        // if order contains novator fraud messages and source fraud flag is Y
        if(order.getFraudCodes() != null 
            && order.getFraudCodes().size() > 0 
            && fraudFlag != null 
            && fraudFlag.equals("Y")
            && !isOrderCardinalVerified)
        {
            FraudCodeHandler fraudCodeHandler = (FraudCodeHandler)CacheManager.getInstance().getHandler("CACHE_NAME_FRAUD_CODES");
            
            Map values = null;
            String fraudCode = null;
            String replacedStr = null;
            StringBuffer fraudDescription = new StringBuffer("");
            
            Iterator fraudCodesIterator = order.getFraudCodes().iterator();
            while(fraudCodesIterator.hasNext())
            {
                fraudCode = (String) fraudCodesIterator.next();

                // look up code description from cache
                if(fraudCodeHandler != null)
                {
                    fraudDescription.append(fraudCodeHandler.getFraudDescriptionById(fraudCode));
                }
                else
                {
                    logger.error("Fraud codes were not loaded correctly into cache.");
                }
                logger.info(fraudCode + " " + fraudDescription);

                if(fraudCodesIterator.hasNext())
                {
                    fraudDescription.append(", ");
                }
            }
            response.put(ValidationConstants.RESPONSE_NOVATOR_FRAUD_WARNING, FieldUtils.replaceAll(ValidationConstants.RESPONSE_NOVATOR_FRAUD_WARNING,"fraud_desc", fraudDescription.toString()));
        }
        
        logger.info("Ending other header field validation");
        
        return response;
    }
}