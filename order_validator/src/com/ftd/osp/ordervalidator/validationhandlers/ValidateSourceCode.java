package com.ftd.osp.ordervalidator.validationhandlers;

import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.BillingInfoVOComparator;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.BillingInfoHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CostCenterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.BillingInfoVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

/**
 * This class validates a source code on an Order against the database SOURCE table.
 * If the source code does not exist an error is returned (RESPONSE_SOURCE_CODE_INVALID).
 * If the source code is expired the RESPONSE_SOURCE_CODE_EXPIRED error is returned.
 * If the source code is valid then no errors are returned.  The functionality of this
 * class may be expanded if other source code validation rules are identified.  
 * These rules are taken from the WebOE ValidationSVC class.
 * 
 * @author Jeff Penney
 *
 */
public class ValidateSourceCode 
{
    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateSourceCode");

    /**
     * Validates the input source code by making sure it exists in teh database and that it
     * is not expired.
     * @param request Should contain the order
     * @exception Exception if a database error occurs
     * @return A ValiationResponse that contains validation errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, Exception
    {
        logger.info("Starting source code validation");
        
        OrderVO order = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String paymentTypeGC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_GIFT_CERT");        
        String paymentMethodPC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_METHOD_PC");
        String billingGiftCert = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "BILLING_GIFT_CERT");
        String ccTypeNC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "CREDIT_CARD_TYPE_NC");

        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();

        java.util.Date startDate = null;
        java.util.Date endDate = null;
        String sourceCode = null;
        String validPayMethod = null;
        String sendToScrub = null;
        /************************TARGET_2521*************************************/
        SourceMasterVO sourceVo = null;
        /************************TARGET_2521*************************************/
        
        // Try source code cache
        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        if(sourceHandler != null)
        {
        /************************TARGET_2521*************************************/
            //SourceVO sourceVo = sourceHandler.getSourceCodeDetails(order.getSourceCode());
            sourceVo = sourceHandler.getSourceCodeById(order.getSourceCode());            
        /************************TARGET_2521*************************************/
            if(sourceVo != null)
            {
                sourceCode = sourceVo.getSourceCode();
                startDate = sourceVo.getStartDate();
                endDate = sourceVo.getEndDate();
                validPayMethod = sourceVo.getValidPayMethod();
                sendToScrub = sourceVo.getSendToScrub();
            }
        }
        // Get the source code data from the database
        else
        {
            dataRequest.addInputParam("SOURCE_CODE", order.getSourceCode());
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
        
            while(rs.next())
            {
                sourceCode = (String)rs.getObject(1);
                startDate = (java.util.Date)rs.getObject(5);
                endDate = (java.util.Date)rs.getObject(6);
                validPayMethod = (String)rs.getObject(10);
                sendToScrub = (String)rs.getObject(33);
            }
        }
        
        if(sourceCode != null)
        {
            // If the source code exits but is before today then send back expired
            if(endDate != null && endDate.before(new java.util.Date()))
            {
                response.put(ValidationConstants.RESPONSE_SOURCE_CODE_EXPIRED, ValidationConstants.RESPONSE_SOURCE_CODE_EXPIRED);
            }
        }
        else
        {
            // if the source code does not exist in the SOURCE table
            // then it is invalid
            response.put(ValidationConstants.RESPONSE_SOURCE_CODE_INVALID, ValidationConstants.RESPONSE_SOURCE_CODE_INVALID);
        }

        // Validate that the source code is applicable for this payment type.
        // i.e. invoice source code cannot have credit card information
        boolean giftCertOverride = false;
        BillingInfoHandler billingInfoHandler = (BillingInfoHandler)CacheManager.getInstance().getHandler("CACHE_NAME_BILLING_INFO");
        if(validPayMethod != null 
            && validPayMethod.equals(ccTypeNC)
            && billingInfoHandler.getBillingInfoBySourceAndPrompt(order.getSourceCode(), billingGiftCert) != null)
        {
            giftCertOverride = true;
        }
        
        String orderPayMethod = null;
        PaymentsVO payment = null;
        List payments = order.getPayments();
        Iterator it = null;        
        if(payments != null)
        {
            // loop through payments
            it = payments.iterator();
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                orderPayMethod = payment.getPaymentType();

                if(payment.getPaymentMethodType() != null && !payment.getPaymentMethodType().equals(paymentTypeGC))
                {                
                    if(validPayMethod != null && !validPayMethod.equals(orderPayMethod) && !validPayMethod.equals(paymentMethodPC) && !giftCertOverride)
                    {
                        response.put(ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD, ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD);
                        break;
                    }
                }
            }
        }    
        // If the source code has a payment method but no payments are supplied
        else if(payments == null && validPayMethod!= null && validPayMethod.length() > 0)
        {
            response.put(ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD, ValidationConstants.RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD);
        }


        /************************TARGET_2521*************************************/
        
        BillingInfoVO ftdApps_BillingInfo_SourceCode = (! billingInfoHandler.getBillingInfoBySource(sourceCode).isEmpty())?((BillingInfoVO) billingInfoHandler.getBillingInfoBySource(sourceCode).get(0)):null;
        CostCenterHandler costCenterHandler = (CostCenterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COST_CENTER");
        String membershipID = null;

        // Check to see if FTD_APPS.BILLING_INFO.SOURCE_CODE exists for this source code
        if (ftdApps_BillingInfo_SourceCode != null) // determine if this is a pay type source code
        {
            // determine if Cost Center Lookup Validation should be done.
            // If FTD_APPS.COST_CENTER_ID.PARTNER_ID exists for this source code
            
            if (costCenterHandler.containsPartnerID(order.getPartnerId())) 
            {
                // Get membership id
                //If FTD_APPS.SOURCE.BILLING_INFO_LOGIC = CONCAT
                if ( (sourceVo.getBillingInfoLogic() != null) && (sourceVo.getBillingInfoLogic().equals("CONCAT")))  
                {
                    membershipID = ((MembershipsVO)order.getMemberships().get(0)).getMembershipIdNumber();
                }
                else
                {
                  // Concatenate the the co_brand fields by using BILLING_INFO_SEQUENCE
                  List coBrandList = order.getCoBrand();
                  List billingInfoVOList = billingInfoHandler.getBillingInfoBySource(order.getSourceCode());
                  Collections.sort(billingInfoVOList, new BillingInfoVOComparator());

                  BillingInfoVO billingInfoVO = null;
                  CoBrandVO coBrandVO = null;
                  StringBuffer membershipID_sb = new StringBuffer();
                  String pattern = null;
                  StringBuffer padder = null;
                  
                  for (Iterator billingInfoVOListIter = billingInfoVOList.iterator();  billingInfoVOListIter.hasNext() ; ) 
                  {
                      billingInfoVO = (BillingInfoVO) billingInfoVOListIter.next();
                      logger.debug("Billing Info Sequence:: " + billingInfoVO.getInfoSequence());

                      // for each co brand vo; get the correct info data
                      for (Iterator coBrandListIter = coBrandList.iterator();  coBrandListIter.hasNext() ;) 
                      {
                        coBrandVO = (CoBrandVO) coBrandListIter.next();
                        if (coBrandVO.getInfoName().equalsIgnoreCase(billingInfoVO.getInfoPrompt())) 
                        {                            
                            pattern = billingInfoVO.getInfoFormat();
                            logger.debug("Pattern:: " + pattern);
                            /**
                             * PERL 5 Expressions
                             * 1. The \d escape sequence means "any digit." 
                             * 2. ^ or \A implies Match at beginning of string only 
                             * 
                             * The following code interprets the '/^\d{' perl expression to mean any digit at 
                             * the beginning of a string
                             */
                            if (pattern.startsWith("/^\\d{")) 
                            {                                
                                int lpad = 0;
                                String lpadCharacter = "0";
                                padder = new StringBuffer();
                                int desiredLength = Integer.parseInt(pattern.substring(5,6));
                                if (coBrandVO.getInfoData() != null && coBrandVO.getInfoData().length() != desiredLength) 
                                {
                                    // left pad the test value to the desired length, as indicated in the Perl 5 expression
                                    lpad = desiredLength - coBrandVO.getInfoData().length();
        
                                    for (int x = 0; x < lpad; x++) 
                                    {
                                      padder.append(lpadCharacter);
                                    }
                                    // finally append the original value to create the effect of an lpad
                                    padder.append(coBrandVO.getInfoData());
                                }
                                else
                                {
                                  padder.append(coBrandVO.getInfoData());
                                }
                                logger.debug("CoBrand InfoData Before Padding:: " + coBrandVO.getInfoData());
                                logger.debug("CoBrand InfoData After Padding:: " + padder.toString());
                            }
                            membershipID_sb.append(padder.toString());
                            break;
                        }
                      }
                      membershipID = membershipID_sb.toString(); 
                  }
                }// membership id acquired

                // check to see if the combination of  membership id, partner id, 
                // and source code are valid; validate against ftd_apps.cost_centers
                if (costCenterHandler.getCostCenter(membershipID, order.getPartnerId(), order.getSourceCode()).isEmpty()) 
                {
                    // the combination does not exist on the table
                    logger.error("Could not find " 
                                  + membershipID 
                                  + ", " + order.getPartnerId() 
                                  + ", " + order.getSourceCode()
                                  + " in ftd_apps.cost_centers" );
                    response.put(ValidationConstants.RESPONSE_SOURCE_CODE_COST_CENTER_LOOKUP_VALIDATION_FAILED, 
                                 ValidationConstants.RESPONSE_SOURCE_CODE_COST_CENTER_LOOKUP_VALIDATION_FAILED + " " + membershipID);                    
                }
                else
                {
                    logger.debug("Found " 
                                  + membershipID 
                                  + ", " + order.getPartnerId() 
                                  + ", " + order.getSourceCode()
                                  + " in ftd_apps.cost_centers" );
                }
                
            }
            
        }
/* // not implemented
        else if (    (ftdApps_BillingInfo_SourceCode == null) 
                  && (sourceVo.getPartnerId() != null) ) // determine if this is a partner type source code
        {
          // this is a partner type source code
          
        }
*/        
        
        /************************TARGET_2521*************************************/

        if(sendToScrub != null && sendToScrub.equals(ValidationConstants.YES))
        {
            response.put(ValidationConstants.RESPONSE_SOURCE_CODE_SEND_TO_SCRUB, ValidationConstants.RESPONSE_SOURCE_CODE_SEND_TO_SCRUB);
        }
        
        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending source code validation");
        
        return response;
    } 
}