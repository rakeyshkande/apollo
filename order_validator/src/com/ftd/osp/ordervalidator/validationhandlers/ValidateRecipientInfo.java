package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.ParseException;
import java.text.StringCharacterIterator;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.osp.ordervalidator.OrderValidatorBO;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.DispatchCancelledOrder;
import com.ftd.osp.ordervalidator.util.StringReplacer;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.util.ValidationHandlerUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PostalCodeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

/**
 * This class Validates fields in the recipient section of an Item object.
 * Checks for missing first name, last name, Address 1, city, state, zip on
 * US and CAN orders.  Checks for last name, Address 1, city, zip on international orders.
 * Check that the recipient state is in the STATE_MASTER table of the FTD_APPS database
 * schema.  If the phon number contains any alpha characters besides
 * "ext" and "x" then an error is returned.  This class also checks that the recipient
 * country matches the product domestic/international flag.
 *
 * @author Jeff Penney
 *
 */
public class ValidateRecipientInfo 
{
    private static final String GET_STATE_DETAILS = "GET STATE DETAILS";
    private static final String GET_COUNTRY_DETAILS = "GET_COUNTRY_DETAILS";
    private static final String GET_ZIP_DETAILS = "GET ZIP DETAILS";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    private static final String COUNTRY_CODE = "COUNTRY_ID";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateRecipientInfo");
        
    /**
     * Validates the following Recipient customer information on each item:
     * Checks for missing first name, last name, Address 1, city, state, zip on
     * US and CAN orders.  Checks for last name, Address 1, city, zip on international orders.
     * Check that zip code and state are valid.  Check that the product matches the 
     * domestic/international indicator.
     * @param request Contains an Order object
     * @exception Exception if a database error occurs.
     * @return Returns any validation errors.
     *
     */
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, ParseException, Exception
    {    
        logger.info("Starting recipient validation");
        
        OrderVO order = null;
        ProductVO productVO = null;
        Connection conn = null;
        
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }      

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        
        
        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();
        OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
        
        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");        
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");
        String phoneNumberTypeHome = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PHONE_NUMBER_TYPE_HOME");
        String phoneNumberTypeWork = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PHONE_NUMBER_TYPE_WORK");
        String phoneNumberTypeFax = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PHONE_NUMBER_TYPE_FAX");
        String addressTypeR = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_RESIDENTIAL");
        String addressTypeF = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_FUNERAL_HOME");
        String addressTypeB = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_BUSINESS");
        String addressTypeH = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_HOSPITAL");
        String addressTypeC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"ADDRESS_TYPE_CEMETERY");        
        
        // Loop through the order's items
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        String country = null;
        String lastName = null;
        String firstName = null;
        String city = null;
        String state = null;
        String zip = null;
        String address1 = null;
        String address2 = null;
        CachedResultSet rs = null;
        String stateIdExists = null;
        String domesticFlag = null;
        String phoneNumber = null;
        StringCharacterIterator sci = null;
        
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            
            // Production Defect Fix : #14565
            if(item.getProductId() != null) {
            	productVO = orderValidatorDAO.getProductDetails(item.getProductId());
            }
            
            // Check for missing customer information
            /* This includes:
             * Last name (US, CAN, INT)
             * First name (US & CAN)
             * Address1 (US, CAN, INT)
             * City (US, CAN, INT)
             * State (US & CAN)
             * Zip (US, CAN, INT)
             */

            RecipientsVO recipient = null;
            RecipientAddressesVO recipAddress = null;
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    // remove any null values
                    country = recipAddress.getCountry();
                    
                    if(country == null) 
                    {
                        country = "";
                    }
                    lastName = recipient.getLastName();
                    if(lastName == null) 
                    {
                        lastName = "";
                    }
                    firstName = recipient.getFirstName();
                    if(firstName == null) 
                    {
                        firstName = "";
                    }
                    city = recipAddress.getCity();
                    if(city == null) 
                    {
                        city = "";
                    }
                    state = recipAddress.getStateProvince();
                    if(state == null) 
                    {
                        state = "";
                    }
                    zip = recipAddress.getPostalCode();
                    if(zip == null) 
                    {
                        zip = "";
                    }
                    address1 = recipAddress.getAddressLine1();
                    if(address1 == null) 
                    {   
                        address1 = "";
                    }
                    address2 = recipAddress.getAddressLine2();
                    if(address2 == null) 
                    {   
                        address2 = "";
                    }               

                    // Check for address 1 and 2 length
                    if(address1.length() > 45)
                    {
                            response.put(ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG);
                    }
                    else if(address2.length() > 45)
                    {
                            response.put(ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ADDRESS_TOO_LONG);
                    }                    
                  //Zip/state validation
                    if(country.equalsIgnoreCase("US")){
    	                OrderValidatorBO orderValidatorBO = new OrderValidatorBO();
    	                if(zip != null && zip.length() >= 5 && !orderValidatorBO.validateStateByZipcode(zip.substring(0, 5), state, "")){
    	                	response.put(ValidationConstants.RESPONSE_INVALID_RECIP_STATE_FOR_ZIP + item.getLineNumber(), ValidationConstants.RESPONSE_INVALID_RECIP_STATE_FOR_ZIP);
    	                }
                    }
                    // Zip code max length CA=6, US=9, INT=12
                    if(country.equals(usCountryCode) && zip.length() > 9)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG);
                    }
                    else if(country.equals(canadaCountryCode) && zip.length() > 6)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG);
                    }
                    else if(zip.length() > 12)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_TOO_LONG);
                    }
                    // City name max length 30
                    if(city.length() > 30)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_CITY_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_CITY_TOO_LONG);
                    }                
                    // State name max length 10
                    if(state.length() > 10)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_STATE_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_STATE_TOO_LONG);
                    }
                    // first name max length 20
                    if(firstName.length() > 20)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_FIRST_NAME_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_FIRST_NAME_TOO_LONG);
                    }                
                    // last name max length 20
                    if(lastName.length() > 20)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_LAST_NAME_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_LAST_NAME_TOO_LONG);
                    }                

                    // Need to get JCPENNEY_FLAG for the source code
                    SourceMasterVO sourceVo = getSourceDetails(order.getSourceCode(), vdau, conn); 
                    String jcpFlag = null;
                    if(sourceVo != null) jcpFlag = sourceVo.getJcpenneyFlag();
                    if(jcpFlag == null) jcpFlag = "";
                    // End getting JCPENNEY_FLAG for the source code                    

                    // If this is a JCP source code then zip code is always required
                    if(jcpFlag.equalsIgnoreCase("Y"))
                    {
                        if(zip.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ZIP_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_MISSING);
                        }                        
                    }

                    if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                    {
                        if(lastName.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_LAST_NAME_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_LAST_NAME_MISSING);
                        }

                        if(firstName.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_FIRST_NAME_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_FIRST_NAME_MISSING);
                        }
            
                        if(city.length() < 1)
                        {
                           response.put(ValidationConstants.RESPONSE_RECIP_CITY_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_CITY_MISSING);
                        }
            
                        if(state.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_STATE_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_STATE_MISSING);
                        }
             
                        if(zip.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ZIP_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_MISSING);
                        }

                   
                        if(address1.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING);
                        }                        
                    }
                    else if((lastName.length() < 1 || city.length() < 1 ||
                            /*zip.length() < 1 ||*/ address1.length() < 1))
                    {
                   
                        if(lastName.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_LAST_NAME_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_LAST_NAME_MISSING);
                        }

                        if(city.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_CITY_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_CITY_MISSING);
                        }

                        /*  JMP 12/30/2003 Removed zip code check per QA defect 60
                        if(zip.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ZIP_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_MISSING);
                        }
                        */
                        
                        if(address1.length() < 1)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ADDRESS1_MISSING);
                        }
                    }
                    

                    // Check for APO and PO boxes in address lines                    
                    String address1Upper = address1.toUpperCase();
                    String address2Upper = address2.toUpperCase();

                    String shipMethod = item.getShipMethod();
                    if(shipMethod == null) shipMethod = "";
                    
                    if(productVO != null){
                      if(!(ValidationConstants.PRODUCT_TYPE_SERVICES.equals(item.getProductType()) && ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP.equals(productVO.getProductSubType())))           
                      {
                        if(checkForPO(address1Upper))
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_ONE + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_ONE);
                        } 
                        if(checkForPO(address2Upper))
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_TWO + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_TWO);
                        }                     
                        // End check for APO and PO boxes in address lines
                      }
                    }
                    // Check that the recipient country id valid
                    CountryMasterVO countryVo = getCountryDetails(country, vdau, conn);
                    String checkCountry = null;
                    String countryType = null;
                    if(countryVo != null)
                    {
                        checkCountry = countryVo.getCountryId();
                        countryType = countryVo.getCountryType();
                    }

                    if(countryType == null) countryType = "";

                    if(checkCountry == null || checkCountry.equals(""))
                    {
                        if(country == null || country.equals(""))
                        {
                           response.put(ValidationConstants.RESPONSE_RECIP_COUNTRY_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_COUNTRY_INVALID);
                        }
                        else
                        {
                           response.put(ValidationConstants.RESPONSE_RECIP_COUNTRY_INVALID + item.getLineNumber(), FieldUtils.replaceAll(ValidationConstants.RESPONSE_RECIP_COUNTRY_INVALID, "country", country));
                        }
                    }
                    // End that the recipient country id valid
                    
                    // Check that the state is valid for US and CA
                    if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                    {
                        boolean bStateExists = false;
                         // Try cache first
                        StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");
                        if(stateHandler != null)
                        {
                            StateMasterVO stateVo = stateHandler.getStateById(state);
                            if(stateVo != null)
                            {
                                bStateExists = true;
                            }
                        }
                        else
                        {
                            dataRequest.setConnection(conn);
                            dataRequest.setStatementID(GET_STATE_DETAILS);
                            dataRequest.addInputParam("STATE_ID", state);
                            stateIdExists = (String)vdau.execute(dataRequest);
                            dataRequest.reset();

                            if(!stateIdExists.equals("Y"))
                            {
                                bStateExists = false; 
                            }
                            else
                            {
                                bStateExists = true; 
                            }
                        }
                        
                        if(!bStateExists)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_STATE_INVALID + item.getLineNumber(), FieldUtils.replaceAll(ValidationConstants.RESPONSE_RECIP_STATE_INVALID, "state", state));
                        }
                    }
                    // End state validation

                    // Check for additional charge to AK and HI
                    String replacedStr = null;
                    Map values = null;
                    // Get the additional service charge                    
                    OEParameters globalParms = null;
                    OEDeliveryDateParm parms = new OEDeliveryDateParm();                    
                    DeliveryDateUTIL.getGlobalParameters(parms, conn);
                    globalParms = parms.getGlobalParms();
                    String price = globalParms.getSpecialSrvcCharge().toString();

                    //only do this check for carrier delivered items Defect#489
                    if(item.getShipMethodCarrierFlag() == null)
                    {
                      item.setShipMethodCarrierFlag("");
                    }
                    if(item.getShipMethodCarrierFlag().equals("Y"))
                    {
                        if(state.equals("AK") || state.equals("HI"))
                        {
                            values = new HashMap();
                            values.put("state", state);
                            values.put("price", price);
                            replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_RECIP_STATE_AK_HI, values);
                            response.put(ValidationConstants.RESPONSE_RECIP_STATE_AK_HI + item.getLineNumber(), replacedStr);
                        }
                    }
                    // End check for additional charge to AK and HI

                    // Check that the zip code is valid if the recipient is in the US or Canada
                    //Only check zip against DB for US orders, emueller, 3/26/04
                    if(country.equals(usCountryCode))
                    {
                        if(country.equals(usCountryCode) && zip.length() > 5)
                        {
                            zip = zip.substring(0, 5);
                        }
                        else if(country.equals(canadaCountryCode)  && zip.length() > 5)
                        {
                            // JMP - 12/04/2003
                            // Only validate the first five character for CA
                            zip = zip.substring(0, 5);
                        }

                        boolean zipExists = false;
                        // Check cache for zip code
                        logger.debug("before PostalCodeHandler");
                        PostalCodeHandler postalCodeHandler = (PostalCodeHandler)CacheManager.getInstance().getHandler("CACHE_NAME_POSTAL_CODE");
                        logger.debug("after PostalCodeHandler");
                        if(postalCodeHandler != null)
                        {
                            zipExists = postalCodeHandler.postalCodeExists(zip);
                        }
                        // Get the zip code data from the database if the
                        // cache handler is null or if the zip code does not exist
                        // in cache
                        if(postalCodeHandler == null || zipExists == false)
                        {        
                            dataRequest.setConnection(conn);
                            dataRequest.setStatementID(GET_ZIP_DETAILS);
                            dataRequest.addInputParam("ZIP_CODE", zip);
                            String zipCodeExists = (String)vdau.execute(dataRequest);
                            dataRequest.reset();
                            if(!zipCodeExists.equals("Y"))
                            {
                                zipExists = false;
                            }
                            else
                            {
                                zipExists = true;
                            }
                        }
                        
                        if(!zipExists)
                        {
                            response.put(ValidationConstants.RESPONSE_RECIP_ZIP_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_ZIP_INVALID);
                        }
                    }
                    // End zip code validation

                    // Check that the domestic indicator matches the recipient country
                    // US and CAN should be domestic
                    // All else should be international
                    // Get flag value
                    logger.debug("before GET_PRODUCT_DETAILS");
                    dataRequest.setConnection(conn);
                    dataRequest.setStatementID(GET_PRODUCT_DETAILS);
                    dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductId());
                    rs = (CachedResultSet)vdau.execute(dataRequest);
                    rs.reset();
                    dataRequest.reset();
                    while(rs.next())
                    {
                        domesticFlag = (String)rs.getObject(6);
                    }
                    
                    if(productVO != null){
                      if(!(ValidationConstants.PRODUCT_TYPE_SERVICES.equals(item.getProductType()) && ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP.equals(productVO.getProductSubType())))           
                      {
                          if(domesticFlag != null && domesticFlag.equals("D") && countryType.equalsIgnoreCase("I"))
                          {
                              response.put(ValidationConstants.RESPONSE_RECIP_INT_DOMESTIC_PROD + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_INT_DOMESTIC_PROD);
                          }
                          else if(domesticFlag != null && countryType.equalsIgnoreCase("D") && domesticFlag.equals("I"))
                          {
                              response.put(ValidationConstants.RESPONSE_RECIP_DOMESTIC_PROD_INT + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_DOMESTIC_PROD_INT);
                          }
                      }
                    }
                    String destType = recipAddress.getAddressType();
                    if(destType == null) destType = "";
                    String destName = recipAddress.getName();
                    if(destName == null) destName = "";
                    
                    // Check that the destination type is valid                        
                    if(!destType.equals(addressTypeB) && !destType.equals(addressTypeR) &&
                       !destType.equals(addressTypeF) && !destType.equals(addressTypeH) && !destType.equals(addressTypeC))
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_DEST_NOT_VALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_DEST_NOT_VALID);
                    }  

                    // Destination name is required for all but residential
                    if(!destType.equals(addressTypeR) && (destName.length() < 1))
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_DEST_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_DEST_MISSING);
                    }

                    // Check the destination name length
                    if(destName.length() > 40)
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_DEST_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_DEST_TOO_LONG);
                    }                        
                    logger.debug("before recipient phones");
                    // Get the recipient phone numbers
                    RecipientPhonesVO recipHomePhone = null;
                    RecipientPhonesVO recipWorkPhone = null;
                    RecipientPhonesVO recipPhone = null;

                    if(recipient.getRecipientPhones() != null)
                    {
                        for(int i = 0; i < recipient.getRecipientPhones().size(); i++)
                        {
                            recipPhone = (RecipientPhonesVO)recipient.getRecipientPhones().get(i);

                            if(recipPhone.getPhoneType().equalsIgnoreCase(phoneNumberTypeHome))
                            {
                                recipHomePhone = recipPhone;
                            }
                            else if(recipPhone.getPhoneType().equalsIgnoreCase(phoneNumberTypeWork))
                            {
                                recipWorkPhone = recipPhone;
                            }
                        }

                        // Check that the phone number does not contain alpha chars except ext or x
                        // Daytime Phone            
                        if(recipWorkPhone != null)
                        {
                            phoneNumber = recipWorkPhone.getPhoneNumber();
                            if(phoneNumber == null) phoneNumber = "";
                            
                            // Daytime phone number max length is US/CA=10, INT=20
                            if((country.equals(usCountryCode) || country.equals(canadaCountryCode)) && phoneNumber.length() > 10)
                            {
                                response.put(ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG);
                            }
                            else if((!country.equals(usCountryCode) && !country.equals(canadaCountryCode))  && phoneNumber.length() > 20)
                            {
                                response.put(ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG);
                            }

                            // Only do this for domestic phones
                            if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                            {                            
                                // remove ext, x and extension
                                phoneNumber = phoneNumber.toLowerCase();
                                if(phoneNumber.indexOf("ext") > -1 || phoneNumber.indexOf("x") > -1 || phoneNumber.indexOf("extension") > -1)
                                {
                                    phoneNumber = phoneNumber.replace('e', '0');
                                    phoneNumber = phoneNumber.replace('x', '0');
                                    phoneNumber = phoneNumber.replace('t', '0');
                                    phoneNumber = phoneNumber.replace('n', '0');
                                    phoneNumber = phoneNumber.replace('s', '0');
                                    phoneNumber = phoneNumber.replace('i', '0');
                                    phoneNumber = phoneNumber.replace('o', '0');
                                    phoneNumber = phoneNumber.replace(' ', '0');
                                }        
                                // Loop through phone numbers
                                sci = new StringCharacterIterator(phoneNumber);
                                for(char c = sci.first(); c != CharacterIterator.DONE; c = sci.next()) 
                                {
                                    if(!Character.isDigit(c))
                                    {
                                        response.put(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);
                                        break;
                                    }
                                }

                                // If phone number is present it must be 10 digits for US and CA
                                if(phoneNumber.length() > 0 && phoneNumber.length() < 10)
                                {
                                    response.put(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID, ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);
                                }
                            }
                        }
                    
                        // Check that the phone number does not contain alpha chars except ext or x
                        // Daytime Phone    
                        if(recipHomePhone != null)
                        {
                            phoneNumber = recipHomePhone.getPhoneNumber(); 
                            if(phoneNumber == null) phoneNumber = "";

                            // Evening phone number max length is US/CA=10, INT=20
                            if((country.equals(usCountryCode) || country.equals(canadaCountryCode)) && phoneNumber.length() > 10)
                            {
                                response.put(ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG);
                            }
                            else if((!country.equals(usCountryCode) && !country.equals(canadaCountryCode))  && phoneNumber.length() > 20)
                            {
                                response.put(ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_TOO_LONG);
                            }

                            // Only do this for domestic phones
                            if(country.equals(usCountryCode) || country.equals(canadaCountryCode))
                            {                                                        
                                // remove ext, x and extension
                                phoneNumber = phoneNumber.toLowerCase();
                                if(phoneNumber.indexOf("ext") > -1 || phoneNumber.indexOf("x") > -1 || phoneNumber.indexOf("extension") > -1)
                                {
                                    phoneNumber = phoneNumber.replace('e', '0');
                                    phoneNumber = phoneNumber.replace('x', '0');
                                    phoneNumber = phoneNumber.replace('t', '0');
                                    phoneNumber = phoneNumber.replace('n', '0');
                                    phoneNumber = phoneNumber.replace('s', '0');
                                    phoneNumber = phoneNumber.replace('i', '0');
                                    phoneNumber = phoneNumber.replace('o', '0');
                                    phoneNumber = phoneNumber.replace(' ', '0');
                                }        
                                // Loop through phone numbers
                                sci = new StringCharacterIterator(phoneNumber);
                                for(char c = sci.first(); c != CharacterIterator.DONE; c = sci.next()) 
                                {
                                    if(!Character.isDigit(c))
                                    {
                                        response.put(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);
                                        break;
                                    }
                                }

                                // If phone number is present it must be 10 digits for US and CA
                                if(phoneNumber.length() > 0 && phoneNumber.length() < 10)
                                {
                                    response.put(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);
                                }                            
                            }
                        }
                    }
                    else
                    {
                        response.put(ValidationConstants.RESPONSE_RECIP_PHONE_INVALID + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_PHONE_INVALID);                        
                    }
                    logger.debug("after recipient phones");
                }
                else
                {
                    response.put(ValidationConstants.RESPONSE_RECIP_INFO_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_INFO_MISSING);
                }
            }
            else
            {
                response.put(ValidationConstants.RESPONSE_RECIP_INFO_MISSING + item.getLineNumber(), ValidationConstants.RESPONSE_RECIP_INFO_MISSING);
            }
        }
        
        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending recipient validation");
        
        return response;
    }

    private static CountryMasterVO getCountryDetails(String countryId, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        CountryMasterVO countryVo = null;
        // Try cache first
        CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");    
        if(countryHandler != null)
        {
            countryVo = countryHandler.getCountryById(countryId);
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(GET_COUNTRY_DETAILS);
            dataRequest.addInputParam(COUNTRY_CODE, countryId);
            dataRequest.setConnection(con);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            String checkCountry = null;
            String countryType = null;
            while(rs.next())
            {
                checkCountry = (String)rs.getObject(1);
                countryType = (String)rs.getObject(2);  

                if(checkCountry != null)
                {
                    countryVo = new CountryMasterVO();
                    countryVo.setCountryId(checkCountry);
                    countryVo.setCountryType(countryType);
                }
            }
        }
        return countryVo;
    }

    private static SourceMasterVO getSourceDetails(String sourceCode, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");

        SourceMasterVO sourceVo = new SourceMasterVO();

        if(sourceHandler != null)
        {
            sourceVo = sourceHandler.getSourceCodeById(sourceCode);
        }
        // If the source handler is not found then lookup the source details from the database        
        else
        {     
            DataRequest dataRequest = new DataRequest();
            dataRequest.addInputParam("SOURCE_CODE", sourceCode);
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            String sourceCodeOut = null;
            String jcpFlag = null;
        
            while(rs.next())
            {
                sourceCodeOut = (String)rs.getObject(1);
                jcpFlag = (String)rs.getObject(30);
            }
            
            if(jcpFlag == null) jcpFlag = "";
            if(sourceCodeOut == null) sourceCodeOut = "";

            sourceVo.setSourceCode(sourceCodeOut);
            sourceVo.setJcpenneyFlag(jcpFlag);
        }

        return sourceVo;
    }    

    private static boolean checkForPO(String inStr)
    {
        boolean ret = false;
        if((inStr.indexOf("APO ") != -1) || (inStr.indexOf("A.P.O.") != -1) ||
           (inStr.indexOf("PO BOX") != -1) || (inStr.indexOf("P.O.") != -1) ||
           (inStr.indexOf("PO. BOX") != -1) || (inStr.indexOf("P.O BOX")!= -1) || (inStr.indexOf("POBOX")!= -1))
        {
        	ret = true;
        }
        return ret;
    }    
}