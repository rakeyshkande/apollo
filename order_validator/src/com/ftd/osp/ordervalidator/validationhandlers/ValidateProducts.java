/*
 *
 */
package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.StringReplacer;
import com.ftd.osp.ordervalidator.util.ValidateOrderPrice;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductAttributeVO;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.pac.util.PACUtil;


/**
 * This class validates product information from an Order object.  Verifies that
 * product information extists on each Item.  Checks for color and size selection
 * where needed.  Makes sure a price point was selected.
 * @author Jeff Penney
 *
 */
public class ValidateProducts
{
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    private static final String GET_PRODUCT_DETAILS_BY_TIMESTAMP = "GET PRODUCT DETAILS BY TIMESTAMP";
    private static final String GET_PRODUCT_SHIP_METHODS = "GET PRODUCT SHIP METHODS";
    private static final String GET_PRODUCT_COLORS = "GET PRODUCT COLORS";
    private static final String GET_PRODUCT_SUBCODE = "GET_PRODUCT_SUBCODE";
    //private static final String SIZE_STANDARD="A";
    //private static final String SIZE_DELUXE="B";
   // private static final String SIZE_PREMIUM="C";

    private static final String SATURDAY_CODE = "SA";
    //private static final String MAX_PRICE_VARIANCE = "MAX_PRICE_VARIANCE_AMOUNT";

    private static final String GET_GLOBAL_PARAM = "GET_GLOBAL_PARAM_VALIDATION";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static final String VALID_FLORAL_SHIP_METHODS = "VALID_SHIP_METHOD_";

    //private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    private static final String GET_BBN_AVAILABILITY = "GET_BBN_AVAILABILITY";

    private static final String PC_PDP_TEMPLATE = "PDP";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateProducts");

    /**
     * Validates the followin product information:
     * Make sure product is valid.  No drop ship delivery to Canada except for
     * gift certificates.  Product color must be selected if available.  Product
     * must have a price point selected.  Product must have price data.  The selected
     * color, price point, etc. must be valid.  Product must be in file.  No drop-ship
     * on Saturdays unless specified.
     * @param request Contains an Order object
     * @exception Exception if a database error occurs.
     * @return Returns any validation errors.
     *
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {
        logger.info("Starting product validation");

        OrderVO order = null;
        Connection conn = null;
        Boolean premierFound = false;

        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }

        OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
        DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");
        //String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");
        String prStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_PUERTO_RICO");
        String viStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_VIRGIN_ISLANDS");
        String akStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_ALASKA");
        String hiStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_HAWAII");
        String sdShipMethod = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "SAME_DAY_SHIP_METHOD");
        BigDecimal maxAddOnTotal = new BigDecimal("0.0");

        // Loop through order items
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        String productId = null;
        CachedResultSet rs = null;
        String domesticFlag = null;
        BigDecimal standardPrice = null;
        BigDecimal deluxePrice = null;
        BigDecimal premiumPrice = null;
        //BigDecimal maxVariablePrice = null;
        String deliveryCountry = null;
        String deliveryState = null;
        String shippingMethod = null;
        List shipMethods = null;
        List colors = null;
        String priceOverrideFlag = null;
        String preferredPartner = null;
        ProductVO productVO = null;

        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if (!premierFound && item.getPremierCollectionFlag() != null && item.getPremierCollectionFlag().equalsIgnoreCase("Y"))
            {
                premierFound = true;
            }

            // Determine if the order is associated with a Preferred Partner (e.g., USAA)
            if (preferredPartner == null && item.getPreferredProcessingPartner() != null) {
                preferredPartner = item.getPreferredProcessingPartner();
            }

            productId = item.getProductId();
            if(productId == null) productId = "";

            logger.info("item price override flag: " + item.getPriceOverrideFlag());
            priceOverrideFlag = item.getPriceOverrideFlag();
            if(priceOverrideFlag == null) priceOverrideFlag = "N";

            // Check for products on each item
            if(productId.length() < 1)
            {
                response.put(ValidationConstants.RESPONSE_PROD_INFO_MISSING +
                         item.getLineNumber(), ValidationConstants.RESPONSE_PROD_INFO_MISSING);
            }
            // end product existence test

            // Check for product existence in the database
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(GET_PRODUCT_DETAILS);
            dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, productId);
            rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();

            domesticFlag = null;
            standardPrice = null;
            deluxePrice = null;
            premiumPrice = null;
            Object standard = null;
            Object deluxe = null;
            Object premium = null;
            String availFlag = null;
            //String variablePriceFlag = null;
            //Object maxVariable = null;
            String shipMethodFlorist = null;
            String shipMethodCarrier = null;
            String productType = null;

            while(rs.next())
            {
                availFlag = (String)rs.getObject(5);
                domesticFlag = (String)rs.getObject(6);
                productType = (String)rs.getObject(8);
                standard = rs.getObject(11);
                deluxe = rs.getObject(12);
                premium = rs.getObject(13);

                // Variable price stuff
                //maxVariable = rs.getObject(15);
                //variablePriceFlag = (String)rs.getObject(60);

                shipMethodCarrier = (String)rs.getObject(56);
                shipMethodFlorist = (String)rs.getObject(57);
            }

            if(shipMethodCarrier == null) shipMethodCarrier = "N";
            if(shipMethodFlorist == null) shipMethodFlorist = "N";

            // If this is a sub-code we need to lookup the its details
            if(item.getProductSubCodeId() != null && item.getProductSubCodeId().length() > 0)
            {
                dataRequest.setConnection(conn);
                dataRequest.setStatementID(GET_PRODUCT_SUBCODE);
                dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductSubCodeId());
                rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
                dataRequest.reset();

                //String subCodeId = null;
                //String masterId = null;
                String activeFlag = null;
                Object price = null;

                while(rs.next())
                {
                    //subCodeId = (String)rs.getObject(2);
                   // masterId = (String)rs.getObject(1);
                    activeFlag = (String)rs.getObject(7);
                    price = rs.getObject(4);
                }

                if(activeFlag != null && activeFlag.equals("Y"))
                {
                    availFlag = "A";
                }
                else
                {
                    availFlag = "U";
                }

                standard = price;

            }

//            if(maxVariable == null)
//            {
//                maxVariablePrice = new BigDecimal("0");
//            }
//            else
//            {
//                maxVariablePrice = new BigDecimal(maxVariable.toString());
//            }

            if(availFlag != null && availFlag.equals("U"))
            {
                response.put(ValidationConstants.RESPONSE_PROD_UNAVAILABLE +
                         item.getLineNumber(), ValidationConstants.RESPONSE_PROD_UNAVAILABLE);
            }

            if(standard == null)
            {
                standardPrice = new BigDecimal("0");
            }
            else
            {
                standardPrice = new BigDecimal(standard.toString());
            }

            if(deluxe == null)
            {
                deluxePrice = new BigDecimal("0");
            }
            else
            {
                deluxePrice = new BigDecimal(deluxe.toString());
            }

            if(premium == null)
            {
                premiumPrice = new BigDecimal("0");
            }
            else
            {
                premiumPrice = new BigDecimal(premium.toString());
            }

            if(domesticFlag == null)
            {
                response.put(ValidationConstants.RESPONSE_PROD_INVALID +
                         item.getLineNumber(), ValidationConstants.RESPONSE_PROD_INVALID);
            }
            // End check for product existence in the database

            // Check for drop-ship to Canada and do not allow unless the product
            // is a gift certificate
            RecipientsVO recipient = null;
            RecipientAddressesVO recipAddress = null;

            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    deliveryCountry = recipAddress.getCountry();
                    deliveryState = recipAddress.getStateProvince();
                }
            }

            if(deliveryState == null) deliveryState = "";

            // No drop ship to Canada, PR or VI
            shippingMethod = item.getShipMethod();

            productVO = orderValidatorDAO.getProductDetails(item.getProductId());
            boolean isFTDWOrder = PACUtil.isFTDWOrder(productVO.getShippingSystem(), productType, shippingMethod);
            logger.info("isFTDWOrder --->" + isFTDWOrder);

            if(deliveryCountry != null && deliveryState != null &&
                     ((deliveryCountry.equals(canadaCountryCode) || deliveryState.equals(prStateCode) ||
                       deliveryState.equals(viStateCode) || deliveryCountry.equals(prStateCode) ||
                       deliveryCountry.equals(viStateCode)) &&
                       shippingMethod != null &&
                       shippingMethod.length() > 1 &&
                       !shippingMethod.equals(sdShipMethod)))
                  {
                      response.put(ValidationConstants.RESPONSE_NO_DROP_SHIP_CANADA +
                                   item.getLineNumber(), ValidationConstants.RESPONSE_NO_DROP_SHIP_CANADA);
                  }
                 if(productVO != null){
                	 if(isFTDWOrder && (deliveryState.equals(akStateCode) || deliveryState.equals(hiStateCode))){
                		   if(productVO.getProductType().equalsIgnoreCase(ValidationConstants.FRESH_CUT)){
                			   logger.info("WEST Freshcuts will not delivered to AK/HI");
               		          response.put(ValidationConstants.RESPONSE_NO_WESTFC_SHIP_AKHI +item.getLineNumber(), ValidationConstants.RESPONSE_NO_WESTFC_SHIP_AKHI);
                		 }
               		  }
                	 else if(!(item.getProductType().equals(ValidationConstants.PRODUCT_TYPE_SERVICES) && productVO.getProductSubType().equals(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP))){
                          // AK and HI can only deliver two day
                          shippingMethod = item.getShipMethod();
                          if((deliveryState.equals(akStateCode) || deliveryState.equals(hiStateCode)) &&
                             shippingMethod != null && !shippingMethod.equals(GeneralConstants.DELIVERY_SAME_DAY_CODE) && !shippingMethod.equals(GeneralConstants.DELIVERY_TWO_DAY_CODE))
                          {
                              response.put(ValidationConstants.RESPONSE_TWO_DAY_SHIPPING_ONLY +
                                           item.getLineNumber(), ValidationConstants.RESPONSE_TWO_DAY_SHIPPING_ONLY);
                          }
                    }
                }
              // #11946 - If customer opted for Morning delivery - validate BBN order for ZIP code, weekday/ SAT BBN delivery
              boolean morningDeliveryUnavailable = false;


            if(!isFTDWOrder) {
            	if ("Y".equals(item.getMorningDeliveryOpted()) || ("Y".equals(item.getOriginalOrderHasMDF())
            			&& new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), conn))) {
					try {
						// Morning Delivery opted, but unavailable in apollo - This check raises Number format exception if morning_delivery_fee is not a number.
						if(item.getMorningDeliveryFee() == null || Double.parseDouble(item.getMorningDeliveryFee()) < 0) {
							morningDeliveryUnavailable = true;
						}
					} catch(NumberFormatException e){
						logger.error("Error caught in ValidateProducts: checking for Morning_delivery_fee - " + e.getMessage());
						morningDeliveryUnavailable = true;
					}

					// Condition will be satisfied, when morning Delivery calculated to non-null double value
					if(!morningDeliveryUnavailable && !"Y".equals(productVO.getMorningDeliveryFlag())) {
						morningDeliveryUnavailable = true;
					}

					// validate for ZIP code availability for a given delivery date.
					if (!morningDeliveryUnavailable) {

						int deliveryDay = getDeliveryDay(recipAddress, conn, item.getDeliveryDate());
						String weekDayDelByNoon = "N";
						String satDelByNoon = "N";

						if (deliveryDay > 1 && deliveryDay < 7) {
							weekDayDelByNoon = "Y";
						} else if (deliveryDay == 7) {
							satDelByNoon = "Y";
						}

						String zipCode = recipAddress.getPostalCode();
						if(zipCode == null) {
							zipCode = "";
						}

						if("Y".equals(weekDayDelByNoon) || "Y".equals(satDelByNoon)) {
							dataRequest.setStatementID(GET_BBN_AVAILABILITY);
							if(zipCode.length() > 5) {
								logger.info("#11946 - Zip Code length is too long - " + zipCode);
								zipCode = zipCode.substring(0,5);
							}
							dataRequest.addInputParam("IN_ZIP_CODE", zipCode);
							dataRequest.addInputParam("WEEKDAY_BY_NOON", weekDayDelByNoon);
							dataRequest.addInputParam("SATURDAY_BY_NOON", satDelByNoon);

							Map outputs = (Map) vdau.execute(dataRequest);
						    String out_status = (String) outputs.get("OUT_STATUS");
						    String out_message = (String) outputs.get("OUT_MESSAGE");
						    // either null or "N"
							if("Y".equals(out_status) == false) {
						        logger.info("BBN availability : " + out_status + ", Error message: " + out_message);
						        morningDeliveryUnavailable = true;
						    }
						} else {
							logger.info("Error getting delivery day : not a weekday or not a saturday - BBN is unavailable");
							morningDeliveryUnavailable = true;
						}
					}
					if(morningDeliveryUnavailable) {
						logger.info("#11496 - ValidateProducts - BBN_UNAVAILBILE is set to response");
						response.put(ValidationConstants.RESPONSE_BBN_UNAVAILABLE + item.getLineNumber(), ValidationConstants.RESPONSE_BBN_UNAVAILABLE);
					}

            	}  else {
		            // If the delivery date is a Saturday make sure the product has Saturday delivery
		            if(shippingMethod != null && shippingMethod.equals(SATURDAY_CODE)) {
		                dataRequest.setStatementID(GET_PRODUCT_SHIP_METHODS);
		                dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, productId);
		                rs = (CachedResultSet)vdau.execute(dataRequest);
		                rs.reset();
		                dataRequest.reset();

		                shipMethods = new ArrayList();
		                while(rs.next()) {
		                    shipMethods.add(rs.getObject(1));
		                }

		                if(!shipMethods.contains("SA")) {
		                    response.put(ValidationConstants.RESPONSE_NO_SATURDAY_DELIVERY + item.getLineNumber(), ValidationConstants.RESPONSE_NO_SATURDAY_DELIVERY);
		                }
		            }
            	}

				MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
	            if(order.getOrderOrigin().equalsIgnoreCase(configUtil.getProperty(METADATA_CONFIG_FILE_NAME, ValidationConstants.AMAZON_ORIGIN)) ||
	            		mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin()) ||
	            		new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), conn)) {
	                //Check to see if item contains an invalid ship method for a product.
	                //For florist delivered orders, check ship method against VALID_SHIP_METHODS from
	                //metadata_config.xml file.

	                //For carrier delivered orders, only products that allow two-day shipping will be
	                //merchandised on Amazon.com/Mercent.  However, for carrier delivered orders that contain
	                //a freshcut or same day fresh cut, the ship method will be set to Next Day Delivery ("ND").
	                //For same day fresh cuts ordered from Amazon.com/Mercent, if Next Day delivery is only available
	                //for these products, the shipping charge is still the same and two day $9.99.

	                boolean invalidCarrierShipMethod = false;
	                boolean invalidFloristShipMethod = false;

	                if( shipMethodCarrier.equalsIgnoreCase("Y")) {
	                  dataRequest.setStatementID(GET_PRODUCT_SHIP_METHODS);
	                  dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, productId);
	                  rs = (CachedResultSet)vdau.execute(dataRequest);
	                  rs.reset();
	                  dataRequest.reset();

	                  shipMethods = new ArrayList();
	                  while(rs.next()) {
	                      shipMethods.add(rs.getObject(1));
	                  }

	                  //check for valid carrier ship methods.
	                  if(!shipMethods.contains(item.getShipMethod()))
	                      invalidCarrierShipMethod = true;
	                }

	                if(shipMethodFlorist.equalsIgnoreCase("Y")) {
	                   //retrieve valid florist related ship methods from config file
	                   //load list with valid prefixes from gatherer_config file
	                   Iterator shipMethodIterator = null;
	                   String key = null;
	                   String value = null;

	                   ArrayList validShipMethods = new ArrayList();
	                   Map map = configUtil.getProperties(METADATA_CONFIG_FILE_NAME);
	                   shipMethodIterator = map.keySet().iterator();

	                   while(shipMethodIterator.hasNext())
	                   {
	                      key = (String)shipMethodIterator.next();
	                      value = (String)map.get(key);

	                      if(key.startsWith(VALID_FLORAL_SHIP_METHODS)) {
	                          validShipMethods.add(value);
	                      }
	                   }

	                   //check for valid florist ship methods.  "" and "SD" are the only valid ship methods for
	                   //florist delivered products for Amazon.com orders
	                   if(!validShipMethods.contains(item.getShipMethod()))
	                        invalidFloristShipMethod = true;
	                }

	                if(invalidCarrierShipMethod && invalidFloristShipMethod) {
	                    response.put(ValidationConstants.RESPONSE_INVALID_SHIP_METHOD +
	                                 item.getLineNumber(), ValidationConstants.RESPONSE_INVALID_SHIP_METHOD);
	                }

	                //if price difference is greater than the max price variance then
	                //set error.

	                try
	                {
	                   boolean ceilingVariance = ValidateOrderPrice.validate(order, item, conn);
	                   logger.info("ceilingVariance: " + ceilingVariance);
	                }
	                catch (Exception e)
	                {
	                    logger.error("exception thrown during order price validation.", e);
	                    throw new ValidationException("Unable to validate order price.");
	                }//end catch

	//                logger.debug("priceOverrideFlag: " + priceOverrideFlag);
	//                if(!priceOverrideFlag.equalsIgnoreCase("Y") && hasPriceVariance)
	//                {
	//                  logger.debug("right before setting response_exceeds_price_variance_max error");
	//                  response.put(ValidationConstants.RESPONSE_EXCEEDS_PRICE_VARIANCE_MAX +
	//                                   item.getLineNumber(), ValidationConstants.RESPONSE_EXCEEDS_PRICE_VARIANCE_MAX);
	//                }

	             }
	        }


           /*
            * COLOR CODES NO LONGER USED FOR ORIGINAL PURPOSE - SO COLOR LOGIC WAS
            * COMMENTED OUT MARCH 2018. COLOR TABLES HAVE BEEN REPURPOSED AS
            * PRODUCT MARKERS FOR PROJECT FRESH.

            // Check for no color selected
            // Get product color choices
            dataRequest.setStatementID(GET_PRODUCT_COLORS);
            dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, productId);
            rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();

            colors = new ArrayList();
            while(rs.next())
            {
                colors.add(rs.getObject(1));
            }

            String colorFirstChoice = item.getColorFirstChoice();
            if(colorFirstChoice == null)
            {
                colorFirstChoice = "";
            }

            String colorSecondChoice = item.getColorSecondChoice();
            if(colorSecondChoice == null)
            {
                colorSecondChoice = "";
            }

            if(colors.size() > 0 && ((colorFirstChoice.length() < 1) || (colorSecondChoice.length() < 1)))
            {
                response.put(ValidationConstants.RESPONSE_NO_COLOR_SELECTED + item.getLineNumber(), FieldUtils.replaceAll(ValidationConstants.RESPONSE_NO_COLOR_SELECTED,"product", productId));
            }
            */

            // Check for no price point selected
            String productAmountStr = item.getProductsAmount();
            if(productAmountStr == null) productAmountStr = "0";
            BigDecimal productAmount = new BigDecimal(productAmountStr);

            String size = item.getSizeChoice();
            if(size == null) size = "";

            if(!size.equals("A") && !size.equals("B") && !size.equals("C"))
            {
                // no price selected and we have more than one price for this product
                response.put(ValidationConstants.RESPONSE_NO_PRICE_POINT_SELECTED + item.getLineNumber(), FieldUtils.replaceAll(ValidationConstants.RESPONSE_NO_PRICE_POINT_SELECTED, "product", productId));
            }

            // Check for a missing price on the item product
            if(productAmount.compareTo(BigDecimal.valueOf(0)) == 0)
            {
                // no price selected and we have more than one price for this product
                response.put(ValidationConstants.RESPONSE_NO_PRICE_SELECTED + item.getLineNumber(), ValidationConstants.RESPONSE_NO_PRICE_SELECTED);
            }

            /*
            // Check for invalid variable price
            if(variablePriceFlag != null && variablePriceFlag.equalsIgnoreCase("Y"))
            {
                double origAmountDouble = Double.parseDouble(productAmountStr);
                if(origAmountDouble > 0 && (origAmountDouble < standardPrice.doubleValue() || origAmountDouble > maxVariablePrice.doubleValue()))
                {
                    Map values = new HashMap();
                    values.put("minprice", standardPrice.toString());
                    values.put("maxprice", maxVariablePrice.toString());
                    String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PROD_PRICE_VARIABLE_INVALID, values);
                    response.put(ValidationConstants.RESPONSE_PROD_PRICE_VARIABLE_INVALID + item.getLineNumber(), replacedStr);
                }
            }
            */
            // Next check price points
            /*else*/
            boolean newWebsiteOrder = false;
            if (order.getOrderCalcType() != null && order.getOrderCalcType().equalsIgnoreCase("CALYX")) {
                newWebsiteOrder = true;
            }
            logger.info("orderCalcType: " + order.getOrderCalcType() + " " + newWebsiteOrder);
            if(productAmount.compareTo(standardPrice) != 0 && productAmount.compareTo(deluxePrice) != 0 &&
                    productAmount.compareTo(premiumPrice) != 0 && !newWebsiteOrder)
            {
                // Defect 1768: If the check for the current product price lookup does not match,
                // attempt a lookup of the historical product price at order creation.
                Map values = new HashMap();
                values.put("product", productId);
                values.put("price_one", standardPrice.toString());
                values.put("price_two", deluxePrice.toString());
                values.put("price_three", premiumPrice.toString());
                values.put("date_time", "current date");
                String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PRICE_POINT_INVALID, values);
                logger.warn("validate: " + replacedStr + ". Attempting to verify HISTORICAL price at order creation.");

                String priceDateString = null;
                Date priceDateTime = null;
                if (item.getUpdatedOnProductsAmountTime() != null) {
                    // Use the last product price-update timestamp on the detail record.
                    priceDateTime = item.getUpdatedOnProductsAmountTime();
                    priceDateString = item.getUpdatedOnProductsAmount();
                } else {
                    // Use the order header order creation date.
                    priceDateString = order.getOrderDate();
                    priceDateTime = order.getOrderDateTime();
                }

                if (priceDateTime != null) {

                    // Record the date.
                    values.put("date_time", priceDateString);
                    replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PRICE_POINT_INVALID, values);

                // Query the historical product data.
                dataRequest.setStatementID(GET_PRODUCT_DETAILS_BY_TIMESTAMP);
                dataRequest.addInputParam("IN_PRODUCT_ID", productId);
                dataRequest.addInputParam("IN_DATE_TIME", new java.sql.Timestamp(priceDateTime.getTime()));
                rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
                dataRequest.reset();

                // Only one record (or none) should return.
                if (rs.next()) {
                    // Record the historical standard price.
                    standardPrice = rs.getBigDecimal("standard_price");
                    BigDecimal subcodePrice = rs.getBigDecimal("subcode_price");
                    if (subcodePrice != null && subcodePrice.floatValue() > 0 ) {
                        standardPrice = subcodePrice;
                    }

                    // Historical premium and deluxe prices.
                    deluxePrice = rs.getBigDecimal("deluxe_price");
                    premiumPrice = rs.getBigDecimal("premium_price");

                    // Record the latest values.
                     values.put("price_one", standardPrice.toString());
                     values.put("price_two", deluxePrice.toString());
                     values.put("price_three", premiumPrice.toString());
                } else {
                    logger.warn("validate: HISTORICAL price changes do not exist for specified date: " + replacedStr);
                }
                } else {
                    logger.warn("validate: Could not verify HISTORICAL price because date value was not populated within OrderVO nor OrderDetailVO value objects: " + replacedStr);
                }

                // Perform a second validation on the latest values. should not be performed for PDP PC products.
                 if(productAmount.compareTo(standardPrice) != 0 && productAmount.compareTo(deluxePrice) != 0 && productAmount.compareTo(premiumPrice) != 0 && !PC_PDP_TEMPLATE.equalsIgnoreCase(productVO.getPersonalizationTemplate())) {
                replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PRICE_POINT_INVALID, values);
                logger.warn("validate: HISTORICAL product price at order creation failed validation: " + replacedStr);
                response.put(ValidationConstants.RESPONSE_PRICE_POINT_INVALID + item.getLineNumber(), replacedStr);
                 } else {
                     replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PRICE_POINT_VALID, values);
                     logger.warn("validate: HISTORICAL product price at order creation passed validation: " + replacedStr);
                 }
            }

            /*
            // Check for invalid color or price point selection
            // Check colors first
            if(colors.size() > 0 && (!colors.contains(colorFirstChoice) || !colors.contains(colorSecondChoice)))
            {
                Map values = new HashMap();
                values.put("color_one", colorFirstChoice);
                values.put("color_two", colorSecondChoice);
                String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_COLOR_INVALID, values);
                response.put(ValidationConstants.RESPONSE_COLOR_INVALID + item.getLineNumber(), replacedStr);
            }
            */

            boolean byPassCardinalCommerceChecks = false;
            GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
            if(parmHandler != null)
            {
                if(parmHandler.getGlobalParm("VALIDATION_SERVICE", "BYPASS_CARDINAL_COMMERCE_MAX_CHECKS").equalsIgnoreCase("Y"))
                {
                	byPassCardinalCommerceChecks = true;
                }
            }
        	logger.info("###byPassCardinalCommerceChecks: "+ byPassCardinalCommerceChecks);

        	Collection payments = order.getPayments();
        	Iterator paymentsIt = null;
            boolean isOrderCardinalVerified = false;
            if(payments != null && payments.size() > 0)
            {
                // Get payments
            	paymentsIt = payments.iterator();
                String cardinalVerifiedFlag = null;

                PaymentsVO payment = null;
                while(paymentsIt.hasNext())
                {
                    payment = (PaymentsVO)paymentsIt.next();
                    cardinalVerifiedFlag = payment.getCardinalVerifiedFlag();

                    if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y"))
                    {
                    	isOrderCardinalVerified = true;
                    }
                }
            }
            logger.info("###isOrderCardinalVerified: "+ isOrderCardinalVerified);

            // Check if addon total exceeds fraud limit
            if(item.getAddOnAmount() != null && item.getAddOnAmount().trim().length() > 0)
            {
                if (preferredPartner != null)
                {
                    String max = getFrpGlobalParm(preferredPartner + "_MAX_ITEM_ADDON_TOTAL", vdau, conn);
                    if (max != null) {
                        maxAddOnTotal = new BigDecimal(max);
                    } else {
                        logger.error("Missing " + preferredPartner + "_MAX_ITEM_ADDON_TOTAL in FRP.GLOBAL_PARMS table");
                        maxAddOnTotal = new BigDecimal(0);
                    }
                } else if (premierFound)
                {
                    maxAddOnTotal = new BigDecimal(getFrpGlobalParm("LUX_MAX_ITEM_ADDON_TOTAL", vdau, conn));
                }
                else if (isOrderCardinalVerified)
	            {
	                maxAddOnTotal = new BigDecimal(getFrpGlobalParm("CARDINAL_COMMERCE_MAX_ITEM_ADDON_TOTAL", vdau, conn));
	            }
                else
                {
                    maxAddOnTotal = new BigDecimal(getFrpGlobalParm("MAX_ITEM_ADDON_TOTAL", vdau, conn));
                }
                BigDecimal addOnTotal = new BigDecimal(item.getAddOnAmount());

	            if(addOnTotal.doubleValue() > maxAddOnTotal.doubleValue())
		        {
	                if(isOrderCardinalVerified && byPassCardinalCommerceChecks)
	                {
	                	//bypass error message
	                	logger.info("bypass item addon total exceeds max error message");
	                }
	                else{
	                	//show error message
	                 	response.put(ValidationConstants.RESPONSE_ITEM_ADDON_TOTAL_EXCEEDS_MAX + item.getLineNumber(), FieldUtils.replaceAll(ValidationConstants.RESPONSE_ITEM_ADDON_TOTAL_EXCEEDS_MAX, "addon", maxAddOnTotal.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString()));
	              	}
	            }
            }

            // Perform Source Product Exclusion Validation
            validateProductNotExcludedForSource(conn, item, productVO, response);

        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending product validation");

        return response;
    }

    private static String getFrpGlobalParm(String key, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        String value = null;

        // Try cache first
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {
            value = parmHandler.getGlobalParm("VALIDATION_SERVICE", key);
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.addInputParam("IN_PARAM", key);
            dataRequest.setStatementID(GET_GLOBAL_PARAM);
            String rs = (String)vdau.execute(dataRequest);
            value = rs;
            dataRequest.reset();
        }

        return value;
    }

   /**
   * Validate the product against its soure code for exclusions.
   * At this time, only the Service Sub Type/FREESHIP exclusion is checked.
   * @param connection
   * @param item
   * @param product
   * @param response
   */
    @SuppressWarnings("unchecked")
	private static void validateProductNotExcludedForSource(Connection connection, OrderDetailsVO item, ProductVO productVO, ValidationResponse response)
    throws Exception
    {
      if(item.getSourceCode() == null || productVO == null) {
        return; // Nothing to check
      }

      // Check for the Product Attribute Restriction for Free Shipping on the
      if(item.getProductType().equals(ValidationConstants.PRODUCT_TYPE_SERVICES) && productVO.getProductSubType().equals(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP)){
        SourceProductUtility sourceProductUtility = new SourceProductUtility();
        List<ProductAttributeVO> restrictions = sourceProductUtility.getProductAttrSourceExcl(connection, item.getSourceCode());

        if(restrictions == null || restrictions.size() == 0)
        {
          return;
        }

        for(ProductAttributeVO productAttributeVO: restrictions)
        {
          if("PRODUCT_SUB_TYPE".equals(productAttributeVO.getProductAttributeName())
          && "FREESHIP".equals(productAttributeVO.getProductAttributeValue()))
          {
              String freeShippingDisplayName = "";

              CacheUtil cacheUtil = CacheUtil.getInstance();
              AccountProgramMasterVO apmVO = cacheUtil.getFSAccountProgramDetailsFromContent();
              if (apmVO != null) {
                  freeShippingDisplayName = apmVO.getDisplayName();
              } else {
                  OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(connection);
                  freeShippingDisplayName = orderValidatorDAO.getProgramDisplayName(ValidationConstants.FREE_SHIPPING_NAME);
              }
            String errorMessage = FieldUtils.replaceAll(ValidationConstants.RESPONSE_PRODUCT_SERVICE_SOURCE_EXCLUDED, "{programName}", freeShippingDisplayName);

            logger.info(errorMessage);
            response.put(ValidationConstants.RESPONSE_PRODUCT_SERVICE_SOURCE_EXCLUDED + item.getLineNumber(), errorMessage);
            break;
          }
        }

      }
    }

	/** Determine the day of the week for a given date.
	 * @param address
	 * @param connection
	 * @param delDate
	 * @return
	 * @throws ParseException
	 */
	private static int getDeliveryDay(RecipientAddressesVO address, Connection connection, String delDate) throws ParseException {

		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date deliveryDate;
		try {
			deliveryDate = formatter.parse(delDate);

			Calendar cal = Calendar.getInstance();
			cal.setTime(deliveryDate);

			logger.debug("#11946 - Day of the week : " + cal.get(Calendar.DAY_OF_WEEK));
			return cal.get(Calendar.DAY_OF_WEEK);

		} catch (Exception e) {
			logger.error("Error parsing delivery date - Delivery date passed is null: "+e.getMessage());
		}

		return -1;
	}
}