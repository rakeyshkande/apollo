package com.ftd.osp.ordervalidator.validationhandlers;


import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.ordervalidator.util.*;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;

import java.sql.Connection;
import java.math.*;


public class ValidateMembershipId 
{
    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    private static final String CHECK_AAA_MEMBERSHIP = "CHECK_AAA_MEMBERSHIP";
    private static final String GET_PARTNER = "GET_PARTNER";
    
    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateMembershipId");
    
    /**
     * Validate a membership ID using a partner ID (AAA, NWA, ...) from the source code. 
     * @param request Contains an Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing an errors
     *
     */
    public static ValidationResponse validate(ValidationRequest request) throws Exception
    {    
        logger.info("Starting membership validation");
    
        OrderVO order = null;
        Connection conn = null;
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        
        
        
        ValidationResponse response = new ValidationResponse();
        
        DataRequest dataRequest = new DataRequest();

        boolean validMembershipId = true;
        String membershipId = null;
        MembershipsVO membership = null;

        // Get the partner ID from the source code
        String partnerId = null; // This is program name.
        String partnerName = null;
        // Try source code cache
        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        if(sourceHandler != null)
        {
            SourceMasterVO sourceVo = sourceHandler.getSourceCodeById(order.getSourceCode());
            if(sourceVo != null)
            {
                partnerId = sourceVo.getPartnerId();
                partnerName = sourceVo.getPartnerName();
            }
        }
        // Get the source code data from the database
        else
        {        
            dataRequest.addInputParam("SOURCE_CODE", order.getSourceCode());
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            
            while(rs.next())
            {
                partnerId = (String)rs.getObject(9);
                partnerName = rs.getString("partner_name");
            }
        }
        
        if(partnerId == null) partnerId = "";
        if(partnerName == null) partnerName = "";

        if(order.getMemberships() != null && order.getMemberships().size() > 0 && partnerId.length() > 0)
        {
            membership = (MembershipsVO)order.getMemberships().get(0);

            membershipId = membership.getMembershipIdNumber();
            validMembershipId = new ValidateMembership().validateMembershipById(membershipId, partnerId, partnerName, conn);
        }
        if(!validMembershipId && partnerId.length() > 0 && (membershipId == null || membershipId.length() == 0))
        {
            response.put(ValidationConstants.RESPONSE_MEMBERSHIP_MISSING, ValidationConstants.RESPONSE_MEMBERSHIP_MISSING);
        }
        else if(!validMembershipId)
        {
            response.put(ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID, ValidationConstants.RESPONSE_MEMBERSHIP_NOT_VALID);
        }

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending membership validation");        

        return response;
        
    }


}