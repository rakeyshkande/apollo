package com.ftd.osp.ordervalidator.validationhandlers;

import com.ftd.osp.ordervalidator.vo.*;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.order.vo.*;

import java.sql.Connection;
import com.ftd.osp.ordervalidator.exception.*;
import com.ftd.osp.ordervalidator.util.*;

public class ValidateGiftCertificate 
{
    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateGiftCertificate");
    
    /**
     * Validates a gift certificate ID against the database. 
     * @param request Contains the Order object
     * @exception Exception if a database error occurs
     * @return The ValidationResponse contains an error if the gift cert does not
     * exist or has already been used.
     *
     */
    public static ValidationResponse validate(ValidationRequest request) throws ValidationException
    {    
        logger.info("Starting gift certificate validation");

        OrderVO order = null;
        Connection conn = null;
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }      

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }
        
        ValidationResponse response = new ValidationResponse();

        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending gift certificate validation");
        
        return response;

/*

        FTDSystemVO systemVO = null;
        double orderTotalAmount;
        double maxAmount;
        double certificateAmount = 0;
        double adjustedTotal = 0;
        double adjustmentAmount = 0;
        boolean validCertificate = false;

        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = xmlDocument.createElement("root");
        Element listElement = xmlDocument.createElement("validation");
        rootElement.appendChild(listElement);
        listElement.setAttribute("object", "GIFT_CERTIFICATE_ID");

        try
        {
            FTDDataRequest dataRequest = new FTDDataRequest();
            dataRequest.addArgument("giftCertificateId", (String) request.get("GIFT_CERTIFICATE_ID"));

            GenericDataService service = this.getGenericDataService(DataConstants.VALIDATE_GIFT_CERTIFICATE);
            FTDDataResponse dataResponse = service.execute(dataRequest);

            NodeList nl = null;
            String xpath = "validateGiftCertificate/giftCertificateCheck/giftCertificate";
            XPathQuery q = new XPathQuery();
            nl = q.query((XMLDocument)dataResponse.getDataVO().getData(), xpath);
            Element giftCertLM = (Element) nl.item(0);

            if ( giftCertLM == null )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Invalid);
            }
            else if ( giftCertLM.getAttribute("redemptionFlag").equals("Y") )
            {
                listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Redeemed);
            }
            else
            {
                java.util.Date expireDate = FieldUtils.formatStringToUtilDate(giftCertLM.getAttribute("expirationDate"));
                java.util.Date currentDate = FieldUtils.formatStringToUtilDate(FieldUtils.formatUtilDateToString(GregorianCalendar.getInstance().getTime()));
                if (( expireDate == null) || (currentDate.compareTo(expireDate) <= 0 ))
                {
                    // Get the order total amount and max gift certificate allowed amount
                    orderTotalAmount = Double.parseDouble((String) request.get("GIFT_ORDER_TOTAL_AMOUNT"));
                    maxAmount = Double.parseDouble((String) request.get("GIFT_MAX_AMOUNT"));

                    // Get the amount of the gift certificate
                    certificateAmount = Double.parseDouble(giftCertLM.getAttribute("certificateAmount"));

                    // If the amount is greater than the max amount allowed, apply the max amount,
                    // otherwise apply the certificate amount
                    adjustmentAmount = Math.min(certificateAmount, maxAmount);
                    adjustedTotal = orderTotalAmount - adjustmentAmount;

                    // If the adjusted order total is less than zero, make it zero
                    adjustedTotal = Math.max(adjustedTotal, 0);

                    listElement.setAttribute("result", "OK");
                    validCertificate = true;
                }
                else
                {
                    listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Expired);
                }
            }

            if ( validCertificate )
            {
                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_ADJUSTED_TOTAL");
                listElement.setAttribute("result", FieldUtils.formatDoubleNoRound(adjustedTotal));

                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_CERTIFICATE_AMOUNT");
                listElement.setAttribute("result", FieldUtils.formatDoubleNoRound(certificateAmount));

                listElement = xmlDocument.createElement("validation");
                rootElement.appendChild(listElement);
                listElement.setAttribute("object", "GIFT_DESCRIPTION");
                listElement.setAttribute("result", "Your gift certificate was applied in the amount of " +
                                            FieldUtils.formatBigDecimalAsCurrency(new BigDecimal(adjustmentAmount), 2));
            }
        }
        catch(Exception e)
        {
            listElement.setAttribute("result", OEValidationErrorMessage.Billing_Certificate_Invalid);
        }
        finally
        {
            XMLDocument document = XMLEncoder.createXMLDocument("validateGiftCertificate");
            XMLEncoder.addSection(document, rootElement.getChildNodes());

            systemVO = new FTDSystemVO();
            systemVO.setXML(document);
        }

        return systemVO;
*/    
    }
}