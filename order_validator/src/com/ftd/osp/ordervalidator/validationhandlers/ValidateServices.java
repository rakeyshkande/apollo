package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MembershipDetailsVO;

/**
 * This class validates various fields if the free_shipping_flag of any item in the
 * cart is set to Y. This means that the website informed the customer that they
 * received Free Shipping.
 * 
 * 
 * @author Tim Schmig
 *
 */
public class ValidateServices {

    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    
    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateServices");
    
    public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, Exception
    {    
        logger.info("Starting services validation");
    
        OrderVO order = null;
        Connection conn = null;
        ValidationResponse response = new ValidationResponse();
        
        // Catch objects that are not of type Order
        try {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null) {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        } catch(ClassCastException cce) {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }
        
        logger.info("order: " + order.getMasterOrderNumber());
        logger.info("fs use: " + order.getFreeShippingUseFlag());
        logger.info("fs purchase: " + order.getFreeShippingPurchaseFlag());
        logger.info("buyerSignedIn: " + order.getBuyerSignedIn());

        String freeShippingDisplayName = "";
        
        AccountProgramMasterVO apmVO = CacheUtil.getInstance().getFSAccountProgramDetailsFromContent();;
        if (apmVO != null) {
            freeShippingDisplayName = apmVO.getDisplayName();
        } else {
            OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
            freeShippingDisplayName = orderValidatorDAO.getProgramDisplayName(ValidationConstants.FREE_SHIPPING_NAME);
        }
        logger.info("freeShippingDisplayName: " + freeShippingDisplayName);        
        Boolean flag = null;
    	List<String> emailList = new ArrayList<String>();
    	emailList.add(order.getBuyerEmailAddress());
    	Map<String, MembershipDetailsVO> membershipMap = FTDCAMSUtils.getMembershipData(emailList, order.getOrderDateTime());
    	if(membershipMap == null){
    		flag = null;		
    	}else{
    		if(membershipMap.containsKey(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE)){
    			MembershipDetailsVO fsMemberDetails = membershipMap.get(FTDCAMSUtils.FREESHIPPING_MEMBER_TYPE);
    			if(fsMemberDetails != null){
    				flag = fsMemberDetails.isMembershipBenefitsApplicable();
    			}else{
    				flag = false;
    			}
    		}else{
    			flag = false;
    		}
    		
    	}
    	logger.info("Flag Value : "+flag);
    	
        Boolean hasFreeShippingAccount = flag;
        if(hasFreeShippingAccount == null)
        {
        	logger.info("Unable to receive response from CAMS ");
            String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_CAMS_UNREACHABLE, "{programName}", freeShippingDisplayName);
            logger.info("msg: " + msg);
            response.put(ValidationConstants.RESPONSE_CAMS_UNREACHABLE, msg);           
        }
       

        if ( (order.getFreeShippingUseFlag() != null && order.getFreeShippingUseFlag().equalsIgnoreCase("Y")) ||
                (order.getFreeShippingPurchaseFlag() != null && order.getFreeShippingPurchaseFlag().equalsIgnoreCase("Y"))) {

            // Get the database connection
            try {
                conn = (Connection)request.get(ValidationConstants.CONNECTION);
                if(conn == null) {
                    throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
                }
            } catch(ClassCastException cce) {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }    

            // Get the ValidationDataAccessUtil from the request
            ValidationDataAccessUtil vdau = null;
            try {
                vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
                if(vdau == null) {
                    throw new ValidationException("ValidationDataAccessUtil cannot be null");
                }
            } catch(ClassCastException cce) {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }        

            DataRequest dataRequest = new DataRequest();

            String freeShippingAllowedFlag = null;

            SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
            if(sourceHandler != null) {
                SourceMasterVO sourceVo = sourceHandler.getSourceCodeById(order.getSourceCode());
                if(sourceVo != null) {
                    freeShippingAllowedFlag = sourceVo.getAllowFreeShippingFlag();
                }
            } else {
                // Get the source code data from the database
                dataRequest.addInputParam("SOURCE_CODE", order.getSourceCode());
                dataRequest.setConnection(conn);
                dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
                CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
        
                while(rs.next()) {
                    freeShippingAllowedFlag = rs.getString("ALLOW_FREE_SHIPPING_FLAG");
                }
            }
            logger.info("source freeShippingAllowedFlag: " + freeShippingAllowedFlag);
          
            if ( (freeShippingAllowedFlag == null || !freeShippingAllowedFlag.equalsIgnoreCase("Y")) &&
                 (order.getFreeShippingUseFlag() != null && order.getFreeShippingUseFlag().equalsIgnoreCase("Y")) ) {
                String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_SOURCE_CODE_FS_INVALID, "{programName}", freeShippingDisplayName);
                logger.info("msg: " + msg);
                response.put(ValidationConstants.RESPONSE_SOURCE_CODE_FS_INVALID, msg);
            }

            logger.info("emailAddress: " + order.getBuyerEmailAddress());
          
          	logger.info("hasFreeShippingAccount: " + hasFreeShippingAccount);
            
        	if (order.getBuyerEmailAddress()!=null && !"".equals(order.getBuyerEmailAddress()) && hasFreeShippingAccount != null && !hasFreeShippingAccount && "Y".equalsIgnoreCase(order.getFreeShippingUseFlag())) {
        	//SN
	        	String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_EMAIL_ADDRESS_NO_FSMEMBERSHIP, "{programName}", freeShippingDisplayName);
	            logger.info("msg: " + msg);
	            response.put(ValidationConstants.RESPONSE_EMAIL_ADDRESS_FS_INVALID, msg);
            }

            // Loop thorugh order items
            Collection items = order.getOrderDetail();
            Iterator it = items.iterator();
            CachedResultSet rs;
            while(it.hasNext()) {
                OrderDetailsVO item = (OrderDetailsVO)it.next();

                dataRequest.setConnection(conn);
                dataRequest.setStatementID(GET_PRODUCT_DETAILS);
                dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductId());
                rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
                dataRequest.reset();
                
                freeShippingAllowedFlag = "";
                String productSubType = "";
                while (rs.next()) {
                    freeShippingAllowedFlag = rs.getString("allowFreeShippingFlag");
                    productSubType = rs.getString("productSubType");
                }
                logger.info("product freeShippingAllowedFlag: " + freeShippingAllowedFlag);
                logger.info("productSubType: " + productSubType);

                if ( (item.getFreeShipping() != null && item.getFreeShipping().equalsIgnoreCase("Y")) &&
                        (freeShippingAllowedFlag == null || !freeShippingAllowedFlag.equalsIgnoreCase("Y")) ) {
                    String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_PRODUCT_FS_NOT_ALLOWED, "{programName}", freeShippingDisplayName);
                    logger.info("msg: " + msg);
                    response.put(ValidationConstants.RESPONSE_PRODUCT_FS_NOT_ALLOWED + item.getLineNumber(), msg);
                }

                if (order.getBuyerSignedIn() == null || !order.getBuyerSignedIn().equalsIgnoreCase("Y")) {
                    if (productSubType != null && productSubType.equalsIgnoreCase(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP)) {
                        String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_BUYER_NOT_SIGNED_IN_PURCHASE, "{programName}", freeShippingDisplayName);
                        logger.info("msg: " + msg);
                        response.put(ValidationConstants.RESPONSE_BUYER_NOT_SIGNED_IN_PURCHASE + item.getLineNumber(), msg);
                    } else {
                        String msg = FieldUtils.replaceAll(ValidationConstants.RESPONSE_BUYER_NOT_SIGNED_IN_USE, "{programName}", freeShippingDisplayName);
                        logger.info("msg: " + msg);
                        response.put(ValidationConstants.RESPONSE_BUYER_NOT_SIGNED_IN_USE + item.getLineNumber(), msg);
                    }
                }
            }
        }
        
        logger.info("Ending Services validation");
        
        return response;
    }
}