package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.DispatchCancelledOrder;
import com.ftd.osp.ordervalidator.util.StringReplacer;
import com.ftd.osp.ordervalidator.util.ValidateOrderPrice;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.pac.util.PACUtil;
import com.ftd.pas.core.domain.ProductAvailVO;

/**
 * Validates the Order object for delivery dates that are deliverable
 * @author Jeff Penney
 *
 */
public class ValidateDeliveryDate
{
    private static final String GET_MAX_DELIVERY_DATE = "GET MAX DELIVERY DATE";
    private static final String DATE_FORMAT = "MM/dd/yyyy"; 
    
    //private static final String GET_PRODUCT_SHIP_METHODS = "GET_PRODUCT_SHIP_METHODS";
    //private static final String GET_GOTO_FLORIST_FLAG = "GET_GOTO_FLORIST_FLAG";
    private static final String GET_COUNTRY_CLOSED_DAYS = "GET_COUNTRY_CLOSED_DAYS";    
    //private static final String GET_PRODUCT_EXCLUDED_STATES = "GET_PRODUCT_EXCLUDED_STATES";    

    private static final String COUNTRY_ID = "COUNTRY_ID";
   // private static final String PRODUCT_ID = "PRODUCT_ID";
    
    private static final String ORDER_ORIGIN_AMAZON = "ORDER_ORIGIN_AMAZON";
    private static final String ORDER_VALIDATOR = "ORDER_VALIDATOR";
   // private static final String USER_NAME = "auto";

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidateDeliveryDate");

    /**
     * Validates a delivery date using information from the order.
     * @param request Contains the Order object
     * @exception Exception if a database error occurs
     * @return A ValidationResponse containing delivery date validation errors
     *
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static ValidationResponse validate(ValidationRequest request)
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException, 
                  SQLException, ParseException, Exception
    {
        logger.info("Starting delivery date validation");

        OrderVO order = null;
        Connection conn = null;

        SimpleDateFormat sdfDate     = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmm");
        
        //the following variables are for Amazon.com/Mercent orders only
        boolean testMode = false;
        //boolean hasCancelledItems = false;
        boolean orderAlreadyBeenRemoved = false;
        boolean isAmazonOrder = false;
        boolean isMercentOrder = false;

        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
            
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }

        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        OrderValidatorDAO orderValidatorDAO = new OrderValidatorDAO(conn);
        //DataRequest dataRequest = new DataRequest();
        ValidationResponse response = new ValidationResponse();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setLenient(false);
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");
        
        if (order.getOrderOrigin().equalsIgnoreCase(configUtil.getProperty(METADATA_CONFIG_FILE_NAME, ORDER_ORIGIN_AMAZON)))
        {
          isAmazonOrder = true;        
        }
        
        if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	isMercentOrder = true;
        }
        if(isAmazonOrder || isMercentOrder)
        {
            //need to add a check that if this order has already been removed
            //do not send cancellation email again
            if(order.getStatus().equals(String.valueOf(OrderStatus.REMOVED)))
            {
            	orderAlreadyBeenRemoved = true;
            }
        }
        
        String test = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "TEST_MODE");
        if(test.equalsIgnoreCase("Y"))
          testMode = true;
        // Get the maximum delivery days out
        long maxDeliveryDate = getMaxDeliveryDate(vdau, conn);

        // Loop thorugh order items
        Collection items = order.getOrderDetail();
        Iterator it = items.iterator();
        OrderDetailsVO item = null;
        java.util.Date todayDate = new java.util.Date();
        java.util.Date deliveryDate = null;
        java.util.Date deliveryDateEnd = null;
        Calendar calendar = null;
        OEDeliveryDateParm parms = null;
        //DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL();
        String deliveryDateStr = null;
        String deliveryDateEndStr = null;
        String lineNumber = "";
        ProductVO productVO = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            productVO = orderValidatorDAO.getProductDetails(item.getProductId());
            
            boolean isFTDWOrder = false;
            if(productVO != null) {
            	isFTDWOrder = PACUtil.isFTDWOrder(productVO.getShippingSystem(), productVO.getProductType(), item.getShipMethod());
            }
            
            logger.info("isFTDWOrder --->: " + isFTDWOrder);
            
                if(isAmazonOrder || isMercentOrder)
                {
                    //need to add a check that if this item has already been removed
                    //do not send cancellation email again
                    if(item.getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
                    {
                    	orderAlreadyBeenRemoved = true;
                    }   
                }
        
                if(item.getLineNumber() != null)
                {
                    lineNumber = item.getLineNumber();
                }
            
                String productType = item.getProductType();
                if (productType == null) productType = "";
                String productSubType = "";
                if (productVO != null) {
                    productSubType = productVO.getProductSubType();
                    if (productSubType == null) productSubType = "";
                }

                if(!(productType.equals(ValidationConstants.PRODUCT_TYPE_SERVICES) && productSubType.equals(ValidationConstants.PRODUCT_SUB_TYPE_FREESHIP)))           
                {
               
                //if carrier delivered then make sure we got a ship method    
                if(item.getShipMethodCarrierFlag() != null && 
                  item.getShipMethodCarrierFlag().equalsIgnoreCase("Y") &&
                  (item.getShipMethod() == null || item.getShipMethod().length() <= 0))
                {
                    
                    response.put(ValidationConstants.RESPONSE_NO_SHIP_METHOD + lineNumber, ValidationConstants.RESPONSE_NO_SHIP_METHOD);
                }
    
                // Check to see if delivery date has passed
                try
                {
                    deliveryDateStr = item.getDeliveryDate();
                    logger.info("Item delivery date :" + deliveryDateStr);
                    deliveryDateEndStr = item.getDeliveryDateRangeEnd();
                    logger.info("Item delivery date range end :" + deliveryDateEndStr);
                    if(deliveryDateStr != null && deliveryDateStr.length() > 0)
                    {
                        // Check length first because sometimes the sdf.parse will try to
                        // make a guess at the date even if the format is wrong
                        if(deliveryDateStr.length() != 10) // MM/dd/yyyy
                        {
                            throw new ParseException("Date must be ten characters in the format MM/dd/yyyy", 0);
                        }
    
                        deliveryDate = sdf.parse(deliveryDateStr);
                        
                        if (deliveryDateEndStr != null && deliveryDateEndStr.length() > 0) {
                            try
                            {
                            	deliveryDateEnd = sdf.parse(deliveryDateEndStr);
                            }
                            catch(ParseException pe)
                            {
                                logger.error("ValidateDeliveryDate: Could not parse delivery date end...");
                            }
                        }
                        
                        long dateDiff = DeliveryDateUTIL.getDateDiff(todayDate, deliveryDate);
                        
                        // Use the end date if the order has a delivery date range
                        if (deliveryDateEndStr != null) {
                        	dateDiff = DeliveryDateUTIL.getDateDiff(todayDate, deliveryDateEnd);
                        }
                        
                        if(dateDiff < 0)
                        {
                        	logger.info("Delivery date passed");
                            if(!testMode)
                              response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_PASSED + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_PASSED);
    
                            // Fix for QA defect 105.  Only report this error alone 
                            continue;
                        }
    
                        // Check to see if the delivery date is farther out than the max delivery days
                        calendar = Calendar.getInstance();
                        calendar.setTime(todayDate);
                                            
                        dateDiff = DeliveryDateUTIL.getDateDiff(calendar.getTime(), deliveryDate);
    
                        // all dates before current date and after last allowed date out are not deliverable
                        if (dateDiff > maxDeliveryDate)
                        {
                        	logger.info("Delivery Date too far off");
                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_TOO_FAR_OFF + lineNumber, FieldUtils.replaceAll(ValidationConstants.RESPONSE_DELIVERY_DATE_TOO_FAR_OFF, "maxdays", Long.toString(maxDeliveryDate)));
    
                            //Defect 106 
                            continue;
                        }
                        
                        deliveryDateStr = item.getDeliveryDate();
                        if(deliveryDateStr == null) deliveryDateStr = "";
                        deliveryDate = null;
                        Calendar calDeliveryDate = null;
                        sdf = new SimpleDateFormat("MM/dd/yyyy");
                        try
                        {
                            deliveryDate = sdf.parse(deliveryDateStr);
                            calDeliveryDate = Calendar.getInstance();
                            calDeliveryDate.setTime(deliveryDate);
                        }
                        catch(ParseException pe)
                        {
                            logger.error("ValidateDeliveryDate: Could not parse delivery date...");
                        }
                		
                        // Check to see if this is a morning delivery order and delivery date equals Monday                     
                        boolean orderHasMorningDelivery = true;
            			if (item.getMorningDeliveryFee() == null) {
            			    orderHasMorningDelivery = false;
            			}
            			if(orderHasMorningDelivery && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY && !isFTDWOrder){
            				logger.info("No Monday for BBN");
                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_MONDAY_FOR_BBN + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_NO_MONDAY_FOR_BBN);
                            continue;
                        }
                            
                        RecipientsVO recipient = null;
                        RecipientAddressesVO recipAddress = null;
                        OEDeliveryDate curDate = null;
                        List countryHolidays = null;
                        java.util.Date nextAvailDate = null;                       
                        if(item.getRecipients() != null && item.getRecipients().size() > 0)
                        {
    
                            recipient = (RecipientsVO)item.getRecipients().get(0);
    
                            if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                            {
                                recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
    
                                // Get required parameter data
                                parms = DeliveryDateUTIL.getParameterData(recipAddress.getCountry(), 
                                                    item.getProductId(), item.getProductSubCodeId(),
                                                    recipAddress.getPostalCode(), canadaCountryCode, usCountryCode, 
                                                    item.getLineNumber(), item.getOccassionId(), conn,
                                                    recipAddress.getStateProvince());
    
                               
                                
    
                                parms.setOrder(order);
    
                                
                                // Only check if the product ID exists
                                if(item.getProductId() != null)
                                {
                                
                                    // Is this date listed as a holiday for the recip's country and the no delivery flag is yes?
                                    // Check the holiday_country table
                                    boolean foundDate = false;
                                    if(!isFTDWOrder)
	                                {
                                    	 // Get country holidays
		                                    countryHolidays = DeliveryDateUTIL.getCountryHolidays(recipAddress.getCountry(), conn);
		                                    for(int i = 0; i < countryHolidays.size(); i++)
		                                    {
		                                        curDate = (OEDeliveryDate)countryHolidays.get(i);
		    
		                                        if(curDate.getDeliveryDate().equals(item.getDeliveryDate()) && curDate.getDeliverableFlag().equals("N"))
		                                        {
		                                            foundDate = true;
		                                            break;
		                                        }
		                                    }
		    
		                                    if(foundDate)
		                                    {
		                                    	logger.info("Delivery date is a holiday");
		                                        response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_HOLIDAY + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_HOLIDAY);
		                                        // Fix for defect 120
		                                        continue;
		                                    }
                                    }
    
                                    //check if current product has exception info attached to it
                                    if(parms.getExceptionCode() != null && deliveryDate != null)
                                    {                              
                                      //java.util.Date today = new java.util.Date();   
                                      //unavailable
                                      if(parms.getExceptionCode().equals("U"))
                                      {
                                          if ( (getDateDiff(parms.getExceptionFrom(), deliveryDate) > -1)  &&
                                               (getDateDiff(parms.getExceptionTo(), deliveryDate) < 1) )
                                          {
                                              Map values = new HashMap();
                                              values.put("expstart", getDateString(parms.getExceptionFrom()));
                                              values.put("expend", getDateString(parms.getExceptionTo()));
                                              String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_DELIVERY_DATE_UNAVAILABLE_RANGE, values);
                                              response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_UNAVAILABLE_RANGE + lineNumber, replacedStr);
                                              continue;
                                          }                                    
                                      }//end unavaliable flag
                                      //avalable only
                                      if(parms.getExceptionCode().equals("A"))
                                      {
                                          // skip date if not in exception range (includes both ends of range)
                                          if ( (getDateDiff(parms.getExceptionFrom(), deliveryDate) < 0)  ||
                                               (getDateDiff(parms.getExceptionTo(), deliveryDate) > 0) )
                                          {
                                        	  
                                        	  if(deliveryDateEnd!=null)
                                        	  {	  
	                                        	  if( (getDateDiff(parms.getExceptionFrom(), deliveryDateEnd) < 0)  ||
	                                                      (getDateDiff(parms.getExceptionTo(), deliveryDateEnd) > 0))
	                                        	  {	  	
		                                              Map values = new HashMap();
		                                              values.put("expstart", getDateString(parms.getExceptionFrom()));
		                                              values.put("expend", getDateString(parms.getExceptionTo()));
		                                              String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_DELIVERY_DATE_AVAILABLE_RANGE, values);
		                                              response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_AVAILABLE_RANGE + lineNumber, replacedStr);
		                                              continue;
	                                        	  }
                                        	  }
                                        	  else
                                        	  {
                                        		  Map values = new HashMap();
	                                              values.put("expstart", getDateString(parms.getExceptionFrom()));
	                                              values.put("expend", getDateString(parms.getExceptionTo()));
	                                              String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_DELIVERY_DATE_AVAILABLE_RANGE, values);
	                                              response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_AVAILABLE_RANGE + lineNumber, replacedStr);
	                                              continue;
                                        	  }
                                          }
                                      }//end avaliable only flag
    
                                    }
    
                                    String shipMethod = item.getShipMethod();
                                    logger.info("Ship method: " + shipMethod);
                                    foundDate = false;
                                    boolean foundShipMethod = false;

                                    if (shipMethod == null) {
                                    	shipMethod = GeneralConstants.DELIVERY_FLORIST_CODE;
                                    } else if(shipMethod.equals(GeneralConstants.DELIVERY_SAME_DAY_CODE)) {
                                        shipMethod = GeneralConstants.DELIVERY_FLORIST_CODE;
                                    }
                                    /*String countryId = recipAddress.getCountry();
                                    String zipCode = recipAddress.getPostalCode();
                                    if(countryId != null && countryId.equals(canadaCountryCode) && zipCode != null && zipCode.length() > 3)
                                    {
                                        zipCode = zipCode.substring(0, 3);
                                    }
                                    else if(countryId != null && countryId.equals(usCountryCode) && zipCode != null && zipCode.length() > 5)
                                    {
                                        zipCode = zipCode.substring(0, 5);
                                    }
*/
                                    logger.info("countryId: " + recipAddress.getCountry());
                                    logger.info("zipCode: " + recipAddress.getPostalCode());
                                    logger.info("deliveryType: " + parms.getDeliveryType());
 
                                    ProductAvailVO paVO = null;
                                    if (parms.getDeliveryType() != null && parms.getDeliveryType().equalsIgnoreCase("D")) {
                                        
                                    	List addonIds = new ArrayList();
                                    	for (int i=0; i<item.getAddOns().size(); i++) {
                                    		AddOnsVO aVO = (AddOnsVO) item.getAddOns().get(i);
                                    		addonIds.add(aVO.getAddOnCode());
                                    	}
                                        // Check availability via PAS
                                        paVO = PASServiceUtil.getProductAvailability(item.getProductId(), deliveryDate, recipAddress.getPostalCode(), recipAddress.getCountry(), addonIds, order.getSourceCode(), false, true);
                                        		

                                        // If florist delivery and there's a date range and start date is today, 
                                        // then check if past today's cutoff.  If so, replace date range with single date
                                        // (using end-date as that date).
                                        // Note that PAS returns a null cutoff date if past cutoff, so we can't do actual cutoff check.
                                        // Instead we rely on the available flag returned from PAS.
                                        //
                                        if (shipMethod.equals(GeneralConstants.DELIVERY_FLORIST_CODE) && deliveryDateEndStr != null 
                                                && !deliveryDateEndStr.equals("")) 
                                        {
                                            dateDiff = DeliveryDateUTIL.getDateDiff(todayDate, deliveryDate);
                                            if (dateDiff == 0 && !paVO.isIsAvailable()) {
                                                // Start range is today, but past cutoff, so use end-date as delivery date (i.e., eliminate the date range)
                                                deliveryDate = deliveryDateEnd;
                                                deliveryDateStr = deliveryDateEndStr;
                                                deliveryDateEnd = null;
                                                deliveryDateEndStr = null;
                                                logger.info("Past today's cutoff so changing delivery date range to single date (i.e., end date): " + deliveryDateStr);                                                
                                                // Reset dates in orderDetailVO since DB record will be updated from it downstream
                                                item.setDeliveryDate(deliveryDate);
                                                item.setDeliveryDateRangeEnd("");                                                                                                
                                                // Call PAS again using the end-date
                                                paVO = PASServiceUtil.getProductAvailability(item.getProductId(), deliveryDate, recipAddress.getPostalCode(), recipAddress.getCountry(), addonIds, order.getSourceCode(), false, true);                                                
                                            }
                                        }

                                        // Normal delivery date
                                        if (deliveryDateEndStr == null || deliveryDateEndStr.equals("")) {
                                            foundDate = paVO.isIsAvailable() && paVO.isIsAddOnAvailable();
                                            logger.info("isAvailable: " + foundDate + " " +
                                            		paVO.isIsAvailable() + " " + paVO.isIsAddOnAvailable());

                                        // Delivery date range
                                        } else {
                                            try {
                                            	
                                            	Calendar startPASDate = Calendar.getInstance();
                                            	startPASDate.setTime(deliveryDate);
                                            	Calendar endPASDate = Calendar.getInstance();
                                            	endPASDate.setTime(deliveryDateEnd);

                                                // Check availability for date range via PAS
                                            	while( !startPASDate.after(endPASDate)) {
                                            		logger.info("Date range check for: " + sdf.format(startPASDate.getTime()));
                                            		paVO = PASServiceUtil.getProductAvailability(item.getProductId(), startPASDate.getTime(), recipAddress.getPostalCode(), recipAddress.getCountry(), addonIds, order.getSourceCode(), false, true);                                                    
                                                    foundDate = paVO.isIsAvailable() && paVO.isIsAddOnAvailable();
                                                    logger.info("isAvailable: " + foundDate + " " +
                                                    		paVO.isIsAvailable() + " " + paVO.isIsAddOnAvailable());
                                                    if (foundDate) break;
                                                    startPASDate.add(Calendar.DATE, 1);
                                                }
                                                
                                            } catch (Exception e) {
                                                logger.error("Date range error: " + e);
                                            }
                                        }
                                        
                                        //check if a delivery date was found
                                        if(!foundDate) {
                                        	nextAvailDate = PASServiceUtil.getNextAvailableDeliveryDate(item.getProductId(), addonIds, recipAddress.getPostalCode(), recipAddress.getCountry(), order.getSourceCode()); 
                                        	if (nextAvailDate == null) {
                                                logger.info("There are no delivery dates available for this product.");         
                                            } else {
                                                // If the delivery date is prior to the next available date for the recip's country
                                                if(deliveryDate.before(nextAvailDate) && item.getShipMethodCarrierFlag() != null && !item.getShipMethodCarrierFlag().equalsIgnoreCase("Y")) {
                                                	logger.info("Delivery date before next available delivery date");
                                                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_BEFORE_NEXT_AVAIL + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_BEFORE_NEXT_AVAIL);
                                                    continue;
                                                } else if(deliveryDate.before(nextAvailDate)) {
                                                	logger.info("Vendor delivery date before next available delivery date");
                                                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_DROP_SHIP_NEXT_AVAIL + lineNumber, FieldUtils.replaceAll(ValidationConstants.RESPONSE_DELIVERY_DATE_DROP_SHIP_NEXT_AVAIL, "date", sdf.format(nextAvailDate)));
                                                    continue;                                    
                                                }
                                            }//end else is not null
                                        } else {
                                            boolean floristDeliveryAvailable = false;
                                            boolean vendorDeliveryAvailable = false; 

                                            Date today = new Date();
                                            String ing = sdfDateTime.format(today);
                                            logger.debug("today: " + ing);
                                            
                                            boolean isExtendedFloristAvailable = false;
                                            if(paVO.getErrorMessage() != null && paVO.getErrorMessage().contains("EFA_FLORIST_AVAILABLE")) {
                                            	isExtendedFloristAvailable = true;
                                            }
                                            logger.info("isExtendedFloristAvailable: " + isExtendedFloristAvailable);
                                            if (!isExtendedFloristAvailable && paVO.getFloristCutoffDate() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getFloristCutoffDate()) + paVO.getFloristCutoffTime();
                                                logger.debug("florist cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	floristDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_FLORIST_CODE)) {
                                                      	logger.info("Has Florist delivery");
                                                       	foundShipMethod = true;
                                                    }
                                                }
                                            }
                                            
                                            if(isExtendedFloristAvailable){
                                            	foundShipMethod = true;
                                            }
                                         
                                            if (paVO.getShipDateGR() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getShipDateGR()) + paVO.getShipDateGRCutoff();
                                                logger.debug("GR cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	vendorDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_STANDARD_CODE)) {
                                                       	logger.info("Has GR delivery");
                                                      	foundShipMethod = true;
                                                    }
                                                }
                                            }
 
                                            if (paVO.getShipDate2D() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getShipDate2D()) + paVO.getShipDate2DCutoff();
                                                logger.debug("2F cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	vendorDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_TWO_DAY_CODE)) {
                                                       	logger.info("Has 2D delivery");
                                                      	foundShipMethod = true;
                                                    }
                                                }
                                            }
 
                                            if (paVO.getShipDateND() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getShipDateND()) + paVO.getShipDateNDCutoff();
                                                logger.debug("ND cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	vendorDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_NEXT_DAY_CODE)) {
                                                       	logger.info("Has ND delivery");
                                                      	foundShipMethod = true;
                                                    }
                                                }
                                            }
                                         
                                            if (paVO.getShipDateSA() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getShipDateSA()) + paVO.getShipDateSACutoff();
                                                logger.debug("SA cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	vendorDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_SATURDAY_CODE)) {
                                                       	logger.info("Has SA delivery");
                                                      	foundShipMethod = true;
                                                    }
                                                }
                                            }
                                          
                                            if (paVO.getShipDateSU() != null) { 
                                                String cutoffString = sdfDate.format(paVO.getShipDateSU()) + paVO.getShipDateSUCutoff();
                                                logger.debug("SU cutoff: " + cutoffString);
                                                if (ing.compareTo(cutoffString) <= 0) {
                                                	vendorDeliveryAvailable = true;
                                                    if (shipMethod.equals(GeneralConstants.DELIVERY_SUNDAY_CODE)) {
                                                       	logger.info("Has SU delivery");
                                                      	foundShipMethod = true;
                                                    }
                                                }
                                            }
                                        	
                                            //We need to let the user know if the other method is available on a SDFC
                                            //Checks for null have already been done.
                                            if(productType.equals("SDFC") && !foundShipMethod ) {
                                                if(shipMethod.equals(GeneralConstants.DELIVERY_FLORIST_CODE) && vendorDeliveryAvailable) {
                                                    logger.info("Florist shipping method was not available, but vendor delivery is." +
                                                        " Order detail id: " + item.getOrderDetailId());
                                                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_SHIP_METHOD_INVALID + lineNumber, 
                                                        ValidationConstants.RESPONSE_DELIVERY_DATE_FROM_FLORIST_TO_VENDOR);                                                   
                                                    foundShipMethod = true;
                                                } else if (!vendorDeliveryAvailable && floristDeliveryAvailable) {
                                                    logger.info("Vendor shipping method was not available, but florist delivery is." +
                                                        " Order detail id: " + item.getOrderDetailId());
                                                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_SHIP_METHOD_INVALID + lineNumber, 
                                                        ValidationConstants.RESPONSE_DELIVERY_DATE_FROM_VENDOR_TO_FLORIST);                                                     
                                                    foundShipMethod = true;
                                                }                                                   
                                            }

                                            if(!foundShipMethod) {
                                                logger.info("Ship method " + shipMethod + " not available for delivery date " + item.getDeliveryDate());
                                                response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_SHIP_METHOD_INVALID + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_SHIP_METHOD_INVALID);
                                            }
                                        }
                                        
                                        // Check if BBN is available for FTDW orders
                                     	if(isFTDWOrder) {
	                                        if ("Y".equals(item.getMorningDeliveryOpted()) || 
	                                        		("Y".equals(item.getOriginalOrderHasMDF()) && new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), conn))) {  
	                                        	logger.info("ValidateDeliveryDate - BBN_AVAILABILITY is set to response for FTDW - " + paVO.isIsMorningDeliveryAvailable());
		                                        if(!paVO.isIsMorningDeliveryAvailable()) {
		                        					response.put(ValidationConstants.RESPONSE_BBN_UNAVAILABLE + item.getLineNumber(), ValidationConstants.RESPONSE_BBN_UNAVAILABLE);
		                        				}
	                                        } 
                                     	}
                                    } else {
                                    	// International delivery
                                    	logger.info("Checking international availability");
                                        boolean isProductAvailable = PASServiceUtil.isInternationalProductAvailable(item.getProductId(), deliveryDate, recipAddress.getCountry());
                                        if (isProductAvailable) {
                                            foundDate = true;
                                        } else {
                                            foundDate = false;
                                        	logger.info("Checking future availability: " + maxDeliveryDate);
                                            ProductAvailVO[] futureDays = PASServiceUtil.getInternationalProductAvailableDates(item.getProductId(), recipAddress.getCountry(), (int) maxDeliveryDate);
                                            if (futureDays == null || futureDays.length <= 0) {
                                            	logger.info("No future dates");
                                            } else {
                                            	logger.info("Delivery date before next available delivery date");
                                                response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_BEFORE_NEXT_AVAIL + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_BEFORE_NEXT_AVAIL);
                                                continue;
                                            }
                                        }
                                    }
    
                                    logger.info("foundDate: " + foundDate);

                                    if(!foundDate) 
                                    {
                                    	// Check for Sunday delivery error on drop ship items
                                        shipMethod = item.getShipMethod();
                                        if(shipMethod == null) shipMethod = "";
                                        
                                        // Cancel Amazon orders with invalid
                                        // delivery values, do not send to Scrub.
                                        // But only cancel the order if this is the first time
                                        // through validation and the order has not already been
                                        // cancelled.
                                        if (!orderAlreadyBeenRemoved && (isAmazonOrder || isMercentOrder)) {
                                        if(order.getOrderOrigin().equalsIgnoreCase(configUtil.getProperty(METADATA_CONFIG_FILE_NAME,ORDER_ORIGIN_AMAZON))) {                                       				
                                        
                                        	if (configUtil.getFrpGlobalParm(ValidationConstants.AMZ_GLOBAL_CONTEXT, ValidationConstants.AMZ_AUTO_CANCEL).equalsIgnoreCase("Y")) {
                                                cancelOrder(order, item, conn);
                                                //hasCancelledItems = true;
                                                item.setAmazonStatus("CANCELLED");
                                        	}
                                            continue;
                                        } else if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                                        	String autoCancel = configUtil.getFrpGlobalParm(ValidationConstants.MERCENT_GLOBAL_CONTEXT, ValidationConstants.MERCENT_AUTO_CANCEL);
                                            logger.info("Mercent Auto Cancel Global Param :" + autoCancel);
                                        	if (autoCancel.equalsIgnoreCase("Y")) {
                                                  logger.info("Mercent Order - auto cancelling due to unavailability of delivery date");
                                                  cancelOrder(order, item, conn);
                                                  //hasCancelledItems = true;
                                                  item.setMercentStatus("CANCELLED");
                                            } else {
                                                  response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_INVALID + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_INVALID);
                                            }
                                              continue;

                                        }
                                        }//end if ()
                                        
                                        // QE3SP-10 | If FTDW order, default ship date to delivery date if an error in encountered calling PAC 
                                        else if(isFTDWOrder && paVO.getErrorMessage() != null && paVO.getErrorMessage().contains("Exception caught calling PAC")) {
                                        	logger.info("Error Message: " + paVO.getErrorMessage());
                                        	logger.info("Delivery date is not available for FTDW order");
                                        	logger.info("Defaulting ship date equal to delivery date");
                                        	item.setShipDate(item.getDeliveryDate());
                                        	// update order in scrub
                                            updateOrderScrub(order, item, conn);
                                        }
                                        
                                        //Per Change Request 2245 the PRODUCT_EXCLUDED_STATES table
                                        //should be used to determine if a product is deliverable to 
                                        //a given state.  This eliminates the no sunday & saturday freshcut delivery.
                                       else if(!isShipToStateDeliverable(parms,item.getProductId(),recipAddress.getStateProvince(),calDeliveryDate.get(Calendar.DAY_OF_WEEK)))
                                           {
                                    	        logger.info("Ship-to state not deliverable");
                                                Map values = new HashMap();
                                                values.put("state", recipAddress.getStateProvince());
                                                values.put("dayofweek", getDayOfWeek(calDeliveryDate.get(Calendar.DAY_OF_WEEK)));
                                                String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_PROD_STATE_EXCLUSION, values);                                    
                                                response.put(ValidationConstants.RESPONSE_PROD_STATE_EXCLUSION + lineNumber, replacedStr);                                      
                                           }
                                        // No Sunday delivery of specialty gift products
                                        else if(calDeliveryDate != null && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && 
                                           ((!shipMethod.equalsIgnoreCase("SD") && !shipMethod.equals("") && 
                                            (!productType.equals("FRECUT") && !productType.equals("SDFC")))
                                            || (productType.equals("SPEGFT"))))
                                        {
                                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_SUNDAY + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_NO_SUNDAY);
                                         }
                                        // No Saturday delivery of specialty gift products
                                        else if(calDeliveryDate != null && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY &&
                                                ((!shipMethod.equalsIgnoreCase("SD") && !shipMethod.equals("") && 
                                                (!productType.equals("FRECUT") && !productType.equals("SDFC")))
                                              || (productType.equals("SPEGFT"))) && !"Y".equals(item.getMorningDeliveryOpted()))
                                        {
                                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_SATURDAY + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_NO_SATURDAY);
                                        }
                                        // Do not display invalid delviery date if the day is Sunday, product is Floral and
                                        // a florist is not available
                                        else if(calDeliveryDate != null && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY &&
                                                productType.equals("FLORAL") && parms.isSundayDelivery().equals(Boolean.FALSE))
                                        {
                                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_FLORIST_SUNDAY + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_NO_FLORIST_SUNDAY);
                                        }
                                        else
                                        {         
                                        	logger.info("Delivery date is not available");
                                            response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_INVALID + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_INVALID);
                                        }
                                    }
                                }
    
                                // Check for country being closed full day on delivery day - Get closed flags   
                                if(!isFTDWOrder && calDeliveryDate != null) {
	                                Map closedFlags = getClosedFlags(recipAddress.getCountry(), vdau, conn); 
	                                String todayFlag = (String)closedFlags.get(new Integer(calDeliveryDate.get(Calendar.DAY_OF_WEEK)));
	                               
	                                if("F".equalsIgnoreCase(todayFlag)) {
	                                    logger.info("Country delivery date closed");
	                                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_CLOSED + lineNumber, 
	                                    		FieldUtils.replaceAll(ValidationConstants.RESPONSE_DELIVERY_DATE_CLOSED, "date", deliveryDateStr));	                                    
	                                }
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        // Product is restricted for delivery every day of the week to a state
                        // Cancel Amazon orders with invalid
                        // delivery values, do not send to Scrub.
                        //But only cancel the order if this is the first time
                        //through validation and the order has not already been
                        //cancelled.
                    	logger.info("Delivery date missing");
                    	response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_MISSING + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_MISSING);
                    	if (!orderAlreadyBeenRemoved) 
                        {
                        	if(order.getOrderOrigin().equalsIgnoreCase(configUtil.getProperty(METADATA_CONFIG_FILE_NAME, ORDER_ORIGIN_AMAZON))) {
	                        	if (configUtil.getFrpGlobalParm(ValidationConstants.AMZ_GLOBAL_CONTEXT, ValidationConstants.AMZ_AUTO_CANCEL).equalsIgnoreCase("Y")) {
	                                cancelOrder(order, item, conn);
	                                //hasCancelledItems = true;
	                                item.setAmazonStatus("CANCELLED");
	                        	}
	                            continue;
                        	} 
                        	else if(isMercentOrder) {
                        		if (configUtil.getFrpGlobalParm(ValidationConstants.MERCENT_GLOBAL_CONTEXT, ValidationConstants.MERCENT_AUTO_CANCEL).equalsIgnoreCase("Y")) {
                                    logger.info("Mercent Order - Auto Cancleing due to empty delivery date");
                        			cancelOrder(order, item, conn);
	                                //hasCancelledItems = true;
	                                item.setMercentStatus("CANCELLED");
	                        	}
	                            continue;
                        	}
                        }//end if ()
                        else
                        {
                          response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_MISSING + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_MISSING);
                        }
                    }
                }
                catch(java.text.ParseException pe)
                {
                    logger.info(pe.getMessage());
                    response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_FORMAT + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_FORMAT);
                }
                
                //hasCancelledItems = false;
    
            } else
            {
              try 
              {
                deliveryDateStr = item.getDeliveryDate();
                logger.info("Validating Delivery Date for Free Shipping Service: " + deliveryDateStr); 
                parseDate(deliveryDateStr, sdf);
              } catch (java.text.ParseException pe)
              {
                logger.info(pe.getMessage());
                response.put(ValidationConstants.RESPONSE_DELIVERY_DATE_FORMAT + lineNumber, ValidationConstants.RESPONSE_DELIVERY_DATE_FORMAT);                
              }
              
            }
        }                
            
        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending delivery date validation");

        return response;
    }

   /**
   * Parses the passed in delivery date with the given date format. Throws an exception on an invalid date
   * @param deliveryDateStr
   * @param sdf
   * @return
   * @throws ParseException
   */
    private static java.util.Date parseDate(String deliveryDateStr, SimpleDateFormat sdf) throws java.text.ParseException 
    {
        // Check length first because sometimes the sdf.parse will try to
        // make a guess at the date even if the format is wrong
        if(deliveryDateStr == null || deliveryDateStr.length() != 10) // MM/dd/yyyy
        {
            throw new ParseException("Date must be ten characters in the format MM/dd/yyyy", 0);
        }

        java.util.Date deliveryDate = sdf.parse(deliveryDateStr);
        return deliveryDate;
    }
    
    
    /*
     * This method is called for both Amazon and Mercent Orders.
     * This method is VERY similar to the method of the same name within
     * com.ftd.osp.ordervalidator.validationhandlers.ValidateFlorist.java.
     */
    private static void cancelOrder(OrderVO order, OrderDetailsVO item, Connection connection) throws ValidationException
    {
        
        // update order item statuses
        item.setStatus(String.valueOf(OrderStatus.REMOVED_ITEM));
        
        // if all the line items have been marked removed, then the order should
        // also be marked removed
        if (isAllLineItemsRemoved(order))
        {
            order.setStatus(String.valueOf(OrderStatus.REMOVED));
        }//end if (isAllLineItemsRemoved())
        
        // update order in scrub
        updateOrderScrub(order, item, connection);
        
        // validate prices
        try
        {
            ValidateOrderPrice.validate(order, item, connection);
        }
        catch (Exception e) 
        {
            logger.error("exception thrown during order price validation.", e);
            throw new ValidationException("Unable to validate order price.");
        }//end catch
        
        // dispatch removed order to HP
        DispatchCancelledOrder dispatcher = new DispatchCancelledOrder();
        try{
          logger.info("dispatching cancelled order number: " + item.getExternalOrderNumber());
          dispatcher.dispatchOrder(order, item, ValidationConstants.MSG_STATUS_DIPSATCHER, connection);
        }
        catch(Exception e)
        {
          logger.error("exception thrown when dispatching cancelled order. " + e.toString());
        }
        
    }//end cancelAmazonOrder
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map getClosedFlags(String countryId, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        Map closedFlags = new HashMap();
        String flag = null;
        
        // Try cache first
        CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");
        if(countryHandler != null)
        {
            CountryMasterVO countryVo = countryHandler.getCountryById(countryId); 
            if(countryVo != null)
            {
                flag = countryVo.getMondayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.MONDAY), flag);

                flag = countryVo.getTuesdayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.TUESDAY), flag);

                flag = countryVo.getWednesdayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.WEDNESDAY), flag);

                flag = countryVo.getThursdayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.THURSDAY), flag);

                flag = countryVo.getFridayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.FRIDAY), flag);

                flag = countryVo.getSaturdayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.SATURDAY), flag);

                flag = countryVo.getSundayClosed();
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.SUNDAY), flag);
            }
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.addInputParam(COUNTRY_ID, countryId);
            dataRequest.setStatementID(GET_COUNTRY_CLOSED_DAYS);

            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            
            while(rs.next())
            {
                flag = (String)rs.getObject(1);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.MONDAY), flag);

                flag = (String)rs.getObject(2);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.TUESDAY), flag);

                flag = (String)rs.getObject(3);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.WEDNESDAY), flag);

                flag = (String)rs.getObject(4);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.THURSDAY), flag);

                flag = (String)rs.getObject(5);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.FRIDAY), flag);

                flag = (String)rs.getObject(6);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.SATURDAY), flag);

                flag = (String)rs.getObject(7);
                if(flag == null) flag = "N";
                closedFlags.put(new Integer(Calendar.SUNDAY), flag);                                
            }            
        }
        return closedFlags;
    }

     /*
     * 
     */
    @SuppressWarnings("rawtypes")
	private static boolean isAllLineItemsRemoved(OrderVO order) 
                                                    throws ValidationException 
    {
        Iterator i = order.getOrderDetail().iterator();
        OrderDetailsVO line = null;
        
        while (i.hasNext()) 
        {
            line = (OrderDetailsVO) i.next();
            if (!line.getStatus().equals(
                                    String.valueOf(OrderStatus.REMOVED_ITEM)))
            {
                return false;
            }//end if
            
        }//end while
        
        return true;
        
    }//end method isAllLineItemsRemoved
    
    private static long getMaxDeliveryDate(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        String maxDays = null;
        long maxDeliveryDate = 0;


        // Try cache first
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {
            FTDAppsGlobalParmsVO ftdAppsParms = parmHandler.getFTDAppsGlobalParms();
            maxDeliveryDate = ftdAppsParms.getDeliveryDaysOut();
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_MAX_DELIVERY_DATE);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
        
            while(rs.next())
            {
                maxDays = rs.getObject(15).toString();
            }

            if(maxDays != null)
            {
                maxDeliveryDate = Integer.parseInt(maxDays);
            }            
        }
        
        return maxDeliveryDate;
    }

    /*
     * This method determines if the product cannot be delivered to the specified state for the passed in
     * day of the week. (PRODUCT_EXCLUDED_STATES table in FTD_APPS)
     * 
     * @param Connection database connection
     * @param ValiationDataAccessUtil
     * @param String product id
     * @param String Recipients state
     * @param int day of the week
     * @returns boolean TRUE=Deliverable
     */
    private static boolean isShipToStateDeliverable(OEDeliveryDateParm parms, String productId, String state, int dayOfWeek)
      throws Exception
    {

     //determine should be used from resultset
     boolean exclusionFlag = false;
     
      switch(dayOfWeek){
        case(Calendar.SUNDAY): 
            exclusionFlag = parms.isStateSunExclusion();
            break;
        case(Calendar.MONDAY): 
            exclusionFlag = parms.isStateMonExclusion();
            break;        
        case(Calendar.TUESDAY): 
            exclusionFlag = parms.isStateTueExclusion();
            break;
        case(Calendar.WEDNESDAY):
            exclusionFlag = parms.isStateWedExclusion();
            break;        
        case(Calendar.THURSDAY):
            exclusionFlag = parms.isStateThrExclusion();
            break;        
        case(Calendar.FRIDAY):
            exclusionFlag = parms.isStateFriExclusion();
            break;        
        case(Calendar.SATURDAY):
            exclusionFlag = parms.isStateSatExclusion();
            break;        
      }

      //exclusionFlag or false means the product is deliverable
       return !exclusionFlag;
        
    }

    /* 
     * Returns the day of the week in string format.
     * @param int day of week as int
     * @returns String day of week as string.
     */
    private static String getDayOfWeek(int dayOfWeekInt)
    {
      String dayOfWeekStr = "";

      switch(dayOfWeekInt){
        case(Calendar.MONDAY): dayOfWeekStr = "Monday"; break;
        case(Calendar.TUESDAY): dayOfWeekStr = "Tuesday"; break;
        case(Calendar.WEDNESDAY): dayOfWeekStr = "Wednesday"; break;
        case(Calendar.THURSDAY): dayOfWeekStr = "Thursday"; break;
        case(Calendar.FRIDAY): dayOfWeekStr = "Friday"; break;
        case(Calendar.SATURDAY): dayOfWeekStr = "Saturday"; break;
        case(Calendar.SUNDAY): dayOfWeekStr = "Sunday"; break;     
      }

      return dayOfWeekStr;
    }

  /* 
   * Returns the earliest date in the list.
   * @param List of dates
   * @returns OEDeliveryDate earliest deliverable date (null if no deliverable dates)
   */
//   private static OEDeliveryDate getFirstDeliveryDate(List deliveryDates) throws Exception
//   {
//      OEDeliveryDate earliestDateObj = null;
//      java.util.Date earliestDate = null;
//
//      //loop through the days
//      for(int i = 0; i < deliveryDates.size(); i++)
//      {          
//          OEDeliveryDate curDateObj = (OEDeliveryDate)deliveryDates.get(i);    
//          //if date is deliverable
//          if(curDateObj.getDeliverableFlag().equals("Y"))
//          {
//            //check if this date is the earliest
//            java.util.Date curDate = getDate(curDateObj.getDeliveryDate());
//            if(earliestDate == null || curDate.before(earliestDate))
//            {
//              earliestDate = curDate;
//              earliestDateObj = curDateObj;
//            }//end, found an earlier date            
//          }//end if deliverable
//      }//end for each date
//
//      //return earliest date 
//      return earliestDateObj;
//   }

/*
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy and converts it
 * 				to a java.util.date.
 *
 * @param String date in string format
 * @return java.util.Date
 * 
 */
//private static java.util.Date getDate(String strDate) throws Exception{
//
//        java.util.Date parsedDate = null;
//        String inDateFormat = "";
//        int dateLength = 0;
//        int firstSep = 0;
//        int lastSep = 0;
//
//    try{
//
//
//
//
//        if ((strDate != null) && (!strDate.trim().equals(""))) {
//
//          //if lenght of 5 then it is the format of MMDD..reformat it
//          if(strDate.length() == 5)
//          {
//            String month = strDate.substring(0,2);
//            String year = strDate.substring(3,5);
//            int yearNum =   (new Integer(year)).intValue() + 2000;
//            strDate = month + "/31/" + yearNum;
//          }
//
//
//		    // set input date format
//		    dateLength = strDate.length();
//		    if ( dateLength > 10) {
//			    firstSep = strDate.indexOf("/");
//          if(firstSep == 1)
//          {
//            inDateFormat = "M/dd/yyyy hh:mm:ss";            
//          }
//          else if (firstSep == 2)
//          {
//            inDateFormat = "MM/dd/yyyy hh:mm:ss";
//          }
//          else
//          {
//  			    firstSep = strDate.indexOf("/");          
//            if(firstSep > 0){
//              inDateFormat = "yyyy-MM-dd hh:mm:ss"; 
//            }
//              //check for spaces
//              firstSep = strDate.indexOf(" ");          
//              if(firstSep <= 0){
//                inDateFormat = "yyyyMMddHHmmsszzz";
//              }
//              else
//              {
//                  if(dateLength > 20){
//                      inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";               
//                  }
//                  else{
//                      inDateFormat = "MMM dd yyyy HH:mmaa";               
//                  }
//              }
//          }
//			    
//		    } else {
//			    firstSep = strDate.indexOf("/");
//			    lastSep = strDate.lastIndexOf("/");
//
//			    switch ( dateLength ) {
//				    case 10:
//			    		inDateFormat = "MM/dd/yyyy";
//				    	break;
//				    case 9:
//				    	if ( firstSep == 1 ) {
//					    	inDateFormat = "M/dd/yyyy";
//				    	} else {
//					    	inDateFormat = "MM/d/yyyy";
//			    		}
//				    	break;
//				    case 8:
//				    	if ( firstSep == 1 ) {
//					    	inDateFormat = "M/d/yyyy";
//				    	} else {
//					    	inDateFormat = "MM/dd/yy";
//			    		}
//				    	break;
//				    case 7:
//				    	if ( firstSep == 1 ) {
//					    	inDateFormat = "M/dd/yy";
//				    	} else {
//					    	inDateFormat = "MM/d/yy";
//			    		}
//				    	break;
//				    case 6:
//				    	inDateFormat = "M/d/yy";
//				    	break;
//				    default:
//				    	break;
//			    }
//		    }
//            //SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
//            SimpleDateFormat sdfInput = new SimpleDateFormat(inDateFormat);
//
//            //must be in this format to convert to date
//            String outDateFormat = "yyyy-MM-dd";             
//
//
//            SimpleDateFormat sdfOutput = new SimpleDateFormat ( outDateFormat );
//
//            parsedDate = sdfInput.parse( strDate );
//
//
//
//        }
//        }
//      catch(Exception e)
//      {
//        throw e;
//      }		  
//
//	return parsedDate;
//}

     /**
     *
     * @param Date first date to compare
     & @param Date second date to compare
     * @return Returns a long value showing the number of days between the 2 dates
     */
     public static long getDateDiff (java.util.Date inFromDate, java.util.Date inToDate)
     {
        long diff = 0;

        // convert dates to same time of day (00:00:00) for better comparison as
        // number of milliseconds between dates can be less than 24 hours
        // which causes function to return 0 as difference
        // ex. 11/06/2002 23:59 & 11/07/2002 23:58 returns 0 as less than 24
        // hours as elapsed
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            String fromDate = sdf.format(inFromDate);
            String toDate = sdf.format(inToDate);

            java.util.Date date1 = sdf.parse(fromDate);
            java.util.Date date2 = sdf.parse(toDate);

            diff = date2.getTime() - date1.getTime();
        }
        catch (Exception e)
        {
            diff = inToDate.getTime() - inFromDate.getTime();
        }
        float tmpFlt = ((float)diff / (float)(1000 * 60 * 60 * 24));
        String tmpStr = String.valueOf(tmpFlt);
        BigDecimal bd = new BigDecimal(tmpStr);
        int ret = bd.setScale(0, BigDecimal.ROUND_HALF_DOWN).intValue();
        return ret;
     }   

     private static String getDateString(java.util.Date utilDate)
     {
        Calendar cal = Calendar.getInstance();
        cal.setTime(utilDate);
        String dateString = zeroPad(cal.get(Calendar.MONTH) + 1) + "/" + zeroPad(cal.get(Calendar.DAY_OF_MONTH)) + "/" + cal.get(Calendar.YEAR);
        return dateString;
     }


    /*
     * 
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private static void updateOrderScrub(OrderVO order, 
                                         OrderDetailsVO line, 
                                         Connection connection) 
                                                    throws ValidationException
    {
        OrderVO clone = null;
        
        // Update order with system name
        order.setCsrId(ORDER_VALIDATOR);
    
        // Create a working copy of the order.  This copy is necessary so that
        // in the subsequent code, a single item can be set on the order.  This
        // will prevent the ScrubMapperDAO.updateOrder(order) from resaving the
        // entire order.  Instead it will only update the header and the single
        // line item in question.
        try
        {
            clone = (OrderVO) new ObjectCopyUtil().deepCopy(order);
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to update order in scrub, due to exception "
                         + "thrown during deepCopy of order.", e);
            throw new ValidationException("Unable to update order.");
        }//end catch

        // Update order with individual item
        if(line != null)
        {
            List singleItemList = new ArrayList();
            try
            {
                singleItemList.add(
                        (OrderDetailsVO) new ObjectCopyUtil().deepCopy(line));
            }//end try
            catch (Exception e) 
            {
                logger.error("Unable to update order in scrub, due to exception"
                             + " thrown during deepCopy of line item.", e);
                throw new ValidationException("Unable to update order.");
            }//end catch
        
            clone.setOrderDetail(singleItemList);
            
        }//end if (line != null)

        // Update scrub database
        try 
        {
            new ScrubMapperDAO(connection).updateOrder(clone);
        }//end try
        catch (Exception e) 
        {
            logger.error("Unable to update order in scrub, due to exception "
                         + "thrown during DAO.updateOrder().", e);
            throw new ValidationException("Unable to update order.");
        }//end catch
        
        
    }//end method updateOrderScrub
    
    
    /*
     *
     * This function left pads values that are less than 10 with zeros
     * @param int integer value that is to be left padded with 0's
      * @return string result of the padded value
     */
    private static String zeroPad(int i)
    {
        String val = Integer.toString(i);
        return i < 10 ? "0" + val : val;
    }     
}
