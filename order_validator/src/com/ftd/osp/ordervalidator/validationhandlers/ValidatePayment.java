package com.ftd.osp.ordervalidator.validationhandlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.oro.text.perl.Perl5Util;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.GiftCodeUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentBinMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryVoComparator;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.ApValidationVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.RedemptionDetails;
import com.ftd.osp.utilities.vo.RetrieveGiftCodeResponse;



/**
 * This class validates payment info for an order. The following credit card 
 * fields are required on an order: credit card type, credit card number and credit
 * card expiration date.  The credit card expiration date must be in the following 
 * format: "/\\d\\d.\\d\\d/".  The credit card type must exist in the PAYMENT_METHODS
 * table of the FTD_APPS schema.  If the order contains gift certificates and the
 * order total is greater than the total of all gift certificates, then the order 
 * must contain the minimum credit card fields. JC Penney credit cards do not have
 * to contain expiration dates.  The check digit functionality was taken from the 
 * ValidateSVC class in WebOE.
 * 
 * 02/12/07: changed class name from ValidateCreditCard to ValidatePayment
 * Made changes to incorporate alternate payment types such as Pay Pal (PP).
 * 
 * @author Jeff Penney
 *
 */
public class ValidatePayment 
{

    private static final String GET_COUNTRY_LIST = "GET_COUNTRY_LIST";
    
    private static final String GET_CC_TYPES = "GET_OV_PAYMENT_METHODS_EX_CB";
    private static final String CC_DATE_FORMAT = "/\\d\\d/\\d\\d/";
    
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.validationhandlers.ValidatePayment");
    

    
    /**
     * Validates a credit card number, type and expiration date.  Also makes sure
     * that credit card info exists for orders with gift certificates that do not
     * cover the total price.
     * @param request Contains an Order object
     * @exception Exception if a database exception occurs
     * @return Returns any validation errors.
     *
     */
    public static ValidationResponse validate(ValidationRequest request) 
                  throws ValidationException, IOException, SAXException,
                  ParserConfigurationException, TransformerException,
                  SQLException, Exception
    {    
        logger.info("Starting payment validation");
        
        OrderVO order = null;
        Connection conn = null;
        boolean isGC_GD_Only = true;
        Iterator it = null;
        // Catch objects that are not of type Order
        try
        {
            order = (OrderVO)request.get(ValidationConstants.ORDER);
            if(order == null)
            {
                throw new ValidationException(ValidationConstants.ORDER_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.ORDER_ERROR);
        }

        // Get the database connection
        try
        {
            conn = (Connection)request.get(ValidationConstants.CONNECTION);
            if(conn == null)
            {
                throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException(ValidationConstants.CONNECTION_ERROR);
        }      
        
        // Get the ValidationDataAccessUtil from the request
        ValidationDataAccessUtil vdau = null;
        try
        {
            vdau = (ValidationDataAccessUtil)request.get(ValidationConstants.VALIDATION_DAU);
            if(vdau == null)
            {
                throw new ValidationException("ValidationDataAccessUtil cannot be null");
            }
        }
        catch(ClassCastException cce)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }        

        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String paymentTypeCC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_CREDIT_CARD");
        String paymentTypeGC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_GIFT_CERT");
        String paymentTypeGD = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_GIFT_CARD");
        String paymentTypeAP = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_ALT_PAY");
        String paymentMethodBML = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_METHOD_BML");
        String ccTypeCodeMS = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "CREDIT_CARD_TYPE_MS");
        String ccTypeCodeJP = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "CREDIT_CARD_TYPE_JP");
        String ccTypeCodeNC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "CREDIT_CARD_TYPE_NC");
        
        DataRequest dataRequest = null;
        ValidationResponse response = new ValidationResponse();

        // Check credit card information
        // If the order is paid with a gift certificate and the gift cert is
        // less than the total amount then credit card information must be 
        // present
        Collection payments = order.getPayments();
        String orderTotalStr = order.getOrderTotal();
        if(orderTotalStr == null) orderTotalStr = "0";
        BigDecimal orderTotal = new BigDecimal(orderTotalStr);
        BigDecimal giftCertTotal = new BigDecimal(0);
        boolean detokenized = true;
        
        if(payments != null && payments.size() > 0)
        {
            // Get the total of all gift certificates
            it = payments.iterator();
            
            PaymentsVO gc = null;
            String paymentMethodAll = "";
            boolean giftFlag = true;
            while(it.hasNext())
            {
                gc = (PaymentsVO)it.next();
                logger.info("Got a payment: " + gc.getPaymentId());
               
                if (payments.size() == 1 && gc.getPaymentType() == null && gc.getTokenId() != null) {
                    logger.info("Payment has not been de-tokenized");
                    detokenized = false;
                }
                
                if(gc.getPaymentMethodType() != null )
                {
                	paymentMethodAll = paymentMethodAll + gc.getPaymentMethodType();
                }
                if(gc.getPaymentMethodType() != null && gc.getPaymentMethodType().equals(paymentTypeGC)  && orderTotal.compareTo(new BigDecimal("0"))==1 && isGCValid(gc))
                {
                	logger.warn("gc.getGcCouponIssueAmount() = " + gc.getGcCouponIssueAmount() );
                	
                	if(gc.getGcCouponIssueAmount() != null)
                	{	
                		giftCertTotal = giftCertTotal.add(gc.getGcCouponIssueAmount());
                		gc.setAmount(gc.getGcCouponIssueAmount().toString());
                	}
                	else
                	{
                		giftCertTotal = giftCertTotal.add(new BigDecimal(gc.getAmount()));
                	}
                    
                }
                if(gc.getPaymentMethodType() != null && gc.getPaymentMethodType().equals(paymentTypeGD)  && orderTotal.compareTo(new BigDecimal("0"))==1 && isGDValid(gc))
                {
                	giftCertTotal = giftCertTotal.add(new BigDecimal(gc.getOrigAuthAmount()));
                	gc.setAmount(gc.getOrigAuthAmount());
                	
                }
            }

            
            
            /*
             * Check if paymentMethodAll has any character other than G and R. 
             * if yes at isGC_GD_Only to false. 
             */
            for(int i = 0; i < paymentMethodAll.length(); i++){
            	if(!paymentTypeGD.equalsIgnoreCase(String.valueOf(paymentMethodAll.charAt(i))) && !paymentTypeGC.equalsIgnoreCase(String.valueOf(paymentMethodAll.charAt(i)))){
            		isGC_GD_Only = false;
            	}
            }
            
            if (!detokenized) {
                isGC_GD_Only = false;
            }
           
            if(orderTotal.compareTo(giftCertTotal) == 1)
            {
                boolean ccPresent = false;
                // Loop through payments to check for credit card completeness
                it = payments.iterator();
                PaymentsVO payment = null;
                CreditCardsVO creditCard = null;
                String expDateStr = null;
                String typeStr = null;
                String numberStr = null;
                /*validate if other payment types (other than gift card/certificate) exists, otherwise
                 * add an error message 
                */
                
                if(isGC_GD_Only){
                	//If any item has been sent to clean, do not allow payment information to be updated.
                	Collection items = order.getOrderDetail();
                    Iterator rit = items.iterator();
                    OrderDetailsVO item = null;
                    boolean hasProcessedItem = false;
                    while(rit.hasNext())
                    {
                    	item = (OrderDetailsVO)rit.next();
                    	String itemStatus = item.getStatus();
                    	if(ValidationConstants.ORDER_DETAIL_STATUS_VALID.equals(itemStatus)
                    	   || ValidationConstants.ORDER_DETAIL_STATUS_PROCESSED.equals(itemStatus))
                    	{
                    		hasProcessedItem = true;
                    		break;
                    	}
                    }
                    if(hasProcessedItem) {
                    	response.put(ValidationConstants.RESPONSE_GC_OVER_ALLOWED_TOTAL, ValidationConstants.RESPONSE_GC_OVER_ALLOWED_TOTAL);
                    } else {
                    	response.put(ValidationConstants.RESPONSE_GC_PAYMENT_AMT_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_AMT_INVALID);
                    }
                }
                while(it.hasNext())
                {
                    payment = (PaymentsVO)it.next();
                    // defect 3695 - do not allow null payment type
                    if(payment.getPaymentType() == null || payment.getPaymentType().equals("")) {
                        response.put(ValidationConstants.RESPONSE_CC_INFO_MISSING, ValidationConstants.RESPONSE_CC_INFO_MISSING);
                    }
                    else if(payment.getPaymentMethodType() == null || payment.getPaymentMethodType().equals(paymentTypeCC))
                    {                        
                        if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                        {
                            ccPresent = true;
                            creditCard = (CreditCardsVO)payment.getCreditCards().get(0);

                        
                            expDateStr = creditCard.getCCExpiration();
                            if(expDateStr == null) expDateStr = "";
                            typeStr = creditCard.getCCType();
                            if(typeStr == null) typeStr = "";
                            numberStr = creditCard.getCCNumber();
                            if(numberStr == null) numberStr = "";

                            //do not allow JCP Penney cards for intl orders
                            if(typeStr.equals(ccTypeCodeJP))
                            {
                                // Get the country list
                                List countryList = getCountryList(vdau, conn);        
                            
                                //check all the recipients
                                List detailList = order.getOrderDetail();
                                List recipList = null;
                                OrderDetailsVO detailVO = null;
                                if(detailList != null)
                                {
                                  int detailCounter = 0;
                                  boolean jcpCardErrorFound = false; 
                                  //loop through items
                                  while(!jcpCardErrorFound && (detailCounter < detailList.size()))
                                  {
                                    detailVO = (OrderDetailsVO)detailList.get(detailCounter);
                                    recipList = detailVO.getRecipients();
                                    if(recipList != null)
                                    {
                                      //check recipient
                                       RecipientsVO recip = (RecipientsVO)recipList.get(0);
                                       List recipAddrList = recip.getRecipientAddresses();
                                       if(recipAddrList != null){
                                           RecipientAddressesVO recipAddr = (RecipientAddressesVO)recipAddrList.get(0);
                                           if(recipAddr != null){
                                                 String country = recipAddr.getCountry();
                                                 if(country != null)
                                                 {
                                                    CountryMasterVO buyerCountryVo = findCountry(country, countryList);                                             
                                                    if(buyerCountryVo != null && buyerCountryVo.getCountryType() != null && buyerCountryVo.getCountryType().equalsIgnoreCase("I"))
                                                    {
                                                        jcpCardErrorFound=true;
                                                        response.put(ValidationConstants.RESPONSE_CC_JCP_CARD_INTL, ValidationConstants.RESPONSE_CC_JCP_CARD_INTL);                                                                                
                                                    }//end, if error found
                                                 }//end if country not null
                                           }//end not null
                                       }//end if recip address not null
                                    }//end recipient list is null
                                    detailCounter++;
                                  }//end detail loop
                                }//end detail list null
                            }//end if JCPenney


                            // only check exp date for non JP and MS cards
                            // For NC fake a date
                            if(typeStr.equals(ccTypeCodeJP) || typeStr.equals(ccTypeCodeMS) || typeStr.equals(ccTypeCodeNC))
                            {
                                expDateStr = " ";
                            }

                            // For NC fake a number
                            if(typeStr.equals(ccTypeCodeNC))
                            {
                                numberStr = " ";
                            }
                        
                            if(typeStr.length() < 1)
                            {
                                response.put(ValidationConstants.RESPONSE_CC_TYPE_MISSING, ValidationConstants.RESPONSE_CC_TYPE_MISSING);
                            }

                            if(numberStr.length() < 1)
                            {
                                response.put(ValidationConstants.RESPONSE_CC_NUMBER_MISSING, ValidationConstants.RESPONSE_CC_NUMBER_MISSING);
                            }

                            if(expDateStr.length() < 1)
                            {
                                response.put(ValidationConstants.RESPONSE_CC_DATE_MISSING, ValidationConstants.RESPONSE_CC_DATE_MISSING);
                            }
                        }                        
                    }
                    else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals(paymentTypeAP))
                    {
                     
                        ApValidationVO av = FTDCommonUtils.getApValidation(payment.getPaymentType(),conn);
                        Map vp = av.getValidationProperties();
                        Set s = vp.keySet();
                        Iterator its = s.iterator();
                        String apAuthTxt = payment.getApAuth();
                        String apAccountTxt = payment.getApAccount();
                        String paymentTypeDesc = payment.getPaymentTypeDesc();
                        String propName;
                        String propValue;

                        while(its.hasNext()) {
                            propName = (String)its.next();
                            propValue = (String)vp.get(propName);
                            
                            if(ValidationConstants.AP_INVALID_AUTH_TXT.equals(propName)) {
                                if(apAuthTxt == null || apAuthTxt.equals("") || propValue.equals(apAuthTxt)) {
                                    response.put(ValidationConstants.RESPONSE_AP_AUTH_INVALID, paymentTypeDesc + ValidationConstants.RESPONSE_AP_AUTH_INVALID);
                                } 
                            } 
                            else if (ValidationConstants.AP_INVALID_ACCOUNT_TXT.equals(propName)) {
                                if(apAccountTxt == null || apAccountTxt.equals("") || propValue.equals(apAccountTxt)) {
                                    response.put(ValidationConstants.RESPONSE_AP_ACCOUNT_INVALID, paymentTypeDesc + ValidationConstants.RESPONSE_AP_ACCOUNT_INVALID);
                                }
                            }
                        }
                        
                        // BML validation
                        if(payment.getPaymentType() != null && paymentMethodBML.equals(payment.getPaymentType())) {
                            // Validate buyer country for BML. Buyer country must be US, PR, or VI
                             String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");
                             String prStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_PUERTO_RICO");
                             String viStateCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "STATE_CODE_VIRGIN_ISLANDS");
                             
                             BuyerVO buyer = null;
                             BuyerAddressesVO buyerAddress = null;

                             if(order.getBuyer() != null && order.getBuyer().size() > 0)
                             {
                                 buyer = (BuyerVO)order.getBuyer().get(0);

                                 if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                                 {
                                     buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                                     String country = (buyerAddress.getCountry() == null)?"":buyerAddress.getCountry();
                                     
                                     if(!usCountryCode.equals(country) && !prStateCode.equals(country) && !viStateCode.equals(country)) {
                                        response.put(ValidationConstants.RESPONSE_BM_CUST_INTERNATIONAL_ADDRESS, 
                                                     ValidationConstants.RESPONSE_BM_CUST_INTERNATIONAL_ADDRESS);
                                     }
                                 }
                             }
                             
                            // Validate recipient country for BML. Recipient country must be US, PR, or VI
                            Collection items = order.getOrderDetail();
                            Iterator rit = items.iterator();
                            OrderDetailsVO item = null;
                            String recipCountry = null;
                            while(rit.hasNext())
                            {
                                item = (OrderDetailsVO)rit.next();
                                RecipientsVO recipient = null;
                                RecipientAddressesVO recipAddress = null;
                                if(item.getRecipients() != null && item.getRecipients().size() > 0)
                                {
                                    recipient = (RecipientsVO)item.getRecipients().get(0);
        
                                    if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                                    {
                                        recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                                        recipCountry = recipAddress.getCountry();
                                        if(!usCountryCode.equals(recipCountry) && !prStateCode.equals(recipCountry) && !viStateCode.equals(recipCountry)) {
                                            response.put(ValidationConstants.RESPONSE_BM_RECIP_INTERNATIONAL_ADDRESS, 
                                                        ValidationConstants.RESPONSE_BM_RECIP_INTERNATIONAL_ADDRESS);
                                            break;
                                        }
                                    }
                                }
                            }
                                        
                    } // end BML validation
                } // end AP
                }
            }
            
        }
        else
        {
            response.put(ValidationConstants.RESPONSE_CC_INFO_MISSING, ValidationConstants.RESPONSE_CC_INFO_MISSING);
        }
        
        // Check for a valid credit card type
        // Get valid cc types
        List ccTypes = new ArrayList();
        
        dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID(GET_CC_TYPES);
        dataRequest.addInputParam("payment_type_id", "C");
        CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
        rs.reset();
        while(rs.next())
        {
            ccTypes.add(rs.getObject(1));
        }

        List newPayments = new ArrayList();
        if(payments != null && payments.size() > 0)
        {
            // Get order credit cards
            it = payments.iterator();
            PaymentsVO payment = null;
            CreditCardsVO creditCard = null;
            String ccType = null;
            String ccNumber = null;
            String ccExpiration = null;
            
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                if(payment.getPaymentMethodType() == null || payment.getPaymentMethodType().equals(paymentTypeCC))
                {	
                	if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);                          

                        ccType = creditCard.getCCType();
                        if(ccType == null) ccType = "";
                        ccNumber = creditCard.getCCNumber();
                        if(ccNumber == null) ccNumber = "";
                        ccExpiration = creditCard.getCCExpiration();
                        if(ccExpiration == null) ccExpiration = "";

                        // All credit cards with the number 4111111111111111
                        // should go to scrub
                        if(ccNumber.equals("4111111111111111"))
                        {
                            response.put(ValidationConstants.RESPONSE_CC_FOUND_MAGIC_NUMBER, ValidationConstants.RESPONSE_CC_FOUND_MAGIC_NUMBER);
                            newPayments.add(payment);
                            continue;
                        }
                        
                        if(!ccTypes.contains(ccType))
                        {
                            response.put(ValidationConstants.RESPONSE_CC_INVALID_TYPE, FieldUtils.replaceAll(ValidationConstants.RESPONSE_CC_INVALID_TYPE, "ccType", ccType));
                            newPayments.add(payment);
                            //Only show this error
                            continue;
                        }
        
                        // Check for a valid credit card number
                        if(!new ValidateMembership().checkCreditCard(ccType, ccNumber) && !ccType.equals(ccTypeCodeNC))
                        {
                            response.put(ValidationConstants.RESPONSE_CC_INVALID_NUMBER, ValidationConstants.RESPONSE_CC_INVALID_NUMBER);
                        }

                        // Check credit card expiration date
                        Perl5Util regExp = new Perl5Util();
                        String month = null;
                        String year = null;
                        // Do not check date on JC Penney and Militart Star cards because they do not have an exp date.
                        if(!ccType.equals(ccTypeCodeJP) && !ccType.equals(ccTypeCodeMS) && !ccType.equals(ccTypeCodeNC))
                        {
                            if(regExp.match(CC_DATE_FORMAT, ccExpiration) && ccExpiration.length() == 5)
                            {
                                // if cc expiration date does not match pattern 2N.2N or the month
                                // is less than 1 or greater than 12            
                                month = ccExpiration.substring(0, 2);
                                year = ccExpiration.substring(3, 5);
                                year  = "20" + year;
                                if(Integer.valueOf(month).intValue() < 1 || Integer.valueOf(month).intValue() > 12)
                                {
                                    response.put(ValidationConstants.RESPONSE_CC_DATE_INVALID, ValidationConstants.RESPONSE_CC_DATE_INVALID);
                                }

                                // if cc expiration date is prior today
                                Calendar ccDate = Calendar.getInstance();
                                int intMonth = Integer.parseInt(ccExpiration.substring(0, 2));
                                int intYear = Integer.parseInt(ccExpiration.substring(3, 5));
                                ccDate.set(Calendar.MONTH, intMonth - 1);
                                ccDate.set(Calendar.YEAR, intYear + 2000);
                                ccDate.set(Calendar.DATE, 31);
                                if(new java.util.Date().after(ccDate.getTime()))
                                {
                                    //Map values = new HashMap();
                                    //values.put("date", ccExpiration);
                                    //String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_CC_DATE_PRIOR_TO_TODAY, values);
                                    response.put(ValidationConstants.RESPONSE_CC_DATE_PRIOR_TO_TODAY, FieldUtils.replaceAll(ValidationConstants.RESPONSE_CC_DATE_PRIOR_TO_TODAY, "date", ccExpiration));
                                }                
                            }
                            else
                            {
                                // Not the correct format
                                //Map values = new HashMap();
                                //values.put("date", ccExpiration);
                                //String replacedStr = StringReplacer.replace(ValidationConstants.RESPONSE_CC_DATE_INVALID, values);                            
                                response.put(ValidationConstants.RESPONSE_CC_DATE_INVALID, FieldUtils.replaceAll(ValidationConstants.RESPONSE_CC_DATE_INVALID, "date", ccExpiration));        
                            }
                        }
                    }
                }
                //validate if gift certificate if the amount is not zero
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals(paymentTypeGC) && 
                		payment.getAmount() != null)
                {
                	if(payment.getGcCouponStatus() == null || !payment.getGcRedeemableStatus()) 
                	{
                		logger.debug("Putting error message for Gift Certificate");
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                	    payment.setAmount("0");                		
                	}
                	else if(payment.getGcCouponExpirationDate() != null){
                		if(!validateExpirationDate(payment.getGcCouponExpirationDate())){
                			logger.debug("Putting error message for Gift Certificate (expired)");
                			response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                			giftCertTotal = new BigDecimal("0");
                			payment.setAmount("0");
                		}
                	}
                }
              //validate if gift card if the amount is not zero
                else if(payment.getPaymentMethodType() != null && payment.getPaymentMethodType().equals(paymentTypeGD) && 
                		payment.getAmount() != null && Double.parseDouble(payment.getAmount()) > 0)
                {
                	creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                	if(creditCard.getPin() == null)
                	{
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                		payment.setAmount("0");
                	}
                	else if(payment.getAuthNumber() == null)
                	{
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                		payment.setAmount("0");
                	}
                	else if(creditCard.getCCNumber() == null)
                	{
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                		payment.setAmount("0");
                	}
                	else if(creditCard.getCCNumber() != null && creditCard.getCCNumber().length() != 19)
                	{
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                		payment.setAmount("0");
                	}
                	PaymentBinMasterHandler paymentBinHdlr = (PaymentBinMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_GET_PAYMENT_BIN_MASTER);
                	String payMethodType = paymentBinHdlr.getPaymentMethodIdByNumber(creditCard.getCCNumber());
                	if(!"GD".equalsIgnoreCase(payMethodType)){
                		response.put(ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID, ValidationConstants.RESPONSE_GC_PAYMENT_INFO_INVALID);
                		giftCertTotal = new BigDecimal("0");
                		payment.setAmount("0");
                	}
                }
                //Logic for order total less than or equal to issue ammount.
                if(isGC_GD_Only)
                {
                	if(orderTotal != null && orderTotal.compareTo(new BigDecimal("0"))==1 && (orderTotal.compareTo(giftCertTotal) == -1 || orderTotal.compareTo(giftCertTotal) == 0))
                	{
                		payment.setAmount(orderTotal.toString());
                	}
                }
                //Logic to set other payment amount to zero if GC/GD alone is able to cover the order total and order total is not zero
                if(orderTotal != null && orderTotal.compareTo(new BigDecimal("0"))==1 && (orderTotal.compareTo(giftCertTotal) == -1 || orderTotal.compareTo(giftCertTotal) == 0))
                {
                	if(payment.getPaymentMethodType() != null && !payment.getPaymentMethodType().equals(paymentTypeGD) && !payment.getPaymentMethodType().equals(paymentTypeGC))
                	{
                		payment.setAmount("0");
                	}else{
                		payment.setAmount(orderTotal.toString());
                	}
                }
                //if order total is grater than gift cert/card amount then apply balance to other payment.
                if(orderTotal != null && orderTotal.compareTo(new BigDecimal("0"))==1 && orderTotal .compareTo(giftCertTotal) == 1)
                {
                	if(payment.getPaymentMethodType() != null && !payment.getPaymentMethodType().equals(paymentTypeGD) && !payment.getPaymentMethodType().equals(paymentTypeGC))
                	{
                		payment.setAmount(orderTotal.subtract(giftCertTotal).toString());
                	}
                }
                newPayments.add(payment);
                
            }
        }
        order.setPayments(newPayments);
        response.put(ValidationConstants.ORDER, order);

        logger.info("Ending payment validation");
        
        return response;      
    }

	private static void populateGiftCertData(PaymentsVO payment) throws Exception {
		RetrieveGiftCodeResponse certResponse = null; 
		BigDecimal issueAmount = new BigDecimal(0);
		BigDecimal remainingAmount = new BigDecimal(0);
		
		try {
			certResponse = GiftCodeUtil.getGiftCodeById(payment.getGiftCertificateId());
			
			issueAmount = certResponse.getIssueAmount();
	       	logger.info("issue amount: " + issueAmount);
	       	
	       	remainingAmount = certResponse.getRemainingAmount();
	       	logger.info("remaining Amount: " + remainingAmount);
	    
	       	payment.setGcCouponStatus(certResponse.getStatus());
	       	
	       	payment.setGcHasRedemptionDetails(!certResponse.getRedemptionDetails().isEmpty());
	       	logger.info(payment.getGcHasRedemptionDetails().toString());
	
			String expiryDateString = certResponse.getExpirationDate();
			if (expiryDateString != null) {
				SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy");
				try {
					payment.setGcCouponExpirationDate(sf
							.parse(expiryDateString));
				} catch (ParseException e) {
					logger.warn("Could not  parse %s" + expiryDateString);
				}
			} 
		}catch (Exception ex) {
			
		}				
		payment.setGcCouponRemainingAmount(remainingAmount);		
		payment.setGcCouponIssueAmount(issueAmount);
	}
	
	private static CountryMasterVO findCountry(String id, List countries)
    {
        CountryMasterVO countryVo = null;
        CountryMasterVO countryVoTmp = null;
        
        Iterator it = countries.iterator();
        while(it.hasNext())
        {
            countryVoTmp = (CountryMasterVO)it.next();

            if(countryVoTmp.getCountryId().equalsIgnoreCase(id))
            {
                countryVo = countryVoTmp;
                break;
            }
        }

        return countryVo;
    }

    private static List getCountryList(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");

        List countryList = new ArrayList();
        if(countryHandler != null)
        {
            countryList = countryHandler.getCountryList();
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COUNTRY_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
        
            String countryId = null;
            String countryName = null;
            Object displayOrder = null;
            CountryMasterVO countryVo = null;
            String countryType = null;
            while(rs.next())
            {
                countryId = (String)rs.getObject(1);
                countryName = (String)rs.getObject(2);
                countryType = (String)rs.getObject(3);
                displayOrder = rs.getObject(4);
                if(displayOrder == null) displayOrder = new BigDecimal("999");
            
                if(countryId != null)
                {
                    countryId = countryId.toUpperCase();
                    countryName = countryName.toUpperCase();

                    countryVo = new CountryMasterVO();
                    countryVo.setCountryId(countryId);
                    countryVo.setCountryType(countryType);
                    countryVo.setDisplayOrder((BigDecimal)displayOrder);
                    countryVo.setCountryName(countryName);
                    countryList.add(countryVo);
                }
            }

            Collections.sort(countryList, new CountryVoComparator());
        }      

        return countryList;
    }
    
    
    private static boolean validateExpirationDate(Date expDate) throws Exception
    {
    	boolean flag = true;
    	Calendar cToday = Calendar.getInstance();
    	cToday.set(Calendar.HOUR, 0);
    	cToday.set(Calendar.MINUTE, 0);
    	cToday.set(Calendar.SECOND, 0);
    	cToday.set(Calendar.MILLISECOND, 0);
    	cToday.set( Calendar.AM_PM, Calendar.AM );
    	// Check credit card expiration date
    	Date today = cToday.getTime();
    	try{
    		if(expDate.before(today)){
    			return false;
    		}
    	}catch (Exception e) {
			throw e;
		}
    	logger.info(String.format("Checking expiry (%s) = %s", expDate, flag)); 
    	return flag;
    }
     
    /*
     * Checks if a GC is valid.
     * returns true if :
     * cert status is not null, not empty and either of reinstate, active clear or issued active. 
     * and it's expiry date gets Validated to be in future.
     */
    private static boolean isGCValid(PaymentsVO payment) throws Exception
    {
    	boolean flag = true;
    	if (payment != null) {
    		try {
    			populateGiftCertData(payment);
    			String[] acceptedStatuses = new String[] { 
        				ValidationConstants.GC_COUPON_REINSTATE.toLowerCase(),
        				ValidationConstants.GC_COUPON_ISSUED_ACTIVE.toLowerCase(),
        				ValidationConstants.GC_COUPON_REDEEMED.toLowerCase()
        		};
    			
        		String certStatus = payment.getGcCouponStatus();
        		logger.info("certStatus: " + certStatus);
        		logger.info("payment.getGcCouponRemainingAmount() " + payment.getGcCouponRemainingAmount() );
        		logger.info("payment.getGcHasRedemptionDetails() " + payment.getGcHasRedemptionDetails() );
        		logger.info("payment.getGcCouponExpirationDate() " + payment.getGcCouponExpirationDate());
        		
        		if (certStatus != null) {
        			logger.info("Arrays.asList(acceptedStatuses).contains(certStatus.toLowerCase()): " + Arrays.asList(acceptedStatuses).contains(certStatus.toLowerCase()));    
        		}
        		flag = certStatus != null &&
        				!certStatus.isEmpty() &&
        				Arrays.asList(acceptedStatuses).contains(certStatus.toLowerCase()) &&
        				!payment.getGcHasRedemptionDetails() &&
        				(payment.getGcCouponExpirationDate() == null ||
        				validateExpirationDate( payment.getGcCouponExpirationDate()) );
    		} catch (IllegalStateException ex) {
    			
    		}
    	}
    	
    	payment.setGcRedeemableStatus(flag);
    	logger.info(String.format("payment.getGcRedeemableStatus() : %s", payment.getGcRedeemableStatus()));
    	logger.info(String.format("Validation (%s) = %s", payment.getGiftCertificateId(), flag));
    	return flag;
    }
//    	boolean flag = true;
//    	if(payment != null)
//    	{
//    		
//        	if (certStatus == null || certStatus.isEmpty() || !Arrays.asList(acceptedStatuses).contains(certStatus) )
//    		 
////	    	if(payment.getGcCouponStatus() == null || payment.getGcCouponStatus().equals("")|| (payment.getGcCouponStatus() != null && !payment.getGcCouponStatus().equalsIgnoreCase(ValidationConstants.GC_COUPON_ISSUED_ACTIVE)
////	    			&& !payment.getGcCouponStatus().equalsIgnoreCase(ValidationConstants.GC_COUPON_REINSTATE)
////	    			&& !payment.getGcCouponStatus().equalsIgnoreCase(ValidationConstants.GC_COUPON_ACTIVE_CLEAR)))
//	    	{
//	    		flag = false;    	       		
//	    	}
////	    	else if(payment.getGcCouponExpirationDate() != null){
////	    		if(!validateExpirationDate(payment.getGcCouponExpirationDate())){
//        	else if(certResponse.getExpirationDt() != null && validateExpirationDate(certResponse.getExpirationDt()))
//	    			flag = false;
//	    		}
//	    	}
//    	}else
//    	{
//    		flag = false;
//    	}
//    	return flag;
    	
    
    
    private static boolean isGDValid(PaymentsVO payment) throws Exception
    {
    	boolean flag = true;
    	if(payment != null)
    	{
	    	CreditCardsVO creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
	    	if(creditCard.getPin() == null)
	    	{
	    		flag = false;
	    	}
	    	else if(payment.getAuthNumber() == null)
	    	{
	    		flag = false;
	    	}
	    	else if(creditCard.getCCNumber() == null)
	    	{
	    		flag = false;
	    	}
	    	else if(creditCard.getCCNumber() != null && creditCard.getCCNumber().length() != 19)
	    	{
	    		flag = false;
	    	}
	    	PaymentBinMasterHandler paymentBinHdlr = (PaymentBinMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_GET_PAYMENT_BIN_MASTER);
	    	String payMethodType = paymentBinHdlr.getPaymentMethodIdByNumber(creditCard.getCCNumber());
	    	if(!"GD".equalsIgnoreCase(payMethodType)){
	    		flag = false;
	    	}
    	}
    	else
    	{
    		flag = false;
    	}
    	
    	return flag;
    }
    
 
    
    
}