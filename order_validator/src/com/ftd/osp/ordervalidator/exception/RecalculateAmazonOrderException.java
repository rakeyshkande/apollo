package com.ftd.osp.ordervalidator.exception;

/** This class defines an exception that occured
 * while attempting to recalculate the amounts
 * on the order VO for Amazon.com orders only.
 * 
 *  @author Rose Lazuk */
public class RecalculateAmazonOrderException extends Exception 
{

  public RecalculateAmazonOrderException()
  {
    super();
  }
  
  public RecalculateAmazonOrderException(String e)
  {
    super(e);
  }
}