package com.ftd.osp.ordervalidator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.ordervalidator.util.ValidationListComparator;
import com.ftd.osp.ordervalidator.vo.ItemValidationNode;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.ordervalidator.vo.ValidationActionVO;
import com.ftd.osp.ordervalidator.vo.ValidationFieldVO;
import com.ftd.osp.ordervalidator.vo.ValidationNode;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.ordervalidator.vo.ValidationVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
import com.ftd.osp.utilities.xml.DOMUtil;


/**
 * This class should be used to validate an entire order object.
 *
 * @author Jeff Penney
 *
 */
public class OrderValidator 
{
    private static final String MSG_TYPE_EXCEPTION = "EXCEPTION";
    private static final String MSG_SOURCE = "ORDER VALIDATOR";

    private static final String PHONE_TYPE_HOME = "Home";

    private static final String VALIDATION_CONFIG_FILE_NAME = "validation_setup.xml";
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";

    private static Logger logger = new Logger("com.ftd.osp.ordervalidator.OrderValidator");
    private static Map validationConfigMap = null;
    private static Map validationErrorTypes = null;

    public static final int VALIDATION_SCRUB = 1;
    public static final int VALIDATION_AUTO = 2;
    public static final int VALIDATION_AMAZON = 3;
    public static final int VALIDATION_OE = 7;
    public static final int VALIDATION_MERCENT = 4;
    public static final int VALIDATION_PARTNER = 5;
    public static final int VALIDATION_ROSES = 6;
    
    
    public OrderValidator() throws SAXException, IOException,
           ParserConfigurationException, TransformerException, Exception
    {    
        // Only load the config files once
        if(validationConfigMap == null)
        {
            // Load config file
            loadValidationConfigs();
        }
    }

    public OrderValidationNode validateOrder(OrderVO order, int validationType, Connection conn)
                               throws ClassNotFoundException, NoSuchMethodException,
                               IllegalAccessException, InvocationTargetException,
                               NoSuchFieldException, IOException, 
                               ValidationException, SAXException, ParserConfigurationException,
                               SQLException, SystemMessengerException, Exception
    {
        ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
        return validateOrder(order, validationType, conn, vdau);
    }    

    /**
     * A method that loads the validation_config.xml file and performs the required validation
     * on an Order object
     * @param order An Order object containing the data do be validated
     * @return An Object containing original order data and ant validation errors
     *
     */
    public OrderValidationNode validateOrder(OrderVO order, int validationType, Connection conn, ValidationDataAccessUtil vdau)
                               throws ClassNotFoundException, NoSuchMethodException,
                               IllegalAccessException, InvocationTargetException,
                               NoSuchFieldException, IOException, 
                               ValidationException, SAXException, ParserConfigurationException,
                               SQLException, SystemMessengerException, Exception
    {
        if(logger.isDebugEnabled())
            logger.debug("inside OrderValidator validate Order method");

        // ValidationDataAccessUtil cannot be null
        if(vdau == null)
        {
            throw new ValidationException("ValidationDataAccessUtil cannot be null");
        }
        
        logger.info("Validating master order number: "+order.getMasterOrderNumber());
    
        ValidationNode vNode = null;
        ValidationRequest request = new ValidationRequest();
        ValidationResponse response = null;      
        ValidationVO validationVO = null;
        Class validationClass = null;
        Class validationConstants = null;
        Method validateMethod = null;
        Map validationResponseMap = null;            

        validationConstants = Class.forName("com.ftd.osp.ordervalidator.util.ValidationConstants");
        Field constantField = null;

        // validate method parameter type
        Class[] parameterTypes = new Class[1];
        parameterTypes[0] = Class.forName("com.ftd.osp.ordervalidator.vo.ValidationRequest");

        // validate method parameters
        request.put(ValidationConstants.ORDER, order);
        request.put(ValidationConstants.CONNECTION, conn);
        request.put(ValidationConstants.VALIDATION_DAU, vdau);
        Object[] parameters = new Object[1];
        parameters[0] = request;

        Map validationActions = null;
        ValidationActionVO actionVo = null;
        Iterator it = null;
        Collection values = null;
        String fieldName = null;
        ValidationFieldVO field = null;
        ValidationNode errorNode = null;
        ValidationNode lineItemNode = null;
        OrderDetailsVO item = null;
        String lineItemNumber = null;
        String codeName = null;
        String nameMatch = null;
        List nameMatchList = null;
        Iterator nameMatchIterator = null;
        String status = null;
        String orderFieldValue = null;

        // Build the order and item validation VOs so we can insert
        // status and messages later after validation is performed
        OrderValidationNode vOrder = new OrderValidationNode();
        vOrder.setAttribute(ValidationNode.NAME, "order");

        //default clean falg
        vOrder.setSendToClean(true);

        // Order header
        ValidationNode vHeader = new ValidationNode();
        vHeader.setAttribute(ValidationNode.NAME, "header");
        
        // Co brands
        ValidationNode vCoBrands = new ValidationNode();
        vCoBrands.setAttribute(ValidationNode.NAME, "cobrands");
        
        // Order Items
        ValidationNode vItems = new ValidationNode();
        vItems.setAttribute(ValidationNode.NAME, "items");
        ItemValidationNode vItem = null;
        ValidationNode vLineItemNumber = null;
        int size = 0;
        if(order.getOrderDetail() != null) size = order.getOrderDetail().size();
        
        for(int i = 0; i < size; i++)
        {
            item = (OrderDetailsVO)order.getOrderDetail().get(i);
            vItem = new ItemValidationNode();
            vItem.setAttribute(ValidationNode.NAME, "item");
            // Set line item number
            vLineItemNumber = new ValidationNode();
            vLineItemNumber.setAttribute(ValidationNode.NAME, ItemValidationNode.LINE_ITEM_NUMBER);
            vLineItemNumber.setText(item.getLineNumber());
            vItem.addChildNode(vLineItemNumber);
            
            vItems.addChildNode(vItem);
        }

        // Add children to vOrder
        vOrder.addChildNode(vHeader);
        vOrder.addChildNode(vItems);
        vOrder.addChildNode(vCoBrands);

        List validationList = null;
        // Use the validationType to determine validation
        // rules
        if(validationType == this.VALIDATION_SCRUB)
        {
            logger.info("Validation used = Scrub");
            validationList = (List)validationConfigMap.get("scrub");
        }
        // If VALIDATION_AUTO is selected then validate based on origin
        else if(validationType == this.VALIDATION_AUTO)
        {
            String origin = order.getOrderOrigin();
            if(origin == null || origin.length() < 1)
            {
                origin = "default";   
            }
            logger.info("Validation used by origin = " + origin);
            validationList = (List)validationConfigMap.get(origin.toLowerCase());

            // if validation list is null or empty then try the default list
            if(validationList == null || validationList.size() == 0)
            {
                logger.info("Validation used = default");
                validationList = (List)validationConfigMap.get("default".toLowerCase());
            }
        }
        // Both Amazon and Mercent order have similar validation rules. 
        // So amazon_validation_config.xml file is used for both Amazon and Mercent.
        else if ( validationType == this.VALIDATION_AMAZON)
        {
            logger.info("Validation used = amazon");
            validationList = (List)validationConfigMap.get("amazon");
      
        }
        else if (validationType == this.VALIDATION_MERCENT) {
        	logger.info("Validation used = mercent");
            validationList = (List)validationConfigMap.get("mercent");
        }
        else if (validationType == this.VALIDATION_PARTNER) {
        	logger.info("Validation used = Partner: " + order.getPartnerName());
            validationList = (List)validationConfigMap.get("partner");
        }
        else if (validationType == this.VALIDATION_ROSES) {
            logger.info("Validation used = roses");
            validationList = (List)validationConfigMap.get("roses");
        }
        else if ( validationType == this.VALIDATION_OE )
        {
            logger.info("Validation used = oe");
            validationList = (List)validationConfigMap.get("oe");
        }

        
        // Loop through validation
        for(int i = 0; i < validationList.size(); i++)
        {
            validationVO = (ValidationVO)validationList.get(i);
            validationClass = Class.forName(validationVO.getStep());
            validateMethod = validationClass.getMethod("validate", parameterTypes);
            // Add auto fix parameter                
            request.put(ValidationConstants.AUTO_FIX, new Boolean(validationVO.isAutoFix()));

            try
            {
                response = (ValidationResponse)validateMethod.invoke(null, parameters);
            }
            catch(InvocationTargetException ite)
            {
            	logger.error(ite.getCause().getMessage());
            	logger.error(ite.getCause());
                SystemMessengerVO smVO = new SystemMessengerVO();
                smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                smVO.setType(MSG_TYPE_EXCEPTION);
                smVO.setSource(MSG_SOURCE);
                StringWriter stringWriter = new StringWriter();
                ite.getTargetException().printStackTrace(new PrintWriter(stringWriter));
                smVO.setMessage(stringWriter.toString());
                SystemMessenger.getInstance().send(smVO, conn);
                throw ite;
            }

            // If any validation fails then decided if the order fails
            // or if any item fails validation
            validationActions = validationVO.getResultActions();
            values = validationActions.values();
            it = values.iterator();            
            while(it.hasNext())
            {
                // Iterate through the validation actions from the validation 
                // config file
                actionVo = (ValidationActionVO)it.next();
                
                status = actionVo.getStatus();
                if(status == null || status.length() < 1)
                {
                    status = ValidationNode.ERROR;
                }
                else
                {
                    status = status.toLowerCase();
                }
                constantField = validationConstants.getField(actionVo.getCode());
                fieldName = (String)constantField.get(null);

                // if the ValidationResponse contains an error
                // then set the validation node
                if(response.containsKey(fieldName) && actionVo.isSendToScrub())
                {
                
                    if(!actionVo.isSendToClean()){
                        vOrder.setSendToClean(false);
                    }
                
                    vOrder.setAttribute(ValidationNode.STATUS, ValidationNode.ERROR);
                    for(int j = 0; j < actionVo.getFields().size(); j++)
                    {
                        field = (ValidationFieldVO)actionVo.getFields().get(j);
                        if(field.getType().equals("order header"))
                        {
                            // Set header status
                            if(vHeader.getAttribute(ValidationNode.STATUS) == null || !vHeader.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                            {
                                vHeader.setAttribute(ValidationNode.STATUS, status);            
                            }

                            // Get the order header field value
                            orderFieldValue = getOrderHeaderFieldValue(order, field.getName());
                            
                            vHeader = createErrorMessageNode(vHeader, field.getName(), (String)response.get(fieldName), "", actionVo.getType(), status, orderFieldValue);                            

                        }
                        else if(field.getType().equals("item"))
                        {
                            // Set items status
                            if(vItems.getAttribute(ValidationNode.STATUS) == null || !vItems.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                            {
                                vItems.setAttribute(ValidationNode.STATUS, status);            
                            }
                        
                            nameMatchList = (List)response.getPartialMatch(fieldName);                    
                            // This error could occur on multiple items so we need to loop through
                            // all errors of this field name and check the line
                            // items
                            nameMatchIterator = nameMatchList.iterator();
                            while(nameMatchIterator.hasNext())
                            {                            
                                nameMatch = (String)nameMatchIterator.next();
                                // Get the line number from the end of the string
                                StringBuffer sb = new StringBuffer();
                                for(int cCount = nameMatch.length() - 1; cCount >= 0; cCount--)
                                {
                                    if(Character.isDigit(nameMatch.charAt(cCount)))
                                    {
                                        sb.append(nameMatch.charAt(cCount));
                                    }
                                }
                                sb = sb.reverse();
                                lineItemNumber = sb.toString();
                                codeName = (String)response.get(nameMatch);
                                // loop through items to get the correct one
                                for(int h = 0; h < vItems.getChildNodes().size(); h++)
                                {
                                    vItem = (ItemValidationNode)vItems.getChildNodes().get(h);
                                    lineItemNode = (ValidationNode)vItem.getChildNodes(ItemValidationNode.LINE_ITEM_NUMBER).get(0);
                                    if(lineItemNode.getText() != null && lineItemNode.getText().equals(lineItemNumber))
                                    {
                                        // Set item status
                                        if(vItem.getAttribute(ValidationNode.STATUS) == null || !vItem.getAttribute(ValidationNode.STATUS).equals(ValidationNode.ERROR))
                                        {
                                            vItem.setAttribute(ValidationNode.STATUS, status);            
                                        }

                                        // Get the order header field value
                                        orderFieldValue = getOrderItemFieldValue(order, lineItemNumber, field.getName());
                                    
                                        vItem = (ItemValidationNode)createErrorMessageNode(vItem, field.getName(), codeName, "", actionVo.getType(), status, orderFieldValue);
                                    }
                                    else if(lineItemNode.getText() == null)
                                    {
                                        throw new ValidationException("Line item number cannot be null");
                                    }
                                }
                            }
                        }
                    }                    
                }
                else
                {
                    //vOrder.setAttribute(ValidationNode.STATUS, ValidationNode.VALID);
                    /*
                    for(int j = 0; j < actionVo.getFields().size(); j++)
                    {
                        System.out.println(actionVo.getFields().get(j) + " is valid");    
                    } 
                    */
                }
            }
             
        }
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
        if ( order.getOrderOrigin() != null && order.getOrderOrigin().equalsIgnoreCase("AMZNI") ) {
            size = 0;
            if(order.getOrderDetail() != null) size = order.getOrderDetail().size();
            for(int i = 0; i < size; i++) {
                item = (OrderDetailsVO)order.getOrderDetail().get(i);
            	logger.info("Amazon status: " + item.getAmazonStatus());
            	boolean success = true;
            	String cancelReason = null;
            	if (item.getAmazonStatus() != null && item.getAmazonStatus().equals("CANCELLED")) {
            		success = false;
            		cancelReason = "Z1";
            	}
            	if (size > 1 && !success) {
            	    logger.info("Sending fake Amazon Acknowledgement");
            	    this.insertAmazonAcknowledgement(conn, item.getExternalOrderNumber(), true, null);
            	    logger.info("Sending Amazon Adjustment");
            	    this.insertAmazonAdjustment(conn, item.getExternalOrderNumber(), cancelReason);
            	} else {
            	    logger.info("Sending Amazon Acknowledgement");
            	    this.insertAmazonAcknowledgement(conn, item.getExternalOrderNumber(), success, cancelReason);
            	}
            }

        } else if ( order.getOrderOrigin() != null && mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	//Perform Mercent related adjustment/acknowledgement feed
        	size = 0;
            if(order.getOrderDetail() != null) size = order.getOrderDetail().size();
            for(int i = 0; i < size; i++) {
                item = (OrderDetailsVO)order.getOrderDetail().get(i);
            	logger.info("Mercent status: " + item.getMercentStatus()); 
            	boolean success = true;
            	String cancelReason = null;
            	if (item.getMercentStatus() != null && item.getMercentStatus().equals("CANCELLED")) {
            		success = false;
            		cancelReason = "Z1";
            	}            	
            	if (!success) {            	                	               	   
            	    this.insertMercentAdjustment(conn, item.getExternalOrderNumber(), cancelReason);
            	} 
            }

        } 

        return vOrder;
    }

    private String getOrderHeaderFieldValue(OrderVO order, String fieldName) throws Exception
    {
        String retStr = "";
        List buyerAddresses = null;
        Collection payments = null;
        Iterator it = null;
        BuyerVO buyer = null;
        BuyerAddressesVO buyerAddress = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        BuyerPhonesVO buyerHomePhone = null;
        BuyerPhonesVO buyerWorkPhone = null;
        BuyerPhonesVO buyerPhone = null;
        MembershipsVO membership = null;
        String ccNumber = null;
        String ccExpDate = null;
        String ccType = null;
        String paymentType = null;
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String paymentTypeCC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_CREDIT_CARD");        

        if(fieldName.equalsIgnoreCase("source_code"))
        {
            retStr = order.getSourceCode();
        }
        else if(fieldName.equalsIgnoreCase("cc_type"))
        {
            payments = order.getPayments();
            if(payments != null && payments.size() > 0)
            {
                // Get order credit cards
                it = payments.iterator();                
                while(it.hasNext())
                {
                    payment = (PaymentsVO)it.next();
                    paymentType = payment.getPaymentMethodType();
                    if(paymentType == null) paymentType = "";
                    if(paymentType.equals(paymentTypeCC) && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                        retStr = creditCard.getCCType();
                    }
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("cc_number"))
        {
            payments = order.getPayments();
            if(payments != null && payments.size() > 0)
            {
                // Get order credit cards
                it = payments.iterator();                
                while(it.hasNext())
                {
                    payment = (PaymentsVO)it.next();
                    paymentType = payment.getPaymentMethodType();
                    if(paymentType == null) paymentType = "";
                    if(paymentType.equals(paymentTypeCC) && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                        retStr = creditCard.getCCNumber();
                    }
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("cc_exp_date"))
        {
            payments = order.getPayments();
            if(payments != null && payments.size() > 0)
            {
                // Get order credit cards
                it = payments.iterator();                
                while(it.hasNext())
                {
                    payment = (PaymentsVO)it.next();
                    paymentType = payment.getPaymentMethodType();
                    if(paymentType == null) paymentType = "";
                    if(paymentType.equals(paymentTypeCC) && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                        retStr = creditCard.getCCExpiration();
                    }
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("buyer_first_name"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {   
                buyer = (BuyerVO)order.getBuyer().get(0);
                retStr = buyer.getFirstName();
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_last_name"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {   
                buyer = (BuyerVO)order.getBuyer().get(0);
                retStr = buyer.getLastName();
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_address1"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                {
                    buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                    retStr = buyerAddress.getAddressLine1();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_city"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                {
                    buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                    retStr = buyerAddress.getCity();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_state"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                {
                    buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                    retStr = buyerAddress.getStateProv();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_postal_code"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                {
                    buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                    retStr = buyerAddress.getPostalCode();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_address2"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
                {
                    buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                    retStr = buyerAddress.getAddressLine2();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("buyer_evening_phone"))
        {
            if(order.getBuyer() != null && order.getBuyer().size() > 0)
            {
                buyer = (BuyerVO)order.getBuyer().get(0);

                if(buyer.getBuyerPhones() != null)
                {                
                    for(int i = 0; i < buyer.getBuyerPhones().size(); i++)
                    {
                        buyerPhone = (BuyerPhonesVO)buyer.getBuyerPhones().get(i);

                        if(buyerPhone.getPhoneType().equalsIgnoreCase(PHONE_TYPE_HOME))
                        {
                            buyerHomePhone = buyerPhone;
                            retStr = buyerHomePhone.getPhoneNumber();
                        }
                    }
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("membership_id"))
        {
            if(order.getMemberships() != null && order.getMemberships().size() > 0)
            {
                membership = (MembershipsVO)order.getMemberships().get(0);

                retStr = membership.getMembershipIdNumber();
            }
        }
        else if(fieldName.equalsIgnoreCase("order_amount"))
        {
            retStr = order.getOrderTotal();
        }

        return retStr;
    }

    private String getOrderItemFieldValue(OrderVO order, String lineNumber, String fieldName) throws Exception
    {
        String retStr = "";
        OrderDetailsVO item = null;
        Iterator it = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        RecipientPhonesVO recipHomePhone = null;
        RecipientPhonesVO recipWorkPhone = null;
        RecipientPhonesVO recipPhone = null;

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        //String phoneNumberTypeHome = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PHONE_NUMBER_TYPE_HOME");

        it = order.getOrderDetail().iterator();

        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if(item.getLineNumber().equals(lineNumber))
            {
                break;
            }
        }

        if(item == null)
        {
            return null;
        }

        if(fieldName.equalsIgnoreCase("special_instructions"))
        {
            retStr = item.getSpecialInstructions();
        }
        else if(fieldName.equalsIgnoreCase("delivery_date"))
        {
            retStr = item.getDeliveryDate();
        }
        else if(fieldName.equalsIgnoreCase("recip_first_name"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);
                retStr = recipient.getFirstName();
            }
        }
        else if(fieldName.equalsIgnoreCase("recip_last_name"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);
                retStr = recipient.getLastName();
            }            
        }
        else if(fieldName.equalsIgnoreCase("recip_address1"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getAddressLine1();
                }
            }
        }
        else if(fieldName.equalsIgnoreCase("recip_city"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getCity();
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("recip_postal_code"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getPostalCode();
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("recip_state"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getStateProvince();
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("recip_address2"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getAddressLine2();
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("recip_phone"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientPhones() != null)
                {
                    for(int i = 0; i < recipient.getRecipientPhones().size(); i++) {
                    	 recipPhone = (RecipientPhonesVO)recipient.getRecipientPhones().get(i);
                    	if (recipPhone != null && recipPhone.getPhoneNumber()!= null && recipPhone.getPhoneType().equals("WORK")) {							
							break; //Day phone							
						}
                    }

                    if(recipPhone != null && recipPhone.getPhoneNumber() != null && recipPhone.getPhoneNumber().length() > 0) {
                        retStr = recipPhone.getPhoneNumber();
                    }
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("productid"))
        {
            retStr = item.getProductId();
        }
        else if(fieldName.equalsIgnoreCase("recip_country"))
        {
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    retStr = recipAddress.getCountry();
                }
            }            
        }
        else if(fieldName.equalsIgnoreCase("order_total"))
        {
            retStr = item.getProductsAmount();
        }
        else if(fieldName.equalsIgnoreCase("first_color_choice"))
        {
            retStr = item.getColorFirstChoice();
        }
        else if(fieldName.equalsIgnoreCase("second_color_choice"))
        {
            retStr = item.getColorSecondChoice();
        }
        else if(fieldName.equalsIgnoreCase("product_price"))
        {
            retStr = item.getProductsAmount();
        }
        else if(fieldName.equalsIgnoreCase("service_fee"))
        {
            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);  
            BigDecimal serviceCharge = new BigDecimal(item.getServiceFeeAmount());
            BigDecimal shippingCharge = new BigDecimal(item.getShippingFeeAmount());
            BigDecimal TotalCharge = shippingCharge.add(serviceCharge);
            retStr = currencyFormat.format(TotalCharge.doubleValue());
        }

        return retStr;
    }
    

    private ValidationNode createErrorMessageNode(ValidationNode node, String name, String description, String sv, String errorType, String status, String orderFieldValue)
    {
        // check to see if this node already has messages
        List childNodes = node.getChildNodes(name);

        ValidationNode childNode = null;
        //Check for existance of the child node
        if(childNodes.size() < 1)
        {
            // create the child node
            childNode = new ValidationNode();
            childNode.setAttribute(ValidationNode.NAME, name);
            childNode.setAttribute(ValidationNode.VALUE, orderFieldValue);
            node.addChildNode(childNode);
        }
        else
        {
            // Get the first node;
            childNode = (ValidationNode)childNodes.get(0);
        }

        // set the status
        childNode.setAttribute(ValidationNode.STATUS, status);

        // check for the messages node
        List messagesList = childNode.getChildNodes("messages");

         ValidationNode vMessages = null;
        if(messagesList.size() > 0)
        {
           vMessages = (ValidationNode)messagesList.get(0);    
        }
        

        if(vMessages == null)
        {
            // create a new messages node
            vMessages = new ValidationNode();
            vMessages.setAttribute(ValidationNode.NAME, "messages");
            childNode.addChildNode(vMessages);
        }

        // Create the new message node
        ValidationNode vMessage = new ValidationNode();
        vMessage.setAttribute(ValidationNode.NAME, "message");
        // Give the message node a description and suggested value
        ValidationNode vDescription = new ValidationNode();
        vDescription.setAttribute(ValidationNode.NAME, "description");
        vDescription.setText(description);
        //ValidationNode vSuggestedValue = new ValidationNode();
        //vSuggestedValue.setAttribute(ValidationNode.NAME, "suggestedValue");
        //vSuggestedValue.setText(sv);

        ValidationNode vErrorType = new ValidationNode();
        vErrorType.setAttribute(ValidationNode.NAME, ValidationNode.ERROR_TYPE);
        vErrorType.setText(errorType);


        vMessage.addChildNode(vDescription);
        //vMessage.addChildNode(vSuggestedValue);
        vMessage.addChildNode(vErrorType);

        // Add new message to messages for this node        
        vMessages.addChildNode(vMessage);

        return node;
    }

    /**
     * Loads the validation config file
     *
     */
    private synchronized void loadValidationConfigs() throws IOException,  
                 SAXException, ParserConfigurationException, TransformerException, Exception
    {
        if(validationConfigMap == null)
        {
            if(logger.isDebugEnabled())
                logger.debug("Begin loading configuration files for Order Validator");
            
            ValidationVO validationVO = null;
            HashMap validationConfigTempMap = new HashMap();
    
            // Load the setup files
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            Map props = configUtil.getProperties(VALIDATION_CONFIG_FILE_NAME);
              
            Iterator it = props.keySet().iterator();
            URL url = null;
            File f = null;
            Document config = null;
            NodeList validationNL =  null;
            NodeList actionNL = null;
            Node validationNode = null;
            Element actionNode = null;
            Node validationAction = null;
            Map resultActions = null;
            ValidationActionVO validationActionVO = null;
            NodeList fieldNL = null;
            Element fieldNode = null;
            List fields = null;
            ValidationFieldVO vField = null;
            List validationList = null;
    
            String confileFileName = null;
            String origin = null;
            
            while(it.hasNext())
            {
                origin = (String)it.next();
                confileFileName = (String)props.get(origin);                
                config = DOMUtil.getDocument(ResourceUtil.getInstance().getResourceFileAsStream(confileFileName));   
                // extract the validation tags
                validationNL =  DOMUtil.selectNodes(config, "//validation");
                validationList = new ArrayList();
                
                for (int i = 0; i < validationNL.getLength(); i++) 
                {
                    validationNode = (Element)validationNL.item(i);
                
                    validationVO = new ValidationVO();
                    resultActions = new HashMap();
    
                    //validationNode.
                    validationVO.setStep(((Element)validationNode).getAttribute("step"));
                    validationVO.setExecutionOrder(((Element)validationNode).getAttribute("order"));
                    validationVO.setAutoFix(Boolean.valueOf(((Element)validationNode).getAttribute("autoFix")).booleanValue());
    
                    String xpath = "//validationConfig/validation[@order = '" + validationVO.getExecutionOrder() + "']/onValidationResponse";
                    String errorType = null;
                    actionNL = DOMUtil.selectNodes(config, xpath);
    
                    // Get the actions
                    for (int j = 0; j < actionNL.getLength(); j++) 
                    {
                        actionNode = (Element)actionNL.item(j);
                        validationActionVO = new ValidationActionVO();
                        validationActionVO.setCode(actionNode.getAttribute("code"));       
                        validationActionVO.setStatus(actionNode.getAttribute("status"));
                        validationActionVO.setSendToScrub(Boolean.valueOf(actionNode.getAttribute("sendToScrub")).booleanValue());
                        validationActionVO.setSendToClean(Boolean.valueOf(actionNode.getAttribute("sendToClean")).booleanValue());
                        
                        // Get the error type
                        errorType = actionNode.getAttribute("type");
                        // If the type is null then default it to ""
                        if(errorType == null)
                        {
                            errorType = "";
                            
                        }
                        validationActionVO.setType(errorType);
    
                        // Get the fields
                        xpath = "//validationConfig/validation[@order = '" + validationVO.getExecutionOrder() + "']/onValidationResponse[@code = '" + validationActionVO.getCode() + "']/field";
                        fieldNL = DOMUtil.selectNodes(config, xpath);
                        fields = new ArrayList();
                        for(int h = 0; h < fieldNL.getLength(); h++)
                        {
                            fieldNode = (Element)fieldNL.item(h);
                            vField = new ValidationFieldVO();
                            vField.setName((String)fieldNode.getAttribute("name"));
                            vField.setType((String)fieldNode.getAttribute("type"));
                            fields.add(vField);
                        }
                        validationActionVO.setFields(fields);
                        resultActions.put(validationActionVO.getCode(), validationActionVO);
                    }
                            
                    validationVO.setResultActions(resultActions);
    
                    validationList.add(validationVO);
                }
    
                // Sort the validation list
                Comparator listComparator = new ValidationListComparator();
                Collections.sort(validationList, listComparator);        
    
                validationConfigTempMap.put(origin.toLowerCase(), validationList);
            }
            
            // load static map with values
            validationConfigMap = new HashMap();
            validationConfigMap.putAll(validationConfigTempMap);
            
            if(logger.isDebugEnabled())
                logger.debug("End loading configuration files for Order Validator");
        }
    }


    private void loadValidationErrorTypes() throws IOException, SAXException,
                                            ParserConfigurationException, TransformerException, Exception
    {
        ConfigurationUtil configUtil = null;
        Map props = null;
        Iterator it = null;
        String key = null;
        String value = null;
        
        if(validationErrorTypes == null)
        {
            if(logger.isDebugEnabled())
                logger.debug("Begin loading validation error types for Order Validator");

            validationErrorTypes = new HashMap();
            configUtil = ConfigurationUtil.getInstance();
            props = configUtil.getProperties(METADATA_CONFIG_FILE_NAME);
            it = props.keySet().iterator();
            while(it.hasNext())
            {
                key = (String)it.next();
                value = (String)props.get(key);

                if(key.startsWith("VALIDATION_ERROR_TYPE_"))
                {
                    validationErrorTypes.put(key, value);
                }
            }
            if(logger.isDebugEnabled())
                logger.debug("End loading validation error types for Order Validator");
        }
    }

    public void insertAmazonAcknowledgement(Connection conn, String confirmationNumber, boolean success, String cancelReason) throws Exception {
    	logger.info("insertAmazonAcknowledgement() " + confirmationNumber + " " + success);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_AMAZON_ACKNOWLEDGEMENT");

        Map paramMap = new HashMap();
        paramMap.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
        paramMap.put("IN_STATUS", success ? "Success" : "Failure");
        paramMap.put("IN_CANCEL_REASON", cancelReason);
        dataRequest.setInputParams(paramMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status.equals("N")) {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }

    public void insertAmazonAdjustment(Connection conn, String confirmationNumber, String cancelReason) throws Exception {
    	logger.info("insertAmazonAdjustment() " + confirmationNumber);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_FULL_AMAZON_ADJUSTMENT");

        Map paramMap = new HashMap();
        paramMap.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
        paramMap.put("IN_ADJUSTMENT_REASON", cancelReason);
        dataRequest.setInputParams(paramMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status.equals("N")) {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }
   

    public void insertMercentAdjustment(Connection conn, String confirmationNumber, String cancelReason) throws Exception {
    	
    	logger.info("insertMercentAdjustment() " + confirmationNumber);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_FULL_MERCENT_ADJUSTMENT");

        Map paramMap = new HashMap();
        paramMap.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
        paramMap.put("IN_ADJUSTMENT_REASON", cancelReason);
        dataRequest.setInputParams(paramMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status.equals("N")) {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }
    
 public void insertMercentAcknowledgement(Connection conn, String confirmationNumber, boolean success, String cancelReason) throws Exception {
    	
    	logger.info("insertMercentAcknowledgement() " + confirmationNumber + " " + success);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_MERCENT_ACKNOWLEDGEMENT");

        Map paramMap = new HashMap();
        paramMap.put("IN_CONFIRMATION_NUMBER", confirmationNumber);
        paramMap.put("IN_STATUS", success ? "Success" : "Failure");
        paramMap.put("IN_CANCEL_REASON", cancelReason);
        dataRequest.setInputParams(paramMap);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);

        String status = (String) outputs.get("OUT_STATUS");
        if(status.equals("N")) {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }

    }
 
}