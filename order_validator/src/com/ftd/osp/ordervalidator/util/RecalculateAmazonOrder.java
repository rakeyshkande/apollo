package com.ftd.osp.ordervalidator.util;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SNHHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyCostHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ShippingKeyDetailHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SNHVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShippingKeyCostVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ShippingKeyDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.osp.ordervalidator.exception.RecalculateAmazonOrderException;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/** This class recalculate the amounts in the
 *  orderVO for Amazon.com orders only.  This class is very similar
 *  to the RecalculateOrderVO class.  However, this class is written specifically
 *  to handle an Amazon.com order only.
 *  
 *  @author Rose Lazuk 
 **/ 

public class RecalculateAmazonOrder 
{

  private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    
  //PRICE/SIZE TYPES
  private static final String SIZE_STANDARD="A";
  private static final String SIZE_DELUXE="B";
  private static final String SIZE_PREMIUM="C";

  //DISCOUNT TYPES
  private static final String DISCOUNT_TYPE_MILES = "M";
  private static final String DISCOUNT_TYPE_DOLLARS = "D";
  private static final String DISCOUNT_TYPE_PERCENT = "P";  

  //PRODUCT TYPES
  private static final String PRODUCT_TYPE_FRESHCUT = "FRECUT";
  private static final String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SDFC";
  private static final String PRODUCT_TYPE_FLORAL = "FLORAL";
  private static final String PRODUCT_TYPE_SAMEDAY_GIFT = "SDG";
  private static final String PRODUCT_TYPE_SPECIALTY_GIFT = "SPEGFT";
  
  //SHIP TYPES
  private static final String FLORIST_DELIVERED = "FL";
  private static final String SATURDAY_DELIVERED = "SA";

  //PROMOTION DISCOUNT TYPES
  private static final String PROMOTION_DISCOUNT_TYPE_DOLLAR = "DOL";  
  private static final String PROMOTION_DISCOUNT_TYPE_ITEM = "ITM";  
  private static final String PROMOTION_DISCOUNT_TYPE_PERCENT = "PER";    

  //STATE CODES
  private static final String HAWAII = "HI";
  private static final String ALASKA = "AK";

  //COUNTRY TYPES
  private static final String DOMESTIC = "D";

  //PRICE CODE IGNORE VALUE  
  private static final String IGNORE_PRICE_CODE_VALUE = "ZZ";

  //FLAGS
  private static final String FRESHCUT_TRIGGER_ON = "Y";
  private static final String DISCOUNTS_NOT_ALLOWED = "N";

  //DATABASE STATEMENTS
  private static final String GET_PRODUCT_BY_ID_STATEMENT = "GET_PRODUCT_BY_ID";
  private static final String PRODUCT_CURSOR = "OUT_CUR";
  private static final String GET_SOURCE_CODE_BY_ID_STATEMENT = "GET_SOURCE_CODE_RECORD";
  private static final String SOURCE_CODE_CURSOR = "OUT_SOURCE_RECORD";
  private static final String OE_GET_GLOBAL_PARMS_STATEMENT = "GET_GLOBAL_PARMS";
  private static final String OUT_PARAMS_CURSOR = "OUT_CUR";
  private static final String GET_COUNTRY_MASTER_BY_ID_STATEMENT = "GET_COUNTRY_MASTER_BY_ID";
  private static final String COUNTRY_CURSOR = "OUT_CUR";
  private static final String GET_ADDON_BY_ID_STATEMENT = "GET_ADDON_BY_ID";
  private static final String ADDON_CURSOR = "OUT_CUR";  
  private static final String GET_PRICE_HEADER_INFO_STATEMENT = "GET_PRICE_HEADER_DETAILS";
  private static final String PRICE_HEADER_CURSOR = "OUT_CUR";
  private static final String GET_MILES_POINTS_STATEMENT = "GET_MILES_POINTS";
  private static final String PROMOTION_CURSOR = "OUT_CUR";  
  private static final String GET_SHIPPING_KEY_DETAIL_ID_STATEMENT = "GET_SHIPPING_KEY_DETAILS_BYIDAMT";
  private static final String SHIPPING_DETAIL_ID_CURSOR = "OUT_CUR";  
  private static final String GET_SHIPPING_KEY_COSTS_STATEMENT = "GET_SHIPPING_KEY_COSTS_BYIDS";
  private static final String SHIPPING_KEY_COSTS_CURSOR = "OUT_CUR";  
  private static final String OE_GET_SNH_STATEMENT = "GET_SNH_BY_ID";
  private static final String OUT_SNH_CURSOR = "OUT_CUR";
  private static final String GET_SUBCODE_BY_ID_STATEMENT = "GET_PRODUCT_SUBCODE";
  private static final String GET_STATE_MASTER_BY_ID_STATEMENT = "GET_STATE_DETAILS";
  private static final String STATE_CURSOR = "OUT_CUR";
  
  //database columns
  private static final int ADDON_PRICE = 4;
  private static final int PRODUCT_MASTER_PRODUCT_TYPE =8;
  private static final int PRODUCT_MASTER_STANDARD_PRICE = 11;
  private static final int PRODUCT_MASTER_DELUXE_PRICE = 12;
  private static final int PRODUCT_MASTER_PREMIUM_PRICE = 13;
  private static final int PRODUCT_MASTER_VARIABLE_MAX_PRICE = 15;
  private static final int PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG=36;
  private static final int PRODUCT_MASTER_SHIPPING_KEY = 58;
  private static final int PRODUCT_MASTER_VARIABLE_PRICE_FLAG = 60;
  private static final int SOURCE_CODE_PARTNER_ID = 9;
  private static final int SOURCE_CODE_PRICING_CODE = 7;
  private static final int SOURCE_CODE_SHIPING_CODE = 8;
  private static final int SOURCE_CODE_JCPENNY_FLAG = 30;
  private static final int PRICE_HEADER_DISCOUNT_TYPE = 4;  
  private static final int PRICE_HEADER_DISCOUNT_AMOUNT = 5;
  private static final int PROMOTION_BASE_POINTS =2;
  private static final int PROMOTION_UNIT_POINTS =3;
  private static final int PROMOTION_UNITS =4;
  private static final int PROMOTION_TYPE=7;
  private static final int SHIPPING_DETAIL_ID_DETAIL_ID=1;
  private static final int SHIPPING_KEY_COSTS_SHIPPING_COST=3;
  private static final int GLOBAL_PARAMS_FRESHCUTS_SVC_CHARGE=17;
  private static final int GLOBAL_PARAMS_SPECIAL_SVC_CHARGE=18;
  private static final int GLOBAL_PARAMS_FRESHCUT_TRIGGER=19;
  private static final int GLOBAL_PARAMS_FRESHCUTS_SAT_SVC_CHARGE=21;
  private static final int COUNTRY_MASTER_COUNTRY_TYPE =2;
  private static final int SNH_DOMESTIC_CHARGE =3;
  private static final int SNH_INTL_CHARGE =6;
  private static final int STATE_MASTER_TAX_RATE=4;
  private static final int SUBCODE_PRICE = 4;  
  private static final int SUBCODE_PARENT_ID = 1;
  //scale to which amounts are calculated
  private static final int AMOUNT_SCALE = 2;
  
  //global params 
  private BigDecimal freshCutServiceCharge;
  private BigDecimal specialServiceCharge;
  private String freshCutServiceChargerTrigger;
  private BigDecimal freshCutSatDeliveryCharge;
  
  private Connection connection;
  private Logger logger;

  /** Constructor.
   * @param Connection database connection */
  public RecalculateAmazonOrder(Connection connection){
      this.connection = connection;
      this.logger = new Logger("com.ftd.osp.ordervalidator.util.RecalculateAmazonOrder");
  }


  /** Recalculate dollar amounts on the order VO.
   * @param OrderVO order
   * @returns OrderVO */
  public OrderVO recalculate(OrderVO order) 
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, TransformerException, RecalculateAmazonOrderException
  {  
    // Save the original product amounts for variable priced products
    Map originalProductAmounts = getProductAmounts(order);

    //data access object(s)
    CachedResultSet rs = null;

    //clear out existing amounts
    clearAmounts(order);

    //set class variables
    setGlobalParams();

    //order totals
    BigDecimal orderTotal = new BigDecimal(0);  
    BigDecimal orderShippingFee = new BigDecimal(0);  
    BigDecimal orderAddonAmount = new BigDecimal(0);
    BigDecimal orderProductAmount = new BigDecimal(0);
    BigDecimal orderTaxAmount = new BigDecimal(0);
    BigDecimal orderServiceFee = new BigDecimal(0);
    BigDecimal orderDiscountTotal = new BigDecimal(0);  
    BigDecimal orderPoints = new BigDecimal(0);  

        
    //source code information
    String sourceCode = null;
    String pricingCode = null;
    String partnerIdCode = null;
    String shippingCode = null;
    String jcPennyFlag = null;

    
    //get lines on order
    List lineItems = order.getOrderDetail();

    RecalculateAmazonOrderException lineException = null;
  
    //for each item in order
    for (int i=0 ;i<lineItems.size() ;i++ ) 
    {   

        //catch any bulk order exception that occurs at the line level and
        //continue processing.  The exception will be rethrown after all the lines have
        //been processed.
        try{
    
              OrderDetailsVO detailVO = (OrderDetailsVO)lineItems.get(i);

              //check for null values
              requiredCheck(detailVO);

              sourceCode = detailVO.getSourceCode();
              SourceMasterHandler sourceCodeHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
              if(sourceCodeHandler != null)  
              {
                  SourceMasterVO sourceVO = sourceCodeHandler.getSourceCodeById(sourceCode);
                  if(sourceVO == null)
                  {
                     String msg= "Source code not found in database(code="+sourceCode + ". (cache)";
                     logger.error(msg);
                     throw new RecalculateAmazonOrderException(msg);
                  }
                  pricingCode = sourceVO.getPricingCode();
                  partnerIdCode = sourceVO.getPartnerId();
                  shippingCode =  sourceVO.getShippingCode();
                  jcPennyFlag = sourceVO.getJcpenneyFlag();
              }
              else
              {
                  Map paramMap = new HashMap();
                  paramMap.put("IN_SOURCE_CODE",sourceCode);
                  DataRequest dataRequest = new DataRequest();
                  dataRequest.setConnection(connection);
                  dataRequest.setStatementID(GET_SOURCE_CODE_BY_ID_STATEMENT);
                  dataRequest.setInputParams(paramMap);
                  Map resultMap = null;      
                  DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
                  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
                  if(rs == null || !rs.next())
                  {
                    throw new RecalculateAmazonOrderException("Source code not found in database (code=" + sourceCode + "");
                  }
                  pricingCode = getValue(rs.getObject(SOURCE_CODE_PRICING_CODE));
                  partnerIdCode = getValue(rs.getObject(SOURCE_CODE_PARTNER_ID));
                  shippingCode =  getValue(rs.getObject(SOURCE_CODE_SHIPING_CODE));      
                  jcPennyFlag = getValue(rs.getObject(SOURCE_CODE_JCPENNY_FLAG));      
              }


            if(jcPennyFlag == null)
            {
              jcPennyFlag = "";
            }

            //retrieve product info from database
            rs = getProduct(detailVO.getProductId());

            /* Note there are two ways a subcode may be passed to this procedure.
             * The calling method may pass in the subcode in the subcode field
             * and the parent product in product id field.  Or the calling
             * object may pass the subcode into the product id field and
             * not pass in the parent product id */


            double subcodePrice = 0.0;
            boolean isSubcode = false;
            if(rs == null || !rs.next())
            {
              //product was not found, this may be a subcode
              rs = getSubcode(detailVO.getProductId());
              if(rs == null || !rs.next())
              {
                //product not found
                String msg = "Product not found in database(" + detailVO.getProductId() + ")";
                logger.error(msg);
                throw new RecalculateAmazonOrderException(msg);                
              }
              else
              {
                //get parent product id and the price
                subcodePrice = formatToNativeDouble(getValue(rs.getObject(SUBCODE_PRICE)));        
                String parentProduct = getValue(rs.getObject(SUBCODE_PARENT_ID));        

                //now retrieve the parent info and continue on
                rs = getProduct(parentProduct);                
                isSubcode = true;

                //update vo
                detailVO.setProductSubCodeId(detailVO.getProductId());
                detailVO.setProductId(parentProduct);

                
                //if parent not found we have a problem
                if(rs == null || !rs.next())
                {
                  //product not found
                  String msg = "Subcode parent product not found in database(" + parentProduct + ")";
                  logger.error(msg);
                  throw new RecalculateAmazonOrderException(msg);                
                }                
              }

            }
            String shippingKey = getValue(rs.getObject(PRODUCT_MASTER_SHIPPING_KEY));        
            double standardPrice = formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_STANDARD_PRICE)));
            double deluxePrice = formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_DELUXE_PRICE)));
            double premiumPrice = formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_PREMIUM_PRICE)));
            double variablePriceMax = formatToNativeDouble(getValue(rs.getObject(PRODUCT_MASTER_VARIABLE_MAX_PRICE)));
            String productType = getValue(rs.getObject(PRODUCT_MASTER_PRODUCT_TYPE));
            String discountAllowedFlag = getValue(rs.getObject(PRODUCT_MASTER_DISCOUNT_ALLOWED_FLAG));
            String variablePriceFlag = getValue(rs.getObject(PRODUCT_MASTER_VARIABLE_PRICE_FLAG));            

            //error if product does not have a product type
            if(productType == null || productType.length() <= 0){
              String msg = "Product does not have a product type.";
              logger.error(msg);
              throw new RecalculateAmazonOrderException(msg);
            }


            //This section of coded checks if a subcode was passed in using
            //the subcode field on the vo.
            String subcode = detailVO.getProductSubCodeId();
            if(!isSubcode && (subcode != null && subcode.length() > 0))
            {
              subcodePrice = getSubcodePrice(subcode);
              isSubcode = true;
            }

        
           //determine base price based on size choice
           BigDecimal itemPrice = new BigDecimal(0);
           String origAmount = (String)originalProductAmounts.get(detailVO.getLineNumber());

           //if origAmount is blank, null or zero then turn off the variable price flag
           //if(origAmount == null || origAmount.length()<=0 || origAmount.equals("0") || origAmount.equals("0.00"))
           //{
           //  variablePriceFlag = "N";
           //}

            //if variable price flag is null or emtpy then set it to N
            if(variablePriceFlag == null || variablePriceFlag.length() <= 0)
            {
              variablePriceFlag = "N";
            }

           
            double origAmountDouble;
            if(variablePriceFlag.equalsIgnoreCase("Y") && origAmount != null && Double.parseDouble(origAmount) > 0)
            {
                origAmountDouble = Double.parseDouble(origAmount);
                itemPrice = new BigDecimal(standardPrice);
            }
           else if(isSubcode)
               itemPrice = new BigDecimal(subcodePrice);
           else if(detailVO.getSizeChoice().equals(SIZE_STANDARD))
              itemPrice = new BigDecimal(standardPrice);
           else if(detailVO.getSizeChoice().equals(SIZE_DELUXE))
              itemPrice = new BigDecimal(deluxePrice);
           else if(detailVO.getSizeChoice().equals(SIZE_PREMIUM))
              itemPrice = new BigDecimal(premiumPrice);
           else 
              throw new RecalculateAmazonOrderException("Invalid size choice:" + detailVO.getSizeChoice());
           itemPrice = itemPrice.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
           
            // Defect 1768: Preserve the original product amount.
            // Only replace if zero.
            if (detailVO.getProductsAmount() == null ||
            (detailVO.getProductsAmount() != null && Float.parseFloat(detailVO.getProductsAmount()) == 0)) {
           detailVO.setProductsAmount(itemPrice.toString());
            }
        
            //get shipping fee
            BigDecimal lineShippingFee = calcShippingFee(detailVO.getShipMethod(),
                            shippingKey,itemPrice.doubleValue(),productType,detailVO).setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
            detailVO.setShippingFeeAmount(lineShippingFee.toString());

            //get service fee
            BigDecimal lineServiceFee = calcServiceFee(detailVO, productType, shippingCode,detailVO.getShipMethod()).setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
            detailVO.setServiceFeeAmount(lineServiceFee.toString());
        
            //calculate addon amounts
            List addonList = detailVO.getAddOns();
            BigDecimal lineAddonTotal = new BigDecimal(0);
            //for each addon on detail line if there are any addons
            if(addonList != null){
                for(int addoni = 0; addoni < addonList.size(); addoni++){
                    AddOnsVO addonVO = (AddOnsVO)addonList.get(addoni);
                    requiredCheck(addonVO);
                    BigDecimal addOnPrice = calcAddonPrice(addonVO.getAddOnCode());
                    BigDecimal addonTotal = addOnPrice.multiply(new BigDecimal(addonVO.getAddOnQuantity()));
                    addonVO.setPrice((addonTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
                    lineAddonTotal = lineAddonTotal.add(addonTotal);
                }//end addon loop
            }
            detailVO.setAddOnAmount((lineAddonTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());        
            
            //set discount and reward amounts
            calcDiscounts(pricingCode,sourceCode,detailVO,discountAllowedFlag);
            calcRewards(sourceCode,detailVO);
            BigDecimal discountAmount = new BigDecimal(detailVO.getDiscountAmount());
            BigDecimal pointAmount = new BigDecimal(detailVO.getMilesPoints());
        
            //get tax amount --!!! Note Tax is rounded DOWN
            BigDecimal taxAmount = calcTax(detailVO,jcPennyFlag);
            taxAmount = taxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_DOWN);
            detailVO.setTaxAmount(taxAmount.toString());

            //calculate line total
            BigDecimal lineTotal = itemPrice;
            lineTotal = lineTotal.subtract(discountAmount);                
            lineTotal = lineTotal.add(lineAddonTotal);
            lineTotal = lineTotal.add(taxAmount);
            lineTotal = lineTotal.add(lineServiceFee);
            lineTotal = lineTotal.add(lineShippingFee);

            lineTotal = lineTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
            detailVO.setExternalOrderTotal(lineTotal.toString());
        
            //increment order level totals
            orderTotal = orderTotal.add(lineTotal);
            orderProductAmount = orderProductAmount.add(itemPrice);
            orderTaxAmount = orderTaxAmount.add(taxAmount);
            orderServiceFee = orderServiceFee.add(lineServiceFee);
            orderShippingFee = orderShippingFee.add(lineShippingFee);
            orderAddonAmount = orderAddonAmount.add(lineAddonTotal);
            orderDiscountTotal = orderDiscountTotal.add(discountAmount);
            orderPoints = orderPoints.add(pointAmount);
        }//end try
        catch(RecalculateAmazonOrderException boe)
        {
          if(lineException == null){
            lineException = boe;
          }
        }
        catch(Throwable t)
        {
          if(lineException == null)
          {
            lineException = new RecalculateAmazonOrderException("Could not calculate product price.");
            logger.error(t);
          }
        }

      }//end for each item

      // Set order total
      order.setOrderTotal((orderTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      //order.setProductsTotal((orderProductAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setTaxTotal((orderTaxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setServiceFeeTotal((orderServiceFee.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setShippingFeeTotal((orderShippingFee.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setAddOnAmountTotal((orderAddonAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setDiscountTotal((orderDiscountTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
      order.setPartnershipBonusPoints((orderPoints.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());

      //if an exception occured at the line level throw it
      if(lineException != null)
      {
        throw lineException;
      }

      //calling objects need zero amounts to be null
      //fixUpAmounts(order);
      //-This is not being done because price values in VO cannot be null
  
    return order;
  }

  /* Add reward  information to order VO.
   * @param String pricing code
   * @param double non-discoutned price
   * @return double discounted price*/
  private void calcRewards(String sourceCode, OrderDetailsVO detailVO)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, RecalculateAmazonOrderException
   {

       logger.info("do calcRewards: " + sourceCode);
       Double productPrice = new Double(detailVO.getProductsAmount());
       Double orderTotal = new Double(detailVO.getExternalOrderTotal());

       //zero out miles
       detailVO.setMilesPoints("0");

       Map paramMap = new HashMap();
       paramMap.put("IN_SOURCE_CODE", sourceCode);
       paramMap.put("IN_PRODUCT_AMOUNT", productPrice);
       paramMap.put("IN_ORDER_AMOUNT", orderTotal);
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(connection);
       dataRequest.setStatementID(GET_MILES_POINTS_STATEMENT);
       dataRequest.setInputParams(paramMap);
       DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

       Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
       String milesPoints = (String) outputs.get("OUT_MILES_POINTS");
       if (milesPoints == null) milesPoints = "0";
       String rewardType = (String) outputs.get("OUT_REWARD_TYPE");

       //detailVO.setPercentOff(unitPoints.toString());              

       detailVO.setMilesPoints(milesPoints); 
       detailVO.setRewardType(rewardType);
       
       logger.info("rewardType: " + rewardType + " - value: " + milesPoints);
     
   }


  /* Add discount information to order VO.
   * @param String pricing code
   * @param double non-discoutned price
   * @return double discounted price*/
   private void calcDiscounts(String pricingCode,
        String sourceCode, OrderDetailsVO detailVO, String discountFlag)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, RecalculateAmazonOrderException
   {

       logger.info("do calcDiscounts: " + sourceCode);

      BigDecimal productPrice = new BigDecimal(detailVO.getProductsAmount());

      //zero out discount amount
      detailVO.setDiscountAmount("0");

      //if no price code, then no discount
      if(pricingCode == null)
      {
        return;
      }

      //Check if the PRICE_HEADER table should be used for discount information
      if(!pricingCode.equals(IGNORE_PRICE_CODE_VALUE)){

            //Use pricing code value and price_header table to determine discount
            //or reward amount

          BigDecimal hundreths = new BigDecimal(0.01);
            
          //retrieve price header information
          Map paramMap = new HashMap();
          paramMap.put("IN_PRICE_HEADER_ID",pricingCode);
          paramMap.put("IN_PRICE_AMT",new Double(detailVO.getProductsAmount()));
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_PRICE_HEADER_INFO_STATEMENT);
          dataRequest.setInputParams(paramMap);
          Map priceHeaderMap = null;      
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          if(rs == null || !rs.next())
          {
           //no discounts
           return;
          }
          String discountType = getValue(rs.getObject(PRICE_HEADER_DISCOUNT_TYPE));
          BigDecimal discountAmount = formatToBigDecimal(getValue(rs.getObject(PRICE_HEADER_DISCOUNT_AMOUNT)));

          //store disocunt type if a discount exist (amount > 0)
          if(discountAmount.doubleValue() > 0){          
            detailVO.setDiscountType(discountType);
          }

          //what kind of discount do we have?
          if(discountType.equals(DISCOUNT_TYPE_MILES))
          {
              detailVO.setMilesPoints(discountAmount.toString());
          }
          else if(discountType.equals(DISCOUNT_TYPE_DOLLARS))
          {
            detailVO.setDiscountAmount(discountAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN).toString());            
          }
          else if(discountType.equals(DISCOUNT_TYPE_PERCENT))
          {
            //!! Note discounts are rounded down
            BigDecimal amountOff = productPrice.multiply(discountAmount).multiply(hundreths);
            amountOff = amountOff.setScale(AMOUNT_SCALE,BigDecimal.ROUND_DOWN);
            detailVO.setDiscountAmount(amountOff.toString());
            detailVO.setPercentOff(discountAmount.toString());
          }
      }//end if using price header table
      else
      {
          Map paramMap = new HashMap();
          paramMap.put("IN_SOURCE_CODE", sourceCode);
          paramMap.put("IN_PRODUCT_AMOUNT", new Double(detailVO.getProductsAmount()));
          paramMap.put("IN_ORDER_AMOUNT", new Double(detailVO.getExternalOrderTotal()));
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_MILES_POINTS_STATEMENT);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
           
          String milesPoints = (String) outputs.get("OUT_MILES_POINTS");
          if (milesPoints == null) milesPoints = "0";
          String rewardType = (String) outputs.get("OUT_REWARD_TYPE");

          //detailVO.setPercentOff(unitPoints.toString());              

          detailVO.setMilesPoints(milesPoints); 
          detailVO.setRewardType(rewardType);
          
          logger.info("rewardType: " + rewardType + " - value: " + milesPoints);
      }

    /* If product is not allowed to have discounts, remove discount from VO, but
     * leave the rewards.*/
     if(discountFlag.equals(DISCOUNTS_NOT_ALLOWED))
     {
       detailVO.setDiscountAmount("0");
     }

     //set discounted price
     BigDecimal discountedPrice = (new BigDecimal(detailVO.getProductsAmount())).subtract(new BigDecimal(detailVO.getDiscountAmount()));
     detailVO.setDiscountedProductPrice(discountedPrice.setScale(2,5).toString());
      
   }

   /* Calc tax amount using SOURCE_CODE table*/
   private BigDecimal calcTax(OrderDetailsVO detailVO,String jcPennyFlag)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, RecalculateAmazonOrderException
   {
      BigDecimal tax = new BigDecimal(0);

      //JC Penny orders are not taxed, (emueller, 4/1/04, prod defect#356)
      if(jcPennyFlag !=null && jcPennyFlag.equals("Y"))
      {
        return tax;
      }


      //get recip state
      RecipientAddressesVO address = getRecipientAddress(detailVO);
      if(address == null)
      {
        return new BigDecimal(0);
      }
      String state = address.getStateProvince();

      //if there is not state (not US) exist
      if(state == null || state.length() <= 0)
      {
        return new BigDecimal(0);
      }

      //get tax percent for state
      BigDecimal taxPercent = new BigDecimal(0);    //getTaxRate(state);
      if(taxPercent == null)
      {
        return new BigDecimal(0);
      }

      //convert percent to decimal
      BigDecimal hundreths = new BigDecimal(0.01);
      taxPercent = taxPercent.multiply(hundreths);

      //calculate amount to be taxed
      BigDecimal addonAmount = new BigDecimal(0);
      BigDecimal shippingFee = new BigDecimal(0);
      BigDecimal serviceCharge = new BigDecimal(0);
      BigDecimal discountAmount = new BigDecimal(0);      
      BigDecimal productAmount = new BigDecimal(0);      
      if(detailVO.getProductsAmount() != null){
        productAmount = new BigDecimal(detailVO.getProductsAmount());
      }
      if(detailVO.getAddOnAmount() != null){
        addonAmount = new BigDecimal(detailVO.getAddOnAmount());
      }
      if(detailVO.getShippingFeeAmount() != null){
        shippingFee = new BigDecimal(detailVO.getShippingFeeAmount());
      }
      if(detailVO.getServiceFeeAmount() != null){
        serviceCharge = new BigDecimal(detailVO.getServiceFeeAmount());
      }
      if(detailVO.getDiscountAmount() != null){
        discountAmount = new BigDecimal(detailVO.getDiscountAmount());
      }


      BigDecimal taxableAmount = productAmount.add(addonAmount).add(shippingFee).add(serviceCharge);
      taxableAmount = taxableAmount.subtract(discountAmount);

      //calculate tax
      tax = taxableAmount.multiply(taxPercent);
         
     return tax;
   }

  /* Calc Service Fee */
  private BigDecimal calcServiceFee(OrderDetailsVO detailVO, String productType,String shippingCode, String shipMethod)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, RecalculateAmazonOrderException
  {
    BigDecimal serviceFee = new BigDecimal(0);

    boolean treatAsFloralOverride = false;


    /* If the product is freshcut and the freshcut service
     * flag is off, then calculate this service fee as
     * if it was a FLORAL product. */
      if(productType.equals(PRODUCT_TYPE_FRESHCUT) || 
          productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT) &&
            !freshCutServiceChargerTrigger.equals(FRESHCUT_TRIGGER_ON)){     
              treatAsFloralOverride = true;
            }
            




     /* If product is a freshcut */
      if(productType.equals(PRODUCT_TYPE_FRESHCUT) && !treatAsFloralOverride)
      {        
            //get the service free from the GLOBAL_PARMS table
            serviceFee = freshCutServiceCharge;
      }
     /* If product is a same day freshcut */
      if(productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT) && !treatAsFloralOverride)
      {        
            //get the service free from the GLOBAL_PARMS table
            serviceFee = freshCutServiceCharge;
      }
      //floral orders get the service fee from SNH
      else if(productType.equals(PRODUCT_TYPE_FLORAL) || treatAsFloralOverride)
      {
        //get service fee from SNH table
        SNHHandler snhHandler = null; //(SNHHandler)CacheController.getInstance().getHandler("CACHE_NAME_SNH");
        BigDecimal domesticFee = null;                
        BigDecimal intlFee = null;                
        if(snhHandler != null)  
        {
            SNHVO snhVO = snhHandler.getSNHDetails(shippingCode);
            if(snhVO == null)
            {
                String msg = "SNH ID key not found in database.["+shippingCode+"] (cache)";
                logger.error(msg);
                throw new RecalculateAmazonOrderException(msg);
            }            
            domesticFee =  snhVO.getFirstOrderDomestic();
            intlFee = snhVO.getFirstOrderInternational();
        }
        else
        {
            String deliveryDate = detailVO.getDeliveryDate();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            try
            {
                deliveryDate = sdf.format(new java.sql.Date(sdf.parse(deliveryDate).getTime()));
            } catch (Exception e) {
                deliveryDate = "";
            }
            logger.info("get_snh_by_id: " + shippingCode + " " + deliveryDate);

            Map paramMap = new HashMap();
            paramMap.put("IN_SNH_ID",shippingCode);
            paramMap.put("IN_DELIVERY_DATE", deliveryDate);
            // Get the order header infomration
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(OE_GET_SNH_STATEMENT);
            dataRequest.setInputParams(paramMap);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
            if(rs == null || !rs.next())
            {
                String msg = "SNH ID key not found in database.["+shippingCode+"]";
                logger.error(msg);
                throw new RecalculateAmazonOrderException(msg);
            }
            domesticFee = formatToBigDecimal(getValue(rs.getObject(SNH_DOMESTIC_CHARGE)));                
            intlFee = formatToBigDecimal(getValue(rs.getObject(SNH_INTL_CHARGE)));                
        }
        //get the recipient country
        RecipientAddressesVO address = getRecipientAddress(detailVO);
        requiredCheck(address);
        if(address == null || isDomestic(address.getCountry()))
        {        
            //set fee as domestic
            serviceFee = domesticFee;

            //if sending to ALASKA or HAWAII an extra charge is applied
             if(address.getStateProvince().equals(HAWAII) || 
                   address.getStateProvince().equals(ALASKA))
                   {
                     //add additionial shipping charge
                     //Only add charge if carrier delivered Defect#489
                     if(detailVO.getShipMethodCarrierFlag().equals("Y")){
                       serviceFee = serviceFee.add(specialServiceCharge);
                     }
                   }
        }
        else
        {
            //intl fee
            serviceFee = intlFee;
        }//end if address empty or is domestic


        //if freshcut and saturday delivery, add additionial charge
        if(productType.equals(PRODUCT_TYPE_FRESHCUT) || 
              productType.equals(PRODUCT_TYPE_SAMEDAY_FRESHCUT))
              {
                if(shipMethod != null && shipMethod.equals(SATURDAY_DELIVERED)){
                  serviceFee = serviceFee.add(freshCutSatDeliveryCharge);
                }
              }

        
      }
      else
      {
        //There is no service charge for this item.
        //There is most likely a shipping fee instead
      }



    //lookup discount in SNH
    return serviceFee;
  }

  /* Calculate the shipping fee.  Shipping fee is based on the product selected,
   * price of the product, and ship method.  Shipping fee only applies to 
   * products which are carrier delivered and not freshcut.  
   * An extra amounts is added to orders going to Hawii and Alaska.
   * 
   * @param String shipMethod, how the time will be shipped
   * @param String shippingKey shipping key for the product
   * @param double price product price
   * @return BigDecimal shipping fee*/
  private BigDecimal calcShippingFee(String shipMethod, String shippingKey, 
        double price,String productType,OrderDetailsVO detailVO)
    throws IOException, SAXException, ParserConfigurationException, 
            SQLException, RecalculateAmazonOrderException
  {



    /* Shipping fees only apply to SAMEDAY GIFT and SPECIALTY GIFT items.*/
     if(!(productType.equals(PRODUCT_TYPE_SAMEDAY_GIFT) || 
          productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)))
     {
        return new BigDecimal("0");
     }

    //if shipMethod is null assume florist delivered
    if(shipMethod == null || shipMethod.length() < 1)
    {
        String msg = "A ship method does not exist for this item.  The item is probably not deliverable for the date selected.";
        logger.error(msg);
        throw new RecalculateAmazonOrderException(msg);
    }

     //used for DB access
     Map paramMap = new HashMap();
     DataRequest dataRequest = new DataRequest();
     DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
     CachedResultSet rs = null;
  
    /*Using the shipping key details table determine what shipping detail
      id should be associated with this item.  Which shipping detail id
      to use is dependant on the shipping key code of the product (PRODUCT_MASTER)
      and the ship method choosen.*/
     ShippingKeyDetailHandler shippingKeyDetailsHandler = (ShippingKeyDetailHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SHIPPING_KEY_DETAIL");
      long shippingDetailId = 0;
      if(shippingKeyDetailsHandler != null)  
      {
          ShippingKeyDetailVO shipKeyDetailVO = shippingKeyDetailsHandler.getShippingKeyDetails(shippingKey,new BigDecimal(price));
          if(shipKeyDetailVO == null)
          {
            String msg = "Shipping key not found in database.["+shippingKey+":"+price+"] (cache)";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          shippingDetailId = Long.parseLong(shipKeyDetailVO.getShippingDetailId());
      }
      else
      {
          paramMap.put("IN_SHIPPING_KEY_ID",shippingKey);
          paramMap.put("IN_PRICE_AMT",new Double(price));
          // Get the order header infomration
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_SHIPPING_KEY_DETAIL_ID_STATEMENT);
          dataRequest.setInputParams(paramMap);
          dataAccessUtil = DataAccessUtil.getInstance();
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          if(rs == null || !rs.next())
          {
            String msg = "Shipping key not found in database.["+shippingKey+":"+price+"]";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          shippingDetailId = new Long(getValue(rs.getObject(SHIPPING_DETAIL_ID_DETAIL_ID))).longValue();
      }


      /* Now using the shipping detail id that was just obtained along with 
       * the ship method to determine the shipping cost.  The SHIPPING_KEY_COSTS
       * table is used to find the cost. */
      ShippingKeyCostHandler shippingKeyCostsHandler = (ShippingKeyCostHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SHIPPING_KEY_COST");
      BigDecimal shippingFee = null;         
      if(shippingKeyCostsHandler != null)  
      {
          ShippingKeyCostVO keyCostsVO = shippingKeyCostsHandler.getShippingKeyCosts(new Long(shippingDetailId).toString(),shipMethod);
          if(keyCostsVO == null)
          {
            String msg = "Shipping cost not found in database.["+shippingDetailId+":"+shipMethod+"] (cache)";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          shippingFee = keyCostsVO.getShippingCost();
      }
      else
      {
          paramMap = new HashMap();
          paramMap.put("IN_SHIPPING_KEY_DETAIL_ID",new Long(shippingDetailId).toString());
          paramMap.put("IN_SHIPPING_METHOD_ID",shipMethod);
          // Get the order header infomration
          dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_SHIPPING_KEY_COSTS_STATEMENT);
          dataRequest.setInputParams(paramMap);
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          if(rs == null || !rs.next())
          {
            String msg = "Shipping cost not found in database.["+shippingDetailId+":"+shipMethod+"]";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          shippingFee = formatToBigDecimal(getValue(rs.getObject(SHIPPING_KEY_COSTS_SHIPPING_COST)));         
      }
      
    /* If the order is being delivered to Hawaii or Alaska and extra charge
     * is added to the shipping cost.  This charge only applies to 
     * specialty gifts.  */
     if(productType.equals(PRODUCT_TYPE_SPECIALTY_GIFT)){
         RecipientAddressesVO address = getRecipientAddress(detailVO);
         if(address != null)
         {
           //we have an address
           if(address.getStateProvince().equals(HAWAII) || 
                 address.getStateProvince().equals(ALASKA))
                 {
                   //add additionial shipping charge
                     //Only add charge if carrier delivered Defect#489
                     if(detailVO.getShipMethodCarrierFlag().equals("Y")){                   
                       shippingFee = shippingFee.add(specialServiceCharge);
                     }
                 }
         }
     }
     
    return shippingFee;
  }

  /* Retrieve the product results set from the datatbase.
   * @param String product id
   * @returns CacchedResultSet*/
  private CachedResultSet getProduct(String productId)  
    throws SAXException, IOException, ParserConfigurationException, SQLException
  {
      Map paramMap = new HashMap();

      paramMap.put("IN_PRODUCT_ID",productId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_PRODUCT_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      Map orderMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      return (CachedResultSet)dataAccessUtil.execute(dataRequest);   
   
  }


  /* Retrieve the subcode results set from the datatbase.
   * @param String product id
   * @returns CacchedResultSet*/
  private double getSubcodePrice(String subcodeId)  
    throws SAXException, IOException, ParserConfigurationException, SQLException,RecalculateAmazonOrderException
  {
      Map paramMap = new HashMap();

      double price = 0;

      paramMap.put("PRODUCT_ID",subcodeId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_SUBCODE_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      DataAccessUtil dau = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
      if(rs.next())
      {
          price = formatToNativeDouble(getValue(rs.getObject(4)));
      }
      else
      {
          String msg = "Subcode not found in database.";
          logger.error(msg);
          throw new RecalculateAmazonOrderException(msg);
      }

      
      return price;   
   
  }


  /* Retrieve the subcode results set from the datatbase.
   * @param String product id
   * @returns CacchedResultSet*/
  private CachedResultSet getSubcode(String subcodeId)  
    throws SAXException, IOException, ParserConfigurationException, SQLException,RecalculateAmazonOrderException
  {
      Map paramMap = new HashMap();

      double price = 0;

      paramMap.put("PRODUCT_ID",subcodeId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_SUBCODE_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      DataAccessUtil dau = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);

      return rs;
      

   
  }

  /* Retrieve the addon price from the database.
   * @param String addonCode
   * @returns BigDecimal the addon price*/
  private BigDecimal calcAddonPrice(String addonCode)
    throws SAXException, IOException, ParserConfigurationException, 
            SQLException,RecalculateAmazonOrderException
  {

      Map paramMap = new HashMap();     

      paramMap.put("IN_ADDON_ID",addonCode);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(connection);
      dataRequest.setStatementID(GET_ADDON_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      Map addonMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
      if(rs == null || !rs.next())
      {
        String msg = "Addon not found in database. Addon:" + addonCode;
        logger.error(msg);
        throw new RecalculateAmazonOrderException(msg);
      }
      BigDecimal price = new BigDecimal(getValue(rs.getObject(ADDON_PRICE)));

      return price;

  }

 /* Retrieve the global parameters from the datbase.*/
  private void setGlobalParams()
    throws SAXException, IOException, ParserConfigurationException, 
            SQLException,RecalculateAmazonOrderException
  {
      //Map paramMap = new HashMap();

      GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
      if(globalParamHandler != null)
      {
          FTDAppsGlobalParmsVO global = globalParamHandler.getFTDAppsGlobalParms();
          if(global == null)
          {
            String msg = "Global params not found in database. (cache)";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          freshCutServiceCharge = global.getFreshCutsSvcCharge();
          specialServiceCharge = global.getSpecialSvcCharge();
          freshCutServiceChargerTrigger = global.getFreshCutsSvcChargerTrigger();
          freshCutSatDeliveryCharge = global.getFreshCutsSatCharge();
      }
      else
      {        
          // Get the order header infomration
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(OE_GET_GLOBAL_PARMS_STATEMENT);
          //dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          //Object test= dataAccessUtil.execute(dataRequest);
          CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);    
          if(rs == null || !rs.next())
          {
            String msg = "Global params not found in database.";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          freshCutServiceCharge = new BigDecimal(getValue(rs.getObject(GLOBAL_PARAMS_FRESHCUTS_SVC_CHARGE)));
          specialServiceCharge = new BigDecimal(getValue(rs.getObject(GLOBAL_PARAMS_SPECIAL_SVC_CHARGE)));
          freshCutServiceChargerTrigger = (String)rs.getObject(GLOBAL_PARAMS_FRESHCUT_TRIGGER);
          freshCutSatDeliveryCharge = new BigDecimal(getValue(rs.getObject(GLOBAL_PARAMS_FRESHCUTS_SAT_SVC_CHARGE)));
      }

  }

/* This method takes in a country code and returns
 * a boolean value indicating if the country is considered
 * domestic.*/
  private boolean isDomestic(String country)
    throws SAXException, IOException, ParserConfigurationException, 
            SQLException,RecalculateAmazonOrderException
  {

      //If united states do not do a database lookup
      if(country.equalsIgnoreCase("US") || country.equalsIgnoreCase("USA"))
      {
        return true;
      }


      CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");
      String domesticFlag = null;
      if(countryHandler != null)
      {  
          CountryMasterVO countryVO = countryHandler.getCountryById(country);
          if(countryVO == null)
          {
            String msg = "Country code not found in database.[" + country + "] (cache)";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          domesticFlag = countryVO.getCountryType();
      }
      else
      {      
          Map paramMap = new HashMap();
          paramMap.put("IN_COUNTRY_ID",country);
          // Get the order header infomration
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_COUNTRY_MASTER_BY_ID_STATEMENT);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          if(rs == null || !rs.next())
          {
            String msg = "Country code not found in database.[" + country + "]";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          domesticFlag = getValue(rs.getObject(COUNTRY_MASTER_COUNTRY_TYPE));
      }
      
      return domesticFlag.equals(DOMESTIC) ? true : false;

  }

  /* This method returns a recipient's address object.  If one does not
   * exist null is returned.
   * @param OrderDetailsVO order line
   * @RecipientAddressVO*/
 private RecipientAddressesVO getRecipientAddress(OrderDetailsVO detailVO)
 {
    RecipientAddressesVO address = null;
 
     List recipientList = detailVO.getRecipients();
     if(recipientList != null && recipientList.size() > 0 ){
       RecipientsVO recipient = (RecipientsVO)recipientList.get(0);
       if(recipient != null){
         List addressList = recipient.getRecipientAddresses();
         if(addressList != null && addressList.size() > 0){
             address = (RecipientAddressesVO)addressList.get(0);
         }
       }
     }

    return address;
 }

/* This method retrieves the tax rate for a give state.
 * @param String state code
 * @return BigDecimal tax rate*/
/*
  private BigDecimal getTaxRate(String state)
    throws SAXException, IOException, ParserConfigurationException, 
            SQLException,RecalculateAmazonOrderException
  {

  

      // Get the order header infomration
      //get service fee from SNH table
      StateMasterHandler statesHandler = (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");
      BigDecimal domesticFee = null;                
      BigDecimal intlFee = null;                
      BigDecimal taxRate = null;
      if(statesHandler != null)  
      {
          StateMasterVO stateVO = statesHandler.getStateById(state);
          if(stateVO == null)
          {
            String msg = "State code not found in database.[" + state + "] (cache)";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          taxRate = stateVO.getTaxRate();
      }
      else
      {
          Map paramMap = new HashMap();
          paramMap.put("IN_STATE_MASTER_ID",state);
          DataRequest dataRequest = new DataRequest();
          dataRequest.setConnection(connection);
          dataRequest.setStatementID(GET_STATE_MASTER_BY_ID_STATEMENT);
          dataRequest.setInputParams(paramMap);
          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
          if(rs == null || !rs.next())
          {
            String msg = "State code not found in database.[" + state + "]";
            logger.error(msg);
            throw new RecalculateAmazonOrderException(msg);
          }
          taxRate = formatToBigDecimal(getValue(rs.getObject(STATE_MASTER_TAX_RATE)));
      }
      
      return taxRate;

  }
*/
    /* This method chech for null values.
     * If any found an exception is thrown.*/
     private void requiredCheck(RecipientAddressesVO addressVO) throws RecalculateAmazonOrderException
     {
       //country
       if(addressVO.getCountry() == null || addressVO.getCountry().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing recipient country");
       }
       else{
         //state
         if(addressVO.getCountry().equals("US") && (addressVO.getStateProvince() == null || addressVO.getStateProvince().length() <= 0))
         {       
           throw new RecalculateAmazonOrderException("Missing recipient state");
         }
       }
     }

    /* This method checks for null values.
     * If any found an exception is thrown.*/
     private void requiredCheck(AddOnsVO addonVO) throws RecalculateAmazonOrderException
     {
       //country
       if(addonVO.getAddOnCode() == null || addonVO.getAddOnCode().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing AddOn Code");
       }
       //state
       if(addonVO.getAddOnQuantity() == null || addonVO.getAddOnQuantity().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing Addon Quantity");
       }
     }


    /* This method chech for null values.     * 
     * If any found an exception is thrown.*/
     private void requiredCheck(OrderDetailsVO detailVO) throws RecalculateAmazonOrderException
     {

       //source code
       if(detailVO.getSourceCode() == null || detailVO.getSourceCode().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing source code on detail.");
       }

       //product id
       if(detailVO.getProductId() == null || detailVO.getProductId().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing product id");
       }

       //size choice
       if(detailVO.getSizeChoice() == null || detailVO.getSizeChoice().length() <= 0)
       {
         throw new RecalculateAmazonOrderException("Missing size");
       }       
     }


    /* Return native double from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/    
    private double formatToNativeDouble(String value)
    {
      return value == null ? 0 : new Double(value).doubleValue();
    } 

    /* Return native double from String object
     * @param String
     * @return double value
     *         returns zero if String is null*/    
    private BigDecimal formatToBigDecimal(String value)
    {
      return value == null ? new BigDecimal(0) : new BigDecimal(value);
    } 

  /* This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*/
  private String getValue(Object obj) 
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();      
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }

  /* This method will null out any amount which contains a zero.
   * The object(s) using this object need the values to be null.*/
   private void fixUpAmounts(OrderVO order)
   {
     //order level amounts
     order.setAddOnAmountTotal(formatZero(order.getAddOnAmountTotal()));
     order.setDiscountTotal(formatZero(order.getDiscountTotal()));
     order.setOrderTotal(formatZero(order.getOrderTotal()));
     order.setPartnershipBonusPoints(formatZero(order.getPartnershipBonusPoints()));
     //order.setProductsTotal(formatZero(order.getProductsTotal()));
     order.setServiceFeeTotal(formatZero(order.getServiceFeeTotal()));
     order.setShippingFeeTotal(formatZero(order.getShippingFeeTotal()));
     order.setTaxTotal(formatZero(order.getTaxTotal()));

     
      //detail level amounts
      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++) 
      {
        OrderDetailsVO detail = (OrderDetailsVO)details.get(i);
        detail.setAddOnAmount(formatZero(detail.getAddOnAmount()));
        detail.setDiscountAmount(formatZero(detail.getDiscountAmount()));
        detail.setExternalOrderTotal(formatZero(detail.getExternalOrderTotal()));
        detail.setMilesPoints(formatZero(detail.getMilesPoints()));
        detail.setProductsAmount(formatZero(detail.getProductsAmount()));
        detail.setServiceFeeAmount(formatZero(detail.getServiceFeeAmount()));
        detail.setShippingFeeAmount(formatZero(detail.getShippingFeeAmount()));
        detail.setTaxAmount(formatZero(detail.getTaxAmount()));
      }
      
     
   }

  /* This method will clear out all existing amounts.*/
   private void clearAmounts(OrderVO order)
   {
     //order level amounts
     order.setAddOnAmountTotal("0");
     order.setDiscountTotal("0");
     order.setOrderTotal("0");
     order.setPartnershipBonusPoints("0");
     //order.setProductsTotal("0");
     order.setServiceFeeTotal("0");
     order.setShippingFeeTotal("0");
     order.setTaxTotal("0");

     
      //detail level amounts
      List details = order.getOrderDetail();
      for (int i = 0; i < details.size(); i++) 
      {
        OrderDetailsVO detail = (OrderDetailsVO)details.get(i);
        detail.setAddOnAmount("0");
        detail.setDiscountAmount("0");
        detail.setExternalOrderTotal("0");
        detail.setMilesPoints("0");
        detail.setProductsAmount("0");
        detail.setServiceFeeAmount("0");
        detail.setShippingFeeAmount("0");
        detail.setTaxAmount("0");
      }
      
     
   }


   /* If the passed in value is blank or zero return null,
    * else return the same value back*/
    private String formatZero(String value)
    {

      String out = value;
    
      if(value == null || value.length() < 1 || Double.parseDouble(value) == 0.0)
      {
        out = null;
      }

      return out;
    }

    private Map getProductAmounts(OrderVO order)
    {
        Map amounts = new HashMap();

        //detail level amounts
        List details = order.getOrderDetail();
        for (int i = 0; i < details.size(); i++) 
        {
            OrderDetailsVO detail = (OrderDetailsVO)details.get(i);
            amounts.put(detail.getLineNumber(), detail.getProductsAmount());
        }

        return amounts;
    }

  public static void main(String[] args)

  {
  BigDecimal discountAmount = new BigDecimal("1.22");
  discountAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN);
  System.out.println("::"+discountAmount.toString());
  

  }

}