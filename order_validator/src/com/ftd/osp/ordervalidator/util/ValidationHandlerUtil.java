package com.ftd.osp.ordervalidator.util;

import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * This class contains methods that are used by more than one class in Order Validator. *
 */
public class ValidationHandlerUtil {
    
    private static Logger logger = new Logger(ValidationHandlerUtil.class.getName());
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    
    public ValidationHandlerUtil() {
    }
    
    
   /*  Check to see if all items in a shopping cart have been removed. 
    */
    public boolean isAllLineItemsRemoved(OrderVO order)
                                                   throws ValidationException 
    {
       Iterator i = order.getOrderDetail().iterator();
       OrderDetailsVO line = null;
       
       while (i.hasNext()) 
       {
           line = (OrderDetailsVO) i.next();
           if (!line.getStatus().equals(
                                   String.valueOf(OrderStatus.REMOVED_ITEM)))
           {
               return false;
           }//end if
           
       }//end while
       return true;
       
    }//end method isAllLineItemsRemoved
    
     /* Update the order statuses 
      */
     public void updateOrderScrub(OrderVO order, 
                                          OrderDetailsVO line, 
                                          Connection connection) 
                                                     throws ValidationException
     {
         OrderVO clone = null;
         
         // Update order with system name
         order.setCsrId(ValidationConstants.VALIDATOR_SYS_USER);
     
         // Create a working copy of the order.  This copy is necessary so that
         // in the subsequent code, a single item can be set on the order.  This
         // will prevent the ScrubMapperDAO.updateOrder(order) from resaving the
         // entire order.  Instead it will only update the header and the single
         // line item in question.
         try
         {
             clone = (OrderVO) new ObjectCopyUtil().deepCopy(order);
         }//end try
         catch (Exception e) 
         {
             logger.error("Unable to update order in scrub, due to exception "
                          + "thrown during deepCopy of order.", e);
             throw new ValidationException("Unable to update order.");
         }//end catch

         // Update order with individual item
         if(line != null)
         {
             List singleItemList = new ArrayList();
             try
             {
                 singleItemList.add(
                         (OrderDetailsVO) new ObjectCopyUtil().deepCopy(line));
             }//end try
             catch (Exception e) 
             {
                 logger.error("Unable to update order in scrub, due to exception"
                              + " thrown during deepCopy of line item.", e);
                 throw new ValidationException("Unable to update order.");
             }//end catch
         
             clone.setOrderDetail(singleItemList);
             
         }//end if (line != null)

         // Update scrub database
         try 
         {
             new ScrubMapperDAO(connection).updateOrder(clone);
         }//end try
         catch (Exception e) 
         {
             logger.error("Unable to update order in scrub, due to exception "
                          + "thrown during DAO.updateOrder().", e);
             throw new ValidationException("Unable to update order.");
         }//end catch
         
         
     }//end method updateOrderScrub
     
    /*
     * Determine if the order is of alternate payment type.
     * Returns true if the order has a payment that has a payment_type of 'P'
     * in ftd_apps.payment_methods
     */
    public boolean isAltPayOrder(OrderVO order) throws Exception
    {
        Iterator it = null;
        Collection payments = order.getPayments();
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String paymentTypeAP = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_ALT_PAY");
    
        if(payments != null && payments.size() > 0)
        {
            // Get the total of all gift certificates
            it = payments.iterator();
            PaymentsVO payment = null;
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                if (paymentTypeAP.equals(payment.getPaymentMethodType())) {
                    return true;
                }
            }
        }
        return false;
    }//end method isAltPayOrder    
    
     /*
      * Returns the alt pay payment type description; null if no alt pay payment type.
      */
     public String getAltPayPaymentTypeDesc(OrderVO order) throws Exception
     {
         Iterator it = null;
         Collection payments = order.getPayments();
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         String paymentTypeAP = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_ALT_PAY");
         String paymentTypeDesc = null;
         
         if(payments != null && payments.size() > 0)
         {
             // Get the total of all gift certificates
             it = payments.iterator();
             PaymentsVO payment = null;
             while(it.hasNext())
             {
                 payment = (PaymentsVO)it.next();
                 if (paymentTypeAP.equals(payment.getPaymentMethodType())) {
                    paymentTypeDesc = payment.getPaymentTypeDesc();
                 }
             }
         }
         return paymentTypeDesc;
     }//end method getFirstPaymentType    
     
     /**
   * This method will take a master order number and return if it is a webOe order of not
   * @param masterOrderNumber
   * @param connection
   * @return
   * @throws Exception
   */
   public Boolean isWebOeOrder(String masterOrderNumber, Connection connection)
   throws Exception
   {
     
       logger.info("isWebOeOrder called with master order number: " + masterOrderNumber);    
       boolean isWebOeOrder = false;
  
       String webOeMasterOrderNumberRegex = "M\\d";
       Pattern webOeMasterOrderNumberPattern = Pattern.compile(webOeMasterOrderNumberRegex);
       Matcher webOeMasterOrderNumberMatcher = webOeMasterOrderNumberPattern.matcher(masterOrderNumber);
       if (webOeMasterOrderNumberMatcher.lookingAt()) 
       {
           String isWebOeOrderReturnValue = "N";
           Map paramMap = new HashMap();     
           paramMap.put("IN_MASTER_ORDER_NUMBER", masterOrderNumber);
           
           
           DataRequest dataRequest = new DataRequest();
           dataRequest.setConnection(connection);
           dataRequest.setStatementID("IS_WEBOE_ORDER");
           dataRequest.setInputParams(paramMap);
           
           DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
           isWebOeOrderReturnValue = (String) dataAccessUtil.execute(dataRequest);
           
           if (isWebOeOrderReturnValue.equals("Y"))
           {
             isWebOeOrder = true;
           }
       }     
       logger.info("isWebOeOrder returning: " + isWebOeOrder);
  
       return isWebOeOrder;
   }
         
}
