package com.ftd.osp.ordervalidator.util;
import com.ftd.osp.utilities.dataaccess.valueobjects.*;
import com.ftd.osp.utilities.dataaccess.*;
import org.xml.sax.*;
import java.io.*;
import javax.xml.parsers.*;
import java.sql.*;
import java.util.*;

public class ValidationDataAccessUtil
{
    private Map resultMap;

    public ValidationDataAccessUtil()
    {
        resultMap = new HashMap();
    }

    public Object execute(DataRequest dataRequest) throws SAXException,
           IOException, ParserConfigurationException, SQLException
    {
        Object rs = null;

        //rs = checkCache(dataRequest);

        //if(rs == null)
        //{
            rs = hitDatabase(dataRequest);
        //}

        return rs;
    }

    private Object checkCache(DataRequest dataRequest)
    {
        Object rs = null;
        try
        {
            ValidationDataAccessVO vdaVo = null;
            List resultList = null;
            Map parms = null;
            Iterator parmIt = null;
            Iterator resultIt = null;
            String paramName = null;
            boolean match = true;
            // If the result map contians the statement ID then check for matching input
            // parameters
            if(resultMap.containsKey(dataRequest.getStatementID()))
            {
                // Loop through possible results
                resultList = (List)resultMap.get(dataRequest.getStatementID());
                resultIt = resultList.iterator();
                while(resultIt.hasNext())
                {
                    vdaVo = (ValidationDataAccessVO)resultIt.next();
                    parms = vdaVo.getInputParms();

                    // If both input parms are null then the results will match
                    if(parms == null && dataRequest.getInputParams() == null)
                    {
                        break;
                    }
                    // Else try to find matching parms
                    else
                    {
                        parmIt = parms.keySet().iterator();

                        // Loop through parameters
                        while(parmIt.hasNext())
                        {
                            paramName = (String)parmIt.next();
                            if(dataRequest.getInputParams().get(paramName) != null &&
                               parms.get(paramName) != null &&
                               !dataRequest.getInputParams().get(paramName).equals(parms.get(paramName)))
                            {
                                match = false;
                                break;
                            }
                        }
                    }

                    if(match)
                    {
                        break;
                    }
                }

                if(match)
                {
                    rs = vdaVo.getRs();
                }
            }
        }
        catch(Exception e)
        {
            // all exceptions are caught and we will just hit the database.
        }

        return rs;
    }

    private Object hitDatabase(DataRequest dataRequest) throws SAXException,
           IOException, ParserConfigurationException, SQLException
    {
        DataAccessUtil dau = DataAccessUtil.getInstance();
        Object rs = dau.execute(dataRequest);

        // put into resultMap
        ValidationDataAccessVO vdaVo = new ValidationDataAccessVO();
        vdaVo.setInputParms(dataRequest.getInputParams());
        vdaVo.setRs(rs);

        List resultList = (List)resultMap.get(dataRequest.getStatementID());
        if(resultList == null)
        {
            resultList = new ArrayList();
        }

        resultList.add(vdaVo);
        resultMap.put(dataRequest.getStatementID(), resultList);

        return rs;
    }
}