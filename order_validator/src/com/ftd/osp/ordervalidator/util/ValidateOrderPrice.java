/*
 * @(#) ValidateOrderPrice.java      1.0     2004/08/18
 * 
 * 
 */


package com.ftd.osp.ordervalidator.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordervalidator.exception.ValidationException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * <code>ValidateOrderPrice</code> is the implementation of validation logic
 * specific to Amazon orders.
 * 
 * If any line item on the order meets at least one of the conditions listed
 * below, an order variance record is created in the AMAZON.AZ_PRICE_VARIANCE
 * table.
 * <p>- Difference in product price.
 * <p>- Difference in service fee for Same Day Freshcut, Freshcut, or Floral
 * <p>- Difference in shipping fee for Specialty Gift
 * <p>- Difference in tax amount
 * <p>- Difference in shipping tax
 * 
 * 
 * @author  Robert Larson
 * @version %I%, %G%
 */
/*
 *                          Revision History
 * -----------------------------------------------------------------------------
 * 1.0      2004/08/18  Initial Release.(RFL)
 * 
 */
public class ValidateOrderPrice  
{
    /* **********************************************************************
     *                                                                      *
     *                          Static Variables                            *
     *                                                                      *
     * **********************************************************************/
     
    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */
    private static Logger logger = 
            new Logger("com.ftd.osp.ordervalidator.util.ValidateOrderPrice");
    
    private static final String GET_STATE_MASTER_BY_ID_STATEMENT = 
                                                            "GET_STATE_DETAILS";
    //database field
    private static final int STATE_MASTER_TAX_RATE = 4;
    private static final int DECIMAL_PRECISION = 2;
    private static final int VARIANCE_CEILING_COLUMN_NUMBER = 4;
  
    
    /* **********************************************************************
     *                                                                      *
     *                          Instance Variables                          *
     *                                                                      *
     * **********************************************************************/

    /* Public */
    
    /* Protected */
    
    /* Package */
    
    /* Private */

    
    /**
     * Default constructor for <code>ValidateOrderPrice</code> class.
     */
    public ValidateOrderPrice() 
    {
        super();
    }//end method ValidateOrderPrice()
    
    
    /**
     * Validates an <code>OrderVO</code> with logic specific to Amazon and Mercent order.
     * 
     * @param   order       <code>OrderVO</code> containing a line item to be 
     *                      validated
     * @param   item        <code>OrderDetailsVO</code> to be validated
     * @param   connection  <code>Connection</code> to be used for database
     *                      access
     * @return  boolean     whether a price variance exists.
     */
    public static boolean validate(OrderVO order, 
            OrderDetailsVO line, 
            Connection connection)
	{
	    boolean result = false;
	    MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
	    try {
	    	if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
	    		result = validateMercentOrder(order, line, connection);
	    	} else if( GeneralConstants.ORDER_ORIGIN_AMAZON.equalsIgnoreCase(order.getOrderOrigin())) {
	    		result = validateAmazonOrder(order, line, connection);
	    	} else {
	    		logger.error("Price variance is not needed for partner orders.Not a valid origin to check variance. Not ValidatingOrderPrice.");
	    	}
			
		} catch (Exception e) {
			logger.error("Unable to validate order price. Error:"+e.getMessage(), e);
		}
	    return result;
	}
    
    private static boolean validateAmazonOrder(OrderVO order, 
                                OrderDetailsVO line, 
                                Connection connection) 
                                                    throws ValidationException,
                                                           Exception
    {
        OrderVO clone = null;
        boolean variance = false;
        OrderDetailsVO cloneLine = null;
        BigDecimal ceiling = new BigDecimal("0");
        BigDecimal cloneSalesTax = null;
        BigDecimal cloneShippingTax = null;
        
        if (order == null) 
        {
            throw new ValidationException("No order provided for order price validation.");
        }
        if (line == null)
        {
            throw new ValidationException("No line item provided for order price validation.");
        }
        
        // Clone the original order
        try {
            clone = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
        } catch (Exception e) {
            logger.error(e);
            throw new ValidationException("Unable to deep copy order");
        }
        
        // START set amazon payment amount - Per Phase 3 defects 1612
        logger.debug("*** PREPARING TO SET AMAZON PAYMENT AMOUNT");
        if(order.getPayments().get(0) != null)
        {
            logger.info("*** SETTING AMAZON PAYMENT AMOUNT TO:" + order.getOrderTotal());
            PaymentsVO payment = (PaymentsVO) order.getPayments().get(0);
            payment.setAmount(order.getOrderTotal() != null?order.getOrderTotal():"0");
            
            if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
            {
                Iterator amazonDetailIterator = order.getOrderDetail().iterator();
                while(amazonDetailIterator.hasNext())
                {
                    OrderDetailsVO amazonOrderDetail = (OrderDetailsVO) amazonDetailIterator.next();
                    logger.info("*** AMAZON: " + amazonOrderDetail.getDiscountedProductPrice());
                    
                    BigDecimal discountedPrice = (new BigDecimal(amazonOrderDetail.getProductsAmount())).subtract(new BigDecimal(amazonOrderDetail.getDiscountAmount()));
                    logger.info("*** AMAZON: " + discountedPrice);
                    logger.info("*** AMAZON: " + amazonOrderDetail.getProductsAmount());
                    logger.info("*** AMAZON: " + amazonOrderDetail.getDiscountAmount());
                    
                    amazonOrderDetail.setDiscountedProductPrice(discountedPrice.setScale(2,5).toString());
                    
                    logger.info("*** AMAZON: SETTING DISCOUNTED PRODUCT PRICE AMOUNT TO:" + discountedPrice);
                }
            }
        }
        // END set amazon payment amount - Per Phase 3 defects 1612
    
        String defaultRecalcSourceCode = ConfigurationUtil.getInstance()
				.getFrpGlobalParm("AMAZON_CONFIG", "default_recalculate_source_code");
		logger.info("defaultRecalcSourceCode: "+defaultRecalcSourceCode);
		clone.setSourceCode(defaultRecalcSourceCode);
		
		List orderDetails = clone.getOrderDetail();
		for (int i = 0; i < orderDetails.size(); i++) 
		{
			//if sourceCode is in IOTW set IOTW source code or set to null
			OrderDetailsVO orderDetail = (OrderDetailsVO) orderDetails.get(i);
			String productId = orderDetail.getProductId();
			CachedResultSet cr = getJoeIOTW(connection, defaultRecalcSourceCode, productId);
			if (cr.next())
			{
			  String iotwSourceCode = cr.getString("iotw_source_code");
			  orderDetail.setSourceCode(iotwSourceCode);
			  logger.info("IOTW SourceCode :"+iotwSourceCode);
			} else {
				orderDetail.setSourceCode(defaultRecalcSourceCode);
				logger.info("Not an IOTW SourceCode");
			}
		}
        // Recalculate the cloned order and set payment amount
        try {
			new RecalculateOrderBO().recalculate(connection, clone);
		} catch (Exception e) {
			logger.error(e);
            throw new ValidationException("Error occured while Recalculating Order :"+e.getMessage());
		}

        cloneLine = getCloneLine(clone, line.getExternalOrderNumber());
        
        if (cloneLine == null) 
        {
            logger.error("Unable to locate line item in clone.");
            throw new ValidationException("Unable to validate order.  Clone failure.");
        }
        
        //Compare product prices
        if (isVariance(line.getProductsAmount(),
                       cloneLine.getProductsAmount(),
                       ceiling))
        {
            variance = true;
            logger.info("variance by ProductsAmount");
        }
        
        BigDecimal ftdShippingFee = formatToBigDecimal(cloneLine.getShippingFeeAmount());
        BigDecimal ftdServiceFee = formatToBigDecimal(cloneLine.getServiceFeeAmount());
        String ftdShipingFeeVal = String.valueOf(ftdShippingFee.add(ftdServiceFee));
        
        BigDecimal amzShippingFee = formatToBigDecimal(line.getShippingFeeAmount());
        BigDecimal amzServiceFee = formatToBigDecimal(line.getServiceFeeAmount());
        String amzShipingFeeVal = String.valueOf(amzShippingFee.add(amzServiceFee));
        
        if (isVariance(amzShipingFeeVal, ftdShipingFeeVal, ceiling))
		{
		    variance = true;
		    logger.info("variance by ShippingFeeAmount/ServiceFeeAmount");
		}
        
        //compare sales tax
        cloneSalesTax = getFTDTax(cloneLine.getProductsAmount(),
                                             getRecipientAddress(line),
                                             connection);
        cloneSalesTax = cloneSalesTax.setScale(DECIMAL_PRECISION, 
                                               BigDecimal.ROUND_DOWN);
        if (isVariance(line.getTaxAmount(), cloneSalesTax, ceiling))
        {
            variance = true;
            logger.info("variance by TaxAmount");
        }
                    
        //compare shipping taxes
        cloneShippingTax = getFTDTax(cloneLine.getShippingFeeAmount(),
                                            getRecipientAddress(line),
                                            connection);
        cloneShippingTax = cloneShippingTax.setScale(DECIMAL_PRECISION, 
                                                     BigDecimal.ROUND_DOWN);
        if (isVariance(line.getShippingTax(), cloneShippingTax, ceiling))
        {
            variance = true;
            logger.info("variance by ShippingTax");
        }
                    
        //generate variance if necessary
        logger.info("variance = "+variance);
        if (variance) 
        {
            createVariance(line.getExternalOrderNumber(),
                           line.getProductId(),
                           cloneLine.getProductsAmount(),
                           cloneLine.getShippingFeeAmount(),
                           cloneSalesTax.toString(),
                           cloneShippingTax.toString(),
                           cloneLine.getServiceFeeAmount(),
                           line.getProductsAmount(),
                           line.getShippingFeeAmount(),
                           line.getTaxAmount(),
                           line.getShippingTax(),
                           line.getServiceFeeAmount(),
                           connection);
        }
        
        return variance;
        
    }
    
    
    /*
     * 
     */
    private static void createVariance(String orderNumber,
                                       String productID,
                                       String FTDProductPrice,
                                       String FTDShippingCost,
                                       String FTDTax,
                                       String FTDShippingTax,
                                       String FTDServiceFee,
                                       String AmazonProductPrice,
                                       String AmazonShippingCost,
                                       String AmazonTax,
                                       String AmazonShippingTax,
                                       String AmazonServiceFee,
                                       Connection connection) 
                                                    throws ValidationException, Exception
    {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dataAccessUtil = null;
        Map outputParameters = null;
        
        //Prepare datastore call
        DatabaseMetaData d = connection.getMetaData();
        logger.info("url: " + d.getURL());
        
        logger.info("Inserting Price Varience for Confirmation Number :"+orderNumber);
        dataRequest.setConnection(connection);
        dataRequest.reset();
        dataRequest.setStatementID("INSERT_PRICE_VARIANCE");
        dataRequest.addInputParam("IN_CONFIRMATION_NUMBER", orderNumber);
        dataRequest.addInputParam("IN_FTD_PRODUCT_AMT", (FTDProductPrice == null ? "0.00" : FTDProductPrice));
        
        BigDecimal ftdShippingFee = formatToBigDecimal(FTDShippingCost);
        BigDecimal ftdServiceFee = formatToBigDecimal(FTDServiceFee);
        
        dataRequest.addInputParam("IN_FTD_SHIPPING_FEE_AMT", ftdShippingFee.add(ftdServiceFee).toString());
        dataRequest.addInputParam("IN_FTD_TAX_AMT",  (FTDTax == null ? "0.00" : FTDTax));
        dataRequest.addInputParam("IN_FTD_SHIPPING_TAX_AMT", (FTDShippingTax == null ? "0.00" : FTDShippingTax));
        dataRequest.addInputParam("IN_AZ_PRODUCT_AMT",  (AmazonProductPrice == null ? "0.00" : AmazonProductPrice));
        
        BigDecimal amzShippingFee = formatToBigDecimal(AmazonShippingCost);
        BigDecimal amzServiceFee = formatToBigDecimal(AmazonServiceFee);
        //dataRequest.addInputParam("IN_AZ_SHIPPING_FEE_AMT",  (AmazonShippingCost == null ? "0.00" : AmazonShippingCost));
        dataRequest.addInputParam("IN_AZ_SHIPPING_FEE_AMT",  (amzShippingFee.add(amzServiceFee).toString()));
        dataRequest.addInputParam("IN_AZ_TAX_AMT",  
                    (AmazonTax == null ? "0.00" : AmazonTax));
        dataRequest.addInputParam("IN_AZ_SHIPPING_TAX_AMT",  
                    (AmazonShippingTax == null ? "0.00" : AmazonShippingTax));
        dataRequest.addInputParam("IN_CREATED_BY",  "SYSTEM");
        dataRequest.addInputParam("IN_UPDATED_BY",  "SYSTEM");
        
        // Make datastore call
        try 
        {
            dataAccessUtil = getDataAccessUtil();
        }
        catch (ValidationException ve) 
        {
            logger.error("Unable to get DataAccessUtil for create variance.", ve);
            throw ve;
        }
        
        try
        {
            outputParameters = (Map) dataAccessUtil.execute(dataRequest);
        }
        catch (IOException ioe) 
        {
            logger.error("exception thrown while executing query.", ioe);
            throw new ValidationException("Unable to access data.");
        }
        catch (ParserConfigurationException pce)
        {
            logger.error("exception thrown while executing query.", pce);
            throw new ValidationException("Unable to access data.");
        }
        catch (SQLException se) 
        {
            logger.error("exception thrown while executing query.", se);
            throw new ValidationException("Unable to access data.");
        }
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during variance insert.");
            throw new ValidationException("Failure validating order price.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.info("status:: " + status);
            logger.info("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure reviewing status of variance insert "
                                 + "- " + message);
                    throw new ValidationException(message);
                    
                }//end if
                
            }//end if
            
        }//end else
        
    }//end method createVariance()
    
    
    /* Return native double from String object
     * @param String
     * @return double value
     * returns zero if String is null
     */
    private static BigDecimal formatToBigDecimal(String value)
    {
      return (value == null || value.trim().length()== 0) ? new BigDecimal(0) : new BigDecimal(value);
    }//end method


    /*
     * 
     */
    private static OrderDetailsVO getCloneLine(OrderVO clone, 
                                               String externalOrderNumber) 
    {
        OrderDetailsVO line = null;
        
        if (clone == null) 
        {
            return null;
        }//end if
        
        Iterator i = clone.getOrderDetail().iterator();
        while (i.hasNext()) 
        {
            line = (OrderDetailsVO)i.next();
            if (line.getExternalOrderNumber().equals(externalOrderNumber))
            {
                break;
            }
        }//end while
        
        return line;
        
    }//end method
    
    
    /*
     * 
     */
    private static DataAccessUtil getDataAccessUtil() throws ValidationException
    {
        DataAccessUtil dataAccessUtil = null;
        
        try 
        {
            dataAccessUtil = DataAccessUtil.getInstance();
        }//end try
        catch (IOException ioe) 
        {
            logger.error("exception thrown getting instance of "
                         + "DataAccessUtil.", ioe);
            throw new ValidationException("Unable to use Data Access "
                                          + "Utility.");
        }
        catch (ParserConfigurationException pce)
        {
            logger.error("exception thrown getting instance of "
                         + "DataAccessUtil.", pce);
            throw new ValidationException("Unable to use Data Access "
                                          + "Utility.");
        }
        catch (SAXException se)
        {
            logger.error("exception thrown getting instance of "
                         + "DataAccessUtil.", se);
            throw new ValidationException("Unable to use Data Access "
                                          + "Utility.");
        }
        
        if (dataAccessUtil == null) 
        {
            logger.error("DataAccessUtil not created.");
            throw new ValidationException("Unable to access data.");
        }
        
        return dataAccessUtil;
        
    }//end method

    
    /*
     * 
     */
    private static BigDecimal getFTDTax(String taxableField,
                                        RecipientAddressesVO address,
                                        Connection connection) 
                                                    throws ValidationException
    {
        BigDecimal tax = null;
        
        if (taxableField != null && !taxableField.equals(""))
        {
            try 
            {
                tax = new BigDecimal(taxableField);
            }//end try
            catch (NumberFormatException nfe) 
            {
                logger.error("Non-numeric value(" + taxableField + ")", nfe);
                throw new ValidationException("Non-numeric value in numeric "
                                              + "field.");
            }//end catch
        }//end if
        else 
        {
            return new BigDecimal(0);
        }//end else
        
        try
        {
            return tax.multiply(getTaxPercentage(address, connection));
        }//end try
        catch (ValidationException ve)
        {
            logger.error("unable to get tax percentage", ve);
            throw ve;
        }//end catch
            
    }//end method getFTDTax()
    
    
    /* This method returns a recipient's address object.  If one does not
     * exist null is returned.
     * @param OrderDetailsVO order line
     * @RecipientAddressVO
     */
    private static RecipientAddressesVO 
                                    getRecipientAddress(OrderDetailsVO detailVO)
    {
        RecipientAddressesVO address = null;
        
        List recipientList = detailVO.getRecipients();
        if (recipientList != null && recipientList.size() > 0 )
        {
            RecipientsVO recipient = (RecipientsVO)recipientList.get(0);
            if (recipient != null)
            {
                List addressList = recipient.getRecipientAddresses();
                if (addressList != null && addressList.size() > 0)
                {
                    address = (RecipientAddressesVO)addressList.get(0);
                }//end if ()
                
           }//end if (recipient != null)
           
        }//end if (recipientList != null & > 0)
    
        return address;
        
    }//end method
    
    
    /*
     * 
     */
    private static BigDecimal getTaxPercentage(RecipientAddressesVO address,
                                               Connection connection)
                                                    throws ValidationException
    {
        if(address == null)
        {
            return new BigDecimal(0);
        }
        String state = address.getStateProvince();
        
        //if there is not state (not US) exist
        if(state == null || state.length() <= 0)
        {
            return new BigDecimal(0);
        }
        
        //get tax percent for state
        BigDecimal taxPercent = new BigDecimal(0); // getTaxRate(state, connection);
        if(taxPercent == null)
        {
            return new BigDecimal(0);
        }
        
        //convert percent to decimal
        BigDecimal hundreths = new BigDecimal(0.01);
        return taxPercent.multiply(hundreths);
        
    }//end method getTaxPercentage()
    
    
    
    /* This method retrieves the tax rate for a give state.
     * @param String state code
     * @return BigDecimal tax rate
     */
/*
    private static BigDecimal getTaxRate(String state, Connection connection)
                                                    throws ValidationException
    {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dataAccessUtil = null;
        CachedResultSet rs = null;
        BigDecimal taxRate = new BigDecimal(0);
        
        // Get the order header information
        // get service fee from SNH table
        StateMasterHandler statesHandler = 
            (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");
        if(statesHandler != null)  
        {
            StateMasterVO stateVO = statesHandler.getStateById(state);
            if(stateVO == null)
            {
                String msg = "State code not found in database.[" + state 
                             + "] (cache)";
                logger.error(msg);
                throw new ValidationException(msg);
                
            }//end if () 
            taxRate = stateVO.getTaxRate();
            
        }//end if (statesHandler != null)
        else
        {
            dataRequest.setConnection(connection);
            dataRequest.reset();
            dataRequest.setStatementID(GET_STATE_MASTER_BY_ID_STATEMENT);
            dataRequest.addInputParam("IN_STATE_MASTER_ID",state);
            
            try 
            {
                dataAccessUtil = getDataAccessUtil();
            }//end try
            catch (ValidationException ve) 
            {
                logger.error("Unable to get ataAccessUtil instance for TaxRate",
                             ve);
                throw ve;
            }
            
            try
            {
                rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
            }//end try
            catch (IOException ioe) 
            {
                logger.error("exception thrown while executing query.", ioe);
                throw new ValidationException("Unable to access data.");
            }
            catch (ParserConfigurationException pce)
            {
                logger.error("exception thrown while executing query.", pce);
                throw new ValidationException("Unable to access data.");
            }
            catch (SQLException se) 
            {
                logger.error("exception thrown while executing query.", se);
                throw new ValidationException("Unable to access data.");
            }
                    
            if(rs == null || !rs.next())
            {
                String msg = "State code not found in database.[" + state + "]";
                logger.error(msg);
                throw new ValidationException(msg);
                
            }//end if
            
            taxRate = formatToBigDecimal(
                    getValue(rs.getObject(STATE_MASTER_TAX_RATE)));
                    
        }//end else (statesHandler == null)
        
        return taxRate;
        
    }//end method
*/    

    /* This method takes in an object and returns
     * it's string value.
     * @param Object
     * @return String value of object
     * If object is null, null is returned
     */
    private static String getValue(Object obj) 
    {
        String value = null;
    
        if(obj == null)
        {
          return null;
        }
        
        if(obj instanceof BigDecimal) 
        {
            value = obj.toString();      
        }
        else if(obj instanceof String) 
        {
            value = (String)obj;
        }
        else
        {
            value = obj.toString();
        }
        
        return value;
        
    }//end method
    
    
    /*
     * 
     */
    private static boolean isVariance(String original, 
                                      BigDecimal clone, 
                                      BigDecimal ceiling) 
                                                    throws ValidationException
    {
        BigDecimal first = null;
        
        if (original == null) 
        {
            first = new BigDecimal(0);
        }//end if
        else
        {
            //Convert original to number, throw exception
            try 
            {
                first = new BigDecimal(original);
            }//end try
            catch (NumberFormatException nfe) 
            {
                logger.error("Original value(" + original + ") is nonNumeric.", 
                             nfe);
                throw new ValidationException("Non-numeric value in numeric.");
            }//end catch (NumberFormatException)
        }//end else
        
        //Determine variance
        return (first.add(clone.negate()).abs().compareTo(ceiling) == 1);
        
    }//end method isVariance()
    
    
    /*
     * Assume nulls are equal to 0
     */
    private static boolean isVariance(String original, 
                                      String clone, 
                                      BigDecimal ceiling) 
                                                    throws ValidationException
    {
    	logger.info("original:"+original+" clone:"+clone+", ceiling:"+ceiling);
        BigDecimal second = null;
        
        if (clone == null) 
        {
            second = new BigDecimal(0);
        }//end if
        else
        {
            //Convert clone to number, throw exception
            try 
            {
                second = new BigDecimal(clone);
            }//end try
            catch (NumberFormatException nfe) 
            {
                logger.error("Clone value(" + clone + ") is nonNumeric.", 
                             nfe);
                throw new ValidationException("Non-numeric value in numeric.");
            }//end catch (NumberFormatException)
        }//end else
        
        //Determine variance
        return isVariance(original, second, ceiling);
        
    }//end method isVariance()
    
    private static CachedResultSet getJoeIOTW(Connection conn, String sourceCode, String productId) throws Exception {
        logger.debug("getJoeIOTW(" + sourceCode + ", " + productId + ")");

        /* build DataRequest object */
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JOE_IOTW");
        Map inputParams = new HashMap();
        inputParams.put("IN_SOURCE_CODE", sourceCode);
        inputParams.put("IN_PRODUCT_ID", productId);
        dataRequest.setInputParams(inputParams);

        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        CachedResultSet cr = (CachedResultSet) dataAccessUtil.execute(dataRequest);

        return cr;
    }
    
    
    private static boolean validateMercentOrder(OrderVO order, 
            OrderDetailsVO line, 
            Connection connection) 
                                throws ValidationException,
                                       Exception
	{
		OrderVO clone = null;
		boolean variance = false;
		OrderDetailsVO cloneLine = null;
		BigDecimal ceiling = new BigDecimal("0");
		BigDecimal cloneSalesTax = null;
		BigDecimal cloneShippingTax = null;
	
		if (order == null) 
		{
		throw new ValidationException("No order provided for order price validation.");
		}
		if (line == null)
		{
		throw new ValidationException("No line item provided for order price validation.");
		}
	
		// Clone the original order
		try {
		clone = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
		} catch (Exception e) {
		logger.error(e);
		throw new ValidationException("Unable to deep copy order");
		}
	
		// START set MERCENT payment amount - Per Phase 3 defects 1612
		logger.debug("*** PREPARING TO SET MERCENT PAYMENT AMOUNT");
		if(order.getPayments().get(0) != null)
		{
			logger.info("*** SETTING MERCENT PAYMENT AMOUNT TO:" + order.getOrderTotal());
			PaymentsVO payment = (PaymentsVO) order.getPayments().get(0);
			payment.setAmount(order.getOrderTotal() != null?order.getOrderTotal():"0");
			
			if(order.getOrderDetail() != null && order.getOrderDetail().size() > 0)
			{
				Iterator mercentDetailIterator = order.getOrderDetail().iterator();
				while(mercentDetailIterator.hasNext())
				{
				OrderDetailsVO mercentOrderDetail = (OrderDetailsVO) mercentDetailIterator.next();
				logger.info("*** MERCENT Discounted Product Price : " + mercentOrderDetail.getDiscountedProductPrice());
				
				BigDecimal discountedPrice = (new BigDecimal(mercentOrderDetail.getProductsAmount())).subtract(new BigDecimal(mercentOrderDetail.getDiscountAmount()));
				logger.info("*** MERCENT Discounted price : " + discountedPrice);
				logger.info("*** MERCENT Product Amount: " + mercentOrderDetail.getProductsAmount());
				logger.info("*** MERCENT Discount Amount: " + mercentOrderDetail.getDiscountAmount());
				
				mercentOrderDetail.setDiscountedProductPrice(discountedPrice.setScale(2,5).toString());
				
				logger.info("*** MERCENT: SETTING DISCOUNTED PRODUCT PRICE AMOUNT TO:" + discountedPrice);
				}
			}
		}
		
		MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);				
		String defaultRecalcSourceCode = mercentOrderPrefixes.getDefaultRecalcSourceCodeForChannel(order.getOrderOrigin());
		logger.info("defaultRecalcSourceCode: "+defaultRecalcSourceCode);
		clone.setSourceCode(defaultRecalcSourceCode);
		
		List orderDetails = clone.getOrderDetail();
		for (int i = 0; i < orderDetails.size(); i++) 
		{
			//if sourceCode is in IOTW set IOTW source code or set to null
			OrderDetailsVO orderDetail = (OrderDetailsVO) orderDetails.get(i);
			String productId = orderDetail.getProductId();
			CachedResultSet cr = getJoeIOTW(connection, defaultRecalcSourceCode, productId);
			if (cr.next())
			{
				String iotwSourceCode = cr.getString("iotw_source_code");
				orderDetail.setSourceCode(iotwSourceCode);
				logger.info("IOTW SourceCode :"+iotwSourceCode);
			} else {
				orderDetail.setSourceCode(defaultRecalcSourceCode);
				logger.info("Not an IOTW SourceCode");
			}
		}
		// Recalculate the cloned order and set payment amount
		try {
			new RecalculateOrderBO().recalculate(connection, clone);
		} catch (Exception e) {
			logger.error(e);
			throw new ValidationException("Error occured while Recalculating Order :"+e.getMessage());
		}
	
		cloneLine = getCloneLine(clone, line.getExternalOrderNumber());
	
		if (cloneLine == null) 
		{
			logger.error("Unable to locate line item in clone.");
			throw new ValidationException("Unable to validate order.  Clone failure.");
		}
	
		//Compare product prices
		if (isVariance(line.getProductsAmount(), cloneLine.getProductsAmount(), ceiling))
		{
			variance = true;
			logger.info("variance by ProductsAmount");
		}
		
		BigDecimal ftdShippingFee = formatToBigDecimal(cloneLine.getShippingFeeAmount());
		BigDecimal ftdServiceFee = formatToBigDecimal(cloneLine.getServiceFeeAmount());
		String ftdShipingFeeVal = String.valueOf(ftdShippingFee.add(ftdServiceFee));
		
		BigDecimal mrcntShippingFee = formatToBigDecimal(line.getShippingFeeAmount());
		BigDecimal mrcntServiceFee = formatToBigDecimal(line.getServiceFeeAmount());
		String mrcntShipingFeeVal = String.valueOf(mrcntShippingFee.add(mrcntServiceFee));
		
		if (isVariance(mrcntShipingFeeVal, ftdShipingFeeVal, ceiling))
		{
			variance = true;
			logger.info("variance by ShippingFeeAmount/ServiceFeeAmount");
		}
		
		//compare sales tax
		cloneSalesTax = getFTDTax(cloneLine.getProductsAmount(), getRecipientAddress(line), connection);
		cloneSalesTax = cloneSalesTax.setScale(DECIMAL_PRECISION, BigDecimal.ROUND_DOWN);
		if (isVariance(line.getTaxAmount(), cloneSalesTax, ceiling))
		{
		variance = true;
		logger.info("variance by TaxAmount");
		}
		
		//compare shipping taxes
		cloneShippingTax = getFTDTax(cloneLine.getShippingFeeAmount(), getRecipientAddress(line), connection);
		cloneShippingTax = cloneShippingTax.setScale(DECIMAL_PRECISION, BigDecimal.ROUND_DOWN);
		if (isVariance(line.getShippingTax(), cloneShippingTax, ceiling))
		{
			variance = true;
			logger.info("variance by ShippingTax");
		}
		
		//generate variance if necessary
		logger.info("variance = "+variance);
		if (variance) 
		{
			createMercentPriceVariance(line.getExternalOrderNumber(), line.getProductId(), cloneLine.getProductsAmount(),
		       cloneLine.getShippingFeeAmount(), cloneSalesTax.toString(), cloneShippingTax.toString(),
		       cloneLine.getServiceFeeAmount(), line.getProductsAmount(), line.getShippingFeeAmount(),
		       line.getTaxAmount(), line.getShippingTax(), line.getServiceFeeAmount(), connection);
		}
		
		return variance;
	
	}
    /*
     * 
     */
    private static void createMercentPriceVariance(
    	 String orderNumber, String productID, String FTDProductPrice, String FTDShippingCost, String FTDTax,
    	 String FTDShippingTax, String FTDServiceFee, String mrcntProductPrice, String mrcntShippingCost,
    	 String mrcntTax, String mrcntShippingTax, String mrcntServiceFee, Connection connection)
    throws ValidationException, Exception
    {
        DataRequest dataRequest = new DataRequest();
        DataAccessUtil dataAccessUtil = null;
        Map outputParameters = null;
        
        //Prepare datastore call
        DatabaseMetaData d = connection.getMetaData();
        logger.info("url: " + d.getURL());
        
        logger.info("Inserting Mercent Price Varience for Confirmation Number : "+orderNumber);
        dataRequest.setConnection(connection);
        dataRequest.reset();
        dataRequest.setStatementID("INSERT_MERCENT_PRICE_VARIANCE");
        dataRequest.addInputParam("IN_CONFIRMATION_NUMBER", orderNumber);
        dataRequest.addInputParam("IN_FTD_PRODUCT_AMT", (FTDProductPrice == null ? "0.00" : FTDProductPrice));
        
        BigDecimal ftdShippingFee = formatToBigDecimal(FTDShippingCost);
        BigDecimal ftdServiceFee = formatToBigDecimal(FTDServiceFee);
        
        dataRequest.addInputParam("IN_FTD_SHIPPING_FEE_AMT", ftdShippingFee.add(ftdServiceFee).toString());
        dataRequest.addInputParam("IN_FTD_TAX_AMT",  (FTDTax == null ? "0.00" : FTDTax));
        dataRequest.addInputParam("IN_FTD_SHIPPING_TAX_AMT", (FTDShippingTax == null ? "0.00" : FTDShippingTax));
        dataRequest.addInputParam("IN_MRCNT_PRODUCT_AMT",  (mrcntProductPrice == null ? "0.00" : mrcntProductPrice));
        
        BigDecimal mrcntShippingFee = formatToBigDecimal(mrcntShippingCost);
        BigDecimal mrcntServiceFeeFormatted = formatToBigDecimal(mrcntServiceFee);
        
        dataRequest.addInputParam("IN_MRCNT_SHIPPING_FEE_AMT",  (mrcntShippingFee.add(mrcntServiceFeeFormatted).toString()));
        dataRequest.addInputParam("IN_MRCNT_TAX_AMT",           (mrcntTax == null ? "0.00" : mrcntTax));
        dataRequest.addInputParam("IN_MRCNT_SHIPPING_TAX_AMT",  (mrcntShippingTax == null ? "0.00" : mrcntShippingTax));
        dataRequest.addInputParam("IN_CREATED_BY",  "SYSTEM");
        dataRequest.addInputParam("IN_UPDATED_BY",  "SYSTEM");
        
        // Make datastore call
        try 
        {
            dataAccessUtil = getDataAccessUtil();
        }
        catch (ValidationException ve) 
        {
            logger.error("Unable to get DataAccessUtil for create variance.", ve);
            throw ve;
        }
        
        try
        {
            outputParameters = (Map) dataAccessUtil.execute(dataRequest);
        }
        catch (IOException ioe) 
        {
            logger.error("exception thrown while executing query.", ioe);
            throw new ValidationException("Unable to access data.");
        }
        catch (ParserConfigurationException pce)
        {
            logger.error("exception thrown while executing query.", pce);
            throw new ValidationException("Unable to access data.");
        }
        catch (SQLException se) 
        {
            logger.error("exception thrown while executing query.", se);
            throw new ValidationException("Unable to access data.");
        }
        
        if (outputParameters == null) 
        {
            logger.error("missing output parameters during variance insert.");
            throw new ValidationException("Failure validating order price.");
            
        }//end if 
        else
        {
            String status = (String) outputParameters.get("OUT_STATUS");
            String message = (String) outputParameters.get("OUT_MESSAGE");
            logger.info("status:: " + status);
            logger.info("message:: " + message);
            if (status != null) 
            {
                status = status.trim();
                if (status.equalsIgnoreCase("N"))  
                {
                    logger.error("Failure reviewing status of variance insert "
                                 + "- " + message);
                    throw new ValidationException(message);                    
                }              
            }           
        }        
    }//end method createMercentPriceVariance()
    
}//end class ValidateOrderPrice