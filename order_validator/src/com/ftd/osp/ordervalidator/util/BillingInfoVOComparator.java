package com.ftd.osp.ordervalidator.util;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.BillingInfoVO;

import java.util.Comparator;

  public class BillingInfoVOComparator implements Comparator
  {
    /**
     * Compares its two arguments for order. Returns a negative integer, zero, 
     * or a positive integer as the first argument is less than, equal to, 
     * or greater than the second.
     */
    public int compare(Object o1, Object o2)
    {
      BillingInfoVO billingInfoVO1 = (BillingInfoVO) o1;
      BillingInfoVO billingInfoVO2 = (BillingInfoVO) o2;

      long infoSequence1 = Long.parseLong(billingInfoVO1.getInfoSequence());
      long infoSequence2 = Long.parseLong(billingInfoVO2.getInfoSequence());

      if (infoSequence1 < infoSequence2 ) 
      {
          return -1;
      }
      else if (infoSequence1 == infoSequence2)
      {
        return 0;
      }
      else // (infoSequence1 > infoSequence2) implied
      {
        return 1;
      }
    }

    /**
     * Indicates whether some other object is "equal to" this Comparator. 
     */
    public boolean equals(Object obj)
    {
      return false;
    }
  }