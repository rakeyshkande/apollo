package com.ftd.osp.ordervalidator.util;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * CommonUtils for OraderValidator.
 * @author skatam
 */
public class CommonUtils 
{

	public CommonUtils(){    
	}
	static Logger logger =  new Logger(ValidationConstants.LOGGER_CATEGORY_COMMONUTILS);
	
	/** 
	 * Get database connection
	 */
	public static Connection getConnection() throws Exception {
		return getConnection("DATABASE_CONNECTION");
	}
	
	
	/**
	 * @param connectionName
	 * @return Connection
	 * @throws Exception
	 */
	public static Connection getConnection(String connectionName) throws Exception {
		ConfigurationUtil config = ConfigurationUtil.getInstance();
		String dbConnection = config.getProperty(
				ValidationConstants.PROPERTY_FILE, connectionName);

		// get DB connection
		DataSource dataSource = (DataSource) lookupResource(dbConnection);
		return dataSource.getConnection();
	}  

	/**
	 * @return DataSource
	 * @throws Exception
	 */
	public static DataSource getDataSource() throws Exception {
		ConfigurationUtil config = ConfigurationUtil.getInstance();
		String dbConnection = config.getProperty(
				ValidationConstants.PROPERTY_FILE, "DATABASE_CONNECTION");

		// get DB connection
		DataSource dataSource = (DataSource) lookupResource(dbConnection);
		return dataSource;

	}
    
	/**
	 * This method sends a message to the System Messenger.
	 * 
	 * @param String message
	 * @returns String message id
	 */
	public static String sendSystemMessage(String message) throws Exception {
		logger.error("Sending System Message:" + message);

		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		String messageSource = configUtil.getProperty(
				ValidationConstants.PROPERTY_FILE, "MESSAGE_SOURCE");

		String messageID = "";

		// build system vo
		SystemMessengerVO sysMessage = new SystemMessengerVO();
		sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
		sysMessage.setSource(messageSource);
		sysMessage.setType("ERROR");
		sysMessage.setMessage(message);

		SystemMessenger sysMessenger = SystemMessenger.getInstance();
		Connection conn = getConnection();
		try {
			messageID = sysMessenger.send(sysMessage, conn);

			if (messageID == null) {
				String msg = "Error occured while attempting to send out a system message. Msg not sent: "
						+ message;
				logger.error(msg);
			}
		} finally {
			conn.close();
		}

		return messageID;
	}

	/**
	 * Returns a transactional resource from the EJB container.
	 * 
	 * @param jndiName
	 * @return object
	 * @throws javax.naming.NamingException
	 */
	public static Object lookupResource(String jndiName) throws NamingException {
		InitialContext initContext = null;
		try {
			initContext = new InitialContext();
			return initContext.lookup(jndiName);
		} finally {
			try {
				initContext.close();
			} catch (Exception ex) {

			} finally {
			}
		}
	}
}