package com.ftd.osp.ordervalidator.util;

import java.util.Comparator;

import com.ftd.osp.ordervalidator.vo.ValidationVO;

public class ValidationListComparator implements Comparator
{
    public ValidationListComparator()
    {
    }

    public int compare(Object obj1, Object obj2)
    {
        int ret = 0;
        int int1 = Integer.valueOf(((ValidationVO)obj1).getExecutionOrder()).intValue();
        int int2 = Integer.valueOf(((ValidationVO)obj2).getExecutionOrder()).intValue();

        if(int1 < int2)
        {
            ret = -1;
        }
        else if(int1 > int2)
        {
            ret = 1;
        }

        return ret;
    }

    public boolean equals(Object obj1)
    {
        return false;
    }
}