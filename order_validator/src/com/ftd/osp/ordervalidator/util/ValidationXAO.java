package com.ftd.osp.ordervalidator.util;

import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.util.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ValidationXAO
{
    private static final String EMPTY = "";
    public ValidationXAO()
    {
    }

    public Document generateValidationXML(OrderValidationNode orderNode) throws Exception
    {
        Document validationDocument = DOMUtil.getDefaultDocument();

        // Create validation root
        Element validationElement = validationDocument.createElement("validation");
        validationDocument.appendChild(validationElement);

        if(orderNode != null)
        {
            // Create order root
            Element orderElement = validationDocument.createElement("order");
            orderElement.setAttribute(OrderValidationNode.STATUS, orderNode.getAttribute(OrderValidationNode.STATUS));
            validationElement.appendChild(orderElement);

            // Add header validation to the document
            Element headerElement = validationDocument.createElement("header");
            ValidationNode headerNode = (ValidationNode) orderNode.getChildNodes("header").toArray()[0];
            if(headerNode.getChildNodes() != null && headerNode.getChildNodes().size() > 0)
            {
                headerElement.setAttribute(OrderValidationNode.STATUS, headerNode.getAttribute(OrderValidationNode.STATUS));
                orderElement.appendChild(headerElement);

                ValidationNode headerDetailNode = null;
                List headerDetailNodeList = headerNode.getChildNodes();
                Iterator headerDetailNodeIterator = headerDetailNodeList.iterator();
                while(headerDetailNodeIterator.hasNext())
                {
                    headerDetailNode = (ValidationNode) headerDetailNodeIterator.next();

                    if(headerDetailNode.getAttribute(OrderValidationNode.STATUS) != null && (headerDetailNode.getAttribute(OrderValidationNode.STATUS).equals(OrderValidationNode.ERROR) || headerDetailNode.getAttribute(OrderValidationNode.STATUS).equals(OrderValidationNode.WARNING)))
                    {
                        headerElement.appendChild(this.processNodeMessages(headerDetailNode, validationDocument, EMPTY));
                    }
                }
            }

            // Add item validation to the document
            Element itemsElement = null;
            itemsElement = validationDocument.createElement("items");
    
            if(orderNode.getChildNodes("items") != null && orderNode.getChildNodes("items").size() > 0)
            {                                                
                List itemDetailList = null;
                List addOnDetailList = null;
                List lineNumberNodeList = null;
                Iterator itemDetailIterator = null;
                Iterator addOnDetailIterator = null;

                ValidationNode itemNode = null;
                ValidationNode itemDetailNode = null;
                ValidationNode addOnDetailNode = null;
                boolean firstTime = true;
                Element itemElement = null;
                Element addOnsElement = null;
                Element addOnElement = null;

                // Cycle through each item
                String lineNumber = null;
                List itemNodeList = ((ValidationNode) orderNode.getChildNodes("items").toArray()[0]).getChildNodes();
                Iterator itemNodeIterator = itemNodeList.iterator();
                while(itemNodeIterator.hasNext())
                {
                    itemNode = (ValidationNode) itemNodeIterator.next();
                    itemElement = validationDocument.createElement("item");
                    itemElement.setAttribute(OrderValidationNode.STATUS, itemNode.getAttribute(OrderValidationNode.STATUS));

                    lineNumber = EMPTY;
                    lineNumberNodeList = itemNode.getChildNodes(ItemValidationNode.LINE_ITEM_NUMBER);

                    if(lineNumberNodeList != null && lineNumberNodeList.size() > 0)
                    {
                        lineNumber = ((ValidationNode) lineNumberNodeList.get(0)).getText();
                        itemElement.setAttribute("line_number", lineNumber);
                    }

                    // Add field validation to document
                    itemDetailList = itemNode.getChildNodes();
                    itemDetailIterator = itemDetailList.iterator();
                    while(itemDetailIterator.hasNext())
                    {
                        itemDetailNode = (ValidationNode) itemDetailIterator.next();

                        if(itemDetailNode.getAttribute(OrderValidationNode.STATUS) != null && (itemDetailNode.getAttribute(OrderValidationNode.STATUS).equals(OrderValidationNode.ERROR) || itemDetailNode.getAttribute(OrderValidationNode.STATUS).equals(OrderValidationNode.WARNING)))
                        {
                            if(firstTime)
                            {
                                orderElement.appendChild(itemsElement);
                                firstTime = false;
                            }

                            itemElement.appendChild(this.processNodeMessages(itemDetailNode, validationDocument, lineNumber));
                            itemsElement.appendChild(itemElement);
                        }
                    }
                }
            }
        }

        return validationDocument;
    }

    private Node processNodeMessages(ValidationNode validationNode, Document validationDocument, String lineNumber) throws Exception
    {
        Element messageElement = null;
        Element messageDetailElement = null;
        ValidationNode messageNode = null;
        ValidationNode messageDetailNode = null;

        Element fieldElement = validationDocument.createElement("data");
        fieldElement.setAttribute("field_name", validationNode.getAttribute(OrderValidationNode.NAME) + lineNumber);
        fieldElement.setAttribute("field_value", validationNode.getAttribute(OrderValidationNode.VALUE));
        fieldElement.setAttribute(OrderValidationNode.STATUS, validationNode.getAttribute(OrderValidationNode.STATUS));

        Element messagesElement = validationDocument.createElement("messages");
        fieldElement.appendChild(messagesElement);

        List messageList = validationNode.getChildNodes("messages");
        if(messageList != null && messageList.size() > 0)
        {
            List messageNodeList = ((ValidationNode) messageList.toArray()[0]).getChildNodes();
            if(messageNodeList != null && messageNodeList.size() > 0)
            {
                Iterator messageIterator = messageNodeList.iterator();
                while(messageIterator.hasNext())
                {
                    messageNode = (ValidationNode) messageIterator.next();

                    messageElement = validationDocument.createElement("message");
                    messagesElement.appendChild(messageElement);

                    List messageDetailNodeList = messageNode.getChildNodes();

                    if(messageDetailNodeList != null && messageDetailNodeList.size() > 0)
                    {
                        Iterator messageDetailNodeIterator = messageDetailNodeList.iterator();
                        while(messageDetailNodeIterator.hasNext())
                        {
                            messageDetailNode = (ValidationNode) messageDetailNodeIterator.next();

                            messageDetailElement = validationDocument.createElement(messageDetailNode.getAttribute(OrderValidationNode.NAME));

                            if(messageDetailNode.getText() != null && !messageDetailNode.getText().equals(""))
                            {
                                messageDetailElement.appendChild(validationDocument.createTextNode(messageDetailNode.getText()));
                            }
                            
                            messageElement.appendChild(messageDetailElement);
                        }
                    }
                }
            }
        }

        return fieldElement;
    }

    public static void main(String[] args)
    {
        ValidationXAO validationXAO = new ValidationXAO();

        try
        {
            Document doc = validationXAO.generateValidationXML(validationXAO.validateTestOrder());
            DOMUtil.print(doc, System.out);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private OrderValidationNode validateTestOrder()
    {
        OrderValidationNode orderNode = new OrderValidationNode();
        ItemValidationNode itemNode = new ItemValidationNode();
        ValidationRequest request = new ValidationRequest();

        ValidationNode vNode = null;
        ValidationResponse response = null;
        ValidationVO validationVO = null;
        Class validationClass = null;
        Map validationResponseMap = null;

        // Order
        orderNode.setAttribute(OrderValidationNode.NAME, "order");
        orderNode.setAttribute(OrderValidationNode.STATUS, "error");

        // Order header
        ValidationNode vHeader = new ValidationNode();
        vHeader.setAttribute(OrderValidationNode.NAME, "header");
        vHeader.setAttribute(OrderValidationNode.STATUS, "error");

        // Header nodes
        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, OrderValidationNode.BUYER_ADDRESS_1);
        vNode.setAttribute(OrderValidationNode.STATUS, "valid");
        vHeader.addChildNode(vNode);

        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, OrderValidationNode.BUYER_CITY);
        vNode.setAttribute(OrderValidationNode.STATUS, "error");

        // messages
        ValidationNode vMessages = new ValidationNode();
        vMessages.setAttribute(OrderValidationNode.NAME, "messages");

        // message
        ValidationNode vMessage = new ValidationNode();
        vMessage.setAttribute(OrderValidationNode.NAME, "message");
        ValidationNode vDescription = new ValidationNode();
        vDescription.setAttribute(OrderValidationNode.NAME, "description");
        vDescription.setText("City does not exist");

        vMessage.addChildNode(vDescription);
        vMessages.addChildNode(vMessage);
        vNode.addChildNode(vMessages);

        vHeader.addChildNode(vNode);

        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, OrderValidationNode.BUYER_COUNTRY);
        vNode.setAttribute(OrderValidationNode.STATUS, "valid");
        vHeader.addChildNode(vNode);
        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, OrderValidationNode.BUYER_STATE);
        vNode.setAttribute(OrderValidationNode.STATUS, "valid");
        vHeader.addChildNode(vNode);
        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, OrderValidationNode.BUYER_POSTAL_CODE);
        vNode.setAttribute(OrderValidationNode.STATUS, "valid");
        vHeader.addChildNode(vNode);

        orderNode.addChildNode(vHeader);

        // Items
        ValidationNode vItems = new ValidationNode();
        vItems.setAttribute(OrderValidationNode.NAME, "items");
        vItems.setAttribute(OrderValidationNode.STATUS, ValidationNode.VALID);

        // Item
        ItemValidationNode vItem = new ItemValidationNode();
        vItem.setAttribute(OrderValidationNode.NAME, "item");
        vItem.setAttribute(OrderValidationNode.STATUS, "error");

        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, "line-number");
        vNode.setText("1");
        vItem.addChildNode(vNode);

        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, ItemValidationNode.DELIVERY_DATE);
        vNode.setAttribute(OrderValidationNode.STATUS, "error");

        // messages
        vMessages = new ValidationNode();
        vMessages.setAttribute(OrderValidationNode.NAME, "messages");

        // message
        vMessage = new ValidationNode();
        vMessage.setAttribute(OrderValidationNode.NAME, "message");
        vDescription = new ValidationNode();
        vDescription.setAttribute(OrderValidationNode.NAME, "description");
        vDescription.setText("Delivery date is too far in advance");

        vMessage.addChildNode(vDescription);
        vMessages.addChildNode(vMessage);
        vNode.addChildNode(vMessages);

        vItem.addChildNode(vNode);


        vNode = new ValidationNode();
        vNode.setAttribute(OrderValidationNode.NAME, ItemValidationNode.ORDER_TOTAL);
        vNode.setAttribute(OrderValidationNode.STATUS, ValidationNode.VALID);
        vItem.addChildNode(vNode);

        vItems.addChildNode(vItem);

        orderNode.addChildNode(vItems);

        return orderNode;
    }

}