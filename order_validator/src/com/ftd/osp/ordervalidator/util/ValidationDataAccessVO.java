package com.ftd.osp.ordervalidator.util;
import java.util.Map;

public class ValidationDataAccessVO 
{
    private Object rs;
    private Map inputParms;

    public ValidationDataAccessVO()
    {
    }

    public Object getRs()
    {
        return rs;
    }

    public void setRs(Object newRs)
    {
        rs = newRs;
    }

    public Map getInputParms()
    {
        return inputParms;
    }

    public void setInputParms(Map newInputParms)
    {
        inputParms = newInputParms;
    }
}