package com.ftd.osp.ordervalidator.util;
import com.ftd.ftdutilities.*;
import java.util.*;

public class StringReplacer 
{
    public StringReplacer()
    {
    }

    /**
     * This method replaces values within a String with the given
     * values from the Map.  The String should contain keys like "replace a {word}"
     * @param inStr String that will be operated on
     * @param values Contains keys and values to replace in the String
     * @author Jeff Penney
     *
     */
    public static String replace(String inStr, Map values)
    {
        Iterator it = values.keySet().iterator();
        String key = null;
        String value = null;
        String ret = inStr;

        while(it.hasNext())
        {
            key = (String)it.next();
            value = (String)values.get(key);

            key = "{" + key + "}";
            
            ret = FieldUtils.replaceAll(ret, key, value);
        }

        return ret;
    }

    public static String stripStringOf(String source, char toStrip)
    {
        byte[] srcBytes = source.getBytes();
        byte[] newBytes = new byte[srcBytes.length];
        int count = 0;
        
        for(int i = 0; i < srcBytes.length; i++)
        {
          if(!(srcBytes[i] == (byte)toStrip))
          {
            newBytes[count] = srcBytes[i];
            count++;
          }
        }
        return new String(newBytes).trim();
    }

    public static void main(String[] args)
    {
        System.out.println(System.currentTimeMillis());
        stripStringOf("sasha", 's');
        System.out.println(System.currentTimeMillis());
       System.out.println("|" + stripStringOf("lkadsf", 's') + "|");
    }
}