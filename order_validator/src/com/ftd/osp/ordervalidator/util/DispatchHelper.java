package com.ftd.osp.ordervalidator.util;


import com.ftd.osp.framework.dispatcher.Dispatcher;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import javax.naming.InitialContext;

import javax.servlet.http.HttpServletRequest;

/**
 * This class is a helper utility for dispatching messages withing the JMS 
 * framework.
 *
 * @author Brian Munter
 */

public class DispatchHelper 
{

    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    private static final String STANDALONE_FLAG = "TEST_MODE";

    /** 
     * Constructor
     */
    public DispatchHelper()
    {
    }

    /**
     * Primary method which generates and dispatches a message within the JMS 
     * framework.  It also updates the statistic tracking in the database.
     * 
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param status String
     * @param request HttpServletRequest
     * @exception Exception
     */
    public static void dispatchOrder(String guid, String orderDetailId, String status) throws Exception
    {
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(METADATA_CONFIG_FILE_NAME, STANDALONE_FLAG);
        Logger logger= new Logger("com.ftd.osp.ordervalidator.util.DispatchHelper");

        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            MessageToken token = new MessageToken(); 
            token.setMessage(guid + "/" + orderDetailId); 
            token.setStatus(status); 
 
            logger.info("Dispatching. Status=" + status + ". Message=" +  token.getMessage() );
 
            Dispatcher dispatcher = Dispatcher.getInstance();
            InitialContext initContext = new InitialContext();
            dispatcher.dispatch(initContext, token);
        }
        else
        {
            logger.warn("Order is not being dispatched.  Config file is set to test mode.");
        }

    }

 
}