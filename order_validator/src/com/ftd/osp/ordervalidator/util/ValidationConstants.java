package com.ftd.osp.ordervalidator.util;


public class ValidationConstants
{
    public static final String RESPONSE_CODE = "RESPONSE_CODE";

    public static final String RESPONSE_INVALID = "INVALID";
    public static final String RESPONSE_VALID = "VALID";

    public static final String RESPONSE_SPEC_INSTR_CONTAINS_KEYWORDS = "Special instruction";
    public static final String RESPONSE_SPEC_INSTR_EXCEEDS_MAX_LEN = "Special instructions exceeds maximum length";
    public static final String RESPONSE_SPEC_INSTR_EXCEEDS_B2B_MAX_LEN = " Special instructions exceeds maximum length";
    public static final String RESPONSE_GIFT_MSG_EXCEEDS_MAX_LEN = "Gift message exceeds maximum length";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_GIFT_MSG="STOP! There is an error within Card message. Clear the field and try again.";
    public static final String RESPONSE_SIGNATURE_EXCEEDS_MAX_LEN = "Signature exceeds maximum length";
    public static final String RESPONSE_OCCASION_MISSING = "Occasion required";
    public static final String RESPONSE_ADDON_INVALID = "One or more of the selected add-ons are not available with this product";
    public static final String RESPONSE_ADDON_PRICE_INVALID = "Price is invalid for one or more add-ons";
    public static final String RESPONSE_ADDON_MAX_QTY_EXCEEDED = "One or more add-ons exceed the maximum quantity allowed for this product";
    public static final String RESPONSE_ADDON_NO_VENDORS = "One or more of the selected add-ons are not available for vendor delivery";

    public static final String RESPONSE_CC_INFO_MISSING = "Credit card information required";
    public static final String RESPONSE_CC_TYPE_MISSING = "Credit card type required";
    public static final String RESPONSE_CC_NUMBER_MISSING = "Credit card number required";
    public static final String RESPONSE_CC_DATE_MISSING = "Credit card expiration date required";
    public static final String RESPONSE_CC_INVALID_TYPE = "Invalid credit card type ({ccType})";
    public static final String RESPONSE_CC_INVALID_NUMBER = "Invalid credit card number";
    public static final String RESPONSE_CC_DATE_PRIOR_TO_TODAY = "Credit card expired {date}";
    public static final String RESPONSE_CC_DATE_INVALID = "Invalid credit card expiration date {date}";
    public static final String RESPONSE_CC_FOUND_MAGIC_NUMBER = "Test credit card";
    public static final String RESPONSE_CC_JCP_CARD_INTL = "International order placed with JCP Credit Card";

    // Alt Pay
    public static final String RESPONSE_AP_OVER_ALLOWED_TOTAL = " updated total exceeded authorized total.";
    public static final String RESPONSE_AP_AUTH_INVALID = " authorization id invalid. Please choose a different payment type.";
    public static final String RESPONSE_AP_ACCOUNT_INVALID = " account number invalid. Please choose a different payment type.";
    public static final String RESPONSE_BM_CUST_INTERNATIONAL_ADDRESS = "Bill Me Later customer with international address. Please choose a different payment type.";
    public static final String RESPONSE_BM_RECIP_INTERNATIONAL_ADDRESS = "Bill Me Later recipient with international address. Please choose a different payment type.";
    public static final String AP_INVALID_AUTH_TXT = "INVALID_AUTH_TXT";
    public static final String AP_INVALID_ACCOUNT_TXT = "INVALID_ACCOUNT_TXT";

    public static final String RESPONSE_SOURCE_CODE_EXPIRED = "Expired source code";
    public static final String RESPONSE_SOURCE_CODE_INVALID = "Invalid source code";
    public static final String RESPONSE_SOURCE_CODE_NOT_VALID_FOR_PAY_METHOD = "Source code not valid for payment method";
    public static final String RESPONSE_SOURCE_CODE_SEND_TO_SCRUB = "Ariba Order - Validate Customer Information";
    public static final String RESPONSE_SOURCE_CODE_COST_CENTER_LOOKUP_VALIDATION_FAILED = "Invalid cost center";

    public static final String RESPONSE_ORDER_TOTAL_EXCEEDS_MAX = "Shopping Cart Total Exceeds ${max}";
    public static final String RESPONSE_ORDER_TOTAL_ZERO = "Order total is zero";
    public static final String RESPONSE_ITEM_TOTAL_ZERO = "Product amount is zero";
    public static final String RESPONSE_ITEM_TOTAL_EXCEEDS_MAX = "Order amount exceeds {amount}";
    public static final String RESPONSE_ORDER_ITEMS_EXCEEDS_MAX = "Possible fraud - excessive items in shopping cart";
    public static final String RESPONSE_ITEM_ADDON_TOTAL_EXCEEDS_MAX = "Addon amount exceeds {addon}";
    
    public static final String RESPONSE_BUYER_INFO_MISSING = "Customer information missing";
    public static final String RESPONSE_BUYER_FIRST_NAME_MISSING = "Customer first name missing";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_FIRST_NAME = "STOP! There is an error within the first name. Clear the field and try again.";
    public static final String RESPONSE_BUYER_LAST_NAME_MISSING = "Customer last name missing";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_LAST_NAME = "STOP! There is an error within last name. Clear the field and try again.";
    public static final String RESPONSE_BUYER_CITY_MISSING = "Customer city missing";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_CITY = "STOP! There is an error within City. Clear the field and try again.";
    public static final String RESPONSE_BUYER_STATE_MISSING = "Customer state missing";
    public static final String RESPONSE_BUYER_ZIP_MISSING = "Customer postal code missing";
    public static final String RESPONSE_BUYER_ADDRESS1_MISSING = "Customer address line one missing";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS1 = "STOP! There is an error within Buyer-address1. Clear the field and try again.";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_ADDRESS2 = "STOP! There is an error within Buyer-address2. Clear the field and try again.";
    public static final String RESPONSE_INVALID_BUYER_STATE_FOR_ZIP = "Invalid state for zip code";
    public static final String RESPONSE_BUYER_STATE_INVALID = "Invalid sold-to state ({state})";
    public static final String RESPONSE_BUYER_ZIP_INVALID = "Customer zip code invalid";
    public static final String RESPONSE_BUYER_EMAIL_INVALID = "Customer email invalid";
    public static final String RESPONSE_CREDIT_CARD_NUMBER_IN_BUYER_EMAIL = "STOP! There is an error within the email address. Clear the field and try again.";
    public static final String RESPONSE_BUYER_COUNTRY_INVALID = "Customer country invalid";
    public static final String RESPONSE_BUYER_EVENING_PHONE_INVALID = "Invalid Phone 2 number";
    public static final String RESPONSE_BUYER_DAYTIME_PHONE_INVALID = "Invalid Phone 1 number";
    public static final String RESPONSE_BUYER_PHONE_MISSING = "Customer phone number missing";
    public static final String RESPONSE_BUYER_ADDRESS_TOO_LONG = "Customer address too long";
    public static final String RESPONSE_BUYER_FIRST_NAME_TOO_LONG = "Customer first name too long";
    public static final String RESPONSE_BUYER_LAST_NAME_TOO_LONG = "Customer last name too long";
    public static final String RESPONSE_BUYER_EMAIL_TOO_LONG = "Customer email too long";
    public static final String RESPONSE_BUYER_ZIP_TOO_LONG = "Customer zip code too long";
    public static final String RESPONSE_BUYER_STATE_TOO_LONG = "Customer state too long";
    public static final String RESPONSE_BUYER_CITY_TOO_LONG = "Customer city too long";
    public static final String RESPONSE_BUYER_COUNTY_TOO_LONG = "Customer county too long";
    public static final String RESPONSE_BUYER_COUNTRY_TOO_LONG = "Customer country too long";
    public static final String RESPONSE_BUYER_EVENING_PHONE_TOO_LONG = "Customer Phone 2 too long";
    public static final String RESPONSE_BUYER_DAYTIME_PHONE_TOO_LONG = "Customer Phone 1 too long";

    public static final String RESPONSE_RECIP_INFO_MISSING = "Recipient information is missing";
    public static final String RESPONSE_RECIP_LAST_NAME_MISSING = "Recipient last name is missing";
    public static final String RESPONSE_RECIP_FIRST_NAME_MISSING = "Recipient first name is missing";
    public static final String RESPONSE_RECIP_CITY_MISSING = "Recipient city is missing";
    public static final String RESPONSE_RECIP_STATE_MISSING = "Recipient state is missing";
    public static final String RESPONSE_RECIP_ZIP_MISSING = "Recipient zip is missing";
    public static final String RESPONSE_RECIP_ADDRESS1_MISSING = "Recipient address line one is missing";
    public static final String RESPONSE_RECIP_STATE_INVALID = "Invalid ship-to state ({state})";
    public static final String RESPONSE_RECIP_ZIP_INVALID = "Invalid recipient zip";
    public static final String RESPONSE_INVALID_RECIP_STATE_FOR_ZIP = "Invalid state for zip code";
    public static final String RESPONSE_RECIP_COUNTRY_INVALID = "Invalid recipient country ({country})";
    public static final String RESPONSE_RECIP_INT_DOMESTIC_PROD = "Domestic product, recipient flagged as international";
    public static final String RESPONSE_RECIP_DOMESTIC_PROD_INT = "International product, recipient flagged as domestic";
    public static final String RESPONSE_RECIP_PHONE_INVALID = "Recipient phone number is invalid";
    public static final String RESPONSE_RECIP_DEST_NOT_VALID = "Recipient destination not valid";
    public static final String RESPONSE_RECIP_DEST_MISSING = "Missing (business) name";
    public static final String RESPONSE_RECIP_DEST_TOO_LONG = "Recipient destination too long";
    public static final String RESPONSE_RECIP_ADDRESS_TOO_LONG = "Recipient address too long";
    public static final String RESPONSE_RECIP_ZIP_TOO_LONG = "Recipient zip code too long";
    public static final String RESPONSE_RECIP_CITY_TOO_LONG = "Recipient city too long";
    public static final String RESPONSE_RECIP_STATE_TOO_LONG = "Recipient state too long";
    public static final String RESPONSE_RECIP_PHONE_TOO_LONG = "Recipient phone too long";
    public static final String RESPONSE_RECIP_FIRST_NAME_TOO_LONG = "Recipient first name too long";
    public static final String RESPONSE_RECIP_LAST_NAME_TOO_LONG = "Recipient last name too long";
    public static final String RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_ONE = "PO or APO box is not allowed in address line one";
    public static final String RESPONSE_RECIP_PO_NOT_ALLOWED_ADDRESS_TWO = "PO or APO box is not allowed in address line two";
    public static final String RESPONSE_RECIP_STATE_AK_HI = "Because the delivery is to {state} there is an additional {price}";

    public static final String RESPONSE_PROD_INFO_MISSING = "Product information required";
    public static final String RESPONSE_PROD_INVALID = "Product is not on file";
    public static final String RESPONSE_PROD_UNAVAILABLE = "Product not available";
    public static final String RESPONSE_NO_DROP_SHIP_CANADA = "Drop ship product. No delivery to Canada, Puerto Rico or Virgin Islands";
    public static final String RESPONSE_NO_SATURDAY_DELIVERY = "Drop-ship order - No delivery on Saturday";
    public static final String RESPONSE_TWO_DAY_SHIPPING_ONLY = "Alaska and Hawaii can only ship two day";
    public static final String RESPONSE_NO_WESTFC_SHIP_AKHI="FTD West fulfilled freshcuts cannot be delivered to AK/HI zip codes";
    public static final String RESPONSE_NO_COLOR_SELECTED = "Product {product} no color selected";
    public static final String RESPONSE_NO_PRICE_POINT_SELECTED = "Product {product} No Price Point Selected";
    public static final String RESPONSE_NO_PRICE_SELECTED = "Product Amount is zero";
    public static final String RESPONSE_COLOR_INVALID = "Product suffix does not exist {color_one} or {color_two}";
    public static final String RESPONSE_PRICE_POINT_INVALID = "Product {product} price does not exist {price_one}, {price_two} or {price_three} for {date_time}";
    public static final String RESPONSE_PRICE_POINT_VALID = "Product {product} price {price_one}, {price_two} or {price_three} is valid for {date_time}";
    public static final String RESPONSE_PROD_PRICE_VARIABLE_INVALID = "Product variable price must be between {minprice} and {maxprice}";
    public static final String RESPONSE_PROD_STATE_EXCLUSION = "{dayofweek} delivery is not available to {state} for this product due to agricultural restrictions";
    public static final String RESPONSE_NO_SHIP_METHOD = "Ship Method Required";
    public static final String RESPONSE_DETAIL_SOURCE_CODE_EXPIRED ="Expired detail source code";
    public static final String RESPONSE_DETAIL_SOURCE_CODE_INVALID ="Invalid detail source code";
    public static final String RESPONSE_INVALID_SHIP_METHOD = "Please see your manager before processing this order.";
    public static final String RESPONSE_EXCEEDS_PRICE_VARIANCE_MAX = "Order exceeds maximum price differential allowed." +
                                        " " + "Please see your manager before processing this order.";

    public static final String RESPONSE_DELIVERY_DATE_PASSED = "Delivery date has passed";
    public static final String RESPONSE_DELIVERY_DATE_FORMAT = "Delivery date format invalid";
    public static final String RESPONSE_DELIVERY_DATE_TOO_FAR_OFF = "Delivery date exceeds {maxdays} days";
    public static final String RESPONSE_DELIVERY_DATE_INVALID = "Delivery date invalid";
    public static final String RESPONSE_DELIVERY_DATE_HOLIDAY = "Invalid delivery date";
    public static final String RESPONSE_DELIVERY_DATE_BEFORE_NEXT_AVAIL = "Delivery date < next available delivery date";
    public static final String RESPONSE_DELIVERY_DATE_SHIP_METHOD_INVALID = "Delivery method not valid for delivery date";
    public static final String RESPONSE_DELIVERY_DATE_FROM_FLORIST_TO_VENDOR = "Please change the delivery method for this order. Please verify with the customer that the order will now arrive in an elegant gift box.";
    public static final String RESPONSE_DELIVERY_DATE_FROM_VENDOR_TO_FLORIST = "Please change the delivery method for this order. Please verify with the customer that the order will be delivered via a local FTD Florist.";
    public static final String RESPONSE_DELIVERY_DATE_NO_MONDAY_FOR_BBN = "Morning Delivery Orders cannot be delivered on Monday. Please contact the customer for an alternate delivery date.";
    
    public static final String RESPONSE_DELIVERY_DATE_MISSING = "Delivery date missing";
    public static final String RESPONSE_DELIVERY_DATE_NO_SUNDAY = "Sunday delivery not valid for dropship";
    public static final String RESPONSE_DELIVERY_DATE_NO_FLORIST_SUNDAY = "No florist available for Sunday delivery";
    public static final String RESPONSE_DELIVERY_DATE_NO_SATURDAY = "Drop Ship Order - No delivery on Saturday";
    public static final String RESPONSE_DELIVERY_DATE_CLOSED = "Invalid Delivery Date {date} Closed Full Day";
    public static final String RESPONSE_DELIVERY_DATE_DROP_SHIP_NEXT_AVAIL = "Next Available Delivery Date for Drop Ship Prod is {date}";
    public static final String RESPONSE_DELIVERY_DATE_PROD_UNAVAILABLE = "Product not available within the near future.";
    public static final String RESPONSE_DELIVERY_DATE_UNAVAILABLE_RANGE = "This product is not available for delivery from {expstart} to {expend}";
    public static final String RESPONSE_DELIVERY_DATE_AVAILABLE_RANGE = "This product is only available for delivery from {expstart} to {expend}";

    public static final String RESPONSE_PRODUCT_STATE_EXCLUSION = "{dayofweek} delivery is not available to {state} due to agricultural restrictions.";

    public static final String RESPONSE_PRODUCT_SERVICE_SOURCE_EXCLUDED  = "Product {programName} is not available for purchase on this source code";
    public static final String RESPONSE_PRODUCT_NOT_AVAIL_IN_ZIP = "Floral product not available for recipient zip code";
    public static final String RESPONSE_NO_FLORIST_AVAILABLE = "No florist found. Please select a florist";
    public static final String RESPONSE_FLORIST_NO_SUNDAY = "No florist available for Sunday delivery for product";
    public static final String RESPONSE_FLORIST_NO_PRODUCT = "The selected florist does not have this product";
    public static final String RESPONSE_FLORIST_DELIVERY_REQUIRED = "Florist delivery is required for this product";
    
    public static final String RESPONSE_BBN_UNAVAILABLE = "Morning Delivery is Unavailable for this order. Please contact the customer.";

    // Address verification errors
    public static final String RESPONSE_QMS_ADDRESS_INVALID = "Invalid street address based on address verification";
    public static final String RESPONSE_QMS_CITY_STATE_INVALID = "Invalid state based on address verification";
    public static final String RESPONSE_QMS_ZIP_INVALID = "Invalid zip code based on address verification";
    public static final String RESPONSE_QMS_CITY_INVALID = "Invalid city based on address verification";
    public static final String RESPONSE_QMS_GENERIC_INVALID = "Invalid address based on address verification";
    public static final String RESPONSE_QMS_ERROR = "RESPONSE_QMS_ERROR";


    public static final String RESPONSE_MEMBERSHIP_NOT_VALID = "The membership information is not valid";
    public static final String RESPONSE_MEMBERSHIP_MISSING = "Membership information missing";

    public static final String RESPONSE_MAX_QUANTITY_EXCEEDED = "Quantity received is greater than max_quantity";
    public static final String RESPONSE_NOVATOR_FRAUD_WARNING = "Potential Fraud: {fraud_desc} - click fraud box";

    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_1 = "Incorrect format";
    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_2 = " Incorrect format";
    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_3 = "  Incorrect format";
    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_4 = "   Incorrect format";
    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_5 = "    Incorrect format";
    public static final String RESPONSE_BILLING_INFO_FORMAT_INVALID_6 = "     Incorrect format";
    
    public static final String ORDER = "ORDER";
    public static final String CONNECTION = "CONNECTION";
    public static final String VALIDATION_DAU = "VALIDATION_DAU";
    public static final String AUTO_FIX = "AUTO_FIX";
    public static final String CONNECTION_ERROR = "CONNECTION must be of type java.sql.Connection";
    public static final String ORDER_ERROR = "ORDER must be of type Order";
    public static final String UPSELL_ERROR = "ValidateFlorist attempted to retrieve information on an upsell product";

    public static final String YES = "Y";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String ZIP_CODE = "ZIP_CODE";

    public static final String AMAZON_SHIP_CODE = "2F";
    public static final String AMAZON_ORIGIN = "ORDER_ORIGIN_AMAZON";
    public static final String AMZ_AUTO_CANCEL               = "AUTO_CANCEL";
    public static final String AMZ_GLOBAL_CONTEXT            = "AMAZON_CONFIG";
    
    public static final String MERCENT_AUTO_CANCEL               = "AUTO_CANCEL";
    public static final String MERCENT_GLOBAL_CONTEXT            = "MERCENT_CONFIG";
    public static final String MSG_STATUS_DIPSATCHER = "ES1005";

    //product types
    public static final String FLORAL = "FLORAL";
    public static final String SAME_DAY_FRESH_CUT = "SDFC";
    public static final String SPECIALTY_GIFT = "SPEGFT";
    public static final String SAME_DAY_GIFT = "SDG";
    public static final String FRESH_CUT = "FRECUT";

    //ship methods
    public static final String FLORIST_ONLY = "SD";
    public static final String FRESH_CUT_CARRIER = "ND";
    public static final String SAME_DAY_FLORIST = "SD";
    public static final String SAME_DAY_CARRIER = "ND";
    public static final String DROP_SHIP = "2F";

    public static final String VALIDATOR_SYS_USER = "ORDER_VALIDATOR";

    public static final String BYPASS_RECALC_ORDER = "BYPASS_RECALCULATE_ORDER";
    public static final String GET_COUNTRY_BY_3_CHAR_CODE = "GET_COUNTRY_BY_3_CHAR_CODE";
    public static final String CALC_DELIVERY_DATE = "CALCULATE_DELIVERY_DATE";
    public static final String CALC_NEXT_AVAIL_DELIVERY_DATE = "CALCULATE_NEXT_AVAIL_DELIVERY_DATE";
    public static final String CHECK_QTY_OF_1 = "CHECK_QTY_OF_ONE";
    //The XXX and YYY are important because they are replaced in the actual displaying of the message.
    public static final String RESPONSE_SERVICE_FEE_INCREASE = "The service charge /shipping charge will be increased from {originalPrice} to {newPrice}. Please verify this with the customer, select another product, or see your Supervisor.";
    public static final String RESPONSE_SCRUB_NO_CHANGE_SERVICE_FEE_INCREASE_PART1 = "The service charge /shipping charge will be increased from XXX to YYY for order {ORDER_NUMBER}. ";
    public static final String RESPONSE_SCRUB_NO_CHANGE_SERVICE_FEE_INCREASE_PART2 = "Please verify this with the customer, select another product, or see your Supervisor.";
    public static final String RESPONSE_SCRUB_CHANGE_SERVICE_FEE_INCREASE = "By changing the delivery method or date, the service charge will be increased from XXX to YYY. Please verify this with the customer, select another product, or see your Supervisor.";

    //PRODUCT TYPES
    public static final String PRODUCT_TYPE_FRESHCUT = "FRECUT";
    public static final String PRODUCT_TYPE_SAMEDAY_FRESHCUT = "SDFC";
    public static final String PRODUCT_TYPE_FLORAL = "FLORAL";
    public static final String PRODUCT_TYPE_SAMEDAY_GIFT = "SDG";
    public static final String PRODUCT_TYPE_SPECIALTY_GIFT = "SPEGFT";
    public static final String PRODUCT_TYPE_SERVICES = "SERVICES";
    
    //PRODUCT SUBTYPES
    public static final String PRODUCT_SUB_TYPE_FREESHIP = "FREESHIP";
    
    //SERVICES
    public static final String FREE_SHIPPING_NAME = "FREESHIP";
    public static final String RESPONSE_SOURCE_CODE_FS_INVALID = "This source code does not allow for {programName} discounts to be applied";
    public static final String RESPONSE_EMAIL_ADDRESS_FS_INVALID = "This email address does not allow for {programName} discounts to be applied";
    //SN Added this for UseCase: 24094
    public static final String RESPONSE_CAMS_UNREACHABLE = "System cannot determine {programName} eligibility at this time. Ask if customer has an {programName}.";
    public static final String RESPONSE_EMAIL_ADDRESS_NO_FSMEMBERSHIP = "This email address is not associated with an {programName}.";
    
    public static final String RESPONSE_PRODUCT_FS_NOT_ALLOWED = "This product does not allow for {programName} discounts to be applied";
    public static final String RESPONSE_BUYER_NOT_SIGNED_IN_USE = "Customer was not signed in to account when order was placed so {programName} discounts were not applied";
    public static final String RESPONSE_BUYER_NOT_SIGNED_IN_PURCHASE = "Customer was not signed in to account when order was placed so product {programName} cannot be ordered. Please Remove this order and contact the customer. The customer will need to order the product again while signed in to a Website personal account.";
    public static final String IN_PROGRAM_NAME = "IN_PROGRAM_NAME";
    public static final String IN_EMAIL_ADDRESS = "IN_EMAIL_ADDRESS";
    public static final String IN_COMPANY_ID = "IN_COMPANY_ID";

    //CommonUtils Constants
    public static final String MSG_SOURCE = "ORDER VALIDATOR";
	public final static String PROPERTY_FILE   = "order_validator_config.xml";
	public final static String DATASOURCE_NAME = "DATASOURCE";
	public static final String LOGGER_CATEGORY_COMMONUTILS = "com.ftd.osp.ordervalidator.util.CommonUtils";
	public static final String ORDER_SCRUB_DATASOURCE_NAME = "ORDER SCRUB";
	
	//Gift Card/Certificate
	public static final String RESPONSE_GC_PAYMENT_INFO_INVALID = "Gift Card/Certificate payment is invalid. Please choose a different payment type.";
	public static final String RESPONSE_GC_PAYMENT_AMT_INVALID = "You may need to contact the customer for their credit card if you need to increase the price of the product or shipping because this order was placed with a gift card/certificate.  If Gift Certificate is valid, then the Gift Code Service may be experiencing an issue.";
	public static final String RESPONSE_GC_OVER_ALLOWED_TOTAL = "Gift Card/Certificate updated total exceeded authorized total.";
	public static final String RESPONSE_GC_GIFT_CODE_SERVICE_ERROR = "Error encountered retrieving Gift Certificate details.  Try again later.";
	public static final String GC_COUPON_ISSUED_ACTIVE = "Issued Active";
	public static final String GC_COUPON_REINSTATE = "Reinstate";
	public static final String GC_COUPON_REDEEMED = "Redeemed";
	
	// Order detail status
	public static final String ORDER_DETAIL_STATUS_VALID = "2007";
	public static final String ORDER_DETAIL_STATUS_PROCESSED = "2006";
	
    public static final String MRCNT_ORD_INDICATOR = "MERCENT";
    public static final String PTN_ORD_INDICATOR = "PARTNER";
    
    public static final int GIFT_MESSAGE_MAX_LEN    = 230;
    public static final int GIFT_MESSAGE_MAX_LEN_PC = 103;

    public final static String GATHERER_CONFIG_FILE = "gatherer_config.xml";
}