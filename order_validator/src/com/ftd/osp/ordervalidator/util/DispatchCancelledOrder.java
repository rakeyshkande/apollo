package com.ftd.osp.ordervalidator.util;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.*;
import com.ftd.osp.utilities.stats.OrderDetailStates;
import com.ftd.osp.utilities.vo.MessageToken;

import java.sql.Connection;

import javax.naming.InitialContext;

/**
 * This class is a helper utility for dispatching cancelled amazon orders within the JMS 
 * framework.
 *
 * @author Rose Lazuk
 */

public class DispatchCancelledOrder
{
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    private static final String STANDALONE_FLAG =  "STANDALONE_FLAG";
    
    private static Logger logger= new Logger("com.ftd.osp.ordervalidator.util.DispatchCancelledOrder");

    
    /** 
     * Constructor
     */
    public DispatchCancelledOrder()
    {
    }

    /**
     * Primary method which generates and dispatches a message within the JMS 
     * framework.  It also updates the statistic tracking in the database.
     * 
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param status String
     * @exception Exception
     */
    public void dispatchOrder(OrderVO order, OrderDetailsVO item, String status, Connection connection) throws Exception
    {
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(METADATA_CONFIG_FILE_NAME, STANDALONE_FLAG);

        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            MessageToken token = new MessageToken(); 
            token.setMessage(this.generateMessage(order, item)); 
            token.setStatus(status); 
 
            Dispatcher dispatcher = Dispatcher.getInstance();
            InitialContext initContext = new InitialContext();
            dispatcher.dispatch(initContext, token);
        }

        // Record exit statistics
        this.recordStats(order, item, null, OrderDetailStates.OV_OUT, connection);
    }

    private String generateMessage(OrderVO order, OrderDetailsVO item)
    {
        return order.getGUID() + "/" + item.getLineNumber();
    }
    
    private void recordStats(OrderVO order, OrderDetailsVO item, String relator, int state, Connection connection) throws Exception
    {
        StatsUtil statsUtil = new StatsUtil();
        statsUtil.setOrder(order);        
        statsUtil.setState(state);
        statsUtil.setRelator(relator);
        statsUtil.setItem(item);
        statsUtil.insert(connection);
        
    }
}