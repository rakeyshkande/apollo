package com.ftd.osp.ordervalidator.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.oro.text.perl.Perl5Util;
import org.xml.sax.SAXException;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.LanguageUtility;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.AddressTypeHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.BillingInfoHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.ColorHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.CountryMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.LanguageMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.OccasionHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourceMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SourcePartnerBinHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.SpecialCharacterMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.StateMasterHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.BillingInfoVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryVoComparator;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.pas.client.util.PASParamUtil;
import com.ftd.pas.client.util.PASQueryUtil;
import com.ftd.pas.core.domain.ProductAvailVO;

public class DataMassager
{
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    private static final String GET_HP_PRODUCT_ID = "GET_HP_PRODUCT_ID";
    private static final String GET_OCCASION_LIST = "GET_OCCASION_LIST";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    private static final String GET_COLORS = "GET_COLORS";
    private static final String GET_COUNTRY_LIST = "GET_COUNTRY_LIST";
    private static final String GET_STATE_LIST = "GET_STATE_LIST";
    private static final String GET_PRODUCT_SUBCODE = "GET_PRODUCT_SUBCODE";
    private static final String GET_ADDRESS_TYPES = "GET_ADDRESS_TYPES";
    private static final String GET_SOURCE_CODE_DETAILS = "GET SOURCE CODE DETAILS";
    private static final String GET_CPC_INFO_RECORD = "GET_CPC_INFO_RECORD";
    private static final String GET_PRODUCT_COLORS = "GET PRODUCT COLORS";
    //private static final String GET_ADDON_BY_ID = "GET_ADDON_BY_ID_OV";
    //private static final String ADDON_ID = "ADDON_ID";

    private static final String CREDIT_CARD_EXP_FORMAT = "MM/yy";

    //private static final String TWO_DAY_DELIVERY = "2F";
    //private static final String  SAME_DAY_DELIVERY = "SD";


    private static final int SHIP_TO_NAME_MAX_LENGTH = 40;
    private static final int SIGNATURE_MAX_LENGTH = 230;

    private static final int ADDRESS_SIZE_MAX = 45;
    private static final String ADDON_TYPE_CARD = "4";

    private static final String PHONE_TYPE_HOME = "Home";

    private static final String DEFAULT_LANGUAGE_ID = "ENUS";
	private static final String GET_ITEM_RECIPIENT_INFO = "GET_ITEM_RECIPIENT_INFO";

	 private static final int AMOUNT_SCALE = 2;

	private static String PC_PDP_TEMPLATE = "PDP";

    private static Logger logger= new Logger("com.ftd.osp.ordervalidator.util.DataMassager");

    // Setup shipping method objects
    private static ShippingMethod standardShipping = new ShippingMethod();
    private static ShippingMethod twoDayShipping = new ShippingMethod();
    private static ShippingMethod nextDayShipping = new ShippingMethod();
    private static ShippingMethod sdShipping = new ShippingMethod();
    private static ShippingMethod satShipping = new ShippingMethod();

    public static OrderVO massageOrderData(OrderVO order, Connection con) throws IOException, SAXException,
            ParserConfigurationException, TransformerException, SQLException,
            ParseException, Exception
    {
        ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();
        return massageOrderData(order, con, vdau);
    }

    public static OrderVO massageOrderData(OrderVO order, Connection con, ValidationDataAccessUtil vdau) throws IOException, SAXException,
            ParserConfigurationException, TransformerException, SQLException,
            ParseException, Exception
    {
        Pattern p = Pattern.compile("[^a-zA-z0-9!,'$%()^:{}&|;/*~=`@ .#_-]+");
        Matcher m;
        String gpValue=null;
        boolean globalParmFlag = false;
        boolean languageIdFlag = false;
        boolean mappingEnableFlag = false;
        boolean characterMappingFlag = false;
        String actualAddressType="";
        SpecialCharacterMasterHandler spcmHandler = (SpecialCharacterMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_CHARACTER_MAPPING");
        Map spcmap = new HashMap();
        if(spcmHandler != null)
        {
        spcmap = spcmHandler.getSpecialCharacterMap();
        }
        logger.debug("###Global Parameter Value Before Handler### "+gpValue);
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {
            gpValue = parmHandler.getGlobalParm("FTDAPPS_PARMS", "SPECIAL_CHAR_MAPPING");
            logger.debug("###Global Parameter Value ### "+gpValue);
        }
        //validate the language id
        validateAndSetLanguageId(order);
        logger.debug("###Global Parameter Value After Handler### "+gpValue);
        if(gpValue.equalsIgnoreCase("Y")){globalParmFlag = true; logger.debug("###globalParmFlag ### "+globalParmFlag);}
        logger.debug("###order.getLanguageId() ### "+order.getLanguageId());
        if(gpValue.equalsIgnoreCase("N") && !order.getLanguageId().equalsIgnoreCase("ENUS")){languageIdFlag = true;
        logger.debug("###languageIdFlag ### "+languageIdFlag);}
        if(globalParmFlag || languageIdFlag){ mappingEnableFlag = true;
        logger.debug("###mappingEnableFlag ### "+mappingEnableFlag);}
        if(order.getCharacterMappingFlag()==null || order.getCharacterMappingFlag().equalsIgnoreCase("N")){ characterMappingFlag = true;
        logger.debug("###characterMappingFlag ### "+characterMappingFlag);}
        logger.debug("###Char Mapping Flag### "+order.getCharacterMappingFlag());
        BuyerVO buyer = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;
        BuyerAddressesVO buyerAddress = null;
        BuyerPhonesVO buyerPhone = null;
        RecipientPhonesVO recipPhone = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;
        AVSAddressVO avsAddress = null;
        OrderDetailsVO item = null;
        MembershipsVO membership = null;
        Iterator it = null;
        Iterator typeIt = null;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;

        boolean amazonOrder  = false;
        boolean mobiOrder  = false;
        boolean tabletOrder  = false;
        boolean mercentOrder = false;
        boolean partnerOrder = false;

        Collection payments = null;
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("MM/yy");
        SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy");
        java.util.Date ccDate = null;
        //DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL();

        String occasionId = null;
        String phoneNumber = null;
        String phoneExt = null;
        String color1 = null;
        String color2 = null;
        String colorCode1 = null;
        String colorCode2 = null;
        String specialInstructions = null;
        String buyerEmail = null;
        String buyerFirstName = null;
        String buyerLastName = null;
        String recipFirstName = null;
        String recipLastName = null;
        String recipCountry = null;
        String address1 = null;
        String address2 = null;
        String zipCode = null;
        String ccNumber = null;
        String ccExpDate = null;
        String ccType = null;
        String paymentType = null;
        String hpProdId = null;
        String resultCode = null;
        String avsAddress1 = null;
        String avsCity = null;
        String avsState = null;
        String avsZip = null;
        String avsOverrideFlag = null;
        String shipDate = "";
       // String shipMethod = null;
       // String deliveryDate = null;
        //String vendorId = null;
        String prodSubCodeId = null;
        String buyerCountry = null;
        //String countryCode = null;
        String state = null;
        String stateCode = null;
        String addType = null;
        String typeKey = null;
        String shipToName = null;
        String shipToType = null;
        CountryMasterVO countryVo = null;
        int deliveryDaysOutMAX = getDeliveryDaysOutMAX(vdau,con);
        boolean isBuyerEmailChanged = false;

        standardShipping.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
        standardShipping.setDeliveryCharge(new BigDecimal(0));
        standardShipping.setDescription(GeneralConstants.DELIVERY_STANDARD);

        twoDayShipping.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
        twoDayShipping.setDeliveryCharge(new BigDecimal(0));
        twoDayShipping.setDescription(GeneralConstants.DELIVERY_TWO_DAY);

        nextDayShipping.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
        nextDayShipping.setDeliveryCharge(new BigDecimal(0));
        nextDayShipping.setDescription(GeneralConstants.DELIVERY_NEXT_DAY);

        sdShipping.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
        sdShipping.setDeliveryCharge(new BigDecimal(0));
        sdShipping.setDescription(GeneralConstants.DELIVERY_FLORIST);

        satShipping.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);
        satShipping.setDeliveryCharge(new BigDecimal(0));
        satShipping.setDescription(GeneralConstants.DELIVERY_SATURDAY);



        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String defaultSourceCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "SOURCE_CODE_DEFAULT");
        //String defaultColorCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COLOR_CODE_DEFAULT");
        String defaultOccasionCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "OCCASION_TYPE_DEFAULT");
        String jcpEmployeeSourceCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "JCP_DEFAULT_EMPLOYEE_SOURCE_CODE");
        String paymentTypeCC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_CREDIT_CARD");
        String paymentTypeInvoice = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_TYPE_INVOICE");
        String jcpIntSourceCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "JCP_DEFAULT_INT_SOURCE_CODE");
        String originNovator = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_NOVATOR");
        String originTest = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_TEST");
        String originBulk = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_BULK");
        String originAriba1 = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_ARIBA1");
        String originAriba2 = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_ARIBA2");
        String canadaCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_CANADA");
        String usCountryCode = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "COUNTRY_CODE_UNITED_STATES");
        String addressTypeB = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_BUSINESS");
        //String addressTypeO = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_OTHER");
        String addressTypeR = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_RESIDENTIAL");
        String addressTypeH = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_HOSPITAL");
        String addressTypeF = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_FUNERAL_HOME");
        String addressTypeC = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ADDRESS_TYPE_CEMETERY");
        String b2bPaymentTypeInvoice = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_METHOD_INVOICE");
       // String b2bPaymentTypePCard = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "PAYMENT_METHOD_PC");
        String receviedItemStatus = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "RECEIVED_ITEM_STATUS");
        String originAmazon = configUtil.getProperty( METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_AMAZON" );
        String originMobile = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_MOBILE");
        String originTablet = configUtil.getProperty(METADATA_CONFIG_FILE_NAME, "ORDER_ORIGIN_TABLET");


        // Get color data
        //Map colorMap = getColors(vdau, con);

        // Get occasion data
        Map occasionMap = getOccasions(vdau, con);

        // Get source code data
        logger.debug("before sourceVo");
        SourceMasterVO sourceVo = getSourceDetails(order.getSourceCode(), vdau, con);
        logger.debug("after sourceVo");
        if(sourceVo == null) sourceVo = new SourceMasterVO();
        if(sourceVo.getJcpenneyFlag() == null) sourceVo.setJcpenneyFlag("");
        if(sourceVo.getSourceCode() == null) sourceVo.setSourceCode("");

        // Get the country list
        List countryList = getCountryList(vdau, con);

        // Get the state list
        Map stateByIdMap = getStateByIdMap(vdau, con);
        Map stateByNameMap = getStateByNameMap(vdau, con);

        // Get address types
        Map addressTypes = getAddressTypes(vdau, con);

        //////////////////////// BEGIN Order header //////////////////////
        // BEGIN if source code is empty or null set it to 350
        if(order.getSourceCode() == null || order.getSourceCode().length() == 0)
        {
            order.setSourceCode(defaultSourceCode);
        }
        // END if source code is empty or null set it to 350

        // Try cache first
        logger.debug("try cache first");
        SourceMasterHandler sourceCodeHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        SourceMasterVO sourceVO =  sourceCodeHandler.getSourceCodeById(order.getSourceCode());

        if (sourceVO != null)
        {
          // FTD_APPS.BILLING_INFO.SOURCE_CODE exists for this source code
          // verify that the billing info logic field = 'CONCAT'
          if ( (sourceVO.getBillingInfoLogic() != null) && (sourceVO.getBillingInfoLogic().equals("CONCAT")))
          {
              // Parse SCRUB.MEMBERSHIP_ID.MEMBERSHIP_ID_NUMBER
              // using FTD_APPS.BILLING_INFO.INFO_FORMAT
              BillingInfoHandler billingInfoHandler = (BillingInfoHandler) CacheManager.getInstance().getHandler("CACHE_NAME_BILLING_INFO");
              List list = billingInfoHandler.getBillingInfoBySource(order.getSourceCode());

              BillingInfoVO billingInfoVO = null;
              DataAccessUtil dau = DataAccessUtil.getInstance();
              DataRequest dataReq = null;
              HashMap parameters = null;
              Map outputParameters = null;
              String match = null;
              boolean matchResult = false;
              String membershipID = ((MembershipsVO)order.getMemberships().get(0)).getMembershipIdNumber();
              Perl5Util regExp = new Perl5Util();

              Collections.sort(list, new BillingInfoVOComparator());

              for (Iterator iter = list.iterator(); iter.hasNext() ; )
              {
                billingInfoVO = (BillingInfoVO) iter.next();
                logger.info("Billing Info Sequence:: " + billingInfoVO.getInfoSequence());

                for (int i = 1; i <= membershipID.length(); i++)
                {
                  matchResult = regExp.match(billingInfoVO.getInfoFormat(),  membershipID.substring(0, i));
                  if (matchResult)
                  {
                      logger.info("Membership ID:: " + membershipID);
                      //set the membership id for the next billing info vo
                      membershipID = membershipID.substring(i);
                      match = regExp.getMatch() == null?"":regExp.getMatch().toString();
                      logger.info("Info Format:: " + billingInfoVO.getInfoFormat());
                      logger.info("Match:: " + match);
                      logger.info("Match Result:: " + matchResult);

                    // Store in SCRUB.CO_BRAND.INFO_DATA
                      dataReq = new DataRequest();
                      parameters = new HashMap();
                      parameters.put("IN_GUID", order.getGUID());
                      parameters.put("IN_INFO_NAME", billingInfoVO.getInfoPrompt());
                      parameters.put("IN_INFO_DATA", match);

                      dataReq.setConnection(con);
                      dataReq.setInputParams(parameters);
                      dataReq.setStatementID("SCRUB.INSERT_CO_BRAND");
                      outputParameters = (Map) dau.execute(dataReq);

                      BigDecimal coBrandID = (BigDecimal) outputParameters.get("RegisterOutParameterCoBrandId");
                      String status = (String) outputParameters.get("RegisterOutParameterStatus");
                      String message = (String) outputParameters.get("RegisterOutParameterMessage");
                      logger.info("Co-Brand ID:: " + coBrandID);
                      logger.info("status:: " + status);
                      logger.info("message:: " + message);
                      if (status != null)
                      {
                        status = status.trim();
                        if (status.equalsIgnoreCase("N"))
                        {
                          throw new Exception(message);
                        }
                      }
                      break;
                  }
                }



              }
          }
        }


        // Set origin for FOX orders to the correct origin based on the company_master
        // internet origin
        boolean novatorOrder = false;
        String orderOrigin = order.getOrderOrigin();
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(con);
        if(orderOrigin == null) orderOrigin = "";
        if(orderOrigin.equalsIgnoreCase(originNovator))
        {
            novatorOrder = true;
            order.setOrderOrigin(sourceVo.getInternetOrigin());

            //If the origin was set to null, then set it back to fox.
            //We do not want null origins.  This can happen if the source
            //code is invalid. Defect#(defect not logged in tracker yet-bheery identified it)
            //emueller, 2/26/04
            if(order.getOrderOrigin() == null || order.getOrderOrigin().length() <= 0)
            {
                order.setOrderOrigin(originNovator);
            }

        }

        boolean b2bOrder = false;
        if(orderOrigin.equalsIgnoreCase(originAriba1) || orderOrigin.equalsIgnoreCase(originAriba2)){
          b2bOrder = true;
        }
        else if( orderOrigin.equalsIgnoreCase(originAmazon) )
        {
          amazonOrder = true;
        }
        else if( orderOrigin.equalsIgnoreCase(originMobile) )
        {
          mobiOrder = true;
        }
        else if( orderOrigin.equalsIgnoreCase(originTablet) )
        {
          tabletOrder = true;
        }
        //Mercent have different origins depending upon the channel. For ex, MRCNT_A for Amazon channel Mercent order
        //and MRCNT_E for Ebay channel mercent order. So check if origin starts with "MRCNT_" to identify mercent orders.
        else if(mercentOrderPrefixes.isMercentOrder(orderOrigin)) {
        	mercentOrder = true;
          }

        else if( new PartnerUtility().isPartnerOrder(orderOrigin,order.getSourceCode(), con)) {
        	partnerOrder = true;
        }

        // Set origin to test if this is a test order
        payments = order.getPayments();
        it = null;
        boolean foundTestCC = false;
        if(!order.isOeOrder() && payments != null && payments.size() > 0)
        {
            // Get order credit cards
            it = payments.iterator();
            payment = null;
            creditCard = null;
            ccNumber = null;
            paymentType = null;
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                paymentType = payment.getPaymentMethodType();
                if(paymentType == null) paymentType = "";
                if(paymentType.equals(paymentTypeCC))
                {
                    if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                        ccNumber = creditCard.getCCNumber();
                        if(ccNumber == null) ccNumber = "";

                        if(ccNumber.equals("4111111111111111"))
                        {
                            foundTestCC = true;
                            break;
                        }
                    }
                }
            }
        }

        if(foundTestCC)
        {
            // Apollo (phase III) defect #636. comment this out leave origin as it is for test orders
            // 11/16/05 uncommenting per mickey to set origin to TEST for test orders on prod
            order.setOrderOrigin(originTest);
        }


        //Set payment values for b2b orders
        if(b2bOrder){
          payments = order.getPayments();
          if(payments != null && payments.size() > 0)
          {
              // Get order credit cards
              it = payments.iterator();
              payment = null;
              creditCard = null;
              ccNumber = null;
              String paymentMethodType = null;
              paymentType = null;
              List creditCards = null;
              while(it.hasNext())
              {
                  payment = (PaymentsVO)it.next();
                  paymentType = b2bPaymentTypeInvoice; //default payment type to invoice
                  paymentMethodType = paymentTypeInvoice; //default payment method type to invoice
                  creditCards = payment.getCreditCards();

                  //check if PCard (credit card type)
                  if(creditCards != null && creditCards.size() > 0)
                      {
                          creditCard = (CreditCardsVO)creditCards.get(0);
                          ccNumber = creditCard.getCCNumber();
                          ccType = creditCard.getCCType();
                          if(ccNumber != null && ccNumber.length() > 0)
                          {
                            paymentMethodType = paymentTypeCC;

                            //changed to set payment type to CC type, emueller, 3/17/04
                            //paymentType = b2bPaymentTypePCard;
                          }

                          if(ccType == null)
                          {
                            ccType = "";
                          }
                          paymentType = ccType;

                          //format expiration date
                          if(creditCard.getCCExpiration() != null)
                          {
                            try{
                                java.util.Date expDate = formatStringToUtilDate(creditCard.getCCExpiration());
                                SimpleDateFormat sdfExpDate = new SimpleDateFormat(CREDIT_CARD_EXP_FORMAT);
                                creditCard.setCCExpiration(sdfExpDate.format(expDate));
                            }
                            catch(Exception e)
                            {
                                logger.info("Could not parse credit card date. Validation will catch error. Date=" + creditCard.getCCExpiration());
                            }
                          }
                      }//end has credit cards

                //set payment type
                payment.setPaymentsType(paymentType);
                payment.setPaymentMethodType(paymentMethodType);

              }//end while more payments
          //else, order does not contain payments..add invoice payment object
          }
          else
          {
            PaymentsVO paymentVO = new PaymentsVO();
            paymentVO.setGuid(order.getGUID());
            paymentVO.setPaymentsType(b2bPaymentTypeInvoice);
            paymentVO.setPaymentMethodType(paymentTypeInvoice);
            List paymentList = new ArrayList();
            paymentList.add(paymentVO);
            order.setPayments(paymentList);
          }
        }//end is b2b order
        else if ( amazonOrder || mercentOrder || partnerOrder)
        {
          //only add a payment if one does not already exist on the order
          //Phase3 Defect 1638. Amazon and Walmart are getting multiple payments added to the order
          //This is because gatherer inserts and payment and then this code does.
          payments = order.getPayments();
          if(payments == null || payments.size() == 0)
          {
              PaymentsVO paymentVO = new PaymentsVO();
              paymentVO.setGuid(order.getGUID());
              paymentVO.setPaymentsType( configUtil.getProperty(METADATA_CONFIG_FILE_NAME,"PAYMENT_METHOD_INVOICE") );
              paymentVO.setPaymentMethodType( paymentTypeInvoice );
              List paymentList = new ArrayList();
              paymentList.add(paymentVO);
              order.setPayments(paymentList);
          }
        }



        ////////////////////// END Order header //////////////////////

        ////////////////////// BEGIN Order contact info //////////////////////
        if(order.getOrderContactInfo() != null && order.getOrderContactInfo().size() > 0)
        {
            OrderContactInfoVO contactInfo = null;
            it = order.getOrderContactInfo().iterator();
            phoneNumber = null;
            phoneExt = null;
            while(it.hasNext())
            {
                contactInfo = (OrderContactInfoVO)it.next();

                // BEGIN remove dashes, spaces and parenthese from phone numbers.
                // If the number is blank then make it a space.
                phoneNumber = contactInfo.getPhone();
                if(phoneNumber == null) phoneNumber = "";
                phoneNumber = removeAllSpecialChars(phoneNumber);

                phoneExt = contactInfo.getExt();
                if(phoneExt == null) phoneExt = "";
                phoneExt = removeAllSpecialChars(phoneExt);

                contactInfo.setPhone(phoneNumber);
                contactInfo.setExt(phoneExt);
                // END remove dashes, spaces and parenthese from phone numbers.
            }
        }
        ////////////////////// END Order contact info //////////////////////

        ////////////////////// BEGIN Membership //////////////////////
        // Convert membership IDs of all zeros and all spaces to blank string
        //membership =
        if(order.getMemberships().size()>0)
        {membership = (MembershipsVO)order.getMemberships().get(0);}
        if(membership != null)
        {
        String membershipFirstName = membership.getFirstName();
        if(membershipFirstName != null && mappingEnableFlag && characterMappingFlag)
        {
         m = p.matcher(membershipFirstName);
         if (m.find()) {membershipFirstName = LanguageUtility.specialCharacterMapping(membershipFirstName, spcmap);}
        }
        membership.setFirstName(membershipFirstName);

        String membershipLastName = membership.getLastName();
        if(membershipLastName != null && mappingEnableFlag && characterMappingFlag)
        {
         m = p.matcher(membershipLastName);
         if (m.find()) {membershipLastName = LanguageUtility.specialCharacterMapping(membershipLastName, spcmap);}
        }
        membership.setLastName(membershipLastName);

        String membershipIdNumber = membership.getMembershipIdNumber();
        if(membershipIdNumber != null && mappingEnableFlag && characterMappingFlag)
        {
         m = p.matcher(membershipIdNumber);
         if (m.find()) {membershipIdNumber = LanguageUtility.specialCharacterMapping(membershipIdNumber, spcmap);}
        }
        membership.setMembershipIdNumber(membershipIdNumber);

        String membershipType = membership.getMembershipType();
        if(membershipType != null && mappingEnableFlag && characterMappingFlag)
        {
         m = p.matcher(membershipType);
         if (m.find()) {membershipType = LanguageUtility.specialCharacterMapping(membershipType, spcmap);}
        }
        membership.setMembershipType(membershipType);
        }
        ////////////////////// END Membership //////////////////////

        ////////////////////// BEGIN Buyer //////////////////////
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            // Check Auto hold
            if(buyer.getAutoHold() == null || buyer.getAutoHold().length() < 1)
            {
                buyer.setAutoHold("N");
            }

            // Check best customer
            if(buyer.getBestCustomer() == null || buyer.getBestCustomer().length() < 1)
            {
                buyer.setBestCustomer("N");
            }

            String buyerMembershipId = buyer.getCustomerId();

            if(buyerMembershipId != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(buyerMembershipId);
                  if (m.find()) {buyerMembershipId = LanguageUtility.specialCharacterMapping(buyerMembershipId, spcmap);}
                }
            buyer.setCustomerId(buyerMembershipId);
            //Removed this code per Mickey & Julie, emueller, 3/22/04
            //-------------------------------------------------------
            // Remove special characters form the buyer name
            buyerFirstName = buyer.getFirstName();
            //if(buyerFirstName == null) buyerFirstName = "";
            //buyerFirstName = removeAllSpecialChars(buyerFirstName);

             if(buyerFirstName != null && mappingEnableFlag && characterMappingFlag)
             {
              m = p.matcher(buyerFirstName);
              if (m.find()) {buyerFirstName = LanguageUtility.specialCharacterMapping(buyerFirstName, spcmap);}
             }
            buyer.setFirstName(buyerFirstName);
            buyerLastName = buyer.getLastName();
            //if(buyerLastName == null) buyerLastName = "";
            //buyerLastName = removeAllSpecialChars(buyerLastName);
            if(buyerLastName != null && mappingEnableFlag && characterMappingFlag)
             {
              m = p.matcher(buyerLastName);
              if (m.find()) {buyerLastName = LanguageUtility.specialCharacterMapping(buyerLastName, spcmap);}
             }
            buyer.setLastName(buyerLastName);

            // Strip TARGET STORES verbiage from last name
            if (orderOrigin.equalsIgnoreCase(originAriba2))
            {
                buyerLastName = buyer.getLastName();
                if(buyerLastName == null) buyerLastName = "";
                buyerLastName = removeTargetStores(buyerLastName);
                buyer.setLastName(buyerLastName);
            }

            // Buyer address
            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);

                String buyerBusinesName = buyerAddress.getAddressEtc();

                if(buyerBusinesName != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(buyerBusinesName);
                  if (m.find()) {buyerBusinesName = LanguageUtility.specialCharacterMapping(buyerBusinesName, spcmap);}
                }
                buyerAddress.setAddressEtc(buyerBusinesName);

                address1 = buyerAddress.getAddressLine1();
                if(address1 == null)address1 = "";
                address2 = buyerAddress.getAddressLine2();
                if(address2 == null)
                address2 = "";

                if(address1 != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(address1);
                if (m.find()) {address1 = LanguageUtility.specialCharacterMapping(address1, spcmap);}
                }
                if(address2 != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(address2);
                  if (m.find()) {address2 = LanguageUtility.specialCharacterMapping(address2, spcmap);}
                }

                String buyerCity = null;
                buyerCity = buyerAddress.getCity();

                if(buyerCity != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(buyerCity);
                  if (m.find()) {buyerCity = LanguageUtility.specialCharacterMapping(buyerCity, spcmap);}
                }
                buyerAddress.setCity(buyerCity);
                // Remove spaces and dashes from buyer zip code
                zipCode = buyerAddress.getPostalCode();
                if(zipCode == null)zipCode = "";
                zipCode = StringReplacer.stripStringOf(zipCode, '-');
                zipCode = StringReplacer.stripStringOf(zipCode, ' ');

                if(zipCode != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(zipCode);
                  if (m.find()) {zipCode = LanguageUtility.specialCharacterMapping(zipCode, spcmap);}
                }
                buyerAddress.setPostalCode(zipCode);

                // BEGIN Breaking address line one into 2 lines if longer than 30 characters
                address1 = StringReplacer.stripStringOf(address1, '$');
                address2 = StringReplacer.stripStringOf(address2, '$');
                String[] addresses = breakUpAddress(address1, address2);
                buyerAddress.setAddressLine1(addresses[0]);
                buyerAddress.setAddressLine2(addresses[1]);

                // BEGIN Convert country to a country code
                // JMP 12/02/2003 - Added section to lookup buyer country per
                // OrderScrub_int defect 69
                buyerCountry = null;
                //countryCode = null;
                buyerCountry = buyerAddress.getCountry();
                if(buyerCountry == null) buyerCountry = "";
                buyerCountry = buyerCountry.toUpperCase();

                Iterator countryListIt = null;
                if(buyerCountry.length() == 2)
                {
                    // Check to make sure the code exists
                    countryListIt = countryList.iterator();
                    countryVo = null;
                    boolean foundCountry = false;
                    while(countryListIt.hasNext())
                    {
                        countryVo = (CountryMasterVO)countryListIt.next();
                        if(countryVo.getCountryId().equalsIgnoreCase(buyerCountry))
                        {
                            foundCountry = true;
                            break;
                        }
                    }
                    if(!foundCountry)
                    {
                        // If not found then set to blank String
                        buyerAddress.setCountry("");
                    }
                    /*
                    if(!countryMap.containsValue(buyerCountry))
                    {
                        // If not found then set to blank String
                        buyerAddress.setCountry("");
                    }
                    */
                }
                else
                {
                    // Try converting to a code
                    boolean foundCountry = false;
                    countryListIt = countryList.iterator();
                    countryVo = null;
                    while(countryListIt.hasNext())
                    {
                        countryVo = (CountryMasterVO)countryListIt.next();
                        if(countryVo.getCountryName().equalsIgnoreCase(buyerCountry))
                        {
                            foundCountry = true;

                            break;
                        }
                    }

                    if(countryVo.getCountryId() == null) countryVo.setCountryId("");
                    if(foundCountry)
                    {
                        buyerAddress.setCountry(countryVo.getCountryId());
                    }
                    else
                    {
                        buyerAddress.setCountry("");
                    }
                    /*
                    countryCode = (String)countryMap.get(buyerCountry);
                    if(countryCode == null) countryCode = "";

                    buyerAddress.setCountry(countryCode);
                    */
                }

                // BEGIN Convert state to a code if US or CA
                state = null;
                stateCode = null;
                buyerCountry = buyerAddress.getCountry();
                if(buyerCountry == null) buyerCountry = "";
                state = buyerAddress.getStateProv();
                if(state == null) state = "";

                // JMP 1/2/2004
                // Added for QA defect 107
                if(state.equalsIgnoreCase("not applicable") || state.equalsIgnoreCase("notapplicable"))
                {
                    state = "NA";
                }

                if(buyerCountry.equals(canadaCountryCode) || buyerCountry.equals(usCountryCode))
                {
                    state = state.toUpperCase();

                    if(state.length() == 2)
                    {
                     // Check to make sure the code exists
                        if(!stateByIdMap.containsKey(state))
                        {
                            // If not found then set to blank String
                            buyerAddress.setStateProv("NA");
                        }
                        else
                        {
                            buyerAddress.setStateProv(state);
                        }
                    }
                    else
                    {
                        // Try converting to a code
                        StateMasterVO stateVo = (StateMasterVO)stateByNameMap.get(state);
                        stateCode = null;
                        if(stateVo == null)
                        {
                            stateCode = "NA";
                        }
                        else
                        {
                            stateCode = stateVo.getStateMasterId();
                        }

                        buyerAddress.setStateProv(stateCode);
                    }
                }
                else
                {
                    // OrderScrub_int defect 65
                    // Default to NA if state is null
                    if(state.length() < 1)
                    {
                        buyerAddress.setStateProv("NA");
                    }
                    else
                    {
                        buyerAddress.setStateProv(state);
                    }
                }

                // Convert address codes and descriptions to ADDRESS_TYPES
                // This section converts address type codes to address types so they
                // conform to the database constraints
                typeIt = null;
                typeKey = null;
                addType = buyerAddress.getAddressType();
                if(addType == null) addType = "";
                addType = addType.toUpperCase();

                // Try to convert it if one character
                if(addType.length() == 1)
                {
                    if(addressTypes.containsValue(addType))
                    {
                        typeIt = addressTypes.keySet().iterator();
                        while(typeIt.hasNext())
                        {
                            typeKey = (String)typeIt.next();
                            if(addressTypes.get(typeKey).equals(addType))
                            {
                                buyerAddress.setAddressType(typeKey);
                                break;
                            }
                        }
                    }

                    // If the type is O convert to B
                    if(addType.equals("O"))
                    {
                        buyerAddress.setAddressType(addressTypeB);
                    }
                }
                // if longer than one check if it exists.  If does not exist default to home
                else if(!addressTypes.containsKey(addType))
                {
                    buyerAddress.setAddressType(addressTypeR);
                }
            }

            // Get the buyer email
            if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
            {
                buyerEmail = ((BuyerEmailsVO)buyer.getBuyerEmails().get(0)).getEmail();

                if(buyerEmail == null) buyerEmail = "";

                // Change buyer email to lower case
                buyerEmail = buyerEmail.toLowerCase();
                ((BuyerEmailsVO)buyer.getBuyerEmails().get(0)).setEmail(buyerEmail);

                // #24607 - Premier circle
                if(((BuyerEmailsVO)buyer.getBuyerEmails().get(0)).isChanged()) {
                	isBuyerEmailChanged = true;
                }
            }

            // Get the buyer phone numbers
            phoneNumber = null;
            // Find if buyer country is domestic
            String tmpBuyerCountry = null;
            if(buyerAddress != null && buyerAddress.getCountry() != null)
            {
                tmpBuyerCountry = buyerAddress.getCountry();
            }
            CountryMasterVO buyerCountryVo = findCountry(tmpBuyerCountry, countryList);
            if(buyer != null && buyerAddress != null && buyerCountryVo != null && buyerCountryVo.getCountryType().equalsIgnoreCase("D") && buyer.getBuyerPhones() != null)
            {
                for(int i = 0; i < buyer.getBuyerPhones().size(); i++)
                {
                    buyerPhone = (BuyerPhonesVO)buyer.getBuyerPhones().get(i);

                    // BEGIN remove dashes, spaces and parenthese from phone numbers.
                    // If the number is blank then make it a space.
                    phoneNumber = buyerPhone.getPhoneNumber();
                    if(phoneNumber == null) phoneNumber = "";
                    phoneNumber = removeAllSpecialChars(phoneNumber);
                    // END remove dashes, spaces and parenthese from phone numbers.
                    // If the number is blank then make it a space

                        // Convert none, n/a, na, unknown to 10 zeros..home numbers only
                        //This was added per Julie Eggerts request, 3/16/04
                        if(buyerPhone.getPhoneType() != null && buyerPhone.getPhoneType().equalsIgnoreCase(PHONE_TYPE_HOME)){
                              if(phoneNumber.toUpperCase().indexOf("NONE") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("N/A") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("NA") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("SAME") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("SAMEASABOVE") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("SAMEASDAY") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("SAMEASDAYTIME") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("SAA") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("NA") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("NA") != -1 ||
                                 phoneNumber.toUpperCase().indexOf("UNKNOWN") != -1)
                              {
                                  phoneNumber = "0000000000";
                              }
                        }

                    // Parse out extension from phone number
                    // JMP 1/13/2004 - Changed the way extensions are parsed
                    // based on QA defect 93.  The extenstion should only be
                    // parse if the phone number contains any of the following
                    // words: ext, ext., x or extension
                    if(checkForExt(phoneNumber))
                    {
                        StringBuffer matchType = new StringBuffer();
                        int j = extensionExists(phoneNumber, matchType);
                        phoneExt = FieldUtils.stripNonNumeric(phoneNumber.substring(j));
                        if(phoneExt == null) phoneExt = "";
                        phoneNumber = phoneNumber.substring(0, (j - matchType.length()));
                    }
                    else
                    {
                        // use extension from object
                        phoneExt = buyerPhone.getExtension();
                        if(phoneExt == null) phoneExt = "";
                        //phoneExt = removeAllSpecialChars(phoneExt);
                        // Remove everything except digits
                        phoneExt = removeAllChars(phoneExt);
                    }

                    if(phoneExt.equals("0") || phoneExt.equalsIgnoreCase("NA") || phoneExt.equalsIgnoreCase("NONE"))
                    {
                        phoneExt = null;
                    }


                    if(phoneNumber != null && mappingEnableFlag && characterMappingFlag)
                    {
                    	m = p.matcher(phoneNumber);
                    	if (m.find()) {phoneNumber = LanguageUtility.specialCharacterMapping(phoneNumber, spcmap);}
                    }

                    if(phoneExt != null && mappingEnableFlag && characterMappingFlag)
                    {
                    	m = p.matcher(phoneExt);
                    	if (m.find()) {phoneExt = LanguageUtility.specialCharacterMapping(phoneExt, spcmap);}
                    }
                    buyerPhone.setPhoneNumber(phoneNumber);
                    buyerPhone.setExtension(phoneExt);
                }
            }
        }
        if(buyerEmail == null) buyerEmail = "";
        ////////////////////// END Buyer //////////////////////

        ////////////////////// BEGIN Payments //////////////////////
        payments = order.getPayments();
        if(payments != null && payments.size() > 0)
        {
            // Get order credit cards
            it = payments.iterator();
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                paymentType = payment.getPaymentMethodType();
                if(paymentType == null) paymentType = "";
                if(paymentType.equals(paymentTypeCC))
                {
                    if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                    {
                        creditCard = (CreditCardsVO)payment.getCreditCards().get(0);

                        // Remove spaces and dashes from the credit card numbers
                        ccNumber = creditCard.getCCNumber();
                        if(ccNumber == null) ccNumber = "";

                        ccNumber = StringReplacer.stripStringOf(ccNumber, '-');
                        ccNumber = StringReplacer.stripStringOf(ccNumber, ' ');

                        creditCard.setCCNumber(ccNumber);

                        ccExpDate = creditCard.getCCExpiration();
                        if(ccExpDate == null) ccExpDate = "";
                        ccType = creditCard.getCCType();
                        if(ccType == null) ccType = "";
                        ccNumber = creditCard.getCCNumber();
                        if(ccNumber == null) ccNumber = "";

                        // Convert exp date if MM/dd/yy format
                        ccExpDate = creditCard.getCCExpiration();
                        if(ccExpDate == null) ccExpDate = "";
                        if(ccExpDate.length() == 8)
                        {
                            try
                            {
                                ccDate = sdf1.parse(ccExpDate);
                                ccExpDate = sdf2.format(ccDate);
                                creditCard.setCCExpiration(ccExpDate);
                            }
                            catch(ParseException pe)
                            {
                                logger.error("Could not parse credit card expiration date to convert to the format MM/yy");
                            }
                        }

                        // If the CC type is empty try to find its type from the CC number
                        if(ccType.length() < 1 && ccNumber.length() > 0)
                        {
                            ccType = findCCType(ccNumber);

                            creditCard.setCCType(ccType);

                            //only set the payment type if this NOT an Ariba order
                            //Ariba orders should leave the payment type as PCard)
//                            if(!b2bOrder){
                              payment.setPaymentsType(ccType);
//                            }

                            payment.setPaymentMethodType(paymentTypeCC);
                        }
                    }
                }
                else if(paymentType.equals(paymentTypeInvoice))
                {
                    // BEGIN if no credit card information is associated with the order and
                    // it is an invoice, the
                    // source code should be checked against the CPC_Info table to see if
                    // the source code is on the table.  If the source code is found, the
                    // credit card information that is on the table should be used for the
                    // order.  OrderScrub_int defect 101
                    boolean cpcInfoFound = getCPCPaymentInfo(con,payment,sourceVo.getSourceCode(),paymentTypeCC);
                    // Apollo project (phase III) defect 722
                    if (!cpcInfoFound)
                    {
                      payment.setPaymentsType("IN");
                    }

                }//end else payment type invoice
            }//while has credit cards
        }//end if payment size > 0
        else
        {
            //There are no payments associated with this order.
            //Check if there is payment information in the CPC table for this source code
            PaymentsVO newPayment = new PaymentsVO();
            boolean cpcInfoFound = getCPCPaymentInfo(con,newPayment,sourceVo.getSourceCode(),paymentTypeCC);

            if(cpcInfoFound)
            {
              List newPaymentList = new ArrayList();
              newPaymentList.add(newPayment);
              order.setPayments(newPaymentList);
            }

        }//end else payment size = 0

        if(ccType == null) ccType = "";
        if(ccExpDate == null) ccExpDate = "";
        if(ccNumber == null) ccNumber = "";

        ////////////////////// End Payments //////////////////////

        // BEGIN if the order received is a JCPenney order and the employee
        // email address ends with jcpenney.com and the credit cart type is not
        // set to IN, then the source code should be changed to the default JCP
        // employee source code, 7731.
        // OrderScrub_int defect 103
        buyerEmail = buyerEmail.toLowerCase();
        if(sourceVo.getJcpenneyFlag().equalsIgnoreCase("Y") && !ccType.equals("IN") && buyerEmail.endsWith("jcpenney.com"))
        {
            order.setSourceCode(jcpEmployeeSourceCode);
        }
        // END if the order received is a JCPenney order and the employee
        // email address ends with jcpenney.com and the credit cart type is not set
        // to IN, then the source code should be changed to the default JCP
        // employee source code, 7731.
        // OrderScrub_int defect 103



        ////////////////////// BEGIN Items //////////////////////
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            //need to make sure SubstituteAcknowledgement is either Y or N
            String substitue = item.getSubstituteAcknowledgement();
            if(substitue != null && (substitue.equalsIgnoreCase("true") || substitue.equalsIgnoreCase("Y")))
            {
                item.setSubstituteAcknowledgement("Y");
            }
            else
            {
                item.setSubstituteAcknowledgement("N");
            }

            if(!amazonOrder && !mercentOrder && !partnerOrder)
            {
              //For non-amazon orders and non-mercent, default quantity to 1
              // BEGIN set quantity to 1 (defect 85)
              item.setQuantity("1");
            }

            //truncate signature if one exists.  emueller, 2/17/04 - Defect #283
            String signature = item.getCardSignature();
            if(signature !=null && item.getCardSignature().length() > SIGNATURE_MAX_LENGTH)
            {
                signature = item.getCardSignature().substring(0, SIGNATURE_MAX_LENGTH);

                if(signature != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(signature);
                  if (m.find()) {signature = LanguageUtility.specialCharacterMapping(signature, spcmap);}
                }
                item.setCardSignature(signature);
            }

            // Default sender release flag
            if(item.getSenderInfoRelease() == null)
            {
                item.setSenderInfoRelease("N");
            }


            // BEGIN Convert novator product ID to HP product ID
            if( novatorOrder || mobiOrder || tabletOrder || (b2bOrder && item.getStatus().equals(receviedItemStatus)) )
            {
                // Get HP product ID
                hpProdId = null;
                dataRequest.setConnection(con);
                dataRequest.setStatementID(GET_HP_PRODUCT_ID);
                dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductId());
                rs = (CachedResultSet)vdau.execute(dataRequest);
                rs.reset();
                dataRequest.reset();

                while(rs.next())
                {
                    hpProdId = (String)rs.getObject(1);
                }

                // Set HP product ID
                if(hpProdId != null)
                {
                    item.setProductId(hpProdId);
                }
            }
            // END Convert novator product ID to HP product ID

            /*
             * COLOR CODES NO LONGER USED FOR ORIGINAL PURPOSE - SO COLOR LOGIC THAT
             * WAS HERE WAS REMOVED MARCH 2018. COLOR TABLES HAVE BEEN REPURPOSED AS
             * PRODUCT MARKERS FOR PROJECT FRESH.
             */


            // Convert occasion name to its ID
            String code = null;
            String desc = null;
            occasionId = item.getOccassionId();
            if(occasionId == null) occasionId = "";
            occasionId = occasionId.toUpperCase();
            Iterator occasionIt = occasionMap.keySet().iterator();
            boolean foundOccasion = false;
            while(occasionIt.hasNext())
            {
                code = (String)occasionIt.next();
                desc = ((String)occasionMap.get(code)).toUpperCase();

                if(desc.equals(occasionId))
                {
                    item.setOccassionId(code);
                    foundOccasion = true;
                    break;
                }
                else if(code.equals(occasionId))
                {
                    foundOccasion = true;
                    break;
                }
            }

            // If the last minute gift email is not null then convert it to lower case
            if(item.getLastMinuteGiftEmail() != null)
            {
                item.setLastMinuteGiftEmail(item.getLastMinuteGiftEmail().toLowerCase());
            }

            // if the occasion code is not found then set it to the default
            if(!foundOccasion)
            {
                item.setOccassionId(defaultOccasionCode);
                specialInstructions = item.getSpecialInstructions();
                if(specialInstructions == null) specialInstructions = "";

                if(specialInstructions != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(specialInstructions);
                  if (m.find()) {specialInstructions = LanguageUtility.specialCharacterMapping(specialInstructions, spcmap);}
                }
                item.setSpecialInstructions(specialInstructions + "OCCASION=" + occasionId);
            }

            String cardMessage = item.getCardMessage();
            if(cardMessage != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(cardMessage);
                  if (m.find()) {cardMessage = LanguageUtility.specialCharacterMapping(cardMessage, spcmap);}
                }
            item.setCardMessage(cardMessage);

            String orderComments = item.getOrderComments();

            if(orderComments != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(orderComments);
                  if (m.find()) {orderComments = LanguageUtility.specialCharacterMapping(orderComments, spcmap);}
                }
            item.setOrderComments(orderComments);

            // BEGIN AVS address
            if(item.getAvsAddress() == null) {
            	avsAddress = new AVSAddressVO();
            	avsAddress.setAvsPerformed("N");
            	resultCode = "";
            	avsAddress1 = "";
            	avsCity = "";
            	avsState = "";
            	avsZip = "";
            	avsOverrideFlag = "";
            } else {
                avsAddress = item.getAvsAddress();

                resultCode = avsAddress.getResult();
                avsAddress1 = avsAddress.getAddress();
                avsCity = avsAddress.getCity();
                avsState = avsAddress.getStateProvince();
                avsZip = avsAddress.getPostalCode();
                avsOverrideFlag = avsAddress.getOverrideflag();
            }
            if(resultCode == null) resultCode = "";
            if(avsAddress1 == null) avsAddress1 = "";
            if(avsCity == null) avsCity = "";
            if(avsState == null) avsState = "";
            if(avsZip == null) avsZip = "";
            if(avsOverrideFlag == null) avsOverrideFlag = "N";

            // BEGIN Recipient
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                // Default auto hold flag
                if(recipient != null && (recipient.getAutoHold() == null || recipient.getAutoHold().length() < 1))
                {
                    recipient.setAutoHold("N");
                }

                //Per Julie this code is being commented out, emueller 3/23/04
                //-------------------------------------------------------------
                // Remove special characters form the recipient name
                recipFirstName = recipient.getFirstName();
                //if(recipFirstName == null) recipFirstName = "";
                //recipFirstName = removeAllSpecialChars(recipFirstName);
                if(recipFirstName != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(recipFirstName);
                  if (m.find()) {recipFirstName = LanguageUtility.specialCharacterMapping(recipFirstName, spcmap);}
                }
                recipient.setFirstName(recipFirstName);
                recipLastName = recipient.getLastName();
                //if(recipLastName == null) recipLastName = "";
                //recipLastName = removeAllSpecialChars(recipLastName);
                if(recipLastName != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(recipLastName);
                  if (m.find()) {recipLastName = LanguageUtility.specialCharacterMapping(recipLastName, spcmap);}
                }
                recipient.setLastName(recipLastName);

                // Recipient address
                if(recipient != null && recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    // Convert the country name to a code
                    //countryCode = null;

                    recipCountry = recipAddress.getCountry();
                    if(recipCountry == null) recipCountry = "";
                    recipCountry = recipCountry.toUpperCase();

                    Iterator countryListIt = null;
                    if(recipCountry.length() == 2)
                    {
                        // Check to make sure the code exists
                        countryListIt = countryList.iterator();
                        countryVo = null;
                        boolean foundCountry = false;
                        while(countryListIt.hasNext())
                        {
                            countryVo = (CountryMasterVO)countryListIt.next();
                            if(countryVo.getCountryId().equalsIgnoreCase(recipCountry))
                            {
                                foundCountry = true;
                                // Set international flag
                                if(countryVo.getCountryType() != null && countryVo.getCountryType().equalsIgnoreCase("D"))
                                {
                                    recipAddress.setInternational("N");
                                }
                                else
                                {
                                    recipAddress.setInternational("Y");
                                }
                                break;
                            }
                        }
                        if(!foundCountry)
                        {
                            // If not found then set to blank String
                            recipAddress.setCountry("");
                        }
                        /*
                        if(!countryMap.containsValue(recipCountry))
                        {
                            // If not found then set to blank String
                            recipAddress.setCountry("");
                        }
                        */
                    }
                    else
                    {
                        // Try converting to a code
                        boolean foundCountry = false;
                        countryListIt = countryList.iterator();
                        countryVo = null;
                        while(countryListIt.hasNext())
                        {
                            countryVo = (CountryMasterVO)countryListIt.next();
                            if(countryVo.getCountryName().equalsIgnoreCase(recipCountry))
                            {
                                foundCountry = true;
                                break;
                            }
                        }

                        if(countryVo.getCountryId() == null) countryVo.setCountryId("");
                        if(foundCountry)
                        {
                            recipAddress.setCountry(countryVo.getCountryId());
                            if(countryVo.getCountryType() != null && countryVo.getCountryType().equalsIgnoreCase("D"))
                            {
                                recipAddress.setInternational("N");
                            }
                            else
                            {
                                recipAddress.setInternational("Y");
                            }
                        }
                        else
                        {
                            recipAddress.setCountry("");
                        }
                        /*
                        countryCode = (String)countryMap.get(recipCountry);
                        if(countryCode == null) countryCode = "";

                        recipAddress.setCountry(countryCode);
                        */
                    }
                    // END Convert country to a country code

                    recipCountry = recipAddress.getCountry();
                    if(recipCountry == null) recipCountry = "";
                    if(recipCountry.equalsIgnoreCase(usCountryCode))
                    {
                        // If avs was not passed or the override flag = Y then use the
                        // customer entered fields for ship to values.  Otherwise use
                        // the AVS values for address1, city, state and zip.
                        if(!GeneralConstants.AVS_VERIFIED_ADDRESS_PASS.equals(resultCode)
                                || GeneralConstants.AVS_YES.equals(avsOverrideFlag))
                        {
                            // Do nothing
                        }
                        else
                        {
                            // Make sure AVS was performed in case AVS is unavailable to turned off
                            if (GeneralConstants.AVS_YES.equals(avsAddress.getAvsPerformed()))
                            {
                                recipAddress.setAddressLine1(avsAddress1);
                                if(recipAddress.getAddressLine2() != null && !avsAddress1.contains(recipAddress.getAddressLine2())){
                                	recipAddress.setAddressLine2(recipAddress.getAddressLine2());
                                }
                                else {
                                	recipAddress.setAddressLine2("");
                                }

                                recipAddress.setCity(avsCity);
                                recipAddress.setStateProvince(avsState);
                                recipAddress.setPostalCode(avsZip);
                            }
                        }
                    }
                    else
                    {
                        // International so use AVS address since AVS address is what comes
                        // from the scrub screen

                    }

                    address1 = recipAddress.getAddressLine1();
                    if(address1 == null) address1 = "";
                    address2 = recipAddress.getAddressLine2();
                    if(address2 == null) address2 = "";


                    if(address1 != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(address1);
                  if (m.find()) {address1 = LanguageUtility.specialCharacterMapping(address1, spcmap);}
                }
                if(address2 != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(address2);
                  if (m.find()) {address2 = LanguageUtility.specialCharacterMapping(address2, spcmap);}
                }
                    // Remove spaces and dashes from recipient zip code
                    zipCode = recipAddress.getPostalCode();
                    if(zipCode == null) zipCode = "";
                    zipCode = StringReplacer.stripStringOf(zipCode, '-');
                    zipCode = StringReplacer.stripStringOf(zipCode, ' ');

                    if(zipCode != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(zipCode);
                  if (m.find()) {zipCode = LanguageUtility.specialCharacterMapping(zipCode, spcmap);}
                }
                    recipAddress.setPostalCode(zipCode);

                    // Break address into two lines if over 30 characters
                    address1 = address1.replace('$',' ');
                    address2 = address2.replace('$',' ');
                    String[] addresses = breakUpAddress(address1, address2);
                    recipAddress.setAddressLine1(addresses[0]);
                    recipAddress.setAddressLine2(addresses[1]);

                    String recipCity = recipAddress.getCity();

                    if(recipCity != null && mappingEnableFlag && characterMappingFlag)
                {
                  m = p.matcher(recipCity);
                  if (m.find()) {recipCity = LanguageUtility.specialCharacterMapping(recipCity, spcmap);}
                }
                    recipAddress.setCity(recipCity);
                    state = null;
                    stateCode = null;
                    recipCountry = recipAddress.getCountry();
                    if(recipCountry == null) recipCountry = "";

                    state = recipAddress.getStateProvince();
                    if(state == null) state = "";

                    // JMP 1/2/2004
                    // Added for QA defect 107
                    if(state.equalsIgnoreCase("not applicable"))
                    {
                        state = "NA";
                    }

                    if(recipCountry.equals(canadaCountryCode) || recipCountry.equals(usCountryCode))
                    {
                        state = state.toUpperCase();

                        if(state.length() == 2)
                        {
                         // Check to make sure the code exists
                            if(!stateByIdMap.containsKey(state))
                            {
                                // If not found then set to blank String
                                recipAddress.setStateProvince("NA");
                            }
                            else
                            {
                                recipAddress.setStateProvince(state);
                            }
                        }
                        else
                        {
                            // Try converting to a code
                            StateMasterVO stateVo = (StateMasterVO)stateByNameMap.get(state);
                            stateCode = null;
                            if(stateVo == null)
                            {
                                stateCode = "NA";
                            }
                            else
                            {
                                stateCode = stateVo.getStateMasterId();
                            }

                            recipAddress.setStateProvince(stateCode);
                        }
                    }
                    else
                    {
                        // OrderScrub_int defect 65
                        // Default to NA if state is null
                        if(state.length() < 1)
                        {
                            recipAddress.setStateProvince("NA");
                        }
                        else
                        {
                            recipAddress.setStateProvince(state);
                        }
                    }
                    // END Convert state to a code if US or CA

                    // BEGIN  if the order received is a JCPenney order, and the ship to
                    // country is not equal to US the source code should be changed to the
                    // default JCP International Web Source Code (7588)
                    // OrderScrub_int defect 102
                    if(sourceVo.getSourceCode().length() > 0 && sourceVo.getJcpenneyFlag().equalsIgnoreCase("Y") && !recipCountry.equals("US"))
                    {
                        order.setSourceCode(jcpIntSourceCode);
                    }
                    //Here it keep actual address type.i.e before overriding.
                    actualAddressType = recipAddress.getAddressType();
                    // Set default address type if not set
                    if(recipAddress.getAddressType() == null)
                    {
                        recipAddress.setAddressType(addressTypeR);
                    }

                    addType = recipAddress.getAddressType();
                    if(addType == null) addType = "";
                    addType = addType.toUpperCase();

                    // Try to convert it if one character
                    if(addType.length() == 1)
                    {
                        if(addressTypes.containsValue(addType))
                        {
                            typeIt = addressTypes.keySet().iterator();
                            while(typeIt.hasNext())
                            {
                                typeKey = (String)typeIt.next();
                                if(addressTypes.get(typeKey).equals(addType))
                                {
                                    recipAddress.setAddressType(typeKey);
                                    break;
                                }
                            }
                        }

                        // If the type is O convert to B
                        if(addType.equals("O"))
                        {
                            recipAddress.setAddressType(addressTypeB);
                        }
                    }
                    // if longer than one check if it exists.  If does not exist default to home
                    else if(!addressTypes.containsKey(addType))
                    {
                        recipAddress.setAddressType(addressTypeR);
                    }
                    // END translating ADDRESS TYPE to its code

                    // Run through address type rules
                    shipToName = recipAddress.getName();

                    if(shipToName == null) shipToName = "";
                    shipToName = shipToName.trim();

                    // Truncate ship to name at 40 chatacters
                    if(shipToName.length() > SHIP_TO_NAME_MAX_LENGTH)
                    {
                        shipToName = shipToName.substring(0, SHIP_TO_NAME_MAX_LENGTH);
                        recipAddress.setName(shipToName);
                    }

                    shipToName = shipToName.toUpperCase().trim();

                    shipToType = recipAddress.getAddressType();
                    if(shipToType == null) shipToType = "";

                    if(shipToType != null && mappingEnableFlag && characterMappingFlag)
                    {
                      m = p.matcher(shipToType);
                      if (m.find()) {shipToType = LanguageUtility.specialCharacterMapping(shipToType, spcmap);}
                    }
                    // OrderScrub_int defect 86
                    // If the ship to name is '-' or 'N/A' and the ship to type
                    // is 'R' then the ship to name should be nulled.
                    if((shipToName.equalsIgnoreCase("-") || shipToName.equalsIgnoreCase("N/A") ||
                        shipToName.equalsIgnoreCase("NA")) && shipToType.equalsIgnoreCase(addressTypeR))
                    {
                        recipAddress.setName(null);
                        shipToName = "";
                    }

                    // OrderScrub_int defect 87
                    // According to the Data Massager document, if the ship to
                    // name contains 'HOSP' (not case senstive) and the ship to
                    // type is 'R', the ship to type should be changed to 'H'.
                    if(shipToName.indexOf("HOSP") != -1 && shipToType.equalsIgnoreCase(addressTypeR))
                    {
                        recipAddress.setAddressType(addressTypeH);
                    }

                    // OrderScrub_int defect 88
                    // According to the Data Massager document, if the ship to
                    // name contains 'Funeral' or 'Mortuary' (not case senstive)
                    // and the ship to type is 'R', the ship to type should be
                    // changed to 'F'.
                    if((shipToName.indexOf("FUNERAL") != -1 || shipToName.indexOf("MORTUARY") != -1) &&
                       shipToType.equalsIgnoreCase(addressTypeR))
                    {
                        recipAddress.setAddressType(addressTypeF);
                    }
                    if((shipToName.indexOf("CEMETERY") != -1) &&
                        shipToType.equalsIgnoreCase(addressTypeR))
                    {
                        recipAddress.setAddressType(addressTypeC);
                    }

                    // OrderScrub_int defect 89
                    // According to the Data Massager document, if the ship to
                    // name contains a value other than 'HOSP', 'Funeral' or
                    // 'MORTUARY' and the ship to type is 'R', the ship to type
                    // should be changed to 'B'.
                    if((shipToName.indexOf("FUNERAL") == -1 && shipToName.indexOf("MORTUARY") == -1 && shipToName.indexOf("HOSP") == -1 && shipToName.indexOf("CEMETERY") == -1) &&
                       shipToType.equalsIgnoreCase(addressTypeR) && shipToName.length() > 0)
                    {
                        recipAddress.setAddressType(addressTypeB);
                    }

                    // OrderScrub_int defect 90
                    // According to the Data Massager document, if the ship to
                    // name is null and the ship to type is 'B', the ship to
                    // type should be changed to 'R'.
                    if(shipToName.length() == 0 && shipToType.equalsIgnoreCase(addressTypeB))
                    {
                        recipAddress.setAddressType(addressTypeR);
                    }

                    //
                    shipToName = recipAddress.getName();

                    if(shipToName != null && mappingEnableFlag && characterMappingFlag)
                    {
                    	m = p.matcher(shipToName);
                    	if (m.find()) {shipToName = LanguageUtility.specialCharacterMapping(shipToName, spcmap);
                    	recipAddress.setName(shipToName);
                    	}
                    }

                    String shipToTypeInfo = recipAddress.getInfo();
                    if(shipToTypeInfo != null && mappingEnableFlag && characterMappingFlag)
                    {
                    	m = p.matcher(shipToTypeInfo);
                    	if (m.find()) {shipToTypeInfo = LanguageUtility.specialCharacterMapping(shipToTypeInfo, spcmap);
                    	recipAddress.setInfo(shipToTypeInfo);
                    	}
                    }//
                    // END Other ship to rules
                }

                // Get the buyer phone numbers
                if(recipient != null && recipAddress != null && recipAddress.getInternational() != null && recipAddress.getInternational().equalsIgnoreCase("N") && recipient.getRecipientPhones() != null)
                {
                    for(int i = 0; i < recipient.getRecipientPhones().size(); i++)
                    {
                        recipPhone = (RecipientPhonesVO)recipient.getRecipientPhones().get(i);

                        // BEGIN remove dashes, spaces and parenthese from phone numbers.
                        // If the number is blank then make it a space
                        phoneNumber = recipPhone.getPhoneNumber();
                        if(phoneNumber == null) phoneNumber = "";
                        phoneNumber = removeAllSpecialChars(phoneNumber);
                        // Convert none, n/a, na, unknown to 10 zeros
                        if(phoneNumber.toUpperCase().indexOf("NONE") != -1 ||
                           phoneNumber.toUpperCase().indexOf("N/A") != -1 ||
                           phoneNumber.toUpperCase().indexOf("NA") != -1 ||
                           phoneNumber.toUpperCase().indexOf("UNKNOWN") != -1)
                        {
                            phoneNumber = "0000000000";

                        }

                        // Parse out extension from phone number
                        // JMP 1/13/2004 - Changed the way extensions are parsed
                        // based on QA defect 93.  The extenstion should only be
                        // parse if the phone number contains any of the following
                        // words: ext, ext., x or extension
                        if(checkForExt(phoneNumber))
                        {
                            StringBuffer matchType = new StringBuffer();
                            int j = extensionExists(phoneNumber, matchType);
                            phoneExt = FieldUtils.stripNonNumeric(phoneNumber.substring(j));
                            if(phoneExt == null) phoneExt = "";
                            phoneNumber = phoneNumber.substring(0, (j - matchType.length()));
                        }
                        else
                        {
                            // use extension from object
                            phoneExt = recipPhone.getExtension();
                            if(phoneExt == null) phoneExt = "";
                            //phoneExt = removeAllSpecialChars(phoneExt);
                            // Remove everything except digits
                            phoneExt = removeAllChars(phoneExt);
                        }

                        if(phoneExt.equals("0") || phoneExt.equalsIgnoreCase("NA") || phoneExt.equalsIgnoreCase("NONE"))
                        {
                            phoneExt = null;
                        }

                        if(phoneNumber != null && mappingEnableFlag && characterMappingFlag)
                        {
                        	m = p.matcher(phoneNumber);
                        	if (m.find()) {phoneNumber = LanguageUtility.specialCharacterMapping(phoneNumber, spcmap);}
                        }

                        if(phoneExt != null && mappingEnableFlag && characterMappingFlag)
                        {
                        	m = p.matcher(phoneExt);
                        	if (m.find()) {phoneExt = LanguageUtility.specialCharacterMapping(phoneExt, spcmap);}
                        }
                        recipPhone.setPhoneNumber(phoneNumber);
                        recipPhone.setExtension(phoneExt);
                    }
                }
            }
            // END Recipient

            shipDate = "";
            //shipMethod = null;
            //deliveryDate = null;
            //vendorId = null;
            prodSubCodeId = null;

            // BEGIN Check for this product being a sub-code
            // If this is a subcode then fill in the sub-code ID, Master ID,
            // active flag (status),
            prodSubCodeId = item.getProductSubCodeId();
            if(prodSubCodeId != null && prodSubCodeId.length() > 1)
            {
                prodSubCodeId = item.getProductSubCodeId();
            }
            else
            {
                prodSubCodeId = item.getProductId();
            }

            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_PRODUCT_SUBCODE);
            dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, prodSubCodeId);
            rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();

            String subCodeId = null;
            String masterId = null;
            //String activeFlag = null;
            Object price = null;
            String variablePriceFlag = null;

            while(rs.next())
            {
                subCodeId = (String)rs.getObject(2);
                masterId = (String)rs.getObject(1);
                //activeFlag = (String)rs.getObject(7);
                price = rs.getObject(4);
            }

            if(subCodeId != null)
            {
                item.setProductId(masterId);
                item.setProductSubCodeId(subCodeId);
                //item.setProductsAmount(price.toString());
            }
            // END Check for this product being a sub-code

            // Get product details
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_PRODUCT_DETAILS);
            dataRequest.addInputParam(ValidationConstants.PRODUCT_ID, item.getProductId());
            rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();

            BigDecimal standardPrice = null;
            BigDecimal deluxePrice = null;
            BigDecimal premiumPrice = null;
            Object standard = null;
            Object deluxe = null;
            Object premium = null;
            String shipMethodFlorist = null;
            String shipMethodCarrier = null;
            //String productType = null;
            String personalizationTemplate=null;


            while(rs.next())
            {

               // productType = (String)rs.getObject(8);
                standard = rs.getObject(11);
                deluxe = rs.getObject(12);
                premium = rs.getObject(13);
                //vendorId = (String)rs.getObject(30);

                shipMethodCarrier = (String)rs.getObject(56);
                shipMethodFlorist = (String)rs.getObject(57);

                variablePriceFlag = (String)rs.getObject(60);
                personalizationTemplate=rs.getString("personalizationTemplate");
            }

            if(shipMethodCarrier == null) shipMethodCarrier = "N";
            if(shipMethodFlorist == null) shipMethodFlorist = "N";

            item.setShipMethodCarrierFlag(shipMethodCarrier);
            item.setShipMethodFloristFlag(shipMethodFlorist);

            //if this is an Amazon.com or Mercent or Wal-Mart.com order, need to set the ship method
            //Please note...if both shipMethodCarrier = "Y" and
            //shipMethodFlorist = "Y", the florist will be given priority
            //to deliver this order, so ship method will be set to "SD".
            /*
             * 04/11/2013 Fix for defect 15730
             * This is very old amazon implementation and we don't need this anymore.
             * I have discussed with Mike and Jassi and we are all in agreement to comment this code.
             * This code is assigning ship method to "2F" for specialty_gift products if amazon or mercent
             * projects are failed to assign ship method
             */
            /*if((amazonOrder || mercentOrder) && (item.getShipMethod() == null || item.getShipMethod() == ""))
            {
              if( shipMethodCarrier.equalsIgnoreCase("Y") )
              {

                if(productType.equalsIgnoreCase(ValidationConstants.FRESH_CUT) )
                  {
                    item.setShipMethod(ValidationConstants.FRESH_CUT_CARRIER);
                  }
                if(productType.equalsIgnoreCase(ValidationConstants.SAME_DAY_FRESH_CUT) )
                  {
                    item.setShipMethod(ValidationConstants.SAME_DAY_CARRIER);
                  }
                if(productType.equalsIgnoreCase(ValidationConstants.SPECIALTY_GIFT))
                  {
                    item.setShipMethod(ValidationConstants.DROP_SHIP);
                  }

              }
              if(shipMethodFlorist.equalsIgnoreCase("Y") )
              {

               if(productType.equalsIgnoreCase(ValidationConstants.FLORAL) )
                {
                  item.setShipMethod(ValidationConstants.FLORIST_ONLY);
                }

              if(productType.equalsIgnoreCase(ValidationConstants.SAME_DAY_FRESH_CUT) )
                {
                  item.setShipMethod(ValidationConstants.SAME_DAY_FLORIST);
                }
              }
            }*/

            if(amazonOrder || mercentOrder || partnerOrder)
            {
            //need to add a check that if shipping fee already contains an amount, skip this logic
            //all together.  What is happening if we don't, when orders are brought up in scrub
            //they are coming through this logic and if the below condition is true, the
            //shipping fee is being set to zero.
            double tempShippingFee = 0;

            if(item.getShippingFeeAmount() != null  )
            {
              try
              {
                tempShippingFee = Double.parseDouble(item.getShippingFeeAmount());
              } catch (Exception ex)
              {
                //ignore
              }

            }
            if(tempShippingFee == 0)
            {
              if( shipMethodCarrier.equalsIgnoreCase("Y") && shipMethodFlorist.equalsIgnoreCase("N") )
                {

                  if( item.getServiceFeeAmount() != null )
                  {
                    item.setShippingFeeAmount(item.getServiceFeeAmount());
                    item.setServiceFeeAmount("0");
                  }

                }
            }

            }

            if(standard == null)
            {
                standardPrice = new BigDecimal("0");
            }
            else
            {
                standardPrice = new BigDecimal(standard.toString());
            }

            if(deluxe == null)
            {
                deluxePrice = new BigDecimal("0");
            }
            else
            {
                deluxePrice = new BigDecimal(deluxe.toString());
            }

            if(premium == null)
            {
                premiumPrice = new BigDecimal("0");
            }
            else
            {
                premiumPrice = new BigDecimal(premium.toString());
            }

            // If the product is a sub-code then set the standard price
            if(item.getProductSubCodeId() != null && item.getProductSubCodeId().length() > 1)
            {
                if(price == null || Float.parseFloat(price.toString()) == 0)
                {
                }
                else
                {
                    standardPrice = new BigDecimal(price.toString());
                }
            }

            boolean mappedPriceOption = false;

            logger.info("Setting Size: " + item.getExternalOrderNumber() + " " + item.getProductId()
                    + " " + item.getProductsAmount());
            logger.info("Origin: " + orderOrigin);
            logger.info("Variable Price Flag: " + variablePriceFlag);
            logger.info("Price points: " + standardPrice + " " + deluxePrice + " " + premiumPrice);

            String productsAmountStr = item.getProductsAmount();
            logger.info("productsAmountStr: " + productsAmountStr);
            String sizeChoiceStr = item.getSizeChoice();
            logger.info("sizeChoiceStr: " + sizeChoiceStr);
            if(productsAmountStr == null) productsAmountStr = "0";
            BigDecimal productsAmount = new BigDecimal(productsAmountStr);
            BigDecimal zero = new BigDecimal("0");

            // BEGIN Lookup of the price type (standard, premuim or deluxe)
            // Also fill in price for bulk orders.  Pick the least expensive option
            // Defect 1768: Added the zero check below.
            if(order.getOrderOrigin() != null && order.getOrderOrigin().equals(originBulk) && productsAmount.compareTo(zero) == 0)
            {
                item.setProductsAmount(standardPrice.toString());
            }

            // Set Product Amount to Standard Price for CAT orders with no previous Product Amount
            if(orderOrigin != null && orderOrigin.equalsIgnoreCase(originAriba2) && productsAmount.compareTo(zero) == 0)
            {
                item.setProductsAmount(standardPrice.toString());
                item.setSizeChoice("A");
                mappedPriceOption = true;
            }
            else if(standardPrice != null && standardPrice.compareTo(productsAmount) == 0 && !productsAmount.equals(zero))
            {
                item.setSizeChoice("A");
                mappedPriceOption = true;
            }
            else if(deluxePrice != null && deluxePrice.compareTo(productsAmount) == 0 && !productsAmount.equals(zero))
            {
                item.setSizeChoice("B");
                mappedPriceOption = true;
            }
            else if(premiumPrice != null && premiumPrice.compareTo(productsAmount) == 0 && !productsAmount.equals(zero))
            {
                item.setSizeChoice("C");
                mappedPriceOption = true;
            }
            else if(variablePriceFlag != null && variablePriceFlag.equalsIgnoreCase("Y"))
            {
                item.setSizeChoice("A");
                mappedPriceOption = true;
            }

            // END Lookup of the price type (standard, premuim or deluxe)
            //if a price option cannot be mapped for this item, then default the price option to
            //standard
            if( (orderOrigin.equalsIgnoreCase(originAmazon) || mercentOrder || partnerOrder) && !mappedPriceOption )
            {
              item.setSizeChoice("A");
            }

            //QE3SP-15 For JOE orders, take product price and size indicator on the order
            if(order.isOeOrder()){
            	logger.info("JOE ORDER");
            	boolean isOrderForFSMembershipSKU = isFSMembershipSKUItem(con, item);
                if(isOrderForFSMembershipSKU)
                {
                	logger.info("Free Shipping product, default size to A");
                	item.setSizeChoice("A");
                }
                else{
                	item.setSizeChoice(sizeChoiceStr);
                }
                item.setProductsAmount(productsAmountStr);
            	mappedPriceOption = true;
            }

            //Q4SP16-41 fix - Default price point A if one isn't mapped
            logger.info("mappedPriceOption: " + mappedPriceOption);
            if (!mappedPriceOption){
            		item.setSizeChoice("A");
            }

            logger.info("Size result = " + item.getSizeChoice());

            // BEGIN occasion and addon validation.
            // If there's a card addon, make sure the card addon is mapped to only one occasion.
            // Make sure the card occasion allows other addons on the order.
            // If all valid, change order occasion to the card occasion.
            List<String> addonIds = new ArrayList();
            List addonList = item.getAddOns();
             if( addonList!=null && addonList.size()>0 )
             {
               AddOnsVO avo;
               Iterator aoit = addonList.iterator();
               boolean done = false;
               int cardCount = 0;
               String cardAddonCode = null;

               while( aoit.hasNext() )
               {
                 avo = (AddOnsVO)aoit.next();

                 if( avo!=null )
                 {
                    if(ADDON_TYPE_CARD.equals(avo.getAddOnType())) {
                        if(cardCount > 0) {
                            // More than one card found. One of them will fail the addon on validation. Do nothing.
                            done = true;
                            break;
                        }

                        cardAddonCode = avo.getAddOnCode();
                        cardCount++;
                    } else {
                        // Add non-card addons in list for validations against card occasion.
                        addonIds.add(avo.getAddOnCode());
                    }
                 }
               }
               // Has one card addon.
               if(cardCount > 0 && !done) {
                  // Retrieve occasion from card adddon.
                  OccasionHandler ocassionHandler = (OccasionHandler)CacheManager.getInstance().getHandler("CACHE_NAME_OCCASION");
                  List<String> occasionList = ocassionHandler.getOccasionListByAddonId(cardAddonCode);
                  if(addonIds.size() > 0) {
                      // We only care if there is one occasion for the card.
                      if(occasionList != null && occasionList.size() == 1) {
                        // All addons must be allowed by the card occasion.
                         String cardOccasionId = occasionList.get(0);
                         boolean replaceOccasion = true;
                         Map<String, AddOnVO> allowedAddonsHash = new HashMap<String, AddOnVO>();

                         // First get list of valid addons for this product/occasion/sourcecode combo
                         // and create hash (so we don't have to loop multiple times)
                         AddOnUtility addonUtil = new AddOnUtility();
                         if (logger.isDebugEnabled()) {
                             logger.debug("Addon validation for product/occasion/source: " + item.getProductId() + "/" + cardOccasionId + "/" + item.getSourceCode());
                         }
                         Map<String, ArrayList<AddOnVO>> aaHash = addonUtil.getActiveAddonListByProductIdAndOccasionAndSourceCode(
                                                                   item.getProductId(), Integer.parseInt(cardOccasionId), item.getSourceCode(), false, con);
                         if (aaHash != null) {
                             Iterator aaIt = aaHash.keySet().iterator();
                             while (aaIt.hasNext()) {
                               String key = (String) aaIt.next();
                               ArrayList aavoList = aaHash.get(key);
                               for(int i = 0; i < aavoList.size(); i++) {
                                   AddOnVO aavo = (AddOnVO)aavoList.get(i);
                                   if (logger.isDebugEnabled()) {
                                     logger.debug("Addon ID/price/maxQuantity " + aavo.getAddOnId() + "/" + aavo.getAddOnPrice() + "/" + aavo.getMaxQuantity());
                                   }
                                   allowedAddonsHash.put(aavo.getAddOnId(), aavo);
                               }
                             }
                             // All addons in the order should be allowed.
                             for(int i = 0; i < addonIds.size(); i++) {
                                 AddOnVO aavo = allowedAddonsHash.get(addonIds.get(i));
                                 if(aavo == null) {
                                    // order addon not found in allowed addon list.
                                    replaceOccasion = false;
                                    break;
                                 }
                             }

                             // Replace order occasion with card occasion.
                             if(replaceOccasion) {
                                logger.debug("replacing occasion to:" + cardOccasionId);
                                item.setOccassionId(cardOccasionId);
                             }

                         }

                      }

                  }
                  else{
                       //retrieve occasion card is associated with
                       //set order occasion to that
                       logger.debug("replacing occasion to:" + occasionList.get(0));
                       item.setOccassionId(occasionList.get(0));
                   }
               }

             }
            // END occasion and addon validations.

             /**************************** STARTING SHIP METHOD AND SHIP DATE CALCULATION ********************************/


             if(item != null && item.getRecipients() != null
            		 && item.getRecipients().size() > 0
            		 && item.getRecipients().get(0) !=null
            		 && ((RecipientsVO)item.getRecipients().get(0)).getRecipientAddresses() != null
            		 &&((RecipientsVO)item.getRecipients().get(0)).getRecipientAddresses().size() > 0)
             {
            	 recipient = (RecipientsVO)item.getRecipients().get(0);
            	 recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);


            	// For bulk orders also calculate the ship method
             	// QA Defect#453, also calculate least expensive for Ariba orders if order does not have ship method
             	logger.info("### Shipping Details -"
             			+ " \n Origin Value: " +order.getOrderOrigin()
             			+ " \n ship method: " + item.getShipMethod()
             			+ " \n delivery date:" + item.getDeliveryDate()
             			+ " \n b2border is: " + b2bOrder
             			+ " \n actualAddressType is: " + actualAddressType);

            	 if(b2bOrder) {// && (item.getShipMethod() == null || item.getShipMethod().trim().length() <= 0 || checkDeliveryDateAvailability(item.getDeliveryDate())))
            	 	 String countryId = recipAddress.getCountry();
            		 String formattedZip = recipAddress.getPostalCode();
            		 if(countryId != null && countryId.equals(canadaCountryCode) && formattedZip != null && formattedZip.length() > 3) {
            			 formattedZip = formattedZip.substring(0, 3);
            		 } else if(countryId != null && countryId.equals(usCountryCode) && formattedZip != null && formattedZip.length() > 5) {
            			 formattedZip = formattedZip.substring(0, 5);
            		 }

            		 logger.info("countryId: " + countryId +", zipCode :"+ formattedZip);
            		 calculateItemShipMethodDetails(recipAddress, actualAddressType, item, shipMethodCarrier, addonIds, formattedZip, con, vdau, deliveryDaysOutMAX);
            	 }
            	 //check if Personal Creation Order && Saturday delivery (only for PC and  non PDP products)
            	 if(personalizationTemplate!=null && personalizationTemplate.trim().length()>0 &&
            			 !PC_PDP_TEMPLATE.equals(personalizationTemplate) &&
            			 isSaturdayDelivery(item.getDeliveryDate())){
            		 logger.info("It's PC order and Saturday Delivery!!");
            		 item.setShipMethod(GeneralConstants.DELIVERY_SATURDAY_CODE);

            	 }

            	 // Only get ship date for carrier delivered
            	 if(item.getShipMethod() != null && item.getShipMethod().trim().length()> 0 && shipMethodCarrier.equals("Y"))
            	 {
            		 boolean validFormat = true;
            		 try
            		 {
            			 sdf3.setLenient(false);
            			 sdf3.parse(item.getDeliveryDate());

            			 if(item.getDeliveryDate().length() != 10)
            			 {
            				 validFormat = false;
            			 }
            		 }
            		 catch(Exception e)
            		 {
            			 validFormat = false;
            		 }

            		 // Only calculate ship date if delivery date is valid format
            		 if(validFormat)
            		 {
            			 if(item.getShipMethod().equals(GeneralConstants.DELIVERY_SAME_DAY_CODE))//If SDFC product
            			 {
            				 // For same day florist delivery use the delivery date
            				 shipDate = item.getDeliveryDate();
            			 }
            			 else
            			 {
            				 try
            				 {
            					 ProductAvailVO paVO = new ProductAvailVO();
            					 paVO = PASServiceUtil.getProductAvailShipData(item.getProductId(), new SimpleDateFormat("MM/dd/yyyy").parse(item.getDeliveryDate()),
            							 zipCode, recipAddress.getCountry(), item.getShipMethod(), shipMethodFlorist,  addonIds, order.getSourceCode());
            					 if(paVO != null)
            					 {
            						 shipDate = paVO.getShipDate();
                					 logger.info("Ship Date By PAS: " + shipDate );
            					 }
            					 //QE3SP-10
            					 //Add logic if status = 2001 and shipDate is null and the error message returned contains 'Exception caught calling PAC'
            					 //add a delay and try again.  If ship date is still null,
            					 //set shipDate = deliveryDate.
            					 //If status != 2001 and shipDate is null, set shipDate = deliveryDate.
            					 String retryCount = null;
            				     String retryInterval = null;
            				     int retry = 0;
            				     boolean pacReturnedShipDate = false;
            				     if(shipDate == null && paVO.getErrorMessage() != null && paVO.getErrorMessage().contains("Exception caught calling PAC")){
            						 if(parmHandler != null)
                				     {
                				            retryCount = parmHandler.getGlobalParm("SERVICE", "PAC_TIMEOUT_MAX_RETRIES");
                				            retryInterval = parmHandler.getGlobalParm("SERVICE", "PAC_TIMEOUT_DELAY");
                				            logger.debug("retryCount: " + retryCount);
                				            logger.debug("retryInterval: " + retryInterval);
                				     }
            						 if(item.getStatus().equals("2001")){
            							 while (!pacReturnedShipDate) {
             				                if(retry < Integer.valueOf(retryCount)) {

             				                	paVO = PASServiceUtil.getProductAvailShipData(item.getProductId(), new SimpleDateFormat("MM/dd/yyyy").parse(item.getDeliveryDate()),
             	            							 zipCode, recipAddress.getCountry(), item.getShipMethod(), shipMethodFlorist,  addonIds, order.getSourceCode());

             				                	 if(paVO != null)
             	            					 {
             	            						 shipDate = paVO.getShipDate();
             	            						 logger.info("Ship Date By PAS: " + shipDate );
             	            					 }
             	            					 if(shipDate == null){
             	            						 Thread.currentThread().sleep(Long.valueOf(retryInterval));
             	            						 retry++;
             	            						logger.info("retryCount: " + retry);
             	            					 }
             	            					 else{
             	            						 //ship date found continue on with processing
             	            						 pacReturnedShipDate = true;
             	            					 }
             				                } else {
             				                	logger.info("Encountered PAC timeout, setting ship date = delivery date");
             				                	shipDate = item.getDeliveryDate();
             				                	pacReturnedShipDate = true;
             				                }
             				            }
            						 }
            					 }
            				 }
            				 catch(Exception pe)
            				 {
            					 logger.error("failed to get the ShipDate for " + item.getDeliveryDate(), pe);
            				 }
            			 }
            		 }

            	 }

            	 item.setShipDate(shipDate);

             }else if(item != null) {
            	 item.setShipDate(shipDate);
             }
             logger.info(" *Ship Date Calculation Done .Ship Method is: "+item.getShipMethod()+" and  Ship Date is    :"+item.getShipDate());
             /********************************************* END ****************************************************/

			// BEGIN Premier Circle
			// UC 24607 - Apollo should perform CAMS call to check if the email belongs to PC Member,
            // only for new orders and when email modified in scrub for existing non PC orders.

            // The status of the order item just received will be 2001.
            // Treating this as new order on which PC check is not already performed.
			order.setBuyerEmailChangedInScrub(false);
			if (item.getPcFlag() == null || "N".equals(item.getPcFlag())) {
				if (isBuyerEmailChanged) {
					if (buyerEmail != null && !buyerEmail.isEmpty()) {
						order.setBuyerEmailChangedInScrub(true);
					}
				}
			}

			logger.info("###Premier Circle -"
					+ "\n PC flag on Item: " + item.getPcFlag()
					+ " \n isBuyerEmailChanged: " + isBuyerEmailChanged
					+ " \n item.getStatus():" + item.getStatus()
					+ "\n buyerEmail: " + buyerEmail);


			// END Premier Circle


        }
        ////////////////////// END Items //////////////////////

        //  Massage fraud
        if(!order.isOeOrder() && (order.getFraudFlag() == null || order.getFraudFlag().trim().equals("")))
        {
            updateFraudFlag(order, con, vdau);
        }

        //Defect 1100 - need to compare original order received against
        //recalculated FTD order and check to see if there are any price variances
        OrderVO clonedOrder = null;
        //clone order passed in and recalculate it
        clonedOrder = (OrderVO)(new ObjectCopyUtil().deepCopy(order));

        //Please note that for pre-existing partners prior to Target and the creation of Partner Integration (PI)
        //we stored partner origins in the metadata_config.xml file and would check to see if the origin on the order
        //had a property of RECALCULATE_OFF in the file.  If it did we would skip recalculating the order.
        //For Target and any other partner we add to PI, we will call a stored procedure passing in the origin on the order
        //to check to see if we should bypass recalculating the order.
        logger.debug("isOeOrder: " + order.isOeOrder());
            if( recalcOrder(order, mercentOrder,partnerOrder) )
            {
              RecalculateOrderBO recalcBO = new RecalculateOrderBO();
              try
              {
                  recalcBO.recalculate(con, order);

                  //Create lists of OrderDetailVOs for the newly recalculated Order
                  List <OrderDetailsVO> lNewOrderDetails = new ArrayList();
                  lNewOrderDetails =  order.getOrderDetail();

//				// Will contain the list of official taxes (aka the taxes we get from the recalcBO.getTaxVOList() method
//				List<TaxVO> lOfficialTaxVOs = new ArrayList<TaxVO>();
//
//				// Setting the "Description" and "Rate" on the TaxVO objects on the orderDetail
//				try {
//					for (int i = 0; i < lNewOrderDetails.size(); i++) {
//						OrderDetailsVO newOrderDetailVO = lNewOrderDetails.get(i);
//						RecipientsVO recipientVO = null;
//						RecipientAddressesVO recipAddressVO = null;
//						recipientVO = (RecipientsVO) newOrderDetailVO.getRecipients().get(0);
//
//						if (recipientVO != null) {
//							recipAddressVO = (RecipientAddressesVO) recipientVO.getRecipientAddresses().get(0);
//						}
//
//						TaxRequestVO taxRequestVO = new TaxRequestVO();
//						taxRequestVO.setRecipientAddress(recipAddressVO);
//						taxRequestVO.setCompanyId(order.getCompanyId());
//						taxRequestVO.setConfirmationNumber(newOrderDetailVO.getExternalOrderNumber());
//						taxRequestVO.setOrderDetailNumber(newOrderDetailVO.getOrderDetailId());
//						taxRequestVO.setShipMethod(newOrderDetailVO.getShipMethod());
//
//						CalculateTaxUtil taxUtil = new CalculateTaxUtil();
//						taxRequestVO.setTaxableAmount(taxUtil.getTaxableAmount(newOrderDetailVO));
//						ItemTaxVO officialItemTaxVO = taxUtil.calculateTaxRates(taxRequestVO);
//
//						boolean changeRates = true;
//						if (officialItemTaxVO != null) {
//							lOfficialTaxVOs = officialItemTaxVO.getTaxSplit();
//
//							// If the service is down when calculating official rates, do not apply any updates.
//							if (lOfficialTaxVOs == null || (lOfficialTaxVOs.size() == 0 && officialItemTaxVO.getTaxAmount() != null
//									&& new BigDecimal(officialItemTaxVO.getTaxAmount()).compareTo(BigDecimal.ZERO) > 0)) {
//								changeRates = false;
//							}
//						}
//
//						if (changeRates) { // set the official rates and amounts to new order detail VO.
//							newOrderDetailVO.getItemTaxVO().setTaxSplit(lOfficialTaxVOs);
//						}
//					}
//				} catch (Exception e) {
//					logger.error("Unable to massage the order detail tax, not massaging the tax data. " + e.getMessage());
//				}

                  logger.info("Starting Bin Processing: " + order.getSourceCode());

                  SourcePartnerBinHandler spbHandler = (SourcePartnerBinHandler) CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_PARTNER_BIN");
                  String partnerName = spbHandler.getSourceBinPartner(order.getSourceCode(), ccNumber);
                  logger.debug("partnerName: " + partnerName);
                  if (partnerName != null)
                  {
                	boolean ignoreBinProcessing = "USAA".equalsIgnoreCase(partnerName)
                				&& hasFSMembershipSKUItemOrWithFSApplied(con, order);
                  	if( ignoreBinProcessing)
                  	{
                  		logger.debug("Skipping Bin Processing because Order is placed using USAA BIN CreditCard and has item with FSM SKU or FS Applied.");
                  	}
                  	else
                  	{
                      logger.info("Bin Processing partner found: " + partnerName);
                      String oldOrderSource = sourceVO.getOrderSource();
                      PartnerVO partnerVO = FTDCommonUtils.getPartnerByName(partnerName, con);
                      String newSourceCode = partnerVO.getDefaultWebSourceCode();
                      String newSourceCodeDescription = partnerVO.getDefaultWebSourceCodeDescription();
                      if (oldOrderSource != null && oldOrderSource.equalsIgnoreCase("P")) {
                          newSourceCode = partnerVO.getDefaultPhoneSourceCode();
                          newSourceCodeDescription = partnerVO.getDefaultPhoneSourceCodeDescription();
                      }
                      logger.debug("oldSourceCode: " + order.getSourceCode() + " " + oldOrderSource + " " + newSourceCode);
                      boolean sourceCodeChanged = false;

                      BigDecimal oldTotal = new BigDecimal("0");
                      BigDecimal newTotal = new BigDecimal("0");
                      String tempTotal = order.getOrderTotal();
                      if (tempTotal != null) {
                          oldTotal = new BigDecimal(tempTotal);
                      }
                      logger.debug("oldtotal: " + oldTotal);

                      it = order.getOrderDetail().iterator();
                      while(it.hasNext())
                      {
                          item = (OrderDetailsVO)it.next();

                          OrderVO myClone = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                          OrderDetailsVO newItem = (OrderDetailsVO) new ObjectCopyUtil().deepCopy(item);
                          newItem.setSourceCode(newSourceCode);
                          List singleItemList = new ArrayList();
                          singleItemList.add(newItem);
                          myClone.setOrderDetail(singleItemList);
                          myClone.setSourceCode(newSourceCode);
                          recalcBO.recalculate(con, myClone); //recalculates dollar amounts on OrderVO object
                          logger.debug("old item: " + item.getExternalOrderTotal());
                          logger.debug("new item: " + myClone.getOrderTotal());
                          String newAmt = myClone.getOrderTotal(); //get the order total from the recalculated OrderVO
                          if (newAmt != null) newTotal = newTotal.add(new BigDecimal(newAmt));
                      }
                      logger.debug("newtotal: " + newTotal);
                      if (newTotal.compareTo(oldTotal) <= 0) {
                          logger.info("new total (" + newTotal + ") is less than old total (" + oldTotal + ")");
                          it = order.getOrderDetail().iterator();
                          while(it.hasNext()) {
                              item = (OrderDetailsVO)it.next();
                              item.setSourceCode(newSourceCode);
                              item.setSourceDescription(newSourceCodeDescription);
                              item.setBinSourceChangedFlag("Y");
                              item.setItemOfTheWeekFlag("N");
                          }
                          order.setSourceCode(newSourceCode);
                          order.setSourceDescription(newSourceCodeDescription);
                          order.setMembershipId(0);
                          order.setMemberships(new ArrayList());
                          sourceCodeChanged = true;

                      }

                      if (sourceCodeChanged) {
                          logger.debug("Source Code was changed, recalcing");
                          recalcBO.recalculate(con, order);
                      }
                  	}
                  }

                  it = order.getOrderDetail().iterator();
                  Iterator oldIt = clonedOrder.getOrderDetail().iterator();
                  while(it.hasNext())
                  {
                      item = (OrderDetailsVO)it.next();
                      OrderDetailsVO oldItem = (OrderDetailsVO)oldIt.next();
                      if (oldItem.getServiceFeeAmount() == null) {
                          oldItem.setServiceFeeAmount("0");;
                      }
                      if (oldItem.getShippingFeeAmount() == null) {
                          oldItem.setShippingFeeAmount("0");;
                      }
                      if(oldItem.getMorningDeliveryFee() == null){
                    	  oldItem.setMorningDeliveryFee("0");
                      }
                      if (item.getServiceFeeAmount() == null) {
                          item.setServiceFeeAmount("0");;
                      }
                      if (item.getShippingFeeAmount() == null) {
                          item.setShippingFeeAmount("0");;
                      }
                      boolean isMDFAvail = true;
                      BigDecimal newMorningDeliveryFee = new BigDecimal("0");
                      try {
                          if(item.getMorningDeliveryFee() == null || Double.parseDouble(item.getMorningDeliveryFee())<0) {
	                      	  isMDFAvail = false;
	                      }
                      } catch(NumberFormatException e){
                    	  logger.error("Error caught converting Morning delivery fee : " + e.getMessage());
                      	  isMDFAvail = false;
                      }
                      //if(item.getMorningDeliveryFee() == null){
                      //  item.setMorningDeliveryFee("0");
                      // }
                      logger.info("old service fee: " + oldItem.getServiceFeeAmount());
                      logger.info("old shipping fee: " + oldItem.getShippingFeeAmount());
                      logger.info("old Morning Delivery fee: " + oldItem.getMorningDeliveryFee());
                      logger.info("new service fee: " + item.getServiceFeeAmount());
                      logger.info("new shipping fee: " + item.getShippingFeeAmount());
                      logger.info("new Morning Delivery fee: "+ item.getMorningDeliveryFee());

                      BigDecimal oldServiceFee = new BigDecimal(oldItem.getServiceFeeAmount());
                      BigDecimal oldShippingFee = new BigDecimal(oldItem.getShippingFeeAmount());
                      BigDecimal oldMorningDeliveryFee = new BigDecimal(oldItem.getMorningDeliveryFee());
                      BigDecimal newServiceFee = new BigDecimal(item.getServiceFeeAmount());
                      BigDecimal newShippingFee = new BigDecimal(item.getShippingFeeAmount());
                      BigDecimal oldTotal = oldServiceFee.add(oldShippingFee);
                      BigDecimal newTotal = newServiceFee.add(newShippingFee);
                      if (isMDFAvail) {
                          newMorningDeliveryFee = new BigDecimal(item.getMorningDeliveryFee());
                      	  logger.debug("Step 1:newServiceFee" + newServiceFee);
                       	  logger.debug("Step 11:newShippingFee" + newShippingFee);
                       	  if (item.getProductType().equals("FRECUT") || item.getProductType().equals("SDFC")){
                        	  newServiceFee = newServiceFee.subtract(newMorningDeliveryFee);
                        	  oldServiceFee = oldServiceFee.subtract(oldMorningDeliveryFee);
                          }
                          else if(item.getProductType().equals("SDG") || item.getProductType().equals("SPEGFT")){
                        	  newShippingFee = newShippingFee.subtract(newMorningDeliveryFee);
                        	  oldShippingFee = oldShippingFee.subtract(oldMorningDeliveryFee);
                          }
                      }

                      logger.debug("Step 1:newServiceFee" + newServiceFee);
                      logger.debug("Step 11:newShippingFee" + newShippingFee);
                      long orderDetailId = item.getOrderDetailId();
                      if ( !oldTotal.equals(newTotal) && orderDetailId > 0 ) {
                          dataRequest = new DataRequest();
                          dataRequest.setConnection(con);
                          dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);
                          dataRequest.addInputParam("IN_NOVATOR_SERVICE_FEE_AMT", oldServiceFee);
                          dataRequest.addInputParam("IN_NOVATOR_SHIPPING_FEE_AMT", oldShippingFee);
                          dataRequest.addInputParam("IN_WEBSITE_MORNING_DELIVERY_FEE", oldMorningDeliveryFee);
                          dataRequest.addInputParam("IN_APOLLO_SERVICE_FEE_AMT", newServiceFee);
                          dataRequest.addInputParam("IN_APOLLO_SHIPPING_FEE_AMT", newShippingFee);
                          dataRequest.addInputParam("IN_APOLLO_MORNING_DELIVERY_FEE", newMorningDeliveryFee);
                          dataRequest.setStatementID("INSERT_FEE_VARIANCE");
                          Map outputMap = (HashMap) vdau.execute(dataRequest);
                          dataRequest.reset();
                      }
                  }

              }
              catch (RecalculateException rpe)
              {
                try
                {
                  // lpuckett 06/28/2006: Add throw to figure out what's broken in RecalculateOrderBO
                  StringWriter stringWriter = new StringWriter();
                  rpe.printStackTrace(new PrintWriter(stringWriter));
                  String error = "Recalculate Order Exception for order guid "+order.getGUID() +" : " + stringWriter.toString();
                  logger.error(error);
                  SystemMessengerVO smVO = new SystemMessengerVO();
                  smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                  smVO.setType("ERROR");
                  smVO.setSource("ORDER VALIDATOR");
                  smVO.setMessage(error);
                  SystemMessenger.getInstance().send(smVO, con, false);
                }
                catch (Exception ex)
                {
                  logger.error("Error trying to send System Message", ex);
                }
              }
            } // END recalculate prices

            // Check if recalculation should be performed.
	        if(mercentOrder || partnerOrder){
	        	performTaxRecal(order, con);
	        }

         logger.info("ship method: " + item.getShipMethod());
         logger.info("delivery date: " + item.getDeliveryDate());
         logger.info("delivery date range: " + item.getDeliveryDateRangeEnd());
         if(order.getCharacterMappingFlag()==null || order.getCharacterMappingFlag().equalsIgnoreCase("N"))
         {
           order.setCharacterMappingFlag("Y");
           logger.debug("###Char Mapping Flag(After setting it)### "+order.getCharacterMappingFlag());
         }

        return order;
    }

	private static boolean isSaturdayDelivery(String deliveryDate) {
		boolean isSaturday=false;
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			Calendar cal=Calendar.getInstance();
			cal.setTime(sdf.parse(deliveryDate));
			isSaturday= Calendar.SATURDAY== cal.get(Calendar.DAY_OF_WEEK);
		}catch(ParseException e){
			logger.error("Error parsing deliveryDate, so can't determin as Saturday delivery !!");
		}
		return isSaturday;
	}

    private static void performTaxRecal(OrderVO order, Connection conn) throws Exception {

    	RecalculateOrderBO roVO = new RecalculateOrderBO();
    	List<OrderDetailsVO> orderItems = order.getOrderDetail();

		BigDecimal orderTotal = new BigDecimal(0);
		BigDecimal orderTaxAmount = new BigDecimal(0);

        for (int i=0; i < orderItems.size(); i++) {
        	OrderDetailsVO orderDetail=orderItems.get(i);
        	if(isStateModified(conn, orderItems.get(i))) {
        		logger.debug("State modified for the order with origin - " + order.getOrderOrigin());
        		roVO.recalculateTaxAndTotal(conn, orderItems.get(i), order.getCompanyId());
        		orderDetail.setExternalOrderTotal(orderDetail.getOrderAmount());
        	}

        	if(orderDetail.getExternalOrderTotal() != null) {
        		orderTotal = orderTotal.add(new BigDecimal(orderDetail.getExternalOrderTotal()));
        	}
        	if(orderDetail.getTaxAmount() != null) {
        		orderTaxAmount = orderTaxAmount.add(new BigDecimal(orderDetail.getTaxAmount()));
        	}
        	if(orderDetail.getShippingTax() != null) {
        		orderTaxAmount = orderTaxAmount.add(new BigDecimal(orderDetail.getShippingTax()));
        	}

        }
        order.setOrderTotal((orderTotal.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());
        order.setTaxTotal((orderTaxAmount.setScale(AMOUNT_SCALE, BigDecimal.ROUND_HALF_DOWN)).toString());

	}


	/** Convenient method to check if the State on the item is modified.
     * @param con
     * @param item
     */
    private static boolean isStateModified(Connection con, OrderDetailsVO item) {
    	DataRequest dataRequest = null;
    	CachedResultSet rs = null;
    	 ValidationDataAccessUtil vdau = null;
    	try{
    		dataRequest = new DataRequest();
	    	vdau = new ValidationDataAccessUtil();

	    	dataRequest.setConnection(con);
	        dataRequest.setStatementID(GET_ITEM_RECIPIENT_INFO);
	        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", item.getOrderDetailId());

	        logger.info("Checking the original state for the item: " +  item.getExternalOrderNumber());

	        rs = (CachedResultSet)vdau.execute(dataRequest);
	        rs.reset();
	        dataRequest.reset();

	        if(rs.next()) {
	        	String modifiedState = ((RecipientAddressesVO)((RecipientsVO)item.getRecipients().get(0)).getRecipientAddresses().get(0)).getStateProvince();
	        	if(modifiedState!=null && !modifiedState.equalsIgnoreCase(rs.getString("STATE_PROVINCE"))) {
	        		logger.info("Recipient State of the item is modified to :" + modifiedState + ", performing recalculation.");
	        		return true;
	        	}
	        }

    	} catch (Exception e) {
			logger.error("Error caught checking if state is modified: " + e.getMessage());
		}
    	return false;
	}

	private static boolean hasFSMembershipSKUItemOrWithFSApplied(Connection conn, OrderVO orderVO)
    {
    	String freeShipping = orderVO.getBuyerHasFreeShippingFlag();
    	logger.info("BuyerHasFreeShippingFlag:"+freeShipping);
    	boolean isOrderWithFSApplied = "Y".equalsIgnoreCase(freeShipping);
    	if(isOrderWithFSApplied){
    		return isOrderWithFSApplied;
    	}
    	logger.info("Order doesn't have any item with FS Applied.");

    	List detailList = orderVO.getOrderDetail();
        for (int i=0; i<detailList.size(); i++)
        {
            OrderDetailsVO odVO = (OrderDetailsVO) detailList.get(i);
            boolean isOrderForFSMembershipSKU = isFSMembershipSKUItem(conn, odVO);
            if(isOrderForFSMembershipSKU)
            {
            	return true;
            }
        }
        logger.info("Order doesn't have FS Membership SKU.");
        return false;
    }

	private static boolean isFSMembershipSKUItem(Connection conn, OrderDetailsVO item)
	{
		logger.info("ProductId:"+item.getProductId());
        String productType = item.getProductType();
        String productSubType = item.getProductSubType();
        logger.info("productType:"+productType);
        logger.info("productSubType:"+productSubType);
        return ("SERVICES".equalsIgnoreCase(productType) && "FREESHIP".equalsIgnoreCase(productSubType));
	}

	private static boolean recalcOrder(OrderVO order, boolean isMercentOrder, boolean isPartnerOrder) throws TransformerException, IOException, SAXException, ParserConfigurationException, Exception
    {

      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
      boolean recalcOrder = true;

      Iterator it = null;
      String key = null;
      String value = null;

      //load list of origins from dispatch_config.xml file
      //where orders will not be recalcuated
      ArrayList excludeOrigins = new ArrayList();
      Map map = configUtil.getProperties(METADATA_CONFIG_FILE_NAME);
      it = map.keySet().iterator();

      while(it.hasNext())
      {
         key = (String)it.next();
         value = (String)map.get(key);

         if(key.startsWith("RECALCULATE_OFF_"))
         {
             excludeOrigins.add(value);
         }
      }

       try
       {
           String origin = null;
           if( order.getOrderOrigin() != null  )
           {
              //loop through the exclude origins list to see
              //if we need to turn off recalculating this order
              for (int i=0; i<excludeOrigins.size(); i++)
              {
                origin = excludeOrigins.get(i).toString();

                if (order.getOrderOrigin().equalsIgnoreCase(origin) || isMercentOrder || isPartnerOrder )
                {
                  recalcOrder = false;
                  break;
                }
              }
           }

       }
       catch(Exception e)
       {
           logger.error("Problems occured in recalcOrder method to check if order should be recalculated or not: " + e.toString());
       }

       logger.info("orderCalcType: " + order.getOrderCalcType() + " " + order.getStatus());
       if (order.getOrderCalcType() != null && order.getOrderCalcType().equalsIgnoreCase("CALYX") &&
               !order.getStatus().equals("1005")) {
           recalcOrder = false;
       }

       logger.info("MasterOrderNumber :"+order.getMasterOrderNumber());
       logger.info("OrderOrigin :"+order.getOrderOrigin());
       logger.info("recalcOrder :"+recalcOrder);

       return recalcOrder;

    }

    private static String findCCType(String ccNumber)
    {
        String ccType = null;

        if(ccNumber != null && ccNumber.length() > 0)
        {
            int idBegin = new ValidateMembership().StringToInt(ccNumber.substring(0,2));

            if ((idBegin == 30) | (idBegin == 36) | (idBegin == 38))
                ccType = "DC";

            if ((idBegin == 34) | (idBegin == 37))
                ccType = "AX";

            if ((idBegin >=40) & (idBegin <=49))
                ccType = "VI";

            if ((idBegin >=50) & (idBegin <=59))
                ccType = "MC";

            if ((idBegin >=60) & (idBegin <=69))
                ccType = "DI";
        }

        return ccType;
    }

    private static String[] breakUpAddress(String address1, String address2)
    {
        StringCharacterIterator cIt = null;
        String[] addresses = new String[2];
        if(address1 == null) address1 = "";
        if(address2 == null) address2 = "";
        String origAddress1 = address1;
        String origAddress2 = address2;


        address1 = address1.trim();
        address2 = address2.trim();

        if(address1.length() > ADDRESS_SIZE_MAX)
        {
            // Go back to first space
            int spaceIndex = ADDRESS_SIZE_MAX;
            cIt = new StringCharacterIterator(address1);
            cIt.setIndex(ADDRESS_SIZE_MAX);
            for(char c = cIt.current(); c != CharacterIterator.DONE; c = cIt.previous())
            {
                if(c == ' ')
                {
                    break;
                }
                spaceIndex--;
            }

            if(spaceIndex != -1)
            {
                address1 = origAddress1.substring(0, spaceIndex).trim();
                address2 = (origAddress1.substring(spaceIndex) + address2).trim();

                // if address 2 still over ADDRESS_SIZE_MAX then set address to null so this order goes to scrub
                if(address2.length() > ADDRESS_SIZE_MAX)
                {
                    address1 = origAddress1;
                    address2 = origAddress2;
                }
            }
        }

        addresses[0] = address1;
        addresses[1] = address2;

        return addresses;
    }

    private static String removeAllSpecialChars(String inStr)
    {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < inStr.length(); i++)
        {
            if(Character.isLetterOrDigit(inStr.charAt(i)))
            {
                sb.append(inStr.charAt(i));
            }
        }

        return sb.toString();
    }

    private static String removeTargetStores(String inStr)
    {
        String targetString = "TARGET STORES,";

        logger.info("Removing " + targetString + ": " + inStr);
        int startPos = inStr.toUpperCase().indexOf(targetString);
        if (startPos > 2)
        {
            inStr = inStr.substring(0,startPos-1);
            logger.info("New value: " + inStr);
        }

        return inStr;

    }

    private static String removeAllChars(String inStr)
    {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < inStr.length(); i++)
        {
            if(Character.isDigit(inStr.charAt(i)))
            {
                sb.append(inStr.charAt(i));
            }
        }

        return sb.toString();
    }

    private static int extensionExists(String phoneNumber, StringBuffer matchType)
    {
        int extIndex = -1;
        if((extIndex = phoneNumber.indexOf("extension")) > -1)
        {
            matchType.append("extension");
            extIndex += 9;
        }
        else if((extIndex = phoneNumber.indexOf("ext.")) > -1)
        {
            matchType.append("ext.");
            extIndex += 4;
        }
        else if((extIndex = phoneNumber.indexOf("ext")) > -1)
        {
            matchType.append("ext");
            extIndex += 3;
        }
        else if((extIndex = phoneNumber.indexOf("x")) > -1)
        {
            matchType.append("x");
            extIndex += 1;
        }

        return extIndex;
    }

    private static CountryMasterVO findCountry(String id, List countries)
    {
        CountryMasterVO countryVo = null;
        CountryMasterVO countryVoTmp = null;

        Iterator it = countries.iterator();
        while(it.hasNext())
        {
            countryVoTmp = (CountryMasterVO)it.next();

            if(countryVoTmp.getCountryId().equalsIgnoreCase(id))
            {
                countryVo = countryVoTmp;
                break;
            }
        }

        return countryVo;
    }

    private static Map getStateByIdMap(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");

        Map stateMap = new HashMap();
        if(stateHandler != null)
        {
            stateMap = stateHandler.getStatesByIdMap();
        }
        // If the states handler is not found then lookup the states from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_STATE_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            String stateId = null;
            String stateName = null;
            StateMasterVO stateVo = null;
            while(rs.next())
            {
                stateId = (String)rs.getObject(1);
                stateName = (String)rs.getObject(2);

                if(stateId != null)
                {
                    stateId = stateId.toUpperCase();
                    stateName = stateName.toUpperCase();
                    stateVo = new StateMasterVO();
                    stateVo.setStateMasterId(stateId);
                    stateVo.setStateName(stateName);
                    stateMap.put(stateId, stateVo);
                }
            }
        }
        return stateMap;
    }

    private static Map getStateByNameMap(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        StateMasterHandler stateHandler = (StateMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_STATE_MASTER");

        Map stateMap = new HashMap();
        if(stateHandler != null)
        {
            stateMap = stateHandler.getStatesByNameMap();
        }
        // If the states handler is not found then lookup the states from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_STATE_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            String stateId = null;
            String stateName = null;
            StateMasterVO stateVo = null;
            while(rs.next())
            {
                stateId = (String)rs.getObject(1);
                stateName = (String)rs.getObject(2);

                if(stateId != null)
                {
                    stateId = stateId.toUpperCase();
                    stateName = stateName.toUpperCase();
                    stateVo = new StateMasterVO();
                    stateVo.setStateMasterId(stateId);
                    stateVo.setStateName(stateName);
                    stateMap.put(stateName, stateVo);
                }
            }
        }
        return stateMap;
    }

    private static Map getColors(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        ColorHandler colorHandler = (ColorHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COLOR");

        Map colorMap = new HashMap();
        if(colorHandler != null)
        {
            colorMap = colorHandler.getColorDescriptionMap();
        }
        // If the color handler is not found then lookup the colors from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COLORS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            colorMap = new HashMap();
            String colorCode = null;
            String colorDesc = null;
            while(rs.next())
            {
                colorCode = (String)rs.getObject(1);
                colorDesc = (String)rs.getObject(2);
                colorMap.put(colorDesc, colorCode);
            }
        }

        return colorMap;
    }

    private static Map getAddressTypes(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        AddressTypeHandler addressTypeHandler = (AddressTypeHandler)CacheManager.getInstance().getHandler("CACHE_NAME_ADDRESS");

        Map addressTypes = new HashMap();
        if(addressTypeHandler != null)
        {
            addressTypes = addressTypeHandler.getAddressTypeMap();
        }
        // If the color AddressTypeHandler is not found then lookup the types from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_ADDRESS_TYPES);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            String addressType = null;
            String typeCode = null;
            while(rs.next())
            {
                addressType = (String)rs.getObject(1);
                typeCode = (String)rs.getObject(2);

                if(addressType != null)
                {
                    addressTypes.put(addressType, typeCode);
                }
            }
        }

        return addressTypes;
    }

    private static SourceMasterVO getSourceDetails(String sourceCode, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");

        SourceMasterVO sourceVo = new SourceMasterVO();

        if(sourceHandler != null)
        {
            sourceVo = sourceHandler.getSourceCodeById(sourceCode);
        }
        // If the source handler is not found then lookup the source details from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.addInputParam("SOURCE_CODE", sourceCode);
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            String sourceCodeOut = null;
            String jcpFlag = null;

            while(rs.next())
            {
                sourceCodeOut = (String)rs.getObject(1);
                jcpFlag = (String)rs.getObject(30);
            }

            if(jcpFlag == null) jcpFlag = "";
            if(sourceCodeOut == null) sourceCodeOut = "";

            sourceVo.setSourceCode(sourceCodeOut);
            sourceVo.setJcpenneyFlag(jcpFlag);
        }

        return sourceVo;
    }

    private static List getCountryList(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        CountryMasterHandler countryHandler = (CountryMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_COUNTRY_MASTER");

        List countryList = new ArrayList();
        if(countryHandler != null)
        {
            countryList = countryHandler.getCountryList();
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_COUNTRY_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();

            String countryId = null;
            String countryName = null;
            Object displayOrder = null;
            CountryMasterVO countryVo = null;
            String countryType = null;
            while(rs.next())
            {
                countryId = (String)rs.getObject(1);
                countryName = (String)rs.getObject(2);
                countryType = (String)rs.getObject(3);
                displayOrder = rs.getObject(4);
                if(displayOrder == null) displayOrder = new BigDecimal("999");

                if(countryId != null)
                {
                    countryId = countryId.toUpperCase();
                    countryName = countryName.toUpperCase();

                    countryVo = new CountryMasterVO();
                    countryVo.setCountryId(countryId);
                    countryVo.setCountryType(countryType);
                    countryVo.setDisplayOrder((BigDecimal)displayOrder);
                    countryVo.setCountryName(countryName);
                    countryList.add(countryVo);
                }
            }

            Collections.sort(countryList, new CountryVoComparator());
        }

        return countryList;
    }

    private static Map getOccasions(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        // Try cache first
        OccasionHandler ocassionHandler = (OccasionHandler)CacheManager.getInstance().getHandler("CACHE_NAME_OCCASION");

        Map occasionMap = new HashMap();
        if(ocassionHandler != null)
        {
            occasionMap = ocassionHandler.getOccasionIdMap();
        }
        // If the occasion handler is not found then lookup the occasions from the database
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_OCCASION_LIST);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();
            dataRequest.reset();
            Object occasionCode = null;
            String occasionDesc = null;
            //Iterator occasionIt = null;
           // String desc = null;
            //String code = null;
            while(rs.next())
            {
                occasionCode = rs.getObject(1);
                occasionDesc = (String)rs.getObject(2);

                if(occasionCode != null)
                {
                    occasionMap.put(occasionCode.toString(), occasionDesc);
                }
            }
        }

        return occasionMap;
    }


    private static boolean contains(String inStr, String compareString)
    {
        boolean ret = false;

        inStr = removeAllSpecialChars(inStr);

        int index = inStr.indexOf(compareString);

        if(index > -1)
        {
            // Found possible string
            ret = true;
            try
            {
                char first = '0';
                char last = '0';
                // check the character before the compareString
                if((index - 1) >= 0)
                {
                    first = inStr.charAt(index - 1);
                }

                if((index + compareString.length()) < inStr.length())
                {
                    last = inStr.charAt(index + compareString.length());
                }

                if(Character.isLetter(first) || Character.isLetter(last))
                {
                    ret = false;
                }
            }
            catch(Exception e)
            {
                // Could not parse string
            }
        }

        return ret;
    }

    private static boolean checkForExt(String phoneNumber)
    {
        boolean ret = false;

        ret = DataMassager.contains(phoneNumber, "extension");
        if(ret) return true;

        ret = DataMassager.contains(phoneNumber, "ext.");
        if(ret) return true;

        ret = DataMassager.contains(phoneNumber, "ext");
        if(ret) return true;

        ret = DataMassager.contains(phoneNumber, "x");
        if(ret) return true;

        return false;

    }

    private static void updateFraudFlag(OrderVO order, Connection connection, ValidationDataAccessUtil vdau) throws Exception
    {
        String fraudFlag = null;
        DataRequest dataRequest = new DataRequest();

        SourceMasterHandler sourceHandler = (SourceMasterHandler)CacheManager.getInstance().getHandler("CACHE_NAME_SOURCE_MASTER");
        if(sourceHandler != null)
        {
            SourceMasterVO sourceVo = sourceHandler.getSourceCodeById(order.getSourceCode());
            if(sourceVo != null)
            {
                fraudFlag = sourceVo.getFraudFlag();
            }
        }
        // Get the source code data from the database
        else
        {
            dataRequest.addInputParam("SOURCE_CODE", order.getSourceCode());
            dataRequest.setConnection(connection);
            dataRequest.setStatementID(GET_SOURCE_CODE_DETAILS);
            CachedResultSet rs = (CachedResultSet)vdau.execute(dataRequest);
            rs.reset();

            while(rs.next())
            {
                fraudFlag = (String)rs.getObject(34);
            }
        }

    	Collection payments = order.getPayments();
    	Iterator it = null;
        boolean isOrderCardinalVerified = false;
        if(payments != null && payments.size() > 0) {
            // Get payments
            it = payments.iterator();
            String cardinalVerifiedFlag = null;

            PaymentsVO payment = null;
            while(it.hasNext()) {
                payment = (PaymentsVO)it.next();
                cardinalVerifiedFlag = payment.getCardinalVerifiedFlag();

                if(cardinalVerifiedFlag != null && cardinalVerifiedFlag.equalsIgnoreCase("Y"))
                {
                	isOrderCardinalVerified = true;
                }
            }
        }
        logger.info("isOrderCardinalVerified: "+ isOrderCardinalVerified);
        logger.info("fraudFlag: "+ fraudFlag);

        // if order contains novator fraud messages and source fraud flag is Y
        if(order.getFraudCodes() != null
            && order.getFraudCodes().size() > 0
            && fraudFlag != null
            && fraudFlag.equals("Y")
            && !isOrderCardinalVerified)
        {
            order.setFraudFlag("Y");
        }
        else
        {
            order.setFraudFlag("N");
        }
    }


/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				nd converts it to a Util date of yyyy-mm-dd.
 * @param String data string
 * @return java.sql.Date  (returns null if invalid date)
 * @throws ParseException
 */
public static java.util.Date formatStringToUtilDate(String strDate)
                throws ParseException{

    java.util.Date utilDate = null;
    String inDateFormat = "";
    int dateLength = 0;
    int firstSep = 0;
   // int lastSep = 0;

    if ((strDate != null) && (!strDate.trim().equals(""))) {

		    // set input date format
		    dateLength = strDate.length();
		    int firstTimeSep = strDate.indexOf(":");
		    int firstDashSep = strDate.indexOf("-");
		    int firstSlashSep = strDate.indexOf("/");
		    int lastSlashSep = strDate.lastIndexOf("/");

        if(dateLength == 5 && firstSlashSep == 2 && lastSlashSep == 2)
        {
            inDateFormat = "MM/yy";
        }
        else if(firstSlashSep == 2 && lastSlashSep == 2)
        {
            inDateFormat = "MM/yyyy";
        }
        else if(dateLength > 20){
            inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";
        }
		    else if ( firstTimeSep > 0) {
			    inDateFormat = "yyyy-MM-dd hh:mm:ss";
		    }
        else if(firstDashSep > 0){
			    int lastDashSep = strDate.lastIndexOf("-");
          //check how many in years
          if(strDate.length() - lastDashSep == 4)
          {//4 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yyyy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yyyy";
            }
          }
          else
          {//2 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yy";
            }
          }
        }
        else {
			    firstSep = strDate.indexOf("/");
			    //lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
    		SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

		    java.util.Date date = sdfInput.parse(strDate);
		    String outDateString = sdfOutput.format(date);

			// now that we have no errors, use the string to make a Util date
	 		utilDate = sdfOutput.parse(outDateString);


    }

	return utilDate;
}

    private static boolean getCPCPaymentInfo(Connection con, PaymentsVO payment,String sourceCode,String paymentMethodType)
      throws Exception
    {

        boolean cpcInfoFound = false;

        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        ValidationDataAccessUtil vdau = new ValidationDataAccessUtil();

                    // Lookup source code on CPC_INFO table
                    dataRequest.setConnection(con);
                    dataRequest.setStatementID(GET_CPC_INFO_RECORD);
                    dataRequest.addInputParam("SOURCE_CODE", sourceCode);
                    rs = (CachedResultSet)vdau.execute(dataRequest);
                    rs.reset();
                    dataRequest.reset();
                    String cpcType = null;
                    String cpcNumber = null;
                    String cpcExpDate = null;
                    String cpcSourceCode = null;
                    while(rs.next())
                    {
                        cpcSourceCode = (String)rs.getObject(1);
                        cpcType = (String)rs.getObject(2);
                        cpcNumber = (String)rs.getObject(3);
                        cpcExpDate = (String)rs.getObject(4);


                    }

                    //replace credit card expiration date with slash
                    if(cpcExpDate != null)
                    {
                      cpcExpDate = cpcExpDate.replace('.','/');
                    }


                    if(cpcSourceCode != null)
                    {
                        cpcInfoFound = true;

                        // Create a credit card for this payment
                        CreditCardsVO creditCardTmp = new CreditCardsVO();
                        List creditCards = new ArrayList();

                        creditCardTmp.setCCType(cpcType);
                        creditCardTmp.setCCNumber(cpcNumber);
                        creditCardTmp.setCCExpiration(cpcExpDate);

                        creditCards.add(creditCardTmp);
                        payment.setCreditCards(creditCards);

                        payment.setPaymentMethodType(paymentMethodType);
                        payment.setPaymentsType(cpcType);
                    }

        return cpcInfoFound;

}


//private static String getAmazonShipMethods( List shippingMethods )
//{
  //String shipMethod = null;
  // if( shippingMethods != null)
 // {

   //   if(shippingMethods.contains(twoDayShipping))
   //   {
     //     shipMethod = twoDayShipping.getCode();
     // }
     // else if(shippingMethods.contains(sdShipping))
     // {
     //     shipMethod = GeneralConstants.DELIVERY_SAME_DAY_CODE;
     // }
     // else if(shippingMethods.contains(nextDayShipping))
     // {
     //     shipMethod = nextDayShipping.getCode();
      //}

 // }

  //return shipMethod;
//}

/*private static String getShipMethods( List shippingMethods )
{
      String shipMethod = "";
      if(shippingMethods != null)
      {
          if(shippingMethods.contains(standardShipping))
          {
              shipMethod = standardShipping.getCode();
          }
          else if(shippingMethods.contains(twoDayShipping))
          {
              shipMethod = twoDayShipping.getCode();
          }
          else if(shippingMethods.contains(sdShipping))
          {
              shipMethod = GeneralConstants.DELIVERY_SAME_DAY_CODE;
          }
          else if(shippingMethods.contains(nextDayShipping))
          {
              shipMethod = nextDayShipping.getCode();
          }
          else if(shippingMethods.contains(satShipping))
          {
              shipMethod = satShipping.getCode();
          }

      }

  return shipMethod;
}*/


    public static void main(String[] args)
    {
        System.out.println(DataMassager.checkForExt("ext 1234") + " true");

        System.out.println(DataMassager.checkForExt("extn 1234") + " false");

        System.out.println(DataMassager.checkForExt("1234567 ext. 1234") + " true");

        System.out.println(DataMassager.checkForExt("1234567 next 1234") + " false");

        System.out.println(DataMassager.checkForExt("1234567 extension. 1234") + " true");

        System.out.println(DataMassager.checkForExt("1234567 x1234") + " true");

        System.out.println(DataMassager.checkForExt("1234567 extn 1234") + " false");

        System.out.println(DataMassager.checkForExt("1234567ADBVVD1234") + " false");

        System.out.println(DataMassager.checkForExt("630 121-432 unpublished") + " false");

        System.out.println(DataMassager.checkForExt("630555123 extension 123") + " true");
    }

    public static int getDeliveryDaysOutMAX(ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        int deliveryDaysOutMAX = 120;
        String deliveryDaysOutMAXKey = "";

        try {
          deliveryDaysOutMAXKey = ConfigurationUtil.getInstance().getProperty("order_validator_config.xml", "delivery.days.out.max.key");
        } catch (Exception ioe) {
          // if we cannot find this property for some reason, just use the default above;
        }

        if (deliveryDaysOutMAXKey != null && !deliveryDaysOutMAXKey.equals("")) {
          String deliveryDaysOutMAXString = getFrpGlobalParm(deliveryDaysOutMAXKey, vdau, con);
          if (deliveryDaysOutMAXString != null && !deliveryDaysOutMAXString.equals("")) {
              Integer i = Integer.valueOf(deliveryDaysOutMAXString);
              if (i != null) {
                deliveryDaysOutMAX = i.intValue();
              }
          }
        }

        return deliveryDaysOutMAX;
    }

    public static String getFrpGlobalParm(String key, ValidationDataAccessUtil vdau, Connection con) throws Exception
    {
        String value = null;

        // Try cache first
        GlobalParmHandler parmHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        if(parmHandler != null)
        {
            value = parmHandler.getGlobalParm("VALIDATION_SERVICE", key);
        }
        else
        {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.addInputParam("IN_PARAM", key);
            dataRequest.setStatementID("GET_GLOBAL_PARAM_VALIDATION");
            String rs = (String)vdau.execute(dataRequest);
            value = rs;
            dataRequest.reset();
        }

        return value;
    }

    /**
     * This method checks whether the Language Id value is valid or not.
     * If it is invalid Language Id will be set to DEFAULT_LANGUAGE_ID.
     * @param order
     * @throws Exception
     */
    private static void validateAndSetLanguageId(OrderVO order) throws Exception {

    	 String languageId = order.getLanguageId();
         logger.debug("Master Order Number :["+order.getMasterOrderNumber()+"], Language Id :["+languageId+"]");

         if(languageId == null || languageId.trim().length() == 0){
        	 logger.debug("Language Id is null or empty. Setting Default Language Id :["+DEFAULT_LANGUAGE_ID+"]");
        	 order.setLanguageId(DEFAULT_LANGUAGE_ID);
         } else {
    		 LanguageMasterHandler languageMasterHandler =
        		 (LanguageMasterHandler)CacheManager.getInstance()
        	 				.getHandler(CacheMgrConstants.CACHE_NAME_LANGUAGE_MASTER);

        	 if(!languageMasterHandler.isValidLanguageId(languageId)) {
        		 logger.debug("Invalid Language Id :["+languageId+"].Setting Default Language Id :["+DEFAULT_LANGUAGE_ID+"]");
        		 order.setLanguageId(DEFAULT_LANGUAGE_ID);
        		 CommonUtils.sendSystemMessage("Error occurring while processing order.  Invalid languageId received: " + languageId);
        	 } else {
        		 order.setLanguageId(languageId.trim().toUpperCase());
        	 }
         }
    }

    /**
     * non-static wrapper around the static method.  Helps with unit testing
     * @param order
     * @param con
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws SQLException
     * @throws ParseException
     * @throws Exception
     */
    public OrderVO massageOrderDataNotStatic(OrderVO order, Connection con) throws IOException, SAXException,
				    ParserConfigurationException, TransformerException, SQLException,
				    ParseException, Exception {

    	return massageOrderData(order, con);

    }

	/**
	 * This method is to calculate Ship Method.
	 *
	 * @param recipAddress
	 * @param actualAddressType
	 * @param item
	 * @param shipMethodCarrier
	 * @param addonIds
	 * @param formattedZip
	 * @param conn
	 * @param vdau
	 * @param maxDelivaryDates
	 * @throws Exception
	 */
	public static void calculateItemShipMethodDetails(RecipientAddressesVO recipAddress, String actualAddressType,
			OrderDetailsVO item, String shipMethodCarrier, List<String> addonIds, String formattedZip, Connection conn,
			ValidationDataAccessUtil vdau, int maxDelivaryDates) throws Exception {

		logger.info("calculateItemShipMethodDetails(): original: "
				+ item.getExternalOrderNumber() + ", shipMethod on item: "
				+ item.getShipMethod() + ", shipDate on item: "
				+ item.getShipDate() + " DeliveryDate: "
				+ item.getDeliveryDate());

		ProductAvailVO nextAvailProdDateVO = getNextB2BAvailableDate(recipAddress, actualAddressType, item.getDeliveryDate(),
				ValidationConstants.YES.equals(shipMethodCarrier), conn, item.getProductId(), addonIds,
				maxDelivaryDates,	item.getSourceCode());

		logger.debug("calculateItemShipMethodDetails(): calculated: " + nextAvailProdDateVO);

		// When product is unavailable, set ship date as null.
		if (nextAvailProdDateVO == null || (!nextAvailProdDateVO.isIsAvailable()) || !(nextAvailProdDateVO.isIsAddOnAvailable())) {
			logger.info("*No Delivery Dates available for this product. " + item.getProductId());
			item.setShipDate("");
		}
		// reset with recalculated ship method as per Business rules
		else if (nextAvailProdDateVO.getDeliveryDate() != null) {
			item.setDeliveryDate(nextAvailProdDateVO.getDeliveryDate());
			item.setShipMethod(getShipMethodForDeliveryDate(nextAvailProdDateVO));
		}
	}

	/** This method will return Product Availability Details Based on Floral or Vendor.
	 * @param recipAddress
	 * @param actualAddressType
	 * @param deliveryDate
	 * @param isCarrierDelivered
	 * @param conn
	 * @param productId
	 * @param formattedZip
	 * @param addonIds
	 * @param maxDeliveryDates
	 * @return
	 * @throws Exception
	 */
	private static ProductAvailVO getNextB2BAvailableDate(RecipientAddressesVO recipAddress, String actualAddressType,
			String deliveryDate, boolean isCarrierDelivered, Connection conn, String productId, List<String> addonIds,
			int maxDeliveryDates, String sourceCode) throws Exception {

		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		ProductAvailVO nextAvailProdDateVO = null;

		try {

			if(!isInValidDeliveryDate(deliveryDate)) { //null, invalid format, past date
				logger.info("DeliveryDate is valid, checking availability for: " + deliveryDate);
				nextAvailProdDateVO = PASServiceUtil.getProductAvailability(productId, sdf.parse(deliveryDate), recipAddress.getPostalCode(), recipAddress.getCountry(), addonIds, sourceCode, false, false);
			} else {
				logger.info("Original DeliveryDate " + "deliveryDate" + " is invalid, default it to today's date.");
				deliveryDate = sdf.format(new Date());
				logger.info("deliveryDate: " + deliveryDate);
			}

			if (nextAvailProdDateVO == null || (!nextAvailProdDateVO.isIsAvailable()) || !nextAvailProdDateVO.isIsAddOnAvailable()) {
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(deliveryDate));

				if (isCarrierDelivered) {
					if ("B".equals(actualAddressType)) {
						logger.info("location type is business (isCarrierDelivered) -- delivery not available or delivery date is SAT. Choose next available delivery date other than SAT");
						nextAvailProdDateVO = PASServiceUtil.getPASNoSATFirstAvailableDate(productId, addonIds, c, recipAddress.getPostalCode(),  recipAddress.getCountry(),
								sourceCode, maxDeliveryDates);
					} else {
						//If location type is anything other than business, the system will not choose Sunday delivery dates.
						logger.info("location type non-business (isCarrierDelivered) -- delivery not available or delivery date is SUN. Choose next available delivery date other than SUN");
						nextAvailProdDateVO = PASServiceUtil.getPASNoSUNFirstAvailableDate(productId, addonIds, c, recipAddress.getPostalCode(),  recipAddress.getCountry(),
								sourceCode, maxDeliveryDates);
					}
				} else {
					if ("B".equals(actualAddressType)) {
						logger.info("location type is business (isFloralDelivered) -- delivery not available or delivery date is today or SUN/SAT. Choose next available delivery date other than SAT/SUN");
						nextAvailProdDateVO = PASServiceUtil.getPASNotSameDayFirstAvailableWeekDate(productId, addonIds, c, recipAddress.getPostalCode(),  recipAddress.getCountry(),
								sourceCode, maxDeliveryDates);
					}
					// If location type is anything other business, assign the next available date as the delivery date.
					else {
						logger.info("location type non-business (isFloralDelivered) -- delivery not available or delivery date is today. Choose next available delivery date.");
						nextAvailProdDateVO = PASServiceUtil.getPASNotSameDayFirstAvailableDate(productId, addonIds, c, recipAddress.getPostalCode(),  recipAddress.getCountry(),
								sourceCode, maxDeliveryDates);
					}
					if(nextAvailProdDateVO != null && nextAvailProdDateVO.isIsAddOnAvailable() == null) {
						nextAvailProdDateVO.setIsAddOnAvailable(true);//Add on is always available from EAST PAS
			    	}
				}
			}

		} catch (Exception e) {
			logger.error("Exception caught checking availability: ", e);
			nextAvailProdDateVO = null;
		}
		return nextAvailProdDateVO;
	}

	/**
	 * This method is to return shipmethod based on delivery date.
	 * @param nextAvailProdDateVO
	 * @return
	 */
	public static String  getShipMethodForDeliveryDate(ProductAvailVO nextAvailProdDateVO)throws Exception{
		logger.debug("DataMassager : getShipMethodForDeliveryDate ");
		String shipMethod=null;
		List<String> shippingMethods = new ArrayList<String>();
		shippingMethods = PASQueryUtil.getValidShippingMethods(nextAvailProdDateVO);
		if (shippingMethods != null) {
			// Find the cheapest ship method
			shipMethod = PASParamUtil.getCheapestShipMethod(shippingMethods);
			logger.info(" new ship method (PAS): "+ shipMethod);
		}
		return shipMethod;
	}

	/**
	 * This method checking 1)delivery date is past or not. 2)delivery date
	 * format.
	 *
	 * @param deliveryDate
	 * @return
	 */
	private static boolean isInValidDeliveryDate(String deliveryDate) {
		DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);

		if (deliveryDate != null && deliveryDate.trim().length() > 0) {
			try {
				long dateDiff = DeliveryDateUTIL.getDateDiff(new Date(), sdf.parse(deliveryDate));
				logger.info("Days to DeliveryStartDate from today::" + dateDiff);
				if (dateDiff < 0) {
					return true; // is past
				}
			} catch (Exception e) {
				logger.info("Delivery Date format is not valid :" + deliveryDate);
				return true; // is invalid
			}
		} else {
			logger.info("delivery date is empty calling next Availability");
			return true; // delivery date does not exist
		}
		return false;
	}
}
