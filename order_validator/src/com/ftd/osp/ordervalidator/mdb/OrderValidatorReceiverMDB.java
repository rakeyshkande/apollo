package com.ftd.osp.ordervalidator.mdb;

import com.ftd.osp.framework.receiver.ReceiverMDB;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;

import javax.naming.Context;
import javax.naming.InitialContext;

public class OrderValidatorReceiverMDB extends ReceiverMDB
{
    public OrderValidatorReceiverMDB()
    {
        super();
    }

    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
        this.messageDrivenContext = ctx;

        try
        {
            logger = new Logger("com.ftd.osp.framework.receiver.Receiver");

            logger.info("Begin Registering Component");
            InitialContext initContext = new InitialContext();
            myenv = (Context) initContext.lookup("");


            businessObjectClassName = "com.ftd.osp.ordervalidator.OrderValidatorBO";
            logger.debug("Business Object Class Name :: " + businessObjectClassName);
            //rollbackStatusName = "com.ftd.osp.ordervalidator.OrderValidatorBO";
            rollbackStatusName = "ROLLBACK";
            logger.debug("Rollback Status Name :: " + rollbackStatusName);
            rollbackWarningStatus = "RB_WARNING";
            logger.debug("Rollback Warning Status :: " + rollbackWarningStatus);
            boRequiresJDBCConnection = true;
            dataSourceName = "ORDER SCRUB";
            logger.debug("Data Source Name :: " + dataSourceName);
            dataSource = DataSourceUtil.getInstance().getDataSource(dataSourceName);
            dispatchOrderToQueue = true;
            logger.info("Dispatch Order To Queue :: " + dispatchOrderToQueue);
            persistMessageToken = false;
            logger.debug("Persist Message Token :: " + persistMessageToken);
            componentRole = "ORDER VALIDATOR";
            logger.info("Component Role :: " + componentRole);
            logger.info("End Registering Component");

        } catch (Exception ex)
        {
            logger.error(ex);
        } finally
        {
            ++count;
            logger.debug("MDB Count " + count);
        }

    }

    public void onMessage(Message msg)
    {
        super.onMessage(msg);
        if (messageDrivenContext.getRollbackOnly())
        	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
    }
}
