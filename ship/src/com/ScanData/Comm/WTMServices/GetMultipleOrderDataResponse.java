/**
 * GetMultipleOrderDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetMultipleOrderDataResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getMultipleOrderDataResult;

    public GetMultipleOrderDataResponse() {
    }

    public GetMultipleOrderDataResponse(
           com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getMultipleOrderDataResult) {
           this.getMultipleOrderDataResult = getMultipleOrderDataResult;
    }


    /**
     * Gets the getMultipleOrderDataResult value for this GetMultipleOrderDataResponse.
     * 
     * @return getMultipleOrderDataResult
     */
    public com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getGetMultipleOrderDataResult() {
        return getMultipleOrderDataResult;
    }


    /**
     * Sets the getMultipleOrderDataResult value for this GetMultipleOrderDataResponse.
     * 
     * @param getMultipleOrderDataResult
     */
    public void setGetMultipleOrderDataResult(com.ScanData.Comm.WTMServices.GetMultipleOrderDataResponseGetMultipleOrderDataResult getMultipleOrderDataResult) {
        this.getMultipleOrderDataResult = getMultipleOrderDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMultipleOrderDataResponse)) return false;
        GetMultipleOrderDataResponse other = (GetMultipleOrderDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getMultipleOrderDataResult==null && other.getGetMultipleOrderDataResult()==null) || 
             (this.getMultipleOrderDataResult!=null &&
              this.getMultipleOrderDataResult.equals(other.getGetMultipleOrderDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetMultipleOrderDataResult() != null) {
            _hashCode += getGetMultipleOrderDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMultipleOrderDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetMultipleOrderDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getMultipleOrderDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetMultipleOrderDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetMultipleOrderDataResponse>GetMultipleOrderDataResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
