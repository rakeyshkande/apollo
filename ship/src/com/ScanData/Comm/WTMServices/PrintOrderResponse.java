/**
 * PrintOrderResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class PrintOrderResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult printOrderResult;

    public PrintOrderResponse() {
    }

    public PrintOrderResponse(
           com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult printOrderResult) {
           this.printOrderResult = printOrderResult;
    }


    /**
     * Gets the printOrderResult value for this PrintOrderResponse.
     * 
     * @return printOrderResult
     */
    public com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult getPrintOrderResult() {
        return printOrderResult;
    }


    /**
     * Sets the printOrderResult value for this PrintOrderResponse.
     * 
     * @param printOrderResult
     */
    public void setPrintOrderResult(com.ScanData.Comm.WTMServices.PrintOrderResponsePrintOrderResult printOrderResult) {
        this.printOrderResult = printOrderResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrintOrderResponse)) return false;
        PrintOrderResponse other = (PrintOrderResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.printOrderResult==null && other.getPrintOrderResult()==null) || 
             (this.printOrderResult!=null &&
              this.printOrderResult.equals(other.getPrintOrderResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPrintOrderResult() != null) {
            _hashCode += getPrintOrderResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrintOrderResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">PrintOrderResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("printOrderResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintOrderResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>PrintOrderResponse>PrintOrderResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
