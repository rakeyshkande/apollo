/**
 * GetLineOfBusinessListResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetLineOfBusinessListResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getLineOfBusinessListResult;

    public GetLineOfBusinessListResponse() {
    }

    public GetLineOfBusinessListResponse(
           com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getLineOfBusinessListResult) {
           this.getLineOfBusinessListResult = getLineOfBusinessListResult;
    }


    /**
     * Gets the getLineOfBusinessListResult value for this GetLineOfBusinessListResponse.
     * 
     * @return getLineOfBusinessListResult
     */
    public com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getGetLineOfBusinessListResult() {
        return getLineOfBusinessListResult;
    }


    /**
     * Sets the getLineOfBusinessListResult value for this GetLineOfBusinessListResponse.
     * 
     * @param getLineOfBusinessListResult
     */
    public void setGetLineOfBusinessListResult(com.ScanData.Comm.WTMServices.GetLineOfBusinessListResponseGetLineOfBusinessListResult getLineOfBusinessListResult) {
        this.getLineOfBusinessListResult = getLineOfBusinessListResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetLineOfBusinessListResponse)) return false;
        GetLineOfBusinessListResponse other = (GetLineOfBusinessListResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getLineOfBusinessListResult==null && other.getGetLineOfBusinessListResult()==null) || 
             (this.getLineOfBusinessListResult!=null &&
              this.getLineOfBusinessListResult.equals(other.getGetLineOfBusinessListResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetLineOfBusinessListResult() != null) {
            _hashCode += getGetLineOfBusinessListResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetLineOfBusinessListResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetLineOfBusinessListResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getLineOfBusinessListResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetLineOfBusinessListResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetLineOfBusinessListResponse>GetLineOfBusinessListResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
