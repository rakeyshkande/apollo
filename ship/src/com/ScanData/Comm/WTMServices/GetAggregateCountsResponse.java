/**
 * GetAggregateCountsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetAggregateCountsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getAggregateCountsResult;

    public GetAggregateCountsResponse() {
    }

    public GetAggregateCountsResponse(
           com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getAggregateCountsResult) {
           this.getAggregateCountsResult = getAggregateCountsResult;
    }


    /**
     * Gets the getAggregateCountsResult value for this GetAggregateCountsResponse.
     * 
     * @return getAggregateCountsResult
     */
    public com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getGetAggregateCountsResult() {
        return getAggregateCountsResult;
    }


    /**
     * Sets the getAggregateCountsResult value for this GetAggregateCountsResponse.
     * 
     * @param getAggregateCountsResult
     */
    public void setGetAggregateCountsResult(com.ScanData.Comm.WTMServices.GetAggregateCountsResponseGetAggregateCountsResult getAggregateCountsResult) {
        this.getAggregateCountsResult = getAggregateCountsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAggregateCountsResponse)) return false;
        GetAggregateCountsResponse other = (GetAggregateCountsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getAggregateCountsResult==null && other.getGetAggregateCountsResult()==null) || 
             (this.getAggregateCountsResult!=null &&
              this.getAggregateCountsResult.equals(other.getGetAggregateCountsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetAggregateCountsResult() != null) {
            _hashCode += getGetAggregateCountsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAggregateCountsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetAggregateCountsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getAggregateCountsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetAggregateCountsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetAggregateCountsResponse>GetAggregateCountsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
