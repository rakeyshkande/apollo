/**
 * PrintBatch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class PrintBatch  implements java.io.Serializable {
    private java.lang.String distributionCenter;

    private java.lang.String lineOfBusiness;

    private java.lang.String printBatchID;

    private int startSequence;

    private int endSequence;

    public PrintBatch() {
    }

    public PrintBatch(
           java.lang.String distributionCenter,
           java.lang.String lineOfBusiness,
           java.lang.String printBatchID,
           int startSequence,
           int endSequence) {
           this.distributionCenter = distributionCenter;
           this.lineOfBusiness = lineOfBusiness;
           this.printBatchID = printBatchID;
           this.startSequence = startSequence;
           this.endSequence = endSequence;
    }


    /**
     * Gets the distributionCenter value for this PrintBatch.
     * 
     * @return distributionCenter
     */
    public java.lang.String getDistributionCenter() {
        return distributionCenter;
    }


    /**
     * Sets the distributionCenter value for this PrintBatch.
     * 
     * @param distributionCenter
     */
    public void setDistributionCenter(java.lang.String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }


    /**
     * Gets the lineOfBusiness value for this PrintBatch.
     * 
     * @return lineOfBusiness
     */
    public java.lang.String getLineOfBusiness() {
        return lineOfBusiness;
    }


    /**
     * Sets the lineOfBusiness value for this PrintBatch.
     * 
     * @param lineOfBusiness
     */
    public void setLineOfBusiness(java.lang.String lineOfBusiness) {
        this.lineOfBusiness = lineOfBusiness;
    }


    /**
     * Gets the printBatchID value for this PrintBatch.
     * 
     * @return printBatchID
     */
    public java.lang.String getPrintBatchID() {
        return printBatchID;
    }


    /**
     * Sets the printBatchID value for this PrintBatch.
     * 
     * @param printBatchID
     */
    public void setPrintBatchID(java.lang.String printBatchID) {
        this.printBatchID = printBatchID;
    }


    /**
     * Gets the startSequence value for this PrintBatch.
     * 
     * @return startSequence
     */
    public int getStartSequence() {
        return startSequence;
    }


    /**
     * Sets the startSequence value for this PrintBatch.
     * 
     * @param startSequence
     */
    public void setStartSequence(int startSequence) {
        this.startSequence = startSequence;
    }


    /**
     * Gets the endSequence value for this PrintBatch.
     * 
     * @return endSequence
     */
    public int getEndSequence() {
        return endSequence;
    }


    /**
     * Sets the endSequence value for this PrintBatch.
     * 
     * @param endSequence
     */
    public void setEndSequence(int endSequence) {
        this.endSequence = endSequence;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrintBatch)) return false;
        PrintBatch other = (PrintBatch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.distributionCenter==null && other.getDistributionCenter()==null) || 
             (this.distributionCenter!=null &&
              this.distributionCenter.equals(other.getDistributionCenter()))) &&
            ((this.lineOfBusiness==null && other.getLineOfBusiness()==null) || 
             (this.lineOfBusiness!=null &&
              this.lineOfBusiness.equals(other.getLineOfBusiness()))) &&
            ((this.printBatchID==null && other.getPrintBatchID()==null) || 
             (this.printBatchID!=null &&
              this.printBatchID.equals(other.getPrintBatchID()))) &&
            this.startSequence == other.getStartSequence() &&
            this.endSequence == other.getEndSequence();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDistributionCenter() != null) {
            _hashCode += getDistributionCenter().hashCode();
        }
        if (getLineOfBusiness() != null) {
            _hashCode += getLineOfBusiness().hashCode();
        }
        if (getPrintBatchID() != null) {
            _hashCode += getPrintBatchID().hashCode();
        }
        _hashCode += getStartSequence();
        _hashCode += getEndSequence();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrintBatch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">PrintBatch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distributionCenter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "DistributionCenter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineOfBusiness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "LineOfBusiness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("printBatchID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "PrintBatchID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "StartSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "EndSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
