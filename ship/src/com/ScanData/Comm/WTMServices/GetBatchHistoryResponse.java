/**
 * GetBatchHistoryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetBatchHistoryResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getBatchHistoryResult;

    public GetBatchHistoryResponse() {
    }

    public GetBatchHistoryResponse(
           com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getBatchHistoryResult) {
           this.getBatchHistoryResult = getBatchHistoryResult;
    }


    /**
     * Gets the getBatchHistoryResult value for this GetBatchHistoryResponse.
     * 
     * @return getBatchHistoryResult
     */
    public com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getGetBatchHistoryResult() {
        return getBatchHistoryResult;
    }


    /**
     * Sets the getBatchHistoryResult value for this GetBatchHistoryResponse.
     * 
     * @param getBatchHistoryResult
     */
    public void setGetBatchHistoryResult(com.ScanData.Comm.WTMServices.GetBatchHistoryResponseGetBatchHistoryResult getBatchHistoryResult) {
        this.getBatchHistoryResult = getBatchHistoryResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBatchHistoryResponse)) return false;
        GetBatchHistoryResponse other = (GetBatchHistoryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBatchHistoryResult==null && other.getGetBatchHistoryResult()==null) || 
             (this.getBatchHistoryResult!=null &&
              this.getBatchHistoryResult.equals(other.getGetBatchHistoryResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBatchHistoryResult() != null) {
            _hashCode += getGetBatchHistoryResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBatchHistoryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetBatchHistoryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBatchHistoryResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetBatchHistoryResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetBatchHistoryResponse>GetBatchHistoryResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
