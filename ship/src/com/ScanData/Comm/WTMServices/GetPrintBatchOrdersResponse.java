/**
 * GetPrintBatchOrdersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class GetPrintBatchOrdersResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getPrintBatchOrdersResult;

    public GetPrintBatchOrdersResponse() {
    }

    public GetPrintBatchOrdersResponse(
           com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getPrintBatchOrdersResult) {
           this.getPrintBatchOrdersResult = getPrintBatchOrdersResult;
    }


    /**
     * Gets the getPrintBatchOrdersResult value for this GetPrintBatchOrdersResponse.
     * 
     * @return getPrintBatchOrdersResult
     */
    public com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getGetPrintBatchOrdersResult() {
        return getPrintBatchOrdersResult;
    }


    /**
     * Sets the getPrintBatchOrdersResult value for this GetPrintBatchOrdersResponse.
     * 
     * @param getPrintBatchOrdersResult
     */
    public void setGetPrintBatchOrdersResult(com.ScanData.Comm.WTMServices.GetPrintBatchOrdersResponseGetPrintBatchOrdersResult getPrintBatchOrdersResult) {
        this.getPrintBatchOrdersResult = getPrintBatchOrdersResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPrintBatchOrdersResponse)) return false;
        GetPrintBatchOrdersResponse other = (GetPrintBatchOrdersResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPrintBatchOrdersResult==null && other.getGetPrintBatchOrdersResult()==null) || 
             (this.getPrintBatchOrdersResult!=null &&
              this.getPrintBatchOrdersResult.equals(other.getGetPrintBatchOrdersResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPrintBatchOrdersResult() != null) {
            _hashCode += getGetPrintBatchOrdersResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPrintBatchOrdersResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">GetPrintBatchOrdersResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPrintBatchOrdersResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "GetPrintBatchOrdersResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>GetPrintBatchOrdersResponse>GetPrintBatchOrdersResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
