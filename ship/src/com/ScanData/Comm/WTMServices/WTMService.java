/**
 * WTMService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public interface WTMService extends javax.xml.rpc.Service {

/**
 * ScanData Web Transportation Manifest
 */
    public java.lang.String getWTMServiceSoapAddress();

    public com.ScanData.Comm.WTMServices.WTMServiceSoap_PortType getWTMServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.ScanData.Comm.WTMServices.WTMServiceSoap_PortType getWTMServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getWTMServiceSoap12Address();

    public com.ScanData.Comm.WTMServices.WTMServiceSoap_PortType getWTMServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.ScanData.Comm.WTMServices.WTMServiceSoap_PortType getWTMServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
