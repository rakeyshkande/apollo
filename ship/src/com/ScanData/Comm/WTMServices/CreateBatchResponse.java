/**
 * CreateBatchResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WTMServices;

public class CreateBatchResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult createBatchResult;

    public CreateBatchResponse() {
    }

    public CreateBatchResponse(
           com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult createBatchResult) {
           this.createBatchResult = createBatchResult;
    }


    /**
     * Gets the createBatchResult value for this CreateBatchResponse.
     * 
     * @return createBatchResult
     */
    public com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult getCreateBatchResult() {
        return createBatchResult;
    }


    /**
     * Sets the createBatchResult value for this CreateBatchResponse.
     * 
     * @param createBatchResult
     */
    public void setCreateBatchResult(com.ScanData.Comm.WTMServices.CreateBatchResponseCreateBatchResult createBatchResult) {
        this.createBatchResult = createBatchResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateBatchResponse)) return false;
        CreateBatchResponse other = (CreateBatchResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.createBatchResult==null && other.getCreateBatchResult()==null) || 
             (this.createBatchResult!=null &&
              this.createBatchResult.equals(other.getCreateBatchResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreateBatchResult() != null) {
            _hashCode += getCreateBatchResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateBatchResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">CreateBatchResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createBatchResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/", "CreateBatchResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/", ">>CreateBatchResponse>CreateBatchResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
