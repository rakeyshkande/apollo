/**
 * UpdateShipUnits.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class UpdateShipUnits  implements java.io.Serializable {
    private int sessionID;

    private com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams updateShipUnitsParams;

    public UpdateShipUnits() {
    }

    public UpdateShipUnits(
           int sessionID,
           com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams updateShipUnitsParams) {
           this.sessionID = sessionID;
           this.updateShipUnitsParams = updateShipUnitsParams;
    }


    /**
     * Gets the sessionID value for this UpdateShipUnits.
     * 
     * @return sessionID
     */
    public int getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this UpdateShipUnits.
     * 
     * @param sessionID
     */
    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the updateShipUnitsParams value for this UpdateShipUnits.
     * 
     * @return updateShipUnitsParams
     */
    public com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams getUpdateShipUnitsParams() {
        return updateShipUnitsParams;
    }


    /**
     * Sets the updateShipUnitsParams value for this UpdateShipUnits.
     * 
     * @param updateShipUnitsParams
     */
    public void setUpdateShipUnitsParams(com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams updateShipUnitsParams) {
        this.updateShipUnitsParams = updateShipUnitsParams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateShipUnits)) return false;
        UpdateShipUnits other = (UpdateShipUnits) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sessionID == other.getSessionID() &&
            ((this.updateShipUnitsParams==null && other.getUpdateShipUnitsParams()==null) || 
             (this.updateShipUnitsParams!=null &&
              this.updateShipUnitsParams.equals(other.getUpdateShipUnitsParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSessionID();
        if (getUpdateShipUnitsParams() != null) {
            _hashCode += getUpdateShipUnitsParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateShipUnits.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UpdateShipUnits"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateShipUnitsParams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UpdateShipUnitsParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnits>UpdateShipUnitsParams"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
