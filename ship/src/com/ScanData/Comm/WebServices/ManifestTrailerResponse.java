/**
 * ManifestTrailerResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class ManifestTrailerResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult manifestTrailerResult;

    public ManifestTrailerResponse() {
    }

    public ManifestTrailerResponse(
           com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult manifestTrailerResult) {
           this.manifestTrailerResult = manifestTrailerResult;
    }


    /**
     * Gets the manifestTrailerResult value for this ManifestTrailerResponse.
     * 
     * @return manifestTrailerResult
     */
    public com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult getManifestTrailerResult() {
        return manifestTrailerResult;
    }


    /**
     * Sets the manifestTrailerResult value for this ManifestTrailerResponse.
     * 
     * @param manifestTrailerResult
     */
    public void setManifestTrailerResult(com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult manifestTrailerResult) {
        this.manifestTrailerResult = manifestTrailerResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManifestTrailerResponse)) return false;
        ManifestTrailerResponse other = (ManifestTrailerResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.manifestTrailerResult==null && other.getManifestTrailerResult()==null) || 
             (this.manifestTrailerResult!=null &&
              this.manifestTrailerResult.equals(other.getManifestTrailerResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getManifestTrailerResult() != null) {
            _hashCode += getManifestTrailerResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManifestTrailerResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestTrailerResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manifestTrailerResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestTrailerResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestTrailerResponse>ManifestTrailerResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
