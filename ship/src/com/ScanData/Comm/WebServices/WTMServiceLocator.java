/**
 * WTMServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class WTMServiceLocator extends org.apache.axis.client.Service implements com.ScanData.Comm.WebServices.WTMService {

/**
 * ScanData Web Transportation Manifest
 */

    public WTMServiceLocator() {
    }


    public WTMServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WTMServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WTMServiceSoap
    private java.lang.String WTMServiceSoap_address = "http://localhost/WTMService.asmx";

    public java.lang.String getWTMServiceSoapAddress() {
        return WTMServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WTMServiceSoapWSDDServiceName = "WTMServiceSoap";

    public java.lang.String getWTMServiceSoapWSDDServiceName() {
        return WTMServiceSoapWSDDServiceName;
    }

    public void setWTMServiceSoapWSDDServiceName(java.lang.String name) {
        WTMServiceSoapWSDDServiceName = name;
    }

    public com.ScanData.Comm.WebServices.WTMServiceSoap_PortType getWTMServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WTMServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWTMServiceSoap(endpoint);
    }

    public com.ScanData.Comm.WebServices.WTMServiceSoap_PortType getWTMServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ScanData.Comm.WebServices.WTMServiceSoap_BindingStub _stub = new com.ScanData.Comm.WebServices.WTMServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getWTMServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWTMServiceSoapEndpointAddress(java.lang.String address) {
        WTMServiceSoap_address = address;
    }


    // Use to get a proxy class for WTMServiceSoap12
    private java.lang.String WTMServiceSoap12_address = "http://localhost/WTMService.asmx";

    public java.lang.String getWTMServiceSoap12Address() {
        return WTMServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WTMServiceSoap12WSDDServiceName = "WTMServiceSoap12";

    public java.lang.String getWTMServiceSoap12WSDDServiceName() {
        return WTMServiceSoap12WSDDServiceName;
    }

    public void setWTMServiceSoap12WSDDServiceName(java.lang.String name) {
        WTMServiceSoap12WSDDServiceName = name;
    }

    public com.ScanData.Comm.WebServices.WTMServiceSoap_PortType getWTMServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WTMServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWTMServiceSoap12(endpoint);
    }

    public com.ScanData.Comm.WebServices.WTMServiceSoap_PortType getWTMServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ScanData.Comm.WebServices.WTMServiceSoap12Stub _stub = new com.ScanData.Comm.WebServices.WTMServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getWTMServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWTMServiceSoap12EndpointAddress(java.lang.String address) {
        WTMServiceSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ScanData.Comm.WebServices.WTMServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ScanData.Comm.WebServices.WTMServiceSoap_BindingStub _stub = new com.ScanData.Comm.WebServices.WTMServiceSoap_BindingStub(new java.net.URL(WTMServiceSoap_address), this);
                _stub.setPortName(getWTMServiceSoapWSDDServiceName());
                return _stub;
            }
            if (com.ScanData.Comm.WebServices.WTMServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ScanData.Comm.WebServices.WTMServiceSoap12Stub _stub = new com.ScanData.Comm.WebServices.WTMServiceSoap12Stub(new java.net.URL(WTMServiceSoap12_address), this);
                _stub.setPortName(getWTMServiceSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WTMServiceSoap".equals(inputPortName)) {
            return getWTMServiceSoap();
        }
        else if ("WTMServiceSoap12".equals(inputPortName)) {
            return getWTMServiceSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ScanData.com/WTM/", "WTMService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "WTMServiceSoap"));
            ports.add(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "WTMServiceSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WTMServiceSoap".equals(portName)) {
            setWTMServiceSoapEndpointAddress(address);
        }
        else 
if ("WTMServiceSoap12".equals(portName)) {
            setWTMServiceSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
