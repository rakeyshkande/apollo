/**
 * RateShipUnits.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class RateShipUnits  implements java.io.Serializable {
    private int sessionID;

    private com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams rateShipUnitsParams;

    public RateShipUnits() {
    }

    public RateShipUnits(
           int sessionID,
           com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams rateShipUnitsParams) {
           this.sessionID = sessionID;
           this.rateShipUnitsParams = rateShipUnitsParams;
    }


    /**
     * Gets the sessionID value for this RateShipUnits.
     * 
     * @return sessionID
     */
    public int getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this RateShipUnits.
     * 
     * @param sessionID
     */
    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the rateShipUnitsParams value for this RateShipUnits.
     * 
     * @return rateShipUnitsParams
     */
    public com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams getRateShipUnitsParams() {
        return rateShipUnitsParams;
    }


    /**
     * Sets the rateShipUnitsParams value for this RateShipUnits.
     * 
     * @param rateShipUnitsParams
     */
    public void setRateShipUnitsParams(com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams rateShipUnitsParams) {
        this.rateShipUnitsParams = rateShipUnitsParams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateShipUnits)) return false;
        RateShipUnits other = (RateShipUnits) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sessionID == other.getSessionID() &&
            ((this.rateShipUnitsParams==null && other.getRateShipUnitsParams()==null) || 
             (this.rateShipUnitsParams!=null &&
              this.rateShipUnitsParams.equals(other.getRateShipUnitsParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSessionID();
        if (getRateShipUnitsParams() != null) {
            _hashCode += getRateShipUnitsParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateShipUnits.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">RateShipUnits"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateShipUnitsParams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "RateShipUnitsParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnits>RateShipUnitsParams"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
