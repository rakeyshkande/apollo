/**
 * CreateShipmentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CreateShipmentResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult createShipmentResult;

    public CreateShipmentResponse() {
    }

    public CreateShipmentResponse(
           com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult createShipmentResult) {
           this.createShipmentResult = createShipmentResult;
    }


    /**
     * Gets the createShipmentResult value for this CreateShipmentResponse.
     * 
     * @return createShipmentResult
     */
    public com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult getCreateShipmentResult() {
        return createShipmentResult;
    }


    /**
     * Sets the createShipmentResult value for this CreateShipmentResponse.
     * 
     * @param createShipmentResult
     */
    public void setCreateShipmentResult(com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult createShipmentResult) {
        this.createShipmentResult = createShipmentResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateShipmentResponse)) return false;
        CreateShipmentResponse other = (CreateShipmentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.createShipmentResult==null && other.getCreateShipmentResult()==null) || 
             (this.createShipmentResult!=null &&
              this.createShipmentResult.equals(other.getCreateShipmentResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreateShipmentResult() != null) {
            _hashCode += getCreateShipmentResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateShipmentResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateShipmentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createShipmentResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipmentResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipmentResponse>CreateShipmentResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
