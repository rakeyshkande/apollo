/**
 * GetShipUnitsLabelsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class GetShipUnitsLabelsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getShipUnitsLabelsResult;

    public GetShipUnitsLabelsResponse() {
    }

    public GetShipUnitsLabelsResponse(
           com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getShipUnitsLabelsResult) {
           this.getShipUnitsLabelsResult = getShipUnitsLabelsResult;
    }


    /**
     * Gets the getShipUnitsLabelsResult value for this GetShipUnitsLabelsResponse.
     * 
     * @return getShipUnitsLabelsResult
     */
    public com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getGetShipUnitsLabelsResult() {
        return getShipUnitsLabelsResult;
    }


    /**
     * Sets the getShipUnitsLabelsResult value for this GetShipUnitsLabelsResponse.
     * 
     * @param getShipUnitsLabelsResult
     */
    public void setGetShipUnitsLabelsResult(com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getShipUnitsLabelsResult) {
        this.getShipUnitsLabelsResult = getShipUnitsLabelsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetShipUnitsLabelsResponse)) return false;
        GetShipUnitsLabelsResponse other = (GetShipUnitsLabelsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getShipUnitsLabelsResult==null && other.getGetShipUnitsLabelsResult()==null) || 
             (this.getShipUnitsLabelsResult!=null &&
              this.getShipUnitsLabelsResult.equals(other.getGetShipUnitsLabelsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetShipUnitsLabelsResult() != null) {
            _hashCode += getGetShipUnitsLabelsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetShipUnitsLabelsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsLabelsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getShipUnitsLabelsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsLabelsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsLabelsResponse>GetShipUnitsLabelsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
