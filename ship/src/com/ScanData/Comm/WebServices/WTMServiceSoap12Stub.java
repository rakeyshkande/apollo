/**
 * WTMServiceSoap12Stub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class WTMServiceSoap12Stub extends org.apache.axis.client.Stub implements com.ScanData.Comm.WebServices.WTMServiceSoap_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[21];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("HelloWorld");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "HelloWorldResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GoodbyeWorld");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GoodbyeWorldResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("StartSession");
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        oper.setReturnClass(int.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "StartSessionResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("EndSession");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipUnitsParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipUnits>CreateShipUnitsParams"), com.ScanData.Comm.WebServices.CreateShipUnitsCreateShipUnitsParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipUnitsResponse>CreateShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateShipment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipmentParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipment>CreateShipmentParams"), com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipmentResponse>CreateShipmentResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipmentResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CreateBillingAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "BillingAccount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateBillingAccount>BillingAccount"), com.ScanData.Comm.WebServices.CreateBillingAccountBillingAccount.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateBillingAccountResponse>CreateBillingAccountResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateBillingAccountResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteOrders");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "OrderList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteOrders>OrderList"), com.ScanData.Comm.WebServices.DeleteOrdersOrderList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteOrdersResponse>DeleteOrdersResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DeleteOrdersResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CancelShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CancelShipUnits>ShipUnitList"), com.ScanData.Comm.WebServices.CancelShipUnitsShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CancelShipUnitsResponse>CancelShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CancelShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnits>ShipUnitList"), com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnitsResponse>DeleteShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DeleteShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipUnitsLabels");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsLabels>ShipUnitList"), com.ScanData.Comm.WebServices.GetShipUnitsLabelsShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsLabelsResponse>GetShipUnitsLabelsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsLabelsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipUnitsInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsInfo>ShipUnitList"), com.ScanData.Comm.WebServices.GetShipUnitsInfoShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsInfoResponse>GetShipUnitsInfoResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsInfoResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipUnitsReports");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsReports>ShipUnitList"), com.ScanData.Comm.WebServices.GetShipUnitsReportsShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsReportsResponse>GetShipUnitsReportsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsReportsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShipmentReports");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipmentList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipmentReports>ShipmentList"), com.ScanData.Comm.WebServices.GetShipmentReportsShipmentList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipmentReportsResponse>GetShipmentReportsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipmentReportsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("LoadShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "LoadShipUnitsParam"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>LoadShipUnits>LoadShipUnitsParam"), com.ScanData.Comm.WebServices.LoadShipUnitsLoadShipUnitsParam.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>LoadShipUnitsResponse>LoadShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "LoadShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UnloadShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UnloadShipUnits>ShipUnitList"), com.ScanData.Comm.WebServices.UnloadShipUnitsShipUnitList.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UnloadShipUnitsResponse>UnloadShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UnloadShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ManifestTrailer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "TrailerNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestTrailerResponse>ManifestTrailerResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestTrailerResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ManifestCarrier");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CarrierID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DistributionCenter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestCarrierResponse>ManifestCarrierResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestCarrierResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UpdateShipUnitsParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnits>UpdateShipUnitsParams"), com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnitsResponse>UpdateShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UpdateShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RateShipUnits");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "RateShipUnitsParams"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnits>RateShipUnitsParams"), com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnitsResponse>RateShipUnitsResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "RateShipUnitsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ManifestZoneJumpTrailer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "TrailerNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestZoneJumpTrailerResponse>ManifestZoneJumpTrailerResult"));
        oper.setReturnClass(com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestZoneJumpTrailerResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

    }

    public WTMServiceSoap12Stub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public WTMServiceSoap12Stub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public WTMServiceSoap12Stub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CancelShipUnits>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipUnitsShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CancelShipUnitsResponse>CancelShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateBillingAccount>BillingAccount");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateBillingAccountBillingAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateBillingAccountResponse>CreateBillingAccountResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipment>CreateShipmentParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipmentResponse>CreateShipmentResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipUnits>CreateShipUnitsParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipUnitsCreateShipUnitsParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipUnitsResponse>CreateShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteOrders>OrderList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteOrdersOrderList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteOrdersResponse>DeleteOrdersResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnits>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnitsResponse>DeleteShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipmentReports>ShipmentList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentReportsShipmentList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipmentReportsResponse>GetShipmentReportsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsInfo>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsInfoShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsInfoResponse>GetShipUnitsInfoResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsLabels>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsLabelsShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsLabelsResponse>GetShipUnitsLabelsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsReports>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsReportsShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>GetShipUnitsReportsResponse>GetShipUnitsReportsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>LoadShipUnits>LoadShipUnitsParam");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.LoadShipUnitsLoadShipUnitsParam.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>LoadShipUnitsResponse>LoadShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestCarrierResponse>ManifestCarrierResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestTrailerResponse>ManifestTrailerResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestZoneJumpTrailerResponse>ManifestZoneJumpTrailerResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnits>RateShipUnitsParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnitsResponse>RateShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UnloadShipUnits>ShipUnitList");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UnloadShipUnitsShipUnitList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UnloadShipUnitsResponse>UnloadShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnits>UpdateShipUnitsParams");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnitsResponse>UpdateShipUnitsResult");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CancelShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CancelShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CancelShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateBillingAccount");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateBillingAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateBillingAccountResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateBillingAccountResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateShipment");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateShipmentResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipmentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.CreateShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteOrders");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteOrders.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteOrdersResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteOrdersResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.DeleteShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipmentReports");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentReports.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipmentReportsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipmentReportsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsInfo");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsInfoResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsInfoResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsLabels");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsLabels.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsLabelsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsReports");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsReports.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">GetShipUnitsReportsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.GetShipUnitsReportsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">LoadShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.LoadShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">LoadShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.LoadShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestCarrier");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestCarrier.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestCarrierResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestCarrierResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestTrailer");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestTrailer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestTrailerResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestTrailerResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestZoneJumpTrailer");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestZoneJumpTrailer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestZoneJumpTrailerResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">RateShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">RateShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.RateShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UnloadShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UnloadShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UnloadShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UnloadShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UpdateShipUnits");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UpdateShipUnits.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UpdateShipUnitsResponse");
            cachedSerQNames.add(qName);
            cls = com.ScanData.Comm.WebServices.UpdateShipUnitsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public java.lang.String helloWorld() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/HelloWorld");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "HelloWorld"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String goodbyeWorld() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/GoodbyeWorld");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GoodbyeWorld"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public int startSession() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/StartSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "StartSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return ((java.lang.Integer) _resp).intValue();
            } catch (java.lang.Exception _exception) {
                return ((java.lang.Integer) org.apache.axis.utils.JavaUtils.convert(_resp, int.class)).intValue();
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void endSession(int sessionID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/EndSession");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "EndSession"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult createShipUnits(int sessionID, com.ScanData.Comm.WebServices.CreateShipUnitsCreateShipUnitsParams createShipUnitsParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/CreateShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), createShipUnitsParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult createShipment(int sessionID, com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams createShipmentParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/CreateShipment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), createShipmentParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult createBillingAccount(int sessionID, com.ScanData.Comm.WebServices.CreateBillingAccountBillingAccount billingAccount) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/CreateBillingAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateBillingAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), billingAccount});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult deleteOrders(int sessionID, com.ScanData.Comm.WebServices.DeleteOrdersOrderList orderList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/DeleteOrders");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DeleteOrders"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), orderList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult cancelShipUnits(int sessionID, com.ScanData.Comm.WebServices.CancelShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/CancelShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CancelShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult deleteShipUnits(int sessionID, com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/DeleteShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DeleteShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getShipUnitsLabels(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsLabelsShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/GetShipUnitsLabels");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsLabels"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult getShipUnitsInfo(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsInfoShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/GetShipUnitsInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult getShipUnitsReports(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsReportsShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/GetShipUnitsReports");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipUnitsReports"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult getShipmentReports(int sessionID, com.ScanData.Comm.WebServices.GetShipmentReportsShipmentList shipmentList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/GetShipmentReports");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "GetShipmentReports"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipmentList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult loadShipUnits(int sessionID, com.ScanData.Comm.WebServices.LoadShipUnitsLoadShipUnitsParam loadShipUnitsParam) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/LoadShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "LoadShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), loadShipUnitsParam});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult unloadShipUnits(int sessionID, com.ScanData.Comm.WebServices.UnloadShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/UnloadShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UnloadShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), shipUnitList});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult manifestTrailer(int sessionID, java.lang.String trailerNumber, java.lang.String distributionCenter) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/ManifestTrailer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestTrailer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), trailerNumber, distributionCenter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult manifestCarrier(int sessionID, java.lang.String carrierID, java.lang.String distributionCenter) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/ManifestCarrier");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestCarrier"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), carrierID, distributionCenter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult updateShipUnits(int sessionID, com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams updateShipUnitsParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/UpdateShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UpdateShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), updateShipUnitsParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult rateShipUnits(int sessionID, com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams rateShipUnitsParams) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/RateShipUnits");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "RateShipUnits"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), rateShipUnitsParams});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult manifestZoneJumpTrailer(int sessionID, java.lang.String trailerNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://ScanData.com/WTM/ManifestZoneJumpTrailer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestZoneJumpTrailer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(sessionID), trailerNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
