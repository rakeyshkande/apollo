/**
 * DeleteShipUnitsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class DeleteShipUnitsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult deleteShipUnitsResult;

    public DeleteShipUnitsResponse() {
    }

    public DeleteShipUnitsResponse(
           com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult deleteShipUnitsResult) {
           this.deleteShipUnitsResult = deleteShipUnitsResult;
    }


    /**
     * Gets the deleteShipUnitsResult value for this DeleteShipUnitsResponse.
     * 
     * @return deleteShipUnitsResult
     */
    public com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult getDeleteShipUnitsResult() {
        return deleteShipUnitsResult;
    }


    /**
     * Sets the deleteShipUnitsResult value for this DeleteShipUnitsResponse.
     * 
     * @param deleteShipUnitsResult
     */
    public void setDeleteShipUnitsResult(com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult deleteShipUnitsResult) {
        this.deleteShipUnitsResult = deleteShipUnitsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeleteShipUnitsResponse)) return false;
        DeleteShipUnitsResponse other = (DeleteShipUnitsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.deleteShipUnitsResult==null && other.getDeleteShipUnitsResult()==null) || 
             (this.deleteShipUnitsResult!=null &&
              this.deleteShipUnitsResult.equals(other.getDeleteShipUnitsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDeleteShipUnitsResult() != null) {
            _hashCode += getDeleteShipUnitsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeleteShipUnitsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteShipUnitsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deleteShipUnitsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "DeleteShipUnitsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnitsResponse>DeleteShipUnitsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
