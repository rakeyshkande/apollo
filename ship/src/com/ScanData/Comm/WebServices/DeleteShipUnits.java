/**
 * DeleteShipUnits.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class DeleteShipUnits  implements java.io.Serializable {
    private int sessionID;

    private com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList shipUnitList;

    public DeleteShipUnits() {
    }

    public DeleteShipUnits(
           int sessionID,
           com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList shipUnitList) {
           this.sessionID = sessionID;
           this.shipUnitList = shipUnitList;
    }


    /**
     * Gets the sessionID value for this DeleteShipUnits.
     * 
     * @return sessionID
     */
    public int getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this DeleteShipUnits.
     * 
     * @param sessionID
     */
    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the shipUnitList value for this DeleteShipUnits.
     * 
     * @return shipUnitList
     */
    public com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList getShipUnitList() {
        return shipUnitList;
    }


    /**
     * Sets the shipUnitList value for this DeleteShipUnits.
     * 
     * @param shipUnitList
     */
    public void setShipUnitList(com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList shipUnitList) {
        this.shipUnitList = shipUnitList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeleteShipUnits)) return false;
        DeleteShipUnits other = (DeleteShipUnits) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sessionID == other.getSessionID() &&
            ((this.shipUnitList==null && other.getShipUnitList()==null) || 
             (this.shipUnitList!=null &&
              this.shipUnitList.equals(other.getShipUnitList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSessionID();
        if (getShipUnitList() != null) {
            _hashCode += getShipUnitList().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeleteShipUnits.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">DeleteShipUnits"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipUnitList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ShipUnitList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>DeleteShipUnits>ShipUnitList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
