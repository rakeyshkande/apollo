/**
 * WTMServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public interface WTMServiceSoap_PortType extends java.rmi.Remote {
    public java.lang.String helloWorld() throws java.rmi.RemoteException;
    public java.lang.String goodbyeWorld() throws java.rmi.RemoteException;
    public int startSession() throws java.rmi.RemoteException;
    public void endSession(int sessionID) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult createShipUnits(int sessionID, com.ScanData.Comm.WebServices.CreateShipUnitsCreateShipUnitsParams createShipUnitsParams) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.CreateShipmentResponseCreateShipmentResult createShipment(int sessionID, com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams createShipmentParams) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult createBillingAccount(int sessionID, com.ScanData.Comm.WebServices.CreateBillingAccountBillingAccount billingAccount) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.DeleteOrdersResponseDeleteOrdersResult deleteOrders(int sessionID, com.ScanData.Comm.WebServices.DeleteOrdersOrderList orderList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult cancelShipUnits(int sessionID, com.ScanData.Comm.WebServices.CancelShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.DeleteShipUnitsResponseDeleteShipUnitsResult deleteShipUnits(int sessionID, com.ScanData.Comm.WebServices.DeleteShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.GetShipUnitsLabelsResponseGetShipUnitsLabelsResult getShipUnitsLabels(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsLabelsShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult getShipUnitsInfo(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsInfoShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.GetShipUnitsReportsResponseGetShipUnitsReportsResult getShipUnitsReports(int sessionID, com.ScanData.Comm.WebServices.GetShipUnitsReportsShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.GetShipmentReportsResponseGetShipmentReportsResult getShipmentReports(int sessionID, com.ScanData.Comm.WebServices.GetShipmentReportsShipmentList shipmentList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult loadShipUnits(int sessionID, com.ScanData.Comm.WebServices.LoadShipUnitsLoadShipUnitsParam loadShipUnitsParam) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.UnloadShipUnitsResponseUnloadShipUnitsResult unloadShipUnits(int sessionID, com.ScanData.Comm.WebServices.UnloadShipUnitsShipUnitList shipUnitList) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.ManifestTrailerResponseManifestTrailerResult manifestTrailer(int sessionID, java.lang.String trailerNumber, java.lang.String distributionCenter) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult manifestCarrier(int sessionID, java.lang.String carrierID, java.lang.String distributionCenter) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult updateShipUnits(int sessionID, com.ScanData.Comm.WebServices.UpdateShipUnitsUpdateShipUnitsParams updateShipUnitsParams) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult rateShipUnits(int sessionID, com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams rateShipUnitsParams) throws java.rmi.RemoteException;
    public com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult manifestZoneJumpTrailer(int sessionID, java.lang.String trailerNumber) throws java.rmi.RemoteException;
}
