/**
 * CreateShipment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CreateShipment  implements java.io.Serializable {
    private int sessionID;

    private com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams createShipmentParams;

    public CreateShipment() {
    }

    public CreateShipment(
           int sessionID,
           com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams createShipmentParams) {
           this.sessionID = sessionID;
           this.createShipmentParams = createShipmentParams;
    }


    /**
     * Gets the sessionID value for this CreateShipment.
     * 
     * @return sessionID
     */
    public int getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this CreateShipment.
     * 
     * @param sessionID
     */
    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the createShipmentParams value for this CreateShipment.
     * 
     * @return createShipmentParams
     */
    public com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams getCreateShipmentParams() {
        return createShipmentParams;
    }


    /**
     * Sets the createShipmentParams value for this CreateShipment.
     * 
     * @param createShipmentParams
     */
    public void setCreateShipmentParams(com.ScanData.Comm.WebServices.CreateShipmentCreateShipmentParams createShipmentParams) {
        this.createShipmentParams = createShipmentParams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateShipment)) return false;
        CreateShipment other = (CreateShipment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sessionID == other.getSessionID() &&
            ((this.createShipmentParams==null && other.getCreateShipmentParams()==null) || 
             (this.createShipmentParams!=null &&
              this.createShipmentParams.equals(other.getCreateShipmentParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSessionID();
        if (getCreateShipmentParams() != null) {
            _hashCode += getCreateShipmentParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateShipment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateShipment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "SessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createShipmentParams");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateShipmentParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateShipment>CreateShipmentParams"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
