/**
 * ManifestCarrierResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class ManifestCarrierResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult manifestCarrierResult;

    public ManifestCarrierResponse() {
    }

    public ManifestCarrierResponse(
           com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult manifestCarrierResult) {
           this.manifestCarrierResult = manifestCarrierResult;
    }


    /**
     * Gets the manifestCarrierResult value for this ManifestCarrierResponse.
     * 
     * @return manifestCarrierResult
     */
    public com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult getManifestCarrierResult() {
        return manifestCarrierResult;
    }


    /**
     * Sets the manifestCarrierResult value for this ManifestCarrierResponse.
     * 
     * @param manifestCarrierResult
     */
    public void setManifestCarrierResult(com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult manifestCarrierResult) {
        this.manifestCarrierResult = manifestCarrierResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManifestCarrierResponse)) return false;
        ManifestCarrierResponse other = (ManifestCarrierResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.manifestCarrierResult==null && other.getManifestCarrierResult()==null) || 
             (this.manifestCarrierResult!=null &&
              this.manifestCarrierResult.equals(other.getManifestCarrierResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getManifestCarrierResult() != null) {
            _hashCode += getManifestCarrierResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManifestCarrierResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">ManifestCarrierResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manifestCarrierResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "ManifestCarrierResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>ManifestCarrierResponse>ManifestCarrierResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
