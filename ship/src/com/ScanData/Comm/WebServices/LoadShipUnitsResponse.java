/**
 * LoadShipUnitsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class LoadShipUnitsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult loadShipUnitsResult;

    public LoadShipUnitsResponse() {
    }

    public LoadShipUnitsResponse(
           com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult loadShipUnitsResult) {
           this.loadShipUnitsResult = loadShipUnitsResult;
    }


    /**
     * Gets the loadShipUnitsResult value for this LoadShipUnitsResponse.
     * 
     * @return loadShipUnitsResult
     */
    public com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult getLoadShipUnitsResult() {
        return loadShipUnitsResult;
    }


    /**
     * Sets the loadShipUnitsResult value for this LoadShipUnitsResponse.
     * 
     * @param loadShipUnitsResult
     */
    public void setLoadShipUnitsResult(com.ScanData.Comm.WebServices.LoadShipUnitsResponseLoadShipUnitsResult loadShipUnitsResult) {
        this.loadShipUnitsResult = loadShipUnitsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LoadShipUnitsResponse)) return false;
        LoadShipUnitsResponse other = (LoadShipUnitsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loadShipUnitsResult==null && other.getLoadShipUnitsResult()==null) || 
             (this.loadShipUnitsResult!=null &&
              this.loadShipUnitsResult.equals(other.getLoadShipUnitsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoadShipUnitsResult() != null) {
            _hashCode += getLoadShipUnitsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LoadShipUnitsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">LoadShipUnitsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loadShipUnitsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "LoadShipUnitsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>LoadShipUnitsResponse>LoadShipUnitsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
