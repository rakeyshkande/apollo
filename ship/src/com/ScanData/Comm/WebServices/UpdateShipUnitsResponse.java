/**
 * UpdateShipUnitsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class UpdateShipUnitsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult updateShipUnitsResult;

    public UpdateShipUnitsResponse() {
    }

    public UpdateShipUnitsResponse(
           com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult updateShipUnitsResult) {
           this.updateShipUnitsResult = updateShipUnitsResult;
    }


    /**
     * Gets the updateShipUnitsResult value for this UpdateShipUnitsResponse.
     * 
     * @return updateShipUnitsResult
     */
    public com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult getUpdateShipUnitsResult() {
        return updateShipUnitsResult;
    }


    /**
     * Sets the updateShipUnitsResult value for this UpdateShipUnitsResponse.
     * 
     * @param updateShipUnitsResult
     */
    public void setUpdateShipUnitsResult(com.ScanData.Comm.WebServices.UpdateShipUnitsResponseUpdateShipUnitsResult updateShipUnitsResult) {
        this.updateShipUnitsResult = updateShipUnitsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateShipUnitsResponse)) return false;
        UpdateShipUnitsResponse other = (UpdateShipUnitsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.updateShipUnitsResult==null && other.getUpdateShipUnitsResult()==null) || 
             (this.updateShipUnitsResult!=null &&
              this.updateShipUnitsResult.equals(other.getUpdateShipUnitsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUpdateShipUnitsResult() != null) {
            _hashCode += getUpdateShipUnitsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateShipUnitsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">UpdateShipUnitsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateShipUnitsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "UpdateShipUnitsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>UpdateShipUnitsResponse>UpdateShipUnitsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
