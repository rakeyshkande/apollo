/**
 * CreateBillingAccountResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class CreateBillingAccountResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult createBillingAccountResult;

    public CreateBillingAccountResponse() {
    }

    public CreateBillingAccountResponse(
           com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult createBillingAccountResult) {
           this.createBillingAccountResult = createBillingAccountResult;
    }


    /**
     * Gets the createBillingAccountResult value for this CreateBillingAccountResponse.
     * 
     * @return createBillingAccountResult
     */
    public com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult getCreateBillingAccountResult() {
        return createBillingAccountResult;
    }


    /**
     * Sets the createBillingAccountResult value for this CreateBillingAccountResponse.
     * 
     * @param createBillingAccountResult
     */
    public void setCreateBillingAccountResult(com.ScanData.Comm.WebServices.CreateBillingAccountResponseCreateBillingAccountResult createBillingAccountResult) {
        this.createBillingAccountResult = createBillingAccountResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateBillingAccountResponse)) return false;
        CreateBillingAccountResponse other = (CreateBillingAccountResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.createBillingAccountResult==null && other.getCreateBillingAccountResult()==null) || 
             (this.createBillingAccountResult!=null &&
              this.createBillingAccountResult.equals(other.getCreateBillingAccountResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreateBillingAccountResult() != null) {
            _hashCode += getCreateBillingAccountResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateBillingAccountResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">CreateBillingAccountResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createBillingAccountResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "CreateBillingAccountResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>CreateBillingAccountResponse>CreateBillingAccountResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
