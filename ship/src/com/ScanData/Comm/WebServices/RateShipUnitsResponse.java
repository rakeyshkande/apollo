/**
 * RateShipUnitsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ScanData.Comm.WebServices;

public class RateShipUnitsResponse  implements java.io.Serializable {
    private com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult rateShipUnitsResult;

    public RateShipUnitsResponse() {
    }

    public RateShipUnitsResponse(
           com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult rateShipUnitsResult) {
           this.rateShipUnitsResult = rateShipUnitsResult;
    }


    /**
     * Gets the rateShipUnitsResult value for this RateShipUnitsResponse.
     * 
     * @return rateShipUnitsResult
     */
    public com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult getRateShipUnitsResult() {
        return rateShipUnitsResult;
    }


    /**
     * Sets the rateShipUnitsResult value for this RateShipUnitsResponse.
     * 
     * @param rateShipUnitsResult
     */
    public void setRateShipUnitsResult(com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult rateShipUnitsResult) {
        this.rateShipUnitsResult = rateShipUnitsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateShipUnitsResponse)) return false;
        RateShipUnitsResponse other = (RateShipUnitsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rateShipUnitsResult==null && other.getRateShipUnitsResult()==null) || 
             (this.rateShipUnitsResult!=null &&
              this.rateShipUnitsResult.equals(other.getRateShipUnitsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRateShipUnitsResult() != null) {
            _hashCode += getRateShipUnitsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateShipUnitsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">RateShipUnitsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateShipUnitsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ScanData.com/WTM/", "RateShipUnitsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ScanData.com/WTM/", ">>RateShipUnitsResponse>RateShipUnitsResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
