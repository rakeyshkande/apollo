package com.ftd.ship.test;
// import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.osp.utilities.plugins.Logger;
// import com.ftd.ship.bo.ShipProcessingBO;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestMultipleOrders extends TestCase 
{
  public TestMultipleOrders(String name)
  {
    super(name);
  }
  
  private Connection conn = null;  
  private static String[] orders = {"10402","11008","11211","11406","11807","12447","12448","12613","12614","12619"};
  private static Logger logger  = new Logger("com.ftd.ship.test.TestMultipleOrders");
  private HashMap carrierMap = new HashMap();
  
  /*
   * vendor nbr = 90-8590AA
   * vendor id  = 00064
   */
  
  public void setUp() throws Exception 
  {
    conn = TestUtil.getConnection();
  }
  
//  public void testMultipleOrders()
//  {
//      ShipProcessingBO shipProcessingBo = new ShipProcessingBO(conn);
//      try 
//      {
//        setUp();
//        updateVendorCarrierRef();
//        
//        for(int i=0;i<orders.length;i++) {
//          shipProcessingBo.processShippingLogistics(orders[i]);
//          
//          //read order-detail-id based on venus id
//          
//          //read carrier status from order-id.
//          
//          //read carrier, add it to list.
//          //carrierList.add();
//        }
//      }catch(Exception e)
//      {
//        logger.error(e);
//        fail("There should not an exception thrown.");
//        
//      }
//      
//      ArrayList keys = new ArrayList(carrierMap.keySet());
//      for(int i=0;i<keys.size();i++) 
//      {
//        String value = ""+carrierMap.get(keys.get(i));
//        logger.debug("Order number - "+ value +" assigned to carrier - "+carrierMap.get(value));
//      }
//      
//  }

//  private void updateVendorCarrierRef() throws Exception
//  {
//    MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
//    maintDAO.updateVendorCarrierRef("00016", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00016", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00016", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00071", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00071", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00071", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00035", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00035", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00035", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00061", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00061", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00061", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00067", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00067", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00067", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00032", "DHL", 50);
//    maintDAO.updateVendorCarrierRef("00032", "FEDEX", 50);
//    maintDAO.updateVendorCarrierRef("00032", "UPS", 0);
//  
//  }


 public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(TestMultipleOrders.class);        
    } 
    catch (Exception e) 
    {
      //System.out.println(e.toString());
      //e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
}
