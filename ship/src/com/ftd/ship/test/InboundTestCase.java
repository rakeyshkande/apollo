package com.ftd.ship.test;

import com.ftd.ship.bo.SDSInboundMsgProcessingBO;

import com.ftd.ship.vo.OrderDetailVO;

import java.sql.Connection;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class InboundTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    
    public InboundTestCase(String testCase) {
        super(testCase);
    }
    
    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext(SPRING_CONFIG_FILE);
        this.conn = resourceProvider.getDatabaseConnection();
    }
    
    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();    
        }
    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(InboundTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }

    public static void main(String[] args) {
        try {
            InboundTestCase inboundTestCase = new InboundTestCase("testInboundMessage");
            inboundTestCase.runBare();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test the processing of an inbound SDS status message by calling 
     * SDSInboundMsgProcessingBO.processMessage
     */
    public void testInboundMessage() {
        try {
            SDSInboundMsgProcessingBO bo = (SDSInboundMsgProcessingBO)context.getBean("sdsInProcessingBO");
            bo.processMessage(conn,"134");
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    public void testSendEmail() {
        try {
            SDSInboundMsgProcessingBO bo = (SDSInboundMsgProcessingBO)context.getBean("sdsInProcessingBO");
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(conn,"1535255");
            bo.sendDropShipEmail(conn,orderDetailVO);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
