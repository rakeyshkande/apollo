package com.ftd.ship.test;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import junit.framework.TestCase;

public class TestUtil extends TestCase 
{
  public TestUtil(String name)
  {
    super(name);
  }
  
  public static Connection getConnection() throws Exception
  {
    Class.forName("oracle.jdbc.driver.OracleDriver");
    Connection conn = DriverManager.getConnection ("jdbc:oracle:thin:@stheno-dev.ftdi.com:1522:qa6", "osp", "osp");
    
    return conn;
  }
  
  
  
}