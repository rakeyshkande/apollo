package com.ftd.ship.test;

import com.ftd.ship.bo.ReviewRejectBO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class RejectReviewTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;


    public RejectReviewTestCase(String sTestName) {
        super(sTestName);
    }

    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\_RLSE_CURRENT_DEV\\FTD\\ship\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }

    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();
        }
    }

    public static TestSuite suite()
    {
        TestSuite suite = null;

        try
        {
            suite = new TestSuite(ShipRequestTestCase.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return suite;
    }

    public void testRejectReview1809() throws Throwable {
        VenusMessageVO vo = shipDAO.getVenusMessage(conn,"96343");
        System.out.println(vo.getVenusOrderNumber());

        ReviewRejectBO bo = (ReviewRejectBO)context.getBean("reviewRejectBO");
        bo.processRequest(conn,vo);
    }

    public static void main(String[] args) {
        RejectReviewTestCase rejectReviewTestCase = new RejectReviewTestCase("testRejectReview1809");

        try {
            rejectReviewTestCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
