package com.ftd.ship.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.plugins.Logger;
import com.providecommerce.api.api.authentication.v1.EmailPasswordUserContext;
import com.providecommerce.api.api.common.v1.ArrayOfValidationError;
import com.providecommerce.api.api.common.v1.FaultDetail;
import com.providecommerce.api.api.common.v1.ValidationError;
import com.providecommerce.api.api.customer.v1.Customer;
import com.providecommerce.api.api.customer.v1.CustomerRecipient;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDelivery;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDeliveryLineItem;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDeliveryLineItemAccessory;
import com.providecommerce.api.api.order.v1.CreateOrderRequest;
import com.providecommerce.api.api.order.v1.Order;
import com.providecommerce.api.api.order.v1.OrderDelivery;
import com.providecommerce.api.api.order.v1.OrderDeliveryDetails;
import com.providecommerce.api.api.order.v1.OrderDeliveryGiftMessage;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItem;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItemAccessory;
import com.providecommerce.api.api.order.v1.OrderService;
import com.providecommerce.api.api.order.v1.OrderServiceCreateOrderFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.order.v1.OrderService_Service;

public class ShipRequestTestServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private static final Logger LOGGER = new Logger("com.ftd.ship.test.ShipRequestTestServlet");

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**Process the HTTP doGet request.
     */
    public void doPost(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        String testString = request.getParameter("testChoice");
//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>ShipRequestTestServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>Test to be performed: "+testString+"</p>");
//        out.println("</body></html>");
//        out.close();
        LOGGER.info("Executing test servlet doget!!!!!!!!!!!!!!");
        
        testCallService();
//        if( StringUtils.equals("testNextAvailableDeliveryDate",testString) ) {
//            
//        }
//        
//        try {
//            ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase(testString);
//            shipRequestTestCase.runBare();
//        } catch (Throwable t) {
//            PrintWriter out = response.getWriter();
//            t.printStackTrace(out);
//        }
    }
    
    
    
    /**Process the HTTP doGet request.
     */
    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        String testString = request.getParameter("testChoice");
        
        
        LOGGER.info("Executing test servlet doget!!!!!!!!!!!!!!");
//        PrintWriter out = response.getWriter();
//        out.println("<html>");
//        out.println("<head><title>ShipRequestTestServlet</title></head>");
//        out.println("<body>");
//        out.println("<p>Test to be performed: "+testString+"</p>");
//        out.println("</body></html>");
//        out.close();

        
        testCallService();
//        if( StringUtils.equals("testNextAvailableDeliveryDate",testString) ) {
//            
//        }
//        
//        try {
//            ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase(testString);
//            shipRequestTestCase.runBare();
//        } catch (Throwable t) {
//            PrintWriter out = response.getWriter();
//            t.printStackTrace(out);
//        }
    }
    
    
    
    private static CreateOrderRequest getDummyrequest(){
		CreateOrderRequest req = new CreateOrderRequest();		
		Order order=new Order();
		
		// 1. Customer
		// 2. Deliveries
		// 3. Payments
		// 4.Promocodes
		// 5.SurchargeDetails
		// 6.OriginalOrderId
		
		//1. Customer
		Customer customer=new Customer();
		customer.setCustomerId("100037813926");//100037813926
		customer.setEmail("50725746_@engmail01.proflowers.com");
//		1.1 Customer Details			
//		CustomerDetails customerDetails=new CustomerDetails();
//		customerDetails.setFirstName("karthi");
//		customerDetails.setLastName("keyan");
//		customerDetails.setAddress1("Test address 1");
//		customerDetails.setAddress2("test2");
//		customerDetails.setCity("Downers Grove");
//		customerDetails.setState("IL");			
//		customerDetails.setPhone1("630-343-2222");			
//		customerDetails.setCountryCode("US");
//		customerDetails.setZip("60606");
//		customer.setDetails(customerDetails);
		
		order.setCustomer(customer);
		
	//	order.setPONumber(arg0);
		//2.Deliveries
		
		ArrayOfOrderDelivery orderDelivariesLst=new ArrayOfOrderDelivery();
		OrderDelivery orderDelivery=new OrderDelivery();
		
		
		//2.1 Delivery Details
		OrderDeliveryDetails dd=new OrderDeliveryDetails();
		orderDelivery.setDetails(dd);
		//odls.add(orderDelivery);
		orderDelivariesLst.getOrderDelivery().add(orderDelivery);
		
		//2.2 OrderDeliveryLineItems
		ArrayOfOrderDeliveryLineItem odLineItemLst=new ArrayOfOrderDeliveryLineItem();			
		OrderDeliveryLineItem odLineItem=new OrderDeliveryLineItem();
		
		ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst=new ArrayOfOrderDeliveryLineItemAccessory();			
		OrderDeliveryLineItemAccessory odliAccessory= new OrderDeliveryLineItemAccessory();
		
		odliAccessory.setAccessoryId("30142682");	
		odliAccessoryLst.getOrderDeliveryLineItemAccessory().add(odliAccessory);
		odLineItem.setProductId("30152988");	
		odLineItem.setAccessories(odliAccessoryLst);
		odLineItemLst.getOrderDeliveryLineItem().add(odLineItem);
		orderDelivery.setLineItems(odLineItemLst);
		orderDelivery.setServiceLevelCode(5);
		orderDelivery.setServiceType("ServiceLevel");
		//orderDelivery.setServiceType("DeliveryDate");
	    order.setDeliveries(orderDelivariesLst);
	    
	    CustomerRecipient custRecipient=new CustomerRecipient();
	    custRecipient.setFirstName("Karthi");
	    custRecipient.setLastName("Keyan");
	    custRecipient.setAddress1("4830 Eastgate Mall");
	    custRecipient.setAddress2("");
	    custRecipient.setCity("San Diego");
	    custRecipient.setState("CA");
	    custRecipient.setCountryCode("US");		    
	    custRecipient.setZip("92121");
	    custRecipient.setPhone("555-555-5555");
	    custRecipient.setLocationType("Residential");
	    
	    OrderDeliveryGiftMessage odGiftMessage=new OrderDeliveryGiftMessage();
	    odGiftMessage.setMessage("test msg");
	    odGiftMessage.setOccasion("Birthday");
	 //   odGiftMessage.setOccasionId("3");
	    orderDelivery.setGiftMessage(odGiftMessage);
	    
	    LOGGER.info("Time"+getXMLGregorianCalendarNow().toString());
	    orderDelivery.setDeliveryDate(getXMLGregorianCalendarNow());
	    orderDelivery.setDeliveryTime("Afternoon");//'Morning' is throwing error
	   //orderDelivery.setServiceType("DeliveryDate");//'DeliveryDate' is not a valid input
	 // orderDelivery.setGiftMessage(odgm);
	    orderDelivery.setRecipient(custRecipient);
	    order.setPONumber("PO1212121");
	    req.setOrder(order);
		return req;
	}
 
    
    private void testCallService() throws MalformedURLException{
    	
    	 URL wsdlURL=new URL("https://lm79apiservice.providecommerce.com/API/Order/v1/SOAP?singleWsdl");
    	 OrderService_Service ss = new OrderService_Service(wsdlURL);
	     OrderService port = ss.getBasicHttpBindingOrderService();  
	
	//logger.info("testing karthi");
	        LOGGER.info("Invoking createOrder...");
	        com.providecommerce.api.api.authentication.v1.EmailPasswordUserContext _createOrder_authenticationContext = new EmailPasswordUserContext();
	       
	        _createOrder_authenticationContext.setApplicationToken("qbica42ygcpthe3lkiy55q2v");
	        _createOrder_authenticationContext.setEmail("50725746_@engmail01.proflowers.com");
	        _createOrder_authenticationContext.setPassword("50725746");
	        
	        com.providecommerce.api.api.order.v1.CreateOrderRequest _createOrder_request = getDummyrequest();
	        
	        LOGGER.info("Invoking createOrder..."+_createOrder_request.getOrder().toString());

	        try {
	            com.providecommerce.api.api.order.v1.CreateOrderResult _createOrder__return = port.createOrder(_createOrder_authenticationContext, _createOrder_request);
	            LOGGER.info("createOrder.result=" + _createOrder__return.getOrderId());
	            

	        } catch (OrderServiceCreateOrderFaultDetailFaultFaultMessage e) { 
	        	FaultDetail fdet=e.getFaultInfo();
	        	
	        	
	        	LOGGER.info("fdet.getFaultType()-->"+fdet.getFaultType());
	        	
	        	ArrayOfValidationError ve= fdet.getValidationErrors();
	        	for(ValidationError verr:ve.getValidationError()){
	        		LOGGER.info("Error Code-->"+verr.getErrorCode());	        		
	        		LOGGER.info("Error msg-->"+verr.getErrorMessage());
	        	}
	        	LOGGER.info("fdet.getFaultType()-->"+fdet.getFaultType());
	        	e.getMessage();	        	
	            LOGGER.info("Expected exception: OrderService_CreateOrder_FaultDetailFault_FaultMessage has occurred.");
	            LOGGER.info(e.toString()+":"+e.getMessage());
	            e.printStackTrace();
	        }
    }
    
    
	 public static XMLGregorianCalendar getXMLGregorianCalendarNow() 
	           
	    {
	        GregorianCalendar gregorianCalendar = new GregorianCalendar();
	        DatatypeFactory datatypeFactory = null;
			try {
				datatypeFactory = DatatypeFactory.newInstance();
			} catch (DatatypeConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        XMLGregorianCalendar now = 
	            datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
	        return now;
	    }
	
}
