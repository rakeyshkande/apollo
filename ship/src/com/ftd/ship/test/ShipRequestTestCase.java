package com.ftd.ship.test;

import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.SDSShipmentProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSCommunications;
import com.ftd.ship.vo.SDSLogisticsVO;
import com.ftd.ship.vo.SDSShipResponseVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.io.StringWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import junit.framework.TestSuite;

import org.w3c.dom.Document;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class ShipRequestTestCase extends SDSTestCase {
    Connection conn;
    ApplicationContext context;
    protected static final SimpleDateFormat SDS_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    protected static final SimpleDateFormat SDS_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public void setUp() throws Exception
    {
        //Start up spring framework
        context = new FileSystemXmlApplicationContext("C:\\_RLSE_CURRENT_DEV\\FTD\\ship\\web\\WEB-INF\\spring-ship-servlet.xml");
        this.conn = resourceProvider.getDatabaseConnection();
    }

    public void tearDown() throws Exception
    {
        if( conn!=null && !conn.isClosed() ) {
            conn.close();
        }
    }

    public static TestSuite suite()
    {
        TestSuite suite = null;

        try
        {
            suite = new TestSuite(ShipRequestTestCase.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return suite;
    }

    public ShipRequestTestCase(String sTestName) {
        super(sTestName);
    }

    /**
     * Test parsing of a response to a SDS ship requests
     */
    public void testParseShipResponse() {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        sb.append("<CreateShipmentXMLResponse xmlns=\"http://ScanData.com/Comm/WebServices/\">");
        sb.append("<SD_PRO_SHIP_UNITS xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_PRO_SHIP_UNITS.xsd\">");
        sb.append("<SHIP_UNITS>");
        sb.append("<CartonNumber>1234567819</CartonNumber>");
        sb.append("<CarrierID>FEDEX</CarrierID>");
        sb.append("<CartonGroupCount>1</CartonGroupCount>");
        sb.append("<CartonGroupID>123456781</CartonGroupID>");
        sb.append("<CartonGroupSequence>1</CartonGroupSequence>");
        sb.append("<Comments>Test ship create</Comments>");
        sb.append("<CountryOfOrigin>US</CountryOfOrigin>");
        sb.append("<CustomerSpecific1>Test ship create</CustomerSpecific1>");
        sb.append("<DateCreated>2006-03-24T09:01:52.867</DateCreated>");
        sb.append("<DistributionCenter>01</DistributionCenter>");
        sb.append("<DownloadAction>INSERT</DownloadAction>");
        sb.append("<DownloadDate>2006-03-24T09:01:53.87</DownloadDate>");
        sb.append("<DownloadStatus>INSERT</DownloadStatus>");
        sb.append("<OrderKey>123456781</OrderKey>");
        sb.append("<OrderNumber>123456781</OrderNumber>");
        sb.append("<ReturnAddress1>10301 N. 92nd Street</ReturnAddress1>");
        sb.append("<ReturnCity>Scottsdale</ReturnCity>");
        sb.append("<ReturnCountry>USA</ReturnCountry>");
        sb.append("<ReturnName>CaremarkAZ</ReturnName>");
        sb.append("<ReturnPhoneNumber>4806613166</ReturnPhoneNumber>");
        sb.append("<ReturnState>AZ</ReturnState>");
        sb.append("<ReturnZipCode>85260</ReturnZipCode>");
        sb.append("<ShipForAddress1>4429 N. 47th Street</ShipForAddress1>");
        sb.append("<ShipForCity>Phoenix</ShipForCity>");
        sb.append("<ShipForCountry>USA</ShipForCountry>");
        sb.append("<ShipForName>Haynam</ShipForName>");
        sb.append("<ShipForPhoneNumber>4806613166</ShipForPhoneNumber>");
        sb.append("<ShipForState>AZ</ShipForState>");
        sb.append("<ShipForZipCode>85018</ShipForZipCode>");
        sb.append("<ShipMethodID>STNDONTPKG</ShipMethodID>");
        sb.append("<TrackingNumber>1234567890123456</TrackingNumber>");
        sb.append("<ShippingOptions>2A</ShippingOptions>");
        sb.append("<Status>INITIAL</Status>");
        sb.append("<OriginID>90-0001AA</OriginID>");
        sb.append("<UPS_SHIP_UNITS>");
        sb.append("<DownloadAction>INSERT</DownloadAction>");
        sb.append("<DownloadDate>2006-03-24T09:01:52.867</DownloadDate>");
        sb.append("<DownloadStatus>INSERT</DownloadStatus>");
        sb.append("<ShippingOptions>A</ShippingOptions>");
        sb.append("<CustomerReference1>123456789</CustomerReference1>");
        sb.append("<CustomerReferenceType1>TN</CustomerReferenceType1>");
        sb.append("</UPS_SHIP_UNITS>");
        sb.append("<Division>01</Division>");
        sb.append("</SHIP_UNITS>");
        sb.append("<STATUS>");
        sb.append("<StatusCode xmlns=\"\">-2627</StatusCode>");
        sb.append("<StatusDescription xmlns=\"\">Violation of PRIMARY KEY constraint 'PK_SHIP_UNITS'. Cannot insert duplicate key in object 'SHIP_UNITS'.</StatusDescription>");
        sb.append("</STATUS>");
        sb.append("</SD_PRO_SHIP_UNITS>");
        sb.append("</CreateShipmentXMLResponse>");

        System.out.println(sb.toString());

        SDSShipmentProcessingBO bo = new SDSShipmentProcessingBO();

        try {
            Document responseDoc = JAXPUtil.parseDocument(sb.toString(),false,false);
            SDSShipResponseVO vo = bo.parseCreateShipUnitsResponse(responseDoc, null);
            System.out.println("Status code: "+vo.getStatusCode());
            System.out.println("Message text: "+vo.getMsgText());
            System.out.println("Carrier id: "+vo.getCarrier());
            System.out.println("Ship method id: "+vo.getShipMethod());
            System.out.println("Origin id: "+vo.getSelectedOrigin());
            System.out.println("Tracking number: "+vo.getTrackingNumber());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests building of a ship requests in the format specified by SDS
     */
    public void testBuildRequest() {
        try {
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"39392");
            System.out.println(vo.getVenusOrderNumber());

            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            SDSLogisticsVO lvo = bo.buildRequest(conn,vo);
            Document doc = lvo.getShipUnit();
            Transformer idTransform = TransformerFactory.newInstance().newTransformer();
            idTransform.setOutputProperty("cdata-section-elements","DynamicData");
            Source input = new DOMSource(doc);
            StringWriter sw = new StringWriter();
            Result output = new StreamResult(sw);
            idTransform.transform(input, output);
            String strXML = sw.toString();
            System.out.println(strXML);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Tests the transmitting and response parsing of a manually built ship request
     */
    public void testSendRequest3() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.append("             <CreateShipmentXML xmlns=\"http://ScanData.com/Comm/WebServices/DataSets/SD_IC_SHIP_UNITS.xsd\">");
            sb.append("                     <SHIP_UNITS>");
            sb.append("                                     <CartonNumber>E0000005</CartonNumber>");
            sb.append("                                     <OrderKey>E0000005</OrderKey>");
            sb.append("                                     <OriginalShipVia>U1</OriginalShipVia>");
            sb.append("                                     <Division>AA</Division>");
            sb.append("                                     <DateBestMethodParameter>2006-08-30</DateBestMethodParameter>");
            sb.append("                                     <EstimatedWeight>4</EstimatedWeight>");
            sb.append("                                     <Status>INITIAL</Status>");
            sb.append("                                     <DatePlannedShipment>2006-08-29T00:00:00.000</DatePlannedShipment>");
            sb.append("                                     <ShipForCompanyName></ShipForCompanyName>");
            sb.append("                                     <ShipForName>Karen Felch</ShipForName>");
            sb.append("                                     <ShipForAddress1>205 S. Princeton Ave</ShipForAddress1>");
            sb.append("                                     <ShipForAddress2>|VENDOR 90-8590AA|</ShipForAddress2>");
            sb.append("                                     <ShipForCity>Arlington Heights</ShipForCity>");
            sb.append("                                     <ShipForState>IL</ShipForState>");
            sb.append("                                     <ShipForZipCode>60005</ShipForZipCode>");
            sb.append("                                     <ShipForCountry>USA</ShipForCountry>");
            sb.append("                                     <ShipForPhoneNumber>8473928058</ShipForPhoneNumber>");
            sb.append("                                     <CountryOfOrigin>US</CountryOfOrigin>");
            sb.append("                                     <ShippingOptions>2</ShippingOptions>");
            sb.append("                                     <Weight>4</Weight>");
            sb.append("                                     <CartonGroupCount>1</CartonGroupCount>");
            sb.append("                                     <CartonGroupSequence>1</CartonGroupSequence>");
            sb.append("                                     <DistributionCenter>915322AA</DistributionCenter>");
            sb.append("                                     <DynamicData>");
            sb.append("                                             <![CDATA[");
            sb.append("                                             <ORIGIN_DISTRIBUTION_CENTERS>");
            sb.append("                                                     <ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                                             <DistributionCenter>911990AA</DistributionCenter>");
            sb.append("                                                             <CARRIERS>");
            sb.append("                                                                     <CarrierID>DHL</CarrierID>");
            sb.append("                                                                     <CarrierID>FEDEX</CarrierID>");
            sb.append("                                                             </CARRIERS>");
            sb.append("                                                             <VendorCost>13.50</VendorCost>");
            sb.append("                                                             <VendorSKU>F104-00050</VendorSKU>");
            sb.append("                                                     </ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                                     <ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                                             <DistributionCenter>913087AA</DistributionCenter>");
            sb.append("                                                             <CARRIERS>");
            sb.append("                                                                     <CarrierID>DHL</CarrierID>");
            sb.append("                                                                     <CarrierID>FEDEX</CarrierID>");
            sb.append("                                                             </CARRIERS>");
            sb.append("                                                             <VendorCost>13.61</VendorCost>");
            sb.append("                                                             <VendorSKU>F104-00061</VendorSKU>");
            sb.append("                                                     </ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                                     <ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                                             <DistributionCenter>913087AC</DistributionCenter>");
            sb.append("                                                             <CARRIERS>");
            sb.append("                                                                     <CarrierID>FEDEX</CarrierID>");
            sb.append("                                                             </CARRIERS>");
            sb.append("                                                             <VendorCost>13.70</VendorCost>");
            sb.append("                                                             <VendorSKU>F104-00070</VendorSKU>");
            sb.append("                                                     </ORIGIN_DISTRIBUTION_CENTER>");
            sb.append("                                             </ORIGIN_DISTRIBUTION_CENTERS>");
            sb.append("                                             <InvoiceData>");
            sb.append("                                                     <order>E0000005</order>");
            sb.append("                                                     <origin name=\"FTD.COM\" phone=\"800-736-3383\"/>");
            sb.append("                                                     <recipient name=\"Karen Felch\" company=\"\" address2=\"205 S. Princeton Ave\" address1=\"|SWITCHED FROM 91-6627AB TO TEST VENDOR 90-8590AA|\" city=\"Arlington Heights\" state=\"IL\" postal=\"60005\"/>");
            sb.append("                                                     <product id=\"\" noun=\"I'LL LOVE YOU FUR ALWAYS BEAR (1725)\"/>");
            sb.append("                                                     <card>test test</card>");
            sb.append("                                             </InvoiceData>");
            sb.append("                                             <FTD_SHIP_UNITS>");
            sb.append("                                                     <FTDOrderNumber>C1000697781</FTDOrderNumber>");
            sb.append("                                                     <FTDProductID>F104</FTDProductID>");
            sb.append("                                                     <FTDProductDescription>I'LL LOVE YOU FUR ALWAYS BEAR (1725)</FTDProductDescription>");
            sb.append("                                                     <LineOfBusiness>AA</LineOfBusiness>");
            sb.append("                                             </FTD_SHIP_UNITS>");
            sb.append("                                             ]]>");
            sb.append("                                     </DynamicData>");
            sb.append("                             </SHIP_UNITS>");
            sb.append("             </CreateShipmentXML>");

            System.out.println(sb.toString());

            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");

            SDSLogisticsVO logisticsVO = new SDSLogisticsVO();
            logisticsVO.setShipUnit(JAXPUtil.parseDocument(sb.toString(),false,false));

            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            System.out.println("Using Axis");
            System.out.println("Start: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            Document response = comm.createShipment(conn,logisticsVO.getShipUnit(), "9999999");
//            String strXML = sb.toString();
//             Document response = comm.createShipment(strXML);
            System.out.println("  End: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            SDSShipResponseVO rVO = bo.parseCreateShipUnitsResponse(response, null);
            System.out.println("Success: "+rVO.isSuccess());
            System.out.println("Status code: "+rVO.getStatusCode());
            System.out.println("Msg Text: "+rVO.getMsgText());

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Tests the building, transmitting, and parsing the response of a ship request by using the SDSShipProcessingBO.
     */
    public void testSendRequest4() {
        try {
            /***************** This works ************************/
            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"26282");
            SDSLogisticsVO logisticsVO = bo.buildRequest(conn,vo);

            SDSCommunications comm = (SDSCommunications)context.getBean("sdsCommunications");
            System.out.println("Start: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            Document response = comm.createShipment(conn,logisticsVO.getShipUnit(), vo.getVenusId());
            System.out.println("  End: "+SDS_DATE_TIME_FORMAT.format(new Date()));
            SDSShipResponseVO rVO = bo.parseCreateShipUnitsResponse(response, null);
            System.out.println("Success: "+rVO.isSuccess());
            System.out.println("Status code: "+rVO.getStatusCode());
            System.out.println("Msg Text: "+rVO.getMsgText());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the building, transmitting, and parsing the response of a ship request by using the SDSShipProcessingBO.
     */
    public void testSendShipRequest() {
        try {
            /***************** This works ************************/
            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"26579");
            bo.processRequest(conn,vo);


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void testBuildXML() {
        try {
            Document doc = JAXPUtil.createDocument();
            Element nvpair = doc.createElement("NVPAIR");
            Element eName = doc.createElement("NAME");
            Node nameData = doc.createCDATASection("the name");
            eName.appendChild(nameData);
            nvpair.appendChild(eName);
            Element eValue = doc.createElement("VALUE");
            Node valueData = doc.createCDATASection("the value");
            eValue.appendChild(valueData);
            nvpair.appendChild(eValue);

            doc.appendChild(nvpair);

            DOMUtil.print(doc, System.out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests the building of a SHIP UNITS xml document by calling SDSShipmentProcessingBO.buildRequest
     */
    public void testBuildXML2() {
        try {
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"25237");
            System.out.println(vo.getVenusOrderNumber());

            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            SDSLogisticsVO lvo = bo.buildRequest(conn,vo);
            Document doc = lvo.getShipUnit();
            DOMUtil.print(doc, System.out);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    /**
     * Tests the building and sending of a shipment request to reprocess by making
     * a direct call to SDSShipProcessingBO.processRequest
     */
    public void testBuildReprocessXML2() {
        try {
            VenusMessageVO vo = shipDAO.getVenusMessage(conn,"29273");
            System.out.println(vo.getVenusOrderNumber());

            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            bo.processRequest(conn,vo);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void testComputeDelayMilliseconds() {
        try {
            SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
            long ms = bo.computeMillisecondsToDelay(SDS_DATE_FORMAT.parse("2006-12-21"),10,0);
            System.out.println( (ms/1000) - Integer.MAX_VALUE);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

//    public static void enqueueToShipRequest() {
//        try {
//            Context context = new InitialContext();
//            CommonUtils.sendJMSMessage(context,"26339",JMSPipeline.SHIPPROCESSSHIP);
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }

     /**
      * Tests building of a ship requests in the format specified by SDS
      * Defect 1809/Target
      */
     public void testBuildRequestDefect1809() {
         try {
             VenusMessageVO vo = shipDAO.getVenusMessage(conn,"96152");
             System.out.println(vo.getVenusOrderNumber());

             SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
             SDSLogisticsVO lvo = bo.buildRequest(conn,vo);
             Document doc = lvo.getShipUnit();
             Transformer idTransform = TransformerFactory.newInstance().newTransformer();
             idTransform.setOutputProperty("cdata-section-elements","DynamicData");
             Source input = new DOMSource(doc);
             StringWriter sw = new StringWriter();
             Result output = new StreamResult(sw);
             idTransform.transform(input, output);
             String strXML = sw.toString();
             System.out.println(strXML);
         } catch (Throwable t) {
             t.printStackTrace();
         }
     }

    /**
     * Test determining a ship date based on a given product, delivery date,
     * and ship method.
     * Defect 1809/Target
     */
    public void testNextShipDateDefect1809() {
        try {
             VenusMessageVO vo = shipDAO.getVenusMessage(conn,"96152");
             System.out.println(vo.getVenusOrderNumber());

             SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
             SDSLogisticsVO logisticsVO = bo.getOrderData(conn,vo);
             logisticsVO.setMessageVo(vo);
             Date shipDate = bo.computeShipDate(conn,logisticsVO,SDS_DATE_FORMAT.parse("2007-06-11"));
             System.out.println(SDS_DATE_FORMAT.format(shipDate));
         } catch (Throwable t) {
             t.printStackTrace();
         }
     }

    /**
     * Determine what the next available delivery date is for a specific order beyound the
     * current delivery date.
     * Defect 1809/Target
     */
    public void testNextAvailableDeliveryDate1809() {
        try {
             VenusMessageVO vo = shipDAO.getVenusMessage(conn,"96183");
             //vo.setDeliveryDate(SDS_DATE_FORMAT.parse("2007-06-12"));
             System.out.println(vo.getVenusOrderNumber());

             SDSShipmentProcessingBO bo = (SDSShipmentProcessingBO)context.getBean("sdsShipProcessingBO");
             SDSLogisticsVO logisticsVO = bo.getOrderData(conn,vo);
             logisticsVO.setMessageVo(vo);
             Date shipDate = bo.getNextAvailableDeliveryDate(conn,logisticsVO);
             if( shipDate==null ) {
                 System.out.println("Date not found.");
             } else {
                System.out.println(SDS_DATE_FORMAT.format(shipDate));
             }
         } catch (Throwable t) {
             t.printStackTrace();
         }
     }

    public static void main(String[] args) {
        try {
        //ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testNextAvailableDeliveryDate1809");
        //ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testBuildRequestDefect1809");
        //ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testExceededSDSPartnerRejects1809");
        //ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testExceededVendorPartnerRejects1809");
//        ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testExceededCarrierPartnerRejects1809");
        ShipRequestTestCase shipRequestTestCase = new ShipRequestTestCase("testBuildRequest");
        shipRequestTestCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
