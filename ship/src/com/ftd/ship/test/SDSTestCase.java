package com.ftd.ship.test;

import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.common.resources.TestResourceProvider;
import com.ftd.ship.dao.OrderDAO;
import com.ftd.ship.dao.QueueDAO;
import com.ftd.ship.dao.ShipDAO;

import junit.framework.TestCase;

public class SDSTestCase extends TestCase {
    protected ShipDAO shipDAO = new ShipDAO();
    protected OrderDAO orderDAO = new OrderDAO();
    protected QueueDAO queueDAO = new QueueDAO();
    protected ResourceProviderBase resourceProvider = new TestResourceProvider();
    protected static final String SPRING_CONFIG_FILE = "C:\\_RLSE_CURRENT_DEV\\FTD\\ship\\web\\WEB-INF\\spring-ship-servlet.xml";

    public SDSTestCase(String sTestName) {
        super(sTestName);
    }
}
