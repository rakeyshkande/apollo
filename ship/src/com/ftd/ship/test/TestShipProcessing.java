package com.ftd.ship.test;

// import com.ftd.ship.bo.ShipProcessingBO;
import java.sql.Connection;
import java.sql.SQLException;
import junit.framework.TestCase;

// import com.ftd.op.maintenance.dao.MaintenanceDAO;
import com.ftd.osp.utilities.plugins.Logger;
import junit.framework.TestSuite;

public class TestShipProcessing extends TestCase 
{
  private static Logger logger  = new Logger("com.ftd.ship.test.TestShipProcessing");
  
  public TestShipProcessing(String name)
  {
    super(name);
  }
   
  private static int testVenusInValidId = 10401;
  private Connection conn = null;

  public void setUp() throws Exception 
  {
    conn = TestUtil.getConnection();
  }
  
//  public void testShipProcessingInValidCarrier() 
//  {
//    
//    try {
//      setUp();
//      updateVendorCarrierRef();
//      
//    }catch(Exception e) 
//    {
//      logger.error(e);
//      fail(e.getMessage()+" thrown. There should be no error thrown in this process.");
//    }
//    
//    try 
//    {
//      ShipProcessingBO shipProcessingBo = new ShipProcessingBO(conn);
//      shipProcessingBo.processShippingLogistics("10401");
//      fail("Exception must be thrown.. ");
//    }catch(Exception e) 
//    {
//      logger.error(e);      
//    }
//  }
  
//  private void updateVendorCarrierRef() throws Exception
//  {
//    MaintenanceDAO maintDAO = new MaintenanceDAO(conn);
//    maintDAO.updateVendorCarrierRef("00064", "DHL", 100);
//    maintDAO.updateVendorCarrierRef("00064", "FEDEX", 0);
//    maintDAO.updateVendorCarrierRef("00064", "UPS", 0);
//    
//    maintDAO.updateVendorCarrierRef("00002", "DHL", 100);
//    maintDAO.updateVendorCarrierRef("00002", "FEDEX", 0);
//    maintDAO.updateVendorCarrierRef("00002", "UPS", 0);
//  
//  }
  
  public static TestSuite suite()
  { 
    TestSuite suite = null;
    
    try 
    {
      suite = new TestSuite(TestShipProcessing.class);        
    } 
    catch (Exception e) 
    {
      //System.out.println(e.toString());
      //e.printStackTrace();
    }

    return suite;
  }

  public static void main(String args[])
  {
    junit.textui.TestRunner.run( suite() );    
  }
  
}