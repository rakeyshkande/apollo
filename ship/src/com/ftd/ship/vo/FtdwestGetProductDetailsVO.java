package com.ftd.ship.vo;

public class FtdwestGetProductDetailsVO {
	
	String pquadProductId;
    boolean isActive;
    
	public String getPquadProductId() {
		return pquadProductId;
	}
	public void setPquadProductId(String pquadProductId) {
		this.pquadProductId = pquadProductId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
 	
}
