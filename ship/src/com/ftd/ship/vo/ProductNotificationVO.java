package com.ftd.ship.vo;

public class ProductNotificationVO {
    private String productId;
    private String emailAddress;

    public ProductNotificationVO()
    {
    }

    public String getProductId()
    {
        return productId;
    }

    public void setProductId(String productId)
    {
        this.productId = productId;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }
}
