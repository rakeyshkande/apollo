package com.ftd.ship.vo;

/**
 * @author kkeyan
 *
 */
public class FtdwestPersonalizationVO {
	private String accessoryId;
	private String displayNames;
	private String templateOrder;
	private boolean personalizationCaseFlag;
	
	
	public String getAccessoryId() {
		return accessoryId;
	}
	public void setAccessoryId(String accessoryId) {
		this.accessoryId = accessoryId;
	}
	public String getDisplayNames() {
		return displayNames;
	}
	public void setDisplayNames(String displayNames) {
		this.displayNames = displayNames;
	}

	public String getTemplateOrder() {
		return templateOrder;
	}
	public void setTemplateOrder(String templateOrder) {
		this.templateOrder = templateOrder;
	}
	
	public boolean isPersonalizationCaseFlag() {
    	return personalizationCaseFlag;
    }

    public void setPersonalizationCaseFlag(boolean personalizationCaseFlag) {
    	this.personalizationCaseFlag = personalizationCaseFlag;
    }
    
	@Override
	public String toString() {
		return "FtdwestPersonalizationVO [accessoryId=" + accessoryId
				+ ", displayNames=" + displayNames + ", templateOrder="
				+ templateOrder + ", personalizationCaseFlag="
				+ personalizationCaseFlag + "]";
	}
	
	
	
	
}
