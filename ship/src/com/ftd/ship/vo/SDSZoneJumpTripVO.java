package com.ftd.ship.vo;

import java.math.BigDecimal;

import java.util.Date;

public class SDSZoneJumpTripVO {
    String tripId;
    String carrierId;
    String shipMethod;
    Date shipDate;
    Date deliveryDate;
    Date zoneJumpDate;
    String trailerNumber;
    String shipPointId;
    String origin; // = distribution center = vendor code
    BigDecimal totalCharge;
    boolean sentToSDS;
    String billingAccount;
    ShipmentOptionVO shipmentOption;
    
    public SDSZoneJumpTripVO() {
    }


    public void setCarrierId(String carrier) {
        this.carrierId = carrier;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setShipMethod(String shipMethod) {
        this.shipMethod = shipMethod;
    }

    public String getShipMethod() {
        return shipMethod;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setZoneJumpDate(Date zoneJumpDate) {
        this.zoneJumpDate = zoneJumpDate;
    }

    public Date getZoneJumpDate() {
        return zoneJumpDate;
    }

    public void setTrailerNumber(String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }

    public String getTrailerNumber() {
        return trailerNumber;
    }

    public void setShipPointId(String shipPointId) {
        this.shipPointId = shipPointId;
    }

    public String getShipPointId() {
        return shipPointId;
    }

    public void setTotalCharge(BigDecimal totalCharge) {
        this.totalCharge = totalCharge;
    }

    public BigDecimal getTotalCharge() {
        return totalCharge;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setSentToSDS(boolean sentToSDS) {
        this.sentToSDS = sentToSDS;
    }

    public boolean isSentToSDS() {
        return sentToSDS;
    }
    
    public void setBillingAccount(String billingAccount) {
        this.billingAccount = billingAccount;
    }

    public String getBillingAccount() {
        return billingAccount;
    }

    public void setShipmentOption(ShipmentOptionVO shipmentOption) {
        this.shipmentOption = shipmentOption;
    }

    public ShipmentOptionVO getShipmentOption() {
        return shipmentOption;
    }
}
