package com.ftd.ship.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class CommentsVO extends BaseVO
{
  private String    commentId;
  private String    customerId;
  private String		orderGuid;
  private String    orderDetailId;
  private	String		commentOrigin;
  private	String		reason;
  private String    dnisId;
  private	String		comment;
  private	String		commentType;
  private Date 	createdOn;
  private String 		createdBy;
  private Date 	updatedOn;
  private String 		updatedBy;

  public CommentsVO()
  {
  }

  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }


  public String getCustomerId()
  {
    return customerId;
  }


  public void setCreatedOn(Date createdOn)
  {
    if(valueChanged(this.createdOn, createdOn))
    {
      setChanged(true);
    }
    this.createdOn = createdOn;
  }


  public Date getCreatedOn()
  {
    return createdOn;
  }


  public void setCreatedBy(String createdBy)
  {
    if(valueChanged(this.createdBy, createdBy))
    {
      setChanged(true);
    }

    this.createdBy = trim(createdBy);
  }


  public String getCreatedBy()
  {
    return createdBy;
  }


  public void setUpdatedOn(Date updatedOn)
  {
    if(valueChanged(this.updatedOn, updatedOn ))
    {
      setChanged(true);
    }
    this.updatedOn = updatedOn;
  }


  public Date getUpdatedOn()
  {
    return updatedOn;
  }


  public void setUpdatedBy(String updatedBy)
  {
    if(valueChanged(this.updatedBy, updatedBy ))
    {
      setChanged(true);
    }
    this.updatedBy = trim(updatedBy);
  }


  public String getUpdatedBy()
  {
    return updatedBy;
  }


  public void setCommentId(String commentId)
  {
    if(valueChanged(this.commentId, commentId))
    {
      setChanged(true);
    }
    this.commentId = commentId;
  }


  public String getCommentId()
  {
    return commentId;
  }


  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }


  public void setOrderDetailId(String orderDetailId)
  {
    this.orderDetailId = orderDetailId;
  }


  public String getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCommentOrigin(String commentOrigin)
  {
    if(valueChanged(this.commentOrigin, commentOrigin))
    {
      setChanged(true);
    }
    this.commentOrigin = trim(commentOrigin);
  }


  public String getCommentOrigin()
  {
    return commentOrigin;
  }


  public void setReason(String reason)
  {
    if(valueChanged(this.reason, reason))
    {
      setChanged(true);
    }
    this.reason = trim(reason);
  }


  public String getReason()
  {
    return reason;
  }


  public void setDnisId(String dnisId)
  {
    this.dnisId = dnisId;
  }


  public String getDnisId()
  {
    return dnisId;
  }


  public void setComment(String comment)
  {
    if(valueChanged(this.comment, comment))
    {
      setChanged(true);
    }
    this.comment = trim(comment);
  }


  public String getComment()
  {
    return comment;
  }


  public void setCommentType(String commentType)
  {
    if(valueChanged(this.commentType, commentType))
    {
      setChanged(true);
    }
    this.commentType = trim(commentType);
  }


  public String getCommentType()
  {
    return commentType;
  }

 private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}
