package com.ftd.ship.vo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kkeyan
 *
 */
public class FtdwestResponseVO {
	private String ftdwestOrderId;
	private String createOrderRequestMessage;
	private String createOrderResponseMessage;
	private String createOrderStatus;
	private HashMap<String, String> errorMap;	
	
	
	public String getFtdwestOrderId() {
		return ftdwestOrderId;
	}
	public void setFtdwestOrderId(String ftdwestOrderId) {
		this.ftdwestOrderId = ftdwestOrderId;
	}
	public String getCreateOrderResponseMessage() {
		return createOrderResponseMessage;
	}
	public void setCreateOrderResponseMessage(String createOrderResponseMessage) {
		this.createOrderResponseMessage = createOrderResponseMessage;
	}
	public String getCreateOrderStatus() {
		return createOrderStatus;
	}
	public void setCreateOrderStatus(String createOrderStatus) {
		this.createOrderStatus = createOrderStatus;
	}
	public HashMap<String, String> getErrorMap() {
		return errorMap;
	}
	public void setErrorMap(HashMap<String, String> errorMap) {
		this.errorMap = errorMap;
	}
	public String getCreateOrderRequestMessage() {
		return createOrderRequestMessage;
	}
	public void setCreateOrderRequestMessage(String createOrderRequestMessage) {
		this.createOrderRequestMessage = createOrderRequestMessage;
	}
	
	
	

}
