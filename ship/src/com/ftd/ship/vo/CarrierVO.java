package com.ftd.ship.vo;

public class CarrierVO 
{
    private String carrierName;
    private Integer ratio;
    private String groundFlag;
    private String referenceNumber;
    private String carrierId;
    private String billingAccountNumber;
   
    public CarrierVO()
    {
    }
    
    public CarrierVO(String carrierName, Integer ratio)
    {
        this.carrierName = carrierName;
        this.ratio = ratio;
    }
    
    public String getCarrierName()
    {
        return carrierName;
    }

    public void setCarrierName(String carrierName)
    {
        this.carrierName = carrierName;
    }

    public Integer getRatio()
    {
        return ratio;
    }

    public void setRatio(Integer ratio)
    {
        this.ratio = ratio;
    }

    public String getGroundFlag()
    {
        return groundFlag;
    }

    public void setGroundFlag(String groundFlag)
    {
        this.groundFlag = groundFlag;
    }

    public String getReferenceNumber()
    {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber)
    {
        this.referenceNumber = referenceNumber;
    }

    public String getCarrierId()
    {
        return carrierId;
    }

    public void setCarrierId(String carrierId)
    {
        this.carrierId = carrierId;
    }

    public void setBillingAccountNumber(String billingAccountNumber) {
        this.billingAccountNumber = billingAccountNumber;
    }

    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }
}
