package com.ftd.ship.vo;

public class WestProductUpdateVO {
	String novatorId;
	String pquadId;
	String processedFlag;
	String status;
	String productType;
	String shipMethodFlorist;
	String shipMethodCarrier;
	String productId;
    
	public String getNovatorId() {
		return novatorId;
	}
	public void setNovatorId(String novatorId) {
		this.novatorId = novatorId;
	}
	public String getPquadId() {
		return pquadId;
	}
	public void setPquadId(String pquadId) {
		this.pquadId = pquadId;
	}
	public String getProcessedFlag() {
		return processedFlag;
	}
	public void setProcessedFlag(String processedFlag) {
		this.processedFlag = processedFlag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getShipMethodFlorist() {
		return shipMethodFlorist;
	}
	public void setShipMethodFlorist(String shipMethodFlorist) {
		this.shipMethodFlorist = shipMethodFlorist;
	}
	public String getShipMethodCarrier() {
		return shipMethodCarrier;
	}
	public void setShipMethodCarrier(String shipMethodCarrier) {
		this.shipMethodCarrier = shipMethodCarrier;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
     
 
}
