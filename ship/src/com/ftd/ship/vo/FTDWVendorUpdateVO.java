package com.ftd.ship.vo;

/**
 * @author vsana
 * Class 			: FTDWVendorUpdateVO
 * Description		: An instance of this VO represents a record read from the vendor updates file sent by FTDWest.
 *
 */
public class FTDWVendorUpdateVO 
{
	private String fillingVendor;
	private String venusOrderNumber;
    private String pid;
    private String unitcost;
    private String indicator;
    
    public FTDWVendorUpdateVO(String fillingVendor, String venusOrderNumber, String pid, String unitcost, String indicator) 
    {
    	this.fillingVendor = fillingVendor;
    	this.venusOrderNumber = venusOrderNumber;
    	this.pid = pid;
    	this.unitcost = unitcost;
    	this.indicator = indicator;
    }

    public String getFillingVendor() {
		return fillingVendor;
	}

	public void setFillingVendor(String fillingVendor) {
		this.fillingVendor = fillingVendor;
	}
    
    public String getVenusOrderNumber() {
		return venusOrderNumber;
	}

	public void setVenusOrderNumber(String venusOrderNumber) {
		this.venusOrderNumber = venusOrderNumber;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getUnitcost() {
		return unitcost;
	}

	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}
}
