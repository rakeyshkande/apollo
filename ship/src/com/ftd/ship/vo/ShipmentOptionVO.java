package com.ftd.ship.vo;

import java.math.BigDecimal;

import java.util.Date;


public class ShipmentOptionVO {

    private String venusOrderNumber;
    private String carrierId;
    private String shipMethodId;
    private Integer zoneCode;
    private BigDecimal rateWeight;
    private BigDecimal rateCharge;
    private BigDecimal handlingCharge;
    private BigDecimal totalCharge;
    private BigDecimal freightCharge;
    private Date dateShipped;
    private Date expectedDeliveryDate;
    private String distributionCenter;
    private String shipPointId;
    private String errorTxt;
    private String zoneJumpFlag;
    private int idealRankNumber;
    private int actualRankNumber;
    private Date originalShipDate;
    private Date secondaryShipDate;
    
    public ShipmentOptionVO() {
    }


    public void setVenusOrderNumber(String venusOrderNumber) {
        this.venusOrderNumber = venusOrderNumber;
    }

    public String getVenusOrderNumber() {
        return venusOrderNumber;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setShipMethodId(String shipMethodId) {
        this.shipMethodId = shipMethodId;
    }

    public String getShipMethodId() {
        return shipMethodId;
    }

    public void setZoneCode(Integer zoneCode) {
        this.zoneCode = zoneCode;
    }

    public Integer getZoneCode() {
        return zoneCode;
    }

    public void setRateWeight(BigDecimal rateWeight) {
        this.rateWeight = rateWeight;
    }

    public BigDecimal getRateWeight() {
        return rateWeight;
    }

    public void setRateCharge(BigDecimal rateCharge) {
        this.rateCharge = rateCharge;
    }

    public BigDecimal getRateCharge() {
        return rateCharge;
    }

    public void setHandlingCharge(BigDecimal handlingCharge) {
        this.handlingCharge = handlingCharge;
    }

    public BigDecimal getHandlingCharge() {
        return handlingCharge;
    }

    public void setTotalCharge(BigDecimal totalCharge) {
        this.totalCharge = totalCharge;
    }

    public BigDecimal getTotalCharge() {
        return totalCharge;
    }

    public void setFreightCharge(BigDecimal freightCharge) {
        this.freightCharge = freightCharge;
    }

    public BigDecimal getFreightCharge() {
        return freightCharge;
    }

    public void setDateShipped(Date dateShipped) {
        this.dateShipped = dateShipped;
    }

    public Date getDateShipped() {
        return dateShipped;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setDistributionCenter(String distributionCenter) {
        this.distributionCenter = distributionCenter;
    }

    public String getDistributionCenter() {
        return distributionCenter;
    }

    public void setShipPointId(String shipPointId) {
        this.shipPointId = shipPointId;
    }

    public String getShipPointId() {
        return shipPointId;
    }

    public void setErrorTxt(String errorTxt) {
        this.errorTxt = errorTxt;
    }

    public String getErrorTxt() {
        return errorTxt;
    }

    public void setZoneJumpFlag(String zoneJumpFlag) {
        this.zoneJumpFlag = zoneJumpFlag;
    }

    public String getZoneJumpFlag() {
        return zoneJumpFlag;
    }

    public void setIdealRankNumber(int idealRankNumber) {
        this.idealRankNumber = idealRankNumber;
    }

    public int getIdealRankNumber() {
        return idealRankNumber;
    }

    public void setActualRankNumber(int actualRankNumber) {
        this.actualRankNumber = actualRankNumber;
    }

    public int getActualRankNumber() {
        return actualRankNumber;
    }


	public Date getOriginalShipDate() {
		return originalShipDate;
	}


	public void setOriginalShipDate(Date originalShipDate) {
		this.originalShipDate = originalShipDate;
	}


	public Date getSecondaryShipDate() {
		return secondaryShipDate;
	}


	public void setSecondaryShipDate(Date secondaryShipDate) {
		this.secondaryShipDate = secondaryShipDate;
	}
}
