package com.ftd.ship.vo;

import java.util.List;

/**
 * @author kkeyan
 *
 */
public class FtdwestLogisticsVO {
	
	private VenusMessageVO venusMessageVO;
	private  OrderVO orderVO;
	private OrderDetailVO orderDetailVO;
	private List<FtdwestPersonalizationVO> ftdwestPersonalizationVO;
	

	public VenusMessageVO getVenusMessageVO() {
		return venusMessageVO;
	}

	public void setVenusMessageVO(VenusMessageVO venusMessageVO) {
		this.venusMessageVO = venusMessageVO;
	}

	public OrderVO getOrderVO() {
		return orderVO;
	}

	public void setOrderVO(OrderVO orderVO) {
		this.orderVO = orderVO;
	}

	public OrderDetailVO getOrderDetailVO() {
		return orderDetailVO;
	}

	public void setOrderDetailVO(OrderDetailVO orderDetailVO) {
		this.orderDetailVO = orderDetailVO;
	}

	public List<FtdwestPersonalizationVO> getFtdwestPersonalizationVO() {
		return ftdwestPersonalizationVO;
	}

	public void setFtdwestPersonalizationVO(List<FtdwestPersonalizationVO> ftdwestPersonalizationVO) {
		this.ftdwestPersonalizationVO = ftdwestPersonalizationVO;
	}
	
}
