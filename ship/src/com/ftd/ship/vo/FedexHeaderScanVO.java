package com.ftd.ship.vo;

import org.apache.commons.lang.StringUtils;

public class FedexHeaderScanVO {

    private String line;
    private String accountNumber;
    private String airbillNumber;
    private String airbillNumberGround;
    private String airbillNumberExpress;
    private String shipDate;
    private String commitDate;
    private String commitTime;
    private String exceptionCode;
    private String exceptionProcess;
    private String reference;
    private String customerQualifier;
    private String formID;
    private String trackingIndicator;
        
    public FedexHeaderScanVO(String line) {
        this.line = line;
        this.accountNumber = StringUtils.trimToEmpty(StringUtils.substring(line,1,10));
        this.airbillNumberExpress = StringUtils.trimToEmpty(StringUtils.substring(line,10,22));
        this.shipDate = StringUtils.trimToEmpty(StringUtils.substring(line,22,30));
        this.commitDate = StringUtils.trimToEmpty(StringUtils.substring(line,30,38));
        this.commitTime = StringUtils.trimToEmpty(StringUtils.substring(line,38,42));
        this.exceptionCode = StringUtils.trimToEmpty(StringUtils.substring(line,42,43));
        this.exceptionProcess = StringUtils.trimToEmpty(StringUtils.substring(line,43,49));
        this.reference = StringUtils.trimToEmpty(StringUtils.substring(line,49,73));
        this.customerQualifier = StringUtils.trimToEmpty(StringUtils.substring(line,73,75));
        this.formID = StringUtils.trimToEmpty(StringUtils.substring(line,75,79));
        this.trackingIndicator = StringUtils.trimToEmpty(StringUtils.substring(line,79,80));
        this.airbillNumberGround = StringUtils.trimToEmpty(StringUtils.substring(line,401,423));

        // Set actual Airbill Number to Express or Ground number.  If Ground order, Express number will be all zeros.
        //
        if (this.airbillNumberExpress.matches("0*")) {
            this.airbillNumber = this.airbillNumberGround;
        } else {
            this.airbillNumber = this.airbillNumberExpress;
        }
    }

    public String getLine() {
        return line;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAirbillNumber() {
        return airbillNumber;
    }

    public String getShipDate() {
        return shipDate;
    }

    public String getCommitDate() {
        return commitDate;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionProcess() {
        return exceptionProcess;
    }

    public String getReference() {
        return reference;
    }

    public String getCustomerQualifier() {
        return customerQualifier;
    }

    public String getFormID() {
        return formID;
    }

    public String getTrackingIndicator() {
        return trackingIndicator;
    }

    public void setAirbillNumberGround(String airbillNumberGround) {
        this.airbillNumberGround = airbillNumberGround;
    }

    public String getAirbillNumberGround() {
        return airbillNumberGround;
    }

    public void setAirbillNumberExpress(String airbillNumberExpress) {
        this.airbillNumberExpress = airbillNumberExpress;
    }

    public String getAirbillNumberExpress() {
        return airbillNumberExpress;
    }
}
