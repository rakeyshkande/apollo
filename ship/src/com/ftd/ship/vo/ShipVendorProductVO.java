package com.ftd.ship.vo;

import com.ftd.ship.vo.VendorProductVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShipVendorProductVO extends VendorProductVO {
    List<CarrierVO> carrierIds;
    String subcodeDescription;
    SDSZoneJumpTripVO zoneJumpTrip;
    
    public ShipVendorProductVO() {
        super();
    }

    public void setSubcodeDescription(String subcodeDescription) {
        this.subcodeDescription = subcodeDescription;
    }

    public String getSubcodeDescription() {
        return subcodeDescription;
    }

    public void setCarrierIds(List<CarrierVO> carrierIds) {
        this.carrierIds = carrierIds;
    }

    public List<CarrierVO> getCarrierIds() {
        return carrierIds;
    }

    public void setZoneJumpTrip(SDSZoneJumpTripVO zoneJumpTrip) {
        this.zoneJumpTrip = zoneJumpTrip;
    }

    public SDSZoneJumpTripVO getZoneJumpTrip() {
        return zoneJumpTrip;
    }
}
