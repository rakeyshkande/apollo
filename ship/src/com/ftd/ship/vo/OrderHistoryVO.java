package com.ftd.ship.vo;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class OrderHistoryVO extends BaseVO
{
  private	long	    orderHistoryId;
  private	String	  orderGuid;
  private	long	    orderDetailId;
  private	String	  commentOrigin;
  private	String	  csrId;
  private	Date	historyStartDate;
  private	Date	historyEndDate;
  private	char	    systemEndedIndicator;


  public OrderHistoryVO()
  {
  }

  public void setOrderGuid(String orderGuid)
  {
    if(valueChanged(this.orderGuid, orderGuid))
    {
      setChanged(true);
    }
    this.orderGuid = trim(orderGuid);
  }


  public String getOrderGuid()
  {
    return orderGuid;
  }



  public void setOrderDetailId(long orderDetailId)
  {
    if(valueChanged(this.orderDetailId, orderDetailId))
    {
      setChanged(true);
    }
    this.orderDetailId = orderDetailId;
  }


  public long getOrderDetailId()
  {
    return orderDetailId;
  }


  public void setCommentOrigin(String commentOrigin)
  {
    if(valueChanged(this.commentOrigin, commentOrigin))
    {
      setChanged(true);
    }
    this.commentOrigin = trim(commentOrigin);
  }


  public String getCommentOrigin()
  {
    return commentOrigin;
  }


  public void setOrderHistoryId(long orderHistoryId)
  {
    if(valueChanged(this.orderHistoryId, orderHistoryId))
    {
      setChanged(true);
    }
    this.orderHistoryId = orderHistoryId;
  }


  public long getOrderHistoryId()
  {
    return orderHistoryId;
  }


  public void setCsrId(String csrId)
  {
    if(valueChanged(this.csrId, csrId))
    {
      setChanged(true);
    }
    this.csrId = trim(csrId);
  }


  public String getCsrId()
  {
    return csrId;
  }


  public void setHistoryStartDate(Date historyStartDate)
  {
    if(valueChanged(this.historyStartDate, historyStartDate))
    {
      setChanged(true);
    }
    this.historyStartDate = historyStartDate;
  }


  public Date getHistoryStartDate()
  {
    return historyStartDate;
  }


  public void setHistoryEndDate(Date historyEndDate)
  {
    if(valueChanged(this.historyEndDate, historyEndDate))
    {
      setChanged(true);
    }
    this.historyEndDate = historyEndDate;
  }


  public Date getHistoryEndDate()
  {
    return historyEndDate;
  }


  public void setSystemEndedIndicator(char systemEndedIndicator)
  {
    if(valueChanged(this.systemEndedIndicator, systemEndedIndicator))
    {
      setChanged(true);
    }
    this.systemEndedIndicator = systemEndedIndicator;
  }


  public char getSystemEndedIndicator()
  {
    return systemEndedIndicator;
  }

  private String trim(String str)
  {
    return (str != null)?str.trim():str;
  }



}
