package com.ftd.ship.vo;

import java.util.List;

import org.w3c.dom.Document;


public class SDSLogisticsVO {
    private boolean rateShopOnly;
    private Document shipUnit;
    private SDSShipResponseVO shipUnitResponse;
    private VenusMessageVO messageVo;
    private OrderVO orderVO;
    private OrderDetailVO orderDetailVO;
    private CompanyVO companyVO;
    private CustomerVO recipientVO;
    private ProductVO productVO;
    private List<ShipVendorProductVO> availableVendors;
    private String shipVia;
    private SDSRateResponseVO rateResponse;
    private List<SDSZoneJumpTripVO> zoneJumpHubs;
    private List<ShipmentOptionVO> shipmentOptions;
    
    public SDSLogisticsVO() {
    }

    public void setRateShopOnly(boolean rateShopOnly) {
        this.rateShopOnly = rateShopOnly;
    }

    public boolean isRateShopOnly() {
        return rateShopOnly;
    }

    public void setMessageVo(VenusMessageVO messageVo) {
        this.messageVo = messageVo;
    }

    public VenusMessageVO getMessageVo() {
        return messageVo;
    }

    public void setShipUnit(Document shipUnit) {
        this.shipUnit = shipUnit;
    }

    public Document getShipUnit() {
        return shipUnit;
    }

    public void setShipUnitResponse(SDSShipResponseVO shipUnitResponse) {
        this.shipUnitResponse = shipUnitResponse;
    }

    public SDSShipResponseVO getShipUnitResponse() {
        return shipUnitResponse;
    }

    public void setOrderVO(OrderVO orderVO) {
        this.orderVO = orderVO;
    }

    public OrderVO getOrderVO() {
        return orderVO;
    }

    public void setOrderDetailVO(OrderDetailVO orderDetailVO) {
        this.orderDetailVO = orderDetailVO;
    }

    public OrderDetailVO getOrderDetailVO() {
        return orderDetailVO;
    }

    public void setCompanyVO(CompanyVO companyVO) {
        this.companyVO = companyVO;
    }

    public CompanyVO getCompanyVO() {
        return companyVO;
    }

    public void setRecipientVO(CustomerVO recipientVO) {
        this.recipientVO = recipientVO;
    }

    public CustomerVO getRecipientVO() {
        return recipientVO;
    }

    public void setProductVO(ProductVO productVO) {
        this.productVO = productVO;
    }

    public ProductVO getProductVO() {
        return productVO;
    }

    public void setAvailableVendors(List<ShipVendorProductVO> availableVendors) {
        this.availableVendors = availableVendors;
    }

    public List<ShipVendorProductVO> getAvailableVendors() {
        return availableVendors;
    }

    public void setShipVia(String shipVia) {
        this.shipVia = shipVia;
    }

    public String getShipVia() {
        return shipVia;
    }

    public void setZoneJumpHubs(List<SDSZoneJumpTripVO> hubs) {
        this.zoneJumpHubs = hubs;
    }

    public List<SDSZoneJumpTripVO> getZoneJumpHubs() {
        return zoneJumpHubs;
    }

    public void setRateResponse(SDSRateResponseVO rateResponse) {
        this.rateResponse = rateResponse;
    }

    public SDSRateResponseVO getRateResponse() {
        return rateResponse;
    }

  public void setShipmentOptions(List<ShipmentOptionVO> shipmentOptions)
  {
    this.shipmentOptions = shipmentOptions;
  }

  public List<ShipmentOptionVO> getShipmentOptions()
  {
    return shipmentOptions;
  }
}
