package com.ftd.ship.vo;

import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;

import java.math.BigDecimal;

import java.util.Date;

public class SDSMessageVO extends SDSResponseVO {
    private MsgType msgType;
    private MsgDirection direction;
    private String cartonNumber;
    private String trackingNumber;
    private String printBatchNumber;
    private BigDecimal printBatchSequence;
    private Date shipDate;
    private String status;
    private Date statusDate;
    private boolean processed = false;
    private BigDecimal msgId;

    public void setMsgType(MsgType msgType) {
        this.msgType = msgType;
    }

    public MsgType getMsgType() {
        return msgType;
    }

    public void setDirection(MsgDirection direction) {
        this.direction = direction;
    }

    public MsgDirection getDirection() {
        return direction;
    }

    public void setCartonNumber(String cartonNumber) {
        this.cartonNumber = cartonNumber;
    }

    public String getCartonNumber() {
        return cartonNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setPrintBatchNumber(String printBatchNumber) {
        this.printBatchNumber = printBatchNumber;
    }

    public String getPrintBatchNumber() {
        return printBatchNumber;
    }

    public void setPrintBatchSequence(BigDecimal printBatchSequence) {
        this.printBatchSequence = printBatchSequence;
    }

    public BigDecimal getPrintBatchSequence() {
        return printBatchSequence;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setMsgId(BigDecimal msgId) {
        this.msgId = msgId;
    }

    public BigDecimal getMsgId() {
        return msgId;
    }
}
