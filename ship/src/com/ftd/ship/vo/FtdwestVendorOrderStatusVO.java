/**
 * 
 */
package com.ftd.ship.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;

/**
 * @author vbejawar
 * @since 08/10/2015
 *
 */
public class FtdwestVendorOrderStatusVO {
	
	BigDecimal orderStatusId;
	String venusOrderNumber;
	Long partnerOrderNumber;
	String partnerId;
	String trackingNumber;
	String carrier;
	String orderStatus;
	Date printed;
	Date shipped;
	Date delivered;
	Date rejected;
	Long rejectCode;
	String rejectMessage;
	String status;
	MsgType msgType;
	MsgDirection msgDirection;
	
	boolean sendCancelMsgToWest = false;
	
	
	/**
	 * @return the orderStatusId
	 */
	public BigDecimal getOrderStatusId() {
		return orderStatusId;
	}
	/**
	 * @param orderStatusId the orderStatusId to set
	 */
	public void setOrderStatusId(BigDecimal orderStatusId) {
		this.orderStatusId = orderStatusId;
	}
	/**
	 * @return the venusOrderNumber
	 */
	public String getVenusOrderNumber() {
		return venusOrderNumber;
	}
	/**
	 * @param venusOrderNumber the venusOrderNumber to set
	 */
	public void setVenusOrderNumber(String venusOrderNumber) {
		this.venusOrderNumber = venusOrderNumber;
	}
	/**
	 * @return the partnerOrderNumber
	 */
	public Long getPartnerOrderNumber() {
		return partnerOrderNumber;
	}
	/**
	 * @param partnerOrderNumber the partnerOrderNumber to set
	 */
	public void setPartnerOrderNumber(Long partnerOrderNumber) {
		this.partnerOrderNumber = partnerOrderNumber;
	}
	/**
	 * @return the partnerId
	 */
	public String getPartnerId() {
		return partnerId;
	}
	/**
	 * @param partnerId the partnerId to set
	 */
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	/**
	 * @return the trackingNumber
	 */
	public String getTrackingNumber() {
		return trackingNumber;
	}
	/**
	 * @param trackingNumber the trackingNumber to set
	 */
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}
	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return orderStatus;
	}
	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * @return the printed
	 */
	public Date getPrinted() {
		return printed;
	}
	/**
	 * @param printed the printed to set
	 */
	public void setPrinted(Date printed) {
		this.printed = printed;
	}
	/**
	 * @return the shipped
	 */
	public Date getShipped() {
		return shipped;
	}
	/**
	 * @param shipped the shipped to set
	 */
	public void setShipped(Date shipped) {
		this.shipped = shipped;
	}
	/**
	 * @return the delivered
	 */
	public Date getDelivered() {
		return delivered;
	}
	/**
	 * @param delivered the delivered to set
	 */
	public void setDelivered(Date delivered) {
		this.delivered = delivered;
	}
	/**
	 * @return the rejected
	 */
	public Date getRejected() {
		return rejected;
	}
	/**
	 * @param rejected the rejected to set
	 */
	public void setRejected(Date rejected) {
		this.rejected = rejected;
	}
	/**
	 * @return the rejectCode
	 */
	public Long getRejectCode() {
		return rejectCode;
	}
	/**
	 * @param rejectCode the rejectCode to set
	 */
	public void setRejectCode(Long rejectCode) {
		this.rejectCode = rejectCode;
	}
	/**
	 * @return the rejectMessage
	 */
	public String getRejectMessage() {
		return rejectMessage;
	}
	/**
	 * @param rejectMessage the rejectMessage to set
	 */
	public void setRejectMessage(String rejectMessage) {
		this.rejectMessage = rejectMessage;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	public MsgType getMsgType() {
		return this.msgType;
	}
	
	/**
	 * @return the sendCancelMsgToWest
	 */
	public boolean isSendCancelMsgToWest() {
		return sendCancelMsgToWest;
	}
	/**
	 * @param sendCancelMsgToWest the sendCancelMsgToWest to set
	 */
	public void setSendCancelMsgToWest(boolean sendCancelMsgToWest) {
		this.sendCancelMsgToWest = sendCancelMsgToWest;
	}
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;		
	}
	
	public MsgDirection getMsgDirection() {
		return this.msgDirection;
	}
	
	public void setDirection(MsgDirection msgDirection) {
		this.msgDirection = msgDirection; 		
	}
}
