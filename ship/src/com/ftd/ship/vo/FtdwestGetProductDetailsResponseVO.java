package com.ftd.ship.vo;

import java.util.HashMap;
import java.util.List;

import com.providecommerce.api.api.product.v1.ArrayOfProductDetail;

public class FtdwestGetProductDetailsResponseVO {
	private List <WestProductUpdateVO> westProductUpdate;
	private String getProductDetailsRequestMessage;
	private String getProductDetailsResponseMessage;
	private String getProductDetailsStatus;
	private HashMap<String, String> errorMap;	
	private ArrayOfProductDetail productDetails;
	

	public List<WestProductUpdateVO> getWestProductUpdate() {
		return westProductUpdate;
	}

	public void setWestProductUpdate(List<WestProductUpdateVO> westProductUpdate) {
		this.westProductUpdate = westProductUpdate;
	}

	public String getGetProductDetailsRequestMessage() {
		return getProductDetailsRequestMessage;
	}

	public void setGetProductDetailsRequestMessage(String getProductDetailsRequestMessage) {
		this.getProductDetailsRequestMessage = getProductDetailsRequestMessage;
	}

	public String getGetProductDetailsResponseMessage() {
		return getProductDetailsResponseMessage;
	}

	public void setGetProductDetailsResponseMessage(String getProductDetailsResponseMessage) {
		this.getProductDetailsResponseMessage = getProductDetailsResponseMessage;
	}

	public String getGetProductDetailsStatus() {
		return getProductDetailsStatus;
	}

	public void setGetProductDetailsStatus(String getProductDetailsStatus) {
		this.getProductDetailsStatus = getProductDetailsStatus;
	}
	
	public HashMap<String, String> getErrorMap() {
		return errorMap;
	}
	public void setErrorMap(HashMap<String, String> errorMap) {
		this.errorMap = errorMap;
	}
	public ArrayOfProductDetail getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(ArrayOfProductDetail productDetails) {
		this.productDetails = productDetails;
	}

}
