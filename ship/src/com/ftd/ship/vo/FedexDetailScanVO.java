package com.ftd.ship.vo;

import org.apache.commons.lang.StringUtils;

public class FedexDetailScanVO {
    private String line;
    private String scanType;
    private String scanCity;
    private String scanCityZipCode;
    private String scanState;
    private String scanDate;
    private String scanTime;
    private String scanStatus;
    private String returnAirbillNumber;
    private String dispatchNumber;
    private String deliveryLocation;
    private String deliverySignature;
    private String service;
    private String weight;
    private String pieces;
    private String codIndicator;
    private String shipperName;
    private String dangerousGoodsIndicator;
    private String recipientName;
    private String attemptedDeliveryTime;
    private String comment;
    private String delayTime;
    private String delayTypeMsg;
    private String delayLocationZip;
    
    public FedexDetailScanVO(String line) {
        this.line = line;
        this.scanType = StringUtils.trimToEmpty(StringUtils.substring(line,1,3));
        this.scanCity = StringUtils.trimToEmpty(StringUtils.substring(line,3,28));
        this.scanCityZipCode = StringUtils.trimToEmpty(StringUtils.substring(line,28,33));
        this.scanState = StringUtils.trimToEmpty(StringUtils.substring(line,33,35));
        this.scanDate = StringUtils.trimToEmpty(StringUtils.substring(line,35,43));
        this.scanTime = StringUtils.trimToEmpty(StringUtils.substring(line,43,47));
        this.scanStatus = StringUtils.trimToEmpty(StringUtils.substring(line,47,49));
        this.returnAirbillNumber = StringUtils.trimToEmpty(StringUtils.substring(line,49,82));
        this.dispatchNumber = StringUtils.trimToEmpty(StringUtils.substring(line,49,54));
        this.deliveryLocation = StringUtils.trimToEmpty(StringUtils.substring(line,49,50));
        this.deliverySignature = StringUtils.trimToEmpty(StringUtils.substring(line,50,80));
        this.service = StringUtils.trimToEmpty(StringUtils.substring(line,49,51));
        this.weight = StringUtils.trimToEmpty(StringUtils.substring(line,51,58));
        this.pieces = StringUtils.trimToEmpty(StringUtils.substring(line,59,62));
        this.codIndicator = StringUtils.trimToEmpty(StringUtils.substring(line,62,63));
        this.shipperName = StringUtils.trimToEmpty(StringUtils.substring(line,63,73));
        this.dangerousGoodsIndicator = StringUtils.trimToEmpty(StringUtils.substring(line,73,74));
        this.recipientName = StringUtils.trimToEmpty(StringUtils.substring(line,74,84));
        this.attemptedDeliveryTime = StringUtils.trimToEmpty(StringUtils.substring(line,49,53));
        this.comment = StringUtils.trimToEmpty(StringUtils.substring(line,49,82));
        this.delayTime = StringUtils.trimToEmpty(StringUtils.substring(line,54,59));
        this.delayTypeMsg = StringUtils.trimToEmpty(StringUtils.substring(line,60,62));
        this.delayLocationZip = StringUtils.trimToEmpty(StringUtils.substring(line,72,77));
    }

    public String getLine() {
        return line;
    }

    public String getScanType() {
        return scanType;
    }

    public String getScanCity() {
        return scanCity;
    }

    public String getScanCityZipCode() {
        return scanCityZipCode;
    }

    public String getScanState() {
        return scanState;
    }

    public String getScanDate() {
        return scanDate;
    }

    public String getScanTime() {
        return scanTime;
    }

    public String getScanStatus() {
        return scanStatus;
    }

    public String getReturnAirbillNumber() {
        return returnAirbillNumber;
    }

    public String getDispatchNumber() {
        return dispatchNumber;
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public String getDeliverySignature() {
        return deliverySignature;
    }

    public String getService() {
        return service;
    }

    public String getWeight() {
        return weight;
    }

    public String getPieces() {
        return pieces;
    }

    public String getCodIndicator() {
        return codIndicator;
    }

    public String getShipperName() {
        return shipperName;
    }

    public String getDangerousGoodsIndicator() {
        return dangerousGoodsIndicator;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public String getAttemptedDeliveryTime() {
        return attemptedDeliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public String getDelayTime() {
        return delayTime;
    }

    public String getDelayTypeMsg() {
        return delayTypeMsg;
    }

    public String getDelayLocationZip() {
        return delayLocationZip;
    }
}
