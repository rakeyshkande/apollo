package com.ftd.ship.mdb;

import java.sql.Connection;
import java.util.ArrayList;

import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.ship.bo.FtdwestProcessingBO;
import com.ftd.ship.bo.SDSOutboundMsgProcessingBO;
import com.ftd.ship.bo.SDSShipmentProcessingBO;
import com.ftd.ship.bo.VenusProcessingBO;
import com.ftd.ship.bo.communications.ftdwest.FtdwestApplicationException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunicationsException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestRuntimeException;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSCommunicationsException;
import com.ftd.ship.bo.communications.sds.SDSRuntimeException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShippingSystem;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.OrderDAO;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.VenusMessageVO;


public class ShipProcessingMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private static Logger logger = 
        new Logger("com.ftd.ship.mdb.ShipProcessingMDB");
    private ResourceProviderBase resourceProvider;
    private VenusProcessingBO venusProcessingBO;
    private SDSShipmentProcessingBO sdsShipProcessingBO;
    private SDSOutboundMsgProcessingBO sdsOutProcessingBO;
    private FtdwestProcessingBO ftdwestProcessingBO;
    
    private ShipDAO shipDAO;
    private OrderDAO orderDAO;
    protected MessageDrivenContext messageDrivenContext = null;
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        venusProcessingBO = (VenusProcessingBO)this.getBeanFactory().getBean("venusProcessingBO");
        sdsShipProcessingBO = (SDSShipmentProcessingBO)this.getBeanFactory().getBean("sdsShipProcessingBO");
        sdsOutProcessingBO = (SDSOutboundMsgProcessingBO)this.getBeanFactory().getBean("sdsOutProcessingBO");
        ftdwestProcessingBO=(FtdwestProcessingBO)this.getBeanFactory().getBean("ftdwestProcessingBO");
        shipDAO = (ShipDAO)this.getBeanFactory().getBean("shipDAO");
        orderDAO = (OrderDAO)this.getBeanFactory().getBean("orderDAO");
    }
    
    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
      this.messageDrivenContext = ctx;
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    *
    * @param msg The Message which contains the payload of the JMS message
    */
    public void onMessage(Message msg) {
        logger.debug("ShipProcessingMDB Start: onMessage()");

        Connection connection = null;
        String venusId = null;
        VenusMessageVO vo = null;

        try{
            try {
                
                venusId = StringUtils.trim(((TextMessage)msg).getText());
    
                if (StringUtils.isBlank(venusId)) {
                    assert (false) : "Ship processing has received an empty payload";
                    //Send out system notification 
                    try {
                        CommonUtils.getInstance().sendSystemMessage("Ship processing has received an empty payload.");
                    } catch (Exception e) {
                        logger.error("Unable to send message to support pager.");
                        logger.fatal(e);
                    }
                    return;
                }
    
                connection = resourceProvider.getDatabaseConnection();
                
                vo = shipDAO.getVenusMessage(connection, venusId);
                AddOnUtility addOnUTIL = new AddOnUtility();
                ArrayList <AddOnVO> addOnVOList = addOnUTIL.getOrderAddonListByVenusId(venusId, true, connection);
                vo.setAddOnVO(addOnVOList);    
                
                if ( vo==null ) {
                    //Order processing has not committed the order yet, so, throw msg back to the container to retry
                    logger.warn("Unable to locate venus id "+venusId+" in the database.  Will give JMS message back to the container to retry.");
                    throw new SDSRuntimeException("Unable to locate venus id "+venusId+" in the database.");
                }
                
                if ( vo.getShippingSystem()==null ) {
                    throw new SDSApplicationException("Venus id "+venusId+" has no shipping system associated with it.");
                }
    
                if (StringUtils.equals(vo.getShippingSystem().toString(), ShippingSystem.ESCALATE.toString())) {
                    venusProcessingBO.processVenusOrder(connection, venusId);
                } else if (StringUtils.equals(vo.getShippingSystem().toString(), ShippingSystem.SDS.toString())) {
                    //Never change a venus pending status
                    if( !VenusStatus.PENDING.equals( vo.getVenusStatus() )  ) {  
                        vo.setVenusStatus(VenusStatus.SHIP_PROCESSING);
                        vo.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
                    }
    
                    if (StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.ORDER.getFtdMsgType())) 
                    {
                        shipDAO.updateVenusStatus(connection, vo );                    

                        long startProcessTime = System.currentTimeMillis();
                        sdsShipProcessingBO.processRequest(connection, vo);
                        long endProcessTime = System.currentTimeMillis();
                        logger.debug("Venus Order Number, " + vo.getVenusOrderNumber() + ", with Venus Id, " + vo.getVenusId() + ", took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                    }         
                    else if (StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.CANCEL.getFtdMsgType()) ||
                               StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.REJECT.getFtdMsgType()) ) {
                               
                        //Technically the reject is an inbound msg, but, EOD is generating it on behalf of
                        //the vendor because they never printed the label, so, it's on the venus record as 
                        //an inbound reject.  We need to do a "Cancel" to SDS via the SDSOutboundProcessingBO.  
                        //By making it an inbound, we are indicating that the vendor implied the reject by 
                        //not printing the ship label.
                        shipDAO.updateVenusStatus(connection, vo );
                        sdsOutProcessingBO.processCancelRequest(connection, vo);
                    } else {
                        throw new SDSApplicationException("Message type of " + 
                                                          vo.getMsgType() + 
                                                          " is not supported.");
                    }
                } else if (StringUtils.equals(vo.getShippingSystem().getValue(), ShippingSystem.FTD_WEST.getValue())) {
                    // implement here for DSI..
                	
                	logger.debug("Ftdwest Message type MessageType==>"+vo.getMsgType());
                	
                	if( !VenusStatus.PENDING.equals( vo.getVenusStatus() )  ) {  
                        vo.setVenusStatus(VenusStatus.SHIP_PROCESSING);
                        vo.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
                    }
                	
                	
                	 if (StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.ORDER.getFtdMsgType())) 
                     {
                		 
                		 	shipDAO.updateVenusStatus(connection, vo );    
                		 
		                	long startProcessTime = System.currentTimeMillis();
		                	ftdwestProcessingBO.processFtdwestOrder(connection, vo);
		                	long endProcessTime = System.currentTimeMillis();                	
		                	logger.info("FTD West Order Venus Order Number, " + vo.getVenusOrderNumber() + ", with Venus Id, " + vo.getVenusId() + ", took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                     }else if (StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.CANCEL.getFtdMsgType()) ||
                             StringUtils.equals(vo.getMsgType().getFtdMsgType(), MsgType.REJECT.getFtdMsgType()) )
                     {
                    	 // Cancel FTDWest Orders here....
                    	 shipDAO.updateVenusStatus(connection, vo );  
                    	 
                    	 ftdwestProcessingBO.cancelFtdwestOrder(connection, vo);
                     }
                	
                	
                } else {
                    throw new SDSApplicationException("Shipping system " + 
                                                      vo.getShippingSystem() + 
                                                      " is not supported.");
                }
    
            } catch (SDSCommunicationsException commE) {
                String errMsg = "Communications error while processing venus id: " + 
                             venusId + 
                             ".  Order will be scheduled for reprocessing";
                logger.error(errMsg);
                logger.error(commE);
                
                // Wrap exception to send error message in page.
                throw new Exception(errMsg);

            } catch (SDSApplicationException appE) {
                String errMsg = 
                    "Error from Argo while processing venus id: " + venusId + 
                    ".  Manual intervention is required.";
                logger.error(errMsg);
                logger.error(appE);

                // Wrap exception to send error message in page.
                throw new Exception(errMsg);
            } catch (SDSRuntimeException rte) {
                String errMsg = "Ship Processing MDB failed with SDSRuntimeException. Venus Id = " + venusId;
                logger.error(errMsg);
                logger.error(rte);
                throw new Exception(errMsg);
            }  catch (FtdwestCommunicationsException commE) {
                String errMsg = "Ftdwest Communications error while processing venus id: " + 
                        venusId + 
                        ".  Order will be scheduled for reprocessing";
	           logger.error(errMsg);
	           logger.error(commE);	           
	           // Wrap exception to send error message in page.
	           throw new Exception(errMsg);

            } catch (FtdwestApplicationException appE) {
	           String errMsg = 
	               "Error from Ftdwest while processing venus id: " + venusId + 
	               ".  Manual intervention is required.";
	           logger.error(errMsg);
	           logger.error(appE);
	
	           // Wrap exception to send error message in page.
	           throw new Exception(errMsg); 
           
            } catch (FtdwestRuntimeException rte) {
                String errMsg = "Ship Processing MDB failed with Ftdwest RuntimeException. Venus Id = " + venusId;
                logger.error(errMsg);
                logger.error(rte);
                throw new Exception(errMsg);
            }catch (Throwable t) {
                String errMsg = "Ship Processing MDB failed with Throwable. Venus Id = " + venusId;
                logger.error(errMsg);
                logger.error(t);
                
                throw new Exception(errMsg);
            } 
        } catch(Exception ge) {
             //Send out system notification 
             String errMsg = ge.getMessage();
             try {
                 CommonUtils.getInstance().sendSystemMessage(errMsg);
             } catch (Exception e) {
                 errMsg = "Unable to send message to support pager.";
                 logger.fatal(errMsg,e);
             }
            
            // Rollback the transaction mananged by container and do not acknowledge message. VENUS_STATUS stays in 'SHIP'.
            messageDrivenContext.setRollbackOnly();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                    logger.warn(e);
            }
            if (messageDrivenContext.getRollbackOnly())
            	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
        }
        logger.debug("End: onMessage()");
    }
}
