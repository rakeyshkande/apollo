package com.ftd.ship.mdb;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.SDSManifestProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSCommunicationsException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;

import java.sql.Connection;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;

import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;


public class SDSManifestingMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
    private SDSManifestProcessingBO sdsManifestProcessingBO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.SDSManifestingMDB");
    
    public SDSManifestingMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        sdsManifestProcessingBO = (SDSManifestProcessingBO)this.getBeanFactory().getBean("sdsManifestProcessingBO");
    }

    public void onMessage(Message message) {
        logger.debug("SDSManifestingMDB Start: onMessage()");
        
        Connection connection = null;
        
        try {
            connection = resourceProvider.getDatabaseConnection();
            sdsManifestProcessingBO.processRequest(connection,((TextMessage)message).getText());
        } catch (SDSCommunicationsException commE) {
            //Really don't care since the process is not transactional
            logger.warn("Comm error with SDS ship server.  Will try again later.",commE);
        } catch (SDSApplicationException appE) {
            logger.error(appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(appE.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } catch (Throwable t) {
            logger.error("Failure deep in the bowels of SDSManifestingMDB: ",t);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage("Failure deep in the bowels of SDSManifestingMDB: " + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Failure to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) {
        this.context = messageDrivenContext;
    }
}
