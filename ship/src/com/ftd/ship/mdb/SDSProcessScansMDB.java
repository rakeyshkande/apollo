package com.ftd.ship.mdb;

import java.math.BigDecimal;
import java.sql.Connection;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.CarrierScansBO;
import com.ftd.ship.bo.DHLScansBO;
import com.ftd.ship.bo.FedExScansBO;
import com.ftd.ship.bo.UPSScansBO;
import com.ftd.ship.bo.USPSScansBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.ShipDAO;

public class SDSProcessScansMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
//    private CarrierScansBO carrierScansBO;
    private DHLScansBO dhlScansBO;
    private FedExScansBO fedexScansBO;
    private UPSScansBO upsScansBO;
    private USPSScansBO uspsScansBO;
    private ShipDAO shipDAO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.SDSProcessScansMDB");
    
    public SDSProcessScansMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
//        carrierScansBO = (CarrierScansBO)this.getBeanFactory().getBean("carrierScansBO");
        dhlScansBO   =  (DHLScansBO)this.getBeanFactory().getBean("dhlScansBO");
        fedexScansBO =  (FedExScansBO)this.getBeanFactory().getBean("fedexScansBO");
        upsScansBO   =  (UPSScansBO)this.getBeanFactory().getBean("upsScansBO");
        shipDAO      =  (ShipDAO)this.getBeanFactory().getBean("shipDAO");
        uspsScansBO  =  (USPSScansBO)this.getBeanFactory().getBean("uspsScansBO");
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    * 
    * @param message The Message which contains the payload of the JMS message
    */
    public void onMessage(Message message) {
        logger.debug("SDSProcessScansMDB Start: onMessage()");
        
        Connection connection = null;
        String textMessage = null;
        
        try {
            textMessage = StringUtils.trim(((TextMessage)message).getText());
            connection = resourceProvider.getDatabaseConnection();
            
            if( StringUtils.isNotBlank(textMessage) ) {
                String[] args = textMessage.split("\\s");
                
                if( args.length==1 ) {
                    BigDecimal id;
                    try {
                        id = new BigDecimal(args[0]);
                    } catch (NumberFormatException nfe) {
                        throw new SDSApplicationException("Expect a number, but, arguement was "+args[0],nfe);
                    }
                    
                    //Doesn't matter what instance of the derived class is
                    //called since the actual code is in the CarrierScansBO code.
                    fedexScansBO.processScan(connection, id); 
                } else if( args.length==2 ) {
                    String carrierId = args[0];
                    String fileName = args[1];
                    
                    // connection = resourceProvider.getDatabaseConnection(); // Already retrieved a conn above
                    CarrierScansBO carrierScansBO;
                    
                    String scanFormat;
                    
                    try {
                        scanFormat = shipDAO.getCarrierScanFormat(connection,carrierId);
                    } catch (Exception e) {
                        throw new SDSApplicationException("Error while retrieving scan format for carrier "+carrierId,e);
                    }
                    
                    if( StringUtils.isEmpty(scanFormat) ) {
                        throw new SDSApplicationException("Scan processing is not available for carrier "+carrierId);
                    }
                    
                    if( StringUtils.equals(scanFormat,CarrierScansBO.DHL_FORMAT) ) {
                        carrierScansBO = dhlScansBO;
                    } else if( StringUtils.equals(scanFormat,CarrierScansBO.FEDEX_FORMAT) ) {
                        carrierScansBO = fedexScansBO;
                    } else if( StringUtils.equals(scanFormat,CarrierScansBO.UPS_FORMAT) ) {
                        carrierScansBO = upsScansBO;
                    } else if (StringUtils.equals(scanFormat,CarrierScansBO.USPS_FORMAT) ) {
                    	carrierScansBO = uspsScansBO;
                    } else {
                        throw new SDSApplicationException("Carrier "+carrierId+" has an unsupported scan format of "+scanFormat);
                    }
                    
                    carrierScansBO.processScanFile(connection,carrierId,fileName);
                } else {
                    throw new SDSApplicationException("Invalid number of arguements (1 or 2) to SDSProcessScansMDB ("+textMessage+")");
                }
            }
            
        } catch (SDSApplicationException appE) {
            String errMsg = 
                "Error while processing scans for arguements " + textMessage + 
                ".  " +
                appE.getMessage();
            logger.error(errMsg,appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }
        } catch (Throwable t) {
            logger.error("SDSProcessScansMDB failed.",t);
            try {
                CommonUtils.getInstance().sendSystemMessage("SDSProcessScansMDB", "SDSProcessScansMDB failed" + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext MessageDrivenContext) throws EJBException {
        this.context = MessageDrivenContext;
    }
}
