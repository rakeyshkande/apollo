package com.ftd.ship.mdb;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.FtdWestProductUpdatesBO;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;

/**
 * Class 		: FtdWestProductUpdatesMDB.java
 * Description	: This message driven bean reads a message posted to ojms.SHIP_WEST_PRODUCT_UPDATE queue. A scheduler job posts a message to this queue every 5 minutes.
 * 				  It creates an instance of the business object FtdWestProductUpdatesBO and invokes getWestProductUpdates() method of it, to start checking
 * 				  FTD West product availability. 	
 */

public class FtdWestProductUpdatesMDB extends AbstractJmsMessageDrivenBean implements MessageListener 
{ 
	private static Logger logger = new Logger(FtdWestProductUpdatesMDB.class.getName());
		
	protected MessageDrivenContext messageDrivenContext = null;
	private ResourceProviderBase resourceProvider;
	private FtdWestProductUpdatesBO ftdWestProductUpdatesBO;
	
	public void ejbCreate() 
	{
		setBeanFactoryLocatorKey("shipProcessingApp");
	    setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
	    super.ejbCreate();
	}
	
	protected void onEjbCreate() 
	{
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        ftdWestProductUpdatesBO = (FtdWestProductUpdatesBO)this.getBeanFactory().getBean("ftdWestProductUpdatesBO");
    }

	public void onMessage(Message msg) 
	{ 
		logger.debug("FtdWestProductUpdatesMDB.java.onMessage() - Started processing west product updates...");
		
		Connection connection = null;
		String payloadTxt = null;
		String fileName = null;
		
		try 
		{
			payloadTxt = ((TextMessage) msg).getText();
			
			logger.debug("Payload:"+payloadTxt);
		 
			connection = resourceProvider.getDatabaseConnection();
					
			if (connection == null) 
			{
				logger.error("Unable to connect to database");
				throw new Exception("Unable to connect to database");
			}
			
			logger.debug("DBConnection obtained...");
		 	
			// Invoking the method checkWestProductAvailability() in FtdWestProductUpdatesBO.
			ftdWestProductUpdatesBO.checkWestProductAvailability(connection);
		}
		catch (Exception exc) 
		{ 
            //Send out system notification 
            String errMsg = exc.getMessage();
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
           
           // Rollback the transaction mananged by container and do not acknowledge message. VENUS_STATUS stays in 'SHIP'.
           messageDrivenContext.setRollbackOnly();
		}
		finally 
		{
			try 
			{
				if(connection != null && connection.isClosed())
					connection.close();
			}
			catch (SQLException e)
			{
				logger.error("Error closing connection." + e.getMessage());
			}
		}
	}

	@Override
	public void ejbRemove() throws EJBException {}

	@Override
	public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) throws EJBException 
	{
		this.messageDrivenContext = messageDrivenContext;
	}
}
