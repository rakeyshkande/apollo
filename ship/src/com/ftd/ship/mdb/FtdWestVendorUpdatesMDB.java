package com.ftd.ship.mdb;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.FtdWestVendorUpdatesBO;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.ShipDAO;

/**
 * @author vsana
 * Class 		: FtdWestVendorUpdatesMDB
 * Description	: This message driven bean reads a message posted to ojms.SHIP_WEST_VENDOR_UPDATE queue. A scheduler job posts a message to this queue daily.
 * 				  It creates an instance of the business object FtdWestVendorUpdatesBO and invokes getWestVendorUpdates() method of it, to start processing the FTDWest files. 	
 */

public class FtdWestVendorUpdatesMDB extends AbstractJmsMessageDrivenBean implements MessageListener 
{ 
	private static Logger logger = new Logger(FtdWestVendorUpdatesMDB.class.getName());
		
	protected MessageDrivenContext messageDrivenContext = null;
	private ResourceProviderBase resourceProvider;
	private FtdWestVendorUpdatesBO ftdWestVendorUpdatesBO;
	
	public void ejbCreate() 
	{
		setBeanFactoryLocatorKey("shipProcessingApp");
	    setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
	    super.ejbCreate();
	}
	
	protected void onEjbCreate() 
	{
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        ftdWestVendorUpdatesBO = (FtdWestVendorUpdatesBO)this.getBeanFactory().getBean("ftdWestVendorUpdatesBO");
    }

	public void onMessage(Message msg) 
	{ 
		logger.debug("FtdWestVendorUpdatesMDB.onMessage() - Started processing west vendor updates...");
		
		Connection connection = null;
		String payloadTxt = null;
		String fileName = null;
		
		try 
		{
			payloadTxt = ((TextMessage) msg).getText();
			
			logger.debug("Payload:"+payloadTxt);
		 
			connection = resourceProvider.getDatabaseConnection();
					
			if (connection == null) 
			{
				logger.error("Unable to connect to database");
				throw new Exception("Unable to connect to database");
			}
			
			logger.debug("DBConnection obtained...");
		 	
			// Invoking the method getWestVendorUpdates() in FtdWestVendorUpdatesBO.
			ftdWestVendorUpdatesBO.getWestVendorUpdates(connection);
		}
		catch (Throwable e) 
		{ 
			logger.error(e.toString());
			//throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
		}
		finally 
		{
			try 
			{
				if(connection != null && connection.isClosed())
					connection.close();
			}
			catch (SQLException e)
			{
				logger.error("Error closing connection." + e.getMessage());
			}
		}
	}

	@Override
	public void ejbRemove() throws EJBException {}

	@Override
	public void setMessageDrivenContext(MessageDrivenContext messageDrivenContext) throws EJBException 
	{
		this.messageDrivenContext = messageDrivenContext;
	}
}
