package com.ftd.ship.mdb;

import java.sql.Connection;
import java.util.StringTokenizer;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.SDSInboundMsgProcessingBO;
import com.ftd.ship.bo.FtdwestInboundMsgProcessingBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;


public class SDSInboundMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private MessageDrivenContext context;
    private ResourceProviderBase resourceProvider;
    private SDSInboundMsgProcessingBO sdsInProcessingBO;
    private FtdwestInboundMsgProcessingBO ftdwestInProcessingBO;
    private static Logger logger = new Logger("com.ftd.ship.mdb.SDSInboundMDB");
    
    public SDSInboundMDB() {
    }
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        sdsInProcessingBO = (SDSInboundMsgProcessingBO)this.getBeanFactory().getBean("sdsInProcessingBO");
        ftdwestInProcessingBO = (FtdwestInboundMsgProcessingBO)this.getBeanFactory().getBean("ftdwestInProcessingBO");
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    * 
    * @param message The Message which contains the payload of the JMS message
    */
    public void onMessage(Message message) {
        logger.debug("SDSInboundMDB Start: onMessage()");
        
        Connection connection = null;
        String msgId = null;
        
        long orderStatusId=0;
        boolean isWestOrderStatus = false;
        
        try {
            msgId = ((TextMessage)message).getText();
            connection = resourceProvider.getDatabaseConnection();
            
			if (msgId.contains("|")) {
				StringTokenizer tokenizer = new StringTokenizer(msgId, "|");
				// msg format = order status id|WEST
				orderStatusId = Long.parseLong(tokenizer.nextToken());
				String shippingSystem = tokenizer.nextToken();
				isWestOrderStatus = (shippingSystem != null && shippingSystem.trim().length() > 0 && shippingSystem.equals("WEST"));
			}
			
			if (isWestOrderStatus) {
				ftdwestInProcessingBO.processMessage(connection, orderStatusId);
			}
			else {
				sdsInProcessingBO.processMessage(connection, msgId);
			}			
        } catch (SDSApplicationException appE) {
            String errMsg = null;
            if(isWestOrderStatus)
            {
               errMsg =  "Error encountered while processing Ftd West order_status_id: " + msgId + 
                ".  Manual intervention is required.";      	
            }
            else
            {
            	errMsg =  "Error from Argo while processing sds_status_message id: " + msgId + 
                ".  Manual intervention is required.";
            }
            logger.error(errMsg,appE);

            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
                throw new EJBException(errMsg,e);
            }  
        } catch (Throwable t) {
            logger.error("SDSInboundMDB failed.",t);
            try {
                CommonUtils.getInstance().sendSystemMessage("SDSInboundMDB", "SDSInboundMDB failed" + t.getMessage());
            } catch (Exception e) {
                String errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
        } finally {
            try 
            {
                if(connection!=null) {
                    connection.close();
                }
            }
            catch (Exception e)
            {
                logger.warn(e);
            }
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext MessageDrivenContext) throws EJBException {
        this.context = MessageDrivenContext;
    }
}
