package com.ftd.ship.mdb;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.ship.bo.ReviewRejectBO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.bo.communications.sds.SDSRuntimeException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.dao.ShipDAO;

import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import javax.ejb.EJBException;

import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;

import javax.jms.TextMessage;

import org.apache.commons.lang.StringUtils;

import org.springframework.context.access.ContextSingletonBeanFactoryLocator;
import org.springframework.ejb.support.AbstractJmsMessageDrivenBean;


//RLSE_2_4_0 new class
/**
 * The purpose of this class is to proved a JMS receiver for the OC4J.PI_REJECT_REVIEW
 * queue.
 */
public class RejectReviewMDB extends AbstractJmsMessageDrivenBean implements MessageListener {
    private static Logger logger = 
        new Logger("com.ftd.ship.mdb.RejectReviewMDB");
    private ResourceProviderBase resourceProvider;
    private ReviewRejectBO reviewRejectBO;
    private ShipDAO shipDAO;
    protected MessageDrivenContext messageDrivenContext;
    
    public void ejbCreate() {
        setBeanFactoryLocatorKey("shipProcessingApp");
        setBeanFactoryLocator(ContextSingletonBeanFactoryLocator.getInstance());
        super.ejbCreate();
    }

    protected void onEjbCreate() {
        resourceProvider = (ResourceProviderBase)this.getBeanFactory().getBean("resourceProvider");
        reviewRejectBO = (ReviewRejectBO)this.getBeanFactory().getBean("reviewRejectBO");
        shipDAO = (ShipDAO)this.getBeanFactory().getBean("shipDAO");
    }
    
    public void setMessageDrivenContext(MessageDrivenContext ctx)
    {
      this.messageDrivenContext = ctx;
    }

    /**
    * This is the onMessage() which execute when the message is consumed
    *
    * @param msg The Message which contains the payload of the JMS message
    */
    public void onMessage(Message msg) {
        logger.debug("RejectReviewMDB Start: onMessage()");

        Connection connection = null;
        String venusId = null;
        VenusMessageVO vo = null;
        
        try{
            try {
                venusId = StringUtils.trim(((TextMessage)msg).getText());
    
                if (StringUtils.isBlank(venusId)) {
                    assert (false) : "Ship processing has received an empty payload";
                    //Send out system notification 
                    try {
                        CommonUtils.getInstance().sendSystemMessage("Ship processing has received an empty payload.");
                    } catch (Exception e) {
                        logger.error("Unable to send message to support pager.");
                        logger.fatal(e);
                    }
                    return;
                }
    
                connection = resourceProvider.getDatabaseConnection();
                
                vo = shipDAO.getVenusMessage(connection, venusId);
                
                reviewRejectBO.processRequest(connection,vo);
                
            } catch (SDSApplicationException appE) {
                String errMsg = 
                    "Error from Argo while processing venus id: " + venusId + 
                    ".  Manual intervention is required.";
                logger.error(errMsg);
                logger.error(appE);
    
                throw new Exception(errMsg);
            } catch (SDSRuntimeException rte) {
                //Throw SDSRuntimeException exceptions back up to the container
                String errMsg = 
                     "SDSRuntimeException received while processing venus id: " + venusId + 
                     ".  Manual intervention is required.";
                logger.error(errMsg);
                logger.error(rte);
                
                throw new Exception(errMsg);
            } catch (Exception e) {
                if( !(e instanceof RuntimeException) )
                assert (false) : "All exceptions thrown should be either SDSCommunicationsException or SDSApplicationException";
                String errMsg = 
                    "Exception received while processing venus id: " + venusId + 
                    ".  Manual intervention is required.";
                logger.error(errMsg);
                logger.error(e);
                
                throw new Exception(errMsg);
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (Exception e) {
                    logger.warn(e);
                }
            }
        } catch (Exception ex) {
            String errMsg = ex.getMessage();
            //Send out system notification 
            try {
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            } catch (Exception e) {
                errMsg = "Unable to send message to support pager.";
                logger.fatal(errMsg,e);
            }
            messageDrivenContext.setRollbackOnly();
        }

        logger.debug("End: onMessage()");
        if (messageDrivenContext.getRollbackOnly())
        	throw new RuntimeException("Rollback detected, exception thrown to force rollback.");
    }
}
