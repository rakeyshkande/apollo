package com.ftd.ship.common;

public enum FtdwestStatusType {
	
	RELEASED("Released", "REJECTED" ),
	IMPORTED("Imported", "REJECTED" ),
	ERRORED("Errored", "REJECTED" ), 
	NEW("New", "REJECTED" ),
	BACKORDER("BackOrder", "REJECTED" ), 
	CANCEL("Cancel", "REJECTED" ),
	CANCELED("Canceled", "REJECTED" ),
	PRINTED("Printed", "PRINTED" ),
	PACKED("Packed", "PRINTED" ),
	SHIPCOMPLETE("ShipComplete", "SHIPPED" ),
	COMPLETE("Complete", "DELIVERED" );
	

	private String status;
	private String ftdStatus;

	
	/**
	 * @return the ftdStatus
	 */
	public String getFtdStatus() {
		return ftdStatus;
	}


	/**
	 * @param ftdStatus the ftdStatus to set
	 */
	public void setFtdStatus(String ftdStatus) {
		this.ftdStatus = ftdStatus;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	private FtdwestStatusType(String status, String ftdStatus) {
		this.status = status;
		this.ftdStatus = ftdStatus;
	}

	public static boolean isRejected(String lineItemStatus) {		
		return (
			RELEASED.getStatus().equalsIgnoreCase(lineItemStatus) || 
			IMPORTED.getStatus().equalsIgnoreCase(lineItemStatus) ||
			ERRORED.getStatus().equalsIgnoreCase(lineItemStatus) ||
			NEW.getStatus().equalsIgnoreCase(lineItemStatus) ||
			BACKORDER.getStatus().equalsIgnoreCase(lineItemStatus) ||
			CANCEL.getStatus().equalsIgnoreCase(lineItemStatus) ||
			CANCELED.getStatus().equalsIgnoreCase(lineItemStatus)
		);
		
		/*
		 * In above conditions, added condition for Canceled along with Cancel also, as the FRD says 'Cancel' but SkyNET doc shows sample as 'Canceled'
		 */
	}

	public static boolean isPrinted(String lineItemStatus) {
		return (
			PRINTED.getStatus().equalsIgnoreCase(lineItemStatus) ||
			PACKED.getStatus().equalsIgnoreCase(lineItemStatus) 
		);
	}


	public static boolean isShipped(String lineItemStatus) {
		return SHIPCOMPLETE.getStatus().equalsIgnoreCase(lineItemStatus);
	}

	public static boolean isDelivered(String lineItemStatus) {
		return COMPLETE.getStatus().equalsIgnoreCase(lineItemStatus);
	}
	
	public static boolean isCanceled(String lineItemStatus) {
		return (
			CANCEL.getStatus().equalsIgnoreCase(lineItemStatus) ||
			CANCELED.getStatus().equalsIgnoreCase(lineItemStatus) 
		);
	}
	
	public static String getFtdStatusForWestStatus(String status) {
		String matchingFtdStatus = null;
		for (FtdwestStatusType statusType : FtdwestStatusType.values()) {
			if (statusType.getStatus().equalsIgnoreCase(status)) {
				matchingFtdStatus = statusType.getFtdStatus();
				break;
			}
		}
		return matchingFtdStatus;
	}
	
	public static MsgType getMsgTypeFromWestStatus(String status) {
		String matchedFtdWestStatus = null;
		MsgType matchedMsgType = null;
		for (FtdwestStatusType statusType : FtdwestStatusType.values()) {
			if (statusType.getStatus().equalsIgnoreCase(status)) {
				matchedFtdWestStatus = statusType.getFtdStatus();
				break;
			}
		}
		if (matchedFtdWestStatus != null) {
			for (MsgType msgType : MsgType.values()) {
				if (msgType.getFtdWestMsgType().equalsIgnoreCase(matchedFtdWestStatus)) {
					matchedMsgType = msgType;
					break;
				}
			}
		}
		return matchedMsgType;
	}
	
}
