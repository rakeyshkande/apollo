package com.ftd.ship.common;

/**
 * @author kkeyan
 *
 */
public class FtdwestConstants {
	
	public static final int FIRST_NAME_LENGTH=20;
	public static final int LAST_NAME_LENGTH=40;
	public static final int CARD_MESSAGE_LENGTH_NON_PC=240;
	public static final int CARD_MESSAGE_LENGTH_PC=103;
	public static final int FLD_LEN_40=40;
	
	public static final int FIRST_NAME_INDX=0;
	public static final int LAST_NAME_INDX=1;
	public static final int NUMBER_OF_QNTY=1;
	
	
	
	public static final String CITY_AND_NAME_FILTER_PATTERN="[^\\x20-\\x7E]";
	public static final String MESSAGE_TEXT_FILTER_PATTERN="[^\\x20-\\x5D\\x5F-\\x7B\\x7D]";
	public static final String LOC_TYPE_BUSINESS = "Business";
	public static final String LOC_TYPE_RESIDENTIAL = "Residential";
	
	public static final String GLOBAL_PARMS_FTDWEST_CUSTOMER_ID = "FTDWEST_CUSTOMER_ID";
	public static final String GLOBAL_PARMS_FTDWEST_SERVICE_URL = "FTDWEST_SERVICE_URL";
	
	public static final String SECURE_PARMS_FTDWEST_APP_TOKEN = "FTDWEST_APP_TOKEN";
	public static final String SECURE_PARMS_FTDWEST_LOGIN = "FTDWEST_LOGIN";
	public static final String SECURE_PARMS_FTDWEST_PASSWORD = "FTDWEST_PASSWORD";
	public static final String SECURE_PARMS_APPLICATION_CONTEXT = "ship";
	
	public static final String ORDER_DELIVERY_MORNING = "Morning";
	public static final String ORDER_DELIVERY_MORNING_FLAG_Y = "Y";
	public static final String ORDER_DELIVERY_AFTERNOON = "Afternoon";
	
	public static final String FTDWEST_CREATEORDER_SUCCESS = "Success";
	public static final String FTDWEST_CANCEL_ORDER_REASON_CD = "215";	
	public static final String FTDWEST_SERVICE_TYPE_DELIVERYDATE= "DeliveryDate";
	public static final String FTDWEST_SERVICE_TYPE_SERVICELELVEL= "ServiceLevel";
	public static final String FTDWEST_SERVICE_CAL_TYPE = "Ground";
	
	public static final String FTDWEST_GETPRODUCTDETAILS_SUCCESS = "Success";
	
	
	

}
