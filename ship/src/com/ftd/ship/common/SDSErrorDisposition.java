package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum SDSErrorDisposition { 
  REPROCESS, 
  QUEUE, 
  CANCEL_ORDER, 
  INCREMENT_SHIP_DATE;
  
  public static SDSErrorDisposition toErrorDisposition(String dispCode) {
      if( StringUtils.equalsIgnoreCase(REPROCESS.toString(),dispCode)) {
          return REPROCESS;
      } else if( StringUtils.equalsIgnoreCase(QUEUE.toString(),dispCode)) {
          return QUEUE;
      } else if( StringUtils.equalsIgnoreCase(CANCEL_ORDER.toString(),dispCode)) {
          return CANCEL_ORDER;
      } else if( StringUtils.equalsIgnoreCase(INCREMENT_SHIP_DATE.toString(),dispCode)) {
          return INCREMENT_SHIP_DATE;
      }
      
      return null;
  }
}
