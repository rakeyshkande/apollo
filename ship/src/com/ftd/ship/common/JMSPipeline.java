package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum JMSPipeline {
    SHIPPROCESSSHIP("OJMS.SHIP_SHIP_PROCESSING"),
    SHIPGETSTATUS("OJMS.SHIP_SDS_GET_STATUS"),
    SHIPPROCESSINBOUND("OJMS.SHIP_SDS_INBOUND_PROCESS"),
    SHIPMANIFEST("OJMS.SHIP_SDS_MANIFEST"),
    GETSCANS("OJMS.SDS_GET_SCANS"),
    PROCESSSCANS("OJMS.SDS_PROCESS_SCANS"),
    DELIVERYCONFIRM("OJMS.SDS_DELIVERY_CONFIRM"),
    SHIPREJECTREVIEW("OJMS.SHIP_SDS_REVIEW_REJECT"),
    REFUND("OJMS.EM_COM"),
    MY_BUYS("OJMS.MY_BUYS"),
    GETCARRIERZIPS("OJMS.CARRIER_GET_ZIPS"),
    PROCESSCARRIERZIPS("OJMS.CARRIER_PROCESS_ZIPS");

    private String tableName;

    private JMSPipeline(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    public static JMSPipeline toJMSPipeline(String strValue) {
        if( StringUtils.equals(SHIPPROCESSSHIP.toString(),strValue)) {
            return SHIPPROCESSSHIP;
        }
        else if( StringUtils.equals(SHIPGETSTATUS.toString(),strValue)) {
            return SHIPGETSTATUS;
        }
        else if( StringUtils.equals(SHIPPROCESSINBOUND.toString(),strValue)) {
            return SHIPPROCESSINBOUND;
        }
        else if( StringUtils.equals(SHIPMANIFEST.toString(),strValue)) {
            return SHIPMANIFEST;
        }
        else if( StringUtils.equals(GETSCANS.toString(),strValue)) {
            return GETSCANS;
        }
        else if( StringUtils.equals(PROCESSSCANS.toString(),strValue)) {
            return PROCESSSCANS;
        }
        else if( StringUtils.equals(DELIVERYCONFIRM.toString(),strValue)) {
            return DELIVERYCONFIRM;
        }
        else if( StringUtils.equals(SHIPREJECTREVIEW.toString(),strValue)) {
            return SHIPREJECTREVIEW;
        }
        else if( StringUtils.equals(REFUND.toString(),strValue)) {
            return REFUND;
        }
        else if( StringUtils.equals(MY_BUYS.toString(),strValue)) {
            return MY_BUYS;
        }
        else if( StringUtils.equals(GETCARRIERZIPS.toString(),strValue)) {
            return GETCARRIERZIPS;
        }
        else if( StringUtils.equals(PROCESSCARRIERZIPS.toString(),strValue)) {
            return PROCESSCARRIERZIPS;
        }

        return null;
    }
}
