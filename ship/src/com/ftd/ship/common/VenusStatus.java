package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum VenusStatus { 
    OPEN,SHIP,SHIP_PROCESSING,PENDING,VERIFIED,ERROR;  
    
    public static VenusStatus toVenusStatus(String venusStatus) {
        if( StringUtils.equals(OPEN.toString(),venusStatus) ) {
            return OPEN;    
        }
        else if( StringUtils.equals(SHIP.toString(),venusStatus) ) {
            return SHIP;    
        }
        else if( StringUtils.equals(SHIP_PROCESSING.toString(),venusStatus) ) {
            return SHIP_PROCESSING;    
        }
        else if( StringUtils.equals(PENDING.toString(),venusStatus) ) {
            return PENDING;    
        }
        else if( StringUtils.equals(VERIFIED.toString(),venusStatus) ) {
            return VERIFIED;    
        }
        else if( StringUtils.equals(ERROR.toString(),venusStatus) ) {
            return ERROR;    
        }
        
        return OPEN;
    }
    
    public boolean equals(VenusStatus checkStatus ) {
        if( checkStatus==null ) {
            return false;
        }
        
        return StringUtils.equals(this.toString(),checkStatus.toString());
    }
}
