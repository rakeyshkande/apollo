package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum ShippingSystem {
    ESCALATE("ESCALATE"),FTP("FTP"),SDS("SDS"),FTD_WEST("FTD WEST");
	
	String value; 
	
	private ShippingSystem(String shippingSys){
		value=shippingSys;
		
	}
    
    public static ShippingSystem toShippingSystem(String shippingSystem) {
        if( StringUtils.equals(ESCALATE.toString(),shippingSystem)) {
            return ESCALATE;
        }
        else if( StringUtils.equals(FTP.toString(),shippingSystem)) {
            return FTP;
        }
        else if( StringUtils.equals(SDS.toString(),shippingSystem)) {
            return SDS;
        }
        else if( StringUtils.equals(FTD_WEST.value,shippingSystem)) {
            return FTD_WEST;
        }
        
        return null;
    }
    
    public String getValue(){
    	return value;
    }
    
    public static void main(String[] args) {
        for (ShippingSystem cName : ShippingSystem.values()) {
               System.out.println("TestEnumString Value: " + cName.getValue() + " - TestEnumString Name: " + cName.toString());
        }
 }


}
