package com.ftd.ship.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.communications.ftdwest.FtdwestApplicationException;
import com.ftd.ship.vo.FtdwestPersonalizationVO;
import com.providecommerce.api.api.common.v1.FaultDetail;
import com.providecommerce.api.api.order.v1.CancelOrderRequest;
import com.providecommerce.api.api.order.v1.CancelOrderResult;
import com.providecommerce.api.api.order.v1.CreateOrderRequest;
import com.providecommerce.api.api.order.v1.CreateOrderResponse;
import com.providecommerce.api.api.order.v1.CreateOrderResult;
import com.providecommerce.api.api.order.v1.OrderServiceCancelOrderFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.order.v1.OrderServiceCreateOrderFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.product.v1.GetProductDetails;
import com.providecommerce.api.api.product.v1.GetProductDetailsResponse;
import com.providecommerce.api.api.product.v1.GetProductDetailsResult;
import com.providecommerce.api.api.product.v1.ProductServiceGetProductDetailFaultDetailFaultFaultMessage;

/**
 * @author kkeyan
 *
 */
public class FtdwestUtils {
	
	private static final String COMMA_DELIMITER = ",";
	private static final String BLANK_SPACE = " ";
	private static final String PERSONALIZATION_TAG_DATA = "data";
	private static final String PERSONALIZATION_TAG_NAME = "name";
	private static final String PERSONALIZATION_TAG_ROOT = "personalization";
	
   private static Logger logger = new Logger("com.ftd.ship.common.FtdwestUtils");
	
	 private static String filterHexChars(String stringToParse, String pattern){
		  String resultString = StringEscapeUtils.unescapeHtml(stringToParse).replaceAll(pattern, "");//pattern ="^[\\x20-\\x7E\\s]+$" non printable chars..
		  return StringEscapeUtils.unescapeHtml(stringToParse).replaceAll(pattern, "");//pattern ="^[\\x20-\\x7E\\s]+$" non printable chars..;
	 }
	 
	 private static String truncateStringToLength(String stringToTrunc, int toLength){
		 String result="";
		 if(stringToTrunc!=null && stringToTrunc.length() >= toLength){			 
			 result=stringToTrunc.substring(0, toLength);
			 logger.info("Truncating name for FTD west:"+stringToTrunc+", to :"+result);
		 }else{
			 result=stringToTrunc;
		 }
		 return result;
	 }
	 
	 public static String filterFieldForFtdwest(String stringToFilter, String filterPattern, int stringLen){
		 
		 if(stringToFilter==null){
			 return stringToFilter;
		 }
		 
		 String parsedAndTruncString="";
				 parsedAndTruncString= truncateStringToLength(filterHexChars(stringToFilter,filterPattern),stringLen);
		return parsedAndTruncString;
	 }
	 
	 
	 
	 public static String[] getFtdWestFirstAndLastNames(String fullName) throws FtdwestApplicationException{
		 
		 
		 if(fullName==null || fullName.length()<=0){
			 throw new FtdwestApplicationException("Name field is empty to create ftdwest request !!");
		 }
		
		 
		 String names[]=new String[2];
		 int fullNameLength=0;
		 String firstName="";
		 String lastName="";
		 
		 String formattedFullName=filterFieldForFtdwest(fullName,FtdwestConstants.CITY_AND_NAME_FILTER_PATTERN,60);		 
		 fullNameLength=formattedFullName.length();
		 
		 
		 if(fullNameLength >FtdwestConstants.FIRST_NAME_LENGTH){
			
			 firstName=formattedFullName.substring(0, FtdwestConstants.FIRST_NAME_LENGTH);			 
			 lastName=formattedFullName.substring(firstName.length());			 
			 
			 logger.debug("firstname:"+firstName);
			 logger.debug("lastName:"+lastName);
			 
			 if(firstName.indexOf(BLANK_SPACE) > 0 && (lastName.length()+firstName.substring(firstName.lastIndexOf(BLANK_SPACE)).length() <=40)){
				 
				 firstName=firstName.substring(0,firstName.lastIndexOf(BLANK_SPACE));
				 
				 logger.debug("after space, firstname:"+firstName);
				 
				 lastName=formattedFullName.substring(firstName.length());
				 logger.debug("after space, lastName:"+lastName);
			 }
		 }else{
			 
			 if(formattedFullName.indexOf(BLANK_SPACE) >0){
				 firstName=formattedFullName.substring(0, formattedFullName.lastIndexOf(BLANK_SPACE));
				 lastName=formattedFullName=formattedFullName.substring(firstName.length());
			 }else{
				 firstName=formattedFullName;
				 lastName=formattedFullName;
			 }
		 }
		 
		 names[FtdwestConstants.FIRST_NAME_INDX]=firstName.trim();
		 names[FtdwestConstants.LAST_NAME_INDX]=lastName.trim();
		 
		 logger.debug("First name==>"+names[FtdwestConstants.FIRST_NAME_INDX] +":length==>"+names[FtdwestConstants.FIRST_NAME_INDX].length());
		 logger.debug("Last name==>"+ names[FtdwestConstants.LAST_NAME_INDX]+":length==>"+names[FtdwestConstants.LAST_NAME_INDX].length());
		 
		 return names;
	 }
	 

	 public static String getFtdwestString(Object req) {
		 
			
		 String rootElement="info";
		 
		 if( req instanceof CreateOrderRequest)
			 rootElement="CreateOrderRequest";
		 else if( req instanceof CreateOrderResponse)
			 rootElement="CreateOrderResponse";
		 else if( req instanceof OrderServiceCreateOrderFaultDetailFaultFaultMessage ||
				 req instanceof OrderServiceCancelOrderFaultDetailFaultFaultMessage)
			 rootElement="FaultMessage";
		 else if( req instanceof CreateOrderResult)
			 rootElement="CreateOrderResult";
		 else if(req instanceof CancelOrderRequest)
			 rootElement="CancelOrderRequest";
		 else if(req instanceof CancelOrderResult)
			 rootElement="CancelOrderResult";
		 else if(req instanceof FaultDetail)
			 rootElement="FaultMessage";
		 else if(req instanceof GetProductDetails)
			 rootElement="GetProductDetails";
		 else if(req instanceof GetProductDetailsResponse)
			 rootElement="GetProductDetailsResponse";
		 
		
		 String output = 	 rootElement;
		 
		 JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(req.getClass());
		
		    Marshaller m = jc.createMarshaller();
		    m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");
		    JAXBElement jx = new JAXBElement(new QName(rootElement), req.getClass(), req);
		    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		    m.marshal(jx, outputStream);
		     output = new String(outputStream.toByteArray());
		} catch (JAXBException e) {
			logger.error("Error creating Ftdwest Transaction message for log !!");
			 return output;
		}
			 
		return output;
		
		
	 }


	 /**
	  * This method is essentially depricated since we are now (since Nov 2017) using PDP (West) style personalizations.
	  * 
	  * @param personalizationStr
	  * @return
	  */
	 public static HashMap<String,String> getPersonalizationMap(String personalizationStr){
		 HashMap<String,String> personalizationMap=new HashMap<String,String>();
		 try {
			 DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			 InputSource is=new InputSource(new StringReader(personalizationStr));
			 Document doc;
				doc = builder.parse(is);
			 doc.getDocumentElement().normalize();			 
			 NodeList nList = doc.getElementsByTagName(PERSONALIZATION_TAG_ROOT);
			 logger.debug("Personalization message length :" + nList.getLength());
	
			 for (int temp = 0; temp < nList.getLength(); temp++) {
			     Element element = (Element) nList.item(temp);
			     
			             NodeList name = element.getElementsByTagName(PERSONALIZATION_TAG_NAME);
			             Element line = (Element) name.item(0);
			             String key=getCharacterDataFromElement(line);
			             logger.debug("Name: " + key);
			             
			              name = element.getElementsByTagName(PERSONALIZATION_TAG_DATA);
			              line = (Element) name.item(0);
			              String value=getCharacterDataFromElement(line);
			             logger.debug("value: " + value);
			             
			             personalizationMap.put(key, value);
			             
			     }
		 	} catch (SAXException e) {
		 		logger.error("Error(SAXException) parsing personalization info !!"+e.getMessage());
			} catch (IOException e) {
				logger.error("Error(IOException) parsing personalization info !!"+e.getMessage());
			} catch (ParserConfigurationException e) {
				logger.error("Error(ParserConfigurationException) parsing personalization info !!"+e.getMessage());
			}
		 return personalizationMap;
	}
	 
	 
	 public static HashMap<String,String> getApolloToPQuadMap(FtdwestPersonalizationVO pesronalizationVO) throws FtdwestApplicationException{
		 HashMap<String,String> personalizationMap=new HashMap<String,String>();
		 try {
			 String displayNameStr=pesronalizationVO.getDisplayNames();
			 String templateOrderStr=pesronalizationVO.getTemplateOrder();
			 
			 if(displayNameStr==null || displayNameStr.trim().length() ==0 ||
					 templateOrderStr==null || templateOrderStr.trim().length() ==0	 ){
				 throw new FtdwestApplicationException("Invalid Personalization info while mapping AppoloToPQuadMap ");
			 }
			 
			 
			 String displayNames[]=displayNameStr.split(COMMA_DELIMITER);
			 String templateOrder[]=templateOrderStr.split(COMMA_DELIMITER);
	
			 for (int i = 0; i < templateOrder.length; i++) {		
			             personalizationMap.put(templateOrder[i], displayNames[i]);
			     }
		 	} catch (StringIndexOutOfBoundsException e) {
		 		logger.error("Error(StringIndexOutOfBoundsException) getting apolloToPQuadMap !!"+e.getMessage());
		 		throw new FtdwestApplicationException("Error(StringIndexOutOfBoundsException) getting apolloToPQuadMap !!");
			}catch(FtdwestApplicationException e){
				logger.error("Error(StringIndexOutOfBoundsException) getting apolloToPQuadMap !!"+e.getMessage());
				throw e;
			}catch(Exception e){			
				logger.error("Error(Exception) getting apolloToPQuadMap !!"+e.getMessage());
				throw new FtdwestApplicationException("Error(Exception) getting apolloToPQuadMap !!"+e.getMessage());
			}
		 return personalizationMap;
	} 
	 
	 private static String getCharacterDataFromElement(Element f) {

         NodeList list = f.getChildNodes();
         String data;

         for(int index = 0; index < list.getLength(); index++){
             if(list.item(index) instanceof CharacterData){
                 CharacterData child  = (CharacterData) list.item(index);
                 data = child.getData();

                 if(data != null && data.trim().length() > 0)
                    return child.getData();
             }
         }
         return "";
}
	 
//	 public static void main(String a[]) throws FtdwestApplicationException{
//// String txttoParse="SDFSDFQEQ#$@#$@$@#$@#RFDC$%#$^@$%V&#$^& ~~~B#axasdasdas$VU%^*%^&*%^&%^&%^&%^&%^&%^& B#$Y^V#E DHE EFGDFGDF SDFDF CDFCS 1231232zxcfsdf";	 
//// 
//// String str = "Thè quïck brøwn føx jumps over the lãzy dôg.";
//// String pattern="[^\\x20-\\x7E]";//[^\\x20-\\x7E]";// "";//[^\\x20-\\x7e]
//// String pattern1="[^\\x20-\\x5D\\x5F-\\x7B\\x7D]";
//// logger.debug("Before parse :"+str);
//// logger.debug("After parse  :"+filterFieldForFtdwest(str, pattern1,100));
//// getFtdWestFirstAndLastNames("To all the nurses and Staff of Dr Gouge Office test1 test2 test3 test4");
//// getFtdWestFirstAndLastNames("Thè quïck brøwn føx jumps over the lãzy dôg.");
//		 
//		 String a1="hello";
//		 String ar[]=a1.split(",");
//		 
//		 System.out.print(ar.length);
//		 
//		 
//		 
//				 
//}
// 
	 
}