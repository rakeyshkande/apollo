package com.ftd.ship.common;

import java.io.BufferedInputStream;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ftd.osp.utilities.plugins.Logger;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


/**
 * @author vsana
 * Class : SftpUtil
 * This utility class includes all the logic to:
 * 		1. Download the file(s) from FTP server via SSH (public - private key pair).
 * 		2. Archive the file(s) to a server.
 * 		3. Removes the file(s) from FTP server.
 */
public class SftpUtil  
{

	private static Logger logger = new Logger(SftpUtil.class.getName());

	public SftpUtil() 
	{
		super();
	}
	
	/**
     * Method 		: downloadFilestoLocalviaSSH()
	 * Description	: This method has the logic to:
	 * 					1. Connect to a SFTP server to authenticate using a private key,
	 * 					2. Navigate to a remote directory
	 * 					3. Read through the file names in the directory and download the files whose names match the expected pattern, saves in a local directory.
	 */
	public Map downloadFilestoLocalviaSSH(String filePattern, String remoteFilePath, String localFilePath, String ftpServer, String username, String privatekey, String passphrase) throws Exception 
	{
		logger.debug("In SftpUtil.downloadFilestoLocalviaSSH() method...");
		
		boolean fileFound = false;
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;
		Map mFiles = new HashMap();
		List<String> lstRemoteFileNames = new ArrayList<String>();
		List<String> lstLocalFileAbsPath = new ArrayList<String>();
		
		try 
		{
			logger.debug("Connecting to SFTP server:"+ftpServer);
			
			JSch jsch = new JSch();
			jsch.addIdentity(privatekey, passphrase);
			session = jsch.getSession(username, ftpServer);
			
			logger.debug("Created a new session...");
			
	        java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			
			logger.debug("Opened a channel from session and connected to it...");
			
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);
			
			logger.debug("Connected to FTP server:"+ftpServer+". Looking for the files in directory:"+remoteFilePath);
			
			Vector<LsEntry> v = channelSftp.ls(remoteFilePath);

			if (v != null && v.size() > 0) 
			{
				logger.debug("Reading files from remote directory: "+remoteFilePath);
				
				// Loop through the files from remote directory and check if the file names match with expected pattern.
				for (int i = 0; i < v.size(); i++) 
				{
					remoteFileName = v.get(i).getFilename();
					logger.debug("Remote file name : " + remoteFileName);
					
					if(remoteFileName != null)
					{
						try
						{
							// Check if the filename matches with the pattern.
							Pattern p = Pattern.compile(filePattern);
							Matcher m = p.matcher(remoteFileName);
							fileFound = m.find();
						}
						catch(Exception e)
						{
							fileFound = false;
						}
						
						// Download the file only if it matches with the expected pattern.
						if(fileFound)
						{
							BufferedInputStream bis = new BufferedInputStream(channelSftp.get(remoteFileName));
							newFile = new File(localFilePath, remoteFileName);
							lstLocalFileAbsPath.add(newFile.getAbsolutePath());
							lstRemoteFileNames.add(remoteFileName);
							OutputStream os = new FileOutputStream(newFile);
							BufferedOutputStream bos = new BufferedOutputStream(os);
							byte[] buffer = new byte[1024];
							int readCount;
							while ((readCount = bis.read(buffer)) > 0) 
							{
								bos.write(buffer, 0, readCount);
							}
							bis.close();
							bos.close();
						}
					}
				}
				logger.debug("Read remote files completed...");
			}
			else 
			{
				// If no files are found in the directory, throw an Exception and a PAGE alert will be sent in the calling method.
				logger.debug("No files found on server");
				throw new Exception("No files found on FTP server");
			}
		} 
		catch (Exception ex) 
		{
			// If any exception occurs while connecting to SFTP server / downloading the files, throw an Exception and a PAGE alert will be sent in the calling method.
			logger.error("Exception occurred while downloading FTDWest vendor updates files..."+ex.getMessage());
			throw ex;
		}
		finally
		{
			//channelSftp.disconnect();
			if(channel != null)
				channel.disconnect();
			if(session != null)
				session.disconnect();
		}
		
		mFiles.put("remoteFileNames", lstRemoteFileNames);
		mFiles.put("localFilesAbsPaths", lstLocalFileAbsPath);
		
		return mFiles;
	}
	// End of method downloadFilestoLocalviaSSH().
	
	
	/**
     * Method 		: archiveFiles()
	 * Description	: This method SFTPs the files provided in the list to a configured apollo server.
	 */
	public void archiveFiles(List<File> files, String remoteFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;

		try
		{
				logger.debug("Connecting to internal Apollo server:"+ftpServer);
				
				JSch jsch = new JSch();
				session = jsch.getSession(username,ftpServer);
				session.setPassword(password);
				
				logger.debug("Created a new session...");
				
				java.util.Properties config = new java.util.Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();
				
				logger.debug("Opened a channel from session and connected to it...");
				
				channelSftp = (ChannelSftp)channel;		
				channelSftp.cd(remoteFilePath);
				
				logger.debug("Connected to the server:"+ftpServer+". Archiving the files to directory:"+remoteFilePath);
				
				for(File file : files)
				{
					channelSftp.put(new FileInputStream(file), file.getName());
					logger.debug("File "+file.getName()+" archived...");
				}
		}
		finally
		{
			// Close the connected channel and session, before returning to calling method.
			if(channelSftp != null && channelSftp.isConnected())
				channel.disconnect();
			
			if(channel != null && channel.isConnected())
				channel.disconnect();
			
			if(session != null && session.isConnected())
				session.disconnect();
		}
	}
	// End of method archiveFiles().

	
	/**
     * Method 		: deleteRemoteFiles()
	 * Description	: This method connects to the SFTP server, navigates to the remote directory and removes the files provided in the list from the directory.
	 */
	public void deleteRemoteFiles(List<String> lstRemoteFiles, String ftpServer, String remoteFilePath, String username, String privatekey, String passphrase) throws Exception
	{
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		File newFile = null;
		String remoteFileName = null;

		try 
		{
			logger.debug("Connecting to SFTP server:"+ftpServer);
			
			JSch jsch = new JSch();
			jsch.addIdentity(privatekey, passphrase);
			session = jsch.getSession(username, ftpServer);
			
			logger.debug("Created a new session");
			
	        java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			
			logger.debug("Opened a channel from session and connected to it...");
			
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteFilePath);
			
			logger.debug("Connected to FTP server:"+ftpServer+". Navigated to the directory:"+remoteFilePath);
			
			for(String s:lstRemoteFiles)
			{	
				channelSftp.rm(s);
				logger.debug("Deleted file "+s+" from FTP server.");
			}	
		}
		finally
		{
			//channelSftp.disconnect();
			if(channel != null)
				channel.disconnect();
			if(session != null)
				session.disconnect();
		}
	}
	// End of method deleteRemoteFiles().
	
}



