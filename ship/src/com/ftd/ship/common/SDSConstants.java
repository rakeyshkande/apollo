package com.ftd.ship.common;

public class SDSConstants {
    
    /************************************************************************************
     * RateShipUnits and CreateShipUnits
     ************************************************************************************/

    /** RateShipUnits only */
    public static final String TAG_RATE_EXPECTED_DELIVERY_DATE = "ExpectedDeliveryDate";
    public static final String TAG_RATE_FREIGHT_CHARGE = "FreightCharge";
    public static final String TAG_RATE_HANDLING_CHARGE = "HandlingCharge";
    public static final String TAG_RATE_RATE_CHARGE = "RateCharge";
    public static final String TAG_RATE_RATE_WEIGHT = "RateWeight";
    public static final String TAG_RATE_TOTAL_CHARGE = "TotalCharge";
    public static final String TAG_RATE_ZONE_CODE = "ZoneCode";
    public static final String TAG_RATE_ZONEJUMP_DEFAULT_DIST_CENTER = "DROPSHIPDC";

    public static final String XPATH_RATE_ERROR = "//sd:RATE_SHIP_UNIT_DOC/sd:SHIP_UNIT_RATE_INFO_ROW/sd:ERROR";
    public static final String XPATH_RATE_SHIP_UNIT_RATE_INFO = "//sd:RATE_SHIP_UNIT_DOC/sd:SHIP_UNIT_RATE_INFO_ROW/sd:SHIP_UNIT_RATE_INFO";



    /** CreateShipUnits only */
    public static final String TAG_CREATE_BILLING_ACCOUNT_ID = "BillingAccountID";

    public static final String XPATH_CREATE_ERROR = "//sd:CREATE_SHIP_UNIT_DOC/sd:ERROR";
    public static final String XPATH_CREATE_SHIP_UNIT_RATE_INFO = "//sd:CREATE_SHIP_UNIT_DOC/sd:SHIP_UNIT_SHIP_INFO_ROW/sd:SHIP_UNIT_SHIP_INFO";



    /** CreateShipUnits only - FTD_SHIP_UNIT_ADD_ONS */
    public static final String TAG_CREATE_FSUAO_ADD_ON_DESCRIPTION = "AddOnDescription";
    public static final String TAG_CREATE_FSUAO_ADD_ON_ID = "AddOnID";
    public static final String TAG_CREATE_FSUAO_ADD_ON_QUANTITY = "AddOnQuantity";
    public static final String TAG_CREATE_FSUAO_ADD_ON_TYPE = "AddOnType";
    public static final String TAG_CREATE_FSUAO_ADD_ON_TYPE_DESCRIPTION = "AddOnTypeDescription";
    public static final String TAG_CREATE_FSUAO_ADD_ON_WEIGHT = "AddOnWeight";
    public static final String TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ON = "FTD_SHIP_UNIT_ADD_ON";
    public static final String TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ONS = "FTD_SHIP_UNIT_ADD_ONS";
    public static final String TAG_CREATE_FSUAO_VENDOR_ADD_ON_COST = "VendorAddOnCost";
    public static final String TAG_CREATE_FSUAO_VENDOR_ADD_ON_ID = "VendorAddOnID";

    
    
    /** RateShipUnits and CreateShipUnits */
    public static final String TAG_RATE_CREATE_CARRIER_ID = "CarrierID";
    public static final String TAG_RATE_CREATE_DATA_XML = "DataXML";
    public static final String TAG_RATE_CREATE_DATE_BEST_METHOD_PARAMETER = "DateBestMethodParameter";
    public static final String TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT = "DatePlannedShipment";
    public static final String TAG_RATE_CREATE_ESTIMATED_WEIGHT = "EstimatedWeight";
    public static final String TAG_RATE_CREATE_MASTER_ITEM_ID = "MasterItemID";
    public static final String TAG_RATE_CREATE_ORDER_NUMBER = "OrderNumber";
    public static final String TAG_RATE_CREATE_SHIPPING_OPTIONS = "ShippingOptions";
    public static final String TAG_RATE_CREATE_SHIPPING_OPTIONS_COMMERCIAL = "COMMERCIAL";
    public static final String TAG_RATE_CREATE_SHIPPING_OPTIONS_OVER21 = "ADULT_SIGNATURE_CONFIRMATION";
    public static final String TAG_RATE_CREATE_SHIPPING_OPTIONS_RESIDENTIAL = "RESIDENTIAL";
    public static final String TAG_RATE_CREATE_SHIPPING_OPTIONS_SATURDAY = "SATURDAY_DELIVERY";
    public static final String TAG_RATE_CREATE_SHIP_METHOD_ID = "ShipMethodID";
    public static final String TAG_RATE_CREATE_SHIP_POINTS = "SHIP_POINTS";
    public static final String TAG_RATE_CREATE_SHIP_UNIT = "SHIP_UNIT";
    public static final String TAG_RATE_CREATE_SHIP_VIA = "ShipVia";
    public static final String TAG_RATE_CREATE_STATUS = "Status";
    public static final String TAG_RATE_CREATE_TRAILER_NUMBER = "TrailerNumber";
    public static final String TAG_RATE_CREATE_WEIGHT = "Weight";

    
    
    /** RateShipUnits and CreateShipUnits - origins element */
    public static final String TAG_RATE_CREATE_ORIGINS = "ORIGIN_DISTRIBUTION_CENTERS";
    public static final String TAG_RATE_CREATE_ORIGINS_CARRIER = "CARRIER";
    public static final String TAG_RATE_CREATE_ORIGINS_CARRIERS = "CARRIERS";
    public static final String TAG_RATE_CREATE_ORIGINS_CARRIER_ID = "CarrierID";
    public static final String TAG_RATE_CREATE_ORIGINS_DIST_CENTER = "DistributionCenter";
    public static final String TAG_RATE_CREATE_ORIGINS_ORIGIN = "ORIGIN_DISTRIBUTION_CENTER";
    public static final String TAG_RATE_CREATE_ORIGINS_TRAILERS = "TRAILERS";
    public static final String TAG_RATE_CREATE_ORIGINS_VENDOR_COST = "VendorCost";
    public static final String TAG_RATE_CREATE_ORIGINS_VENDOR_SKU = "VendorSKU";

    
    
    /** RateShipUnits and CreateShipUnits - FTD_SHIP_UNITS */
    public static final String TAG_RATE_CREATE_FSU_FTD_SHIP_UNITS = "FTD_SHIP_UNITS";
    public static final String TAG_RATE_CREATE_FSU_LOB = "LineOfBusiness";
    public static final String TAG_RATE_CREATE_FSU_MARKETING_INSERT = "MarketingInsert";
    public static final String TAG_RATE_CREATE_FSU_ORDER_NUMBER = "FTDOrderNumber";
    public static final String TAG_RATE_CREATE_FSU_PRODUCT_DESC = "FTDProductDescription";
    public static final String TAG_RATE_CREATE_FSU_PRODUCT_ID = "FTDProductID";
    public static final String TAG_RATE_CREATE_FSU_PRODUCT_WEIGHT = "ProductWeight";
    public static final String TAG_RATE_CREATE_FSU_TOTAL_VENDOR_COST = "TotalVendorCost";
    public static final String TAG_RATE_CREATE_FSU_VENDOR_COST = "VendorCost";
    public static final String TAG_RATE_CREATE_FSU_VENDOR_SKU = "VendorSKU";

    
    
    /** RateShipUnits and CreateShipUnits - ADDRESS */
    public static final String TAG_RATE_CREATE_ADDRESS = "ADDRESS";
    public static final String TAG_RATE_CREATE_ADDRESS_ADDRESS1 = "Address1";
    public static final String TAG_RATE_CREATE_ADDRESS_CITY = "City";
    public static final String TAG_RATE_CREATE_ADDRESS_CLASS = "Class";
    public static final String TAG_RATE_CREATE_ADDRESS_CLASS_DELIVER_TO = "DELIVER_TO";
    public static final String TAG_RATE_CREATE_ADDRESS_CLASS_RETURN = "RETURN";
    public static final String TAG_RATE_CREATE_ADDRESS_COMPANY_NAME = "CompanyName";
    public static final String TAG_RATE_CREATE_ADDRESS_COUNTRY = "Country";
    public static final String TAG_RATE_CREATE_ADDRESS_INDIVIDUAL_NAME = "IndividualName";
    public static final String TAG_RATE_CREATE_ADDRESS_PHONE_NUMBER = "PhoneNumber";
    public static final String TAG_RATE_CREATE_ADDRESS_STATE = "State";
    public static final String TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS = "StreetAddress";
    public static final String TAG_RATE_CREATE_ADDRESS_ZIP_CODE = "ZIPCode";
    public static final String TAG_RATE_CREATE_RETURN_ADDRESS_CITY = "Downers Grove";
    public static final String TAG_RATE_CREATE_RETURN_ADDRESS_COMPANY_NAME = "FTD.COM";
    public static final String TAG_RATE_CREATE_RETURN_ADDRESS_STATE = "IL";
    public static final String TAG_RATE_CREATE_RETURN_ADDRESS_STREET_ADDRESS = "3113 Woodcreek Drive";
    public static final String TAG_RATE_CREATE_RETURN_ADDRESS_ZIP_CODE = "60515";

   
    /************************************************************************************
     * CancelShipUnits
     ************************************************************************************/
    public static final String TAG_CANCEL_ROOT = "SHIP_UNIT_LIST_PARAMS";

    public static final String XPATH_CANCEL_ERROR = "//sd:SHIP_UNIT_RESULT_DOC/sd:SHIP_UNIT_RESULT_ROW/sd:ERROR";

    
    /************************************************************************************
     * GetShipUnitsInfo
     ************************************************************************************/
    public static final String TAG_SHIPPING_INFO_ROOT = "GetShipmentInfoXML";
    public static final String TAG_SHIPPING_INFO_REQUEST = "REQUEST";
    public static final String TAG_SHIPPING_INFO_DISTRIBUTION_CENTER = "DistributionCenter";

    public static final String XPATH_SHIPPING_INFO_NAMESPACE_URI = "http://ScanData.com/Comm/WebServices/DataSets/SD_PRO_SHIP_UNITS.xsd";

    public static final String XPATH_SHIPPING_INFO_CARRIER_ID_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:CarrierID";
    public static final String XPATH_SHIPPING_INFO_DELIVERY_DATE_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:DateExpectedDelivery";
    public static final String XPATH_SHIPPING_INFO_ORIGIN_ID_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:DistributionCenter";
    public static final String XPATH_SHIPPING_INFO_SHIPMETHOD_ID_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:ShipMethodID";
    public static final String XPATH_SHIPPING_INFO_SHIP_DATE_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:DatePlannedShipment";
    public static final String XPATH_SHIPPING_INFO_STATUS_CODE_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:STATUS/StatusCode";
    public static final String XPATH_SHIPPING_INFO_STATUS_DESC_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:STATUS/StatusDescription";
    public static final String XPATH_SHIPPING_INFO_TRACKING_NUMBER_XPATH = "//sd:SD_PRO_SHIP_UNITS/sd:SHIP_UNITS/sd:TrackingNumber";

    
    /************************************************************************************
     * ManifestTrailer and ManifestCarrier
     ************************************************************************************/
    public static final String XPATH_MANIFEST_ERROR = "//sd:MANIFEST_RESULTS_DOC/sd:ERROR";

    
    /************************************************************************************
     * Miscellaneous Tags
     ************************************************************************************/
    /** ERROR */
    public static final String TAG_ERROR= "ERROR";
    public static final String TAG_ERROR_DESCRIPTION = "Description";
    public static final String TAG_ERROR_NUMBER = "Number";

    
    
    /** Status*/
    public static final String TAG_STATUS_BATCH_ID = "PrintBatchID";
    public static final String TAG_STATUS_BATCH_SEQUENCE = "PrintBatchSequence";
    public static final String TAG_STATUS_MISSING_CREATED_DATE = "DateCreated";
    public static final String TAG_STATUS_MISSING_ORDER_STATUS_ROW = "ORDER_STATUS_DATA_ROW";
    public static final String TAG_STATUS_MISSING_PACKED_DATE = "DatePacked";
    public static final String TAG_STATUS_MISSING_SHIP_DATE = "DateShipped";
    public static final String TAG_STATUS_SHIP_DATE = "DatePlannedShipment";
    public static final String TAG_STATUS_STATUS_DATE = "DateStatus";
    public static final String TAG_STATUS_TYPE = "Status";

    
    
    /** Common across multiple calls */
    public static final String TAG_CARTON_NUMBER = "CartonNumber";
    public static final String TAG_DISTRIBUTION_CENTER = "DistributionCenter";
    public static final String TAG_SHIP_POINT_ID = "ShipPointID";
    public static final String TAG_TRACKING_NUMBER = "TrackingNumber";

    /************************************************************************************
     * Namespace
     ************************************************************************************/
    public static final String NAMESPACE_PREFIX = "sd";
    public static final String NAMESPACE_URI = "http://ScanData.com/WTM/XMLSchemas/WTM_XMLSchema_14.00.0000.xsd";
    public static final String NAMESPACE_MSN = "1"; 
    
    
    /************************************************************************************
     * Constants
     ************************************************************************************/
    public static final String LOB_FTDCOM = "AA";
    public static final String MISSING_STATUS_REQUEST = "MISSING";
    public static final String MISSING_WEST_STATUS_REQUEST = "MISSING_WEST";
    public static final String SHIPPING_INFO_DISTRIGUTION_CENTER = "AA";

    
    /** Status Codes */
    public static final String STATUS_CANCEL_ERROR_ALREADY_CANCELLED = "102012";
    public static final String STATUS_CANCEL_ERROR_TOO_LATE = "-102013";
    public static final String STATUS_DUPLICATE = "-2627";
    public static final String STATUS_FEDEX = "FEDEX";
    public static final String STATUS_FEDEX_ERROR = "-50523";
    public static final String STATUS_INITIAL = "INITIAL";
    public static final String STATUS_MANIFEST_NOTHING_TO_DO = "103012";
    public static final String STATUS_OK = "0";
    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_SDS = "SDS";
    public static final String STATUS_SDS_APP_EXCEPTION = "999";

    
    /** Zone Jump */
    public static final String ZONE_SKIP_BEST_METHOD = "ZS";

    
    
    /** Trip Status */
    public static final String ZJ_STATUS_TRIP_ACCEPTING_ORDERS = "Accepting Orders";
    public static final String ZJ_STATUS_TRIP_ACTIVE = "A";
    public static final String ZJ_STATUS_TRIP_CANCELED = "X";
    public static final String ZJ_STATUS_TRIP_CLOSED = "C";
    public static final String ZJ_STATUS_TRIP_CUTOFF_REACHED = "Cutoff Reached";
    public static final String ZJ_STATUS_TRIP_FULL = "F";    

    

    
    public SDSConstants() {
    }
}
