package com.ftd.ship.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.ShipConstants;

import com.ftd.ship.dao.ShipDAO;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

public class TestResourceProvider extends ResourceProviderBase {
    private static boolean INITIALIZED=false;
    private static String DB_DRIVER_CLASS;
    private static String DB_USER_NAME;
    private static String DB_USER_CREDENTIALS;
    private static String DB_JDBC_URL;
    private ShipDAO shipDAO;
    
    public TestResourceProvider() {
    }
    
    public Connection getDatabaseConnection() throws Exception {
        Connection conn = null;
        
        try {
            if( !INITIALIZED ) {
                init();    
            }
            
            conn = DriverManager.getConnection(DB_JDBC_URL, DB_USER_NAME, DB_USER_CREDENTIALS);
        }
        catch (Exception e) {
            throw e;    
        }

        return conn;
    }
    
    private synchronized void init() throws Exception {
        if( !INITIALIZED ) {
            ConfigurationUtil config = ConfigurationUtil.getInstance();
            DB_DRIVER_CLASS = config.getProperty(ShipConstants.TEST_PROPERTY_FILE,"db.driver.class");
            DB_USER_NAME = config.getProperty(ShipConstants.TEST_PROPERTY_FILE,"db.user.name");
            DB_USER_CREDENTIALS = config.getProperty(ShipConstants.TEST_PROPERTY_FILE,"db.user.credentials");
            DB_JDBC_URL = config.getProperty(ShipConstants.TEST_PROPERTY_FILE,"db.jdbc.url");
            
            DriverManager.registerDriver((Driver)(Class.forName(DB_DRIVER_CLASS).newInstance()));
            INITIALIZED=true;
        }
    }

    /** Return the value of the record for the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return the value column returned from the database or null
     * @throws Exception for all errors and exceptions
     */
    public String getGlobalParameter( String context, String name) throws Exception {
        Connection conn = null;
        String retval;
        
        try {
            conn = getDatabaseConnection();
            retval = shipDAO.getGlobalParameter(conn,context,name);
        } catch (Exception e) {
            retval = null;
        } finally {
            try {
                if( conn!=null ) {
                    conn.close();
                }
            } catch (Exception e2) {
                //Ignore it
            }
        }
        
        return retval;
    }

    /**
     * Returns the unecripted value from the test configuration file
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception
     */
    public String getSecureProperty(String context, String name) throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        return config.getProperty(ShipConstants.TEST_PROPERTY_FILE,name);
    }

    public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }
}
