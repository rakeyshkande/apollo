package com.ftd.ship.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.ship.common.ShipConstants;


import com.ftd.ship.dao.ShipDAO;

import java.sql.Connection;


public class J2EEResourceProvider extends ResourceProviderBase {

    private ShipDAO shipDAO;

    public J2EEResourceProvider() {
    }
    
  /**
   * Obtain connectivity with the database
   * @return Connection - db connection
   * @throws Exception
   */

    public Connection getDatabaseConnection()
            throws Exception
    {
            return DataSourceUtil.getInstance().getConnection(
                               ConfigurationUtil.getInstance().getProperty(ShipConstants.CONFIG_FILE,ShipConstants.DATASOURCE_NAME));
    }

    /** Return the cached value of the record for the frp.global_parms table
     * @param context filter
     * @param name filter
     * @return the value column returned from the database or null
     * @throws Exception for all errors and exceptions
     */
    public String getGlobalParameter( String context, String name) throws Exception {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        return configUtil.getFrpGlobalParm(context, name);
    }

    /**
     * Returns the unecripted value from frp.secure_config
     * @param context filter
     * @param name filter
     * @return value of record returned from database or null
     * @throws Exception
     */
    public String getSecureProperty(String context, String name) throws Exception {
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        return configUtil.getSecureProperty(context, name);
    }

    public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }
}
