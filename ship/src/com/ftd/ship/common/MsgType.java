package com.ftd.ship.common;

import org.apache.commons.lang.StringUtils;

public enum MsgType {
    CANCEL("CAN","CANCELED","NA"),
    PRINT("ANS","LABELED","PRINTED"),
    SHIP("ANS","SHIPPED","SHIPPED"),
    ORDER("FTD","shipRequest","NA"),
    REJECT("REJ","REJECTED","REJECTED"),
    ANSWER("ANS","NA","NA"),
    CANCELED_NOT_LABELED("CAN","CANCELED_NOT_LABELED","NA"), 
    DELIVERED("ANS", "NA", "DELIVERED"),
    ADJUSTMENT("ADJ","NA","NA");
    

    private String ftdMsgType;
    private String sdsMsgType;
    private String ftdWestMsgType;
   
    private MsgType(String ftdMsgType, String sdsMsgType, String ftdWestMsgType) {
        this.ftdMsgType=ftdMsgType; 
        this.sdsMsgType=sdsMsgType; 
        this.ftdWestMsgType=ftdWestMsgType;
    }
    
    public String getFtdMsgType(){
        return ftdMsgType;
    }
    
    public String getSdsMsgType() {
        return sdsMsgType;
    }
    
    public String getFtdWestMsgType(){
    	return ftdWestMsgType;
    }
    
    public boolean equals(MsgType msgType) {
        return ( msgType!=null && 
            StringUtils.equals(this.getFtdMsgType(),msgType.getFtdMsgType()) && 
            StringUtils.equals(this.getSdsMsgType(),msgType.getSdsMsgType()) && 
            StringUtils.equals(this.getFtdWestMsgType(),msgType.getFtdWestMsgType()));
    }
    
    public static MsgType ftdToMsgType(String msgType) {
        if( StringUtils.equals(CANCEL.getFtdMsgType(),msgType)) {
            return CANCEL;
        }
        else if( StringUtils.equals(PRINT.getFtdMsgType(),msgType)) {
            return PRINT;
        }
        else if( StringUtils.equals(SHIP.getFtdMsgType(),msgType)) {
            return SHIP;
        }
        else if( StringUtils.equals(ORDER.getFtdMsgType(),msgType)) {
            return ORDER;
        }
        else if( StringUtils.equals(REJECT.getFtdMsgType(),msgType)) {
            return REJECT;
        }
        else if( StringUtils.equals(ANSWER.getFtdMsgType(),msgType)) {
            return ANSWER;
        }
        
        
        return null;
    }
    
    public static MsgType sdsToMsgType(String msgType) {
        if( StringUtils.equals(CANCEL.getSdsMsgType(),msgType)) {
            return CANCEL;
        }
        else if( StringUtils.equals(PRINT.getSdsMsgType(),msgType)) {
            return PRINT;
        }
        else if( StringUtils.equals(SHIP.getSdsMsgType(),msgType)) {
            return SHIP;
        }
        else if( StringUtils.equals(REJECT.getSdsMsgType(),msgType)) {
            return REJECT;
        }
        else if( StringUtils.equals(CANCELED_NOT_LABELED.getSdsMsgType(),msgType)) {
            return CANCELED_NOT_LABELED;
        }
       
        
        return null;
    }
    
    
    public static MsgType ftdWestToMsgType(String msgType) {
        if( StringUtils.equalsIgnoreCase(CANCEL.getFtdWestMsgType(),msgType)) {
            return CANCEL;
        }
        else if( StringUtils.equalsIgnoreCase(PRINT.getFtdWestMsgType(),msgType)) {
            return PRINT;
        }
        else if( StringUtils.equalsIgnoreCase(SHIP.getFtdWestMsgType(),msgType)) {
            return SHIP;
        }
        else if( StringUtils.equalsIgnoreCase(ORDER.getFtdWestMsgType(),msgType)) {
            return ORDER;
        }
        else if( StringUtils.equalsIgnoreCase(REJECT.getFtdWestMsgType(),msgType)) {
            return REJECT;
        }
        else if( StringUtils.equalsIgnoreCase(ANSWER.getFtdWestMsgType(),msgType)) {
            return ANSWER;
        }
        else if( StringUtils.equalsIgnoreCase(DELIVERED.getFtdWestMsgType(),msgType)) {
        	return DELIVERED;
        }
        
        return null;
    }
}
