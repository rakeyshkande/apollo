package com.ftd.ship.common;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPFile;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ftd.osp.utilities.plugins.Logger;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * @author vsana
 *
 */
public class FTPUtil {
	
	private Logger logger = new Logger(FTPUtil.class.getName());

	private boolean isLoggedIn;
	private FTPClient ftpClient;

	public FTPUtil() {
		isLoggedIn = false;
	}

	
	/**
	 * login into home directory
	 * 
	 * @param ftpServer
	 * @param username
	 * @param password
	 * @throws java.lang.Exception
	 */
	public void login(String ftpServer, String username, String password) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering login");
			logger.debug("Opening FTP connection to " + ftpServer + " as user " + username);
		}
		
		try {
			/* connect to FTP server */
			ftpClient = new FTPClient(ftpServer);
			ftpClient.login(username, password);
			ftpClient.setConnectMode(FTPConnectMode.PASV);

			/* set the transfer type */
			ftpClient.setType(FTPTransferType.ASCII);

			/* set the login state */
			isLoggedIn = true;
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting login");
			}
		}
	}
	

	/**
	 * Logs in to the FTP Location and sets the transfer type
	 * 
	 * @param ftpLocation
	 * @param username
	 * @param password
	 * @throws java.lang.Exception
	 */
	public void login(String ftpServer, String ftpLocation, String username, String password) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering login ftpLocation : " + ftpLocation);
			logger.debug("Opening FTP connection to " + ftpServer + " as user "
					+ username);
		}
		
		try {
			/* connect to FTP server */
			ftpClient = new FTPClient(ftpServer);
			ftpClient.login(username, password);
			ftpClient.chdir(ftpLocation);
			ftpClient.setConnectMode(FTPConnectMode.PASV);

			/* set the transfer type */
			ftpClient.setType(FTPTransferType.ASCII);

			/* set the login state */
			isLoggedIn = true;
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting login");
			}
		}
	}

	/**
	 * Logs out from the FTP Location Throws an exception if not logged in.
	 * 
	 * @throws java.lang.Exception
	 */
	public void logout() throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering logout");
		}
		
		try {
			if (isLoggedIn) {
				ftpClient.quit();
				isLoggedIn = false;
			} else {
				throw new FTPException("Login Required");
			}
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting logout");
			}
		}
	}
	

	/**
	 * Retrieves the file from the ftp server.
	 * 
	 * Throws an exception if not logged in.
	 * 
	 * @param remoteFile
	 * @throws java.lang.Exception
	 */
	public byte[] getFileData(String remoteFile, String remotePath) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Changing to remote dir: " + remotePath);
		}
		ftpClient.chdir(remotePath);
		return getFile(remoteFile);
	}

	
	/**
	 * Retrieves the file from the ftp server. Throws an exception if not logged in.
	 * 
	 * @param remoteFile
	 * @throws java.lang.Exception
	 */
	public byte[] getFile(String remoteFile) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering (byte) getFile");
		}
		
		byte[] data;
		try {
			if (isLoggedIn) {
				if (logger.isDebugEnabled()) {
					logger.debug("Downloading data for file " + remoteFile);
				}
				data = ftpClient.get(remoteFile);
			} else {
				throw new FTPException("Login Required");
			}
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting (byte) getFile");
			}
		}
		return data;
	}
	
	/**Delete the file from the remote location.
	 * 
	 * @param remoteFile
	 * @throws Exception
	 */
	public void deleteFile(String remoteFile) throws Exception {
		
		if (logger.isDebugEnabled()) {
			logger.debug("Entering deleteFile");
		}
		
		try {
			if (isLoggedIn) {
				if (logger.isDebugEnabled()) {
					logger.debug("Deleting remote file " + remoteFile);
				}
				try {
					ftpClient.delete(remoteFile);
				} catch (Exception e) {
					// Simply log any delete attempt failure.
					logger.warn("deleteFile: Could not delete file : "
							+ remoteFile, e);
				}
			} else {
				throw new FTPException("Login Required");
			}
			
		} finally {
			if (logger.isDebugEnabled()) {
				logger.debug("Exiting deleteFile");
			}
		}
	}
	
	/**
	 * @param remoteDir
	 * @param remoteFileName
	 * @return
	 * @throws Exception
	 */
	
	public List<String> getRemoteFTPFiles(String remoteDir, String remoteFileName) throws Exception {
		
		List<String> lstFTDWFiles = new ArrayList<String>();
		
		FTPFile[] ftpFiles = ftpClient.dirDetails(remoteDir);
		for (FTPFile ftpFile : ftpFiles) 
		{
			if(ftpFile != null && ftpFile.getName().indexOf(remoteFileName) != -1)
				lstFTDWFiles.add(ftpFile.getName());
		}
		
		if(lstFTDWFiles.size() > 1)
			Collections.sort(lstFTDWFiles);
		
		return lstFTDWFiles;
	}
	
	public void archiveFiles(List<File> files, String remoteFilePath, String ftpServer,String username,String password) throws Exception
	{
		Session 	session 	= null;
		Channel 	channel 	= null;
		ChannelSftp channelSftp = null;
	

		try{
			JSch jsch = new JSch();
			session = jsch.getSession(username,ftpServer);
			session.setPassword(password);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;		
			channelSftp.cd(remoteFilePath);
			for(File file : files){
				channelSftp.put(new FileInputStream(file), file.getName());
			}
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
		catch(Exception ex){			
			throw new Exception("Upload of file failed", ex);
		}finally{
			if(session != null && session.isConnected()){
				session.disconnect();
			}
			if(channel != null && channel.isConnected()){
				channel.disconnect();
			}
			
		}
	}
	
	
}
