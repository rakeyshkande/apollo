package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.CarrierScanVO;

import java.io.File;

import java.io.IOException;

import java.sql.Connection;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class UPSScansBO extends CarrierScansBO {
    private static Logger LOGGER = new Logger("com.ftd.ship.bo.UPSScansBO");
    private static final String UPS_EVENT_FORMAT = "yyyyMMddhhmmss";
    private static final String PARAM_UPS_SERVER = "NOSCAN_UPS_URL";
    private static final String PARAM_UPS_ACCESS_LICENSE = "NOSCAN_UPS_LICENSE";
    private static final String PARAM_UPS_SUBSCRIPTION = "NOSCAN_UPS_SUBSCRIPTION";
    private static final String PARAM_UPS_USERID = "NOSCAN_UPS_USERID";
    private static final String PARAM_UPS_PASSWORD = "NOSCAN_UPS_PASSWORD";
    private static final String XML_HEADER =  "<?xml version='1.0'?>";
    private static final String NON_ERROR_NO_UNREAD_FILE = "no unread file";
        
    public UPSScansBO() {
        carrierId = "UPS";
    }
    
    public void getScans(Connection connection) throws SDSApplicationException { 
        HttpClient httpclient = new HttpClient();
        PostMethod httppost = null; 
        
        try { 
            String bookmark="";

            do {
                try {
                    httppost = new PostMethod(getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_UPS_SERVER,"")); 
                    RequestEntity entity = new StringRequestEntity(buildRequest(connection,bookmark), "application/x-www-formurlencoded", "ISO-8859-1");
                    httppost.setRequestEntity(entity);
         
                    int result = httpclient.executeMethod(httppost);
                    //status code
                    LOGGER.info("Response status code: " + result);
                    
                    //status line
                    LOGGER.info("Response status line: "+httppost.getStatusLine().toString());
                    
                    //response body
                    String responseString = httppost.getResponseBodyAsString();
                    //LOGGER.debug(responseString);
                    
                    //Save the scan file
                    String fileName = (new SimpleDateFormat(UPS_EVENT_FORMAT)).format(new Date())+".xml";
                    shipDAO.insertScanFile(connection,carrierId,fileName,"SP",responseString);
                     
                    //Send the JMS message to process
                    LOGGER.debug("Sending JMS message to process file "+fileName);
                    //CommonUtils.sendJMSMessage(new InitialContext(),carrierId+" "+fileName,JMSPipeline.PROCESSSCANS);
                    CommonUtils.sendJMSMessage(connection,carrierId+" "+fileName,carrierId+" "+fileName,JMSPipeline.PROCESSSCANS);
                    LOGGER.debug("Sent JMS message to process file "+fileName);
                                        
                    //Convert to an xml Document
                    Document doc = JAXPUtil.parseDocument(responseString);
                    bookmark=JAXPUtil.getFirstChildNodeTextByTagName((Element)doc.getFirstChild(),"Bookmark");
                
                    //Move to archive directory
                    String archiveDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_ARCHIVE_DIRECTORY,DEFAULT_ARCHIVE_ROOT)+"/"+carrierId+"/"+(new SimpleDateFormat(ARCHIVE_FORMAT)).format(new Date());
                        
                    //Check to see if the download directory exists
                    File archiveDir = new File(archiveDirectory);
                    if( !archiveDir.exists() ) {
                        LOGGER.debug("Creating archive directory "+archiveDir.getCanonicalPath());
                        FileUtils.forceMkdir(archiveDir);
                    }
                    
                    File archiveFile = new File(archiveDirectory, fileName);
                    LOGGER.debug("Moving file "+fileName+" to archive "+archiveDirectory);
                    FileUtils.writeStringToFile(archiveFile, responseString, null);
                    
                } catch (IOException e) {
                    LOGGER.error("Error while archiving UPS scan file.  Will continue processing...",e);
                } finally {
                    if( httppost!=null ) {
                        httppost.releaseConnection();
                    }
                }
                
            //If a bookmark is returned, then continue
            } while(StringUtils.isNotBlank(bookmark));
            
            
        } catch (Exception e) {
            throw new SDSApplicationException(e);
        }
    }   
    
    public String buildRequest(Connection connection, String bookmark) throws Exception {
        /*
         <?xml version="1.0"?>
         <AccessRequest xml:lang="en-US">
           <AccessLicenseNumber>YOURACCESSLICENSENUMBER</AccessLicenseNumber>
           <UserId>YOURUSERID</UserId>
           <Password>YOURPASSWORD</Password>
         </AccessRequest>
         
         <?xml version="1.0"?>
         <QuantumViewRequest xml:lang="en-US">
           <Request>
             <TransactionReference>
               <CustomerContext>Test XML</CustomerContext>
               <XpciVersion>1.0007</XpciVersion>
             </TransactionReference>
             <RequestAction>QVEvents</RequestAction>
             <RequestOption></RequestOption>
           </Request>
         </QuantumViewRequest>
         */
         
         ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
         Document accessDoc = JAXPUtil.createDocument();
         Element accessRequest = accessDoc.createElement("AccessRequest");
         accessRequest.setAttribute("xml:lang","en-US");
         accessDoc.appendChild(accessRequest);
         accessRequest.appendChild(JAXPUtil.buildSimpleXmlNode(accessDoc,"AccessLicenseNumber",getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_UPS_ACCESS_LICENSE,"1234567890")));         
         accessRequest.appendChild(JAXPUtil.buildSimpleXmlNode(accessDoc,"UserId", configUtil.getSecureProperty(APPLICATION_CONTEXT, PARAM_UPS_USERID)));         
         accessRequest.appendChild(JAXPUtil.buildSimpleXmlNode(accessDoc,"Password", configUtil.getSecureProperty(APPLICATION_CONTEXT, PARAM_UPS_PASSWORD)));
         
         
         Document rqstDoc = JAXPUtil.createDocument();
         Element qvRqst = rqstDoc.createElement("QuantumViewRequest");
         qvRqst.setAttribute("xml:lang","en-US");
         rqstDoc.appendChild(qvRqst);
         
         Element rqst = rqstDoc.createElement("Request");
         qvRqst.appendChild(rqst);
         
         Element transRef = rqstDoc.createElement("TransactionReference");
         rqst.appendChild(transRef);
         
         transRef.appendChild(JAXPUtil.buildSimpleXmlNode(rqstDoc,"CustomerContext","Programatic request"));           
         transRef.appendChild(JAXPUtil.buildSimpleXmlNode(rqstDoc,"XpciVersion","1.0007"));  
         
         rqst.appendChild(JAXPUtil.buildSimpleXmlNode(rqstDoc,"RequestAction","QVEvents"));     
         rqst.appendChild(rqstDoc.createElement("RequestOption"));

         Element subsRef = rqstDoc.createElement("SubscriptionRequest");
         qvRqst.appendChild(subsRef);
         
         subsRef.appendChild(JAXPUtil.buildSimpleXmlNode(rqstDoc,"Name", getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_UPS_SUBSCRIPTION,"abcdefg")));
         
         if( StringUtils.isNotBlank(bookmark) ) {
             qvRqst.appendChild(JAXPUtil.buildSimpleXmlNode(rqstDoc,"Bookmark",bookmark));
         }
         String accessStr  = JAXPUtil.toString(accessDoc);
         String rqstStr    = JAXPUtil.toString(rqstDoc);
         String totalStr   = (accessStr.startsWith("<?xml"))?accessStr:(XML_HEADER+accessStr);
         totalStr         += (rqstStr.startsWith("<?xml"))?rqstStr:(XML_HEADER+rqstStr);
         return totalStr;
    }
    
    public List<CarrierScanVO> parseScanFile(String fileName, String localDirectory) {
        List<CarrierScanVO> list = new ArrayList<CarrierScanVO>();
        
        try {
            // Create a sax parser
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setValidating(true);
            SAXParser saxParser = factory.newSAXParser();
            
            File file = new File(localDirectory+"/"+fileName);
            DeliveryHandler handler = new DeliveryHandler(fileName);
            saxParser.parse(file, handler);
            
            list = handler.getScansList();
          
        } catch (Throwable t) {
          LOGGER.error(t);
        }
        
        return list;
    }
    
    class DeliveryHandler extends DefaultHandler {
        public final String DELIVERY_TAG = "Delivery";
        public final String ORIGIN_TAG = "Origin";
        public final String EXCEPTION_TAG = "Exception";
        public final String TRACKING_NUMBER_TAG = "TrackingNumber";
        public final String DATE_TAG = "Date";
        public final String TIME_TAG = "Time";
        public final String REASON_CODE = "ReasonCode";
        public final String ERROR_TAG = "Error";
        public final String ERROR_DESCRIPTION = "ErrorDescription";
        
        private boolean inScanElement = false;
        private boolean wasErrorReturned  = false;
        private String currentElement = "";
        private String currentInScanElement = "";
        private String trackingNumber;
        private String deliveryDate;
        private String deliveryTime;
        private String scanType;
        
        private String fileName = null;
        private List<CarrierScanVO> scansList = new ArrayList<CarrierScanVO>();
        
        public DeliveryHandler(String fileName) {
            this.fileName = fileName;
        }
        
        public List<CarrierScanVO> getScansList() {
            return scansList;
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if ((inScanElement == false) && 
                (StringUtils.equals(DELIVERY_TAG,localName) || 
                 StringUtils.equals(ORIGIN_TAG,localName) || 
                 StringUtils.equals(ERROR_TAG,localName) || 
                 StringUtils.equals(EXCEPTION_TAG,localName))) 
            {
                if (wasErrorReturned == true) {
                    return;
                }
                inScanElement = true;
                trackingNumber = "";
                deliveryDate = "";
                deliveryTime = "";
                scanType = "";
                currentInScanElement = localName;
                if (StringUtils.equals(ERROR_TAG,localName)) {
                    wasErrorReturned = true;
                }
            } 
            
            currentElement = localName;
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            
            if((inScanElement == true) && (StringUtils.equals(currentInScanElement,localName))) {
                inScanElement = false;
                currentInScanElement = "";
                
                if (wasErrorReturned == true) {
                    return;
                }
                //LOGGER.debug("Tracking Number: "+trackingNumber);
                //LOGGER.debug("Delivery Date: "+deliveryDate);
                //LOGGER.debug("Delivery Time: "+deliveryTime);
                
                //Convert the date and time to a java date
                
                //Create a new vo
                CarrierScanVO vo = new CarrierScanVO();
                
                vo.setFileName(fileName);
                vo.setCarrierId(carrierId);
                vo.setOrderId("");  //UPS does not pass the venus order number back
                vo.setProcessed(false);
                vo.setScanType(StringUtils.substring((localName + scanType),0,31));
                vo.setTrackingNumber(trackingNumber);
                                    
                //Format 20061231104900 (yyyymmddHH24miss)
                String dateString = deliveryDate + deliveryTime;
                Date eventDate;
                try {
                    eventDate = (new SimpleDateFormat(UPS_EVENT_FORMAT)).parse(dateString);
                } catch (Exception e) {
                    LOGGER.warn("Error parsing event date for tracking number "+trackingNumber+".  Will use current timestamp instead.");
                    eventDate = new Date();
                }
                vo.setTimestamp(new Timestamp(eventDate.getTime()));
                
                //Add to the list
                scansList.add(vo);
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            
            if( inScanElement ) {
                if( StringUtils.equals(TRACKING_NUMBER_TAG,currentElement) ) {
                    trackingNumber += StringUtils.trimToEmpty(String.valueOf(ch,start,length));
                } else if( StringUtils.equals(DATE_TAG,currentElement) ) {
                    deliveryDate += StringUtils.trimToEmpty(String.valueOf(ch,start,length));
                } else if( StringUtils.equals(TIME_TAG,currentElement) ) {
                    deliveryTime += StringUtils.trimToEmpty(String.valueOf(ch,start,length));
                } else if( StringUtils.equals(REASON_CODE,currentElement)) {
                    scanType += StringUtils.trimToEmpty(String.valueOf(ch,start,length));
                } else if( StringUtils.equals(ERROR_DESCRIPTION,currentElement)) {
                    String errDescription = StringUtils.trimToEmpty(String.valueOf(ch,start,length));
                    if (errDescription != null && (errDescription.indexOf(NON_ERROR_NO_UNREAD_FILE) > -1)) {
                        LOGGER.info("No data to read from UPS: " + errDescription);
                    } else {
                        LOGGER.error("Error returned in scan file from UPS: " + errDescription);
                    }
                }
            }
        }
    }
}
