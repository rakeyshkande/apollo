package com.ftd.ship.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.constants.MessageConstants;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunications;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.CompanyVO;
import com.ftd.ship.vo.CustomerPhoneVO;
import com.ftd.ship.vo.CustomerVO;
import com.ftd.ship.vo.EmailVO;
import com.ftd.ship.vo.FtdwestVendorOrderStatusVO;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.OrderTrackingVO;
import com.ftd.ship.vo.OrderVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.VenusMessageVO;


public class FtdwestInboundMsgProcessingBO extends SDSProcessingBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.FtdwestInboundMsgProcessingBO");
    private boolean updateMode = false;
    
    private static final String SHIPPED = "SHIPPED";
    private static final String PROCESS_CREATE_ORD_STATUS_UPDATES = "PROCESS_CREATE_ORD_STATUS_UPDATES";
    private final static String JMS_PIPELINE_FOR_EM_PARTNERS = "SUCCESS";
    
    FtdwestCommunications ftdwestCommunications;
    
	public FtdwestInboundMsgProcessingBO() {
    }
    
    public void processMessage( Connection conn, Long orderStatusId ) throws SDSApplicationException {
        FtdwestVendorOrderStatusVO vosVO = null;
        boolean orderHasTrackingNumber = false;
        
        try { 
            vosVO = shipDAO.getVendorOrderStatusUpdate(conn,orderStatusId);
        } catch (Exception e) {
            throw new SDSApplicationException("Error while retrieving vendor order status for order status id "+orderStatusId+".  "+e.getMessage());
        }
        
        if( vosVO==null ) {
            throw new SDSApplicationException("Vendor Order status id "+orderStatusId+" was not found in the database");
        }
        
        MsgType msgType = vosVO.getMsgType();
        assert( msgType!=null ) : "MsgType not set in FtdwestInboundMsgProcessingBO:processMessage";
        assert( vosVO.getVenusOrderNumber()!=null ) : "Venus Order number is NULL in FtdwestInboundMsgProcessingBO:processMessage";

        //Get the associated venus order number
        VenusMessageVO associatedOrderVO = this.getAssociatedOrder(conn,vosVO.getVenusOrderNumber());
        if( associatedOrderVO==null ) {
            String msg = "Unable to locate associated order for "+vosVO.getVenusOrderNumber()+" in FtdwestInboundMsgProcessingBO::processMessage";
            throw new SDSApplicationException(msg);
        }
        
        // Check if the retrieved status was already taken care earlier. If yes, then don't make new insertions but just make updates for the status -
        updateMode =
        	( 
    			( MsgType.PRINT.equals(msgType) && associatedOrderVO.getPrintedStatusDate() != null)	// Printed status date was already available in VENUS
    			||
    			( MsgType.SHIP.equals(msgType) && associatedOrderVO.getShippedStatusDate() != null)		// Ship status date was already available in VENUS
    			||
    			( MsgType.DELIVERED.equals(msgType) && associatedOrderVO.getDeliveryScan() != null)		// Delivery scan date was already available in VENUS		
    			||
    			( MsgType.REJECT.equals(msgType) && associatedOrderVO.getRejectedStatusDate() != null)	// Reject status date was already available in VENUS
        	);
        
        LOGGER.debug("FtdwestInboundMsgProcessingBO->processMessage:: associatedOrderVO -> deliveryScan = " + associatedOrderVO.getDeliveryScan() );
        LOGGER.debug("FtdwestInboundMsgProcessingBO->processMessage:: updateMode = " + updateMode );
                
        //Get the order detail record associated to the venus order
        OrderDetailVO orderDetailVO = null;
        //boolean updateInvTrk = false; //Update inventory if message type is REJ.
        try {
            orderDetailVO = orderDAO.getOrderDetail(conn, associatedOrderVO.getReferenceNumber());
        } catch (Exception e) {
            throw new SDSApplicationException("Error while retrieving order detail record for "+associatedOrderVO.getVenusOrderNumber(),e);
        }
        
        //Get the order record for the message
        OrderVO orderVO = null;
        try {
            orderVO = orderDAO.getOrder(conn, orderDetailVO.getOrderGuid());
        } catch (Exception e) {
            throw new SDSApplicationException("Error while retrieving order record for "+associatedOrderVO.getVenusOrderNumber(),e);
        }
        
        //insert the venus inbound message into the database  
        VenusMessageVO inboundMessage = CommonUtils.copyVenusMessage(associatedOrderVO,msgType);
        QueueVO queueVO = null;
        
        inboundMessage.setMessageDirection(MsgDirection.INBOUND);
        inboundMessage.setOperator(ShipConstants.FTD_WEST_SYSTEM);
        inboundMessage.setTransmissionTime(new Date());
        inboundMessage.setVenusStatus(VenusStatus.VERIFIED);
        inboundMessage.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
        StringBuilder sb = new StringBuilder();
        if( MsgType.REJECT.equals(msgType) ) {
            try {
                associatedOrderVO.setRejectedStatusDate(vosVO.getRejected());
                associatedOrderVO.setCancelReasonCode(ShipConstants.VENDOR_CANCEL_REASON_CODE);
                
                sb.append("Order cannot be fulfilled due to ");
                sb.append(vosVO.getRejectMessage());
                sb.append("(");
                sb.append(vosVO.getVenusOrderNumber());
                sb.append(")");

                inboundMessage.setMessageText(sb.toString());
                inboundMessage.setComments(sb.toString());
                inboundMessage.setRejectedStatusDate(vosVO.getRejected());
                inboundMessage.setCancelReasonCode(ShipConstants.VENDOR_CANCEL_REASON_CODE);
                queueVO = processRejectMessage(conn, inboundMessage, orderVO, orderDetailVO);
                shipDAO.markVenusMsgRejected(conn,associatedOrderVO);
               
            } catch (Exception e) {
                throw new SDSApplicationException("Failed to process reject message for order "+vosVO.getVenusOrderNumber(),e);
            }                 
        } else if( MsgType.PRINT.equals(msgType) ) {
            try {
            	
            	// Check for tracking number, and update Venus ANSI messages with tracking number accordingly -
            	orderHasTrackingNumber = updateTrackingDetails(conn, vosVO, orderDetailVO, associatedOrderVO.getDeliveryDate());
            	
                //Set the printed flag on the Venus VO
                associatedOrderVO.setPrintedStatusDate(vosVO.getPrinted());
                associatedOrderVO.setPrinted(true);
               
                sb.append("Order printed by FTD West at ");
                sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(vosVO.getPrinted()));
                inboundMessage.setMessageText(sb.toString());
                inboundMessage.setComments(sb.toString());
                inboundMessage.setPrintedStatusDate(vosVO.getPrinted());
                processOrderPrintedMessage(conn, inboundMessage, orderDetailVO);
                shipDAO.markVenusMsgPrinted(conn,associatedOrderVO);
            } catch (Exception e) {
                throw new SDSApplicationException("Failed to process print message for order "+vosVO.getVenusOrderNumber(),e);
            }
        } else if( MsgType.SHIP.equals(msgType) ) {
            try {
                if( associatedOrderVO.getPrintedStatusDate()==null ) {
                	//DI-28
                	//If no print date is received in Ship Status, then default Printed On date to current date/time
                	if(vosVO.getPrinted() == null){
                		associatedOrderVO.setPrintedStatusDate(new Date());
                	}
                	else{
                		associatedOrderVO.setPrintedStatusDate(vosVO.getPrinted());
                    }
                    associatedOrderVO.setPrinted(true);
                    processOrderPrintedMessage(conn, inboundMessage, orderDetailVO);
                    shipDAO.markVenusMsgPrinted(conn,associatedOrderVO);
                }
                
                orderHasTrackingNumber = updateTrackingDetails(conn, vosVO, orderDetailVO, associatedOrderVO.getDeliveryDate());            	                
                
                associatedOrderVO.setShippedStatusDate(vosVO.getShipped());

                sb.append("Order marked shipped by FTD West at ");
                sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(vosVO.getShipped()));
                inboundMessage.setMessageText(sb.toString());
                inboundMessage.setComments(sb.toString());
                inboundMessage.setShippedStatusDate(vosVO.getShipped());
                inboundMessage.setOrderOrigin(orderVO.getOriginId());
                processItemShippedMessage(conn, inboundMessage, orderDetailVO, !updateMode, orderHasTrackingNumber);
                shipDAO.markVenusMsgShipped(conn,associatedOrderVO);
                
                associatedOrderVO.setTrackingNumber(vosVO.getTrackingNumber());
                LOGGER.info("Ship TrackingNumber on associatedOrderVO: " + associatedOrderVO.getTrackingNumber());
                 
                if (!updateMode ) { 
                	PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(orderVO.getOriginId(),orderVO.getSourceCode(), conn);
	                if(partnerMappingVO != null && partnerMappingVO.isSendFulfillmentFeed()) {
	                	shipDAO.insertPartnerShipData(conn,associatedOrderVO);
	                } else if(orderVO.getOriginId().equals("AMZNI")){
	            		shipDAO.insertAmazonPartnerShipment(conn,associatedOrderVO);
	            	} else if (new MercentOrderPrefixes(conn).isMercentOrder(orderVO.getOriginId())){
	                	shipDAO.insertMercentPartnerShipment(conn,associatedOrderVO);
	                }
	                
	                //For Ship Notice Updates 
                    if(partnerMappingVO != null && partnerMappingVO.isSendShipStatusUpdFeed())
                    {
                    	String externalOrderNumber = orderDAO.getOrderDetail(conn, associatedOrderVO.getReferenceNumber()).getExternalOrderNumber();
                    	StringBuffer payLoad = new StringBuffer().append("orderNumbers:").append(externalOrderNumber).append("|").append("operation:").append(SHIPPED);
                    	
						boolean isShipStsUpdSuccess = sendJMSMessage(JMS_PIPELINE_FOR_EM_PARTNERS, PROCESS_CREATE_ORD_STATUS_UPDATES, payLoad.toString());
						
						LOGGER.debug("Inserted JMS message for PROCESS_CREATE_ORD_STATUS_UPDATES for Ship Notice update "+isShipStsUpdSuccess);
					}
	                
                }
                
           } catch (Exception e) {
                throw new SDSApplicationException("Failed to process ship message for order "+vosVO.getVenusOrderNumber(),e);
           }
           
        } else if( MsgType.DELIVERED.equals(msgType) ) {
        	try {
        
	        	if( associatedOrderVO.getPrintedStatusDate()==null ) {
                	//DI-28
                	//If no print date is received in Delivered Status, then default Printed On date to current date/time
                	if(vosVO.getPrinted() == null){
                		associatedOrderVO.setPrintedStatusDate(new Date());
                	}
                	else{
                		associatedOrderVO.setPrintedStatusDate(vosVO.getPrinted());
                    }
	                associatedOrderVO.setPrinted(true);
	                processOrderPrintedMessage(conn, inboundMessage, orderDetailVO);
	                shipDAO.markVenusMsgPrinted(conn,associatedOrderVO);
	            }
	        	
	        	orderHasTrackingNumber = updateTrackingDetails(conn, vosVO, orderDetailVO, associatedOrderVO.getDeliveryDate()); 
	        	
	        	if( associatedOrderVO.getShippedStatusDate()==null ) {
                	//DI-28
                	//If no ship date is received in Delivered Status, then default Printed On date to current date/time
                	if(vosVO.getShipped() == null){
                		associatedOrderVO.setShippedStatusDate(new Date());
                	}
                	else{
                		associatedOrderVO.setShippedStatusDate(vosVO.getShipped());
                    }
	        		

	                sb.append("Order marked shipped by FTD West at ");
	                sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(vosVO.getShipped()));
	                inboundMessage.setMessageText(sb.toString());
	                inboundMessage.setComments(sb.toString());
	                inboundMessage.setShippedStatusDate(vosVO.getShipped());
	                inboundMessage.setOrderOrigin(orderVO.getOriginId());
	                processItemShippedMessage(conn, inboundMessage, orderDetailVO, false, orderHasTrackingNumber);	// process shipped message but don't send ship confirmation email
	                shipDAO.markVenusMsgShipped(conn,associatedOrderVO);
	                
	                PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(orderVO.getOriginId(),orderVO.getSourceCode(), conn);
	                if(partnerMappingVO != null && partnerMappingVO.isSendFulfillmentFeed()) {
	                	shipDAO.insertPartnerShipData(conn,associatedOrderVO);
	                } else if(orderVO.getOriginId().equals("AMZNI")){
	            		shipDAO.insertAmazonPartnerShipment(conn,associatedOrderVO);
	            	} else if (new MercentOrderPrefixes(conn).isMercentOrder(orderVO.getOriginId())){
	                	shipDAO.insertMercentPartnerShipment(conn,associatedOrderVO);
	                }  
	        	}
	        	
	        	// Update Delivery scan date in Venus - 
	        	shipDAO.updateVenusDeliveryScan(conn, vosVO.getVenusOrderNumber(), vosVO.getDelivered() );
	        	
	        	if ( !updateMode) {
		        	// Poll message into delivery confirm queue - 
		        	enqueueFtdwestDeliveryConfirmation(associatedOrderVO.getReferenceNumber(), associatedOrderVO.getVenusId());
	        	}
	        	
        	}
        	catch(Exception e) {
        		throw new SDSApplicationException("Failed to process delivered message for order "+vosVO.getVenusOrderNumber(),e);
        	}
        
        } else {
            throw new SDSApplicationException("Unexpected message type of "+ msgType.toString() + " found in processMessage" );
        }

        String venusMessageId = null;
        try {
        	if ( !updateMode  && !MsgType.DELIVERED.equals(msgType)) {
        		venusMessageId = shipDAO.insertVenusMessage(conn, inboundMessage);
        	}
                        
        } catch (Exception e) {
            throw new SDSApplicationException("Error while inserting venus message for "+associatedOrderVO.getVenusOrderNumber(),e);
        }
        
        if( queueVO!=null ) {
            queueVO.setMercuryId(venusMessageId);
            try {
                queueDAO.insertQueueRecord(conn,queueVO);
            } catch (Exception e) {
                throw new SDSApplicationException("Error while inserting order into the queue for "+associatedOrderVO.getVenusOrderNumber(),e);
            }
        }
            
        //Update the vendor order status record as processed
        try {
            shipDAO.updateVendorOrderStatus(conn, vosVO.getOrderStatusId());
        } catch (Exception e) {
            throw new SDSApplicationException("Unable to update the status of vendorOrderStatus id " + orderStatusId,e);
        }
    }

    /*
     * This method will update the order detail record to have a disposition of �printed�.
     */
    private void processOrderPrintedMessage(Connection conn, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO)
        throws Exception
    {
    	if ( ! updateMode ) {
	        //update the order detail disposition        
	        orderDAO.updateOrderDisposition(conn,orderDetailVO.getOrderDetailId(),ShipConstants.PRINTED_DISPOSITION, ShipConstants.FTD_WEST_SYSTEM);
    	}
    	
        Date printedDate = venusMessageVO.getPrintedStatusDate();

        //add comment to order
        StringBuilder sb = new StringBuilder();
        sb.append("Ship label for order ");
        sb.append(venusMessageVO.getVenusOrderNumber());
        sb.append(" printed by vendor ");
        sb.append(venusMessageVO.getFillingVendor());
        if( printedDate!=null ) {
            sb.append(" at ");
            sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(printedDate));
        }
        sb.append(".");
        //don't create comment on order for FTD West status updates 
        //createComment(conn,sb.toString(),orderDetailVO);
    }
    
    /* 
     * This method will parse out a ship date from the passed in message text.  
     * The order detail record will then be updated with this ship date and 
     * disposition of the order detail will be set to �shipped�.  
     * An Fed Ex confirmation will also be sent out.
     */
    private void processItemShippedMessage(Connection conn, VenusMessageVO venusMessageVO, 
    		OrderDetailVO orderDetailVO, boolean sendShipConfEmail, boolean orderHasTrackingNumber) throws Exception {
    	
        //Update order details with ship date
        Date shippedDate = venusMessageVO.getShippedStatusDate();
        if ( ! updateMode ) {
        	orderDetailVO.setOrderDispCode(ShipConstants.SHIPPED_DISPOSITION);
        }
        orderDetailVO.setShipDate(shippedDate);
        orderDAO.updateOrder(conn,orderDetailVO);

        //add comment to order
        StringBuilder sb = new StringBuilder();
        sb.append("Order ");
        sb.append(venusMessageVO.getVenusOrderNumber());
        sb.append(" marked shipped at ");
        if( shippedDate==null ) {
            shippedDate = new Date();
        }
        sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(shippedDate));
        sb.append(".");        
        
        //don't add comment to order for FTD West status update
        //createComment(conn,sb.toString(),orderDetailVO);

        //Send the email confirmation if it is not an Amazon Order
        try {
			MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(conn);
			PartnerMappingVO partnerMappingVO = null;

			if (!StringUtils.isEmpty(venusMessageVO.getOrderOrigin())) {
				partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(venusMessageVO.getOrderOrigin(),orderDetailVO.getSourceCode(), conn);
			}

        	if (sendShipConfEmail) {
	        	if(venusMessageVO.getOrderOrigin().equals("AMZNI")) {
	        		LOGGER.info("Amazon order - do not send ship confirmation email.");
	        		sendShipConfEmail = false;
	        	} else if(mercentOrderPrefixes != null && mercentOrderPrefixes.isMercentOrder(venusMessageVO.getOrderOrigin())) {
	        		LOGGER.info("Mercent order - do not send ship confirmation email.");
	        		sendShipConfEmail = false;
	        	} else if(partnerMappingVO != null) {
	        		if(!("Y".equals(partnerMappingVO.getSendShipConfEmail()))) {
	        			LOGGER.info("Partner order - SEND_SHIP_CONF_EMAIL is set to N/NULL, origin - " + venusMessageVO.getOrderOrigin());
	        			sendShipConfEmail = false;
	        		}
	        	}
        	}
        	
        	if(sendShipConfEmail) {
        		sendDropShipEmail(conn,orderDetailVO,orderHasTrackingNumber);
        	}
        	
        } catch (Throwable t) {
            //Don't roll back transaction if this fails
            LOGGER.warn("Failed to send shipping confirmation email.  Continuing with transaction.",t);
        }
    }
    
    private QueueVO processRejectMessage(Connection conn, VenusMessageVO venusMessageVO, OrderVO orderVO, OrderDetailVO orderDetailVO) throws Exception {
        //update the order detail disposition        
        orderDAO.updateOrderDisposition(conn,orderDetailVO.getOrderDetailId(),ShipConstants.PROCESSED_DISPOSITION, ShipConstants.FTD_WEST_SYSTEM);
        
        QueueVO rejQueue = null;
        //insert into queue
        rejQueue = new QueueVO();
        rejQueue.setQueueType("REJ");
        rejQueue.setMessageType(venusMessageVO.getMsgType().getFtdMsgType());
        rejQueue.setSystem(ShipConstants.FTD_WEST_SYSTEM);
        rejQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
        rejQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
        rejQueue.setOrderGuid(orderDetailVO.getOrderGuid());
        rejQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
        rejQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
        rejQueue.setMessageTimestamp(new java.util.Date());
         
        Date rejectDate = venusMessageVO.getRejectedStatusDate();
        
        //add comment to order
        StringBuilder sb = new StringBuilder();
        sb.append("Order ");
        sb.append(venusMessageVO.getVenusOrderNumber());
        sb.append(" rejected by vendor ");
        sb.append(venusMessageVO.getFillingVendor());
        if( rejectDate!=null ) {
            sb.append(" at ");
            sb.append((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).format(rejectDate));
        }
        sb.append(".");
        
        
         
         //don't add comment to order for FTD West status update
         //createComment(conn,sb.toString(),orderDetailVO);
         
         return rejQueue;
    }
    
    public void sendDropShipEmail(Connection conn, OrderDetailVO orderDetailVO, boolean orderHasTrackingNumber) throws SDSApplicationException
    {
        try {
            GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
        
            //get the sender email address from config file
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();         
            String fromEmailAddress = configUtil.getProperty(ShipConstants.CONFIG_FILE,"FEDEX_EMAIL_FROM_ADDRESS");                 
            String fedExEmailTemplate = null;
            if (orderHasTrackingNumber)
        	{
            	fedExEmailTemplate = configUtil.getProperty(ShipConstants.CONFIG_FILE,"FEDEX_EMAIL_TEMPLATE_ID");   
        	}
            else
            {
            	fedExEmailTemplate = configUtil.getProperty(ShipConstants.CONFIG_FILE,"FEDEX_EMAIL_TEMPLATE_ID_GENERIC"); 	
            }
                       
            String prefPartner = null;
            com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
            if(partnerVO != null) {
                prefPartner = partnerVO.getPartnerName();
            }
            
            if(!StringUtils.isBlank(prefPartner)) {
                fedExEmailTemplate = fedExEmailTemplate + "." + prefPartner;  
            }
    
            //Get data needed for email
            OrderVO orderVO = orderDAO.getOrder(conn,orderDetailVO.getOrderGuid());
            CustomerVO buyerVO = orderDAO.getCustomer(conn,orderVO.getCustomerId());
            EmailVO emailVO = orderDAO.getCustomerEmailInfo(conn,buyerVO.getCustomerId(),orderVO.getCompanyId());
        
            //if email info was not found then do not send out email
            if(emailVO == null)
            {
                LOGGER.info("Email information was not found for customer " + buyerVO.getCustomerId() + " for company " + orderVO.getCompanyId() + ".  Tracking number will not be emailed.");
                return;
            }
    
            CompanyVO companyVO = orderDAO.getCompany(conn, orderVO.getCompanyId());
    
    
            //get  tracking vo from order
            OrderTrackingVO trackingVO = orderDAO.getTrackingInfo(conn, orderDetailVO.getOrderDetailId());
                       
            //if tracking number does not exist on order exit
            if(trackingVO == null && orderHasTrackingNumber)
            {
                String errorMsg = "Tracking number was not found for order detail id " + orderDetailVO.getOrderDetailId();
                throw new Exception(errorMsg);
            }
                       
            //create poc xml
            Document xml = JAXPUtil.createDocument();
            Element root = xml.createElement("root");
            xml.appendChild(root);
            if(orderHasTrackingNumber){
	            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"carrier_name",trackingVO.getCarrierName()));
	            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"carrier_url",trackingVO.getCarrierURL()));
	            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"carrier_phone",trackingVO.getCarrierPhone()));
	            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"tracking_number",trackingVO.getTrackingNumber()));
            }
            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"external_order_number", getOrderNumber(orderDetailVO, orderVO.getOriginId(), conn)));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"company_name",companyVO.getCompanyName()));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(xml,"company_url",companyVO.getURL()));
    
            Element ordersNode = xml.createElement("ORDERS");
            root.appendChild(ordersNode);
            Element orderNode = xml.createElement("ORDER");
            ordersNode.appendChild(orderNode);
            Element value = xml.createElement("language_id");
            orderNode.appendChild(value);
            value.appendChild(xml.createTextNode(orderVO.getLanguageId()));

            //dump out xml for debugging
            LOGGER.debug(JAXPUtil.toString(xml));
    
            //create a point of contact VO
            PointOfContactVO pocVO = new PointOfContactVO();
            pocVO.setCustomerId(buyerVO.getCustomerId());
            pocVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
            pocVO.setOrderGuid(orderDetailVO.getOrderGuid());
            pocVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
            pocVO.setExternalOrderNumber(getOrderNumber(orderDetailVO, orderVO.getOriginId(), conn));
            Calendar cal = new GregorianCalendar();
            cal.setTime(orderDetailVO.getDeliveryDate());
            pocVO.setDeliveryDate(cal);
            pocVO.setCompanyId(orderVO.getCompanyId()); 
            pocVO.setFirstName(buyerVO.getFirstName());
            pocVO.setLastName(buyerVO.getLastName());

            List phones = buyerVO.getCustomerPhoneVOList();
            if( phones!=null ) {
                for( int idx=0; idx<phones.size(); idx++ ) {
                    CustomerPhoneVO phoneVO = (CustomerPhoneVO)phones.get(idx);
                    String phone = phoneVO.getPhoneType();
                    if( StringUtils.equals("Day",phone) ) {
                        pocVO.setDaytimePhoneNumber(phone);
                    } else if( StringUtils.equals("Evening",phone) ) {
                        pocVO.setEveningPhoneNumber(phone);
                    }
                }
            }
            pocVO.setSenderEmailAddress(fromEmailAddress + "@" + companyVO.getURL()); 
            pocVO.setLetterTitle(fedExEmailTemplate); 
            pocVO.setEmailSubject("Ship Confirmation");
            pocVO.setRecipientEmailAddress(emailVO.getEmailAddress()); 
            
            pocVO.setTemplateId(null); 
            pocVO.setPointOfContactType(ShipConstants.POC_EMAIL_TYPE); 
            pocVO.setCommentType("Order"); 
            if (globalParamHandler != null) {
                pocVO.setMailserverCode(globalParamHandler.getFrpGlobalParm(MessageConstants.MESSAGE_GEN_CONFIG_CONTEXT, MessageConstants.MAILSERVER_KEY)); 
            }
    
            pocVO.setDataDocument(xml);   
            
            StockMessageGenerator messageGenerator = new StockMessageGenerator();
            
            // Changes for ET
            pocVO.setSourceCode(orderDetailVO.getSourceCode());
            pocVO.setEmailType(MessageConstants.SHIP_CONFIRMATION_EMAIL_TYPE);            
           
            //Populate RecordAttributesXML attribute based on orderguid
            String orderGuid = orderDetailVO.getOrderGuid();            
            if(StringUtils.isNotEmpty(orderGuid)) {            	
            	String recordAttributesXML = messageGenerator.generateRecordAttributeXMLFromOrderGuid(orderGuid);
            	pocVO.setRecordAttributesXML(recordAttributesXML);
            } else {            	
            	if (LOGGER.isDebugEnabled())
                {
            		LOGGER.debug("Not populating RecordAttributesXML as there is no orderguid information");
                }
            }
            
            messageGenerator.processMessage(pocVO);
            
        } catch (Exception e) {
            LOGGER.error(e);
            throw new SDSApplicationException(e.getMessage(),e);
        }
    }
    
	/** Get order number as per the origin. If not FTD order, replace order number with the XX partner order number.
	 * @param tmpDetailVO
	 * @param orderOrigin
	 * @return
	 */
	private String getOrderNumber(OrderDetailVO tmpDetailVO, String orderOrigin, Connection connection) {
		// Default value is FTD external order number.
		String orderNumber = tmpDetailVO.getExternalOrderNumber();
		
		if(StringUtils.isEmpty(orderOrigin)) {
			LOGGER.error("Unable to determine if the order is partner order, invalid order origin");
			return orderNumber;
		}
		// If partner order replace it with partner order number
		CachedResultSet result = new PartnerUtility()
				.getPtnOrderDetailByConfNumber(tmpDetailVO.getExternalOrderNumber(), orderOrigin, connection);
		if (result != null && result.next()) {
			if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
				orderNumber = result.getString("PARTNER_ORDER_ITEM_NUMBER");
			}
		}
		return orderNumber;
	}
	
	private boolean isEmpty(String str) {
		return (str == null || str.trim().length() == 0);
	}
	
	private boolean updateTrackingDetails( Connection conn, FtdwestVendorOrderStatusVO vosVO, OrderDetailVO orderDetailVO, Date venusDeliveryDate ) throws Exception
	{
		
		boolean orderHasTrackingInfo = false;
		if ( vosVO != null && vosVO.getCarrier() != null && !vosVO.getCarrier().equalsIgnoreCase("UNKNOWN") ) 
		{	// Updates be made only when valid carrier value
			orderHasTrackingInfo = true;
			LOGGER.info("orderHasTrackingInfo: " + orderHasTrackingInfo);
			if ( ! ( isEmpty( vosVO.getTrackingNumber() ) && venusDeliveryDate == null ) ) {	// Either tracking number or delivery date is provided
				
				// Construct message text -
				StringBuilder trackingNumberComment = new StringBuilder();
				if ( ! isEmpty( vosVO.getTrackingNumber() ) ) {
					trackingNumberComment.append("Your ");
					trackingNumberComment.append(vosVO.getCarrier());
					trackingNumberComment.append(" tracking number is ");
					trackingNumberComment.append(vosVO.getTrackingNumber());
					trackingNumberComment.append(".\r\n");
				}
				
				if (venusDeliveryDate != null) {
					trackingNumberComment.append("Your order is scheduled for delivery on ");
					trackingNumberComment.append((new SimpleDateFormat(EMAIL_FORMAT)).format(venusDeliveryDate));
					trackingNumberComment.append(".");
				}
				
				// Update tracking number for the FTD record, and message text & comments for the 1st ANS message record in Venus table -
				shipDAO.updateTrackingDetails( conn, vosVO.getVenusOrderNumber(), vosVO.getTrackingNumber(), 
						trackingNumberComment.toString(), vosVO.getCarrier() );			
			}
			else if (LOGGER.isDebugEnabled() ) {
				LOGGER.debug("No comments update to ANS as no Tracking Number and Delivered date provided.");
			}
			
			if ( ! isEmpty(vosVO.getTrackingNumber()) ) {
				// Update tracking number for the Order - 
				OrderTrackingVO trackingVO = new OrderTrackingVO();
				trackingVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
				trackingVO.setTrackingDescription("");
				trackingVO.setTrackingNumber(vosVO.getTrackingNumber());
				trackingVO.setCarrierId(vosVO.getCarrier());
				orderDAO.updateTrackingNumber(conn, trackingVO);
			} else if (LOGGER.isDebugEnabled() ) {
				LOGGER.debug("No tracking number provided hence no update to Order detail record");
			}
			
		}
		else {
			LOGGER.debug("No tracking number and comments are updated for VENUS or ORDER_TRACKING, " +
					"since the carrier is : " + vosVO.getCarrier() );
		}
		LOGGER.info("orderHasTrackingInfo2: " + orderHasTrackingInfo);
		return orderHasTrackingInfo;
	
	}
	
	private void enqueueFtdwestDeliveryConfirmation(String orderDetailId, String venusId) throws Exception {
		
		LOGGER.debug("Sending order detail id:" + orderDetailId + " delivery confirmation...");

        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(orderDetailId);
        token.setStatus("EMAILDELIVERYCONFIRM");
        token.setJMSCorrelationID(venusId);
        //put in a 5 second delay
        token.setProperty("JMS_OracleDelay", String.valueOf(5),"int");
        Dispatcher.getInstance().dispatchTextMessage(context, token);
    }
	
	private void enqueueFtdwestCancelMsg(String venusId) throws Exception {
		
		LOGGER.debug("Enqueuing to Invoke Cancel WEST Order for venus id:" + venusId + "...");

        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(venusId);
        token.setStatus("SHIPPROCESSSHIP");
        token.setJMSCorrelationID(venusId);
        //put in a 5 second delay
        token.setProperty("JMS_OracleDelay", String.valueOf(5),"int");
        Dispatcher.getInstance().dispatchTextMessage(context, token);
    }

	/**
	 * Process for missing WEST orders status update
	 * @param connection - The database connection
	 */
	public void retrieveLatestStatusForPendingWestOrders(Connection connection) throws Exception {
		
		// 1. Retrieve a list of non-rejected WESt orders that are in PROCESSED status -
		
		Map<String,String> westOrdersPendingStatus = shipDAO.getVendorOrdersPendingStatusUpdate( connection );
		
		// 2. Invoke skynet call to retrieve latest status for each order and update database -
		if (westOrdersPendingStatus != null && westOrdersPendingStatus.size() > 0) {
			for (Map.Entry<String, String> westVenusRecord : westOrdersPendingStatus.entrySet() ) {
				FtdwestVendorOrderStatusVO vosVO = ftdwestCommunications.getOrder(connection, westVenusRecord.getValue() );
				shipDAO.insertVendorOrderStatus(connection, vosVO);
				if (vosVO.isSendCancelMsgToWest()) {
					enqueueFtdwestCancelMsg(westVenusRecord.getKey());
				}
			}
		}
	}
	
	/**
	 * @return the ftdwestCommunications
	 */
	public FtdwestCommunications getFtdwestCommunications() {
		return ftdwestCommunications;
	}

	/**
	 * @param ftdwestCommunications the ftdwestCommunications to set
	 */
	public void setFtdwestCommunications(FtdwestCommunications ftdwestCommunications) {
		this.ftdwestCommunications = ftdwestCommunications;
	}
	
	
	/**
	 * @param status
	 * @param corrId
	 * @param message
	 * @return
	 */
	public boolean sendJMSMessage(String status, String corrId, String message) {
		boolean success = true;
		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			LOGGER.error(e);
			success = false;
		}
		return success;
	}
}
