package com.ftd.ship.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.messagegenerator.StockMessageGenerator;
import com.ftd.messagegenerator.vo.PointOfContactVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.VendorProductAvailabilityUtility;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.osp.utilities.vo.VendorAddOnVO;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.SDSErrorDisposition;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.CarrierVO;
import com.ftd.ship.vo.CompanyVO;
import com.ftd.ship.vo.CustomerVO;
import com.ftd.ship.vo.GlobalParameterVO;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.OrderTrackingVO;
import com.ftd.ship.vo.OrderVO;
import com.ftd.ship.vo.ProductNotificationVO;
import com.ftd.ship.vo.ProductVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.SDSErrorVO;
import com.ftd.ship.vo.SDSLogisticsVO;
import com.ftd.ship.vo.SDSRateResponseVO;
import com.ftd.ship.vo.SDSShipResponseVO;
import com.ftd.ship.vo.SDSZoneJumpTripVO;
import com.ftd.ship.vo.ShipMethodVO;
import com.ftd.ship.vo.ShipVendorProductVO;
import com.ftd.ship.vo.ShipmentOptionVO;
import com.ftd.ship.vo.VenusMessageVO;

public class SDSShipmentProcessingBO extends SDSProcessingBO
{
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSShipmentProcessingBO");
    protected static final int DEFAULT_MAX_SHIP_DAYS_OUT = 10;
    protected static final int DEFAULT_DEQUEUE_HOUR = 16;
    protected static final long REPROCESS_DELAY = 5000L;
    protected static final String ADDRESS_TYPE_RESIDENTIAL = "HOME";
    protected static final int DIGITS_IN_ORDER_NUMBER = 7;
    protected static final long DEFAULT_PARTNER_MAX_SYSTEM_REJECTS = 5L;
    protected static final long DEFAULT_PARTNER_MAX_VENDOR_REJECTS = 3L;
    protected static final long DEFAULT_PARTNER_MAX_CARRIER_REJECTS = 3L;
    protected static final String DELIVERY_DATE_FORMAT = "MM/dd/yyyy";
    protected static final int DEFAULT_SHIPMENT_OPTION_RANK = 0;
    protected static final String ZONE_JUMP_SUCCESS = "ZONE_JUMP_SUCCESS";
    protected static final String SHIPMENT_OPTION_IDEAL_RANK = "SHIPMENT_OPTION_IDEAL_RANK";
    protected static final String SHIPMENT_OPTION_ACTUAL_RANK = "SHIPMENT_OPTION_ACTUAL_RANK";
    protected static final String TOTAL_ADD_ON_WEIGHT = "TOTAL_ORDER_WEIGHT";
    protected static final String TOTAL_ADD_ON_COST = "TOTAL_ORDER_COST";

    public SDSShipmentProcessingBO()
    {
    }

    public void processRequest(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        assert (connection != null) : "Database connection is null in SDSProcessingBO.processShipRequest";
        StringBuilder sb;
        int idealRank = 0;
        int actualRank = 0;

        //If the order has been cancelled, then just change the status and exit
        //since we are no longer interested in getting a tracking number.
        //Defect 3634 ... ignore all cancelled orders
        if (StringUtils.equals(venusMessageVO.getSdsStatus(), "CANCELLED"))
        {
            sb = new StringBuilder();
            sb.append("SDS order ");
            sb.append(venusMessageVO.getVenusOrderNumber());
            sb.append(" has been cancelled.  No need to request a tracking number.");
            LOGGER.info("processRequest -> " + sb.toString());
            return;
        }

        //Defect 2619 - Inventory Tracking. Increment FTD message count against product inventory.
        if (venusMessageVO.getInvTrkCountDate() == null)
        {
            try
            {
                shipDAO.updateInvTrkCount(connection, venusMessageVO);
            }
            catch (Exception e)
            {
                LOGGER.error("processRequest -> " + e);
                CommonUtils.getInstance().sendSystemMessage(
                        "Product inventory deduction failed for venus id:" + venusMessageVO.getVenusId() + ".\nReason:"
                                + e.getMessage());
            }
        }

        SDSLogisticsVO logisticsVO = null;
        String shipUnit;
        boolean orderZoneJumped = false;
        GlobalParameterVO gpVO = CommonUtils.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT,
                ShipConstants.GLOBAL_PARMS_MAX_SHIP_DAYS_OUT, String.valueOf(DEFAULT_MAX_SHIP_DAYS_OUT));
        int maxShipDaysOut = Integer.parseInt(gpVO.getValue());
        boolean willOrderShipAfterMaxDays = willOrderShipAfterMaxDays(venusMessageVO, maxShipDaysOut);
        //Defect 4777 - Orders REJ when they qualify for a ZoneJump more than 10 days out.
        //Need to bypass ZoneJump logic for orders with a ship date > 10 days out.
        //Defect 11946 - Blooms by Noon - need to bypass ZoneJump if order contains morning delivery fee
        if (!willOrderShipAfterMaxDays
                && !orderDAO.orderHasMorningDeliveryFee(connection, venusMessageVO.getReferenceNumber()))
        {

            logisticsVO = buildRequestZoneJump(connection, venusMessageVO);
            if (logisticsVO.getZoneJumpHubs().size() == 0)
            {
                //No ZoneJump vendor available for order. Send order for regular processing.
                String msg = "No ZoneJump hubs were available to service this venus order ("
                        + venusMessageVO.getVenusOrderNumber() + ").";
                LOGGER.error("processRequest -> " + msg);
                logisticsVO = null;
            }
            else
            {
                HashMap zoneJumpResults = processOrderZoneJump(connection, venusMessageVO, logisticsVO);
                orderZoneJumped = ((Boolean) zoneJumpResults.get(ZONE_JUMP_SUCCESS)).booleanValue();
                idealRank = ((Integer) zoneJumpResults.get(SHIPMENT_OPTION_IDEAL_RANK)).intValue();
                actualRank = ((Integer) zoneJumpResults.get(SHIPMENT_OPTION_ACTUAL_RANK)).intValue();
                LOGGER.debug("processRequest -> orderZoneJumped:" + orderZoneJumped);
                LOGGER.debug("processRequest -> ideal rank:" + ((Integer) zoneJumpResults.get(SHIPMENT_OPTION_IDEAL_RANK)).intValue());
                LOGGER.debug("processRequest -> actual rank:" + ((Integer) zoneJumpResults.get(SHIPMENT_OPTION_ACTUAL_RANK)).intValue());
            }
        }

        if (orderZoneJumped)
        {
            return;
        }
        
        if (venusMessageVO.isGetSdsResponse())
        {
            logisticsVO = buildRequest(connection, venusMessageVO);

            if (logisticsVO.getAvailableVendors() == null || logisticsVO.getAvailableVendors().size() == 0)
            {
                String msg = "No vendors were available to service this venus order ("
                        + venusMessageVO.getVenusOrderNumber() + ").";
                LOGGER.error("processRequest -> " + msg);
                createComment(connection, msg, logisticsVO.getOrderDetailVO());
                rejectAndQueue(connection, logisticsVO, msg, true);
                return;
            }

            shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
            LOGGER.debug("processRequest -> shipUnitInfo request is " + shipUnit);

            sb = new StringBuilder();
            sb.append("Sending request to Argo:\r\n");
            sb.append("Order Number: ");
            sb.append(venusMessageVO.getVenusId());
            sb.append("\r\nForcing Shipment: ");
            sb.append(venusMessageVO.isForcedShipment());
            sb.append("\r\nShipment Already Rated: ");
            sb.append(venusMessageVO.isRatedShipment());
            sb.append("\r\nRequesting Response Data: ");
            sb.append(venusMessageVO.isGetSdsResponse());

            OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
            createComment(connection, sb.toString(), orderDetailVO);
            Document response = null;

            try
            {
                response = sdsCommunications.getShipUnitInfo(connection, logisticsVO.getShipUnit());

                if (response == null)
                    throw new SDSApplicationException("processRequest -> Null response generated.  Check for timeout from SDS server.");

            }
            finally
            {
                if (response != null)
                    insertSDSTransaction(connection, venusMessageVO.getVenusId(),
                            JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
            }

            processResponse(connection, venusMessageVO, logisticsVO, response, "GetShipUnitsInfo");
        }
        else
        {
            if (willOrderShipAfterMaxDays)
            {
                processOrderShipAfterMaxDays(connection, venusMessageVO, logisticsVO);
            }
            else
            {
                processOrder(connection, venusMessageVO, logisticsVO, idealRank, actualRank);
            }
        }
    }

    public void processResponse(Connection connection, VenusMessageVO venusMessageVO, SDSLogisticsVO logisticsVO,
            Document response) throws Exception
    {
        processResponse(connection, venusMessageVO, logisticsVO, response, null, 0);
    }

    public void processResponse(Connection connection, VenusMessageVO venusMessageVO, SDSLogisticsVO logisticsVO,
            Document response, String requestType) throws Exception
    {
        processResponse(connection, venusMessageVO, logisticsVO, response, requestType, 0);
    }


    public void processResponse(Connection connection, VenusMessageVO venusMessageVO, SDSLogisticsVO logisticsVO,
            Document response, String requestType, int vendorIndex) throws Exception
    {
        StringBuilder sb;
        boolean ratedOnEntry = venusMessageVO.isRatedShipment();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();

        boolean success = true;
        SDSShipResponseVO shipResponse = null;

        if (requestType.equalsIgnoreCase("CreateShipUnits"))
        {
            shipResponse = parseCreateShipUnitsResponse(response, logisticsVO);
            LOGGER.debug("processResponse -> CreateShipUnits Response: " + shipResponse.getValue());
        }
        else if (requestType.equalsIgnoreCase("CreateShipUnitsZoneJump"))
        {
            shipResponse = parseCreateShipUnitsResponseZoneJump(response, logisticsVO, vendorIndex);
            LOGGER.debug("processResponse -> CreateShipUnitsZoneJump Response: " + shipResponse.getValue());
        }
        else if (requestType.equalsIgnoreCase("CreateShipUnitsPending"))
        {
            shipResponse = parseCreateShipUnitsResponsePending(response, logisticsVO);
            LOGGER.debug("processResponse -> CreateShipUnitsPending Response: " + shipResponse.getValue());
        }
        else if (requestType.equalsIgnoreCase("GetShipUnitsInfo"))
        {
            shipResponse = parseGetShipUnitsInfoResponse(response);
            LOGGER.debug("processResponse -> GetShipUnitsInfo Response: " + shipResponse.getValue());
        }
        else
        {
            LOGGER.error("processResponse -> Invalid request type found while processing response: " + requestType);
            throw new SDSApplicationException("processRequest -> Invalid request type found while processing response: " + requestType);
        }

        logisticsVO.setShipUnitResponse(shipResponse);

        if (shipResponse == null)
        {
            throw new SDSApplicationException("processRequest -> Ship unit reponse not received for " + venusMessageVO.getVenusId()
                    + " (" + venusMessageVO.getVenusOrderNumber()
                    + ").  Cannot continue...Manual intervention is required.");
        }

        if (!shipResponse.isSuccess())
        {
            success = false;
            LOGGER.error("processResponse -> Create request for " + venusMessageVO.getVenusId() + " ("
                    + venusMessageVO.getVenusOrderNumber() + ") has failed.");
        }

        ShipVendorProductVO selectedVendor = null;
        Date now = new Date();

        if (success)
        {
            //check ship date to see if it has changed
            Date sdsShipDate = shipResponse.getShipDate();
            Date venusShipDate = venusMessageVO.getShipDate();
            assert (venusShipDate != null) : "Ship date on venus record is null in SDSProcessingBO.processSDSOrder";
            assert (sdsShipDate != null) : "Ship date returned from SDS server is null in SDSProcessingBO.processSDSOrder";
            if (sdsShipDate == null)
            {
                throw new SDSApplicationException("processRequest -> Ship date returned from SDS server is null for order "
                        + venusMessageVO.getVenusOrderNumber());
            }

            String selectedVendorCode = shipResponse.getSelectedOrigin();
            //Find the vendor id selected
            List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
            assert (availableVendors != null) : "availableVendors is null in SDSProcessingBO.processShipRequest";
            for (int idx = 0; idx < availableVendors.size(); idx++)
            {
                ShipVendorProductVO svp = availableVendors.get(idx);
                if (StringUtils.equals(svp.getVendorCode(), selectedVendorCode))
                {
                    selectedVendor = svp;
                    break;
                }
            }

            assert (selectedVendor != null) : "selectedVendors is null in SDSProcessingBO.processShipRequest";

            if (selectedVendor == null)
            {
                sb = new StringBuilder();
                sb.append("Unable to locate selected vendor ");
                sb.append(selectedVendorCode);
                sb.append(" in list of available vendors for venus order ");
                sb.append(venusMessageVO.getVenusOrderNumber());
                sb.append(".  Vendor product availability may have changed.  Contact merchandising.");
                LOGGER.error("processResponse -> " + sb.toString());
                createComment(connection, sb.toString(), logisticsVO.getOrderDetailVO());
                rejectAndQueue(connection, logisticsVO, sb.toString(), true);
                return;
            }

            if (sdsShipDate.getTime() != venusShipDate.getTime())
            {
                sb = new StringBuilder();
                sb.append("Ship server has changed the ship date of order ");
                sb.append(venusMessageVO.getVenusOrderNumber());
                sb.append(" from ");
                sb.append((new SimpleDateFormat(MMDDYYYY_FORMAT)).format(venusMessageVO.getShipDate()));
                sb.append(" to ");
                sb.append((new SimpleDateFormat(MMDDYYYY_FORMAT)).format(sdsShipDate));
                sb.append(".");

                //Validate that the selected vendor can ship on the new ship date
                if (shipDAO.canVendorShipOnShipDate(connection, selectedVendor.getVendorId(), sdsShipDate))
                {
                    sb.append("  System verified that the vendor can ship on the new ship date.");
                    createComment(connection, sb.toString(), orderDetailVO);
                }
                else
                {
                    if (venusMessageVO.isForcedShipment())
                    {
                        sb.append("  Vendor ");
                        sb.append(venusMessageVO.getVendorId());
                        sb.append(" cannot ship on the new ship date.  Tried forcing shipment on customer requested ship date.  Unable to ship order.  FTD order");
                        if (StringUtils.isNotBlank(venusMessageVO.getTrackingNumber()))
                        {
                            sb.append(" and tracking number");
                        }
                        sb.append(" will be cancelled.  Sending to the \"REJ\" queue.");
                    }
                    else
                    {
                        sb.append("  Vendor ");
                        sb.append(venusMessageVO.getVendorId());
                        sb.append(" cannot ship on the new ship date.  FTD order");
                        if (StringUtils.isNotBlank(venusMessageVO.getTrackingNumber()))
                        {
                            sb.append(" and tracking number");
                        }
                        sb.append(" will be cancelled and a new FTD order will be created to force shipment on the original ship date of ");
                        sb.append((new SimpleDateFormat(MMDDYYYY_FORMAT)).format(venusMessageVO.getShipDate()));
                        sb.append(".");
                    }
                    createComment(connection, sb.toString(), orderDetailVO);

                    //Cancel the tracking number
                    cancelTrackingNumber(connection, logisticsVO.getMessageVo());

                    if (venusMessageVO.isForcedShipment())
                    {
                        rejectAndQueue(connection, logisticsVO, sb.toString(), true);
                        venusMessageVO.setVenusStatus(VenusStatus.ERROR);
                    }
                    else
                    {
                        reprocessOrder(connection, logisticsVO, true, false);
                        venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
                    }
                    venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);

                    shipDAO.updateVenusSDS(connection, venusMessageVO);
                    return;
                }
            }

            //Check to see if the delivery date has changed
            //If the delivery date is later then expected, notify the customer
            if (shipResponse.getDeliveryDate().after(venusMessageVO.getDeliveryDate()))
            {
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                String trackingMessageDeliveryDateMsg = configUtil.getProperty(ShipConstants.CONFIG_FILE,
                        "COMMENT_TRACKING_MESSAGE_DEL_DATE");
                createComment(connection, trackingMessageDeliveryDateMsg, orderDetailVO);
                venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
                venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                shipDAO.updateVenusSDS(connection, venusMessageVO);
                sendToQueue(connection, logisticsVO.getMessageVo(), logisticsVO.getOrderDetailVO(),
                        logisticsVO.getOrderVO());
                //Allow the order to flow through and send tracking ANS
            }

            //All is well, let's continue
            String carrierId = logisticsVO.getShipUnitResponse().getCarrier();
            String shipMethod = logisticsVO.getShipUnitResponse().getShipMethod();
            String carrierDelivery;
            ShipMethodVO smVO;

            boolean shipmentIsRated;

            if (StringUtils.equals(carrierId, SDSConstants.STATUS_PENDING))
            {
                shipmentIsRated = false;
                venusMessageVO.setRatedShipment(false);
                venusMessageVO.setForcedShipment(false);
                carrierDelivery = null;
                venusMessageVO.setFillingVendor(selectedVendorCode);
                venusMessageVO.setVendorId(selectedVendor.getVendorId());
            }
            else
            {
                String ftdShipMethod;

                try
                {
                    smVO = shipDAO.getFtdShipMethodData(connection, carrierId, shipMethod);
                    carrierId = smVO.getCarrierId();
                    ftdShipMethod = smVO.getShipMethod();
                    carrierDelivery = smVO.getCarrierDelivery();
                }
                catch (Throwable t)
                {
                    ftdShipMethod = logisticsVO.getOrderDetailVO().getShipMethod();

                    //FIXME: We need to change this to look it up out of the database
                    if (StringUtils.equals(ftdShipMethod, "ND"))
                    {
                        carrierDelivery = "ND" + StringUtils.substring(carrierId, 0, 1);
                    }
                    else if (StringUtils.equals(ftdShipMethod, "2F"))
                    {
                        carrierDelivery = "2D" + StringUtils.substring(carrierId, 0, 1);
                    }
                    else if (StringUtils.equals(ftdShipMethod, "GR"))
                    {
                        carrierDelivery = "3D" + StringUtils.substring(carrierId, 0, 1);
                    }
                    else if (StringUtils.equals(ftdShipMethod, "PO"))
                    {
                        carrierDelivery = "PO" + StringUtils.substring(carrierId, 0, 1);
                    }
                    else if (StringUtils.equals(ftdShipMethod, "SA"))
                    {
                        Calendar shipDate = Calendar.getInstance();
                        shipDate.setTime(logisticsVO.getOrderDetailVO().getShipDate());
                        if (shipDate.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                        {
                            carrierDelivery = "2F" + StringUtils.substring(carrierId, 0, 1);
                        }
                        else
                        {
                            carrierDelivery = "ND" + StringUtils.substring(carrierId, 0, 1);
                        }
                    }
                    else
                    {
                        carrierDelivery = "ND" + StringUtils.substring(carrierId, 0, 1);
                    }

                    sb = new StringBuilder();
                    sb.append("Error while determining the FTD ship method for venus id ");
                    sb.append(venusMessageVO.getVenusId());
                    sb.append("\r\nVenus order number: ");
                    sb.append(venusMessageVO.getVenusOrderNumber());
                    sb.append("\r\nCarrier id: ");
                    sb.append(carrierId);
                    sb.append("\r\nShip Method: ");
                    sb.append(shipMethod);
                    sb.append("\r\nDefaulting to ");
                    sb.append(ftdShipMethod);
                    sb.append("/");
                    sb.append(carrierDelivery);
                    sb.append("\r\nCheck to see if a new ship method has been added to SDS.");
                    LOGGER.error("processResponse -> " + sb.toString());
                    CommonUtils.getInstance().sendSystemMessage(sb.toString());
                }

                shipmentIsRated = true;
                venusMessageVO.setFillingVendor(selectedVendorCode);
                venusMessageVO.setVendorId(selectedVendor.getVendorId());
                venusMessageVO.setVendorSKU(selectedVendor.getVendorSku());
                venusMessageVO.setPrice(selectedVendor.getVendorCost());
                venusMessageVO.setFinalShipMethod(shipResponse.getShipMethod());
                venusMessageVO.setShipMethod(ftdShipMethod);
            }

            venusMessageVO.setFinalCarrier(carrierId);

            if (logisticsVO.isRateShopOnly())
            {
                venusMessageVO.setRatedShipment(shipmentIsRated);
                venusMessageVO.setForcedShipment(false);
                venusMessageVO.setVenusStatus(VenusStatus.PENDING);
                venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
                shipDAO.updateVenusSDS(connection, venusMessageVO);
                
                //#16015 - Decrement the pending order inventory. 
                LOGGER.debug("PENDING ORDER - DECREMENT INVENTORY");                
                processAutoshutdownCheck(venusMessageVO, connection);
                
                //Loop through the add ons and update the venus add ons table with vendor price and vendor add on id
                AddOnVO aovo;
                if (venusMessageVO.getVendorId() != null && !venusMessageVO.getVendorId().equalsIgnoreCase(""))
                {
                    for (int idx2 = 0; idx2 < venusMessageVO.getAddOnVO().size(); idx2++)
                    {
                        aovo = venusMessageVO.getAddOnVO().get(idx2);
                        HashMap vendorCostMap = aovo.getVendorCostsMap();
                        VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(venusMessageVO.getVendorId()));
                        if (vendorAddOnVO == null) {
                        	LOGGER.info("vendorAddOnVO is null");
                        } else {
                            shipDAO.updateVenusAddOns(connection, venusMessageVO.getVenusId(), vendorAddOnVO.getSKU(),
                                vendorAddOnVO.getCost(), vendorAddOnVO.getAddOnId());
                        }
                    }
                }
                GlobalParameterVO gpVO = CommonUtils.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT,
                        ShipConstants.GLOBAL_PARMS_MAX_SHIP_DAYS_OUT, String.valueOf(DEFAULT_MAX_SHIP_DAYS_OUT));
                int maxShipDaysOut = Integer.parseInt(gpVO.getValue());
                int dequeueStartHour = Integer.parseInt(getGlobalParameter(connection,
                        ShipConstants.GLOBAL_PARMS_CONTEXT,
                        ShipConstants.GLOBAL_PARMS_DELAYED_ORDER_DEQUEUE_START_HOUR,
                        String.valueOf(DEFAULT_DEQUEUE_HOUR)));

                long diff = computeMillisecondsToDelay(shipResponse.getShipDate(), maxShipDaysOut, dequeueStartHour);
                //The ship server may change the ship date to a date that is in range (diff==0),
                //but, we are already past the point
                //of getting a tracking number, so we are just going to send the order back through
                //to get the tracking number

                sb = new StringBuilder();
                if (shipmentIsRated)
                {
                    sb.append("Shipment has been rated.  Carrier and ship method has been selected.  A");
                }
                else
                {
                    sb.append("Unable to rate shipment at this time.  Shipment will be rated and a");
                }
                sb.append(" tracking number will be issued when the ship date is within 10 days of the current date.");
                createComment(connection, sb.toString(), orderDetailVO);
                enqueueJmsMessage(connection, JMSPipeline.SHIPPROCESSSHIP, venusMessageVO.getVenusId(), diff);
            }
            else
            {
                venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
                venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                venusMessageVO.setTrackingNumber(shipResponse.getTrackingNumber());
                shipDAO.updateVenusSDS(connection, venusMessageVO);

                //Loop through the add ons and update the venus add ons table with vendor price and vendor add on id
                AddOnVO aovo;
                for (int idx2 = 0; idx2 < venusMessageVO.getAddOnVO().size(); idx2++)
                {
                    aovo = venusMessageVO.getAddOnVO().get(idx2);
                    HashMap vendorCostMap = aovo.getVendorCostsMap();
                    VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(venusMessageVO.getVendorId()));
                    shipDAO.updateVenusAddOns(connection, venusMessageVO.getVenusId(), vendorAddOnVO.getSKU(),
                            vendorAddOnVO.getCost(), vendorAddOnVO.getAddOnId());
                }

                assert (shipResponse.getTrackingNumber() != null) : "Tracking number is null in SDSShipmentProcessingBO.processShipRequest";
                sb = new StringBuilder();
                sb.append("Your ");
                sb.append(logisticsVO.getShipUnitResponse().getCarrier());
                sb.append(" tracking number is ");
                sb.append(logisticsVO.getShipUnitResponse().getTrackingNumber());
                sb.append(".\r\nYour order is scheduled to ship on ");
                sb.append((new SimpleDateFormat(EMAIL_FORMAT)).format(logisticsVO.getShipUnitResponse().getShipDate()));
                sb.append(" for delivery on ");
                sb.append((new SimpleDateFormat(EMAIL_FORMAT)).format(logisticsVO.getMessageVo().getDeliveryDate()));
                sb.append(".");

                VenusMessageVO ansMsg = CommonUtils.copyVenusMessage(venusMessageVO, MsgType.ANSWER);
                ansMsg.setMessageDirection(MsgDirection.INBOUND);
                ansMsg.setMessageText(sb.toString());
                ansMsg.setOperator(ShipConstants.SDS_USER);
                ansMsg.setTransmissionTime(now);
                ansMsg.setComments(sb.toString());
                ansMsg.setVenusStatus(VenusStatus.VERIFIED);
                ansMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                shipDAO.insertVenusMessage(connection, ansMsg);

                //Update order with trackingNumber that was parsed from message text.
                OrderTrackingVO trackingVO = new OrderTrackingVO();
                trackingVO.setOrderDetailId(orderDetailVO.getOrderDetailId());
                trackingVO.setTrackingDescription("");
                trackingVO.setTrackingNumber(shipResponse.getTrackingNumber());
                trackingVO.setCarrierId(shipResponse.getCarrier());
                orderDAO.updateTrackingNumber(connection, trackingVO);

                try
                {
                    //update DCON status to pending if preferred partner.
                    PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
                    String prefPartner = null;
                    if (partnerVO != null)
                    {
                        prefPartner = partnerVO.getPartnerName();
                    }
                    LOGGER.debug("processResponse -> source code is:" + orderDetailVO.getSourceCode());
                    LOGGER.debug("processResponse -> prefpartner is:" + prefPartner);
                    if (StringUtils.isNotBlank(prefPartner))
                    {
                        orderDAO.updateDeliveryConfirmationStatus(connection, venusMessageVO.getReferenceNumber(),
                                ShipConstants.DCON_STATUS_PENDING);
                    }
                }
                catch (Exception e)
                {
                    LOGGER.error("processResponse -> " + e);
                    CommonUtils.getInstance().sendSystemMessage(
                            "Attempt to update DCON status to Pending failed. venus id:" + venusMessageVO.getVenusId());
                }

                //Defect 2619 - Inventory Tracking. Increment FTD message count against assigend product inventory.
                if (venusMessageVO.getInvTrkAssignedCountDate() == null)
                {
                    try
                    {
                        //if secondary ship date replaced original ship date then reallocate inventory count
                    	//from original ship date to secondary ship date
                    	if(venusMessageVO.getSecondaryShipDate() != null){
                    		shipDAO.reallocateInvTrkCount(connection, venusMessageVO);
                    	}
                    	shipDAO.updateInvTrkAssignedCount(connection, venusMessageVO);
                        processAutoshutdownCheck(venusMessageVO, connection);
                    }
                    catch (Exception e)
                    {
                        LOGGER.info("processResponse -> " + e);
                        CommonUtils.getInstance().sendSystemMessage(
                                "Product inventory tracking tasks failed for venus id:" + venusMessageVO.getVenusId()
                                        + ".\nReason:" + e.getMessage());
                    }
                }
            }

            if (shipmentIsRated)
            {
                //Update the order detail record
                orderDetailVO.setVendorId(selectedVendor.getVendorId());
                orderDetailVO.setCarrierDelivery(carrierDelivery);
                orderDetailVO.setFloristId(selectedVendorCode);
                orderDAO.updateOrderDetailCarrierInfo(connection, orderDetailVO);

                //Add vendor code to the order florist used table
                //(Defect 2795) 
                orderDAO.insertOrderFloristUsed(connection, orderDetailVO, selectedVendor.getVendorCode());
            }

            LOGGER.debug("processResponse -> ratedOnEntry = " + ratedOnEntry);
            if (!ratedOnEntry)
            {
                //decrement inventory //
                boolean invLevelHit = false;

                //if order has subcodes use the subcode
                String inventoryProductId = "";
                if (orderDetailVO.getSubcode() != null && orderDetailVO.getSubcode().length() > 0)
                {
                    inventoryProductId = orderDetailVO.getSubcode();
                }
                else
                {
                    inventoryProductId = orderDetailVO.getProductId();
                }

                invLevelHit = shipDAO.decrementProductInventory(connection, inventoryProductId,
                        venusMessageVO.getVendorId(), 1);

                LOGGER.debug("processResponse -> invLevelHit = " + invLevelHit);
                if (invLevelHit)
                {
                    sendInventoryNotifications(connection, inventoryProductId, venusMessageVO.getVendorId());
                }

            }

            //Failed - Already sent
        }
        else if (StringUtils.equals(shipResponse.getStatusCode(), SDSConstants.STATUS_DUPLICATE)
                && !venusMessageVO.isGetSdsResponse())
        {
            //If the order has already been processed as a duplicate, then process as any other error.
            String msg = "Received indication that we have already sent venus order "
                    + venusMessageVO.getVenusOrderNumber() + " to SDS.  Will mark venus order for data retrieval.";
            LOGGER.info("processResponse -> " + msg);

            venusMessageVO.setVenusStatus(VenusStatus.SHIP);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
            venusMessageVO.setGetSdsResponse(true);
            shipDAO.updateVenusSDS(connection, venusMessageVO);

            createComment(connection, msg, orderDetailVO);
            enqueueJmsMessage(connection, JMSPipeline.SHIPPROCESSSHIP, venusMessageVO.getVenusId(), REPROCESS_DELAY);

            return;

            //Failed - other reasons
        }
        else
        {
            String errorId = shipResponse.getStatusCode();
            String errorSystem = SDSConstants.STATUS_SDS;

            if (errorId == SDSConstants.STATUS_FEDEX_ERROR)
            {
                //Parse out the FedEx error
                errorSystem = SDSConstants.STATUS_FEDEX;
                String errMsg = shipResponse.getMsgText();
                if (errMsg == null)
                {
                    shipResponse.setMsgText("Unknown error from FEDEX server");
                }
                else
                {
                    try
                    {
                        int pos = errMsg.lastIndexOf(":");
                        errorId = errMsg.substring(pos + 1);
                    }
                    catch (Exception e)
                    {
                        errorId = SDSConstants.STATUS_FEDEX_ERROR;
                        errorSystem = SDSConstants.STATUS_SDS;
                    }
                }
            }

            venusMessageVO.setMessageText(shipResponse.getMsgText());
            venusMessageVO.setComments(shipResponse.getMsgText());
            venusMessageVO.setErrorDescription(String.valueOf(errorId));
            venusMessageVO.setVenusStatus(VenusStatus.ERROR);
            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            venusMessageVO.setRejectedStatusDate(now);
            shipDAO.updateVenusSDS(connection, venusMessageVO);

            SDSErrorVO errorVO = null;

            //No partner or no partner sds error record...go for the default
            if (errorVO == null)
            {
                errorVO = shipDAO.getErrorProcessing(connection, errorId, errorSystem);

                //Final check...never queue a partner order if they are configured
                //not to have their orders queued. Order will be cancelled in
                //a subsequent check.
                if (errorVO != null && SDSErrorDisposition.QUEUE.equals(errorVO.getDisposition()))
                {
                    errorVO = null;
                }
            }

            //Cannot find the error in the VENUS.SDS_ERRORS table
            if (errorVO == null)
            {
                errorVO = new SDSErrorVO();
                //Defect 1809
                String errMsg = "Unknown error return code received from SDS server:  (" + shipResponse.getStatusCode()
                        + ") " + shipResponse.getMsgText() + ".  ";

                if (errorVO.getDisposition() == null)
                {
                    errorVO.setDisposition(SDSErrorDisposition.CANCEL_ORDER);
                    errMsg += "  Order will be rejected in Apollo";
                    errMsg += ".";
                }
                else
                {
                    errMsg += "  Default disposition of " + errorVO.getDisposition() + " will be performed.";
                }
                errorVO.setPageSupport(true);
                errorVO.setOrderComment(errMsg);
                CommonUtils.getInstance().sendSystemMessage(errMsg);
            }

            sb = new StringBuilder();
            sb.append("Error received from SDS shipping system:");
            sb.append("\r\nCode: ");
            sb.append(errorVO.getErrorId());
            sb.append("\r\nMessage: ");
            sb.append(shipResponse.getMsgText());
            String orderComments = errorVO.getOrderComment();
            if (StringUtils.isNotBlank(orderComments))
            {
                sb.append("\r\n");
                sb.append(orderComments);
            }
            if (errorVO.isPageSupport())
            {
                sb.append("\r\n  ");
                sb.append(ConfigurationUtil.getInstance().getProperty(ShipConstants.CONFIG_FILE, "COMMENT_IT_NOTIFIED"));
            }

            VenusMessageVO rejectMsg = CommonUtils.copyVenusMessage(venusMessageVO, MsgType.REJECT);
            rejectMsg.setOperator(ShipConstants.SDS_USER);
            rejectMsg.setMessageDirection(MsgDirection.INBOUND);
            rejectMsg.setVenusStatus(VenusStatus.ERROR);
            rejectMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            rejectMsg.setTransmissionTime(now);
            rejectMsg.setMessageText(sb.toString());
            rejectMsg.setComments(sb.toString());
            String rejectId = shipDAO.insertVenusMessage(connection, rejectMsg);
            rejectMsg.setVenusId(rejectId);

            if (errorVO.isPageSupport())
            {
                CommonUtils.getInstance().sendSystemMessage("Error while processing ship request: " + sb.toString());
            }

            SDSErrorDisposition disp = errorVO.getDisposition();
            assert (disp != null) : "Error disposition = null in SDSShipProcessingBO:ProcessMessage";

            //Defect 1809
            if (SDSErrorDisposition.QUEUE.equals(disp))
            {
                sendToQueue(connection, rejectMsg, logisticsVO.getOrderDetailVO(), logisticsVO.getOrderVO());
            }
            else if (SDSErrorDisposition.INCREMENT_SHIP_DATE.equals(disp))
            {
                reprocessOrder(connection, logisticsVO, venusMessageVO.isForcedShipment(),
                        venusMessageVO.isRatedShipment(), true);
            }
            else
            { //Reprocess
                reprocessOrder(connection, logisticsVO, venusMessageVO.isForcedShipment(),
                        venusMessageVO.isRatedShipment());
            }
        }

    }

    public SDSLogisticsVO buildRequest(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {
        return buildRequest(connection, venusMessageVO, false);
    }

    public SDSLogisticsVO buildRequest(Connection connection, VenusMessageVO venusMessageVO, boolean retrieveAllVendors)
            throws Exception
    {
        //First gather all the data
        SDSLogisticsVO logisticsVO = getOrderData(connection, venusMessageVO);
        logisticsVO.setMessageVo(venusMessageVO);

        if (!venusMessageVO.isForcedShipment() && !venusMessageVO.isRatedShipment())
        {
            //Is the order within 10 days of ship day
            GlobalParameterVO gpVO = CommonUtils.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT,
                    ShipConstants.GLOBAL_PARMS_MAX_SHIP_DAYS_OUT, String.valueOf(DEFAULT_MAX_SHIP_DAYS_OUT));
            int maxShipDaysOut = Integer.parseInt(gpVO.getValue());

            if (willOrderShipAfterMaxDays(venusMessageVO, maxShipDaysOut))
            {
                logisticsVO.setRateShopOnly(true);
            }
        }

        //lookup VOs //
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        List<ShipVendorProductVO> availableVendors = null;

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();
        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        //Do we know what vendor/carrier/shipmethod
        if (venusMessageVO.isForcedShipment() || venusMessageVO.isRatedShipment())
        {
            availableVendors = new ArrayList<ShipVendorProductVO>();
            ShipVendorProductVO svpVO = new ShipVendorProductVO();
            svpVO.setVendorCode(StringUtils.replace(venusMessageVO.getFillingVendor(), "-", ""));
            svpVO.setVendorCost(venusMessageVO.getPrice());
            svpVO.setAvailable(true);
            svpVO.setVendorId(venusMessageVO.getVendorId());
            List<CarrierVO> carrierList = new ArrayList<CarrierVO>();
            CarrierVO cvo = new CarrierVO();
            cvo.setCarrierId(venusMessageVO.getFinalCarrier());
            carrierList.add(cvo);
            svpVO.setCarrierIds(carrierList);
            availableVendors.add(svpVO);
        }
        else
        {
            //Defect 3749 - trim the postal code to the appropriate number of characters
            String postalCode = StringUtils.upperCase(venusMessageVO.getZip());
            if (StringUtils.equals(venusMessageVO.getCountry(), "US"))
            {
                postalCode = StringUtils.substring(postalCode, 0, 5);
            }
            else if (StringUtils.equals(venusMessageVO.getCountry(), "CA"))
            {
                postalCode = StringUtils.substring(postalCode, 0, 3);
            }

            if (retrieveAllVendors)
                availableVendors = shipDAO.getShippingVendorsAll(connection, isSubcode ? subcodeId : productId,
                        venusMessageVO.getDeliveryDate(), venusMessageVO.getShipDate(), postalCode);
            else
                availableVendors = shipDAO.getShippingVendors(connection, isSubcode ? subcodeId : productId,
                        venusMessageVO.getDeliveryDate(), venusMessageVO.getShipDate(), postalCode);
        }
        logisticsVO.setAvailableVendors(availableVendors);

        if (venusMessageVO.isGetSdsResponse())
        {
            logisticsVO = buildGetShipmentInfoRequest(logisticsVO);
        }
        else
        {
            logisticsVO = generateRateShipUnitsRequest(connection, logisticsVO);
        }
        
        // #17789 - Log the vendors being sent to SDS
        try{
	        if(logisticsVO.getAvailableVendors() != null && logisticsVO.getAvailableVendors().size() > 0) {        	
		        StringBuffer vendors = new StringBuffer();
		        for (ShipVendorProductVO shipVendorProductVO : availableVendors) {
		        	if(shipVendorProductVO != null && shipVendorProductVO.getVendorId() != null) {
		        		vendors.append(shipVendorProductVO.getVendorId()).append(", ");
		        	}
				}	        
		        LOGGER.debug("VENDORS BEING SENT TO SDS : " + vendors.toString());
	        }
        }catch (Exception e) {
			LOGGER.error("Error logging the vendors being sent to SDS");
		}
        return logisticsVO;
    }

    private SDSLogisticsVO buildGetShipmentInfoRequest(SDSLogisticsVO logisticsVO) throws Exception
    {
        Document infoRequest = JAXPUtil.createDocument();
        Element root = infoRequest.createElement(SDSConstants.TAG_SHIPPING_INFO_ROOT);
        root.setAttribute("xmlns", SDSConstants.NAMESPACE_URI);
        infoRequest.appendChild(root);
        Element rqst = infoRequest.createElement(SDSConstants.TAG_SHIPPING_INFO_REQUEST);
        root.appendChild(rqst);

        rqst.appendChild(JAXPUtil.buildSimpleXmlNode(infoRequest, SDSConstants.TAG_CARTON_NUMBER,
                logisticsVO.getMessageVo().getVenusOrderNumber()));
        rqst.appendChild(infoRequest.createElement(SDSConstants.TAG_SHIPPING_INFO_DISTRIBUTION_CENTER));
        //rqst.appendChild(JAXPUtil.buildSimpleXmlNode(infoRequest,SDSConstants.TAG_SHIPPING_INFO_DISTRIBUTION_CENTER,SDSConstants.SHIPPING_INFO_DISTRIGUTION_CENTER));

        logisticsVO.setShipUnit(infoRequest);

        return logisticsVO;
    }

    //Defect 1809 - add logic for looking for the partner guarentee, partner label name, and custom phone number
    private SDSLogisticsVO generateRateShipUnitsRequest(Connection connection, SDSLogisticsVO logisticsVO) throws Exception
    {

        VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();
        CompanyVO companyVO = logisticsVO.getCompanyVO();
        List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
        ProductVO productVO = logisticsVO.getProductVO();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();

        boolean compAddonFlag = false;

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();
        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        Document shipUnit = JAXPUtil.createDocument();

        Element docRoot;
        //this is for PENDING order - Pending order must send a create ship but we have to build it like a rate request
        if (logisticsVO.isRateShopOnly())
            docRoot = shipUnit.createElement("CREATE_SHIP_UNITS_PARAMS");
        else
            docRoot = shipUnit.createElement("RATE_SHIP_UNITS_PARAMS");

        docRoot.setAttribute("xmlns", "http://ScanData.com/WTM/XMLSchemas/WTM_XMLSchema_14.00.0000.xsd");
        docRoot.setAttribute("MSN", "1");

        shipUnit.appendChild(docRoot);
        Element shipUnitRoot = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIP_UNIT);
        docRoot.appendChild(shipUnitRoot);
        logisticsVO.setShipUnit(shipUnit);

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_CARTON_NUMBER,
                venusMessageVO.getVenusOrderNumber()));

        //Per Email From Jason Chen at ScanData on 09/05/2006:
        //[Jason Chen] Please use <DistributionCenter></DistributionCenter>inside SHIP_UNITS XML
        //while performing RateShopping, the rate shop routine will use the proper DistributionCenter
        //from the DynamicData block.
        if (availableVendors.size() > 0)
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_DISTRIBUTION_CENTER,
                    StringUtils.replace(((ShipVendorProductVO) availableVendors.get(0)).getVendorCode(), "-", "")));
        }

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORDER_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
                SDSConstants.TAG_RATE_CREATE_DATE_BEST_METHOD_PARAMETER,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getDeliveryDate())));

        //add total add on weight to product weight to derive estimated order weight
        BigDecimal productWeight = new BigDecimal(venusMessageVO.getProductWeight());
        BigDecimal totalAddOnWeight = this.calculateTotalAddOnWeight(venusMessageVO);
        BigDecimal totalOrderWeight = productWeight.add(totalAddOnWeight);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ESTIMATED_WEIGHT,
                totalOrderWeight.toString()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_STATUS,
                logisticsVO.isRateShopOnly() ? SDSConstants.STATUS_PENDING : SDSConstants.STATUS_INITIAL));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getShipDate())));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_SHIP_VIA,
                logisticsVO.getShipVia()));

        //Per Email From Jason Chen at ScanData on 09/05/2006:
        //[Jason Chen] Yes, I do know. Please download <MasterIterID></MasterItemID?
        //In side SHIP_UNITS XML, the rate shop route will use the appropriate
        //MasterItemID from the DynamicData block.
        if (availableVendors.size() > 0)
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    ((ShipVendorProductVO) availableVendors.get(0)).getProductSkuId()));
        }
        else
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    venusMessageVO.getProductId()));
        }

        Element shipOptions = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS);
        shipUnitRoot.appendChild(shipOptions);

        String shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_COMMERCIAL;
        //Service options
        String serviceOptions = "";
        CustomerVO recipientVO = logisticsVO.getRecipientVO();
        if (recipientVO != null && StringUtils.equals(recipientVO.getAddressType(), ADDRESS_TYPE_RESIDENTIAL))
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_RESIDENTIAL;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(venusMessageVO.getDeliveryDate());
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_SATURDAY;
        }

        if (productVO.isOver21())
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_OVER21;
        }
        Element shipOptionElement = shipUnit.createElement(shipOption);
        shipOptions.appendChild(shipOptionElement);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_WEIGHT, totalOrderWeight.toString()));

        Element dynamicData = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_DATA_XML);
        Element origins = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS);

        //Loop through the vendors
        String vendorSku = null;
        String origVendorSku = null;
        for (int idx = 0; idx < availableVendors.size(); idx++)
        {
            ShipVendorProductVO svpVO = availableVendors.get(idx);
            vendorSku = svpVO.getVendorSku();
            origVendorSku = svpVO.getVendorSku();

            Element origin = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_ORIGIN);
            origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_DIST_CENTER,
                    StringUtils.replace(svpVO.getVendorCode(), "-", "")));
            Element carriers = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIERS);
            Element carrier = null;

            //Loop through the carriers
            CarrierVO cvo;
            for (int idx2 = 0; idx2 < svpVO.getCarrierIds().size(); idx2++)
            {
                cvo = svpVO.getCarrierIds().get(idx2);
                carrier = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER);
                carrier.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER_ID,
                        cvo.getCarrierId()));

                carriers.appendChild(carrier);
            }

            origin.appendChild(carriers);

            //add total add on cost to product cost to derive total vendor cost
            BigDecimal productVendorCost = new BigDecimal(svpVO.getVendorCost());
            BigDecimal totalVendorCost = productVendorCost.add(this.calculateTotalAddOnCost(venusMessageVO,
                    svpVO.getVendorId()));
            //Add the vendor cost and SKU
            origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_VENDOR_COST,
                    String.format("%6.2f", totalVendorCost.doubleValue())));
            origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_VENDOR_SKU, vendorSku));

            origins.appendChild(origin);

        }
        dynamicData.appendChild(origins);

        Element invoiceData = shipUnit.createElement(ShipConstants.TICKET_ROOT);

        //E number
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_ORDER,
                venusMessageVO.getVenusOrderNumber()));

        //Defect 1809
        //Add partner data to the label
        String originName = null;
        String originPhone = null;
        String guarantee = null;

        if (StringUtils.isBlank(originName))
        {
            originName = companyVO.getCompanyName();
        }

        if (StringUtils.isBlank(originPhone))
        {
            PartnerVO partnerMasterVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
            String prefPartner = null;
            if (partnerMasterVO != null)
            {
                prefPartner = partnerMasterVO.getPartnerName();
            }
            if (StringUtils.isNotBlank(prefPartner))
            {
                //String partnerPhoneNumber = oDAO.getContentWithFilter("", "TRANSFER_EXTENSION", "USAA", null);
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                originPhone = cu.getContentWithFilter(connection, ShipConstants.CONTENT_CONTEXT_PREFERRED_PARTNER,
                        ShipConstants.CONTENT_NAME_PREFERRED_PARTNER, partnerMasterVO.getPartnerName(), null);
            }
            else
                originPhone = companyVO.getPhoneNumber();
        }

        //Origin
        Element element = shipUnit.createElement(ShipConstants.TICKET_ORIGINATOR);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_NAME, originName);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_PHONE, originPhone);
        invoiceData.appendChild(element);

        //Recipient
        element = shipUnit.createElement(ShipConstants.TICKET_RECIPIENT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_NAME, venusMessageVO.getRecipient());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_COMPANY, venusMessageVO.getBusinessName());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR1, venusMessageVO.getAddress1());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR2, venusMessageVO.getAddress2());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_CITY, venusMessageVO.getCity());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_STATE, venusMessageVO.getState());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_POSTAL, venusMessageVO.getZip());
        invoiceData.appendChild(element);

        //Product
        element = shipUnit.createElement(ShipConstants.TICKET_PRODUCT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_PRODUCT_DESC, venusMessageVO.getProductDescription());
        invoiceData.appendChild(element);

        //Card message
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_CARD_MESSAGE,
                venusMessageVO.getCardMessage()));

        dynamicData.appendChild(invoiceData);

        //Guarantee (Leave blank if non is available.
        //SDS will default to the standard FTD.COM guarantee)
        //Defect 1809
        if (StringUtils.isBlank(guarantee))
        {
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_GUARANTEE));
        }
        else
        {
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_GUARANTEE, guarantee));
        }

        //Personal Greeting ID (Leave blank if none is available).
        if (StringUtils.isBlank(venusMessageVO.getPersonalGreetingID()))
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_PERSONAL_GREETING_ID));
        else
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_PERSONAL_GREETING_ID,
                    venusMessageVO.getPersonalGreetingID()));

        //Build the FTD_SHIP_UNITS section
        Element ftdShipUnits = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_FSU_FTD_SHIP_UNITS);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_ORDER_NUMBER,
                orderDetailVO.getExternalOrderNumber()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_ID,
                isSubcode ? orderDetailVO.getSubcode() : venusMessageVO.getProductId()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_DESC,
                venusMessageVO.getProductDescription()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_LOB,
                SDSConstants.LOB_FTDCOM));

        dynamicData.appendChild(ftdShipUnits);

        shipUnitRoot.appendChild(dynamicData);

        //Add recipient address
        Element address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);

        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_DELIVER_TO));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
                StringUtils.substring(venusMessageVO.getBusinessName(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_INDIVIDUAL_NAME,
                StringUtils.substring(venusMessageVO.getRecipient(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
                StringUtils.substring(venusMessageVO.getAddress1(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
                StringUtils.substring(venusMessageVO.getAddress2(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
                venusMessageVO.getCity()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
                venusMessageVO.getState()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
                venusMessageVO.getZip()));

        //FIXME: Need the three char country code from the database
        //shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,SDSConstants.TAG_SHIP_SHIP_FOR_COUNTRY,venusMessageVO.getCountry()));
        String countryCode = venusMessageVO.getCountry();
        if (StringUtils.equals("US", countryCode))
        {
            countryCode = "USA";
        }
        else if (StringUtils.equals("CA", countryCode))
        {
            countryCode = "CAN";
        }
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COUNTRY, countryCode));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_PHONE_NUMBER,
                venusMessageVO.getPhoneNumber()));
        shipUnitRoot.appendChild(address);

        if (logisticsVO.getAvailableVendors().size() > 0)
        {
        //Add return address
	        address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
	                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_RETURN));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
	                SDSConstants.TAG_RATE_CREATE_RETURN_ADDRESS_COMPANY_NAME));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
	                StringUtils.substring(availableVendors.get(0).getAddress1(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
	                StringUtils.substring(availableVendors.get(0).getAddress2(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
	        		availableVendors.get(0).getCity()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
	        		availableVendors.get(0).getState()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
	        		availableVendors.get(0).getZipCode()));
	        shipUnitRoot.appendChild(address);
        }

        Element fedexUnit = null;
        if (fedexUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element upsUnit = null;
        if (upsUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element intlUnit = null;
        //Complete this when we go intl
        if (intlUnit != null)
        {
            shipUnitRoot.appendChild(intlUnit);
        }

        return logisticsVO;
    }

    public SDSShipResponseVO parseCreateShipUnitsResponse(Document responseDoc, SDSLogisticsVO logisticsVO) throws SDSApplicationException
    {

        SDSShipResponseVO responseVO = new SDSShipResponseVO();

        //First look for an error node
        String errorNumber = null;
        String errorDescription = null;
        String strResponse = null;
        VenusMessageVO vmVO = logisticsVO.getMessageVo();

        List<ShipmentOptionVO> shipmentOptions = null;
        if (logisticsVO.getShipmentOptions() != null)
            shipmentOptions = logisticsVO.getShipmentOptions();

        ShipmentOptionVO soVO = null;
        if (shipmentOptions != null && shipmentOptions.get(0) != null)
            soVO = shipmentOptions.get(0);

        List<ShipVendorProductVO> availableVendors = null;
        if (logisticsVO.getAvailableVendors() != null && logisticsVO.getAvailableVendors().size() > 0)
            availableVendors = logisticsVO.getAvailableVendors();

        ShipVendorProductVO svpVO = null;
        if (availableVendors != null && availableVendors.get(0) != null)
            svpVO = availableVendors.get(0);

        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseCreateShipUnitsResponse -> Regular createShip response is " + strResponse);

        }
        catch (Exception e)
        {
            LOGGER.warn("parseCreateShipUnitsResponse -> Error received while trying to determine if an \"Error\" node exists.  This is not fatal.");
        }

        //In V14, status node does not exist. Once we get the response, check CREATE_SHIP_UNIT_DOC/ERROR
        //There will be cases where ScanData will send an error node at this level: example, ScanData cannot even parse the request
        try
        {
            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_CREATE_ERROR, SDSConstants.NAMESPACE_PREFIX,
                    SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseCreateShipUnitsResponse -> nlErrorMain.getLength() = " + nlErrorMain.getLength());

            if (nlErrorMain.getLength() > 0)
            {
                Element errorElementMain = (Element) nlErrorMain.item(0);
                errorNumber = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_NUMBER);
                errorDescription = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseCreateShipUnitsResponse -> errorNumber = " + errorNumber);
            LOGGER.debug("parseCreateShipUnitsResponse -> errorDescription = " + errorDescription);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseCreateShipUnitsResponse -> Failed to retrieve error info rate response.", e);
        }


        if (StringUtils.isBlank(errorNumber))
        {
            responseVO.setStatusCode("0");
            responseVO.setMsgText("");
            responseVO.setSuccess(true);

            String trackingNumber = null;
            try
            {
                NodeList nl = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_CREATE_SHIP_UNIT_RATE_INFO, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.NAMESPACE_URI);

                LOGGER.debug("parseCreateShipUnitsResponse -> nl.getLength() = " + nl.getLength());
                if (nl.getLength()>0)
                {
                    Element shipElem = (Element) nl.item(0);
                    trackingNumber = getFirstChildNoNull(shipElem, SDSConstants.TAG_TRACKING_NUMBER);
                }

                trackingNumber = StringUtils.trimToEmpty(trackingNumber);

            }
            catch (Exception e)
            {
                LOGGER.error("parseCreateShipUnitsResponse -> " + e);
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> Exception while locating ship methods in create response.",e);
            }

            //Check for carrier id
            if ( soVO == null || soVO.getCarrierId() == null || StringUtils.isBlank(soVO.getCarrierId()))
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> carrierId not found");
            else
                responseVO.setCarrier(soVO.getCarrierId());

            //Ship Method
            if ( soVO == null || soVO.getShipMethodId() == null || StringUtils.isBlank(soVO.getShipMethodId()))
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> shipMethodId not found");
            else
                responseVO.setShipMethod(soVO.getShipMethodId());

            //Origin
            if (svpVO == null || svpVO.getVendorCode() == null || StringUtils.isBlank(svpVO.getVendorCode()))
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> vendorCode not found");
            else
                responseVO.setSelectedOrigin(svpVO.getVendorCode());

            //Tracking Number
            if( StringUtils.isBlank(trackingNumber) )
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> trackingNumber not found");
            else
                responseVO.setTrackingNumber(trackingNumber);

            //ShipDate
            if (vmVO == null || vmVO.getShipDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> shipDate not found");
            else
                responseVO.setShipDate(vmVO.getShipDate());

            //Delivery Date
            if (vmVO == null || vmVO.getDeliveryDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponse -> deliveryDate not found");
            else
                responseVO.setDeliveryDate(vmVO.getDeliveryDate());
        }
        else
        {
            if (StringUtils.equals(errorNumber, SDSConstants.STATUS_SDS_APP_EXCEPTION))
            {
                //If an exception is being thrown from the sds ship server, then put
                //the entire response in the description field
                LOGGER.warn("parseCreateShipUnitsResponse -> The SDS ship server has thrown an exception.");
                errorDescription = strResponse;
            }
            else if (StringUtils.isBlank(errorDescription))
            {
                LOGGER.debug("parseCreateShipUnitsResponse -> No status description found in create response.  Putting entire response in message text.");
                errorDescription = strResponse;
            }
            else
                LOGGER.debug("parseCreateShipUnitsResponse -> The SDS ship server has thrown an error.  Error Number = " + errorNumber
                        + " and Error Description " + errorDescription);

            responseVO.setStatusCode(errorNumber);
            responseVO.setMsgText(errorDescription);
            responseVO.setSuccess(false);
        }

        return responseVO;
    }


    public SDSShipResponseVO parseCreateShipUnitsResponsePending(Document responseDoc, SDSLogisticsVO logisticsVO) throws SDSApplicationException
    {

        SDSShipResponseVO responseVO = new SDSShipResponseVO();

        //First look for an error node
        String errorNumber = null;
        String errorDescription = null;
        String strResponse = null;
        VenusMessageVO vmVO = logisticsVO.getMessageVo();

        List<ShipVendorProductVO> availableVendors = null;
        if (logisticsVO.getAvailableVendors() != null && logisticsVO.getAvailableVendors().size() > 0)
            availableVendors = logisticsVO.getAvailableVendors();

        ShipVendorProductVO svpVO = null;
        if (availableVendors != null && availableVendors.get(0) != null)
            svpVO = availableVendors.get(0);

        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseCreateShipUnitsResponsePending -> Pending createShip response is " + strResponse);

        }
        catch (Exception e)
        {
            LOGGER.warn("parseCreateShipUnitsResponsePending -> Error received while trying to determine if an \"Error\" node exists.  This is not fatal.");
        }

        //In V14, status node does not exist. Once we get the response, check CREATE_SHIP_UNIT_DOC/ERROR
        //There will be cases where ScanData will send an error node at this level: example, ScanData cannot even parse the request
        try
        {
            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_CREATE_ERROR, SDSConstants.NAMESPACE_PREFIX,
                    SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseCreateShipUnitsResponsePending -> nlErrorMain.getLength() = " + nlErrorMain.getLength());
            if (nlErrorMain.getLength() > 0)
            {
                Element errorElementMain = (Element) nlErrorMain.item(0);
                errorNumber = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_NUMBER);
                errorDescription = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseCreateShipUnitsResponsePending -> errorNumber = " + errorNumber);
            LOGGER.debug("parseCreateShipUnitsResponsePending -> errorDescription = " + errorDescription);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseCreateShipUnitsResponsePending -> Failed to retrieve error info rate response.", e);
        }


        if (StringUtils.isBlank(errorNumber))
        {
            responseVO.setStatusCode("0");
            responseVO.setMsgText("");
            responseVO.setSuccess(true);

            //carrier id
            responseVO.setCarrier(SDSConstants.STATUS_PENDING);

            //Ship Method
            responseVO.setShipMethod(null);

            //Origin
            if (svpVO == null || svpVO.getVendorCode() == null || StringUtils.isBlank(svpVO.getVendorCode()))
                throw new SDSApplicationException("parseCreateShipUnitsResponsePending -> vendorCode not found");
            else
                responseVO.setSelectedOrigin(svpVO.getVendorCode());

            //Tracking Number
            responseVO.setTrackingNumber(null);

            //ShipDate
            if (vmVO == null || vmVO.getShipDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponsePending -> shipDate not found");
            else
                responseVO.setShipDate(vmVO.getShipDate());

            //Delivery Date
            if (vmVO == null || vmVO.getDeliveryDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponsePending -> deliveryDate not found");
            else
                responseVO.setDeliveryDate(vmVO.getDeliveryDate());
        }
        else
        {
            if (StringUtils.equals(errorNumber, SDSConstants.STATUS_SDS_APP_EXCEPTION))
            {
                //If an exception is being thrown from the sds ship server, then put
                //the entire response in the description field
                LOGGER.warn("parseCreateShipUnitsResponsePending -> The SDS ship server has thrown an exception.");
                errorDescription = strResponse;
            }
            else if (StringUtils.isBlank(errorDescription))
            {
                LOGGER.debug("parseCreateShipUnitsResponsePending -> No status description found in create response.  Putting entire response in message text.");
                errorDescription = strResponse;
            }
            else
                LOGGER.debug("parseCreateShipUnitsResponsePending -> The SDS ship server has thrown an error.  Error Number = " + errorNumber
                        + " and Error Description " + errorDescription);

            responseVO.setStatusCode(errorNumber);
            responseVO.setMsgText(errorDescription);
            responseVO.setSuccess(false);
        }

        return responseVO;
    }


    public SDSShipResponseVO parseCreateShipUnitsResponseZoneJump(Document responseDoc, SDSLogisticsVO logisticsVO, int availableVendorIdx)
        throws SDSApplicationException
    {
        SDSShipResponseVO responseVO = new SDSShipResponseVO();

        //First look for an error node
        String errorNumber = null;
        String errorDescription = null;
        String strResponse = null;

        VenusMessageVO vmVO = logisticsVO.getMessageVo();

        List<ShipVendorProductVO> availableVendors = null;
        if (logisticsVO.getAvailableVendors() != null && logisticsVO.getAvailableVendors().size() > 0)
            availableVendors = logisticsVO.getAvailableVendors();

        //use specified index
        ShipVendorProductVO svpVO = null;
        if (availableVendors != null && availableVendors.get(availableVendorIdx) != null)
            svpVO = availableVendors.get(availableVendorIdx);

        SDSZoneJumpTripVO trip = null;
        if (svpVO != null)
            trip = svpVO.getZoneJumpTrip();


        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseCreateShipUnitsResponseZoneJump -> ZoneJump createShip response is " + strResponse);

        }
        catch (Exception e)
        {
            LOGGER.warn("parseCreateShipUnitsResponseZoneJump -> Error received while trying to determine if an \"Error\" node exists.  This is not fatal.");
        }

        //In V14, status node does not exist. Once we get the response, check CREATE_SHIP_UNIT_DOC/ERROR
        //There will be cases where ScanData will send an error node at this level: example, ScanData cannot even parse the request
        try
        {
            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_CREATE_ERROR, SDSConstants.NAMESPACE_PREFIX,
                    SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> nlErrorMain.getLength() = " + nlErrorMain.getLength());
            if (nlErrorMain.getLength() > 0)
            {
                Element errorElementMain = (Element) nlErrorMain.item(0);
                errorNumber = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_NUMBER);
                errorDescription = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorNumber = " + errorNumber);
            LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorDescription = " + errorDescription);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> Failed to retrieve error info rate response.", e);
        }


        if (StringUtils.isBlank(errorNumber))
        {
            responseVO.setStatusCode("0");
            responseVO.setMsgText("");
            responseVO.setSuccess(true);

            String trackingNumber = null;
            try
            {
                NodeList nl = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_CREATE_SHIP_UNIT_RATE_INFO, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.NAMESPACE_URI);

                LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> nl.getLength() = " + nl.getLength());
                if (nl.getLength()>0)
                {
                    Element shipElem = (Element) nl.item(0);
                    trackingNumber = getFirstChildNoNull(shipElem, SDSConstants.TAG_TRACKING_NUMBER);
                }

                trackingNumber = StringUtils.trimToEmpty(trackingNumber);

            }
            catch (Exception e)
            {
                LOGGER.error("parseCreateShipUnitsResponseZoneJump -> " + e);
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> Exception while locating ship methods in create response.",e);
            }

            //Check for carrier id
            if ( trip == null || trip.getCarrierId() == null || StringUtils.isBlank(trip.getCarrierId()))
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> carrierId not found");
            else
                responseVO.setCarrier(trip.getCarrierId());

            //Ship Method
            if ( trip == null || trip.getShipMethod() == null || StringUtils.isBlank(trip.getShipMethod()))
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> shipMethodId not found");
            else
                responseVO.setShipMethod(trip.getShipMethod());

            //Origin
            if (svpVO == null || svpVO.getVendorCode() == null || StringUtils.isBlank(svpVO.getVendorCode()))
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> vendorCode not found");
            else
                responseVO.setSelectedOrigin(svpVO.getVendorCode());

            //Tracking Number
            if( StringUtils.isBlank(trackingNumber) )
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> trackingNumber not found");
            else
                responseVO.setTrackingNumber(trackingNumber);

            //ShipDate
            if (vmVO == null || vmVO.getShipDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> shipDate not found");
            else
                responseVO.setShipDate(vmVO.getShipDate());

            //Delivery Date
            if (vmVO == null || vmVO.getDeliveryDate() == null)
                throw new SDSApplicationException("parseCreateShipUnitsResponseZoneJump -> deliveryDate not found");
            else
                responseVO.setDeliveryDate(vmVO.getDeliveryDate());

        }
        else
        {
            if (StringUtils.equals(errorNumber, SDSConstants.STATUS_SDS_APP_EXCEPTION))
            {
                //If an exception is being thrown from the sds ship server, then put
                //the entire response in the description field
                LOGGER.warn("parseCreateShipUnitsResponseZoneJump -> The SDS ship server has thrown an exception.");
                errorDescription = strResponse;
            }
            else if (StringUtils.isBlank(errorDescription))
            {
                LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> No status description found in create response.  Putting entire response in message text.");
                errorDescription = strResponse;
            }
            else
                LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> The SDS ship server has thrown an error.  Error Number = " + errorNumber
                        + " and Error Description " + errorDescription);

            responseVO.setStatusCode(errorNumber);
            responseVO.setMsgText(errorDescription);
            responseVO.setSuccess(false);
        }

        return responseVO;
    }



    public SDSShipResponseVO parseGetShipUnitsInfoResponse(Document responseDoc) throws SDSApplicationException
    {
        SDSShipResponseVO responseVO = new SDSShipResponseVO();

        //First look for an error node
        String Error = null;
        String strResponse = null;
        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseGetShipUnitsInfoResponse -> shipUnitInfo response is " + strResponse);
            Error = JAXPUtil.selectSingleNodeText(responseDoc, "//Error", "",
                    SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
        }
        catch (Exception e)
        {
            LOGGER.warn("parseGetShipUnitsInfoResponse -> Error received while trying to determine if an \"Error\" node exists.  This is not fatal.");
        }
        if (StringUtils.isNotBlank(Error))
        {
            LOGGER.error("parseGetShipUnitsInfoResponse -> Error returned (in non standard format) from SDS server.");
        }
        String statusCode;
        try
        {
            statusCode = JAXPUtil.selectSingleNodeText(responseDoc, SDSConstants.XPATH_SHIPPING_INFO_STATUS_CODE_XPATH,
                    SDSConstants.NAMESPACE_PREFIX, SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Failed to retrieve status code in ship status response.", e);
        }

        if (StringUtils.isBlank(statusCode))
        {
            throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> No status code found in ship request response.");
        }
        responseVO.setStatusCode(statusCode);

        String statusDesc;
        try
        {
            statusDesc = JAXPUtil.selectSingleNodeText(responseDoc, SDSConstants.XPATH_SHIPPING_INFO_STATUS_DESC_XPATH,
                    SDSConstants.NAMESPACE_PREFIX, SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Failed to retrieve status description in ship status response.", e);
        }

        if (StringUtils.equals(statusCode, SDSConstants.STATUS_SDS_APP_EXCEPTION))
        {
            //If an exception is being thrown from the sds ship server, then put
            //the entire response in the description field
            LOGGER.warn("parseGetShipUnitsInfoResponse -> The SDS ship server has thrown an exception.");
            statusDesc = strResponse;
        }
        else if (StringUtils.isBlank(statusDesc))
        {
            LOGGER.debug("parseGetShipUnitsInfoResponse -> No status description found in ship request response.  Putting entire response in message text.");
            statusDesc = strResponse;
        }
        else
        {
            responseVO.setMsgText(statusDesc);
        }

        if (statusCode.equals(SDSConstants.STATUS_OK))
        {
            responseVO.setSuccess(true);

            //Check for carrier id
            String carrierId;
            try
            {
                carrierId = JAXPUtil.selectSingleNodeText(responseDoc,
                        SDSConstants.XPATH_SHIPPING_INFO_CARRIER_ID_XPATH, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                carrierId = StringUtils.trimToEmpty(carrierId);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Exception while locating carrier id in ship request response.", e);
            }

            if (StringUtils.isBlank(carrierId))
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> No carrier id ship request response");
            }
            responseVO.setCarrier(carrierId);

            //Ship Method
            String shipMethod = null;
            try
            {
                shipMethod = JAXPUtil.selectSingleNodeText(responseDoc,
                        SDSConstants.XPATH_SHIPPING_INFO_SHIPMETHOD_ID_XPATH, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                shipMethod = StringUtils.trimToEmpty(shipMethod);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Exception while locating ship method in ship request response.", e);
            }

            if (StringUtils.isBlank(shipMethod))
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Empty ship method element in ship request response");
            }
            responseVO.setShipMethod(shipMethod);

            //Origin
            String origin = null;
            try
            {
                origin = JAXPUtil.selectSingleNodeText(responseDoc, SDSConstants.XPATH_SHIPPING_INFO_ORIGIN_ID_XPATH,
                        SDSConstants.NAMESPACE_PREFIX, SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                origin = StringUtils.trimToEmpty(origin);
                origin = StringUtils.substring(origin, 0, 2) + "-" + StringUtils.substring(origin, 2);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Exception while locating origin in ship request response.", e);
            }

            if (StringUtils.isBlank(origin))
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Empty origin element in ship request response.");
            }
            responseVO.setSelectedOrigin(origin);

            //Tracking Number
            String trackingNumber = null;
            try
            {
                trackingNumber = JAXPUtil.selectSingleNodeText(responseDoc,
                        SDSConstants.XPATH_SHIPPING_INFO_TRACKING_NUMBER_XPATH, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                trackingNumber = StringUtils.trimToEmpty(trackingNumber);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Exception while locating tracking number in ship request response.",
                        e);
            }

            if (StringUtils.isBlank(trackingNumber))
            {
                LOGGER.warn("parseGetShipUnitsInfoResponse -> Empty tracking number element in ship request response.  This may have been a \"RATE ONLY\" request.");
            }
            responseVO.setTrackingNumber(trackingNumber);

            //ShipDate
            String shipDate;
            try
            {
                shipDate = JAXPUtil.selectSingleNodeText(responseDoc, SDSConstants.XPATH_SHIPPING_INFO_SHIP_DATE_XPATH,
                        SDSConstants.NAMESPACE_PREFIX, SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                shipDate = StringUtils.trimToEmpty(shipDate);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Error retrieving ship date from ship request response.", e);
            }

            if (StringUtils.isBlank(shipDate))
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> No ship date found in ship request response.");
            }

            try
            {
                responseVO.setShipDate(tzDateTimeToDate(shipDate));
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Unable to parse ship date of " + shipDate
                        + " in ship request response");
            }

            //Delivery Date
            String deliveryDate;
            try
            {
                deliveryDate = JAXPUtil.selectSingleNodeText(responseDoc,
                        SDSConstants.XPATH_SHIPPING_INFO_DELIVERY_DATE_XPATH, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.XPATH_SHIPPING_INFO_NAMESPACE_URI);
                deliveryDate = StringUtils.trimToEmpty(shipDate);
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Error retrieving delivery date from ship request response.", e);
            }

            if (StringUtils.isBlank(deliveryDate))
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> No delivery date found in ship request response.");
            }

            try
            {
                responseVO.setDeliveryDate(tzDateTimeToDate(deliveryDate));
            }
            catch (Exception e)
            {
                throw new SDSApplicationException("parseGetShipUnitsInfoResponse -> Unable to parse delivery date of " + deliveryDate
                        + " in ship request response");
            }

        }
        else
        {
            responseVO.setSuccess(false);
        }

        return responseVO;

    }

    public SDSLogisticsVO getOrderData(Connection connection, VenusMessageVO venusMessageVO)
            throws SDSApplicationException
    {
        SDSLogisticsVO logisticsVO = new SDSLogisticsVO();

        try
        {
            //lookup VOs //
            OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection, venusMessageVO.getReferenceNumber());
            if (orderDetailVO == null)
            {
                throw new SDSApplicationException("getOrderData -> No order detail record for the given order detail id: "
                        + venusMessageVO.getReferenceNumber());
            }
            logisticsVO.setOrderDetailVO(orderDetailVO);

            OrderVO orderVO = orderDAO.getOrder(connection, orderDetailVO.getOrderGuid());
            if (orderVO == null)
            {
                throw new SDSApplicationException("getOrderData -> No order record for given order guid: "
                        + orderDetailVO.getOrderGuid());
            }
            logisticsVO.setOrderVO(orderVO);

            CompanyVO companyVO = orderDAO.getCompany(connection, orderVO.getCompanyId());
            if (companyVO == null)
            {
                throw new SDSApplicationException("getOrderData -> No company record for given company id: " + orderVO.getCompanyId());
            }
            logisticsVO.setCompanyVO(companyVO);

            CustomerVO recipientVO = orderDAO.getCustomer(connection, orderDetailVO.getRecipientId());
            if (recipientVO == null)
            {
                throw new SDSApplicationException("getOrderData -> No customer record for the given recipient id: "
                        + orderDetailVO.getRecipientId());
            }
            logisticsVO.setRecipientVO(recipientVO);

            ProductVO productVO = orderDAO.getProduct(connection, orderDetailVO.getProductId());
            if (productVO == null)
            {
                throw new SDSApplicationException("getOrderData -> No product record for the given id: "
                        + venusMessageVO.getProductId());
            }
            logisticsVO.setProductVO(productVO);

            String shipVia = shipDAO.getShipVia(connection, venusMessageVO.getShipMethod(), productVO.isExpressOnly());
            logisticsVO.setShipVia(shipVia);

        }
        catch (SDSApplicationException sApp)
        {
            throw sApp;
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("getOrderData -> Error collecting data to process venus order "
                    + venusMessageVO.getVenusOrderNumber(), e);
        }

        return logisticsVO;
    }

    /**
     * Cancel a tracking number by generating a cancel message
     *
     * @param conn
     *            database connection
     * @param originalFTDOrderVO
     *            Order vo containing tracking number to cancel
     * @throws SDSApplicationException
     */
    protected void cancelTrackingNumber(Connection conn, VenusMessageVO originalFTDOrderVO)
            throws SDSApplicationException
    {
        try
        {
            if (originalFTDOrderVO.equals(MsgType.ORDER)
                    && StringUtils.isNotBlank(originalFTDOrderVO.getTrackingNumber()))
            {
                //Create a cancel message
                //Processing of the cancel message will decrement the inventory if necessary.
                VenusMessageVO cancelMessage = CommonUtils.copyVenusMessage(originalFTDOrderVO, MsgType.CANCEL);
                cancelMessage.setComments("Order automatically cancelled by Argo processing.");
                cancelMessage.setMessageText("Order automatically cancelled by Argo processing.");
                cancelMessage.setCancelReasonCode("SHP");
                cancelMessage.setMessageDirection(MsgDirection.OUTBOUND);
                cancelMessage.setTransmissionTime(new java.util.Date());
                cancelMessage.setVenusStatus(VenusStatus.OPEN);
                cancelMessage.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);

                shipDAO.insertVenusMessage(conn, cancelMessage);
            }
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("cancelTrackingNumber -> " + e);
        }
    }

    //Defect 1809
    /**
     * This method will find out how many times an order was sent to Venus. If the number of times it was sent out is
     * less then a global setting the order will be sent again. Otherwise a message will be placed in the FTD Queue.
     *
     * @param conn
     *            database connection
     * @param logisticsVO
     *            contains all the order related VOs
     * @param markForced
     *            mark the order forced
     * @param markRated
     *            mark the order rated
     * @throws SDSApplicationException
     *             for all errors and exceptions
     */
    private void reprocessOrder(Connection conn, SDSLogisticsVO logisticsVO, boolean markForced, boolean markRated)
            throws SDSApplicationException
    {
        reprocessOrder(conn, logisticsVO, markForced, markRated, false);
    }

    /**
     * This method will find out how many times an order was sent to Venus. If the number of times it was sent out is
     * less then a global setting the order will be sent again. Otherwise a message will be placed in the FTD Queue.
     *
     * @param conn
     *            database connection
     * @param logisticsVO
     *            contains all the order related VOs
     * @param markForced
     *            mark the order forced
     * @param markRated
     *            mark the order rated
     * @param incrementShipDate
     *            increment the ship date to the next ship date
     * @throws SDSApplicationException
     *             for all errors and exceptions
     */
    protected void reprocessOrder(Connection conn, SDSLogisticsVO logisticsVO, boolean markForced, boolean markRated,
            boolean incrementShipDate) throws SDSApplicationException
    {
        try
        {
            //Get Needed Data
            VenusMessageVO originalFTDOrderVO;
            VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();
            if (MsgType.ORDER.equals(venusMessageVO.getMsgType()))
            {
                originalFTDOrderVO = venusMessageVO;
            }
            else if (MsgType.REJECT.equals(venusMessageVO.getMsgType()))
            {
                List<VenusMessageVO> orders = shipDAO.getVenusMessage(conn, venusMessageVO.getVenusOrderNumber(),
                        MsgType.ORDER.getFtdMsgType());
                if (orders.size() == 0)
                {
                    String errorMsg = "Unable to locate FTD message in venus.venus for reject (venus_id "
                            + logisticsVO.getMessageVo().getVenusId() + ").";
                    LOGGER.error("reprocessOrder -> " + errorMsg);
                    throw new SDSApplicationException("reprocessOrder -> " + errorMsg);
                }
                originalFTDOrderVO = orders.get(0);
                markForced = originalFTDOrderVO.isForcedShipment();
                markRated = originalFTDOrderVO.isRatedShipment();
            }
            else
            {
                String strMsg = "Message type of " + venusMessageVO.getMsgType().toString()
                        + " passed to method reprocessOrder.  Only types of " + MsgType.ORDER.toString() + " or "
                        + MsgType.REJECT + " can be processed.";
                LOGGER.error("reprocessOrder -> " + strMsg);
                throw new SDSApplicationException("reprocessOrder -> " + strMsg);
            }

            OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
            OrderVO orderVO = logisticsVO.getOrderVO();
            CustomerVO recipientVO = logisticsVO.getRecipientVO();

            //add comment to order
            createComment(conn, "Apollo ship processing will attempt to reprocess this order.", orderDetailVO);

            //Defect 1809
            boolean bResendOrder = true;
            boolean bQueueOrder = false;
            Date newDeliveryDate = null;
            Date newShipDate = null;
            String strMessage = "";
            String cancelReasonCode = null;

            if (bResendOrder)
            {
                //Find out how many times this order has been sent out.
                int timesSent = shipDAO.getNumberOfTimesSent(conn, originalFTDOrderVO.getReferenceNumber(),
                        MsgType.ORDER.getFtdMsgType());

                //Get the number of times a message should be resent.
                GlobalParameterVO globalVO = orderDAO.getGlobalParameter(conn, "ORDER_PROCESSING_VENUS",
                        "VENUS_FTD_RETRY_AMOUNT");
                int retryMax = Integer.parseInt(globalVO.getValue());

                //If we tried the max amount of times
                if (timesSent >= retryMax)
                {
                    LOGGER.info("reprocessOrder -> Order cannot be resent, placing into the FTD queue. timesSent=" + timesSent
                            + "retryMax=" + retryMax);

                    bResendOrder = false;

                    bQueueOrder = true;
                }
                else
                {
                    //resend the order
                    LOGGER.debug("reprocessOrder -> Resending Order. timesSent=" + timesSent + "retryMax=" + retryMax);

                    boolean canStillDeliver = true;
                    if (incrementShipDate)
                    {
                        newDeliveryDate = getNextAvailableDeliveryDate(conn, logisticsVO);
                        newShipDate = computeShipDate(conn, logisticsVO, newDeliveryDate);
                        if (newDeliveryDate == null || newShipDate == null)
                        {
                            canStillDeliver = false;

                            bQueueOrder = true;
                        }
                        else
                        {
                            canStillDeliver = true;
                        }
                    }
                    else
                    {
                        //if ship method is next day check if can be shipped out again based on delivery date
                        if (orderDetailVO.getShipMethod().equals(ShipConstants.SHIPPING_METHOD_NEXTDAY))
                        {
                            canStillDeliver = canResend(recipientVO.getCountry(), orderDetailVO.getProductId(),
                                    recipientVO.getZipCode(), orderDetailVO.getOccasion(), recipientVO.getState(),
                                    orderDetailVO.getDeliveryDate(), orderDetailVO.getShipMethod(), conn);
                        }
                    }

                    bResendOrder = canStillDeliver;

                    if (!bResendOrder)
                    {
                        LOGGER.debug("reprocessOrder -> Order detail id " + venusMessageVO.getReferenceNumber() + " cannot be resent.");
                    }
                }
            }

            if (bResendOrder)
            {
                //Now create a new FTD message
                VenusMessageVO newFTD = CommonUtils.copyVenusMessage(originalFTDOrderVO, MsgType.ORDER);
                newFTD.setVenusStatus(VenusStatus.SHIP);
                newFTD.setMessageDirection(MsgDirection.OUTBOUND);
                newFTD.setVenusStatus(VenusStatus.SHIP);
                newFTD.setForcedShipment(markForced);
                newFTD.setRatedShipment(markRated);
                newFTD.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
                newFTD.setVenusOrderNumber("E"
                        + StringUtils.leftPad(String.valueOf(shipDAO.generateVenusOrderNumber(conn)),
                                DIGITS_IN_ORDER_NUMBER, "0"));
                if (incrementShipDate)
                {
                    boolean bNewDeliveryDate = false;
                    if (newDeliveryDate != null && newDeliveryDate.compareTo(newFTD.getDeliveryDate()) != 0)
                    {
                        bNewDeliveryDate = true;
                        newFTD.setDeliveryDate(newDeliveryDate);

                        if (newShipDate != null)
                        {
                            newFTD.setShipDate(newShipDate);
                        }
                    }

                    //Save an order comment indicating that the delivery/ship dates have changed
                    StringBuilder sb = new StringBuilder();
                    sb.append("Ship date has been changed to ");
                    sb.append((new SimpleDateFormat(MMDDYYYY_FORMAT)).format(newFTD.getShipDate()));
                    sb.append(" in an attempt to reprocess the order.");
                    if (bNewDeliveryDate)
                    {
                        sb.append("  The delivery date has been changed to ");
                        sb.append((new SimpleDateFormat(MMDDYYYY_FORMAT)).format(newDeliveryDate));
                    }
                    createComment(conn, sb.toString(), logisticsVO.getOrderDetailVO());

                    //Defect 1809
                    //Save the date changes to the clean.order_details record
                    if (bNewDeliveryDate)
                    {
                        logisticsVO.getOrderDetailVO().setDeliveryDate(newDeliveryDate);

                        if (newShipDate != null)
                        {
                            logisticsVO.getOrderDetailVO().setShipDate(newShipDate);
                        }
                    }

                    orderDAO.updateOrder(conn, logisticsVO.getOrderDetailVO());
                }
                String newId = shipDAO.insertVenusMessage(conn, newFTD);

                //Put out on the JMS queue
                enqueueJmsMessage(conn, JMSPipeline.SHIPPROCESSSHIP, newId);
            }
            else
            {
                if (bQueueOrder)
                {
                    //insert into queue
                    QueueVO retQueue = new QueueVO();

                    //Always queue the venus message that is in the logisticsVO
                    retQueue.setQueueType(venusMessageVO.getMsgType().getFtdMsgType());
                    retQueue.setMessageType(venusMessageVO.getMsgType().getFtdMsgType());
                    retQueue.setSystem(ShipConstants.SDS_USER);
                    retQueue.setMercuryNumber(venusMessageVO.getVenusOrderNumber());

                    retQueue.setMasterOrderNumber(orderVO.getMasterOrderNumber());
                    retQueue.setOrderGuid(orderDetailVO.getOrderGuid());
                    retQueue.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
                    retQueue.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
                    retQueue.setMessageTimestamp(new java.util.Date());
                    queueDAO.insertQueueRecord(conn, retQueue);
                }

            }

        }
        catch (Exception e)
        {
            throw new SDSApplicationException("reprocessOrder -> Received error while trying to reprocess FTD shipment.", e);
        }
    }

    private boolean canResend(String country, String product, String zip, String occasion, String state,
            Date deliveryDate, String deliveryMethod, Connection conn) throws Exception
    {
        boolean canResend = false;

        //check if the deliery date on the order is still valid
        DeliveryDateUTIL util = new DeliveryDateUTIL();

        //get number of days before delivery date
        long diffMillis = deliveryDate.getTime() - (new Date()).getTime();
        int diffDays = new Long(diffMillis / (24 * 60 * 60 * 1000)).intValue() + 1;

        OEDeliveryDateParm oeParms = util.getCOMParameterData(country, getProductMasterId(product, conn), zip, "CA",
                "US", "1", occasion, conn, state, "FTD", false, false, diffDays);
        if (util.validDelivery(oeParms, deliveryMethod, deliveryDate))
        {
            canResend = true;
        }

        return canResend;
    }

    /**
     * Determines what the underlying product id is for the passed product id/sub-code
     *
     * @param productId
     * @param conn
     * @return the product id from ftd_apps.product_master table.
     */
    private String getProductMasterId(String productId, Connection conn)
    {
        String productMasterId = null;
        try
        {
            productMasterId = shipDAO.getProductMasterId(conn, productId);
        }
        catch (Exception e)
        {
            productMasterId = productId;
        }

        return productMasterId;
    }

    protected boolean willOrderShipAfterMaxDays(VenusMessageVO vo, int maxShipDaysOut)
    {

        assert (vo != null) : "VenusMessageVO is null in SDSProcessingBO.willOrderShipAfterMaxDays";
        assert (vo.getShipDate() != null) : "Ship date is null in SDSProcessingBO.willOrderShipAfterMaxDays";
        Calendar testDate = GregorianCalendar.getInstance();

        testDate.clear(Calendar.HOUR_OF_DAY);
        testDate.clear(Calendar.MINUTE);
        testDate.clear(Calendar.SECOND);
        testDate.clear(Calendar.MILLISECOND);
        testDate.set(Calendar.AM_PM, Calendar.AM);

        testDate.add(Calendar.DATE, maxShipDaysOut);
        return vo.getShipDate().getTime() - testDate.getTimeInMillis() >= 0;
    }

    public long computeMillisecondsToDelay(Date shipDate, int maxShipDaysOut, int dequeueHour)
    {
        assert (shipDate != null) : "Ship date is null in SDSProcessingBO.computeMillisecondsToDelay";

        Calendar testDate = GregorianCalendar.getInstance();

        testDate.clear(Calendar.HOUR_OF_DAY);
        testDate.clear(Calendar.MINUTE);
        testDate.clear(Calendar.SECOND);
        testDate.clear(Calendar.MILLISECOND);
        testDate.set(Calendar.AM_PM, Calendar.AM);

        testDate.add(Calendar.DATE, maxShipDaysOut - 1);

        long diff = shipDate.getTime() - testDate.getTimeInMillis();

        if (diff < 0)
        {
            diff = 0;
        }

        //See defect 5797 - this is a temporary work around since we don't want
        //to disturb too much before Mothers Day, so we leave the determination
        //of if there should be a delay untouched above.
        //The final fix is that this method and willOrderShipAfterMaxDays should
        //use common logic for the date comparison.
        //Also the date comparison math is incorrect due to use of "clear" (instead of "set").
        //
        if (diff > 0)
        {
            Calendar maxDaysOutDate = GregorianCalendar.getInstance();
            maxDaysOutDate.add(Calendar.DATE, maxShipDaysOut - 1);

            //Adjust ship date hour to be at dequeueHour.
            //Note we don't clear minutes/secs so all are not set to dequeue at top of dequeueHour
            Calendar shipDateAdjusted = GregorianCalendar.getInstance();
            shipDateAdjusted.setTime(shipDate);
            shipDateAdjusted.set(Calendar.HOUR_OF_DAY, dequeueHour);

            long diff2 = shipDateAdjusted.getTimeInMillis() - maxDaysOutDate.getTimeInMillis();
            if (diff2 > 0)
            { //Just to be extra cautious
                diff = diff2;
                if (LOGGER.isDebugEnabled())
                {
                    Calendar dequeueDate = GregorianCalendar.getInstance();
                    dequeueDate.add(Calendar.SECOND, (new Long(diff2 / 1000)).intValue());
                    LOGGER.debug("computeMillisecondsToDelay -> Ship date is: " + shipDate + " so dequeue date is: " + dequeueDate.getTime());
                }
            }
        }

        return diff;
    }

    public void sendInventoryNotifications(Connection connection, String productId, String vendorId) throws Exception
    {
        LOGGER.debug("sendInventoryNotifications -> Inventory level hit for product " + productId);

        try
        {
            //retrieve list of email addresses to send notifcation to
            List notifyList = shipDAO.getInventoryNotifications(connection, productId, vendorId);

            //retrieve inventory data for product
            Document xml = shipDAO.getInventoryRecordXML(connection, productId, vendorId);

            LOGGER.debug("sendInventoryNotifications -> " + JAXPUtil.toString(xml));

            //if list is not null
            if (notifyList != null)
            {

                StockMessageGenerator messageGenerator = new StockMessageGenerator();

                //get the sender email address from config file
                ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
                String fromEmailAddress = configUtil.getProperty(ShipConstants.CONFIG_FILE,
                        "INVENTORY_EMAIL_FROM_ADDRESS");
                String inventoryEmailTemplate = configUtil.getProperty(ShipConstants.CONFIG_FILE,
                        "INVENTORY_EMAIL_TEMPLATE_ID");
                String myBuysFeedEnabled = configUtil.getFrpGlobalParm("MY_BUYS", "FEED_ENABLED_FLAG");

                //create a point of contact VO
                PointOfContactVO pocVO = new PointOfContactVO();
                pocVO.setCompanyId("FTD");
                pocVO.setSenderEmailAddress(fromEmailAddress);
                pocVO.setTemplateId(null);
                pocVO.setLetterTitle(inventoryEmailTemplate);
                pocVO.setPointOfContactType("Email");
                pocVO.setCommentType("Order");

                pocVO.setDataDocument(xml);

                //for each email in the list
                Iterator iter = notifyList.iterator();
                while (iter.hasNext())
                {
                    ProductNotificationVO notifyVO = (ProductNotificationVO) iter.next();

                    //set email address
                    pocVO.setRecipientEmailAddress(notifyVO.getEmailAddress());

                    LOGGER.debug("sendInventoryNotifications -> Email being sent to " + notifyVO.getEmailAddress());

                    messageGenerator.processMessage(pocVO);
                }

                if (StringUtils.equalsIgnoreCase(myBuysFeedEnabled, "Y"))
                {
                    LOGGER.debug("sendInventoryNotifications -> Calling sendMyBuys");
                    try
                    {

                        sendMyBuys(connection, productId);
                        LOGGER.debug("sendInventoryNotifications -> Succesfull in sending MyBuys message() for order_processing for product id: "
                                + productId);
                    }
                    catch (Exception e)
                    {
                        LOGGER.error("sendInventoryNotifications -> Error in sending MyBuys message() for product id" + e);
                        CommonUtils.getInstance().sendSystemMessage("Error sending MyBuys message. " + e.getMessage());
                    }
                }

            }
            else
            {
                LOGGER.debug("sendInventoryNotifications -> Nobody is on notification list");
            }
        }
        catch (Throwable t)
        {
            //catch the exception, but do not rethrow it. That way this error will
            //be logged and paged out on, but the processing of venus orders will
            //not be affected.
            LOGGER.error("sendInventoryNotifications -> Error sending inventory level notification email.");
            LOGGER.error("sendInventoryNotifications -> " + t);
            CommonUtils.getInstance().sendSystemMessage(
                    "Error sending inventory level notification email." + t.toString());
        }
    }

    private void rejectAndQueue(Connection conn, SDSLogisticsVO logisticsVO, String message, boolean bQueue)
            throws SDSApplicationException
    {
        try
        {
            //Create the reject
            VenusMessageVO msgVO = logisticsVO.getMessageVo();
            VenusMessageVO rejectMsg = CommonUtils.copyVenusMessage(msgVO, MsgType.REJECT);
            rejectMsg.setOperator(ShipConstants.SDS_USER);
            rejectMsg.setMessageDirection(MsgDirection.INBOUND);

            if (msgVO.getMsgType() == MsgType.ORDER && msgVO.getVenusStatus() == VenusStatus.PENDING)
            {
                LOGGER.debug("rejectAndQueue -> Setting message status on reject to \"OPEN\" for \"PENDING\" FTD");
                rejectMsg.setVenusStatus(VenusStatus.OPEN);
            }
            else
            {
                rejectMsg.setVenusStatus(VenusStatus.ERROR);
            }

            rejectMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
            rejectMsg.setTransmissionTime(new Date());
            rejectMsg.setComments(message);
            rejectMsg.setMessageText(message);
            String venusId = shipDAO.insertVenusMessage(conn, rejectMsg);
            rejectMsg.setVenusId(venusId);

            //Queue the reject
            if (bQueue)
            {
            	sendToQueue(conn,rejectMsg,logisticsVO.getOrderDetailVO(),logisticsVO.getOrderVO());
            }

            try
            {
                shipDAO.updateInvTrkCount(conn, rejectMsg.getVenusId(), rejectMsg.getMsgType().getFtdMsgType());// no need for ftdwest..
            }
            catch (Exception e)
            {
                LOGGER.error("rejectAndQueue -> " + e);
                CommonUtils.getInstance().sendSystemMessage(
                        "Product inventory deduction failed for venus id:" + venusId + ".\nReason:" + e.getMessage());
            }
        }
        catch (SDSApplicationException sase)
        {
            throw sase;
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("rejectAndQueue -> " + e);
        }
    }

    //Defect 1809
    /**
     * Puts a message on the JMS queue �OJMS.PI_REVIEW_REJECT with the rejectVenusId in the payload
     *
     * @param conn
     *            database connection
     * @param rejectVenusId
     *            to be put on the JMS message payload
     * @throws Exception
     */
    private void sendToRejectReview(Connection conn, String rejectVenusId) throws Exception
    {
        LOGGER.debug("sendToRejectReview -> Placing reject " + rejectVenusId + " on the reject review queue.");
        enqueueJmsMessage(conn, JMSPipeline.SHIPREJECTREVIEW, rejectVenusId);
    }

    //Defect 1809
    /**
     * Calculates the next available delivery date of an order which is > the current delivery date
     *
     * @param conn
     *            database connection
     * @param logisticsVO
     *            containing the order as it currently exists
     * @return the new delivery date
     * @throws Exception
     */
    public Date getNextAvailableDeliveryDate(Connection conn, SDSLogisticsVO logisticsVO) throws Exception
    {

        OrderVO orderVO = logisticsVO.getOrderVO();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        VenusMessageVO msgVO = logisticsVO.getMessageVo();

        OEDeliveryDateParm oeParms = DeliveryDateUTIL.getCOMParameterData(msgVO.getCountry(), msgVO.getProductId(),
                msgVO.getZip(), "CA", "US", "1", orderDetailVO.getOccasion(), conn, msgVO.getState(),
                orderVO.getOriginId(), false, true);

        DeliveryDateUTIL util = new DeliveryDateUTIL();
        List deliveryDates = util.getOrderProductDeliveryDates(oeParms);

        String shipMethod = msgVO.getShipMethod();
        Date orderDeliveryDate = msgVO.getDeliveryDate();
        Date newDeliveryDate = null;
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        today.set(Calendar.AM_PM, Calendar.AM);

        for (int idx = 0; idx < deliveryDates.size() && newDeliveryDate == null; idx++)
        {
            OEDeliveryDate deliveryVO = (OEDeliveryDate) deliveryDates.get(idx);
            Date testDate = (new SimpleDateFormat(DELIVERY_DATE_FORMAT)).parse(deliveryVO.getDeliveryDate());

            if (testDate.after(orderDeliveryDate) && testDate.after(today.getTime()))
            {
                for (int idx2 = 0; idx2 < deliveryVO.getShippingMethods().size(); idx2++)
                {
                    String testShipMethod = ((ShippingMethod) deliveryVO.getShippingMethods().get(idx2)).getCode();

                    if (StringUtils.equals(testShipMethod, shipMethod))
                    {
                        newDeliveryDate = testDate;
                        break;
                    }
                }
            }
        }

        return newDeliveryDate;
    }

    //Defect 1809
    /**
     * Calculates the ship date of an order based on the passed in delivery date
     *
     * @param conn
     *            database connection
     * @param logisticsVO
     *            containing the order
     * @param deliveryDate
     *            to compute ship date on
     * @return new ship date
     * @throws Exception
     */
    public Date computeShipDate(Connection conn, SDSLogisticsVO logisticsVO, Date deliveryDate) throws Exception
    {
        Date retval = null;
        VenusMessageVO msgVO = logisticsVO.getMessageVo();

        if (logisticsVO != null && deliveryDate != null)
        {
            DeliveryDateUTIL util = new DeliveryDateUTIL();

            String shipDate = util.getShipDate(msgVO.getShipMethod(),
                    (new SimpleDateFormat(DELIVERY_DATE_FORMAT)).format(deliveryDate), msgVO.getState(),
                    msgVO.getCountry(), msgVO.getProductId(), conn);

            if (StringUtils.isNotBlank(shipDate))
            {
                retval = (new SimpleDateFormat(DELIVERY_DATE_FORMAT)).parse(shipDate);
            }
        }

        return retval;
    }

    /**
     * Execute same logic as in SDSShipmentProcessingBO to create SDSLogisticsVO. In addition, add a list of
     * SDSZoneJumpTripVO as available ZoneJump carrier hubs.
     *
     * @param connection
     * @param venusMessageVO
     * @return
     * @throws Exception
     */
    public SDSLogisticsVO buildRequestZoneJump(Connection connection, VenusMessageVO venusMessageVO) throws Exception
    {

        SDSLogisticsVO logisticsVO = this.buildRequest(connection, venusMessageVO);

        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        List<SDSZoneJumpTripVO> hubs = null;

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();

        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        //Add ZoneJump hubs
        hubs = shipDAO.getZoneJumpHubs(connection, isSubcode ? subcodeId : productId, venusMessageVO.getDeliveryDate(),
                venusMessageVO.getReferenceNumber());
        logisticsVO.setZoneJumpHubs(hubs);

        return logisticsVO;
    }

    /**
     * Process the order and check to see if it can go ZoneJump. Returns the following fields in HashMap:
     * ZONE_JUMP_SUCCESS: Boolean type with value of true or false SHIPMENT_OPTION_IDEAL_RANK: Integer type with value
     * for current ideal rank SHIPMENT_OPTION_ACTUAL_RANK: Integer type with value for current actual rank
     *
     * @param connection
     * @param venusMessageVO
     * @param logisticsVO
     * @return boolean
     * @throws Exception
     */
    public HashMap processOrderZoneJump(Connection connection, VenusMessageVO venusMessageVO, SDSLogisticsVO logisticsVO)
            throws Exception
    {
        StringBuilder sb;
        Document response = null;
        String shipUnit;
        List<SDSZoneJumpTripVO> hubs = logisticsVO.getZoneJumpHubs();
        HashMap retMap = new HashMap();
        boolean zoneJumpSuccess = false;
        int shipmentOptionIdealRank = 0;
        int shipmentOptionActualRank = 0;

        //ZoneJump hubs found. Send all ZoneJump hubs to SDS for a list of valid ZoneJump hubs / dates.
        logisticsVO = generateRateShipUnitsRequestZoneJump(connection, logisticsVO);
        sb = new StringBuilder();
        sb.append("Sending rate request to Argo:\r\n");
        sb.append("Order Number: ");
        sb.append(venusMessageVO.getVenusId());
        sb.append("\r\nForcing Shipment: ");
        sb.append(venusMessageVO.isForcedShipment());
        sb.append("\r\nShipment Already Rated: ");
        sb.append(venusMessageVO.isRatedShipment());
        sb.append("\r\nRequesting Response Data: ");
        sb.append(venusMessageVO.isGetSdsResponse());

        shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
        LOGGER.debug("processOrderZoneJump -> ZoneJump rateShip request is " + shipUnit);

        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        try
        {
            response = sdsCommunications.rateShipment(connection, logisticsVO.getShipUnit(), venusMessageVO.getVenusId());
        }
        finally
        {
            if (response != null)
            {
                insertSDSTransaction(connection, venusMessageVO.getVenusId(),
                        JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
            }
        }
        //We'll get back a list of hubs. Use that information to retrieve available vendors.
        SDSRateResponseVO rateResponseVO = this.parseRateShipUnitsResponseZoneJump(response, logisticsVO);
        if (!rateResponseVO.isSuccess())
        {
            //If status of fail, then follow non ZoneJump ship logic
            LOGGER.info("processOrderZoneJump -> No ZoneJump hubs found");
            retMap.put(ZONE_JUMP_SUCCESS, Boolean.valueOf(zoneJumpSuccess));
            retMap.put(SHIPMENT_OPTION_IDEAL_RANK, Integer.valueOf(shipmentOptionIdealRank));
            retMap.put(SHIPMENT_OPTION_ACTUAL_RANK, Integer.valueOf(shipmentOptionActualRank));
            return retMap;
        }
        logisticsVO.setRateResponse(rateResponseVO);

        //Update the ZoneJump trip list with response from SDS.
        hubs = rateResponseVO.getZoneJumpTrips();
        logisticsVO.setZoneJumpHubs(hubs);

        //For each hub, determine if the hub is available and if it has available vendors.

        LOGGER.debug("processOrderZoneJump -> total hubs returned = " + hubs.size());

        for (int hubIndex = 0; hubIndex < hubs.size(); hubIndex++)
        {
            LOGGER.debug("processOrderZoneJump -> working on hubIndex = " + hubIndex);
            List<ShipVendorProductVO> availableVendors = this.getZoneJumpVendorsByIndex(connection, logisticsVO,
                    hubIndex);
            SDSZoneJumpTripVO hub = hubs.get(hubIndex);
            ShipmentOptionVO soVO = hub.getShipmentOption();
            soVO.setVenusOrderNumber(venusMessageVO.getVenusOrderNumber());

            LOGGER.debug("processOrderZoneJump -> soVO.getErrorTxt() = " + soVO.getErrorTxt());
            //If hub is not available from rate response, ideal rank and actual rank remain at 0.
            if (soVO.getErrorTxt() == null)
            {
                //If the hub is allowed from rate response, increment ideal rank.
                soVO.setIdealRankNumber(++shipmentOptionIdealRank);
                if (availableVendors != null && availableVendors.size() > 0)
                {
                    //If the hub is allowed from rate response and the hub also has vendors, increment actual rank.
                    soVO.setActualRankNumber(++shipmentOptionActualRank);
                }
            }

            LOGGER.debug("processOrderZoneJump -> soVO.getActualRankNumber() = " + soVO.getActualRankNumber());
            //For the first available hub that has available vendors, send create shipment request.
            if (soVO.getActualRankNumber() == 1)
            {
                logisticsVO.setAvailableVendors(availableVendors);
                //Try each vendor until successful
                for (int i = 0; i < availableVendors.size(); i++)
                {
                    logisticsVO = this.generateCreateShipUnitsRequestZoneJump(connection, logisticsVO, i);
                    sb = new StringBuilder();
                    sb.append("Sending ship request to Argo:\r\n");
                    sb.append("Order Number: ");
                    sb.append(venusMessageVO.getVenusId());
                    sb.append("\r\nForcing Shipment: ");
                    sb.append(venusMessageVO.isForcedShipment());
                    sb.append("\r\nShipment Already Rated: ");
                    sb.append(venusMessageVO.isRatedShipment());
                    sb.append("\r\nRequesting Response Data: ");
                    sb.append(venusMessageVO.isGetSdsResponse());

                    shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
                    LOGGER.debug("processOrderZoneJump -> ZoneJump createShip request is " + shipUnit); //need to move this outside of loop

                    createComment(connection, sb.toString(), orderDetailVO);

                    try
                    {
                        //Send create shipment request with ZoneJump parameters.
                        response = sdsCommunications.createShipment(connection, logisticsVO.getShipUnit(), venusMessageVO.getVenusId());
                        //Update available vendors on logisticsVO.
                        ArrayList<ShipVendorProductVO> validVendor = new ArrayList();
                        validVendor.add(availableVendors.get(i));
                        logisticsVO.setAvailableVendors(validVendor);
                        this.processResponse(connection, venusMessageVO, logisticsVO, response, "CreateShipUnitsZoneJump", i);
                        ShipVendorProductVO svpVO = availableVendors.get(i);
                        SDSZoneJumpTripVO trip = svpVO.getZoneJumpTrip();
                        shipDAO.assignOrderToVendor(connection, venusMessageVO, trip);
                        shipDAO.updateVenusZoneJump(connection, venusMessageVO, trip);
                        zoneJumpSuccess = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        LOGGER.info("processOrderZoneJump -> order unable to ZoneJump. Error = " + e.toString());
                        zoneJumpSuccess = false;
                        shipmentOptionIdealRank--;
                        shipmentOptionActualRank--;
                        soVO.setIdealRankNumber(DEFAULT_SHIPMENT_OPTION_RANK);
                        soVO.setActualRankNumber(DEFAULT_SHIPMENT_OPTION_RANK);
                        soVO.setErrorTxt(e.getMessage());
                        break;
                    }
                    finally
                    {
                        if (response != null)
                        {
                            insertSDSTransaction(connection, venusMessageVO.getVenusId(),
                                    JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
                        }
                    }
                }

            }

            //Shipment option ranking assigned. Store it.
            try
            {
                shipDAO.insertShipmentOption(connection, soVO);
            }
            catch (Exception e)
            {
                LOGGER.error("processOrderZoneJump -> " + e);
                CommonUtils.getInstance().sendSystemMessage(
                        "Error occurred when inserting shipment option:" + venusMessageVO.getVenusId() + ".\nReason:"
                                + e.getMessage());
            }

        }
        if (!zoneJumpSuccess)
        {
            LOGGER.info("processOrderZoneJump -> ZoneJump hubs were available but order could not be ZoneJumped. Error = " + venusMessageVO.getVenusId());
        }
        retMap.put(ZONE_JUMP_SUCCESS, Boolean.valueOf(zoneJumpSuccess));
        retMap.put(SHIPMENT_OPTION_IDEAL_RANK, Integer.valueOf(shipmentOptionIdealRank));
        retMap.put(SHIPMENT_OPTION_ACTUAL_RANK, Integer.valueOf(shipmentOptionActualRank));
        return retMap;
    }

    /**
     * Creates ZoneJump rate request.
     *
     * @param connection
     * @param logisticsVO
     * @return
     * @throws Exception
     */
    private SDSLogisticsVO generateRateShipUnitsRequestZoneJump(Connection connection, SDSLogisticsVO logisticsVO) throws Exception
    {

        VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();
        CompanyVO companyVO = logisticsVO.getCompanyVO();
        List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
        ProductVO productVO = logisticsVO.getProductVO();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        List<SDSZoneJumpTripVO> hubs = logisticsVO.getZoneJumpHubs();

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();
        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        Document shipUnit = JAXPUtil.createDocument();
        Element docRoot = shipUnit.createElement("RATE_SHIP_UNITS_PARAMS");
        docRoot.setAttribute("xmlns", "http://ScanData.com/WTM/XMLSchemas/WTM_XMLSchema_14.00.0000.xsd");
        docRoot.setAttribute("MSN", "1");

        shipUnit.appendChild(docRoot);
        Element shipUnitRoot = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIP_UNIT);
        docRoot.appendChild(shipUnitRoot);
        logisticsVO.setShipUnit(shipUnit);

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_CARTON_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_DISTRIBUTION_CENTER, ""));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORDER_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
                SDSConstants.TAG_RATE_CREATE_DATE_BEST_METHOD_PARAMETER,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getDeliveryDate())));

        //add total add on weight to product weight to derive estimated order weight
        BigDecimal productWeight = new BigDecimal(venusMessageVO.getProductWeight());
        BigDecimal totalAddOnWeight = this.calculateTotalAddOnWeight(venusMessageVO);
        BigDecimal totalOrderWeight = productWeight.add(totalAddOnWeight);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ESTIMATED_WEIGHT,
                totalOrderWeight.toString()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_STATUS,
                logisticsVO.isRateShopOnly()?SDSConstants.STATUS_PENDING:SDSConstants.STATUS_INITIAL));


        //Using the ship date and ZoneJump date of the first carrier in list.
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(hubs.get(0).getShipDate())));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_SHIP_VIA,
                SDSConstants.ZONE_SKIP_BEST_METHOD));

        //Per Email From Jason Chen at ScanData on 09/05/2006:
        //[Jason Chen] Yes, I do know. Please download <MasterIterID></MasterItemID?
        //In side SHIP_UNITS XML, the rate shop route will use the appropriate
        //MasterItemID from the DynamicData block.
        if (availableVendors.size() > 0)
        {
            ShipVendorProductVO svpVO = availableVendors.get(0);
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    svpVO.getProductSkuId()));
        }
        else
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    venusMessageVO.getProductId()));
        }

        Element shipOptions = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS);
        shipUnitRoot.appendChild(shipOptions);

        String shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_COMMERCIAL;
        //Service options
        String serviceOptions = "";
        CustomerVO recipientVO = logisticsVO.getRecipientVO();
        if (recipientVO != null && StringUtils.equals(recipientVO.getAddressType(), ADDRESS_TYPE_RESIDENTIAL))
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_RESIDENTIAL;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(venusMessageVO.getDeliveryDate());
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_SATURDAY;
        }

        if (productVO.isOver21())
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_OVER21;
        }
        Element shipOptionElement = shipUnit.createElement(shipOption);
        shipOptions.appendChild(shipOptionElement);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_WEIGHT, totalOrderWeight.toString()));

        Element dynamicData = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_DATA_XML);
        Element origins = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS);

        Element origin = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_ORIGIN);
        origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_DIST_CENTER,
                SDSConstants.TAG_RATE_ZONEJUMP_DEFAULT_DIST_CENTER));

        Element carriers = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIERS);

        //define a set and list to store all the unique carrier id
        Set<String> uniqueCarrierIdsSet = new HashSet<String>();
        List uniqueCarrierIdsList = null;

        //define a set and list to store all the unique ship points
        Set<String> uniqueShipPointsPerCarrierIdSet = null;
        List uniqueShipPointsPerCarrierIdList = null;

        //hashmap where key = carrier id, and value = arraylist of unique ship points
        HashMap uniqueCarrierShipPointIdMap = new HashMap();

        //Loop through the hubs to create a set of unique carriers only
        for (int idx = 0; idx < hubs.size(); idx++)
        {
            uniqueCarrierIdsSet.add(((SDSZoneJumpTripVO) hubs.get(idx)).getCarrierId());
        }
        //and set the list based on this set
        uniqueCarrierIdsList = new ArrayList<String>(new HashSet<String>(uniqueCarrierIdsSet));

        //loop thru the uniqueCarrierIdsList and for each carrier, create a unique ArrayList of ship point ids.
        //Put it in a HashMap - key = carrier and value = list of unique ship point ids for that carrier
        String carrierId1;
        for (int i = 0; i < uniqueCarrierIdsList.size(); i++)
        {
            carrierId1 = (String) uniqueCarrierIdsList.get(i);
            uniqueShipPointsPerCarrierIdSet = new HashSet<String>();

            for (int idx = 0; idx < hubs.size(); idx++)
            {
                SDSZoneJumpTripVO vo = (SDSZoneJumpTripVO) hubs.get(idx);
                String carrierId = vo.getCarrierId();
                String shipPointId = vo.getShipPointId();
                if (carrierId.equalsIgnoreCase(carrierId1))
                {
                    uniqueShipPointsPerCarrierIdSet.add(shipPointId);
                }
            }

            uniqueShipPointsPerCarrierIdList = new ArrayList<String>(new HashSet<String>(
                    uniqueShipPointsPerCarrierIdSet));
            uniqueCarrierShipPointIdMap.put(carrierId1, uniqueShipPointsPerCarrierIdList);
        }

        Element shipPoints = null;
        Element carrier = null;

        //loop thru the uniqueCarrierShipPointIdMap HashMap to create the xml node
        Set ks = uniqueCarrierShipPointIdMap.keySet();
        Iterator iter = ks.iterator();
        String carrierId2;

        while (iter.hasNext())
        {
            //retrieve the carrier id from the HashMap
            carrierId2 = iter.next().toString();

            //and create a CarrierId node
            carrier = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER);
            carrier.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER_ID, carrierId2));

            //create a ship points node
            shipPoints = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIP_POINTS);
            carrier.appendChild(shipPoints);

            uniqueShipPointsPerCarrierIdList = (ArrayList) uniqueCarrierShipPointIdMap.get(carrierId2);

            //Loop through the unique ship point ids and create the ship point id nodes
            for (int i = 0; i < uniqueShipPointsPerCarrierIdList.size(); i++)
            {
                shipPoints.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_SHIP_POINT_ID,
                        (String) uniqueShipPointsPerCarrierIdList.get(i)));
            }

            //append the carrier node to carriers
            carriers.appendChild(carrier);
        }

        //append carriers to origin
        origin.appendChild(carriers);

        //Leaving the vendor specific tags until hearing back from SDS.
        origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_VENDOR_COST, ""));
        origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_VENDOR_SKU, ""));

        origins.appendChild(origin);
        dynamicData.appendChild(origins);

        Element invoiceData = shipUnit.createElement(ShipConstants.TICKET_ROOT);

        //E number
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_ORDER,
                venusMessageVO.getVenusOrderNumber()));

        //Defect 1809
        //Add partner data to the label
        String originName = null;
        String originPhone = null;
        String guarantee = null;

        if (StringUtils.isBlank(originName))
        {
            originName = companyVO.getCompanyName();
        }

        if (StringUtils.isBlank(originPhone))
        {
            PartnerVO partnerMasterVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
            String prefPartner = null;
            if (partnerMasterVO != null)
            {
                prefPartner = partnerMasterVO.getPartnerName();
            }
            if (StringUtils.isNotBlank(prefPartner))
            {
                //String partnerPhoneNumber = oDAO.getContentWithFilter("", "TRANSFER_EXTENSION", "USAA", null);
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                originPhone = cu.getContentWithFilter(connection, ShipConstants.CONTENT_CONTEXT_PREFERRED_PARTNER,
                        ShipConstants.CONTENT_NAME_PREFERRED_PARTNER, partnerMasterVO.getPartnerName(), null);
            }
            else
                originPhone = companyVO.getPhoneNumber();
        }

        //Origin
        Element element = shipUnit.createElement(ShipConstants.TICKET_ORIGINATOR);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_NAME, originName);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_PHONE, originPhone);
        invoiceData.appendChild(element);

        //Recipient
        element = shipUnit.createElement(ShipConstants.TICKET_RECIPIENT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_NAME, venusMessageVO.getRecipient());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_COMPANY, venusMessageVO.getBusinessName());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR1, venusMessageVO.getAddress1());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR2, venusMessageVO.getAddress2());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_CITY, venusMessageVO.getCity());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_STATE, venusMessageVO.getState());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_POSTAL, venusMessageVO.getZip());
        invoiceData.appendChild(element);

        //Product
        element = shipUnit.createElement(ShipConstants.TICKET_PRODUCT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_PRODUCT_DESC, venusMessageVO.getProductDescription());
        invoiceData.appendChild(element);

        //Card message
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_CARD_MESSAGE,
                venusMessageVO.getCardMessage()));
        dynamicData.appendChild(invoiceData);

        //Guarantee (Leave blank if non is available.
        //SDS will default to the standard FTD.COM guarantee)
        //Defect 1809
        if (StringUtils.isBlank(guarantee))
        {
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_GUARANTEE));
        }
        else
        {
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_GUARANTEE, guarantee));
        }

        //Personal Greeting ID (Leave blank if none is available).
        if (StringUtils.isBlank(venusMessageVO.getPersonalGreetingID()))
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_PERSONAL_GREETING_ID));
        else
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_PERSONAL_GREETING_ID,
                    venusMessageVO.getPersonalGreetingID()));

        //Build the FTD_SHIP_UNITS section
        Element ftdShipUnits = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_FSU_FTD_SHIP_UNITS);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_ORDER_NUMBER,
                orderDetailVO.getExternalOrderNumber()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_ID,
                isSubcode ? orderDetailVO.getSubcode() : venusMessageVO.getProductId()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_DESC,
                venusMessageVO.getProductDescription()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_LOB,
                SDSConstants.LOB_FTDCOM));
        dynamicData.appendChild(ftdShipUnits);

        shipUnitRoot.appendChild(dynamicData);

        Element address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);

        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_DELIVER_TO));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
                StringUtils.substring(venusMessageVO.getBusinessName(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_INDIVIDUAL_NAME,
                StringUtils.substring(venusMessageVO.getRecipient(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
                StringUtils.substring(venusMessageVO.getAddress1(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
                StringUtils.substring(venusMessageVO.getAddress2(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
                venusMessageVO.getCity()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
                venusMessageVO.getState()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
                venusMessageVO.getZip()));

        //FIXME: Need the three char country code from the database
        //shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,SDSConstants.TAG_SHIP_SHIP_FOR_COUNTRY,venusMessageVO.getCountry()));
        String countryCode = venusMessageVO.getCountry();
        if (StringUtils.equals("US", countryCode))
        {
            countryCode = "USA";
        }
        else if (StringUtils.equals("CA", countryCode))
        {
            countryCode = "CAN";
        }
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COUNTRY, countryCode));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_PHONE_NUMBER,
                venusMessageVO.getPhoneNumber()));
        shipUnitRoot.appendChild(address);

        if (logisticsVO.getAvailableVendors().size() > 0)
        {
            //Add return address
	        address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
	                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_RETURN));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
	                SDSConstants.TAG_RATE_CREATE_RETURN_ADDRESS_COMPANY_NAME));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
	                StringUtils.substring(availableVendors.get(0).getAddress1(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
	                StringUtils.substring(availableVendors.get(0).getAddress2(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
	        		availableVendors.get(0).getCity()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
	        		availableVendors.get(0).getState()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
	        		availableVendors.get(0).getZipCode()));
	        shipUnitRoot.appendChild(address);
        }

        Element fedexUnit = null;
        if (fedexUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element upsUnit = null;
        if (upsUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element intlUnit = null;
        //Complete this when we go intl
        if (intlUnit != null)
        {
            shipUnitRoot.appendChild(intlUnit);
        }

        return logisticsVO;
    }

    /**
     * Parse rate response.
     *
     * @param responseDoc
     * @return
     * @throws SDSApplicationException
     */
    public SDSRateResponseVO parseRateShipUnitsResponseZoneJump(Document responseDoc, SDSLogisticsVO logisticsVO) throws SDSApplicationException
    {
        List<SDSZoneJumpTripVO> hubs = logisticsVO.getZoneJumpHubs();

        SDSRateResponseVO responseVO = new SDSRateResponseVO();
        List zoneJumpTrips = new ArrayList<SDSZoneJumpTripVO>();
        responseVO.setZoneJumpTrips(zoneJumpTrips);

        String strResponse = null;
        String errorNumberMain = null;
        String errorDescriptionMain = null;
        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseRateShipUnitsResponseZoneJump -> ZoneJump rateShip response is " + strResponse);
        }
        catch (Exception e)
        {
            LOGGER.warn("parseRateShipUnitsResponseZoneJump -> Error received while parsing.");
        }

        //In V14, status node does not exist. Once we get the response, check
        //RATE_SHIP_UNIT_DOC/SHIP_UNIT_RATE_INFO_ROW/ERROR
        //There will be cases where ScanData will send an error node at this level: example, ScanData cannot even parse
        //the request
        try
        {
            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_RATE_ERROR, SDSConstants.NAMESPACE_PREFIX,
                    SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseRateShipUnitsResponseZoneJump -> nlErrorMain.getLength() = " + nlErrorMain.getLength());
            if (nlErrorMain.getLength() > 0)
            {
                Element errorElement = (Element) nlErrorMain.item(0);
                errorNumberMain = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_NUMBER);
                errorDescriptionMain = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorNumberMain = " + errorNumberMain);
            LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorDescriptionMain = " + errorDescriptionMain);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseRateShipUnitsResponseZoneJump -> Failed to retrieve error info rate response.", e);
        }

        if (StringUtils.isBlank(errorNumberMain))
        {
            responseVO.setStatusCode("0");
            responseVO.setMsgText("");
            responseVO.setSuccess(true);

            try
            {
                String errorNumber = null;
                String errorDescription = null;
                NodeList nl = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_RATE_SHIP_UNIT_RATE_INFO, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.NAMESPACE_URI);

                LOGGER.debug("parseRateShipUnitsResponseZoneJump -> nl.getLength() = " + nl.getLength());
                for (int i = 0; i < nl.getLength(); i++)
                {
                    Element shipElem = (Element) nl.item(i);
                    String carrierId = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_CREATE_CARRIER_ID);
                    String shipMethodId = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_CREATE_SHIP_METHOD_ID);
                    String deliveryDateStr = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_EXPECTED_DELIVERY_DATE);
                    deliveryDateStr = StringUtils.trimToEmpty(deliveryDateStr);

                    String shipPointId = getFirstChildNoNull(shipElem, SDSConstants.TAG_SHIP_POINT_ID);
                    String origin = getFirstChildNoNull(shipElem, SDSConstants.TAG_SHIP_POINT_ID);
                    String zoneCode = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_ZONE_CODE);
                    String rateWeight = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_RATE_WEIGHT);
                    String rateCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_RATE_CHARGE);
                    String handlingCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_HANDLING_CHARGE);
                    String totalCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_TOTAL_CHARGE);
                    String freightCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_FREIGHT_CHARGE);
                    String distributionCenter = getFirstChildNoNull(shipElem, SDSConstants.TAG_DISTRIBUTION_CENTER);

                    NodeList nlError = shipElem.getElementsByTagName(SDSConstants.TAG_ERROR);
                    LOGGER.debug("parseRateShipUnitsResponseZoneJump -> i = " + i + " and nlError.getLength() = " + nlError.getLength());
                    if (nlError.getLength() > 0)
                    {
                        Element errorElement = (Element) nlError.item(0);
                        errorNumber = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_NUMBER);
                        errorDescription = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_DESCRIPTION);
                    }
                    LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorNumber = " + errorNumber);
                    LOGGER.debug("parseCreateShipUnitsResponseZoneJump -> errorDescription = " + errorDescription);

                    SDSZoneJumpTripVO tripVO = new SDSZoneJumpTripVO();
                    tripVO.setCarrierId(carrierId);
                    tripVO.setShipMethod(shipMethodId);

                    //ScanData is not sending this ship date in the V14 rate response XML any more
                    tripVO.setShipDate(hubs.get(0).getShipDate());

                    if (deliveryDateStr != null && StringUtils.isNotBlank(deliveryDateStr))
                        tripVO.setDeliveryDate(tzDateTimeToDate(deliveryDateStr));
                    tripVO.setShipPointId(shipPointId);
                    tripVO.setOrigin(origin);

                    //Retrieve shipment option and attached it to the hub.
                    ShipmentOptionVO soVO = new ShipmentOptionVO();
                    soVO.setZoneJumpFlag("Y");
                    soVO.setCarrierId(carrierId);
                    soVO.setShipMethodId(shipMethodId);
                    if (zoneCode != null && StringUtils.isNotBlank(zoneCode))
                    {
                        soVO.setZoneCode(Integer.valueOf(zoneCode));
                    }
                    if (rateWeight != null && StringUtils.isNotBlank(rateWeight))
                    {
                        soVO.setRateWeight(new BigDecimal(rateWeight));
                    }
                    if (rateCharge != null && StringUtils.isNotBlank(rateCharge))
                    {
                        soVO.setRateCharge(new BigDecimal(rateCharge));
                    }
                    if (handlingCharge != null && StringUtils.isNotBlank(handlingCharge))
                    {
                        soVO.setHandlingCharge(new BigDecimal(handlingCharge));
                    }
                    if (totalCharge != null && StringUtils.isNotBlank(totalCharge))
                    {
                        soVO.setTotalCharge(new BigDecimal(totalCharge));
                    }
                    if (freightCharge != null && StringUtils.isNotBlank(freightCharge))
                    {
                        soVO.setFreightCharge(new BigDecimal(freightCharge));
                    }

                    //ScanData is not sending this ship date in the V14 rate response XML any more
                    soVO.setDateShipped(hubs.get(0).getShipDate());

                    if (deliveryDateStr != null && StringUtils.isNotBlank(deliveryDateStr))
                        soVO.setExpectedDeliveryDate(tzDateTimeToDate(deliveryDateStr));

                    soVO.setDistributionCenter(distributionCenter);
                    soVO.setShipPointId(shipPointId);
                    if (StringUtils.isNotBlank(errorNumber))
                        soVO.setErrorTxt("ERROR NUMBER = " + errorNumber + " and ERROR DESCRIPTION = "
                                + errorDescription);
                    else
                        soVO.setErrorTxt(null);
                    soVO.setIdealRankNumber(DEFAULT_SHIPMENT_OPTION_RANK);
                    soVO.setActualRankNumber(DEFAULT_SHIPMENT_OPTION_RANK);

                    tripVO.setShipmentOption(soVO);
                    zoneJumpTrips.add(tripVO);
                }
            }
            catch (Exception e)
            {
                LOGGER.error("parseRateShipUnitsResponseZoneJump -> " + e);
                throw new SDSApplicationException("parseRateShipUnitsResponseZoneJump -> Exception while locating ship methods in rate request response.", e);
            }
            responseVO.setZoneJumpTrips(zoneJumpTrips);
        }
        else
        {
            if (StringUtils.equals(errorNumberMain, SDSConstants.STATUS_SDS_APP_EXCEPTION))
            {
                //If an exception is being thrown from the sds ship server, then put
                //the entire response in the description field
                LOGGER.warn("parseRateShipUnitsResponseZoneJump -> The SDS ship server has thrown an exception.");
                errorDescriptionMain = strResponse;
            }
            else if (StringUtils.isBlank(errorDescriptionMain))
            {
                LOGGER.debug("parseRateShipUnitsResponseZoneJump -> No status description found in rate response.  Putting entire response in message text.");
                errorDescriptionMain = strResponse;
            }
            else
                LOGGER.debug("parseRateShipUnitsResponseZoneJump -> The SDS ship server has thrown an error.  Error Number = " + errorNumberMain
                        + " and Error Description " + errorDescriptionMain);

            responseVO.setStatusCode(errorNumberMain);
            responseVO.setMsgText(errorDescriptionMain);
            responseVO.setSuccess(false);
        }
        return responseVO;
    }

    private String getFirstChildNoNull(Element elem, String tagName) throws Exception
    {
        String retStr = null;
        NodeList nl = elem.getElementsByTagName(tagName);
        if (nl != null)
        {
            Element firstElem = (Element) nl.item(0);
            if (firstElem != null)
            {
                Text firstChild = (Text) firstElem.getFirstChild();
                if (firstChild != null)
                {
                    retStr = firstChild.getNodeValue();
                }
            }
        }
        return retStr;
    }

    /**
     * Find ZoneJump vendors based on rate response.
     *
     * @param connection
     * @param logisticsVO
     * @return
     * @throws Exception
     */
    public List<ShipVendorProductVO> getZoneJumpVendorsByIndex(Connection connection, SDSLogisticsVO logisticsVO,
            int index) throws Exception
    {
        List availableVendors = new ArrayList<ShipVendorProductVO>();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        SDSRateResponseVO responseVO = logisticsVO.getRateResponse();

        VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();

        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }
        String productSubcodeId = isSubcode ? subcodeId : productId;
        availableVendors = shipDAO.getShippingVendorsByHub(connection, productSubcodeId, responseVO, index);
        ShipVendorProductVO svpVO;
        for (int i = 0; i < availableVendors.size(); i++)
        {
            svpVO = (ShipVendorProductVO) availableVendors.get(i);
            //check if vendor has add ons associated with venus order. If it does, leave in the list
            //of available vendors, else remove
            AddOnUtility addOnUTIL = new AddOnUtility();
            boolean vendorAddOnsAvailable = addOnUTIL.vendorAddOnAvailable(venusMessageVO.getVenusId(),
                    svpVO.getVendorId(), connection);

            if (!vendorAddOnsAvailable)
            {
                availableVendors.remove(i);
            }
        }

        return availableVendors;
    }

    /**
     * Builds a createShipment request for a ZoneJump order.
     *
     * @param connection
     * @param logisticsVO
     * @param availableVendorIdx
     *            - index of availableVendors to send
     * @return
     * @throws Exception
     */
    private SDSLogisticsVO generateCreateShipUnitsRequestZoneJump(Connection connection, SDSLogisticsVO logisticsVO,
            int availableVendorIdx) throws Exception
    {

        VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();
        CompanyVO companyVO = logisticsVO.getCompanyVO();
        List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
        ShipVendorProductVO svpVO = new ShipVendorProductVO();
        
        if(availableVendors != null && availableVendors.size() > 0 && availableVendors.get(availableVendorIdx) != null) {
        	svpVO = availableVendors.get(availableVendorIdx); //use specified index
        }
        
        SDSZoneJumpTripVO trip = svpVO.getZoneJumpTrip();
        ProductVO productVO = logisticsVO.getProductVO();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        boolean isMarketingInsertAllowed = true;
        boolean compAddonFlag = false;

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();
        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        //Determine if this is a USAA preferred partner
        PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
        if (partnerVO != null)
            isMarketingInsertAllowed = partnerVO.isMarketingInsertAllowed();

        HashMap addOnTotals = this.calculateAddOnTotals(venusMessageVO, svpVO);

        Document shipUnit = JAXPUtil.createDocument();
        Element docRoot = shipUnit.createElement("CREATE_SHIP_UNITS_PARAMS");
        docRoot.setAttribute("xmlns", "http://ScanData.com/WTM/XMLSchemas/WTM_XMLSchema_14.00.0000.xsd");
        docRoot.setAttribute("MSN", "1");

        shipUnit.appendChild(docRoot);
        Element shipUnitRoot = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIP_UNIT);
        docRoot.appendChild(shipUnitRoot);
        logisticsVO.setShipUnit(shipUnit);

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_CARTON_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_DISTRIBUTION_CENTER,
                StringUtils.replace(svpVO.getVendorCode(), "-", "")));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORDER_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
                SDSConstants.TAG_RATE_CREATE_DATE_BEST_METHOD_PARAMETER,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getDeliveryDate())));

        //add total add on weight to product weight to derive estimated order weight
        BigDecimal productWeight = new BigDecimal(venusMessageVO.getProductWeight());
        BigDecimal totalAddOnWeight = ((BigDecimal) addOnTotals.get(TOTAL_ADD_ON_WEIGHT));
        BigDecimal totalOrderWeight = productWeight.add(totalAddOnWeight);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ESTIMATED_WEIGHT,
                totalOrderWeight.toString()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_CREATE_BILLING_ACCOUNT_ID,
                trip.getBillingAccount()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_STATUS,
                logisticsVO.isRateShopOnly()?SDSConstants.STATUS_PENDING:SDSConstants.STATUS_INITIAL));

        //Per Scott, this date is the date when the shipment leaves the injection hub.
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(trip.getShipDate())));

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_CARRIER_ID,
                trip.getCarrierId()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_SHIP_METHOD_ID,
                trip.getShipMethod()));

        if (svpVO != null)
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    svpVO.getProductSkuId()));
        }
        else
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    venusMessageVO.getProductId()));
        }
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_SHIP_POINT_ID,
                trip.getShipPointId()));

        Element shipOptions = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS);
        shipUnitRoot.appendChild(shipOptions);

        String shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_COMMERCIAL;
        //Service options
        String serviceOptions = "";
        CustomerVO recipientVO = logisticsVO.getRecipientVO();
        if (recipientVO != null && StringUtils.equals(recipientVO.getAddressType(), ADDRESS_TYPE_RESIDENTIAL))
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_RESIDENTIAL;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(venusMessageVO.getDeliveryDate());
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_SATURDAY;
        }

        if (productVO.isOver21())
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_OVER21;
        }
        Element shipOptionElement = shipUnit.createElement(shipOption);
        shipOptions.appendChild(shipOptionElement);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_WEIGHT, totalOrderWeight.toString()));

        Element dynamicData = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_DATA_XML);
        Element origins = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS);

        Element origin = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_ORIGIN);
        origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_DIST_CENTER,
                StringUtils.replace(svpVO.getVendorCode(), "-", "")));
        Element carriers = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIERS);
        Element carrier = null;
        carrier = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER);
        carrier.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER_ID,
                trip.getCarrierId()));
        carriers.appendChild(carrier);
        origin.appendChild(carriers);

        origins.appendChild(origin);

        dynamicData.appendChild(origins);

        Element trailers = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_TRAILERS);
        trailers.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_TRAILER_NUMBER,
                trip.getTrailerNumber()));
        //Per Scott, this is the ZoneJump Date.
        trailers.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT,
               (new SimpleDateFormat(SDS_DATE_FORMAT)).format(trip.getZoneJumpDate())));
        dynamicData.appendChild(trailers);

        Element invoiceData = shipUnit.createElement(ShipConstants.TICKET_ROOT);

        //E number
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_ORDER,
                venusMessageVO.getVenusOrderNumber()));

        //Defect 1809
        //Add partner data to the label
        String originName = null;
        String originPhone = null;
        String guarantee = null;

        if (StringUtils.isBlank(originName))
        {
            originName = companyVO.getCompanyName();
        }

        if (StringUtils.isBlank(originPhone))
        {
            PartnerVO partnerMasterVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
            String prefPartner = null;
            if (partnerMasterVO != null)
            {
                prefPartner = partnerMasterVO.getPartnerName();
            }
            if (StringUtils.isNotBlank(prefPartner))
            {
                //String partnerPhoneNumber = oDAO.getContentWithFilter("", "TRANSFER_EXTENSION", "USAA", null);
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                originPhone = cu.getContentWithFilter(connection, ShipConstants.CONTENT_CONTEXT_PREFERRED_PARTNER,
                        ShipConstants.CONTENT_NAME_PREFERRED_PARTNER, partnerMasterVO.getPartnerName(), null);
            }
            else
                originPhone = companyVO.getPhoneNumber();
        }

        //Origin
        Element element = shipUnit.createElement(ShipConstants.TICKET_ORIGINATOR);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_NAME, originName);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_PHONE, originPhone);
        invoiceData.appendChild(element);

        //Recipient
        element = shipUnit.createElement(ShipConstants.TICKET_RECIPIENT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_NAME, venusMessageVO.getRecipient());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_COMPANY, venusMessageVO.getBusinessName());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR1, venusMessageVO.getAddress1());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR2, venusMessageVO.getAddress2());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_CITY, venusMessageVO.getCity());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_STATE, venusMessageVO.getState());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_POSTAL, venusMessageVO.getZip());
        invoiceData.appendChild(element);

        //Product
        element = shipUnit.createElement(ShipConstants.TICKET_PRODUCT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_PRODUCT_DESC, venusMessageVO.getProductDescription());
        invoiceData.appendChild(element);

        //Card message
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_CARD_MESSAGE,
                venusMessageVO.getCardMessage()));
        dynamicData.appendChild(invoiceData);

        //Guarantee (Leave blank if non is available.
        //SDS will default to the standard FTD.COM guarantee)
        //Defect 1809
        if (StringUtils.isBlank(guarantee))
        {
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_GUARANTEE));
        }
        else
        {
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_GUARANTEE, guarantee));
        }

        //Personal Greeting ID (Leave blank if none is available).
        if (StringUtils.isBlank(venusMessageVO.getPersonalGreetingID()))
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_PERSONAL_GREETING_ID));
        else
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_PERSONAL_GREETING_ID,
                    venusMessageVO.getPersonalGreetingID()));

        //Build the FTD_SHIP_UNITS section
        Element ftdShipUnits = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_FSU_FTD_SHIP_UNITS);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_ORDER_NUMBER,
                orderDetailVO.getExternalOrderNumber()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_ID,
                isSubcode ? orderDetailVO.getSubcode() : venusMessageVO.getProductId()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_DESC,
                venusMessageVO.getProductDescription()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_LOB,
                SDSConstants.LOB_FTDCOM));
        BigDecimal productVendorCost = new BigDecimal(svpVO.getVendorCost());
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_VENDOR_COST,
                String.format("%6.2f", productVendorCost.doubleValue())));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_WEIGHT,
                venusMessageVO.getProductWeight()));
        //add total add on cost to product cost to derive total vendor cost
        BigDecimal vendorAddOnCost = ((BigDecimal) addOnTotals.get(TOTAL_ADD_ON_COST));
        BigDecimal totalVendorCost = productVendorCost.add(vendorAddOnCost);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_TOTAL_VENDOR_COST,
                String.format("%6.2f", totalVendorCost.doubleValue())));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_VENDOR_SKU,
                svpVO.getVendorSku()));
        if (isMarketingInsertAllowed)
            ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_MARKETING_INSERT,
                    "Y"));
        else
            ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_MARKETING_INSERT,
                    "N"));

        dynamicData.appendChild(ftdShipUnits);

        Element ftdShipUnitAddOns = shipUnit.createElement(SDSConstants.TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ONS);
        Element ftdShipUnitAddOn = null;

       //Loop through the add ons
        AddOnVO aovo;
        GlobalParameterVO globalVO = orderDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "EXCLUDED_VASE_ADDONS");
        String excludedVaseAddons = globalVO.getValue();
        for (int idx2 = 0; idx2 < venusMessageVO.getAddOnVO().size(); idx2++)
        {
            aovo = venusMessageVO.getAddOnVO().get(idx2);
            boolean excludeAddon = false;
            StringTokenizer tokenizer = new StringTokenizer(excludedVaseAddons,",");
            while(tokenizer.hasMoreTokens()){
            	String excludedAddonId = tokenizer.nextToken();
            	if(aovo.getAddOnId().equalsIgnoreCase(excludedAddonId)){
            		excludeAddon = true;
            		break;
            	}
            }
            if(!excludeAddon)
            {
	            ftdShipUnitAddOn = shipUnit.createElement(SDSConstants.TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ON);
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_ID, aovo.getAddOnId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_DESCRIPTION, aovo.getAddOnDescription()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_TYPE, aovo.getAddOnTypeId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_TYPE_DESCRIPTION, aovo.getAddOnTypeDescription()));
	            HashMap vendorCostMap = aovo.getVendorCostsMap();
	            VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(svpVO.getVendorId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_VENDOR_ADD_ON_ID, vendorAddOnVO.getSKU()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_VENDOR_ADD_ON_COST, vendorAddOnVO.getCost()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_WEIGHT, aovo.getAddOnWeight()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_QUANTITY, aovo.getOrderQuantity()));
	            ftdShipUnitAddOns.appendChild(ftdShipUnitAddOn);
	            ftdShipUnitAddOns.appendChild(ftdShipUnitAddOn);
        		
            }
        }
        dynamicData.appendChild(ftdShipUnitAddOns);

        shipUnitRoot.appendChild(dynamicData);

        Element address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);

        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_DELIVER_TO));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
                StringUtils.substring(venusMessageVO.getBusinessName(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_INDIVIDUAL_NAME,
                StringUtils.substring(venusMessageVO.getRecipient(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
                StringUtils.substring(venusMessageVO.getAddress1(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
                StringUtils.substring(venusMessageVO.getAddress2(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
                venusMessageVO.getCity()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
                venusMessageVO.getState()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
                venusMessageVO.getZip()));

        //FIXME: Need the three char country code from the database
        //shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,SDSConstants.TAG_SHIP_SHIP_FOR_COUNTRY,venusMessageVO.getCountry()));
        String countryCode = venusMessageVO.getCountry();
        if (StringUtils.equals("US", countryCode))
        {
            countryCode = "USA";
        }
        else if (StringUtils.equals("CA", countryCode))
        {
            countryCode = "CAN";
        }
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COUNTRY, countryCode));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_PHONE_NUMBER,
                venusMessageVO.getPhoneNumber()));
        shipUnitRoot.appendChild(address);

        if (logisticsVO.getAvailableVendors().size() > 0)
        {
            //Add return address
	        address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
	                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_RETURN));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
	                SDSConstants.TAG_RATE_CREATE_RETURN_ADDRESS_COMPANY_NAME));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
	                StringUtils.substring(availableVendors.get(0).getAddress1(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
	                StringUtils.substring(availableVendors.get(0).getAddress2(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
	        		availableVendors.get(0).getCity()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
	        		availableVendors.get(0).getState()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
	        		availableVendors.get(0).getZipCode()));
	        shipUnitRoot.appendChild(address);
        }

        Element fedexUnit = null;
        if (fedexUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element upsUnit = null;
        if (upsUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element intlUnit = null;
        //Complete this when we go intl
        if (intlUnit != null)
        {
            shipUnitRoot.appendChild(intlUnit);
        }

        return logisticsVO;
    }

    /**
     * This method handles orders with ship date that are beyond the configured MAX_SHIP_DAYS_OUT. CreateShipment and
     * queue.
     *
     * @param connection
     * @param venusMessageVO
     * @param logisticsVO
     * @throws Exception
     */
    public void processOrderShipAfterMaxDays(Connection connection, VenusMessageVO venusMessageVO,
            SDSLogisticsVO logisticsVO) throws Exception
    {
        Document response = null;
        String shipUnit;
        try
        {
            logisticsVO = buildRequest(connection, venusMessageVO);
            
            if (logisticsVO.getAvailableVendors() == null || logisticsVO.getAvailableVendors().size() == 0)
            {
                String msg = "No vendors were available to service this venus order ("
                        + venusMessageVO.getVenusOrderNumber() + ").";
                LOGGER.error("processOrderShipAfterMaxDays -> " + msg);
                createComment(connection, msg, logisticsVO.getOrderDetailVO());
                rejectAndQueue(connection, logisticsVO, msg, true);
                return;
            }

            shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
            LOGGER.debug("processOrderShipAfterMaxDays -> Pending createShip request is " + shipUnit);

            response = sdsCommunications.createShipment(connection, logisticsVO.getShipUnit(), venusMessageVO.getVenusId());

            if (response == null)
            {
                throw new SDSApplicationException("processOrderShipAfterMaxDays -> Null response generated.  Check for timeout from SDS server.");
            }

        }
        finally
        {
            if (response != null)
            {
                insertSDSTransaction(connection, venusMessageVO.getVenusId(),
                        JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
            }
        }

        processResponse(connection, venusMessageVO, logisticsVO, response, "CreateShipUnitsPending");
    }

    /**
     * This method will process the order using the regular route (non-ZoneJump)
     *
     * @param connection
     * @param venusMessageVO
     * @param logisticsVO
     * @return boolean
     * @throws Exception
     */
    public void processOrder(Connection connection, VenusMessageVO venusMessageVO, SDSLogisticsVO logisticsVO,
            int idealRank, int actualRank) throws Exception
    {
        StringBuilder sb;
        Document response = null;
        String shipUnit;

        logisticsVO = buildRequest(connection, venusMessageVO, true);

        if (logisticsVO.getAvailableVendors() == null || logisticsVO.getAvailableVendors().size() == 0)
        {
            String msg = "No vendors were available to service this venus order ("
                    + venusMessageVO.getVenusOrderNumber() + ").";
            LOGGER.error("processOrder -> " + msg);
            createComment(connection, msg, logisticsVO.getOrderDetailVO());
            rejectAndQueue(connection, logisticsVO, msg, true);
            return;
        }

        shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
        LOGGER.debug("processOrder -> Regular rateShip request is " + shipUnit);

        sb = new StringBuilder();
        sb.append("Sending request to Argo:\r\n");
        sb.append("Order Number: ");
        sb.append(venusMessageVO.getVenusId());
        sb.append("\r\nForcing Shipment: ");
        sb.append(venusMessageVO.isForcedShipment());
        sb.append("\r\nShipment Already Rated: ");
        sb.append(venusMessageVO.isRatedShipment());
        sb.append("\r\nRequesting Response Data: ");
        sb.append(venusMessageVO.isGetSdsResponse());

        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        createComment(connection, sb.toString(), orderDetailVO);
        long startTime = (new Date()).getTime();
        try
        {
            response = sdsCommunications.rateShipment(connection, logisticsVO.getShipUnit(), venusMessageVO.getVenusId());

            if (response == null)
              throw new SDSApplicationException("processOrder -> Null response generated.  Check for timeout from SDS server.");

        }
        finally
        {
       	  	logServiceResponseTracking(venusMessageVO.getVenusId(), startTime, connection);
    	
        	if (response != null)
                insertSDSTransaction(connection, venusMessageVO.getVenusId(),
                        JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
        }

        //Create a HashMap of availableVendors
        List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
        HashMap hAvailableVendors = new HashMap();
        for (int i = 0; i < availableVendors.size(); i++)
        {
            ShipVendorProductVO svpVO = availableVendors.get(i);
            String vendorCode = StringUtils.trim(svpVO.getVendorCode());
            String vendorCodeFormatted = StringUtils.replace(vendorCode, "-", "");

            hAvailableVendors.put(vendorCodeFormatted, svpVO);
        }

        //call parseRateShipUnitsResponse to parse the response
        //Added delivery date to the method signature as part of USPS project.
        SDSRateResponseVO rateResponseVO = this.parseRateShipUnitsResponse(connection, response, hAvailableVendors,
                venusMessageVO.getVenusOrderNumber(), venusMessageVO.getVenusId(), idealRank, actualRank,
                venusMessageVO.getReferenceNumber(), venusMessageVO.getShipDate(), venusMessageVO.getDeliveryDate(), venusMessageVO.getShipMethod(),
                venusMessageVO.getZip());

        //and retrieve the shipmentOptions list
        List<ShipmentOptionVO> shipmentOptions = rateResponseVO.getShipmentOptions();

        //check to see if
        //1) response was successful
        //2) we received any ship methods - if yes, regardless of the ranks, insert into the table, otherwise, raise an
        //Exception
        if (!rateResponseVO.isSuccess() || shipmentOptions.size() == 0)
        {
            String msg = "No vendors were available to service this venus order ("
                    + venusMessageVO.getVenusOrderNumber() + ").";
            LOGGER.error("processOrder -> " + msg);
            createComment(connection, msg, logisticsVO.getOrderDetailVO());
            rejectAndQueue(connection, logisticsVO, msg, true);
            return;
        }

        logisticsVO.setRateResponse(rateResponseVO);
        ShipmentOptionVO createShipSoVO = null;
        boolean isOrderPending = false;

        //need to loop through the shipment options and see if secondary ship date is greater than 10 days out
        for (int i = 0; i < shipmentOptions.size(); i++)
        {
            ShipmentOptionVO soVO = shipmentOptions.get(i);
           
            //save the first actual ShipVendorProductVO as createShipVendor
            if (soVO.getActualRankNumber() != 0 && createShipSoVO == null)
                createShipSoVO = soVO;
            
            if(soVO.getActualRankNumber() == 1){
            	venusMessageVO.setShipDate(soVO.getDateShipped());
                venusMessageVO.setOriginalShipDate(soVO.getOriginalShipDate());
            	venusMessageVO.setSecondaryShipDate(soVO.getSecondaryShipDate());   
            	//check if secondary ship date was returned and if the ship date is > 10 days out
                //if it is put the order in pending status 
            	GlobalParameterVO gpVO = CommonUtils.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT,
                        ShipConstants.GLOBAL_PARMS_MAX_SHIP_DAYS_OUT, String.valueOf(DEFAULT_MAX_SHIP_DAYS_OUT));
            	int maxShipDaysOut = Integer.parseInt(gpVO.getValue());
            	boolean willOrderShipAfterMaxDays = willOrderShipAfterMaxDays(venusMessageVO, maxShipDaysOut);
            	if(willOrderShipAfterMaxDays){
            		processOrderShipAfterMaxDays(connection, venusMessageVO, logisticsVO);
            		isOrderPending = true;
            		break;
            	}
            }
           
        }
        
        if(!isOrderPending)
        {  
        
	        //if we received at least one ship method, regardless of the ranks, insert data into the table
	        for (int i = 0; i < shipmentOptions.size(); i++)
	        {
	            //insert data
	            ShipmentOptionVO soVO = shipmentOptions.get(i);
	            try
	            {
	                shipDAO.insertShipmentOption(connection, soVO);
	            }
	            catch (Exception e)
	            {
	                LOGGER.error("processOrder -> " + e);
	                CommonUtils.getInstance().sendSystemMessage(
	                        "Error occurred when inserting shipment option:" + venusMessageVO.getVenusId() + ".\nReason:"
	                                + e.getMessage());
	            }
	            //save the first actual ShipVendorProductVO as createShipVendor
	            if (soVO.getActualRankNumber() != 0 && createShipSoVO == null)
	                createShipSoVO = soVO;
	            
	            if(soVO.getActualRankNumber() == 1){
	            	venusMessageVO.setShipDate(soVO.getDateShipped());
	                venusMessageVO.setOriginalShipDate(soVO.getOriginalShipDate());
	            	venusMessageVO.setSecondaryShipDate(soVO.getSecondaryShipDate());            	
	            }
	           
	        }
	
	        //now, check to see if actual was rolled forward. If yes, we need to build and request a createShipmentXML
	        if (createShipSoVO == null)
	        {
	            //If status of fail, then follow non ZoneJump ship logic
	            String msg = "No vendors were available to service this venus order ("
	                    + venusMessageVO.getVenusOrderNumber() + ").";
	            LOGGER.error("processOrder -> " + msg);
	            createComment(connection, msg, logisticsVO.getOrderDetailVO());
	            rejectAndQueue(connection, logisticsVO, msg, true);
	            return;
	
	        }
	        else
	        {
	            /** recreate availableVendors using one vendor, and set logistics.availableVendors() **/
	            //new lists to be appended to the logisticsVO
	            List<ShipVendorProductVO> newAvailableVendors = new ArrayList<ShipVendorProductVO>();
	            List<ShipmentOptionVO> newShipmentOptions = new ArrayList<ShipmentOptionVO>();
	
	            //retrieve vendor code
	            String vendorCode = StringUtils.trim(createShipSoVO.getDistributionCenter());
	            //create ShipVendorProductVO
	            ShipVendorProductVO createShipSvpVO = (ShipVendorProductVO) hAvailableVendors.get(vendorCode);
	            //add ShipVendorProductVO to the list
	            newAvailableVendors.add(createShipSvpVO);
	            //and append the ShipVendorProductVO to the list
	            logisticsVO.setAvailableVendors(newAvailableVendors);
	
	            //add ShipmentOptionVO to the list
	            newShipmentOptions.add(createShipSoVO);
	            //and append the ShipmentOptionVO to the list
	            logisticsVO.setShipmentOptions(newShipmentOptions);
	
	            //build and request a CreateShipmentXML that bypasses rate shopping
	            logisticsVO = this.generateCreateShipUnitsRequest(connection, logisticsVO);
	
	            sb = new StringBuilder();
	            sb.append("Sending ship request to Argo:\r\n");
	            sb.append("Order Number: ");
	            sb.append(venusMessageVO.getVenusId());
	            sb.append("\r\nForcing Shipment: ");
	            sb.append(venusMessageVO.isForcedShipment());
	            sb.append("\r\nShipment Already Rated: ");
	            sb.append(venusMessageVO.isRatedShipment());
	            sb.append("\r\nRequesting Response Data: ");
	            sb.append(venusMessageVO.isGetSdsResponse());
	
	            shipUnit = JAXPUtil.toString(logisticsVO.getShipUnit());
	            LOGGER.debug("processOrder -> Regular createShip request is " + shipUnit);
	
	            createComment(connection, sb.toString(), orderDetailVO);
	
	            try
	            {
	                response = sdsCommunications.createShipment(connection, logisticsVO.getShipUnit(), venusMessageVO.getVenusId());
	
	                if (response == null)
	                    throw new SDSApplicationException("processOrder -> Null response generated.  Check for timeout from SDS server.");
	            }
	            finally
	            {
	                if (response != null)
	                    insertSDSTransaction(connection, venusMessageVO.getVenusId(),
	                            JAXPUtil.toString(logisticsVO.getShipUnit()), JAXPUtil.toString(response));
	            }
	
	            processResponse(connection, venusMessageVO, logisticsVO, response, "CreateShipUnits");
	
	        }
        }
    }

    /**
     * Parse the rate response for a regular (non-ZoneJump) request.
     *
     * @param responseDoc
     * @return
     * @throws SDSApplicationException
     */
    private SDSRateResponseVO parseRateShipUnitsResponse(Connection connection, Document responseDoc,
            HashMap hAvailableVendors, String venusOrderNumber, String venusId, int originalIdealRank,
            int originalActualRank, String orderDetailId, Date shipDate, Date deliveryDate, String vShipMethod, String zipCode) throws SDSApplicationException
    {
        SDSRateResponseVO responseVO = new SDSRateResponseVO();
        List<ShipmentOptionVO> shipmentOptions = new ArrayList<ShipmentOptionVO>();

        int newIdealRank = originalIdealRank;
        int newActualRank = originalActualRank;
        boolean bShipMethodBlockExists;
        String errorNumberMain = null;
        String errorDescriptionMain = null;
        String strResponse = null;
        Date vOrginalShipDate = null;
        Date vSecondaryShipDate = null;
        boolean vendorCanStillShip;

        responseVO.setShipmentOptions(shipmentOptions);

        try
        {
            strResponse = JAXPUtil.toString(responseDoc);
            LOGGER.info("parseRateShipUnitsResponse ->  Regular rateShip response is " + strResponse);
        }
        catch (Exception e)
        {
            LOGGER.warn("parseRateShipUnitsResponse -> Error received while trying to determine if an \"Error\" node exists.  This is not fatal.");
        }

        //In V14, status node does not exist. Once we get the response, check RATE_SHIP_UNIT_DOC/SHIP_UNIT_RATE_INFO_ROW/ERROR
        //There will be cases where ScanData will send an error node at this level: example, ScanData cannot even parse the request
        try
        {
            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_RATE_ERROR, SDSConstants.NAMESPACE_PREFIX,
                    SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseRateShipUnitsResponse -> nlErrorMain.getLength() = " + nlErrorMain.getLength());
            if (nlErrorMain.getLength() > 0)
            {
                Element errorElementMain = (Element) nlErrorMain.item(0);
                errorNumberMain = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_NUMBER);
                errorDescriptionMain = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseRateShipUnitsResponse -> errorNumberMain = " + errorNumberMain);
            LOGGER.debug("parseRateShipUnitsResponse -> errorDescriptionMain = " + errorDescriptionMain);
        }
        catch (Exception e)
        {
            throw new SDSApplicationException("parseRateShipUnitsResponse -> Failed to retrieve error info rate response.", e);
        }


        if (StringUtils.isBlank(errorNumberMain))
        {
            responseVO.setStatusCode("0");
            responseVO.setMsgText("");
            responseVO.setSuccess(true);

            try
            {
                String errorNumber = null;
                String errorDescription = null;
                NodeList nl = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_RATE_SHIP_UNIT_RATE_INFO, SDSConstants.NAMESPACE_PREFIX,
                        SDSConstants.NAMESPACE_URI);

                LOGGER.info("parseRateShipUnitsResponse -> Parsing the rate response - nl length = " + nl.getLength());
                                
                for (int i = 0; i < nl.getLength(); i++)
                {
                	errorNumber = null;
                	errorDescription = null;
                    Element shipElem = (Element) nl.item(i);
                    String carrierId = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_CREATE_CARRIER_ID);
                    String shipMethodId = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_CREATE_SHIP_METHOD_ID);
                    String deliveryDateStr = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_EXPECTED_DELIVERY_DATE);
                    deliveryDateStr = StringUtils.trimToEmpty(deliveryDateStr);
                    
                    Date expectedDeliveryDate = null; 
                    if(!StringUtils.isBlank(deliveryDateStr)){
                    	expectedDeliveryDate = tzDateTimeToDate(deliveryDateStr);
                    }
                    
                    String shipPointId = (shipElem.getElementsByTagName(SDSConstants.TAG_SHIP_POINT_ID).item(0)).getFirstChild().getNodeValue();
                    String zoneCode = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_ZONE_CODE);
                    String rateWeight = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_RATE_WEIGHT);
                    String rateCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_RATE_CHARGE);
                    String handlingCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_HANDLING_CHARGE);
                    String totalCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_TOTAL_CHARGE);
                    String freightCharge = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_FREIGHT_CHARGE);
                    String distributionCenter = StringUtils.trim(getFirstChildNoNull(shipElem,
                            SDSConstants.TAG_DISTRIBUTION_CENTER));
                    String secondaryShipDateStr = getFirstChildNoNull(shipElem, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT);
                    secondaryShipDateStr = StringUtils.trimToEmpty(secondaryShipDateStr);
                    
                    Date secondaryShipDate = null; 
                    if(!StringUtils.isBlank(secondaryShipDateStr)){
                    	secondaryShipDate = tzDateTimeToDate(secondaryShipDateStr);
                    }
           
                    NodeList nlError = shipElem.getElementsByTagName(SDSConstants.TAG_ERROR);
                    LOGGER.debug("parseRateShipUnitsResponse -> i = " + i + " and nlError.getLength() = " + nlError.getLength());
                    if (nlError.getLength() > 0)
                    {
                        Element errorElement = (Element) nlError.item(0);
                        errorNumber = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_NUMBER);
                        errorDescription = getFirstChildNoNull(errorElement, SDSConstants.TAG_ERROR_DESCRIPTION);
                    }
                    LOGGER.debug("parseRateShipUnitsResponse -> errorNumber = " + errorNumber);
                    LOGGER.debug("parseRateShipUnitsResponse -> errorDescription = " + errorDescription);
                    /*
                     * Logic to check if the expected delivery date is less than original delivery date
                     */
                    if(deliveryDate != null && expectedDeliveryDate != null && expectedDeliveryDate.compareTo(deliveryDate)<0){
                    	LOGGER.debug("Expected delivery date " +expectedDeliveryDate+ " returned from SCAN DATA is less than original delivery Date " +deliveryDate);
                    	errorNumber = "FTD_Delivery_Date_Check";
                    	errorDescription = "Expected delivery date " +deliveryDateStr+ " returned from SCAN DATA is less than original delivery Date " +deliveryDate.toString();
                    	
                    }

                    //Create shipment option and add it to the list
                    ShipmentOptionVO soVO = new ShipmentOptionVO();

                    soVO.setVenusOrderNumber(venusOrderNumber);
                    soVO.setZoneJumpFlag("N");
                    soVO.setCarrierId(carrierId);
                    soVO.setShipMethodId(shipMethodId);
                    if (zoneCode != null && StringUtils.isNotBlank(zoneCode))
                        soVO.setZoneCode(Integer.valueOf(zoneCode));
                    if (rateWeight != null && StringUtils.isNotBlank(rateWeight))
                        soVO.setRateWeight(new BigDecimal(rateWeight));
                    if (rateCharge != null && StringUtils.isNotBlank(rateCharge))
                        soVO.setRateCharge(new BigDecimal(rateCharge));
                    if (handlingCharge != null && StringUtils.isNotBlank(handlingCharge))
                        soVO.setHandlingCharge(new BigDecimal(handlingCharge));
                    if (totalCharge != null && StringUtils.isNotBlank(totalCharge))
                        soVO.setTotalCharge(new BigDecimal(totalCharge));
                    if (freightCharge != null && StringUtils.isNotBlank(freightCharge))
                        soVO.setFreightCharge(new BigDecimal(freightCharge));

                    //ScanData is not sending this ship date in the V14 rate response XML any more... have to get it from VenusMessageVO
                    if(shipDate != null && secondaryShipDate != null && secondaryShipDate.compareTo(shipDate)!=0){
                    	LOGGER.debug("Secondary ship date " +secondaryShipDate+ " returned from SCAN DATA is different than the ship date on the order " +shipDate);
                    	//set original ship date and secondary ship date on the ShipmentOptionVO
                    	vOrginalShipDate = shipDate;
                    	vSecondaryShipDate = secondaryShipDate;
                    	soVO.setOriginalShipDate(vOrginalShipDate);
                    	soVO.setSecondaryShipDate(vSecondaryShipDate);
                        soVO.setDateShipped(vSecondaryShipDate);
                    }
                    else
                    {
                    	soVO.setDateShipped(shipDate);
                    }	                         

                    if (deliveryDateStr != null && StringUtils.isNotBlank(deliveryDateStr))
                        soVO.setExpectedDeliveryDate(tzDateTimeToDate(deliveryDateStr));
                    soVO.setDistributionCenter(distributionCenter);
                    soVO.setShipPointId(shipPointId);
                    if (StringUtils.isNotBlank(errorNumber))
                        soVO.setErrorTxt("ERROR NUMBER = " + errorNumber + " and ERROR DESCRIPTION = "
                                + errorDescription);
                    else
                        soVO.setErrorTxt(null);

                                   
                    //check if an error was returned. If an error was not returned, initialize ideal and actual ranks
                    if (StringUtils.isBlank(soVO.getErrorTxt()))
                    {
                        //ideal rank will simply be incremented
                        newIdealRank++;
                        soVO.setIdealRankNumber(newIdealRank);

                        //for actual rank, see if the vendor is available. If yes, increment the actual rank. If the
                        //vendor is not available, the actual rank will be 0.
                        ShipVendorProductVO svpVO = (ShipVendorProductVO) hAvailableVendors.get(StringUtils.trim(soVO.getDistributionCenter()));

                        //check to see if a Ship Method Blocks exists
                        bShipMethodBlockExists = shipDAO.shipMethodBlockExists(connection, svpVO.getVendorId(),
                                svpVO.getProductSkuId(), soVO.getExpectedDeliveryDate(), soVO.getCarrierId(),
                                soVO.getShipMethodId());
                        LOGGER.debug("parseRateShipUnitsResponse -> vendorId = " + svpVO.getVendorId()
                                + " and productId = " + svpVO.getProductSkuId() + " and deliveryDate = "
                                + deliveryDateStr + " and carrierId = " + soVO.getCarrierId()
                                + " and sdsShipMethodId = " + soVO.getShipMethodId()
                                + " ---- bShipMethodBlockExists = " + new Boolean(bShipMethodBlockExists).toString());

                        //check to see if Vendor can ship on Secondary Ship date if one exists
                        vendorCanStillShip = true;
                        LOGGER.debug("check to see if secondary ship date exists -> vendorId = " + svpVO.getVendorId()
                                + " and productId = " + svpVO.getProductSkuId());
                        LOGGER.debug("getSecondaryShipDate(): " + soVO.getSecondaryShipDate());
                        if (soVO.getSecondaryShipDate() != null){
                        	vendorCanStillShip = shipDAO.vendorCanStillShip(connection, svpVO.getProductSkuId(), soVO.getSecondaryShipDate(), svpVO.getVendorId());
                        	LOGGER.info("vendorCanStillShip: " + vendorCanStillShip);
                        }
                        LOGGER.debug("check to see if this is a morning delivery order if the vendor can ship the order -> vendorId = " + svpVO.getVendorId()
                                + " and productId = " + svpVO.getProductSkuId());
                        if (orderDAO.orderHasMorningDeliveryFee(connection, orderDetailId)){
                        	vendorCanStillShip = shipDAO.vendorCanStillShip(connection, svpVO.getProductSkuId(), soVO.getDateShipped(), svpVO.getVendorId());
                        	LOGGER.info("vendorCanStillShip: " + vendorCanStillShip);
                        }
                        
                        //check if vendor has all associated add ons available
                        AddOnUtility addOnUTIL = new AddOnUtility();
                        boolean vendorAddOnsAvailable = addOnUTIL.vendorAddOnAvailable(venusId, svpVO.getVendorId(),
                                connection);

                        //if the vendor/product/add on is available and there is no Ship Method Block, increment the
                        //actual rank
                        if (svpVO.isAvailable() && !bShipMethodBlockExists && vendorAddOnsAvailable && vendorCanStillShip)
                        {
                        	
                        	if (orderDAO.orderHasMorningDeliveryFee(connection, orderDetailId))
                            {
                                if (soVO.getShipMethodId().equalsIgnoreCase("PRIO OVNT")
                                        || soVO.getShipMethodId().equalsIgnoreCase("UPSND"))
                                {
                                    String deliveryType = null;
                                    
                                    if(vShipMethod.equalsIgnoreCase("SA"))
                                    {
                                    	deliveryType = "SATURDAY";
                                    }
                                    else
                                    {
                                    	deliveryType = "WEEKDAY";                                    	
                                    }
                                                                      
                                	if(shipDAO.carrierAllowsMorningDelivery(connection, soVO.getCarrierId(), deliveryType, zipCode))
                                	{
                                		newActualRank++;
                                		soVO.setActualRankNumber(newActualRank);
                                	}
                                	else
                                	{
                                		soVO.setActualRankNumber(0);
                                	}
                                }
                                else
                                {
                                    soVO.setActualRankNumber(0);
                                }
                            }
                            else
                            {
                                newActualRank++;
                                soVO.setActualRankNumber(newActualRank);
                            }

                        }
                        //else set the actual rank to 0
                        else
                            soVO.setActualRankNumber(0);
                    }
                    //If an error was returned, the ideal and actual ranks will be 0
                    else
                    {
                        soVO.setIdealRankNumber(0);
                        soVO.setActualRankNumber(0);
                    }

                    shipmentOptions.add(soVO);
                }
            }
            catch (Exception e)
            {
                LOGGER.error("parseRateShipUnitsResponse -> " + e);
                throw new SDSApplicationException("parseRateShipUnitsResponse -> Exception while locating ship methods in rate request response.", e);
            }
            responseVO.setShipmentOptions(shipmentOptions);
        }
        else
        {
            if (StringUtils.equals(errorNumberMain, SDSConstants.STATUS_SDS_APP_EXCEPTION))
            {
                //If an exception is being thrown from the sds ship server, then put
                //the entire response in the description field
                LOGGER.warn("parseRateShipUnitsResponse -> The SDS ship server has thrown an exception.");
                errorDescriptionMain = strResponse;
            }
            else if (StringUtils.isBlank(errorDescriptionMain))
            {
                LOGGER.debug("parseRateShipUnitsResponse -> No status description found in rate response ZoneJump.  Putting entire response in message text.");
                errorDescriptionMain = strResponse;
            }
            else
                LOGGER.debug("parseRateShipUnitsResponse -> The SDS ship server has thrown an error.  Error Number = " + errorNumberMain
                        + " and Error Description " + errorDescriptionMain);

            responseVO.setStatusCode(errorNumberMain);
            responseVO.setMsgText(errorDescriptionMain);
            responseVO.setSuccess(false);
        }

        return responseVO;
    }

    /**
     * Creates shipment request. Note that this method will build the createShipXML for a regular order that has ALREADY
     * been rate shopped. It will force ScanData to select vendor/carrier as dictated by SDSLogisticsVO
     *
     * @param connection
     * @param logisticsVO
     * @return
     * @throws Exception
     */
    private SDSLogisticsVO generateCreateShipUnitsRequest(Connection connection, SDSLogisticsVO logisticsVO)
            throws Exception
    {
        VenusMessageVO venusMessageVO = logisticsVO.getMessageVo();
        CompanyVO companyVO = logisticsVO.getCompanyVO();
        List<ShipVendorProductVO> availableVendors = logisticsVO.getAvailableVendors();
        ShipVendorProductVO svpVO = new ShipVendorProductVO();
        
        if(availableVendors != null && availableVendors.size() > 0 && availableVendors.get(0) != null) {
        	svpVO = availableVendors.get(0);
        }
        
        List<ShipmentOptionVO> shipmentOptions = logisticsVO.getShipmentOptions();
        ShipmentOptionVO soVO = shipmentOptions.get(0);

        ProductVO productVO = logisticsVO.getProductVO();
        OrderDetailVO orderDetailVO = logisticsVO.getOrderDetailVO();
        boolean isMarketingInsertAllowed = true;
        boolean compAddonFlag = false;

        //Is the product a subcode?
        String productId = orderDetailVO.getProductId();
        String subcodeId = orderDetailVO.getSubcode();
        boolean isSubcode;
        if (StringUtils.isBlank(orderDetailVO.getSubcode()) || StringUtils.equals(productId, subcodeId))
        {
            isSubcode = false;
        }
        else
        {
            isSubcode = true;
        }

        //Determine if this is a USAA preferred partner
        PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
        if (partnerVO != null)
            isMarketingInsertAllowed = partnerVO.isMarketingInsertAllowed();

        HashMap addOnTotals = this.calculateAddOnTotals(venusMessageVO, svpVO);

        Document shipUnit = JAXPUtil.createDocument();
        Element docRoot = shipUnit.createElement("CREATE_SHIP_UNITS_PARAMS");
        docRoot.setAttribute("xmlns", "http://ScanData.com/WTM/XMLSchemas/WTM_XMLSchema_14.00.0000.xsd");
        docRoot.setAttribute("MSN", "1");

        shipUnit.appendChild(docRoot);
        Element shipUnitRoot = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIP_UNIT);
        docRoot.appendChild(shipUnitRoot);
        logisticsVO.setShipUnit(shipUnit);

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_CARTON_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_DISTRIBUTION_CENTER,
                StringUtils.replace(svpVO.getVendorCode(), "-", "")));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORDER_NUMBER,
                venusMessageVO.getVenusOrderNumber()));
        //companyVO.getBrand()));
        //shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_SHIP_BILLING_ACCOUNT_ID,
        //trip.getBillingAccount()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
                SDSConstants.TAG_RATE_CREATE_DATE_BEST_METHOD_PARAMETER,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getDeliveryDate())));

        //add total add on weight to product weight to derive estimated order weight
        BigDecimal productWeight = new BigDecimal(venusMessageVO.getProductWeight());
        BigDecimal totalAddOnWeight = ((BigDecimal) addOnTotals.get(TOTAL_ADD_ON_WEIGHT));
        BigDecimal totalOrderWeight = productWeight.add(totalAddOnWeight);

        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ESTIMATED_WEIGHT,
                totalOrderWeight.toString()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_STATUS,
                logisticsVO.isRateShopOnly()?SDSConstants.STATUS_PENDING:SDSConstants.STATUS_INITIAL));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_DATE_PLANNED_SHIPMENT,
                (new SimpleDateFormat(SDS_DATE_FORMAT)).format(venusMessageVO.getShipDate())));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_CARRIER_ID,
                soVO.getCarrierId()));
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_SHIP_METHOD_ID,
                soVO.getShipMethodId()));

        if (svpVO != null)
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    svpVO.getProductSkuId()));
        }
        else
        {
            shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_MASTER_ITEM_ID,
                    venusMessageVO.getProductId()));
        }
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_SHIP_POINT_ID,
                soVO.getShipPointId()));

        Element shipOptions = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS);
        shipUnitRoot.appendChild(shipOptions);

        String shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_COMMERCIAL;
        //Service options
        String serviceOptions = "";
        CustomerVO recipientVO = logisticsVO.getRecipientVO();
        if (recipientVO != null && StringUtils.equals(recipientVO.getAddressType(), ADDRESS_TYPE_RESIDENTIAL))
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_RESIDENTIAL;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(venusMessageVO.getDeliveryDate());
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_SATURDAY;
        }

        if (productVO.isOver21())
        {
            shipOption = SDSConstants.TAG_RATE_CREATE_SHIPPING_OPTIONS_OVER21;
        }
        Element shipOptionElement = shipUnit.createElement(shipOption);
        shipOptions.appendChild(shipOptionElement);
        shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_WEIGHT, totalOrderWeight.toString()));

        Element dynamicData = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_DATA_XML);
        Element origins = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS);

        Element origin = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_ORIGIN);
        origin.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_DIST_CENTER,
                StringUtils.replace(svpVO.getVendorCode(), "-", "")));
        Element carriers = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIERS);
        Element carrier = null;
        carrier = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER);
        carrier.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ORIGINS_CARRIER_ID,
                soVO.getCarrierId()));

        carriers.appendChild(carrier);
        origin.appendChild(carriers);

        origins.appendChild(origin);

        dynamicData.appendChild(origins);

        Element invoiceData = shipUnit.createElement(ShipConstants.TICKET_ROOT);

        //E number
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_ORDER,
                venusMessageVO.getVenusOrderNumber()));

        //Add partner data to the label
        String originName = null;
        String originPhone = null;
        String guarantee = null;

        if (StringUtils.isBlank(originName))
        {
            originName = companyVO.getCompanyName();
        }

        if (StringUtils.isBlank(originPhone))
        {
            PartnerVO partnerMasterVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
            String prefPartner = null;
            if (partnerMasterVO != null)
            {
                prefPartner = partnerMasterVO.getPartnerName();
            }
            if (StringUtils.isNotBlank(prefPartner))
            {
                //String partnerPhoneNumber = oDAO.getContentWithFilter("", "TRANSFER_EXTENSION", "USAA", null);
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                originPhone = cu.getContentWithFilter(connection, ShipConstants.CONTENT_CONTEXT_PREFERRED_PARTNER,
                        ShipConstants.CONTENT_NAME_PREFERRED_PARTNER, partnerMasterVO.getPartnerName(), null);
            }
            else
                originPhone = companyVO.getPhoneNumber();
        }

        //Origin
        Element element = shipUnit.createElement(ShipConstants.TICKET_ORIGINATOR);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_NAME, originName);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_ORIGINATOR_PHONE, originPhone);
        invoiceData.appendChild(element);

        //Recipient
        element = shipUnit.createElement(ShipConstants.TICKET_RECIPIENT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_NAME, venusMessageVO.getRecipient());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_COMPANY, venusMessageVO.getBusinessName());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR1, venusMessageVO.getAddress1());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_ADDR2, venusMessageVO.getAddress2());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_CITY, venusMessageVO.getCity());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_STATE, venusMessageVO.getState());
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_RECIPIENT_POSTAL, venusMessageVO.getZip());
        invoiceData.appendChild(element);

        //Product
        element = shipUnit.createElement(ShipConstants.TICKET_PRODUCT);
        JAXPUtil.addAttribute(element, ShipConstants.TICKET_PRODUCT_DESC, venusMessageVO.getProductDescription());
        invoiceData.appendChild(element);

        //Card message
        invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_CARD_MESSAGE,
                venusMessageVO.getCardMessage()));
        dynamicData.appendChild(invoiceData);

        //Guarantee (Leave blank if none is available).
        //SDS will default to the standard FTD.COM guarantee)
        if (StringUtils.isBlank(guarantee))
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_GUARANTEE));
        else
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_GUARANTEE, guarantee));

        //Personal Greeting ID (Leave blank if none is available).
        if (StringUtils.isBlank(venusMessageVO.getPersonalGreetingID()))
            invoiceData.appendChild(shipUnit.createElement(ShipConstants.TICKET_PERSONAL_GREETING_ID));
        else
            invoiceData.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, ShipConstants.TICKET_PERSONAL_GREETING_ID,
                    venusMessageVO.getPersonalGreetingID()));

        //Build the FTD_SHIP_UNITS section
        Element ftdShipUnits = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_FSU_FTD_SHIP_UNITS);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_ORDER_NUMBER,
                orderDetailVO.getExternalOrderNumber()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_ID,
                isSubcode ? orderDetailVO.getSubcode() : venusMessageVO.getProductId()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_DESC,
                venusMessageVO.getProductDescription()));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_LOB,
                SDSConstants.LOB_FTDCOM));
        BigDecimal productVendorCost = new BigDecimal(svpVO.getVendorCost());
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_VENDOR_COST,
                String.format("%6.2f", productVendorCost.doubleValue())));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_PRODUCT_WEIGHT,
                venusMessageVO.getProductWeight()));
        //add total add on cost to product cost to derive total vendor cost
        BigDecimal vendorAddOnCost = ((BigDecimal) addOnTotals.get(TOTAL_ADD_ON_COST));
        BigDecimal totalVendorCost = productVendorCost.add(vendorAddOnCost);
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_TOTAL_VENDOR_COST,
                String.format("%6.2f", totalVendorCost.doubleValue())));
        ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_VENDOR_SKU,
                svpVO.getVendorSku()));
        if (isMarketingInsertAllowed)
            ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_MARKETING_INSERT,
                    "Y"));
        else
            ftdShipUnits.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_FSU_MARKETING_INSERT,
                    "N"));
        dynamicData.appendChild(ftdShipUnits);

        Element ftdShipUnitAddOns = shipUnit.createElement(SDSConstants.TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ONS);
        Element ftdShipUnitAddOn = null;

        //Loop through the add ons
        AddOnVO aovo;
        GlobalParameterVO globalVO = orderDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "EXCLUDED_VASE_ADDONS");
        String excludedVaseAddons = globalVO.getValue();
        for (int idx2 = 0; idx2 < venusMessageVO.getAddOnVO().size(); idx2++)
        {
            aovo = venusMessageVO.getAddOnVO().get(idx2);
            boolean excludeAddon = false;
            StringTokenizer tokenizer = new StringTokenizer(excludedVaseAddons,",");
            while(tokenizer.hasMoreTokens()){
            	String excludedAddonId = tokenizer.nextToken();
            	if(aovo.getAddOnId().equalsIgnoreCase(excludedAddonId)){
            		excludeAddon = true;
            		break;
            	}
            }
            if(!excludeAddon)
            {
	            ftdShipUnitAddOn = shipUnit.createElement(SDSConstants.TAG_CREATE_FSUAO_FTD_SHIP_UNIT_ADD_ON);
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_ID, aovo.getAddOnId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_DESCRIPTION, aovo.getAddOnDescription()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_TYPE, aovo.getAddOnTypeId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_TYPE_DESCRIPTION, aovo.getAddOnTypeDescription()));
	            HashMap vendorCostMap = aovo.getVendorCostsMap();
	            VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(svpVO.getVendorId()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_VENDOR_ADD_ON_ID, vendorAddOnVO.getSKU()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_VENDOR_ADD_ON_COST, vendorAddOnVO.getCost()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_WEIGHT, aovo.getAddOnWeight()));
	            ftdShipUnitAddOn.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,
	                    SDSConstants.TAG_CREATE_FSUAO_ADD_ON_QUANTITY, aovo.getOrderQuantity()));
	            ftdShipUnitAddOns.appendChild(ftdShipUnitAddOn);
            }
   
	}

        dynamicData.appendChild(ftdShipUnitAddOns);

        shipUnitRoot.appendChild(dynamicData);

        Element address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);

        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_DELIVER_TO));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
                StringUtils.substring(venusMessageVO.getBusinessName(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_INDIVIDUAL_NAME,
                StringUtils.substring(venusMessageVO.getRecipient(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
                StringUtils.substring(venusMessageVO.getAddress1(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
                StringUtils.substring(venusMessageVO.getAddress2(), 0, 40)));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
                venusMessageVO.getCity()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
                venusMessageVO.getState()));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
                venusMessageVO.getZip()));

        //FIXME: Need the three char country code from the database
        //shipUnitRoot.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit,SDSConstants.TAG_SHIP_SHIP_FOR_COUNTRY,venusMessageVO.getCountry()));
        String countryCode = venusMessageVO.getCountry();
        if (StringUtils.equals("US", countryCode))
        {
            countryCode = "USA";
        }
        else if (StringUtils.equals("CA", countryCode))
        {
            countryCode = "CAN";
        }
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COUNTRY, countryCode));
        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_PHONE_NUMBER,
                venusMessageVO.getPhoneNumber()));
        shipUnitRoot.appendChild(address);

        if (logisticsVO.getAvailableVendors().size() > 0)
        {
            //Add return address
	        address = shipUnit.createElement(SDSConstants.TAG_RATE_CREATE_ADDRESS);
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS,
	                SDSConstants.TAG_RATE_CREATE_ADDRESS_CLASS_RETURN));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_COMPANY_NAME,
	                SDSConstants.TAG_RATE_CREATE_RETURN_ADDRESS_COMPANY_NAME));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STREET_ADDRESS,
	                StringUtils.substring(availableVendors.get(0).getAddress1(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ADDRESS1,
	                StringUtils.substring(availableVendors.get(0).getAddress2(), 0, 40)));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_CITY,
	        		availableVendors.get(0).getCity()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_STATE,
	        		availableVendors.get(0).getState()));
	        address.appendChild(JAXPUtil.buildSimpleXmlNode(shipUnit, SDSConstants.TAG_RATE_CREATE_ADDRESS_ZIP_CODE,
	        		availableVendors.get(0).getZipCode()));
	        shipUnitRoot.appendChild(address);
        }

        Element fedexUnit = null;
        if (fedexUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element upsUnit = null;
        if (upsUnit != null)
        {
            shipUnitRoot.appendChild(fedexUnit);
        }

        Element intlUnit = null;
        //Complete this when we go intl
        if (intlUnit != null)
        {
            shipUnitRoot.appendChild(intlUnit);
        }

        return logisticsVO;
    }

    /**
     * Helper method to calculate the add on totals for weight and cost Returns the following fields in a HashMap:
     * TOTAL_ADD_ON_WEIGHT: value of total add on weight per order TOTAL_ADD_ON_COST: value of total add on cost per
     * order
     *
     * @param VenusMessageVO
     * @param ShipVendorProductVO
     * @return HashMap
     * @throws Exception
     */
    private HashMap calculateAddOnTotals(VenusMessageVO venusMessageVO, ShipVendorProductVO svpVO) throws Exception
    {
        HashMap totalsMap = new HashMap();
        ArrayList<AddOnVO> addOnVOList = venusMessageVO.getAddOnVO();

        AddOnVO addOnVO;

        BigDecimal totalAddOnCost = new BigDecimal("0");
        BigDecimal totalAddOnWeight = new BigDecimal("0");

        for (Iterator it = addOnVOList.iterator(); it.hasNext();)
        {
            addOnVO = (AddOnVO) it.next();
            {
                HashMap vendorCostMap = addOnVO.getVendorCostsMap();
                VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(svpVO.getVendorId()));

                String perAddOnPrice = vendorAddOnVO.getCost();
                if (perAddOnPrice == null)
                    perAddOnPrice = "0";

                Integer addOnQty = addOnVO.getOrderQuantity();
                if (addOnQty == null)
                    addOnQty = 0;

                String perAddOnWeight = addOnVO.getAddOnWeight();
                if (perAddOnWeight == null)
                    perAddOnWeight = "0";

                BigDecimal addOnPrice = new BigDecimal(perAddOnPrice);
                BigDecimal addOnQuanity = new BigDecimal(addOnQty);
                BigDecimal addOnWeight = new BigDecimal(perAddOnWeight);

                BigDecimal cost = new BigDecimal(0);
                cost = addOnPrice.multiply(addOnQuanity);
                totalAddOnCost = totalAddOnCost.add(cost);

                BigDecimal weight = new BigDecimal(0);
                weight = addOnWeight.multiply(addOnQuanity);
                totalAddOnWeight = totalAddOnWeight.add(weight);
            }
        }

        totalsMap.put(TOTAL_ADD_ON_WEIGHT, totalAddOnWeight);
        totalsMap.put(TOTAL_ADD_ON_COST, totalAddOnCost);

        return totalsMap;
    }

    /**
     * Helper method to calculate the add on total for weight
     *
     * @param VenusMessageVO
     * @return BigDecimal
     * @throws Exception
     */
    private BigDecimal calculateTotalAddOnWeight(VenusMessageVO venusMessageVO) throws Exception
    {
        BigDecimal totalAddOnWeight = new BigDecimal("0");

        ArrayList<AddOnVO> addOnVOList = venusMessageVO.getAddOnVO();

        AddOnVO addOnVO;

        for (Iterator it = addOnVOList.iterator(); it.hasNext();)
        {
            addOnVO = (AddOnVO) it.next();
            {
                Integer addOnQty = addOnVO.getOrderQuantity();
                if (addOnQty == null)
                    addOnQty = 0;

                String perAddOnWeight = addOnVO.getAddOnWeight();
                if (perAddOnWeight == null)
                    perAddOnWeight = "0";

                BigDecimal addOnQuanity = new BigDecimal(addOnQty);
                BigDecimal addOnWeight = new BigDecimal(perAddOnWeight);

                BigDecimal weight = new BigDecimal(0);
                weight = addOnWeight.multiply(addOnQuanity);
                totalAddOnWeight = totalAddOnWeight.add(weight);
            }
        }

        return totalAddOnWeight;
    }

    /**
     * Helper method to calculate the add on total for cost
     *
     * @param VenusMessageVO
     * @return BigDecimal
     * @throws Exception
     */
    private BigDecimal calculateTotalAddOnCost(VenusMessageVO venusMessageVO, String vendorId) throws Exception
    {

        BigDecimal totalAddOnCost = new BigDecimal("0");

        ArrayList<AddOnVO> addOnVOList = venusMessageVO.getAddOnVO();

        AddOnVO addOnVO;

        for (Iterator it = addOnVOList.iterator(); it.hasNext();)
        {
            addOnVO = (AddOnVO) it.next();
            {
                HashMap vendorCostMap = addOnVO.getVendorCostsMap();

                VendorAddOnVO vendorAddOnVO = ((VendorAddOnVO) vendorCostMap.get(vendorId));
                String perAddOnPrice = null;
                if (vendorAddOnVO == null)
                {
                    perAddOnPrice = "0";
                }
                else
                {
                    perAddOnPrice = vendorAddOnVO.getCost();
                }
                if (perAddOnPrice == null)
                    perAddOnPrice = "0";

                Integer addOnQty = addOnVO.getOrderQuantity();
                if (addOnQty == null)
                    addOnQty = 0;

                BigDecimal addOnPrice = new BigDecimal(perAddOnPrice);
                BigDecimal addOnQuanity = new BigDecimal(addOnQty);

                BigDecimal cost = new BigDecimal(0);
                cost = addOnPrice.multiply(addOnQuanity);
                totalAddOnCost = totalAddOnCost.add(cost);

            }
        }

        return totalAddOnCost;
    }

    /**
     * calls the CommonUtils.sendJMSMessage for sending JMS message
     *
     * @param List
     *            List of objects to for whom messages need to be sent
     * @return HashMap collection of errors occured if any
     */

    public void sendMyBuys(Connection connection, String productId) throws Exception
    {

        CommonUtils.sendJMSMessage(connection, productId, productId, JMSPipeline.MY_BUYS);
    }

	/** #16015 - Shipping system once successfully increments vendor assigned order count or pending count, 
	 * inventory should be recalculated for auto shutdown.
	 * @param venusMessageVO
	 * @param connection
	 * @throws Exception
	 */
	private void processAutoshutdownCheck(VenusMessageVO venusMessageVO, Connection connection) throws Exception {
						
		boolean isVenusStatusPending = false;
		if(venusMessageVO.getVenusStatus().equals(VenusStatus.PENDING)){
			isVenusStatusPending = true;
		}
		VendorProductAvailabilityUtility.checkVendorProductAvailability(venusMessageVO.getProductId(), 
				venusMessageVO.getVendorId(), isVenusStatusPending, venusMessageVO.getShipDate(), connection);
	}
	
	/**
	 * Service response tracking
	 * 
	 * @param requestId
	 * @param startTime
	 * @param jccasStatsConn
	 * @throws Exception
	 */
	private void logServiceResponseTracking(String requestId, long startTime, Connection connection) {
		try {
			long responseTime = System.currentTimeMillis() - startTime;
			LOGGER.info("responseTime: " + responseTime);
			ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
			srtVO.setServiceName("ScanData Service");
			srtVO.setServiceMethod("RateRequest");
			srtVO.setTransactionId(requestId);
			srtVO.setResponseTime(responseTime);
			srtVO.setCreatedOn(new Date());
			ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
			srtUtil.insert(connection, srtVO);
		} catch (Exception e) {
			LOGGER.error("Exception occured while persisting ScanData response times.");
		}
		
	}


}
