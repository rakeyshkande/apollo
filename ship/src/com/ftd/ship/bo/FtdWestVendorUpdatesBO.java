package com.ftd.ship.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.csvreader.CsvReader;
import com.enterprisedt.net.ftp.FTPFile;
import com.ftd.ship.bo.communications.ftdwest.FtdWestDataException;
import com.ftd.ship.common.FTPUtil;
import com.ftd.ship.common.SftpUtil;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.FTDWVendorUpdateVO;
import com.ftd.ftdutilities.CalculateTaxUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

/**
 * @author vsana
 * Class : FtdWestVendorUpdatesBO
 * This business object includes all the logic to:
 * 		1. Download the file(s) from FTP server (FTDWest system drops the file in FTP server) - Staged temporarily on the backend server where the daily process kicks-off.
 * 		2. Process the file(s) and save the contents in Apollo database.
 * 		3. Archive the file(s) in an Apollo backend server.
 * 		4. Removes the file(s) from FTP server.
 * 		5. Removes the file(s) staged on the backend server.
 *
 */
public class FtdWestVendorUpdatesBO 
{
	
	private static Logger logger = new Logger(FtdWestVendorUpdatesBO.class.getName());

	private static final String SHIP_GLOBAL_CONFIG = "SHIP_CONFIG";
	private static final String SHIP_SECURE_CONFIG = "ship";
	private static final String ACCOUNTING_SECURE_CONFIG = "accounting_reporting";
	
	private static final String FTDW_FTP_SERVER = "FTDW_FTP_SERVER";
	private static final String FTDW_FTP_DIR = "FTDW_FTP_DIR";
	private static final String FTDW_FTP_USERNAME = "FTDW_FTP_USERNAME";
	private final static String FTDW_FTP_PRIVATE_KEY_PATH = "FTDW_FTP_PRIVATE_KEY_PATH";
	private final static String FTDW_FTP_PRIVATE_KEY_PASSPHRASE = "FTDW_FTP_PRIVATE_KEY_PASSPHRASE";
	private static final String FTDW_VENDOR_UPDATES_FILE_NAME_PATTERN = "FTDW_VENDOR_UPDATES_FILE_NAME_PATTERN";
	private static final String FTDW_VENDOR_UPDATES_LOCAL_DIR = "FTDW_VENDOR_UPDATES_LOCAL_DIR";
	
	private static final String FTDW_INTERNAL_ARCHIVE_SERVER =  "FTDW_INTERNAL_ARCHIVE_SERVER";
	private static final String FTDW_INTERNAL_ARCHIVE_DIR =  "FTDW_INTERNAL_ARCHIVE_DIR";
	private final static String PTS_INTERNAL_FTP_USER = "ptsInternalFtpUsername";
	private final static String PTS_INTERNAL_FTP_PWD = "ptsInternalFtpPassword";
	
	protected ShipDAO shipDAO;
	
	public void setShipDAO(ShipDAO shipDAO) {
        this.shipDAO = shipDAO;
    }

    public ShipDAO getShipDAO() {
        return shipDAO;
    }
    
    /**
     * Method 		: getWestVendorUpdates()
     * Description 	: This is the entry method to the business object. It gets invoked from onMessage() method of FtdWestVendorUpdatesMDB.
     * 				  It invokes all the other methods in the BO which downloads, process, archives and deletes the file(s) dropped by FTDWest in FTP server. 	 
     */
	public void getWestVendorUpdates(Connection connection) throws Exception
	{
		logger.debug("Entered FtdWestVendorUpdatesBO...");
		
		ConfigurationUtil cacheUtil = ConfigurationUtil.getInstance();
		List<String> lstRemoteFileNames = null;
		List<String> lstLocalFilesAbsPaths = new ArrayList<String>();
		List<File> lstFTDWLocalFiles = new ArrayList<File>();
		SftpUtil sftpUtil = null;
		String errMsg = null;
		Map mFiles = null;
		boolean isFileArchiveSuccess = true;
						
		try
		{
			logger.debug("Downloading FTDWest file(s)...");
			
			sftpUtil = new SftpUtil();
			
			// Invoke the method which connects to SFTP server, downloads the file onto the same backend server where this process is running.
			mFiles = downloadFTDWFiles(cacheUtil,sftpUtil);
			
			logger.debug("Completed downloading FTDWest file(s)...");
			
			// From the returned hashmap, get the lists of names(s) of remote file(s) and absolute path(s) of file(s) staged on the server.
			lstRemoteFileNames = (List)mFiles.get("remoteFileNames");
			lstLocalFilesAbsPaths = (List)mFiles.get("localFilesAbsPaths");
			
			/*
			 * If the list of remote file names is empty, the process has not found any files the FTP server - directory which match the expected pattern.
			 * So send a PAGE alert.
			 */
			if(lstRemoteFileNames != null && lstRemoteFileNames.size() == 0)
			{
				errMsg = "No FTDWest vendor updates file(s) with expected format found in FTP server.";
				sendSystemMessage(errMsg);
				throw new Exception(errMsg);
			}
			
			// Logging the no. of files downloaded from FTP server and their names.
			logger.debug("No. of files downloaded:"+lstRemoteFileNames.size());
			logger.debug("File names are:");
			for(String s:lstRemoteFileNames)
				logger.debug(s+"\n");
			
			/*
			 *  The file(s) downloaded from the FTP server are expected to include date stamp in them. So, if multiple day's files are sent, they must sorted by date
			 *  and then processed in the same order i.e., oldest file ---> latest file. 
			 *  That way the records in Apollo database will maintain the integrity of the data. 
			 */
			
			Collections.sort(lstLocalFilesAbsPaths);
			logger.debug("Names of staged files are sorted...");
			
			// By now, all the files are downloaded and staged on the backend server. Start processing each file in the order of date (if multiple files are present).
			for(String sLocalFile: lstLocalFilesAbsPaths)
			{
				// Invoke the method which processes the file and saves the file content in database.
				logger.debug("Processing file:"+sLocalFile);
				processFTDWFiles(sLocalFile, connection);
				
				// This list of File(s) is required for archiving the files.
				lstFTDWLocalFiles.add(new File(sLocalFile));
			}
			
			/*
			 * After processing the file(s), the same must be archived onto an Apollo server (which is configured as a Global Parameter).
			 * If archiving the files fails, a NOPAGE alert will be triggered.
			 */
			try
			{
				logger.debug("Archiving the files...");
				archiveFTDWFiles(lstFTDWLocalFiles, cacheUtil, sftpUtil);
				logger.debug("Files are archived...");
			}
			catch(Exception e)
			{
				isFileArchiveSuccess = false;
				logger.error(e);
			}
			
			/*
			 * Irrespective of whether the files are archived or not, (the file(s) is/are still available in the staged directory) the file(s) need to be deleted from FTP server.
			 * In case of any issue while deleting the files from FTP server, a NOPAGE alert will be triggered.
			 */
			try
			{
				logger.debug("Deleting remote files...");
				deleteRemoteFiles(lstRemoteFileNames, cacheUtil, sftpUtil);
				logger.debug("Files are deleted from FTP server...");
			}
			catch(Exception e)
			{
				logger.error(e);
			}
			
			/*
			 * Once the files are archived successfully, the same need to be removed from the staged directory on local server.
			 * The staged file(s) should be removed only if archiving is successful.
			 */
			if(isFileArchiveSuccess)
			{
				try
				{
					logger.debug("Deleting staged files...");
					deleteStagedFiles(lstLocalFilesAbsPaths);
					logger.debug("Staged files are deleted...");
				}
				catch(Exception e)
				{
					logger.error(e);
				}
			}
			else
				logger.debug("Skipping staged files delete, as archiving the files failed...");
		}
		catch(Exception e)
		{
			logger.error(e);
			throw e;
		}
	}
	// End of method getWestVendorUpdates().
	
	
	/**
     * Method 		: downloadFTDWFiles()
     * Description 	: This method gets all the required global and secure config parameters, passes them to sftpUtil.downloadFilestoLocalviaSSH () method
     * 				  which has the logic to connect to SFTP server and download the files into a stage directory in local server. 	 
     */
	public Map downloadFTDWFiles(ConfigurationUtil cacheUtil, SftpUtil sftpUtil) throws Exception
	{
		logger.debug("In downloadFTDWFiles() method.");
		String fileNamePattern = null;
		String ftpServer = null;
		String remoteFilePath = null;
		String ftpUsername = null;
		String ftpPrivatekeyPath = null;
		String ftpPrivateKeyPassphrase = null;
		String localFilePath = null;
		Map mFiles = null;
		
		try
		{
			// Global parameters.
			fileNamePattern = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_VENDOR_UPDATES_FILE_NAME_PATTERN);
			ftpServer = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_SERVER);
			remoteFilePath = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_DIR); 
			ftpPrivatekeyPath = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_PRIVATE_KEY_PATH);
			localFilePath =  cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_VENDOR_UPDATES_LOCAL_DIR);
			
			logger.debug("Global params values:");
			logger.debug("ftpServer:"+ftpServer);
			logger.debug("remoteFilePath:"+remoteFilePath);
			logger.debug("fileNamePattern:"+fileNamePattern);
			logger.debug("localFilePath:"+localFilePath);

			// Secure parameters.
			ftpUsername = cacheUtil.getSecureProperty(SHIP_SECURE_CONFIG, FTDW_FTP_USERNAME);
			ftpPrivateKeyPassphrase = cacheUtil.getSecureProperty(SHIP_SECURE_CONFIG, FTDW_FTP_PRIVATE_KEY_PASSPHRASE);
			
			// Pass all the above params and invoke sftpUtil.downloadFilestoLocalviaSSH(). 
			mFiles = sftpUtil.downloadFilestoLocalviaSSH(fileNamePattern, remoteFilePath, localFilePath, ftpServer, ftpUsername, ftpPrivatekeyPath, ftpPrivateKeyPassphrase);
		}
		catch(Exception e)
		{
			// If an exception occurs while downloading the files, send a PAGE alert.
			String errMsg = "Exception occured while downloading FTDWest vendor updates file(s) to local server. "+e.getMessage();
			sendSystemMessage(errMsg);
			throw e;
		}
		
		return mFiles;
	}
	// End of method downloadFTDWFiles().
	
	
	/**
     * Method 		: processFTDWFiles()
     * Description 	: This method accepts the absolute path to a staged file, parses the contents of the file and invokes shipDAO.updateFTDWVendorOrder() method to save
     * 				  the data in database. 	 
     */	
	public void processFTDWFiles(String localFileAbsPath, Connection connection) throws Exception
	{
		CsvReader reader = null;
		FTDWVendorUpdateVO ftdwVendorUpdateVO = null;
		StringBuffer errMsg = new StringBuffer();
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(connection);
		String fillingVendor = null;
		String venusOrderNumber = null;
	    String pid = null;
	    String unitcost = null;
	    String indicator = null;
		
		try
		{
			// Provide the absolute path of the file as input to CsvReader. CsvReader loops through the records and extracts the info. 
			reader = new CsvReader(localFileAbsPath);
			reader.setSkipEmptyRecords(true);	
			reader.getValues();
			logger.debug("Started reading the file...");
			
			// Read out the header record and move on to actual data records.
			reader.readRecord();
			
			// Loop through the data records until end of the file.
			while (reader.readRecord()) 
			{
				try
				{
					/*
					 * Prepare a new FTDWVendorUpdateVO instance out of the fields from a record and invoke shipDAO.updateFTDWVendorOrder() passing the VO instance.
					 * If any data issue occurs while saving the data, FtdWestDataException will be thrown. Same will be caught and the error message will be added to the list.
					 */
					
					fillingVendor = reader.get(0);
					venusOrderNumber = reader.get(1);
					pid = reader.get(2);
				    unitcost = reader.get(4);
				    indicator = reader.get(5);
				    
				    //logger.debug(fillingVendor+"-"+venusOrderNumber+"-"+pid+"-"+unitcost+"-"+indicator);
				    
				    if(StringUtils.isEmpty(fillingVendor))
				    {	
				    	errMsg.append("Record found with empty fillingVendor.\n");
				    	continue;
				    }	
				    else if(StringUtils.isEmpty(venusOrderNumber))
				    {
				    	errMsg.append("Record found with empty venusOrderNumber. FillingVendor:"+fillingVendor+".\n");
				    	continue;
				    }
				    else if(StringUtils.isEmpty(pid))
				    {
				    	errMsg.append("Record found with empty PID. Venus Order Number:"+venusOrderNumber+".\n");
				    	continue;
				    }
				    else if(StringUtils.isEmpty(unitcost))
				    {
				    	errMsg.append("Record found with empty unitCost. Venus Order Number:"+venusOrderNumber+".\n");
				    	continue;
				    }
				    else if(StringUtils.isEmpty(indicator) || !("P".equalsIgnoreCase(indicator) || "A".equalsIgnoreCase(indicator)))
				    {
				    	errMsg.append("Record found with empty/unknown indicator:"+indicator+". Venus Order Number:"+venusOrderNumber+".\n");
				    	continue;
				    }
				    
					ftdwVendorUpdateVO = new FTDWVendorUpdateVO(fillingVendor, venusOrderNumber, pid, unitcost, indicator);
				
					shipDAO.updateFTDWVendorOrder(ftdwVendorUpdateVO, dataRequest);
				}
				catch(FtdWestDataException ftdwexception)
				{
					errMsg.append(ftdwexception.getMessage()+".\n");
				}
				catch(Exception e)
				{
					errMsg.append("Exception occurred while processing a record with params: fillingVendor-> "+fillingVendor+
																							", venusOrderNumber-> "+venusOrderNumber+
																							", pid-> "+pid+
																							", unitcost-> "+unitcost+
																							", indicator-> "+indicator+". Exception message: "+e.getMessage()+".\n");
				}
			}
			
			/*
			 * If any errors occured during processing a record in the file, such record will not be updated in DB, add it to the list errRecords 
			 * and proceed with the next record. Once processing of a file is completed, check the size of the list errRecords and if it's not empty,
			 * send a PAGE alert including all the contents of the list. It includes appropriate error messages of corresponding records in the file. 
			 */
			logger.debug("File processing completed...");
			
			if(errMsg.length() > 0)
			{
				logger.debug("Erroneous records found in FTDWest vendor updates file: "+localFileAbsPath+"\n"+errMsg.toString());
				
				errMsg.insert(0, "Erroneous records found in FTDWest vendor updates file: "+localFileAbsPath+"\n");
			}
		}
		catch(Exception e)
		{
			// If an exception occurs while processing the file, send a PAGE alert.
			errMsg.append("Exception occured while processing FTDWest vendor updates file . "+e.getMessage());
			throw e;
		}
		finally
		{
			// Close the CsvReader instance once the file processing is complete.
			if(reader != null)
				reader.close();
			
			// If there're error messages, send a PAGE alert.
			if(errMsg.length() > 0)
				sendSystemMessage(errMsg.toString());
		}
	}
	// End of method processFTDWFiles().
	
	
	/**
     * Method 		: archiveFTDWFiles()
     * Description 	: This method accepts the list of Files and archives them onto an Apollo server. Which server the files must be archived onto is configured as a
     * 				  global parameter. So irrespective of on which backend server the process kicks-off, all the file(s) will be archived onto a single server.
     */	
	public void archiveFTDWFiles(List<File> lstFTDWLocalFiles, ConfigurationUtil cacheUtil, SftpUtil sftpUtil) throws Exception
	{
		String sIntArchiveServer = null;
		String sIntArchivePath = null;
		String sIntArchiveUser = null;
		String sIntArchivePwd = null;
		
		try
		{
			// Global parameters.
			sIntArchiveServer = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_INTERNAL_ARCHIVE_SERVER);
			sIntArchivePath = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_INTERNAL_ARCHIVE_DIR);
			
			// Secure config parameters.
			sIntArchiveUser = cacheUtil.getSecureProperty(ACCOUNTING_SECURE_CONFIG, PTS_INTERNAL_FTP_USER);
			sIntArchivePwd = cacheUtil.getSecureProperty(ACCOUNTING_SECURE_CONFIG, PTS_INTERNAL_FTP_PWD);
			
			// Invoke sftpUtil.archiveFiles() method which SFTPs all the files to archive server.
			sftpUtil.archiveFiles(lstFTDWLocalFiles, sIntArchivePath, sIntArchiveServer, sIntArchiveUser, sIntArchivePwd);
		}
		catch(Exception e)
		{
			// If an exception occurs while archiving the file(s), send a NOPAGE alert.
			logger.error("Exception occured while archiving FTDWest vendor updates files on internal server: "+e.getMessage());
			String errMsg = "Exception occured while archiving FTDWest vendor updates files on internal server. "+e.getMessage();
			sendSystemMessage(errMsg);
						
			throw e;
		}
	}
	// End of method archiveFTDWFiles().
	
	
	/**
     * Method 		: deleteRemoteFiles()
     * Description 	: This method accepts the list of file names downloaded from SFTP server and deletes the same from SFTP server.
     */	
	public void deleteRemoteFiles(List<String> lstRemoteFileNames, ConfigurationUtil cacheUtil, SftpUtil sftpUtil) throws Exception
	{
		String ftpServer = null;
		String remoteFilePath = null;
		String ftpUsername = null;
		String ftpPrivatekeyPath = null;
		String ftpPrivateKeyPassphrase = null;
		
		try
		{
			// Global parametes.
			ftpServer = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_SERVER);
			remoteFilePath = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_DIR);
			ftpPrivatekeyPath = cacheUtil.getFrpGlobalParm(SHIP_GLOBAL_CONFIG, FTDW_FTP_PRIVATE_KEY_PATH);
			
			// Secure config parameters.
			ftpUsername = cacheUtil.getSecureProperty(SHIP_SECURE_CONFIG, FTDW_FTP_USERNAME);
			ftpPrivateKeyPassphrase = cacheUtil.getSecureProperty(SHIP_SECURE_CONFIG, FTDW_FTP_PRIVATE_KEY_PASSPHRASE);
						
			// Invoke sftpUtil.deleteRemoteFiles() which connects to SFTP server, navigates to remote directory and removes all the files provided in the list.
			sftpUtil.deleteRemoteFiles(lstRemoteFileNames, ftpServer, remoteFilePath, ftpUsername, ftpPrivatekeyPath, ftpPrivateKeyPassphrase);
		}
		catch(Exception e)
		{
			// If an exception occurs while archiving the file(s), send a NOPAGE alert.
			logger.error("Exception occured while deleting FTDWest vendor updates files on FTP server. "+e.getMessage());
			String errMsg = "Exception occured while deleting FTDWest vendor updates files on FTP server. "+e.getMessage();
			sendSystemMessage(errMsg);
						
			throw e;
		}
	}
	// End of method archiveFTDWFiles().
	
	
	/**
     * Method 		: deleteStagedFiles()
     * Description 	: This method accepts the list of absolute paths to the staged files and deletes the same from local server.
     */	
	public void deleteStagedFiles(List<String> lstLocalFilesAbsPaths) throws Exception
	{
		File stagedFile = null;
		try
		{
			// Loop through the list, check if the file exists and delete if it does.
			for(String s: lstLocalFilesAbsPaths)
			{
				stagedFile = new File(s);
				if(stagedFile.exists())
					stagedFile.delete();
			}
		}
		catch(Exception e)
		{
			// If an exception occurs while archiving the file(s), send a NOPAGE alert.
			logger.error("Exception occured while deleting the FTDWest vendor updates files staged on local server. "+e.getMessage());
			String errMsg = "Exception occured while deleting the FTDWest vendor updates files staged on local server. "+e.getMessage();
			sendSystemMessage(errMsg);
			throw e;
		}
	}
	// End of method deleteStagedFiles().
	
	
	/**
     * Method 		: sendSystemMessage()
     * Description 	: This method accepts the message that needs to be included in the PAGE / NOPAGE alert email's body and invokes the utility method to send a 
     * 				  system message.
     */	
	public void sendSystemMessage(String errMsg)
	{
		try 
		{
            CommonUtils.getInstance().sendSystemMessage(errMsg);
        } 
		catch (Exception exception) 
		{
            errMsg = "Unable to send message to support pager.";
            logger.fatal(errMsg,exception);
        }
	}
	// End of method sendSystemMessage().
	
}
