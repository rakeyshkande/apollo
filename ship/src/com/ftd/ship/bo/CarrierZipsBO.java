package com.ftd.ship.bo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.JMSPipeline;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.ShipDAO;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;

public class CarrierZipsBO extends SDSProcessingBO {

	private static final Logger logger = new Logger("com.ftd.ship.bo.CarrierZipsBO");
    public static final String FEDEX_CARRIER = "FEDEX";
    public static final String UPS_CARRIER = "UPS";
    public static final String APPLICATION_CONTEXT = "ship";

    private static final String PARAM_LOGIN = "CARRIER_ZIP_FTP_LOGIN_";
    private static final String PARAM_PASSWORD = "CARRIER_ZIP_FTP_PASSWORD_";

    private static final String PARAM_SERVER = "CARRIER_ZIP_FTP_SERVER_";
    private static final String PARAM_REMOTE_DIRECTORY = "CARRIER_ZIP_FTP_REMOTE_DIRECTORY_";
    private static final String PARAM_DELETE_REMOTE_FILE = "CARRIER_ZIP_FTP_DELETE_REMOTE_FILE_";

    private static final String PARAM_LOCAL_DIRECTORY = "CARRIER_ZIP_FTP_LOCAL_DIRECTORY_";
    protected static final String PARAM_ARCHIVE_DIRECTORY = "CARRIER_ZIP_ARCHIVE_ROOT_DIRECTORY";

    protected static final String DEFAULT_ARCHIVE_ROOT = "/u02/apollo/carrier_zips/processed";
    protected static final String ARCHIVE_FORMAT = "yyyyMM";

    protected static final String DAYS_SINCE_LAST = "DAYS_SINCE_LAST_";
    protected static final String FILE_RECEIVED = "_FILE_RECEIVED";
    public boolean checkMissingCarrierFiles = false;
    public static int numOfFedexRecordsProcessed = 0;
    public static int numOfUpsRecordsProcessed = 0;
    public static Connection conn;

    public CarrierZipsBO() {
    }

    public void getCarrierZips(Connection connection, String carrierId) throws SDSApplicationException {
        //Download file for FedEx using FTP client
    	//if carrierId is FEDEX, call processFedExZipCodesFile
    	//if carrierId is UPS, call processUPSZipCodesFile

    	boolean hasErrors = false;
        try {
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            String host = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_SERVER+carrierId,"localhost");
            String userId = configUtil.getSecureProperty(APPLICATION_CONTEXT,PARAM_LOGIN+carrierId);
            String credentials = configUtil.getSecureProperty(APPLICATION_CONTEXT,PARAM_PASSWORD+carrierId);
            String localDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_LOCAL_DIRECTORY+carrierId,"./");
            String remoteDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_REMOTE_DIRECTORY+carrierId,"./");
            boolean deleteRemoteFile = BooleanUtils.toBoolean(getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_DELETE_REMOTE_FILE+carrierId,"true"));

            logger.debug("host: "+host);
            logger.debug("userId: "+userId);
            logger.debug("credentials: ##########");
            logger.debug("localDirectory: "+localDirectory);
            logger.debug("remoteDirectory: "+remoteDirectory);
            logger.debug("delete remote file: "+deleteRemoteFile);

            FTPClient ftpClient = new FTPClient(host);
            ftpClient.login(userId, credentials);
            ftpClient.setType(FTPTransferType.BINARY);
            ftpClient.setConnectMode(FTPConnectMode.PASV);
            if( remoteDirectory!=null && remoteDirectory.length()>0) {
                ftpClient.chdir(remoteDirectory);
            }

            String[] fileNames = null;
            try {
            	//logger.debug();
                logger.debug("Getting the list of files");
                fileNames = ftpClient.dir("*");
                if( fileNames.length==0 ) {
                	//Put a condition here to check last Friday of the month - and send email to WebOps
                	if(checkMissingCarrierFiles){
                		//send email to webops
                		logger.debug("sending email to webops ...");
                		sendEmailToWebOps(connection, carrierId);
                	}
                    logger.debug("Nothing to download");
                    return;
                }
            } catch (Exception e) {
            	if(checkMissingCarrierFiles){
            		//send email to webops
            		logger.debug("sending email to webops ...");
            		sendEmailToWebOps(connection, carrierId);
            	}
                logger.debug("Nothing to download");
                return;
            }

            //Check to see if the download directory exists
            File fLocal = new File(localDirectory);
            if( !fLocal.exists() ) {
                FileUtils.forceMkdir(fLocal);
            }

            for (int i = 0; i < fileNames.length; i++) {
                String fileName = fileNames[i];

                try {

                    String remoteFileName;
                    if( remoteDirectory!=null && remoteDirectory.length()>0) {
                        remoteFileName = remoteDirectory + "/" + fileName;
                    } else {
                        remoteFileName = fileName;
                    }

                    //Download the file from the server
                    logger.debug("Downloading file: " + fileName);
                    ftpClient.get(localDirectory + "/" + fileName,remoteFileName);
                    logger.debug("File downloaded: " + fileName);


                    //Unzip file
                    logger.debug("Unzipping File: "+fileName);
                    String unzippedFileName = unzipLocalFile(localDirectory,fileName);
                    //logger.debug("Unzipped entry: "+unzippedFileName);
                    //end unzip file

                    //Save to the archive folder
                    File archiveFile = new File(localDirectory,fileName);

                    //Delete the file off of the remote file system
                    if( deleteRemoteFile ) {
                    	logger.debug("Removing file "+remoteFileName+" from FTP server.");
                        ftpClient.delete(remoteFileName);
                    }

                    //Move to archive directory
                    String archiveDirectory = getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,PARAM_ARCHIVE_DIRECTORY,"./") + "/" + carrierId.toLowerCase();

                    //Check to see if the archive directory exists
                    File archiveDir = new File(archiveDirectory);
                    if( !archiveDir.exists() ) {
                        logger.debug("Creating archive directory "+archiveDir.getCanonicalPath());
                        FileUtils.forceMkdir(archiveDir);
                    }

                    logger.debug("Moving file "+fileName+" to archive "+archiveDirectory);
                    FileUtils.copyFileToDirectory(archiveFile,archiveDir);

                    logger.debug("Removing "+fileName+" from local file system.");
                    FileUtils.forceDelete(archiveFile);
                    //End saving to the archive folder


                    //Start Processing and delete local file after processing
                    logger.debug("Start Processing .. ");

                    if(carrierId.equalsIgnoreCase(FEDEX_CARRIER)){
                    	numOfFedexRecordsProcessed = 0;
                    	logger.debug("Proessing "+carrierId+" File"+".. ");
                    	processFedExZipCodesFile(connection, carrierId, localDirectory + "/" + unzippedFileName);

                    	logger.debug("numOfFedexRecordsProcessed: "+numOfFedexRecordsProcessed);
                    	if(numOfFedexRecordsProcessed>0){
                    	logger.debug("Proessing "+carrierId+" File"+"Complete. Sending Process complete JMS message.. ");
                    	CommonUtils.sendJMSMessage(connection,"COMPLETE-"+carrierId,carrierId,JMSPipeline.PROCESSCARRIERZIPS);
                    	}
                    	else{
                    		CommonUtils.getInstance().sendSystemMessage("CarrierZipsBO", "Could not process zip file for carrier "+carrierId+". Invalid File.");
                    	}
                    }
                    if(carrierId.equalsIgnoreCase(UPS_CARRIER)){
                    	numOfUpsRecordsProcessed = 0;
                    	logger.debug("Proessing "+carrierId+" File"+".. ");
                    	processUPSZipCodesFile(connection, carrierId, localDirectory + "/" + unzippedFileName, unzippedFileName);

                    	logger.debug("numOfUpsRecordsProcessed: "+numOfUpsRecordsProcessed);
                    	if(numOfUpsRecordsProcessed>0){
                    	logger.debug("Proessing "+carrierId+" File"+"Complete. Sending Process complete JMS message.. ");
                    	CommonUtils.sendJMSMessage(connection,"COMPLETE-"+carrierId,carrierId,JMSPipeline.PROCESSCARRIERZIPS);
                    	}
                    	else{
                    		CommonUtils.getInstance().sendSystemMessage("CarrierZipsBO", "Could not process zip file for carrier "+carrierId+". Invalid File.");
                    	}
                    }

                    File localFile = new File(localDirectory,unzippedFileName);
                    logger.debug("Removing "+unzippedFileName+" from file system.");
                    FileUtils.forceDelete(localFile);
                    //end Processing

                } catch (IOException e) {
                    logger.debug("Error while archiving carrier zip file "+fileName+".  Will continue processing..."+e);
                    hasErrors = true;
                }
            }

            //logger.info("Zip code ingestion process completed.");

        } catch (Exception e) {
            throw new SDSApplicationException("Unable to ftp zip files for carrier "+carrierId,e);
        } finally {
            if ( hasErrors ) {
                throw new SDSApplicationException("Errors were found while FTPing zip files for carrier "+carrierId+".  See ship processing log for details.");
            }
        }

    }
	/**
	 * To check if the carrier zip files hasn't been received in a month or over
	 * @param connection
	 * @param carrierId
	 */
    public void checkCarrierZipFiles(Connection connection){
    	ShipDAO shipDAO = new ShipDAO();

    	ArrayList<String> carrierIdList = new ArrayList<String>();
    	String checkCarrierId = "";
    	String last_date = "";

    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date last_recvd_date;
        Date todays_date = new Date();

        long diff = 0;
        int days_passed = 0;
        int days_since_last_carrier_file_recvd = 0;

    	try {
    		//adding carried ids to list
    		carrierIdList.add(FEDEX_CARRIER);
    		carrierIdList.add(UPS_CARRIER);

    		for(String carrierId : carrierIdList){
	    		checkCarrierId = carrierId;
	    		last_date = shipDAO.doGetLastZipFileReceivedDate(connection, checkCarrierId);

	    		if(last_date != null && StringUtils.isNotEmpty(last_date) && StringUtils.isNotBlank(last_date)){
			         last_recvd_date = (Date)formatter.parse(last_date);
			         diff = todays_date.getTime() - last_recvd_date.getTime();
			         days_passed = (int) (diff / (1000 * 60 * 60 * 24));
			         days_since_last_carrier_file_recvd = Integer.parseInt(getGlobalParameter(connection,ShipConstants.GLOBAL_PARMS_CONTEXT,DAYS_SINCE_LAST+checkCarrierId+FILE_RECEIVED,"35"));

	    		}
	    		if(days_passed != 0 && days_passed>=days_since_last_carrier_file_recvd){
	    			checkMissingCarrierFiles = true;
	    			getCarrierZips(connection, checkCarrierId);
	    		}
    		}

		} catch (SDSApplicationException e) {
			logger.debug(e);
		} catch (Exception e1) {
			logger.debug(e1);
		}
    }
    public void sendEmailToWebOps(Connection connection, String carrierId){
    	ShipDAO shipDAO = new ShipDAO();
    	String toAddress= "";
		try {
			toAddress = shipDAO.doGetZipCodeReportEmailList(connection);
		} catch (Exception e1) {
			logger.debug(e1);
		}
    	String fromAddress ="no-reply@ftdi.com";
    	String subject ="";
    	String content ="";

    	String month = "";
    	Date date = new Date();
    	month = new SimpleDateFormat("MMM").format(date);

    	try {
    		ConfigurationUtil cu = ConfigurationUtil.getInstance();
			subject = cu.getContentWithFilter(connection, "BLOOMS_BY_NOON", "EMAIL_SUBJECT", null, null);
			content = cu.getContentWithFilter(connection, "BLOOMS_BY_NOON", "EMAIL_BODY", null, null);

			subject = subject.replaceAll("~carrier~", carrierId);
			subject = subject.replaceAll("~month~", month);
			content = content.replaceAll("~carrier~", carrierId);
		} catch (Exception e) {
			logger.debug(e);
		}

    	try {
            NotificationVO notifVO = new NotificationVO();

            notifVO.setMessageTOAddress(toAddress);
            notifVO.setMessageFromAddress(fromAddress);
            notifVO.setSMTPHost(ConfigurationUtil.getInstance().getFrpGlobalParm("MESSAGE_GENERATOR_CONFIG", "SMTP_HOST_NAME"));
            notifVO.setMessageSubject(subject);
            notifVO.setMessageContent(content);

            //send e-mail
            NotificationUtil mail = NotificationUtil.getInstance();
            mail.notify(notifVO);
        } catch (Exception e) {
            //throw new EmailDeliveryException(e.getMessage());
        	logger.debug(e);
        }
    }

    public String unzipLocalFile(String fileLocation, String fileName) {

		String fName = fileLocation + "/" + fileName;
	    byte[] buf = new byte[1024];
	    String zipEntryName = "";
	    try{
	    ZipInputStream zinstream = new ZipInputStream(
	        new FileInputStream(fName));
	    ZipEntry zentry = zinstream.getNextEntry();
	    //logger.debug("Name of current Zip Entry : " + zentry + "\n");
	    while (zentry != null) {
	      String entryName = zentry.getName();
	      logger.debug("Name of  Zip Entry : " + entryName);

	      zipEntryName = entryName;

	      FileOutputStream outstream = new FileOutputStream(fileLocation + "/" + entryName);
	      int n;

	      while ((n = zinstream.read(buf, 0, 1024)) > -1) {
	        outstream.write(buf, 0, n);

	      }
	      logger.debug("Successfully Extracted File Name : "
	          + entryName);
	      outstream.close();

	      zinstream.closeEntry();
	      zentry = zinstream.getNextEntry();
	    }
	    zinstream.close();
    }catch(Exception e){
    	e.printStackTrace();
    }
	return zipEntryName;
    }

	public void processUPSZipCodesFile(Connection connection, String carrierId, String fileLocation, String fileName) throws SDSApplicationException {
		// parse file using Jackcess library
		// Read each row and send JMS message to process it

		String tableName = fileName;

		//get table name from file name
		StringTokenizer st = null;
    	int tokenNo = 0;
    	String tokenData = "";

                st = new StringTokenizer(tableName, ".");

                while(st.hasMoreTokens())
                {
                	tokenNo++;
                    tokenData = st.nextToken();

                    if(tokenNo == 1){
                    	tableName = tokenData;
                    }
                }
                logger.debug("File Location: "+fileLocation);
                logger.debug("Table name: "+tableName);
		//!get table name

		int cnt = 0;
		Table table;

		String zipCode="";
		String city="";
		String weekdayDeliveryData="";
		String saturdayDeliveryData="";
		int weekdayDelivery=0;
		int saturdayDelivery=0;

		String jmsMessage=""; //contains zip, carrier id, weekdayDelivery Flag, saturdayDelivery Flag, and saturday availability
		try {

			table = Database.open(new File(fileLocation)).getTable(tableName);

			for(Map<String, Object> row : table) {

				  zipCode = (String) row.get("Postal");
				  zipCode = zipCode.trim();
				  city = (String) row.get("City");
				  city = city.trim();
				  weekdayDeliveryData = (String) row.get("1DA");
				  weekdayDeliveryData = weekdayDeliveryData.trim();
				  saturdayDeliveryData = (String) row.get("Sat 1DA");
				  saturdayDeliveryData = saturdayDeliveryData.trim();


				  if(weekdayDeliveryData != null && StringUtils.isNotEmpty(weekdayDeliveryData) && StringUtils.isNotBlank(weekdayDeliveryData)){
					  weekdayDelivery = Integer.parseInt(weekdayDeliveryData);
				  }

				  if(saturdayDeliveryData != null && StringUtils.isNotEmpty(saturdayDeliveryData) && StringUtils.isNotBlank(saturdayDeliveryData)){
					  saturdayDelivery = Integer.parseInt(saturdayDeliveryData);
				  }

				  if(city.equalsIgnoreCase("ALL POINTS")){ // compile and send JMS message only when City equals ALL POINTS
						  jmsMessage = zipCode+"-"+carrierId;

						  if(weekdayDelivery != 0 && weekdayDelivery <= 1200){
							  jmsMessage = jmsMessage+"-"+"WY";  //W - weekday delivery Y - yes
						  }
						  else{
							  jmsMessage = jmsMessage+"-"+"WN";  //W - weekday delivery N - NO
						  }
						  if(saturdayDelivery != 0 && saturdayDelivery <= 1200){
							  jmsMessage = jmsMessage+"-"+"SY"; //S - saturday delivery Y - yes
						  }
						  else{
							  jmsMessage = jmsMessage+"-"+"SN"; //S - saturday delivery N - NO
						  }
						  //if saturdayDelivery is not null or not eq "" then decide sat availability
						  if(saturdayDelivery != 0){
							  jmsMessage = jmsMessage+"-"+"AY"; //A - saturday availability Y - yes
						  }
						  else{
							  jmsMessage = jmsMessage+"-"+"AN"; //A - saturday availability N - NO
						  }
						  //Send the JMS message to process
			              //logger.debug("Sending JMS message to process record..");
			              CommonUtils.sendJMSMessage(connection,jmsMessage,carrierId,JMSPipeline.PROCESSCARRIERZIPS);
				  }
				  cnt++;
				  weekdayDelivery=0;
				  saturdayDelivery=0;
			}
			numOfUpsRecordsProcessed = cnt;
			logger.debug("Number of records processed: "+numOfUpsRecordsProcessed);
		} catch (IOException e) {
			//e.printStackTrace();
			logger.debug(e);
		} catch (Exception e) {
			//e.printStackTrace();
			logger.debug(e);
		}
	}


	public void processFedExZipCodesFile(Connection connection, String carrierId, String fileLocation) throws SDSApplicationException {
	    // parse file using Apache POI library
		// Read each row and send JMS message to process it
		try {
			this.conn = connection;
			processAllSheets(fileLocation);
		} catch (Exception e) {
			logger.debug(e);
		}
	}

	public void sendFedExMessage(String jmsMessage){
		try {
			CommonUtils.sendJMSMessage(conn,jmsMessage,CarrierZipsBO.FEDEX_CARRIER,JMSPipeline.PROCESSCARRIERZIPS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * START FEDEX PROCESS
	 */
	public boolean isFedexZipCode = false;
	public boolean isFedexWeekdayDelivery = false;
	public boolean isFedexSaturdayDelivery = false;
	//public String fedexCarrierId = "FEDEX";

	public void processAllSheets(String filename) throws Exception {
		int cnt = 0;
		OPCPackage pkg = OPCPackage.open(filename);
		XSSFReader r = new XSSFReader( pkg );
		SharedStringsTable sst = r.getSharedStringsTable();

		XMLReader parser = fetchSheetParser(sst);

		Iterator<InputStream> sheets = r.getSheetsData();
		while(sheets.hasNext()) {
			cnt++;
			InputStream sheet = sheets.next();
			InputSource sheetSource = new InputSource(sheet);
			if(cnt==2){ //for reading 02-Zips sheet only
			parser.parse(sheetSource);
			}
			sheet.close();
		}
	}

	public XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
		XMLReader parser =
			XMLReaderFactory.createXMLReader(
					"org.apache.xerces.parsers.SAXParser"
			);
		ContentHandler handler = new SheetHandler(sst);
		parser.setContentHandler(handler);
		return parser;
	}

	public boolean isRequiredField(String colRowPosition){

		// if the first letter is either A (zip) or K (weekday priority overnight) or S (saturday priority overnight)
		// and if second letter is >= 1 (this is to skip columns AA, AB, AC etc)
		// then it is the required cell

		String firstLetter = "";
		String secondLetter = "";
		boolean isReqField = false;

		firstLetter = colRowPosition.substring(0, 1);
		secondLetter = colRowPosition.substring(1, 2);

		try{
			if( (firstLetter.equalsIgnoreCase("A") ||
					firstLetter.equalsIgnoreCase("K") ||
					firstLetter.equalsIgnoreCase("S") )
					&&
					Integer.parseInt(secondLetter)>=1 ){

				isReqField = true;
			}
			else{
				isReqField = false;
			}

			if(isReqField && firstLetter.equalsIgnoreCase("A")){ // column A is zip code
				this.isFedexZipCode = true;
			}
			if(isReqField && firstLetter.equalsIgnoreCase("K")){ // column K is weekday delivery
				this.isFedexWeekdayDelivery = true;
			}
			if(isReqField && firstLetter.equalsIgnoreCase("S")){ // column S is saturday delivery
				this.isFedexSaturdayDelivery = true;
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		return isReqField;
	}

	public boolean convertTimeForFlags(String time){

		boolean isDeliverable = false;

		try{
				StringTokenizer st = null;
		    	int tokenNo = 0;
		    	String tokenData = "";

		    	String hr_mn = "";
		    	String hour = "";
		    	String min = "";
		    	int hour_min = 0;
		    	String am_pm = "";

		    	if(time.length() <= 8){

			            st = new StringTokenizer(time, " ");

			            while(st.hasMoreTokens())
			            {
			            	tokenNo++;
			                tokenData = st.nextToken();

			                if(tokenNo == 1){
			                	hr_mn = tokenData;
			                }

			                if(tokenNo == 2){
			                	am_pm = tokenData;
			                }
			            }

			            tokenNo = 0; //reset token
			            st = new StringTokenizer(hr_mn, ":");

		                while(st.hasMoreTokens())
		                {
		                	tokenNo++;
		                    tokenData = st.nextToken();

		                    if(tokenNo == 1){
		                    	hour = tokenData;
		                    }
		                    if(tokenNo == 2){
		                    	min = tokenData;
		                    }
		                }

		                hour_min = Integer.parseInt(hour+min);

		                if( hour_min < 1200 && am_pm.equalsIgnoreCase("AM") ){
		                	isDeliverable = true;
		                }
		                else if( hour_min == 1200 && am_pm.equalsIgnoreCase("PM") ){
		                	isDeliverable = true;
		                }
		                else{
		                	isDeliverable = false;
		                }
		    	}
		}catch(Exception e){
			logger.debug(e);
		}

		return isDeliverable;
	}

	public boolean convertTimeForSatAvailability(String time){

		boolean isAvailable = false;

		try{
				StringTokenizer st = null;
		    	int tokenNo = 0;
		    	String tokenData = "";

		    	String hr_mn = "";
		    	String hour = "";
		    	String min = "";
		    	int hour_min = 0;
		    	String am_pm = "";

		    	if(time.length() <= 8){

			            st = new StringTokenizer(time, " ");

			            while(st.hasMoreTokens())
			            {
			            	tokenNo++;
			                tokenData = st.nextToken();

			                if(tokenNo == 1){
			                	hr_mn = tokenData;
			                }

			                if(tokenNo == 2){
			                	am_pm = tokenData;
			                }
			            }

			            tokenNo = 0; //reset token
			            st = new StringTokenizer(hr_mn, ":");

		                while(st.hasMoreTokens())
		                {
		                	tokenNo++;
		                    tokenData = st.nextToken();

		                    if(tokenNo == 1){
		                    	hour = tokenData;
		                    }
		                    if(tokenNo == 2){
		                    	min = tokenData;
		                    }
		                }

		                hour_min = Integer.parseInt(hour+min);

		                if( hour_min >= 0 && (am_pm.equalsIgnoreCase("AM") || am_pm.equalsIgnoreCase("PM"))){
		                	isAvailable = true;
		                }
		                else{
		                	isAvailable = false;
		                }
		    	}
		}catch(Exception e){
			logger.debug(e);
		}

		return isAvailable;
	}
	/**
	 * See org.xml.sax.helpers.DefaultHandler javadocs
	 */
	private static class SheetHandler extends DefaultHandler {
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;

		//add required variables
		private boolean IsZipCode;
		private boolean IsWeekdayDelivery;
		private boolean IsSaturdayDelivery;
		private String carrierId;

		private String colRowLabel;// = attributes.getValue("r");
		private boolean isReqField;// = false;
		CarrierZipsBO carrierZipsBO;// = new ReadExcelPOINew();
		private String jmsMessage;
		//end add required variables

		private SheetHandler(SharedStringsTable sst) {
			this.sst = sst;
		}

		public void startElement(String uri, String localName, String name,
				Attributes attributes) throws SAXException {
			// c => cell
			if(name.equals("c")) {
				// Print the cell reference

				/*start additional code*/
				colRowLabel = attributes.getValue("r");
				isReqField = false;
				carrierZipsBO = new CarrierZipsBO();
				isReqField = carrierZipsBO.isRequiredField(colRowLabel);

				carrierId = carrierZipsBO.FEDEX_CARRIER;
				IsZipCode = carrierZipsBO.isFedexZipCode;
				IsWeekdayDelivery = carrierZipsBO.isFedexWeekdayDelivery;
				IsSaturdayDelivery = carrierZipsBO.isFedexSaturdayDelivery;
				/*end additional code*/

				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if(cellType != null && cellType.equals("s") && isReqField) {
					//System.out.print(attributes.getValue("r") + " - ");
					nextIsString = true;
				} else {
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}

		public void endElement(String uri, String localName, String name)
				throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if(nextIsString) {
				int idx = 0;
				try{
					 idx = Integer.parseInt(lastContents);
					 lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
				}catch(NumberFormatException nfe){

				}catch(IndexOutOfBoundsException ioobe){

				}

			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if(name.equals("v") && nextIsString) {
				//logger.debug(lastContents);
				if(IsZipCode){
					//logger.debug("Zip Code: "+lastContents);
					jmsMessage = lastContents+"-"+carrierId;
				}

				if(IsWeekdayDelivery){

					if(carrierZipsBO.convertTimeForFlags(lastContents)){

						jmsMessage = jmsMessage+"-"+"WY";
					}
					else{

						jmsMessage = jmsMessage+"-"+"WN";
					}
				}

				if(IsSaturdayDelivery){

					if(carrierZipsBO.convertTimeForFlags(lastContents)){

						jmsMessage = jmsMessage+"-"+"SY";
					}
					else{

						jmsMessage = jmsMessage+"-"+"SN";
					}

					if(carrierZipsBO.convertTimeForSatAvailability(lastContents)){

						jmsMessage = jmsMessage+"-"+"AY";
						numOfFedexRecordsProcessed++;
						carrierZipsBO.sendFedExMessage(jmsMessage);
					}
					else{

						jmsMessage = jmsMessage+"-"+"AN";
						numOfFedexRecordsProcessed++;
						carrierZipsBO.sendFedExMessage(jmsMessage);
					}

				}
			}


		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}
	/**
	 * END FEDEX PROCESS
	 */
}
