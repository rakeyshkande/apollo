package com.ftd.ship.bo;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.vo.CarrierScanVO;
import com.ftd.ship.vo.FedexHeaderScanVO;


public class USPSScansBO extends CarrierScansBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.USPSScansBO");
    private static final String USPS_EVENT_FORMAT = "yyyyMMddhhmm";
    
    public USPSScansBO() {
        carrierId = "USPS";
    }
    
    public List<CarrierScanVO> parseScanFile(String fileName, String localDirectory) {
        ArrayList<CarrierScanVO> list = new ArrayList<CarrierScanVO>();
        
        RandomAccessFile raf = null;
        try {
        	String pattern = ConfigurationUtil.getInstance().getFrpGlobalParm(ShipConstants.GLOBAL_PARMS_CONTEXT, ShipConstants.GLOBAL_PARMS_USPS_TRACKING_REGEX);
        	raf = new RandomAccessFile(localDirectory+"/"+fileName, "r");
            String line = "";
            FedexHeaderScanVO headerVO = null;
            String scanType;
            String scanDate;
            String scanTime;
            String trackingNumber;
            while ((line = raf.readLine()) != null) {
            
                if( StringUtils.isBlank(line) ) {
                    continue;
                }
                scanType = "";
			    scanDate = "";
			    scanTime = "";
			    trackingNumber = "";
			    try{
			    	// Regular expression for extracting tracking number is in global parms in case 
			    	// USPS changes format.  Originally it was the last 22 chars of number:
			    	// pattern = "(\\d{22})$";
			    	trackingNumber = line.substring(7, 41);
			    	scanType = line.substring(173, 175);
				    scanDate = line.substring(365, 373);
				    scanTime = line.substring(376, 380);
			    }catch(StringIndexOutOfBoundsException ee){
			    	/*
			    	 * If any event in the scans file is not comply to the USPS layout
			    	 * send a system message along with the scan event record and file namme
			    	 * continue processing other events in the scan file.
			    	 */
			    	LOGGER.error("Error occured while parsing the scan event : "+line + " ########### " + ee.getMessage());
			    	try {
		                CommonUtils.getInstance().sendSystemMessage("USPSScansBO", "USPSScansBO failed to parse scan event"+line+" from file " +fileName+" ####### "+ ee.getMessage());
		            } catch (Exception e) {
		                String errMsg = "Unable to send message to support pager.";
		                LOGGER.fatal(errMsg,e);
		            }
		            continue;
			    }
                
                CarrierScanVO vo = new CarrierScanVO();
                vo.setCarrierId(carrierId);
                vo.setFileName(fileName);
                vo.setProcessed(false);
                if(scanType != null){
                	vo.setScanType(scanType.trim());
                }
                
                String finalTrackingNumber = null;
                if(trackingNumber != null && trackingNumber.trim().length() > 0){
		    		trackingNumber = trackingNumber.trim();
		    		// Create a Pattern object
	                Pattern p = Pattern.compile(pattern);

	                // Now create matcher object.
	                Matcher m = p.matcher(trackingNumber);
	                if (m.find( )) {
	                	finalTrackingNumber = m.group(1);
	                	LOGGER.info("Match found : " + finalTrackingNumber);
	                }
	                
	                if(finalTrackingNumber != null){
	                	vo.setTrackingNumber(finalTrackingNumber.trim());
	                }else{
	                	LOGGER.info("Match not found : "+ trackingNumber);
	                	vo.setTrackingNumber(trackingNumber.trim());
	                }
		    	}
                
                
                
                
                //Format 200610311630
                String dateString = scanDate + scanTime;
                Date eventDate;
                try {
                    eventDate = (new SimpleDateFormat(USPS_EVENT_FORMAT)).parse(dateString);
                } catch (Exception e) {
                    LOGGER.warn("Error parsing event date for order/tracking number "+vo.getOrderId()+"/"+vo.getTrackingNumber()+".  Will use current timestamp instead.");
                    eventDate = new Date();
                }
                if(eventDate != null){
                	vo.setTimestamp(new Timestamp(eventDate.getTime()));
                }
                
                list.add(vo);
                
            }
        } catch (Exception e) {
            LOGGER.error("Error while reading USPS Scan file "+fileName,e);
        } finally {
            if( raf!=null ) {
                try {
                    raf.close();
                } catch (IOException ioe) {
                    LOGGER.warn("Failed to closing USPS Scan file "+fileName,ioe);
                }
            }
        }
        
        return list;
    }
}
