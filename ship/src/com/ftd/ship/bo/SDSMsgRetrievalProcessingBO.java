package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.common.resources.SDSStatusParm;
import com.ftd.ship.vo.SDSMessageVO;

import java.math.BigDecimal;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class SDSMsgRetrievalProcessingBO extends SDSProcessingBO {
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSMsgRetrievalProcessingBO");
    private static final long DUPLICATE = -1L;

    public SDSMsgRetrievalProcessingBO() {
    }

    public void processRequest(Connection connection, SDSStatusParm statusParm, Date startDate, Date endDate) throws Exception {
        assert(connection!=null) : "Database connection is null in SDSProcessingBO.processSDSBatch";

        String startDateStr;
        String endDateStr;

        if( startDate!=null ) {
            try {
                startDateStr = (new SimpleDateFormat(SDS_DATE_FORMAT)).format(startDate);
            } catch (Exception e) {
                LOGGER.warn("Unable to format startDate to a string");
                startDateStr = null;
            }
        } else {
            startDateStr=null;
        }

        if( endDate!=null ) {
            try {
                endDateStr = (new SimpleDateFormat(SDS_DATE_FORMAT)).format(endDate);
            } catch (Exception e) {
                LOGGER.warn("Unable to format startDate to a string");
                endDateStr = null;
            }
        } else {
            endDateStr=null;
        }

        //If either date is null, make them both null
        if( startDateStr==null || endDateStr==null ) {
            startDateStr=null;
            endDateStr=null;
        }

        LOGGER.debug("Requesting "+statusParm+" from SDS server.");

        Document responseDoc = sdsCommunications.getStatusUpdates(connection,SDSConstants.LOB_FTDCOM,statusParm,startDateStr,endDateStr);
        LOGGER.debug(JAXPUtil.toString(responseDoc));

        List<SDSMessageVO> messages = parseResponse(responseDoc);

        processResponse(connection,messages);
    }

    public void processResponse(Connection connection, List<SDSMessageVO> messages) throws Exception{

        int msgCount = messages.size();
        LOGGER.debug("Received "+msgCount+" messages to process.");
        StringBuffer errString = new StringBuffer();

        for( int idx=0; idx<msgCount; idx++ ) {
             SDSMessageVO vo = messages.get(idx);
            try {
                //Did we get a valid status type back?
                if( vo.getMsgType()==null ) {
                    throw new Exception("Unknown status type of "+vo.getStatus()+" received.");
                }

                //Save to database venus.sds_status_msg (trigger will fire off JMS msg)
                BigDecimal msgId = shipDAO.insertSDSMessage(connection,vo);

                if( msgId==null ) {
                    errString.append("Null value returned from shipDAO.insertSDSMessage for cartnon number "+vo.getCartonNumber());
                    continue;
                }
                else if( msgId.longValue()==DUPLICATE ) {
                    LOGGER.info("Status "+vo.getStatus()+" for order "+vo.getCartonNumber()+"already exists.  Nothing to do.");
                } else {
                    LOGGER.info("Successfully saved status "+msgId+" for order "+vo.getCartonNumber());
                }

                if( LOGGER.isDebugEnabled() ) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Carton Number: ");
                    sb.append(vo.getCartonNumber());
                    sb.append("\r\nDirection: ");
                    sb.append(vo.getDirection());
                    sb.append("\r\nTracking Number: ");
                    sb.append(vo.getTrackingNumber());
                    sb.append("\r\nBatch Number: ");
                    sb.append(vo.getPrintBatchNumber());
                    sb.append("\r\nBatch Sequence: ");
                    sb.append(vo.getPrintBatchSequence());
                    sb.append("\r\nShip Date Date: ");
                    sb.append((new SimpleDateFormat(SDS_DATE_FORMAT)).format(vo.getShipDate()));
                    sb.append("\r\nStatus: ");
                    sb.append(vo.getStatus());
                    sb.append("\r\nStatus Date: ");
                    sb.append(getSdsDateTimeFormatLenient().format(vo.getShipDate()));

                    LOGGER.debug(sb.toString());
                }

            } catch (Throwable e) {
                StringBuilder tempSB = new StringBuilder();
                tempSB.append("Error while processing message:\r\n");
                tempSB.append(e.getMessage());
                tempSB.append("\r\n");
                if( vo!=null ) {
                    tempSB.append(vo.getValue());
                    tempSB.append("\r\n");
                }
                errString.append(tempSB.toString());
                errString.append("\r\n");
                LOGGER.error(e);
            }
        }

        if( errString.length()>0 ) {
            throw new SDSApplicationException("Errors found while processing SDS batch:\r\n"+errString.toString().trim());
        }
    }

    public List<SDSMessageVO> parseResponse(Document doc) throws Exception {
        List<SDSMessageVO> response = new ArrayList<SDSMessageVO>();

        NodeList list = JAXPUtil.selectNodes(doc,"//sd:PLD_ROW","sd","http://ScanData.com/FTD/WTM/DataSetSchemas/WTM_XMLSchema.xsd");
        LOGGER.info("Status update found = "+String.valueOf(list.getLength()));

        SDSMessageVO msgVO;
        for( int idx=0; idx<list.getLength(); idx++ ) {
            Element row = (Element)list.item(idx);
            msgVO = new SDSMessageVO();

            //Carton number
            String strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_CARTON_NUMBER);
            msgVO.setCartonNumber(strValue);

            //Tracking number
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_TRACKING_NUMBER);
            msgVO.setTrackingNumber(strValue);

            //Print Number
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_BATCH_ID);
            if( StringUtils.isNotBlank(strValue) ) {
                msgVO.setPrintBatchNumber(strValue);
            }

            //Batch Sequence
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_BATCH_SEQUENCE);
            try {
                BigDecimal batchSequence = new BigDecimal(strValue);
                msgVO.setPrintBatchSequence(batchSequence);
            } catch (Exception e) {
                msgVO.setPrintBatchSequence(null);
            }

            //Ship Date
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_SHIP_DATE);
            try {
                 msgVO.setShipDate((new SimpleDateFormat(SDS_DATE_FORMAT)).parse(strValue));
            } catch (Exception e) {
                 LOGGER.error("Error converting ship date "+strValue+".  Setting to current date");
                 msgVO.setShipDate(new Date());
            }

            //Status
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_TYPE);
            if( StringUtils.equals(strValue,MsgType.CANCELED_NOT_LABELED.toString()) ) {
                LOGGER.debug("SDS status CANCELED_NOT_LABELED being changed to CANCELED");
                msgVO.setStatus(MsgType.CANCEL.toString());
                msgVO.setMsgType(MsgType.CANCEL);
            } else {
                msgVO.setStatus(strValue);
                msgVO.setMsgType(MsgType.sdsToMsgType(strValue));
            }

            if( StringUtils.equals(strValue,SDSStatusParm.CANCELED.toString()) ) {
                msgVO.setDirection(MsgDirection.OUTBOUND);
            } else {
                msgVO.setDirection(MsgDirection.INBOUND);
            }

             //Status Date
            strValue = JAXPUtil.getFirstChildNodeTextByTagName(row,SDSConstants.TAG_STATUS_STATUS_DATE);
            try {
                msgVO.setStatusDate((new SimpleDateFormat(SDS_DATE_TIME_FORMAT_NO_MILLISECONDS)).parse(strValue));
            } catch (Exception e) {
                LOGGER.error("Error converting status date "+strValue+".  Setting to current date");
                msgVO.setStatusDate(new Date());
            }

            msgVO.setProcessed(false);

            if( msgVO.getMsgType()==null ) {
                //Invalid status received so return the element to report on
                msgVO.setValue(JAXPUtil.toString(row));
            }
            response.add(msgVO);
        }


        return response;
    }
}
