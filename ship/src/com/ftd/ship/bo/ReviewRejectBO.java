package com.ftd.ship.bo;

import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.vo.SDSLogisticsVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;


//RLSE_2_4_0 (new Class)
/**
 * The purpose of this class is to provide a way to review rejects on orders
 * to see if there is special processing required for partner orders.
 */
public class ReviewRejectBO extends SDSShipmentProcessingBO {

    /**
     * Default constructor
     */
    public ReviewRejectBO() {
        
    }

    /**
     * 1.  Get's the order data
     * 2.  Determines if the reject was a vendor type
     * 3.  Calls super.reprocessOrder method
     * @param connection database connection
     * @param rejectMessageVO contains venus.venus record to process
     * @throws Exception
     */
    public void processRequest( Connection connection, VenusMessageVO rejectMessageVO) throws Exception {
        assert(connection!=null) : "Database connection is null in ReviewRejectBO.processRequest";
         
        SDSLogisticsVO logisticsVO = getOrderData(connection,rejectMessageVO);
         
        logisticsVO.setMessageVo(rejectMessageVO);
         
        //Determine if the reject was a vendor reject (VENUS_STATUS != 'ERROR');
        boolean bIncrementShipDate = !VenusStatus.ERROR.equals(rejectMessageVO.getVenusStatus());
         
        reprocessOrder(connection,logisticsVO,false,false,bIncrementShipDate);
    }
}
