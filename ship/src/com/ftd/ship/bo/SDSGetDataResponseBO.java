package com.ftd.ship.bo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.vo.SDSShipResponseVO;
import com.ftd.ship.vo.VenusMessageVO;

import java.sql.Connection;

import org.w3c.dom.Document;

/**
 * @deprecated
 */
public class SDSGetDataResponseBO extends SDSProcessingBO {
    private static final Logger logger  = new Logger("com.ftd.ship.bo.SDSGetDataResponse");
    
    /**
     * @deprecated
     */
    public SDSGetDataResponseBO() {
    }

    /**
     * @deprecated
     * @param connection
     * @param venusMessageVO
     * @throws Exception
     */
    public void processRequest( Connection connection, VenusMessageVO venusMessageVO) throws Exception {
        //TODO: Complete once ScanData is done
        logger.info("Received request to get sds response data for venus order number "+venusMessageVO.getVenusOrderNumber());
        
        venusMessageVO.setVenusStatus(VenusStatus.ERROR);
        venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
        shipDAO.updateVenusSDS(connection,venusMessageVO);
    }

    /**
     * @deprecated
     * @param responseDoc
     * @return
     * @throws SDSApplicationException
     */
    public SDSShipResponseVO parseResponse(Document responseDoc) throws SDSApplicationException {
        //TODO: Complete once ScanData is done
        return new SDSShipResponseVO();
    }
}
