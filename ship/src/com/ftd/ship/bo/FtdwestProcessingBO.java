package com.ftd.ship.bo;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.ship.bo.communications.ftdwest.FtdwestApplicationException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunications;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunicationsException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestRuntimeException;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.ExternalSystemStatus;
import com.ftd.ship.common.FtdwestConstants;
import com.ftd.ship.common.MsgDirection;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.VenusStatus;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.OrderDAO;
import com.ftd.ship.dao.QueueDAO;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.CommentsVO;
import com.ftd.ship.vo.FtdwestLogisticsVO;
import com.ftd.ship.vo.FtdwestPersonalizationVO;
import com.ftd.ship.vo.FtdwestResponseVO;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.OrderVO;
import com.ftd.ship.vo.QueueVO;
import com.ftd.ship.vo.SDSTransactionVO;
import com.ftd.ship.vo.VenusMessageVO;

/**
 * @author kkeyan
 *
 */

public class FtdwestProcessingBO {
	private static final String CREATE_ORDER_STR = "CreateOrder";
	private static final String CANCEL_ORDER = "CancelOrder";
	private static final String FTDWEST_SKYNET_SERVICE = "Ftdwest Skynet Service";
	private static Logger logger = new Logger("com.ftd.ship.bo.FtdwestProcessingBO");
	protected static final String EMAIL_FORMAT = "EEE, d MMMMM, yyyy";
	private FtdwestCommunications ftdwestCommunications;
	private ShipDAO shipDAO;
	protected OrderDAO orderDAO;
	protected QueueDAO queueDAO;
	
	
	public FtdwestResponseVO processFtdwestOrder(Connection connection, VenusMessageVO venusMessageVO) throws FtdwestApplicationException, FtdwestCommunicationsException,FtdwestRuntimeException{
		FtdwestResponseVO ftdwestResponseVO=null;
		try {
			logger.info("FTDWest Create Order processing started");
            long startTotalTime = (new Date()).getTime();
			FtdwestLogisticsVO logisticsVO=gatherOrderData(connection, venusMessageVO);		
			long startTime = (new Date()).getTime();
			ftdwestResponseVO=ftdwestCommunications.createOrder(connection,logisticsVO);
			
			if(FtdwestConstants.FTDWEST_CREATEORDER_SUCCESS.equalsIgnoreCase(ftdwestResponseVO.getCreateOrderStatus())){
				logServiceResponseTracking(venusMessageVO.getVenusId(), CREATE_ORDER_STR,startTime, connection);
				
				logger.debug("Update venus messge as Verified");
				//1. update Venus record..
				venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
                venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                venusMessageVO.setFtdwestOrderId(ftdwestResponseVO.getFtdwestOrderId());
                shipDAO.updateVenusSDS(connection, venusMessageVO);
                
                //2. create Ans message                
                logger.debug("Create ANS message");
                StringBuilder sb = new StringBuilder();
        		sb = new StringBuilder();
        		sb.append("Your order is scheduled for delivery on ");
        		sb.append((new SimpleDateFormat(EMAIL_FORMAT)).format(logisticsVO.getVenusMessageVO().getDeliveryDate()));
        		sb.append(".");
                
                Date now = new Date();
                VenusMessageVO ansMsg = CommonUtils.copyVenusMessage(venusMessageVO, MsgType.ANSWER);
                ansMsg.setMessageDirection(MsgDirection.INBOUND);
                ansMsg.setMessageText(sb.toString());
                ansMsg.setOperator(ShipConstants.SDS_USER);
                ansMsg.setTransmissionTime(now);
                ansMsg.setComments(sb.toString());
                ansMsg.setVenusStatus(VenusStatus.VERIFIED);
                ansMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                shipDAO.insertVenusMessage(connection, ansMsg);
                
                OrderDetailVO orderDetailVO=logisticsVO.getOrderDetailVO();
                try
                {
                    //update DCON status to pending if preferred partner.
                	
                    PartnerVO partnerVO = FTDCommonUtils.getPreferredPartnerBySource(orderDetailVO.getSourceCode());
                    String prefPartner = null;
                    if (partnerVO != null)
                    {
                        prefPartner = partnerVO.getPartnerName();
                    }
                    logger.debug("processResponse -> source code is:" + orderDetailVO.getSourceCode());
                    logger.debug("processResponse -> prefpartner is:" + prefPartner);
                    if (StringUtils.isNotBlank(prefPartner))
                    {
                        orderDAO.updateDeliveryConfirmationStatus(connection, venusMessageVO.getReferenceNumber(),
                                ShipConstants.DCON_STATUS_PENDING);
                    }
                }
                catch (Exception e)
                {
                    logger.error("processResponse -> " + e);
                    CommonUtils.getInstance().sendSystemMessage(
                            "Attempt to update DCON status to Pending failed. venus id:" + venusMessageVO.getVenusId());
                }
                
                logger.info("Update venus messge as Verified");
                //Update the order detail record
                orderDetailVO.setVendorId(venusMessageVO.getVendorId());
                //orderDetailVO.setCarrierDelivery(carrierDelivery);  // ??? TODO !!!
                orderDetailVO.setFloristId(venusMessageVO.getFillingVendor());
                orderDAO.updateOrderDetailCarrierInfo(connection, orderDetailVO);

                // For old SDS system we would call below, but OP is already setting florist-used for West, so not necessary 
                //orderDAO.insertOrderFloristUsed(connection, orderDetailVO, venusMessageVO.getVendorId());
                
			}else {
				logServiceResponseTracking(venusMessageVO.getVenusId(), CREATE_ORDER_STR,startTime, connection);
				StringBuilder sb = new StringBuilder();
        		sb = new StringBuilder();
        		sb.append("No vendors were available to service this venus order ");
        		sb.append("("+venusMessageVO.getVenusOrderNumber()+")");
        		sb.append(" - ");
        		HashMap errmorMsgMap=ftdwestResponseVO.getErrorMap();
        		
        		if(errmorMsgMap !=null && ! errmorMsgMap.isEmpty()) {
        			Iterator mapIter=errmorMsgMap.keySet().iterator();
	        		while(mapIter.hasNext()){
	        			sb.append(" "+errmorMsgMap.get(mapIter.next()));
	        		}
        		}
        		if(! sb.toString().endsWith(".")){
        			sb.append(".");
        		}
        		
				rejectAndQueue(connection, logisticsVO, sb.toString());
				
				venusMessageVO.setVenusStatus(VenusStatus.ERROR);
                venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
                shipDAO.updateVenusSDS(connection, venusMessageVO);

			}
			
			//logServiceResponseTracking(venusMessageVO.getVenusId(), startTime, connection);
			insertFtdwestTransaction(connection, venusMessageVO.getVenusId(),ftdwestResponseVO.getCreateOrderRequestMessage(),ftdwestResponseVO.getCreateOrderResponseMessage());
            long totalTime = System.currentTimeMillis() - startTotalTime;
            logger.info("FTDWest Create Order processing completed in (ms): " + totalTime);

            
		} catch (FtdwestApplicationException e) {
			logger.error("Error (FtdwestApplicationException) processing Ftdwest Create order"+e.getMessage());
			throw e;
		} catch (FtdwestCommunicationsException e) {
			logger.error("Error (FtdwestCommunicationsException) processing Ftdwest Create order"+e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error (Exception) processing Ftdwest Create order"+e.getMessage());
			throw new FtdwestRuntimeException(e.getMessage());
		}
		return ftdwestResponseVO;
		
	}
	
	
	public void cancelFtdwestOrder(Connection connection, VenusMessageVO venusMessageVO) throws FtdwestApplicationException, FtdwestCommunicationsException{
		
		logger.info("Processing ftdWest Cancel Orders !!");
		 try {
			 long startTime = (new Date()).getTime();
			 
			
			//Set the status on the original order and mark canceled       
			 VenusMessageVO assocOrderVO = getAssociatedOrder(connection, venusMessageVO.getVenusOrderNumber());
			 
			 FtdwestResponseVO ftdwestResponseVO=ftdwestCommunications.cancelOrder(connection, assocOrderVO);
			 
				if(FtdwestConstants.FTDWEST_CREATEORDER_SUCCESS.equalsIgnoreCase(ftdwestResponseVO.getCreateOrderStatus())){
					 	assocOrderVO.setCancelledStatusDate(new java.util.Date());
			            shipDAO.markVenusMsgCancelled(connection, assocOrderVO);
		
			            //Mark the can as being successful
			            venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
			            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.NOTVERIFIED);
			            shipDAO.updateVenusSDS(connection, venusMessageVO);
			            logger.info("Successfully cancelled tracking number for " + venusMessageVO.getVenusOrderNumber());
				}else {
					
					logger.info("ftdWest Orders, cancellation failed");
					//if already cancelled..
					boolean alreadyCancelled=false; // not implemented
					boolean tooLateToCancel=false;// not implemented
					 FtdwestLogisticsVO logisticsVO=gatherOrderData(connection, venusMessageVO);	
					
					if(alreadyCancelled){
						assocOrderVO.setVenusStatus(VenusStatus.VERIFIED);
						assocOrderVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
						shipDAO.updateVenusSDS(connection, venusMessageVO);
						
						//Mark the can as being successful
			            venusMessageVO.setVenusStatus(VenusStatus.VERIFIED);
			            venusMessageVO.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
			            shipDAO.updateVenusSDS(connection, venusMessageVO);
			            
			            // createOrderComment...
			            
			            
					}else if(tooLateToCancel){
						
						
						rejectAndQueue(connection,logisticsVO, null);
						// createOrderComment...
					}else{
						 //QE3SP-2: Add logic to not reject and queue the order if 
				         //FTD West responds with an error code of 004032001066 and the delivery date is in the past
						 //otherwise Reject and Queue the order
						 boolean rejectAndQueueOrder = true;
						 StringBuilder errString = new StringBuilder();
				            errString.append("Error processing cancel request\r\n");
				            errString.append("Error code: ");
				            HashMap errmorMsgMap=ftdwestResponseVO.getErrorMap();			            
				            if(errmorMsgMap !=null && ! errmorMsgMap.isEmpty()) {
			        			Iterator mapIter=errmorMsgMap.keySet().iterator();
				        		while(mapIter.hasNext()){
				        			String errCode= (String) mapIter.next();
				        			// check delivery date to see if it's before today
				        			// if it is don't Reject and Queue the order
				        			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

				        			// remove time information from today for comparison purposes
				        			Date today = format.parse(format.format(new Date()));
				        			
				        			if(errCode.equalsIgnoreCase("004032001066") && today.compareTo(assocOrderVO.getDeliveryDate()) == 1){
				        				rejectAndQueueOrder = false;
				        			}
				        			 errString.append(errCode);
				        			 errString.append("\r\n");
				        			 errString.append("Error message: ");
				        			 errString.append(errCode+" "+errmorMsgMap.get(errCode));
				        		}
			        		}
										        
				        if (rejectAndQueueOrder){
				        	logger.info("Rejecting the CAN message, and Queue");
				        	rejectAndQueue(connection,logisticsVO, errString.toString());
				        }
						// createOrderComment...
						 logger.debug("CreateOrderComment");
						createComment(connection, errString.toString(), logisticsVO.getOrderDetailVO());
						
						//Send System message
						logger.info("Sending System message: " + errString.toString());
			             CommonUtils.getInstance().sendSystemMessage(errString.toString());

					}
					
					// Always create an ADJ if CAN fails for any reason
					createAdjForCancelError(connection, venusMessageVO, assocOrderVO); 
               createComment(connection, "Cancel message to FTD West failed.  System generated ADJ will go to Venus.", logisticsVO.getOrderDetailVO());
				}
		
				logServiceResponseTracking(venusMessageVO.getVenusId(), CANCEL_ORDER, startTime, connection);
				insertFtdwestTransaction(connection, venusMessageVO.getVenusId(),ftdwestResponseVO.getCreateOrderRequestMessage(),ftdwestResponseVO.getCreateOrderResponseMessage());
		 } catch (FtdwestCommunicationsException e) {
			 logger.error("Error(FtdwestCommunicationsException) canceling Ftdwest Order !!"+e.getMessage());
			 throw e;
		} catch (FtdwestApplicationException e) {
			logger.error("Error(FtdwestApplicationException) canceling Ftdwest Order !!"+e.getMessage());	
			 throw e;
		} catch (FtdwestRuntimeException e) {
			 logger.error("Error(FtdwestRuntimeException) canceling Ftdwest Order !!"+e.getMessage());	
			 throw e;
		} catch (Exception e) {
			throw new FtdwestRuntimeException("Error canceling Ftdwest Order !!");
		}
	}
	
	 /*
     * Get the order message assocated with the past in venus messageVO
     */
    protected VenusMessageVO getAssociatedOrder(Connection conn,String externalOrderNumber) throws SDSApplicationException
    {
        //Retrieve venus FTD order message related to this message
        List venusList = null;
        try {
            venusList = shipDAO.getVenusMessage(conn, externalOrderNumber, MsgType.ORDER.getFtdMsgType());
        } catch (Exception e) {
            throw new SDSApplicationException("Error retrieving associated order for "+externalOrderNumber);
        }
        if(venusList == null || venusList.size() == 0)
        {
            throw new SDSApplicationException("Corresponding order message could not be found for Venus order " + externalOrderNumber);
        }
        else
        {
            if(venusList.size() > 1)
            {
                throw new SDSApplicationException("Multiple FTDs were found for the for Venus order " + externalOrderNumber);
            }
        }
        VenusMessageVO foundOrder = (VenusMessageVO)venusList.get(0);

        return foundOrder;
    }
	

//	private StringBuilder getAnsMessageText(FtdwestLogisticsVO logisticsVO) {
//		StringBuilder sb = new StringBuilder();
//		sb = new StringBuilder();
//		sb.append("Your order is scheduled for delivery on ");
//		sb.append((new SimpleDateFormat(EMAIL_FORMAT)).format(logisticsVO.getVenusMessageVO().getDeliveryDate()));
//		sb.append(".");
//		return sb;
//	}

	public FtdwestCommunications getFtdwestCommunications() {
		return ftdwestCommunications;
	}

	public void setFtdwestCommunications(FtdwestCommunications ftdwestCommunications) {
		this.ftdwestCommunications = ftdwestCommunications;
	}
	
	private FtdwestLogisticsVO gatherOrderData(Connection connection, VenusMessageVO venusMessageVO) throws FtdwestApplicationException{
		FtdwestLogisticsVO ftdwestLogisticsVO= new FtdwestLogisticsVO();
		//ftdwestLogisticsVO.setVenusMessageVO(venusMessageVO);
		String customShippingCarrierCode =null;
		
		try {
			 OrderDetailVO orderDetailVO = orderDAO.getOrderDetail(connection, venusMessageVO.getReferenceNumber());
			 
	            if (orderDetailVO == null)
	            {
	                throw new FtdwestApplicationException("getOrderData -> No order detail record for the given order detail id: "
	                        + venusMessageVO.getReferenceNumber());
	            }
	            ftdwestLogisticsVO.setOrderDetailVO(orderDetailVO);

	            OrderVO orderVO = orderDAO.getOrder(connection, orderDetailVO.getOrderGuid());
	            if (orderVO == null)
	            {
	                throw new FtdwestApplicationException("getOrderData -> No order record for given order guid: "
	                        + orderDetailVO.getOrderGuid());
	            }
	            
	            ftdwestLogisticsVO.setOrderVO(orderVO);
	            
	            customShippingCarrierCode = shipDAO.getCustomShippingCarrierCode(connection,orderVO.getSourceCode());
	            logger.debug("CustomShippingCarrierCode :"+customShippingCarrierCode);
	            venusMessageVO.setCustShippingCarrierCode(customShippingCarrierCode);
	            ftdwestLogisticsVO.setVenusMessageVO(venusMessageVO);
	            
	            
	            if(orderDetailVO.getPersonalizationData()!=null && orderDetailVO.getPersonalizationData().length() >0 ){
		            
		            logger.info("Loading Personalization Info for the productID"+orderDetailVO.getProductId());
		            
		            List<FtdwestPersonalizationVO> FtdwestPersonalizationVOLst=orderDAO.getFtdwestPersonalizationsForProduct(connection,orderDetailVO.getProductId());
		            
		            if(FtdwestPersonalizationVOLst!=null){
		            	 logger.debug("Personalization Info size"+FtdwestPersonalizationVOLst.size());
		            }else{
	                    logger.debug("Personalization Info is Null");		            	
		            } 
		            
		            ftdwestLogisticsVO.setFtdwestPersonalizationVO(FtdwestPersonalizationVOLst);
	            }
	            
			
		}catch (FtdwestApplicationException sApp)
	            {
	                throw sApp;
	            }
	            catch (Exception e)
	            {
	                throw new FtdwestApplicationException("getOrderData -> Error collecting data to process venus order "
	                        + venusMessageVO.getVenusOrderNumber(), e);
	            }
		
		
		return ftdwestLogisticsVO;
	}
   
	/*
	 * Creates an auto-adjustment record.  To be called when an error occurred sending a cancel.
	 * Uses CAN as base for record, then steals price from original FTD.
	 */
   private void createAdjForCancelError(Connection conn, VenusMessageVO canVo, VenusMessageVO origFtdVo) throws SDSApplicationException {
      logger.info("Creating auto-ADJ since cancel failed");
      try {
         VenusMessageVO adjMsg = CommonUtils.copyVenusMessage(canVo, MsgType.ADJUSTMENT);
         adjMsg.setOperator("AUTO_ADJ");
         adjMsg.setMessageDirection(MsgDirection.OUTBOUND);
         adjMsg.setTransmissionTime(new Date());
         adjMsg.setComments("CANCEL FOR WEST ORDER FAILED. AUTO ADJ GENERATED.");
         adjMsg.setPrice(origFtdVo.getPrice());
         shipDAO.insertVenusMessage(conn, adjMsg);
      } catch (Exception e) {
         throw new SDSApplicationException("createAdjForCancelError -> " + e);
      }
   }

   
	  private void rejectAndQueue(Connection conn, FtdwestLogisticsVO logisticsVO, String message)
	            throws SDSApplicationException
	    {
	        try
	        {
	            //Create the reject
	            VenusMessageVO msgVO = logisticsVO.getVenusMessageVO();
	            VenusMessageVO rejectMsg = CommonUtils.copyVenusMessage(msgVO, MsgType.REJECT);
	            rejectMsg.setOperator(ShipConstants.SDS_USER);
	            rejectMsg.setMessageDirection(MsgDirection.INBOUND);

	            if (msgVO.getMsgType() == MsgType.ORDER && msgVO.getVenusStatus() == VenusStatus.PENDING)
	            {
	                logger.debug("rejectAndQueue -> Setting message status on reject to \"OPEN\" for \"PENDING\" FTD");
	                rejectMsg.setVenusStatus(VenusStatus.OPEN);
	            }
	            else
	            {
	                rejectMsg.setVenusStatus(VenusStatus.ERROR);
	            }

	            rejectMsg.setExternalSystemStatus(ExternalSystemStatus.VERIFIED);
	            rejectMsg.setTransmissionTime(new Date());
	            rejectMsg.setComments(message);
	            rejectMsg.setMessageText(message);
	            String venusId = shipDAO.insertVenusMessage(conn, rejectMsg);
	            rejectMsg.setVenusId(venusId);

	            //Queue the reject
	           
	            sendToQueue(conn,rejectMsg,logisticsVO.getOrderDetailVO(),logisticsVO.getOrderVO());
	        }
	        catch (SDSApplicationException sase)
	        {
	            throw sase;
	        }
	        catch (Exception e)
	        {
	            throw new SDSApplicationException("rejectAndQueue -> " + e);
	        }
	    }
	
	  protected void sendToQueue(Connection connection, VenusMessageVO venusMessageVO, OrderDetailVO orderDetailVO, OrderVO orderVO ) throws SDSApplicationException {
	        try {
	            //insert into queue
	            QueueVO queueVO = new QueueVO();
	            queueVO.setQueueType(venusMessageVO.getMsgType().getFtdMsgType());
	            queueVO.setMessageTimestamp(new java.util.Date());
	            queueVO.setMessageType(venusMessageVO.getMsgType().getFtdMsgType());
	            queueVO.setSystem(ShipConstants.FTD_WEST_SYSTEM);
	            queueVO.setMercuryNumber(venusMessageVO.getVenusOrderNumber());
	            queueVO.setMercuryId(venusMessageVO.getVenusId());
	            if(orderVO!=null ){
	            	queueVO.setMasterOrderNumber(orderVO.getMasterOrderNumber());
	            }
	            
	            if(orderDetailVO!=null ){
		            queueVO.setOrderGuid(orderDetailVO.getOrderGuid());
		            queueVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
		            queueVO.setExternalOrderNumber(orderDetailVO.getExternalOrderNumber());
	            }
	            
	            queueDAO.insertQueueRecord(connection,queueVO);
	        } catch (Exception e) {
	            throw new SDSApplicationException("Error while trying to send msg to queue",e);
	        }
	    }
	  
	  
	  protected void insertFtdwestTransaction(Connection conn, String venusId, String request, String response) throws Exception {
	        SDSTransactionVO vo = new SDSTransactionVO();
	        vo.setVenusId(venusId);
	        vo.setRequest(request);
	        vo.setResponse(response);

	        shipDAO.insertSDSTransaction(conn,vo);
	    }
	  
	  private void logServiceResponseTracking(String requestId,String serviceMethod, long startTime, Connection connection) {
			try {
				long responseTime = System.currentTimeMillis() - startTime;
				ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
				srtVO.setServiceName(FTDWEST_SKYNET_SERVICE);
				srtVO.setServiceMethod(serviceMethod);
				srtVO.setTransactionId(requestId);
				srtVO.setResponseTime(responseTime);
				srtVO.setCreatedOn(new Date());
				ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
				srtUtil.insert(connection, srtVO);
			} catch (Exception e) {
				logger.error("Exception occured while persisting Ftdwest CreateOrder response times.");
			}
			
		}
	  
	  
	  
	  protected void createComment(Connection conn, String comment, OrderDetailVO orderDetailVO)
	    {
	        try {
	            CommentsVO commentsVO = new CommentsVO();
	            commentsVO.setComment(comment);
	            commentsVO.setCommentOrigin(ShipConstants.SHIP_COMMENT_ORIGIN);
	            commentsVO.setCommentType(ShipConstants.COMMENT_TYPE_ORDER);
	            commentsVO.setCreatedBy(ShipConstants.SHIP_COMMENT_ORIGIN);
	            commentsVO.setCustomerId(Long.toString(orderDetailVO.getRecipientId()));
	            commentsVO.setOrderDetailId(Long.toString(orderDetailVO.getOrderDetailId()));
	            commentsVO.setOrderGuid(orderDetailVO.getOrderGuid());
	            orderDAO.insertComment(conn,commentsVO);
	        } catch (Throwable t) {
	            logger.error("Adding comment to the database has failed.  Continuing with the transaction.",t);
	        }
	    }


	public void setOrderDAO(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}

	public void setShipDAO(ShipDAO shipDAO) {
		this.shipDAO = shipDAO;
	}

	public void setQueueDAO(QueueDAO queueDAO) {
		this.queueDAO = queueDAO;
	}

}
