package com.ftd.ship.bo.communications.sds;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.Date;

import javax.xml.rpc.ServiceException;

import org.apache.axis.message.MessageElement;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ScanData.Comm.WTMServices.GetOrderStatusOrderStatusParams;
import com.ScanData.Comm.WTMServices.GetOrderStatusResponseGetOrderStatusResult;
import com.ScanData.Comm.WTMServices.GetPLDUpdateResponseGetPLDUpdateResult;
import com.ScanData.Comm.WTMServices.WTMServiceSoap_BindingStub;
import com.ScanData.Comm.WebServices.CancelShipUnitsResponseCancelShipUnitsResult;
import com.ScanData.Comm.WebServices.CancelShipUnitsShipUnitList;
import com.ScanData.Comm.WebServices.CreateShipUnitsCreateShipUnitsParams;
import com.ScanData.Comm.WebServices.CreateShipUnitsResponseCreateShipUnitsResult;
import com.ScanData.Comm.WebServices.GetShipUnitsInfoResponseGetShipUnitsInfoResult;
import com.ScanData.Comm.WebServices.GetShipUnitsInfoShipUnitList;
import com.ScanData.Comm.WebServices.ManifestCarrierResponseManifestCarrierResult;
import com.ScanData.Comm.WebServices.ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult;
import com.ScanData.Comm.WebServices.RateShipUnitsRateShipUnitsParams;
import com.ScanData.Comm.WebServices.RateShipUnitsResponseRateShipUnitsResult;
import com.ScanData.Comm.WebServices.WTMServiceSoap12Stub;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.common.resources.SDSStatusParm;
import com.ftd.ship.vo.GlobalParameterVO;

/**
 * Public API to the ScanData web service
 */
public class SDSCommunications
{
    private static Logger logger = new Logger("com.ftd.ship.bo.communications.SDSCommunications");
    private ResourceProviderBase resourceProvider;
    
    private static final int DEFAULT_TIMEOUT = 900; // fifteen minutes

    private static final String SHIP_CONFIG_CONTEXT = "SHIP_CONFIG";
    private static final String SDS_URL = "SDS_URL";
    private static final String WTM_URL = "WTM_URL";
    private static final String SDS_TIMEOUT_SECONDS = "SDS_TIMEOUT_SECONDS";
    private String sdsUrl = null;
    private String wtmUrl = null;

    /**
     * Default constructor
     * 
     * @throws Exception
     */
    public SDSCommunications()
    {
    }

    /**
     * Sends a rate shipment request for the passed in parameters
     * 
     * @param connection
     *            database connection
     * @param rateDocument
     *            contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document rateShipment(Connection connection, Document rateDocument, String venusId) throws SDSCommunicationsException,
            SDSApplicationException
    {
        logger.debug("rateShipment -> Entering rateShipment");
        Document responseDoc = null;

        try
        {
            WTMServiceSoap12Stub binding;
            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("rateShipment -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("rateShipment -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element) rateDocument.getFirstChild());
            int sessionID = (int)(new Date()).getTime() + (StringUtils.isNotBlank(venusId)?Integer.valueOf(venusId).intValue():0);

            RateShipUnitsRateShipUnitsParams csu = new RateShipUnitsRateShipUnitsParams(msgs);

            long startProcessTime = 0L;
            long endProcessTime = 0L;
            
            // Send Document
            try
            {
                logger.debug("rateShipment -> Sending rateShipment request to " + getSdsUrl() + " AND venusId = " + venusId + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                RateShipUnitsResponseRateShipUnitsResult x = binding.rateShipUnits(sessionID, csu);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("rateShipment -> Rate request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("rateShipment -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("rateShipment -> " + re);
                throw new SDSCommunicationsException("rateShipment exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("rateShipment -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (rateShipment)", e);
            }

        }
        finally
        {
            logger.debug("rateShipment -> Leaving rateShipment");
        }

        return responseDoc;
    }

    /**
     * Sends a create shipment request for the passed in parameters
     * 
     * @param connection
     *            database connection
     * @param shipDocument
     *            contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document createShipment(Connection connection, Document shipDocument, String venusId) throws SDSCommunicationsException,
            SDSApplicationException
    {
        logger.debug("createShipment -> Entering createShipment");
        Document responseDoc = null;

        try
        {
            WTMServiceSoap12Stub binding;
            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("createShipment -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("createShipment -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element) shipDocument.getFirstChild());
            int sessionID = (int)(new Date()).getTime() + (StringUtils.isNotBlank(venusId)?Integer.valueOf(venusId).intValue():0);

            CreateShipUnitsCreateShipUnitsParams csu = new CreateShipUnitsCreateShipUnitsParams(msgs);
            
            long startProcessTime = 0L;
            long endProcessTime = 0L;

            // Send Document
            try
            {
                logger.debug("createShipment -> Sending createShipment request to " + getSdsUrl() + " AND venusId = " + venusId + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                CreateShipUnitsResponseCreateShipUnitsResult x = binding.createShipUnits(sessionID, csu);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("createShipment -> Create request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("createShipment -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("createShipment -> " + re);
                throw new SDSCommunicationsException("createShipment exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("createShipment -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (createShipment)", e);
            }

        }
        finally
        {
            logger.debug("createShipment -> Leaving createShipment");
        }

        return responseDoc;
    }

    /**
     * Sends a cancel request for the passed in parameters
     * 
     * @param connection
     *            database connection
     * @param cancelDocument
     *            contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document cancelShipment(Connection connection, Document cancelDocument, String venusId) throws SDSCommunicationsException,
            SDSApplicationException
    {
        logger.debug("cancelShipment -> Entering cancelShipment");
        Document responseDoc = null;

        try
        {
            WTMServiceSoap12Stub binding;
            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("cancelShipment -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("cancelShipment -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element) cancelDocument.getFirstChild());

            CancelShipUnitsShipUnitList request = new CancelShipUnitsShipUnitList(msgs);
            int sessionID = (int)(new Date()).getTime() + (StringUtils.isNotBlank(venusId)?Integer.valueOf(venusId).intValue():0);

            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            try
            {
                logger.debug("cancelShipment -> Sending cancelShipment request to " + getSdsUrl() + " AND venusId = " + venusId + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                CancelShipUnitsResponseCancelShipUnitsResult x = binding.cancelShipUnits(sessionID, request);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("cancelShipment -> Cancel request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("cancelShipment -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("cancelShipment -> " + re);
                throw new SDSCommunicationsException("cancelShipment exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("cancelShipment -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (cancelShipment)", e);
            }

        }
        finally
        {
            logger.debug("cancelShipment -> Leaving cancelShipment");
        }

        return responseDoc;
    }

    /**
     * Sends a close request for the passed in parameters
     * 
     * @param connection
     *            database connection
     * @param closeDocument
     *            contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document manifestTrailer(Connection connection, String trailerNumber)
            throws SDSCommunicationsException, SDSApplicationException
    {
        Document responseDoc = null;
        logger.debug("manifestTrailer -> Entering manifestTrailer");

        try
        {
            WTMServiceSoap12Stub binding;
            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("manifestTrailer -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("manifestTrailer -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            int sessionID = (int) (new Date()).getTime();
            
            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            try
            {
                logger.debug("manifestTrailer -> Sending manifestTrailer request to " + getSdsUrl() + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                ManifestZoneJumpTrailerResponseManifestZoneJumpTrailerResult x = binding.manifestZoneJumpTrailer(sessionID, trailerNumber);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("manifestTrailer -> Manifest Trailer request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("manifestTrailer -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("manifestTrailer -> " + re);
                throw new SDSCommunicationsException("manifestTrailer exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("manifestTrailer -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (manifestTrailer)", e);
            }
        }
        finally
        {
            logger.debug("manifestTrailer -> Leaving manifestTrailer");
        }

        return responseDoc;
    }

    /**
     * Sends a close request for the passed in parameters
     * 
     * @param connection
     *            database connection
     * @param closeDocument
     *            contains the close parameters
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document manifestCarrier(Connection connection, String carrierID, String distributionCenter)
            throws SDSCommunicationsException, SDSApplicationException
    {
        Document responseDoc = null;
        logger.debug("manifestCarrier -> Entering manifestCarrier");

        try
        {
            WTMServiceSoap12Stub binding;

            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("manifestCarrier -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("manifestCarrier -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            int sessionID = (int) (new Date()).getTime();

            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            try
            {
                logger.debug("manifestCarrier -> Sending manifestCarrier request to " + getSdsUrl() + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                ManifestCarrierResponseManifestCarrierResult x = binding.manifestCarrier(sessionID, carrierID,
                        distributionCenter);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("manifestCarrier -> Manifest Carrier request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("manifestCarrier -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("manifestCarrier -> " + re);
                throw new SDSCommunicationsException("manifestCarrier exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("manifestCarrier -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (manifestCarrier)", e);
            }
        }
        finally
        {
            logger.debug("manifestCarrier -> Leaving manifestCarrier");
        }

        return responseDoc;
    }

    /**
     * This method will send a request for status updates to the SDS ship server.
     * 
     * @param connection
     *            database connection
     * @param lineOfBusiness
     *            should always be "AA"
     * @param statusParm
     *            what type of status is being requested
     * @param startDate
     *            starting date of the inquiry. May be null for new status only
     * @param endDate
     *            ending date of the inquiry. May be null for new status only
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document getStatusUpdates(Connection connection, String lineOfBusiness, SDSStatusParm statusParm,
            String startDate, String endDate) throws SDSCommunicationsException, SDSApplicationException
    {

        logger.debug("getStatusUpdates -> Entering getStatusUpdates");

        Document response = null;
        try
        {
            WTMServiceSoap_BindingStub binding;
            MessageElement[] msgs;

            try
            {
                URL url = new URL(getWtmUrl());
                binding = (WTMServiceSoap_BindingStub) new com.ScanData.Comm.WTMServices.WTMServiceLocator().getWTMServiceSoap(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("getStatusUpdates -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("getStatusUpdates -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            GetPLDUpdateResponseGetPLDUpdateResult results;
            try
            {
                logger.debug("getStatusUpdates -> Sending getStatusUpdates request to " + getSdsUrl());
                startProcessTime = System.currentTimeMillis();
                results = binding.getPLDUpdate(lineOfBusiness, statusParm == null ? null : statusParm.toString(),
                        startDate, endDate);
                msgs = results.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("getStatusUpdates -> Status Update request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("getStatusUpdates -> Received " + msgs.length + " responses from SDS.");
                response = msgs[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("getStatusUpdates -> " + re);
                throw new SDSCommunicationsException("getStatusUpdates exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("getStatusUpdates -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (getStatusUpdates)", e);
            }
        }
        finally
        {
            logger.debug("getStatusUpdates -> Leaving getStatusUpdates");
        }

        return response;
    }

    /**
     * Request previous ship units for the passed in parameters
     * @param connection database connection
     * @param request document containing the parameters needed to request the
     *        ship unit document.
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document getShipUnitInfo(Connection connection, Document getShipUnitInfoDocument) throws SDSCommunicationsException,
            SDSApplicationException
    {
        logger.debug("getShipUnitInfo -> Entering getShipUnitInfo");
        Document responseDoc = null;

        try
        {
            WTMServiceSoap12Stub binding;
            try
            {
                URL url = new URL(getSdsUrl());
                binding = (WTMServiceSoap12Stub) new com.ScanData.Comm.WebServices.WTMServiceLocator().getWTMServiceSoap12(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("getShipUnitInfo -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("getShipUnitInfo -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            MessageElement[] msgs = new MessageElement[1];
            msgs[0] = new MessageElement((Element) getShipUnitInfoDocument.getFirstChild());

            GetShipUnitsInfoShipUnitList request = new GetShipUnitsInfoShipUnitList(msgs);
            int sessionID = (int) (new Date()).getTime();
            
            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            try 
            {
                logger.debug("getShipUnitInfo -> Sending getShipUnitInfo info request to " + getSdsUrl() + " AND sessionID = " + sessionID);
                startProcessTime = System.currentTimeMillis();
                GetShipUnitsInfoResponseGetShipUnitsInfoResult x = binding.getShipUnitsInfo(sessionID, request);
                MessageElement[] responses = x.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("getShipUnitInfo -> Ship Unit request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("getShipUnitInfo -> Received " + responses.length + " responses from SDS.");
                responseDoc = responses[0].getAsDocument();
            }
            catch (RemoteException re) {
                logger.error("getShipUnitInfo -> " + re);
                throw new SDSCommunicationsException("getShipUnitInfo exception caught: " + re);
            }
            catch (Exception e) {
                logger.error("getShipUnitInfo -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (getShipUnitInfo)",e);
            }

        } finally {
            logger.debug("getShipUnitInfo -> Leaving getShipUnitInfo");
        }

        return responseDoc;
     }

    /**
     * Request order status for the passed in parameters
     * @param connection database connection
     * @param request document containing the parameters needed to request the
     *        ship unit document.
     * @return
     * @throws SDSCommunicationsException
     * @throws SDSApplicationException
     */
    public Document getOrderStatus(Connection connection, Document request) throws SDSCommunicationsException,
            SDSApplicationException
    {
        logger.debug("getOrderStatus -> Entering getOrderStatus");
        Document response = null;

        try
        {
            WTMServiceSoap_BindingStub binding;
            MessageElement[] msgs = new MessageElement[1];

            try
            {
                URL url = new URL(getWtmUrl());
                binding = (WTMServiceSoap_BindingStub) new com.ScanData.Comm.WTMServices.WTMServiceLocator().getWTMServiceSoap(url);
            }
            catch (MalformedURLException mue)
            {
                logger.error("getOrderStatus -> Error setting SDS url", mue);
                throw new SDSCommunicationsException("Error setting SDS url", mue);
            }
            catch (ServiceException jre)
            {
                if (jre.getLinkedCause() != null)
                    logger.error("getOrderStatus -> " + jre.getLinkedCause());
                throw new SDSCommunicationsException("JAX-RPC ServiceException caught: " + jre);
            }

            // Time out value
            binding.setTimeout(getSDSTimeout(connection));

            msgs[0] = new MessageElement((Element) request.getFirstChild());
            GetOrderStatusOrderStatusParams params = new GetOrderStatusOrderStatusParams(msgs);

            long startProcessTime = 0L;
            long endProcessTime = 0L;
                        
            // Send Document
            try
            {
                logger.debug("getOrderStatus -> Sending getOrderStatus request to " + getSdsUrl());
                startProcessTime = System.currentTimeMillis();
                GetOrderStatusResponseGetOrderStatusResult results = binding.getOrderStatus(params);
                msgs = results.get_any();
                endProcessTime = System.currentTimeMillis();
                logger.debug("getOrderStatus -> Order Status request took " + (endProcessTime - startProcessTime) + " milliseconds to process");
                logger.debug("getOrderStatus -> Received " + msgs.length + " responses from SDS.");
                response = msgs[0].getAsDocument();
            }
            catch (RemoteException re)
            {
                logger.error("getOrderStatus -> " + re);
                throw new SDSCommunicationsException("getOrderStatus exception caught: " + re);
            }
            catch (Exception e)
            {
                logger.error("getOrderStatus -> " + e);
                throw new SDSApplicationException("Error retrieving results from SDS server (getOrderStatus)", e);
            }
        }
        finally
        {
            logger.debug("getOrderStatus -> Leaving getOrderStatus");
        }

        return response;
    }

    /**
     * Get a value from global parms. Will return the default value if not found or any error occurs.
     * 
     * @param connection
     *            database connection
     * @param context
     *            global param context
     * @param paramName
     *            parameter name
     * @param defaultValue
     *            default value to return
     * @return
     */
    private String getGlobalParameter(Connection connection, String context, String paramName, String defaultValue)
    {
        String retVal = "";
        GlobalParameterVO vo = null;
        try
        {
            vo = CommonUtils.getGlobalParameter(connection, context, paramName, defaultValue);
        }
        catch (Exception e)
        {
            logger.warn("Global parameter " + context + "/" + paramName + " not found.  Will use default value of "
                    + defaultValue, e);
        }

        if (vo == null)
        {
            if (StringUtils.isNotBlank(defaultValue))
            {
                retVal = defaultValue;
            }
        }
        else
        {
            retVal = vo.getValue();
        }

        return retVal;
    }

    /**
     * Get the timeout value, stored in global parms
     * 
     * @param connection
     *            database connection
     * @return Integer containing the timeout value. Returned value will never be null.
     */
    private Integer getSDSTimeout(Connection connection)
    {
        Integer timeout = new Integer(DEFAULT_TIMEOUT * 1000);
        try
        {
            String strTimeout = this.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT,
                    SDS_TIMEOUT_SECONDS, String.valueOf(DEFAULT_TIMEOUT));
            timeout = Integer.parseInt(strTimeout) * 1000;
        }
        catch (Exception e)
        {
            logger.error(SDS_TIMEOUT_SECONDS
                    + " does not appear to be configured correctly in global parms.  Defaulting to " + DEFAULT_TIMEOUT
                    + " seconds.", e);
            timeout = DEFAULT_TIMEOUT * 1000;
        }

        return timeout;
    }

    public void setResourceProvider(ResourceProviderBase resourceProvider)
    {
        this.resourceProvider = resourceProvider;
    }

    public ResourceProviderBase getResourceProvider()
    {
        return resourceProvider;
    }

    public String getSdsUrl() throws SDSApplicationException
    {
        if (sdsUrl == null)
        {
            try
            {
                sdsUrl = resourceProvider.getGlobalParameter(SHIP_CONFIG_CONTEXT, SDS_URL);
            }
            catch (Exception e)
            {
                logger.fatal("Unable to get sds url for ship server", e);
                throw new SDSApplicationException(e);
            }

        }
        return sdsUrl;
    }

    public String getWtmUrl() throws SDSApplicationException
    {
        if (wtmUrl == null)
        {
            try
            {
                wtmUrl = resourceProvider.getGlobalParameter(SHIP_CONFIG_CONTEXT, WTM_URL);
            }
            catch (Exception e)
            {
                logger.fatal("Unable to get wtm url for ship server", e);
                throw new SDSApplicationException(e);
            }
        }
        return wtmUrl;
    }
}
