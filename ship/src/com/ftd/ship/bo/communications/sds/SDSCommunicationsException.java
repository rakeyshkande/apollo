package com.ftd.ship.bo.communications.sds;

/**
 * The purpose of this class is to be able to distinguish the exeption that
 * happens when there is a communications failure while sharing data with the 
 * SDS ship server.  This exception should only be thrown if there is a break down
 * in the communications transport.  Application errors or errors returned from
 * the ship server should not be reported with this exception.  This exception 
 * should be thrown all the way up to the MDB where it can be rescheduled for
 * processing.
 */
public class SDSCommunicationsException extends Exception {
    public SDSCommunicationsException(Throwable t) {
        super(t);
    }
    public SDSCommunicationsException(String message) {
        super(message);
    }
    public SDSCommunicationsException(String message, Throwable t) {
        super(message,t);
    }
}
