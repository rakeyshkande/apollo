package com.ftd.ship.bo.communications.sds;

/**
 * The purpose of this class is to be able to distinguish the exeption that
 * happens when there is an unexpected/non-complient response received from the  
 * SDS ship server.  Properly formatted errors should not be reported with this 
 * exception.
 */
public class SDSApplicationException extends Exception {
    public SDSApplicationException(Throwable t) {
        super(t);
    }
    public SDSApplicationException(String message) {
        super(message);
    }
    public SDSApplicationException(String message, Throwable t) {
        super(message,t);
    }
}
