package com.ftd.ship.bo.communications.ftdwest;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.BindingProvider;

import org.apache.axis.utils.StringUtils;
import org.xml.sax.SAXException;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.OccasionHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.vo.VendorAddOnVO;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ship.common.FtdwestConstants;
import com.ftd.ship.common.FtdwestStatusType;
import com.ftd.ship.common.FtdwestUtils;
import com.ftd.ship.common.MsgType;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.common.resources.ResourceProviderBase;
import com.ftd.ship.vo.FtdwestGetProductDetailsResponseVO;
import com.ftd.ship.vo.FtdwestLogisticsVO;
import com.ftd.ship.vo.FtdwestPersonalizationVO;
import com.ftd.ship.vo.FtdwestResponseVO;
import com.ftd.ship.vo.FtdwestVendorOrderStatusVO;
import com.ftd.ship.vo.OrderDetailVO;
import com.ftd.ship.vo.OrderVO;
import com.ftd.ship.vo.VenusMessageVO;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.providecommerce.api.api.authentication.v1.EmailPasswordUserContext;
import com.providecommerce.api.api.common.v1.ArrayOfValidationError;
import com.providecommerce.api.api.common.v1.FaultDetail;
import com.providecommerce.api.api.common.v1.ValidationError;
import com.providecommerce.api.api.customer.v1.Customer;
import com.providecommerce.api.api.customer.v1.CustomerRecipient;
import com.providecommerce.api.api.order.v1.ArrayOfOrderActionReason;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDelivery;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDeliveryLineItem;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDeliveryLineItemAccessory;
import com.providecommerce.api.api.order.v1.ArrayOfOrderDeliveryLineItemAccessoryPersonalizationItem;
import com.providecommerce.api.api.order.v1.CancelOrderRequest;
import com.providecommerce.api.api.order.v1.CancelOrderResult;
import com.providecommerce.api.api.order.v1.CreateOrderRequest;
import com.providecommerce.api.api.order.v1.CreateOrderResult;
import com.providecommerce.api.api.order.v1.GetOrderResult;
import com.providecommerce.api.api.order.v1.Order;
import com.providecommerce.api.api.order.v1.OrderActionReason;
import com.providecommerce.api.api.order.v1.OrderDelivery;
import com.providecommerce.api.api.order.v1.OrderDeliveryDetails;
import com.providecommerce.api.api.order.v1.OrderDeliveryGiftMessage;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItem;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItemAccessory;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItemAccessoryPersonalization;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItemAccessoryPersonalizationItem;
import com.providecommerce.api.api.order.v1.OrderDeliveryLineItemDetails;
import com.providecommerce.api.api.order.v1.OrderService;
import com.providecommerce.api.api.order.v1.OrderServiceCancelOrderFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.order.v1.OrderServiceCreateOrderFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.order.v1.OrderService_Service;
import com.providecommerce.api.api.product.v1.ArrayOfProductDetail;
import com.providecommerce.api.api.product.v1.GetProductDetails;
import com.providecommerce.api.api.product.v1.GetProductDetailsResult;
import com.providecommerce.api.api.product.v1.ProductService;
import com.providecommerce.api.api.product.v1.ProductServiceGetProductDetailsFaultDetailFaultFaultMessage;
import com.providecommerce.api.api.product.v1.ProductService_Service;

/**
 * @author kkeyan
 *
 */
public class FtdwestCommunications {
	
	private static final String COMMA_DELIMITER = ",";
	private static Logger logger = new Logger("com.ftd.ship.bo.communications.ftdwest.FtdwestCommunications");
    private ResourceProviderBase resourceProvider;
    private EmailPasswordUserContext applicationContext; 
    private OrderService orderServicePort;
    private ProductService productServicePort;
    private String global_param_customerId;
    private boolean isInitialized=false;
    private boolean isPSInitialized=false;
    private static final String FTD_WEST_DEF_URL="http://localhost:8080/API/Order/v1/SOAP?singleWsdl";
    private static final String FTD_WEST_PRODUCT_DEF_URL="http://localhost:8080/API/Product/v1/SOAP?singleWsdl";
    private static final String FTD_WEST_PDP_PERSONALIZATION_PAYLOAD="PersonalizationPayload";

    
    
    private void init(Connection connection) throws FtdwestRuntimeException{
    	
    	try {
		    	if(!isInitialized){
		    		logger.debug("Initializing FtdwestCommunication Parameters !!");
			    	// 1.Construct OrderServicePort			    	
			    	 String OrderServiceURL=CommonUtils.getGlobalParamValue(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, FtdwestConstants.GLOBAL_PARMS_FTDWEST_SERVICE_URL, FTD_WEST_DEF_URL);		
			    	 logger.debug("OrderServiceURL-->"+OrderServiceURL);
			    	 URL wsdlURL= new URL(OrderServiceURL);
					
					 OrderService_Service ss = new OrderService_Service(wsdlURL);
				     orderServicePort = ss.getBasicHttpBindingOrderService();  
				     
			    	// 2.Construct ApplicationContext				    
					 ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
				     String applicationToken= configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_APP_TOKEN);		
				     String email=configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_LOGIN);
				     String pwd=configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_PASSWORD);
								     
				      applicationContext=new EmailPasswordUserContext();
				      
				      applicationContext.setApplicationToken(applicationToken);
				      applicationContext.setEmail(email);
				      applicationContext.setPassword(pwd);
				    
				      // set global params
				      global_param_customerId=CommonUtils.getGlobalParamValue(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, FtdwestConstants.GLOBAL_PARMS_FTDWEST_CUSTOMER_ID, "-9999");
				      isInitialized=true;
		    	}else{
		    		logger.debug("FtdwestCommunication Parameters has initialized, already!!");
		    	}
	    	} catch (MalformedURLException e) {
	    		logger.error("Error(MalformedURLException) initializing Ftdwest Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(MalformedURLException) initializing Ftdwest Parameters !! ");
			} catch (IOException e) {
				logger.error("Error(IOException) initializing Ftdwest Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(IOException) initializing Ftdwest Parameters !! ");
			} catch (SAXException e) {
				logger.error("Error(SAXException) initializing Ftdwest Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(SAXException) initializing Ftdwest Parameters !! ");
			} catch (ParserConfigurationException e) {
				logger.error("Error(ParserConfigurationException) initializing Ftdwest Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(ParserConfigurationException) initializing Ftdwest Parameters !! ");
			}catch (Exception e){
				logger.error("Error(Exception) initializing Ftdwest Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(Exception) initializing Ftdwest Parameters !! ");
			}
    	
    	
    }
    
    
public void init(Connection connection, String ftdWestServiceURL, String ftdWestDefUrl) throws FtdwestRuntimeException{
    	
    	try {
    			if(!isPSInitialized){
		    		logger.info("Initializing FtdwestCommunication Parameters for " + ftdWestServiceURL + "!!");
			    	// 1.Construct ProductServicePort			    	
			    	 String ServiceURL=CommonUtils.getGlobalParamValue(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, ftdWestServiceURL, ftdWestDefUrl);		
			    	 logger.info("ServiceURL-->"+ServiceURL);
			    	 URL wsdlURL= new URL(ServiceURL);

			    	 String timeout = CommonUtils.getGlobalParamValue(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_PRODUCT_SERVICE_TIMEOUT", "10000");
			    	 logger.info("FTDWEST_PRODUCT_SERVICE_TIMEOUT: "  + timeout);
					 ProductService_Service ss = new ProductService_Service(wsdlURL);
							             					   
					 productServicePort = ss.getBasicHttpBindingProductService();
					 ((BindingProvider) productServicePort).getRequestContext().put("javax.xml.ws.client.connectionTimeout", timeout);
					 ((BindingProvider) productServicePort).getRequestContext().put("javax.xml.ws.client.receiveTimeout", timeout);
					 ((BindingProvider) productServicePort).getRequestContext().put("set-jaxb-validation-event-handler", "false");

					 
				    // 2.Construct ApplicationContext				    
					 ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
					
				     String applicationToken= configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_APP_TOKEN);				     
				     String email=configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_LOGIN);
				     String pwd=configUtil.getSecureProperty(FtdwestConstants.SECURE_PARMS_APPLICATION_CONTEXT, FtdwestConstants.SECURE_PARMS_FTDWEST_PASSWORD);
				     
				      applicationContext=new EmailPasswordUserContext();
				      applicationContext.setApplicationToken(applicationToken);
				      applicationContext.setEmail(email);
				      applicationContext.setPassword(pwd);
				    
				      // set global params
				      global_param_customerId=CommonUtils.getGlobalParamValue(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, FtdwestConstants.GLOBAL_PARMS_FTDWEST_CUSTOMER_ID, "-9999");
				      isPSInitialized=true;
		    	}else{
		    		logger.debug("FtdwestCommunication Parameters for Product Service has initialized, already!!");
		    	}
	    	} catch (MalformedURLException e) {
	    		logger.error("Error(MalformedURLException) initializing Ftdwest Product Service Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(MalformedURLException) initializing Ftdwest Product Service Parameters !! " +e.getMessage()) ;
			} catch (IOException e) {
				logger.error("Error(IOException) initializing Ftdwest Product Service Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(IOException) initializing Ftdwest Product Service Parameters !! " +e.getMessage());
			} catch (SAXException e) {
				logger.error("Error(SAXException) initializing Ftdwest Product Service Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(SAXException) initializing Ftdwest Product Service Parameters !! " +e.getMessage());
			} catch (ParserConfigurationException e) {
				logger.error("Error(ParserConfigurationException) initializing Ftdwest Product Service Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(ParserConfigurationException) initializing Ftdwest Product Service Parameters !! " +e.getMessage());
			}catch (Exception e){
				logger.error("Error(Exception) initializing Ftdwest Product Service Parameters !! "+e.getMessage());
				throw new FtdwestRuntimeException("Error(Exception) initializing Ftdwest Product Service Parameters !! " +e.getMessage());
			}
    	
    	
    }
    
    
    public FtdwestResponseVO createOrder(Connection connection, FtdwestLogisticsVO logisticsVO) throws FtdwestRuntimeException, FtdwestCommunicationsException {
    	
    	logger.info("createOrder() !!");
    	FtdwestResponseVO ftdwestResponseVO=new FtdwestResponseVO();
    	
    	try {
    		    //1. initialize ..
	    		init(connection);
	    		
	    		//2. build create order request..
	    		CreateOrderRequest request=buildCreateRequest(logisticsVO);    
	    		String requestString=FtdwestUtils.getFtdwestString(request);
	    		ftdwestResponseVO.setCreateOrderRequestMessage(requestString);
	    		logger.debug(" Ftdwest Create Order request has been created !!"+requestString);
	    		
	    		//3. Create Ftdwest orders..
	    		CreateOrderResult result= orderServicePort.createOrder(applicationContext, request);
	    		String ftdwestOrderId=result.getOrderId();
	    		logger.info("Ftdwest Order Created:ftdwestOrderId=" + ftdwestOrderId);
	    		ftdwestResponseVO.setCreateOrderStatus(FtdwestConstants.FTDWEST_CREATEORDER_SUCCESS);
	    		ftdwestResponseVO.setFtdwestOrderId(ftdwestOrderId);
	    		String ftdwestResponseString=FtdwestUtils.getFtdwestString(result);
	    		ftdwestResponseVO.setCreateOrderResponseMessage(ftdwestResponseString);
	    		logger.debug(" Ftdwest Create Order response has been received !!"+ftdwestResponseString);
    		
			} catch (OrderServiceCreateOrderFaultDetailFaultFaultMessage e) {
				
				logger.error(" OrderServiceCreateOrderFaultDetailFaultFaultMessage has occurred.");
				FaultDetail faultDetail=e.getFaultInfo();
				String ftdwestResponseString=FtdwestUtils.getFtdwestString(faultDetail);
	    		ftdwestResponseVO.setCreateOrderResponseMessage(ftdwestResponseString);
	    		logger.debug(" Ftdwest Create Order response has been received !!"+ftdwestResponseString);	    		
	        	ArrayOfValidationError validationError= faultDetail.getValidationErrors();
	        	
	        	if(validationError !=null){
	        		HashMap<String,String> ValidationErrorMap=new HashMap<String,String>();
		        	for(ValidationError verr:validationError.getValidationError()){
		        		ValidationErrorMap.put(verr.getErrorCode(), verr.getErrorMessage());
		        		logger.debug("Error Code-->"+verr.getErrorCode());	        		
		        		logger.debug("Error msg-->"+verr.getErrorMessage());
		        	}
		        	ftdwestResponseVO.setCreateOrderStatus("Failiure");
		        	ftdwestResponseVO.setErrorMap(ValidationErrorMap);
	        	}else{
	        		throw new FtdwestCommunicationsException("Error Communicating Ftdwest service for Create Order !!");
	        	}	        	
	        	
				return ftdwestResponseVO;
				
			} catch (FtdwestRuntimeException e) {
				logger.error("Error(FtdwestRuntimeException) creating Ftdwest Order !!"+e.getMessage());
				throw e;
			
			}catch(Exception e){
				logger.error("Error(Exception) creating Ftdwest Order !!"+e.getMessage());
				throw new FtdwestRuntimeException("Error(Exception) creating Ftdwest Order !!"+e.getMessage());
			}
    	
    	return ftdwestResponseVO;
   
    	
    }
 

	public FtdwestResponseVO cancelOrder(Connection connection, VenusMessageVO assocOrderVO) throws FtdwestRuntimeException,FtdwestCommunicationsException{
		FtdwestResponseVO ftdwestResponseVO=new FtdwestResponseVO();
		try {
		//1. initialize ..
		init(connection);		
		
		//2. build Cancel order request..
		CancelOrderRequest cancelRequest=buildCancelRequest(assocOrderVO);    
		
		String requestString=FtdwestUtils.getFtdwestString(cancelRequest);
		ftdwestResponseVO.setCreateOrderRequestMessage(requestString);
		
		logger.debug(" Ftdwest Cancel Order request has been created !!"+requestString);
		//3. Cancel orders..
		CancelOrderResult cancelOrderResult=orderServicePort.cancelOrder(applicationContext, cancelRequest);
		logger.debug(" FtdwestOrder cancelled successfully!!");
		ftdwestResponseVO.setCreateOrderStatus(FtdwestConstants.FTDWEST_CREATEORDER_SUCCESS);
		String ftdwestResponseString=FtdwestUtils.getFtdwestString(cancelOrderResult);
		ftdwestResponseVO.setCreateOrderResponseMessage(ftdwestResponseString);
		
		} catch (OrderServiceCancelOrderFaultDetailFaultFaultMessage e) {
			logger.error(" OrderServiceCreateOrderFaultDetailFaultFaultMessage has occurred.");
			FaultDetail faultDetail=e.getFaultInfo();
			String ftdwestResponseString=FtdwestUtils.getFtdwestString(faultDetail);
    		ftdwestResponseVO.setCreateOrderResponseMessage(ftdwestResponseString);
    		logger.debug(" Ftdwest Cancel Order response has been received !!"+ftdwestResponseString);
    		
        	ArrayOfValidationError validationError= faultDetail.getValidationErrors();
        	
        	if(validationError !=null){
        		HashMap<String,String> ValidationErrorMap=new HashMap<String,String>();
	        	for(ValidationError verr:validationError.getValidationError()){
	        		ValidationErrorMap.put(verr.getErrorCode(), verr.getErrorMessage());
	        		logger.debug("Error Code-->"+verr.getErrorCode());	        		
	        		logger.debug("Error msg-->"+verr.getErrorMessage());
	        	}
	        	ftdwestResponseVO.setCreateOrderStatus("Failiure");
	        	ftdwestResponseVO.setErrorMap(ValidationErrorMap);
        	}else{
        		throw new FtdwestCommunicationsException("Error Communicating Ftdwest service for Cancel Order!!");
        	}
        	
			return ftdwestResponseVO;
			
		}catch(FtdwestRuntimeException e){
			 logger.error("Error(FtdwestRuntimeException) canceling Ftdwest Order !!"+e.getMessage());
			throw e;
		}catch(Exception e){
			logger.error("Error(Exception) canceling Ftdwest Order !!"+e.getMessage());
			throw new FtdwestRuntimeException("Error(Exception) canceling Ftdwest Order !!"+e.getMessage());
		}
		
		return ftdwestResponseVO;
    	
    }
    
    
    private CancelOrderRequest buildCancelRequest(VenusMessageVO venusMessageVO) {
    	logger.info("Cancelling Ftdwest OrderId==>"+venusMessageVO.getFtdwestOrderId());
    	CancelOrderRequest cancelOrderRequest= new CancelOrderRequest();
    	cancelOrderRequest.setOrderId(venusMessageVO.getFtdwestOrderId());
    	ArrayOfOrderActionReason reasonAr=new ArrayOfOrderActionReason();
    	OrderActionReason reason=new OrderActionReason();
    	reason.setReasonId(FtdwestConstants.FTDWEST_CANCEL_ORDER_REASON_CD);
    	reasonAr.getOrderActionReason().add(reason);
    	cancelOrderRequest.setReasons(reasonAr);
		return cancelOrderRequest;
	}


	public  CreateOrderRequest buildCreateRequest(FtdwestLogisticsVO ftdWestLogisticsVO) throws FtdwestRuntimeException {
    	CreateOrderRequest req = new CreateOrderRequest();	
    	
    	try {    		
    	
	    	VenusMessageVO venusMsgVO=ftdWestLogisticsVO.getVenusMessageVO();
	    	OrderVO orderVO=ftdWestLogisticsVO.getOrderVO();
	    	OrderDetailVO orderDetailsVO=ftdWestLogisticsVO.getOrderDetailVO();
	    	
			Order order=new Order();
			
			
			//1. Customer
			Customer customer=new Customer();
			customer.setCustomerId(global_param_customerId);//100037813926
			logger.info("Customer ID>: " + global_param_customerId);
			order.setCustomer(customer);
			logger.info("PO Number>: " + venusMsgVO.getVenusOrderNumber());
			order.setPONumber(venusMsgVO.getVenusOrderNumber());
			
			//2.Deliveries
			ArrayOfOrderDelivery orderDelivariesLst=new ArrayOfOrderDelivery();
			OrderDelivery orderDelivery=new OrderDelivery();
			
			
			//2.1 Delivery Details
			OrderDeliveryDetails dd=new OrderDeliveryDetails();
			orderDelivery.setDetails(dd);
			//odls.add(orderDelivery);
			orderDelivariesLst.getOrderDelivery().add(orderDelivery);
			
			//2.2 OrderDeliveryLineItems
			ArrayOfOrderDeliveryLineItem odLineItemLst=new ArrayOfOrderDeliveryLineItem();			
			OrderDeliveryLineItem odLineItem=new OrderDeliveryLineItem();
			
			ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst=new ArrayOfOrderDeliveryLineItemAccessory();	
			
							
			createOrderdeliveryLineItems(ftdWestLogisticsVO, venusMsgVO, orderDetailsVO, orderDelivery,
					odliAccessoryLst);
			
			//odliAccessory.setAccessoryId("30142682");//for testing only
			logger.info("Product ID>: " + venusMsgVO.getVendorSKU());
			odLineItem.setProductId(venusMsgVO.getVendorSKU());
			logger.info("Quantity>: " + FtdwestConstants.NUMBER_OF_QNTY);
			odLineItem.setQuantity(FtdwestConstants.NUMBER_OF_QNTY);
			
			if(odliAccessoryLst.getOrderDeliveryLineItemAccessory()!=null && odliAccessoryLst.getOrderDeliveryLineItemAccessory().size()>0){
				odLineItem.setAccessories(odliAccessoryLst);
			}
			odLineItemLst.getOrderDeliveryLineItem().add(odLineItem);
			orderDelivery.setLineItems(odLineItemLst);
		    order.setDeliveries(orderDelivariesLst);
		    
		    CustomerRecipient custRecipient=new CustomerRecipient();
		    String recipientFnandLn[]=FtdwestUtils.getFtdWestFirstAndLastNames(venusMsgVO.getRecipient());
		    
		    custRecipient.setFirstName(recipientFnandLn[FtdwestConstants.FIRST_NAME_INDX]);
		    custRecipient.setLastName(recipientFnandLn[FtdwestConstants.LAST_NAME_INDX]);
		    custRecipient.setAddress1(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getAddress1(), FtdwestConstants.CITY_AND_NAME_FILTER_PATTERN, FtdwestConstants.FLD_LEN_40));
		    custRecipient.setAddress2(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getAddress2(), FtdwestConstants.CITY_AND_NAME_FILTER_PATTERN, FtdwestConstants.FLD_LEN_40));
		    custRecipient.setCity(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getCity(), FtdwestConstants.CITY_AND_NAME_FILTER_PATTERN, FtdwestConstants.FLD_LEN_40));
		    custRecipient.setState(venusMsgVO.getState());
		    custRecipient.setCountryCode(venusMsgVO.getCountry());	
		    String zipCode=venusMsgVO.getZip()!=null && venusMsgVO.getZip().length()>5?venusMsgVO.getZip().substring(0, 5):venusMsgVO.getZip();
		    custRecipient.setZip(zipCode);
		    custRecipient.setPhone(venusMsgVO.getPhoneNumber());		    
		    
		    if(venusMsgVO.getBusinessName()!=null){
		    	custRecipient.setLocationType(FtdwestConstants.LOC_TYPE_BUSINESS);
		    	custRecipient.setCompanyName(venusMsgVO.getBusinessName());
		    	logger.info("Recipient Company Name>: " + custRecipient.getCompanyName());
		    }else{
		    	custRecipient.setLocationType(FtdwestConstants.LOC_TYPE_RESIDENTIAL);
		    }
		    
		    logger.info("Location Type>: " + custRecipient.getLocationType());	
		    orderDelivery.setDeliveryDate(getXMLDate( venusMsgVO.getDeliveryDate()));
		    logger.info("Delivery date>: " + orderDelivery.getDeliveryDate());
		    if(FtdwestConstants.ORDER_DELIVERY_MORNING_FLAG_Y.equalsIgnoreCase(orderDetailsVO.getOrderHasMorningDelivery())){
		    	 orderDelivery.setDeliveryTime(FtdwestConstants.ORDER_DELIVERY_MORNING);
		    }else{
		    	 orderDelivery.setDeliveryTime(FtdwestConstants.ORDER_DELIVERY_AFTERNOON);
		    }
		   
		    logger.info("Delivery time>: " + orderDelivery.getDeliveryTime());
		  
		    
		    orderDelivery.setRecipient(custRecipient);
		    order.setAffiliateCode(venusMsgVO.getCustShippingCarrierCode()); //Setting Custom Shipping Carrier Code (Affiliate Code) 
		    
		    req.setOrder(order);
    	} catch (Exception e) {
    		e.printStackTrace();
    		logger.error("Error creating Ftdwest Create Order Request!! "+e.getMessage());
			throw new FtdwestRuntimeException("Error creating Ftdwest Create Order Request!! ");
		}
		return req;
	}


	private void createOrderdeliveryLineItems(FtdwestLogisticsVO ftdWestLogisticsVO, VenusMessageVO venusMsgVO,
			OrderDetailVO orderDetailsVO, OrderDelivery orderDelivery,
			ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst) throws Exception {
			
	    OrderDeliveryGiftMessage odGiftMessage=new OrderDeliveryGiftMessage();		    
	    OccasionHandler occHandler = (OccasionHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_OCCASION);
	    
	   if( isPersonalCreationPDPOrder(orderDetailsVO) ) { // For PCOrders (new PDP style)
         createPCPDPOrderdeliveryLineItems(orderDetailsVO, orderDelivery, odliAccessoryLst);
         odGiftMessage.setMessage(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getCardMessage(), FtdwestConstants.MESSAGE_TEXT_FILTER_PATTERN, FtdwestConstants.CARD_MESSAGE_LENGTH_PC));   
         logger.info("Gift Message>: " + venusMsgVO.getCardMessage());
	   } else if( isPersonalCreationOrder(orderDetailsVO) ) { // For PCOrders (old style)
			createPCOrderdeliveryLineItems(ftdWestLogisticsVO, orderDetailsVO, orderDelivery,
					odliAccessoryLst);
			odGiftMessage.setMessage(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getCardMessage(), FtdwestConstants.MESSAGE_TEXT_FILTER_PATTERN, FtdwestConstants.CARD_MESSAGE_LENGTH_PC));	
		    logger.info("Gift Message>: " + venusMsgVO.getCardMessage());
		
		} else { //for nonPCOrder
			createNonPCOrderdeliveryLineItems(venusMsgVO, orderDelivery, odliAccessoryLst);
			odGiftMessage.setMessage(FtdwestUtils.filterFieldForFtdwest(venusMsgVO.getCardMessage(), FtdwestConstants.MESSAGE_TEXT_FILTER_PATTERN, FtdwestConstants.CARD_MESSAGE_LENGTH_NON_PC));	
		    logger.info("Gift Message>: " + venusMsgVO.getCardMessage());
		} //end PCORder
		String ftdwestOccasionId = occHandler.getFtdWestOccasionId(ftdWestLogisticsVO.getOrderDetailVO().getOccasion());	
		logger.info("Ftd West Occasion ID>: " + ftdwestOccasionId);		    
	    odGiftMessage.setOccasionId(ftdwestOccasionId);
	    orderDelivery.setGiftMessage(odGiftMessage);
	}


	private void createNonPCOrderdeliveryLineItems(VenusMessageVO venusMsgVO, OrderDelivery orderDelivery,
			ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst) {
		logger.info("It is NOT a Personal Creation Order!!");
		OrderDeliveryLineItemAccessory odliAccessory=null;		
		List<AddOnVO> addonLst=venusMsgVO.getAddOnVO();
		orderDelivery.setServiceType(FtdwestConstants.FTDWEST_SERVICE_TYPE_DELIVERYDATE);
		if(addonLst!=null)
			logger.info("addonLst is not null and size is==>"+addonLst.size());
		else
			logger.error("addonLst is Null");		
		
		for(AddOnVO addOnVO:addonLst){	
			logger.info("Addon ID>: " + addOnVO.getAddOnId());
			HashMap<String, VendorAddOnVO> vendorMap=addOnVO.getVendorCostsMap();
			if(vendorMap!=null && ! vendorMap.isEmpty() ){					
				for(VendorAddOnVO vendorAddOnVO:vendorMap.values()){
					//vendorAddOnVO=vendorMap.get(addOnVO.getAddOnId());
					if(venusMsgVO.getVendorId().equalsIgnoreCase(vendorAddOnVO.getVendorId())){
						odliAccessory= new OrderDeliveryLineItemAccessory();
						logger.info("Vendor SKU>: " + vendorAddOnVO.getSKU());
						odliAccessory.setAccessoryId(vendorAddOnVO.getSKU());
						odliAccessoryLst.getOrderDeliveryLineItemAccessory().add(odliAccessory);
						break;//just get the right value.
					}
				}
									
			}else{
				logger.error("Addon ID>: not found " );
			}
		}
		
	}

   /**
    * Prepare a Personal Creations order.
    * Note this is the old-style of PC orders (pre-Nov 2017) and remains only for legacy purposes.
    * This style of PC involved mapping template data between data from website and product definitions in PDB. 
    * 
    * @param ftdWestLogisticsVO
    * @param orderDetailsVO
    * @param orderDelivery
    * @param odliAccessoryLst
    * @throws FtdwestApplicationException
    */
	private void createPCOrderdeliveryLineItems(FtdwestLogisticsVO ftdWestLogisticsVO, OrderDetailVO orderDetailsVO,
			OrderDelivery orderDelivery, ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst) throws FtdwestApplicationException {
		logger.info("It's an old-style Personal Creation Order!!");
		
		    orderDelivery.setServiceType(FtdwestConstants.FTDWEST_SERVICE_TYPE_SERVICELELVEL);//'DeliveryDate' is for PC orders specific
		    orderDelivery.setServiceCalendarOption(FtdwestConstants.FTDWEST_SERVICE_CAL_TYPE);
		    OrderDeliveryLineItemAccessory odliAccessory= null;
		    
			List<FtdwestPersonalizationVO> ftdwestPersonalizationVOLst=ftdWestLogisticsVO.getFtdwestPersonalizationVO();
			//quandity = quandity+ftdwestPersonalizationVOLst.size();
			
			for(FtdwestPersonalizationVO ftdwestPersonalizationVO:ftdwestPersonalizationVOLst){
				odliAccessory= new OrderDeliveryLineItemAccessory();
				
				logger.info(ftdwestPersonalizationVO.toString());
				odliAccessory.setAccessoryId(ftdwestPersonalizationVO.getAccessoryId());						
				HashMap<String,String> userXmlMapData=FtdwestUtils.getPersonalizationMap(orderDetailsVO.getPersonalizationData() );
				logger.info("userXmlMapData==>"+userXmlMapData.toString());					
				
				HashMap<String,String> mapApolloToPQuad=FtdwestUtils.getApolloToPQuadMap(ftdwestPersonalizationVO);
				logger.info("mapApolloToPQuad==>"+mapApolloToPQuad.toString());
				
				logger.info("ftdwestPersonalizationVO.getTemplateOrder()==>"+ftdwestPersonalizationVO.getTemplateOrder());
				String templateOrder[]=ftdwestPersonalizationVO.getTemplateOrder().split(COMMA_DELIMITER);
				
				OrderDeliveryLineItemAccessoryPersonalization odliAccessoryPersonalization=new OrderDeliveryLineItemAccessoryPersonalization();					
				ArrayOfOrderDeliveryLineItemAccessoryPersonalizationItem odliAccessoryPersonalizationItemLst=new ArrayOfOrderDeliveryLineItemAccessoryPersonalizationItem();
				for(int i=0;i< templateOrder.length ;i++){
					logger.info("templateOrder==>"+templateOrder[i]);
					OrderDeliveryLineItemAccessoryPersonalizationItem odliAccessoryPersonalizationItem =new OrderDeliveryLineItemAccessoryPersonalizationItem();
						String dispNm=mapApolloToPQuad.get(templateOrder[i])!=null?mapApolloToPQuad.get(templateOrder[i]).trim():null;
						String value=userXmlMapData.get(templateOrder[i])!=null?userXmlMapData.get(templateOrder[i]).trim():null;
						logger.info("ftdwestPersonalizationVO.isPersonalizationCaseFlag()==>" +ftdwestPersonalizationVO.isPersonalizationCaseFlag());
						if(!(StringUtils.isEmpty(value)) && ftdwestPersonalizationVO.isPersonalizationCaseFlag() )
						{							
							value=value.toUpperCase();
						}
					 logger.info("personalization data is "+value);
					odliAccessoryPersonalizationItem.setDisplayName(dispNm);
					odliAccessoryPersonalizationItem.setValue(value);
					odliAccessoryPersonalizationItemLst.getOrderDeliveryLineItemAccessoryPersonalizationItem().add(odliAccessoryPersonalizationItem);
				}
				
				if(odliAccessoryPersonalizationItemLst.getOrderDeliveryLineItemAccessoryPersonalizationItem() !=null &&
				   odliAccessoryPersonalizationItemLst.getOrderDeliveryLineItemAccessoryPersonalizationItem().size() >0	){
					odliAccessoryPersonalization.setPersonalizationItems(odliAccessoryPersonalizationItemLst);
					odliAccessory.setPersonalization(odliAccessoryPersonalization);
				}
				odliAccessoryLst.getOrderDeliveryLineItemAccessory().add(odliAccessory);
			}
		
	}

	
   /**
    * Prepare a Personal Creations PDP order.
    * This is the new style (Nov 2017) of PC orders where the personalized data originates from PDP. 
    * Essentially the website calls PDP (West) to collect personalization data, then passes it down to order-gatherer as-is.
    * 
    * @param ftdWestLogisticsVO
    * @param orderDetailsVO
    * @param orderDelivery
    * @param odliAccessoryLst
    * @throws FtdwestApplicationException
    */
   private void createPCPDPOrderdeliveryLineItems(OrderDetailVO orderDetailsVO, OrderDelivery orderDelivery, 
         ArrayOfOrderDeliveryLineItemAccessory odliAccessoryLst) throws FtdwestApplicationException, Exception {
      
      HashMap<String, String> pcValueMap = new HashMap<String, String>();
      HashMap<String, String> pcSortMap = new HashMap<String, String>();
      boolean atLeastOnePcItem = false;
      
      logger.info("It's a PDP Personal Creation Order!!");
      
      orderDelivery.setServiceType(FtdwestConstants.FTDWEST_SERVICE_TYPE_SERVICELELVEL);//'DeliveryDate' is for PC orders specific
      orderDelivery.setServiceCalendarOption(FtdwestConstants.FTDWEST_SERVICE_CAL_TYPE);
      
      // Get PDP personalization data that was passed from website into hashes
      //
      String accessoryId = FTDCommonUtils.populatePDPPersonalizationMaps(orderDetailsVO.getPersonalizationData(), pcValueMap, pcSortMap);
      if (accessoryId == null || accessoryId.isEmpty()) {
         throw new FtdwestApplicationException("Accessory ID could not be extracted from PDP data, so aborting - not making call to createOrder"); 
      }

      // Loop over hashes and build SKYNet object
      //
      ArrayOfOrderDeliveryLineItemAccessoryPersonalizationItem odliPcItemLst = new ArrayOfOrderDeliveryLineItemAccessoryPersonalizationItem();      
      OrderDeliveryLineItemAccessoryPersonalization odliAccessoryPersonalization = new OrderDeliveryLineItemAccessoryPersonalization(); 
      OrderDeliveryLineItemAccessory odliAccessory = new OrderDeliveryLineItemAccessory();

      Iterator<Map.Entry<String, String>> iter = pcValueMap.entrySet().iterator();
      while (iter.hasNext()) {
         Map.Entry<String, String> pcValuePair = (Map.Entry<String,String>)iter.next();
         OrderDeliveryLineItemAccessoryPersonalizationItem odliPcItem = new OrderDeliveryLineItemAccessoryPersonalizationItem();
         odliPcItem.setDisplayName(pcValuePair.getKey());
         odliPcItem.setValue(pcValuePair.getValue());
         String sortOrderStr = pcSortMap.get(pcValuePair.getKey());
         if (sortOrderStr != null && !sortOrderStr.isEmpty()) {
            try {
               // BEGIN TEMPORARY GLOBAL PARM LOGIC
               // Temporary global parm added here since West was not ready to handle SortOrder (when Apollo was deployed in Nov 2017).
               // This allows us to enable it when they are ready (and later remove this global_parm at our convenience).
               // We have to do this since they choke on the SortOrder parameter right now.
               // All lines below can be removed except for the setSortOrder line.
               // 
               GlobalParmHandler globalParamHandler = (GlobalParmHandler)CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
               if (globalParamHandler != null) {
                  if ("Y".equalsIgnoreCase(globalParamHandler.getFrpGlobalParm(ShipConstants.GLOBAL_PARMS_CONTEXT, "PERSONALIZATION_SORT_ORDER_ENABLED"))) {
                     odliPcItem.setSortOrder(Integer.valueOf(sortOrderStr));  // TODO - This line should remain after global parm code is removed
                     logger.info("PDP SortOrder will be included in CreateOrder call");
                  }
               } 
               // END TEMPORARY GLOBAL PARM LOGIC
            } catch (Exception e) {
               logger.error("PDP key: " + pcValuePair.getKey() + " had invalid SortOrder (so ignored it): " + sortOrderStr);
            }
         }
         odliPcItemLst.getOrderDeliveryLineItemAccessoryPersonalizationItem().add(odliPcItem);
         atLeastOnePcItem = true;
      }
      if (atLeastOnePcItem) {         
         odliAccessoryPersonalization.setPersonalizationItems(odliPcItemLst);
         odliAccessory.setPersonalization(odliAccessoryPersonalization);
         odliAccessory.setAccessoryId(accessoryId); 
      }
      odliAccessoryLst.getOrderDeliveryLineItemAccessory().add(odliAccessory);      
   }
	
	
	/**
	 * Determine if this is a PDP Personal Creations order.
	 * This is the new style (Nov 2017) of PC orders.
	 * 
	 * @param orderDetailsVO
	 * @return
	 */
   private boolean isPersonalCreationPDPOrder(OrderDetailVO orderDetailsVO){
      String pcDataStr = orderDetailsVO.getPersonalizationData();
      return (pcDataStr != null && pcDataStr.length() > 0 && pcDataStr.contains(FTD_WEST_PDP_PERSONALIZATION_PAYLOAD));
   }
	
   /**
    * Determine if this is an old-style (pre-Nov 2017) Personal Creations order.
    * 
    * @param orderDetailsVO
    * @return
    */
	private boolean isPersonalCreationOrder(OrderDetailVO orderDetailsVO){
		return (orderDetailsVO.getPersonalizationData()!=null && orderDetailsVO.getPersonalizationData().length() > 0);
	}
	
	
	/**
	 * Get latest order status from WEST
	 * @param connection - The database connection
	 * @param orderId	 - The ftd west orderId from venus table, to get the status for 
	 * @return	- FtdwestVendorOrderStatusVO
	 * @throws FtdwestCommunicationsException
	 */
	public FtdwestVendorOrderStatusVO getOrder(Connection connection, String orderId) throws FtdwestCommunicationsException {

		try {
			logger.info("getOrder()");

			// 1. initialize ..
			init(connection);

			// 2. Invoke Get order call -
			GetOrderResult getOrderResult = orderServicePort.getOrder(applicationContext, orderId);
			if (getOrderResult == null) {
				throw new Exception("GetOrderResult is returned NULL for OrderId : "+ orderId);
			}
			return mapGetOrderResponseToVO(getOrderResult);
		} catch (Exception e) {
			throw new FtdwestCommunicationsException(
					"Error while retrieving the order status from WEST for OrderId : " + orderId, e);
		}
	}

	/**
	 * Map SkyNet's GETORDERRESULT to FtdwestVendorOrderStatusVO
	 * @param getOrderResult - SkyNET's GetOrder response object
	 * @return - FtdwestVendorOrderStatusVO
	 * @throws Exception
	 */
	private FtdwestVendorOrderStatusVO mapGetOrderResponseToVO( GetOrderResult getOrderResult ) throws Exception {

		FtdwestVendorOrderStatusVO vosVO = new FtdwestVendorOrderStatusVO();

		OrderDeliveryLineItemDetails lineItemDetails = getOrderResult.getOrder()
				.getDeliveries().getOrderDelivery().get(0).getLineItems()
				.getOrderDeliveryLineItem().get(0).getDetails();

		String lineItemStatus = lineItemDetails.getItemStatus();
		String venusOrderNumber = getOrderResult.getOrder().getPONumber();

		MsgType msgType = FtdwestStatusType.getMsgTypeFromWestStatus(lineItemStatus);
		if (msgType == null) {
			throw new Exception("Unknown Status received from WEST : " + lineItemStatus + " for Order : " + venusOrderNumber);
		}

		vosVO.setVenusOrderNumber(venusOrderNumber);
		vosVO.setMsgType(msgType);
		vosVO.setPartnerOrderNumber(new Long(getOrderResult.getOrder().getOrderId())); // TODO - This should be read from getOrderResult.....after got clarified from ProFlowers. Setting as west's orderId temporarily
		vosVO.setPartnerId(getOrderResult.getOrder().getDetails()
				.getPartnerCode());
		vosVO.setTrackingNumber(lineItemDetails.getTrackingNumber());
		vosVO.setCarrier(lineItemDetails.getCarrierName());
		vosVO.setOrderStatus(FtdwestStatusType
				.getFtdStatusForWestStatus(lineItemStatus));
		vosVO.setPrinted(new Date()); // TODO - This should be read from getOrderResult.....after got clarified from ProFlowers
		vosVO.setShipped(getDate(lineItemDetails.getShipDate()));
		vosVO.setDelivered(getDate(getOrderResult.getOrder().getDeliveries()
				.getOrderDelivery().get(0).getDeliveryDate()));
		
		if (MsgType.REJECT.equals(msgType)) {
			vosVO.setRejected(new Date()); 					// TODO - This should be read from getOrderResult.....after got clarified from ProFlowers 
			vosVO.setRejectCode(121L); 						// TODO - This should be read from getOrderResult.....after got clarified from ProFlowers
			vosVO.setRejectMessage("Test Reject Message"); 	// TODO - This should be read from getOrderResult.....after got clarified from ProFlowers
		}
		
		vosVO.setStatus("NEW");
		vosVO.setSendCancelMsgToWest( MsgType.REJECT.equals(msgType) && ! FtdwestStatusType.isCanceled(lineItemStatus) );

		return vosVO;
	}
    
    public static XMLGregorianCalendar getXMLDate(Date date) 
    
    {
    	XMLGregorianCalendar now = null;
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
			now = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.debug("FtdwestCommunications --> getXMLDate() :: Error while converting date to xml date == " + e.getMessage());
		}        
            
        return now;
    }
    
	 
	public static java.util.Date getDate(XMLGregorianCalendar xmlGC) {
		try {
			if (xmlGC != null) {
				return xmlGC.toGregorianCalendar().getTime();
			}
		}
		catch(Exception e) {
			//e.printStackTrace();
			logger.debug("FtdwestCommunications --> getDate() :: Error while converting xml date to date == " + e.getMessage());
		}
		return null;
	}
	
	public FtdwestGetProductDetailsResponseVO getProductDetails(Connection connection, ArrayList<String> pquadIdList) throws FtdwestRuntimeException, FtdwestCommunicationsException {
    	
    	logger.debug("getProductDetails() !!");
	    FtdwestGetProductDetailsResponseVO ftdwestGetProductDetailsResponseVO = new FtdwestGetProductDetailsResponseVO();
    	    	
    	try {
    		    //1. initialize ..
	    		init(connection, "FTDWEST_PRODUCT_SERVICE_URL", FTD_WEST_PRODUCT_DEF_URL);
	    		//2. build create order request..
	    		GetProductDetails request=buildGetProductDetailsRequest(pquadIdList);
	    		ftdwestGetProductDetailsResponseVO.setGetProductDetailsStatus(FtdwestConstants.FTDWEST_GETPRODUCTDETAILS_SUCCESS);
	    		String requestString=FtdwestUtils.getFtdwestString(request);
	    		ftdwestGetProductDetailsResponseVO.setGetProductDetailsRequestMessage(requestString);
	    		logger.debug(" Ftdwest Get Product Details request has been created !! ");
	    		
	    		//3. Get Product Details..
	    		GetProductDetailsResult result= productServicePort.getProductDetails(applicationContext, request.getProductIds());
	    		
	    		ArrayOfProductDetail pquadProducts=result.getProducts();
	    		ftdwestGetProductDetailsResponseVO.setProductDetails(pquadProducts); 
	    			    		
	    		String ftdwestResponseString=FtdwestUtils.getFtdwestString(result);
	    		ftdwestGetProductDetailsResponseVO.setGetProductDetailsResponseMessage(ftdwestResponseString);
	    		logger.debug(" Ftdwest Get Product Details response has been received !! ");
    		
			} catch (ProductServiceGetProductDetailsFaultDetailFaultFaultMessage e) {
				logger.error(" ProductServiceGetProductDetailFaultDetailFaultFaultMessage has occurred.");
				FaultDetail faultDetail=e.getFaultInfo();
				String ftdwestResponseString=FtdwestUtils.getFtdwestString(faultDetail);
	    		ftdwestGetProductDetailsResponseVO.setGetProductDetailsResponseMessage(ftdwestResponseString);
	    		logger.debug(" Ftdwest Get Product Details response has been received !!");	    		
	        	ArrayOfValidationError validationError= faultDetail.getValidationErrors();
	        	
	        	if(validationError !=null){
	        		HashMap<String,String> ValidationErrorMap=new HashMap<String,String>();
		        	for(ValidationError verr:validationError.getValidationError()){
		        		ValidationErrorMap.put(verr.getPropertyName(), verr.getErrorMessage());
		        		logger.debug("Error Code-->"+verr.getErrorCode());	        		
		        		logger.debug("Error msg-->"+verr.getErrorMessage());
		        		logger.debug("Error Property Name-->"+verr.getPropertyName());
		        	}
		        	ftdwestGetProductDetailsResponseVO.setGetProductDetailsStatus("Failiure");
		        	ftdwestGetProductDetailsResponseVO.setErrorMap(ValidationErrorMap);
	        	}else{
	        		throw new FtdwestCommunicationsException("Error Communicating Ftdwest service for Get Product Details !!");
	        	}	        	
	        	
				return ftdwestGetProductDetailsResponseVO;
				
			} catch (FtdwestRuntimeException e) {
				logger.error("Error(FtdwestRuntimeException) getting Ftdwest Product Details !!"+e.getMessage());
				throw e;
			
			}catch(Exception e){
				logger.error("Error(Exception) getting Ftdwest Product Details !!"+e.getMessage());
				throw new FtdwestRuntimeException("Error(Exception) get Ftdwest Product Details !!"+e.getMessage());
			}
    	
    	return ftdwestGetProductDetailsResponseVO;
   
    	
    }
	
	public  GetProductDetails buildGetProductDetailsRequest(ArrayList<String> pquadIdList) throws FtdwestRuntimeException {
		GetProductDetails getProductDetailsRequest = new GetProductDetails();	
    	
    	try {        	
	    	 			
			//Product Ids
    		ArrayOfstring pquadProductIdsArray = new ArrayOfstring();
    		for (int x=0; x<pquadIdList.size(); x++)
    		{
    			pquadProductIdsArray.getString().add(pquadIdList.get(x));
  	    	}
    		getProductDetailsRequest.setProductIds(pquadProductIdsArray);
   	  	   			
    	} catch (Exception e) {
    		e.printStackTrace();
    		logger.error("Error creating Ftdwest Get Product Details Request!! "+e.getMessage());
			throw new FtdwestRuntimeException("Error creating Ftdwest Get Product Details Request Request!! ");
		}
		return getProductDetailsRequest;
	}
	


        
}
