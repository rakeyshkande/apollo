package com.ftd.ship.bo.communications.ftdwest;

/**
 * The purpose of this class is to be able to distinguish the exeption that
 * happens when there is an unexpected/non-complient response received from the  
 * FTD West/Skynet order server.  Properly formatted errors should not be reported with this 
 * exception.
 */
public class FtdwestApplicationException extends Exception {

    public FtdwestApplicationException(Throwable t) {
        super(t);
    }
    public FtdwestApplicationException(String message) {
        super(message);
    }
    public FtdwestApplicationException(String message, Throwable t) {
        super(message,t);
    }
}
