package com.ftd.ship.bo.communications.ftdwest;

public class FtdwestRuntimeException extends RuntimeException {
    public FtdwestRuntimeException(Throwable t) {
        super(t);
    }
    public FtdwestRuntimeException(String message) {
        super(message);
    }
    public FtdwestRuntimeException(String message, Throwable t) {
        super(message,t);
    }
}
