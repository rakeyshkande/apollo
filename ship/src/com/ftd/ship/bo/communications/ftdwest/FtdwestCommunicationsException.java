package com.ftd.ship.bo.communications.ftdwest;

/**
 * The purpose of this class is to be able to distinguish the exeption that
 * happens when there is a communications failure while sharing data with the 
 * FTD West/Skynet order server.  This exception should only be thrown if there is a break down
 * in the communications transport.  Application errors or errors returned from
 * the ship server should not be reported with this exception.  This exception 
 * should be thrown all the way up to the MDB where it can be rescheduled for
 * processing.
 */
public class FtdwestCommunicationsException extends Exception {

    public FtdwestCommunicationsException(Throwable t) {
        super(t);
    }
    public FtdwestCommunicationsException(String message) {
        super(message);
    }
    public FtdwestCommunicationsException(String message, Throwable t) {
        super(message,t);
    }
}
