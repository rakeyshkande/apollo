/**
 * author : vsana
 * An instance of this exception will be created and thrown if a record with improper data is included in the file received from FTDWest.
 * E.g: Invalid Venus order number or invalid vendor code or invalid addon SKU. 
 */
package com.ftd.ship.bo.communications.ftdwest;

public class FtdWestDataException extends Exception {
    public FtdWestDataException(String message) {
        super(message);
    }
}
