package com.ftd.ship.bo;

import java.sql.Connection;
import java.util.StringTokenizer;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.CarrierZipVO;

public class ProcessCarrierZipsBO extends SDSProcessingBO {

	private static final Logger logger = new Logger("com.ftd.ship.bo.ProcessCarrierZipsBO");
	
	public ProcessCarrierZipsBO() {
    }

    public void processCarrierZips(Connection connection, String processMessage) throws SDSApplicationException {
   	
    	CarrierZipVO carrierZipVO = new CarrierZipVO();
    	String userId = "CARRIER_ZIPS";
    	ShipDAO shipDAO = new ShipDAO();        
    	//processMessage = "60515-UPS-WY-SY-AY";
    	//processMessage = "COMPLETE-FEDEX";
    	boolean isInsert = false;
    	boolean isDelete = false;
    	
    	StringTokenizer st = null;
    	int tokenNo = 0;
    	String tokenData = "";
                
                st = new StringTokenizer(processMessage, "-");
               
                while(st.hasMoreTokens())
                {
                	tokenNo++;
                    tokenData = st.nextToken();
                    
                    if(tokenNo == 1){
                    	carrierZipVO.setZipCode(tokenData);
                    	
                    	if(tokenData.equalsIgnoreCase("COMPLETE")){
                    		isDelete = true;
                    	}
                    	else{
                    		isInsert = true;
                    	}
                    
                    	if(tokenData.equalsIgnoreCase("ZIP")){ //to ignore first row for Fedex
                    		isInsert = false;
                    	}
                    }
                    if(tokenNo == 2){
                    	if(tokenData.equalsIgnoreCase("UPS"))
                    		carrierZipVO.setCarrierId(tokenData);
                    	
                    	if(tokenData.equalsIgnoreCase("FEDEX"))		
                    		carrierZipVO.setCarrierId(tokenData);
                    }
                    if(tokenNo == 3){
                    	if(tokenData.equalsIgnoreCase("WY"))
                    		carrierZipVO.setWeekdayByNoonDelFlag("Y");
                    	
                        if(tokenData.equalsIgnoreCase("WN"))
                        	carrierZipVO.setWeekdayByNoonDelFlag("N");
                    }
                    if(tokenNo == 4){
                    	if(tokenData.equalsIgnoreCase("SY"))
                    		carrierZipVO.setSaturdayByNoonDelFlag("Y");
                    	
                        if(tokenData.equalsIgnoreCase("SN"))
                        	carrierZipVO.setSaturdayByNoonDelFlag("N");
                    }
                    if(tokenNo == 5){
                    	if(tokenData.equalsIgnoreCase("AY"))
                    		carrierZipVO.setSaturdayDelAvailable("Y");
                    	
                        if(tokenData.equalsIgnoreCase("AN"))
                        	carrierZipVO.setSaturdayDelAvailable("N");
                    }
                	
                }
                carrierZipVO.setUserId(userId);
                
                try {
                	if(isInsert){
                		//logger.debug("inserting .. ");                		
                		shipDAO.insertUpdateCarrierZips(connection, carrierZipVO);                		
                	}
				} catch (Exception e) {
					String errMsg = "Unable to insert zip code:  "+carrierZipVO.getZipCode();
	                logger.fatal(errMsg,e);
					e.printStackTrace();
					
					try {
		                CommonUtils.getInstance().sendSystemMessage("ProcessCarrierZipsBO", "ProcessCarrierZipsBO failed while inserting zip code - " + e.getMessage());
		            } catch (Exception e1) {
		                errMsg = "Unable to send message to support pager.";
		                logger.fatal(errMsg,e1);
		            }
				}
				
				try {
                	if(isDelete){
                		logger.debug("deleting unprocessed zip codes.. ");
                		shipDAO.deleteCarrierZips(connection, carrierZipVO.getCarrierId());
                		logger.debug("Process COMPLETE !!");
                	}
				} catch (Exception e) {
					String errMsg = "Unable to delete zip codes - "+carrierZipVO.getCarrierId();
	                logger.fatal(errMsg,e);
					e.printStackTrace();
					
					try {
		                CommonUtils.getInstance().sendSystemMessage("ProcessCarrierZipsBO", "ProcessCarrierZipsBO failed while deleting zip code - " + e.getMessage());
		            } catch (Exception e1) {
		                errMsg = "Unable to send message to support pager.";
		                logger.fatal(errMsg,e1);
		            }
				}
    }       
}
