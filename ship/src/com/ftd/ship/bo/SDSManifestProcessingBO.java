package com.ftd.ship.bo;

import java.sql.Connection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.ship.bo.communications.sds.SDSApplicationException;
import com.ftd.ship.common.SDSConstants;
import com.ftd.ship.vo.ManifestVO;
import com.ftd.ship.vo.SDSResponseVO;

public class SDSManifestProcessingBO extends SDSProcessingBO
{
    private static final Logger LOGGER = new Logger("com.ftd.ship.bo.SDSManifestProcessingBO");
    private static final Pattern CODE_PATTERN = Pattern.compile("\\d\\d-\\d\\d\\d\\d[A-Z][A-Z]");
    private static final int VENDOR_CODE_LENGTH = 9;
    private static final Pattern ID_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d");
    private static final int VENDOR_ID_LENGTH = 5;
    private static final String ZONE_JUMP_PATTERN = "ZJ";

    public SDSManifestProcessingBO()
    {
    }

    public void processRequest(Connection connection, String arg) throws Exception
    {
        LOGGER.debug("processRequest -> Entering processRequest");

        //What type of request is this
        String vendorId = null;
        String vendorCode = null;
        String carrierId = null;
        String vendor = null;
        boolean isZoneJump = false;
        String daysInTransitQty = null;
        try
        {
            arg = StringUtils.upperCase(StringUtils.trimToEmpty(arg));

            if (StringUtils.isBlank(arg))
            {
                vendorId = null;
                vendorCode = null;
                carrierId = null;
            }
            else
            {
                //If this is for ZoneJump then argument string will end in ZoneJump characters
                if ((arg.substring(0, (arg.length() - 1))).endsWith(ZONE_JUMP_PATTERN))
                {
                    daysInTransitQty = arg.substring(arg.length() - 1, arg.length());
                    arg = arg.substring(0, (arg.length() - 1));
                    arg = StringUtils.trimToEmpty(arg.substring(0, (arg.length() - ZONE_JUMP_PATTERN.length())));
                    isZoneJump = true;
                    LOGGER.info("processRequest -> Manifest processing for ZoneJump");
                }

                //Test to see if you got an id
                String testStr = StringUtils.substring(arg, 0, VENDOR_ID_LENGTH);
                Matcher m = ID_PATTERN.matcher(testStr);

                if (m.matches())
                {
                    vendorId = testStr;
                    vendorCode = null;
                }
                else
                {
                    //Test for a vendor (florist) code
                    testStr = StringUtils.substring(arg, 0, VENDOR_CODE_LENGTH);
                    m = CODE_PATTERN.matcher(testStr);

                    if (m.matches())
                    {
                        vendorId = null;
                        vendorCode = testStr;
                    }
                    else
                    {
                        //It's not a vendor id or a vendor code so assume that it's a carrier id
                        carrierId = arg;
                        vendorCode = null;
                        vendorId = null;
                    }
                }

                //Check to see if we got a vendor code/id and a carrier id
                if (vendorCode != null && arg.length() > VENDOR_CODE_LENGTH)
                {
                    carrierId = arg.substring(VENDOR_CODE_LENGTH);
                }
                else if (vendorId != null && arg.length() > VENDOR_ID_LENGTH)
                {
                    carrierId = arg.substring(VENDOR_ID_LENGTH);
                }
            }
        }
        catch (Throwable t)
        {
            if (isZoneJump)
            {
                //Go no further if ZoneJump
                throw new SDSApplicationException("processRequest -> Error during ZoneJump Manifest processing: " + t);
            }
            LOGGER.error("processRequest -> Error while parsing passed arguement of " + arg + ".  Will process with default values.", t);
            vendorId = null;
            vendorCode = null;
            carrierId = null;
        }

        //ZoneJump only allowed for carrierId
        if (isZoneJump && (carrierId == null || vendorId != null || vendorCode != null))
        {
            throw new SDSApplicationException("processRequest -> Invalid Manifest arguments for ZoneJump: " + arg);
        }

        if (vendorId != null)
        {
            vendor = vendorId;
        }
        else if (vendorCode != null)
        {
            vendor = vendorCode;
        }

        try
        {
            if (LOGGER.isDebugEnabled())
            {
                StringBuilder sb = new StringBuilder();
                sb.append("Vendor: ");
                sb.append(vendor);
                sb.append(" and Carrier: ");
                sb.append(carrierId);
                sb.append(" and ZoneJump flag: ");
                sb.append(isZoneJump ? "Y" : "N");
                sb.append(" and daysInTransit: ");
                sb.append(daysInTransitQty);
                LOGGER.debug("processRequest -> " + sb.toString());
            }

            List<ManifestVO> manifestList = shipDAO.getVendorsToClose(connection, vendor, carrierId, isZoneJump,
                    daysInTransitQty);
            StringBuffer errString = new StringBuffer();

            LOGGER.debug("processRequest -> Manifest query returned : " + manifestList.size() + " records.");

            for (int idx = 0; idx < manifestList.size(); idx++)
            {

                ManifestVO manifestVO = manifestList.get(idx);
                Document doc;
                SDSResponseVO responseVO = null;

                try
                {

                    if (isZoneJump)
                    {
                        LOGGER.debug("processRequest -> Close Request for ZoneJump.  Vendor = " + manifestVO.getVendorCode()
                                + " and trailer = " + manifestVO.getZoneJumpTrailerNumber());
                        doc = sdsCommunications.manifestTrailer(connection, manifestVO.getZoneJumpTrailerNumber());
                        LOGGER.debug("processRequest -> Close Response for ZoneJump received from ScanData is : "
                                + JAXPUtil.toString(doc));
                    }
                    else
                    {
                        LOGGER.debug("processRequest -> Close Request for non-ZoneJump.  Vendor = " + manifestVO.getVendorCode()
                                + " and carrier = " + manifestVO.getCarrierId());
                        doc = sdsCommunications.manifestCarrier(connection, manifestVO.getCarrierId(),
                                manifestVO.getVendorCode());
                        LOGGER.debug("processRequest -> Close Response received from ScanData is : " + JAXPUtil.toString(doc));
                    }

                    responseVO = parseResponse(doc);

                    LOGGER.debug("processRequest -> ResponseVO is : " + responseVO.toString());
                    LOGGER.debug("processRequest -> ResponseVO - status code is : " + responseVO.getStatusCode());

                    if (responseVO.isSuccess())
                    {
                        if (isZoneJump)
                            shipDAO.updateTripStatus(connection, manifestVO.getZoneJumpTrailerNumber(),
                                    SDSConstants.ZJ_STATUS_TRIP_CLOSED);

                        LOGGER.info("processRequest -> Successfully closed " + manifestVO.getCarrierId() + " for "
                                + manifestVO.getVendorCode() + " and trailer = "
                                + manifestVO.getZoneJumpTrailerNumber());

                    }
                    else
                    {
                        errString.append("Failed to close ");
                        errString.append(manifestVO.getCarrierId());
                        errString.append("\r\n");
                        errString.append("Error code: ");
                        errString.append(responseVO.getStatusCode());
                        errString.append("\r\n");
                        errString.append("Error message: ");
                        errString.append(responseVO.getMsgText());
                        errString.append("\r\n");
                        LOGGER.info("processRequest -> Failed to close - SDSApplicationException will be thrown after processing remaining vendors.  Error message was: "
                                + responseVO.getMsgText());
                    }
                }

                catch (Throwable t)
                {

                    errString.append("Caught an exception ");
                    errString.append("\r\n");
                    errString.append(t.getMessage());
                    errString.append("Manifest VO Carrier ID:");
                    errString.append("\r\n");
                    errString.append(manifestVO.getCarrierId());
                    errString.append("\r\n");
                    if (responseVO != null)
                    {
                        errString.append("Response VO Status code: ");
                        errString.append(responseVO.getStatusCode());
                        errString.append("\r\n");
                        errString.append("Response VO Error message: ");
                        errString.append(responseVO.getMsgText());
                        errString.append("\r\n");
                    }
                    LOGGER.error("processRequest -> Error while processing manifest: " + idx + "\n");
                }

            }

            if (errString.length() > 0)
            {
                throw new SDSApplicationException("processRequest -> Errors found while processing SDS close:\r\n"
                        + errString.toString().trim());
            }

        }
        finally
        {
            LOGGER.debug("processRequest -> Leaving processRequest");
        }
    }

    public SDSResponseVO parseResponse(Document responseDoc) throws Exception
    {
        SDSResponseVO responseVO = new SDSResponseVO();

        String errorNumber = null;
        String errorDescription = null;
        try
        {

            NodeList nlErrorMain = JAXPUtil.selectNodes(responseDoc, SDSConstants.XPATH_MANIFEST_ERROR,
                    SDSConstants.NAMESPACE_PREFIX, SDSConstants.NAMESPACE_URI);

            LOGGER.debug("parseResponse -> nlErrorMain.getLength() = " + nlErrorMain.getLength());

            if (nlErrorMain.getLength() > 0)
            {
                Element errorElementMain = (Element) nlErrorMain.item(0);
                errorNumber = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_NUMBER);
                errorDescription = getFirstChildNoNull(errorElementMain, SDSConstants.TAG_ERROR_DESCRIPTION);
            }

            LOGGER.debug("parseResponse -> errorNumber = " + errorNumber);
            LOGGER.debug("parseResponse -> errorDescription = " + errorDescription);
        }
        catch (Exception e)
        {
            LOGGER.info("parseResponse -> Failed to retrieve error code and description in manifest response.", e);
        }

        responseVO.setStatusCode(errorNumber);
        responseVO.setMsgText(errorDescription);

        if (StringUtils.isBlank(errorNumber))
        {
            responseVO.setSuccess(true);
        }
        else
        {
            responseVO.setSuccess(false);
        }

        return responseVO;
    }

    private String getFirstChildNoNull(Element elem, String tagName) throws Exception
    {
        String retStr = null;
        NodeList nl = elem.getElementsByTagName(tagName);
        if (nl != null)
        {
            Element firstElem = (Element) nl.item(0);
            if (firstElem != null)
            {
                Text firstChild = (Text) firstElem.getFirstChild();
                if (firstChild != null)
                {
                    retStr = firstChild.getNodeValue();
                }
            }
        }
        return retStr;
    }

}
