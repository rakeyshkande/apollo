package com.ftd.ship.bo;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.VendorProductAvailabilityUtility;
import com.ftd.osp.utilities.constants.VendorProductAvailabilityConstants;
import com.ftd.osp.utilities.feed.NovatorFeedProductUtil;
import com.ftd.osp.utilities.feed.NovatorFeedUtil;
import com.ftd.osp.utilities.feed.vo.NovatorFeedResponseVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.stats.ServiceResponseTrackingUtil;
import com.ftd.osp.utilities.vo.ServiceResponseTrackingVO;
import com.ftd.ship.bo.communications.ftdwest.FtdwestApplicationException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunications;
import com.ftd.ship.bo.communications.ftdwest.FtdwestCommunicationsException;
import com.ftd.ship.bo.communications.ftdwest.FtdwestRuntimeException;
import com.ftd.ship.common.FtdwestConstants;
import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.common.framework.util.CommonUtils;
import com.ftd.ship.dao.ShipDAO;
import com.ftd.ship.vo.FtdwestGetProductDetailsResponseVO;
import com.ftd.ship.vo.FtdwestGetProductDetailsVO;
import com.ftd.ship.vo.WestProductUpdateVO;
import com.providecommerce.api.api.product.v1.ArrayOfProductDetail;
import com.providecommerce.api.api.product.v1.ProductDetail;

public class FtdWestProductUpdatesBO {
	private static final String GET_PRODUCT_DETAILS_STR = "GetProductDetails";
	private static final String FTDWEST_SKYNET_SERVICE = "Ftdwest Skynet Product Service";
	private static Logger logger = new Logger("com.ftd.ship.bo.FtdWestProductUpdatesBO");
	protected static final String EMAIL_FORMAT = "EEE, d MMMMM, yyyy";
	private FtdwestCommunications ftdwestCommunications;
	private ShipDAO shipDAO;
	private int start;
	private int end;
	private int totalNumberProducts;
	
	public FtdwestGetProductDetailsResponseVO checkWestProductAvailability(Connection connection) throws FtdwestApplicationException, FtdwestCommunicationsException, FtdwestRuntimeException, Exception{
		FtdwestGetProductDetailsResponseVO ftdwestGetProductDetailsResponseVO=null;
		try {
			logger.info("FTDWest Check West Product Availability started");
						
			long startTotalTime = (new Date()).getTime();
			ArrayList<WestProductUpdateVO> allWestProductUpdates = new ArrayList<WestProductUpdateVO>();
				
			//Get list of unique available pquad ids
			ArrayList<String> pqaudIdlist = new ArrayList<String>();
			pqaudIdlist = shipDAO.getFtdwestProductList(connection);
					
			//Retrieve FTDWEST_UPDATE_PRODUCT Global Parms
			String westProductMaxBatchSize = shipDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_MAX_BATCH_SIZE");
			logger.info("westProductMaxBatchSize: " + westProductMaxBatchSize);
			String westProductUpdateBatchDelay = shipDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_BATCH_DELAY");
			logger.info("westProductUpdateBatchDelay: " + westProductUpdateBatchDelay);
			
			String addProcessDelay = shipDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "PROCESS_DELAY");
			
			if(addProcessDelay.equalsIgnoreCase("Y")){
				//Add a delay to avoid numerous jobs processing at same time
				int processDelay = 1 + (int) (Math.random() * (((100000 - 10000) / 5) + 1));
				Thread.currentThread().sleep(Long.valueOf(processDelay));
			}
			
			String westProductUpdateJobRunning = shipDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING");
			logger.info("westProductUpdateJobRunning: " + westProductUpdateJobRunning);
			
            if( westProductUpdateJobRunning != null && westProductUpdateJobRunning.equalsIgnoreCase("N") ){
	            	//Set FTDWEST_UPDATE_PRODUCT_JOB_RUNNING to Y so that it will not be triggered again.
            	    shipDAO.setGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING", "Y", "SHIP");
            	    
            	    //Break into batches of max batch size and call west
            	    logger.info("Number of Available Ftd West Products: " + pqaudIdlist.size());
            	    this.totalNumberProducts = pqaudIdlist.size();
            	    int count = pqaudIdlist.size()/Integer.parseInt(westProductMaxBatchSize);
           	        logger.info("Number of batches that contain the FTDWEST_UPDATE_PRODUCT_MAX_BATCH_SIZE: " + count);
           	     
	           	    int remainder = pqaudIdlist.size()%Integer.parseInt(westProductMaxBatchSize);
	           	    logger.info("Remainder of products.  If remainder > 0, then one more batch will be sent with the remaining " + remainder + " products.");   
           	     
	           	    int counter=0;
	           		int batchSize = Integer.parseInt(westProductMaxBatchSize);
	           	    this.start = 0;
	           	    this.end = batchSize;
	           	    
		           	for(counter = this.start ; counter < count ; counter ++)
		            {
		           		allWestProductUpdates = checkProductAvailability(ftdwestGetProductDetailsResponseVO, pqaudIdlist, allWestProductUpdates, batchSize, westProductUpdateBatchDelay, false, connection);
		           	}
	
				    if(remainder!=0){
					     if(counter == 0){
					    	 this.end = remainder; 
					     }
					     else{
					    	 this.end = end + remainder;
					     }
					    				
					     allWestProductUpdates = checkProductAvailability(ftdwestGetProductDetailsResponseVO, pqaudIdlist, allWestProductUpdates, batchSize, westProductUpdateBatchDelay, true, connection);
					   
			     	}//end remainder check
				    //If any product status changed, insert a record into the ftd_apps.west_product_update table
					if( allWestProductUpdates!=null && allWestProductUpdates.size()>0 )
			        {
			        	   WestProductUpdateVO westProductUpdateVO;
			               Iterator<WestProductUpdateVO> wpuit = allWestProductUpdates.iterator();
			               boolean done = false;
			               
			               while( wpuit.hasNext() )
			               {
			            	   westProductUpdateVO = (WestProductUpdateVO)wpuit.next();
			            	   shipDAO.insertUpdateWestProductUpdate(connection, westProductUpdateVO);
				               //Feed products to the website
			            	   sendProductFeed(connection, westProductUpdateVO.getProductId());
			               }
			               //Email merchops list of products updated
			               shipDAO.sendWestProductUpdates(connection);
			        }
				    				     
		            //Setting the FTDWEST_UPDATE_PRODUCT_JOB_RUNNING to N so that the process can be triggered again. 
		            shipDAO.setGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING", "N", "SHIP");
		            
		            long totalTime = System.currentTimeMillis() - startTotalTime;
		            logger.info("FTDWest Update Product completed in (ms): " + totalTime);
		    } 
            else{
            	logger.info("FTDWEST_UPDATE_PRODUCT_JOB_RUNNING = Y.  Process is currently running, so skipping processing.");	
            }
		} catch (FtdwestApplicationException e) {
			logger.error("Error (FtdwestApplicationException) processing Ftdwest Product Updates "+e.getMessage());
			//Setting the FTDWEST_UPDATE_PRODUCT_JOB_RUNNING to N so that the process can be triggered again. 
            shipDAO.setGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING", "N", "SHIP");
            throw e;
		} catch (FtdwestCommunicationsException e) {
			logger.error("Error (FtdwestCommunicationsException) processing Ftdwest Product Updates "+e.getMessage());
			//Setting the FTDWEST_UPDATE_PRODUCT_JOB_RUNNING to N so that the process can be triggered again. 
            shipDAO.setGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING", "N", "SHIP");
			throw e;
		} catch (Exception e) {
			logger.error("Error (Exception) processing Ftdwest Product Updates "+e.getMessage());
			//Setting the FTDWEST_UPDATE_PRODUCT_JOB_RUNNING to N so that the process can be triggered again. 
            shipDAO.setGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "FTDWEST_UPDATE_PRODUCT_JOB_RUNNING", "N", "SHIP");
			throw new FtdwestRuntimeException(e.getMessage());
			
		}
		return ftdwestGetProductDetailsResponseVO;
		
	}	

	public FtdwestCommunications getFtdwestCommunications() {
		return ftdwestCommunications;
	}

	public void setFtdwestCommunications(FtdwestCommunications ftdwestCommunications) {
		this.ftdwestCommunications = ftdwestCommunications;
	}
	
    private void logServiceResponseTracking(String requestId,String serviceMethod, long startTime, Connection connection, String request, String response) {
			try {
				long responseTime = System.currentTimeMillis() - startTime;
				ServiceResponseTrackingVO srtVO = new ServiceResponseTrackingVO();
				srtVO.setServiceName(FTDWEST_SKYNET_SERVICE);
				srtVO.setServiceMethod(serviceMethod);
				srtVO.setTransactionId(requestId);
				srtVO.setResponseTime(responseTime);
				srtVO.setCreatedOn(new Date());
				String saveRequestResponse = 
					shipDAO.getGlobalParameter(connection, ShipConstants.GLOBAL_PARMS_CONTEXT, "SAVE_REQUEST_RESPONSE");
				
				if(saveRequestResponse.equalsIgnoreCase("Y")){
					srtVO.setRequest(request);
					srtVO.setResponse(response);
				}
				ServiceResponseTrackingUtil srtUtil = new ServiceResponseTrackingUtil();
				srtUtil.insertWithRequest(connection, srtVO);
			} catch (Exception e) {
				logger.error("Exception occured while persisting Ftdwest GetProductDetails response times.");
			}
			
		}
	  
	public void setShipDAO(ShipDAO shipDAO) {
		this.shipDAO = shipDAO;
	}
	
	  
	public void sendProductFeed(Connection con, String productId)
	    throws Exception
	{
		String errorMessage = "Product id: " + productId + ". "; 
		NovatorFeedResponseVO resVO;
		NovatorFeedProductUtil feedUtil = new NovatorFeedProductUtil();
		
		
		//build the environment array for product feed utility
        NovatorFeedUtil novatorFeedUtil = null;                            
        novatorFeedUtil = new NovatorFeedUtil();
        List <String>envKeys = novatorFeedUtil.getNovatorEnvironmentsAllowedAndChecked();  
		try
		{
			    //call feed utility
			 if (envKeys.size() > 0)
			 {
				resVO = feedUtil.sendProductFeed(con, productId, envKeys);
			    errorMessage += resVO.getErrorString() + "\n";
			  			   
			    if (!resVO.isSuccess())
			      throw new Exception(); 
			    }
     		 }
			 catch (Exception e)
			 {
		
			    CommonUtils.getInstance().sendSystemMessage(errorMessage);
			 }
 
 	    }
	
	public ArrayList<WestProductUpdateVO> checkProductAvailability(FtdwestGetProductDetailsResponseVO ftdwestGetProductDetailsResponseVO, ArrayList<String> pqaudIdlist, 
			ArrayList<WestProductUpdateVO> allWestProductUpdates, int batchSize, String westProductUpdateBatchDelay, boolean finalBatch, Connection connection) throws Exception{
		 
  		 ArrayList<String> al = new ArrayList<String>(pqaudIdlist.subList(start, end));
		 //Start timer for service call
		 long startTime = (new Date()).getTime();
		 //Call FTD West getProductDetails
		 ftdwestGetProductDetailsResponseVO=ftdwestCommunications.getProductDetails(connection,al);
		 //stop timer for service call and log (need unique id to store)
		 if(FtdwestConstants.FTDWEST_GETPRODUCTDETAILS_SUCCESS.equalsIgnoreCase(ftdwestGetProductDetailsResponseVO.getGetProductDetailsStatus())){
			 logServiceResponseTracking(al.get(0), GET_PRODUCT_DETAILS_STR,startTime, connection, ftdwestGetProductDetailsResponseVO.getGetProductDetailsRequestMessage(), ftdwestGetProductDetailsResponseVO.getGetProductDetailsResponseMessage());
    		 //parse response
			 ArrayOfProductDetail pquadProducts = ftdwestGetProductDetailsResponseVO.getProductDetails();
			 List<ProductDetail> productDetail = pquadProducts.getProductDetail();
	    	 for( int j=0; j<productDetail.size(); j++){
	    		 ftdwestGetProductDetailsResponseVO.setGetProductDetailsStatus(FtdwestConstants.FTDWEST_GETPRODUCTDETAILS_SUCCESS);
				 FtdwestGetProductDetailsVO ftdwestGetProductDetailsVO = new FtdwestGetProductDetailsVO();
		    	 ftdwestGetProductDetailsVO.setPquadProductId(productDetail.get(j).getProductId());	
		    	 ftdwestGetProductDetailsVO.setActive(productDetail.get(j).isIsActive());
		    	 //look up all novator ids, product type where pquad id = pdb pqaud id if isIsActive if false
		    	 if(!ftdwestGetProductDetailsVO.isActive()){
		    		 //retrieve pquad product details
		             List productList = shipDAO.getPquadProductDetails(connection, ftdwestGetProductDetailsVO.getPquadProductId());
		             if (productList != null)
		             {
		            	 //for each product in the list
		                 Iterator iter = productList.iterator();
		                 while (iter.hasNext())
		                 {
		                	   WestProductUpdateVO westProductUpdateVO = (WestProductUpdateVO) iter.next();
				               //check if product type = SDFC
				     	       //if not, update status to Unavailable (make sure product feed/pas/store feed is triggered)
				     	       //if Product type = SDFC and ship_method_florist = 'Y', change ship_method_carrier to 'N' (make sure product feed/pas is triggered)
		                	   logger.info("westProductUpdateVO.getNovatorId(): " + westProductUpdateVO.getNovatorId());
		                	   logger.info("westProductUpdateVO.getPquadId(): " + westProductUpdateVO.getPquadId());
		                	   logger.info("westProductUpdateVO.getShipMethodFlorist(): " + westProductUpdateVO.getShipMethodFlorist());
		                	   logger.info("westProductUpdateVO.getShipMethodCarrier(): " + westProductUpdateVO.getShipMethodCarrier());
		                	   logger.info("westProductUpdateVO.getProductType(): " + westProductUpdateVO.getProductType());
		                	   logger.info("westProductUpdateVO.getStatus(): " + westProductUpdateVO.getStatus());
		                	   if(westProductUpdateVO.getShipMethodFlorist().equalsIgnoreCase("Y") && westProductUpdateVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {	
			           				if(GeneralConstants.OE_PRODUCT_TYPE_SDFC.equals(westProductUpdateVO.getProductType()) || 
			           						GeneralConstants.OE_PRODUCT_TYPE_SDG.equals(westProductUpdateVO.getProductType())) {
			           					logger.info("Product type: " + westProductUpdateVO.getProductType() +", performing Dropship shutdown on product: " + westProductUpdateVO.getNovatorId());
			           					VendorProductAvailabilityUtility.updateProdDropshipDelivery(westProductUpdateVO.getProductId(), VendorProductAvailabilityConstants.INACTIVE, "FtdWestProductUpdates", connection);
			           					//keep track of novator ids/pquad ids that have been updated
			           					allWestProductUpdates.add(westProductUpdateVO);
			           				} else {
			           					logger.info("Product type: " + westProductUpdateVO.getProductType() +", Dropship shutdown is not performed for " + westProductUpdateVO.getNovatorId());
			           				}
			           			} else if(westProductUpdateVO.getStatus().equalsIgnoreCase("A") && !westProductUpdateVO.getShipMethodFlorist().equalsIgnoreCase("Y") && westProductUpdateVO.getShipMethodCarrier().equalsIgnoreCase("Y")) {
			           				logger.info("Product is vendor deliverable, performing product shutdown for product: " + westProductUpdateVO.getNovatorId());
			           				VendorProductAvailabilityUtility.updateProductStatus(westProductUpdateVO.getProductId(), VendorProductAvailabilityConstants.UNAVAILABLE, "FtdWestProductUpdates", connection);
			           			    //keep track of novator ids/pquad ids that have been updated
		           					allWestProductUpdates.add(westProductUpdateVO);
			           			}
				     	       
		                 }
		             }
		    	 }
	    	 }

	    	
		 }//end successful batch processing
		 else{
			 //send system message with errors, remove products in error from list and resubmit batch
			 logServiceResponseTracking(al.get(0), GET_PRODUCT_DETAILS_STR, startTime, connection, ftdwestGetProductDetailsResponseVO.getGetProductDetailsRequestMessage(), ftdwestGetProductDetailsResponseVO.getGetProductDetailsResponseMessage());
			 StringBuilder sb = new StringBuilder();
	   		 sb = new StringBuilder();
	       	 sb.append("Error encountered when sending a batch of products to FTD West to check product availability. Error received from Ftd West: ");
	   		 HashMap errorMsgMap=ftdwestGetProductDetailsResponseVO.getErrorMap();
	   		 Set entrySet = errorMsgMap.entrySet();			        		
	   		 if(errorMsgMap !=null && ! errorMsgMap.isEmpty()) {
	   			Iterator mapIter= entrySet.iterator();
	       		while(mapIter.hasNext()){
	                    Map.Entry me = (Map.Entry)mapIter.next();
	                    String index = me.getKey().toString();
	                    if( me.getKey().toString().contains("productIds[") && me.getKey().toString().contains("]") )
	                    { 
	                    	 index = index.substring(index.indexOf("[")+1,index.indexOf("]"));
	                    	 sb.append(" "+me.getValue() + " " + al.get(Integer.parseInt(index)));
		        		 }
	                    else
	                    {
	                   	 sb.append(" "+me.getValue() + " " + me.getKey());
	                    }
	       		}
	   		 }
	   		 if(! sb.toString().endsWith(".")){
	   			sb.append(".");
	   		 }
	   		 CommonUtils.getInstance().sendSystemMessage(sb.toString());
		 }
		 if(!finalBatch){
	    	    this.start = this.start + batchSize;
	    	    int tmpEnd = this.end + batchSize;
	    	    if(tmpEnd <= this.totalNumberProducts){
		            this.end = this.end + batchSize;
	    	    }
	            logger.info("Pausing processing based off of westProductUpdateBatchDelay: " + westProductUpdateBatchDelay);
	            Thread.currentThread().sleep(Long.valueOf(westProductUpdateBatchDelay));
	            logger.info("Processing continuing...");
	     }
		 return allWestProductUpdates;
	}


}
