package com.ftd.ship.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.ship.common.ShipConstants;
import com.ftd.ship.vo.CommentsVO;

import com.ftd.ship.vo.CompanyVO;
import com.ftd.ship.vo.CustomerPhoneVO;
import com.ftd.ship.vo.CustomerVO;
import com.ftd.ship.vo.EmailVO;
import com.ftd.ship.vo.FtdwestPersonalizationVO;
import com.ftd.ship.vo.GlobalParameterVO;
import com.ftd.ship.vo.OrderDetailVO;

import com.ftd.ship.vo.OrderTrackingVO;
import com.ftd.ship.vo.OrderVO;

import com.ftd.ship.vo.ProductVO;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class OrderDAO {
    private static Logger logger  = new Logger("com.ftd.ship.dao.OrderDAO");

    public OrderDAO() {
    }

    /**
   * This is a wrapper for CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS.
   * @throws java.lang.Exception
   * @param commentsVO
   */
    public void insertComment(Connection conn, CommentsVO commentsVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CUSTOMER_ID", commentsVO.getCustomerId());
        inputParams.put("IN_ORDER_GUID", commentsVO.getOrderGuid());
        inputParams.put("IN_ORDER_DETAIL_ID", commentsVO.getOrderDetailId());
        inputParams.put("IN_COMMENT_ORIGIN", commentsVO.getCommentOrigin());
        inputParams.put("IN_REASON", commentsVO.getReason());
        inputParams.put("IN_DNIS_ID", commentsVO.getDnisId());
        inputParams.put("IN_COMMENT_TEXT", commentsVO.getComment());
        inputParams.put("IN_COMMENT_TYPE", commentsVO.getCommentType());
        inputParams.put("IN_CSR_ID", commentsVO.getCreatedBy());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("INSERT_COMMENTS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message =
                (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the GET_ORDER_DETAILS SP.
   * It populates an Order Detail VO based on the returned record set.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param orderDetailID to retrieve
   * @return OrderDetailVO
   */
    public OrderDetailVO getOrderDetail(Connection conn, String orderDetailID) throws Exception {
        DataRequest dataRequest = new DataRequest();
        OrderDetailVO orderDetail = new OrderDetailVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);

        try {
            logger.debug("getOrderDetail (String orderDetailID(" +
                         orderDetailID + ")) :: OrderDetailVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(ShipConstants.ORDER_DETAIL_ID,
                            new Long(orderDetailID));

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_ORDER_DETAILS");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                orderDetail.setOrderDetailId(outputs.getLong(ShipConstants.ORDER_DETAIL_ID));
                orderDetail.setDeliveryDate(outputs.getString(ShipConstants.DELIVERY_DATE) ==
                                            null ? null :
                                            df.parse(outputs.getString(ShipConstants.DELIVERY_DATE)));
                orderDetail.setRecipientId(outputs.getLong(ShipConstants.RECIPIENT_ID));
                orderDetail.setProductId(outputs.getString(ShipConstants.PRODUCT_ID));
                orderDetail.setQuantity(outputs.getLong(ShipConstants.QUANTITY));
                orderDetail.setExternalOrderNumber(outputs.getString("EXTERNAL_ORDER_NUMBER"));
                orderDetail.setColor1(outputs.getString(ShipConstants.COLOR_1));
                orderDetail.setColor2(outputs.getString(ShipConstants.COLOR_2));
                orderDetail.setSubstitutionIndicator(outputs.getString(ShipConstants.SUBSTITUTION_INDICATOR));
                orderDetail.setSameDayGift(outputs.getString(ShipConstants.SAME_DAY_GIFT));
                orderDetail.setOccasion(outputs.getString(ShipConstants.OCCASION));
                orderDetail.setCardMessage(outputs.getString(ShipConstants.CARD_MESSAGE));
                orderDetail.setCardSignature(outputs.getString(ShipConstants.CARD_SIGNATURE));
                orderDetail.setSpecialInstructions(outputs.getString(ShipConstants.SPECIAL_INSTRUCTIONS));
                orderDetail.setReleaseInfoIndicator(outputs.getString(ShipConstants.RELEASE_INFO_INDICATOR));
                orderDetail.setFloristId(outputs.getString(ShipConstants.FLORIST_ID));
                orderDetail.setShipMethod(outputs.getString(ShipConstants.SHIP_METHOD));
                orderDetail.setShipDate(outputs.getDate(ShipConstants.SHIP_DATE));
                orderDetail.setOrderDispCode(outputs.getString(ShipConstants.ORDER_DISP_CODE));
                orderDetail.setDeliveryDateRangeEnd(outputs.getString(ShipConstants.DELIVERY_DATE_RANGE_END) ==
                                                    null ? null :
                                                    df.parse(outputs.getString(ShipConstants.DELIVERY_DATE_RANGE_END)));
                orderDetail.setScrubbedOn(outputs.getDate(ShipConstants.SCRUBBED_ON_DATE));
                orderDetail.setScrubbedBy(outputs.getString(ShipConstants.USER_ID));
                orderDetail.setOrderGuid(outputs.getString(ShipConstants.ORDER_GUID));
                orderDetail.setSourceCode(outputs.getString(ShipConstants.SOURCE_CODE));
                orderDetail.setSecondChoiceProduct(outputs.getString("SECOND_CHOICE_PRODUCT"));
                orderDetail.setRejectRetryCount(outputs.getLong("REJECT_RETRY_COUNT"));
                orderDetail.setSizeIndicator(outputs.getString("SIZE_INDICATOR"));
                orderDetail.setSubcode(outputs.getString("SUBCODE"));
                orderDetail.setOpStatus(outputs.getString("OP_STATUS"));
                orderDetail.setCarrierDelivery(outputs.getString("CARRIER_DELIVERY"));
                orderDetail.setCarrierId(outputs.getString("CARRIER_ID"));
                orderDetail.setVenusMethodOfPayment(outputs.getString("METHOD_OF_PAYMENT"));
                orderDetail.setOrderHasMorningDelivery(outputs.getString(ShipConstants.ORDER_HAS_MORNING_DELIVERY));
                //orderDetail.setPersonalizationData(outputs.getString("PERSONALIZATION_DATA"));
                Clob pdataTemp = outputs.getClob("PERSONALIZATION_DATA");
                if(pdataTemp != null) {
                    orderDetail.setPersonalizationData(pdataTemp.getSubString((long)1, (int) pdataTemp.length()));
                }
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return orderDetail;
    }


    /**
   * This method is a wrapper for the SP_GET_ORDER SP.
   * It populates a Order VO based on the returned record set.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param orderGuid filter
   * @return OrderVO
   */
    public OrderVO getOrder(Connection conn, String orderGuid) throws Exception {
        DataRequest dataRequest = new DataRequest();
        OrderVO order = new OrderVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        try {
            logger.debug("getOrder (String orderGuid) :: OrderVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(ShipConstants.ORDER_GUID, orderGuid);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_ORDER_BY_GUID");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                order.setMasterOrderNumber(outputs.getString(ShipConstants.MASTER_ORDER_NUMBER));
                order.setCustomerId(outputs.getLong(ShipConstants.CUSTOMER_ID));
                order.setMembershipId(outputs.getLong(ShipConstants.MEMBERSHIP_ID));
                order.setCompanyId(outputs.getString(ShipConstants.COMPANY_ID));
                order.setSourceCode(outputs.getString(ShipConstants.SOURCE_CODE));
                order.setOriginId(outputs.getString(ShipConstants.ORIGIN_ID));
                order.setOrderDate(outputs.getDate(ShipConstants.ORDER_DATE));
                order.setOrderTotal(outputs.getDouble(ShipConstants.ORDER_TOTAL));
                order.setProductTotal(outputs.getDouble(ShipConstants.PRODUCT_TOTAL));
                order.setAddOnTotal(outputs.getDouble(ShipConstants.ADD_ON_TOTAL));
                order.setServiceFeeTotal(outputs.getDouble(ShipConstants.SERVICE_FEE_TOTAL));
                order.setShippingFeeTotal(outputs.getDouble(ShipConstants.SHIPPING_FEE_TOTAL));
                order.setDiscountTotal(outputs.getDouble(ShipConstants.DISCOUNT_TOTAL));
                order.setTaxTotal(outputs.getDouble(ShipConstants.TAX_TOTAL));
                order.setLossPreventionIndicator(outputs.getString(ShipConstants.LOSS_PREVENTION_INDICATOR));
                order.setFraudFlag(outputs.getString("FRAUD_INDICATOR"));
                order.setLanguageId(outputs.getString("LANGUAGE_ID"));
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return order;
    }

    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_DISPOSITION.
   * @throws java.lang.Exception
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param updatedBy user
   * @param newDisposition to set
   * @param orderDetailId record id
   */
    public void updateOrderDisposition(Connection conn,
                                       long orderDetailId,
                                       String newDisposition,
                                       String updatedBy) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        logger.debug("updateOrderDisposition(long orderDetailId (" +
                     orderDetailId +
                     "), String newDisposition, String updatedBy)");

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        inputParams.put("IN_DISPOSITION", newDisposition);
        inputParams.put("IN_UPDATED_BY", updatedBy);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_ORDER_DISPOSITION");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message =
                (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the SP_UPDATE_ORDER_DETAIL SP.
   * It updates an order detail record to match the passed in VO.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param orderDtl record to update
   * @return none
   */
    public void updateOrder(Connection conn, OrderDetailVO orderDtl) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.debug("updateOrder (OrderDetailVO orderDtl (" +
                     orderDtl.getOrderDetailId() + ")) :: void" + orderDtl);
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(ShipConstants.ORDER_DETAIL_ID,
                        new Long(orderDtl.getOrderDetailId()));
        inputParams.put(ShipConstants.DELIVERY_DATE,
                        orderDtl.getDeliveryDate() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDate().getTime()));
        inputParams.put(ShipConstants.RECIPIENT_ID,
                        new Long(orderDtl.getRecipientId()));
        inputParams.put(ShipConstants.PRODUCT_ID, orderDtl.getProductId());
        inputParams.put(ShipConstants.QUANTITY,
                        new Long(orderDtl.getQuantity()));
        inputParams.put(ShipConstants.COLOR_1, orderDtl.getColor1());
        inputParams.put(ShipConstants.COLOR_2, orderDtl.getColor2());
        inputParams.put(ShipConstants.SUBSTITUTION_INDICATOR,
                        orderDtl.getSubstitutionIndicator());
        inputParams.put(ShipConstants.SAME_DAY_GIFT,
                        orderDtl.getSameDayGift());
        inputParams.put(ShipConstants.OCCASION, orderDtl.getOccasion());
        inputParams.put(ShipConstants.CARD_MESSAGE,
                        orderDtl.getCardMessage());
        inputParams.put(ShipConstants.CARD_SIGNATURE,
                        orderDtl.getCardSignature());
        inputParams.put(ShipConstants.SPECIAL_INSTRUCTIONS,
                        orderDtl.getSpecialInstructions());
        inputParams.put(ShipConstants.RELEASE_INFO_INDICATOR,
                        orderDtl.getReleaseInfoIndicator());
        inputParams.put(ShipConstants.FLORIST_ID, orderDtl.getFloristId());
        inputParams.put(ShipConstants.SHIP_METHOD, orderDtl.getShipMethod());
        inputParams.put(ShipConstants.SHIP_DATE,
                        orderDtl.getShipDate() == null ? null :
                        new java.sql.Date(orderDtl.getShipDate().getTime()));
        inputParams.put(ShipConstants.ORDER_DISP_CODE,
                        orderDtl.getOrderDispCode());
        inputParams.put(ShipConstants.SECOND_CHOICE_PRODUCT,
                        orderDtl.getSecondChoiceProduct());
        inputParams.put(ShipConstants.ZIP_QUEUE_COUNT,
                        new Long(orderDtl.getZipQueueCount()));
        inputParams.put("REJECT_RETRY_COUNT",
                        orderDtl.getRejectRetryCount() + "");
        inputParams.put(ShipConstants.UPDATED_BY,
                        ShipConstants.UPDATED_BY_VL);
        inputParams.put(ShipConstants.DELIVERY_DATE_RANGE_END,
                        orderDtl.getDeliveryDateRangeEnd() == null ? null :
                        new java.sql.Date(orderDtl.getDeliveryDateRangeEnd().getTime()));
        inputParams.put(ShipConstants.SCRUBBED_ON,
                        orderDtl.getScrubbedOn() == null ? null :
                        new java.sql.Timestamp(orderDtl.getScrubbedOn().getTime()));
        inputParams.put(ShipConstants.SCRUBBED_BY, orderDtl.getScrubbedBy());
        inputParams.put(ShipConstants.ARIBA_UNSPSC_CODE,
                        orderDtl.getAribaUnspscCode());
        inputParams.put(ShipConstants.ARIBA_PO_NUMBER,
                        orderDtl.getAribaPoNumber());
        inputParams.put(ShipConstants.ARIBA_AMS_PROJECT_CODE,
                        orderDtl.getAribaAmsProjectCode());
        inputParams.put(ShipConstants.ARIBA_COST_CENTER,
                        orderDtl.getAribaCostCenter());
        inputParams.put(ShipConstants.SIZE_INDICATOR,
                        orderDtl.getSizeIndicator());
        inputParams.put(ShipConstants.MILES_POINTS,
                        null); // never updated by OP
        inputParams.put(ShipConstants.SUBCODE, orderDtl.getSubcode());
        inputParams.put(ShipConstants.SOURCE_CODE, orderDtl.getSourceCode());
        inputParams.put("OP_STATUS", orderDtl.getOpStatus());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_ORDER_DETAILS");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message =
                (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the SP_GET_CUSTOMER SP.
   * It populates a customer VO based on the returned record set.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param customerId to locate
   * @return CustomerVO
   */
    public CustomerVO getCustomer(Connection conn, long customerId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        CustomerVO customer = new CustomerVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;
        CachedResultSet outputs2 = null;

        try {
            logger.debug("getCustomer (long customerId (" + customerId +
                         ")) :: CustomerVO");

            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(ShipConstants.CUSTOMER_ID, new Long(customerId));

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_CUSTOMER");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            customer.setCustomerId(new Long(customerId).longValue());
            while (outputs.next()) {
                isEmpty = false;
                customer.setConcatId(outputs.getString(ShipConstants.CONCAT_ID));
                customer.setFirstName(outputs.getString(ShipConstants.CUSTOMER_FIRST_NAME));
                customer.setLastName(outputs.getString(ShipConstants.CUSTOMER_LAST_NAME));
                customer.setBusinessName(outputs.getString(ShipConstants.BUSINESS_NAME));
                customer.setAddress1(outputs.getString(ShipConstants.CUSTOMER_ADDRESS_1));
                customer.setAddress2(outputs.getString(ShipConstants.CUSTOMER_ADDRESS_2));
                customer.setCity(outputs.getString(ShipConstants.CUSTOMER_CITY));
                customer.setState(outputs.getString(ShipConstants.CUSTOMER_STATE));

                // fixed to deal with international zip_code of N/A

                String zip_code =
                    outputs.getString(ShipConstants.CUSTOMER_ZIP_CODE);
                if (zip_code != null) {
                    zip_code =
                            zip_code.substring(0, zip_code.length() >= 5 ? 5 :
                                                  zip_code.length());
                    customer.setZipCode(zip_code);
                }

                customer.setCountry(outputs.getString(ShipConstants.CUSTOMER_COUNTRY));
                customer.setAddressType(outputs.getString("address_type"));
            }

            /* setup store procedure input parameters */
            HashMap inputParams2 = new HashMap();
            inputParams2.put("CUSTOMER_ID", new Long(customerId).toString());

            /* build DataRequest object */
            DataRequest dataRequest2 = new DataRequest();
            dataRequest2.setConnection(conn);
            dataRequest2.setStatementID("GET_CUSTOMER_PHONES");
            dataRequest2.setInputParams(inputParams2);

            /* execute the store prodcedure */
            outputs2 = (CachedResultSet)dataAccessUtil.execute(dataRequest2);

            /* populate object */
            List phoneList = new ArrayList();
            while (outputs2.next()) {
                CustomerPhoneVO phoneVO = new CustomerPhoneVO();
                phoneVO.setCustomerId(customerId);
                phoneVO.setExtension(outputs2.getString("CUSTOMER_EXTENSION"));
                phoneVO.setPhoneId(outputs2.getLong("CUSTOMER_PHONE_ID"));
                phoneVO.setPhoneNumber(outputs2.getString("CUSTOMER_PHONE_NUMBER"));
                phoneVO.setPhoneType(outputs2.getString("CUSTOMER_PHONE_TYPE"));
                phoneList.add(phoneVO);
            }
            customer.setCustomerPhoneVOList(phoneList);

        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return customer;
    }

    /**
   * This is a wrapper for CLEAN.CUSTOMER_QUERY_PKG.GET_CUSTOMER_EMAIL_INFO.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   * @throws java.lang.Exception
   * @return EmailVO
   * @param companyId
   * @param customerId
   */
    public EmailVO getCustomerEmailInfo(Connection conn, long customerId,
                                        String companyId) throws Exception {
        CachedResultSet results = null;
        EmailVO email = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_CUSTOMER_ID", new Long(customerId));
        inParms.put("IN_COMPANY_ID", companyId);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_CUSTOMER_EMAIL_INFO");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        if (results.next()) {
            if (results.getObject(3) != null) {
                email = new EmailVO();
                email.setEmailAddress(results.getObject(3).toString());
            }
        }
        return email;
    }

    /**
   * This method is a wrapper for the SP_GET_COMPANY SP.
   * It populates a company VO based on the returned record set.
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @param conn database connection
   * @param companyId record id
   * @return CompanyVO
   */
    public CompanyVO getCompany(Connection conn, String companyId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        CompanyVO company = new CompanyVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        logger.debug("getCompany (String companyId (" + companyId +
                     ")) :: CompanyVO");
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put(ShipConstants.COMPANY_ID, companyId);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_COMPANY");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        /* populate object */
        while (outputs.next()) {
            isEmpty = false;
            company.setCompanyId(companyId);
            company.setCompanyName(outputs.getString(ShipConstants.COMPANY_NAME));
            company.setInternetOrigin(outputs.getString(ShipConstants.INTERNET_ORIGIN));
            company.setDefaultProgramId(outputs.getLong(ShipConstants.DEFAULT_PROGRAM_ID));
            company.setPhoneNumber(outputs.getString(ShipConstants.PHONE_NUMBER));
            company.setClearingMemberNumber(outputs.getString(ShipConstants.CLEARING_MEMBER_NUMBER));
            company.setLogoFileName(outputs.getString(ShipConstants.LOGO_FILE_NAME));
            company.setEnableLpProcessing(outputs.getString(ShipConstants.ENABLE_LP_PROCESSING));
            company.setURL(outputs.getString(ShipConstants.URL));
            company.setBrand(outputs.getString(ShipConstants.BRAND));
        }
        if (isEmpty)
            return null;
        else
            return company;
    }

  /**
   * This is a wrapper for CLEAN.ORDER_QUERY_PKG.GET_ORDER_TRACKING
   * (Taken from com.ftd.op.order.dao.OrderDAO)
   *
   * @throws java.lang.Exception
   * @return OrderTrackingVO
   * @param orderDetailId
   */
    public OrderTrackingVO getTrackingInfo(Connection conn, long orderDetailId) throws Exception {
        CachedResultSet results = null;
        OrderTrackingVO trackingVO = null;
        List resultList = new ArrayList();
        DataRequest dataRequest = new DataRequest();
        HashMap inParms = new HashMap();

        inParms.put("IN_ORDER_DETAIL_ID", Long.toString(orderDetailId));

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ORDER_TRACKING");
        dataRequest.setInputParams(inParms);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        results = (CachedResultSet)dataAccessUtil.execute(dataRequest);

        if (results.next()) {
            trackingVO = new OrderTrackingVO();
            trackingVO.setOrderDetailId(Long.parseLong(results.getObject(1).toString()));
            trackingVO.setTrackingNumber(results.getObject(2).toString());
            trackingVO.setCarrierId(results.getObject(3).toString());
            trackingVO.setCarrierName(results.getObject(4).toString());
            trackingVO.setCarrierURL(results.getObject(5).toString());
            trackingVO.setCarrierPhone(results.getObject(6).toString());

            resultList.add(trackingVO);
        }
        return trackingVO;
    }

    /**
   * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.
   * It populates a global parameter VO based on the returned record set.
   *
   * @param conn database connection
   * @param context filter
   * @param name filter
   * @return GlobalParameterVO
   */
    public GlobalParameterVO getGlobalParameter(Connection conn,
                                                String context,
                                                String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        GlobalParameterVO param = new GlobalParameterVO();
        boolean isEmpty = true;
        String outputs = null;

        try {
            logger.debug("getGlobalParameter (String context, String name) :: GlobalParameterVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(ShipConstants.CONTEXT, context);
            inputParams.put(ShipConstants.NAME, name);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("GET_GLOBAL_PARM_VALUE");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (String)dataAccessUtil.execute(dataRequest);
            if (outputs != null)
                isEmpty = false;
            /* populate object */
            param.setName(name);
            param.setValue(outputs);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        if (isEmpty)
            return null;
        else
            return param;
    }


    /**
   * This method is a wrapper for the SP_GET_PRODUCT SP.
   * It populates a product VO based on the returned record set.
   *
   * @param conn database connection
   * @param productId to retrieve
   * @return ProductVO
   */
    public ProductVO getProduct(Connection conn, String productId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        ProductVO product = new ProductVO();
        boolean isEmpty = true;
        CachedResultSet outputs = null;

        try {
            logger.debug("getProduct (String productId (" + productId +
                         ")) :: ProductVO");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put(ShipConstants.PRODUCT_ID, productId);

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SP_GET_PRODUCT_BY_ID_RESULTSET");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (CachedResultSet)dataAccessUtil.execute(dataRequest);

            /* populate object */
            while (outputs.next()) {
                isEmpty = false;
                product.setProductId(outputs.getString(ShipConstants.PRODUCT_ID_LC));
                product.setNovatorId(outputs.getString(ShipConstants.NOVATOR_ID_LC));
                product.setProductName(outputs.getString(ShipConstants.PRODUCT_NAME_LC));
                product.setNovatorName(outputs.getString(ShipConstants.NOVATOR_NAME_LC));
                product.setStatus(outputs.getString(ShipConstants.STATUS_LC));
                product.setDeliveryType(outputs.getString(ShipConstants.DELIVERY_TYPE_LC));
                product.setCategory(outputs.getString(ShipConstants.CATEGORY_LC));
                product.setProductType(outputs.getString(ShipConstants.PRODUCT_TYPE_LC));
                product.setProductSubType(outputs.getString(ShipConstants.PRODUCT_SUB_TYPE_LC));
                product.setColorSizeFlag(outputs.getString(ShipConstants.COLOR_SIZE_FLAG_LC));
                product.setStandardPrice(outputs.getDouble(ShipConstants.STANDARD_PRICE_LC));
                product.setDeluxePrice(outputs.getDouble(ShipConstants.DELUXE_PRICE_LC));
                product.setPremiumPrice(outputs.getDouble(ShipConstants.PREMUIM_PRICE_LC));
                product.setPreferredPricePoint(outputs.getDouble(ShipConstants.PREFERRED_PRICE_POINT_LC));
                product.setVariablePriceMax(outputs.getDouble(ShipConstants.VARIABLE_PRICE_MAX_LC));
                product.setShortDescription(outputs.getString(ShipConstants.SHORT_DESCRIPTION_LC));
                product.setLongDescription(outputs.getString(ShipConstants.LONG_DESCRIPTION_LC));
                product.setFloristReferenceNumber(outputs.getString(ShipConstants.FLORIST_REFERENCE_NUMBER_LC));
                product.setMercuryDescription(outputs.getString(ShipConstants.MERCURY_DESCRIPTION_LC));
                product.setItemComments(outputs.getString(ShipConstants.ITEM_COMMENTS_LC));
                product.setAddOnBalloonsFlag(outputs.getString(ShipConstants.ADD_ON_BALLOONS_FLAG_LC));
                product.setAddOnBearsFlag(outputs.getString(ShipConstants.ADD_ON_BEARS_FLAG_LC));
                product.setAddOnCardsFlag(outputs.getString(ShipConstants.ADD_ON_CARDS_FLAG_LC));
                product.setAddOnFuneralFlag(outputs.getString(ShipConstants.ADD_ON_FUNERAL_FLAG_LC));
                product.setCodifiedFlag(outputs.getString(ShipConstants.CODIFIED_FLAG_LC));
                product.setExceptionCode(outputs.getString(ShipConstants.EXECPTION_CODE_LC));
                product.setExceptionStartDate(outputs.getDate(ShipConstants.EXCEPTION_START_DATE_LC));
                product.setExceptionEndDate(outputs.getDate(ShipConstants.EXECPTION_END_DATE_LC));
                product.setExceptionMessage(outputs.getString(ShipConstants.EXCEPTION_MESSAGE_LC));
                product.setSecondChoiceCode(outputs.getString(ShipConstants.SECOND_CHOICE_CODE_LC));
                product.setHolidaySecondChoiceCode(outputs.getString(ShipConstants.HOLIDAY_SECOND_CHOICE_CODE_LC));
                product.setDropshipCode(outputs.getString(ShipConstants.DROPSHIP_CODE_LC));
                product.setDiscountAllowedFlag(outputs.getString(ShipConstants.DISCOUNT_ALLOWED_FLAG_LC));
                product.setDeliveryIncludedFlag(outputs.getString(ShipConstants.DELIVERY_INCLUDED_FLAG_LC));
                product.setTaxFlag(outputs.getString(ShipConstants.TAX_FLAG_LC));
                product.setServiceFeeFlag(outputs.getString(ShipConstants.SERVICE_FEE_FLAG_LC));
                product.setExoticFlag(outputs.getString(ShipConstants.EXOTIC_FLAG_LC));
                product.setEgiftFlag(outputs.getString(ShipConstants.EGIFT_FLAG_LC));
                product.setCountryId(outputs.getString(ShipConstants.COUNTRY_ID_LC));
                product.setArrangementSize(outputs.getString(ShipConstants.ARRANGEMENT_SIZE_LC));
                product.setArrangementColors(outputs.getString(ShipConstants.ARRANGEMENT_COLORS_LC));
                product.setDominantFlowers(outputs.getString(ShipConstants.DOMINANT_FLOWERS_LC));
                product.setSearchPriority(outputs.getString(ShipConstants.SEARCH_PRIORITY_LC));
                product.setRecipe(outputs.getString(ShipConstants.RECIPE_LC));
                product.setSubcodeFlag(outputs.getString(ShipConstants.SUBCODE_FLAG_LC));
                product.setDimWeight(outputs.getString(ShipConstants.DIM_WEIGHT_LC));
                product.setNextDayUpgradeFlag(outputs.getString(ShipConstants.NEXT_DAT_UPGRADE_FLAG_LC));
                product.setCorporateSite(outputs.getString(ShipConstants.CORPORATE_SITE_LC));
                product.setUnspscCode(outputs.getString(ShipConstants.UNSPSC_CODE_LC));
                product.setPriceRank1(outputs.getString(ShipConstants.PRICE_RANK1_LC));
                product.setPriceRank2(outputs.getString(ShipConstants.PRICE_RANK2_LC));
                product.setPriceRank3(outputs.getString(ShipConstants.PRICE_RANK3_LC));
                product.setShipMethodCarrier(outputs.getString(ShipConstants.SHIP_METHOD_CARRIER_LC));
                product.setShipMethodFlorist(outputs.getString(ShipConstants.SHIP_METHOD_FLORIST_LC));
                product.setShippingKey(outputs.getString(ShipConstants.SHIPPING_KEY_LC));
                product.setVariablePriceFlag(outputs.getString(ShipConstants.VARIABLE_PRICE_FLAG_LC));
                product.setHolidaySku(outputs.getString(ShipConstants.HOLIDAY_SKU_LC));
                product.setHolidayPrice(outputs.getDouble(ShipConstants.HOLIDAY_PRICE_LC));
                product.setCatalogFlag(outputs.getString(ShipConstants.CATALOG_FLAG_LC));
                product.setHolidayDeluxePrice(outputs.getDouble(ShipConstants.HOLIDAY_DELUXE_PRICE_LC));
                product.setHolidayPremiumPrice(outputs.getDouble(ShipConstants.HOLIDAY_PREMIUM_PRICE_LC));
                product.setHolidayStartDate(outputs.getDate(ShipConstants.HOLIDAY_START_DATE_LC));
                product.setHolidayEndDate(outputs.getDate(ShipConstants.HOLIDAY_END_DATE_LC));
                product.setHoldUntilAvailable(outputs.getString(ShipConstants.HOLD_UNTIL_AVAILABLE_LC));
                product.setMondayDeliveryFreshcut(outputs.getString(ShipConstants.MONDAY_DELIVERY_FRESHCUT_LC));
                product.setTwoDaySatFreshcut(outputs.getString(ShipConstants.TWO_DAY_SAT_FRESHCUT_LC));
                product.setOver21(StringUtils.equals(outputs.getString(ShipConstants.OVER_21_LC),ShipConstants.VL_YES));
                product.setExpressOnly(StringUtils.equals(outputs.getString(ShipConstants.EXPRESS_ONLY_LC),ShipConstants.VL_YES));
            }
        } catch (Exception e) {
            logger.error(e);
            throw (e);
        }
        if (isEmpty)
            return null;
        else
            return product;
    }

    /**
   * This is a wrapper for CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_TRACKING.
   * @throws java.lang.Exception
   *
   * @param conn database connection
   * @param trackingVO
   */
    public

    void updateTrackingNumber(Connection conn, OrderTrackingVO trackingVO) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID",
                        new Long(trackingVO.getOrderDetailId()));
        inputParams.put("IN_TRACKING_NUMBER", trackingVO.getTrackingNumber());
        inputParams.put("IN_TRACKING_DESCRIPTION",
                        trackingVO.getTrackingDescription());
        inputParams.put("IN_CARRIER_ID", trackingVO.getCarrierId());

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_ORDER_TRACKING");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
        String status = (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message =
                (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_DETAIL_SHIP_INFO SP.
   * It updates carrier and vendor info in order detail record to match the passed in VO.
   *
   * @param conn database connection
   * @param orderDtl record to update
   * @return none
   */
    public void updateOrderDetailCarrierInfo(Connection conn, OrderDetailVO orderDtl) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;


        logger.debug("updateOrder (OrderDetailVO orderDtl (" +
                     orderDtl.getOrderDetailId() + ")) :: void" + orderDtl);
        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDtl.getOrderDetailId()));
        inputParams.put("IN_VENDOR_ID", orderDtl.getVendorId());
        inputParams.put("IN_FLORIST_ID", orderDtl.getFloristId());
        inputParams.put("IN_CARRIER_DELIVERY", orderDtl.getCarrierDelivery());
        inputParams.put("IN_UPDATED_BY", ShipConstants.UPDATED_BY_VL);

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_ORDER_DETAIL_SHIP_INFO");
        dataRequest.setInputParams(inputParams);

        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        outputs = (Map)dataAccessUtil.execute(dataRequest);

        /* read stored prodcedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message =
                (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
   * This method is a wrapper for the SP_INSERT_ORDER_FLORIST_USED SP.
   * It is used to insert a record into the order_florist_used table
   * which is used to keep track of the florists that have been used for
   * a particular order.
   *
   * This method was taken from order_process.OrderDAO
   *
   * @param orderDtl OrderDetailVO holding updating order detail
   * @param floristId String with florist id to update in order detail
   * @return none
   */
    public void insertOrderFloristUsed(Connection conn, OrderDetailVO orderDtl,
                                       String floristId) throws Exception {
        DataRequest dataRequest = new DataRequest();
        Map outputs = null;
        if (floristId == null) {
            logger.info("Tried to insert null into order florist used table -- Ignoring");
            return;
        }
        try {
            logger.debug("insertOrderFloristUsed (OrderDetailVO orderDtl (" +
                         orderDtl.getOrderDetailId() +
                         "), String floristId) :: void");
            /* setup store procedure input parameters */
            HashMap inputParams = new HashMap();
            inputParams.put("IN_ORDER_DETAIL_ID",
                            new Long(orderDtl.getOrderDetailId()));
            inputParams.put("IN_FLORIST_ID", floristId);
            inputParams.put("IN_SELECTION_DATA", "Vendor");

            /* build DataRequest object */
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("SHIP_INSERT_ORDER_FLORIST_USED");
            dataRequest.setInputParams(inputParams);

            /* execute the store prodcedure */
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            outputs = (Map)dataAccessUtil.execute(dataRequest);

            /* read store prodcedure output parameters to determine
       * if the procedure executed successfully */
            String status =
                (String)outputs.get(ShipConstants.OUT_STATUS_PARAM);
            if (status != null &&
                status.equalsIgnoreCase(ShipConstants.VL_NO)) {
                String message =
                    (String)outputs.get(ShipConstants.OUT_MESSAGE_PARAM);
                throw new Exception(message);
            }
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Retrieves complimentary addon for the source code and product.
     * @param conn
     * @param sourceCode
     * @param productId
     * @return
     * @throws Exception
     */
    public CachedResultSet getCompAddOnInfo(Connection conn, String sourceCode, String productId)  throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_COMP_ADD_ON_INFO");
        dataRequest.addInputParam("IN_SOURCE_CODE",sourceCode);
        dataRequest.addInputParam("IN_PRODUCT_ID",productId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

        CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        return rs;
    }

    /**
     * Updates delivery confirmation status
     *
     * @param conn
     * @param orderDetailId
     * @param newStatus
     * @param updatedBy
     * @throws Exception
     */
    public void updateDeliveryConfirmationStatus(Connection conn, String orderDetailId, String newStatus) throws Exception {
        logger.debug("updateDeliveryConfirmationStatus");

        Map output = null;
        DataRequest dataRequest = new DataRequest();

        /* setup store procedure input parameters */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        inputParams.put("IN_STATUS", newStatus);
        inputParams.put("IN_CSR_ID", "SHIP");

        /* build DataRequest object */
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("UPDATE_DCON_STATUS");
        dataRequest.setInputParams(inputParams);

        /* execute the store procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        output = (Map)dataAccessUtil.execute(dataRequest);

        /* read store procedure output parameters to determine
        * if the procedure executed successfully */
        String status = (String)output.get(ShipConstants.OUT_STATUS_PARAM);
        if (status != null && status.equalsIgnoreCase(ShipConstants.VL_NO)) {
            String message = (String)output.get(ShipConstants.OUT_MESSAGE_PARAM);
            throw new Exception(message);
        }

    }

    /**
     * This method will check if an order has morning delivery fee or not.  If so, it will return a 'Y'
     * otherwise it will return a 'N'.
     *
     * @throws java.lang.Exception
     * @param String orderDetailId
     * @return boolean
     */

    public boolean orderHasMorningDeliveryFee(Connection conn, String orderDetailId) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("ORDER_HAS_MORNING_DELIVERY_FEE");
        dataRequest.addInputParam("IN_ORDER_DETAIL_ID", orderDetailId);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String morningDeliveryFlag = (String) dataAccessUtil.execute(dataRequest);
        if (morningDeliveryFlag.equalsIgnoreCase("Y"))
        	return true;
        else
        	return false;
    }
    
    
    
    public List<FtdwestPersonalizationVO> getFtdwestPersonalizationsForProduct(Connection conn,String productId) throws Exception{
    	
    	List<FtdwestPersonalizationVO> ftdwestPersonalizationList=new ArrayList<FtdwestPersonalizationVO>();
    	ResultSet outputs = null;
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PQUAD_PC_INFO");
        dataRequest.addInputParam("IN_PRODUCT_ID", productId);
        outputs = (ResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while( outputs.next() ) {
        	FtdwestPersonalizationVO personalizationVO=new FtdwestPersonalizationVO();
        	personalizationVO.setAccessoryId(outputs.getString("PQUAD_PC_ID"));//accessoryId
        	personalizationVO.setDisplayNames( outputs.getString("PQUAD_PC_DISPLAY_NAMES"));
        	personalizationVO.setTemplateOrder(outputs.getString("PERSONALIZATION_TEMPLATE_ORDER"));
        	personalizationVO.setPersonalizationCaseFlag(StringUtils.equals(outputs.getString("PERSONALIZATION_CASE_FLAG"),ShipConstants.VL_YES));
        	ftdwestPersonalizationList.add(personalizationVO);
        }
    	
    	
    	return ftdwestPersonalizationList;
    	
    }

}
