Notes about the wsdls:

Version 8.5:
-------------
    wtmservice.wsdl - used by Argo
    VSAMS.wsdl - used by ship processing


Version 14
-------------
    wtmservice.wsdl - used by Argo - no changes were done to this wsdl - has been checked-in in Argo folder
    WTMService.wsdl - used by ship - replaces VSAMS.wsdl - has been checked-in in Apollo folder

