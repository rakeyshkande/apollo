/****** Object:  Table [FTD].[FTD_SHIP_UNITS]    Script Date: 12/11/2012 14:25:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FTD].[FTD_SHIP_UNITS](
	[ShipUnitID] [int] NOT NULL,
	[FTDOrderNumber] [varchar](20) NOT NULL,
	[FTDProductID] [varchar](20) NOT NULL,
	[FTDProductDescription] [varchar](40) NULL,
	[LineOfBusiness] [char](2) NOT NULL,
	[VendorCost] [money] NULL,
	[VendorTotalCost] [money] NULL,
	[ProductWeight] [numeric](10, 4) NULL,
	[MarketingInsert] [char](1) NULL,
	[OriginDistributionCenters] [xml] NULL,
	[InvoiceData] [xml] NOT NULL,
	[AddOns] [xml] NULL,
	[AddOnIDList] [varchar](500) NULL,
	[Type7AddOnIDList] [varchar](500) NULL,
	[PrintBatchID] [varchar](50) NULL,
	[PrintBatchSequence] [int] NULL,
	[ModifiedUserID] [varchar](60) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[TimeStamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FTD_SHIP_UNITS_SHIP_UNIT_ID] PRIMARY KEY CLUSTERED 
(
	[ShipUnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FTD].[FTD_SHIP_UNIT_ADD_ONS]    Script Date: 12/11/2012 14:25:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FTD].[FTD_SHIP_UNIT_ADD_ONS](
	[ShipUnitID] [int] NOT NULL,
	[AddOnType] [varchar](25) NOT NULL,
	[AddOnID] [varchar](10) NOT NULL,
	[ModifiedUserID] [varchar](60) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[TimeStamp] [timestamp] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FTD].[FTD_HOST_PLD]    Script Date: 12/11/2012 14:25:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FTD].[FTD_HOST_PLD](
	[DistributionCenter] [varchar](20) NOT NULL,
	[CartonNumber] [dbo].[CartonNumberType] NOT NULL,
	[TrackingNumber] [dbo].[TrackingNumberType] NULL,
	[PrintBatchID] [varchar](50) NULL,
	[PrintBatchSequence] [int] NULL,
	[DatePlannedShipment] [datetime] NULL,
	[Status] [varchar](20) NOT NULL,
	[DateStatus] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
