package com.ftd.osp.test.qmsvalidation;

import junit.framework.*;

import java.sql.Connection;
import com.ftd.osp.test.ordervalidator.TestHelper;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.qmsvalidation.QMSValidationBO;
import com.ftd.osp.utilities.ConfigurationUtil;

public class QMSValidationBOTest extends TestCase
{
    private String guid = null;
    private static final String TEST_SETUP_FILE_NAME = "test_setup.xml";
    
    public QMSValidationBOTest(String name)
    {
        super(name);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("QMSValidationBOTest");

        suite.addTest(new QMSValidationBOTest("testExecute"));

        return suite;
    }

    public void setUp()
    {
        try
        {
            // Load GUID for this test
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
            guid = configUtil.getProperty(TEST_SETUP_FILE_NAME, "GUID");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void testExecute()
    {
        try
        {
            this.assertNotNull(guid);
            Connection con = TestHelper.getConnection();

            QMSValidationBO qmsValidation = new QMSValidationBO();
            BusinessConfig config = new BusinessConfig();
            config.setConnection(TestHelper.getConnection());
            MessageToken token = new MessageToken();
            token.setMessage(guid);
            config.setInboundMessageToken(token);
            qmsValidation.execute(config);

        }
        catch(Exception e)
        {
            this.fail();
        }
    }

    public static void main(String[] args)
    {
        int iCount = args.length;
        if(iCount > 0)
        {
            QMSValidationBOTest qmsValidationTest = null;

            for (int i = 0; i < iCount; i++) 
            {
                qmsValidationTest = new QMSValidationBOTest(args[i]);
                qmsValidationTest.run();
            }            
        }
        else
        {
            junit.textui.TestRunner.run(suite());
        }
    }
}