package com.ftd.osp.qmsvalidation;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.ftd.avs.webservice.Address;
import com.ftd.avs.webservice.AddressVerificationService;
import com.ftd.avs.webservice.AddressVerificationServiceBingImplService;
import com.ftd.avs.webservice.DevaluationReason;
import com.ftd.avs.webservice.VerificationRequest;
import com.ftd.avs.webservice.VerificationResponse;
import com.ftd.avs.webservice.VerifiedAddress;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.vo.ValidationRequest;
import com.ftd.osp.ordervalidator.vo.ValidationResponse;
import com.ftd.osp.qmsvalidation.exception.QMSValidationException;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.AVSAddressScoreVO;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;

/**
 * This class validates recipient addresses using the ABClient jar. If auto fix
 * is turned on in the CONFIG file then the address fields of the order will be
 * updated. This class uses a pool of AddressBroker connections.
 *
 * @author Jeff Penney
 *
 */
public class ValidateQmsAddress {

	private static Logger logger = new Logger(ValidateQmsAddress.class.getName()); 
	private static AddressVerificationService addressVerificationSerivce;
	private static String avsURL;
	
	
	private final static String QMS_CONFIG_FILE = "qms_config.xml";

	/**
	 * Validates an address against QMS Address Broker
	 * 
	 * @param request - A ValidationRequest object containing the order
	 * @exception Exception - if a QMS error occurs
	 * @return A ValidationResponse object containing the validation results
	 *
	 */
	@SuppressWarnings("unchecked")
	public ValidationResponse validate(ValidationRequest request) throws QMSValidationException, Exception {
		
		if(logger.isDebugEnabled()) {
			logger.debug("Starting ValidateQmsAddress.validate()");
		}
		
		Connection conn = null;
		ConfigurationUtil config = null;
		ScrubMapperDAO scrubDAO = null;
		
		ValidationResponse response = null;
		OrderVO order = null;		
		OrderDetailsVO item = null;
		
		PartnerMappingVO partnerMappingVO = null;
		
		try {	
			
			order = (OrderVO) request.get(ValidationConstants.ORDER);
			if (order == null) {
				throw new QMSValidationException("AVS is not invoked, OrderVO cannot be null");
			}
			
			// Check master order number because it cannot be null
			if (order.getMasterOrderNumber() == null || order.getMasterOrderNumber().length() < 1) {
				throw new QMSValidationException("AVS is not invoked, master order number cannot be null or empty");
			}
			
			// QMS each item recipient address 
			List<OrderDetailsVO> items = order.getOrderDetail();			
			if (items == null) {
				throw new QMSValidationException("AVS is not invoked, OrderDetailsVO list cannot be null");
			}
			
			config = ConfigurationUtil.getInstance(); 
			conn = DataSourceUtil.getInstance().getConnection(config.getProperty(QMS_CONFIG_FILE, "DATABASE_CONNECTION"));
			
			// Check if partner Order
			partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), conn); 
			
			Iterator<OrderDetailsVO> it = items.iterator();
			while (it.hasNext()) {
				item = it.next();
				if(logger.isDebugEnabled()) {
					logger.debug("Found an OrderDetailsVO " + item.getLineNumber() + ":" + item.getProductId() + ":" + order.getMasterOrderNumber());
				}
				item.setAvsAddress(fetchVerifiedAddress(item, partnerMappingVO));
			}			
			
			// Put the AVS scores to validation response
			response = new ValidationResponse();
			response.put(ValidationConstants.ORDER, order);
			
			try {
				// insert AVS Scores
				scrubDAO = new ScrubMapperDAO(conn);
				scrubDAO.insertAVSAddress(order);
				scrubDAO.insertAVSAddressScores(order);
			} catch (Exception e) {
				logger.error("Error occured while creating avs address and scores");
				logger.error (e);
				throw(e);
			}

		} catch (Exception e) {
			logger.error("Error caught validating the address");
			throw (e);
		} finally {
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("Ending ValidateQmsAddress.validate()");
		}

		return response;
	}

	/**
	 * Runs QMS Address Broker on an address.
	 * 
	 * @param item - An Item object containing the address to be checked
	 * @param isPartnerOrder 
	 * @exception Exception - if a QMS exception occurs
	 * @return The QMS response for this address
	 *
	 */
	private AVSAddressVO fetchVerifiedAddress(OrderDetailsVO item, PartnerMappingVO partnerMappingVO) throws QMSValidationException {
		RecipientsVO recipient = null;
		RecipientAddressesVO recipAddress = null;
		
		// Check for Recipient
		if (item.getRecipients() != null && item.getRecipients().size() > 0) {
			recipient = (RecipientsVO) item.getRecipients().get(0);

			if (recipient == null) {
				throw new QMSValidationException("Recipient cannot be null");
			}
			
			if(logger.isDebugEnabled()) {
				logger.debug("Recipient name = " + recipient.getFirstName() + " " + recipient.getLastName());
			}

			if (recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0) {
				recipAddress = (RecipientAddressesVO) recipient.getRecipientAddresses().get(0);				
			}
		}
		
		if (recipAddress == null) {
			throw new QMSValidationException("Recipient address cannot be null");
		}

		AVSAddressVO avsAddressVO = null;
		// if AVS Address does not exists, Default the overridden flag to N
		if (item.getAvsAddress() == null) {
			avsAddressVO = new AVSAddressVO();			
			avsAddressVO.setOverrideFlag(GeneralConstants.AVS_NO); 
		} else {
			avsAddressVO = (AVSAddressVO) item.getAvsAddress();
		}
		
		String avsOrderType = getAVSOrderType(recipAddress.getCountry(), item, partnerMappingVO);
		if(avsOrderType  == null) {
			logger.info("AVS disabled for order.  Skipping Address verification for item " + item.getExternalOrderNumber());
			avsAddressVO.setAvsPerformed(GeneralConstants.AVS_NO);
			avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS); 
			return avsAddressVO;
		}

		// Perform AVS 
		try { 
			logger.info("AVS is ON.  Performing Address verification for item " + item.getExternalOrderNumber());
			
			VerificationResponse verificationRes;
			List<VerifiedAddress> verifiedAddresses;
			
			try {
				verificationRes = getAvsValidatedAddress(recipAddress, avsOrderType);
				verifiedAddresses = sortVerifiedAddress(verificationRes);
			} catch (Exception e) {
				logger.error("Exception when verifying address for item " + item.getExternalOrderNumber() + ". skipping verification", e);
				avsAddressVO.setAvsPerformed(GeneralConstants.AVS_NO);
				avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS);				
				return avsAddressVO;
			}
			
			if (verifiedAddresses.size() == 0) {
				logger.info("zero address verification results returned for item " + item.getExternalOrderNumber() + ". result=FAIL");
				avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_FAIL);
				avsAddressVO.setAvsPerformed(GeneralConstants.AVS_YES);
				avsAddressVO.setAvsPerformedOrigin(GeneralConstants.AVS_PERFORMED_APOLLO);				
				return avsAddressVO;
				
			} else if (verifiedAddresses.size() == 1) {
				if (verifiedAddresses.get(0).getStatus().equalsIgnoreCase(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS)) {
					logger.info("single passing result returned for item " + item.getExternalOrderNumber() + ". result=PASS");
					avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_PASS);
				} else {
					logger.info("single failing result returned for item " + item.getExternalOrderNumber() + ". result=FAIL");
					avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_FAIL);
				}
				avsAddressVO.setAvsPerformed(GeneralConstants.AVS_YES);
				avsAddressVO.setAvsPerformedOrigin(GeneralConstants.AVS_PERFORMED_APOLLO);

			} else if (verifiedAddresses.size() > 1) {
				logger.info("multiple passing results returned for item " + item.getExternalOrderNumber() + ". result=FAIL");
				avsAddressVO.setResult(GeneralConstants.AVS_VERIFIED_ADDRESS_FAIL);
				avsAddressVO.setAvsPerformedOrigin(GeneralConstants.AVS_PERFORMED_APOLLO);
				avsAddressVO.setAvsPerformed(GeneralConstants.AVS_YES);
				avsAddressVO.setAvsPerformedOrigin(GeneralConstants.AVS_PERFORMED_APOLLO);
			}
			
			VerifiedAddress verifiedAddr = verifiedAddresses.get(0);
			mapVerifiedAddress(verifiedAddr, avsAddressVO, recipient.getRecipientId());

		} catch (Exception ioe) {
			logger.error(ioe);
			throw new QMSValidationException(ioe.getMessage());
		}

		return avsAddressVO;
	}

	
	/** Rules to perform AVS
	 *
	 * 1. Should be a US recipient
	 * 2. the flag verify_address_dropship_orders for Non Partner DROPSHIP orders should be Y.
	 * 3. the flag verify_address_floral_orders for Non Partner FLORAL orders should be Y.
	 * 4. the flag AVS_DROPSHIP on partner_mapping should be Y for Partner DROPSHIP orders.
	 * 5. the flag AVS_FLORAL on partner_mapping should be Y for Partner FLORAL orders. 
	 * 
	 * @param recipCountry
	 * @param item 
	 * @param isPartnerOrder
	 * @return
	 */
	private String getAVSOrderType(String recipCountry, OrderDetailsVO item, PartnerMappingVO partnerMappingVO) {
		GlobalParmHandler globalParmHdlr = null;		
		
		if(!"US".equalsIgnoreCase(recipCountry)) {
			logger.info("AVS will not be performed for non_US recipients");
			return null;
		} 
		
		String shipMethod = item.getShipMethod();
		boolean checkFloralFlag = false;
		boolean checkVendorFlag = false;
		
		if(shipMethod == null || shipMethod.trim().length() == 0) {
			
			if(logger.isDebugEnabled()) {
				logger.debug("Ship method on original order is null, checking PDB flags." );
			}
			
			if("Y".equals(item.getShipMethodFloristFlag())) {
				checkFloralFlag = true;
			} else if("Y".equals(item.getShipMethodCarrierFlag())) {
				checkVendorFlag = true;
			}
			
		} else {
			
			if(shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_SAME_DAY_CODE)) {
				checkFloralFlag = true;
			} else {
				checkVendorFlag = true;
			}
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("AVS Checks: checkFloralFlag:" + checkFloralFlag + ", checkVendorFlag: " + checkVendorFlag);
		}
		
		// Partner orders
		if(partnerMappingVO != null) { 
			if(checkVendorFlag && partnerMappingVO.isPerformDropshipAVS()) {
				return GeneralConstants.AVS_ORDER_TYPE_DROPSHIP;
			}  
			
			if(checkFloralFlag && partnerMappingVO.isPerformFloristAVS()) {
				return GeneralConstants.AVS_ORDER_TYPE_FLORIST;
			}			
			return null;
		}
		
		// Non partner orders
		globalParmHdlr = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
		if (checkVendorFlag && "on".equalsIgnoreCase(globalParmHdlr.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_DROPSHIP_ORDERS))) {  
			return GeneralConstants.AVS_ORDER_TYPE_DROPSHIP;
		} 
		
		if(checkFloralFlag && "on".equalsIgnoreCase(globalParmHdlr.getFrpGlobalParm("AVS_CONFIG", GeneralConstants.VERIFY_ADDRESS_FLORIST_ORDERS))) { 
			return GeneralConstants.AVS_ORDER_TYPE_FLORIST;
		}
		
		return null;
	}

	/*
	 * This method calls AVS web-service to validate recipient address and
	 * returns validated AVS address along with scores.
	 */
	private VerificationResponse getAvsValidatedAddress( RecipientAddressesVO recipAddress, String orderType) throws Exception {

		VerificationRequest vr = new VerificationRequest();
		Address a = new Address();
		VerificationResponse resp = null;

		try {

			a.setStreet1(recipAddress.getAddressLine1());
			a.setStreet2(recipAddress.getAddressLine2());
			a.setCity(recipAddress.getCity());
			a.setState(recipAddress.getStateProvince());
			a.setZip(recipAddress.getPostalCode());
			a.setCountry(recipAddress.getCountry());
			vr.setOrderType(orderType);

			vr.setAddress(a);
			vr.setClientIdentifier("SCRUB");
			// get the AVS web service reference and call it
			AddressVerificationService avs = getAddressVerificationService();
			resp = avs.verifyAddress(vr);
		} catch (Exception e) {
			logger.error("Error occured while calling Address Verification Service" + e);
			throw e;
		}

		return resp;

	}

	// Get the address verification service
	private AddressVerificationService getAddressVerificationService() throws CacheException, Exception {
		GlobalParmHandler gph = (GlobalParmHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_NAME_GLOBAL_PARM);
		
		if (addressVerificationSerivce == null 
				|| (avsURL != null && !avsURL.equals(gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL")))) {

			avsURL = gph.getFrpGlobalParm("SERVICE", "ADDRESS_VERIFICATION_SERVICE_URL");
			logger.warn("Creating Address Verification Service reference from wsdl: " + avsURL);
			try {
				URL avsServiceURL = new URL(avsURL);
				AddressVerificationServiceBingImplService avs = new AddressVerificationServiceBingImplService(avsServiceURL);
				addressVerificationSerivce = avs.getAddressVerificationServiceBingImplPort();
			} catch (MalformedURLException e) {
				logger.error("Error when getting bing web service", e);
				e.printStackTrace();
			}
		}

		return addressVerificationSerivce;
	}

	/**
	 * This method will map verified address and devaluation reason codes to
	 * avsAdressvo and avsScores respectively
	 */
	private void mapVerifiedAddress(VerifiedAddress verifiedAddr, AVSAddressVO avsAddress, long recipientId) throws Exception {

		avsAddress.setAddress(verifiedAddr.getStreet1());
		avsAddress.setCity(verifiedAddr.getCity());
		avsAddress.setStateProvince(verifiedAddr.getState());
		avsAddress.setPostalCode(verifiedAddr.getZip());
		avsAddress.setCountry(verifiedAddr.getCountry());
		avsAddress.setEntityType(verifiedAddr.getEntityType());
		avsAddress.setLongitude(verifiedAddr.getLongitutde());
		avsAddress.setLatitude(verifiedAddr.getLatitude());
		if (verifiedAddr.getConfidenceThreshold() != null) {
			avsAddress.setThreshold(verifiedAddr.getConfidenceThreshold().toString());
		}
		
		avsAddress.setRecipientId(recipientId);

		List<DevaluationReason> reasons = verifiedAddr.getDevaluationReasons();
		AVSAddressScoreVO scoreVO = null;
		List<AVSAddressScoreVO> scoreVOs = new ArrayList<AVSAddressScoreVO>();
		for (DevaluationReason reason : reasons) {
			scoreVO = new AVSAddressScoreVO();
			scoreVO.setReason(reason.getReasonKey());
			if (reason.getReasonScore() != null) { 
				scoreVO.setScore(reason.getReasonScore().toString());
			}
			scoreVOs.add(scoreVO);
		}
		avsAddress.setScores(scoreVOs);

	}

	/**
	 * Create a list of addresses based on the AVS response. Order is important.
	 * If there are no addresses, return an empty list If at least one is valid,
	 * return the top 0-3 valid addresses, ordered highest to lowest If there
	 * are zero valid addresses, and 1 to many invalid addresses, return the
	 * top invalid addresses
	 * 
	 * @param resp
	 * @return
	 */
	private List<VerifiedAddress> sortVerifiedAddress(VerificationResponse resp) {
		List<VerifiedAddress> results = new ArrayList<VerifiedAddress>();

		if (resp.getVerifiedAddresses().size() > 0) {
			// put the verified addresses in the arrayList.
			for (VerifiedAddress va : resp.getVerifiedAddresses()) {
				results.add(va);
			}

			// sort them by score. This sort method uses an anonymous comparator which that determines the sort order (based on score)
			Collections.sort(results, new Comparator<VerifiedAddress>() {
			public int compare(VerifiedAddress a1, VerifiedAddress a2) {
					/**
					 * return 1, 0, or -1
					 * 
					 * 1 means a1 has the higher score 0 means that a1 and a2
					 * have the same score -1 means that a1 has the lower score
					 */
					if (a1.getFTDConfidenceScore() > a2.getFTDConfidenceScore()) {
						return 1;
					} else if (a2.getFTDConfidenceScore() == a2
							.getFTDConfidenceScore()) {
						return 0;
					} else {
						return -1;
					}
				}
			});

		} else {
			// We have a zero sized results list. This is ok.
		}
		return results;
	}
}