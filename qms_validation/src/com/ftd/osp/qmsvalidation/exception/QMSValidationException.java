package com.ftd.osp.qmsvalidation.exception;

public class QMSValidationException extends Exception 
{
    public QMSValidationException()
    {
        super();
    }

    public QMSValidationException(String msg)
    {
        super(msg);
    }
    
}