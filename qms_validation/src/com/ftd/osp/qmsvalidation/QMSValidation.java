package com.ftd.osp.qmsvalidation;

import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.ordervalidator.vo.*;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.*;
import java.lang.reflect.*;

import com.ftd.osp.qmsvalidation.exception.*;

public class QMSValidation {
	private static Logger logger = new Logger("com.ftd.osp.qmsvalidation.QMSValidation");

	public QMSValidation() {
	}

	/**
	 * A method that loads the qms_validation_config.xml file and performs the
	 * required validation on an Order object
	 * 
	 * @param order
	 *            An Order object containing the data do be validated
	 * @return An Object containing original order data and ant validation
	 *         errors
	 *
	 */
	@SuppressWarnings("unchecked")
	public void qmsAddress(OrderVO order) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, NoSuchFieldException, IOException, QMSValidationException, Exception {
		
		if(logger.isDebugEnabled()) {
			logger.debug("Entering com.ftd.osp.qmsvalidation.QMSValidation.qmsAddress(OrderVO)");
		}
		
		ValidationRequest request = new ValidationRequest();
		request.put(ValidationConstants.ORDER, order);
		ValidateQmsAddress qmsAddress= new ValidateQmsAddress();
		qmsAddress.validate(request);
		
		if(logger.isDebugEnabled()) { 
			logger.debug("Exiting com.ftd.osp.qmsvalidation.QMSValidation.qmsAddress(OrderVO)");
		}
	}
}