package com.ftd.osp.qmsvalidation;

import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.order.vo.*;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.*;
import com.ftd.osp.utilities.order.*;

import java.io.*;
import javax.xml.parsers.*;
import java.util.*;
import com.ftd.osp.qmsvalidation.exception.*;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.stats.*;
import java.sql.Connection;

public class QMSValidationBO implements IBusinessObject
{
    private static Logger logger= new Logger("com.ftd.osp.qmsvalidation.QMSValidationBO");
    private static final String es_1002 = "ES1002";
    private static final String SYSTEM_USER = "_SYSTEM__";
    private static final String INSERT_APP_COUNTER = "INSERT_APP_COUNTER";

    public QMSValidationBO()
    {
    }

    public void execute(BusinessConfig config)
    {
        MessageToken messageToken = new MessageToken();
        
        try
        {            
            // Assemble order
            OrderDataMapper orderDao = new ScrubMapperDAO(config.getConnection());
            OrderVO order = orderDao.mapOrderFromDB((String)config.getInboundMessageToken().getMessage());

            // Set system user ID
            order.setCsrId(SYSTEM_USER);

            // Record entry
            recordStats(order, OrderDetailStates.QMS_IN, config.getConnection());                        
            
            QMSValidation qmsValidation = new QMSValidation();
            qmsValidation.qmsAddress(order);

            // Set header status
            logger.debug("Setting header status to " + String.valueOf(OrderStatus.RECEIVED));
            order.setStatus(String.valueOf(OrderStatus.RECEIVED));
            this.setBuyerStatus(order, String.valueOf(OrderStatus.RECEIVED));
            
            // Set item status
            OrderDetailsVO item = null;
            Collection items = order.getOrderDetail();
            Iterator it = items.iterator();
            while(it.hasNext())
            {
                item = (OrderDetailsVO)it.next();
                logger.debug("Setting item status to " + String.valueOf(OrderStatus.RECEIVED));
                item.setStatus(String.valueOf(OrderStatus.RECEIVED_ITEM));
                this.setRecipientStatus(item, String.valueOf(OrderStatus.RECEIVED_ITEM));
            }

            logger.debug("Updating order in SCRUB");
            orderDao.updateOrder(order);

            // Set message status
            logger.debug("Setting message status to " + es_1002 + " for GUID " + order.getGUID());
            messageToken.setMessage(order.getGUID());
            messageToken.setStatus(es_1002);  

            // Record exit
            recordStats(order, OrderDetailStates.QMS_OUT, config.getConnection());

            logger.debug("shipMethodCarrierFlag: " + item.getShipMethodCarrierFlag());
            if (item.getShipMethodCarrierFlag() != null && item.getShipMethodCarrierFlag().equals("Y")) {
                DataAccessUtil dau = DataAccessUtil.getInstance();
                DataRequest dataRequest = new DataRequest();
                dataRequest.setConnection(config.getConnection());
                dataRequest.addInputParam("IN_APPLICATION_NAME", "QMS");
                dataRequest.setStatementID(INSERT_APP_COUNTER);
                Map outputs = (Map) dau.execute(dataRequest);
                dataRequest.reset();
            }
        }
        catch(IOException ioe)
        {
            logException(ioe, messageToken);
        }
        catch(QMSValidationException qve)
        {
            logException(qve, messageToken);
        }
        catch(NoSuchFieldException nsfe)
        {
            logException(nsfe, messageToken);
        }
        catch(IllegalAccessException iae)
        {
            logException(iae, messageToken);
        }
        catch(ParserConfigurationException pce)
        {
            logException(pce, messageToken);
        }      
        catch(Exception e)
        {
            logException(e, messageToken);
        }
        catch(Throwable t)
        {
            logException(t, messageToken);
        }
        finally
        {
            config.setOutboundMessageToken(messageToken);
        } 
    }

    private void recordStats(OrderVO order, int state, Connection connection) throws Exception
    {
        StatsUtil statsUtil = new StatsUtil();
        statsUtil.setOrder(order);        
        statsUtil.setState(state);

        OrderDetailsVO item = null;
        Iterator it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            statsUtil.setItem(item);
            statsUtil.insert(connection);            
        }
    }

    private void setBuyerStatus(OrderVO order, String status)
    {
        BuyerVO buyer = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer != null)
            {
                buyer.setStatus(status);
            }
        }
    }

    private void setRecipientStatus(OrderDetailsVO item, String status)
    {
        RecipientsVO recipient = null;

        if(item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO)item.getRecipients().get(0);

            if(recipient != null)
            {
                recipient.setStatus(status);
            }
        }
    }    

    private void logException(Throwable e, MessageToken token)
    {
        logger.error(e);        
        token.setStatus("ROLLBACK");        
    }    
}