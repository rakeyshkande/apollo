package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.AclDataRequestBuilder;
import com.ftd.security.cache.vo.AbstractVO;
import com.ftd.security.cache.vo.AclVO;
import com.ftd.security.cache.vo.ContextVO;
import com.ftd.security.cache.vo.PermissionVO;
import com.ftd.security.cache.vo.ResourceVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add ACL (number), view/update ACL
 * (name), remove ACL (name), add/remove ACL (resource/permission combination).
 * @author Christy Hu
 */
public class ACLAction extends Action  {

  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.ACLAction";
  private AclDataRequestBuilder requestBuilder = new AclDataRequestBuilder();
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private SecurityManager securityManager;
  private File errorStylesheet;
  private String errorStylesheetName;


  /**
     * The method determines the request action and dispatches request to its appropriate handler.
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering ACLAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);
    try  {
        // authentication call
         requestBuilder = new AclDataRequestBuilder();
         this.securityManager = SecurityManager.getInstance();
         this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
         this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
        if (ServletHelper.isValidToken(context, securityToken))  {     
            if (action != null)  {    
              if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW))  {
                  this.view(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_POPUP))  {
                  // Called from 'Map Acl To Role' page.
                  this.viewPopup(request, response, mapping);              
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_ALL))  {
                  this.viewAll(request, response, mapping);     
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
                  this.displayAdd(request, response, mapping);     
              }else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
                  this.add(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
                  this.remove(request, response, mapping);   
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
                  this.update(request, response, mapping);                
              }  else {
                  logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
                  ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
              }        
            }
            else 
            {
              logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
              ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
            }
        } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          logger.error(securityToken);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
        }            
    } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);      
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } 
    return null;
  }


 /**
  * Displays add ACL name page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("entering displayAdd...");
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // context document
      Document rdoc = null; // resource document
      Document pdoc = null; // permission document
      String contextId = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_CREATE ))  {    
              doc = this.getContextDoc(request);
              Document fdoc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
              fdoc.getDocumentElement().appendChild(fdoc.importNode(doc.getDocumentElement(), true));
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDACL, mapping);

              traxUtil.transform(request, response, fdoc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDACL,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }        
      } catch (Exception ex) { 
        throw ex;
      }  finally 
      {
        if (con != null) 
        {
          con.close();
        }
      }    
  }

 /**
  * Sends ACL detail.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void viewPopup(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    { 
      logger.debug("Entering viewPopup...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // the resulting document to be transformed.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
          
              // Read ACL with acl_number. 
              inputParams = requestBuilder.getViewAclInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_ACL);
              
              doc = (Document) util.execute(dataRequest);
              doc = XMLHelper.getUniqueSubTree(doc, AclVO.XML_BOTTOM, AclVO.TB_RESOURCE_ID, AclVO.TB_PERMISSION_ID);
            
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_ACLPOPUP,mapping);       
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_ACLPOPUP,mapping),ServletHelper.getParameterMap(request));
              
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          } 
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }   
  }

 /**
  * Sends View ACL page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void view(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {         
      logger.debug("entering view...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document rdoc = null; // Resource document
      Document pdoc = null; // Permission document
      Document adoc = null; // ACL document
      Document doc = null; // the resulting document to be transformed.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              // Call view_acl_resources to read all resources in this context.
              // Resources associated with this acl have an acl_number element.          
              inputParams = requestBuilder.getViewAclResourcesInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_ACL_RESOURCES);
              rdoc = (Document)util.execute(dataRequest);
          
              dataRequest.setInputParams(null);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_PERMISSIONS);
              pdoc = (Document)util.execute(dataRequest);         
         
              // Read ACL with acl_number. This does not return distinct acl_ids
              // because the call also returns related roles.
              inputParams = requestBuilder.getViewAclInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_ACL);
              adoc = (Document) util.execute(dataRequest);     
 
              // Add permissions to each resource.
              // Mark the permission associated with this ACL with attribute acl_number.
              rdoc = this.addPermissionToResource(rdoc, pdoc, adoc); 
        
              // Create document with root node "CONTEXT", attribute context_id, acl_number
              doc = XMLHelper.getDocumentWithRoot(ContextVO.XML_BOTTOM);
              ((Element)doc.getDocumentElement()).setAttribute
                  (AclVO.TB_CONTEXT_ID, (String)request.getParameter(AclVO.TB_CONTEXT_ID));
              ((Element)doc.getDocumentElement()).setAttribute
                  (AclVO.TB_ACL_NAME, (String)request.getParameter(AclVO.RQ_ACL_NAME));    
              logger.debug("Setting attribute context_id on root CONTEXT: " + request.getParameter(AclVO.RQ_CONTEXT_ID));
              logger.debug("Setting attribute acl_name on root CONTEXT: " + request.getParameter(AclVO.RQ_ACL_NAME));
          
              // Add resources tree to context document.
              DOMUtil.addSection(doc, rdoc.getElementsByTagName(ResourceVO.XML_TOP));
  
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_ACL, mapping);
        
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_ACL,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          } 
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }   
  }  

 /**
  * Sends View ACLs page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void viewAll(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering viewAll...");    
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;  // context document
      Document adoc = null; // acl document
      NodeList contexts = null;
      NodeList acls = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              inputParams = new HashMap();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              // Get all contexts
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXTS);
              doc = (Document) util.execute(dataRequest);

              // For each context, get ACLs.
              if (doc != null) {
                contexts = doc.getElementsByTagName(ContextVO.XML_BOTTOM);
              }
              if (contexts != null ) {
                for (int i = 0; i < contexts.getLength(); i++) 
                {  
                  // Get context_id element child off CONTEXT element
                  Element contextIdElem = (Element)((((Element)(contexts.item(i))).getElementsByTagName(ContextVO.TB_CONTEXT_ID)).item(0));
                  Text contextIdText = (Text)contextIdElem.getFirstChild();
                  String contextId = contextIdText.getNodeValue();        
                  
                  logger.debug("Processing context " + i + " with context_id " + contextId);
                  // get ACLs
                  dataRequest.setStatementID(StatementConstants.ACL_VIEW_CONTEXT_ACL_NAMES);
                  inputParams.put(AclVO.ST_CONTEXT_ID, contextId);
                  dataRequest.setInputParams(inputParams);
                  adoc = (Document)util.execute(dataRequest);
                  acls = adoc.getElementsByTagName(AclVO.XML_TOP);
                  // append ACLs to context node
                  ((Element)(contexts.item(i))).appendChild(doc.importNode(acls.item(0), true));
                }
              }
        
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_ACLLIST,mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_ACLLIST,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }
  }

 /**
  * Adds an ACL (name) and sends the ACL list page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void add(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering add...");   
      Connection con = null;
      String status = null;
      String message = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Map outputMap = null;
      String aclName = request.getParameter(AclVO.RQ_ACL_NAME);
      BigDecimal result = null;
      ArrayList rList = null; // list of resources
      ArrayList pList = null; // list of permissions
      String rIds = null; // string with resource ids
      String pIds = null; // string with permission ids
      String rid = null; // resource id index
      String pid = null; // permission id index
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      AbstractVO aclVO = null;
            
      try
      { 
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_CREATE ))  {      
              aclVO = new AclVO(request);
              if (!aclVO.isValid()) {
                  throw new RequiredFieldMissingException();
              } else {
                try {               
                    util = DataAccessUtil.getInstance();
                    dataRequest = new DataRequest();
                    con = DataRequestHelper.getDBConnection(); 
                    con.setAutoCommit(false);
                    dataRequest.setConnection(con);

                    // Check if acl_name exists.
                    inputParams = new HashMap();
                    inputParams.put(AclVO.ST_ACL_NAME, aclName);
                    dataRequest.setInputParams(inputParams);
              
                    dataRequest.setStatementID(StatementConstants.ACL_NAME_EXISTS);
                    status = (String)util.execute(dataRequest);
        
                    if ("Y".equalsIgnoreCase(status)) {
                      logger.debug("ACL Name Exists");
                      throw new DuplicateObjectException();
                    }

                    // Get list of resources
                    rIds = (String)request.getParameter(AclVO.RQ_RESOURCE_IDS);

                    if(rIds == null || rIds.length() == 0) {
                      throw new SecurityAdminException("7", "RESOURCE ID IS REQUIRED");
                    }

                    rList = (ArrayList)StringUtil.getIds(rIds);
                    dataRequest.setStatementID(StatementConstants.ACL_ADD_ACL);
                    for (int i = 0; i < rList.size(); i++) {
                        // Get list of permissions
                        rid = (String)rList.get(i);
                        pIds = (String) request.getParameter(AclVO.RQ_RESOURCE_ID + rid + AclVO.RQ_PERM_IDS);
                        if (rIds == null || rIds.length() == 0) {
                          throw new SecurityAdminException("7", "PERMISSION ID IS REQUIRED");
                        }
                        pList = (ArrayList) StringUtil.getIds(pIds);
                        for (int j = 0 ; j < pList.size(); j++) {
                            // Add ACL.
                            pid = (String)pList.get(j);
                            inputParams = requestBuilder.getAddAclInputParams
                              (request, aclName, rid, pid, ServletHelper.getMyId(securityToken));
                            dataRequest.setInputParams(inputParams);
                            outputMap = (Map)util.execute(dataRequest);
                            status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                            message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                            logger.debug("status is: " + status);
                            logger.debug("message is: " + message);

                            // check status
                            if (!"Y".equalsIgnoreCase(status))  {
                                logger.debug("FAILED TO ADD ACL.");
                                // "ERROR OCCURRED" is an unexpected error.
                                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                                    throw new SecurityAdminException("6");
                                } else {
                                    throw new SecurityAdminException("12", message);
                                }
                            } 
                        }
                    }
                    con.commit(); 
                } catch (Exception e) {
                    con.rollback();
                    logger.error(e);
                    throw e;
                }  finally {
                    con.setAutoCommit(true);
                    if (con != null) {
                      con.close();
                    }
                }
                this.viewAll(request, response, mapping);   
              }
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }    
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          // Rebuild XML tree and send back.
          Document doc = aclVO.rebuildTree(e);
          // Get context document. Selected context has resouce/permission subtree.
          Document cdoc = getContextDoc(request);
          // Mark selected resource/permission on context tree.
          cdoc = markContextDoc(cdoc, request);         
          // Combine documents.
          (doc.getDocumentElement()).appendChild(doc.importNode(cdoc.getDocumentElement(), true));
          
          stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDACL,mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDACL,mapping),ServletHelper.getParameterMap(request));          
      } catch (Exception ex) { 
        con.rollback();
        throw ex;
      } 
  }

  /**
   * Rebuild the context section of the tree. (CONTEXTS/CONTEXT/RESOURCES/RESOURCE)
   */
  private Document getContextDoc(HttpServletRequest request) 
  throws Exception {
      logger.debug("entering getContextDoc...");
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // context document
      Document rdoc = null; // resource document
      Document pdoc = null; // permission document
      String contextId = null;

      try {
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              // get all contexts
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXTS);
              doc = (Document) util.execute(dataRequest);

              if(doc != null && doc.getElementsByTagName(ContextVO.XML_BOTTOM) != null) {
        
                contextId = (String)request.getParameter(AclVO.RQ_CONTEXT_ID);       
                if (contextId == null || contextId.length() == 0) {
                  // This is the default page. Send resources related to the first context.            
                  Element first = (Element)((doc.getElementsByTagName(ContextVO.XML_BOTTOM)).item(0));
                  if (first != null) {
                    // Get context_id element off CONTEXT element.
                    Element contextIdElem = (Element)((first.getElementsByTagName(ContextVO.TB_CONTEXT_ID)).item(0));
                    Text contextIdText = (Text)contextIdElem.getFirstChild();
                    contextId = contextIdText.getNodeValue();
                  }
                }
          
                // for selected context, get associated resources
                inputParams = requestBuilder.getContextResourcesInputParams(request, contextId);
                dataRequest.setInputParams(inputParams);
                dataRequest.setStatementID(StatementConstants.ACL_VIEW_CONTEXT_RESOURCES);
                rdoc = (Document)util.execute(dataRequest);
                   
                if (rdoc != null) {
                  // Find the CONTEXT element that has a child context_id element with the contextId value.
                  Element contextElem = XMLHelper.findElementByChild(doc, ContextVO.TB_CONTEXT_ID, contextId);
            
                  if(contextElem != null) {
                      // get permissions
                      dataRequest.setStatementID(StatementConstants.ACL_VIEW_PERMISSIONS);
                      dataRequest.setInputParams(null);
                      pdoc = (Document)util.execute(dataRequest);
                
                      // add the permission tree to each resource node.
                      if (pdoc != null) {
                          NodeList rList = rdoc.getElementsByTagName(ResourceVO.XML_BOTTOM);
                          if(rList != null) {
                              for (int j = 0; j < rList.getLength(); j++) {
                                  ((Element)(rList.item(j))).appendChild(rdoc.importNode((Element)(pdoc.getElementsByTagName(PermissionVO.XML_TOP).item(0)),true));
                              }
                          }
                      }          
                      // add the resource tree to the context node.
                      contextElem.appendChild(doc.importNode((Element)(rdoc.getElementsByTagName(ResourceVO.XML_TOP).item(0)),true));        
                  }
                }
              }
          return doc;
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {
          if (con != null) {
              con.close();
          }
      }
  }


  /**
   * Analyze the request. Mark the selected resource and permission 
   * on contextDoc, with attribute 'selected' = 'Y'; 
   * @param contextDoc context document CONTEXTS/CONTEXT/RESOURCES/RESOURCE/PERMISSION/PERMISSION
   * @param request http servlet request
   * @throws Exception
   */
  private Document markContextDoc(Document contextDoc, HttpServletRequest request) throws Exception {
        String rIds = null; // resource id string
        String pIds = null; // permission id string
        ArrayList rList = null; // resource list
        ArrayList pList = null; // permission list
        String rid = null; // resource id
        String pid = null; // permission id
        String resource = null; // resource
        String permission = null; // permission
        
        // Get list of selected resources
        rIds = (String)request.getParameter(AclVO.RQ_RESOURCE_IDS);      
        rList = (ArrayList)StringUtil.getIds(rIds);
     
        for (int i = 0; i < rList.size(); i++) {
            rid = (String)rList.get(i);
            resource = request.getParameter(AclVO.RQ_RESOURCE_ID + rid);
            // Find this resource in context doc and mark it.
            Element resourceElem = XMLHelper.findElementByChild(contextDoc, ResourceVO.TB_RESOURCE_ID, resource);
            if(resourceElem != null) {
                resourceElem.setAttribute(SecurityAdminConstants.XML_ATTR_SELECTED, SecurityAdminConstants.COMMON_VALUE_YES);
            }
            // Get list of selected permissions
            pIds = (String) request.getParameter(AclVO.RQ_RESOURCE_ID + rid + AclVO.RQ_PERM_IDS);
            pList = (ArrayList) StringUtil.getIds(pIds);
            for (int j = 0 ; j < pList.size(); j++) {
                pid = (String)pList.get(j);
                permission = request.getParameter
                  (AclVO.RQ_RESOURCE_CB + rid + AclVO.RQ_PERMISSION_CB + pid);
                // Find this permission in context doc and mark it.  
                Element permissionElem = XMLHelper.findElementByChild
                  (resourceElem, PermissionVO.TB_PERMISSION_ID, permission);
                if (permissionElem != null) {
                  permissionElem.setAttribute
                    (SecurityAdminConstants.XML_ATTR_SELECTED, SecurityAdminConstants.COMMON_VALUE_YES);
                }
            }
        }
        return contextDoc;
  }
  
 /**
  * Removes ACL name(s) and refreshes the page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void remove(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering remove...");
      logger.debug("Retrieved these acl_numbers to be removed " 
        + request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
      
      Connection con = null;
      DataAccessUtil util = null;
      HashMap inputParams = null;
      DataRequest dataRequest = null;
      Map outputMap = null;
      ArrayList ids = null; // This list contains the acl_numbers
      String status = null;
      String message = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_REMOVE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              con.setAutoCommit(false); 
              dataRequest.setConnection(con);
              ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
        
              for (int i = 0; i < ids.size(); i++) 
              {        
                  inputParams = requestBuilder.getRemoveAclNameInputParams((String)ids.get(i));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.ACL_REMOVE_ACL_NAME);
                  outputMap = (Map) util.execute(dataRequest);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);        

                  logger.debug("status is: " + status);
                  logger.debug("message is: " + message);

                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO REMOVE ACL NAME.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                          throw new SecurityAdminException("6");
                      } else {
                          throw new SecurityAdminException("12", message);
                      }
                  } 
              }
              con.commit(); 
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        con.rollback();
        throw ex;
      }  finally {
        con.setAutoCommit(true);
        if (con != null) {
          con.close();
        }
      }  
      this.viewAll(request, response, mapping);
  }

 /**
  * Updates an ACL (name).
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering update...");   
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document adoc = null; // ACL document for old ACL
      ArrayList oldAclList = null; // list of old VOs
      ArrayList newAclList = new ArrayList(); // list of new VOs
      String rIndices = null; // string that has resource indices for the ACL
      String pIndices = null; // string that has permission indices for the ACL
      ArrayList rList = null; // list of resource indices
      ArrayList pList = null; // list of permission indices
      String aclName = (String)request.getParameter(AclVO.RQ_ACL_NAME);
      String contextId = (String)request.getParameter(AclVO.RQ_CONTEXT_ID);
      Map outputMap = null;
      String status = null;
      String message = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
 
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ACL, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              con.setAutoCommit(false);
              dataRequest.setConnection(con);

              // Call view_acl with acl_name. Save distinct AclVO's to oldAclList.
              inputParams = requestBuilder.getViewAclInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_ACL);
              adoc = (Document) util.execute(dataRequest);
              oldAclList = getAclVOs(adoc);

              // Get the list of resources from request
              rIndices = (String)request.getParameter(AclVO.RQ_RESOURCE_IDS);
              rList = StringUtil.getIds(rIndices);
              logger.debug("Receiving new resource id string=" + rIndices);

              // For each resource, get the permission list.
              for (int i = 0; i < rList.size(); i++) {
                  String rid = (String)rList.get(i);
                  String resource = (String)request.getParameter(AclVO.RQ_RESOURCE_ID + rid);
                  pIndices = (String)request.getParameter(AclVO.RQ_RESOURCE_ID + rid + AclVO.RQ_PERM_IDS);
                  pList = StringUtil.getIds(pIndices);
                  logger.debug("\tReceiving new Permission id string=" + pIndices);

                  // For each resource permission combination, create AclVo and save to newAclList.
                  // If AclVO doesn't exist in oldAclList, call add_acl to add it.
                  for (int j = 0; j < pList.size(); j++) {
                      String pid = (String)pList.get(j);
                      String permission = (String)request.getParameter
                          ("resourceCB" + rid + "permissionCB" + pid);
                      logger.debug("Add VO to newAclList: context=" + contextId 
                          + ",resource=" + resource + ",permission=" + permission);
                      AclVO newVO = new AclVO(contextId, resource, permission);
                      newAclList.add(newVO);

                      // If VO exists, it stays the same. Do nothing.
                      if (!voExists(oldAclList, resource, permission)) {
                          // This is a new acl to this acl_name. Add it.
                          inputParams = requestBuilder.getAddAclInputParams
                            (request, aclName, rid, pid, ServletHelper.getMyId(securityToken));
                          dataRequest.setInputParams(inputParams);
                          dataRequest.setStatementID(StatementConstants.ACL_ADD_ACL);
                          outputMap = (Map)util.execute(dataRequest);

                          status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                          message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                          logger.debug("status is: " + status);
                          logger.debug("message is: " + message);

                          // check status
                          if (!"Y".equalsIgnoreCase(status))  {
                              logger.debug("FAILED TO ADD ACL.");
                              // "ERROR OCCURRED" is an unexpected error.
                              if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                                  throw new SecurityAdminException("6");
                              } else {
                                  throw new SecurityAdminException("12", message);
                              }
                          }
                    
                      } 
                  }   
              }

              // For each acl that's in oldAclList and not in newAclList, call remove_acl to remove.
              for(int k = 0; k < oldAclList.size(); k++) {
                  AclVO oldAcl = (AclVO)oldAclList.get(k);
                  String resource = oldAcl.getResource();
                  String permission = oldAcl.getPermission();
                  oldAcl.setAclName(aclName);
                  if (!voExists(newAclList, resource, permission)) {
                      // call remove
                      inputParams = requestBuilder.getRemoveAclInputParams(oldAcl);
                      dataRequest.setInputParams(inputParams);
                      dataRequest.setStatementID(StatementConstants.CONTEXT_REMOVE_ACL_RP);
                      outputMap = (Map)util.execute(dataRequest);

                      status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                      message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                      logger.debug("status is: " + status);
                      logger.debug("message is: " + message);

                      // check status
                      if (!"Y".equalsIgnoreCase(status))  {
                          logger.debug("FAILED TO REMOVE ACL.");
                          // "ERROR OCCURRED" is an unexpected error.
                          if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                              throw new SecurityAdminException("6");
                          } else {
                              throw new SecurityAdminException("12", message);
                          }
                      }               
                  }
              }
        
              con.commit();        
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        logger.debug("Rolling back database changes...");
        con.rollback();
        throw ex;
      } finally {
        con.setAutoCommit(true);
        if (con != null)  {
          con.close();
        }
      }  
      this.view(request, response, mapping);   
      
  } 

 /**
  * Adds permission doc to each resource node.
  * @param rdoc resource document
  * @param pdoc permission document
  * @param adoc ACL document
  * @throws Exception
  */
  private Document addPermissionToResource(Document rdoc, Document pdoc, Document adoc)
  throws Exception
  {
      logger.debug("Merging resource and permission tree...");
      // Add permission document to each resource node.
      // For each resource node that has an acl_name attribute (is related to acl),
      // also mark the acl_name on permission node.
      
      // Create distinct ACL VO from adoc (It's not distinct because of role info).
      ArrayList aclVOs = getAclVOs(adoc); // List of AclVOs
      String resource = null;
      String permission = null;
      String aclName = null;
      
      NodeList rList = rdoc.getElementsByTagName(ResourceVO.XML_BOTTOM);
      if (rList != null )  {
          for (int i = 0; i < rList.getLength(); i++) {
              Element resourceElem = (Element)rList.item(i);
              Element resourceIdElem = (Element)((resourceElem.getElementsByTagName(ResourceVO.TB_RESOURCE_ID)).item(0));
              Text resourceIdText = (Text)resourceIdElem.getFirstChild();
              resource = resourceIdText.getNodeValue();
              resourceElem.appendChild(rdoc.importNode((pdoc.getElementsByTagName(PermissionVO.XML_TOP)).item(0),true));

              // For the resources that have an acl_name, mark appropiate perission
              Element aclNameElem = (Element)((resourceElem.getElementsByTagName(AclVO.TB_ACL_NAME)).item(0));
              Text aclNameText = (Text)aclNameElem.getFirstChild();
              if (aclNameText != null) {
                  aclName = aclNameText.getNodeValue();
              }
              logger.debug("Resource=" + resource + ",acl_name=" + aclName);
              if (aclName != null && aclName.length() > 0) {
                  // Get PERMISSION nodes under this resource.
                  NodeList pList = ((resourceElem.getElementsByTagName(PermissionVO.XML_TOP).item(0))).getChildNodes();
                  for (int j = 0; j < pList.getLength(); j++) {
                      String aclID = null;
                      Element permissionElem = (Element)pList.item(j);
                      Element permissionIdElem = (Element)((permissionElem.getElementsByTagName(PermissionVO.TB_PERMISSION_ID)).item(0));
                      Text permissionIdText = (Text)permissionIdElem.getFirstChild();
                      permission = permissionIdText.getNodeValue();
                      logger.debug("Looking for resource=" + resource + ", permission=" + permission);
                      
                      if (voExists(aclVOs, resource, permission)) {
                          // This vo is related to the acl name. Mark it.
                          permissionElem.setAttribute(AclVO.TB_ACL_NAME, aclName);
                          logger.debug("Found aclName="+ aclName + " resource=" + resource + " permission=" + permission);
                      }
                  }
              }
     
          }
      }
      return rdoc;
  }  

 /**
  * Generates and returns an ArrayList of AclVO's from the ACL document.
  * @param adoc ACL document
  * @throws Exception
  */
  private ArrayList getAclVOs(Document adoc) 
  throws Exception
  {  
      NodeList aList = adoc.getElementsByTagName(AclVO.XML_BOTTOM);
      ArrayList voList = new ArrayList(); // the list to return with distinct AclVOs
      Element elem = null;
      String aclId = null;
      String context = null;
      String resource = null;
      String permission = null;
      
      if (aList != null )  {
          for (int i = 0; i < aList.getLength(); i++) { 
              elem = (Element)aList.item(i);
              context = elem.getAttribute(AclVO.TB_CONTEXT_ID);
              resource = elem.getAttribute(AclVO.TB_RESOURCE_ID);
              permission = elem.getAttribute(AclVO.TB_PERMISSION_ID);
              logger.debug("VIEW_ACL returned context=" + context
                + ",resource=" + resource + ",permission=" + permission);

              // Only add distinct acl_id to list. Context is assumed.
              if(!voExists(voList, resource, permission)) {
                  logger.debug("Creating AclVO and adding it to voList...");
                  AclVO vo = new AclVO(context, resource, permission);
                  voList.add(vo);
              }
          }
      }
      return voList;
  } 

  /**
   * Checks if a VO with the resource and permission exists in list. 
   * With the given context, resource and permission combined is unique.
   * Returns true if found, false otherwise.
   * @param voList List of VOs
   * @param resource resource id
   * @param permission permission id
   * @return boolean
   * @throws Exception
   */
  private boolean voExists(ArrayList voList, String resource, String permission) 
  throws Exception
  {
      boolean found = false;

      for(int i = 0; i < voList.size(); i++) {
          AclVO vo = (AclVO)voList.get(i);
          if (resource.equals(vo.getResource()) && permission.equals(vo.getPermission())) {
            found = true;
            break;
          }
      }

      return found;
  }      
  
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    } 
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}