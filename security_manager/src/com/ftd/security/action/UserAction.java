package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.statements.CStmtReturnXMLDOM;
import com.ftd.osp.utilities.dataaccess.util.ResultSet2XML;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.json.JSONUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.UserDataRequestBuilder;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.cache.vo.UserVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.manager.UserManager;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add user, update user,
 * remove user, view user, and view all users.
 * @author Christy Hu
 */
public class UserAction extends Action  {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.UserAction";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private UserDataRequestBuilder requestBuilder = new UserDataRequestBuilder();
  private SecurityManager securityManager;
  private File errorStylesheet;
  private String errorStylesheetName;

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering UserAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
    if (action == null) {
        // not a forwarded request
        action = request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    }
    String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);

    try  {
        requestBuilder = new UserDataRequestBuilder();
        this.securityManager = SecurityManager.getInstance();
        this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
        this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
      // authentication call
      if (ServletHelper.isValidToken(context, securityToken))  {
          if (action != null)  {
            if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW))  {
                this.view(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_ALL))  {
                this.viewAll(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
                this.add(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
                this.remove(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
                this.update(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
                this.displayAdd(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REFRESH_DISPLAY_ADD))  {
                this.refreshDisplayAdd(request, response, mapping);                
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_SEARCH))  {
                this.displaySearch(request, response, mapping);
            } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_SEARCH))  {
                this.search(request, response, mapping);
            } else if(action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_USER_SEARCH)) 
            {
            	this.searchUser(request, response, mapping);
            }
            else {
                // REQUEST UNKNOWN
                logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
                ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
            }
          }
          else
          {
            logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
            ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
          }
        } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          logger.error(securityToken);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
        }

    } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } 
    return null;
  }

 /**
  * Sends View User page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void view(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering view...");
      Connection con = null;
      Document doc = null; // resulting document
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_VIEW ))  {
            con = DataRequestHelper.getDBConnection();
            UserManager userManager = new UserManager();
            
            doc = userManager.buildViewUserDoc(request, con);
    
            TraxUtil traxUtil = TraxUtil.getInstance();
            File stylesheet = getXSL(SecurityAdminConstants.FN_UPDATE_USER, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_UPDATE_USER,mapping),ServletHelper.getParameterMap(request));

        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }

  }

 /**
  * Sends View Users page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  * @deprecated
  */
  private void viewAll(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering viewAll...");
      Connection con = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_VIEW))  {
      
            con = DataRequestHelper.getDBConnection();
            UserManager userManager = new UserManager();
            doc = userManager.getViewUsersDoc(con);

            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_USERLIST, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_USERLIST,mapping),ServletHelper.getParameterMap(request));
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }
  }

 /**
  * Displays Create User page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering displayAdd...");
      String xslFile = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE))  {
            UserManager userManager = new UserManager();
            doc = userManager.buildDisplayAddUserDoc();
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDUSER, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDUSER,mapping),ServletHelper.getParameterMap(request));
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
      }
  }


 /**
  * Displays Create User page when context selection changes. Rebuilds tree but does not validate data.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void refreshDisplayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering refreshDisplayAdd...");
      String xslFile = null;
      Document doc = null;
    
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE))  {
            UserVO userVO = new UserVO(request);
            IdentityVO identityVO = new IdentityVO(request);
            UserManager userManager = new UserManager(userVO, identityVO);
            doc = userManager.buildRefreshDisplayAddUserDoc();
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDUSER, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDUSER,mapping),ServletHelper.getParameterMap(request));
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
      }
  }
 /**
  * Adds a user and sends back View User page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void add(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("Entering add...");
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO userVO = null;
      IdentityVO identityVO = null;
      UserManager userManager = null;
      String credential = null;
      int userId = 0;
      BigDecimal supervisorId = null;

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE))  {
            
            con = DataRequestHelper.getDBConnection();
            con.setAutoCommit(false);
            
            userVO = new UserVO(request);
            
            /*String supervisorIdenitity = request.getParameter(UserVO.RQ_SUPERVISOR_ID);
            if(supervisorIdenitity != null && supervisorIdenitity.trim().length() > 0) {
            	try {
                    HashMap inputParams = new HashMap();
                	inputParams.put("in_user_identity", supervisorIdenitity);
                	supervisorId = (BigDecimal)DataRequestHelper.executeStatement
                            (con, inputParams , StatementConstants.GET_USERID_FROM_IDENTITY);
        			logger.debug("supervisorId is: " + supervisorId);
            	}
                catch(Exception e){
                	logger.error("Exception occured while getting supervisor Id", e);
                }
            	if(supervisorId != null) {
            		userVO.setSupervisorId(supervisorId.toString());
            	} else {
            		userVO.setSupervisorId(UserVO.INVALID_SUPERVISOR_ID);
            	}
            }    */        	
            identityVO = new IdentityVO(request);
            userManager = new UserManager(userVO, identityVO);
            
            try {
                if (!userVO.isValid() || !identityVO.isValid(SecurityAdminConstants.ACTION_PARM_ADD) 
                		|| UserVO.INVALID_SUPERVISOR_ID.equals(userVO.getSupervisorId())) {
                    throw new RequiredFieldMissingException();
                }            
                userId = userManager.addUser(securityToken, con);
                credential = userManager.addIdentity(securityToken, con, userId);
                userManager.addIdentityRoles(securityToken, con);
                request.setAttribute(UserVO.RQ_USER_ID, new Integer(userId));
                request.setAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credential);
                con.commit();
            } catch (Exception e) {
                con.rollback();
                logger.error(e);
                throw e;
            } finally {
                if (con != null) {
                  con.setAutoCommit(true);
                  con.close();
                }
            }
            this.view(request, response, mapping);
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          Document doc = userManager.buildInvalidAddUserRequestDoc(e);
          
          File stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDUSER, mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDUSER,mapping),ServletHelper.getParameterMap(request));             
      } catch (Exception e) {
          logger.error(e);
          throw e;
      }  finally {
          if (con != null) {
             con.close();
          }
      }
  }


 /**
  * Remove the user and sends View Users page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  * @deprecated
  */
  private void remove(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering remove...");
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_REMOVE))  {
            util = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
            con = DataRequestHelper.getDBConnection();

            dataRequest.setConnection(con);
            inputParams = requestBuilder.getRemoveUserInputParams(request);
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID(StatementConstants.USERS_REMOVE_USER);
            outputMap = (Map) util.execute(dataRequest);
            status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO REMOVE USER.");
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
                } else {
                    throw new SecurityAdminException("12", message);
                }
            }

        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }
      //forward request to view all
      this.viewAll(request, response, mapping);
  }

 /**
  * Updates the user info and sends back View User page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering update...");

      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserManager userManager = null;
      BigDecimal supervisorId = null;
      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken,
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_UPDATE))  {
            try {
                con = DataRequestHelper.getDBConnection();
                con.setAutoCommit(false);
                UserVO user = new UserVO(request);

                /*String supervisorIdenitity = request.getParameter(UserVO.RQ_SUPERVISOR_ID);
                if(supervisorIdenitity != null && supervisorIdenitity.trim().length() > 0) {
                	try {
                        HashMap inputParams = new HashMap();
                    	inputParams.put("in_user_identity", supervisorIdenitity);
                    	supervisorId = (BigDecimal)DataRequestHelper.executeStatement
                                (con, inputParams , StatementConstants.GET_USERID_FROM_IDENTITY);
            			logger.debug("supervisorId is: " + supervisorId);
                	}
                    catch(Exception e){
                    	logger.error("Exception occured while getting supervisor Id", e);
                    }
                	if(supervisorId != null) {
                		user.setSupervisorId(supervisorId.toString());
                	} else {
                		user.setSupervisorId(UserVO.INVALID_SUPERVISOR_ID);
                	}
                }*/   
                
                userManager = new UserManager(user, null);
           
                if (!user.isValid()) {
                    throw new RequiredFieldMissingException();
                }
                userManager.updateUser(con, securityToken);
                con.commit();
            } catch (Exception e) {
                con.rollback();
                logger.error(e);
                throw e;
            } finally {      
                if (con != null)  {
                    con.setAutoCommit(true);
                }
            }
            //forward request to view
            this.view(request, response, mapping);
          
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException is caught here.
          Document doc = userManager.buildInvalidUpdateUserRequestDoc(e, con);
          
          File stylesheet = getXSL(SecurityAdminConstants.FN_UPDATE_USER, mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_UPDATE_USER,mapping),ServletHelper.getParameterMap(request)); 
        
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {      
          if (con != null)  {
              con.close();
          }
      }
  }


 /**
  * Displays User Search page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displaySearch(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering displaySearch...");
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      HashMap parameters = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      String searchOption = request.getParameter(UserVO.RQ_SEARCH_OPTION);
      String searchKey = request.getParameter(UserVO.RQ_SEARCH_KEY);
      Object error = request.getAttribute(SecurityAdminConstants.ERROR);
      
      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_VIEW))  {
            doc = DOMUtil.getDocument();
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_SEARCH, mapping);
            parameters = ServletHelper.getParameterMap(request);
            parameters.put(UserVO.RQ_SEARCH_OPTION, searchOption == null? "" : searchOption);
            parameters.put(UserVO.RQ_SEARCH_KEY, searchKey == null? "" : searchKey);
            parameters.put(SecurityAdminConstants.ERROR, error == null? "" : (String)error);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_SEARCH,mapping),parameters);
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } 
  }  


 /**
  * Returns user search result.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void search(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering search...");

      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Map outputMap = null;
      Document doc = null;
      Document rdoc = null; //role document
      File stylesheet = null;
      TraxUtil traxUtil = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken,
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_VIEW))  {

            String searchOption = (String) request.getParameter(UserVO.RQ_SEARCH_OPTION);
            if(searchOption == null || searchOption.length() == 0) {
                throw new RequiredFieldMissingException();
            }
            util = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
            con = DataRequestHelper.getDBConnection();
            dataRequest.setConnection(con);

            // Get identities
            inputParams = requestBuilder.getSearchUserInputParams(request);
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID(StatementConstants.USERS_SEARCH_USERS);
            doc = (Document)util.execute(dataRequest);

            // Add role section to identity
            NodeList identityList = doc.getElementsByTagName(UserVO.XML_BOTTOM);
            if (identityList != null ) {
                for(int i = 0; i < identityList.getLength(); i++ ) {
                    // Get roles for each identity
                    Element identityElem = (Element)identityList.item(i);
                    Element identityIdElem = (Element)(identityElem.getElementsByTagName(IdentityVO.TB_IDENTITY_ID)).item(0);
                    Text identityIdText = (Text)identityIdElem.getFirstChild();
                    String identity = identityIdText.getData();
                    
                    inputParams = new HashMap();
                    inputParams.put(IdentityVO.ST_IDENTITY_ID, identity);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID(StatementConstants.USERS_SEARCH_IDENTITY_ROLES);
                    rdoc = (Document)util.execute(dataRequest);
                    System.out.println(rdoc.getXmlEncoding());
                    //((XMLDocument)rdoc).print(System.out);

                    // Add role document to identity document.
                    identityElem.appendChild(doc.importNode(rdoc.getDocumentElement(), true));
                }

            }
            
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_SEARCHUSER_RESULT, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_SEARCHUSER_RESULT,mapping),ServletHelper.getParameterMap(request));

            
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (RequiredFieldMissingException e) {
        request.setAttribute(SecurityAdminConstants.ERROR, SecurityAdminConstants.ERROR_SEARCH_OPTION_REQUIRED);
        this.displaySearch(request, response, mapping);
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null)  {
          con.close();
        }
      }
  }  
  
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }   
    
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    

    //15689 changes
    private void searchUser(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering search...");

      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      Document doc = null;
      Document rdoc = null; //role document
      File stylesheet = null;
      TraxUtil traxUtil = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken,
        SecurityAdminConstants.Resource_RESET_PASSWORD, SecurityAdminConstants.PERMISSION_VIEW))  {

            String searchOption = (String) request.getParameter(UserVO.RQ_SEARCH_OPTION);
            if(searchOption == null || searchOption.length() == 0) {
                throw new RequiredFieldMissingException();
            }
            util = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
            con = DataRequestHelper.getDBConnection();
            dataRequest.setConnection(con);

            // Get identities
            inputParams = requestBuilder.getSearchUserInputParams(request);
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID(StatementConstants.USERS_SEARCH_USERS);
            doc = (Document)util.execute(dataRequest);

            response.setContentType("application/json");
            response.setContentType("text/xml");
        	response.getWriter().write(JSONUtil.xmlToJSON(doc));        	
            
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (RequiredFieldMissingException e) {
        request.setAttribute(SecurityAdminConstants.ERROR, SecurityAdminConstants.ERROR_SEARCH_OPTION_REQUIRED);
        this.displaySearch(request, response, mapping);
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null)  {
          con.close();
        }
      }
  }
    
    
}
