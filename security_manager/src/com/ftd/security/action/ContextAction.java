package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.ContextDataRequestBuilder;
import com.ftd.security.cache.vo.AbstractVO;
import com.ftd.security.cache.vo.ContextConfigVO;
import com.ftd.security.cache.vo.ContextVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add context, view/update context
 * remove context, list contexts, add/view/update/remove context config.
 * @author Christy Hu
 */
public class ContextAction extends Action  {

  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.ContextAction";
  private ContextDataRequestBuilder requestBuilder;
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private String errorStylesheetName;
  private SecurityManager securityManager;
  private File errorStylesheet;
  
 /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering ContextAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);
    
    try  {
        requestBuilder = new ContextDataRequestBuilder();
        this.securityManager = SecurityManager.getInstance();
        this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
        this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
        
        // authentication call
        if (ServletHelper.isValidToken(context, securityToken))  {     
            if (action != null)  {    
              if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW))  {
                  this.view(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_ALL))  {
                  this.viewAll(request, response, mapping); 
              }else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
                  this.add(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
                  this.remove(request, response, mapping);   
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
                  this.update(request, response, mapping);                
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_MANAGE_CONFIG))  {
                  this.manageConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_CONFIG))  {
                  this.viewConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD_CONFIG))  {
                  this.displayAddConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD_CONFIG))  {
                  this.addConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE_CONFIG))  {
                  this.removeConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE_CONFIG))  {
                  this.updateConfig(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
                  this.displayAdd(request, response, mapping);            
              }  else {
                  logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
                  ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
              }        
            }
            else 
            {
              logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
              ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
            }
        } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          logger.error(securityToken);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
        }            
        
    } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } finally  {
    }
      return null;
  }

 /**
  * Displays view context page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void view(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {        
      logger.debug("entering view...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // the resulting document to be transformed.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
          
              inputParams = requestBuilder.getViewContextInputParams(request);
              dataRequest.setInputParams(inputParams);
              // Get context and associated context configs.
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXT);
              doc = (Document)util.execute(dataRequest);

              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_CONTEXT,mapping);        
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_CONTEXT,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }      
  }

 /**
  * Displays view contexts page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void viewAll(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering viewAll...");    
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;  // context document
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_VIEW ))  {       
              doc = this.getViewContextsDoc();
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_CONTEXTLIST,mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_CONTEXTLIST,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } 
  }

/**
  * Displays Create Context page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering displayAdd...");
      String xslFile = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_CREATE ))  { 
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDCONTEXT,mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDCONTEXT,mapping),ServletHelper.getParameterMap(request));
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } 
  }
  
 /**
  * Adds the context.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void add(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering add...");   
      Connection con = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;
      AbstractVO contextVO = null;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_CREATE ))  {       
              // Determine if required fields are missing.
              contextVO = new ContextVO(request);
              if (!contextVO.isValid()) {
                  throw new RequiredFieldMissingException();
              } else {
                try { 
                    DataAccessUtil util = DataAccessUtil.getInstance();
                    DataRequest dataRequest = new DataRequest();
                    con = DataRequestHelper.getDBConnection();   
                    dataRequest.setConnection(con);

                    // Check if context exists.
                    HashMap inputParams = requestBuilder.getContextExistsInputParams(request);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID(StatementConstants.CONTEXT_CONTEXT_EXISTS);
                    status = (String)util.execute(dataRequest);

                    if ("Y".equalsIgnoreCase(status)) { 
                        logger.error("CONTEXT EXISTS");
                        throw new DuplicateObjectException();
                    }
               
                    inputParams = requestBuilder.getAddContextInputParams
                      (request, ServletHelper.getMyId(securityToken));
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID(StatementConstants.CONTEXT_UPDATE_CONTEXT);
                    Map outputMap = (Map) util.execute(dataRequest);
                    status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                    message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                    logger.debug("status is: " + status);
                    logger.debug("message is: " + message);

                    // check status
                    if (!"Y".equalsIgnoreCase(status))  {
                        logger.debug("FAILED TO ADD CONTEXT.");
                        // "ERROR OCCURRED" is an unexpected error.
                        if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                             throw new SecurityAdminException("6");
                        } else {
                             throw new SecurityAdminException("12", message);
                        }
                    } 
                } catch (Exception e) {
                    logger.error(e);
                    throw e;
                }  finally {
                    if (con != null) {
                      con.close();
                    }
                }
                this.view(request, response, mapping);
              }                    
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }    
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          // Rebuild XML tree and send back.
          Document doc = contextVO.rebuildTree(e);
          stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDCONTEXT,mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDCONTEXT,mapping),ServletHelper.getParameterMap(request));             
      } catch (Exception ex) {
        throw ex;
      } finally {
        if (con != null) {
            con.close();
        }
      }
  }

 /**
  * Removes context(s).
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  * @throws InvalidRequestException when attempts to delete an object in use.
  * The exception is handled inside the function.
  */
  private void remove(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering remove...");
      logger.debug("Retrieved these id indices to be removed: " 
        + request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));

      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Connection con = null;
      HashMap inputParams = null;
      Map outputMap = null;
      ArrayList ids = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;
      int i = 0;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_REMOVE ))  { 
              try{
                  util = DataAccessUtil.getInstance();
                  dataRequest = new DataRequest();
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false);
                  dataRequest.setConnection(con);
        
                  // Retrieve the ids to be deleted.
                  ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
       
                  for (i = 0; i < ids.size(); i++) 
                  {        
                      inputParams = requestBuilder.getRemoveContextInputParams(request, (String)ids.get(i));
                      dataRequest.setInputParams(inputParams);
                      dataRequest.setStatementID(StatementConstants.CONTEXT_REMOVE_CONTEXT);
                      outputMap = (Map) util.execute(dataRequest);
                      status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                      message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);       

                      logger.debug("status is: " + status);
                      logger.debug("message is: " + message);

                      // check status
                      if (!"Y".equalsIgnoreCase(status))  {
                          logger.debug("FAILED TO REMOVE CONTEXT.");
                          // "ERROR OCCURRED" is an unexpected error.
                          if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                              throw new SecurityAdminException("6");
                          } else {
                              // Object is being used.
                              throw new InvalidRequestException();
                          }
                      }    
                  }
                  con.commit();
              } catch (Exception e) {
                  con.rollback();
                  throw e;
              } finally {
                  con.setAutoCommit(true);
                  if (con != null) {
                    con.close();
                  }
              }
              this.viewAll(request, response, mapping);
           
          } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }      
      } catch (InvalidRequestException ex) {
      
          // Rebuild XML tree and send back.
          Document doc = getViewContextsDoc();
          // Find context that throws this exception.
          String errorContext = request.getParameter(ContextVO.RQ_REMOVE_CONTEXT_ID + (String)ids.get(i));
          doc = markViewContextsDoc(request, doc, errorContext);
          stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_CONTEXTLIST, mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_CONTEXTLIST,mapping),ServletHelper.getParameterMap(request));              
      } catch (Exception ex ) {
          throw ex;
      } finally {
        if (con != null) {
            con.close();
        }
      } 
      
  }

 /**
  * Marks the document to be sent back due to error. 
  * 1. Mark an 'error' attribute='Object is being used.'
  * 2. Mark a 'selected' attribute='Y' if the context is selected in the request.
  * @param request HttpServletRequest
  * @param doc the context document
  * @param errorContext the context id that has errored out
  * @throws Exception
  */
  private Document markViewContextsDoc(HttpServletRequest request, Document doc, String errorContext) 
  throws Exception {
      // Find all contexts marked for deletion.
      ArrayList ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
      for (int i = 0; i < ids.size(); i++){
          String sContext = request.getParameter(ContextVO.RQ_REMOVE_CONTEXT_ID + (String)ids.get(i));
          
          // Find the context element with the same value and mark 'selected' attribute.
          Element contextElem = XMLHelper.findElementByChild(doc, ContextVO.TB_CONTEXT_ID, sContext);
          if (contextElem != null) {
              contextElem.setAttribute(SecurityAdminConstants.XML_ATTR_SELECTED, SecurityAdminConstants.COMMON_VALUE_YES);
          }
      }

      // Find the element in error.
      Element errorElem = XMLHelper.findElementByChild(doc, ContextVO.TB_CONTEXT_ID, errorContext);
      // Mark error attribute on element.
      if (errorElem != null) {
        errorElem.setAttribute(SecurityAdminConstants.XML_ATTR_ERROR, SecurityAdminConstants.ERROR_OBJECT_IN_USE);     
      }
      return doc;
  }
  
 /**
  * Returns the document as a result of view all contexts.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private Document getViewContextsDoc()
    throws Exception 
  {
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      
      try
      {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXTS);
              doc = (Document) util.execute(dataRequest);
              return doc;
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }    
  }
 /**
  * Updates a context.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering update...");   
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // context document
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;
 
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONTEXT, SecurityAdminConstants.PERMISSION_UPDATE ))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              inputParams = requestBuilder.getUpdateContextInputParams
                (request, ServletHelper.getMyId(securityToken));
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.CONTEXT_UPDATE_CONTEXT);
              outputMap = (Map) util.execute(dataRequest);

              status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
              message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
              logger.debug("status is: " + status);
              logger.debug("message is: " + message);

              // check status
              if (!"Y".equalsIgnoreCase(status))  {
                   logger.debug("FAILED TO UPDATE CONTEXT.");
                   // "ERROR OCCURRED" is an unexpected error.
                  if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                        throw new SecurityAdminException("6");
                  } else {
                        throw new SecurityAdminException("12", message);
                  }
              }
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally { 
        if (con != null)  {
          con.close();
        }
      } 
      this.view(request, response, mapping);
  } 

 /**
  * Displays context config page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void manageConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping )
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {        
      logger.debug("Entering manageConfig...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // the resulting document to be transformed.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_VIEW ))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              inputParams = requestBuilder.getViewContextInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXT);
              doc = (Document)util.execute(dataRequest);

              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_MANAGE_CONFIG, mapping);        
              traxUtil.transform(request, response, doc, stylesheet,getXslPathAndName(SecurityAdminConstants.FN_MANAGE_CONFIG,mapping) ,ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }       
  } 

 /**
  * Displays add config page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAddConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {        
      logger.debug("Entering displayAddConfig...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // context.
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      String sContextId = request.getParameter(ContextVO.RQ_CONTEXT_ID);
      logger.debug("Setting context_id to " + sContextId);
      if (sContextId == null || sContextId.length() == 0) {
          logger.error("context_id is null");
          throw new SecurityAdminException("INTERNAL ERROR");
      }

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_CREATE ))  {
              doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
              Element contextElem = doc.createElement(ContextVO.XML_BOTTOM);
              contextElem.setAttribute(ContextVO.TB_CONTEXT_ID, sContextId);
              ((Element)doc.getDocumentElement()).appendChild(contextElem);
              //doc = (Document)XMLHelper.getDocumentWithRoot(ContextVO.XML_BOTTOM);
              //((Element)doc.getDocumentElement()).setAttribute(ContextVO.TB_CONTEXT_ID, sContextId);

              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDCONFIG, mapping);        
              traxUtil.transform(request, response, doc, stylesheet,getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDCONFIG,mapping) ,ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }       
  } 

 /**
  * Displays view config page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void viewConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {    
    
      logger.debug("Entering viewConfig...");
      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // context document.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_VIEW ))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              // Call 
              inputParams = requestBuilder.getViewConfigInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONFIG);
              doc = (Document)util.execute(dataRequest);

              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_CONFIG, mapping);        
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_CONFIG,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      } 
      
  } 

 /**
  * Remove config.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void removeConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {      
      // Need to handle transaction   
      logger.debug("Entering removeConfig...");
      logger.debug("Retrieved these id indices to be removed: " 
        + request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));

      Connection con = null;
      String xslFile = null;
      HashMap inputParams = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null; // the resulting document to be transformed.
      ArrayList ids = null; // list for all ids to be removed.
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_REMOVE ))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              con.setAutoCommit(false);
              dataRequest.setConnection(con);

              ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
              for (int i = 0 ; i < ids.size(); i++) {
                  inputParams = requestBuilder.getRemoveConfigInputParams(request,(String)ids.get(i));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.CONTEXT_REMOVE_CONFIG);
                  outputMap = (Map)util.execute(dataRequest);

                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
                  logger.debug("status is: " + status);
                  logger.debug("message is: " + message);
                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                       logger.debug("FAILED TO REMOVE CONFIG.");
                       // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                            throw new SecurityAdminException("6");
                      } else {
                            throw new SecurityAdminException("12", message);
                      }
                  }
              }
              con.commit();
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        con.rollback();
        throw ex;
      } finally {
        con.setAutoCommit(true);
        if (con != null) {
          con.close();
        }
      }  
      this.manageConfig(request, response, mapping);
  }    

 /**
  * Updates config.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void updateConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering updateConfig...");   
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // context document
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;
 
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_UPDATE ))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              inputParams = requestBuilder.getUpdateConfigInputParams
                (request, ServletHelper.getMyId(securityToken));
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.CONTEXT_UPDATE_CONFIG);
              outputMap = (Map) util.execute(dataRequest);

              status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
              message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
              logger.debug("status is: " + status);
              logger.debug("message is: " + message);
              // check status
              if (!"Y".equalsIgnoreCase(status))  {
                   logger.debug("FAILED TO UPDATE CONFIG.");
                   // "ERROR OCCURRED" is an unexpected error.
                  if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                        throw new SecurityAdminException("6");
                  } else {
                        throw new SecurityAdminException("12", message);
                  }
              }
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) { 
        throw ex;
      } finally { 
        if (con != null)  {
          con.close();
        }
      } 
      this.manageConfig(request, response, mapping);
  } 

 /**
  * Adds the config.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void addConfig(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering addConfig...");   
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // context document
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      File stylesheet = null;
      AbstractVO config = null;
 
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_CONFIG, SecurityAdminConstants.PERMISSION_CREATE ))  {       
              // Determine if required fields are missing.
              config = new ContextConfigVO(request);
              if (!config.isValid()) {
                  throw new RequiredFieldMissingException();
              } else {
                try { 
                    util = DataAccessUtil.getInstance();
                    dataRequest = new DataRequest();

                    con = DataRequestHelper.getDBConnection();
                    dataRequest.setConnection(con);

                    // Check if config exists
                    inputParams = requestBuilder.getConfigExistsInputParams(request);
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID(StatementConstants.CONTEXT_CONFIG_EXISTS);
                    status = (String)util.execute(dataRequest);

                    if ("Y".equalsIgnoreCase(status)) {
                        logger.error("CONTEXT CONFIG EXISTS");
                        throw new DuplicateObjectException();
                    }
        
                    inputParams = requestBuilder.getAddConfigInputParams
                      (request, ServletHelper.getMyId(securityToken));
                    dataRequest.setInputParams(inputParams);
                    dataRequest.setStatementID(StatementConstants.CONTEXT_UPDATE_CONFIG);
                    outputMap = (Map) util.execute(dataRequest);

                    status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                    message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
                    logger.debug("status is: " + status);
                    logger.debug("message is: " + message);
                    // check status
                    if (!"Y".equalsIgnoreCase(status))  {
                         logger.debug("FAILED TO ADD CONFIG.");
                         // "ERROR OCCURRED" is an unexpected error.
                        if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                              throw new SecurityAdminException("6");
                        } else {
                              throw new SecurityAdminException("12", message);
                        }
                    }
                } catch (Exception e) {
                    logger.error(e);
                    throw e;
                }  finally {
                    if (con != null) {
                      con.close();
                    }
                }
                this.manageConfig(request, response, mapping);
              }                    
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          // Rebuild XML tree and send back.
          doc = config.rebuildTree(e);
          // Add context element.
          Element contextElem = doc.createElement(ContextVO.XML_BOTTOM);
          contextElem.setAttribute(ContextVO.TB_CONTEXT_ID, request.getParameter(ContextVO.RQ_CONTEXT_ID));
          ((Element)doc.getDocumentElement()).appendChild(contextElem);
          stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDCONFIG,mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDCONFIG,mapping),ServletHelper.getParameterMap(request));                   
      } catch (Exception ex) { 
        throw ex;
      } 
  }   
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }   
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}

