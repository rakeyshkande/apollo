package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.RoleDataRequestBuilder;
import com.ftd.security.cache.vo.AbstractVO;
import com.ftd.security.cache.vo.AclVO;
import com.ftd.security.cache.vo.RoleVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add role, view/update role,
 * remove roles, view role ACLs, and add/remove role ACLs.
 * @author Christy Hu
 */
public class RoleAction extends Action {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.RoleAction";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private RoleDataRequestBuilder requestBuilder = new RoleDataRequestBuilder();
  private SecurityManager securityManager; 
  private File errorStylesheet;
  private String errorStylesheetName;

/**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering RoseAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);
    
    try  {
      this.securityManager = SecurityManager.getInstance();
      this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
      this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
        // authentication call
        if (ServletHelper.isValidToken(context, securityToken))  {    
            if (action != null)  {    
              if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_ALL))  {
                  this.viewAll(request, response, mapping);     
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW)) {
                  this.view(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
                  this.displayAdd(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
                  this.add(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
                  this.remove(request, response, mapping);   
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
                  this.update(request, response, mapping);      
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_MAP_ACL))  {
                  this.mapACL(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD_ACL))  {
                  this.addACL(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE_ACL))  {
                  this.removeACL(request, response, mapping);                 
              }  else {
                  logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
                  ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
              }        
            }
            else 
            {
              logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
              ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
            } 
        } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
        }            
  } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } finally  {
    }
    return null;
  }


 /**
  * Sends View Roles page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void viewAll(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException,
           SecurityAdminException, 
           Exception 
  {
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      File stylesheet = null;
      TraxUtil traxUtil = null;
      HashMap parameters = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      logger.debug("Entering viewAll...");
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
              dataRequest.setStatementID(StatementConstants.ROLE_VIEW_ROLES);
              doc = (Document) util.execute(dataRequest);

              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_ROLELIST,mapping);       
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_ROLELIST,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error("Not Authorized to perform action");
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }    
  }

 /**
  * Sends the Add Role page with selections of contexts.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      Document rdoc = null; // response document
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_CREATE ))  {         
              doc = getViewContextsDoc();
              
              rdoc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
              rdoc.getDocumentElement().appendChild(rdoc.importNode(doc.getDocumentElement(), true));
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDROLE,mapping);
              traxUtil.transform(request, response, rdoc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDROLE,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } 
  }

 /**
  * Returns the document as a result of view all contexts.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private Document getViewContextsDoc()
    throws Exception 
  {
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      
      try
      {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXTS);
              doc = (Document) util.execute(dataRequest);
              return doc;
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }    
  }

 /**
  * Adds a role and sends back the View Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */  
  private void add(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering add..."); 
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      HashMap inputParams = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      BigDecimal roleId = null; // role id returned from SQL function call to get the next id.
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      AbstractVO role = null;
      
      try
      { 
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_CREATE ))  {  

              // Determine if required fields are missing.
              role = new RoleVO(request);
              if (!role.isValid()) {
                  throw new RequiredFieldMissingException();
              } else {
                try {           
                  util = DataAccessUtil.getInstance();
                  dataRequest = new DataRequest();
                  con = DataRequestHelper.getDBConnection();      
                  dataRequest.setConnection(con);
                  inputParams = requestBuilder.getAddRoleInputParams(request, ServletHelper.getMyId(securityToken));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.ROLE_ADD_ROLE);
                  outputMap = (Map) util.execute(dataRequest);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);    
                  // Returned next role id from SQL function call.
                  roleId = (BigDecimal)outputMap.get(RoleVO.RQ_ROLE_ID);

                  logger.debug("status is: " + status);
                  logger.debug("message is: " + message);
                  logger.debug("roleId is: " + roleId.intValue());

                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO ADD ROLE.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                          throw new SecurityAdminException("6");
                      } else if(message.equalsIgnoreCase("DUPLICATE ROLE/CONTEXT")) {
                          throw new DuplicateObjectException();
                      } else {
                          throw new SecurityAdminException("12", message);
                      }
                  }

                  request.setAttribute(RoleVO.RQ_ROLE_ID, new String(new Integer(roleId.intValue()).toString())); 
                } catch (Exception e) {
                    logger.error(e);
                    throw e;
                }  finally {
                    if (con != null) {
                      con.close();
                    }
                }
                this.view(request, response, mapping);
              }
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          // Rebuild XML tree and send back.
          doc = role.rebuildTree(e);
          Document cdoc = getViewContextsDoc();
          // Add context doc to rebuilt document 
          doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(),true));
          stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDROLE,mapping);
          traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDROLE,mapping),ServletHelper.getParameterMap(request));   
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } 
  }

 /**
  * Sends View Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void view(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering view...");  
      Connection con = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      { 
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              DataAccessUtil util = DataAccessUtil.getInstance();
              DataRequest dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();      
              dataRequest.setConnection(con);
        
              // Get document for role.
              HashMap inputParams = requestBuilder.getViewRoleInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ROLE_VIEW_ROLE);       
              Document doc = (Document) util.execute(dataRequest);

               // Get document for ACLs.
              inputParams = requestBuilder.getViewRoleAclInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ROLE_VIEW_ROLE_ACLS);
              Document adoc = (Document) util.execute(dataRequest);

              // Merget Role doc and ACL doc.
              // This includes all ACLs. XSL will filter out unassociated roles.
              DOMUtil.addSection(doc, adoc.getElementsByTagName(AclVO.XML_TOP));
              
              TraxUtil traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_ROLE, mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_ROLE,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex) {
          throw ex;
      } finally {
        if (con != null) {
          con.close();
        }
      }       
  }

 /**
  * Remove the role and sends back the View Roles page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */  
  private void remove(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {      
      logger.debug("Entering remove...");
      logger.debug("Retrieved these id indices to be removed: " + request.getParameter("remove_ids"));
       
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      ArrayList ids = null;
      HashMap inputParams = null; 
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_REMOVE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection(); 
              con.setAutoCommit(false);
              dataRequest.setConnection(con);

              ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
        
              for (int i = 0; i < ids.size(); i++) 
              {
                  inputParams = requestBuilder.getRemoveRoleInputParams(request, (String)ids.get(i));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.ROLE_REMOVE_ROLE);
                  outputMap = (Map) util.execute(dataRequest);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO REMOVE ROLE.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                          throw new SecurityAdminException("6");
                      } else {
                          throw new SecurityAdminException("12", message);
                      }
                  } 
              }
              con.commit();
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex ) {
          con.rollback();
          throw ex;
      } finally {
        con.setAutoCommit(true);
        if (con != null) {
          con.close();
        }
      } 
      this.viewAll(request, response, mapping);  
  }    

 /**
  * Updates the role information and sends back the View Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering update..."); 
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();      
              dataRequest.setConnection(con);
              inputParams = requestBuilder.getUpdateRoleInputParams(request, ServletHelper.getMyId(securityToken));
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ROLE_UPDATE_ROLE);
              outputMap = (Map) util.execute(dataRequest);
              status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
              message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);    
          
              logger.debug("status is: " + status);
              logger.debug("message is: " + message);

              // check status
              if (!"Y".equalsIgnoreCase(status))  {
                  logger.debug("FAILED TO UPDATE ROLE.");
                  // "ERROR OCCURRED" is an unexpected error.
                  if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                      throw new SecurityAdminException("6");
                  } else {
                      throw new SecurityAdminException("12", message);
                  }
              } 
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }
      this.view(request, response, mapping);
  }

 /**
  * Sends View Role ACLs page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */  
  private void mapACL(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering mapACL...");  
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null; // resulting document
      Document adoc = null; // acl document
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {  
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();      
              dataRequest.setConnection(con);
        
              // Get document for role.
              inputParams = requestBuilder.getViewRoleInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ROLE_VIEW_ROLE);       
              doc = (Document) util.execute(dataRequest);

               // Get document for ACLs.
              inputParams = requestBuilder.getViewRoleAclInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ROLE_VIEW_ROLE_ACLS);
              adoc = (Document) util.execute(dataRequest);

              // Merget Role doc and ACL doc.
              // This includes all ACLs. XSL will filter out unassociated roles.
              DOMUtil.addSection(doc, adoc.getElementsByTagName(AclVO.XML_TOP));

              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_MAP_ACLTOROLE, mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_MAP_ACLTOROLE,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error("Not Authorized to perform action");
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }
         
  }

 /**
  * Adds an ACL to the role and send back View Role ACL page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */  
  private void addACL(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering addACL..."); 
      logger.debug("Retrieved these ids to be added: " + request.getParameter(RoleVO.RQ_ACL_NUMBERS));
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      ArrayList ids = null;
      HashMap inputParams = null;
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      { 
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();  
              con.setAutoCommit(false);
              dataRequest.setConnection(con);
       
              ids = StringUtil.getIds(request.getParameter(RoleVO.RQ_ACL_NUMBERS));
              for (int i = 0; i < ids.size(); i++) 
              {
                  inputParams = requestBuilder.getAddRoleAclInputParams
                    (request, (String)ids.get(i), ServletHelper.getMyId(securityToken));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.ROLE_UPDATE_ROLE_ACL);
                  outputMap = (Map) util.execute(dataRequest);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);    

                  logger.debug("status is: " + status);
                  logger.debug("message is: " + message);        
                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO ADD ACL.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                          throw new SecurityAdminException("6");
                      } else {
                          throw new SecurityAdminException("12", message);
                      }
                  }
              } 
          
              con.commit();
          } else {
          // display 'Not Authorized' message
            logger.error("Not Authorized to perform action");
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }              
      } catch (Exception ex ) {
          con.rollback();
          throw ex;
      } finally {
        con.setAutoCommit(true);
        if (con != null) {
          con.close();
        }        
      }
      this.mapACL(request, response, mapping);   
  }

 /**
  * Removes an ACL from the role and sends back View Role ACL page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void removeACL(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
  {
      logger.debug("Entering removeACL..."); 
      logger.debug("Retrieved these ids to be removed: " + request.getParameter(RoleVO.RQ_ACL_NUMBERS));
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      ArrayList ids = null;
      HashMap inputParams = null; 
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_ROLE, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
            util = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
            con = DataRequestHelper.getDBConnection();  
            con.setAutoCommit(false);
            dataRequest.setConnection(con);
        
            ids = StringUtil.getIds(request.getParameter(RoleVO.RQ_ACL_NUMBERS));        
            for (int i = 0; i < ids.size(); i++) 
            {
                inputParams = requestBuilder.getRemoveRoleAclInputParams(request, (String)ids.get(i));
                dataRequest.setInputParams(inputParams);
                dataRequest.setStatementID(StatementConstants.ROLE_REMOVE_ROLE_ACL);
                outputMap = (Map) util.execute(dataRequest);
                status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                message = (String)outputMap.get(SecurityAdminConstants.MESSAGE); 

                logger.debug("status is: " + status);
                logger.debug("message is: " + message);
                // check status
                if (!"Y".equalsIgnoreCase(status))  {
                    logger.debug("FAILED TO REMOVE ACL.");
                    // "ERROR OCCURRED" is an unexpected error.
                    if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                        throw new SecurityAdminException("6");
                    } else {
                        throw new SecurityAdminException("12", message);
                    }
                }
            } 

            con.commit();
          } else {
          // display 'Not Authorized' message
            logger.error("Not Authorized to perform action");
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex ) {
          con.rollback();
          throw ex;
      } finally 
      {
        con.setAutoCommit(true);
        if (con != null) {
          con.close();
        }        
      }
      this.mapACL(request, response, mapping);   
  }

    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    } 
    
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }
}