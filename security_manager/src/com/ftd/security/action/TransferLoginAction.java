package com.ftd.security.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.util.ServletHelper;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Action class that handles single sign on.
 * Retrieves SSL url from database and redirects to the SSL Login page.
 * @author GaneshM
 */
public class TransferLoginAction extends Action {

    private static final String LOGGER_CATEGORY = "com.ftd.security.action.TransferLoginAction";
    private Logger logger = new Logger(LOGGER_CATEGORY);

    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
     public ActionForward execute(ActionMapping mapping, ActionForm form, 
         HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
        logger.info("Entering TransferLoginAction...");
        redirectToSecureLoginServlet(request,response);
        return null;
    }


    private void redirectToSecureLoginServlet(HttpServletRequest request, 
                                              HttpServletResponse response) {
        String protocol = "https://";
        StringBuffer url = new StringBuffer(protocol);
        try {
            String sitename = ServletHelper.getSiteName();
            String siteSslPort = ServletHelper.getSiteSslPort();

            String sitenameSsl = 
                ServletHelper.switchServerPort(sitename, siteSslPort);
            url.append(sitenameSsl);

            url.append("/secadmin/security/Login.do");
            logger.info("Redirecting to url=" + url);

            response.sendRedirect(url.toString());
        } catch (Exception e) {
            logger.error("redirectToLogin: Failed to redirect to URL: " + 
                         url.toString(), e);
        }
    }

    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    } 
}
