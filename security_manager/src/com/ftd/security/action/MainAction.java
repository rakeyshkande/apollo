package com.ftd.security.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.builder.AuthorizationBuilder;
import com.ftd.security.constants.MenuConstants;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;


/**
 * Action class that handles single sign on.
 * @author Christy Hu
 */
public class MainAction extends Action  {

  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.MainAction";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private String applicationContext;
  private String breURL;
  private String marsreportsURL;
    /**
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
        logger.info("Entering MainAction...");
        response.setContentType(CONTENT_TYPE);
        String context = null;
        String securityToken = null;
        String identity = null;
        String adminAction = null;
        //String isExit = null;
        String sourceMenu = null;

        try {

          if(ServletHelper.isMultipartFormData(request)
            && request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT) == null) {

              request = ServletHelper.formatMultipartRequest(request);
          }
          context = ServletHelper.getValueFromRequest(request, SecurityAdminConstants.COMMON_PARM_CONTEXT);
          securityToken = ServletHelper.getValueFromRequest(request, SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);


         applicationContext = ConfigurationUtil.getInstance().getProperty
               (SecurityAdminConstants.CONFIG_FILE,SecurityAdminConstants.COMMON_PARM_APP_CONTEXT);
        breURL = ConfigurationUtil.getInstance().getFrpGlobalParm(SecurityAdminConstants.CONFIG_CONTEXT, "MARS_BRE_URL");
        marsreportsURL = ConfigurationUtil.getInstance().getFrpGlobalParm(SecurityAdminConstants.CONFIG_CONTEXT, "MARS_REPORTS_URL");
          logger.debug("Retrieved context = " + context);
          logger.debug("Retrieved securityToken = " + securityToken);

          // authentication call
          if (ServletHelper.isValidToken(context, securityToken))  {
              // Redirect to specific menu based on adminAction.
              String forwardName = getRequestedMenu(request);
              this.land(request, response, forwardName, mapping);
          } else {
              logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
              // redirect to login page

              ServletHelper.redirectToLogin(request, response);
          }
        } catch (ExpiredSessionException ex) {
            // Session has been invalidated.
            logger.error(ex);
            ServletHelper.redirectToLogin(request, response);
        } catch (Exception ex)  {
            logger.error(ex);
            // Send to login page.
            ServletHelper.redirectToLogin(request, response);
        }
        return null;
  }


  /**
   * Returns the menu name based on request action.
   * @param adminAction request action
   * @return String XSL menu file name
   */
  private String getRequestedMenu(HttpServletRequest request) throws Exception {
      String adminAction = ServletHelper.getValueFromRequest
        (request, SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);

      // Check the session attribute to determine the source menu
      // default to main menu
      //String stylesheet = MenuConstants.XSL_MAINMENU;
      String forwardName = SecurityAdminConstants.FN_MAIN_MENU;
      if (MenuConstants.ACTION_PARM_MARKETING.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_MARKETING_MENU;
          forwardName = SecurityAdminConstants.FN_MARKETING_MENU;
      } else if (MenuConstants.ACTION_PARM_CUSTOMERSERVICE.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_CUSTOMERSERVICE_MENU;
          forwardName = SecurityAdminConstants.FN_CUSTOMER_SERVICE_MENU;
      } else if (MenuConstants.ACTION_PARM_OPERATIONS.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_OPERATIONS_MENU;
          forwardName = SecurityAdminConstants.FN_OPERATIONS_MENU;
      } else if (MenuConstants.ACTION_PARM_MERCHANDISING.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_MERCHANDISING_MENU;
          forwardName = SecurityAdminConstants.FN_MERCHANDISING_MENU;
      } else if (MenuConstants.ACTION_PARM_ACCOUNTING.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_ACCOUNTING_MENU;
          forwardName = SecurityAdminConstants.FN_ACCOUNTING_MENU;
      } else if (MenuConstants.ACTION_PARM_SYSTEMADMIN.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_SYSTEM_ADMIN_MENU;
          forwardName = SecurityAdminConstants.FN_ADMINISTRATIONS_MENU;
      } else if (MenuConstants.ACTION_PARM_SECURITYADMIN.equalsIgnoreCase(adminAction)) {
          //stylesheet = getSourceMenu(request);
          forwardName = getSourceMenu(request);
      } else if (MenuConstants.ACTION_PARM_MAIN.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_MAINMENU;
          forwardName = SecurityAdminConstants.FN_MAIN_MENU;
      } else if (MenuConstants.ACTION_PARM_DISTRIBUTION.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_DISTRIBUTION_MENU;
          forwardName = SecurityAdminConstants.FN_DISTRIBUTION_MENU;
      } else if (MenuConstants.ACTION_PARM_WORKFORCEREPORTS.equalsIgnoreCase(adminAction)) {
          //stylesheet = MenuConstants.XSL_WORKFORCE_REPORTS_MENU;
          forwardName = SecurityAdminConstants.FN_WORKFORCE_REPORT_MENU;
      } 
      //15689 changes
      else if(MenuConstants.ACTION_RESET_USER_PASSWORD.equalsIgnoreCase(adminAction))
      {
    	  forwardName = SecurityAdminConstants.FN_RESET_USER_PASSWORD;
      }

      return forwardName;
  }

 /**
  * Determines which menu to go to based on admin action and the source menu.
  * When user is exitting from the System Administration user section, either the
  * Customer Service menu or the System Administration menu is redered depending
  * on from which menu the user comes from.
  */
  private String getSourceMenu(HttpServletRequest request) throws Exception {
      String forwardName = null;
      String isExit = ServletHelper.getValueFromRequest(request, SecurityAdminConstants.COMMON_PARM_IS_EXIT);
      
      if (isExit != null && isExit.equalsIgnoreCase(SecurityAdminConstants.COMMON_VALUE_YES))
      {
          // User is exitting. Check source menu.
          Object o = ServletHelper.getCommonData(request, SecurityAdminConstants.SOURCE_MENU);
          String sourceMenu = null;
          if (o != null) {
            sourceMenu = (String) o;
          }

          if (MenuConstants.MENU_CUSTOMER_SERVICE.equalsIgnoreCase(sourceMenu)) {
              forwardName = SecurityAdminConstants.FN_CUSTOMER_SERVICE_MENU;
          } else if (MenuConstants.MENU_SYSTEM_ADMINISTRATION.equalsIgnoreCase(sourceMenu)){
              forwardName = SecurityAdminConstants.FN_ADMINISTRATIONS_MENU;
          } else {
              forwardName = SecurityAdminConstants.FN_ADMINISTRATIONS_MENU;
          }
      }
      else {
          forwardName = SecurityAdminConstants.FN_SECURITY_ADMIN_MENU;
      }

      return forwardName;
  }

 /**
  * Displays landing page.
  * @param request http request
  * @param response http response
  * @param stylesheetName name of the stylesheet to be transformed as response
  * @throws Exception
  */
  private void land(HttpServletRequest request, HttpServletResponse response, String forwardName, ActionMapping mapping)
    throws Exception
    {
      logger.debug("Entering land...");
      logger.debug("forwardName is:" + forwardName);
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      String reportServer = ConfigurationUtil.getInstance().getFrpGlobalParm(SecurityAdminConstants.GLOBAL_PARM_CONTEXT, SecurityAdminConstants.GLOBAL_PARM_REPORT_SERVER);

      if (context == null || context.length() == 0)  {
          context = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      }
      if (securityToken == null || securityToken.length() == 0)  {
          securityToken = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      }
      try {
          TraxUtil traxUtil = TraxUtil.getInstance();
          File stylesheet = getXSL(forwardName,mapping);
          String stylesheetName = getXslPathAndName(forwardName, mapping);
          Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
          String sitename = ServletHelper.getSiteName();
          String siteSslPort = ServletHelper.getSiteSslPort();

          // Defect 907, 1238 PCI compliance.
          // Compose the SSL domain.
          String sitenameSsl = ServletHelper.switchServerPort(sitename, siteSslPort);

          //String id = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_IDENTITY);
          // Get parameter map of authorized resources on the page.
          HashMap parameters = AuthorizationBuilder.getAuthorizationParams(request, stylesheetName);

          parameters = ServletHelper.getParameterMap(request, parameters);
          parameters.put(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken == null? "" : securityToken);
          parameters.put(SecurityAdminConstants.COMMON_PARM_CONTEXT, context == null? "" : context);
          parameters.put(SecurityAdminConstants.COMMON_PARM_APP_CONTEXT, applicationContext == null? "" : applicationContext);
          parameters.put(MenuConstants.COMMON_PARM_SITENAME, sitename == null? "" : sitename);
          parameters.put(MenuConstants.COMMON_PARM_SITENAME_SSL, sitenameSsl == null? "" : sitenameSsl);
          parameters.put(SecurityAdminConstants.COMMON_CONFIG, breURL == null? "" : breURL);
          parameters.put(SecurityAdminConstants.REPORT_SERVER, reportServer);
          parameters.put(SecurityAdminConstants.COMMON_CONFIG_REPORTS, marsreportsURL == null? "" : marsreportsURL);
          
          if(stylesheetName.equalsIgnoreCase(MenuConstants.XSL_CUSTOMERSERVICE_MENU))
          {
              parameters.put(MenuConstants.CUST_SERV_PARM_CALL_DNIS,request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_DNIS)             == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_DNIS));
              parameters.put(MenuConstants.CUST_SERV_PARM_CALL_BRAND_NAME,request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_BRAND_NAME) == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_BRAND_NAME));
              parameters.put(MenuConstants.CUST_SERV_PARM_CALL_CS_NUMBER,request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_CS_NUMBER)   == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_CS_NUMBER));
              parameters.put(MenuConstants.CUST_SERV_PARM_CALL_TYPE_FLAG,request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_TYPE_FLAG)   == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_CALL_TYPE_FLAG));
              parameters.put(MenuConstants.CUST_SERV_PARM_T_CALL_LOG_ID,request.getParameter(MenuConstants.CUST_SERV_PARM_T_CALL_LOG_ID)     == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_T_CALL_LOG_ID));
              parameters.put(MenuConstants.CUST_SERV_PARM_DNIS_ERROR_MSG,request.getParameter(MenuConstants.CUST_SERV_PARM_DNIS_ERROR_MSG)   == null   ? ""   : request.getParameter(MenuConstants.CUST_SERV_PARM_DNIS_ERROR_MSG));
          }

          traxUtil.transform(request, response, doc, stylesheet, stylesheetName, parameters);
      } catch (Exception ex) {
        logger.error(ex);
        throw ex;
      }

    }

    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String forwardName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(forwardName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }

    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();

        return xslFilePathAndName;
    }

}
