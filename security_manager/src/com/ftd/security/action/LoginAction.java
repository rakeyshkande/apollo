package com.ftd.security.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.UserDataRequestBuilder;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.constants.MenuConstants;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.exceptions.TooManyAttemptsException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Action class that handles single sign on.
 * @author Christy Hu
 */
public class LoginAction extends Action  {

  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.LoginAction";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private SecurityManager securityManager;  
  private String applicationContext ;
  private String errorStylesheetName;
  private File errorStylesheet;
  private UserDataRequestBuilder requestBuilder;
  private String unitid;
  
  /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering LoginAction...");
    
    String securityToken = null;
    String context = null;
    String identity = null;
    String credentials = null;
    String adminAction = null;

    response.setContentType(CONTENT_TYPE);
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server


    try  {
        requestBuilder = new UserDataRequestBuilder();
        securityManager = SecurityManager.getInstance();
        applicationContext = ConfigurationUtil.getInstance().getProperty
          (SecurityAdminConstants.CONFIG_FILE,SecurityAdminConstants.COMMON_PARM_APP_CONTEXT);
        unitid = ServletHelper.getUnitID();
        this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR, mapping);
        this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR, mapping);
    
        if(ServletHelper.isMultipartFormData(request)
          && request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT) == null) { 
            // Request is multipart/form-data
            HashMap parameters = ServletHelper.getMultipartFilterParam(request);
            securityToken = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
            context = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_CONTEXT);
            identity = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_IDENTITY);
            credentials = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);
            adminAction = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken);
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT, context);
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_IDENTITY, identity);
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credentials);
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION, adminAction);
        } else {
        
            securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
            context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
            identity = request.getParameter(SecurityAdminConstants.COMMON_PARM_IDENTITY);
            credentials = request.getParameter(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);
            adminAction = request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
       }

        logger.debug("Login Action execute::context=" + context);
        logger.debug("identity=" + identity);
        logger.debug("securityToken=" + securityToken);   
        
        // Authenticate user.
        if (identity != null && identity.length() > 0
            && credentials != null && credentials.length() > 0)  {
          if(context == null || context.length() == 0){ 
              logger.error("CONTEXT NULL");
              ServletHelper.handleError(request, response, new SecurityAdminException("20"), errorStylesheet, errorStylesheetName);
          } else {
              logger.debug("Retrieved context = " + context);
              logger.debug("Retrieved identity = " + identity);    
              // Obtain security token.
              securityToken = (String) securityManager.authenticateIdentity
                    (context, unitid, identity, credentials);            
          
              // User has been authenticated. Forward to Main Servlet.
              request.setAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken); 
              return mapping.findForward(SecurityAdminConstants.FN_SUCCESS);
          }
        } else if ((MenuConstants.ACTION_PARM_LOGOFF).equalsIgnoreCase(adminAction)) {
            // Log user off.
            this.sendToSignOn(request, response, SecurityAdminConstants.LOGOFF_MSG,mapping);
        } 
        else if ((MenuConstants.ACTION_PARM_DISPLAYCHANGEPASSWORD).equalsIgnoreCase(adminAction)) 
        {
            // Send change password popup.
            this.sendToChangePasswordPopup(request, response, mapping);
            
        } else if ((MenuConstants.ACTION_PARM_CHANGEPASSWORD).equalsIgnoreCase(adminAction)) {
            // Change password.
            this.processChangePassword(request, response, mapping);
        } else if (securityToken != null && securityToken.length() > 0 ) {
            // authenticate the security token
            securityManager.authenticateSecurityToken(context, unitid, securityToken);
            
            // User has been authenticated. Forward to Main Servlet.
            //request.setAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken);
            //RequestDispatcher rd = request.getRequestDispatcher(SecurityAdminConstants.MAIN_SERVLET);
            //rd.forward(request, response);
             return mapping.findForward(SecurityAdminConstants.FN_SUCCESS);
        } else {
            this.sendToSignOn(request, response, "", mapping);
        }
    } catch (AuthenticationException ex) {
        // redirect back to single sign-on page
        logger.debug(ex);
        this.sendToSignOn(request, response, ex.getMessage(),mapping); 
    } catch (ExpiredIdentityException ex) {
        logger.debug(ex);
        this.sendToSignOn(request, response, ex.getMessage(),mapping); 
    } catch (ExpiredCredentialsException ex) {
        logger.debug(ex);
        try {
            // If password is expired, send a window opener login page which will
            // open up the change password popup.
            this.sendToChangePasswordParent(request, response, ex.getMessage(), mapping); 
        } catch (Exception e) {
            logger.error(e);
            ServletHelper.handleError(request, response, e, errorStylesheet, errorStylesheetName); 
        }
    } catch (ExpiredSessionException ex) {
        logger.debug(ex);
        this.sendToSignOn(request, response, ex.getMessage(),mapping);
    } catch (TooManyAttemptsException ex) {
        logger.debug(ex);
        this.sendToSignOn(request, response, ex.getMessage(),mapping);        
    } catch (Exception ex)  {
        logger.error(ex);
        this.sendToSignOn(request, response,SecurityAdminConstants.CONTACT_ADMIN,mapping); 
    } 
    return null;
  }

 /**
  * Displays login page.
  * @param request http request
  * @param response http response
  * @param message display error if there is any.
  */
  private void sendToSignOn(HttpServletRequest request, 
                            HttpServletResponse response,
                            String message, ActionMapping mapping)
    {        
      logger.debug("Entering sendToSignOn...");
      String xslFile = null;
      HashMap inputParams = null;
      Document doc = null; 
      String identity = request.getParameter(SecurityAdminConstants.COMMON_PARM_IDENTITY);
      String credentials = request.getParameter(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);     
      String releaseNumber = "";
      try
      {
    	  releaseNumber = DataRequestHelper.getGlobalParameter(SecurityAdminConstants.GLOBAL_PARM_CONTEXT, SecurityAdminConstants.GlOBAL_PARM_RELEASE_NUMBER);
    	  //releaseNumber = "RLSE_1_0";
          String context = ServletHelper.getContext();
          doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.ERROR);
          ((Element)(doc.getDocumentElement())).setAttribute(SecurityAdminConstants.MESSAGE, message);
          TraxUtil traxUtil = TraxUtil.getInstance();
          File stylesheet = getXSL(SecurityAdminConstants.FN_LOGIN,mapping);        
          HashMap parameters = ServletHelper.getParameterMap(request);
          parameters.put(SecurityAdminConstants.COMMON_PARM_IDENTITY, identity == null? "" : identity);
          parameters.put(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credentials == null? "" : credentials);
          parameters.put(SecurityAdminConstants.COMMON_PARM_APP_CONTEXT, applicationContext == null? "" : applicationContext);
          parameters.put(SecurityAdminConstants.COMMON_PARM_CONTEXT, context == null? "" : context);
          parameters.put("release_number", releaseNumber);

          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_LOGIN,mapping), parameters);
         
      } catch (Exception ex) { 
        logger.error(ex);
        // Will not rethrow the exception because this is the base.
      }    
  }


 /**
  * Displays login page window opener. Set the flag so that password change
  * window will popup.
  * @param request http request
  * @param response http response
  * @param message error to be displayed if any.
  * @thows Exception
  */
  private void sendToChangePasswordParent(HttpServletRequest request, 
                            HttpServletResponse response,
                            String message,
                            ActionMapping mapping) 
  throws Exception
    {        
      logger.debug("Entering sendToChangePasswordParent...");
      String xslFile = null;
      HashMap inputParams = null;
      Document doc = null; 
      String identity = request.getParameter(SecurityAdminConstants.COMMON_PARM_IDENTITY);

      try
      {
          doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.ERROR);
          ((Element)(doc.getDocumentElement())).setAttribute(SecurityAdminConstants.MESSAGE, message);
          TraxUtil traxUtil = TraxUtil.getInstance();
          File stylesheet = getXSL(SecurityAdminConstants.FN_LOGIN,mapping);        
          HashMap parameters = ServletHelper.getParameterMap(request);
          parameters.put(SecurityAdminConstants.COMMON_PARM_IDENTITY, identity == null? "" : identity);
          // Flag the opener to open the password change popup.
          parameters.put(MenuConstants.COMMON_PARM_PASSWORDEXPIRED, MenuConstants.COMMON_VALUE_TRUE);
          parameters.put(SecurityAdminConstants.COMMON_PARM_APP_CONTEXT, applicationContext == null? "" : applicationContext);
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_LOGIN,mapping), parameters);
         
      } catch (Exception ex) { 
        logger.error(ex);
        throw ex;
      }    
  }


 /**
  * Displays password change popup.
  * @param request http request
  * @param response http response
  * @throws Exception
  */
  private void sendToChangePasswordPopup(HttpServletRequest request, 
                            HttpServletResponse response, ActionMapping mapping) 
  throws Exception
    {        
      logger.debug("Entering sendToChangePasswordPopup...");
      String xslFile = null;
      Connection con = null;
      HashMap inputParams = null;
      Map outputMap = null;
      Document doc = null; 
      DataAccessUtil util = null;
      DataRequest dataRequest = null;      
      String status = null;
      String message = null;
      String tmpUserId = null;
      try
      {          
          util = DataAccessUtil.getInstance();
          dataRequest = new DataRequest();
    
          con = DataRequestHelper.getDBConnection();
          dataRequest.setConnection(con);
          dataRequest.setInputParams(requestBuilder.getViewIdentityInputParams(request));
          dataRequest.setStatementID(StatementConstants.USERS_VIEW_IDENTITY);   
          doc = (Document)util.execute(dataRequest);
          
          tmpUserId = DOMUtil.getNodeText(doc, "/IDENTITIES/IDENTITY/user_id");
          
          TraxUtil traxUtil = TraxUtil.getInstance();
          File stylesheet = null;
          
          String stylesheetName = "";
          if(tmpUserId == null || tmpUserId.length() == 0)
          {
              stylesheetName = getXslPathAndName(SecurityAdminConstants.FN_NO_SUCH_USER,mapping);
              stylesheet = getXSL(SecurityAdminConstants.FN_NO_SUCH_USER,mapping);
          }
          else
          {
              stylesheetName = getXslPathAndName(SecurityAdminConstants.FN_LOAD_PASSWORD_CHANGE_POPUP,mapping);
              stylesheet = getXSL(SecurityAdminConstants.FN_LOAD_PASSWORD_CHANGE_POPUP,mapping);
          }    
          
          HashMap parameters = ServletHelper.getParameterMap(request);
          traxUtil.transform(request, response, doc, stylesheet, stylesheetName, parameters);
          
      } catch (Exception ex) { 
        logger.error(ex);
        throw ex;
      } finally {
          if (con != null) {
            con.close();
          }
      } 
  }  


 /**
  * Processes password change. Set the flag so this window will close 
  * and return to the opener.
  * @param request http request
  * @param response http response
  * @throws Exception
  */
  private void processChangePassword(HttpServletRequest request, 
                            HttpServletResponse response, ActionMapping mapping) 
  throws Exception
  {        
      logger.debug("Entering processChangePassword...");
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      File stylesheet = null;
      Document doc = null; // resulting document
      TraxUtil traxUtil = null;
      String feedback = null; // message to send back
      Map outputMap = null;
      HashMap parameters = ServletHelper.getParameterMap(request);
      String status = null;
      String message = null;
      String password = request.getParameter(IdentityVO.RQ_ACCOUNT_NEWCREDENTIALS);
      String identity = request.getParameter(IdentityVO.RQ_ACCOUNT_IDENTITY_ID);
      
      try
      {  
          util = DataAccessUtil.getInstance();
          dataRequest = new DataRequest();
          con = DataRequestHelper.getDBConnection();
          dataRequest.setConnection(con);

          // Check if identity exists.
          HashMap inputParams = new HashMap();
          inputParams.put(IdentityVO.ST_IDENTITY_ID, identity == null? "" : identity); 
          dataRequest.setInputParams(inputParams);
          dataRequest.setStatementID(StatementConstants.USERS_IDENTITY_EXISTS);
          status = (String)util.execute(dataRequest);
        
          if ("N".equalsIgnoreCase(status)) {
                // Identity doesn't exist. Set flag to close popup window.
                parameters.put(MenuConstants.COMMON_PARM_CLOSEWINDOW, MenuConstants.COMMON_VALUE_TRUE);                
          }          
          else {                              
              con.setAutoCommit(false);              
              dataRequest.setInputParams
                (requestBuilder.getUserUpdateCredentialsInputParams(request, identity));
              dataRequest.setStatementID(StatementConstants.USERS_UPDATE_CREDENTIALS);
              outputMap = (Map) util.execute(dataRequest);
              status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
              message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
 
              logger.error("status = " + status);
              logger.error("message = " + message); 
              // check status
              if (!"Y".equalsIgnoreCase(status))  {
                 logger.debug("FAILED TO CHANGE PASSWORD." + message);
                 // "ERROR OCCURRED" is an unexpected error.
                 if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                     feedback = MenuConstants.ERROR_PASSWORD_CHANGE_FAILURE;
                 } else {
                     if ("APP-01007".equals(message) || "APP-01008".equals(message)) {
                         // Too many attempts. Set flag to close popup window.
                         parameters.put(MenuConstants.COMMON_PARM_CLOSEWINDOW, MenuConstants.COMMON_VALUE_TRUE);
                     } else {            
                         // Send back the error value looked up from error xml.
                         feedback = ServletHelper.getErrorMsg(message);
                     }
                 }  
              }  else {
                 // Password changed. Set flag to close popup window.
                 parameters.put(MenuConstants.COMMON_PARM_CLOSEWINDOW, MenuConstants.COMMON_VALUE_TRUE);
                 // Set password for window operner to login.
                 parameters.put(MenuConstants.COMMON_PARM_PASSWORD, password == null? "" : password);
                 con.commit();
              }
          }
          doc = rebuildChangePasswordTree(request);
          
          doc.getDocumentElement().setAttribute(SecurityAdminConstants.MESSAGE, feedback);
          traxUtil = TraxUtil.getInstance();
          stylesheet = getXSL(SecurityAdminConstants.FN_CHANGE_PASSWORD_COMPLETE, mapping);                 
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_CHANGE_PASSWORD_COMPLETE,mapping), parameters);        
      } catch (Exception ex ) {
          con.rollback();
          if(!response.isCommitted()) {
              // Display the change password screen again.
              doc = rebuildChangePasswordTree(request);          
              doc.getDocumentElement().setAttribute
                (SecurityAdminConstants.MESSAGE, MenuConstants.ERROR_PASSWORD_CHANGE_FAILURE);
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_LOAD_PASSWORD_CHANGE_POPUP,mapping);                 
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_LOAD_PASSWORD_CHANGE_POPUP,mapping), parameters);        
          }
          
      } finally {
          if(con != null && !con.getAutoCommit()) {
            con.setAutoCommit(true);
          }
          if (con != null) {
            con.close();
          }
      } 
  }    

  /**
   * Rebuilds the tree to be sent back to change password page in case of an error.
   * @param request HttpServletRequest
   * @return Document xml tree that contains data sent by user
   * @throws Exception
   */
  private Document rebuildChangePasswordTree(HttpServletRequest request) throws Exception 
  {
      String identityId = request.getParameter(IdentityVO.RQ_ACCOUNT_IDENTITY_ID);
      String oldCredentials = request.getParameter(IdentityVO.RQ_ACCOUNT_OLDCREDENTIALS);
      String newCredentials = request.getParameter(IdentityVO.RQ_ACCOUNT_NEWCREDENTIALS);
      String newRecredentials = request.getParameter(IdentityVO.RQ_ACCOUNT_NEWRECREDENTIALS);      
      Document doc = XMLHelper.getDocumentWithRoot(IdentityVO.XML_TOP);
      Element identityElem = doc.createElement(IdentityVO.XML_BOTTOM);
      doc.getDocumentElement().appendChild(identityElem);
      
      // Create identity id element and its text node.
      doc = XMLHelper.createElementWithTextData(doc, IdentityVO.XML_BOTTOM, IdentityVO.TB_IDENTITY_ID, 
                                                identityId == null? "" : identityId);
      
      // Create old credentials element and its text node.
      doc = XMLHelper.createElementWithTextData(doc, IdentityVO.XML_BOTTOM, IdentityVO.RQ_ACCOUNT_OLDCREDENTIALS, 
                                                oldCredentials == null? "" : oldCredentials);
    
      // Create new credentials element and its text node.
      doc = XMLHelper.createElementWithTextData(doc, IdentityVO.XML_BOTTOM, IdentityVO.RQ_ACCOUNT_NEWCREDENTIALS, 
                                                newCredentials == null? "" : newCredentials); 

      // Create new credentials element and its text node.
      doc = XMLHelper.createElementWithTextData(doc, IdentityVO.XML_BOTTOM, IdentityVO.RQ_ACCOUNT_NEWRECREDENTIALS, 
                                                newRecredentials == null? "" : newRecredentials); 
      return doc;    
  }
  
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }
    
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}