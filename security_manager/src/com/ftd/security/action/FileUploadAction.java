package com.ftd.security.action;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.ExcelProcessingException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.manager.UserManager;
import com.ftd.security.util.FileProcessor;
import com.ftd.security.util.ServletHelper;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.cxf.common.util.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to load data (such as users) from an Excel Spreadsheet.
 * @author Christy Hu
 */
public class FileUploadAction extends Action  {

  //private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.FileUploadAction";
  private static final String ERROR_NO_FILE_ITEM = "Cannot Find File Item";
  private static final String ERROR_LOAD_FILE = "Error Loading File";
  private static final String SUCCESS = "File Processing Succeeded";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private SecurityManager securityManager;
  private File errorStylesheet;
  private String errorStylesheetName;

 /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException { 
      logger.info("Entering FileUploadAction...");
      try {
          this.securityManager = SecurityManager.getInstance();
          this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
          this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
          if (ServletHelper.isMultipartFormData(request)) {
              // The form is sent with multipart/form-data enctype. 
              this.processFormData(request, response, mapping);
          } else {
        	  String actionType = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ACTION_TYPE);            
              String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
              String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
              if (ServletHelper.isValidToken(context, securityToken))  {
                  if (actionType.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW)) {                      
                      this.viewUpload(request, response, null, mapping);
                  }
              } else {
                  logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN + securityToken);
                  // redirect to the login page
                  ServletHelper.redirectToLogin(request, response);
               }    
          }
          
      } catch (ExpiredSessionException ex) {
          logger.error(ex);
          ServletHelper.redirectToLogin(request, response);           
      } catch (Exception ex) {
          logger.error(ex);
          ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
      }
      return null;
  }


  /**
   * Displays the page for file upload.
  * @param request http request
  * @param response http response
  * @param message message to display. Null if no message.
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws Exception
   */
   private void viewUpload(HttpServletRequest request, HttpServletResponse response, String message, ActionMapping mapping)
    throws SAXException,
           ParserConfigurationException,
           IOException,
           SQLException,
           Exception        
   {
      logger.debug("Entering viewUpload...");
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      if (securityToken == null || securityToken.length() == 0 ) {      
          securityToken = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      }
      if (context == null || context.length() == 0 ) {      
          context = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      }
      
      try{
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE ))  {
              UserManager userManager = new UserManager();

              //Document doc = XMLHelper.getDocumentWithAttribute(SecurityAdminConstants.XML_ROOT, SecurityAdminConstants.MESSAGE, message);
              Document doc = userManager.buildUserBatchLoadDoc(request, message);
          
              TraxUtil traxUtil = TraxUtil.getInstance();
              String adminActionxsl = request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
              if (StringUtils.isEmpty(adminActionxsl) && ServletHelper.isMultipartFormData(request)) {
            	  HashMap fieldMap = ServletHelper.getMultipartFilterParam(request);
            	  adminActionxsl = (String) fieldMap.get(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
              }
              File stylesheet = getXSL(adminActionxsl, mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(adminActionxsl,mapping),ServletHelper.getParameterMap(request));
          } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception ex)  {
          logger.error(ex);       
          ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
      }      
  }

  /**
   * Confirms request. Dispatches to File processor.
  * @param request http request
  * @param response http response
  * @throws Exception
   */
  private void processFormData(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws Exception
  {
      logger.debug("Entering processFormData...");
      HashMap fieldMap = ServletHelper.getMultipartFilterParam(request);
      FileItem fileItem = null;
      String adminActionXSL = null;
      String actionType = null;
      String subject = null;
      String context = null;
      String securityToken = null;
      String xsl = null;
            
      try{         
          adminActionXSL = (String)fieldMap.get(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
          actionType = (String)fieldMap.get(SecurityAdminConstants.COMMON_PARM_ACTION_TYPE);
          subject = (String)fieldMap.get(SecurityAdminConstants.COMMON_PARM_SUBJECT);
          context = (String)fieldMap.get(SecurityAdminConstants.COMMON_PARM_CONTEXT);
          securityToken = (String)fieldMap.get(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
          fileItem = (FileItem)fieldMap.get(SecurityAdminConstants.FILE_ITEM);
          
          // Set attributes for Main servlet.
          request.setAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT, context);
          request.setAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken);
          logger.debug("adminActionXSL: " + adminActionXSL + ", actionType: " + actionType + ", subject=" + subject);
          logger.debug("context=" + context + "...securityToken=" + securityToken);

          // Authentication call.
          if (ServletHelper.isValidToken(context, securityToken))  {
              // Authorization call.
              if (securityManager.assertPermission(context, securityToken, SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_VIEW ))  {
                  // Confirm request from form field and process file. Otherwise error.
                  if (fileItem == null) {
                      logger.error(ERROR_NO_FILE_ITEM);
                      this.viewUpload(request, response, ERROR_NO_FILE_ITEM, mapping);
                  }
                  else if (adminActionXSL.equalsIgnoreCase(SecurityAdminConstants.ACTION_USER_UPLOAD)) {
                      ArrayList newUsers = FileProcessor.getInstance().processFile(fileItem.getInputStream(), subject, securityToken);
                      request.setAttribute(SecurityAdminConstants.COMMON_PARM_USER_LIST, newUsers);
                      this.viewUpload(request, response, SUCCESS, mapping);
                      
                  } else if ( (adminActionXSL.equalsIgnoreCase(SecurityAdminConstants.ACTION_USER_GROUP_UPDATE)) ||
                		  (adminActionXSL.equalsIgnoreCase(SecurityAdminConstants.ACTION_BULK_USER_TERMINATE)) ) {
                      ArrayList userList = FileProcessor.getInstance().processUserFile(fileItem.getInputStream(), subject, actionType, securityToken);
                      request.setAttribute(SecurityAdminConstants.COMMON_PARM_USER_LIST, userList);
                      this.viewUpload(request, response, SUCCESS, mapping);
                      
                  } else {
                      // request unknown
                      this.viewUpload(request, response, SecurityAdminConstants.ERROR_INVALID_REQUEST, mapping);
                  }
                  
              } else {
                  // display 'Not Authorized' message
                  logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
                  ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
              }
          } else {
            logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
            logger.error(securityToken);
            // redirect to the login page
            ServletHelper.redirectToLogin(request, response);
          }              
      } catch(FileUploadException ex){
          logger.error(ex);
          this.viewUpload(request, response, ERROR_LOAD_FILE, mapping);
      } catch(ExcelProcessingException ex) {
          this.viewUpload(request, response, ex.getMessage(), mapping);         
      } catch(Exception ex)  {
          logger.error(ex);       
          this.viewUpload(request, response, ex.getMessage(), mapping);
      }            
  }  
  
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }   
    
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}