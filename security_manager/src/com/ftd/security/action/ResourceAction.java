package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.ResourceDataRequestBuilder;
import com.ftd.security.cache.vo.AbstractVO;
import com.ftd.security.cache.vo.ResourceVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add resource, update resource,
 * remove resource, view resource, and view all resources.
 * @author Christy Hu
 */
public class ResourceAction extends Action  {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.ResourceAction";
  private ResourceDataRequestBuilder requestBuilder  = new ResourceDataRequestBuilder();
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private SecurityManager securityManager;
  private File errorStylesheet;
  private String errorStylesheetName;


    /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering ResouceAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);
    
    try  {
        this.securityManager = SecurityManager.getInstance();
        this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
        this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
        // authentication call
        if (ServletHelper.isValidToken(context, securityToken))  {     
            if (action != null)  {    
              if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW))  {
                  this.view(request, response, mapping);
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW_ALL))  {
                  this.viewAll(request, response, mapping);     
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
                  this.displayAdd(request, response, mapping);     
              }else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
                  this.add(request, response, mapping);                 
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
                  this.remove(request, response, mapping);   
              } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
                  this.update(request, response, mapping);                
              }  else {
                  logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
                  ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
              }        
            }
            else 
            {
              logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
              ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
            }
        } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          logger.error(securityToken);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
        } 
    } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } finally  {
    }
    return null;
  }


 /**
  * Displays Add Resource page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           Exception 
  {
      logger.debug("entering displayAdd...");
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      Document rdoc = null; // response document
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_CREATE ))  {      
              doc = getViewContextsDoc();
              
              rdoc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
              rdoc.getDocumentElement().appendChild(rdoc.importNode(doc.getDocumentElement(), true));
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDRESOURCE, mapping);      
              traxUtil.transform(request, response, rdoc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDRESOURCE,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } 
  }

 /**
  * Returns the document as a result of view all contexts.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private Document getViewContextsDoc()
    throws Exception 
  {
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Document doc = null;
      
      try
      {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
              dataRequest.setStatementID(StatementConstants.CONTEXT_VIEW_CONTEXTS);
              doc = (Document) util.execute(dataRequest);
              return doc;
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }    
  }

 /**
  * Sends View Resource page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void view(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {       
      logger.debug("entering view...");
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_VIEW ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);

              inputParams = requestBuilder.getViewResourceInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_VIEW_RESOURCE);

              doc = (Document) util.execute(dataRequest);
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_RESOURCE, mapping);
        
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_RESOURCE,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }     
  }

 /**
  * Sends View Resources page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void viewAll(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering viewAll...");    
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_VIEW ))  {      

              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();

              doc = getViewResourcesDoc(request);
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_RESOURCELIST, mapping);
              traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_RESOURCELIST,mapping),ServletHelper.getParameterMap(request));
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) {
          con.close();
        }
      }
  }

  /**
   * Get document as a result of reading all resources.
   * @param request
   * @return Document
   * @throws Exception
   */
    private Document getViewResourcesDoc(HttpServletRequest request) throws Exception {
  
      Connection con = null;
      String xslFile = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Document doc = null;   

      try {
          util = DataAccessUtil.getInstance();
          dataRequest = new DataRequest();

          con = DataRequestHelper.getDBConnection();
          dataRequest.setConnection(con);
          dataRequest.setStatementID(StatementConstants.ACL_VIEW_RESOURCES);
          inputParams = requestBuilder.getViewResourcesInputParams(request);
          dataRequest.setInputParams(inputParams);
          doc = (Document) util.execute(dataRequest);
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } finally {
          if(con != null) {
              con.close();
          }
      }
      return doc;
      
   }
   
 /**
  * Adds a resource and displays the view detail page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void add(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering add...");   
      Connection con = null;
      String status = null;
      String message = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      File stylesheet = null;
      HashMap inputParams = null;
      Map outputMap = null;
      TraxUtil traxUtil = null;
      Document doc = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      AbstractVO resource = null;
      
      try
      {   
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_CREATE ))  {      
              resource = new ResourceVO(request);
              // Determine if required fields are missing.
              if (!resource.isValid()) {
                  throw new RequiredFieldMissingException();
              } else {
                  try {
                      util = DataAccessUtil.getInstance();
                      dataRequest = new DataRequest();
                      con = DataRequestHelper.getDBConnection();   
                      dataRequest.setConnection(con);

                      // Check if resource exists.
                      inputParams = requestBuilder.getResourceExistsInputParams(request);
                      dataRequest.setInputParams(inputParams);
                      dataRequest.setStatementID(StatementConstants.ACL_RESOURCE_EXISTS);
                      status = (String)util.execute(dataRequest);

                      if ("Y".equalsIgnoreCase(status)) { 
                          logger.error("RESOURCE EXISTS");
                          throw new DuplicateObjectException("10");
                      }
               
                      inputParams = requestBuilder.getAddResourceInputParams
                        (request, ServletHelper.getMyId(securityToken));
                      dataRequest.setInputParams(inputParams);
                      dataRequest.setStatementID(StatementConstants.ACL_UPDATE_RESOURCE);
                      outputMap = (Map) util.execute(dataRequest);
                      status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                      message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                      logger.error("status = " + status);
                      logger.error("message = " + message);
                      // check status
                      if (!"Y".equalsIgnoreCase(status))  {
                          logger.debug("FAILED TO ADD RESOURCE.");
                          // "ERROR OCCURRED" is an unexpected error.
                          if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                              throw new SecurityAdminException("6");
                          } else {
                              throw new SecurityAdminException("12", message);
                          }
                      }
                  } catch (Exception e) {
                      logger.error(e);
                      throw e;
                  }  finally {
                      if (con != null) {
                        con.close();
                      }
                  }
                  this.view(request, response, mapping);
              }
                      
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }  
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          // Rebuild XML tree and send back.
          doc = resource.rebuildTree(e);
          Document cdoc = getViewContextsDoc();
          // Add context doc and the rebuilt document to root element.
          doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(),true));
          stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDRESOURCE, mapping);
          traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDRESOURCE,mapping),ServletHelper.getParameterMap(request));          
      } catch (Exception e) {
        logger.error(e);
        throw e;
      }  finally {
         if (con != null) {
             con.close();
         }
      } 
  }

 /**
  * Removes a resource and dispatches to View Resources page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void remove(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering remove...");
      logger.debug("Retrieved these ids to be removed: " + request.getParameter("remove_ids"));

      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      Connection con = null;
      ArrayList ids = null;
      HashMap inputParams = null;
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      int i = 0;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_REMOVE ))  {      
              try {

                  util = DataAccessUtil.getInstance();
                  dataRequest = new DataRequest();
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false); 
                  dataRequest.setConnection(con);
        
                  ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
                  for (i = 0; i < ids.size(); i++) 
                  {        
                      inputParams = requestBuilder.getRemoveResourceInputParams(request, (String)ids.get(i));
                      dataRequest.setInputParams(inputParams);
                      dataRequest.setStatementID(StatementConstants.ACL_REMOVE_RESOURCE);
                      outputMap = (Map) util.execute(dataRequest);
                      status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                      message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);       

                      logger.error("status = " + status);
                      logger.error("message = " + message);
                      // check status
                      if (!"Y".equalsIgnoreCase(status))  {
                          logger.debug("FAILED TO REMOVE RESOURCE.");
                          // "ERROR OCCURRED" is an unexpected error.
                          if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                              throw new SecurityAdminException("6");
                          } else {
                              // Object is being used.
                              throw new InvalidRequestException();
                          }
                      }
                  }
                  con.commit();
                } catch (Exception ex ) {
                    con.rollback();
                    throw ex;
                } finally {
                    con.setAutoCommit(true);
                    if (con != null) {
                      con.close();
                    }
                }
                this.viewAll(request, response, mapping);
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }
      } catch (InvalidRequestException ex) { 
          // Rebuild XML tree and send back.
          Document doc = getViewResourcesDoc(request);
          // Find context that throws this exception.
          String errorResource = request.getParameter(ResourceVO.RQ_REMOVE_RESOURCE_ID + (String)ids.get(i));
          String errorContext = request.getParameter(ResourceVO.RQ_REMOVE_CONTEXT_ID + (String)ids.get(i));
          doc = markViewResourcesDoc(request, doc, errorResource, errorContext);

          stylesheet = getXSL(SecurityAdminConstants.FN_VIEW_RESOURCELIST, mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_VIEW_RESOURCELIST,mapping),ServletHelper.getParameterMap(request));          
      } catch (Exception ex ) {
          con.rollback();
          throw ex;
      }  finally {
          if (con != null) {
              con.close();
          }
      }
  }

 /**
  * Marks the document to be sent back due to object in use in a removal attempt. 
  * 1. Mark an 'error' attribute='Object is being used.'
  * 2. Mark a 'selected' attribute='Y' if the resource is selected in the request.
  * @param request HttpServletRequest
  * @param doc the context document
  * @param errorContext the context id that has errored out
  * @throws Exception
  */
  private Document markViewResourcesDoc(HttpServletRequest request, Document doc, String errorResource, String errorContext) 
  throws Exception {
      // Find all resources marked for deletion.
      ArrayList ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_IDS));
      for (int i = 0; i < ids.size(); i++){
          String sResource = request.getParameter(ResourceVO.RQ_REMOVE_RESOURCE_ID + (String)ids.get(i));
          String sContext = request.getParameter(ResourceVO.RQ_REMOVE_CONTEXT_ID + (String)ids.get(i));

          // Find the context element with the same value and mark 'selected' attribute.
          Element resourceElem = XMLHelper.findElementByChild
            (doc, ResourceVO.TB_RESOURCE_ID, sResource, ResourceVO.TB_CONTEXT_ID, sContext);
          if (resourceElem != null) {
              resourceElem.setAttribute(SecurityAdminConstants.XML_ATTR_SELECTED, SecurityAdminConstants.COMMON_VALUE_YES);
          }
      }

      // Find the element in error. The element is identified by the resource id and context id.
      Element errorElem = XMLHelper.findElementByChild
        (doc, ResourceVO.TB_RESOURCE_ID, errorResource, ResourceVO.TB_CONTEXT_ID, errorContext);
      // Mark error attribute on element.
      if (errorElem != null) {
        errorElem.setAttribute(SecurityAdminConstants.XML_ATTR_ERROR, SecurityAdminConstants.ERROR_OBJECT_IN_USE);     
      }
      return doc;
  }
  
 /**
  * Updates a resources.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("entering update...");   
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      File stylesheet = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
 
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken, 
          SecurityAdminConstants.RESOURCE_RESOURCE, SecurityAdminConstants.PERMISSION_UPDATE ))  {      
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
              con = DataRequestHelper.getDBConnection();     
              dataRequest.setConnection(con);
              inputParams = requestBuilder.getUpdateResourceInputParams
                (request, ServletHelper.getMyId(securityToken));
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.ACL_UPDATE_RESOURCE);
              outputMap = (Map) util.execute(dataRequest);
              status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
              message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

              logger.error("status = " + status);
              logger.error("message = " + message);
              // check status
              if (!"Y".equalsIgnoreCase(status))  {
                  logger.debug("FAILED TO UPDATE RESOURCE.");
                  // "ERROR OCCURRED" is an unexpected error.
                  if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                      throw new SecurityAdminException("6");
                  } else {
                      throw new SecurityAdminException("12", message);
                  }
              }
          } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }              
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null)  {
          con.close();
        }
      } 
      //forward request to view all
      this.view(request, response, mapping);      
  } 
  
    /**
    * Retrieve name of Action XSL file
    * @param1 String - the xsl name  
    * @param2 ActionMapping
    * @return File - XSL File name
    * @throws none
    */
    private File getXSL(String xslName, ActionMapping mapping)
    {

      File xslFile = null;
      String xslFilePathAndName = null;

      ActionForward forward = mapping.findForward(xslName);
      xslFilePathAndName = forward.getPath();
      //get real file name
      xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
      return xslFile;
    }   
    
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }    
}