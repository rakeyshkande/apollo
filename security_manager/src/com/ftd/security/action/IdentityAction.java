package com.ftd.security.action;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.json.JSONUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.builder.UserDataRequestBuilder;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.cache.vo.RoleVO;
import com.ftd.security.cache.vo.UserVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidRequestException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.manager.UserManager;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;


/**
 * Action class that handles requests related to add identity, view/update identities,
 * remove identities, view identity role, and view/update identity roles.
 * @author Christy Hu
 */
public class IdentityAction extends Action  {
  private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
  private static final String LOGGER_CATEGORY = "com.ftd.security.action.IdentityAction";
  private Logger logger = new Logger(LOGGER_CATEGORY);
  private UserDataRequestBuilder requestBuilder; // shares data request builder with user
  private SecurityManager securityManager;
  private File errorStylesheet;
  private String errorStylesheetName;

   /**
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     */
  public ActionForward execute(ActionMapping mapping, ActionForm form, 
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    logger.info("Entering IdentityAction...");
    response.setContentType(CONTENT_TYPE);
    String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION); 
    String context = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    String securityToken = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
    logger.debug("Request is " + action);
    logger.debug("SecurityToken is " + securityToken);
    logger.debug("context is " + context);
    try  {
        requestBuilder = new UserDataRequestBuilder();
        this.securityManager = SecurityManager.getInstance();
        this.errorStylesheetName = getXslPathAndName(SecurityAdminConstants.FN_ERROR,mapping);
        this.errorStylesheet = getXSL(SecurityAdminConstants.FN_ERROR,mapping);
      // authentication call
      if (ServletHelper.isValidToken(context, securityToken))  {    
        if (action != null)  {    
          if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_VIEW))  {
              this.view(request, response, mapping);  
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_DISPLAY_ADD))  {
              this.displayAdd(request, response, mapping); 
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REFRESH_DISPLAY_ADD))  {
              this.refreshDisplayAdd(request, response, mapping);                
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD))  {
              return this.add(request, response, mapping);                 
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE))  {
              this.remove(request, response);   
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_UPDATE))  {
              this.update(request, response, mapping);     
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_RESET_PASSWORD))  {
              this.resetPassword(request, response, mapping);              
        /*  } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_MAP_ROLE))  {
              this.mapRole(request, response);   */              
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_RESET_USER_PASSWORD))  {
              this.resetPassword(request, response, mapping);
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD_ROLE))  {
              this.addRole(request, response, mapping);                 
          } else if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_REMOVE_ROLE))  
          {
            System.out.println("Entered");
              this.removeRole(request, response, mapping);                 
            System.out.println("PASSED");

          } else {
              logger.error(SecurityAdminConstants.ERROR_INVALID_REQUEST + ":" + action);
              ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
          }        
        }
        else 
        {
          logger.error(SecurityAdminConstants.ERROR_EMPTY_REQUEST);
          ServletHelper.handleError(request, response, new SecurityAdminException("5"), errorStylesheet, errorStylesheetName);
        } 

      } else {
          logger.error(SecurityAdminConstants.INVALID_SECURITY_TOKEN);
          logger.error(securityToken);
          // redirect to the login page
          ServletHelper.redirectToLogin(request, response);
      }
    } catch (SAXException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ParserConfigurationException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);  
    } catch (SQLException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (ExpiredIdentityException ex) {
      // redirect to sign on
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredCredentialsException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (ExpiredSessionException ex) {
      logger.error(ex);
      ServletHelper.redirectToLogin(request, response);
    } catch (SecurityAdminException ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } catch (Exception ex)  {
      logger.error(ex);
      ServletHelper.handleError(request, response, ex, errorStylesheet, errorStylesheetName);
    } 
    return null;
  }

  
 /**
  * Sends Update User Identities page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void view(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering view...");
      Connection con = null;
      Document idoc = null; // identities docuemnt
      File stylesheet = null;
      Document doc = null; // resulting document
      TraxUtil traxUtil = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO user = null;
      IdentityVO identity = null;
      UserManager userManager = null;

      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_VIEW))  {      
              con = DataRequestHelper.getDBConnection();
              user = new UserVO(request);
              identity = new IdentityVO(request);
              userManager = new UserManager(user, identity);
               
              doc = userManager.buildViewIdentityDoc(con);
              traxUtil = TraxUtil.getInstance();
              stylesheet = getXSL(SecurityAdminConstants.FN_UPDATE_IDENTITY,mapping);       
              traxUtil.transform(request, response, doc, stylesheet,getXslPathAndName(SecurityAdminConstants.FN_UPDATE_IDENTITY, mapping),ServletHelper.getParameterMap(request));
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {


        logger.error(e);
        throw e;
      } finally {
        if (con != null && !con.isClosed()) {
          con.close();
        }
      }
  }  


 /**
  * Displays Create Additional Identity page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void displayAdd(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering displayAdd...");
      String xslFile = null;
      Document doc = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE))  {
            con = DataRequestHelper.getDBConnection();
            
            UserVO user = new UserVO(request);
            UserManager userManager = new UserManager(user, null);
            doc = userManager.buildDisplayAddIdentityDoc(con);
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY, mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY,mapping),ServletHelper.getParameterMap(request));
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null && !con.isClosed()) {
        	try {
        		con.close();
        	} catch (Exception ex) {
        		logger.warn("Close Connection Exception: " + ex.getMessage(), ex);
        	}
        }
      }
  }

 /**
  * Displays Add Additional Identity page when context selection changes. 
  * Rebuilds tree but does not validate data.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */
  private void refreshDisplayAdd(HttpServletRequest request, HttpServletResponse response,ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception
    {
      logger.debug("entering refreshDisplayAdd...");
      String xslFile = null;
      Document doc = null;
      Connection con = null;
      TraxUtil traxUtil = null;
      File stylesheet = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      try
      {
        // authorization call
        if (securityManager.assertPermission(context, securityToken, 
        SecurityAdminConstants.RESOURCE_USER, SecurityAdminConstants.PERMISSION_CREATE))  {
            con = DataRequestHelper.getDBConnection();
            UserVO userVO = new UserVO(request);
            IdentityVO identityVO = new IdentityVO(request);
            UserManager userManager = new UserManager(userVO, identityVO);
            doc = userManager.buildRefreshDisplayAddIdentityDoc(con);
            traxUtil = TraxUtil.getInstance();
            stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY,mapping);
            traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY,mapping),ServletHelper.getParameterMap(request));
        } else {
            // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null && !con.isClosed()) {
            con.close();
        }
      }
  }  
 /**
  * Adds an identity and sends back View User Identities page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private ActionForward add(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering add...");
    
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO userVO = null;
      IdentityVO identityVO = null;
      UserManager userManager = null;
      String credential = null;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_CREATE))  {        
              con = DataRequestHelper.getDBConnection(); 
              con.setAutoCommit(false);
              userVO = new UserVO(request);
              identityVO = new IdentityVO(request);
              userManager = new UserManager(userVO, identityVO);
            try {
                if (!identityVO.isValid(SecurityAdminConstants.ACTION_PARM_ADD)) {
                    throw new RequiredFieldMissingException();
                }
            
                String userId = userVO.getUserId();
                credential = userManager.addIdentity(securityToken, con, (Integer.valueOf(userId)).intValue());
                userManager.addIdentityRoles(securityToken, con);
                request.setAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credential);
                con.commit();
                
            } catch (Exception e) {
                con.rollback();
                logger.error(e);
                throw e;
            } finally {
                if (con != null) {
                  con.setAutoCommit(true);
                }
            }     
            if (con != null) {
                con.close();
            }
            //RequestDispatcher rd = super.getServletContext().getRequestDispatcher(SecurityAdminConstants.USER_SERVLET);
             //RequestDispatcher rd = null;
            request.setAttribute(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION, SecurityAdminConstants.ACTION_PARM_VIEW);
            //rd.forward(request, response);
            return mapping.findForward(SecurityAdminConstants.FN_USER_ACTION);
        } else {
          // display 'Not Authorized' message
            logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
            ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
        }
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException and DuplicateObjectException are caught here.
          Document doc = userManager.buildInvalidAddIdentityRequestDoc(con, e);
          
          File stylesheet = getXSL(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY,mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          UserManager.printDocument(doc);
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_DISPLAY_ADDIDENTITY,mapping),ServletHelper.getParameterMap(request));              
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null && !con.isClosed()) {
          con.close();
        }
      } 
      return null;
    }

 /**
  * Remove the one or more identities and sends View User Identities page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  * @deprecated
  */
  private void remove(HttpServletRequest request, HttpServletResponse response)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {
      logger.debug("Entering remove...");
      logger.debug("Retrieved these ids to be removed: " + request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN));
       
      Connection con = null;
      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      String status = null;
      String message = null;
      HashMap inputParams = null;
      Map outputMap = null;
      ArrayList ids = null; // list of id indices to be removed
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
           
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_REMOVE))  {       
              util = DataAccessUtil.getInstance();
              dataRequest = new DataRequest();
 
              con = DataRequestHelper.getDBConnection();
              con.setAutoCommit(false);
              dataRequest.setConnection(con);

              ids = StringUtil.getIds(request.getParameter(SecurityAdminConstants.COMMON_PARM_REMOVE_ID));
        
              for (int i = 0; i < ids.size(); i++) 
              {
                  inputParams = requestBuilder.getRemoveIdentityInputParams(request, (String)ids.get(i));
                  dataRequest.setInputParams(inputParams);
                  dataRequest.setStatementID(StatementConstants.USERS_REMOVE_IDENTITY);
                  outputMap = (Map) util.execute(dataRequest);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

                  logger.error("status = " + status);
                  logger.error("message = " + message); 
                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO REMOVE IDENTITY.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                          throw new SecurityAdminException("6");
                      } else {
                          throw new SecurityAdminException("12", message);
                      }
                  }    
                }//end for

                con.commit();
          } else {
              // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }               
      } catch (Exception ex ) {
          con.rollback();
          throw ex;
      } finally {
        if (con != null && !con.isClosed()) 
        {
          con.setAutoCommit(true);
          con.close();
        }
      }  
      //this.viewAll(request, response);
    }
    
 /**
  * Handles updating one or more identities and sends back View User Identities page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void update(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException,
           SQLException, 
           SecurityAdminException, 
           Exception 
    {    
      // The ids of the identity are stored in the field 'update_id'
      // Tokenize this string and update each identity.    
      logger.debug("Entering update...");
      logger.debug("Retrieved these id indices to be updated: " 
        + request.getParameter(SecurityAdminConstants.COMMON_PARM_UPDATE_ID));
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO user = null;
      IdentityVO identity = null;
      UserManager userManager = null;
      String credential = null;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_UPDATE))  {  

              try {
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false);
                  user = new UserVO(request);
                  identity = new IdentityVO(request);
                  userManager = new UserManager(user, identity);

                  if(!identity.isValid()) {
                      throw new RequiredFieldMissingException();
                  }
                  userManager.updateIdentity(con, securityToken);

                  //System.out.println("RESET PASS IS " + request.getParameter("resetpass"));

                  if(request.getParameter("resetpass").equals("y"))
                  {
                    credential = userManager.resetPassword(con, securityToken);
                    request.setAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credential);
                  }                  
                  con.commit();
              } catch (Exception ex ) {
                  con.rollback();
                  throw ex;
              } finally {
                  con.setAutoCommit(true);
              }    
              if (con != null) {
                  con.close();
              }              
              this.view(request, response, mapping);
         } else {
              // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }          
      } catch (InvalidRequestException e) {
          // RequiredFieldsMissingException is caught here.
          Document doc = userManager.buildInvalidUpdateIdentityRequestDoc(e, con);   

          File stylesheet = getXSL(SecurityAdminConstants.FN_UPDATE_IDENTITY, mapping);
          TraxUtil traxUtil = TraxUtil.getInstance();
          traxUtil.transform(request, response, doc, stylesheet, getXslPathAndName(SecurityAdminConstants.FN_UPDATE_IDENTITY,mapping),ServletHelper.getParameterMap(request)); 
        
      } catch (Exception ex ) {
          throw ex;
      } finally {
          if (con != null && !con.isClosed()) {
            con.close();
          }
      } 
    } 


    
 /**
  * Handles resetting password.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void resetPassword(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException,
           SQLException, 
           SecurityAdminException, 
           Exception 
    {    
      // The ids of the identity are stored in the field 'update_id'
      // Tokenize this string and update each identity.    
      logger.debug("Entering resetPassword...");
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO user = null;
      IdentityVO identity = null;
      UserManager userManager = null;
      String credential = null;
      
      try
      {
    	  
          // authorization call
          if (securityManager.assertPermission(context, securityToken,SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_UPDATE)  || 
        		  securityManager.assertPermission(context, securityToken,SecurityAdminConstants.Resource_RESET_PASSWORD, SecurityAdminConstants.PERMISSION_UPDATE)  )
          {  
        	  String action = (String)request.getParameter(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
              try {
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false);
                  user = new UserVO(request);
                  identity = new IdentityVO(request);
                  userManager = new UserManager(user, identity);

                  // Have to comment this out for now. Otherwise the existing identities
                  // less than 6 char will fail.
                  if(!identity.isValid()) {
                      // Internal error. 
                      throw new SecurityAdminException("1");
                  }
                  credential = userManager.resetPassword(con, securityToken);
                  request.setAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credential);
                  con.commit();
                                    
                  if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_RESET_USER_PASSWORD))  {
                	  Document doc = DOMUtil.stringToDom("<credential>"+credential+"</credential>");
                	  
                	  response.setContentType("application/json");
                      response.setContentType("text/xml");
                  	  response.getWriter().write(JSONUtil.xmlToJSON(doc));
                  }
                  
              } catch (Exception ex ) {
                  con.rollback();
                  throw ex;
              } finally {
                  con.setAutoCommit(true);
                  if (con != null) {
                      con.close();
                  }
              }    
              if (!(action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_RESET_USER_PASSWORD)))  {              
            	  this.view(request, response, mapping);
              }
         } 
          else {
              // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }          
      } catch (Exception ex ) {
          throw ex;
      } finally {
          if (con != null && !con.isClosed()) {
            con.close();
          }
      } 
    }
 /**
  * Displays the Manage Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  /*
  private void mapRole(HttpServletRequest request, HttpServletResponse response)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, SecurityAdminException, Exception 
    { 
      logger.debug("Entering mapRole...");
      Connection con = null;
      String xslFile = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
    
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_UPDATE))  {         
              DataAccessUtil util = DataAccessUtil.getInstance();
              DataRequest dataRequest = new DataRequest();

              con = DataRequestHelper.getDBConnection();
              dataRequest.setConnection(con);
              HashMap inputParams = requestBuilder.getViewIdentityRolesInputParams(request);
              dataRequest.setInputParams(inputParams);
              dataRequest.setStatementID(StatementConstants.USERS_VIEW_IDENTITY_ROLES);

              // Get roles.
              Document rdoc = (Document) util.execute(dataRequest);
      
              // Build Identity doc.
              Document idoc = XMLHelper.getDocumentWithRoot(IdentityVO.XML_BOTTOM);
              idoc.getDocumentElement().setAttribute
                (IdentityVO.TB_IDENTITY_ID, request.getParameter(IdentityVO.RQ_ROLE_IDENTITY_ID));
        
              // Append Role to Identity.
              DOMUtil.addSection(idoc, rdoc.getElementsByTagName(RoleVO.XML_BOTTOM));
        
              // Build User XML doc.
              Document doc = XMLHelper.getDocumentWithRoot(UserVO.XML_BOTTOM);
              doc.getDocumentElement().setAttribute(UserVO.TB_USER_ID, request.getParameter(UserVO.TB_USER_ID));
              doc.getDocumentElement().setAttribute(UserVO.TB_FIRST_NAME, request.getParameter(UserVO.TB_FIRST_NAME));
              doc.getDocumentElement().setAttribute(UserVO.TB_LAST_NAME, request.getParameter(UserVO.TB_LAST_NAME));

              // Append Identity to User.
              DOMUtil.addSection(doc, idoc.getElementsByTagName(IdentityVO.XML_BOTTOM));

              TraxUtil traxUtil = TraxUtil.getInstance();
              File stylesheet = new File(getServletContext().getRealPath(SecurityAdminConstants.XSL_MANAGE_ROLE));    
              traxUtil.transform(request, response, doc, stylesheet, ServletHelper.getParameterMap(request));   
          } else {
              // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet);
          }  
      } catch (Exception e) {
        logger.error(e);
        throw e;
      } finally {
        if (con != null) 
        {
          con.close();
        }
      }  
    }
*/
 /**
  * Adds one or more roles to an identity and sends back Manage Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void addRole(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    { 
      // The ids of the roles are stored in the field 'role_ids'
      // Tokenize this string and add each role.    
      logger.debug("Entering addRole...");
      logger.debug("Retrieved these role id indices to be added: " 
        + request.getParameter(RoleVO.RQ_ROLE_IDS));
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO user = null;
      IdentityVO identity = null;
      UserManager userManager = null;
      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_UPDATE))  {         

              try {
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false);

                  user = new UserVO(request);
                  identity = new IdentityVO(request);
                  userManager = new UserManager(user, identity);

                  // Retrieve the role id list to be added and set it on identity vo.
                  ArrayList ids = StringUtil.getIds(request.getParameter(RoleVO.RQ_ROLE_IDS));
                  String[] roleIds = new String[ids.size()];
                  for (int i = 0; i < ids.size(); i++) {
                      roleIds[i] = request.getParameter(IdentityVO.RQ_ROLE_ADD_ROLE_ID + (String)ids.get(i));
                  }
                  identity.setRoleIdList(roleIds);
                  userManager.addIdentityRoles(securityToken, con);
                  con.commit();
                  this.view(request, response, mapping);
              } catch (Exception e){
                  con.rollback();
                  throw e;
              } finally {
                  con.setAutoCommit(true);
                  if (con != null) {
                    con.close();
                  }
              }
      
          } else {
            // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }  
      } catch (Exception ex ) {
          throw ex;
      } finally {
          if (con != null) {
            con.close();
          }
      } 
    } 

 /**
  * Removes one or more roles to an identity and sends back Manage Role page.
  * @param request http request
  * @param response http response
  * @throws SAXException
  * @throws ParserConfigurationException
  * @throws IOException
  * @throws SQLException
  * @throws SecurityAdminException
  * @throws Exception
  */ 
  private void removeRole(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping)
    throws SAXException, 
           ParserConfigurationException, 
           IOException, 
           SQLException, 
           SecurityAdminException, 
           Exception 
    {    
      logger.debug("Entering removeRole...");
      logger.debug("Retrieved these role id indices to be removed: " + request.getParameter(RoleVO.RQ_ROLE_IDS));
      Connection con = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      UserVO user = null;
      IdentityVO identity = null;
      UserManager userManager = null;      
      try
      {
          // authorization call
          if (securityManager.assertPermission(context, securityToken,
          SecurityAdminConstants.RESOURCE_IDENTITY, SecurityAdminConstants.PERMISSION_UPDATE))  {         

              try {
                  con = DataRequestHelper.getDBConnection();
                  con.setAutoCommit(false);

                  user = new UserVO(request);
                  identity = new IdentityVO(request);
                  userManager = new UserManager(user, identity);

                  // Retrieve the role id list to be removed and set it on identity vo.
                  ArrayList ids = StringUtil.getIds(request.getParameter(RoleVO.RQ_ROLE_IDS));
                  String[] roleIds = new String[ids.size()];
                  for (int i = 0; i < ids.size(); i++) {
                      roleIds[i] = request.getParameter(IdentityVO.RQ_ROLE_REMOVE_ROLE_ID + (String)ids.get(i));
                  }
                  identity.setRoleIdList(roleIds);
                  userManager.removeIdentityRoles(securityToken, con);
                  con.commit();
                  this.view(request, response, mapping);
              } catch (Exception e){
              System.out.println("EXCEPTION");
                  con.rollback();
                  throw e;
              } finally {
                  con.setAutoCommit(true);
                  if (con != null) {
                    con.close();
                  }
              }      
          } else {
            // display 'Not Authorized' message
              logger.error(SecurityAdminConstants.NOT_AUTHORIZED);
              ServletHelper.handleError(request, response, new SecurityAdminException("15"), errorStylesheet, errorStylesheetName);
          }                
      } catch (Exception ex ) {
          con.rollback();          
          throw ex;
      } finally {
        if (con != null && !con.isClosed()) {
          con.setAutoCommit(true);
          con.close();
        }
      } 
      //this.mapRole(request, response);
    }     

       /**
       * Retrieve name of Action XSL file
       * @param1 String - the xsl name  
       * @param2 ActionMapping
       * @return File - XSL File name
       * @throws none
       */
       private File getXSL(String xslName, ActionMapping mapping)
       {

         File xslFile = null;
         String xslFilePathAndName = null;

         ActionForward forward = mapping.findForward(xslName);
         xslFilePathAndName = forward.getPath();
         //get real file name
         xslFile = new File(this.getServlet().getServletContext().getRealPath(xslFilePathAndName));
         return xslFile;
       } 
       
    /**
     * Retrieves XSL path and name
     * @param forwardName
     * @param mapping
     * @return
     */
    private String getXslPathAndName(String forwardName, ActionMapping mapping)
    {
        String xslFilePathAndName = null;

        ActionForward forward = mapping.findForward(forwardName);
        xslFilePathAndName = forward.getPath();
        
        return xslFilePathAndName;
    }       
}