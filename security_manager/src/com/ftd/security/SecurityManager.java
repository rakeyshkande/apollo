package com.ftd.security;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.exceptions.AuthenticationException;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.security.exceptions.TooManyAttemptsException;
import com.ftd.security.service.model.Resource;

public class SecurityManager
{
  private static SecurityManager SECURITYMANAGER = new SecurityManager();
  private Logger logger = new Logger("com.ftd.security.SecurityManager");;

  // resource bundle base
  private static final String RESOURCE_PROPERTITY_FILE = "SecurityResources";
  private static final String EXPIRED_IDENTITY = "expired_identity";
  private static final String EXPIRED_CREDENTIALS = "expired_credentials";
  private static final String INVALID_IDENTITY_AND_CREDENTIALS = "invalid_identity_and_credentials";
  private static final String IDENTITY_LOCKED = "identity_locked";
  private static final String TOO_MANY_ATTEMPTS = "too_many_attempts";
  private static final String EXPIRED_SESSION = "expired_session";
  private static final String INVALID_SESSION = "invalid_session";
  private static final String DATA_SOURCE_NAME = "ORDER SCRUB";

  
 /**
  * The static initializer for the SecurityManager. It ensures that
  * at a given time there is only a single instance
  *
  * @return the SecurityManager
  **/
  public static synchronized SecurityManager getInstance() throws Exception
  {
      return SECURITYMANAGER;
  }

  /**
   * The private Constructor
   **/
  private SecurityManager()
  {
    super();
  }


    /**************************Identity****************************************/

    /**
     * Return the user information for a security token. It will return a null
     * if the security token is invalid.
     *
     * @param newSecurityToken the security token
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @return the the user information for a security token
     */
     public UserInfo getUserInfo(Object newSecurityToken)
        throws SQLException, IOException, ParserConfigurationException, SAXException, Exception
     {
        if (newSecurityToken == null)  {
            logger.error("Security Token cannot be null");
            return null;
        }

        Connection con = null;
        String securityToken = (String) newSecurityToken;
        UserInfo userInfo = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("securityToken", securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("users_pkg.view_user_by_session_id");

          Document userInfoDoc = (Document) util.execute(request);
          //userInfoDoc.print(System.out);

          // populate the User Info Object
          userInfo = new UserInfo();
          userInfo.setAddress1((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@address_1")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@address_1").getNodeValue());
          //logger.debug( "Address1:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@address_1") );
          //userInfo.setAddress2((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@address_2")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@address_2").getNodeValue());
          userInfo.setCity((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@city")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@city").getNodeValue());
          //logger.debug( "city:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@city") );
          userInfo.setEmail((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@email")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@email").getNodeValue());
          //logger.debug( "email:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@email") );
          userInfo.setExpireDate((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@expire_date")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@expire_date").getNodeValue());
          //logger.debug( "expire_date:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@expire_date") );
          userInfo.setExtension((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@extension")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@extension").getNodeValue());
          //logger.debug( "extension:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@extension") );
          userInfo.setFax((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@fax")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@fax").getNodeValue());
          //logger.debug( "fax:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@fax") );
          userInfo.setFirstName((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@first_name")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@first_name").getNodeValue());
          //logger.debug( "first_name:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@first_name") );
          userInfo.setLastName((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@last_name")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@last_name").getNodeValue());
          //logger.debug( "last_name:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@last_name") );
          userInfo.setPhone((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@phone")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@phone").getNodeValue());
          //logger.debug( "phone:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@phone") );
          userInfo.setState((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@state")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@state").getNodeValue());
          //logger.debug( "state:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@state") );
          userInfo.setUserID((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@identity_id")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@identity_id").getNodeValue());
          //logger.debug( "identity_id:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@identity_id") );
          userInfo.setZip((DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@zip")==null)?"":DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@zip").getNodeValue());
          //logger.debug( "zip:: " + DOMUtil.selectSingleNode(userInfoDoc, "/USERS/USER/@zip") );

        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return userInfo;
     }

  /**
   * Return the user information for a security role. 
   * 
   * @param role
   * @param context
   * @return 
   * @throws java.sql.SQLException
   * @throws java.lang.Exception
   */
    public List getUsersInRole(String role, String context)
      throws SQLException, Exception
    {
      Connection con = null;
      UserInfo userInfo = null;
      
      ArrayList userInfoList = new ArrayList();
      
      try 
      {
        con = DataSourceUtil.getInstance().getConnection(DATA_SOURCE_NAME);
        logger.debug(con.getMetaData().getURL());
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();      
  
        dataRequest.setConnection(con);      
        dataRequest.setStatementID("users_pkg.view_user_by_role_name");
        
        HashMap inputParams = new HashMap();
        inputParams.put("roleName", role);
        inputParams.put("context", context);
        dataRequest.setInputParams(inputParams);
        
        Document userInfoDoc = (Document) dau.execute(dataRequest);
        NodeList usersNL = DOMUtil.selectNodes(userInfoDoc, "/USERS/USER");
        Element user = null;
        
        for (int i = 0; i < usersNL.getLength(); i++) 
        {
          user = (Element)usersNL.item(i);
          
          // populate the User Info Object
          userInfo = new UserInfo();
          userInfo.setAddress1(user.getAttribute("address_1"));
          //userInfo.setAddress2((user.getAttribute("address_2"));
          userInfo.setCity(user.getAttribute("city"));
          userInfo.setEmail(user.getAttribute("email"));
          userInfo.setExpireDate(user.getAttribute("expire_date"));
          userInfo.setExtension(user.getAttribute("extension"));
          userInfo.setFax(user.getAttribute("fax"));
          userInfo.setFirstName(user.getAttribute("first_name"));
          userInfo.setLastName(user.getAttribute("last_name"));
          userInfo.setPhone(user.getAttribute("phone"));
          userInfo.setState(user.getAttribute("state"));
          userInfo.setUserID(user.getAttribute("identity_id"));
          userInfo.setZip(user.getAttribute("zip"));
          
          userInfoList.add(userInfo);
        }
              
      } finally 
      {
        if (con != null && (! con.isClosed() )) 
        {
            con.close();
        }
      }
      
      return userInfoList;      
    }

    /**************************Authenticate*************************************/
    /**
     * Authenticates an identity
     *
     * @param context the context of the call
     * @param unitID used for tracking the user
     * @param identity the identity of a user
     * @param credentials the credentials of the identity
     * @throws AuthenticationException
     * @throws ExpiredIdentityException
     * @throws ExpiredCredentialsException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @return the security token
     */
     public Object authenticateIdentity(String context, String unitID, String identity, Object credentials)
        throws  AuthenticationException,
                ExpiredIdentityException,
                ExpiredCredentialsException,
                TooManyAttemptsException,
                SAXException,
                ParserConfigurationException, IOException, SQLException, Exception
     {
        Connection con = null;
        String securityToken = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", context);
          inputParams.put("identity", identity);
          inputParams.put("credentials", credentials);
          GUIDGenerator guidGenerator = GUIDGenerator.getInstance();
          securityToken = guidGenerator.getGUID();
          inputParams.put("securityToken", securityToken);
          inputParams.put("unitId", unitID);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_identity");

          Map outputParameters = (Map) util.execute(request);

          String status = (String) outputParameters.get("status");
          String message = (String) outputParameters.get("message");
          logger.debug("status:: " + status);
          logger.debug("message:: " + message);

          // check messages
          if (message != null)  {
            message = message.trim();

            if (message.equalsIgnoreCase("EXPIRED IDENTITY"))  {
                throw new ExpiredIdentityException(getResourceValue(EXPIRED_IDENTITY));
            }
            if (message.equalsIgnoreCase("EXPIRED CREDENTIALS"))  {
                throw new ExpiredCredentialsException(getResourceValue(EXPIRED_CREDENTIALS));
            }
            if (message.equalsIgnoreCase("INVALID IDENTITY AND CREDENTIALS"))  {
                throw new AuthenticationException(getResourceValue(INVALID_IDENTITY_AND_CREDENTIALS));
            }
            if (message.equalsIgnoreCase("IDENTITY LOCKED"))  {
                throw new AuthenticationException(getResourceValue(IDENTITY_LOCKED));
            }
            if (message.equalsIgnoreCase("TOO MANY FAILED LOGIN ATTEMPTS"))  {
                throw new TooManyAttemptsException(getResourceValue(TOO_MANY_ATTEMPTS));
            }
          }

          // check status
          if (status != null) {
            status = status.trim();
            if (status.equalsIgnoreCase("N"))  {
              throw new AuthenticationException("Invalid Credentials. Please re-enter.");
            }
          }

        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return securityToken;
     }

    /**
     * Authenticates an identity
     *
     * @param context the context of the call
     * @param unitID used for tracking the user
     * @param securityToken the security token
     * @throws ExpiredIdentityException
     * @throws ExpiredSessionException
     * @throws InvalidSessionException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @return true or false
     */
      public boolean authenticateSecurityToken(String context, String unitID, Object securityToken)
        throws  ExpiredIdentityException,
                ExpiredSessionException,
                InvalidSessionException,
                TooManyAttemptsException,
                SAXException,
                ParserConfigurationException, IOException, SQLException, Exception
      {
        Connection con = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", context);
          logger.debug("CONTEXT:: " +context);
          inputParams.put("securityToken", securityToken);
          logger.info("securityToken:: " +securityToken);
          inputParams.put("unitId", unitID);
          logger.debug("unitId:: " + unitID);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_security_token");

          Map outputParameters = (Map) util.execute(request);

          String status = (String) outputParameters.get("status");
          String message = (String) outputParameters.get("message");
          logger.debug("status:: " + status);
          logger.debug("message:: " + message);

          // check messages
          if (message != null)  {
            message = message.trim();
            if (message.equalsIgnoreCase("EXPIRED IDENTITY"))  {
                throw new ExpiredIdentityException(getResourceValue(EXPIRED_IDENTITY));
            }
            if (message.equalsIgnoreCase("EXPIRED SESSION"))  {
                throw new ExpiredSessionException(getResourceValue(EXPIRED_SESSION));
            }
            if (message.equalsIgnoreCase("INVALID SESSION"))  {
                throw new InvalidSessionException(getResourceValue(INVALID_SESSION));
            }

          }
          // check status
          if (status != null)  {
            status = status.trim();
            if (status.equalsIgnoreCase("Y"))  {
                return true;
            } else if (status.equalsIgnoreCase("N"))  {
              return false;
            }
          }


        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return false;
      }

    /**
     * Authenticates an identity
     *
     * @param context the context of the call
     * @param identity the identity of a user
     * @param credentials the credentials of the identity
     * @throws AuthenticationException
     * @throws ExpiredIdentityException
     * @throws ExpiredCredentialsException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @return the security token
     * @deprecated
     */
     public Object authenticate(String context, String identity, Object credentials)
        throws  AuthenticationException,
                ExpiredIdentityException,
                ExpiredCredentialsException,
                TooManyAttemptsException,
                SAXException,
                ParserConfigurationException, IOException, SQLException, Exception
     {
        Connection con = null;
        String securityToken = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", context);
          inputParams.put("identity", identity);
          inputParams.put("credentials", credentials);
          GUIDGenerator guidGenerator = GUIDGenerator.getInstance();
          securityToken = guidGenerator.getGUID();
          inputParams.put("securityToken", securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_identity");

          Map outputParameters = (Map) util.execute(request);

          String status = (String) outputParameters.get("status");
          String message = (String) outputParameters.get("message");
          logger.debug("status:: " + status);
          logger.debug("message:: " + message);

          // check messages
          if (message != null)  {
            message = message.trim();

            if (message.equalsIgnoreCase("EXPIRED IDENTITY"))  {
                throw new ExpiredIdentityException("The Identity has been expired.");
            }
            if (message.equalsIgnoreCase("EXPIRED CREDENTIALS"))  {
                throw new ExpiredCredentialsException("The credentials for the identity have been expired.");
            }
            if (message.equalsIgnoreCase("INVALID IDENTITY AND CREDENTIALS"))  {
                throw new AuthenticationException("Invalid Identity and Credentials.");
            }
            if (message.equalsIgnoreCase("TOO MANY FAILED LOGIN ATTEMPTS"))  {
                throw new TooManyAttemptsException("Userid and password expired due to failed login attempts.");
            }
          }

          // check status
          if (status != null) {
            status = status.trim();
            if (status.equalsIgnoreCase("N"))  {
              throw new AuthenticationException("Invalid Credentials. Please re-enter.");
            }
          }

        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return securityToken;
     }

    /**
     * Authenticates an identity
     *
     * @param context the context of the call
     * @param securityToken the security token
     * @throws ExpiredIdentityException
     * @throws ExpiredSessionException
     * @throws InvalidSessionException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     * @return true or false
     * @deprecated
     */
      public boolean authenticate(String context, Object securityToken)
        throws  ExpiredIdentityException,
                ExpiredSessionException,
                InvalidSessionException,
                TooManyAttemptsException,
                SAXException,
                ParserConfigurationException, IOException, SQLException, Exception
      {
        Connection con = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", context);
          logger.info("CONTEXT:: " +context);
          inputParams.put("securityToken", securityToken);
          logger.info("securityToken:: " +securityToken);
          request.setInputParams(inputParams);
          request.setStatementID("authenticate_security_token");

          Map outputParameters = (Map) util.execute(request);

          String status = (String) outputParameters.get("status");
          String message = (String) outputParameters.get("message");
          logger.debug("status:: " + status);
          logger.debug("message:: " + message);

          // check messages
          if (message != null)  {
            message = message.trim();
            if (message.equalsIgnoreCase("EXPIRED IDENTITY"))  {
                throw new ExpiredIdentityException("The Identity has been expired");
            }
            if (message.equalsIgnoreCase("EXPIRED SESSION"))  {
                throw new ExpiredSessionException("The session for the identity has been expired");
            }
            if (message.equalsIgnoreCase("INVALID SESSION"))  {
                throw new InvalidSessionException("Invalid session for the identity");
            }

          }
          // check status
          if (status != null)  {
            status = status.trim();
            if (status.equalsIgnoreCase("Y"))  {
                return true;
            } else if (status.equalsIgnoreCase("N"))  {
              return false;
            }
          }


        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return false;
      }


    /************************Authorization**************************************/

   /**
    * Assert a permission on a resource
    *
    * @param context the context of the call
    * @param securityToken security token established for an identity
    * @param resource the resource
    * @param permission the  permission
    * @return true or false
    * @throws SAXException
    * @throws ParserConfigurationException
    * @throws IOException
    * @throws SQLException
    * @throws Exception
    */
    public boolean assertPermission(String context,
                                      Object securityToken,
                                      String resource,
                                      String permission)
      throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    {
        Connection con = null;
        try
        {
          DataAccessUtil util = DataAccessUtil.getInstance();
          DataRequest request = new DataRequest();
          DataSourceUtil dsUtil = DataSourceUtil.getInstance();

          con = dsUtil.getConnection(DATA_SOURCE_NAME);

          request.setConnection(con);
          HashMap inputParams = new HashMap();
          inputParams.put("context", context);
          inputParams.put("securityToken", securityToken);
          inputParams.put("resource", resource);
          inputParams.put("permission", permission);
          request.setInputParams(inputParams);
          request.setStatementID("assert_permission");

          Map outputParameters = (Map) util.execute(request);

          String status = (String) outputParameters.get("status");
          String message = (String) outputParameters.get("message");

          // check messages
          logger.debug("status is:" + status);
          logger.debug("message is:" + message);
          // check status
          if (status != null)  {
            status = status.trim();
            if (status.equalsIgnoreCase("Y"))  {
                return true;
            } else if (status.equalsIgnoreCase("N"))  {
              return false;
            }
          }

        }
        finally
        {
          if (con != null)
          {
            con.close();
          }
        }
        return false;
    }



    /*****************************Role******************************************/

    /**
     * Returns whether or not an identity is in a role
     *
     * @param context the context of the call
     * @param identity the identity of a user
     * @param role the role
     * @return true or false
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public boolean isCallerInRole(String context, String identity, String role)
      throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    {
      return this.resolveCallerInRole(context, null, identity, role);
    }

    /**
     * Returns whether or not an identity is in a role
     *
     * @param context the context of the call
     * @param securityToken security token established for an identity
     * @param role the role
     * @return true or false
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public boolean isCallerInRole(String context, Object securityToken, String role)
      throws SAXException, ParserConfigurationException, IOException, SQLException, Exception
    {
      return this.resolveCallerInRole(context, securityToken, null, role);
    }

    /**
     * Returns whether or not an identity is in a role
     *
     * @param context the context of the call
     * @param securityToken security token established for an identity
     * @param identity the identity of a user
     * @param role the role
     * @return true or false
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    private boolean resolveCallerInRole(String context,
                                        Object securityToken,
                                        String identity,
                                        String role)
      throws SAXException,
              ParserConfigurationException,
              IOException,
              SQLException,
              Exception
    {
      Connection con = null;
      try
      {
        DataAccessUtil util = DataAccessUtil.getInstance();
        DataRequest request = new DataRequest();
        DataSourceUtil dsUtil = DataSourceUtil.getInstance();

        con = dsUtil.getConnection(DATA_SOURCE_NAME);

        request.setConnection(con);
        HashMap inputParams = new HashMap();
        inputParams.put("securityToken", identity);
        inputParams.put("identity", identity);
        inputParams.put("role", role);
        inputParams.put("context", context);
        request.setInputParams(inputParams);
        request.setStatementID("is_caller_in_role");
        CachedResultSet crset = (CachedResultSet) util.execute(request);

        if (crset.getRowCount() > 0)
        {
            return true;
        }
        else
        {
          return false;
        }
      }
      finally
      {
        if (con != null)
        {
          con.close();
        }
      }
    }
    /*****************Session Management****************************************/

    /**
     * Compares the user session time to the timeout value configured for the
     * application.
     *
     * @param context the context of the call
     * @param securityToken security token established for an identity
     * @return whether or not a session has timed out
     */
     public boolean isSessionValid(String context, Object securityToken){
       return true;
     }

     /**
      * Returns a String containing the value of the named parameter,
      * or null if the parameter does not exist.
      *
      * @param context the context of the call
      * @param paramName a String specifying the name of the parameter
      * @return a String containing the value of the parameter
      */
     public String getConfigParameter(String context, String name) {
       return null;
     }

    /**
     * Returns the value from resource bundle
     */
    private String getResourceValue(String key) throws Exception {
        //use default locale
        ResourceBundle secResources = ResourceBundle.getBundle(RESOURCE_PROPERTITY_FILE);
        return secResources.getString(key);
    }     
    /***************************************************************************/
    
    
	/** Retrieves the list of permissions for a given user session and resource.
	 * @param context
	 * @param securityToken
	 * @param resource
	 * @return
	 * @throws Exception
	 */
	public List<String> assertPermission(String context, Object securityToken,
			String resource) throws Exception {
		Connection con = null;
		HashMap inputParams = null;
		
		try {
			DataAccessUtil util = DataAccessUtil.getInstance();
			DataRequest request = new DataRequest();
			DataSourceUtil dsUtil = DataSourceUtil.getInstance();

			con = dsUtil.getConnection(DATA_SOURCE_NAME);
			
			request.setConnection(con);
			inputParams = new HashMap();
			inputParams.put("context", context);
			inputParams.put("securityToken", securityToken);
			inputParams.put("resource", resource);
			request.setInputParams(inputParams);
			request.setStatementID("assert_resource_permission");
			
			CachedResultSet rs = (CachedResultSet)util.execute(request);
			
			List<String> permissions = new ArrayList<String>();		
			while(rs.next()) {
				permissions.add(rs.getString("permission_id"));
			}
			return permissions;
			
		} catch (Exception e) {
			throw new Exception("Error caught getting permissions associated with a resource : " + e.getMessage());
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
    
}