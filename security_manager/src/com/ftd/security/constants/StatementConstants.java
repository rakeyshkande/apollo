package com.ftd.security.constants;

/**
 * Statement id Constants
 * @author Christy Hu
 */
public class StatementConstants
{
  public static final String USERS_VIEW_USER = "users_pkg.view_user";
  public static final String USERS_VIEW_IDENTITIES = "users_pkg.view_user_identities";
  public static final String USERS_VIEW_IDENTITY_ROLES = "users_pkg.view_identity_roles";
  public static final String USERS_VIEW_USERS = "view_users";
  public static final String USERS_ADD_USERS = "add_user";
  public static final String GET_USERID_FROM_IDENTITY = "get_userid_from_identity";
  public static final String USERS_REMOVE_USER = "users_pkg.remove_user";
  public static final String USERS_UPDATE_USER = "update_user";
  public static final String USERS_SEARCH_USERS = "users_pkg.search_users";
  public static final String USERS_SEARCH_IDENTITY_ROLES = "users_pkg.search_identity_roles";
  public static final String ROLE_VIEW_ROLES = "role_pkg.view_roles";
  public static final String ROLE_ADD_ROLE = "role_pkg.add_role";
  public static final String ROLE_VIEW_ROLE = "role_pkg.view_role";
  public static final String ROLE_VIEW_ROLE_ACLS = "role_pkg.view_role_acls";
  public static final String ROLE_REMOVE_ROLE = "role_pkg.remove_role";
  public static final String ROLE_UPDATE_ROLE = "role_pkg.update_role";
  public static final String ROLE_UPDATE_ROLE_ACL = "role_pkg.update_rel_role_acl";
  public static final String ROLE_REMOVE_ROLE_ACL = "role_pkg.remove_rel_role_acl";
  public static final String CONTEXT_VIEW_CONTEXTS = "context_pkg.view_contexts";  
  public static final String ACL_VIEW_RESOURCE = "acl_pkg.view_resource";
  public static final String ACL_VIEW_RESOURCES = "acl_pkg.view_resources";
  public static final String ACL_RESOURCE_EXISTS = "acl_pkg.resource_exists";
  public static final String ACL_UPDATE_RESOURCE = "acl_pkg.update_resource";
  public static final String ACL_REMOVE_RESOURCE = "acl_pkg.remove_resource";
  public static final String ACL_VIEW_PERMISSION = "acl_pkg.view_permission";
  public static final String USERS_IDENTITY_EXISTS = "users_pkg.identity_exists";
  public static final String USERS_ADD_IDENTITY = "users_pkg.add_identity";
  public static final String USERS_REMOVE_IDENTITY = "users_pkg.remove_identity";  
  public static final String USERS_UPDATE_IDENTITY = "users_pkg.update_identity";
  public static final String USERS_UPDATE_CREDENTIALS = "users_pkg.update_credentials";
  public static final String ROLE_UPDATE_IDENTITY_ROLE = "role_pkg.update_rel_identity_role";
  public static final String ROLE_REMOVE_IDENTITY_ROLE = "role_pkg.remove_rel_identity_role";  
  public static final String USERS_VIEW_IDENTITY = "users_pkg.view_identity";     
  public static final String ACL_VIEW_ACL = "acl_pkg.view_acl";
  public static final String ACL_UPDATE_PERMISSION = "acl_pkg.update_permission";
  public static final String ACL_REMOVE_PERMISSION = "acl_pkg.remove_permission";  
  public static final String ACL_VIEW_PERMISSIONS = "acl_pkg.view_permissions"; 
  public static final String ACL_PERMISSION_EXISTS = "acl_pkg.permission_exists";
  public static final String CONTEXT_VIEW_CONTEXT = "context_pkg.view_context";
  public static final String CONTEXT_CONTEXT_EXISTS = "context_pkg.context_exists";
  public static final String CONTEXT_UPDATE_CONTEXT = "context_pkg.update_context";
  public static final String CONTEXT_REMOVE_CONTEXT = "context_pkg.remove_context";
  public static final String CONTEXT_VIEW_CONFIG = "context_pkg.view_config"; 
  public static final String CONTEXT_REMOVE_CONFIG = "context_pkg.remove_config";
  public static final String CONTEXT_UPDATE_CONFIG = "context_pkg.update_config";
  public static final String CONTEXT_CONFIG_EXISTS = "context_pkg.context_config_exists";
  public static final String ACL_REMOVE_ACL_NAME = "acl_pkg.remove_acl_name";
  public static final String ACL_ADD_ACL = "acl_pkg.add_acl";
  public static final String CONTEXT_REMOVE_ACL_RP = "acl_pkg.remove_acl_rp";
  public static final String ACL_VIEW_CONTEXT_RESOURCES = "acl_pkg.view_context_resources";
  public static final String ACL_VIEW_ACL_RESOURCES = "acl_pkg.view_acl_resources";
  public static final String ACL_VIEW_CONTEXT_ACL_NAMES = "acl_pkg.view_context_acl_names";
  public static final String ACL_NAME_EXISTS = "acl_pkg.acl_name_exists";
  public static final String GROUP_UPDATE_GROUP = "users_pkg.update_group_id";
  public static final String BULK_USER_TERMINATE = "users_pkg.bulk_user_terminate";
  public static final String USERS_VIEW_DEPARTMENTS = "users_pkg.view_departments";
  public static final String USERS_VIEW_CALLCENTERS = "users_pkg.view_callcenters";
  public static final String USERS_VIEW_CSGROUPS = "users_pkg.view_csgroups";
  public static final String ROLES_VIEW_ROLES_BY_CONTEXT = "role_pkg.view_roles_by_context";
}