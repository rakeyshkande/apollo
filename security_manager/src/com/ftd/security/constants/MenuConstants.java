 package com.ftd.security.constants;

 /**
  * Constants
  * @author Christy Hu
  */
 public class MenuConstants
{

    public static final String XSL_MAINMENU = "/xsl/menu/mainMenu.xsl";
    public static final String XSL_MARKETING_MENU = "/xsl/menu/marketingMenu.xsl";
    public static final String XSL_CUSTOMERSERVICE_MENU = "/xsl/menu/customerServiceMenu.xsl";
    public static final String XSL_OPERATIONS_MENU = "/xsl/menu/operationsMenu.xsl";
    public static final String XSL_MERCHANDISING_MENU = "/xsl/menu/merchandisingMenu.xsl";
    public static final String XSL_ACCOUNTING_MENU = "/xsl/menu/accountingFinanceMenu.xsl";
    public static final String XSL_SECURITY_ADMIN_MENU = "/xsl/menu/securityAdminMenu.xsl";
    public static final String XSL_SYSTEM_ADMIN_MENU = "/xsl/menu/adminMain.xsl";
    public static final String XSL_DISTRIBUTION_MENU = "/xsl/menu/distributionFulfillmentMenu.xsl";
    public static final String XSL_WORKFORCE_REPORTS_MENU = "/xsl/menu/workforceReportsMenu.xsl";


    // action for main menu landing page
    public static final String ACTION_PARM_LOGOFF = "logoff";
    public static final String ACTION_PARM_DISPLAYCHANGEPASSWORD = "displayChangePassword";
    public static final String ACTION_PARM_CHANGEPASSWORD = "change_password";
    public static final String ACTION_PARM_MARKETING = "marketing";
    public static final String ACTION_PARM_CUSTOMERSERVICE = "customerService";
    public static final String ACTION_PARM_DISTRIBUTION = "distributionFulfillment";
    public static final String ACTION_PARM_OPERATIONS = "operations";
    public static final String ACTION_PARM_MERCHANDISING = "merchandising";
    public static final String ACTION_PARM_SYSTEMADMIN = "systemAdministration";
    public static final String ACTION_PARM_SECURITYADMIN = "securityAdministration";
    public static final String ACTION_PARM_ACCOUNTING = "accounting";
    public static final String ACTION_PARM_MAIN = "mainMenu";
    public static final String ACTION_PARM_WORKFORCEREPORTS = "workforceReports";
    //15689 changes
    public static final String ACTION_RESET_USER_PASSWORD = "resetUserPassword";
    
    // session variables
    public static final String SESSION_SOURCE_MENU = "sourceMenu";

    public static final String MENU_SYSTEM_ADMINISTRATION = "systemAdministration";
    public static final String MENU_CUSTOMER_SERVICE = "customerService";

    // common parameters
    public static final String COMMON_PARM_PASSWORDEXPIRED = "passwordExpired";
    public static final String COMMON_PARM_CLOSEWINDOW = "closeWindow";
    public static final String COMMON_PARM_PASSWORD = "password";
    public static final String COMMON_VALUE_TRUE = "true";

    // customer service menu parameters
    public static final String CUST_SERV_PARM_CALL_DNIS = "call_dnis";
    public static final String CUST_SERV_PARM_CALL_BRAND_NAME = "call_brand_name";
    public static final String CUST_SERV_PARM_CALL_CS_NUMBER = "call_cs_number";
    public static final String CUST_SERV_PARM_CALL_TYPE_FLAG = "call_type_flag";
    public static final String CUST_SERV_PARM_T_CALL_LOG_ID = "t_call_log_id";
    public static final String CUST_SERV_PARM_DNIS_ERROR_MSG = "dnis_error_msg";

    // error messages
    public static final String ERROR_PASSWORD_CHANGE_FAILURE = "Your password cannot be changed due to internal error.";

    // permissions
    public static final String PERMISSION_VIEW = "view";
    public static final String PERMISSION_PROCESS = "process";
    public static final String PERMISSION_USE = "use";
    public static final String PERMISSION_MAINTAIN = "maintain";
    public static final String PERMISSION_ACCESS = "access";

    // site name
    public static final String COMMON_PARM_SITENAME = "sitename";
    public static final String COMMON_PARM_SITENAME_SSL = "sitenamessl";
    public static final String COMMON_PARM_SITE_SSL_PORT = "sitesslport";

}

