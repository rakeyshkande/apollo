package com.ftd.security.constants;

/**
 * Constants
 * @author Christy Hu
 */
public class SecurityAdminConstants {
	// Configuration file
	public static final String CONFIG_FILE = "security-config.xml";
	public static final String GLOBAL_PARM_CONTEXT = "SECURITY_MANAGER_CONFIG";
	public static final String GlOBAL_PARM_RELEASE_NUMBER = "releasenumber";
    public static final String GLOBAL_PARM_REPORT_SERVER = "reportserver";

	// Error file
	public static final String ERROR_FILE = "security-errors.xml";

	// Resources specific to Security Admin application.
	public static final String RESOURCE_USER = "User";
	public static final String RESOURCE_IDENTITY = "Identity";
	public static final String RESOURCE_ROLE = "Role";
	public static final String RESOURCE_RESOURCE = "Resource";
	public static final String RESOURCE_PERMISSION = "Permission";
	public static final String RESOURCE_ACL = "ACL";
	public static final String RESOURCE_CONTEXT = "Context";
	public static final String RESOURCE_CONFIG = "Config";

	// 15689
	public static final String Resource_RESET_PASSWORD = "ResetPassword";

	// Permissions specific to Security Admin application.
	public static final String PERMISSION_VIEW = "View";
	public static final String PERMISSION_CREATE = "Add";
	public static final String PERMISSION_REMOVE = "Delete";
	public static final String PERMISSION_UPDATE = "Update";

	// Common output parameters
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";

	// Common parameters
	public static final String COMMON_PARM_ADMIN_ACTION = "adminAction";
	public static final String COMMON_PARM_CONTEXT = "context";
	public static final String COMMON_PARM_SEC_TOKEN = "securitytoken";
	public static final String COMMON_PARM_APP_CONTEXT = "applicationcontext";
	public static final String COMMON_PARM_IDENTITY = "identity";
	public static final String COMMON_PARM_CREDENTIALS = "credentials";
	public static final String COMMON_PARM_SUBJECT = "subject";
	public static final String COMMON_PARM_STYLESHEET = "stylesheet";
	public static final String COMMON_PARM_REMOVE_IDS = "remove_ids";
	public static final String COMMON_PARM_REMOVE_ID = "remove_id";
	public static final String COMMON_PARM_UPDATE_ID = "update_id";
	public static final String COMMON_PARM_UNIT_ID = "unitID";
	public static final String COMMON_PARM_IS_EXIT = "isExit";
	public static final String COMMON_PARM_USER_LIST = "userList";
	public static final String CONFIG_CONTEXT = "MARS_CONFIG";
	public static final String COMMON_CONFIG = "breURL";
	public static final String REPORT_SERVER = "reportserver";
	public static final String COMMON_CONFIG_REPORTS = "marsreportsURL";
	
	// Common constant value
	public static final String COMMON_VALUE_YES = "Y";
	public static final String COMMON_VALUE_NO = "N";

	// Action parameters
	public static final String ACTION_PARM_VIEW = "view";
	public static final String ACTION_PARM_ADD = "add";
	public static final String ACTION_PARM_UPDATE = "update";
	public static final String ACTION_PARM_REMOVE = "remove";
	public static final String ACTION_PARM_DISPLAY_ADD = "display_add";
	public static final String ACTION_PARM_VIEW_ALL = "view_all";
	public static final String ACTION_PARM_ACCOUNT = "account";
	public static final String ACTION_PARM_ADD_ROLE = "addRole";
	public static final String ACTION_PARM_MAP_ROLE = "mapRole";
	public static final String ACTION_PARM_REMOVE_ROLE = "removeRole";
	public static final String ACTION_PARM_ADD_ACL = "add_ACL";
	public static final String ACTION_PARM_MAP_ACL = "map_ACL";
	public static final String ACTION_PARM_REMOVE_ACL = "remove_ACL";
	public static final String ACTION_PARM_VIEW_POPUP = "view_popup";
	public static final String ACTION_PARM_MANAGE_CONFIG = "manage_config";
	public static final String ACTION_PARM_VIEW_CONFIG = "view_config";
	public static final String ACTION_PARM_DISPLAY_ADD_CONFIG = "display_add_config";
	public static final String ACTION_PARM_ADD_CONFIG = "add_config";
	public static final String ACTION_PARM_REMOVE_CONFIG = "remove_config";
	public static final String ACTION_PARM_UPDATE_CONFIG = "update_config";
	public static final String ACTION_PARM_CHANGE_PASSWORD = "change_password";
	public static final String ACTION_PARM_VIEW_UPLOAD = "view_upload";
	public static final String ACTION_PARM_VIEW_USER_GROUP_UPDATE = "view_usr_grp_update";
	public static final String ACTION_PARM_LOAD = "load";
	public static final String ACTION_PARM_LOAD_UPDATE = "loadUpdate";
	public static final String ACTION_PARM_DISPLAY_SEARCH = "display_search";
	public static final String ACTION_PARM_SEARCH = "search";
	public static final String ACTION_PARM_REFRESH_DISPLAY_ADD = "refresh_display_add";
	public static final String ACTION_PARM_RESET_PASSWORD = "reset_password";
	public static final String ACTION_PARM_RESET_USER_PASSWORD = "reset_user_password";

	// public static final String ACTION_PARM_DISPLAY_ADD_IDENTITY =
	// "display_add_dentity";
	// file upload subject
	public static final String ACTION_SUBJECT_USER = "user";

	// servlets
	public static final String MAIN_SERVLET = "/security/Main.do";
	public static final String LOGIN_SERVLET = "/security/Login.do";
	public static final String USER_SERVLET = "/security/SecurityUser.do";

	// Error Strings
	public static final String LOGOFF_MSG = "You have successfully logged off.";
	public static final String ERROR = "ERROR";
	public static final String ERROR_OCCURRED = "ERROR OCCURRED"; // unexpected
																	// DB error
	public static final String ERROR_INVALID_REQUEST = "Invalid Request";
	public static final String ERROR_EMPTY_REQUEST = "Empty Request";
	public static final String INVALID_SECURITY_TOKEN = "Invalid Security Token.";
	public static final String NOT_AUTHORIZED = "Not Authorized to Perform Action.";
	public static final String CONTACT_ADMIN = "Please contact your administrator if you have difficulty signing in.";
	public static final String ERROR_ENTER_VALUE = "Please enter a value.";
	public static final String ERROR_OBJECT_IN_USE = "Object is being used.";
	public static final String ERROR_SEARCH_OPTION_REQUIRED = "Search Option required.";
	// File upload constants
	public static final String FILE_ITEM = "file_item";

	// XML ROOT element
	public static final String XML_ROOT = "ROOT";
	// XML param
	public static final String XML_FIELD_TOP = "FIELDS";
	public static final String XML_FIELD_BOTTOM = "FIELD";
	public static final String XML_ERROR_TOP = "ERRORS";
	public static final String XML_ERROR_BOTTOM = "ERROR";
	public static final String XML_ATTR_FIELD = "fieldname";
	public static final String XML_ATTR_VALUE = "value";
	public static final String XML_ATTR_DESCRIPTION = "description";
	public static final String XML_ATTR_SELECTED = "selected";
	public static final String XML_ATTR_ERROR = "error";

	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss.S";
	public static final String DEFAULT_CONTEXT = "Order Proc";

	public static final String APP_PARAMETERS = "parameters";
	public static final String SOURCE_MENU = "sourceMenu";

	// Struts forward names
	public static final String FN_ERROR = "Error";
	public static final String FN_SUCCESS = "Success";
	public static final String FN_LOGOFFF = "Logoff";
	public static final String FN_LOAD_PASSWORD_CHANGE = "LoadPasswordChange";
	public static final String FN_LOAD_PASSWORD_CHANGE_POPUP = "LoadPasswordChangePopup";
	public static final String FN_CHANGE_PASSWORD_COMPLETE = "ChangePasswordComplete";
	public static final String FN_AUTHENTICATE_PASSWORD = "AuthenticatePassword";
	public static final String FN_LOGIN = "LoadSignOn";
	public static final String FN_NO_SUCH_USER = "NoSuchUser";
	public static final String FN_MARKETING_MENU = "MarketingMenu";
	public static final String FN_CUSTOMER_SERVICE_MENU = "CustomerServiceMenu";
	public static final String FN_OPERATIONS_MENU = "OperationsMenu";
	public static final String FN_MERCHANDISING_MENU = "MerchandisingMenu";
	public static final String FN_ACCOUNTING_MENU = "AccountingMenu";
	public static final String FN_ADMINISTRATIONS_MENU = "AdministrationsMenu";
	public static final String FN_SECURITY_ADMIN_MENU = "SecurityAdminMenu";
	public static final String FN_MAIN_MENU = "MainMenu";
	public static final String FN_DISTRIBUTION_MENU = "DistributionMenu";
	public static final String FN_WORKFORCE_REPORT_MENU = "WorkforceReportsMenu";
	public static final String FN_USER_ACTION = "UserAction";
	public static final String FN_VIEW_ACLLIST = "ViewACLList";
	public static final String FN_VIEW_ACL = "ViewACL";
	public static final String FN_VIEW_ACLPOPUP = "ViewACLPopup";
	public static final String FN_DISPLAY_ADDACL = "LoadAddACL";
	public static final String FN_VIEW_CONTEXTLIST = "ViewContextList";
	public static final String FN_DISPLAY_ADDCONTEXT = "LoadAddContext";
	public static final String FN_VIEW_CONTEXT = "ViewContext";
	public static final String FN_MANAGE_CONFIG = "ManageConfig";
	public static final String FN_DISPLAY_ADDCONFIG = "LoadAddConfig";
	public static final String FN_VIEW_CONFIG = "ViewConfig";
	public static final String FN_DISPLAY_ADDIDENTITY = "LoadAddIdentity";
	public static final String FN_UPDATE_IDENTITY = "UpdateIdentity";
	public static final String FN_DISPLAY_ADDPERMISSION = "LoadAddPermission";
	public static final String FN_VIEW_PERMISSION = "ViewPermission";
	public static final String FN_VIEW_PERMISSIONLIST = "ViewPermissionList";
	public static final String FN_DISPLAY_ADDRESOURCE = "LoadAddResource";
	public static final String FN_VIEW_RESOURCELIST = "ViewResourceList";
	public static final String FN_VIEW_RESOURCE = "ViewResource";
	public static final String FN_UPDATE_USER = "UpdateUser";
	public static final String FN_VIEW_USERLIST = "ViewUserList";
	public static final String FN_DISPLAY_ADDUSER = "LoadAddUser";
	public static final String FN_DISPLAY_SEARCH = "LoadSearchUser";
	public static final String FN_SEARCHUSER_RESULT = "ViewSearchResult";
	public static final String FN_VIEW_ROLELIST = "ViewRoleList";
	public static final String FN_DISPLAY_ADDROLE = "LoadAddRole";
	public static final String FN_VIEW_ROLE = "ViewRole";
	public static final String FN_MAP_ACLTOROLE = "MapAclRole";

	public static final String MULTIPART_PARAM = "multipartParam";

	// 15689 changes
	public static final String FN_RESET_USER_PASSWORD = "ResetUserPasswordMenu";
	public static final String ACTION_PARM_USER_SEARCH = "userSearch";

	public static final String COMMON_PARM_ACTION_TYPE = "actionType";
	public static final String ACTION_USER_UPLOAD = "viewUpload";
	public static final String ACTION_USER_GROUP_UPDATE = "viewUserGroupUpdate";
	public static final String ACTION_BULK_USER_TERMINATE = "viewBulkUserTerminate";
}
