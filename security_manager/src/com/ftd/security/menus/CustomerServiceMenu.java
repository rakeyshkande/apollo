package com.ftd.security.menus;


/**
 * Representation of the Customer Service menu
 * @author Christy Hu
 */

public class CustomerServiceMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public CustomerServiceMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.CustomerServiceMenu";
        init(); // initializes logger

        resourceGroupList.add(new ResourceGroup("Customer Service Menu link",
                              "CustomerService",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("LossPreventionUpdate",
                              "LossPreventionSearch",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Unlock Order link",
                              "UnlockOrder",
                              "Yes",
                              "Yes"));
        resourceGroupList.add(new ResourceGroup("Order Scrub",
                              "OrderScrub",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Re-instate Orders link",
                              "ReinstateOrder",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Bulk Order Processing link",
                              "BulkOrder",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("CS Management",
                              "CSManagement",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Manage Users link",
                              "ManageUser",
                              "View",
                              "View")); 
        resourceGroupList.add(new ResourceGroup("CS View Queue Link",
                              "ViewQueue",
                              "View",
                              "View"));  
        resourceGroupList.add(new ResourceGroup("Gift Certificates/Coupons",
                              "GiftCert",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("CustomerCatalogRequestData",
                              "CustomerCatalogRequest",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("CustReport",
            "ReportLinks",
            "Yes",
            "Yes"));
        resourceGroupList.add(new ResourceGroup("Queue Management Menu link",
                              "QueueManagement",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("OldWebOE",
                              "OldWebOE",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Product Favorites Maint Link",
                              "ProdFavoritesMaintLink",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Card Message Maint Link",
                              "CardMsgMaintLink",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("CallDispositionAdmin",
                              "CallDispMaintLink",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("ServicesInfo",
                "ServicesInfoLink",
                "View",
                "View"));
        resourceGroupList.add(new ResourceGroup("PhoenixData",
				        		"PhoenixAdmin",
				        		"View",
				        		"View"));
        resourceGroupList.add(new ResourceGroup("MARS_REPORTS",
        		"MarsReportsLink",
        		"View",
        		"View"));
        resourceGroupList.add(new ResourceGroup("MARS_BRE",
        		"MarsBreLink",
        		"View",
        		"View"));
        resourceGroupList.add(new ResourceGroup("MARS_SIMULATOR",
        		"MarsSimulatorLink",
        		"View",
        		"View"));
    }
}