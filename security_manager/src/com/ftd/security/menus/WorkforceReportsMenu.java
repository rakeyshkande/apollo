package com.ftd.security.menus;

/**
 * Representation of the Distribution Fulfillment Menu
 * @author Christy Hu
 */

public class WorkforceReportsMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public WorkforceReportsMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.WorkforceReportsMenu";
        init(); // initializes logger
        resourceGroupList.add(new ResourceGroup("CustReport",
            "ReportLinks",
            "Yes",
            "Yes"));         
    }

}