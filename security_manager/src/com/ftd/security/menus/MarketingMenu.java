package com.ftd.security.menus;


/**
 * Representation of the Marketing menu
 * @author Christy  Hu
 */
public class MarketingMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource group on the menu.
     */
    public MarketingMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.MarketingMenu";
        init(); // initializes logger
        resourceGroupList.add(new ResourceGroup("Marketing Menu link",
                              "Marketing",
                              "View",
                              "View"));

        resourceGroupList.add(new ResourceGroup("MktgReport",
                              "ReportLinks",
                              "Yes",
                              "Yes"));

        resourceGroupList.add(new ResourceGroup("PremierCircleData",
        					  "PremierCircleAdmin",
        					  "View",
                			  "View"));
    }

}