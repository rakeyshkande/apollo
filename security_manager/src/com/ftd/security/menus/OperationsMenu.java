package com.ftd.security.menus;


/**
 * Representation of the Operations menu
 * @author Christy Hu
 */

public class OperationsMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public OperationsMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.OperationsMenu";
        init(); // initializes logger

        resourceGroupList.add(new ResourceGroup("Operations Menu link",
                              "Operations",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Item Of The Week Maint Link",
                              "IOTWMaintLink",
                              "View",
                              "View"));                              
    }

}