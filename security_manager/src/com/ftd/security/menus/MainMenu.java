package com.ftd.security.menus;


/**
 * Representation of the main menu
 * @author Christy Hu
 */

public class MainMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public MainMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.MainMenu";
        init(); // initializes logger

        // Initialize resources on this menu.
        // Change resourceLink and permission (the first and third parameter) 
        // if the resource permission definitions change.
        resourceGroupList.add(new ResourceGroup("Customer Service Menu link",
                              "CustomerService",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Operations Menu link",
                              "Operations",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Merchandising Menu link",
                              "Merchandising",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Marketing Menu link",
                              "Marketing",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Accounting/Finance Menu link",
                              "AccountingFinance",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("System Administration link",
                              "SystemAdministration",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Reports Menu link",
                              "Reports",
                              "View",
                              "View"));    
        resourceGroupList.add(new ResourceGroup("Distribution Fulfillment Menu link",
                              "DistributionFulfillment",
                              "View",
                              "View"));        
        resourceGroupList.add(new ResourceGroup("Dashboard Menu link",
                              "Dashboard",
                              "View",
                              "View"));        
        resourceGroupList.add(new ResourceGroup("ResetPassword",
                "ResetPasswordAdmin",
                "View",
                "View"));
        
    }

}