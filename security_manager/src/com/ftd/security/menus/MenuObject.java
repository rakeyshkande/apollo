package com.ftd.security.menus;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Representation of a menu in order processing system
 * @author Christy Hu
 */

public abstract class MenuObject
{
    protected ArrayList resourceGroupList;
    protected SecurityManager securityManager;
    protected Logger logger;
    protected String logger_category = "com.ftd.security.menus.MenuObject";

    public MenuObject()
    {
        try {            
            resourceGroupList = new ArrayList();
            this.securityManager = SecurityManager.getInstance();
        } catch (Exception ex) {
          logger.error(ex);
        }
    }

    protected void init() 
    {
      logger = new Logger(logger_category);
    }
    /**
    *  Go through the resources on menu. Returns the authorized resource and permission
    *  for the specified security token and context
    *  @param securityToken security token
    *  @param context context
    *  @return HashMap map of authorized resource and permission
    *  @throws Exception
    */    
    public HashMap getAuthorizedResources(String securityToken, String context) 
    throws Exception {
        HashMap authorizationMap = new HashMap();
        try {
            if (resourceGroupList != null) {
                for (int i = 0; i < resourceGroupList.size(); i++) {
                    ResourceGroup resourceGroup = (ResourceGroup)resourceGroupList.get(i);
                    String resource = resourceGroup.getResource();
                    String resourceConst = resourceGroup.getResourceConst();
                    String permission = resourceGroup.getPermission();
                    String permissionConst = resourceGroup.getPermissionConst();
                    //logger.info("Swamy resource : "+ resource + "-->"+permission);
                    // If assert permission is successful, add the pair to map.
                    if(securityManager.assertPermission(context, securityToken, resource, permission)) {
                        //logger.info("Swamy resource Constant: "+ resourceConst + "-->"+permission);
                        authorizationMap.put(resourceConst, permission);
                    }
                } 
            }
        } catch (Exception ex) {
            logger.debug(ex);
            throw ex;
        }
        return authorizationMap;
    }

}