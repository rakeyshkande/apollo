package com.ftd.security.menus;


/**
 * Representation of the Customer Service menu
 * @author Christy Hu
 */

public class MerchandisingMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public MerchandisingMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.MerchandisingMenu";
        init(); // initializes logger

        resourceGroupList.add(new ResourceGroup("Merchandising Menu link",
                              "Merchandising",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("MerchReport",
                              "ReportLinks",
                              "Yes",
                              "Yes"));                              
        resourceGroupList.add(new ResourceGroup("Cross Sell Maint Link",
                              "CrossSellMaintLink",
                              "View",
                              "View"));     
        resourceGroupList.add(new ResourceGroup("Carrier Zip Blocking Link",
                              "CarrierZipBlockingLink",
                              "View",
                              "View"));       
        
        resourceGroupList.add(new ResourceGroup("Reprocess Orders Link",
                              "ReprocessOrdersLink",
                              "View",
                              "View"));      

        resourceGroupList.add(new ResourceGroup("Price Pay Vendor Link",
                              "PricePayVendorLink",
                              "View",
                              "View"));
        
        //15420 changes to add the PDBAdminGroup to resource Group
        resourceGroupList.add(new ResourceGroup("PDBMassEdit",
                "PDBMassEdit",
                "View",
                "View"));
    }

}