package com.ftd.security.menus;


/**
 * Representation of the System Administration menu
 * @author Christy Hu
 */

public class SecurityAdministrationMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public SecurityAdministrationMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.SecurityAdministrationMenu";
        init(); // initializes logger

        resourceGroupList.add(new ResourceGroup("User",
                              "ResourceUser",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Role",
                              "ResourceRole",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Resource",
                              "ResourceResource",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("Context",
                              "ResourceContext",
                              "View",
                              "View"));
    }

}