package com.ftd.security.menus;

/**
 * Representation of the Distribution Fulfillment Menu
 * @author Christy Hu
 */

public class DistributionFulfillmentMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public DistributionFulfillmentMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.DistributionFulfillmentMenu";
        init(); // initializes logger
        resourceGroupList.add(new ResourceGroup("DisFullReport",
            "ReportLinks",
            "Yes",
            "Yes"));
        // TODO:
        resourceGroupList.add(new ResourceGroup("OSCAR Maintenance Menu",
        		                                "OSCARMaintenanceDisplay",
        		                                "View",
        		                                "View"));
    
        resourceGroupList.add(new ResourceGroup("PAS Exclusion Zone Maintenance Link",
                "PEZMaintenance",
                "Yes",
                "Yes"));
    
    }

}