package com.ftd.security.menus;


/**
 * Representation of a resource group
 * @author Christy Hu
 */

public class ResourceGroup
{
      //resource defined by the business. Must match the resource defined in System Admin tool.
      private String resource;  
      //resource consts to send back to XSL
      private String resourceConst; 
      //permission defined for each resource. Must match the resource defined in System Admin
      private String permission; 
      //permission consts to send back to XSL
      private String permissionConst; 

      /**
       * Default constructor.
       */
      public ResourceGroup () {
      }

      /**
       * Constructor that initializes business defined resource,
       * the constant resource that business resource maps to,
       * the permission, and the constant permission the business
       * permission maps to.
       * @param inResource business defined resource
       * @param inResourceConst constant resource that business resource maps to
       * @param inPermission business defined permission
       * @param inPermissionConst constant permission that business permission maps to
       */
      public ResourceGroup(String inResource,
                        String inResourceConst,
                        String inPermission,
                        String inPermissionConst)
      {
          resource = inResource;
          resourceConst = inResourceConst;
          permission = inPermission;
          permissionConst = inPermissionConst;
      }

      public String getResource() {
          return resource;
      }

      public String getResourceConst() {
          return resourceConst;
      }

      public String getPermission() {
          return permission;
      }

      public String getPermissionConst() {
          return permissionConst;
      }
}
