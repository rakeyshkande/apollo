package com.ftd.security.menus;


/**
 * Representation of the System Administration menu
 * @author Christy Hu
 */

public class SystemAdministrationMenu extends MenuObject
{
    /**
     * Constructor. Initializes the resource groups on the menu.
     */
    public SystemAdministrationMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.SystemAdministrationMenu";
        init(); // initializes logger

        resourceGroupList.add(new ResourceGroup("Security link",
                              "ResourceSecurity",
                              "View",
                              "View"));
        //15689 changes
        resourceGroupList.add(new ResourceGroup("ResetPassword",
                "ResetPasswordAdmin",
                "View",
                "View"));
    }

}