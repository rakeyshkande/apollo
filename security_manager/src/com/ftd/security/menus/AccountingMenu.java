package com.ftd.security.menus;


/**
 * Representation of the Accounting/Finance menu
 * @author Christy Hu
 */

public class AccountingMenu extends MenuObject
{
    /**
     * Constructor. Initialized the resource groups on the menu.
     */
    public AccountingMenu()
    {
        super();
        logger_category = "com.ftd.security.menus.AccountingMenu";
        init(); // initializes logger

        // Initialize resources on this menu.
        // Change resourceLink and permission (the first and third parameter) 
        // if the resource permission definitions change.
        resourceGroupList.add(new ResourceGroup("Accounting/Finance Menu link",
                              "AccountingFinance",
                              "View",
                              "View"));
        resourceGroupList.add(new ResourceGroup("AcctReport",
                              "ReportLinks",
                              "Yes",
                              "Yes"));                              
        resourceGroupList.add(new ResourceGroup("OutstandingReceivablesResource",
                              "OutstandingReceivablesLink",
                              "Yes",
                              "Yes"));
        resourceGroupList.add(new ResourceGroup("Accounting/Finance Menu link",
            									"TaxAdminLink",
            									"View",
            									"View"));
    }

}