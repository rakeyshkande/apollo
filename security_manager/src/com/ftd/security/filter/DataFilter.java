package com.ftd.security.filter;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.constants.SecurityAdminConstants;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;


public class DataFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.security.filter.DataFilter");

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";
    

    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get shared data.
     * 
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        logger.debug("Entering DataFilter...");
        String sourceMenu = null;
        
        try {
            if(request != null) {
                HashMap parameters  = (HashMap)request.getAttribute(SecurityAdminConstants.APP_PARAMETERS);
                    
                if(parameters == null)
                {
                  parameters = new HashMap();
                }        
                if(!isMultipartFormData(request)) {
                    sourceMenu = request.getParameter(SecurityAdminConstants.SOURCE_MENU);
                } 
                else {
                    // store multipart parameters 
                    HashMap mParam = getMultipartParam((HttpServletRequest)request);
                    request.setAttribute(SecurityAdminConstants.MULTIPART_PARAM, mParam);
                    
                    sourceMenu = (String)mParam.get(SecurityAdminConstants.SOURCE_MENU);
                }
                
                logger.debug("source menu is:" + sourceMenu);
                parameters.put(SecurityAdminConstants.SOURCE_MENU, sourceMenu != null ? sourceMenu.trim() : "");
        
                request.setAttribute(SecurityAdminConstants.APP_PARAMETERS, parameters);
                
                chain.doFilter(request, response);
            }
        } catch (Exception e) {
            logger.error(e);
            throw new ServletException (e.getMessage());
        }
    }

    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     */
     public static boolean isMultipartFormData(ServletRequest request)
     {
        boolean isMultipartFormData = false;
        String headerContentType = ((HttpServletRequest)request).getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
    }
    
    /**
     * Returns a HashMap of form fields and file item from a multipart/form-data request.
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
    public HashMap getMultipartParam(HttpServletRequest request) 
    throws Exception {
        HashMap fieldMap = new HashMap();
        List items = null;

        try {
            // Create a new file upload handler
            DiskFileUpload upload = new DiskFileUpload();
            items = upload.parseRequest(request);
              
            // Get file items. Put in a map.
            for(int i = 0; i < items.size(); i++) {
                FileItem item = (FileItem) items.get(i);

                if (item.isFormField()) {
                      logger.debug("Found Form field Name=" + item.getFieldName() + "...value=" + item.getString());
                      fieldMap.put(item.getFieldName(), item.getString());          
                } else {
                      logger.debug("Found a file...");
                      fieldMap.put(SecurityAdminConstants.FILE_ITEM, item);
                }
            } 
            return fieldMap;
        } catch (Exception ex) {
            logger.error(ex);
            throw ex;
        }
    }
        
}
