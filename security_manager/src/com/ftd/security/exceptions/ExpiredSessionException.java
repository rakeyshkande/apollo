package com.ftd.security.exceptions;

public class ExpiredSessionException  extends Exception {
  public ExpiredSessionException() {
    super();
  }
  public ExpiredSessionException(String message){
    super(message);
  }
}