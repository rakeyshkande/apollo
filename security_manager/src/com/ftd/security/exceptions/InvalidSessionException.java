package com.ftd.security.exceptions;

public class InvalidSessionException  extends Exception {

  public InvalidSessionException() {
    super();
  }

  public InvalidSessionException(String message){
    super(message);
  }
}