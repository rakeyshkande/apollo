package com.ftd.security.exceptions;

/**
 * Represents an Exception thrown when user tries to create
 * an object that already exists in system.
 * @author Christy Hu
 */
public class DuplicateObjectException  extends InvalidRequestException {
  public DuplicateObjectException() {
    super();
  }
  public DuplicateObjectException(String message){
    super(message);
  }
}