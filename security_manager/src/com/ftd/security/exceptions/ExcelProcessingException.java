package com.ftd.security.exceptions;

/**
 * Exception raised when unable to process data loaded from Excel spreadsheet.
 * @author Christy Hu
 */
public class ExcelProcessingException  extends Exception {
  public ExcelProcessingException() {
    super();
  }
  
  public ExcelProcessingException(String message){
    super(message);
  }

  public ExcelProcessingException(String message, int lineNumber, String columnName){
    super(message + ": Line #" + lineNumber + "; Column Name: " + columnName);
  }  

  public ExcelProcessingException(String message, int lineNumber){
    super(message + ": Line #" + lineNumber);
  }   
}