package com.ftd.security.exceptions;

public class InvalidIdentityException  extends Exception {
  public InvalidIdentityException() {
    super();
  }
  public InvalidIdentityException(String message){
    super(message);
  }
}