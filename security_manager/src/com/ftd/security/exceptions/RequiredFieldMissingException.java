package com.ftd.security.exceptions;

/**
 * Represents an Exception thrown when user has not provided
 * all required fileds or data is not in correct format.
 * @author Christy Hu
 */
public class RequiredFieldMissingException  extends InvalidRequestException {
  public RequiredFieldMissingException() {
    super();
  }
  public RequiredFieldMissingException(String message){
    super(message);
  }
}