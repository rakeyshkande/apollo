package com.ftd.security.exceptions;

/**
 * Represents an Exception thrown when user tries to log in
 * for allowed attempts without providing correct userid/password.
 * @author Christy Hu
 */
public class TooManyAttemptsException  extends Exception {
  public TooManyAttemptsException() {
    super();
  }
  public TooManyAttemptsException(String message){
    super(message);
  }
}