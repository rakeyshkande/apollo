package com.ftd.security.exceptions;

public class ExpiredCredentialsException  extends Exception {
  public ExpiredCredentialsException() {
    super();
  }
  public ExpiredCredentialsException(String message){
    super(message);
  }
}
