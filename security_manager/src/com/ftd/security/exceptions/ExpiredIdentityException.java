package com.ftd.security.exceptions;

public class ExpiredIdentityException  extends Exception {
  public ExpiredIdentityException() {
    super();
  }
  public ExpiredIdentityException(String message){
    super(message);
  }
}