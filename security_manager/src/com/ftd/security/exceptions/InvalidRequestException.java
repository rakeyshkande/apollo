package com.ftd.security.exceptions;

/**
 * Represents an Exception thrown when user sends an invalid request 
 * and the application should respond without fulfilling the request.
 * @author Christy Hu
 */
public class InvalidRequestException  extends Exception {
  public InvalidRequestException() {
    super();
  }
  public InvalidRequestException(String message){
    super(message);
  }
}