package com.ftd.security;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.security.util.ServletHelper;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SecurityFilterAjax implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.security.SecurityFilterAjax");

    private static final String SECURITY_CONTEXT = "context";
    private static final String SECURITY_TOKEN = "securitytoken";
    private static final String PARAMETERS = "parameters";
    private static final String SKIP_SECURITY = "skip_security";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get security context and security token.
     * 
     * If security checking is on, verify session information by checking for:
     *   1. expired identity,
     *   2. expired session,
     *   3. invalid session.
     *   
     *   If one of the above business error conditions occurs, forward control to 
     *   the login page.  Otherwise, place the security information into a hashmap,
     *   load onto the request object and forward to the next program in the 
     *   command chain.
     *   
     * If security checking is off, pass control to the next program in the command 
     * chain.
     * 
     * If a non-business exception (unlike the business exceptions listed above) 
     * occurs, attempt to redirect to an error page.  If an error occurs during the 
     * redirect, log an error and stop.
     *   
     * @param request ServletRequest  - input request object
     * @param response ServletResponse - object used to respond to the user
     * @param chain FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String context = null;
        String securityToken = null;
        String errorMessage = null;

        String securityCheck = _filterConfig.getInitParameter("PERFORM-SECURITY-CHECK");
        boolean performSecurityCheck = (securityCheck != null  &&  securityCheck.equalsIgnoreCase("true")) ? true : false;

        try
        {
            HashMap parameters = new HashMap();

            if(isMultipartFormData(((HttpServletRequest)request )))
            {
                context = (String)request.getAttribute(SECURITY_CONTEXT);
                securityToken = (String)request.getAttribute(SECURITY_TOKEN);
                
                // This added logic fixes the incorrect assumption that
                // all multipart HTTP requests have the security tokens
                // as attributes.
                if(securityToken == null) {
                    // Note: Do NOT invoke ServletHelper.getMultipartParam at this location
                    // since it is *destructive* to the request object,
                    // making it unreadable to any other chained servlet.
                    context = request.getParameter(SECURITY_CONTEXT);
                    securityToken = request.getParameter(SECURITY_TOKEN);
                }
            }
            else
            {
                //Check the request parameter to see if security should
                //not be checked for this request.  This allows a page
                //to specify if the security token should not be checked.               
                String skipSecurity = request.getParameter(SKIP_SECURITY);
                if(skipSecurity != null && skipSecurity.equals("Y"))
                {
                    performSecurityCheck = false;
                }

                
                context = request.getParameter(SECURITY_CONTEXT);
                securityToken = request.getParameter(SECURITY_TOKEN);
            }

            if (performSecurityCheck)
            {
                try
                {
                    if (! ServletHelper.isValidToken(context, securityToken))
                    {
                        errorMessage = "Invalid Security Token.";
                        logger.error(errorMessage);
                        logger.error(securityToken);
                        returnErrorResponse((HttpServletResponse) response, errorMessage);
                    }
                    else
                    {
                        parameters.put(SECURITY_CONTEXT, (context == null ? "" : context));
                        logger.debug("Setting contextId in parameter map: " + context);

                        parameters.put(SECURITY_TOKEN, (securityToken == null ? "" : securityToken));
                        logger.debug("Setting securityToken in parameter map: " + securityToken);

                        request.setAttribute(PARAMETERS, parameters);
                        chain.doFilter(request, response);
                    }
                }
                catch(ExpiredIdentityException e)
                {
                    errorMessage = "Expired Identity";
                    logger.warn(errorMessage);
                    logger.warn(securityToken);
                    returnErrorResponse((HttpServletResponse) response, errorMessage);
                }
                catch(ExpiredSessionException e)
                {
                    errorMessage = "Expired Session";
                    logger.warn(errorMessage);
                    logger.warn(securityToken);
                    returnErrorResponse((HttpServletResponse) response, errorMessage);
                    //chain.doFilter(request, response);
                }
                catch(InvalidSessionException e)
                {
                    errorMessage = "Invalid Session";
                    logger.warn(errorMessage);
                    logger.warn(securityToken);
                    returnErrorResponse((HttpServletResponse) response, errorMessage);
                }
            }
            else
            {
                chain.doFilter(request, response);
            }
        } 
        catch (Exception ex)
        {
            logger.error(ex);
            try
            {
                returnErrorResponse((HttpServletResponse) response, ex.getMessage());
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }
    }

    /**
     * Return an error response to the calling app.
     * 
     * @param response ServletResponse
     * @param message String - error message to be returned
     * 
     */
    private void returnErrorResponse(HttpServletResponse response, String message)
    {
        try {
            response.setContentType("text/xml");

            Document doc = JAXPUtil.createDocument();
            Element result = doc.createElement("result");
            doc.appendChild(result);
            JAXPUtil.addAttribute(result, "status", "N");
            JAXPUtil.addAttribute(result, "message", message);
            
            String responseXML = JAXPUtil.toString(doc);
            logger.debug(responseXML);
            response.getWriter().write(responseXML);

        } catch (Exception e) {
            logger.error(e);
        }
    }


    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
        throws Exception
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
       }

}