package com.ftd.security.util;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.exceptions.SecurityAdminException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/** 
 * Helper class to perform String related tasks.
 * @author Christy Hu 
 */
public class StringUtil 
{
  private static final String LOGGER_CATEGORY = "com.ftd.security.util.StringUtil";
  private static Logger logger;

  // Initializes logger on load.
  static {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**
   * returns a list of ids from an input string of comma dilimited ids.
   * @param str source string
   * @returns ArrayList list of tokenized ids
   * @throws SecurityAdminException
   */
  public static ArrayList getIds(String str) throws SecurityAdminException
  {

    ArrayList ids = new ArrayList();
    try{
      if (str != null && str.length() > 0) 
      {
        StringTokenizer st = new StringTokenizer(str, ",");
        while (st.hasMoreTokens()) 
        {
          ids.add(st.nextToken());
        }
      }
    } catch (Exception ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");
    }
    return ids;
  }

  /**
   * returns an sql date from a string of the format mm/dd/yyyy.
   * @param str source string
   * @returns ArrayList list of tokenized ids
   * @throws SecurityAdminException
   */  
  public static java.sql.Date toSqlDate(String str)
    throws SecurityAdminException
  {
    java.sql.Date sqlDate = null;
    StringTokenizer st = new StringTokenizer(str, "/");
    String m = "0";
    String d = "1";
    String y = "1900";
    
    try 
    {
      if(st.hasMoreTokens()) {
          m = st.nextToken();
      }
      if(st.hasMoreTokens()) {
          d = st.nextToken();
      }
      if(st.hasMoreTokens()) {
          y = st.nextToken();  
      }
    } catch (Exception ex) 
    {
      logger.error(ex);
      // CANNOT PARSE DATE STRING
      throw new SecurityAdminException("2");
    }
    try {
      Calendar cal = Calendar.getInstance();
      cal.set((new Integer(y)).intValue(), (new Integer(m)).intValue()-1, (new Integer(d)).intValue());
      java.util.Date utilDate = cal.getTime();
      sqlDate = new java.sql.Date(utilDate.getTime());  
    } catch (Exception ex) {
      logger.error(ex);
      // CANNOT CREATE DATE OBJECT
      throw new SecurityAdminException("3");  
    }
    return sqlDate;
  }  

  /**
   * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
   *              as MM/dd/yyyy (which is the format we show in the screens).
   *
   * @param java.util.Date date to convert
   * @return String
   * @throws ParseException
   */
  public static String formatUtilDateToString(java.util.Date utilDate)
            throws ParseException
  {
    String strDate = "";
    String inDateFormat = "";
    java.util.Date newDate = null;

    if (utilDate != null) {
          SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

            newDate = dfOut.parse(dfOut.format(utilDate));
            strDate = dfOut.format(newDate);

      } 
      else {
          strDate = "";
      }

    return strDate;
  }  


  /**
   * Description: Takes a String in the format of 'yyyy-MM-dd HH:mm:ss.S'
   * and convert it to a java.util.Date.
   *
   * @param java.util.Date date to convert
   * @return String
   * @throws ParseException
   */
  public static java.util.Date formatStringToUtilDate(String pattern, String dateString)
            throws ParseException
  {
        //String pattern = "yyyy-MM-dd HH:mm:ss.S";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        java.util.Date d = df.parse(dateString);
        
        return d;
  }
  /**
   * Returns first initials plus last name.
   */
  public static String getBaseIdentity(String firstName, String lastName) {
      StringBuffer sb = new StringBuffer();
      sb.append(firstName.charAt(0));
      sb.append(lastName);
      return (sb.toString()).toLowerCase();
  }


  /**
   * If baseId doesn't contain '_', append '_2'; if it does,
   * increment the number after '_' by one.
   */
  public static String getNextIdentity(String baseId) {
      StringBuffer sb = new StringBuffer(baseId);
      String underscore = "_";
      int lastUnderscore = baseId.lastIndexOf(underscore);
      int curNum = 2;
      
      if (lastUnderscore != -1) {
          curNum = (Integer.valueOf(baseId.substring(lastUnderscore + 1))).intValue();
          curNum++;
          sb.delete(lastUnderscore + 1, baseId.length());
          sb.append(curNum);
      } else {
          sb.append(underscore).append(curNum);
      }
      return sb.toString();
  }  

  /**
   *  Tests if a string is all numeric. Returns true if it is,
   * or the input string is null.
   */
  public static boolean isNumricString(String s){
      boolean isNumeric = true;
      if(s != null) {
          for(int i=0; i < s.length(); i++) {
              char c = s.charAt(i);
              if(!Character.isDigit(c)) {
                  isNumeric = false;
                  break;
              }  
          }
      }
      return isNumeric;
  }

  /**
   * Tests if the string contains the character. Returns true if it does,
   * or the input string is null.
   */
  public static boolean contains(String s, char c) {
      int index = 0 ;
      if(s != null && s.length() > 0) {
          index = s.indexOf(c);          
      }
      return index != -1;
  }

  /**
   * Tests if the string contains either alphabetics or numerics.
   * Returns true if so; false otherwise.
   */
  public static boolean isAlphaNumeric(String s) {
      boolean isAlphaNumeric = true;
     
      for(int i=0; i < s.length(); i++) {
          char c = s.charAt(i);
          if(!Character.isLetter(c) && !Character.isDigit(c)) {
              isAlphaNumeric = false;
              break;
          }  
      }
      return isAlphaNumeric;
  }      

  /**
   * Checks if the String is in valid date format (m)m/(d)d/yyyy
   */
  public static boolean isValidDateString(String dateString) {
      StringTokenizer st = new StringTokenizer(dateString, "/");

      try {
          String m = st.nextToken();
          String d = st.nextToken();
          String y = st.nextToken();
          int month = (Integer.valueOf(m)).intValue();
          int date = (Integer.valueOf(d)).intValue();
          int year = (Integer.valueOf(y)).intValue();

          if (dateString.compareTo(m + "/" + d + "/" + y) > 0) {
              return false;
          }
          if (!isValidMonth(month)) {
              return false;
          } 
          if (!isValidDate(month, date, year)) {
              return false;
          } 
          if (!isValidYear(year)) {
              return false;
          }
          
      } catch (NumberFormatException e) {
          return false;
      } catch (NullPointerException e) {
          return false;
      } catch (NoSuchElementException e) {
          return false;
      }

      return true;
  }

  /**
   * Checks if the month is valid. Month is vaild if it's between 1 and 12.
   */
  private static boolean isValidMonth(int month) {
      return month > 0 && month <= 12;
  }

  /**
   * Checks if the year is valid. year is vaild if it's between 1000 and 9999.
   */
  private static boolean isValidYear(int year) {
      return year >= 1000 && year <= 9999;
  }  

  /**
   * Checks if the date is valid. 
   */
  private static boolean isValidDate(int month, int date, int year) {
      return date > 0 && date <= daysInMonth(month, year);
  }

  /**
   * Returns number of days in February.
   */
  private static int daysInFebruary (int year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
  }
  
  private static int daysInMonth(int month, int year) {
      int days = 31;
      if (month == 2) {
          days = daysInFebruary(year);
      } else if (month == 4 || month == 6 || month == 9 || month == 11) {
          days = 30;
      }
	
      return days;
  }  
  
}