package com.ftd.security.util;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.LineItemVO;
import com.ftd.security.cache.vo.UserLineItemVO;
import com.ftd.security.exceptions.ExcelProcessingException;

import java.io.IOException;
import java.io.InputStream;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.*;
///import oracle.xml.parser.v2.XSLException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import org.xml.sax.SAXException;


/** This class contains utility methods used to work with an Excel spreadsheet.
 *  @author Christy Hu, courtesy of Ed Mueller for the most part
 */
public class SpreadsheetUtil
{
    private static Logger logger = new Logger("com.ftd.security.util.SpreadsheetUtil");
    private static final String BAD_FILE = "Invalid user file. Please submit valid user file";
    private static final String USER = "user";
    private static final int EXCEL_START_ROW = 1; 
    private static SpreadsheetUtil spreadsheetUtil;  
   
    /** Private constructor.  
     */
    private SpreadsheetUtil() {
    }

    public static SpreadsheetUtil getInstance() 
    {
       if(spreadsheetUtil == null ){
          spreadsheetUtil = new SpreadsheetUtil();
       }
       return spreadsheetUtil;
    }
    /**
     *  Constructs and returns a list of LineItem objects from InputStream.
     */
    public static List getLineItemObjects(InputStream spreadsheetStream, String subject) 
    throws Exception {
        logger.debug("Entering getLineItemObjects...");
        List lineItems = new ArrayList();
        HSSFSheet sheet = null;
        //set row counter
        int rowNumber = EXCEL_START_ROW;
        int cols = 0;
 
        try{
            // Get reference to worksheet.
            sheet = getWorksheet(spreadsheetStream, 0);
            
            if(USER.equalsIgnoreCase(subject)) { 
                UserLineItemVO.init();
                cols = UserLineItemVO.getColumnSize();
            }
            
            //get first row in file
            HSSFRow row = sheet.getRow(rowNumber);

            //while not at end of file
            while (!eof(row, cols)){
                logger.debug("Processing row " + rowNumber + "...");
                //get vo representation of line item--parse Exel doc
                lineItems.add(getLine(row, rowNumber, subject));
                    
                //increment line counter
                rowNumber++;

                //get next row
                row = sheet.getRow(rowNumber);
            }//end while
            spreadsheetStream.close();
        }
        catch(Exception e)
        {   
            logger.error(e);  
            throw e;
        }
        return lineItems;
    }
    
    public static List getLineItemObjects(InputStream spreadsheetStream, Class lineItemVOClass) 
    throws Exception {
        logger.debug("Entering getLineItemObjects...");
        List lineItems = new ArrayList();
        HSSFSheet sheet = null;
        //set row counter
        int rowNumber = EXCEL_START_ROW;
        int cols = 0;
    
        try{
            // Get reference to worksheet.
            sheet = getWorksheet(spreadsheetStream, 0);
            
            cols = ((LineItemVO) lineItemVOClass.getConstructor(new Class[] {}).newInstance(new Object[] {})).getColumnSize();
//            
//            if(USER.equalsIgnoreCase(subject)) { 
//                UserLineItemVO.init();
//                cols = UserLineItemVO.getColumnSize();
//            }
            
            //get first row in file
            HSSFRow row = sheet.getRow(rowNumber);

            //while not at end of file
            while (!eof(row, cols)){
                logger.debug("Processing row " + rowNumber + "...");
                //get vo representation of line item--parse Exel doc
                lineItems.add(getLine(row, rowNumber, lineItemVOClass));
                    
                //increment line counter
                rowNumber++;

                //get next row
                row = sheet.getRow(rowNumber);
            }//end while
            spreadsheetStream.close();
        }
        catch(Exception e)
        {   
            logger.error(e);  
            throw e;
        }
        return lineItems;
    }


  /**
   * get HSSFSheet from InputStream
   */
    private static HSSFSheet getWorksheet(InputStream spreadsheetStream, int sheetNum)
      throws ExcelProcessingException, Exception
    {
        try {
          POIFSFileSystem fs = new POIFSFileSystem(spreadsheetStream);
          HSSFWorkbook wb = new HSSFWorkbook(fs);
          HSSFSheet sheet = wb.getSheetAt(sheetNum);
          return sheet;
        } catch (IOException ex) {
          throw new ExcelProcessingException(BAD_FILE);
        }
    }


   
  /**
   * This method gets the value from a cell.  If the cell is null then
   * an empty string is returned. 
   * @param HSSFRow row from which the value should be obtained
   * @param short column from which the value should be obtained
   * @return String the value 
   * @throw ExcelProcessingException
   * @throw IOException could not read from property file
   * @throw TransformerException count not read from property file
   * @throw SAXException count not read property file
   * @throw ParserConfigurationException
   * @throw XSLException error reading property file
   */
    private static String getCellValue(HSSFRow row, short column) 
        throws ExcelProcessingException,IOException,TransformerException,
              SAXException,ParserConfigurationException, Exception{

        HSSFCell cell = row.getCell(column);

        //if cell is null return empty string
        if(cell == null)
        {
          return "";
        }

        //The format value returned for a date is 14
        short EXCEL_DATE_FORMAT = 14;
        String value = "";
        
        switch(cell.getCellType())
        {
          //type string
          case (HSSFCell.CELL_TYPE_STRING):
                value = cell.getStringCellValue();
                break;

          //type date
          case (HSSFCell.CELL_TYPE_NUMERIC):
                if(cell.getCellStyle().getDataFormat() == EXCEL_DATE_FORMAT){
                    try{
                        value = StringUtil.formatUtilDateToString(cell.getDateCellValue());
                        logger.debug("The date value=" + value);
                    }
                    catch(ParseException pe)
                    {
                        String msg = " Row:" + row.getRowNum() + " Column:" + column + 1;
                        logger.error(msg);
                        logger.error(pe);
                        throw new ExcelProcessingException(msg);                        
                    }
                    catch(Exception e)
                    {
                        String msg = " Row:" + row.getRowNum() + " Column:" + column + 1;
                        logger.error(msg);
                        logger.error(e);
                        throw new Exception(msg);                        
                    }
                }
                else
                {
                    //Use int value if it is the same as the double value
                    //This fixes the problem of loading numeric product ids with a ".0" appended (i.e. 8212.0)
                    int cellValueInt = (int) cell.getNumericCellValue();
                    double cellValueDouble = cell.getNumericCellValue();
                    if (cellValueInt == cellValueDouble) {
                        value = String.valueOf(cellValueInt);
                    } else {
                        value = String.valueOf(cellValueDouble);
                    }
                }
                break;
          //blank cell                
          case (HSSFCell.CELL_TYPE_BLANK):
                value = "";
                break;
          default:
                String msg = " Row:" + row.getRowNum() + " Column:" + column + 1;
                logger.error(msg);
                logger.error("Cell type=" + cell.getCellType());
                throw new ExcelProcessingException(msg);
        }


        //System.out.println("|" + value + "|");
    
        return value;
   }

    /** This method is used generated the VO for an order line item.
      * @param HSSFRow Excel row 
      * @param int line number
      * @return LineItemVO line item vo
      * @throw ExcelProcessingException
      * @throw IOException Could not obtain an input stream
      * @throw SAXException Could not parse property file
      * @throw TransformerException Could not parse property file
      * @throw ParserConfigurationException
      * @throw XSLException
      */
    private static LineItemVO getLine(HSSFRow row, int lineNumber, String subject) 
        throws ExcelProcessingException, IOException, SAXException, 
            TransformerException,ParserConfigurationException, Exception{
        //HashMap fieldColMap = null;
        LineItemVO line = null;
        int max_excel_columns = 0;

        // Create specific LineItemVO based on subject.
        if(USER.equalsIgnoreCase(subject)) {            
            // Process user spreadsheet.            
            max_excel_columns = UserLineItemVO.getColumnSize();            
            line = new UserLineItemVO();
        }        
               
        line.setLineNumber(lineNumber);

        for(int i = 0; i < max_excel_columns; i++) {
          line.setFieldValue(i, getCellValue(row, (short)i).trim());
          logger.debug("Setting field " + i + " to value=" + getCellValue(row, (short)i));
        }

        return line;
    }
    
    private static LineItemVO getLine(HSSFRow row, int lineNumber, Class lineItemVOClass) 
        throws ExcelProcessingException, IOException, SAXException, 
            TransformerException,ParserConfigurationException, Exception{
            
        // Create specific LineItemVO.
         LineItemVO line = (LineItemVO) lineItemVOClass.getConstructor(new Class[] {}).newInstance(new Object[] {});
        
        int max_excel_columns = line.getColumnSize();         
               
        line.setLineNumber(lineNumber);

        for(int i = 0; i < max_excel_columns; i++) {
          line.setFieldValue(i, getCellValue(row, (short)i).trim());
          logger.debug("Setting field " + i + " to value=" + getCellValue(row, (short)i));
        }

        return line;
    }


    /** This method determines of the Excel spreedsheet EOF has been reached.
      * @param HSSFRow Excel Row
      * @return boolean True=EOF
      * @throws ExcelProcessingException Could not parse file
      * @throws IOException Could not obtain an input stream
      * @throws SAXException Could not parse property file
      * @throws TransformerException Could not parse property file
      * @throws ParserConfigurationException
      * @throws XSLException*/
    private static boolean eof(HSSFRow row, int cols) 
        throws ExcelProcessingException, IOException, SAXException, 
            TransformerException,ParserConfigurationException, Exception{

        //check if there is any date in this row
        StringBuffer sb = new StringBuffer("");

        //if row is not null (null row means eof)
        
        if(row != null){
            for(short i=0;i< cols;i++){
                HSSFCell cell = row.getCell(i);
                if (cell != null) {
                    sb.append(getCellValue(row,i));
                }
            }//end for
        }
        
        return sb.toString().equals("") ? true : false;
    }

}