package com.ftd.security.util;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.exceptions.SecurityAdminException;

/** 
 * Helper class to set up database connection.
 * @author Christy Hu 
 */
public class DataRequestHelper {
  private static DataSourceUtil dsUtil; 
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.util.DataRequestHelper";
  private static final String DATASOURCE = "ORDER SCRUB";
 
  static {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**
  * Get connection to the data source.
  * @return Connection
  * @throws SecurityAdminException
  */
  public static Connection getDBConnection() throws SecurityAdminException
  {
    Connection con = null;

    try{       
      DataSourceUtil dsUtil = DataSourceUtil.getInstance();
      con = dsUtil.getConnection(DATASOURCE);
    }
    catch(Exception ex)
    {
      logger.error(ex);
      // CANNOT OBTAIN DATASOURCE
      throw new SecurityAdminException("4");
    }      
    return con;      
  } 

  /**
   * Excecutes the statement and returns the result.
   */
  public static Object executeStatement(Connection con, HashMap inputParams, String statement)
  throws Exception {
      DataRequest dataRequest = new DataRequest();
      DataAccessUtil util = DataAccessUtil.getInstance();
      dataRequest.setConnection(con);               
      dataRequest.setStatementID(statement);
      dataRequest.setInputParams(inputParams);
      Object obj = util.execute(dataRequest);
      return obj;
  }

    /**
     * Returns a document with the specified statement. No input parameter is required.
     */
     public static Object executeStatement(String statementId, HashMap inputParams) throws Exception {
          Connection con = null; 
          Object obj = null;
        
          try {
              con = getDBConnection();                          
              obj = executeStatement(con,inputParams,statementId);
              return obj;
          } catch (Exception e) {
              throw e;
          } finally {
              if(con != null) {
                  con.close();
              }
          }
     }
     
     /**
      * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.  
      * It populates a global parameter VO based on the returned record set.
      * 
      * @param String - context
      * @param String - name
      * @return GlobalParameterVO 
      */
	public static String getGlobalParameter(String context, String name)
			throws Exception {

		String value = null;
		String outputs = null;
		Connection conn = null;

		try {
			/* setup store procedure input parameters */
			Map inputParams = new HashMap();

			conn = getDBConnection();

			DataAccessUtil dau = DataAccessUtil.getInstance();
			DataRequest dataRequest = new DataRequest();
			dataRequest.setConnection(conn);

			dataRequest.setStatementID("GET_GLOBAL_PARAM");

			// input parameters
			inputParams.put("IN_CONTEXT", context);
			inputParams.put("IN_PARAM", name);
			dataRequest.setInputParams(inputParams);

			value = (String) dau.execute(dataRequest);
			return value;
		} catch (Exception e) {
			logger.error(e);
			throw e;
        } finally {
            if(conn != null) {
                conn.close();
            }
		}
	}
}