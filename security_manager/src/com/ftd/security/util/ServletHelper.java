package com.ftd.security.util;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.constants.MenuConstants;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.security.exceptions.SecurityAdminException;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;

/**
 * Collection of methods used by the Servlets.
 */
public class ServletHelper  {
    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";
    private static Logger logger = new Logger("com.ftd.security.util.ServletHelper");
    private static String applicationContext;
    private static SecurityManager secMgr = null;
   
    static {
        try {
            applicationContext = ConfigurationUtil.getInstance().getProperty
                  (SecurityAdminConstants.CONFIG_FILE,SecurityAdminConstants.COMMON_PARM_APP_CONTEXT);
            secMgr = SecurityManager.getInstance();
        } catch (IOException e) {
            logger.warn(SecurityAdminConstants.CONFIG_FILE + " was not found in this project: " + e.getMessage());
        } catch (Exception e) {
            logger.error(e);
        }
    }
    /**
     * Indicates whether or not the security token is a valid.
     * It checks the validity of the security token.
     * 
     * @param securityToken the security token
     * @exception ExpiredIdentityException
     * @exception ExpiredSessionException
     * @exception InvalidSessionException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     * @exception SQLException
     * @exception Exception
     * @return whether or not the security token is valid
     */
    public static boolean isValidToken(String context, String securityToken) 
      throws ExpiredIdentityException, 
             ExpiredSessionException, 
             InvalidSessionException, 
             SAXException, 
             ParserConfigurationException,
             IOException,
             SQLException,
             Exception{

      if (securityToken == null || (securityToken.equals("")))  {
          logger.debug("No Security Token found");
          return false;
      } 
      
      logger.debug("authenticating security token");
      SecurityManager securityManager = SecurityManager.getInstance();
      return securityManager.authenticateSecurityToken(context, getUnitID(), securityToken);
  }

  /**
   * Categorizes exceptions and dispatches to send error page.
   * @param request HttpRequest
   * @param response HttpResponse
   * @param ex exception
   * @param errorStylesheet error stylesheet
   * @throws ServletException
   * @throws IOException 
   */
  public static void handleError(HttpServletRequest request, 
                                 HttpServletResponse response, 
                                 Exception ex,
                                 File errorStylesheet,
                                 String errorStylesheetName) 
  throws ServletException, IOException {
      logger.debug("Handling error...");
      request.setAttribute(SecurityAdminConstants.COMMON_PARM_STYLESHEET, errorStylesheet);
      try {
          if (ex instanceof SecurityAdminException) {
              sendErrorPage(request, response, ex, errorStylesheetName);
          } else if (ex instanceof SQLException) {
              // DATABASE ERROR
              sendErrorPage(request, response, new SecurityAdminException("6"), errorStylesheetName);
          }
          else {
              // INTERNAL ERROR
              sendErrorPage(request, response, new SecurityAdminException("1"), errorStylesheetName);
          }
      } catch(Exception e) {
        logger.error(e);
        throw new ServletException(e.getMessage());
    }
  }
 /**
  * Displays an apology page.
  * 
  * @param request http request
  * @param response http response
  * @throws ServletException
  * @throws IOException
  */ 
  private static void sendErrorPage(HttpServletRequest request, 
                                    HttpServletResponse response, 
                                    Exception ex,
                                    String stylesheetName) 
                throws ServletException, IOException
  {
    try{
        TraxUtil traxUtil = TraxUtil.getInstance();
        File stylesheet = (File) request.getAttribute(SecurityAdminConstants.COMMON_PARM_STYLESHEET);
      
        Document doc = XMLHelper.getErrorDocument(ex);
        traxUtil.transform(request, response, doc, stylesheet, stylesheetName, getParameterMap(request));
    } catch(Exception e) {
        logger.error(e);
        throw new ServletException(e.getMessage());
    }
  }  

 /**
  * Returns a HashMap with context, securitytoken, and application context from request.
  * @param request http request
  * @return HashMap with context, securitytoken, application context, and credential (if found)
  * @throws Exception
  */ 
  public static HashMap getParameterMap(HttpServletRequest request) 
    throws Exception {

      HashMap parameters = (HashMap)request.getAttribute(SecurityAdminConstants.APP_PARAMETERS);
      if (parameters == null) {
            parameters = new HashMap();
      }
      logger.debug("source menu in parameters");
      logger.debug(parameters.get("sourceMenu")==null?"null":(String)parameters.get("sourceMenu"));
      String contextId = null;
      String securityToken = null;
      String credential = null;
      
      if(isMultipartFormData(request)) {
          contextId = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT);
          securityToken = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      } else {
          contextId = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);
          securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      }
      credential = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);       
      
      parameters.put(SecurityAdminConstants.COMMON_PARM_CONTEXT, contextId == null? "" : contextId);
      parameters.put(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken == null? "" : securityToken);
      parameters.put(SecurityAdminConstants.COMMON_PARM_APP_CONTEXT, applicationContext == null? "" : applicationContext);
      parameters.put(SecurityAdminConstants.COMMON_PARM_CREDENTIALS, credential == null? "" : credential);
      
      logger.debug("Setting contextId in parameter map: " + contextId);
      logger.debug("Setting securityToken in parameter map: " + securityToken);
      logger.debug("Setting application context in parameter map: " + applicationContext);
      return parameters;
  }

    /**
     * Retrieve common data from request and add them to the hash map passed in.
     * Add thm
     * @param request http request
     * @return HashMap with context, securitytoken, application context, and credential (if found)
     * @throws Exception
     */ 
     public static HashMap getParameterMap(HttpServletRequest request, HashMap parameters) 
       throws Exception {

         // Map that contains common data from page to page.
         HashMap param = getParameterMap(request);
         
         // add elements from input map
         if(parameters != null) {
             Iterator i = parameters.keySet().iterator();
             while(i.hasNext()) {
                String keyName = (String)i.next();
                String keyValue = (String)parameters.get(keyName);
                
                if (keyValue == null) {
                    keyValue = "";
                }
                
                param.put(keyName, keyValue);
             }
         }
         return param;
     }

  /**
   * Returns a HashMap of form fields and file item from a multipart/form-data request.
   * @param request HttpServlet request to be analyzed
   * @throws Exception
   */
   public static HashMap getMultipartParam(HttpServletRequest request) 
   throws Exception {
       HashMap fieldMap = new HashMap();
       List items = null;

       try {
           // Create a new file upload handler
           DiskFileUpload upload = new DiskFileUpload();
           items = upload.parseRequest(request);
             
           // Get file items. Put in a map.
           for(int i = 0; i < items.size(); i++) {
               FileItem item = (FileItem) items.get(i);

               if (item.isFormField()) {
                     logger.debug("Found Form field Name=" + item.getFieldName() + "...value=" + item.getString());
                     fieldMap.put(item.getFieldName(), item.getString());          
               } else {
                     logger.debug("Found a file...");
                     fieldMap.put(SecurityAdminConstants.FILE_ITEM, item);
               }
           } 
           return fieldMap;
       } catch (Exception ex) {
           logger.error(ex);
           throw ex;
       }
   }

    /**
     * Returns a HashMap of parameters saved in the filter.
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
    public static HashMap getMultipartFilterParam(HttpServletRequest request) 
    throws Exception {
          
          HashMap mParam = null;
          try {
              mParam = (HashMap)request.getAttribute(SecurityAdminConstants.MULTIPART_PARAM);
              if (mParam == null) {
                  mParam = new HashMap();
              }
              return mParam;
        } catch (Exception ex) {
            logger.error(ex);
            throw ex;
        }
    }
    

  /**
   * Returns true if request content type is multipart/form-data.
   * @param request HttpServlet request to be analyzed
   * @throws Exception
   */
   public static boolean isMultipartFormData(HttpServletRequest request)
   throws Exception {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null && headerContentType.startsWith(MULTPART_FORM_DATA)) {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
   }

   /**
    * Looks up and returns error message from error file based on error code.
    */
   public static String getErrorMsg(String errorCode) throws Exception {
        String msg = null;
        try {
            msg = ConfigurationUtil.getInstance().getProperty
            (SecurityAdminConstants.ERROR_FILE, errorCode);
        } catch (Exception e) {
            logger.error(e);
            throw e;
        }
        return msg;  
   }

  /**
   * Looks up the unit ID required for authentication.
   */
   public static String getUnitID() throws Exception {
        return ConfigurationUtil.getInstance().getProperty
            (SecurityAdminConstants.CONFIG_FILE,SecurityAdminConstants.COMMON_PARM_UNIT_ID);               
   }

  /**
   * Looks up the context required for authentication.
   */
   public static String getContext() throws Exception {
        return ConfigurationUtil.getInstance().getProperty
            (SecurityAdminConstants.CONFIG_FILE,SecurityAdminConstants.COMMON_PARM_CONTEXT);               
   }
   
  /**
   * Looks up the sitename from configuration file.
   */
   public static String getSiteName() throws Exception {
        return ConfigurationUtil.getInstance().getFrpGlobalParm(
                    SecurityAdminConstants.GLOBAL_PARM_CONTEXT,MenuConstants.COMMON_PARM_SITENAME);               
   }
  /**
   * Looks up the site SSL port from configuration file.
   */
   public static String getSiteSslPort() throws Exception {
        return ConfigurationUtil.getInstance().getFrpGlobalParm(
                    SecurityAdminConstants.GLOBAL_PARM_CONTEXT,MenuConstants.COMMON_PARM_SITE_SSL_PORT);               
   }
  /**
   * Returns parameter or attribute from the request.
   */
   public static String getValueFromRequest(HttpServletRequest request, String name) {
        String value = null;
        value = request.getParameter(name);
        if (value == null || value.length()  == 0) {
            value = (String)request.getAttribute(name);
        }
        return value;
   }

   /**
    * Get parameters and set them on the request as attributes.
    */
    public static HttpServletRequest formatMultipartRequest(HttpServletRequest request) 
    throws Exception {
        String context = null;
        String securityToken = null;
        String identity = null;
        String adminAction = null;
        String isExit = null;
        String sourceMenu = null;
        
              HashMap parameters = ServletHelper.getMultipartFilterParam(request);
              securityToken = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
              context = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_CONTEXT);
              adminAction = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION);
              isExit = (String)parameters.get(SecurityAdminConstants.COMMON_PARM_IS_EXIT);
              sourceMenu = (String)parameters.get(SecurityAdminConstants.SOURCE_MENU);
              request.setAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN, securityToken);
              request.setAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT, context);
              request.setAttribute(SecurityAdminConstants.COMMON_PARM_ADMIN_ACTION, adminAction);
              request.setAttribute(SecurityAdminConstants.COMMON_PARM_IS_EXIT, isExit);
              request.setAttribute(SecurityAdminConstants.SOURCE_MENU, sourceMenu);   
          return request;
    }

    /**
     * Returns the identity logged in.
     */
    public static String getMyId(Object securityToken) throws Exception {
    
        UserInfo myInfo = secMgr.getUserInfo(securityToken);
        if (myInfo != null) {
            return myInfo.getUserID();
        }
        return null;
    }
    
    /**
   * Enables switching between SSL and non-SSL URL ports.
   * @return 
   * @param newPort
   * @param serverName
   */
  public static String switchServerPort(String serverName, String newPort) 
    {
      int pos = serverName.indexOf(":");
      if (pos > -1) 
      {
        if (newPort != null && newPort.length() > 0) {
        // Substitute the new port.
        String newServerName = serverName.replaceFirst(serverName.substring(pos + 1, serverName.length()),
        newPort);
        
        return newServerName;
        } else 
        {
          throw new IllegalArgumentException("switchServerPort: newPort input can not be null.");
        }
      } else 
      {
        return serverName;
      }
    }
    
    public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response)
    {        
        redirectToLogin(request, response, request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN));
    }
    
    public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response, String securityToken)
    {        
        redirectToLogin(request, response, securityToken, null);
    }
    
    public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response, String securityToken, String query)
    {   
        String protocol = request.isSecure() ? "https://" : "http://";
        StringBuffer url = new StringBuffer(protocol);
        try
        { 
          url.append(request.getServerName());
          if (request.getServerPort() != 0) {
            url.append(":");
            url.append(request.getServerPort());
          }
          
          url.append("/secadmin/security/html/SingleSignOn.html");
  
          if (securityToken != null && securityToken.length() > 0) {
            url.append("?");
            url.append(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
            url.append("=");
            url.append(securityToken);
            
            if (query != null && query.length() > 0) {
              url.append("&");
              url.append(query);
            }
          } else if (query != null && query.length() > 0) 
          {
              url.append("?");
              url.append(query);
          }
          response.sendRedirect(url.toString());
        }
        catch (Exception e)
        {
          logger.error("redirectToLogin: Failed to redirect to URL: " + url.toString(), e);
        }
    }

    public static Object getCommonData(HttpServletRequest request, String paramName) 
      throws Exception {
        Object data = null;
        HashMap parameters = (HashMap)request.getAttribute(SecurityAdminConstants.APP_PARAMETERS);
        if(parameters != null) {
            data = parameters.get(paramName);
        }
        
        return data;
    }
}