package com.ftd.security.util;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.SecurityAdminException;

import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/** 
 * Helper class to perform XML related tasks.
 * @author Christy Hu 
 */
public class XMLHelper
{
  private static Logger logger = new Logger("com.ftd.security.util.XMLHelper");
  private static final String ERRORS = "ERRORS";
  private static final String ERROR = "ERROR";
  private static final String CODE = "code";
  private static final String INFO = "info";
  private static final String OTHER = "other";
  
  /**
   * Returns a new Document with root element tagged as the input string.
   * @param tag name of root element to be created.
   * @returns Document the created document
   * @throws SecurityAdminException
   */
  public static Document getDocumentWithRoot(String tag) 
    throws SecurityAdminException
  {
    Document doc = null;
    try {
      doc = DOMUtil.getDefaultDocument();
      Element rootElem = (Element)doc.createElement(tag);
      doc.appendChild(rootElem);
    } catch (ParserConfigurationException ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");
    } catch (Exception ex) {
      logger.error(ex);
      throw new SecurityAdminException("1");
    }
    return doc;
  }

  /**
   * Returns a new Document with root element tagged as the input element name 
   * and attribute.
   * @param tag name of root element to be created.
   * @returns Document the created document
   * @throws SecurityAdminException
   */
  public static Document getDocumentWithAttribute(String tag, String attrName, String attrValue) 
    throws SecurityAdminException
  {
    Document doc = null;
    try {
      doc = DOMUtil.getDefaultDocument();
      Element rootElem = (Element)doc.createElement(tag);
      if (attrValue != null) {
          rootElem.setAttribute(attrName, attrValue);
      }
      doc.appendChild(rootElem);
    } catch (ParserConfigurationException ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");
    } catch (Exception ex) {
      logger.error(ex);
      throw new SecurityAdminException("1");
    }
    return doc;
  }
  /**
   * Creates and returns a document with the structure ERRORS/ERROR, and 
   * an attribute of ERROR element with attribute name 'code', value as
   * the input parameter.
   * @param code error code (The reference to the codes can be found in error.xsl.)
   * @returns Document the created document
   * @throws SecurityAdminException
   */
  public static Document getErrorDocument(Exception ex) 
    throws SecurityAdminException
  {
    logger.debug("Generating error page...");
    Document doc = null;
    String code = null;
    String info = null;
    try {
      doc = getDocumentWithRoot(ERRORS);
      Element errorElem = (Element)doc.createElement(ERROR);
      if (ex instanceof SecurityAdminException) 
      {
          info = ((SecurityAdminException)ex).getInfo();
          errorElem.setAttribute(CODE, ex.getMessage()); // sets the code to be looked up in error.xsl
          errorElem.setAttribute(INFO, info);            // sets the detail message generated by proc.
      } else {
          errorElem.setAttribute(OTHER, ex.getMessage()); // generic exception.
      }
      doc.getDocumentElement().appendChild(errorElem);
    } catch (Exception e) {
      logger.error(e);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");
    }
    return doc;
  }
  
  /**
   * Returns the first element in doc that has a child element with the requested value
   * @param Document document to perform the search
   * @param childName attribute name to look for a match
   * @param childValue value to look for a match
   * @return the element
   */
  public static Element findElementByChild
    (Node doc, String childName, String childValue) 
    throws SecurityAdminException
  {
  /*
    logger.debug("Entering findElementByChild...");
    logger.debug("Looking for childName " + childName + ",childValue " + childValue);

    try {
      // Get list of elements with child element name.
      NodeList list = null;
      if (doc instanceof Document) {
          list = ((Document)doc).getElementsByTagName(childName);
      } else if (doc instanceof Element) {
          list = ((Element)doc).getElementsByTagName(childName);
      }

      if (list != null) {
          String value = null;
          for (int i = 0; i < list.getLength(); i++) {
              Element e = (Element)list.item(i);
              Text t = (Text)e.getFirstChild();
              value = t.getNodeValue();
              if(childValue.equals(value)) {
                  logger.debug("Element found.");
                  return (Element)e.getParentNode();
              }
          }
      }
    } catch (Exception ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");     
    }
    return null;
    */
    return findElementByChild(doc, childName, childValue, null, null);
  }   

  
  /**
   * Returns the first element in doc that has two children with requested value.
   * @param Document document to perform the search
   * @param childName element name to look for a match
   * @param childValue value to look for a match
   * @param refChildName another child element name that needs to be matched
   * @param refChildValue the value of the other child element whose value also needs to be matched.
   * @return the element
   */
  public static Element findElementByChild
    (Node doc, String childName, String childValue, String refChildName, String refChildValue) 
    throws SecurityAdminException
  {
    logger.debug("Entering findElementByChild...");
    logger.debug("Looking for childName " + childName + ",childValue " + childValue);
    Element returnElem = null;
    NodeList list = null;
    
    try {
      // Get list of elements with child element name.      
      if (doc instanceof Document) {
          list = ((Document)doc).getElementsByTagName(childName);
      } else if (doc instanceof Element) {
          list = ((Element)doc).getElementsByTagName(childName);
      }

      if (list != null) {
          String value = null;
          for (int i = 0; i < list.getLength(); i++) {            
              Element e = (Element)list.item(i);
              Text t = (Text)e.getFirstChild();
              value = t.getNodeValue();
              if(childValue.equals(value)) {
                  
                  Element curElem = (Element)e.getParentNode();
                  // Verify the element also has another child element with refChildValue.
                  if (refChildName != null &&  refChildValue != null) {
                      NodeList refList = curElem.getElementsByTagName(refChildName);
                      if (refList != null) {
                          Element refElem = (Element)refList.item(0);
                          Text refText = (Text)refElem.getFirstChild();
                          String refValue = refText.getNodeValue();   
                          if (refChildValue.equals(refValue)) {
                              logger.debug("Element found.");
                              returnElem = curElem;
                              break;
                          }
                      } 
                  } else {
                      logger.debug("Element found.");
                      returnElem = curElem;
                      break;
                  }
                  
              }
              
          }
      }

    } catch (Exception ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");     
    }
    return returnElem;
  }   
  
  /**
   * Merges two documents. For each element in bdoc, set the corresponding 
   * element in adoc with the child value in bdoc. Returns adoc.
   * @param adoc document
   * @param bdoc document
   * @param elemName name of elements
   * @param attrNameRef name of attribute to find corresponding element
   * @param attrName name of attributes to set on adoc
   * @returns adoc
   * @throws SecurityAdminException
   */
  public static Document mergeDocuments
    (Document adoc, Document bdoc, String elemName, String childNameRef, String childName) 
    throws SecurityAdminException
  {
    // Look for an element named childNameRef that has the same value in adoc and bdoc.
    // Set the text value of 'childName' on elemName as an attribute.
    logger.debug("Entering mergeDocoments...");
    logger.debug("Looking for elemName=" + elemName + ",childNameRef=" + childNameRef + ",childName=" + childName);
    
    try {
      NodeList aList = adoc.getElementsByTagName(elemName);
      NodeList bList = bdoc.getElementsByTagName(elemName);

      if (aList != null && bList != null) {
          String value = null;
          for (int i = 0; i < aList.getLength(); i++) {
              Element aElem = (Element)aList.item(i);
              // Get referencing child element value.
              Element aChildElem = (Element)(aElem.getElementsByTagName(childNameRef)).item(0);
              Text aChildText = (Text)aChildElem.getFirstChild();
              String aRefValue = aChildText.getNodeValue();
             
              for (int j = 0; j < bList.getLength(); j++) {
                  Element bElem = (Element)bList.item(j);
                  Element bChildElem = (Element)(bElem.getElementsByTagName(childNameRef)).item(0);
                  Text bChildText = (Text)bChildElem.getFirstChild();
                  String bRefValue = bChildText.getNodeValue(); 
                  
                  if (aRefValue.equals(bRefValue)) {
                      // Found element in bdoc. Set attrName/attrValue on this element.
                      
                      Element childElem = (Element)(bElem.getElementsByTagName(childName)).item(0);
                      Text childText = (Text)childElem.getFirstChild();
                      String attrValue = childText.getNodeValue();
                      logger.debug("Found Element " + childNameRef + "=" + aRefValue);
                      logger.debug("Setting Element name=" + elemName + "=" + childName +" to value=" + attrValue);
                      // Take the element from bdoc and set it as an attribute.
                      aElem.setAttribute(childName, attrValue);
                      break;
                  }
              }
          }
        }
    } catch (Exception ex) {
      logger.error(ex);
      // INTERNAL ERROR
      throw new SecurityAdminException("1");     
    }
    return adoc;
  }

  /**
   * Creates an element with the speficified element name. Append
   * the text node as a child of the elment. Append the element to
   * its parent.
   * @param doc document to create the element
   * @param parent parent element name tag
   * @param element element name tag to be created
   * @param text text node to be created
   */
  public static Document createElementWithTextData(Document doc, 
                                        String parent,
                                        String element, 
                                        String text) {
      Element elem = doc.createElement(element);
      Text textNode = doc.createTextNode(text == null? "" : text);
      elem.appendChild(textNode);

      //Find it's parent node and append to it.
      NodeList parentList = (NodeList)doc.getElementsByTagName(parent);
      if (parentList != null ) {
          Element parentElem = (Element)(parentList.item(0));
          if (parentElem != null) {
              parentElem.appendChild(elem);
          }
      }
      return doc;
  }

  /**
   * Returns error XML in the format of ERRORS/ERROR(@fieldname, @description)
   */
  public static Document getErrorDocumentFromMap(HashMap errorMap) throws Exception {
      return getDocumentFromMap(SecurityAdminConstants.XML_ERROR_TOP,
                                SecurityAdminConstants.XML_ERROR_BOTTOM,
                                SecurityAdminConstants.XML_ATTR_FIELD,
                                SecurityAdminConstants.XML_ATTR_DESCRIPTION,
                                errorMap);
  }

  /**
   * Create Document with top, bottom as element tags and the map key/values as attributes.
   */
  public static Document getDocumentFromMap(String top, 
                                            String bottom, 
                                            String keyname,
                                            String keyvalue,
                                            HashMap params)
  throws Exception {
      Document doc = XMLHelper.getDocumentWithRoot(top);
      
      Iterator keyIterator = params.keySet().iterator();
      String key = null;
      if (keyIterator != null) {
        while (keyIterator.hasNext()) {
            key = (String)keyIterator.next();
            Element errorElem = doc.createElement(bottom);
            errorElem.setAttribute(keyname, key);
            errorElem.setAttribute(keyvalue, (String)params.get(key));
            doc.getDocumentElement().appendChild(errorElem);
        }
      }
      return doc;   
  }

  /**
   * Returns a tree that has unique firstRefAttrName and secondRefAttrName value.
   * @doc document
   * @elemName name of the element to do comarison
   * @firstRefAttrName first attribute name
   * @secondRefAttrName second attribute name
   */
  public static Document getUniqueSubTree(Document doc, 
                                    String elemName,
                                    String firstRefAttrName,
                                    String secondRefAttrName) 
  throws Exception {
      HashMap uniqueMap = new HashMap();
      NodeList nl = doc.getElementsByTagName(elemName);
      Object mapValueObj = null;
      String mapValueString = null;

      if (nl != null) {
          for (int i = 0; i < nl.getLength(); i++) {
              Element elem = (Element)nl.item(i);
              String key = elem.getAttribute(firstRefAttrName);
              String value = elem.getAttribute(secondRefAttrName);
              mapValueObj = uniqueMap.get(key);
              
              if(mapValueObj != null) {
                  mapValueString = (String) mapValueObj;
              }
              if (mapValueString != null && value.equals((String)mapValueString)){
                  // Remove element
                  elem.getParentNode().removeChild(elem);                 
              } else {
                  uniqueMap.put(key, value);             
              }             
          }
          
      }
      return doc;

  }

}