package com.ftd.security.util;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.cxf.common.util.StringUtils;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.Field;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.cache.vo.LineItemVO;
import com.ftd.security.cache.vo.UserLineItemVO;
import com.ftd.security.cache.vo.UserVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.ExcelProcessingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.manager.UserManager;

/**
 * File processor that performs tasks to load data for persistence from Excel spreadsheet..
 * @author Christy Hu
 */
public class FileProcessor
{
  private static final String LOGGER_CATEGORY = "com.ftd.security.util.FileProcessor";
  private static final int EXCEL_START_ROW = 1;
  private static final String REQUIRED_FIELD_MISSING = "Required Field Missing";
  private static final String DATA_TOO_LARGE = "Data Too Large";
  private static final String INVALID_DATE = "Invalid Date";
  private static final String INVALID_DEPARTMENT = "Invalid Department";
  private static final String INVALID_CS_GROUP = "Invalid CS Group";
  private static final String INVALID_IDENTITY_ID = "Invalid Identity ID";
  private static final String INVALID_CALL_CENTER = "Invalid Call Center";
  private static final String INVALID_ROLE = "Invalid Role";
  private static final String INVALID_STRING = "Field must be alphanumeric.";
  private static final String DATA_TOO_SMALL = "Data Too Small.";
  private static final String IDENTITY_EXISTS = "User ID Exists";
  private static final String DATABASE_ERROR = "Database Error - Cannot Persist";
  private static Logger logger;
  private static FileProcessor fileProcessor;

  /**
   * Private constructor prevents object being instantiated by user.
   */
  private FileProcessor() {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**
   * Returns the static object.
   * @return FileProcessor
   */
  public static FileProcessor getInstance ()
  {
	  if(fileProcessor == null) {
    		fileProcessor = new FileProcessor();
	  }

    return fileProcessor;
  }

  /**
   * Persists object from file.
   * @param uploadedStream InputStream received from request
   * @param subject Indicator of the type of object to be loaded.
   * @throws ExcelProcessingException
   * @throws Exception
   * @return ArrayList containing 
   */
  public static ArrayList processFile(InputStream uploadedStream, String subject, Object securityToken)
    throws ExcelProcessingException, Exception
  {
        logger.debug("Entering processFile...");
   
        // Get a list of LineItemVO's from InputStream
        List lineItemList = SpreadsheetUtil.getInstance().getLineItemObjects(uploadedStream, subject);

        // Call procedure to persist VO.  
        LineItemVO item = null;
        ArrayList newIdentities = new ArrayList();

        // Validate data first. Throw error if any object is invalid.            
        for (int i = 0; i < lineItemList.size(); i++)
        {
            
            item = (LineItemVO)lineItemList.get(i);
            //item.
    
            validateItemObject(item);
        }

        // All objects are valid. Persist to DB. User one connection.
        Connection con = null;
        try {
            con = DataRequestHelper.getDBConnection();
            con.setAutoCommit(false);
            for (int i = 0; i < lineItemList.size(); i++)
            {
                item = (LineItemVO)lineItemList.get(i);
                persistItemObject(con, item, securityToken);
                newIdentities.add(item);
            }
            con.commit();
        } catch (Exception e) {
            con.rollback();
            throw e;
        } finally {
            if (con != null ) {
                con.close();
            }               
        }
        
        
        return newIdentities;
  }
  
  /**
   * Persists the CS Group for a particular user
 * @param uploadedStream
 * @param subject
 * @param securityToken
 * @return ArrayList containing user IDs
 * @throws ExcelProcessingException
 * @throws Exception
 */
  public static ArrayList processUserFile(InputStream uploadedStream, String subject, String actionType, Object securityToken)
		    throws ExcelProcessingException, Exception
		  {
		        logger.debug("Entering processUserFile...");
		   
		        // Get a list of LineItemVO's from InputStream
		        List lineItemList = SpreadsheetUtil.getInstance().getLineItemObjects(uploadedStream, subject);

		        // Call procedure to persist VO.  
		        LineItemVO item = null;
		        ArrayList userIds = new ArrayList();

		        // Validate data first. Throw error if any object is invalid.            
		        for (int i = 0; i < lineItemList.size(); i++)
		        {
		            
		            item = (LineItemVO)lineItemList.get(i);
		            //item.
		            
		            if("terminate".equalsIgnoreCase(actionType))
		            {
		            	validateBulkUserTerminateObject(item);
		            }
		            else //("load".equalsIgnoreCase(actionType))
		            {
		            	validateUserGroupItemObject(item);
		            }
		        }

		        // All objects are valid. Persist to DB. Use one connection.
		        Connection con = null;
		        try {
		            con = DataRequestHelper.getDBConnection();
		            con.setAutoCommit(false);
		            for (int i = 0; i < lineItemList.size(); i++)
		            {
		                item = (LineItemVO)lineItemList.get(i);
		                persistUserObject(con, item, actionType, securityToken);
		                userIds.add(item);
		            }
		            con.commit();
		        } catch (Exception e) {
		            con.rollback();
		            throw e;
		        } finally {
		            if (con != null ) {
		                con.close();
		            }               
		        }
		        
		        
		        return userIds;
		  }
  
	/** Validate the File data for Bulk User Termination process.
	 * 1. A user identity is required by default, cannot be non alpha numeric.  
	 * 2. If Expire date is null - treated as curr date. Should be valid format when non null.
	 * @param item
	 * @throws ExcelProcessingException
	 * @throws Exception
	 */
	private static void validateBulkUserTerminateObject(LineItemVO item) throws ExcelProcessingException, Exception {
		if(logger.isDebugEnabled()) {
			logger.debug("Entering validateBulkUserTerminateObject...");
		}
		List fieldList = item.getFieldList();
		
		for (int i = 0; i < fieldList.size(); i++) {
			Field field = (Field) fieldList.get(i);
			
			if (field.getName().equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)) {
				if (StringUtils.isEmpty(field.getValue())) {
					throw new ExcelProcessingException(REQUIRED_FIELD_MISSING, item.getLineNumber() + 1, field.getName());
				}				
				if (!StringUtil.isAlphaNumeric(field.getValue())) {
					throw new ExcelProcessingException(INVALID_STRING, item.getLineNumber() + 1, field.getName());
				}				
				if (!identityExists(field.getValue())) {
					throw new ExcelProcessingException("The User ID entered doesn't exist in Apollo. Please correct it in ", item.getLineNumber() + 1);
				}
			}

			if (field.getName().equalsIgnoreCase(UserLineItemVO.EXP_DATE) && !StringUtils.isEmpty(field.getValue())) {
				if (!StringUtil.isValidDateString(field.getValue().trim())) {
					throw new ExcelProcessingException(INVALID_DATE, item.getLineNumber() + 1, field.getName());
				}
			}
		}
		logger.debug("exiting validateBulkUserTerminateObject...");
	}

public static List processSpreadsheetFile(InputStream uploadedStream, Class lineItemVOClass)
      throws ExcelProcessingException, Exception
    {
          logger.debug("Entering processSpreadsheetFile...");
     
          // Get a list of LineItemVO's from InputStream
          List lineItemList = SpreadsheetUtil.getInstance().getLineItemObjects(uploadedStream, lineItemVOClass);
 
          LineItemVO item = null;
          // Validate data. Throw error if any object is invalid.            
          for (int i = 0; i < lineItemList.size(); i++)
          {
              item = (LineItemVO)lineItemList.get(i);
              item.validate();
          }
          return lineItemList;
    }

  /**
   * Validates the LineItemVO object loaded from file.
   * @param item Generic LineItemVO.
   * @throws ExcelProcessingException
   * @throws Exception
   */
  public static void validateItemObject(LineItemVO item) 
  throws ExcelProcessingException, Exception {

          // Get list of all fields.
          ArrayList fieldList = item.getFieldList();
          String department = null; // User this to track what the department is.
          for (int i = 0; i < fieldList.size(); i++) {
              Field field = (Field)fieldList.get(i);
              boolean isRequired = field.isRequired();
              int maxSize = field.getMaxSize();
              String type = field.getType();
              String value = field.getValue();
              String columnName = field.getName();
              
              
              // For each field, validate the following:
              // If the field is required, it's value cannot be null.
              if (isRequired && (value == null || value.length() == 0)) {
                  throw new ExcelProcessingException(REQUIRED_FIELD_MISSING, item.getLineNumber()+1, columnName);             
              }
              // Field size cannot be greater than field max size.
              if (value != null && value.length() > maxSize) {
                  throw new ExcelProcessingException(DATA_TOO_LARGE, item.getLineNumber()+1, columnName);
              }
              // If the field is first name, last name, or userid, check if there are special chars.
              if(columnName.equalsIgnoreCase(UserLineItemVO.FIRST_NAME)
                || columnName.equalsIgnoreCase(UserLineItemVO.LAST_NAME)
                || columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)){
                  if (!StringUtil.isAlphaNumeric(value)){
                      throw new ExcelProcessingException(INVALID_STRING, item.getLineNumber()+1, columnName);
                  }
                  // If the field is userid, size must be at least 6 char.
                  if (columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)
                      && value.length() < 6){
                      throw new ExcelProcessingException(DATA_TOO_SMALL, item.getLineNumber()+1, columnName);
                  }
                }
              
              
              // If the field is of date type, validate it's format.
              if (type.equals(Field.TYPE_DATE)) 
              {
/*
                  try {
                      Date sqlDate = StringUtil.toSqlDate(value);
                  } catch (SecurityAdminException ex) {
                      logger.error(ex);
                      throw new ExcelProcessingException(INVALID_DATE, item.getLineNumber()+1, columnName);
                  }
*/
                  try 
                  {
                
                     DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
                     df.setLenient(false);  // this is important!
                     java.util.Date dt2 = df.parse(value);
                  }
                  catch (ParseException pe) 
                  {
                     logger.error(pe);
                     throw new ExcelProcessingException(INVALID_DATE, item.getLineNumber()+1, columnName);
                  }
                  catch (IllegalArgumentException iae) 
                  {
                     logger.error(iae);
                     throw new ExcelProcessingException(INVALID_DATE, item.getLineNumber()+1, columnName);
                  }
                  
              }

              // If the field name is 'Department', make sure it's a valid department value.
              if (columnName.equalsIgnoreCase(UserLineItemVO.DEPARTMENT)) {
                  String id = UserManager.getValidDepartmentId(value);
                  if (id == null || id.length() == 0) {
                      throw new ExcelProcessingException(INVALID_DEPARTMENT, item.getLineNumber()+1, columnName);
                  }
                  department = id;
              }
              
              // If the field name is 'CS Group', make sure it's a valid CS Group value.
              // Required if department is CS.
              if (columnName.equalsIgnoreCase(UserLineItemVO.CS_GROUP)) {                  
                  if (UserVO.DEPT_CS.equals(department) || (value!= null && value.trim().length() > 0)) {
                      String id = UserManager.getValidCsGroupId(value);
                      if (id == null || id.length() == 0) {
                          throw new ExcelProcessingException(INVALID_CS_GROUP, item.getLineNumber()+1, columnName);
                      }      
                  } 
              }
              // If the field name is 'Call Center' , make sure it's a valid Call Center.
              // Required if department is CS.
              if (columnName.equalsIgnoreCase(UserLineItemVO.CALL_CENTER)) {
                  if (UserVO.DEPT_CS.equals(department) || (value!= null && value.trim().length() > 0)) {
                      String id = UserManager.getValidCallCenterId(value);
                      if (id == null || id.length() == 0) {
                          throw new ExcelProcessingException(INVALID_CALL_CENTER, item.getLineNumber()+1, columnName);
                      }   
                  }
              }
              // If the field name is 'Role', make sure it's a valid Role Name in Order Proc.

			if (columnName.equalsIgnoreCase(UserLineItemVO.ROLE) && value != null && value.toString().trim().length() > 0) {
				StringTokenizer roleNames = new StringTokenizer(value, ",");
				while (roleNames.hasMoreElements()) {
					String role = (String) roleNames.nextElement();
					role = role.trim();
					logger.debug("Role: " + role);
					String id = UserManager.getValidRoleId(role);
					if (id == null || id.length() == 0) {
						throw new ExcelProcessingException(INVALID_ROLE + ": " + role, item.getLineNumber() + 1, columnName);
					}
				} 
              }
			
              // Check if identity exists.
              if (columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)) {
                  if (identityExists(value)) {
                      throw new ExcelProcessingException(IDENTITY_EXISTS, item.getLineNumber()+1, columnName);
                  }
              }
          }
  }

  /**
   * Validates the LineItemVO object loaded from file.
   * @param item Generic LineItemVO.
   * @throws ExcelProcessingException
   * @throws Exception
   */
  public static void validateUserGroupItemObject(LineItemVO item) 
		  throws ExcelProcessingException, Exception {
	  ArrayList fieldList = item.getFieldList();
      String department = null; // User this to track what the department is.
      for (int i = 0; i < fieldList.size(); i++) {
          Field field = (Field)fieldList.get(i);
          int maxSize = field.getMaxSize();
          String value = field.getValue();
          String columnName = field.getName();
	  
          
          if (value != null && value.length() > maxSize) {
              throw new ExcelProcessingException(DATA_TOO_LARGE, item.getLineNumber()+1, columnName);
          }
          // If the field is userid, check if there are special chars.
          if(columnName.equalsIgnoreCase(UserLineItemVO.FIRST_NAME)
                  || columnName.equalsIgnoreCase(UserLineItemVO.LAST_NAME)
                  || columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)){
                    if (!StringUtil.isAlphaNumeric(value)){
                        throw new ExcelProcessingException(INVALID_STRING, item.getLineNumber()+1, columnName);
              }
              // If the field is userid, size must be at least 6 char.
              if (columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)
                  && value.length() < 6){
                  throw new ExcelProcessingException(DATA_TOO_SMALL, item.getLineNumber()+1, columnName);
              }
            }
          	if (columnName.equalsIgnoreCase(UserLineItemVO.CS_GROUP)) {                  
              if (UserVO.DEPT_CS.equals(department) || (value!= null && value.trim().length() > 0)) {
                  String id = UserManager.getValidCsGroupId(value);
                  if (id == null || id.length() == 0) {
                      throw new ExcelProcessingException(INVALID_CS_GROUP, item.getLineNumber()+1, columnName);
                  }      
              } 
          }
          	if (columnName.equalsIgnoreCase(UserLineItemVO.IDENTITY_ID)) {
	          	if (!identityExists(value)) {
	                throw new ExcelProcessingException(INVALID_IDENTITY_ID, item.getLineNumber()+1, columnName);
	            }
	      }
          
         // Check if identity exists.
			if (columnName.equalsIgnoreCase(UserLineItemVO.SUPERVISOR_ID)) {
				if (!StringUtils.isEmpty(value)) {
					if (identityExists(value)) {
						BigDecimal supervisorId = null;
						try {
							HashMap inputParams = new HashMap();
							inputParams.put("in_user_identity", value);
							supervisorId = (BigDecimal) DataRequestHelper.executeStatement(
											DataRequestHelper.getDBConnection(),inputParams,
											StatementConstants.GET_USERID_FROM_IDENTITY);
							logger.debug("supervisorId is: " + supervisorId);
						} catch (Exception e) {
							logger.error("Exception occured while getting supervisor Id", e);
						}
						if (supervisorId != null) {
							field.setValue(supervisorId.toString());
						} else {
							throw new ExcelProcessingException(IDENTITY_EXISTS, item.getLineNumber() + 1, columnName);
						}
					} else {
						throw new ExcelProcessingException(IDENTITY_EXISTS, item.getLineNumber() + 1, columnName);
					}
				}
			}
      }       	
  }
  
  
  /**
   * Checks if identity exists.
   */
  public static boolean identityExists(String identityId) throws Exception {
      Connection con = null;
      String status = null;
      
      try 
      {
        con = DataRequestHelper.getDBConnection();
        HashMap inputParams = new HashMap();
        inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId); 
        logger.debug("In getIdentityExistsInputParams()...");
        logger.debug("Setting in_identity_id to: " + identityId);          
        status = (String)DataRequestHelper.executeStatement(con, 
                                                            inputParams, 
                                                            StatementConstants.USERS_IDENTITY_EXISTS);                  
      } finally 
      {
        if ((con != null) && (!con.isClosed())) 
        {
            con.close();
        }
        
      }      
      return "Y".equalsIgnoreCase(status);
  }
  
  /**
   * Persists the LineItemVO object.
   * @param item Generic LineItem value object.
   * @throws SQLException
   * @throws ExcelProcessingException
   * @throws Exception
   */
  public static void persistItemObject(Connection con, LineItemVO item, Object securityToken) 
  throws SQLException, ExcelProcessingException, Exception {
      //try {
          // Call specific persist procedure based on the particurlar LineItemVO object.
          if(item instanceof UserLineItemVO){
              logger.debug("Found UserLineItemVO...");
              int userId = persistUser(con, (UserLineItemVO)item, securityToken);
              // Create user.
              ((UserLineItemVO)item).setUserId(userId);
              // Create identity. Identity has been verified and does not exist in system.
              persistIdentity(con, (UserLineItemVO)item, securityToken);
              // Add role info.
              addIdentityRole(con, (UserLineItemVO)item, securityToken);
          }
          /*
      } catch (Exception e) {
          logger.error(e);
          throw e;
      }*/
  }

  
  /**
   * Persists the LineItemVO object.
   * @param item Generic LineItem value object.
   * @throws SQLException
   * @throws ExcelProcessingException
   * @throws Exception
   */
	public static void persistUserObject(Connection con, LineItemVO item, String actionType,
			Object securityToken) throws SQLException,
			ExcelProcessingException, Exception {
		// Call specific persist procedure based on the particular LineItemVO
		// object.
		if (item instanceof UserLineItemVO) {
			logger.debug("Found UserLineItemVO...");
			if("terminate".equalsIgnoreCase(actionType))
			{
				terminateUser(con, (UserLineItemVO) item, securityToken);
			}
			else
				updateGroup(con, (UserLineItemVO) item, securityToken);
		}
	}

  /**
   * Persists a UserLineItemVO object. 
   * @param item UserLineItem object
   * @return user id.
   * @throws SQLException
   * @throws ExcelProcessingException
   * @throws Exception
   */
  public static int persistUser(Connection con, UserLineItemVO item, Object securityToken) 
  throws SQLException, SecurityAdminException, ExcelProcessingException, Exception {

      DataAccessUtil util = null;
      DataRequest dataRequest = null;
      HashMap inputParams = null;
      Map outputMap = null;
      String status = null;
      String message = null;
      BigDecimal userId = null;
      int lineNumber = item.getLineNumber();

      //try {
            util = DataAccessUtil.getInstance();
            dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            inputParams = getAddUserInputParams(item, securityToken);
            logger.debug("persistUser :: add_user input params:" + inputParams);
            dataRequest.setInputParams(inputParams);
            dataRequest.setStatementID(StatementConstants.USERS_ADD_USERS);
            outputMap = (Map) util.execute(dataRequest);
            status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
            userId = (BigDecimal)outputMap.get(UserVO.ST_USER_ID);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO ADD USER." + message);
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new ExcelProcessingException(DATABASE_ERROR, lineNumber+1);
                } else {
                    throw new ExcelProcessingException(message, lineNumber+1);
                }
            } else if (userId == null) {
                logger.error("UserId is null");
                throw new ExcelProcessingException(DATABASE_ERROR, lineNumber+1);
            }      
            return userId.intValue();
            /*
      } catch (Exception e) {
          logger.error(e);
          throw e;
      } */
  }

  /**
   * Converts a  UserLineItemVO to a HashMap
   * @param lineItem UserLineItemVO to be analyzed
   * @throws Exception
   */
   private static HashMap getAddUserInputParams(UserLineItemVO lineItem, Object securityToken) 
   throws Exception {
      logger.debug("Entering getAddUserInputParams...");
      HashMap inputParams = new HashMap();
      String sLastName = "";
      String sFirstName = "";
      String sDepartment = "";
      String sCallCenter = "";
      String sCsGroup = "";
      String sEmailId = "";
      String sEmployeeId = "";
      String sSupervisorId = "";
      //String sAddr = null;
      //String sAddr2 = null;
      //String sCity = null;
      //String sState = null;
      //String sZip = null;
      //String sPhone = null;
      //String sExpDate = null;
      //String sEmail = null;
      //String sFax = null;
      //String sExt = null;
      
      try {
          ArrayList fieldList = lineItem.getFieldList();
          for(int i = 0 ; i < fieldList.size(); i++) {
              Field field = (Field)fieldList.get(i);
              String name = field.getName();
              String value = field.getValue();
              if(name != null && name.equals(UserLineItemVO.FIRST_NAME)) {
                  sFirstName = value;
              } else if (name != null && name.equals(UserLineItemVO.LAST_NAME)) {
                  sLastName = value;
              } else if (name != null && name.equals(UserLineItemVO.DEPARTMENT)) {
                  sDepartment = UserManager.getValidDepartmentId(value);
              } else if (name != null && name.equals(UserLineItemVO.CS_GROUP)) {
                  sCsGroup = UserManager.getValidCsGroupId(value);
              } else if (name != null && name.equals(UserLineItemVO.CALL_CENTER)) {
                  sCallCenter = UserManager.getValidCallCenterId(value);
              } else if (name != null && name.equals(UserLineItemVO.EMAIL)) {
            	  sEmailId = value;
              } else if (name != null && name.equals(UserLineItemVO.EMPLOYEE_ID)) {
                  sEmployeeId = value;
              } else if (name != null && name.equals(UserLineItemVO.SUPERVISOR_ID)) {
                  sSupervisorId = value;
              }
                  /*
              } else if (name.equals(UserLineItemVO.ADDRESS_ONE)) {
                  sAddr = value;
              } else if (name.equals(UserLineItemVO.ADDRESS_TWO)) {
                  sAddr2 = value;
              } else if (name.equals(UserLineItemVO.CITY)) {
                  sCity = value;
              } else if (name.equals(UserLineItemVO.STATE)) {
                  sState = value;
              } else if (name.equals(UserLineItemVO.ZIP)) {
                  sZip = value;
              } else if (name.equals(UserLineItemVO.PHONE)) {
                  sPhone = value;                 
              } else if (name.equals(UserLineItemVO.EXP_DATE)) {
                  sExpDate = value;
              } else if (name.equals(UserLineItemVO.EMAIL)) {
                  sEmail = value;
              } else if (name.equals(UserLineItemVO.FAX)) {
                  sFax = value;
              } else if (name.equals(UserLineItemVO.PHONE_EXT)) {
                  sExt = value;
                  
              } */
          }
          
          logger.debug("Setting in_last_name to: " +  sLastName);
          logger.debug("Setting in_first_name: " +  sFirstName);
          logger.debug("Setting in_department_id: " +  sDepartment);
          logger.debug("Setting in_cs_group_id: " +  sCsGroup);
          logger.debug("Setting in_call_center_id: " +  sCallCenter);
          logger.debug("Setting user_id to null, providing the type java.sql.Types.INTEGER");
          /*
          logger.debug("Setting in_address_1: " +  sAddr);
          logger.debug("Setting in_address_2: " +  sAddr2);
          logger.debug("Setting in_city: " +  sCity);
          logger.debug("Setting in_state: " +  sState);
          logger.debug("Setting in_zip: " +  sZip);
          logger.debug("Setting in_phone: " +  sPhone);
          logger.debug("Setting in_expire_date: " +  sExpDate);
          logger.debug("Setting in_email: " +  sEmail);
          logger.debug("Setting in_fax: " +  sFax);
          logger.debug("Setting in_extension: " +  sExt);
          */
          
          inputParams.put(UserVO.ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
          inputParams.put(UserVO.ST_LAST_NAME, sLastName);
          inputParams.put(UserVO.ST_FIRST_NAME, sFirstName);
          inputParams.put(UserVO.ST_ADDRESS_1, "");
          inputParams.put(UserVO.ST_ADDRESS_2, "");
          inputParams.put(UserVO.ST_CITY, "");
          inputParams.put(UserVO.ST_STATE, "");
          inputParams.put(UserVO.ST_ZIP, "");
          inputParams.put(UserVO.ST_PHONE, "");
          //inputParams.put(UserVO.ST_EXP_DATE, StringUtil.toSqlDate("12/31/2003"));
          inputParams.put(UserVO.ST_EXP_DATE, Integer.valueOf(String.valueOf(java.sql.Types.DATE)));
          inputParams.put(UserVO.ST_EMAIL, "");
          inputParams.put(UserVO.ST_FAX, "");
          inputParams.put(UserVO.ST_PHONE_EXT, "");
          inputParams.put(UserVO.ST_DEPARTMENT_ID, sDepartment);
          inputParams.put(UserVO.ST_CALL_CENTER_ID, sCallCenter);
          inputParams.put(UserVO.ST_CS_GROUP_ID, sCsGroup);          
          inputParams.put(UserVO.ST_EMAIL, sEmailId);          
          inputParams.put(UserVO.ST_IN_EMPLOYEE_ID, sEmployeeId);
          inputParams.put(UserVO.ST_IN_SUPERVISOR_ID, sSupervisorId);          
         
          // Set user_id to null, providing the type.
          inputParams.put(UserVO.ST_USER_ID, Integer.valueOf(String.valueOf(java.sql.Types.INTEGER)));

      } catch (Exception ex) {
          logger.error("Caught exception in getAddUserInputParams...");
          throw ex;
      }
      
      return inputParams;
   }

  /**
   * Creates an identity for UserLineItemVO object. 
   * @param item UserLineItemVO that the identity is associated.
   * @throws SQLException
   * @throws ExcelProcessingException
   * @throws Exception
   */
  public static void persistIdentity(Connection con, UserLineItemVO item, Object securityToken) 
  throws SQLException, SecurityAdminException, ExcelProcessingException, Exception {
          DataAccessUtil util = null;
          DataRequest dataRequest = null;
          HashMap inputParams = null;
          Map outputMap = null;
          String status = null;
          String message = null;  
          String password = null;
      
          int userId = item.getUserId();

         
          util = DataAccessUtil.getInstance();
          dataRequest = new DataRequest();  
          dataRequest.setConnection(con);
          
          inputParams = getAddIdentityInputParams(item, securityToken);                  
          dataRequest.setInputParams(inputParams);         
          dataRequest.setStatementID(StatementConstants.USERS_ADD_IDENTITY);
          
          outputMap = (Map) util.execute(dataRequest);
          status = (String)outputMap.get(SecurityAdminConstants.STATUS); 
          message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
          item.setPassword((String)outputMap.get(SecurityAdminConstants.COMMON_PARM_CREDENTIALS));
        
          logger.error("status = " + status);
          logger.error("message = " + message);
          // check status
          if (!"Y".equalsIgnoreCase(status))  {
              logger.debug(DATABASE_ERROR + item.getLineNumber());
              throw new ExcelProcessingException(DATABASE_ERROR, item.getLineNumber()); 
          } 

  }


     /**
      * Returns a HashMap to add identity.
      */
     public static HashMap getAddIdentityInputParams(UserLineItemVO lineItem, Object securityToken)
      throws Exception
     {
          HashMap inputParams = new HashMap();
          String sIdentityId = "";
          String sIdExpireDate = "";
          String sLockedFlag = "N";
          int userId = lineItem.getUserId();
          
          ArrayList fieldList = lineItem.getFieldList();
          
          for(int i = 0 ; i < fieldList.size(); i++) {
              Field field = (Field)fieldList.get(i);
              String name = field.getName();
              String value = field.getValue();
              if(name != null && name.equals(UserLineItemVO.IDENTITY_ID)) {
                  sIdentityId = value;
              }
              else if(name != null && name.equals(UserLineItemVO.EXP_DATE)) {
                  sIdExpireDate = value;
              }
          }        
          try{  
              logger.debug("Setting " + IdentityVO.ST_UPDATED_BY + "to: " + ServletHelper.getMyId(securityToken));
              logger.debug("Setting " + IdentityVO.ST_IDENTITY_ID + "to: " + sIdentityId);   
              logger.debug("Setting " + IdentityVO.ST_USER_ID + "to: " + userId);
              logger.debug("Setting " + IdentityVO.ST_DESCRIPTION + "to: ");
              logger.debug("Setting " + IdentityVO.ST_IDENTITY_EXP_DATE + "to: " + sIdExpireDate);
              logger.debug("Setting " + IdentityVO.ST_LOCKED + "to: " + sLockedFlag);                       
              inputParams.put(IdentityVO.ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
              inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);   
              inputParams.put(IdentityVO.ST_USER_ID, new Integer(userId));
              inputParams.put(IdentityVO.ST_DESCRIPTION, "");
              inputParams.put(IdentityVO.ST_IDENTITY_EXP_DATE, StringUtil.toSqlDate(sIdExpireDate));
              inputParams.put(IdentityVO.ST_LOCKED, sLockedFlag);
              return inputParams;           
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }          
         
     }

  /**
   * Add identity role.
   */
  public static void addIdentityRole(Connection con, UserLineItemVO item, Object securityToken)
  throws ExcelProcessingException, Exception
  {
      Field fIdentityId = item.getFieldByName(UserLineItemVO.IDENTITY_ID);
      Field fRoleName = item.getFieldByName(UserLineItemVO.ROLE);      
      if (fIdentityId == null) {
          throw new SecurityAdminException("1");
      }
      if (fRoleName != null ){
          String identityId = fIdentityId.getValue();
          StringTokenizer roleNames = new StringTokenizer(fRoleName.getValue(), ",");
          
          while (roleNames.hasMoreElements()) { 

	          String roleId = UserManager.getValidRoleId(String.valueOf(roleNames.nextElement()).trim());
	          if (roleId != null && roleId.length() > 0 ) {
	              HashMap inputParams = new HashMap();
	              logger.debug("Setting " + IdentityVO.ST_UPDATED_BY + "to: " + ServletHelper.getMyId(securityToken));
	              logger.debug("Setting " + IdentityVO.ST_IDENTITY_ID + "to: " + identityId);  
	              logger.debug("Setting " + IdentityVO.ST_ROLE_ID + "to: " + roleId);
	          
	              inputParams.put(IdentityVO.ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
	              inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);  
	              inputParams.put(IdentityVO.ST_ROLE_ID, new Long(roleId));  
	              
	              HashMap outputMap = (HashMap)DataRequestHelper.executeStatement
	                  (con, inputParams, StatementConstants.ROLE_UPDATE_IDENTITY_ROLE);
	              // check status
	              String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
	              String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
	              logger.debug("Status: " + status);
	              logger.debug("Message: " + message);
	              if (!"Y".equalsIgnoreCase(status))  {
	                  throw new ExcelProcessingException(DATABASE_ERROR, item.getLineNumber());
	              }
	          }
	      }
     }
  }
  /**
   * Adds a CS Group to a particular Identity ID.
   */
  public static void updateGroup(Connection con, UserLineItemVO item, Object securityToken)
  throws ExcelProcessingException, Exception
  {
      Field fIdentityId = item.getFieldByName(UserLineItemVO.IDENTITY_ID);
      Field fGroupName = item.getFieldByName(UserLineItemVO.CS_GROUP);
      if (fIdentityId == null) {
          throw new SecurityAdminException("1");
      }
      if (fGroupName != null ){
          String identityId = fIdentityId.getValue();
          String groupId = fGroupName.getValue();          
     	  String validGroupId = UserManager.getValidCsGroupId(groupId);
          if (validGroupId != null && validGroupId.length() > 0 ) {
              HashMap inputParams = new HashMap();
              logger.debug("Setting " + IdentityVO.ST_UPDATED_BY + " to: " + ServletHelper.getMyId(securityToken));
              logger.debug("Setting " + IdentityVO.ST_IDENTITY_ID + " to: " + identityId);  
              logger.debug("Setting " + IdentityVO.ST_GROUP_ID + " to: " + validGroupId);
          
              inputParams.put(IdentityVO.ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
              inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);  
              inputParams.put(IdentityVO.ST_GROUP_ID, validGroupId);  
              
              HashMap outputMap = (HashMap)DataRequestHelper.executeStatement
                  (con, inputParams, StatementConstants.GROUP_UPDATE_GROUP);
              // check status
              String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
              String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
              logger.debug("Status: " + status);
              logger.debug("Message: " + message);
              if (!"Y".equalsIgnoreCase(status))  {
                  throw new ExcelProcessingException(DATABASE_ERROR, item.getLineNumber());
              }
          }
	      
     }
  }

	/** Terminates the user(Expire the user account to given date)
	 * @param con
	 * @param item
	 * @param securityToken
	 * @throws ExcelProcessingException
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void terminateUser(Connection con, UserLineItemVO item, Object securityToken) 
			throws ExcelProcessingException, Exception {
		
		if(logger.isDebugEnabled()) {
			logger.debug("Entering terminateUser...");
		}
		
		Field fIdentityId = item.getFieldByName(UserLineItemVO.IDENTITY_ID);
		Field fExpDate = item.getFieldByName(UserLineItemVO.EXP_DATE);
		
		if (fIdentityId == null) {
			throw new SecurityAdminException("1");
		}
		  
		String sExpDate = fExpDate.getValue();
		HashMap inputParams = new HashMap();
		
		if (logger.isDebugEnabled()) {
			logger.debug("Setting " + IdentityVO.ST_UPDATED_BY + " to " + ServletHelper.getMyId(securityToken) + " and "
					+ IdentityVO.ST_IDENTITY_EXP_DATE + " to " + sExpDate + " for user, " + fIdentityId.getValue());
		}
			
		inputParams.put(IdentityVO.ST_IDENTITY_ID, fIdentityId.getValue());		
		if (sExpDate == null || sExpDate.trim().length() == 0) {
			Date date = Calendar.getInstance().getTime();
			sExpDate = StringUtil.formatUtilDateToString(date);
		}
		inputParams.put(IdentityVO.ST_IDENTITY_EXP_DATE, StringUtil.toSqlDate(sExpDate));
		inputParams.put(IdentityVO.ST_UPDATED_BY, ServletHelper.getMyId(securityToken));

		Map outputMap = (HashMap) DataRequestHelper.executeStatement(con, inputParams, StatementConstants.BULK_USER_TERMINATE);
		if(outputMap == null || !"Y".equalsIgnoreCase((String) outputMap.get(SecurityAdminConstants.STATUS))) {
			throw new ExcelProcessingException(DATABASE_ERROR, item.getLineNumber());
		}		
		logger.info("Succesfully updated identity: " + (String) outputMap.get(SecurityAdminConstants.MESSAGE));		
	}
}