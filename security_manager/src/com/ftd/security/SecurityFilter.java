package com.ftd.security;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.security.util.ServletHelper;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class SecurityFilter implements Filter
{
    private FilterConfig _filterConfig = null;
    private Logger logger = new Logger("com.ftd.security.SecurityFilter");

    private static final String SECURITY_CONTEXT = "context";
    private static final String SECURITY_TOKEN = "securitytoken";
    private static final String PARAMETERS = "parameters";
    private static final String SKIP_SECURITY = "skip_security";

    private static final String HEADER_CONTENT_TYPE = "CONTENT-TYPE";
    private static final String MULTPART_FORM_DATA = "multipart/form-data";

    private static final String PROPERTY_FILE = "security-config.xml";

    private static final String LOGIN_PAGE = "login_page";
    private static final String ERROR_PAGE = "error_page";


    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        _filterConfig = filterConfig;
    }


    public void destroy()
    {
        _filterConfig = null;
    }


    /**
     * Get security context and security token.
     * 
     * If security checking is on, verify session information by checking for:
     *   1. expired identity,
     *   2. expired session,
     *   3. invalid session.
     *   
     *   If one of the above business error conditions occurs, forward control to 
     *   the login page.  Otherwise, place the security information into a hashmap,
     *   load onto the request object and forward to the next program in the 
     *   command chain.
     *   
     * If security checking is off, pass control to the next program in the command 
     * chain.
     * 
     * If a non-business exception (unlike the business exceptions listed above) 
     * occurs, attempt to redirect to an error page.  If an error occurs during the 
     * redirect, log an error and stop.
     *   
     * @param ServletRequest  - input request object
     * @param ServletResponse - object used to respond to the user
     * @param FilterChain     - next program to be executed in the chain
     * 
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String context = null,
               securityToken = null;

        String securityCheck = _filterConfig.getInitParameter("PERFORM-SECURITY-CHECK");
        boolean performSecurityCheck = (securityCheck != null  &&  securityCheck.equalsIgnoreCase("true")) ? true : false;

        try
        {
            HashMap parameters = new HashMap();

            if(isMultipartFormData(((HttpServletRequest)request )))
            {
                context = (String)request.getAttribute(SECURITY_CONTEXT);
                securityToken = (String)request.getAttribute(SECURITY_TOKEN);
                
                // This added logic fixes the incorrect assumption that
                // all multipart HTTP requests have the security tokens
                // as attributes.
                if(securityToken == null) {
                    // Note: Do NOT invoke ServletHelper.getMultipartParam at this location
                    // since it is *destructive* to the request object,
                    // making it unreadable to any other chained servlet.
                    context = request.getParameter(SECURITY_CONTEXT);
                    securityToken = request.getParameter(SECURITY_TOKEN);
                }
            }
            else
            {
                //Check the request parameter to see if security should
                //not be checked for this request.  This allows a page
                //to specify if the security token should not be checked.               
                String skipSecurity = request.getParameter(SKIP_SECURITY);
                if(skipSecurity != null && skipSecurity.equals("Y"))
                {
                    performSecurityCheck = false;
                }

                
                context = request.getParameter(SECURITY_CONTEXT);
                securityToken = request.getParameter(SECURITY_TOKEN);
            }

            if (performSecurityCheck)
            {
                try
                {
                    if (! ServletHelper.isValidToken(context, securityToken))
                    {
                        logger.error("Invalid Security Token.");
                        logger.error(securityToken);
                        // redirect to the login page
                        ServletHelper.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response, securityToken);
                    }
                    else
                    {
                        parameters.put(SECURITY_CONTEXT, (context == null ? "" : context));
                        logger.debug("Setting contextId in parameter map: " + context);

                        parameters.put(SECURITY_TOKEN, (securityToken == null ? "" : securityToken));
                        logger.debug("Setting securityToken in parameter map: " + securityToken);

                        request.setAttribute(PARAMETERS, parameters);
                        chain.doFilter(request, response);
                    }
                }
                catch(ExpiredIdentityException e)
                {
                    logger.warn("Invalid Security Token.");
                    logger.warn(securityToken);
                    ServletHelper.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response, securityToken);
                }
                catch(ExpiredSessionException e)
                {
                    logger.warn("Invalid Security Token.");
                    logger.warn(securityToken);
                    ServletHelper.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response, securityToken);
                }
                catch(InvalidSessionException e)
                {
                    logger.warn("Invalid Security Token.");
                    logger.warn(securityToken);
                    ServletHelper.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response, securityToken);
                }
            }
            else
            {
                chain.doFilter(request, response);
            }
        } 
        catch (Exception ex)
        {
            logger.error(ex);
            try
            {
                redirectToError(response);
            }
            catch (Exception e)
            {
                logger.error(e);
            }
        }
    }

    /**
     * Redirect csr to the error page.
     * 
     * @param ServletResponse
     * 
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws XSLException
     */
    private void redirectToError(ServletResponse response)
        throws IOException, ParserConfigurationException, SAXException, 
               TransformerException, Exception
    {
        ((HttpServletResponse) response).sendRedirect(ConfigurationUtil.getInstance().getProperty(PROPERTY_FILE,ERROR_PAGE));
    }


    /**
     * Returns true if request content type is multipart/form-data.
     * 
     * @param request HttpServlet request to be analyzed
     * @throws Exception
     */
     public static boolean isMultipartFormData(HttpServletRequest request)
        throws Exception
     {
        boolean isMultipartFormData = false;
        String headerContentType = request.getHeader(HEADER_CONTENT_TYPE);
        if(headerContentType != null  &&  headerContentType.startsWith(MULTPART_FORM_DATA))
        {
            isMultipartFormData = true;
        }

        return isMultipartFormData;
       }

}