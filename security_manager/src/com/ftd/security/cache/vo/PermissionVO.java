package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

  /**
   * Constants that pertain to a Permission.
   * @author Christy Hu
   */
  public class PermissionVO extends AbstractVO
  {
      // Statement column input constants for calling SQL procedures.
      public static final String ST_PERMISSION_ID = "in_permission_id";
      public static final String ST_DESCRIPTION = "in_description";

      // Output XML structure.
      public static final String XML_TOP = "PERMISSIONS";
      public static final String XML_BOTTOM = "PERMISSION";

      // Data field that come from the request.
      public static final String RQ_PERMISSION_ID = "permission_id";
      public static final String RQ_DESCRIPTION = "description";
      // remove
      public static final String RQ_REMOVE_PERMISSION_ID = "permissionId";
      // table column (xml tag name)
      public static final String TB_PERMISSION_ID = "permission_id";

      private String permissionId;
      private String description;   

      public PermissionVO(){}

      public PermissionVO(HttpServletRequest request){
          permissionId = request.getParameter(RQ_PERMISSION_ID);
          description = request.getParameter(RQ_DESCRIPTION);
      }   

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null.
       */
      public boolean isValid() {
          return permissionId != null 
                 && permissionId.length() > 0;
      }     

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if(permissionId == null || permissionId.length() == 0) {
              errorMap.put(RQ_PERMISSION_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          } 
          return errorMap;
      } 

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The permission requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_PERMISSION_ID, "Permission already exists.");
          return errorMap;
      }  
      
      /**
        * Returns field document. FIELDS/FIELD (@fieldname, @value)
        */
      public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field permission_id
            Element permissionIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            permissionIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_PERMISSION_ID);
            permissionIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, permissionId);
            doc.getDocumentElement().appendChild(permissionIdElem);
            
            // Field description
            Element descriptionElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_DESCRIPTION);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, description);
            doc.getDocumentElement().appendChild(descriptionElem);       

            return doc;
      }      
  }