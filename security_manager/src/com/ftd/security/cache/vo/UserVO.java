package com.ftd.security.cache.vo;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
  
  /**
   * Constants that pertain to a User.
   * @author Christy Hu
   */
  public class UserVO extends AbstractVO
  {
      private static Logger logger = new Logger("com.ftd.security.cache.vo.UserVO");
  
      // Statement column input constants for calling SQL procedures.
      
      public static final String ST_LAST_NAME = "in_last_name";
      public static final String ST_FIRST_NAME = "in_first_name";
      public static final String ST_ADDRESS_1 = "in_address_1";
      public static final String ST_ADDRESS_2 = "in_address_2";
      public static final String ST_CITY = "in_city";
      public static final String ST_STATE = "in_state";
      public static final String ST_ZIP = "in_zip";         
      public static final String ST_PHONE = "in_phone";
      public static final String ST_EXP_DATE = "in_expire_date";
      public static final String ST_EMAIL = "in_email";
      public static final String ST_PHONE_EXT = "in_extension";
      public static final String ST_FAX = "in_fax";
      public static final String ST_CALL_CENTER_ID = "in_call_center_id";
      public static final String ST_DEPARTMENT_ID = "in_department_id";
      public static final String ST_CS_GROUP_ID= "in_cs_group_id";
      public static final String ST_USER_ID = "user_id";
      public static final String ST_IN_USER_ID = "in_user_id";
      public static final String ST_IN_IDENTITY_ID = "in_identity_id";
      public static final String ST_IN_EMPLOYEE_ID = "in_employee_id";
	  public static final String ST_IN_SUPERVISOR_ID = "in_supervisor_id";
	  
	  public static final String INVALID_SUPERVISOR_ID = "-99999";

      // Output XML structure.
      public static final String XML_TOP = "USERS";
      public static final String XML_BOTTOM = "USER";

      // Data field that come from the request.
      public static final String RQ_LAST_NAME = "lastName";
      public static final String RQ_FIRST_NAME = "firstName";
   
      public static final String RQ_PHONE = "phone";
      //public static final String RQ_EXP_DATE = "expireDate";
      public static final String RQ_EMAIL = "emailAddress";
      public static final String RQ_PHONE_EXT = "phoneExt";
      public static final String RQ_USER_ID = "user_id";
      public static final String RQ_SEARCH_OPTION = "searchOption";
      public static final String RQ_SEARCH_KEY = "searchKey";
      public static final String RQ_CALL_CENTER = "callCenter";
      public static final String RQ_DEPARTMENT = "department";
      public static final String RQ_CS_GROUP = "csGroup";      
      public static final String RQ_IDENTITYID = "identityId";
      public static final String DEPT_CS = "CS";
      public static final String RQ_EMPLOYEE_ID = "employee_id";
	  public static final String RQ_SUPERVISOR_ID = "supervisor_id";

      // table columns
      public static final String TB_USER_ID = "user_id";
      public static final String TB_LAST_NAME = "last_name";
      public static final String TB_FIRST_NAME = "first_name";
      public static final String TB_PHONE = "phone";   
      public static final String TB_PHONE_EXT = "extension";
      public static final String TB_EMAIL = "email";
      public static final String TB_CALL_CENTER = "call_center_id";
      public static final String TB_DEPARTMENT = "department_id";
      public static final String TB_CS_GROUP = "cs_group_id";
      public static final String TB_CALL_CENTER_DESC = "description";
      public static final String TB_DEPARTMENT_DESC = "description";
      public static final String TB_CS_GROUP_DESC = "description";
      public static final String TB_EMPLOYEE_ID = "employee_id";
	  public static final String TB_SUPERVISOR_ID = "supervisor_id";

      public static final char AT = '@';

      private String firstName;
      private String lastName;
      private String phone;
      private String phoneExt;
      private String email;
      private String callCenter;
      private String department;
      private String csGroup;
      private String userId;
      private String employeeId;
	  private String supervisorId;

      public UserVO(){}
      public UserVO(HttpServletRequest request) throws Exception {
          firstName = request.getParameter(RQ_FIRST_NAME);
          lastName = request.getParameter(RQ_LAST_NAME);
          phone = request.getParameter(RQ_PHONE);
          phoneExt = request.getParameter(RQ_PHONE_EXT);
          email = request.getParameter(RQ_EMAIL);
          callCenter = request.getParameter(RQ_CALL_CENTER);
          department = request.getParameter(RQ_DEPARTMENT);
          csGroup = request.getParameter(RQ_CS_GROUP);   
          userId = request.getParameter(RQ_USER_ID);
          if (userId == null || userId.length() == 0) {
              Integer id = (Integer)request.getAttribute(RQ_USER_ID);
              if (id != null ) {
                  userId = String.valueOf(id.intValue());
              }
          }
          employeeId = request.getParameter(RQ_EMPLOYEE_ID);
          
		  supervisorId = request.getParameter(RQ_SUPERVISOR_ID);
          /*logger.debug("Creating UserVO with: firstName = " + firstName);
          logger.debug("Creating UserVO with: lastName = " + lastName);
          logger.debug("Creating UserVO with: phone = " + phone);
          logger.debug("Creating UserVO with: phoneExt = " + phoneExt);
          logger.debug("Creating UserVO with: email = " + email);
          logger.debug("Creating UserVO with: callCenter = " + callCenter);
          logger.debug("Creating UserVO with: department = " + department);
          logger.debug("Creating UserVO with: csGroup = " + csGroup);
          logger.debug("Creating UserVO with: userId = " + userId);*/       
      }
      public String getUserId() {
          return userId;
      }
      public void setFirstName(String arg0) {
          firstName = arg0;
      }

      public void setLastName(String arg0) {
          lastName = arg0;
      }

      public void setPhone(String arg0) {
          phone = arg0;
      }

      public void setPhoneExt(String arg0) {
          phoneExt = arg0;
      }

      public void setEmail(String arg0) {
          email = arg0;
      }

      public void setCallCenter(String arg0) {
          callCenter = arg0;
      }

      public void setDepartment(String arg0) {
          department = arg0;
      }

      public void setCsGroup(String arg0) {
          csGroup = arg0;
      }      

      public void setEmployeeId(String arg0) {
		  employeeId = arg0;
	  }
      
	  public void setSupervisorId(String arg0) {
		  supervisorId = arg0;
	  }
	  
	  public String getSupervisorId() {
		  return supervisorId;
	  }
	/**
       * Checks if the object is valid. Object is valid if all fields are valid.
       */
      public boolean isValid() {
          if (firstName == null || firstName.length() == 0 || !isValidName(firstName)) {
              return false;
          }
          if (lastName == null || lastName.length() == 0 || !isValidName(lastName)) {
              return false;
          }   
          if (phone != null && phone.length() != 0 && !isValidPhone(phone)) {
              return false;
          }          
          if ((phone == null || phone.length() == 0) 
              && (phoneExt != null && phoneExt.length() > 0) ) {
              return false;
          } else {
              if(phoneExt != null && phoneExt.length() > 0 && !isValidPhone(phoneExt)) {
                  return false;
              }
          }
          if (email != null && email.length() > 0 && !isValidEmail(email)){
              return false;
          }
          if (department == null || department.length() == 0) {
              return false;
          } else if(department.equals(DEPT_CS)) {
              if (callCenter == null 
                  || callCenter.length() == 0 ) {
                    return false;
              }
              if (csGroup == null || csGroup.length() == 0) {
                  return false;
              } 
          }
          
                             
          return true;
      }      
      
      /**
       * Tests if the phone number is valid. Phone number is valid
       * if it is a numric string.
       */
      public boolean isValidPhone(String phone) {
          return StringUtil.isNumricString(phone.trim());
      }

      /**
       * Tests if the email is valid. Email is valid if it contains '@'.
       */
      public boolean isValidEmail(String email) {
          return StringUtil.contains(email.trim(), AT);
      }

      /**
       * Tests if the name string is valid. Name string is valid
       * if it's alphanumeric.
       */
      public boolean isValidName(String name) {
          // Name is not null when calling this function.
          return StringUtil.isAlphaNumeric(name);
      }

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if (firstName == null || firstName.length() == 0) {
              errorMap.put(RQ_FIRST_NAME, "First Name required.");
          } else if(!isValidName(firstName)) {
              errorMap.put(RQ_FIRST_NAME, "First Name invalid.");
          }
          if (lastName == null || lastName.length() == 0 ) {
              errorMap.put(RQ_LAST_NAME, "Last Name required.");
          } else if (!isValidName(lastName)) {
              errorMap.put(RQ_LAST_NAME, "Last Name invalid.");
          }   
          if (phone != null && phone.length() != 0 && !isValidPhone(phone)) {
              errorMap.put(RQ_PHONE, "Phone must be numeric.");
          }          
          if ((phone == null || phone.length() == 0) 
              && (phoneExt != null && phoneExt.length() > 0) ) {
              errorMap.put(RQ_PHONE_EXT, "Phone number required for Extension.");
          } else {
              if(phoneExt != null && phoneExt.length() > 0 && !isValidPhone(phoneExt)) {
                  errorMap.put(RQ_PHONE_EXT, "Phone Extension must be numeric.");
              }
          }
          if (email != null && email.length() > 0 && !isValidEmail(email)) {
              errorMap.put(RQ_EMAIL, "Email address invalid.");
          }          
          if (department == null || department.length() == 0) {
              errorMap.put(RQ_DEPARTMENT, "Department required.");
          } else if(department.equals(DEPT_CS)) {
              if (callCenter == null 
                  || callCenter.length() == 0 ) {
                    errorMap.put(RQ_CALL_CENTER, "Call Center required for Custome Service user.");
                  }
              if (csGroup == null || csGroup.length() == 0) {
                  if (csGroup == null 
                      || csGroup.length() == 0) {
                        errorMap.put(RQ_CS_GROUP, "Group required for Custome Service user.");
                      }
              }        
          }          
          return errorMap;
      }  

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          return null;         
      } 

       /**
        * Returns document for displayAdd. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field firstName
            Element firstNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_FIRST_NAME);
            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, firstName);
            doc.getDocumentElement().appendChild(firstNameElem);

            // Field lastName
            Element lastNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_LAST_NAME);
            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, lastName);
            doc.getDocumentElement().appendChild(lastNameElem);
            
            // Field phone
            Element phoneElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            phoneElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_PHONE);
            phoneElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, phone);
            doc.getDocumentElement().appendChild(phoneElem);       

            // Field phoneExt
            Element phoneExtElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            phoneExtElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_PHONE_EXT);
            phoneExtElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, phoneExt);
            doc.getDocumentElement().appendChild(phoneExtElem); 

            // Field email
            Element emailElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            emailElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_EMAIL);
            emailElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, email);
            doc.getDocumentElement().appendChild(emailElem);  
            
            // Field callCenter
            Element callCenterElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            callCenterElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CALL_CENTER);
            callCenterElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, callCenter);
            doc.getDocumentElement().appendChild(callCenterElem);   
           
            // Field department
            Element departmentElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            departmentElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_DEPARTMENT);
            departmentElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, department);
            doc.getDocumentElement().appendChild(departmentElem);    
           
            // Field csGroup
            Element csGroupElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            csGroupElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CS_GROUP);
            csGroupElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, csGroup);
            doc.getDocumentElement().appendChild(csGroupElem);       

            // Field external user id
            Element employeeIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            employeeIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_EMPLOYEE_ID);
            employeeIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, employeeId);
            doc.getDocumentElement().appendChild(employeeIdElem);  

			// Field external user id
            Element supervisorIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            supervisorIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_SUPERVISOR_ID);
            supervisorIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, supervisorId);
            doc.getDocumentElement().appendChild(supervisorIdElem);  

            return doc;
        }

     /**
      * Returns a HashMap to add user.
      */
     public HashMap getAddUserInputParams(Object securityToken)
      throws SecurityAdminException, Exception
     {
          HashMap inputParams = new HashMap();
          try{  
            inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
            inputParams.put(ST_LAST_NAME, lastName);
            inputParams.put(ST_FIRST_NAME, firstName);
            inputParams.put(ST_ADDRESS_1, "");
            inputParams.put(ST_ADDRESS_2, "");
            inputParams.put(ST_CITY, "");
            inputParams.put(ST_STATE, "");
            inputParams.put(ST_ZIP, "");
            inputParams.put(ST_PHONE, phone);
            inputParams.put(ST_EXP_DATE, Integer.valueOf(String.valueOf(java.sql.Types.DATE)));
            inputParams.put(ST_EMAIL, email);
            inputParams.put(ST_FAX, "");
            inputParams.put(ST_PHONE_EXT, phoneExt);
            inputParams.put(ST_DEPARTMENT_ID, department);
            inputParams.put(ST_CALL_CENTER_ID, callCenter);
            inputParams.put(ST_CS_GROUP_ID, csGroup);
            inputParams.put(ST_IN_EMPLOYEE_ID, employeeId);
			inputParams.put(ST_IN_SUPERVISOR_ID, supervisorId);
            
            // Set user_id to null, providing the type.
            inputParams.put(UserVO.ST_USER_ID, Integer.valueOf(String.valueOf(java.sql.Types.INTEGER)));
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }          
          return inputParams;
     }

     /**
      * Returns a HashMap to add user.
      */
     public HashMap getUpdateUserInputParams(Object securityToken)
      throws SecurityAdminException, Exception
     {
          HashMap inputParams = new HashMap();
          try{  
            inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
            inputParams.put(ST_LAST_NAME, lastName);
            inputParams.put(ST_FIRST_NAME, firstName);
            inputParams.put(ST_ADDRESS_1, "");
            inputParams.put(ST_ADDRESS_2, "");
            inputParams.put(ST_CITY, "");
            inputParams.put(ST_STATE, "");
            inputParams.put(ST_ZIP, "");
            inputParams.put(ST_PHONE, phone);
            inputParams.put(ST_EXP_DATE, Integer.valueOf(String.valueOf(java.sql.Types.DATE)));
            inputParams.put(ST_EMAIL, email);
            inputParams.put(ST_FAX, "");
            inputParams.put(ST_PHONE_EXT, phoneExt);
            inputParams.put(ST_DEPARTMENT_ID, department);
            inputParams.put(ST_CALL_CENTER_ID, callCenter);
            inputParams.put(ST_CS_GROUP_ID, csGroup);
            inputParams.put(ST_IN_EMPLOYEE_ID, employeeId);
			inputParams.put(ST_IN_SUPERVISOR_ID, supervisorId);
            
            // Set user_id to null, providing the type.
            inputParams.put(UserVO.ST_USER_ID, Integer.valueOf(userId));

            /*logger.debug("Updating UserVO with: firstName = " + firstName);
            logger.debug("Updating UserVO with: lastName = " + lastName);
            logger.debug("Updating UserVO with: phone = " + phone);
            logger.debug("Updating UserVO with: phoneExt = " + phoneExt);
            logger.debug("Updating UserVO with: email = " + email);
            logger.debug("Updating UserVO with: callCenter = " + callCenter);
            logger.debug("Updating UserVO with: department = " + department);
            logger.debug("Updating UserVO with: csGroup = " + csGroup);
            logger.debug("Updating UserVO with: userId = " + userId);*/            
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }          
          return inputParams;
     }
     
  }