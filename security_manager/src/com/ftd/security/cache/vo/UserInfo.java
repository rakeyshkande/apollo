package com.ftd.security.cache.vo;
import java.io.Serializable;

import java.lang.reflect.Field;

/**
 * Represents the information for a user
 * 
 * @author Anshu Gaind
 */
public class UserInfo  implements Serializable{

  private String userID;
  private String lastName;
  private String firstName;
  private String address1;
  private String city;
  private String state;
  private String zip;
  private String phone;
  private String expireDate;
  private String email;
  private String extension;
  private String fax;
  private String callCenter = "FTDP";// remove later
  private String department;
  private String customerServiceGroup;

  /**
   * Default Constructor
   */
  public UserInfo() {
  }

  public String getUserID() {
    return userID;
  }

  public void setUserID(String newUserID) {
    userID = newUserID;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String newLastName) {
    lastName = newLastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String newFirstName) {
    firstName = newFirstName;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String newAddress1) {
    address1 = newAddress1;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String newCity) {
    city = newCity;
  }

  public String getState() {
    return state;
  }

  public void setState(String newState) {
    state = newState;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String newZip) {
    zip = newZip;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String newPhone) {
    phone = newPhone;
  }

  public String getExpireDate() {
    return expireDate;
  }

  public void setExpireDate(String newExpireDate) {
    expireDate = newExpireDate;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String newEmail) {
    email = newEmail;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String newExtension) {
    extension = newExtension;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String newFax) {
    fax = newFax;
  }

  public String getCallCenter() {
    return callCenter;
  }

  public void setCallCenter(String newCallCenter) {
    callCenter = newCallCenter;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String newDepartment) {
    department = newDepartment;
  }

  public String getCustomerServiceGroup() {
    return customerServiceGroup;
  }

  public void setCustomerServiceGroup(String newCustomerServiceGroup) {
    customerServiceGroup = newCustomerServiceGroup;
  }
  
  public String toXML()
  {
    StringBuffer sb = new StringBuffer();
    try 
    {
      sb.append("<user-info>");
      
      Field[] fields = this.getClass().getDeclaredFields();
      
      for (int i = 0; i < fields.length; i++) 
      {        
        sb.append("<" + fields[i].getName() + ">");
        sb.append(fields[i].get(this));
        sb.append("</" + fields[i].getName() + ">");
      }
      sb.append("</user-info>");
      
    } catch (Exception ex) 
    {
      // do nothing     
    }
    
    return sb.toString();
  }
  
}