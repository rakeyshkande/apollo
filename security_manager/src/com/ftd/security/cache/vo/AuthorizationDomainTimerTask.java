package com.ftd.security.cache.vo;
import java.util.*;

public class AuthorizationDomainTimerTask  extends TimerTask{
  /**
   * Constructor
   */
  public AuthorizationDomainTimerTask() {
    super();
  }

  /**
   * The action to be performed by this timer task.
   */
  public void run(){
      System.out.println(new Date() + ":: authorization cache refreshed");
  }

}