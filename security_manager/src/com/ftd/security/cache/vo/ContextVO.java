package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
  /**
   * Constants that pertain to a Context.
   * @author Christy Hu
   */
  public class ContextVO extends AbstractVO
  {
      // Statement column input constants for calling SQL procedures.
      public static final String ST_CONTEXT_ID = "in_context_id";
      public static final String ST_DESCRIPTION = "in_description";
      public static final String ST_CONFIG_NAME = "in_name";
      public static final String ST_CONFIG_VALUE = "in_value";

      // Output XML structure.
      public static final String XML_TOP = "CONTEXTS";
      public static final String XML_BOTTOM = "CONTEXT";
      
      // Data field that come from the request.
      public static final String RQ_CONTEXT_ID = "context_id";
      public static final String RQ_DESCRIPTION = "description";
      public static final String RQ_REMOVE_CONTEXT_ID = "contextId";
      public static final String RQ_CONFIG_NAME = "config_name";
      public static final String RQ_CONFIG_VALUE = "value";
      public static final String RQ_REMOVE_CONFIG_CB = "removeConfigCB";
      
      // Table columns (xml tag name)
      public static final String TB_CONTEXT_ID = "context_id";

      private String contextId;
      private String description;
      
      public ContextVO(){}

      public ContextVO(HttpServletRequest request){
          contextId = request.getParameter(RQ_CONTEXT_ID);
          description = request.getParameter(RQ_DESCRIPTION);
      }

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null.
       */
      public boolean isValid() {
          return contextId != null
                 && contextId.length() >0;
      }   

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();

          if(contextId == null || contextId.length() == 0) {
              errorMap.put(RQ_CONTEXT_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          return errorMap;
      }      

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The context requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_CONTEXT_ID, "Context already exists.");
          return errorMap;
      }      

       /**
        * Returns field document. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field context_id
            Element contextIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CONTEXT_ID);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, contextId);
            doc.getDocumentElement().appendChild(contextIdElem);
            
            // Field description
            Element descriptionElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_DESCRIPTION);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, description);
            doc.getDocumentElement().appendChild(descriptionElem);       

            return doc;
        }      
  }