package com.ftd.security.cache.vo;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Representation of a User line item to be loaded from Excel spreadsheet.
 * @author Christy Hu
 */
public class UserLineItemVO extends LineItemVO
{
    // attributes common to all instances.
    private final static int COLUMNS = 12;
    public final static String FIRST_NAME = "First Name";
    public final static String LAST_NAME = "Last Name";
    //public final static String ADDRESS_ONE = "Address 1";
    //public final static String ADDRESS_TWO = "Address 2";
    //public final static String CITY = "City";
    //public final static String STATE = "State";
    //public final static String ZIP = "Zip";
    //public final static String PHONE = "Phone";
    //public final static String PHONE_EXT = "Phone Ext";
    //public final static String FAX = "Fax";
    //public final static String EMAIL = "Email";
    public final static String IDENTITY_ID = "Userid";
    public final static String EXP_DATE = "ID Expiration Date";  
    public final static String DEPARTMENT = "Department";
    public final static String CS_GROUP = "CS Group";
    public final static String CALL_CENTER = "Call Center";
    public final static String ROLE = "Role";

    public final static String EMAIL = "Email Id";
    public final static String EMPLOYEE_ID = "Employee Id";
    public final static String SUPERVISOR_ID = "Supervisor Id";
    
      // Output XML structure.
      public static final String XML_TOP = "USERS";
      public static final String XML_BOTTOM = "USER";

      public static final String RQ_LAST_NAME = "last_name";
      public static final String RQ_FIRST_NAME = "first_name";
      public static final String RQ_IDENTITY_ID = "identity_id";
      public static final String RQ_CS_GROUP = "cs_group";
      public static final String RQ_CREDENTIALS = "credentials";

    // attribute to each instance
    // fieldList inherited from parent.
    private int userId;
    private String password;

    /**
     * Initialize the list of Field. Specifies the Field name, type, 
     * column position, max size, and if it's a required field.
     */
    public UserLineItemVO() 
    {
        // create fieldList if it's null.
        super();
        fieldList.add(new Field(FIRST_NAME, Field.TYPE_STRING, 0, 20, true));
        fieldList.add(new Field(LAST_NAME, Field.TYPE_STRING, 1, 20, true));
        //fieldList.add(new Field(ADDRESS_ONE, Field.TYPE_STRING, 2, 4000, false));
        //fieldList.add(new Field(ADDRESS_TWO, Field.TYPE_STRING, 3, 4000, false));
        //fieldList.add(new Field(CITY, Field.TYPE_STRING, 4, 255, false));
        //fieldList.add(new Field(STATE, Field.TYPE_STRING, 5, 2, false));
        //fieldList.add(new Field(ZIP, Field.TYPE_STRING, 6, 20, false));
        //fieldList.add(new Field(PHONE, Field.TYPE_STRING, 7, 40, false));
        //fieldList.add(new Field(PHONE_EXT, Field.TYPE_STRING, 8, 40, false));
        //fieldList.add(new Field(FAX, Field.TYPE_STRING, 9, 40, false));
        //fieldList.add(new Field(EMAIL, Field.TYPE_STRING, 10, 100, false));
        fieldList.add(new Field(IDENTITY_ID, Field.TYPE_STRING, 2, 20, true));
        fieldList.add(new Field(EXP_DATE, Field.TYPE_DATE, 3, 10, true));  
        fieldList.add(new Field(DEPARTMENT, Field.TYPE_STRING, 4, 50, true));
        fieldList.add(new Field(CS_GROUP, Field.TYPE_STRING, 5, 50, false));
        fieldList.add(new Field(CALL_CENTER, Field.TYPE_STRING, 6, 50, false));
        fieldList.add(new Field(ROLE, Field.TYPE_STRING, 7, 500, false));
        fieldList.add(new Field(EMAIL, Field.TYPE_STRING, 8, 100, false));
        fieldList.add(new Field(EMPLOYEE_ID, Field.TYPE_STRING, 9, 100, false));
        fieldList.add(new Field(SUPERVISOR_ID, Field.TYPE_STRING, 10, 100, false));
    }
    
    public void validate() throws Exception {
        throw new UnsupportedOperationException("validate: method not implemented.");
    }

    public static void init() {
        LineItemVO.setColumnSize(COLUMNS);
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int arg0) {
        userId = arg0;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String arg0) {
        password = arg0;
    }

       /**
        * Returns document for displayAdd. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field firstName
            Element firstNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_FIRST_NAME);
            firstNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
                                       this.getFieldByName(FIRST_NAME).getValue());
            doc.getDocumentElement().appendChild(firstNameElem);

            // Field lastName
            Element lastNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_LAST_NAME);
            lastNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
                                      this.getFieldByName(LAST_NAME).getValue());
            doc.getDocumentElement().appendChild(lastNameElem);
            
            // Field lastName
            Element identityElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            identityElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_IDENTITY_ID);
            identityElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
                                      this.getFieldByName(IDENTITY_ID).getValue());
            doc.getDocumentElement().appendChild(identityElem);
            
            Element csGroup = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            csGroup.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CS_GROUP);
            csGroup.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, 
                                      this.getFieldByName(CS_GROUP).getValue());
            doc.getDocumentElement().appendChild(csGroup);
            
            // Credentials
            Element credentialElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            credentialElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CREDENTIALS);
            credentialElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, getPassword());
            doc.getDocumentElement().appendChild(credentialElem);
            
            return doc;
        }
}