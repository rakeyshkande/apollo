package com.ftd.security.cache.vo;
import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;

public class Role implements Serializable {
  private String name;
  private Set ACLS;

  public Role() {
  }

  public String getName() {
    return name;
  }

  public void setName(String newName) {
    name = newName;
  }

  public Set getACLS() {
    return ACLS;
  }

  public void setACLS(Set newACLS) {
    ACLS = newACLS;
  }

  public void setACL(Object ACL) {
    if (ACLS == null)  {
        ACLS = new HashSet();
    }
    
    ACLS.add(ACL);
  }

}