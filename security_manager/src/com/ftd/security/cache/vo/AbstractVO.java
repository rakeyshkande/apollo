package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.RequiredFieldMissingException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import org.w3c.dom.Document;
/**
 * Representation of a value object.
 * @autor Christy Hu
 */
public abstract class AbstractVO {

      public static final String ST_UPDATED_BY = "in_updated_by";
      
      public abstract boolean isValid();
      protected abstract HashMap getMissingRequiredFieldErrorMap() throws Exception;
      protected abstract HashMap getDuplicateObjectErrorMap() throws Exception;
      public abstract Document getFieldDoc() throws Exception;
       
      /**
       * Rebuilds XML tree when a request cannot be processed due to invalid input.
       */
      public Document rebuildTree(Exception e) throws Exception {
          HashMap errorMap = null;
          if (e instanceof RequiredFieldMissingException) {
              errorMap = this.getMissingRequiredFieldErrorMap();
          } else if (e instanceof DuplicateObjectException) {
              errorMap = this.getDuplicateObjectErrorMap();
          } else {
              throw new SecurityAdminException("1");
          }
          Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
          Document errorDoc = XMLHelper.getErrorDocumentFromMap(errorMap);
          Document fieldDoc = getFieldDoc();

          // Add error branch to root element. ERRORS/ERROR(@fieldname, @description)
          if(errorDoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(errorDoc.getDocumentElement(), true));
          }

          // Add field branch to root element. FIELDS/FIELDS(@fieldname, @value)
          if(fieldDoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(fieldDoc.getDocumentElement(), true));
          }
          return doc;
      }


}