package com.ftd.security.cache.vo;
import java.util.*;

public class AuthenticationDomainTimerTask  extends TimerTask{
  /**
   * Constructor
   */
  public AuthenticationDomainTimerTask() {
    super();
  }

  /**
   * The action to be performed by this timer task.
   */
  public void run(){
      System.out.println( new Date()+ ":: authentication cache refreshed");
  }

}