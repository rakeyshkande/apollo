package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


  /**
   * Constants that pertain to a Role.
   * @author Christy Hu
   */
  public class RoleVO extends AbstractVO
  {
      // Statement column input constants for calling SQL procedures.
      public static final String ST_CONTEXT_ID = "in_context_id";
      public static final String ST_ROLE_NAME = "in_role_name";
      public static final String ST_DESCRIPTION = "in_description";
      public static final String ST_ROLE_ID = "role_id";
      public static final String ST_IN_ROLE_ID = "in_role_id";
      public static final String ST_ACL_NAME = "in_acl_name";

      // Output XML structure.
      public static final String XML_TOP = "ROLES";
      public static final String XML_BOTTOM = "ROLE";

      // Data field that come from the request.
      public static final String RQ_ROLE_NAME = "role_name";
      public static final String RQ_CONTEXT_ID = "context_id";
      public static final String RQ_DESCRIPTION = "description";
      public static final String RQ_ROLE_IDS = "role_ids";
      public static final String RQ_ROLE_ID = "role_id";
      public static final String RQ_REMOVE_ROLE_ID = "removeRoleId";
      public static final String RQ_ADD_ACL_NAME = "addAclName";
      public static final String RQ_REMOVE_ACL_NAME = "removeAclName";
      public static final String RQ_ACL_NUMBERS = "acl_numbers";

      // Table columns
      public static final String TB_ROLE_NAME = "role_name";
      public static final String TB_ROLE_ID = "role_id";

      private String roleName;
      private String contextId;
      private String description;

      public RoleVO(){}

      public RoleVO(HttpServletRequest request){
          roleName = request.getParameter(RQ_ROLE_NAME);
          contextId = request.getParameter(RQ_CONTEXT_ID);
          description = request.getParameter(RQ_DESCRIPTION);
      }

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null.
       */
      public boolean isValid() {
          return roleName != null 
                 && roleName.length() > 0
                 && contextId != null
                 && contextId.length() >0;
      }

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if(roleName == null || roleName.length() == 0) {
              errorMap.put(RQ_ROLE_NAME, SecurityAdminConstants.ERROR_ENTER_VALUE);
          } 
          if(contextId == null || contextId.length() == 0) {
              errorMap.put(RQ_CONTEXT_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          return errorMap;
      }  


      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The role requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_ROLE_NAME, "Role name already exists in this context.");
          return errorMap;
      } 

       /**
        * Returns field document. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field role_name
            Element roleNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            roleNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_ROLE_NAME);
            roleNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, roleName);
            doc.getDocumentElement().appendChild(roleNameElem);

            // Field context_id
            Element contextIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CONTEXT_ID);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, contextId);
            doc.getDocumentElement().appendChild(contextIdElem);
            
            // Field description
            Element descriptionElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_DESCRIPTION);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, description);
            doc.getDocumentElement().appendChild(descriptionElem);       

            return doc;
        }
  }