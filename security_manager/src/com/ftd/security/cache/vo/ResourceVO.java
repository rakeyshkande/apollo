package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
  /**
   * Constants that pertain to a Resource.
   * @author Christy Hu
   */
  public class ResourceVO extends AbstractVO
  {
      // Statement column input constants for calling SQL procedures.
      public static final String ST_CONTEXT_ID = "in_context_id";
      public static final String ST_RESOURCE_ID = "in_resource_id";
      public static final String ST_DESCRIPTION = "in_description";
 
      // Output XML structure.
      public static final String XML_TOP = "RESOURCES";
      public static final String XML_BOTTOM = "RESOURCE";

      // Data field that come from the request.
      public static final String RQ_RESOURCE_ID = "resource_id";
      public static final String RQ_CONTEXT_ID = "context_id";
      public static final String RQ_DESCRIPTION = "description";
      // remove
      public static final String RQ_REMOVE_RESOURCE_ID = "resourceId";
      public static final String RQ_REMOVE_CONTEXT_ID = "contextId";

      // Table columns (xml tag name)
      public static final String TB_CONTEXT_ID = "context_id"; 
      public static final String TB_RESOURCE_ID = "resource_id";      

      private String resourceId;
      private String contextId;
      private String description;   

      public ResourceVO(){}

      public ResourceVO(HttpServletRequest request){
          resourceId = request.getParameter(RQ_RESOURCE_ID);
          contextId = request.getParameter(RQ_CONTEXT_ID);
          description = request.getParameter(RQ_DESCRIPTION);
      }   

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null.
       */
      public boolean isValid() {
          return resourceId != null 
                 && resourceId.length() > 0
                 && contextId != null
                 && contextId.length() >0;
      }  

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if(resourceId == null || resourceId.length() == 0) {
              errorMap.put(RQ_RESOURCE_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          if(contextId == null || contextId.length() == 0) {
              errorMap.put(RQ_CONTEXT_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          return errorMap;
      } 

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The context requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_RESOURCE_ID, "Resource already exists in this context.");
          return errorMap;
      } 

       /**
        * Returns field document. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field resource_id
            Element resourceIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            resourceIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_RESOURCE_ID);
            resourceIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, resourceId);
            doc.getDocumentElement().appendChild(resourceIdElem);

            // Field context_id
            Element contextIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CONTEXT_ID);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, contextId);
            doc.getDocumentElement().appendChild(contextIdElem);
            
            // Field description
            Element descriptionElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_DESCRIPTION);
            descriptionElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, description);
            doc.getDocumentElement().appendChild(descriptionElem);       

            return doc;
        }       
  }