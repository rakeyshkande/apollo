package com.ftd.security.cache.vo;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.ServletHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
  /**
   * Constants that pertain to an identity.
   * @author Christy Hu
   */
  public class IdentityVO extends AbstractVO
  {
      private static Logger logger = new Logger("com.ftd.security.cache.vo.IdentityVO");
      // Statement column input constants for calling SQL procedures.
      public static final String ST_IDENTITY_ID = "in_identity_id";
      public static final String ST_LOCKED = "in_locked_flag";
      
      public static final String RQ_IDENTITYID = "identityId";
      public static final String RQ_CALL_CENTER = "callCenter";
      public static final String RQ_DEPARTMENT = "department";
      public static final String RQ_CS_GROUP = "csGroup";
      public static final String RQ_LOCKED = "locked";
      public static final String RQ_CONTEXT_ID = "contextId";
      public static final String RQ_EXP_DATE = "expireDate";
      public static final String RQ_PWD_EXP_DATE = "pwdExpireDate";
      public static final String RQ_ROLES = "roles";

      // table column
      public static final String TB_IDENTITY_ID = "identity_id";
      public static final String TB_ID_EXPIRE_DATE = "identity_expire_date";
      public static final String TB_PWD_EXPIRE_DATE = "credentials_expire_date";
      public static final String TB_LOCKED_FLAG = "locked_flag";
      
      // Output XML structure.
      public static final String XML_TOP = "IDENTITIES";
      public static final String XML_BOTTOM = "IDENTITY";      

      private String identityId;
      private String idExpDate;      
      private String pwdExpDate;
      //private java.util.Date dateIdExpDate; // merge with string var when time allows
      //private java.util.Date datePwdExpDate;
      private String locked;
      private String contextId;
      private String[] roleIdList;  // selected roles

      public IdentityVO() {}
      public IdentityVO(HttpServletRequest request) {
          identityId = request.getParameter(RQ_IDENTITYID);
          idExpDate = request.getParameter(RQ_EXP_DATE);
          pwdExpDate = request.getParameter(RQ_PWD_EXP_DATE);
          locked = request.getParameter(RQ_LOCKED);
          contextId = request.getParameter(RQ_CONTEXT_ID);    
          roleIdList = request.getParameterValues(RQ_ROLES);    
          /*logger.debug("Creating IdentityVO with: identityId = " + identityId);
          logger.debug("Creating IdentityVO with: idExpDate = " + idExpDate);
          logger.debug("Creating IdentityVO with: pwdExpDate = " + pwdExpDate);
          logger.debug("Creating IdentityVO with: locked = " + locked);
          logger.debug("Creating IdentityVO with: contextId = " + contextId);
          logger.debug("Creating IdentityVO with: roleIdList = " + roleIdList);*/           
      }
      
      public String getIdentityId() {
          return identityId;
      }

      public String getIdExpDate() {
          return idExpDate;
      }

      public String getLockedFlag() {
          return locked;
      }      

      public String getContextId() {
          return contextId;
      }

      public String[] getRoleIds() {
          return roleIdList;
      }      
      public void setRoleIdList(String[] arg0) {
          roleIdList = arg0;
      }

      public void setIdentityId(String arg0) {
          identityId = arg0;
      }

      public void setIdExpDate(String arg0) {
          idExpDate = arg0;
      }
      
      public void setContextId(String arg0) {
          contextId = arg0;
      }   
      public void setPwdExpDate(String arg0) {
          pwdExpDate = arg0;
      }       

      public void setLockedFlag(String arg0) {
          locked = arg0;
      }       
      /**
       * Checks if the object is valid. Object is valid if all fields are valid.
       */
      public boolean isValid() {
      /*
          if (identityId == null || identityId.length() == 0 
              || identityId.length() < 6 || !isValidName(identityId)) {
              return false;
          }   
        */
          if (idExpDate == null || idExpDate.length() == 0 
              || !StringUtil.isValidDateString(idExpDate)) {
              return false;
          } 
          
          if (roleIdList != null && roleIdList.length > 0) {
              if ( contextId == null || contextId.length() == 0) {
                  return false;
              }
          }
          if (locked == null || locked.length() == 0) {
              return false;
          }          
          return true;
      }

      /**
       * Checks if the object is valid. Object is valid if all fields are valid.
       * Do not validate identity id if action is not add. This allows existing
       * identities that do not comply with policy to be valid.
       */
      public boolean isValid(String action) {
        if (action.equalsIgnoreCase(SecurityAdminConstants.ACTION_PARM_ADD)) {
          if (identityId == null || identityId.length() == 0 
              || identityId.length() < 6 || !isValidName(identityId)) {
              return false;
          }
        }
        return isValid();
      }
      /**
       * Tests if the name string is valid. Name string is valid
       * if it's alphanumeric.
       */
      public boolean isValidName(String name) {
          // Name is not null when calling this function.
          return StringUtil.isAlphaNumeric(name);
      }  

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if (identityId == null || identityId.length() == 0) {
              errorMap.put(RQ_IDENTITYID, "User ID required.");
          } else if (identityId.length() < 6 ) {
              errorMap.put(RQ_IDENTITYID, "User ID must be at least 6 characters long.");
          } else if (!isValidName(identityId)) {
              errorMap.put(RQ_IDENTITYID, "User ID invalid. Must be alphanumeric.");
          }    
          if (locked == null || locked.length() == 0) {
              errorMap.put(RQ_LOCKED, "Locked required.");
          }    
          if (idExpDate == null || idExpDate.length() == 0) {
              errorMap.put(RQ_EXP_DATE, "ID Expire Date required.");
          } else if(!StringUtil.isValidDateString(idExpDate)) {
              errorMap.put(RQ_EXP_DATE, "Invalid date.");
          }
          
          if (roleIdList != null && roleIdList.length > 0) {
              if ( contextId == null || contextId.length() == 0) {
                  errorMap.put(RQ_ROLES, "Context required for role.");
              }
          }         
          return errorMap;
      }

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The role requested to add already exists.
          
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_IDENTITYID, "User ID already exists.");
          return errorMap;
          
      }     

       /**
        * Returns field document. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception 
        {
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field identityId
            Element identityIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            identityIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_IDENTITYID);
            identityIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, identityId);
            doc.getDocumentElement().appendChild(identityIdElem); 

            // Field idExpDate
            Element idExpDateElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            idExpDateElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_EXP_DATE);
            idExpDateElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, idExpDate);
            doc.getDocumentElement().appendChild(idExpDateElem);   

            // Field pwdExpDate
            Element pwdExpDateElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            pwdExpDateElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_PWD_EXP_DATE);
            pwdExpDateElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, pwdExpDate);
            doc.getDocumentElement().appendChild(pwdExpDateElem);
            
            // Field locked
            Element lockedElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            lockedElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_LOCKED);
            lockedElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, locked);
            doc.getDocumentElement().appendChild(lockedElem);    

            // Field contextId
            Element contextIdElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_CONTEXT_ID);
            contextIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, contextId);
            doc.getDocumentElement().appendChild(contextIdElem);   
            
            return doc;
        }
  

 /**
  * Marks the selected roles on document. Set 'selected' attribute='Y' if the role is selected in the request.
  * @param request HttpServletRequest
  * @param doc the role document
  * @throws Exception
  */
  public Document markSelectedRoles(Document doc) 
  throws Exception {

      // Field roleIdList
      if (roleIdList != null && roleIdList.length > 0) {
          for (int i = 0; i < roleIdList.length; i++ ) {
              Element roleIdElem = XMLHelper.findElementByChild
                  (doc, RoleVO.TB_ROLE_ID, roleIdList[i]);
              // Find the context element with the same value and mark 'selected' attribute.
              if (roleIdElem != null) {
                  roleIdElem.setAttribute(SecurityAdminConstants.XML_ATTR_SELECTED, SecurityAdminConstants.COMMON_VALUE_YES);
              }    
          }
      }
     
      return doc;
  }     

  /**
   * Builds the HashMap to check if identity exists.
   */
  public HashMap getIdentityExistsInputParams() throws Exception {
      HashMap inputParams = new HashMap(); 
      inputParams.put(ST_IDENTITY_ID, identityId); 
      logger.debug("In getIdentityExistsInputParams()...");
      logger.debug("Setting in_identity_id to: " + identityId);
      return inputParams;
  }


     /**
      * Returns a HashMap to add identity.
      */
     public HashMap getAddIdentityInputParams(Object securityToken, int userId)
      throws SecurityAdminException, Exception
     {
          HashMap inputParams = new HashMap();
          try{  
              /*logger.debug("Setting " + ST_UPDATED_BY + "to: " + ServletHelper.getMyId(securityToken));
              logger.debug("Setting " + ST_IDENTITY_ID + "to: " + identityId);   
              logger.debug("Setting " + ST_USER_ID + "to: " + String.valueOf(userId));
              logger.debug("Setting " + ST_DESCRIPTION + "to: ");
              logger.debug("Setting " + ST_IDENTITY_EXP_DATE + "to: " + idExpDate);
              logger.debug("Setting " + ST_LOCKED + "to: " + locked);*/                       
              inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
              inputParams.put(ST_IDENTITY_ID, identityId);   
              inputParams.put(ST_USER_ID, new Integer(userId));
              inputParams.put(ST_DESCRIPTION, "");
              inputParams.put(ST_IDENTITY_EXP_DATE, StringUtil.toSqlDate(idExpDate));
              inputParams.put(ST_LOCKED, locked);
              return inputParams;           
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }          
         
     }


     /**
      * Returns a HashMap to reset password by admin.
      */
     public HashMap getAdminResetPasswordInputParams(Object securityToken)
      throws Exception
     {
          HashMap inputParams = new HashMap();
          String adminReset = SecurityAdminConstants.COMMON_VALUE_YES;
          /*logger.debug("Setting " + ST_UPDATED_BY + "to: " + ServletHelper.getMyId(securityToken));
          logger.debug("Setting " + IdentityVO.ST_IDENTITY_ID + "to: " + identityId);   
          logger.debug("Setting " + IdentityVO.ST_OLD_CREDENTIALS + "to: null");
          logger.debug("Setting " + IdentityVO.ST_NEW_CREDENTIALS + "to: null");
          logger.debug("Setting " + IdentityVO.ST_NEW_RECREDENTIALS + "to: null");
          logger.debug("Setting " + IdentityVO.ST_ADMIN_RESET + "to: " + adminReset);*/

          inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
          inputParams.put(ST_IDENTITY_ID, identityId);   
          inputParams.put(ST_OLD_CREDENTIALS, null);
          inputParams.put(ST_NEW_CREDENTIALS, null);
          inputParams.put(ST_NEW_RECREDENTIALS, null);
          inputParams.put(ST_ADMIN_RESET, adminReset);
          return inputParams;                  
     }

     /**
      * Returns a HashMap to add identity role.
      */
     public HashMap getAddIdentityRoleInputParams(Object securityToken, String roleId)
      throws SecurityAdminException, Exception
     {
          HashMap inputParams = new HashMap();
          try{  
              /*logger.debug("Setting " + ST_UPDATED_BY + "to: " + ServletHelper.getMyId(securityToken));
              logger.debug("Setting " + ST_IDENTITY_ID + "to: " + identityId);  
              logger.debug("Setting " + ST_ROLE_ID + "to: " + roleId);*/
          
              inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
              inputParams.put(ST_IDENTITY_ID, identityId);  
              inputParams.put(ST_ROLE_ID, new Long(roleId));
              return inputParams;           
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }                   
     }  
     /**
      * Returns a HashMap to remove identity role.
      */
     public HashMap getRemoveIdentityRoleInputParams(Object securityToken, String roleId)
      throws SecurityAdminException, Exception
     {
          HashMap inputParams = new HashMap();
          try{  
              /*logger.debug("Setting in_identity_id to: " + identityId);
              logger.debug("Setting in_role_id to: " + roleId);*/
          
              //inputParams.put(ST_UPDATED_BY, ServletHelper.getMyId(securityToken));
              inputParams.put(ST_IDENTITY_ID, identityId);  
              inputParams.put(ST_ROLE_ID, new Long(roleId));
              return inputParams;           
          } catch (Exception ex) {
            logger.error(ex);
            throw new SecurityAdminException("1");
          }                   
     }
     
      // Statement column input constants for calling SQL procedures.
      //public static final String ST_IDENTITY_ID = "in_identity_id";
      public static final String ST_OLD_CREDENTIALS = "in_old_credentials";
      public static final String ST_NEW_CREDENTIALS = "in_new_credentials";
      public static final String ST_NEW_RECREDENTIALS = "in_new_recredentials";
      public static final String ST_USER_ID = "in_user_id";
      public static final String ST_DESCRIPTION = "in_description";
      public static final String ST_IDENTITY_EXP_DATE = "in_identity_expire_date";
      public static final String ST_CREDENTIALS_EXP_DATE = "in_credentials_expire_date";
      public static final String ST_ROLE_ID = "in_role_id";
      public static final String ST_GROUP_ID = "in_group_id";
      public static final String ST_ADMIN_RESET = "in_admin_reset";
      // Output XML structure.
      //public static final String XML_TOP = "IDENTITIES";
      //public static final String XML_BOTTOM = "IDENTITY";

      // Data field that come from the request.
      // Code improvement when time allows: consolidate data fields in XSL
      // in different use cases to use same string name for the same field.
      // add
      public static final String RQ_ADD_IDENTITY_ID = "newIdentityId";
      public static final String RQ_ADD_NEW_CREDENTIALS = "newCredentials";
      public static final String RQ_ADD_NEW_RECREDENTIALS = "newReCredentials";
      public static final String RQ_ADD_USER_ID = "user_id";
      public static final String RQ_ADD_DESCRIPTION = "newDescription";
      public static final String RQ_ADD_IDENTITY_EXP_DATE = "newIdentityExpDate";
      public static final String RQ_ADD_CREDENTIALS_EXP_DATE = "newCredentialsExpDate";
      // update
      public static final String RQ_UPDATE_IDENTITY_ID = "identityId";
      public static final String RQ_UPDATE_CREDENTIALS = "credentials";
      public static final String RQ_UPDATE_RECREDENTIALS = "recredentials";
      public static final String RQ_UPDATE_USER_ID = "user_id";
      public static final String RQ_UPDATE_DESCRIPTION = "description";
      public static final String RQ_UPDATE_IDENTITY_EXP_DATE = "identityExpDate";
      public static final String RQ_UPDATE_CREDENTIALS_EXP_DATE = "credentialsExpDate";      
      // user manage account
      public static final String RQ_ACCOUNT_IDENTITY_ID = "identity";
      public static final String RQ_ACCOUNT_OLDCREDENTIALS = "oldCredentials";
      public static final String RQ_ACCOUNT_NEWCREDENTIALS = "newCredentials";
      public static final String RQ_ACCOUNT_NEWRECREDENTIALS = "newRecredentials";
      //public static final String RQ_ACCOUNT_USER_ID = "user_id";
      //public static final String RQ_ACCOUNT_DESCRIPTION = "description";
      //public static final String RQ_ACCOUNT_IDENTITY_EXP_DATE = "identityExpDate";
      //public static final String RQ_ACCOUNT_CREDENTIALS_EXP_DATE = "credentialsExpDate";           

      // data fields related to identity role
      public static final String RQ_ROLE_IDENTITY_ID = "identity_id";
      public static final String RQ_ROLE_ADD_ROLE_ID = "addRoleId";
      public static final String RQ_ROLE_REMOVE_ROLE_ID = "removeRoleId";


      
  }