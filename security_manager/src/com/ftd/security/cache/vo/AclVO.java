package com.ftd.security.cache.vo;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.io.Serializable;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 * Representation of an ACL
 * @author Christy Hu
 */
public class AclVO extends AbstractVO implements Serializable {

  // Output XML structure.
  public static final String XML_TOP = "ACLS";
  public static final String XML_BOTTOM = "ACL";

  // Statement column input constants for calling SQL procedures.
  public static final String ST_CONTEXT_ID = "in_context_id";
  public static final String ST_RESOURCE_ID = "in_resource_id";
  public static final String ST_PERMISSION_ID = "in_permission_id";
  public static final String ST_ACL_NAME = "in_acl_name";
  
  // Data field that come from the request.
  public static final String RQ_CONTEXT_ID = "context_id";
  public static final String RQ_RESOURCE_ID = "resourceId";
  public static final String RQ_RESOURCE_CB = "resourceCB"; // RESOURCE CHECKBOX
  public static final String RQ_PERMISSION_CB = "permissionCB"; // PERMISSION CHECKBOX
  public static final String RQ_ACL_NAME = "acl_name";
  public static final String RQ_RESOURCE_IDS = "resource_ids";
  public static final String RQ_PERM_IDS = "perm_ids";

  // Table columns (xml tag name)
  public static final String TB_CONTEXT_ID = "context_id";
  public static final String TB_ACL_NAME = "acl_name";
  public static final String TB_RESOURCE_ID = "resource_id";
  public static final String TB_PERMISSION_ID = "permission_id";

  private String ID; // deprecated
  private String context;
  private String resource;
  private String permission;
  private String aclName;

  public AclVO() {
  }

  public AclVO(String newContext, String newResource, String newPermission) 
  {
    context = newContext;
    resource = newResource;
    permission = newPermission;
  }
  
  public AclVO(HttpServletRequest request){
      context = request.getParameter(RQ_CONTEXT_ID);
      aclName = request.getParameter(RQ_ACL_NAME);
  }
      
  public String getAclName() {
    return aclName;
  }  

  public void setAclName(String newName) {
    aclName = newName;
  } 

  /**
   * @deprecated
   */
  public String getID() {
    return ID;
  }
  /**
   * @deprecated
   */
  public void setID(String newID) {
    ID = newID;
  }

  public String getContext() {
    return context;
  }

  public void setContext(String newContext) {
    context = newContext;
  }

  public String getResource() {
    return resource;
  }

  public void setResource(String newResource) {
    resource = newResource;
  }

  public String getPermission() {
    return permission;
  }

  public void setPermission(String newPermission) {
    permission = newPermission;
  }

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null. Resource and permission are checked via javascript.
       */
      public boolean isValid() {
          return context != null 
                 && context.length() > 0
                 && aclName != null
                 && aclName.length() >0;
      }
      
      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if(context == null || context.length() == 0) {
              errorMap.put(RQ_CONTEXT_ID, SecurityAdminConstants.ERROR_ENTER_VALUE);
          } 
          if(aclName == null || aclName.length() == 0) {
              errorMap.put(RQ_ACL_NAME, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          return errorMap;
      }  

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The role requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(RQ_ACL_NAME, "Resource Group name already exists.");
          return errorMap;
      }  

       /**
        * Returns document for displayAdd. FIELDS/FIELD (@fieldname, @value)
        */
        public Document getFieldDoc() throws Exception {        
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);
           // Field aclName
            Element aclNameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            aclNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, RQ_ACL_NAME);
            aclNameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, aclName);
            doc.getDocumentElement().appendChild(aclNameElem);       

            return doc;
        }      
}