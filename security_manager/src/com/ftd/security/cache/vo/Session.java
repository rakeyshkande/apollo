package com.ftd.security.cache.vo;
import java.io.*;
import java.util.Calendar;
import java.util.Set;
import java.util.*;

public class Session implements Serializable {
  private String identityID;
  private String securityToken;
  private String context;
  private Calendar lastAccessed;
  private Calendar timeoutDate;
  private Set roles;

  public Session() {
  }

  public String getIdentityID() {
    return identityID;
  }

  public void setIdentityID(String newIdentityID) {
    identityID = newIdentityID;
  }

  public String getSecurityToken() {
    return securityToken;
  }

  public void setSecurityToken(String newSecurityToken) {
    securityToken = newSecurityToken;
  }

  public String getContext() {
    return context;
  }

  public void setContext(String newContext) {
    context = newContext;
  }

  public Calendar getLastAccessed() {
    return lastAccessed;
  }

  public void setLastAccessed(Calendar newLastAccessed) {
    lastAccessed = newLastAccessed;
  }

  public Calendar getTimeoutDate() {
    return timeoutDate;
  }

  public void setTimeoutDate(Calendar newTimeoutDate) {
    timeoutDate = newTimeoutDate;
  }

  public Set getRoles() {
    return roles;
  }

  public void setRoles(Set newRoles) {
    roles = newRoles;
  }

  public void setRole(Role newRole) {
    if (roles == null)  {
        roles = new HashSet();
    }
    
    roles.add(newRole);
  }

}