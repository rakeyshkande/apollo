package com.ftd.security.cache.vo;

import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.util.XMLHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


  /**
   * Representation of a Context Configration object.
   * @author Christy Hu
   */
  public class ContextConfigVO extends AbstractVO
  {
      private String contextId;
      private String name;
      private String value;

      public ContextConfigVO(){}

      public ContextConfigVO(HttpServletRequest request){
          name = request.getParameter(ContextVO.RQ_CONFIG_NAME);
          value = request.getParameter(ContextVO.RQ_CONFIG_VALUE);
      }

      /**
       * Checks if the object is valid. Object is valid if none of the required
       * fields is null.
       */
      public boolean isValid() {
          return name != null 
                 && name.length() > 0
                 && value != null
                 && value.length() >0;
      }

      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getMissingRequiredFieldErrorMap() throws Exception {
          HashMap errorMap = new HashMap();
          if(name == null || name.length() == 0) {
              errorMap.put(ContextVO.RQ_CONFIG_NAME, SecurityAdminConstants.ERROR_ENTER_VALUE);
          } 
          if(value == null || value.length() == 0) {
              errorMap.put(ContextVO.RQ_CONFIG_VALUE, SecurityAdminConstants.ERROR_ENTER_VALUE);
          }
          return errorMap;
      }  


      /**
       * Returns the invalid fields and message in a map.
       */
      protected HashMap getDuplicateObjectErrorMap() throws Exception {
          // The role requested to add already exists.
          HashMap errorMap = new HashMap();
          errorMap.put(ContextVO.RQ_CONFIG_NAME, "Configuration name already exists in this context.");
          return errorMap;
      } 

      
      /**
       * Returns field document. FIELDS/FIELD (@fieldname, @value)
       */
      public Document getFieldDoc() throws Exception {   
            Document doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_FIELD_TOP);

            // Field name
            Element nameElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            nameElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, ContextVO.RQ_CONFIG_NAME);
            nameElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, name);
            doc.getDocumentElement().appendChild(nameElem);

            // Field value
            Element valueElem = doc.createElement(SecurityAdminConstants.XML_FIELD_BOTTOM);
            valueElem.setAttribute(SecurityAdminConstants.XML_ATTR_FIELD, ContextVO.RQ_CONFIG_VALUE);
            valueElem.setAttribute(SecurityAdminConstants.XML_ATTR_VALUE, value);
            doc.getDocumentElement().appendChild(valueElem);

            return doc;
      }
  }