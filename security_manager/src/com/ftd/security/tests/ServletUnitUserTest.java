package com.ftd.security.tests;

import com.kizna.servletunit.*;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import java.io.IOException;
import junit.framework.*;
import com.ftd.security.servlets.User;
import com.ftd.security.SecurityManager;
import java.util.Enumeration;
import com.ftd.security.util.ServletHelper;

/**
 * Fixture class to test the User servlet. 
 * The sequence of this suite of tests is:
 * 1. Get security token. This requires the IDENTITY is an administrator with
 *    full rights to resources in this application. Otherwise the tests will fail.
 * 2. Create a user. It is recommended that you make first name and last name
 *    distinct every time the tests are run. Will create a random name generator
 *    when time permits.
 * 3. View users. Tests the created user is in the list.
 * 4. View user. Verifies user details match the input user information.
 * 5. Update user. Verifies the changes are persisted.
 * 6. Create an identity. It is recommended that you make identity id distinct.
 * 7. Update identity. Verifies user identity detail is changed.
 * 8. Map identity role. Verifies that role is now associated to identity.
 * 9. Remove identity role association.
 * 10. Expire identity credentials.
 * 11. Expire identity.
 * 12. Expire user account.
 * 13. Remove identity.
 * 14. Remove user.
 */
public class ServletUnitUserTest extends TestCase
{
    private static final String CONTEXT = "GLOBAL";
    private static final String IDENTITY = "chu";
    private static final String CREDENTIALS = "chu";
    private static SecurityManager securityManager;
    private static String securityToken;
    private static String user_id;
    private static String first_name;
    private static String last_name;
    private static String unitid;

    protected void setUp() {
        securityToken = null;
        try  {
          this.securityManager = SecurityManager.getInstance();
          unitid = ServletHelper.getUnitID();
          // Get security Token.
          testGetSecurityToken();          
        } catch (Exception ex)  {
          ex.printStackTrace();
        } finally  {
        }
    }
    /**
      *
      * @param name java.lang.String
      */
    public ServletUnitUserTest(String name)
    {
        super(name);
    }

    /**
     * Test authenticate to get security token.
     */
    public static void testGetSecurityToken() 
    {
        try {
            securityToken = 
            (String)securityManager.authenticateIdentity(CONTEXT, unitid, IDENTITY, CREDENTIALS);
            System.out.println("Got Security Token:" + securityToken);
            assertNotNull("Returned null security token...", securityToken);
        } catch (Exception ex) {
            ex.printStackTrace();
            assertTrue("Received Exception when authenticating...", false);
        }
    }

    /**
     * Test view user.
     */
    public void testViewUser()
		{
			try
			{
          HttpServletRequestSimulator req = new HttpServletRequestSimulator();
          HttpServletResponseSimulator res = new HttpServletResponseSimulator();
          
          User servlet = new User();

          //success test
          req.addParameter("adminAction", "view");
          req.addParameter("context", CONTEXT);
          req.addParameter("securitytoken", securityToken);
          // get user_id dynamically
          req.addParameter("user_id","100048");

          // Make a POST call.
          servlet.init(servlet.getServletConfig());
          servlet.doPost(req,res);

          // Test the output
          // Bring in the output
          String result=  res.getWriterBuffer().toString();
          System.out.println("result: " + result);

          // Check if the result string contains first name.
          assertTrue("Returned unexpected first name...", result.indexOf(first_name) != -1);
          assertTrue("Returned unexpected last name...", result.indexOf(last_name) != -1);
			}
			catch (IOException e)
			{
					assertTrue("IOException occurred while talking to servlet",false);
			}
			catch (ServletException e)
			{
					assertTrue("ServletException occurred while talking to servlet",false);
			}
		}

    /**
     * Test add user.
     */
    public void testAddUser()
		{
			try
			{

          HttpServletRequestSimulator req = new HttpServletRequestSimulator();
          HttpServletResponseSimulator res = new HttpServletResponseSimulator();
          //ServletConfigSimulator config = new ServletConfigSimulator();
          User servlet = new User();

          //success test
          req.addParameter("adminAction", "add");
          req.addParameter("context", CONTEXT);
          req.addParameter("securitytoken", securityToken);
          
          // Try to make these names unique, although not required.
          first_name = "myFirstName";
          last_name = "myLastName";
          
          req.addParameter("firstName", first_name);
          req.addParameter("lastName", last_name);
          req.addParameter("expireDate","10/10/2004");

          servlet.init(servlet.getServletConfig());
          // Make a POST call.
          servlet.doPost(req,res);

          // Test the output
          // Bring in the output
          String result=  res.getWriterBuffer().toString();
          System.out.println("result: " + result);
          
          // Check if the result string contains first name.
          assertTrue("Returned unexpected first name...", result.indexOf(first_name) != -1);
          assertTrue("Returned unexpected last name...", result.indexOf(last_name) != -1);
			}
			catch (IOException e)
			{
					assertTrue("IOException occurred while talking to servlet",false);
			}
			catch (ServletException e)
			{
					assertTrue("ServletException occurred while talking to servlet",false);
			}
		}
    /**
     * Test view users.
     */
    public void testViewListOfUsers()
		{
			try
			{
          HttpServletRequestSimulator req = new HttpServletRequestSimulator();
          HttpServletResponseSimulator res = new HttpServletResponseSimulator();
          User servlet = new User();

          //success test
          req.addParameter("adminAction", "view_all");
          req.addParameter("context", CONTEXT);
          req.addParameter("securitytoken", securityToken);

          // Post request
          servlet.doPost(req,res);

          // Test the output
          // Bring in the output
          String result=  res.getWriterBuffer().toString();
          System.out.println("result: " + result);
          // Instantiate the expected data
          String expectedFirstName = "";
          String expectedLastName = "";

          // Check if the result string contains first name.
          assertTrue("Returned unexpected first name...", result.indexOf(expectedFirstName) != -1);
          assertTrue("Returned unexpected last name...", result.indexOf(expectedLastName) != -1);
			}
			catch (IOException e)
			{
					assertTrue("IOException occurred while talking to servlet",false);
			}
			catch (ServletException e)
			{
					assertTrue("ServletException occurred while talking to servlet",false);
			}
		}
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      //suite.addTest(new ServletUnitUserTest("testGetSecurityToken"));
      suite.addTest(new ServletUnitUserTest("testAddUser"));
      return suite;
    }


}
