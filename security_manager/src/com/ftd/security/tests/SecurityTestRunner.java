package com.ftd.security.tests;

import junit.framework.TestSuite;

/**
 * Driver to initiate all tests in this package.
 * @author Christy Hu
 */
public class SecurityTestRunner{

    public static void main(String args[]) { 
      TestSuite suite= new TestSuite();

      // Test utilities
      //suite.addTest(StringUtilTest.suite());
      //suite.addTest(XMLHelperTest.suite());
      //suite.addTest(DataRequestHelperTest.suite());

      // Servlet tests are replaced by black box testing recommended by Anshu Gaind.
      // Please follow Word document.
      // suite.addTest(HttpUnitUserTest.suite());     
      // suite.addTest(ServletUnitUserTest.suite());
      // suite.addTest(ServletUserTest.suite());
      suite.addTest(UserManagerTest.suite());
      junit.textui.TestRunner.run(suite);

    }

}
