package com.ftd.security.tests;

import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.XMLHelper;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Fixture to test XMLHelper utility.
 * @author Christy Hu
 */
public class XMLHelperTest extends TestCase {
    XMLHelper xmlHelper;
    
    public XMLHelperTest(String test) {
        super(test);
    }

    protected void setUp() {
         xmlHelper = new XMLHelper();
    }

    /**
     * Tests getDocumentWithRoot
     */
    public void testGetDocumentWithRoot() {
      try{       
        Document doc = xmlHelper.getDocumentWithRoot("ROOT");
        Element elem = (Element)doc.getDocumentElement();        
        assertTrue("Got results back", "ROOT".equals(elem.getTagName()));	

        System.out.println("\n-----testGetDocumentWithRoot succeeded!-----");
      } catch(Exception e) {
          e.printStackTrace();
      }
    }
    
    /**
     * Tests getErrorDocument
     */
    public void testGetErrorDocument() {
      try{       
        SecurityAdminException ex = new SecurityAdminException("1");
        Document doc = xmlHelper.getErrorDocument(ex);
        Element elem = (Element)doc.getDocumentElement();
        Element err = (Element)elem.getElementsByTagName("ERROR").item(0);
        assertTrue("Got results back", "ERRORS".equals(elem.getTagName()));
        assertTrue("Got results back", "ERROR".equals(err.getTagName()));
        assertTrue("Got results back", "1".equals(err.getAttribute("code")));
        System.out.println("\n-----testGetErrorDocument succeeded!-----");
      } catch(Exception e) {
          e.printStackTrace();
      }
    }

    /**
     * Tests findElementByChild
     */
    public void testFindElementByChild() {
      try{       
        Document doc = xmlHelper.getDocumentWithRoot("ROOT");
        Element elem = doc.createElement("CHILD");
        Text t = doc.createTextNode("data");
        elem.appendChild(t);
        ((Element)doc.getDocumentElement()).appendChild(elem);

        Element result = (Element)xmlHelper.findElementByChild(doc, "CHILD", "data");
        
        assertTrue("Got results back", "ROOT".equals(result.getTagName()));
        System.out.println("\n-----testFindElementByChild succeeded!-----");
      } catch(Exception e) {
          e.printStackTrace();
      }
    }
    
    /**
     * Tests mergeDocuments
     */
    public void testMergeDocuments() {
      try{       
        Document adoc = xmlHelper.getDocumentWithRoot("ROOTS");
        Document bdoc = xmlHelper.getDocumentWithRoot("ROOTS");

        Element aroot = (Element)adoc.createElement("ROOT");
        Element broot = (Element)bdoc.createElement("ROOT");       
        Element achild = (Element)adoc.createElement("CHILD");
        Text atext = adoc.createTextNode("ref");
        achild.appendChild(atext);
        
        Element bchild = (Element)bdoc.createElement("CHILD");
        Text btext = bdoc.createTextNode("ref");
        bchild.appendChild(btext);
        Element bdata = (Element)bdoc.createElement("DATA");
        Text t = bdoc.createTextNode("data");
        bdata.appendChild(t);
        broot.appendChild(bdata);
        aroot.appendChild(achild);
        broot.appendChild(bchild);

        adoc.getDocumentElement().appendChild(aroot);
        bdoc.getDocumentElement().appendChild(broot);
        Document resultDoc = xmlHelper.mergeDocuments(adoc, bdoc, "ROOT", "CHILD", "DATA");
        Element parent = (Element)(adoc.getElementsByTagName("ROOT")).item(0);
        
        assertTrue("Got results back", "data".equals(parent.getAttribute("DATA")));
        System.out.println("\n-----testMergeDocuments succeeded!-----");
      } catch(Exception e) {
          e.printStackTrace();
      }
    }
                
    public static Test suite() { 
    	TestSuite suite= new TestSuite(); 
    	suite.addTest(new XMLHelperTest("testGetDocumentWithRoot")); 
    	suite.addTest(new XMLHelperTest("testGetErrorDocument"));
      suite.addTest(new XMLHelperTest("testFindElementByChild"));
      suite.addTest(new XMLHelperTest("testMergeDocuments")); 
    	return suite;
    }


 }
