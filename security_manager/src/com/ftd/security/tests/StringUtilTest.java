package com.ftd.security.tests;

import com.ftd.security.util.StringUtil;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import java.text.DateFormat;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Fixture to test StringUtil class.
 * @author Christy Hu
 */ 
public class StringUtilTest extends TestCase {
    // test getIds
    protected String normalStr;
    protected String emptyStr;
    protected String nullStr;

    // test toSqlDate
    protected String normalDate;
    protected String shortDate;
    protected String emptyDate;
    protected String nullDate;    

    public StringUtilTest(String test) {
        super(test);
    }

    protected void setUp() {
        // test getIds
        normalStr = ",1,4,7,11";
        emptyStr = "";
        nullStr = null;

        // test toSqlDate
        normalDate = "11/22/2003";
        shortDate = "1/2/2004";
        emptyDate = "";
        nullDate = null;
    }

    /**
     * Tests getIds
     */
    public void testGetIds() {
      try{
        ArrayList result = StringUtil.getIds(normalStr);

        assertTrue("Got result back", result!=null);	
        assertTrue("Element 1: " + result.get(0), "1".equals((String)result.get(0)));
        assertTrue("Element 2: " + result.get(1), "4".equals((String)result.get(1)));
        assertTrue("Element 3: " + result.get(2), "7".equals((String)result.get(2)));
        assertTrue("Element 4: " + result.get(3), "11".equals((String)result.get(3)));

        result = StringUtil.getIds(emptyStr);
        assertTrue("Got result back", result!=null && result.size() == 0);


        result = StringUtil.getIds(nullStr);
        assertTrue("Got result back", result!=null && result.size() == 0);
        System.out.println("\n-----testGetIds succeeded!-----");
      } catch(Exception e) {
          e.printStackTrace();
      }
    }

    /**
     * Tests toSqlDate
     */
    public void testToSqlDate() {
      try {
        Date result = StringUtil.toSqlDate(normalDate);
        Calendar cal = Calendar.getInstance();
        cal.set(2003, Calendar.NOVEMBER, 22);
	
        assertTrue("Got result back", result!=null);	
        System.out.println(result.getTime());
        System.out.println((cal.getTime()).getTime());
        assertTrue(result.getTime() == (cal.getTime()).getTime());
	
        result = StringUtil.toSqlDate(shortDate);
        cal.set(2004, Calendar.JANUARY, 2);

        assertTrue("Got result back", result!=null);	
        assertTrue(result.getTime() == (cal.getTime()).getTime());


        result = StringUtil.toSqlDate(emptyDate);

        System.out.println("\n-----testToSqlDate succeeded!-----");
      } catch (Exception e) {
          e.printStackTrace();
      }
    }

    /**
     * Tests isValidDateString
     */
    public void testIsValidDateString() {
      try {
        String s1 = "1/1/2003";
        String s4 = "a/4/2007";
        boolean valid = StringUtil.isValidDateString(s1);
        assertTrue("is valid date", valid==true);	
        valid = StringUtil.isValidDateString(s4);
        assertTrue("not a valid date", valid==false);  

        
        
        System.out.println("\n-----testIsValidDateString succeeded!-----");
      } catch (Exception e) {
          e.printStackTrace();
      }
    }

    /**
     * Tests formatUtilDateToString
     */
    public void testFormatUtilDateToString() {
      try {
        java.util.Date utilDate = new java.util.Date();
        String value = StringUtil.formatUtilDateToString(utilDate);
        System.out.println(value);  
        
        String pattern = "yyyy-MM-dd HH:mm:ss.S";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
    
        java.util.Date d = df.parse("2003-12-01 00:00:00.0");
        value = StringUtil.formatUtilDateToString(d);
        System.out.println(value);
        System.out.println("\n-----testFormatUtilDateToString succeeded!-----");
      } catch (Exception e) {
          e.printStackTrace();
      }
    }

    
    public static Test suite() { 
    	TestSuite suite= new TestSuite(); 
    	//suite.addTest(new StringUtilTest("testGetIds")); 
    	//suite.addTest(new StringUtilTest("testToSqlDate")); 
      //suite.addTest(new StringUtilTest("testIsValidDateString")); 
      suite.addTest(new StringUtilTest("testFormatUtilDateToString"));
    	return suite;
    }
 }


