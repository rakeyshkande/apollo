package com.ftd.security.tests;

import com.ftd.security.util.StringUtil;

import java.sql.Date;

import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import java.text.DateFormat;
import com.ftd.security.manager.UserManager;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Fixture to test StringUtil class.
 * @author Christy Hu
 */
public class UserManagerTest extends TestCase {


    public UserManagerTest(String test) {
        super(test);
    }

    protected void setUp() {

    }
    public void testGetValidDepartmentId() {
        try {
            String result = null;
            String value = "Accounting/Finance";
            //String value = "";
            //String value = "null";
            //String value = "Customer ServiceS";
            result = UserManager.getValidDepartmentId(value);
            System.out.println("RESULT IS" + result);
            assertTrue("Result...", result.equals("ACCT"));
            //assertTrue("Result...", result.equals(""));
            
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void testGetValidRoleId() {
        try {
            String result = null;
            String value = "System Admin";
            //String value = "";
            //String value = "null";
           
            result = UserManager.getValidRoleId(value);
            System.out.println("Result..."+ result);
            //assertTrue("Result...", result.equals("CS"));
            assertTrue("Result...", !result.equals(""));
            
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Test suite() {
    	TestSuite suite= new TestSuite();

      suite.addTest(new UserManagerTest("testGetValidDepartmentId"));
      suite.addTest(new UserManagerTest("testGetValidRoleId"));
    	return suite;
    }
 }
