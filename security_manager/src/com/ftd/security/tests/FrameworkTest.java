package com.ftd.security.tests;

import com.ftd.security.SecurityManager;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.ftd.security.cache.vo.*;


/**
 * The test class will extend junit.framework.TestCase
 **/
public class FrameworkTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public FrameworkTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
  public void testUserInformation(){
    
    try  {
        SecurityManager secMgr = SecurityManager.getInstance();
        String securityToken = (String) secMgr.authenticate("Order Proc", "againd", "password1");
        System.out.println(securityToken);        
        UserInfo userInfo =  secMgr.getUserInfo(securityToken);        
        System.out.println(userInfo.getUserID());
    } catch (Exception ex)  {
        ex.printStackTrace();
    } 
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
  public void testCredentialsBasedAuthentication(){
    
    try  {
        SecurityManager secMgr = SecurityManager.getInstance();
        String securityToken = (String) secMgr.authenticate("KMTOOL", "againd", "againd");
        System.out.println(securityToken);        
        System.out.println(secMgr.authenticate("KMTOOL", securityToken));        
    } catch (Exception ex)  {
        ex.printStackTrace();
    } 
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
  public void testSecurityTokenBasedAuthentication(){
    
    try  {
        SecurityManager secMgr = SecurityManager.getInstance();
      String st = "FTD_GUID_5804213130-11758913470-4701028510102704874002821145270-9581244705505318920-141963682012071861945063241453508331924330675083486248175252040100-1293817777986349470069-53051097646";
        System.out.println(secMgr.authenticate( "GLOBAL", st));        
    } catch (Exception ex)  {
        ex.printStackTrace();
    } 
  }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
  public void testAssertPermission(){
    
    try  {
        SecurityManager secMgr = SecurityManager.getInstance();
        //String securityToken = (String) secMgr.authenticate("TEST", "CREYES", "CREYES1");
        //System.out.println(securityToken);
        String st = "FTD_GUID_5804213130-11758913470-4701028510102704874002821145270-9581244705505318920-141963682012071861945063241453508331924330675083486248175252040100-1293817777986349470069-53051097646";
        System.out.println(secMgr.assertPermission( "GLOBAL", 
                                                    st,
                                                    "User",
                                                    "v"));        
    } catch (Exception ex)  {
        ex.printStackTrace();
    } 
  }


  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
   // suite.addTest(new FrameworkTest("testCredentialsBasedAuthentication"));
    //suite.addTest(new FrameworkTest("testSecurityTokenBasedAuthentication"));
    suite.addTest(new FrameworkTest("testUserInformation"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
}