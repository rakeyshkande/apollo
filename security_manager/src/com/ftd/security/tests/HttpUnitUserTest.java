package com.ftd.security.tests;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebForm;
import com.meterware.httpunit.WebLink;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

import java.net.URL;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *  Fixture intended to test User servlet. Only testCreateUser can be run due
 *  to HttpUnit limitation on javascript.
 *  @author Christy Hu
 */
public class HttpUnitUserTest extends TestCase
{
    //String url = "http://www.dice.com";
    String url = "http://192.168.111.206:8988/Order%20Scrub%20Project-Security%20Manager-context-root/security/html/manageUserAndIdentity/CreateUser.html";

    public HttpUnitUserTest(String s)
    {
        super(s);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite();
        // To test create user, un-comment the following line and change the firstName,
        // and lastName parameter. Otherwise it makes the next tests ambiguous by having
        // multiple users with the same name.
        // suite.addTest(new HttpUnitUserTest("testCreateUser"));
        // testViewListOfUsers cannot be run due to HttpUnit limitations on javascript (div).
        // suite.addTest(new HttpUnitUserTest("testViewListOfUsers"));
        return suite;
    }

/**
 * Simulates the creation of a user. 
 */
    public void testCreateUser() {
        try {
            // Get request and response
            URL serverUrl = new URL(url);
            WebConversation     conversation = new WebConversation();
            WebRequest request = new GetMethodWebRequest(serverUrl,"");
            WebResponse response = conversation.getResponse(request);
            assertNotNull("response of url: " + url, response);

            // Get form
            WebForm userForm = response.getForms()[0];
            assertNotNull("find jobs form",userForm);
            System.out.println("got the form" );

            // Fill the form
            userForm.setParameter("lastName","Test");
            userForm.setParameter("firstName","Test");
            userForm.setParameter("expireDate","10/10/2004");

            userForm.submit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/**
 * Simulates viewing user list. Won't run until HttpUnit supports 'div'.
 */
    public void testViewListOfUsers() {
        try {
            url = "http://192.168.111.206:8988/Order%20Scrub%20Project-Security%20Manager-context-root/servlet/User";
            // Get request and response
            URL serverUrl = new URL(url);
            WebConversation     conversation = new WebConversation();
            WebRequest request = new GetMethodWebRequest(serverUrl,"");
            WebResponse response = conversation.getResponse(request);
            assertNotNull("response of url: " + url, response);

            // By default the servlet sends an error page because no action has been defined.
            // Follow the links: 'Main Menu','Manage User and Identity','View List of Users'.
            WebLink link = response.getLinkWith("Main Menu");
            response = link.click();
            link = response.getLinkWith("Manage User and Identity");
            response = link.click();
            link = response.getLinkWith("View List of Users");
            response = link.click(); 
            // Change the following firstName lastName to what you used in createUserTest.
            link = response.getLinkWith("Test Test");
            assertNotNull("found the user just created", link);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}