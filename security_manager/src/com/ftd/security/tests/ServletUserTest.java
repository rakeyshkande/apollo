package com.ftd.security.tests;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URL;
import java.util.HashMap;
import com.ftd.security.*;
import com.ftd.security.SecurityManager;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.ftd.security.util.ServletHelper;

/**
 * Fixture class to test the User servlet. 
 * The sequence of this suite of tests is:
 * 1. Get security token. This requires the IDENTITY be an administrator with
 *    full rights to resources within the context in this application. 
 *    Otherwise the tests will fail.
 * 2. Create a user. It is recommended that the first name and last name be
 *    distinct every time the tests are run. Will create a random name generator
 *    when time permits.
 * 3. View users. Tests the created user is in the list.
 * 4. View user. Verifies user details match the input user information.
 * 5. Update user. Verifies the changes are persisted.
 * 6. Create an identity. It is recommended that you make identity id distinct.
 * 7. Update identity. Verifies user identity detail is changed.
 * 8. Map identity role. Verifies that role is now associated to identity.
 * 9. Remove identity role association.
 * 10. Expire identity credentials.
 * 11. Expire identity.
 * 12. Expire user account.
 * 13. Remove identity.
 * 14. Remove user.
 * @author Christy Hu
 */
public class ServletUserTest  extends TestCase
{
    private static final String  URL = "http://192.168.111.206:8988/Order%20Scrub%20Project-Security%20Manager-context-root/security/User";
    private static final String CONTEXT = "GLOBAL";
    private static final String IDENTITY = "chu";
    private static final String CREDENTIALS = "chu";
    private static SecurityManager securityManager;
    private static String securityToken;
    private static String user_id;
    private static String first_name;
    private static String last_name;    
    private static String unitid;
    
    /** 
     * Create a constructor that take a String parameter and passes it 
     * to the super class 
     **/ 
    public ServletUserTest(String name) { 
        super(name); 
    }

    protected void setUp() {
        securityToken = null;
        try  {
          this.securityManager = SecurityManager.getInstance();
          unitid = ServletHelper.getUnitID();
          testGetSecurityToken();
        } catch (Exception ex)  {
          ex.printStackTrace();
        } finally  {
        }
    }
    /**
     * Test authenticate to get security token.
     */
    private static void testGetSecurityToken() 
    {
        try {
            securityToken = 
            (String)securityManager.authenticateIdentity(CONTEXT, unitid, IDENTITY, CREDENTIALS);
            System.out.println("Got Security Token:" + securityToken);
            assertNotNull("Returned null security token...", securityToken);
        } catch (Exception ex) {
            ex.printStackTrace();
            assertTrue("Received Exception when authenticating...", false);
        }
    }

    /** 
    * Test add user and view user
    **/ 
    public void testAddUser() throws Exception{  
      

      first_name = "FIRST_NAME_XYZ";
      last_name = "LAST_NAME_XYZ";
      String expDate = "10/23/2004";
      Map map = new HashMap();
      map.put("adminAction", "add");
      map.put("context", CONTEXT);
      map.put("securitytoken", securityToken);
      map.put("firstName", first_name);
      map.put("lastName", last_name);
      map.put("expireDate", expDate);
  
      String response = send(URL,map);          
      System.out.println(response);
      
      //validate response
       if(response.indexOf(first_name) <= 0){
          assertTrue("Response did not come back with first name.",false);
       }

       if(response.indexOf(last_name) <=0){
          assertTrue("Response did not come back with last name.",false);
       }
       
  }     
    /** 
    * Test view user. Since addUser calls viewUser in the servlet. 
    * This is already tested with testAddUser.
    **/ 
    public void testViewUser() throws Exception{  
          Map map = new HashMap();
          map.put("adminAction","view");
          map.put("context",CONTEXT);
          map.put("securitytoken",securityToken);
         // map.put("user_id",user_id);
          map.put("user_id", "100048");
          String response = send(URL,map);          
          System.out.println(response);
      
          //validate response
          
           if(response.indexOf(first_name) <= 0){
              assertTrue("Response did not come back with first name.",false);
           }

           if(response.indexOf(last_name) <=0){
              assertTrue("Response did not come back with last name.",false);
           }
       
  } 

  /**
   * Get connection to URL. Construct request and get back a response.
   */
	private synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            //con.setRequestProperty("Content-Type", " " + contentType);
            //con.setRequestProperty("enctype", " " + encrypt);
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send
    
    public static TestSuite suite()
    {
      TestSuite suite = new TestSuite();
      try {
      
      suite.addTest(new ServletUnitUserTest("testAddUser"));
      //suite.addTest(new ServletUserTest("testViewUser"));
      } catch (Exception e) {
          e.printStackTrace();
      }

      return suite;
    }    
}