package com.ftd.security.tests;

import com.ftd.security.util.DataRequestHelper;

import java.sql.Connection;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Fixture to test DataRequestHelper utility.
 * @author Christy Hu
 */
public class DataRequestHelperTest extends TestCase {

    public DataRequestHelperTest(String test) {
        super(test);
    }

    protected void setUp() {
    }

/**
 * Tests getDBConnection
 */
    public void testGetDBConnection() {
      try{
        Connection con = DataRequestHelper.getDBConnection();

        assertNotNull("Got result back", con);	
        System.out.println("\n-----testGetDBConenction succeeded!-----");
        con.close();
      } catch(Exception e) {
          e.printStackTrace();
      }
    }

    public static Test suite() { 
    	TestSuite suite= new TestSuite(); 
    	suite.addTest(new DataRequestHelperTest("testGetDBConnection")); 
    	return suite;
    }


 }
