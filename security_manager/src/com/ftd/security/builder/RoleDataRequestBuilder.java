package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.RoleVO;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.ServletHelper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build Role DataRequest input parameters.
 *  @author Christy Hu 
 */
public class RoleDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.RoleDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public RoleDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**  
   * Builds and returns input paramter map for 'Add Role'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/    
  public HashMap getAddRoleInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException, Exception
  {
      logger.debug("Entering getAddRoleInputParams...");
      HashMap inputParams = new HashMap();
      try {
          String sRoleName = request.getParameter(RoleVO.RQ_ROLE_NAME);
          String sContextId = request.getParameter(RoleVO.RQ_CONTEXT_ID);
          String sDescription = request.getParameter(RoleVO.RQ_DESCRIPTION);
          logger.debug("Setting in_context_id to: " + sContextId);
          logger.debug("Setting in_role_name to: " + sRoleName);
          logger.debug("Setting in_description to: " + sDescription);
          logger.debug("Setting role_id to: null, type: java.sql.Types.INTEGER");

          if(sRoleName == null || sRoleName.length() == 0 
            || sContextId == null || sContextId.length() == 0) 
            {
                logger.error("MISSING REQUIRED FIELD - Role Name or Context Id");
                throw new SecurityAdminException("7", "ROLE NAME AND CONTEXT ID REQUIRED");
            }      
          inputParams.put(RoleVO.ST_UPDATED_BY, updatedBy);
          inputParams.put(RoleVO.ST_CONTEXT_ID, sContextId);
          inputParams.put(RoleVO.ST_ROLE_NAME, sRoleName);
          inputParams.put(RoleVO.ST_DESCRIPTION, sDescription);
          inputParams.put(RoleVO.ST_ROLE_ID, Integer.valueOf(String.valueOf(java.sql.Types.INTEGER)));
      } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID TO SET");
          logger.error(ex);
          throw new SecurityAdminException("1");
      } 
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Update Role'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getUpdateRoleInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException, Exception
  {
       HashMap inputParams = new HashMap();
       String sRoleId = request.getParameter(RoleVO.RQ_ROLE_ID);

       if (sRoleId == null || sRoleId.length() == 0) {
         logger.error("NULL ROLE ID TO UPDATE");
         throw new SecurityAdminException("1");
       }
       // Not allowing changes to role_name and context_id.
       // Get the original values.
       String sRoleName = request.getParameter(RoleVO.RQ_ROLE_NAME);
       String sContextId = request.getParameter(RoleVO.RQ_CONTEXT_ID);
       String sDescription = request.getParameter(RoleVO.RQ_DESCRIPTION);

       logger.debug("Setting in_context_id to: " + sContextId);
       logger.debug("Setting in_role_name to: " + sRoleName);
       logger.debug("Setting in_description to: " + sDescription);
       logger.debug("Setting role_id to: " + sRoleId);

       if(sRoleName == null || sRoleName.length() == 0 
            || sContextId == null || sContextId.length() == 0) 
       {
            logger.error("MISSING REQUIRED FIELD - Role Name or Context Id");
            throw new SecurityAdminException("7", "ROLE NAME AND CONTEXT ID REQUIRED");
       } 
       inputParams.put(RoleVO.ST_UPDATED_BY, updatedBy);
       inputParams.put(RoleVO.ST_CONTEXT_ID, sContextId);
       inputParams.put(RoleVO.ST_ROLE_NAME, sRoleName);
       inputParams.put(RoleVO.ST_DESCRIPTION, sDescription);
       inputParams.put(RoleVO.ST_ROLE_ID, Integer.valueOf(sRoleId));
       return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'View Role'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getViewRoleInputParams(HttpServletRequest request)
    throws SecurityAdminException, Exception
  {
        logger.debug("Entering getViewRoleInputParams...");
        HashMap inputParams = new HashMap();
        String roleId = request.getParameter(RoleVO.RQ_ROLE_ID);
        
        if (roleId == null) {
          // This is a forwarded request.
          roleId = (String)request.getAttribute(RoleVO.RQ_ROLE_ID);
        }
        
        logger.debug("Setting in_role_id to: " + roleId);
        if (roleId == null) {
          logger.error("INVALID ROLE ID TO RETRIEVE");
          throw new SecurityAdminException("1");
        }        
        try {
          inputParams.put(RoleVO.ST_IN_ROLE_ID, Integer.valueOf(roleId));
        } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID TO RETRIEVE");
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'View Role ACL'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getViewRoleAclInputParams(HttpServletRequest request)
    throws SecurityAdminException, Exception
  {
        HashMap inputParams = new HashMap();
        String roleId = request.getParameter(RoleVO.RQ_ROLE_ID);
        String contextId = request.getParameter(RoleVO.RQ_CONTEXT_ID);
        logger.debug("Setting in_role_d to: " + roleId);
        logger.debug("Setting in_context_d to: " + contextId);
        
        if (roleId == null) {
          roleId = (String)request.getAttribute(RoleVO.RQ_ROLE_ID);
        }

        if(roleId == null || roleId.length() == 0 
          || contextId == null || contextId.length() == 0) 
          {
              logger.error("MISSING REQUIRED FIELD - roleId/contextId");
              // Should not happen from the users stand point.
              throw new SecurityAdminException("1");
          }        
        try {
          inputParams.put(RoleVO.ST_IN_ROLE_ID, Integer.valueOf(roleId));
          inputParams.put(RoleVO.ST_CONTEXT_ID, contextId);
        } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID TO RETRIEVE ACL");
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for 'Remove Role'. Allows multiple
   * removal at one time. Use the id string param as the parameter suffix 
   * to get role id parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @param id parameter name suffix
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getRemoveRoleInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException, Exception
  {
        logger.debug("Entering getRemoveRoleInputParams...");
        logger.debug("Using remove id suffix " + id + " to retrive input parameters...");
        HashMap inputParams = null;
        if (id == null || id.length() == 0) {
         logger.error("NULL ROLE ID SUFFIX TO REMOVE");
         throw new SecurityAdminException("1");
        }
        inputParams = new HashMap();
        String roleId = request.getParameter(RoleVO.RQ_REMOVE_ROLE_ID + id);
        logger.debug("Setting in_role_id to: " + roleId);
        
        try {
          inputParams.put(RoleVO.ST_IN_ROLE_ID, Integer.valueOf(roleId));   
        } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID TO REMOVE");
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }  
  
  /**  
   * Builds and returns input paramter map for 'Add Role ACL'. Allows multiple
   * addings at one time. Use the id string param as the parameter suffix 
   * to get ACL name parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @param id parameter name suffix
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getAddRoleAclInputParams(HttpServletRequest request, String id, String updatedBy)
    throws SecurityAdminException, Exception
  {
        logger.debug("Entering getRemoveRoleAclInputParams...");
        HashMap inputParams = new HashMap();
        String sRoleId = request.getParameter(RoleVO.RQ_ROLE_ID);
        String sAclName = request.getParameter(RoleVO.RQ_ADD_ACL_NAME + id);
        logger.debug("Using remove id suffix " + id + " to retrive input parameters...");
        logger.debug("Setting in_role_id to " + sRoleId);
        logger.debug("Setting in_acl_name to " + sAclName);
        
        try {
          inputParams.put(RoleVO.ST_UPDATED_BY, updatedBy);
          inputParams.put(RoleVO.ST_IN_ROLE_ID, Integer.valueOf(sRoleId)); 
          inputParams.put(RoleVO.ST_ACL_NAME, sAclName);
        } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID / ACL NAME TO ADD");
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }    

  /**  
   * Builds and returns input paramter map for 'Remove Role ACL'. Allows multiple
   * removals at one time. Use the id string param as the parameter suffix 
   * to get ACL name parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @param id parameter name suffix
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getRemoveRoleAclInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException, Exception
  {
        logger.debug("Entering getRemoveRoleAclInputParams...");
        HashMap inputParams = new HashMap();
        String sRoleId = request.getParameter(RoleVO.RQ_ROLE_ID);
        String sAclName = request.getParameter(RoleVO.RQ_REMOVE_ACL_NAME + id);
        logger.debug("Using remove id " + id + " to retrive input parameters...");
        logger.debug("Setting in_role_id to " + sRoleId);
        logger.debug("Setting in_acl_name to " + sAclName);
        
        try {
          inputParams.put(RoleVO.ST_IN_ROLE_ID, Integer.valueOf(sRoleId)); 
          inputParams.put(RoleVO.ST_ACL_NAME, sAclName);
        } catch (NumberFormatException ex) {
          logger.error("INVALID ROLE ID / ACL NAME TO REMOVE");
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }    
}