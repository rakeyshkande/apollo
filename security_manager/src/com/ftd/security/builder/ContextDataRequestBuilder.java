package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.ContextVO;
import com.ftd.security.exceptions.SecurityAdminException;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build Context and Context Config DataRequest input parameters.
 *  @author Christy Hu 
 */
public class ContextDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.ContextDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public ContextDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }
  
  /**  
   * Builds and returns input paramter map for 'Update Context'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getUpdateContextInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      HashMap inputParams = new HashMap();
      logger.debug("Entering getUpdateContextInputParams...");  
      String sContextId = (String)request.getParameter(ContextVO.RQ_CONTEXT_ID);
      String sDescription = (String)request.getParameter(ContextVO.RQ_DESCRIPTION);
      logger.debug("Setting context_id to " + sContextId);
      logger.debug("Setting description to " + sDescription);

      if(sContextId == null || sContextId.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD - CONTEXT ID");
            throw new SecurityAdminException("7", "CONTEXT ID REQUIRED");
        } 
      inputParams.put(ContextVO.ST_UPDATED_BY, updatedBy);
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ContextVO.ST_DESCRIPTION, sDescription);
      return inputParams;
    
  }
  
  /**  
   * Builds and returns input paramter map for 'Add Context'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
   public HashMap getAddContextInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getAddContextInputParams..."); 
      return getUpdateContextInputParams(request, updatedBy);   
  } 

  /**  
   * Builds and returns input paramter map for 'View Context'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getViewContextInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getViewContextInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sContextId = (String)request.getParameter(ContextVO.RQ_CONTEXT_ID);
      logger.debug("Setting in_context_id to " + sContextId);

      if(sContextId == null || sContextId.length() == 0) 
      {
            logger.error("MISSING REQUIRED FIELD - CONTEXT ID");
            throw new SecurityAdminException("7", "CONTEXT ID REQUIRED");
      } 
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
       
      return inputParams;
  }
  
  /**  
   * Builds and returns input paramter to check if context exists.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getContextExistsInputParams(HttpServletRequest request)
    throws SecurityAdminException
  { 
      logger.debug("Entering getContextExistsInputParams...");  
      return getViewContextInputParams(request);    
  }  

  /**  
   * Builds and returns input paramter map for 'Remove Context'.
   * @param request HttpServletRequest to be analyzed.
   * @param id parameter id suffix
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getRemoveContextInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
      logger.debug("Entering getRemoveContextInputParams...");
      HashMap inputParams = new HashMap();
      
      logger.debug("Using remove id "+ id + " to retrieve input parameters...");
      String sContextId = (String)request.getParameter(ContextVO.RQ_REMOVE_CONTEXT_ID + id);
      
      logger.debug("Setting in_context_id to " + sContextId);
      if(sContextId == null || sContextId.length() == 0) 
      {
            logger.error("MISSING REQUIRED FIELD - CONTEXT ID");
            throw new SecurityAdminException("7", "CONTEXT ID REQUIRED");
      } 
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
       
      return inputParams;
  } 

  /**  
   * Builds and returns input paramter map for 'View Config'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getViewConfigInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getViewConfigInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sContextId = (String)request.getParameter(ContextVO.RQ_CONTEXT_ID);
      String sConfigName = (String)request.getParameter(ContextVO.RQ_CONFIG_NAME);
      logger.debug("Setting in_context_id to " + sContextId);
      logger.debug("Setting in_name to " + sConfigName);

      if(sContextId == null || sContextId.length() == 0
        || sConfigName == null || sConfigName.length() == 0) {
         logger.debug("NULL CONTEXT ID OR CONFIG NAME");
         throw new SecurityAdminException("7", "CONTEXT ID/CONFIG NAME REQUIRED");
      }
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ContextVO.ST_CONFIG_NAME, sConfigName);
       
      return inputParams;
  }
  
  /**  
   * Builds and returns input paramter map for 'Update Config'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getUpdateConfigInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getUpdateConfigInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sContextId = (String)request.getParameter(ContextVO.RQ_CONTEXT_ID);
      String sConfigName = (String)request.getParameter(ContextVO.RQ_CONFIG_NAME);
      String sConfigValue = (String)request.getParameter(ContextVO.RQ_CONFIG_VALUE);
      logger.debug("Setting in_context_id to " + sContextId);
      logger.debug("Setting in_name to " + sConfigName);
      logger.debug("Setting in_value to " + sConfigValue);

      if(sContextId == null || sContextId.length() == 0
        || sConfigName == null || sConfigName.length() == 0
        || sConfigValue == null || sConfigValue.length() == 0) {
         logger.debug("Config name and value cannot be null.");
         throw new SecurityAdminException("7", "CONFIG NAME/CONFIG VALUE REQUIRED");
      }
      inputParams.put(ContextVO.ST_UPDATED_BY, updatedBy);
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ContextVO.ST_CONFIG_NAME, sConfigName);
      inputParams.put(ContextVO.ST_CONFIG_VALUE, sConfigValue);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Remove Config'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getRemoveConfigInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
      logger.debug("Entering getRemoveConfigInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sContextId = (String)request.getParameter(ContextVO.RQ_CONTEXT_ID);
      String sConfigName = (String)request.getParameter(ContextVO.RQ_REMOVE_CONFIG_CB + id);
      logger.debug("Setting in_context_id to " + sContextId);
      logger.debug("Setting in_name to " + sConfigName);

      if(sContextId == null || sContextId.length() == 0
        || sConfigName == null || sConfigName.length() == 0) {
         logger.debug("NULL CONTEXT ID OR CONFIG NAME");
         throw new SecurityAdminException("7", "CONTEXT ID/CONFIG NAME REQUIRED");
      }
      inputParams.put(ContextVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ContextVO.ST_CONFIG_NAME, sConfigName);
       
      return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for 'Add Config'
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getAddConfigInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getAddConfigInputParams...");  
      return getUpdateConfigInputParams(request, updatedBy);     
  }  

  /**  
   * Builds and returns input paramter map to check if context config exists.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getConfigExistsInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getConfigExistsInputParams...");  
      return getViewConfigInputParams(request);
  }  
}