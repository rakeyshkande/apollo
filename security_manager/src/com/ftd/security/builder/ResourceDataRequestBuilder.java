package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.ResourceVO;
import com.ftd.security.exceptions.SecurityAdminException;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build Resource DataRequest input parameters.
 *  @author Christy Hu 
 */
public class ResourceDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.ResourceDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public ResourceDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**  
   * Builds and returns input paramter map for 'Add Resource'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getAddResourceInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getAddResourceInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sResourceId = (String)request.getParameter(ResourceVO.RQ_RESOURCE_ID);
      String sContextId = (String)request.getParameter(ResourceVO.RQ_CONTEXT_ID);
      String sDescription = (String)request.getParameter(ResourceVO.RQ_DESCRIPTION);
      logger.debug("Setting in_resource_id to " + sResourceId);
      logger.debug("Setting in_context_id to " + sContextId);  
      logger.debug("Setting in_description to " + sDescription);

      if(sResourceId == null || sResourceId.length() == 0
        || sContextId == null || sContextId.length() == 0) {
         logger.debug("NULL RESOURCE ID OR CONTEXT ID");
         throw new SecurityAdminException("7","RESOURCE ID AND CONTEXT REQUIRED");
      } 
      inputParams.put(ResourceVO.ST_UPDATED_BY, updatedBy);
      inputParams.put(ResourceVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ResourceVO.ST_RESOURCE_ID, sResourceId);
      inputParams.put(ResourceVO.ST_DESCRIPTION, sDescription);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'UpdateResource'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getUpdateResourceInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getUpdateResourceInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sResourceId = (String)request.getParameter(ResourceVO.RQ_RESOURCE_ID);
      String sContextId = (String)request.getParameter(ResourceVO.RQ_CONTEXT_ID);
      String sDescription = (String)request.getParameter(ResourceVO.RQ_DESCRIPTION);
      logger.debug("Setting in_resource_id to " + sResourceId);
      logger.debug("Setting in_context_id to " + sContextId);  
      logger.debug("Setting in_description to " + sDescription);

      if(sResourceId == null || sResourceId.length() == 0
        || sContextId == null || sContextId.length() == 0) {
         logger.debug("NULL RESOURCE ID OR CONTEXT ID");
         throw new SecurityAdminException("7","RESOURCE ID AND CONTEXT REQUIRED");
      } 
      inputParams.put(ResourceVO.ST_UPDATED_BY, updatedBy);
      inputParams.put(ResourceVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ResourceVO.ST_RESOURCE_ID, sResourceId);
      inputParams.put(ResourceVO.ST_DESCRIPTION, sDescription);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'View Resource'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getViewResourceInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getViewResourceInputParams...");
      HashMap inputParams = new HashMap();
       
      String sResourceId = (String)request.getParameter(ResourceVO.RQ_RESOURCE_ID);
      String sContextId = (String)request.getParameter(ResourceVO.RQ_CONTEXT_ID);
      logger.debug("Setting in_resource_id to " + sResourceId);
      logger.debug("Setting in_context_id to " + sContextId);       

      if(sResourceId == null || sResourceId.length() == 0
        || sContextId == null || sContextId.length() == 0) {
         logger.debug("NULL RESOURCE ID OR CONTEXT ID");
         throw new SecurityAdminException("7","RESOURCE ID AND CONTEXT REQUIRED");
      }
     
      inputParams.put(ResourceVO.ST_CONTEXT_ID, sContextId);
      inputParams.put(ResourceVO.ST_RESOURCE_ID, sResourceId);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'View Resources'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   **/   
  public HashMap getViewResourcesInputParams(HttpServletRequest request)
  {
      logger.debug("Entering getViewResourceInputParams...");  
      logger.debug("Setting in_context_id to null, type java.sql.Types.VARCHAR...");

      HashMap inputParams = new HashMap();
      // send null context id to get all resources.
      inputParams.put(ResourceVO.ST_CONTEXT_ID, Integer.valueOf(String.valueOf(java.sql.Types.VARCHAR)));
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map to check if resource exists.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getResourceExistsInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {       
      logger.debug("Entering getResourceExistsResourceInputParams...");
      return getViewResourceInputParams(request);
  }  

  /**  
   * Builds and returns input paramter map for 'Remove Resource'. Use the id string param 
   * as the parameter suffix to get parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @param id parameter suffix
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/ 
  public HashMap getRemoveResourceInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
        logger.debug("Entering getRemoveResourceInputParams...");
        HashMap inputParams = new HashMap();
        String sResourceId = request.getParameter(ResourceVO.RQ_REMOVE_RESOURCE_ID + id);
        String sContextId = request.getParameter(ResourceVO.RQ_REMOVE_CONTEXT_ID + id);
        logger.debug("Using remove id " + id + " to retrieve input...");
        logger.debug("Setting in_resource_id to " + sResourceId);
        logger.debug("Setting in_context_id to " + sContextId);

        if(sResourceId == null || sResourceId.length() == 0
          || sContextId == null || sContextId.length() == 0) {
           logger.debug("NULL RESOURCE ID OR CONTEXT ID");
           throw new SecurityAdminException("7", "RESOURCE ID AND CONTEXT REQUIRED");
        }       
        inputParams.put(ResourceVO.ST_RESOURCE_ID, sResourceId);
        inputParams.put(ResourceVO.ST_CONTEXT_ID, sContextId);
       
        return inputParams;
  }  

}