package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.AclVO;
import com.ftd.security.exceptions.SecurityAdminException;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build ACL DataRequest input parameters.
 *  @author Christy Hu 
 */
public class AclDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.AclDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public AclDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**  
   * Builds and returns input paramter map to get resources for a given context.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @param contextId context id chosen by user.
   * @throws SecurityAdminException
   * @throws Exception
   **/
  public HashMap getContextResourcesInputParams(HttpServletRequest request, String contextId) 
  throws SecurityAdminException, Exception {
        logger.debug("Entering getContextResourcesInputParams...");
        HashMap inputParams = new HashMap();
        String context_id = (String)request.getParameter(AclVO.RQ_CONTEXT_ID);

        if (context_id == null || context_id.length() == 0) {
            // User has not selected a context. Select the first.
            logger.debug("Setting in_context_id to the first in list...");
            context_id = contextId;
        }

        logger.debug("Setting in_context_id to " + context_id);
        inputParams.put(AclVO.ST_CONTEXT_ID, context_id);
        return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Add ACL'.
   * @param request HttpServletRequest to be analyzed.
   * @param aclName the acl name to which the new resource/permission is added.
   * @params resourceIndex resource id suffix to retrieve input data from.
   * @params permissionIndex permission id suffix to retrieve input data from.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/
  public HashMap getAddAclInputParams
    (HttpServletRequest request, String aclName, String resourceIndex, String permissionIndex, String updatedBy) 
    throws SecurityAdminException, Exception {
        logger.debug("Entering getAddAclInputParams...");
        HashMap inputParams = new HashMap();
        String context_id = (String)request.getParameter(AclVO.RQ_CONTEXT_ID);
        String resource_id = (String)request.getParameter(AclVO.RQ_RESOURCE_ID + resourceIndex);
        String permission_id = (String)request.getParameter
            (AclVO.RQ_RESOURCE_CB + resourceIndex + AclVO.RQ_PERMISSION_CB + permissionIndex);
        logger.debug("Setting in_context_id to " + context_id);
        logger.debug("Setting in_resource_id to " + resource_id);
        logger.debug("Setting in_permission_id to " + permission_id);
        logger.debug("Setting in_acl_name to " + aclName);

        if (context_id == null || context_id.length() == 0
            || resource_id == null || resource_id.length() == 0
            || permission_id == null || permission_id.length() == 0) {
            logger.error("MISSING REQUIRED FIELD(S)");
            throw new SecurityAdminException("7", "CONTEXT ID, RESOURCE ID, PERMISSION ID REQUIRED");
        }
        inputParams.put(AclVO.ST_UPDATED_BY, updatedBy);
        inputParams.put(AclVO.ST_CONTEXT_ID, context_id);
        inputParams.put(AclVO.ST_RESOURCE_ID, resource_id);
        inputParams.put(AclVO.ST_PERMISSION_ID, permission_id);
        inputParams.put(AclVO.ST_ACL_NAME, aclName);
                
        return inputParams;
  }  

  /**  
   * Builds and returns input paramter map to get resources for a given ACL name.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @throws Exception
   **/
  public HashMap getViewAclResourcesInputParams(HttpServletRequest request) 
    throws SecurityAdminException, Exception {
        logger.debug("Entering getViewAclResourcesInputParams...");
        HashMap inputParams = new HashMap();
        String context_id = (String)request.getParameter(AclVO.RQ_CONTEXT_ID);
        String acl_name = (String)request.getParameter(AclVO.RQ_ACL_NAME);
        logger.debug("Setting in_context_id to " + context_id);
        logger.debug("Setting in_acl_name to " + acl_name);

        if (context_id == null || context_id.length() == 0
            || acl_name == null || acl_name.length() == 0) {
            logger.error("INVALID ACL NAME OR CONTEXT ID");
            throw new SecurityAdminException("1");
        }

        inputParams.put(AclVO.ST_CONTEXT_ID, context_id);
        inputParams.put(AclVO.ST_ACL_NAME, acl_name);
        return inputParams;
  } 

  /**  
   * Builds and returns input paramter map for View ACL.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @throws Exception
   **/
  public HashMap getViewAclInputParams(HttpServletRequest request) 
    throws SecurityAdminException, Exception {
        logger.debug("Entering getViewAclInputParams...");
        HashMap inputParams = new HashMap();
        String acl_name = (String)request.getParameter(AclVO.RQ_ACL_NAME);
        logger.debug("Setting in_acl_name to " + acl_name);

        if (acl_name == null || acl_name.length() == 0) {
            logger.error("INVALID ACL NAME");
            throw new SecurityAdminException("1");
        }

        inputParams.put(AclVO.ST_ACL_NAME, acl_name);
        return inputParams;
  } 

  /**  
   * Builds and returns input paramter map to get all resources in a context.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/
  public HashMap getViewContextResourcesInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getViewContextResourcesInputParams...");
      HashMap inputParams = new HashMap();
      String contextId = (String) request.getParameter(AclVO.RQ_CONTEXT_ID);
      
      logger.debug("Setting in_context_id to " + contextId);  
      if (contextId == null || contextId.length() == 0) {
          logger.error("INVALID CONTEXT ID");
          throw new SecurityAdminException("1");
      }      
      inputParams.put(AclVO.ST_CONTEXT_ID, contextId);
       
      return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for remove ACL.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/
  public HashMap getRemoveAclInputParams(AclVO acl)
    throws SecurityAdminException
  {
      logger.debug("Entering getRemoveAclInputParams...");      
      HashMap inputParams = new HashMap();
      String contextId = acl.getContext();
      String resourceId = acl.getResource();
      String permissionId = acl.getPermission();
      String aclName = acl.getAclName();
      logger.debug("Setting in_context_id to " + contextId);
      logger.debug("Setting in_resource_id to " + resourceId);
      logger.debug("Setting in_permission_id to " + permissionId);
      logger.debug("Setting in_acl_name to " + aclName);
      
      try {
        inputParams.put(AclVO.ST_CONTEXT_ID, contextId);
        inputParams.put(AclVO.ST_RESOURCE_ID, resourceId);
        inputParams.put(AclVO.ST_PERMISSION_ID, permissionId);
        inputParams.put(AclVO.ST_ACL_NAME, aclName);
      } catch (Exception e) {
          logger.error("Missing required field.");
          throw new SecurityAdminException("7");
      }
      return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for remove ACL (name)
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws Exception
   **/
  public HashMap getRemoveAclNameInputParams(String acl_name)
    throws Exception
  {
      logger.debug("Entering getRemoveAclNameInputParams...");
      logger.debug("Setting in_acl_name to " + acl_name); 
      HashMap inputParams = new HashMap();
 
      inputParams.put(AclVO.ST_ACL_NAME, acl_name);
      
      return inputParams;
  }  
  
 
}