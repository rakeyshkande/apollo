package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.cache.vo.UserVO;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.StringUtil;
import com.ftd.security.constants.SecurityAdminConstants;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build User and Identity DataRequest input parameters.
 *  @author Christy Hu 
 */
public class UserDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.UserDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public UserDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**  
   * Builds and returns input paramter map for 'Add User'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getAddUserInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getAddUserInputParams...");
      HashMap inputParams = new HashMap();
      /*
      String sLastName = (String) request.getParameter(UserVO.RQ_LAST_NAME);
      String sFirstName = (String) request.getParameter(UserVO.RQ_FIRST_NAME);
      String sAddr = (String) request.getParameter(UserVO.RQ_ADDRESS_1);
      String sAddr2 = (String) request.getParameter(UserVO.RQ_ADDRESS_2);
      String sCity = (String) request.getParameter(UserVO.RQ_CITY);
      String sState = (String) request.getParameter(UserVO.RQ_STATE);
      String sZip = (String) request.getParameter(UserVO.RQ_ZIP);
      String sPhone = (String) request.getParameter(UserVO.RQ_PHONE);
      String sExpDate = (String) request.getParameter(UserVO.RQ_EXP_DATE);
      String sEmail = (String) request.getParameter(UserVO.RQ_EMAIL);
      String sFax = (String) request.getParameter(UserVO.RQ_FAX);
      String sExt = (String) request.getParameter(UserVO.RQ_PHONE_EXT);
      logger.debug("Setting in_last_name to: " +  sLastName);
      logger.debug("Setting in_first_name: " +  sFirstName);
      logger.debug("Setting in_address_1: " +  sAddr);
      logger.debug("Setting in_address_2: " +  sAddr2);
      logger.debug("Setting in_city: " +  sCity);
      logger.debug("Setting in_state: " +  sState);
      logger.debug("Setting in_zip: " +  sZip);
      logger.debug("Setting in_phone: " +  sPhone);
      logger.debug("Setting in_expire_date: " +  sExpDate);
      logger.debug("Setting in_email: " +  sEmail);
      logger.debug("Setting in_fax: " +  sFax);
      logger.debug("Setting in_extension: " +  sExt);
      logger.debug("Setting user_id to null, providing the type java.sql.Types.INTEGER");

      if(sLastName == null || sLastName.length() == 0 
        || sFirstName == null || sFirstName.length() == 0
        || sExpDate == null || sExpDate.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("7", "LAST NAME, FIRST NAME, AND EXPIRE DATE REQUIRED.");
        }
        
      try{  
        inputParams.put(UserVO.ST_LAST_NAME, sLastName);
        inputParams.put(UserVO.ST_FIRST_NAME, sFirstName);
        inputParams.put(UserVO.ST_ADDRESS_1, sAddr);
        inputParams.put(UserVO.ST_ADDRESS_2, sAddr2);
        inputParams.put(UserVO.ST_CITY, sCity);
        inputParams.put(UserVO.ST_STATE, sState);
        inputParams.put(UserVO.ST_ZIP, sZip);
        inputParams.put(UserVO.ST_PHONE, sPhone);
        inputParams.put(UserVO.ST_EXP_DATE, StringUtil.toSqlDate(sExpDate));
        inputParams.put(UserVO.ST_EMAIL, sEmail);
        inputParams.put(UserVO.ST_FAX, sFax);
        inputParams.put(UserVO.ST_PHONE_EXT, sExt);
        // Set user_id to null, providing the type.
        inputParams.put(UserVO.ST_USER_ID, Integer.valueOf(String.valueOf(java.sql.Types.INTEGER)));
      } catch (Exception ex) {
        logger.error(ex);
        throw new SecurityAdminException("1");
      }
      */
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Update User'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getUpdateUserInputParams(HttpServletRequest request)
    throws SecurityAdminException  
  {
        logger.debug("Entering getUpdateUserInputParams...");
        HashMap inputParams = new HashMap();
        /*
        String sUserId = (String)request.getParameter(UserVO.RQ_USER_ID);
        String sLastName = (String) request.getParameter(UserVO.RQ_LAST_NAME);
        String sFirstName =  (String) request.getParameter(UserVO.RQ_FIRST_NAME);
        String sAddr =  (String) request.getParameter(UserVO.RQ_ADDRESS_1);
        String sAddr2 =  (String) request.getParameter(UserVO.RQ_ADDRESS_2);
        String sCity =  (String) request.getParameter(UserVO.RQ_CITY);
        String sState =  (String) request.getParameter(UserVO.RQ_STATE);
        String sZip = (String) request.getParameter(UserVO.RQ_ZIP);
        String sPhone = (String) request.getParameter(UserVO.RQ_PHONE);
        String sExpDate = (String) request.getParameter(UserVO.RQ_EXP_DATE);
        String sEmail = (String) request.getParameter(UserVO.RQ_EMAIL);
        String sFax = (String) request.getParameter(UserVO.RQ_FAX);
        String sExt = (String) request.getParameter(UserVO.RQ_PHONE_EXT);
        logger.debug("Setting in_last_name to: " +  sLastName);
        logger.debug("Setting in_first_name: " +  sFirstName);
        logger.debug("Setting in_address_1: " +  sAddr);
        logger.debug("Setting in_address_2: " +  sAddr2);
        logger.debug("Setting in_city: " +  sCity);
        logger.debug("Setting in_state: " +  sState);
        logger.debug("Setting in_zip: " +  sZip);
        logger.debug("Setting in_phone: " +  sPhone);
        logger.debug("Setting in_expire_date: " +  sExpDate);
        logger.debug("Setting in_email: " +  sEmail);
        logger.debug("Setting in_fax: " +  sFax);
        logger.debug("Setting in_extension: " +  sExt);
        logger.debug("Setting user_id to: " + sUserId);

        try {
          inputParams.put(UserVO.ST_USER_ID, Integer.valueOf(sUserId));
        } catch (NumberFormatException ex) {
          logger.error(ex);
          throw new SecurityAdminException("1");
        } 
        
        if(sLastName == null || sLastName.length() == 0 
          || sFirstName == null || sFirstName.length() == 0
          || sExpDate == null || sExpDate.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("7", "LAST NAME, FIRST NAME, AND EXPIRE DATE REQUIRED.");
        }        
        inputParams.put(UserVO.ST_LAST_NAME, sLastName);
        inputParams.put(UserVO.ST_FIRST_NAME, sFirstName);
        inputParams.put(UserVO.ST_ADDRESS_1, sAddr);
        inputParams.put(UserVO.ST_ADDRESS_2, sAddr2);
        inputParams.put(UserVO.ST_CITY, sCity);
        inputParams.put(UserVO.ST_STATE, sState);
        inputParams.put(UserVO.ST_ZIP, sZip);
        inputParams.put(UserVO.ST_PHONE, sPhone);
        inputParams.put(UserVO.ST_EXP_DATE, StringUtil.toSqlDate(sExpDate));
        inputParams.put(UserVO.ST_EMAIL, sEmail);
        inputParams.put(UserVO.ST_FAX, sFax);
        inputParams.put(UserVO.ST_PHONE_EXT, sExt);
*/
        return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'View User'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getViewUserInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getViewUserInputParams...");
        HashMap inputParams = new HashMap();
        String userId = (String)request.getParameter(UserVO.RQ_USER_ID);        

        if (userId == null) {
          // This is a forwarded request.
          userId = (String)request.getAttribute(UserVO.RQ_USER_ID);
        }
        logger.debug("Setting user_id to: " + userId);

        if (userId == null || userId.length() == 0) {
          logger.error("user_id is null");
          throw new SecurityAdminException("1");
        }
        try {
          inputParams.put(UserVO.ST_IN_USER_ID, Integer.valueOf(userId));
        } catch (NumberFormatException ex) {
          logger.error(ex);
          throw new SecurityAdminException("1");
        }
        return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for 'View User Identities'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getViewUserIdentitiesInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getViewUserIdentitiesInputParams...");
        // same input as 'view user'.
        return getViewUserInputParams(request);
  }  

  /**  
   * Builds and returns input paramter map for 'Add Identity'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getAddIdentityInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
    logger.debug("Entering getAddIdentityInputParams...");
    HashMap inputParams = new HashMap();
     
    String sIdentityId = (String) request.getParameter(IdentityVO.RQ_ADD_IDENTITY_ID);   
    String sUserId =  (String) request.getParameter(IdentityVO.RQ_ADD_USER_ID);
    //String sCredentials =  (String) request.getParameter(IdentityVO.RQ_ADD_NEW_CREDENTIALS);
    String sDescription = (String) request.getParameter(IdentityVO.RQ_ADD_DESCRIPTION);
    //String sReCredentials =  (String) request.getParameter(IdentityVO.RQ_ADD_NEW_RECREDENTIALS);
    String sIdentityExpDate = (String) request.getParameter(IdentityVO.RQ_ADD_IDENTITY_EXP_DATE);
    String sCredentialExpDate = (String) request.getParameter(IdentityVO.RQ_ADD_CREDENTIALS_EXP_DATE);
    /*logger.debug("Setting in_identity_id to: " + sIdentityId);   
    logger.debug("Setting in_user_id to: " + sUserId);
    logger.debug("Setting in_description to: " + sDescription);
    logger.debug("Setting in_identity_expire_date to: " + sIdentityExpDate);*/

    if(sIdentityId == null || sIdentityId.length() == 0 
          || sIdentityExpDate == null || sIdentityExpDate.length() == 0) 
    {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException
              ("7", "IDENTITY, IDENTITY EXPIRE DATE REQUIRED.");
    }    
    if(sUserId == null || sUserId.length() == 0 )
    {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("1");      
    }
    /*
    if(!sCredentials.equals(sReCredentials) )
    {
            logger.error("Password and re-entered password don't match!");
            throw new SecurityAdminException("17");      
    }        
    */

    inputParams.put(IdentityVO.ST_IDENTITY_ID,sIdentityId);   
    inputParams.put(IdentityVO.ST_USER_ID, sUserId);
    //inputParams.put(IdentityVO.ST_OLD_CREDENTIALS, sCredentials);
    inputParams.put(IdentityVO.ST_DESCRIPTION, sDescription);
    //inputParams.put(IdentityVO.ST_NEW_CREDENTIALS, null);
    inputParams.put(IdentityVO.ST_IDENTITY_EXP_DATE, StringUtil.toSqlDate(sIdentityExpDate));
    inputParams.put(IdentityVO.ST_CREDENTIALS_EXP_DATE, StringUtil.toSqlDate(sCredentialExpDate));

    return inputParams;
  } 

  /**  
   * Builds and returns input paramter map for 'Update Identity'. The user interface
   * allows for multiple updates at one button click. This method processes one
   * of those updates. The string id is to be used as the parameter suffix 
   * to get parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getUpdateIdentityInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
        logger.debug("Entering getUpdateIdentityInputParams...");
        logger.debug("Id used to retrieve input parameter: " + id);
        HashMap inputParams = new HashMap();
        
        if (id == null || id.length() == 0) {
          logger.error("INVALID ID TO UPDATE IDENTITY");
          throw new SecurityAdminException("1");
        }
        
        //String credentials = (String) request.getParameter("credentials" + id);
        //String recredentials = (String) request.getParameter("recredentials" + id);
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_UPDATE_IDENTITY_ID + id);
        String sUserId = (String) request.getParameter(IdentityVO.RQ_UPDATE_USER_ID);
        String sDescription = (String) request.getParameter(IdentityVO.RQ_UPDATE_DESCRIPTION + id);
        String sIdentityExpDate = (String) request.getParameter(IdentityVO.RQ_UPDATE_IDENTITY_EXP_DATE + id);
        String sCredentialExpDate = (String) request.getParameter(IdentityVO.RQ_UPDATE_CREDENTIALS_EXP_DATE + id);

        /*logger.debug("Setting in_identity_id to : " + sIdentityId);   
        logger.debug("Setting in_user_id to : " + sUserId);
        logger.debug("Setting in_description to : " + sDescription);
        logger.debug("Setting in_identity_expire_date to : " + sIdentityExpDate);*/      

        if(sIdentityId == null || sIdentityId.length() == 0 
           || sIdentityExpDate == null || sIdentityExpDate.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException
              ("7", "IDENTITY, IDENTITY EXPIRE DATE REQUIRED.");
        }    
        if(sUserId == null || sUserId.length() == 0 )
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("1");      
        }
        
        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);   
        inputParams.put(IdentityVO.ST_USER_ID, sUserId);
        inputParams.put(IdentityVO.ST_DESCRIPTION, sDescription);
        //inputParams.put("in_old_credentials", String.valueOf(java.sql.Types.VARCHAR));
        //inputParams.put("in_new_credentials", String.valueOf(java.sql.Types.VARCHAR));
        //inputParams.put(IdentityVO.ST_OLD_CREDENTIALS, null);
        //inputParams.put(IdentityVO.ST_NEW_CREDENTIALS, null);
        inputParams.put(IdentityVO.ST_IDENTITY_EXP_DATE, StringUtil.toSqlDate(sIdentityExpDate));
        inputParams.put(IdentityVO.ST_CREDENTIALS_EXP_DATE, StringUtil.toSqlDate(sCredentialExpDate));
        
        return inputParams;
  }  

  /**  
   * Builds and returns input paramter map for 'Update Identity' by the administrator. 
   * The credentials are requested to be updated. 
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getAdminUpdateCredentialsInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
        logger.debug("Entering getAdminUpdateIdentityInputParams...");
        logger.debug("Id used to retrieve input parameter: " + id);
        HashMap inputParams = new HashMap();
        
        if (id == null || id.length() == 0) {
          logger.error("INVALID ID TO UPDATE IDENTITY");
          throw new SecurityAdminException("1");
        }
        
        //String credentials = (String) request.getParameter(IdentityVO.RQ_UPDATE_CREDENTIALS + id);
        //String recredentials = (String) request.getParameter(IdentityVO.RQ_UPDATE_RECREDENTIALS + id);
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_UPDATE_IDENTITY_ID + id);
        String adminReset = SecurityAdminConstants.COMMON_VALUE_YES;
        //String sUserId = (String) request.getParameter(IdentityVO.RQ_UPDATE_USER_ID);
        //String sDescription = (String) request.getParameter(IdentityVO.RQ_UPDATE_DESCRIPTION + id);
        //String sIdentityExpDate = (String) request.getParameter(IdentityVO.RQ_UPDATE_IDENTITY_EXP_DATE + id);
        //String sCredentialExpDate = (String) request.getParameter(IdentityVO.RQ_UPDATE_CREDENTIALS_EXP_DATE + id);

        // Check if passwords match.
        /*
        if (!credentials.equals(recredentials)) {
            logger.error("Password and re-entered password don't match!");
            throw new SecurityAdminException("17");      
        }
        */
        //logger.debug("Setting in_identity_id to : " + sIdentityId);        

        if(sIdentityId == null || sIdentityId.length() == 0 ) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException
              ("7", "IDENTITY REQUIRED.");
        }    
        
        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);   
        inputParams.put(IdentityVO.ST_OLD_CREDENTIALS, null);
        inputParams.put(IdentityVO.ST_NEW_CREDENTIALS, null);
        inputParams.put(IdentityVO.ST_NEW_RECREDENTIALS, null);
        inputParams.put(IdentityVO.ST_ADMIN_RESET, adminReset);
        
        return inputParams;
  }
  /**  
   * Builds and returns input paramter map for 'Remove User'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getRemoveUserInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getRemoveUserInputParams...");
        HashMap inputParams = new HashMap();
        String userId = (String)request.getParameter(UserVO.ST_USER_ID);
        logger.debug("Setting in_user_id to " + userId);

        if(userId == null || userId.length() == 0 )
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("1");      
        }        
        try {
          inputParams.put(UserVO.ST_IN_USER_ID, new Long(userId));
        } catch (NumberFormatException ex) {
          logger.error(ex);
          throw new SecurityAdminException("1");
        }   

        return inputParams;
  }     

  /**  
   * Builds and returns input paramter map for 'Remove Identity'. The user interface
   * allows for multiple removals at one button click. This method processes one
   * of those updates. The string id is to be used as the parameter suffix 
   * to get parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/ 
  public HashMap getRemoveIdentityInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
        logger.debug("Entering getRemoveIdentityInputParams...");
        logger.debug("Id used to retrieve input parameter: " + id);
        HashMap inputParams = new HashMap();
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_UPDATE_IDENTITY_ID + id);
        
        //logger.debug("Setting in_identity_id to: " + sIdentityId);
        if (id == null || id.length() == 0) {
          logger.error("INVALID IDENTITY ID TO REMOVE");
          throw new SecurityAdminException("1");
        }
        
        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);   

        return inputParams;
  }   

  /**  
   * Builds and returns input paramter map for 'View Identity Role'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/  
  public HashMap getViewIdentityRolesInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getViewIdentityRolesInputParams...");
        HashMap inputParams = new HashMap();
        String sIdentityId = request.getParameter(IdentityVO.RQ_ROLE_IDENTITY_ID);
        
        //logger.debug("Setting in_identity_id to: " + sIdentityId);
        if(sIdentityId == null || sIdentityId.length() == 0 )
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException("1");      
        }

        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);
        return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Add Identity Role'. The user interface
   * allows for multiple associations at one button click. This method processes one
   * of those updates. The string id is to be used as the parameter suffix 
   * to get parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/ 
  public HashMap getAddIdentityRoleInputParams(HttpServletRequest request, String id) 
    throws SecurityAdminException
  {
        logger.debug("Entering getAddIdentityRoleInputParams...");
        HashMap inputParams = new HashMap();
        String sIdentityId = (String)request.getParameter(IdentityVO.RQ_ROLE_IDENTITY_ID);
        String sRoleId = request.getParameter(IdentityVO.RQ_ROLE_ADD_ROLE_ID + id);
        
        if (id == null || id.length() == 0) {
          logger.error("INVALID IDENTITY ROLE ID TO ADD");
          throw new SecurityAdminException("1");
        }
        
        //logger.debug("Setting in_identity_id to: " + sIdentityId);
        //logger.debug("Setting in_role_id to: " + sRoleId);
        
        inputParams.put(IdentityVO.ST_IDENTITY_ID,(String) request.getParameter(IdentityVO.RQ_ROLE_IDENTITY_ID));   
        try {
          inputParams.put(IdentityVO.ST_ROLE_ID, new Long(sRoleId));
        } catch (NumberFormatException e){
          logger.error(e);
          throw new SecurityAdminException("1");
        }        
        return inputParams;
  } 

  /**  
   * Builds and returns input paramter map for 'Remove Identity Role'. The user interface
   * allows for multiple removals at one button click. This method processes one
   * of those updates. The string id is to be used as the parameter suffix 
   * to get parameters from request.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/ 
  public HashMap getRemoveIdentityRoleInputParams(HttpServletRequest request, String id) 
    throws SecurityAdminException
  {
        logger.debug("Entering getRemoveIdentityRoleInputParams...");
        logger.debug("Id used to retrieve input parameter: " + id);
        HashMap inputParams = new HashMap();
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_ROLE_IDENTITY_ID);
        String sRoleId = request.getParameter(IdentityVO.RQ_ROLE_REMOVE_ROLE_ID + id);
        
        if (id == null || id.length() == 0
            || sIdentityId == null || sIdentityId.length() == 0
            || sRoleId == null || sRoleId.length() == 0) {
          logger.error("INVALID IDENTITY ROLE ID TO REMOVE");
          throw new SecurityAdminException("1");
        }

        /*logger.debug("Setting in_identity_id to: " + sIdentityId);
        logger.debug("Setting in_role_id to: " + sRoleId);*/        
        inputParams.put(IdentityVO.ST_IDENTITY_ID,sIdentityId);   
        try {
          inputParams.put(IdentityVO.ST_ROLE_ID, new Long(sRoleId));
        } catch (NumberFormatException e) {
          logger.error(e);
          throw new SecurityAdminException("1");
        }
        
        return inputParams;
  }   

  /**  
   * Builds and returns input paramter map for querying if identity exists.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   * @deprecated
   **/ 
  public HashMap getIdentityExistsInputParams(HttpServletRequest request) 
    throws SecurityAdminException
  {
        logger.debug("Entering getIdentityExistsInputParams...");
        HashMap inputParams = new HashMap(); 
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_ADD_IDENTITY_ID);

        //logger.debug("Setting in_identity_id to: " + sIdentityId);
        // Check for mandatory fields.
        if(sIdentityId == null || sIdentityId.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException
              ("7", "IDENTITY, IDENTITY EXPIRE DATE, AND PASSWORD EXPIRE DATE REQUIRED.");
        }           
        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);  
        return inputParams;
  }     

  /**  
   * Builds and returns input paramter map for 'Manage Account'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/  
  public HashMap getViewIdentityInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getViewIdentityInputParams...");
        HashMap inputParams = new HashMap();
        String identityId = (String)request.getParameter(IdentityVO.RQ_ACCOUNT_IDENTITY_ID);         
        //logger.debug("Setting identity_id to: " + identityId);

        if (identityId == null || identityId.length() == 0) {
          logger.error("identity is null");
          throw new SecurityAdminException("1");
        }
       
        inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);
        return inputParams;
  }   

  /**  
   * Builds and returns input paramter map for 'Update Credentials'. 
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/  
  public HashMap getUserUpdateCredentialsInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
        logger.debug("Entering getUserUpdateCredentialsInputParams...");
        HashMap inputParams = new HashMap();
        
        String oldCredentials = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_OLDCREDENTIALS);
        String newCredentials = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_NEWCREDENTIALS);
        String newRecredentials = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_NEWRECREDENTIALS);
        String sIdentityId = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_IDENTITY_ID);
        String adminReset = SecurityAdminConstants.COMMON_VALUE_NO;
        //String sUserId = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_USER_ID);
        //String sDescription = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_DESCRIPTION);
        //String sIdentityExpDate = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_IDENTITY_EXP_DATE);
        //String sCredentialExpDate = (String) request.getParameter(IdentityVO.RQ_ACCOUNT_CREDENTIALS_EXP_DATE);
        /*logger.debug("Setting in_update_by to : " + updatedBy);
        logger.debug("Setting in_identity_id to : " + sIdentityId);   
        logger.debug("Setting in_old_credentials to : " + oldCredentials);
        logger.debug("Setting in_new_credentials to : " + newCredentials);*/       

        if(sIdentityId == null || sIdentityId.length() == 0) 
        {
            logger.error("MISSING REQUIRED FIELD");
            throw new SecurityAdminException
              ("7", "IDENTITY REQUIRED.");
        }    
        inputParams.put(IdentityVO.ST_UPDATED_BY, updatedBy);
        inputParams.put(IdentityVO.ST_IDENTITY_ID, sIdentityId);   
        inputParams.put(IdentityVO.ST_OLD_CREDENTIALS, oldCredentials);
        inputParams.put(IdentityVO.ST_NEW_CREDENTIALS, newCredentials);
        inputParams.put(IdentityVO.ST_NEW_RECREDENTIALS, newRecredentials);
        inputParams.put(IdentityVO.ST_ADMIN_RESET, adminReset);
        return inputParams;
  }  


  /**  
   * Builds and returns input paramter map for 'Search User'. 
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/  
  public HashMap getSearchUserInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
        logger.debug("Entering getSearchUserInputParams...");
        HashMap inputParams = new HashMap();
        
        String searchOption = request.getParameter(UserVO.RQ_SEARCH_OPTION);
        String searchKey = request.getParameter(UserVO.RQ_SEARCH_KEY);

        logger.debug("searchOption is " + searchOption);
        if(searchKey == null || searchKey.length() == 0) {
            //error: Please enter a value.
        }
        
        if(UserVO.RQ_LAST_NAME.equals(searchOption)) {
            logger.debug("Setting in_last_name to : " + searchKey);
            inputParams.put(UserVO.ST_IN_IDENTITY_ID, null);
            inputParams.put(UserVO.ST_LAST_NAME, searchKey);
        } else if(UserVO.RQ_IDENTITYID.equals(searchOption)) {
            //logger.debug("Setting in_identity_id to : " + searchKey); 
            inputParams.put(UserVO.ST_IN_IDENTITY_ID, searchKey);
            inputParams.put(UserVO.ST_LAST_NAME, null);
        } else {
            //error:Please select a valid option.
        }

        return inputParams;
  }    
}