package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.ResourceVO;
import com.ftd.security.exceptions.SecurityAdminException;
import java.util.HashMap;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.MenuConstants;
import javax.servlet.http.HttpServletRequest;
import com.ftd.security.menus.*;

/**
 *  Helper class to build authorized resource and permission.
 *  @author Christy Hu
 */
public class AuthorizationBuilder {
  private static Logger logger = new Logger("com.ftd.security.builder.AuthorizationBuilder");

  /**
   * Constuctor to initialize logging service.
   **/
  public AuthorizationBuilder()
  {
  }

  /**
   * Builds authorization parameters based on the page to be presented.
   * @param request HttpServletRequest to be analyzed.
   * @param stylesheetName menu stylesheet name
   * @returns HashMap map of permissions and resources the user is authorized.
   * @throws SecurityAdminException
   **/
  public static HashMap getAuthorizationParams(HttpServletRequest request, String stylesheetName)
    throws SecurityAdminException, Exception
  {
      logger.debug("Entering getAuthorizationParams...");
      HashMap authorizationParams = null;
      String securityToken = request.getParameter(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      String context = request.getParameter(SecurityAdminConstants.COMMON_PARM_CONTEXT);

      if (context == null || context.length() == 0)  {
          context = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_CONTEXT);
      }      
      if (securityToken == null || securityToken.length() == 0)  {
          securityToken = (String)request.getAttribute(SecurityAdminConstants.COMMON_PARM_SEC_TOKEN);
      }      

      authorizationParams = authorizePage(securityToken, context, stylesheetName);
      return authorizationParams;
  }

  /**
   * Builds authorization parameters based on the page to be presented.
   * @param securityToken security token
   * @param context context
   * @param stylesheetName menu stylesheet name
   * @returns HashMap map of permissions and resources the user is authorized.
   * @throws Exception
   **/
  private static HashMap authorizePage(String securityToken, String context, String stylesheetName) 
  throws Exception {
      HashMap authorizationParams = null;
      MenuObject menu = new MainMenu(); //default

      if (MenuConstants.XSL_CUSTOMERSERVICE_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new CustomerServiceMenu();
      } else if (MenuConstants.XSL_OPERATIONS_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new OperationsMenu();
      } else if (MenuConstants.XSL_MERCHANDISING_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new MerchandisingMenu();
      } else if (MenuConstants.XSL_MARKETING_MENU.equalsIgnoreCase(stylesheetName)) {         
          menu = new MarketingMenu();
      } else if (MenuConstants.XSL_ACCOUNTING_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new AccountingMenu();
      } else if (MenuConstants.XSL_SYSTEM_ADMIN_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new SystemAdministrationMenu();
      } else if (MenuConstants.XSL_SECURITY_ADMIN_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new SecurityAdministrationMenu();
      } else if (MenuConstants.XSL_DISTRIBUTION_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new DistributionFulfillmentMenu();
      }else if (MenuConstants.XSL_WORKFORCE_REPORTS_MENU.equalsIgnoreCase(stylesheetName)) {
          menu = new WorkforceReportsMenu();
      }
      //else if (MenuConstants.XSL_MAINMENU.equalsIgnoreCase(stylesheetName)) {}
      authorizationParams = menu.getAuthorizedResources(securityToken, context);
      return authorizationParams;
  }
}