package com.ftd.security.builder;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.PermissionVO;
import com.ftd.security.exceptions.SecurityAdminException;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/** 
 *  Helper class to build Permission DataRequest input parameters.
 *  @author Christy Hu 
 */
public class PermissionDataRequestBuilder {
  private static Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.security.builder.PermissionDataRequestBuilder";

  /**  
   * Constuctor to initialize logging service.
   **/
  public PermissionDataRequestBuilder()
  {
      logger = new Logger(LOGGER_CATEGORY);
  }

  /**  
   * Builds and returns input paramter map for 'Add Permission'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/     
  public HashMap getAddPermissionInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getAddPermissionInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sPermissionId = (String)request.getParameter(PermissionVO.RQ_PERMISSION_ID);
      String sDescription = (String)request.getParameter(PermissionVO.RQ_DESCRIPTION);
      logger.debug("Setting in_permission_id to " + sPermissionId);
      logger.debug("Setting in_description to " + sDescription);

      if(sPermissionId == null || sPermissionId.length() == 0) {
         logger.debug("NULL PERMISSION ID");
         throw new SecurityAdminException("7", "PERMISSION ID REQUIRED");
      }
      inputParams.put(PermissionVO.ST_UPDATED_BY, updatedBy);
      inputParams.put(PermissionVO.ST_PERMISSION_ID, sPermissionId);
      inputParams.put(PermissionVO.ST_DESCRIPTION, sDescription);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map for 'Update Permission'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getUpdatePermissionInputParams(HttpServletRequest request, String updatedBy)
    throws SecurityAdminException
  {
      logger.debug("Entering getUpdatePermissionInputParams...");  
      return getAddPermissionInputParams(request, updatedBy);
  }

  /**  
   * Builds and returns input paramter map for 'View Permission'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/     
  public HashMap getViewPermissionInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {
      logger.debug("Entering getViewPermissionInputParams...");  
      HashMap inputParams = new HashMap();
       
      String sPermissionId = (String)request.getParameter(PermissionVO.RQ_PERMISSION_ID);
      logger.debug("Setting in_permission_id to " + sPermissionId);

      if(sPermissionId == null || sPermissionId.length() == 0) {
         logger.debug("NULL PERMISSION ID");
         throw new SecurityAdminException("7", "PERMISSION ID REQUIRED");
      }
      inputParams.put(PermissionVO.ST_PERMISSION_ID, sPermissionId);
       
      return inputParams;
  }

  /**  
   * Builds and returns input paramter map to check if a permission exists.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @throws SecurityAdminException
   **/   
  public HashMap getPermissionExistsInputParams(HttpServletRequest request)
    throws SecurityAdminException
  {   
      logger.debug("Entering getPermissionExistsInputParams...");  
      return getViewPermissionInputParams(request);
  }  

  /**  
   * Builds and returns input paramter map for 'Remove Permission'.
   * @param request HttpServletRequest to be analyzed.
   * @returns HashMap input paramter map to be set on DataRequest to make SQL procedure call.
   * @id parameter suffix
   * @throws SecurityAdminException
   **/   
  public HashMap getRemovePermissionInputParams(HttpServletRequest request, String id)
    throws SecurityAdminException
  {
      logger.debug("Entering getRemovePermissionInputParams...");
      HashMap inputParams = new HashMap();
      
      logger.debug("Using remove id "+ id + " to retrieve input parameters...");
      String sPermissionId = (String)request.getParameter(PermissionVO.RQ_REMOVE_PERMISSION_ID + id);
      
      if(sPermissionId == null || sPermissionId.length() == 0) {
         logger.debug("NULL PERMISSION ID");
         throw new SecurityAdminException("7", "PERMISSION ID REQUIRED");
      }
      inputParams.put(PermissionVO.ST_PERMISSION_ID, sPermissionId);
       
      return inputParams;
  }  

}