package com.ftd.security.manager;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.IdentityVO;
import com.ftd.security.cache.vo.RoleVO;
import com.ftd.security.cache.vo.UserLineItemVO;
import com.ftd.security.cache.vo.UserVO;
import com.ftd.security.constants.SecurityAdminConstants;
import com.ftd.security.constants.StatementConstants;
import com.ftd.security.exceptions.DuplicateObjectException;
import com.ftd.security.exceptions.SecurityAdminException;
import com.ftd.security.util.DataRequestHelper;
import com.ftd.security.util.StringUtil;
import com.ftd.security.util.XMLHelper;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.ftd.osp.utilities.xml.DOMUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Class that manages create and update user and identity.
 * @author Christy Hu
 */
public class UserManager{
    private static Logger logger = new Logger("com.ftd.security.manager.UserManager");
    private UserVO user;
    private IdentityVO identity;

    public UserManager(){}
    
    public UserManager(UserVO uvo, IdentityVO ivo) {
        user = uvo;
        identity = ivo;
    }

    /**
     * Returns the CS groups document.
     */
     public static Document getCsGroupsDoc() throws Exception {
          return (Document)DataRequestHelper.executeStatement(StatementConstants.USERS_VIEW_CSGROUPS, null);              
     }  
    
    /**
     * Returns the departments document.
     */
     public static Document getDepartmentsDoc() throws Exception {
          return (Document)DataRequestHelper.executeStatement(StatementConstants.USERS_VIEW_DEPARTMENTS, null);              
     }   

    /**
     * Returns the call centers document.
     */
     public static Document getCallCentersDoc() throws Exception {
          return (Document)DataRequestHelper.executeStatement(StatementConstants.USERS_VIEW_CALLCENTERS, null);              
     }   




    /**
     * Returns the contexts document.
     */
     public static Document getContextsDoc() throws Exception {
          return (Document)DataRequestHelper.executeStatement(StatementConstants.CONTEXT_VIEW_CONTEXTS, null);
     }   

     /**
      * Adds the user in system and returns the user id.
      */
     public int addUser(Object securityToken, Connection con) throws Exception {
          try {
            HashMap inputParams = user.getAddUserInputParams(securityToken);
            HashMap outputMap = (HashMap)DataRequestHelper.executeStatement
                      (con, inputParams, StatementConstants.USERS_ADD_USERS); 
            String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
            BigDecimal userId = (BigDecimal)outputMap.get(UserVO.RQ_USER_ID);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO ADD USER.");
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
                } else {
                    throw new SecurityAdminException("12", message);
                }
            } else if (userId == null) {
              logger.error("USER ID IS NULL AFTER SUCCESSFUL INSERT");
              throw new SecurityAdminException("6");
            }     
            return userId.intValue();
          } catch (Exception e){
              throw e;
          }
     }   

     /**
     * Returns the context roles document.
     */
     public Document getContextRolesDoc() throws Exception {
          HashMap parameters = new HashMap();
          parameters.put("in_context_id", identity.getContextId());
          return (Document)DataRequestHelper.executeStatement(StatementConstants.ROLES_VIEW_ROLES_BY_CONTEXT, parameters);
     }     


  /**
   * Checks if the identity exists.
   */
  public boolean identityExists(Connection con) throws Exception {
      try {
          // Check if identity exists.
          HashMap inputParams = identity.getIdentityExistsInputParams();
          String status = (String)DataRequestHelper.executeStatement
                      (con, inputParams, StatementConstants.USERS_IDENTITY_EXISTS);          

          return "Y".equalsIgnoreCase(status);
      } catch (Exception e) {
          throw e;
      }
  }

  
  /**
   * Add an identity to the user in the system.
   * @throws DuplicateObjectException when identity exists
   * @return generated password for the new identity
   */
  public String addIdentity(Object securityToken, Connection con, int userId) 
  throws DuplicateObjectException, Exception {
      String credential = null;
      
      try {
          if (identityExists(con)) {
              throw new DuplicateObjectException();
          }
          
          HashMap inputParams = identity.getAddIdentityInputParams(securityToken, userId);
          HashMap outputMap = (HashMap)DataRequestHelper.executeStatement
                      (con, inputParams, StatementConstants.USERS_ADD_IDENTITY);
          String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
          String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
          //credential = "foobar";
          credential = (String)outputMap.get(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);
              
          logger.error("status = " + status);
          logger.error("message = " + message);
          // check status
          if (!"Y".equalsIgnoreCase(status))  {
              logger.debug("FAILED TO ADD IDENTITY.");
              // "ERROR OCCURRED" is an unexpected error.
              if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
              } else {
                    throw new SecurityAdminException("12", message);
              }
          }
      } catch (Exception e){
          throw e;
      }
      
      return credential;
  }  
 
  /**
   * Associate selected roles to identity.
   */
  public void addIdentityRoles(Object securityToken, Connection con) 
  throws Exception {
      try {      
          String[] roleIdList = identity.getRoleIds();
          if (roleIdList != null) {
              HashMap inputParams = null;
              HashMap outputMap = null;
              String status = null;
              String message = null;
              
              for (int i = 0; i < roleIdList.length; i++) {
                  inputParams = identity.getAddIdentityRoleInputParams(securityToken, roleIdList[i]);
                  
                  outputMap = (HashMap)DataRequestHelper.executeStatement
                      (con, inputParams, StatementConstants.ROLE_UPDATE_IDENTITY_ROLE);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS);
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
                  logger.error("status = " + status);
                  logger.error("message = " + message);
                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO ADD IDENTITY ROLE.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                            throw new SecurityAdminException("6");
                      } else {
                            throw new SecurityAdminException("12", message);
                      }
                  }                  
              }
          }
      } catch (Exception e){
          throw e;
      }
  }   

  /**
   * Remove identity role association.
   */
  public void removeIdentityRoles(Object securityToken, Connection con) 
  throws Exception {
      try {      
          String[] roleIdList = identity.getRoleIds();
          if (roleIdList != null) {
              HashMap inputParams = null;
              HashMap outputMap = null;
              String status = null;
              String message = null;
              
              for (int i = 0; i < roleIdList.length; i++) {
                  // ???need updated_by?
                  inputParams = identity.getRemoveIdentityRoleInputParams(securityToken, roleIdList[i]);
                  
                  outputMap = (HashMap)DataRequestHelper.executeStatement
                      (con, inputParams, StatementConstants.ROLE_REMOVE_IDENTITY_ROLE);
                  status = (String)outputMap.get(SecurityAdminConstants.STATUS);
                  message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
                  logger.error("status = " + status);
                  logger.error("message = " + message);
                  // check status
                  if (!"Y".equalsIgnoreCase(status))  {
                      logger.debug("FAILED TO REMOVE IDENTITY ROLE.");
                      // "ERROR OCCURRED" is an unexpected error.
                      if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                            throw new SecurityAdminException("6");
                      } else {
                            throw new SecurityAdminException("12", message);
                      }
                  }                  
              }
          }
      } catch (Exception e){
          throw e;
      }
  }   
  /**
   *  Builds the document transformed when the request is display_add user.
   */ 
  public Document buildDisplayAddUserDoc() throws Exception {
      Document doc = null;
      Document cdoc = null; // context document
      Document ddoc = null; // department document
      Document ccdoc = null; // call center document
      Document csgdoc = null; // cs group document

      try {
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            cdoc = getContextsDoc();
            ddoc = getDepartmentsDoc();
            ccdoc = getCallCentersDoc();
            csgdoc = getCsGroupsDoc();
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            }            
            if (ddoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ddoc.getDocumentElement(), true));
            }
            if (ccdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ccdoc.getDocumentElement(), true));
            }
            if (csgdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(csgdoc.getDocumentElement(), true));  
            }   
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }


  /**
   *  Builds the document transformed when the request is display_add user.
   */ 
  public Document buildRefreshDisplayAddUserDoc() throws Exception {
      Document doc = null;
      Document ufdoc = null; // user field document
      Document ifdoc = null; // identity field document
      Document rdoc = null; // role document
      Document cdoc = null; // context document
      Document ddoc = null; // department document
      Document ccdoc = null; // call center document
      Document csgdoc = null; // cs group document  

      try {
            ufdoc = user.getFieldDoc();
            ifdoc = identity.getFieldDoc();
            cdoc = getContextsDoc();
            rdoc = getContextRolesDoc();
            //rdoc = identity.markSelectedRoles(rdoc);           
            ddoc = getDepartmentsDoc();
            ccdoc = getCallCentersDoc();
            csgdoc = getCsGroupsDoc();
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            if(ufdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ufdoc.getDocumentElement(), true));
            }
            if(ifdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ifdoc.getDocumentElement(), true));
            }            
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            }            
            if (rdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
            }
            if (ddoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ddoc.getDocumentElement(), true));
            }
            if (ccdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ccdoc.getDocumentElement(), true));
            }
            if (csgdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(csgdoc.getDocumentElement(), true));  
            }
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }


  /**
   *  Builds the document transformed when the request is add and the request is invalid.
   */ 
  public Document buildInvalidAddUserRequestDoc(Exception ex) throws Exception {  
      Document doc = null;
      try {
          doc = rebuildUserIdentityTree(ex);
          Document cdoc = getContextsDoc();
          Document  rdoc = getContextRolesDoc();
          rdoc = identity.markSelectedRoles(rdoc);            
          Document ddoc = getDepartmentsDoc();
          Document ccdoc = getCallCentersDoc();
          Document csgdoc = getCsGroupsDoc();
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            }            
            if (rdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
            }
            if (ddoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ddoc.getDocumentElement(), true));
            }
            if (ccdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ccdoc.getDocumentElement(), true));
            }
            if (csgdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(csgdoc.getDocumentElement(), true));  
            }
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }

  /**
   *  Rebuilds the user and identity section of tree when request is invalid.
   */
  private Document rebuildUserIdentityTree
  (Exception e) throws Exception {
      Document udoc = null;
      Document idoc = null;
      Document doc = null;

      udoc = user.rebuildTree(e);
      idoc = identity.rebuildTree(e);
      doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);

      Element uRootElem = udoc.getDocumentElement();
      Element iRootElem = idoc.getDocumentElement();

      NodeList uList = uRootElem.getChildNodes();
      NodeList iList = iRootElem.getChildNodes();

      // Merge documents
      if (uList != null) {
          // Add the ERRORS section from identity to user if identity errored.
          for (int i = 0; i < uList.getLength(); i++) {
              doc.getDocumentElement().appendChild(doc.importNode(uList.item(i), true));
          }          
      } 

      if (iList != null) {
          // Add the ERRORS section from identity to user if identity errored.
          for (int i = 0; i < iList.getLength(); i++) {
              doc.getDocumentElement().appendChild(doc.importNode(iList.item(i), true));
          }          
      }      
      return doc;
  }  

  /**
   * Initialize UserVO object from document.
   */
      public void initUserFromDoc(Document doc) throws Exception {
      //((XMLDocument)doc).print(System.out);
          String path = "/" + UserVO.XML_TOP + "/" + UserVO.XML_BOTTOM + "/";  // path = "/USERS/USER/"
          user = new UserVO();
          
          user.setFirstName((DOMUtil.selectSingleNode(doc, path + UserVO.TB_FIRST_NAME).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_FIRST_NAME).getFirstChild().getNodeValue());
          user.setLastName((DOMUtil.selectSingleNode(doc, path + UserVO.TB_LAST_NAME).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_LAST_NAME).getFirstChild().getNodeValue());
          user.setPhone((DOMUtil.selectSingleNode(doc, path + UserVO.TB_PHONE).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_PHONE).getFirstChild().getNodeValue());
          user.setPhoneExt((DOMUtil.selectSingleNode(doc, path + UserVO.TB_PHONE_EXT).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_PHONE_EXT).getFirstChild().getNodeValue());
          user.setEmail((DOMUtil.selectSingleNode(doc, path + UserVO.TB_EMAIL).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_EMAIL).getFirstChild().getNodeValue());
          user.setCallCenter((DOMUtil.selectSingleNode(doc, path + UserVO.TB_CALL_CENTER).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_CALL_CENTER).getFirstChild().getNodeValue());
          user.setDepartment((DOMUtil.selectSingleNode(doc, path + UserVO.TB_DEPARTMENT).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_DEPARTMENT).getFirstChild().getNodeValue());
          user.setCsGroup((DOMUtil.selectSingleNode(doc, path + UserVO.TB_CS_GROUP).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_CS_GROUP).getFirstChild().getNodeValue());
          user.setEmployeeId((DOMUtil.selectSingleNode(doc, path + UserVO.TB_EMPLOYEE_ID).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_EMPLOYEE_ID).getFirstChild().getNodeValue());
		  user.setSupervisorId((DOMUtil.selectSingleNode(doc, path + UserVO.TB_SUPERVISOR_ID).getFirstChild() == null)?"":DOMUtil.selectSingleNode(doc, path + UserVO.TB_SUPERVISOR_ID).getFirstChild().getNodeValue());
      }


  /**
   * Initialize UserVO object from document.
   */
      public void initIdentityFromDoc(Document doc) throws Exception {
          String path = "/" + IdentityVO.XML_TOP + "/" + IdentityVO.XML_BOTTOM + "/";  // path = "/IDENTITIES/IDENTITY/"
          identity = new IdentityVO();
          
          identity.setIdentityId((DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_IDENTITY_ID) == null)?"":DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_IDENTITY_ID).getFirstChild().getNodeValue());
          identity.setIdExpDate((DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_ID_EXPIRE_DATE) == null)?"":DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_ID_EXPIRE_DATE).getFirstChild().getNodeValue());
          identity.setPwdExpDate((DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_PWD_EXPIRE_DATE) == null)?"":DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_PWD_EXPIRE_DATE).getFirstChild().getNodeValue());
          identity.setLockedFlag((DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_LOCKED_FLAG) == null)?"":DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_LOCKED_FLAG).getFirstChild().getNodeValue());            
      }      
  /**
   *  Builds the document transformed when the request is view user.
   */ 
  public Document buildViewUserDoc(HttpServletRequest request, Connection con) throws Exception { 
      Document doc = null; // return doc
      Document udoc = null; // user doc
      Document fdoc = null; // user field document
      Document idoc = null; // identity doc
      Document ddoc = getDepartmentsDoc();
      Document ccdoc = getCallCentersDoc();
      Document csgdoc = getCsGroupsDoc();
      Integer userId = null;
      
      try {
          doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
          // get user identity role section
          String sUserId = (String)request.getParameter(UserVO.RQ_USER_ID);
          logger.debug("Retrieved sUserId from request = " + sUserId);
          if (sUserId == null || sUserId.length() == 0) {
              sUserId = String.valueOf(((Integer)request.getAttribute(UserVO.RQ_USER_ID)).intValue());
          } 
          udoc = getViewUserDoc(sUserId, con);  
          // Convert the user document to field document.
          this.initUserFromDoc((Document)udoc);
          //initUserFromDoc created a new User. Populated info from database
          //instead fetching supervisor Identity name from DB, 
//          user.setSupervisorIdentityName(request.getParameter(UserVO.RQ_SUPERVISOR_ID));
          fdoc = user.getFieldDoc();
          idoc = getViewUserIdentitiesDoc(sUserId, con);
          udoc = mergeUserIdentityRole(udoc, idoc, con);

          // add drop down data sections
          if (ddoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(ddoc.getDocumentElement(), true));
          }
          if (ccdoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(ccdoc.getDocumentElement(), true));
          }
          if (csgdoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(csgdoc.getDocumentElement(), true));  
          }   
          if (fdoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(fdoc.getDocumentElement(), true));  
          }  
          if (udoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));  
          }
          printDocument(doc);
//          ((XMLDocument)doc).print(System.out);
          return doc;
      } catch (Exception e) {
          throw e;
      }      
  }

  public static void printDocument(Document doc) throws IOException, TransformerException {
      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer transformer = tf.newTransformer();
      transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
      transformer.setOutputProperty(OutputKeys.METHOD, "xml");
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
      StringWriter outWriter = new StringWriter();
      StreamResult result = new StreamResult( outWriter );
      transformer.transform(new DOMSource(doc), result );  
      StringBuffer sb = outWriter.getBuffer(); 
      
      logger.info("printDocument :::: " + sb.toString());
      
  }
  /**
   *  Builds the document transformed when the request is view user.
   */ 
  public Document buildUserBatchLoadDoc(HttpServletRequest request, String message) throws Exception { 
      Document doc = null; // return doc
      Document fdoc = null; // fields
	  ArrayList newUsers = null;
      
      try {
          doc = XMLHelper.getDocumentWithAttribute(SecurityAdminConstants.XML_ROOT, SecurityAdminConstants.MESSAGE, message);

          // get list of new users
          newUsers = (ArrayList) request.getAttribute(SecurityAdminConstants.COMMON_PARM_USER_LIST);
          logger.debug("Retrieved newUser from request");
          if (newUsers != null) {
			  UserLineItemVO item = null;

              for(int i=0; i < newUsers.size(); i++) {
                  item = (UserLineItemVO) newUsers.get(i);
                  fdoc = item.getFieldDoc();

                  if (fdoc != null) {
                      doc.getDocumentElement().appendChild(doc.importNode(fdoc.getDocumentElement(), true));  
                  }
              }
          } 

          DOMUtil.print(doc, System.out);
      } catch (Exception e) {
          throw e;
      }      

      return doc;
  }

  /**
   * Create the identity and role subtree given the user document.
   * Remove the ROLE subtree if ROLE/identity_id is null.
   */
  public Document mergeUserIdentityRole(Document udoc, Document idoc, Connection con) throws Exception {
      try 
      {
          NodeList inl = idoc.getElementsByTagName(IdentityVO.XML_BOTTOM);
          if(inl != null) {
              for(int i = 0; i < inl.getLength(); i++)
              {
                  //get roles by identity_id
                  // get identity_id element off IDENTITY
                  Element identityIdElem = (Element)(((Element)(inl.item(i))).getElementsByTagName(IdentityVO.TB_IDENTITY_ID)).item(0);
                  Text identityIdText = (Text)identityIdElem.getFirstChild();
              
                  //Document rdoc = getViewIdentityRolesDoc(identityIdText.getNodeValue(), con); 
                  Document rdoc = getViewRolesByIdentityDoc(identityIdText.getNodeValue(), con);
                  NodeList rnl = rdoc.getElementsByTagName(RoleVO.XML_BOTTOM);
                  if(rnl != null) {
                    for (int j=0; j<rnl.getLength(); j++) {
                      inl.item(i).appendChild(idoc.importNode(rnl.item(j),true));
                    }
                  }
                  // Append identity doc to user doc. There's only one user in doc.
                  (udoc.getElementsByTagName(UserVO.XML_BOTTOM)).item(0).appendChild(udoc.importNode(inl.item(i),true));
              }
          }
          return udoc;
      } catch (Exception e) {
          logger.error(e);
          throw e;
      }
  }
  /**
   * Returns the user document for the given user id.
   */
  public Document getViewUserDoc(String userId, Connection con) throws Exception {
      HashMap inputParams = new HashMap();
      inputParams.put(UserVO.ST_IN_USER_ID, Integer.valueOf(userId));
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, inputParams, StatementConstants.USERS_VIEW_USER);  
      return doc;
  }

  /**
   * Returns the identities document for the given user id.
   */
  public Document getViewUserIdentitiesDoc(String userId, Connection con) throws Exception {
      HashMap inputParams = new HashMap();
      inputParams.put(UserVO.ST_IN_USER_ID, new Integer(userId));
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, inputParams, StatementConstants.USERS_VIEW_IDENTITIES);
 
      return doc;
  }  


  /**
   * Returns the identity document for the given identity id.
   */
  public Document getViewIdentityDoc(String identityId, Connection con) throws Exception {
      HashMap inputParams = new HashMap();
      inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, inputParams, StatementConstants.USERS_VIEW_IDENTITY);
 
      return doc;
  } 
  
  /**
   * Returns the roles document for the given identity id. The document returns
   * all roles with the roles associated with this identity marked by identity_id.
   */
  public Document getViewIdentityRolesDoc(String identityId, Connection con) throws Exception {
      HashMap inputParams = new HashMap();
      inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, inputParams, StatementConstants.USERS_VIEW_IDENTITY_ROLES);
 
      return doc;
  }    
  
  /**
   * Returns the roles document for the given identity id.
   */
  public Document getViewRolesByIdentityDoc(String identityId, Connection con) throws Exception {
      HashMap inputParams = new HashMap();
      inputParams.put(IdentityVO.ST_IDENTITY_ID, identityId);
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, inputParams, StatementConstants.USERS_SEARCH_IDENTITY_ROLES);
 
      return doc;
  } 
  
  /**
   * Returns document of all users.
   */
  public Document getViewUsersDoc(Connection con) throws Exception {
      
      Document doc = (Document)DataRequestHelper.executeStatement
          (con, null, StatementConstants.USERS_VIEW_USERS);
 
      return doc;
  }

  /**
   * Updates the user in system.
   */
  public void updateUser(Connection con, Object securityToken) throws Exception {
      try {
            HashMap inputParams = user.getUpdateUserInputParams(securityToken);
            HashMap outputMap = (HashMap)DataRequestHelper.executeStatement(con, inputParams, StatementConstants.USERS_UPDATE_USER);
            String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO UPDATE USER.");
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
                } else {
                    throw new SecurityAdminException("12", message);
                }
            }      

      } catch (Exception e) {
          throw e;
      }

  }

  /**
   *  Builds the document transformed when the request is add and the request is invalid.
   */ 
  public Document buildInvalidUpdateUserRequestDoc(Exception ex, Connection con) throws Exception {  
      
      try {
          // Creates ERRORS and FIELDS section.
          Document doc = user.rebuildTree(ex);
          // Creates USER section.
          Document udoc = getViewUserDoc(user.getUserId(), con);
          Document idoc = getViewUserIdentitiesDoc(user.getUserId(), con);
          //Document rdoc = null;

          udoc = mergeUserIdentityRole(udoc, idoc, con);
          Document ddoc = getDepartmentsDoc();
          Document ccdoc = getCallCentersDoc();
          Document csgdoc = getCsGroupsDoc();
                      
            if (udoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
            }
            if (ddoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ddoc.getDocumentElement(), true));
            }
            if (ccdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ccdoc.getDocumentElement(), true));
            }
            if (csgdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(csgdoc.getDocumentElement(), true));  
            }
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }

  /**
   *  Builds the document transformed when the request is display_add user.
   */ 
  public Document buildDisplayAddIdentityDoc(Connection con) throws Exception {
      Document doc = null;
      Document udoc = null;
      Document cdoc = null; // context document

      try {
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            udoc = getViewUserDoc(user.getUserId(), con);
            cdoc = getContextsDoc();
            Document idoc = getViewUserIdentitiesDoc(user.getUserId(), con);
            udoc = mergeUserIdentityRole(udoc, idoc, con);

            // Add USERS section
            if (udoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
            }             
            // Add drop down data CONTEXTS sections
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            } 
   //((XMLDocument)doc).print(System.out);       
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }

  /**
   *  Builds the document transformed when the request is refresh_display_add user.
   */ 
  public Document buildRefreshDisplayAddIdentityDoc(Connection con) throws Exception {
      Document doc = null;
      Document udoc = null; // user document
      Document idoc = null; // identity document
      Document ifdoc = null; // identity field document
      Document rdoc = null; // role document
      Document cdoc = null; // context document
      Document ddoc = null; // department document
      Document ccdoc = null; // call center document
      Document csgdoc = null; // cs group document  

      try {
            // Get USERS/USER section.
            udoc = getViewUserDoc(user.getUserId(), con);
            // Get FIELDS/FIELD section
            ifdoc = identity.getFieldDoc();
            // Get IDENTITY section and add to USERS/USER
            idoc = getViewUserIdentitiesDoc(user.getUserId(), con);
            udoc = mergeUserIdentityRole(udoc, idoc, con);
            // Get CONTEXTS/CONTEXT section
            cdoc = getContextsDoc();
            // Get ROLES/ROLE section
            rdoc = getContextRolesDoc();
            //rdoc = identity.markSelectedRoles(rdoc);           
            
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            if(udoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
            }
            if(ifdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(ifdoc.getDocumentElement(), true));
            }            
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            }            
            if (rdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
            }

            return doc;
      } catch (Exception e) {
          throw e;
      }
  }

  /**
   *  Builds the document transformed when the request is add identity and the request is invalid.
   */ 
  public Document buildInvalidAddIdentityRequestDoc(Connection con, Exception ex) throws Exception {  
      Document doc = null;
      Document udoc = null;
      Document idoc = null;
      Document cdoc = null;
      Document rdoc = null;
      try {
          // Get ROOT/FIELDS, ROOT/ERRORS section
          doc = identity.rebuildTree(ex);
          // Get USERS/USER section.
          udoc = getViewUserDoc(user.getUserId(), con);            
          // Get IDENTITY section and add to USERS/USER
          idoc = getViewUserIdentitiesDoc(user.getUserId(), con);
          udoc = mergeUserIdentityRole(udoc, idoc, con);            
          // Get CONTEXTS/CONTEXT section.
          cdoc = getContextsDoc();
          // Get ROLES/ROLE section.
          rdoc = getContextRolesDoc();
          rdoc = identity.markSelectedRoles(rdoc);   
          
            if(udoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
            }
            if(cdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(cdoc.getDocumentElement(), true));
            }            
            if (rdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
            }
            
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }

  
  /**
   *  Builds the document transformed when the request is display_add user.
   */ 
  public Document buildViewIdentityDoc(Connection con) throws Exception {
      Document doc = null;
      Document udoc = null; // user document
      Document idoc = null; // identity document
      Document fdoc = null; // field document
      Document rdoc = null; // role document
      
      try {
            doc = XMLHelper.getDocumentWithRoot(SecurityAdminConstants.XML_ROOT);
            
            // Get USERS/USER section.
            udoc = getViewUserDoc(user.getUserId(), con);
            
            // Get IDENTITIES/IDENTITY section.
            idoc = getViewIdentityDoc(identity.getIdentityId(), con);
            
            // Format idExpireDate and pwdExpireDate to mm/dd/yyyy. Formatting cannot be done in XSL because
            // when user input is captured, data need to be displayed as is.             
            idoc = formatIdentityDBDate((Document)idoc);   
            
            // Convert the identity document to field document. Need this step because
            // when user input is invalidated, FIELDS/FIELD is used to capture user input.
            this.initIdentityFromDoc((Document)idoc);
            fdoc = identity.getFieldDoc();

            // Get ROLES/ROLE section.
            rdoc = getViewIdentityRolesDoc(identity.getIdentityId(), con);
            
            // Add USERS section to ROOT.
            if (udoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
            }             
            
            // Add FIELDS section to ROOT.
            if(fdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(fdoc.getDocumentElement(), true));
            }  
            
            // Add ROLES section to ROOT.
            if(rdoc != null) {
                doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
            }
            printDocument(doc);
            return doc;
      } catch (Exception e) {
          throw e;
      }
  }  
 
  /**
   * Format identity date from db format to screen format.
   */
   private Document formatIdentityDBDate(Document doc) throws Exception {
      java.util.Date d = null;
      String formatedDate = "";
      String path = "/" + IdentityVO.XML_TOP + "/" + IdentityVO.XML_BOTTOM + "/";  // path = "/IDENTITIES/IDENTITY/"
      Node iNode = DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_ID_EXPIRE_DATE);
      Node pNode = DOMUtil.selectSingleNode(doc, path + IdentityVO.TB_PWD_EXPIRE_DATE);
      String sIdExpDate = (iNode == null) ? "" : iNode.getFirstChild().getNodeValue();
      String sPwdExpDate = (pNode == null) ? "" : pNode.getFirstChild().getNodeValue();
      if (!sIdExpDate.equals("")) {
          // convert from "yyyy-MM-dd HH:mm:ss.S" to date.
          d = StringUtil.formatStringToUtilDate(SecurityAdminConstants.DATE_TIME_PATTERN, sIdExpDate);
          // convert from date to mm/dd/yyyy
          iNode.getFirstChild().setNodeValue(StringUtil.formatUtilDateToString(d));
          
      } else {
          throw new SecurityAdminException("1");
      }
      if (!sPwdExpDate.equals("")) {
          // convert from "yyyy-MM-dd HH:mm:ss.S" to date.
          d = StringUtil.formatStringToUtilDate(SecurityAdminConstants.DATE_TIME_PATTERN, sPwdExpDate);
          // convert from date to mm/dd/yyyy
          pNode.getFirstChild().setNodeValue(StringUtil.formatUtilDateToString(d));
          //identity.setDatePwdExpDate(d);
      } else {
          throw new SecurityAdminException("1");
      }
      
      return doc;
   }
  
  /**
   *  Builds the document transformed when the request is update identity and the request is invalid.
   */ 
  public Document buildInvalidUpdateIdentityRequestDoc(Exception ex, Connection con) throws Exception {  
      
      try {
          // Creates ERRORS and FIELDS section.
          Document doc = identity.rebuildTree(ex);
          // Creates USERS section.
          Document udoc = getViewUserDoc(user.getUserId(), con);
          // Get ROLES/ROLE section.
          Document rdoc = getViewIdentityRolesDoc(identity.getIdentityId(), con);          
                      
          if (udoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(udoc.getDocumentElement(), true));
          }
            
          // Add ROLES section to ROOT.
          if(rdoc != null) {
              doc.getDocumentElement().appendChild(doc.importNode(rdoc.getDocumentElement(), true));
          }          
  //((XMLDocument)doc).print(System.out);
          return doc;
            
      } catch (Exception e) {
          throw e;
      }
  }  

  /**
   * Updates identity.
   */
  public void updateIdentity(Connection con, Object securityToken) throws Exception {
      try {
            HashMap inputParams = identity.getAddIdentityInputParams(securityToken, (Integer.valueOf(user.getUserId()).intValue()));
            HashMap outputMap = (HashMap)DataRequestHelper.executeStatement(con, inputParams, StatementConstants.USERS_UPDATE_IDENTITY);
            String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO UPDATE IDENTITY.");
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
                } else {
                    throw new SecurityAdminException("12", message);
                }
            }      

      } catch (Exception e) {
          logger.error(e);
          throw e;
      }
  }


  /**
   * Resets password.
   * @return password that was reset
   */
  public String resetPassword(Connection con, Object securityToken) throws Exception {
      String credential = null;
      try {
            HashMap inputParams = identity.getAdminResetPasswordInputParams(securityToken);
            HashMap outputMap = (HashMap)DataRequestHelper.executeStatement(con, inputParams, StatementConstants.USERS_UPDATE_CREDENTIALS);
            String status = (String)outputMap.get(SecurityAdminConstants.STATUS);
            String message = (String)outputMap.get(SecurityAdminConstants.MESSAGE);
            //credential = "foobar";
            credential = (String)outputMap.get(SecurityAdminConstants.COMMON_PARM_CREDENTIALS);

            logger.debug("status is: " + status);
            logger.debug("message is: " + message);
            // check status
            if (!"Y".equalsIgnoreCase(status))  {
                logger.debug("FAILED TO RESET PASSWORD.");
                // "ERROR OCCURRED" is an unexpected error.
                if (message != null && message.startsWith(SecurityAdminConstants.ERROR_OCCURRED)) {
                    throw new SecurityAdminException("6");
                } else {
                    throw new SecurityAdminException("12", message);
                }
            }      

      } catch (Exception e) {
          logger.error(e);
          throw e;
      }
      
      return credential;
  }  

    /**
     * Checks if the department description has a matching department code.
     */
    public static String getValidDepartmentId(String value) throws Exception {
        String departmentId = "";
        String path = "/DEPARTMENTS/DEPARTMENT/"; 
        Document doc = (Document)getDepartmentsDoc();
        Element foundElem = XMLHelper.findElementByChild(doc, UserVO.TB_DEPARTMENT_DESC, value);
        if(foundElem != null) {
            departmentId = (foundElem.getElementsByTagName(UserVO.TB_DEPARTMENT)).item(0).getFirstChild().getNodeValue();
        }
        
        return departmentId;
    }  

    /**
     * Checks if the department description has a matching department code.
     */
     
    public static String getValidRoleId(String value) throws Exception {
        String id = "";
        String path = "/" + RoleVO.XML_TOP + "/" + RoleVO.XML_BOTTOM + "/";  // path = "/ROLES/ROLE/"    
        HashMap parameters = new HashMap();
        parameters.put("in_context_id", SecurityAdminConstants.DEFAULT_CONTEXT);
        Document doc = (Document)DataRequestHelper.executeStatement(StatementConstants.ROLES_VIEW_ROLES_BY_CONTEXT, parameters);
        Element foundElem = XMLHelper.findElementByChild(doc, RoleVO.TB_ROLE_NAME, value);
        if(foundElem != null) {
            id = (foundElem.getElementsByTagName(RoleVO.TB_ROLE_ID)).item(0).getFirstChild().getNodeValue();
        }
        
        return id;        
    }

    /**
     * Checks if the department description has a matching department code.
     */
    public static String getValidCsGroupId(String value) throws Exception {
        String id = "";
        String path = "/CSGROUPS/CSGROUP/"; 
        Document doc = (Document)getCsGroupsDoc();
        Element foundElem = XMLHelper.findElementByChild(doc, UserVO.TB_CS_GROUP_DESC, value);
        if(foundElem != null) {
            id = (foundElem.getElementsByTagName(UserVO.TB_CS_GROUP)).item(0).getFirstChild().getNodeValue();
        }        
        
        return id;
    }

    /**
     * Checks if the department description has a matching department code.
     */
    public static String getValidCallCenterId(String value) throws Exception {
        String id = "";
        String path = "/CALLCENTERS/CALLCENTER/"; 
        Document doc = (Document)getCallCentersDoc();

        Element foundElem = XMLHelper.findElementByChild(doc, UserVO.TB_CALL_CENTER_DESC, value);
        if(foundElem != null) {
            id = (foundElem.getElementsByTagName(UserVO.TB_CALL_CENTER)).item(0).getFirstChild().getNodeValue();
        }         
        
        return id;
    }    
}