package com.ftd.security.service.model;


/**
 * @author smeka
 *
 */
public class ApolloSecurityServiceResp {

	protected String responseCode;	
	protected String errorCode;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String value) {
		this.responseCode = value;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
}
