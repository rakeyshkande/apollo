package com.ftd.security.service.model;

import java.util.ArrayList;
import java.util.List;


/**
 * @author smeka
 *
 */
public class Resource extends ApolloSecurityServiceResp {

	protected List<String> permissions;
	protected String name;		
	
	public Resource() {	}
	
	public Resource(List<String> permissions, String name) {
		this.permissions = permissions;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}
	
	public List<String> getPermissions() {
		if (permissions == null) {
			permissions = new ArrayList<String>();
		}
		return this.permissions;
	}
}
