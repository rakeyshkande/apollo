package com.ftd.security.service.model;

/**
 * @author smeka
 *
 */
public class SecurityServiceConstants {
	
	public static final String SECURITY_SERVICE_PROPERTITY_FILE = "SecurityService";	
	
	//Soap Header - Client Authentication Constants
	public static final String SECURITY_SERVICE_CLIENT_ID = "SECURITY_SERVICE_CLIENT_ID";
	public static final String SECURITY_SERVICE_CLIENT_PWD = "SECURITY_SERVICE_CLIENT_PWD";
	
	// Soap Body - Constants
	public static final String ORDER_PROC_CTXT = "Order Proc";
	
	// Error Codes
	public static final String TOKEN_VALIDATION_FAILED = "token.validation.failure.response.code";

	public static final String APOLLO_INTERNAL_ERROR = "apollo.internal.error";

	public static final String TOKEN_REQUIRED = "token.mandatory";

	public static final String TOKEN_INVALID = "token.invalid";

	public static final String TOKEN_EXPIRED = "token.expired"; 

	public static final String USERINFO_INVALID = "token.userinfo.invalid";

	public static final String AUTH_NOT_REQUIRED = "auth.not.required";

	public static final String RESOURCE_EMPTY = "resource.empty";
}
