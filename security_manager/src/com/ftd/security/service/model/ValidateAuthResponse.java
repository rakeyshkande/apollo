package com.ftd.security.service.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author smeka
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidateAuthResponse extends ApolloSecurityServiceResp {

	@XmlElementWrapper(name = "resources")
    @XmlElement(name = "resource")    
	protected List<Resource> resourceList;
	
	protected String userIdentityId;

	public List<Resource> getResourceList() {
		if(resourceList == null) {
			resourceList = new ArrayList<Resource>();
		}
		return resourceList;
	}

	public void setResourceList(List<Resource> resourceList) {
		this.resourceList = resourceList;
	}


	public String getUserIdentityId() {
		return userIdentityId;
	}

	public void setUserIdentityId(String value) {
		this.userIdentityId = value;
	}

}
