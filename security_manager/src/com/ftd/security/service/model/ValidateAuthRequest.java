
package com.ftd.security.service.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author smeka
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidateAuthRequest {

    protected String context;
    
    @XmlElementWrapper(name = "resources")
    @XmlElement(name = "resource")
    protected List<Resource> resourceList;
    
    protected String userSecurityToken;

    public String getContext() {
        return context;
    }

    public void setContext(String value) {
        this.context = value;
    }

    public String getUserSecurityToken() {
        return userSecurityToken;
    }

    public void setUserSecurityToken(String value) {
        this.userSecurityToken = value;
    }

	public List<Resource> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<Resource> resourceList) {
		this.resourceList = resourceList;
	}
}
