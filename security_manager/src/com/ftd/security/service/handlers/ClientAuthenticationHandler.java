/**
 * 
 */
package com.ftd.security.service.handlers;

import java.util.List;
import java.util.Map;

import org.apache.cxf.binding.soap.interceptor.SoapHeaderInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.service.model.SecurityServiceConstants;
import com.ftd.service.util.AuthenticationUtil;
import com.ftd.service.util.Client;
import com.ftd.service.util.Credentials;

/**
 * @author smeka
 *
 */
public class ClientAuthenticationHandler extends SoapHeaderInterceptor {
		
		private Logger logger = new Logger(ClientAuthenticationHandler.class.getName());

		/** @see org.apache.cxf.binding.soap.interceptor.SoapHeaderInterceptor#handleMessage(org.apache.cxf.message.Message)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message message) throws Fault {		
			logger.info("########## ClientAuthenticationHandler.handleMessage() #############");
		
			Map<String, List<String>> http_headers =  
					(Map<String, List<String>>)message.get(Message.PROTOCOL_HEADERS);
			
			List<String> userList = (List<String>) http_headers.get(SecurityServiceConstants.SECURITY_SERVICE_CLIENT_ID);
	        List<String> passList = (List<String>) http_headers.get(SecurityServiceConstants.SECURITY_SERVICE_CLIENT_PWD);
	 
	        String username = null;
	        String password = null;
	 
	        if(userList != null){
	        	username = userList.get(0).toString();
	        }
	 
	        if(passList != null){
	        	password = passList.get(0).toString();
	        }
	        
	        Client client = new Client();
	        Credentials credentials = new Credentials(username, password);
	        client.setCredentials(credentials);
	        try {
		        if(!(AuthenticationUtil.authenticateClient(client))) {
		        	logger.error("Security Service Authentication failed");
		        	throw new Fault(new Exception("Security Service Authentication failed"));
		        }
	        } catch(Exception e){
	        	throw new Fault(e);
	        }
		}
}
