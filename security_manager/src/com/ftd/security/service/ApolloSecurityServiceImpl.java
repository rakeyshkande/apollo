/**
 * 
 */
package com.ftd.security.service;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.jws.WebService;

import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.security.service.model.Resource;
import com.ftd.security.service.model.SecurityServiceConstants;
import com.ftd.security.service.model.ValidateAuthRequest;
import com.ftd.security.service.model.ValidateAuthResponse;
import com.ftd.security.util.ServletHelper;

/**
 * @author smeka
 * 
 */
@WebService(endpointInterface = "com.ftd.security.service.ApolloSecurityService")
@InInterceptors(interceptors = "com.ftd.security.service.handlers.ClientAuthenticationHandler")
public class ApolloSecurityServiceImpl implements ApolloSecurityService {

	private com.ftd.security.SecurityManager securityManager;
	private Logger logger = new Logger(ApolloSecurityService.class.getName());

	private static ResourceBundle resrcBundle = ResourceBundle.getBundle(SecurityServiceConstants.SECURITY_SERVICE_PROPERTITY_FILE);

	/** com.ftd.security.service.ApolloSecurityService#validateAndAuthorize(com.ftd.security.service.model.ValidateAuthRequest)
	 *  Service call to validate the security token and perform authorization against the requested resources.
	 **/
	public ValidateAuthResponse validateAndAuthorize(ValidateAuthRequest request) {

		ValidateAuthResponse response = new ValidateAuthResponse();		
		UserInfo userInfo = null;
		
		try {

			// Step1: validate Request for mandatory
			if(!isValidRequest(request, response)) {
				throw new Exception("ValidateAuthRequest - Validation failed");
			}

			// Step2: check if the token is still valid.
			if (StringUtils.isEmpty(request.getContext())) {
				request.setContext(SecurityServiceConstants.ORDER_PROC_CTXT);
			}
			if (!ServletHelper.isValidToken(request.getContext(), request.getUserSecurityToken())) {
				response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.TOKEN_INVALID));
				throw new Exception("Invalid token - Authentication/validation failed.");
			}

			// Step3: Get the User Info for the token.
			this.securityManager = com.ftd.security.SecurityManager.getInstance();
			userInfo = securityManager.getUserInfo(request.getUserSecurityToken());			
			if (userInfo == null || userInfo.getUserID() == null) {
				response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.USERINFO_INVALID));
				throw new Exception("Invalid User Info - Unable to rertieve user info");
			}
			response.setUserIdentityId(userInfo.getUserID());
			
			// Perform authorization only if resource included.
			if(!performAuth(request)) {
				response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.AUTH_NOT_REQUIRED));
				return response;
			}

			// Step4: Authorize User.
			authorizeUser(request, response);
			
		} catch (ExpiredSessionException ex) {
			response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.TOKEN_EXPIRED));
			logger.error(ex.getMessage());
		} catch (InvalidSessionException ex) {
			response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.TOKEN_INVALID));
			logger.error(ex.getMessage());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			if(response.getErrorCode() == null) { // validation failure already might have set error codes
				response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.APOLLO_INTERNAL_ERROR));
			}
		} finally {
			if (!StringUtils.isEmpty(response.getErrorCode())
					&& !resrcBundle.getString(SecurityServiceConstants.AUTH_NOT_REQUIRED).equals(response.getErrorCode())) {
				response.setResponseCode(resrcBundle.getString(SecurityServiceConstants.TOKEN_VALIDATION_FAILED));
			}
		}
		return response;
	}

	/** Check if at-least one resource is included in request to perform authorization
	 * @param request
	 * @return
	 */
	private boolean performAuth(ValidateAuthRequest request) {		
		if (request.getResourceList() != null && request.getResourceList().size() > 0) {
			return true;
		}
		return false;
	}

	/** Authorize User for a given list of valid resources.
	 * @param request
	 * @param response
	 */
	private void authorizeUser(ValidateAuthRequest request, ValidateAuthResponse response) {
		List<Resource> resources = new ArrayList<Resource>();
		
		for (Resource requestedResource : request.getResourceList()) {			
			Resource resource = new Resource(new ArrayList<String>(), requestedResource.getName());			
			try {				
				if(StringUtils.isEmpty(requestedResource.getName())) {					
					resource.setErrorCode(resrcBundle.getString(SecurityServiceConstants.RESOURCE_EMPTY));				
				} else {				
					resource.setPermissions(securityManager.assertPermission(
							request.getContext(), request.getUserSecurityToken(), resource.getName()));						
				}	
			} catch (Exception e) {
				logger.error("Authorization cannot be performed for resource : " + requestedResource);
				logger.error(e.getMessage());				
			}
			resources.add(resource);
		}
		response.setResourceList(resources);
	}

	/** Check if the request is valid to be processed.
	 * @param request
	 * @param response
	 * @return
	 */
	private boolean isValidRequest(ValidateAuthRequest request, ValidateAuthResponse response) {		
		if (StringUtils.isEmpty(request.getUserSecurityToken())) {
			response.setErrorCode(resrcBundle.getString(SecurityServiceConstants.TOKEN_REQUIRED));
			return false;
		}	
		return true;
	}
}
