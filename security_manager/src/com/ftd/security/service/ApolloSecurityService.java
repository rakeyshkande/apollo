package com.ftd.security.service;

import javax.jws.WebService;


import com.ftd.security.service.model.ValidateAuthRequest;
import com.ftd.security.service.model.ValidateAuthResponse;

@WebService(name = "ApolloSecurityService", targetNamespace = "http://service.security.ftd.com/")
public interface ApolloSecurityService {	
	public ValidateAuthResponse validateAndAuthorize(ValidateAuthRequest validateAuthRequest);

}
