function getStyleObject(fieldName)
{
    if (document.getElementById && document.getElementById(fieldName))
        return document.getElementById(fieldName).style;
}

function digitOnlyListener()
{
    var input = event.keyCode;
    if (input < 48 || input > 57){
        event.returnValue = false;
    }
}

function setErrorFields(){
    for (var i = 0; i < errorFields.length; i++){
        var element = document.getElementById(errorFields[i]);
        if (element != false){
            element.className = "errorField";
        }
    }
}

function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = '#000099';
}

function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

function imageOver()
{
    document.forms[0].style.cursor = "hand";
}

function imageOut()
{
    document.forms[0].style.cursor = "default";
}

function addDefaultListenersArray(elements)
{
    for (var i = 0; i < elements.length; i++){
        addDefaultListenersSingle(elements[i]);
    }
}

function addDefaultListenersSingle(element)
{
    document.getElementById(element).attachEvent("onfocus", fieldFocus);
    document.getElementById(element).attachEvent("onblur", fieldBlur);
}

function addImageCursorListenerArray(elements)
{
    for (var i = 0; i < elements.length; i++){
        addImageCursorListenerSingle(elements[i])
    }
}

function addImageCursorListenerSingle(element)
{
    document.getElementById(element).attachEvent("onmouseover", imageOver);
    document.getElementById(element).attachEvent("onmouseout", imageOut);
}