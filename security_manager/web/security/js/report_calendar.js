// the ubiqutious cross browser check.
var isNav      = (navigator.appName == 'Netscape');


/////// WCDate implementation /////////
var monthNames = new Array("Jan","Feb","Mar","Apr","May","Jun", 
                                             "Jul","Aug","Sep","Oct","Nov","Dec");
var monthLengths = new Array( 31,28,31,30,31,30,31,31,30,31,30,31);

var weekDayTitles = new Array("Sunday", "Monday", "Tuesday", "Wednesday",
                              "Thursday", "Friday", "Saturday");

// WCDate - provides an encapsulation of extended date functionality
// required by WC. It is Y2k compliant.
function WCDate()
{
  // functions
  // first day of month
  this.FirstDayOfMonth = WCDate_firstDayOfMonth;
  // first day of week
  this.FirstWeekDate = WCDate_firstWeekDate;
// number of days in month
  this.NumDaysInMonth = WCDate_numDaysInMonth;
  // week number of first week of month w.r.t beginning of year
  this.StartWeekNumber = WCDate_startWeekNumber;
// returns a string repesentation of date
  this.ToString = WCDate_toString;
  // gets year as full year
  this.GetYear  = WCDate_getYear;
  // sets year as full year
  this.SetYear = WCDate_setYear;
  this.Today  = WCDate_today;

  this.Today();
  var args = WCDate.arguments;
  if (args.length == 1)
  {  
    this.day = args[0].day;
    this.month = args[0].month;
    this.year = args[0].GetYear();
  } 
  else if (args.length == 3)
  {
    this.SetYear(args[0]);
    this.month = args[1];
    this.day = args[2];
  }
}

// gets the selected months first week number
function WCDate_startWeekNumber()
{
  var jan1 = new Date (this.year, 0, 1); // Jan 1st
  var curr = new Date(this.year, this.month, 1); // current month
  var diff = curr.getTime() - jan1.getTime();

  diff = Math.floor(diff/ (1000*60*60*24));

  return (Math.floor(diff/7) + 1);
}

// get the first day of the week for the selected month
function WCDate_firstDayOfMonth()
{
  var firstDate = new Date(this.year, this.month, 1);
  return firstDate.getDay()+1;
}

// gives the number of days in the selected month
function WCDate_numDaysInMonth()
{
  // need to handle leap years, update num days in Feb every 4 years
  if ((this.month == 1) && ((this.year)%4 == 0))
    return monthLengths[this.month]+1;

  return monthLengths[this.month];
}

// Return date rep. beginning of week
function WCDate_firstWeekDate()
{
  var date = new Date(this.year, this.month, this.day);
  var weekDay = date.getDay();

  if ( this.day  <= weekDay)
  {
    if (this.month == 0)
    {
      this.month = 11;
      this.year = this.year - 1;
    }
    else
      this.month = this.month - 1;
    this.day = this.NumDaysInMonth() - (weekDay - this.day);
  }
  else
    this.day = this.day - weekDay;
}

function WCDate_toString()
{
  var outMonth = (this.month + 1);
  var outDay   = (this.day);
  var outYear  = this.GetYear();
  
  if(outMonth < 10) outMonth = "0" + outMonth;
  if(outDay   < 10) outDay   = "0" + outDay;
  
  var retStr = outMonth + "/" + outDay + "/" + outYear;
  
  //var retStr = (this.month+1) + "/" + (this.day) + "/" + this.GetYear();
  return retStr;
}

// Currently JavaScript and java have different values returned
// by get and set year functions.
// This standardizes our mechanism of dealing with the year field.
// stores internally as a (x -1900) year
function WCDate_getYear()
{
  return this.year;
}

// always stores the year as a full year (i.e: 1998)
function WCDate_setYear(yr)
{
  if (yr < 1000)
    this.year = yr + 1900;
  else
    this.year = yr;
}

// sets internal day, month and year to today
function WCDate_today()
{
  var todayDate = new Date();
  this.month = todayDate.getMonth();
  this.SetYear(todayDate.getYear()); // do not directly access this property
  this.day = todayDate.getDate();
}

// represents today.
var today = new WCDate();


//////////Calendar View //////////////
// The main Calendar constructor
function Calendar()      // type == ID
{
  this.MONTHLY = "monthly";
  this.YEARLY = "yearly";

  // Data
  this.embedded = false;      // the calendar needs to be drawn embedded.
  this.layerName = "";
  this.hasMonthTitle = false; // Determines if month title gets displayed for a month.
  this.opener = null;
  this.userStr = "";
  this.target = null;
  this.currDate = new WCDate(); // default today
  this.str= "";

  // generic functions
  this.showPopupCalendar = Calendar_showPopup;
  this.showEmbeddedCalendar = Calendar_showEmbedded;
  this.SelectDate = Calendar_select;
  this.DrawYearly = Calendar_drawYearly;
  this.DrawMonthly = Calendar_drawMonthly;
  
  this.FillMonthTable = Calendar_fillMonth;
  this.wwrite = Calendar_wwrite;
  this.wclose = Calendar_wclose;
}

function Calendar_showPopup(win, type, userStr) {
   this.target = win.open("", "Calendar", "width=210,height=160,status=yes,resizable=no,top=200,left=200");
   this.target.opener = win;
   this.opener = win;  
   this.embedded = false;
   this.layerName = "";
     this.userStr = userStr;
     this.currDate = new WCDate();

   if (type == this.YEARLY) {
       this.hasMonthTitle = true;
       this.DrawYearly();
   }
   else {
       this.hasMonthTitle = false;
       this.DrawMonthly();
   }
}

function Calendar_showEmbedded(win, type, layerName) {
   this.target = (isNav) ? win.document.layers[layerName] : win;
   this.target.opener= win;
   this.opener = win;  
   this.embedded = true;
   this.layerName = layerName;
     this.currDate = new WCDate();
   
   if (type == this.YEARLY) {
      this.hasMonthTitle = true;
       this.DrawYearly();
   }
   else {
       this.hasMonthTitle = false;
       this.DrawMonthly();
   }
}

function Calendar_select(day, month) {
  
  if (!this.embedded) {
       this.target.close();
       this.target= null;
  }
  this.currDate.day=day;
  this.currDate.month=month;
  
  this.opener.OnCalendarSelect(this.currDate.ToString(), this.userStr);
}

function Calendar_drawMonthly()
{
  var args = Calendar_drawMonthly.arguments;
  if (args.length == 1) {
       this.currDate = new WCDate(args[0]);
  }
  else {
     var currForm = (isNav) ? this.target.document.forms[0] : this.target.document.all["DateChanger"];
       if (currForm)
     {
          var oldmonth = this.currDate.month;
        for (var i=0; i < currForm.month.options.length; i++)
        {
            if (currForm.month.options[i].selected == true)
            {
               oldmonth = this.currDate.month;
               this.currDate.month = i;
               break;
            }
        }
     
          if (currForm.year.value > 2899)
        {
           alert("Please choose a year less than 2899");
           currForm.year.value = this.currDate.year;
           currForm.month[oldmonth].selected = true;
           return;
        }
        this.currDate.SetYear(currForm.year.value); 
     }
  }

  this.str = "";
  if (!this.embedded) {  // This means it is not embedded
     this.wwrite("<html><LINK rel=stylesheet href='../../css/report_calendar.css' type='text/css'>");          
     this.wwrite("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>");
       this.wwrite("<script language='JavaScript1.2'>var logic=window.opener;</script>");
       this.wwrite("</html><body bottommargin=0 leftmargin=0 topmargin=0 rightmargin=0 onLoad='this.focus()'>");
  }

  this.wwrite("<table border=0 width=100% cellspacing=1 cellpadding=0 class='cal_outertablecolor'>");
  this.wwrite("<form name='DateChanger' method='post' onSubmit='javascript:logic.gCalendar.DrawMonthly(); return false'>");
  this.wwrite("<tr class='cal_dateselectorcolor'>");
  this.wwrite("<td align=LEFT NOWRAP><font class=cal_dateselectorfont>");
  this.wwrite("&nbsp;<select class=cal_dateselectorfont id='month' name='month' selectedIndex=" + this.currDate.month + " onChange='javascript:logic.gCalendar.DrawMonthly(); return true'>");
  for (i=0; i < monthNames.length; i++)
  {
     if (i == this.currDate.month)
           this.wwrite("<option value='' selected>" + monthNames[i] + "</option>");
       else
           this.wwrite("<option value=''>" + monthNames[i] + "</option>");
  }
  this.wwrite("</select>");
  this.wwrite("<input class=cal_dateselectorfont id='year' name='year' type='text' value='" + this.currDate.GetYear() + "' maxlength=4 size=4>");
  this.wwrite("</font></td>");
  this.wwrite("<td align=LEFT width='100%'><a href='#' onclick='javascript:logic.gCalendar.DrawMonthly(logic.today)'>");
  this.wwrite("<img src='../../images/sup_today_off.gif' width=43 height=18 border=0></a></td>"); 
  this.wwrite("</tr><tr><td colspan=2>");
  this.FillMonthTable(this.currDate);
  this.wwrite("</td></tr></form></table>");

  if (!this.embedded) {
       this.wwrite("</body></html>");
  }

  this.wclose();
}

function Calendar_drawYearly()
{
  var args = Calendar_drawYearly.arguments;
  if (args.length == 1) {
       this.currDate = new WCDate(args[0]);
  }
  else {
     var currForm = (isNav) ? this.target.document.forms[0] : this.target.document.all["DateChanger"];
       if (currForm)
     {
        if (currForm.year.value > 2899)
        {
           alert("Please choose a year less than 2899");
           currForm.year.value = this.currDate.year;
           return;
        }
            this.currDate.day = 1;
            this.currDate.month = 0;
        this.currDate.SetYear(currForm.year.value); 
     }
  }

  this.str = "";
  if (!this.embedded) {  // This means it is not embedded
     this.wwrite("<html><LINK rel=stylesheet href='../../css/report_calendar.css' type='text/css'>");      
     this.wwrite("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>");
       this.wwrite("<script language='JavaScript1.2'>var logic=window.opener;</script>");
       this.wwrite("</html><body>");
  }

  this.wwrite("<table border=0 cellspacing=2 cellpadding=0 width=100% class='cal_outertablecolor'>");
  this.wwrite("<form name='DateChanger' method='post' onSubmit='javascript:logic.gCalendar.DrawYearly(); return false'>");
  this.wwrite("<tr class='cal_dateselectorcolor'><td align=LEFT NOWRAP><font class=cal_dateselectorfont>");
  this.wwrite("Year: <input class=cal_dateselectorfont id='year' name='year' type='text' value='" + this.currDate.GetYear() + "' maxlength=4 size=4>&nbsp;");
  this.wwrite("</font></td>");
  this.wwrite("<td align=LEFT width='100%'><a href='#' onclick='javascript:logic.gCalendar.DrawYearly(logic.today); return false'>");
  this.wwrite("<img src='../../images/sup_today_off.gif' width=43 height=18 border=0></a>");
  this.wwrite("</td></tr>");
  this.wwrite("<tr><td colspan=2>");
  
  this.wwrite("<table width='100%' border=1 cellspacing=2 cellpadding=3 class='cal_middletablecolor'>");
  for (var i=0; i < 4; i++) {
          this.wwrite("<tr class='cal_outertablecolor'>");
          for (var j=0; j < 3; j++) {
                this.wwrite("<td valign=top width='33%'>");
                this.FillMonthTable(this.currDate);
              this.wwrite("</td>");
                this.currDate.month++;
          }
          this.wwrite("<tr>");
  }
  this.wwrite("</table></td></tr></form></table>");

  if (!this.embedded) {
       this.wwrite("</body></html>");
  }

  this.wclose();
}

///////////// Utility Calendar functions ////////////// 
// This draws the month table.
function Calendar_fillMonth(date)
{
  var tmpDate = new WCDate(date);
  this.wwrite("<table border=0 cellspacing=2 cellpadding=2 width='100%'" + "class='cal_innertablecolor'>");
  // draw month title
  if (this.hasMonthTitle) {
     this.wwrite("<tr><td align=center color='#FFFFFF' class='cal_monthtitlecolor'colspan=7><font class=cal_monthtitlefont>");
     this.wwrite(monthNames[tmpDate.month]);
     this.wwrite("</font></td></tr>");
  }

  this.wwrite("<tr>");
  for (var i=0; i < weekDayTitles.length; i++)
  {
    this.wwrite("<td align=center color='#FFFFFF' class='cal_weekdaytitlecolor'>" +
                "<font class=cal_weekdaytitlefont>" + weekDayTitles[i].substring(0,1) +
                "</font></td>");
  }

  // draw the rest of the month with week numbers
  var day=1, maxDays=tmpDate.NumDaysInMonth();
  var weekNum= tmpDate.StartWeekNumber();
  for (var j=0; j < 6; j++)
  {
    this.wwrite("</tr><tr class='cal_dayofmonthcolor'>");
    for (i=1; i < 8; i++)
    {
      if ((day > maxDays) || 
          ((j == 0) && (i < tmpDate.FirstDayOfMonth())))
        this.wwrite("<td><font class=cal_dayofmonthfont>&nbsp;</font></td>");
      else
      {
        var tmpstr1 = "<td align=center";
          if ((tmpDate.GetYear() == today.GetYear()) &&
            (tmpDate.month == today.month) && (day == today.day))     {
           tmpstr1 += " class=cal_currentdaycolor";
            }
        tmpstr1 += "><a href='javascript:logic.gCalendar.SelectDate(" + day + "," + tmpDate.month + ")'><font class=cal_dayofmonthfont>" + day +
                   "</font></a></td>";

        this.wwrite(tmpstr1);
        day++;
      }
    }
  }
  this.wwrite("</tr></table>");
}

// This wraps the write difference between IE and NSCP
function Calendar_wwrite(wtext) {
  // now output the string in a cross-browser manner
  if ((!this.embedded) || (isNav)) {
       this.target.document.writeln(wtext);
  }
  else {  // If it is embedded & IE has to be in layer
       this.str += wtext;
  }
}

// This wraps the close difference between IE and NSCP
function Calendar_wclose() {
     if ((!this.embedded) || (isNav)) {
        this.target.document.close();
   }
     else {
      this.target.document.all[this.layerName].innerHTML = this.str;
     }
}

///////////// GLOBALS ///////////////
// Global calendar object - only one calendar can be active at a time.
var gCalendar = new Calendar();

