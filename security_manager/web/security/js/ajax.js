function ajax_search_user()
{
    var ajaxRequest;  // The variable that makes Ajax possible!         
    var radio_check_val = "";
    var search_key_val = document.getElementsByName('searchKey')[0].value;
	
	if (search_key_val==null || search_key_val=="")
	{
		alert("User ID / Name is a required field.");
		return false;
	}
	
    for (i = 0; i < document.getElementsByName('searchOption').length; i++) 
    {
      if (document.getElementsByName('searchOption')[i].checked)
      {              
           radio_check_val = document.getElementsByName('searchOption')[i].value;
      }
    }
                
                try{
                                // Opera 8.0+, Firefox, Safari
                                ajaxRequest = new XMLHttpRequest();
                } 
                catch (e)
                {
                                // Internet Explorer Browsers
                                try{
                                                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
                                                alert("Your browser broke!");
                                }
                                catch (e)
                                  {
                                                try{
                                                                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                                } 
                                                catch (e)
                                                {
                                                                // Something went wrong
                                                                alert("Your browser broke!");
                                                                return false;
                                                }
                                }
                }

                var context = document.getElementsByName('context')[0].value;
                var contextJs = document.getElementsByName('contextJs')[0].value;   
                var securitytoken=document.getElementsByName('securitytoken')[0].value;
				var sitename=document.getElementsByName('sitename')[0].value;

                ajaxRequest.open("POST", "https://"+sitename+"/secadmin/security/SecurityUser.do?adminAction=userSearch&context="+context+"&securitytoken="+securitytoken+"&isExit=&sourceMenu=systemAdministration&searchOption="+radio_check_val+"&searchKey="+search_key_val, false);   
                ajaxRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                ajaxRequest.send();
                //alert(ajaxRequest.responseText);

		var divTxt = "<table width='50%' border='3' align='left' cellpadding='1' cellspacing='1' bordercolor='#006699'>"+
			"<tr>"+
				"<td>"+
					"<table width='100%' border='0' cellpadding='2' cellspacing='2' bordercolor='#006699'>"+
						"<tr>"+
							"<td class='colheader'>User ID</td>"+
							"<td class='colheader'>User Name</td>"+
							"<td class='colheader'>Locked</td>"+
							"<td class='colheader'>ID Expire Date</td>"+
							"<td class='colheader'>Password Expire Date</td>"+
						"</tr>";
				
				var txt = ajaxRequest.responseText;
				var obj = eval('(' + txt + ')');
				//alert(obj.length);
				//alert(obj.USERS.length);
				if(obj.USERS.length == 0){
					alert('User not found. Please try different user.');
					return false;
				}
				
				if (typeof obj.USERS.USER.length === "undefined"){
					
					showHide('userSearchResultsHeader');
						divTxt = divTxt + "<tr>"+
							"<td>"+obj.USERS.USER.identity_id+"</td>"+
							"<td>"+obj.USERS.USER.first_name+" "+obj.USERS.USER.last_name+"</td>"+
							"<td>"+obj.USERS.USER.locked_flag+"</td>"+
							"<td>"+obj.USERS.USER.identity_expire_date+"</td>"+
							"<td>"+obj.USERS.USER.credentials_expire_date+"</td>"+
							"<td width='30%' align='center'>"+
								"<input type='button' name='resetPassword' Value='Reset Password' class='BlueButton' tabindex='3' onclick='javascript:goResetPassword(\""+obj.USERS.USER.user_id+"\",\""+obj.USERS.USER.identity_id+"\",\""+obj.USERS.USER.identity_expire_date+"\",\""+obj.USERS.USER.locked_flag+"\",\""+obj.USERS.USER.credentials_expire_date+"\")'/>"+
							"</td>"+
						"</tr>";
				}
				else{
					
					showHide('userSearchResultsHeader');
					for(var i=0; i<obj.USERS.USER.length; i++){
						
						divTxt = divTxt + "<tr>"+
							"<td>"+obj.USERS.USER[i].identity_id+"</td>"+
							"<td>"+obj.USERS.USER[i].first_name+" "+obj.USERS.USER[i].last_name+"</td>"+
							"<td>"+obj.USERS.USER[i].locked_flag+"</td>"+
							"<td>"+obj.USERS.USER[i].identity_expire_date+"</td>"+
							"<td>"+obj.USERS.USER[i].credentials_expire_date+"</td>"+
							"<td width='30%' align='center'>"+
								"<input type='button' name='resetPassword' Value='Reset Password' class='BlueButton' tabindex='3' onclick='javascript:goResetPassword(\""+obj.USERS.USER[i].user_id+"\",\""+obj.USERS.USER[i].identity_id+"\",\""+obj.USERS.USER[i].identity_expire_date+"\",\""+obj.USERS.USER[i].locked_flag+"\",\""+obj.USERS.USER[i].credentials_expire_date+"\")'/>"+
							"</td>"+
						"</tr>";
					}
				}

				divTxt = divTxt + "</table>"+
            "</td>"+
        "</tr>"+
    "</table>";
				document.getElementById("userResultsDiv").innerHTML=divTxt;
				document.getElementById("userPasswordDiv").style.display="none";
}

function goResetPassword(userid, identityid, idexpdate, lockedflag, pwdexpdate) 
{
    var ajaxRequest;  // The variable that makes Ajax possible!         
                
                try{
                                // Opera 8.0+, Firefox, Safari
                                ajaxRequest = new XMLHttpRequest();
                } 
                catch (e)
                {
                                // Internet Explorer Browsers
                                try{
                                                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
                                                alert("Your browser broke!");
                                }
                                catch (e)
                                  {
                                                try{
                                                                ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                                } 
                                                catch (e)
                                                {
                                                                // Something went wrong
                                                                alert("Your browser broke!");
                                                                return false;
                                                }
                                }
                }

                var context = document.getElementsByName('context')[0].value;
                var contextJs = document.getElementsByName('contextJs')[0].value;   
                var securitytoken=document.getElementsByName('securitytoken')[0].value;
				var sitename=document.getElementsByName('sitename')[0].value;
				
				var idyy = idexpdate.substring(0,4);
				var idmm = idexpdate.substring(5,7);
				var iddd = idexpdate.substring(8,10);
				var id_exp_date = idmm+'/'+iddd+'/'+idyy
				
				var pwdyy = pwdexpdate.substring(0,4);
				var pwdmm = pwdexpdate.substring(5,7);
				var pwddd = pwdexpdate.substring(8,10);				
				var pwd_exp_date = pwdmm+'/'+pwddd+'/'+pwdyy

                ajaxRequest.open("POST", "https://"+sitename+"/secadmin/security/Identity.do?adminAction=reset_user_password&context="+context+"&securitytoken="+securitytoken+"&isExit=&sourceMenu=systemAdministration&user_id="+userid+"&identityId="+identityid+"&expireDate="+id_exp_date+"&locked="+lockedflag+"&pwdExpireDate="+pwd_exp_date, false);   
                ajaxRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                ajaxRequest.send();
                //alert(ajaxRequest.responseText);
				
				
				var divTxt = "";
				var txt = ajaxRequest.responseText;
				var obj = eval('(' + txt + ')');
				if (typeof obj.length === "undefined"){
					divTxt = divTxt +"<table width='30%' border='3' align='left' cellpadding='1' cellspacing='1' bordercolor='#006699' style='margin:50px'>"+
					"<tr>"+
						"<td align='center' style='padding:20px'>"+
							"<b>Temporary Password for "+identityid+":   </b>"+obj.credential+
						"</td>"+
					"</tr>"+
					"</table>";
				}

				document.getElementById("userPasswordDiv").innerHTML=divTxt;
				showHide('userPasswordDiv');
				window.location.hash = "jumpToPassword";
}

function showHide(eleId){
    var theEle = document.getElementById(eleId);
    if(theEle.style.display=="none"){
        theEle.style.display="";
    }
	/*else{
        theEle.style.display="none";
    }*/ 
}