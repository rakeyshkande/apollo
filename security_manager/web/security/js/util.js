/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;

      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }

      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Atl + Left Arrow
   if(window.event.altiLeft){
      window.event.returnValue = false;
      return false;
   }

   // Atl + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }
}

function getStyleObject(fieldName)
{
    if (document.getElementById && document.getElementById(fieldName))
        return document.getElementById(fieldName).style;
}

function digitOnlyListener()
{
    var input = event.keyCode;
    if (input < 48 || input > 57){
        event.returnValue = false;
    }
}

function setErrorFields(){
    for (var i = 0; i < errorFields.length; i++){
        var element = document.getElementById(errorFields[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}

function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = 'red';
}

function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = 'red';
}

function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}

function imageOver()
{
    document.forms[0].style.cursor = "hand";
}

function imageOut()
{
    document.forms[0].style.cursor = "default";
}

function addDefaultListenersArray(elements)
{
    for (var i = 0; i < elements.length; i++){
        addDefaultListenersSingle(elements[i]);
    }
}

function addDefaultListenersSingle(element)
{
    document.getElementById(element).attachEvent("onfocus", fieldFocus);
    document.getElementById(element).attachEvent("onblur", fieldBlur);
}

function addImageCursorListenerArray(elements)
{
    for (var i = 0; i < elements.length; i++){
        addImageCursorListenerSingle(elements[i])
    }
}

function addImageCursorListenerSingle(element)
{
    document.getElementById(element).attachEvent("onmouseover", imageOver);
    document.getElementById(element).attachEvent("onmouseout", imageOut);
}


function checkRequiredFields(fields)
{
    for (var i = 0; i < fields.length; i++){
		if(document.all(fields[i]).value == "") {
			alert(document.all(fields[i]).name + " is required");
			return false;
		}
   	}
	return true;
}

/*
 *  Show a modal dialog with an Iframe and put the url in the
 *  Iframe src.  This allows the url to go through an XSLT transform.
 */
function showModalDialogIFrame(url,vArguements,sFeatures)
{
  var iframeURL = "modalDialog.html";
  var imbeddedURL = new Object();
  imbeddedURL.dialogURL = url;

  return window.showModalDialog(iframeURL, imbeddedURL ,sFeatures);
}


function getReportServerUrl()
{
	// Report server name should be in URL, so extract it and create full report server URL
	var srvrStr = "";
	var inStr = window.location.search.substring(1);
	var pArray = inStr.split("&");
	for (var i=0; i<pArray.length; i++) {
		var pVals = pArray[i].split("=");
		if (pVals[0] == "repoServer") {
			srvrStr = pVals[1];
			break;
		}
	}
	return "http://" + srvrStr + "/reports/rwservlet?opslogin";
}