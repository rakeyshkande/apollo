// $Id: common.js,v 1.2 2006/05/26 15:49:59 mkruger Exp $
var Browser = navigator.appName.substring(0,9);
var Version = navigator.appVersion;
var isNav   = (Browser == 'Netscape');

// GENERAL UTILITIES
function getCurrentTimestamp()
{
   return today.getTime();
}

function isEmptyString(testString)
{
	if (testString == null) {
		return true;
	}

   var newString = testString.replace(/\s/g, "");
   if ((newString == null) || (newString.length == 0)) {
      return true;
   }

   return false;
}
// END GENERAL UTILITYES

// BEGIN INTN: for internationalization - we need to support our own parse and isNaN.

function scltIsNaN(value)
{
//   return scltIsDouble(value) == false;
    return isNaN(value);
}

function scltIsInteger(str)
{
	var s = new String(str);
	if (isEmptyString(s) || scltIsNaN(s)) {
		return false;
	}
	
	var len = s.length;
	var index = 0;
	if ((s.charAt(0) == "-") && (len > 1)) {
		index = 1;
	}
	for (var i=index; i<len; i++) {
		var c = s.charAt(i);
		if (!((c >= "0") && (c <= "9"))) {
			return false;
		}
   }
	return true; 
}


function scltIsDouble(value)
{
    var str = new String(value)
    if (isEmptyString(str) || scltIsNaN(str)) {
        return false;
    }
    // var pattern = new RegExp("[\d\s]+(\.\d{2})?");
    // The pattern specified, look for a character that is not a
	// digit, space, plus, or minus sign.  If we find any char that
	// is not one of those, we will assume that value is not a double.
	// TODO: We need to improve the reg. expression but we need to be
	// careful and only use the charaters we are using at the moment
	// otherwise we might introduce anoth i18n problem, for example
	// checking a-z is not acceptable.
	var pattern = new RegExp("[^0-9 -+\.]");
    m = str.search(pattern);
    if (m == -1){
        return true
    }
    else{
        return false;
    }
}

// Date is formated as mm/dd/yyyy returns truee if in valid date format.
function scltIsDate(str)
{
	var s = new String(str);
	if (!isEmptyString(s)) {
	   var parts = s.split("/");
	   if ((parts != null) && (parts.length== 3)) {
			if ((parts[0] <= 12) && (parts[1] <= 31)) {
				return true;
			}
	   }
   }

	return false; 
}

// Return 1 if lhs > rhs, 0 if lhs = rhs and -1 if lhs < rhs or -2 if invalid
function scltCompareInt(lhsValueStr, rhsValueStr)
{
	if (!scltIsInteger(lhsValueStr) || !scltIsInteger(rhsValueStr))
		return -2;

   var val = parseInt(lhsValueStr);
   var val2 = parseInt(rhsValueStr);
   if (val == val2) return 0;
   if (val < val2) return -1;
   return 1;
}

// Return 1 if lhs > rhs, 0 if lhs = rhs and -1 if lhs < rhs or -2 if invalid
function scltCompareDouble(lhsValueStr, rhsValueStr)
{
	if (!scltIsDouble(lhsValueStr) || !scltIsDouble(rhsValueStr))
		return -2;
   
	var val = parseFloat(lhsValueStr);
   var val2 = parseFloat(rhsValueStr);
   if (val == val2) return 0;
   if (val < val2) return -1;
   return 1;
}

// Returns 1 if lhs > rhs, 0 if lhs = rhs and -1 if lhs < rhs or -2 if invalid
function scltCompareDate(lhs, rhs)
{  
	if (!scltIsDate(lhs) || !scltIsDate(rhs)) {
		return -2;
	}

	var parts = lhs.split("/");
	var thisDate = new Date(parts[2], parts[0], parts[1]);

	parts = rhs.split("/");
	var whenDate = new Date(parts[2], parts[0], parts[1]);

	var time1 = thisDate.getTime();
	var time2 = whenDate.getTime();
   
	if (time1 == time2) return 0;
   if (time1 < time2) return -1;
   
   return 1;
}

// END INTERNATIONALIZATION

// BEGIN COMMON DATA VALIDATION FUNCTIONS
function checkDate(title, elem1, opr, elem2) {
  var retStatus = false;

  if (opr.selectedIndex == 3) { // between amts
     retStatus = (scltCompareDate(elem2.value, elem1.value) >= 0) ? true : false;
  } else if ((opr.selectedIndex > 0) && (isEmptyString(elem2.value))) { // for equal and less than should have only one date
  	  retStatus = scltIsDate(elem1.value);
  }

  if (!retStatus) {
    alert("Invalid " + title + " field. Please reenter the date and select the correct operator.");
	 elem1.focus();
  }

  return retStatus;
}


function checkAmount(title , elem1, opr, elem2) {
  var retStatus = false;

  if (opr.selectedIndex == 3) { // between amts
 	  retStatus = (scltCompareDouble(elem2.value, elem1.value) >= 0) ? true : false; 
  } else if ((opr.selectedIndex > 0) && (isEmptyString(elem2.value))) {
 	  retStatus = scltIsDouble(elem1.value);
  }

  if (!retStatus) {
 	  alert("Invalid " + title + " field. Please reenter the amount and select the correct operator.");
  	  elem1.focus();
  }

  return retStatus;
}


function emailCheck (emailStr) {
	/* The following pattern is used to check if the entered e-mail address
		fits the user@domain format.  It also is used to separate the username
		from the domain. */
	var emailPat=/^(.+)@(.+)$/
	/* The following string represents the pattern for matching all special
		characters.  We don't want to allow special characters in the address. 
		These characters include ( ) < > @ , ; : \ " . [ ]    */
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
	/* The following string represents the range of characters allowed in a 
		username or domainname.  It really states which chars aren't allowed. */
	var validChars="\[^\\s" + specialChars + "\]"
	/* The following pattern applies if the "user" is a quoted string (in
		which case, there are no rules about which characters are allowed
		and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
		is a legal e-mail address. */
	var quotedUser="(\"[^\"]*\")"
	/* The following pattern applies for domains that are IP addresses,
		rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
		e-mail address. NOTE: The square brackets are required. */
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	/* The following string represents an atom (basically a series of
		non-special characters.) */
	var atom=validChars + '+'
	/* The following string represents one word in the typical username.
		For example, in john.doe@somewhere.com, john and doe are words.
		Basically, a word is either an atom or quoted string. */
	var word="(" + atom + "|" + quotedUser + ")"
	// The following pattern describes the structure of the user
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")
	/* The following pattern describes the structure of a normal symbolic
		domain, as opposed to ipDomainPat, shown above. */
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
	
	
	/* Finally, let's start trying to figure out if the supplied address is
		valid. */
	
	/* Begin with the coarse pattern to simply break up user@domain into
		different pieces that are easy to analyze. */
	var matchArray=emailStr.match(emailPat)
	if (matchArray==null) {
	  /* Too many/few @'s or something; basically, this address doesn't
		  even fit the general mould of a valid e-mail address. */
		alert("Email address seems incorrect (check @ and .'s)")
		return false
	}
	var user=matchArray[1]
	var domain=matchArray[2]
	
	// See if "user" is valid 
	if (user.match(userPat)==null) {
		 // user is not valid
		 alert("The username doesn't seem to be valid.")
		 return false
	}
	
	/* if the e-mail address is at an IP address (as opposed to a symbolic
		host name) make sure the IP address is valid. */
	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null) {
		 // this is an IP address
		  for (var i=1;i<=4;i++) {
			 if (IPArray[i]>255) {
				  alert("Destination IP address is invalid!")
			return false
			 }
		 }
		 return true
	}
	
	// Domain is symbolic name
	var domainArray=domain.match(domainPat)
	if (domainArray==null) {
		alert("The domain name doesn't seem to be valid.")
		 return false
	}
	
	/* domain name seems valid, but now make sure that it ends in a
		three-letter word (like com, edu, gov) or a two-letter word,
		representing country (uk, nl), and that there's a hostname preceding 
		the domain or country. */
	
	/* Now we need to break up the domain to get a count of how many atoms
		it consists of. */
	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (domArr[domArr.length-1].length<2 || 
		 domArr[domArr.length-1].length>3) {
		// the address must end in a two letter or three letter word.
		alert("The address must end in a three-letter domain, or two letter country.")
		return false
	}
	
	// Make sure there's a host name preceding the domain.
	if (len<2) {
		var errStr="This address is missing a hostname!"
		alert(errStr)
		return false
	}
	
	// If we've gotten this far, everything's valid!
	return true;
}
// END COMMON DATA VALIDATION FUNCTIONS
