<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="reportserver"><xsl:value-of select="key('security','reportserver')/value" /></xsl:param>
<!-- Security related parameters. -->
<xsl:param name="Operations"><xsl:value-of select="key('security','Operations')/value" /></xsl:param>
<xsl:param name="IOTWMaintLink"><xsl:value-of select="key('security','IOTWMaintLink')/value" /></xsl:param>

<xsl:variable name="adminAction" select="'operations'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Operations Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

  	<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
  	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		submitFormSsl("Login.do");
  	}

  	function goMain() {
		document.forms[0].adminAction.value = "mainMenu";
		submitForm("Main.do");
  	}

	function comingSoon() {
		alert("Coming soon!");
	}


	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}

  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}

	]]>
  </script>
  </head>
  <body onload="javascript: init()">
    <form name="form" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Operations Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="400" onClick="javascript: logoff();"/>
		</td>
	</tr>
	</table>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
	<td>
	<xsl:variable name="permissionView" select="'View'"/>
	<xsl:if test="$Operations = $permissionView">
                  <!-- General Maintenance Section -->
                <tr>
                  <td>
                    <table>
                       <tr>
                          <td valign="top" width ="41%">
                            <!-- Column 1 -->
                            <table>
                              <tr><td class="label">General Maintenance</td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/loadmemberdata/servlet/CodifyMaintServlet?action=loadcodification&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="10">Codified Products</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/CreateGiftCertAction.do?context={$context}&amp;securitytoken={$securitytoken}&amp;gcc_action=display_create&amp;gcc_user=user_op" tabindex="20">Create Gift Certificates/Coupons</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/emailadmin/EmailAdminMenuServlet?menuAction=EmailConfirmation&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="40">Email Confirmation</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/FuelSurchargeMaintAction.do?origin=oper&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=Load" tabindex="45">Fuel Surcharge</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/SearchGiftCertAction.do?context={$context}&amp;securitytoken={$securitytoken}&amp;gcc_action=display_search&amp;gcc_user=user_op" tabindex="50">Gift Certificates/Coupon Maintenance</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/loadmemberdata/servlet/GNADDSettingServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="60">GNADD Settings</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/loadmemberdata/servlet/MemberDataLoadServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="75">Member Data File Load</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/deliveryFeeMaintAction.do?context={$context}&amp;securitytoken={$securitytoken}&amp;actionMethod=loadDeliveryFeeList&amp;deliveryType=1" tabindex="75">Morning Delivery</a></td></tr>
                            </table>
                          </td>
                          <td valign="top" width ="30%">
                            <!-- Column 2 -->
                            <table>
                              <tr><td>&nbsp;</td></tr>
                             <!-- QE-68 <tr><td><a class="link" href="http://{$sitename}/pdb/showOccasionMaintenance.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="80">Occasion</a></td></tr> -->
                              <tr><td><a class="link" href="http://{$sitename}/orderprocessingmaint/OccasionMaintAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="90">Occasion Code Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/pdb/showProductMaintList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="100">Product Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/orderprocessingmaint/MPRedemptionRateMaintAction.do?origin=oper&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=loadRedemptionRateList" tabindex="105">Redemption Rate Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/orderprocessingmaint/SecondChoiceMaintAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="110">Second Choice Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/orderprocessingmaint/ServiceFeeMaintAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="115">Service Charge Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/pdb/showVendorProductOverride.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="120">Ship Method Override</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/pdb/showShippingKeyControl.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="125">Shipping Key Maintenance</a></td></tr>
                            </table>
                          </td>
                          <td valign="top" width ="30%">
                            <!-- Column 3 -->
                            <table>
                              <tr><td>&nbsp;</td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/pdb/showUpsellMaintList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="130">Upsell</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/orderprocessingmaint/VendorListAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="140">Vendor Maintenance</a></td></tr>
                              <tr><td><a class="link" href="http://{$sitename}/loadmemberdata/servlet/HolidayProductsServlet?action=load&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="145">Holiday Products</a></td></tr>
                            </table>
                          </td>
                        </tr>
                    </table>
                  </td>
                </tr>
                <!-- Marketing Maintenance Section -->
                <tr>
                  <td>
                    <table>
                       <tr>
                          <td valign="top" width="250">
                            <!-- Column 1 -->
                            <table>
                              <tr><td class="label">Marketing Maintenance</td></tr>
                      				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/clubCodeMaint.do?adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="155">AAA Club Code Maintenance</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/costCenterMaint.do?adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="160">Cost Center Maintenance</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/partnerPromoMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="170">Partner Program Maintenance</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/priceHeaderMaint.do?adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="180">Price Code Maintenance</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/sourceCodeSearch.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="190">Source Code Maintenance - Search</a></td></tr>
                              <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/sourceTypeMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="200">Source Type Maintenance - Search</a></td></tr>
                              <xsl:if test="$IOTWMaintLink = $permissionView">
                                <tr>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/joe/iotw.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="245">Item Of The Week Maintenance</a></td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                          <td valign="top" width="300">
                            <!-- Column 2 -->
                          </td>
                        </tr>
                    </table>
                  </td>
                </tr>
                <!-- Customer Service Maintenance Section -->
                <tr>
                  <td>
                    <table>
                    	<tr>
                    	  <td valign="top" width="250">
                            <!-- Column 1 -->
                            <table>
                              <tr><td class="label">Customer Service Maintenance</td></tr>
		                      <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadDnisMaintenance.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;page_position=1" tabindex="250">DNIS Maintenance</a></td></tr>
		                      <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadMsgMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_titles&amp;message_type=Email" tabindex="260">Stock Email Maintenance</a></td></tr>
		                	  <!-- <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadMsgMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_titles&amp;message_type=Letter" tabindex="270">Stock Letter Maintenance</a></td></tr> -->
                            </table>
                          </td>
                          <td valign="top" width="300">
                            <!-- Column 2 -->
                            <table>
                              <tr><td class="label">Shipping</td></tr>
  							  <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/FedExShippingTableAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_page" tabindex="275">FedEx Shipping Table Maintenance</a></td></tr>
                            </table>
                          </td>
                    	</tr>
                    </table>
                  </td>
                </tr>
                <!-- Reports Section -->
                <tr>
                  <td>
                    <table>
                      <tr><td class="label">Reports</td></tr>
                      <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/ScrubStatistics.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="280">Order Scrub Statistics Report</a></td></tr>
                    </table>
                  </td>
                </tr>
		</xsl:if>
	</td>
	</tr>
</table>
<table width="98%" border="0" cellpadding="1" cellspacing="1">
<tr>
		<td width="55%" align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="290" onclick="javascript: goMain();"/>
		</td>
		<td width="43%" align="right">
				<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="300" onClick="javascript: logoff();"/>
		</td>
</tr>
</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
