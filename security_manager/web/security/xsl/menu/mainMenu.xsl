<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes" doctype-system="XSLT-compat"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>


<!-- Security related parameters. -->
<xsl:param name="CustomerService"><xsl:value-of select="key('security','CustomerService')/value" /></xsl:param>
<xsl:param name="DistributionFulfillment"><xsl:value-of select="key('security','DistributionFulfillment')/value" /></xsl:param>
<xsl:param name="Operations"><xsl:value-of select="key('security','Operations')/value" /></xsl:param>
<xsl:param name="Merchandising"><xsl:value-of select="key('security','Merchandising')/value" /></xsl:param>
<xsl:param name="Marketing"><xsl:value-of select="key('security','Marketing')/value" /></xsl:param>
<xsl:param name="AccountingFinance"><xsl:value-of select="key('security','AccountingFinance')/value" /></xsl:param>
<xsl:param name="SystemAdministration"><xsl:value-of select="key('security','SystemAdministration')/value" /></xsl:param>
<xsl:param name="Reports"><xsl:value-of select="key('security','Reports')/value" /></xsl:param>
<xsl:param name="Dashboard"><xsl:value-of select="key('security','Dashboard')/value" /></xsl:param>
<xsl:param name="ResetPasswordAdmin"><xsl:value-of select="key('security','ResetPasswordAdmin')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Order Processing Main Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

  	<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name = '<xsl:value-of select="$sitename"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
  	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		submitFormSsl("Login.do");
  	}

	function comingSoon() {
		alert("Coming soon!");
	}
	
	function goCustomerService() {
		document.forms[0].adminAction.value = "customerService";
		submitForm("Main.do");
	}
	function goDistributionFulfillment() {
		document.forms[0].adminAction.value = "distributionFulfillment";
		submitForm("Main.do");
	}
	function goOperations() {
		document.forms[0].adminAction.value = "operations";
		submitForm("Main.do");
	}
	function goMerchandising() {
		document.forms[0].adminAction.value = "merchandising";
		submitForm("Main.do");
	}
	function goMarketing() {
		document.forms[0].adminAction.value = "marketing";
		submitForm("Main.do");
	}
	function goAcctgFinancing() {
		document.forms[0].adminAction.value = "accounting";
		submitForm("Main.do");
	}
	function goSystemAdministration() {
		document.forms[0].adminAction.value = "systemAdministration";
		document.forms[0].isExit.value="N";
		submitForm("Main.do");
	}
	function goRestPassword()
	{
		document.forms[0].adminAction.value = "resetUserPassword";
		document.forms[0].isExit.value="N";
		submitFormSsl("Main.do");
	}

	function submitForm(servlet) {
		document.forms[0].action = "http://" + site_name + servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}
	
	]]>
  </script>
  </head>
  <body onload="javascript: init()">
    <form name="form" method="post">
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <input type="hidden" name="isExit" value=""/>
    <xsl:call-template name="securityanddata"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="Header">Order Processing Main Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="8" onClick="javascript: logoff();"/>
		</td>
	</tr>
	</table>
	<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
		<td>
		<table width="98%" border="0" cellpadding="2" cellspacing="2">
			<xsl:variable name="permissionView" select="'View'"/>
			<!-- Check authorizaion -->
			<xsl:if test="$CustomerService = $permissionView">
				<tr>
					<td width="45%">&nbsp;</td>
					<td width="53%" class="TextLink"><a href="javascript: goCustomerService()" tabindex="1">Customer Service</a></td>
				</tr>
			</xsl:if>
			<xsl:if test="$DistributionFulfillment = $permissionView">
				<tr>
					<td width="45%">&nbsp;</td>
					<td width="53%" class="TextLink"><a href="javascript: goDistributionFulfillment()" tabindex="1">Distribution/Fulfillment</a></td>
				</tr>
			</xsl:if>
			<xsl:if test="$Operations = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goOperations()" tabindex="2">Operations</a></td>
			</tr>
			</xsl:if>
			<xsl:if test="$Merchandising = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goMerchandising()" tabindex="3">Merchandising</a></td>
			</tr>
			</xsl:if>
			<xsl:if test="$Marketing = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goMarketing()" tabindex="4">Marketing</a></td>
			</tr>
			</xsl:if>
			<xsl:if test="$AccountingFinance = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goAcctgFinancing()" tabindex="5">Accounting/Finance</a></td>
			</tr>
			</xsl:if>
			<xsl:if test="$Dashboard = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
                                <td width="53%" class="TextLink"><a href="http://{$sitename}/dashboard/Dashboard.do?context={$context}&amp;securitytoken={$securitytoken}" name="dashboardMenuLink" id="dashboardMenuLink" tabindex="7">Dashboard</a></td>
			</tr>
			</xsl:if>
			<!--xsl:if test="$SystemAdministration = $permissionView"-->
			<xsl:if test="$SystemAdministration = $permissionView">
			<tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goSystemAdministration()" tabindex="6">System Administration</a></td>
			</tr>
			</xsl:if>
			<xsl:if test="$ResetPasswordAdmin = $permissionView">
                <tr>
                <td width="45%">&nbsp;</td>
                <td width="53%" class="TextLink"><a href="javascript:goRestPassword()" tabindex="7">Reset Password Administration</a></td></tr>
            </xsl:if> 			
			<!--tr>
				<td width="45%">&nbsp;</td>
				<td width="53%" class="TextLink"><a href="javascript: goReports()">Reports</a></td>
			</tr-->
		</table>
		</td>
	</tr>
	</table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="8" onClick="javascript: logoff();"/>
		</td>
	</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>