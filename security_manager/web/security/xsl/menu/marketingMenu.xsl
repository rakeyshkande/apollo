<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<!-- Security related parameters. -->
<xsl:param name="Marketing"><xsl:value-of select="key('security','Marketing')/value" /></xsl:param>
<xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
<xsl:param name="PremierCircleAdmin"><xsl:value-of select="key('security','PremierCircleAdmin')/value" /></xsl:param>
<xsl:variable name="adminAction" select="'marketing'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Marketing Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
 	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

 	<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
  	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
	function goMain() {
		document.forms[0].adminAction.value = "mainMenu";
		submitForm("Main.do");
	}

  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		submitFormSsl("Login.do");
  	}

	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}

  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}

	function comingSoon() {
		alert("Coming soon!");
	}

	]]>
  	</script>
  </head>
  <body onload="javascript: init()">
    <form name="form" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Marketing Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="300" onClick="javascript: logoff();"/>
		</td>
	</tr>
	</table>
	<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
	    <tr>
			<td>
				<xsl:variable name="permissionView" select="'View'"/>
        <xsl:variable name="permissionYes" select="'Yes'"/>
				<!-- Check authorizaion -->
				<xsl:if test="$Marketing = $permissionView">
          <table width="98%" border="0" align="center">
            <tr>
              <td>
                <xsl:if test="$Marketing = $permissionView">
                  <table border="0" width="100%">
                    <tr>
                      <!-- Margin -->
                      <td width="7%">&nbsp;</td>
                      <!--Column 1 -->
                      <td width="44%" valign="top">
                        <table border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td class="label">Maintenance</td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/bulk/bulkload/SetupBulkOrder?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="30">Bulk Order</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/pdb/showImageFTP.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="40">FTP Image Upload</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/joe/iotw.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="45">Item Of The Week Maintenance</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/marketing/sourceListMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="50">Master Source Code Maintenance</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/marketing/sourceCodeSearch.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="60">Source Code Maintenance - Search</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/marketing/sourceTypeMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="70">Source Type Maintenance - Search</a>
                            </td>
                          </tr>
                          <xsl:if test="$PremierCircleAdmin = $permissionView">
                          <tr>
                            <td class="TextLink">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/marketing/premierCircleMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="70">Premier Circle Membership Maintenance</a>
                            </td>
                           </tr>
                           </xsl:if>                        
                           <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/marketing/productFeedSearch.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="70">Product Feed Maintenance - Search</a>
                            </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="label">Queues</td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              <a href="http://{$sitename}/loadmemberdata/servlet/ViewQueueServlet?action=loadQueue&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}"
                                 tabindex="80">View Queue</a>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <!-- Column 2 -->
                      <td width="44%" valign="top">
                        <table>
                          <xsl:if test="$ReportLinks = $permissionYes">
                            <tr>
                              <td class="label">Reports</td>
                            </tr>
                            <tr>
                              <td class="TextLink">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=marketing&amp;securitytoken={$securitytoken}"
                                   tabindex="90">Report Submission</a>
                              </td>
                            </tr>
                            <tr>
                              <td class="TextLink">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=marketing&amp;securitytoken={$securitytoken}"
                                   tabindex="100">Report Viewer</a>
                              </td>
                            </tr>
                          </xsl:if>
                        </table>
                      </td>
                    </tr>
                  </table>
                </xsl:if>
              </td>
            </tr>
          </table>
				</xsl:if>
			</td>
		</tr>
	</table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
			<td width="55%" align="right">
					<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="100" onclick="javascript: goMain()"/>
			</td>
			<td width="43%" align="right">
					<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="120" onClick="javascript: logoff()"/>
			</td>
	</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>