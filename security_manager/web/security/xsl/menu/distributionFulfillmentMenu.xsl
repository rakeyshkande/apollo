<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<!-- Security related parameters. -->
<xsl:param name="DistributionFulfillment"><xsl:value-of select="key('security','DistributionFulfillment')/value" /></xsl:param>
<xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
<xsl:param name="OSCARMaintenanceDisplay"><xsl:value-of select="key('security','OSCARMaintenanceDisplay')/value" /></xsl:param>
<xsl:param name="PEZMaintenance"><xsl:value-of select="key('security','PEZMaintenance')/value" /></xsl:param>

<xsl:variable name="adminAction" select="'distributionFulfillment'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Distribution/Fulfillment Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

  	<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
  	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		submitFormSsl("Login.do");
  	}

  	function goMain() {
		document.forms[0].adminAction.value = "mainMenu";
		submitForm("Main.do");
  	}

	function comingSoon() {
		alert("Coming soon!");
	}


	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}

	]]>
  </script>
  </head>
  <body onload="javascript: init()">
    <form name="form" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Distribution/Fulfillment Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="90" onClick="javascript: logoff();"/>
		</td>
	</tr>
	</table>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
	<td>
	<xsl:variable name="permissionView" select="'View'"/>
  <xsl:variable name="permissionYes" select="'Yes'"/>
	<table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
      <!--Margin-->
      <td width = "7%">&nbsp;</td>
      <!--Column 1 -->
      <td width = "44%" valign="top">
        <table width="100%">
          <tr><td class="label">Florist Maintenance</td></tr>
                    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "10" class="link" href="http://{$sitename}/loadmemberdata/servlet/FloristSearchServlet?action=loadFloristSearch&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" >Florist Profiles</a></td></tr>
          <tr><td>&nbsp;</td></tr>
  				<tr><td class="label">Queues</td></tr>
  				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "20" class="link" href="http://{$sitename}/loadmemberdata/servlet/ViewQueueServlet?action=loadQueue&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}">View Queue</a></td></tr>
          <tr><td>&nbsp;</td></tr>
   				<tr><td class="label">Application Settings</td></tr>
  				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "30" class="link" href="http://{$sitename}/loadmemberdata/servlet/FTDMShutdownServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}">FTDM Shutdown</a></td></tr>
                <xsl:if test="$PEZMaintenance = $permissionYes">
                	<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "30" class="link" href="http://{$sitename}/loadmemberdata/servlet/ExclusionZonesServlet?action=load&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}">PAS Exclusion Zone Maintenance</a></td></tr>
                </xsl:if>
          <tr><td>&nbsp;</td></tr>
                <xsl:if test="$OSCARMaintenanceDisplay = $permissionView">        
                    <tr><td class="label">OSCAR Maintenance</td></tr>
                    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "30" class="link" href="http://{$sitename}/loadmemberdata/servlet/OSCARFileUploadServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}">OSCAR File Maintenance</a></td></tr>
                </xsl:if>
        </table>
      </td>
      <!--Column 2 -->
      <td width = "44%" valign="top">
        <xsl:if test="$ReportLinks = $permissionYes">        
          <table width="100%">
            <tr><td class="label">Reports</td></tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "40" class="link" href="http://{$sitename}/loadmemberdata/servlet/MemberDataLoadServlet?action=view-florist-audit-report-list&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" >Florist Audit Report</a></td></tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "50" class="link" href="http://{$sitename}/loadmemberdata/servlet/MemberDataLoadServlet?action=view-florist-weight-audit-report-list&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" >Weight Audit Report</a></td></tr>
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "50" class="link" href="http://{$sitename}/loadmemberdata/servlet/MemberDataLoadServlet?action=view-zip-removals-report&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" >Zip Removals Report</a></td></tr>
	    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "60" class="link" href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=distribution&amp;securitytoken={$securitytoken}" >Report Submission</a></td></tr>  
            <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "70" class="link" href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=distribution&amp;securitytoken={$securitytoken}">Report Viewer</a></td></tr>
          </table>
        </xsl:if>
      </td>
    </tr>
  </table>
	</td>
	</tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
<tr>
		<td width="55%" align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="80" onclick="javascript: goMain();"/>
		</td>
		<td width="43%" align="right">
				<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="100" onClick="javascript: logoff();"/>
		</td>
</tr>
</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>