<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>

<!-- Security related parameters. -->
<xsl:param name="ResourceUser"><xsl:value-of select="key('security','ResourceUser')/value" /></xsl:param>
<xsl:param name="ResourceRole"><xsl:value-of select="key('security','ResourceRole')/value" /></xsl:param>
<xsl:param name="ResourceResource"><xsl:value-of select="key('security','ResourceResource')/value" /></xsl:param>
<xsl:param name="ResourceContext"><xsl:value-of select="key('security','ResourceContext')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Security Administration</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

	<script language="javascript">

	var context_js = '<xsl:value-of select="$applicationcontext"/>';
        var site_name = '<xsl:value-of select="$sitename"/>';
	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		document.forms[0].securitytoken.value = "";
		submitFormSsl("Login.do");
  	}

    function goExitMenu()
    {
        document.forms[0].adminAction.value = "securityAdministration";
        document.forms[0].isExit.value="Y";
        submitForm("Main.do");
    }

  	function goMain() {
  		document.forms[0].adminAction.value = "mainMenu";
      // Revert to unsecured HTTP.
		submitForm("Main.do");
  	}

	function goSystemAdministration() {
		document.forms[0].adminAction.value = "systemAdministration";
		// Revert to unsecured HTTP.
    submitForm("Main.do");
	}

	function submitForm(servlet) {
		document.forms[0].action = "http://" + site_name + servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}
        function goLink(servlet, admin_action) {
             document.forms[0].adminAction.value = admin_action;
             submitFormSsl(servlet);
        }
	]]>
	</script>
  </head>
  <body onload="javascript: init()">

    <form name="form" action="" method="post">
	<input type="hidden" name="adminAction" value=""/>
	<input type="hidden" name="actionType" value="view"/>
	<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
	<input type="hidden" name="context" value="{$context}"/>
	<input type="hidden" name="identity" value="{$identity}"/>
        <input type="hidden" name="isExit" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Security Administration'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="exit" Value="Exit" tabindex="16" onClick="javascript: goExitMenu();"/>
		</td>
	</tr>
	</table>
     <table width="98%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
        	<td>
        		<table width="100%">
        			<xsl:variable name="permissionView" select="'View'"/>
                    <!-- Check authorizaion -->
                    <xsl:if test="$ResourceUser = $permissionView">
                    <tr>
                        <td class="label"><font color="#000099"><li>Manage User</li></font></td>
                    </tr>
                    <tr>
                    	<td>
                            <table width="100%">
                            	<tr>
                                    <td width="10%"></td>
                            		<td><a class="link" href="javascript:goLink('SecurityUser.do','display_add')">Create User</a></td>
                                </tr>
                            	<!--tr>
                                    <td width="10%"></td>
                            		<td><a class="link" href="/{$applicationcontext}/security/SecurityUser.do?adminAction=view_all&amp;context={$context}&amp;securitytoken={$securitytoken}">View List of Users</a></td>
                                </tr-->
                            	<tr>
                                    <td width="10%"></td>
                                    <td><a class="link" href="javascript:goLink('FileUpload.do','viewUpload')">Create User Batch Load</a></td>
                                </tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('SecurityUser.do','display_search')">Search for User</a></td>
                                </tr>
                                <tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('FileUpload.do','viewUserGroupUpdate')">Update User Group Batch Load</a></td>
                                </tr>
                                 <tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('FileUpload.do','viewBulkUserTerminate')">Apollo Bulk Termination</a></td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    </xsl:if>
                    <xsl:if test="$ResourceRole = $permissionView">
                    <tr>
                        <td class="label"><font color="#000099"><li>Manage Role</li></font></td>
                    </tr>
                    <tr>
                    	<td>
                            <table width="100%">
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Role.do','display_add')">Create Role</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Role.do','view_all')">View List of Roles</a></td>
                            	</tr>
                            </table>
                        </td>
                    </tr>
                    </xsl:if>
                    <xsl:if test="$ResourceResource = $permissionView">
                    <tr>
                        <td class="label"><font color="#000099"><li>Manage Resource Group</li></font></td>
                    </tr>
                    <tr>
                    	<td>
                            <table width="100%">
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Resource.do','display_add')">Create Resource</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Resource.do','view_all')">View List of Resources</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Permission.do','display_add')">Create Permission</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Permission.do','view_all')">View List of Permissions</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('ACL.do','display_add')">Create Resource Group</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('ACL.do','view_all')">View List of Resource Groups</a></td>
                            	</tr>
                            </table>
                        </td>
                    </tr>
                    </xsl:if>
                    <xsl:if test="$ResourceContext = $permissionView">
                    <tr>
                        <td class="label"><font color="#000099"><li>Manage Context</li></font></td>
                    </tr>
                    <tr>
                    	<td>
                            <table width="100%">
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Context.do','display_add')">Create Context</a></td>
                            	</tr>
                            	<tr>
                                    <td width="10%"></td>
                                        <td><a class="link" href="javascript:goLink('Context.do','view_all')">View List of Contexts</a></td>
                            	</tr>
                            </table>
                        </td>
                    </tr>
					</xsl:if>
                </table>
        	</td>
        </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
			<td width="55%" align="right">
					<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" onclick="javascript: goMain();"/>
			</td>
			<td width="43%" align="right">
					<input type="button" class="BlueButton" name="exit" Value="Exit" onClick="javascript: goExitMenu();"/>
			</td>
	</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>