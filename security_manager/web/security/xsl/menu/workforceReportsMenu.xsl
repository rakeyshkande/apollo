<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="../footer.xsl"/>
  <xsl:import href="../securityanddata.xsl"/>
  <xsl:output method="html" indent="yes"/>

  <xsl:key name="security" match="/ROOT/security/data" use="name"/>

  <xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
  <xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
  <xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
  <xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
  <xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
  <xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
  <xsl:param name="reportserver"><xsl:value-of select="key('security','reportserver')/value" /></xsl:param>
  <!-- Security related parameters. -->
  <xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
  <xsl:variable name="adminAction" select="'marketing'"/>
  <xsl:template match="/">
    <html>
      <head>
        <title>FTD - Workforce Reports Menu</title>
        <link rel="stylesheet" type="text/css"
              href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
        <script type="text/javascript"
                src="/{$applicationcontext}/security/js/clock.js">
        </script>
        <script type="text/javascript"
                src="/{$applicationcontext}/security/js/util.js">
        </script>
        <script language="javascript">
          var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
          <![CDATA[

          function init() {
              	setNavigationHandlers();
              }

          	function goMain() {
          		document.forms[0].adminAction.value = "mainMenu";
          		submitForm("Main.do");
          	}

            function goCustomerService() {
              document.forms[0].adminAction.value = "customerService";
              submitForm("Main.do");
            }

          	function logoff() {
        		document.forms[0].adminAction.value = "logoff";
        		submitFormSsl("Login.do");
          	}
          
          	function submitForm(servlet) {
              var servlet_prefix = "/" + document.getElementById('applicationcontext').value + "/security/";
          		document.forms[0].action = servlet_prefix + servlet;
          		document.forms[0].submit();
          	}
            
            function submitFormSsl(servlet) {
		          var servlet_prefix = "/" + document.getElementById('applicationcontext').value + "/security/";
              document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		          document.forms[0].submit();
	          }
          	
          	function comingSoon() {
          		alert("Coming soon!");
          	}

          	]]>
        </script>
      </head>
      
  <body onload="javascript: init()">      
<!--  <body>-->
        <form name="form" method="post">
          <xsl:call-template name="securityanddata"/>
          <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
          <input type="hidden" name="context" value="{$context}"/>
          <input type="hidden" name="adminAction" value=""/>
          <input type="hidden" name="identity" value="{$identity}"/>
          <input type="hidden" name="applicationcontext" value="{$applicationcontext}"/>
          <table width="98%" border="0" align="center" cellpadding="0"
                 cellspacing="0">
            <tr>
              <td width="20%" align="left">
                <img border="0"
                     src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif"
                     width="131" height="32"/>
              </td>
              <td width="60%" align="center" colspan="1" class="header">Workforce Reports</td>
              <td width="20%" align="right" class="label">
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td id="time" align="right" class="label">
                    </td>
                    <script type="text/javascript">startClock();</script>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <hr/>
              </td>
            </tr>
          </table>
          <table width="98%" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td align="right">
                <input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="90"
                       onClick="javascript: goCustomerService();"/>
              </td>
            </tr>
          </table>
          <table width="98%" border="3" align="center" cellpadding="1"
                 cellspacing="1" bordercolor="#006699">
            <tr>
              <td>
                <xsl:variable name="permissionView" select="'View'"/>
                <xsl:variable name="permissionYes" select="'Yes'"/>
                <table border="0" width="100%">
                  <tr>
                    <td width="7%">&nbsp;</td>
                    <td width="93%" valign="top">
                      <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                          <td class="label">Reports</td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/CSRProductivity.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="10">CSR Scrub Productivity Report</a></td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/ScrubStatistics.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="20">Order Scrub Statistics Report</a></td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/RemovedOrders.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="30">Removed Orders Report</a></td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/OrdersScrubbedByCSR.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="40">Scrub Orders by CSR Report</a></td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" tabindex="50" href="javascript:void(0)" onclick="window.open ('html/reports/OrdersByHalfHour.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')">Scrub Orders by Half Hour Report</a></td>
                        </tr>
                        <xsl:if test="$ReportLinks = $permissionYes">
                          <tr>
                            <td class="TextLink">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                
                              <a href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=customerservice&amp;securitytoken={$securitytoken}"
                                 tabindex="60">Report Submission</a>
                            </td>
                          </tr>
                          <tr>
                            <td class="TextLink">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                
                              <a href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=customerservice&amp;securitytoken={$securitytoken}"
                                 tabindex="70">Report Viewer</a>
                            </td>
                          </tr>
                        </xsl:if>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table width="98%" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td width="100%" align="right">
                <input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="80"
                       onClick="javascript: goCustomerService()"/>
              </td>
            </tr>
          </table>
        </form>
        <xsl:call-template name="footer"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
