<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<!-- Security related parameters. -->
<xsl:param name="ResourceSecurity"><xsl:value-of select="key('security','ResourceSecurity')/value" /></xsl:param>
<xsl:param name="ResetPasswordAdmin"><xsl:value-of select="key('security','ResetPasswordAdmin')/value" /></xsl:param>

<xsl:variable name="adminAction" select="'systemAdministration'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - System Administration</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	<script language="javascript">
	
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
  var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
      window.history.back();
  	}
  	
  	function goMain() {
  		document.forms[0].adminAction.value = "mainMenu";
		submitForm("Main.do");
  	}

	function goSecurityAdministration() {
		document.forms[0].adminAction.value = "securityAdministration";
		document.forms[0].sourceMenu.value="systemAdministration";
		document.forms[0].isExit.value="N";
    
    // Compose the full URL for https.
    // Substitute the secure SSL port in the site name, if the port already exists.
		submitFormSsl("Main.do");
	}
	
	function goRestPassword() {
		document.forms[0].adminAction.value = "resetUserPassword";
		document.forms[0].sourceMenu.value="systemAdministration";
		document.forms[0].isExit.value="N";
    
		submitFormSsl("Main.do");
	}
	
	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}
	]]>
	</script>
  </head>
  <body onload="javascript: init()">

    <form name="form" action="" method="post">
	<input type="hidden" name="adminAction" value=""/>
	<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
	<input type="hidden" name="context" value="{$context}"/>
	<input type="hidden" name="identity" value="{$identity}"/>
    <input type="hidden" name="isExit" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'System Administration'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="4" onClick="javascript: goMain();"/>
		</td>
	</tr>
	</table>
	<xsl:variable name="permissionView" select="'View'"/>
  <table width="98%" align = "center" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
   <tr>
        	<td align = "center" width = "100%">
        		<table>
              <tr><td class="TextLink"><a href="http://{$sitename}/opsadmin/servlet/OpsAdminServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="1">Operations Administration</a></td></tr>
              <xsl:if test="$ResourceSecurity = $permissionView">
                <tr><td class="TextLink"><a href="javascript:goSecurityAdministration()" tabindex="2">Security Administration</a></td></tr>
              </xsl:if>
              <xsl:if test="$ResetPasswordAdmin = $permissionView">
                <tr><td class="TextLink"><a href="javascript:goRestPassword()" tabindex="3">Reset Password Administration</a></td></tr>
              </xsl:if>      	   
            </table>
        	</td>
        </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
			<td width="55%" align="right">
					<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="3" onclick="javascript: goMain();"/>
			</td>
			<td width="43%" align="right">
					<input type="button" class="BlueButton" name="Exit" Value="Exit" tabindex="5" onClick="javascript: goMain();"/>
			</td>
	</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
