<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="reportserver"><xsl:value-of select="key('security','reportserver')/value" /></xsl:param>

<!-- Security related parameters. -->
<xsl:param name="AccountingFinance"><xsl:value-of select="key('security','AccountingFinance')/value" /></xsl:param>
<xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
<xsl:param name="OutstandingReceivablesLink"><xsl:value-of select="key('security','OutstandingReceivablesLink')/value" /></xsl:param>
<xsl:param name="TaxAdminLink"><xsl:value-of select="key('security','TaxAdminLink')/value" /></xsl:param>
<xsl:variable name="adminAction" select="'accounting'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Accounting/Finance Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
  	<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
  	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }

    function logoff() {
	document.forms[0].adminAction.value = "logoff";
	submitFormSsl("Login.do");
    }

   function goMain() {
  	document.forms[0].adminAction.value = "mainMenu";
	submitForm("Main.do");
   }

   function comingSoon() {
	alert("Coming soon!");
   }

   function submitForm(servlet) {
	document.forms[0].action = servlet_prefix + servlet;
	document.forms[0].submit();
   }

   function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}
   ]]>
  </script>
  </head>

  <body onload="javascript: init()">
    <form name="form" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Accounting/Finance Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>

    <table width="98%" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td align="right">
			<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="300" onClick="javascript: logoff();"/>
		</td>
	</tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr><td><table width="98%">
	<xsl:variable name="permissionView" select="'View'"/>
  <xsl:variable name="permissionYes" select="'Yes'"/>
	<!-- Check authorizaion -->
	<xsl:if test="$AccountingFinance = $permissionView">
 		<tr>
      <td width="8%">&nbsp;</td>
		 	<td width="46%">
  	 	<table width="98%" border="0" cellpadding="2" cellspacing="2">
   				<tr><td class="label">Queues</td></tr>
          <tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/loadmemberdata/servlet/ViewQueueServlet?action=loadQueue&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="10">View Queue</a></td>
       		</tr>
          <xsl:if test="$ReportLinks = $permissionYes">
      				<tr><td class="label">Reports</td></tr>
              <tr>
           				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript:void(0)" onclick="window.open ('html/reports/WalMartPriceVarianceReport.html?repoServer={$reportserver}','newWin','scrollbars=yes,status=no,menubar=no,width=550,height=250')" tabindex="1">Wal-Mart Price Variance Report</a></td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="20">Report Submission</a></td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="30">Report Viewer</a></td>
              </tr>
          </xsl:if>
          <xsl:if test="$OutstandingReceivablesLink = $permissionYes">
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/ReceivablesAction.do?action_type=load&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_all" tabindex="30">Outstanding Receivables</a></td>
              </tr>
          </xsl:if>
  		</table>
			</td>
			<td width="46%">
  	 	<table width="98%" border="0" cellpadding="2" cellspacing="2">
  	 		<xsl:if test="$TaxAdminLink = $permissionView">
   				<tr><td class="label">Tax Service</td></tr>
          <tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/TaxAdminAction.do?action=load&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;siteName={$sitename}" tabindex="10">Edit Tax Settings</a></td>
       		</tr>
			<tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			<tr>
     				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
          </xsl:if>
  		</table>
			</td>
		</tr>
	</xsl:if>
  </table></td></tr>
    </table>

    <table width="98%" border="0" cellpadding="1" cellspacing="1">
    <tr>
	<td width="55%" align="right">
		<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="40" onclick="javascript: goMain();"/>
	</td>
	<td width="43%" align="right">
		<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="50" onClick="javascript: logoff();"/>
	</td>
    </tr>
    </table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>

</xsl:template>
</xsl:stylesheet>
