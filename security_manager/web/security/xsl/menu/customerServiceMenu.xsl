<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="../footer.xsl"/>
	<xsl:import href="../securityanddata.xsl"/>
	<xsl:output method="html" indent="yes"/>
    <xsl:key name="security" match="/ROOT/security/data" use="name"/>
    <xsl:variable name="YES" select="'Y'"/>
    <xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
    <xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
    <xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
    <xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
    <xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
    <xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
    <xsl:param name="breURL"><xsl:value-of select="key('security','breURL')/value" /></xsl:param>
    <xsl:param name="marsreportsURL"><xsl:value-of select="key('security','marsreportsURL')/value" /></xsl:param>
    
    <xsl:param name="call_dnis"><xsl:value-of select="key('security','call_dnis')/value" /></xsl:param>
    <xsl:param name="call_brand_name"><xsl:value-of select="key('security','call_brand_name')/value" /></xsl:param>
    <xsl:param name="call_cs_number"><xsl:value-of select="key('security','call_cs_number')/value" /></xsl:param>
    <xsl:param name="call_type_flag"><xsl:value-of select="key('security','call_type_flag')/value" /></xsl:param>
    <xsl:param name="t_call_log_id"><xsl:value-of select="key('security','t_call_log_id')/value" /></xsl:param>
    <xsl:param name="dnis_error_msg"><xsl:value-of select="key('security','dnis_error_msg')/value" /></xsl:param>
	<!-- Security related parameters. -->
    <xsl:param name="CustomerService"><xsl:value-of select="key('security','CustomerService')/value" /></xsl:param>
    <xsl:param name="OrderScrub"><xsl:value-of select="key('security','OrderScrub')/value" /></xsl:param>
    <xsl:param name="ReinstateOrder"><xsl:value-of select="key('security','ReinstateOrder')/value" /></xsl:param>
    <xsl:param name="BulkOrder"><xsl:value-of select="key('security','BulkOrder')/value" /></xsl:param>
    <xsl:param name="CSManagement"><xsl:value-of select="key('security','CSManagement')/value" /></xsl:param>
    <xsl:param name="LossPreventionSearch"><xsl:value-of select="key('security','LossPreventionSearch')/value" /></xsl:param>
    <xsl:param name="ManageUser"><xsl:value-of select="key('security','ManageUser')/value" /></xsl:param>
    <xsl:param name="ViewQueue"><xsl:value-of select="key('security','ViewQueue')/value" /></xsl:param>
    <xsl:param name="GiftCert"><xsl:value-of select="key('security','GiftCert')/value" /></xsl:param>
    <xsl:param name="CustomerCatalogRequest"><xsl:value-of select="key('security','CustomerCatalogRequest')/value" /></xsl:param>
    <xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
    <xsl:param name="UnlockOrder"><xsl:value-of select="key('security','UnlockOrder')/value" /></xsl:param>
    <xsl:param name="QueueManagement"><xsl:value-of select="key('security','QueueManagement')/value" /></xsl:param>
    <xsl:param name="OldWebOE"><xsl:value-of select="key('security','OldWebOE')/value" /></xsl:param>
    <xsl:param name="ProdFavoritesMaintLink"><xsl:value-of select="key('security','ProdFavoritesMaintLink')/value" /></xsl:param>
    <xsl:param name="CardMsgMaintLink"><xsl:value-of select="key('security','CardMsgMaintLink')/value" /></xsl:param>
    <xsl:param name="CallDispMaintLink"><xsl:value-of select="key('security','CallDispMaintLink')/value" /></xsl:param>
    <xsl:param name="ServicesInfoLink"><xsl:value-of select="key('security','ServicesInfoLink')/value" /></xsl:param>
    <xsl:param name="PhoenixAdmin"><xsl:value-of select="key('security','PhoenixAdmin')/value" /></xsl:param>
    <xsl:param name="MarsReportsLink"><xsl:value-of select="key('security','MarsReportsLink')/value" /></xsl:param>
    <xsl:param name="MarsBreLink"><xsl:value-of select="key('security','MarsBreLink')/value" /></xsl:param>
    <xsl:param name="MarsSimulatorLink"><xsl:value-of select="key('security','MarsSimulatorLink')/value" /></xsl:param>
	<xsl:variable name="adminAction" select="'customerService'"/>
  
	<xsl:template match="/">
		<html>
			<head>
				<title>FTD - Customer Service Menu</title>
				<link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
				<link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftd.css"/>
				<script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
				<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
				<script language="javascript">
  	var context_js = '<xsl:value-of select="$applicationcontext"/>';
  	var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";
    function init()
    {
    	setNavigationHandlers();
    	if (document.getElementById('dnis_num').disabled == false)
    	{
    	   if(document.getElementById('dnis_num').value == '')
    	   {
            document.getElementById('dnis_num').focus();
    	   }
    	}
      else
      {
        document.getElementById('WOE').focus();
      }
      var DNIS = document.getElementById("call_dnis").value;
      if (DNIS == '')
      {
        if (document.getElementById("end_call_but") != null)
        {
          document.getElementById("end_call_but").disabled = true;
        }
      }
   }

    function checkForLogId()
    {
      return true;
    }

    function goWorkforceReports()
    {
	var result = checkForLogId();

    	if(result)
    	{     
		document.forms[0].adminAction.value = "workforceReports";
      		document.forms[0].sourceMenu.value="customerService";
  		document.forms[0].isExit.value="N";
	        submitForm("Main.do");
        }
    }

  	function logoff() {

    var result = checkForLogId();

    if(result)
    {
		  document.forms[0].adminAction.value = "logoff";
		  submitFormSsl("Login.do");
    }
  	}

  	function goMain()
    {
      var result = checkForLogId();

      if(result)
      {
        document.forms[0].adminAction.value = "mainMenu";
        submitForm("Main.do");
      }
  	}

	function comingSoon()
  {
		alert("Coming soon!");
	}

	function goManageUser()
  {
		document.forms[0].adminAction.value = "securityAdministration";
		document.forms[0].sourceMenu.value="customerService";
		document.forms[0].isExit.value="N";
		submitFormSsl("Main.do");
	}

	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
		document.forms[0].submit();
	}


	function doDNISAction()
	{
	  var site = document.getElementById('site_name').value
	  document.forms[0].action = "http://" + site + "/customerordermanagement/Dnis.do";
		document.forms[0].submit();
	}

	function doCallReasonCode()
	{
	  var site = document.getElementById('site_name').value
    // Call log is passed to the CallReasonCode.do.  It is stored as a hidden field
	  document.forms[0].action = "http://" + site + "/customerordermanagement/CallReasonCode.do";
	  document.forms[0].submit();
	}

    function entrykey()
    {
    if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;

             if(window.event.keyCode == 13)
	             {
	             if (document.getElementById('dnis_num').value == '')
	             alert('Please Enter a Dnis Number')
	             else
	             doDNISAction();
	             }
   }

   function checkKey()
   {
     if(window.event.keyCode != 13)
     {
      	document.getElementById('dnis_num').focus();
     }
   }
   
   function menuServicesInfoPopUp() {
   var result = checkForLogId();
    if(result)
      {      
        var site = document.getElementById('site_name').value;
       
		var menuSecuritytoken = document.getElementById('securitytoken').value;        
        var menuContext = document.getElementById('context').value;       
        var url = "http://" + site + "/customerordermanagement/customerOrderSearch.do?action=services_info_lookup_menu&adminAction=customerService"+
        "&securitytoken=" + menuSecuritytoken + "&context=" + menuContext ;
                                       
    	var sFeatures="dialogHeight:240px;dialogWidth:700px;";
        sFeatures +=  "center:yes;status:0;scroll:0;resizable:0";
     	showModalDialogIFrame(url, "" ,sFeatures);
	    
   	  }
   }
	]]>
				</script>
			</head>
			<body onload="javascript: init()" onkeypress="checkKey()">
				<form name="form" method="post">
					<input type="hidden" name="securitytoken" value="{$securitytoken}"/>
					<input type="hidden" name="context" value="{$context}"/>
					<input type="hidden" name="adminAction" value=""/>
					<input type="hidden" name="identity" value="{$identity}"/>
					<input type="hidden" name="isExit" value=""/>
					<input type="hidden" name="call_dnis" value="{$call_dnis}"/>
					<input type="hidden" name="t_call_log_id" value="{$t_call_log_id}"/>
					<input type="hidden" name="site_name" value="{$sitename}"/>
                                        <xsl:call-template name="securityanddata"/>
					<!-- Header-->
					<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%" align="left">
								<img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/>
							</td>
							<td width="60%" align="center" colspan="1" class="header">Customer Service Menu</td>
							<td width="20%" align="right" class="label">
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td id="time" align="right" class="label"></td>
										<script type="text/javascript">startClock();</script>
									</tr>
								</table>
							</td>
						</tr>
						<td colspan="3">
							<div class="floatleft">
								<xsl:choose>
									<xsl:when test="$call_dnis != '' and $call_dnis ='0000'">
										<span class="PopupHeader"></span>
									</xsl:when>
									<xsl:when test="$call_dnis != '' and $call_brand_name != ''">
										<span class="PopupHeader">
											<xsl:value-of select="$call_dnis"/> - 
											<xsl:value-of select="$call_brand_name" />
										</span>
									</xsl:when>
									<xsl:when test="$call_dnis != ''">
										<span class="PopupHeader">
											<xsl:value-of select="$call_dnis"/>
										</span>
									</xsl:when>
									<xsl:when test="$call_brand_name != ''">
										<span class="PopupHeader">
											<xsl:value-of select="$call_brand_name"/>
										</span>
									</xsl:when>
								</xsl:choose>
							</div>
							<div class="floatright" >
								<span class="PopupHeader" >
									<xsl:value-of select="$call_cs_number"/>&nbsp;
									<xsl:value-of select="$call_type_flag"/>
								</span>
							</div>
						</td>
						<tr>
							<td colspan="3">
								<hr/>
							</td>
						</tr>
					</table>
					<!-- Display table-->
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td align="right">
								<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="300" onClick="javascript: logoff();"/>
							</td>
						</tr>
					</table>
          <xsl:variable name="permissionView" select="'View'"/>
          <xsl:variable name="permissionYes" select="'Yes'"/>
          <table width="98%" border="3" align="center" cellpadding="1"
                 cellspacing="1" bordercolor="#006699">
            <tr>
              <td>
                <table width="98%" border="0" cellpadding="2" cellspacing="2">
    							<tr class="PopupHeader">
      							<xsl:choose>
        							<xsl:when test="$call_dnis = ''">
  											<td colspan="4" align="center">
                          <b>DNIS Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
													<input name="dnis_num" id="dnis_num" type="text" value="" size="10" maxlength="4" tabindex="10" onkeypress="entrykey();"/>
                          <xsl:if test="$dnis_error_msg != ''">
                            <span class="errorMessage">
                              &nbsp;&nbsp;&nbsp;<xsl:value-of select="$dnis_error_msg"/>
                            </span>
                          </xsl:if>
												</td>
 											</xsl:when>
 											<xsl:when test="$call_dnis = '0000'">
  											<td colspan="4" align="center">
  												<b>DNIS Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
													<input name="dnis_num" id="dnis_num" type="text" size="10" maxlength="4" disabled="true" style="background-color:#CCCCCC;" value=""/>
                          <xsl:if test="$dnis_error_msg != ''">
                            <span class="errorMessage">
                              &nbsp;&nbsp;&nbsp;<xsl:value-of select="$dnis_error_msg"/>
                            </span>
                          </xsl:if>
												</td>
 											</xsl:when>
 											<xsl:otherwise>
  											<td colspan="4" align="center">
  												<b>DNIS Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
  												<input name="dnis_num" id="dnis_num" type="text" size="10" maxlength="4" disabled="true" value="{$call_dnis}"/>
  											</td>
                          <xsl:if test="$dnis_error_msg != ''">
                            <span class="errorMessage">
                              &nbsp;&nbsp;&nbsp;<xsl:value-of select="$dnis_error_msg"/>
                            </span>
                          </xsl:if>
 											</xsl:otherwise>
										</xsl:choose>
                    </tr>
                    <tr>
                      <!--Margin-->
                      <td width="7%">&nbsp;</td>
                      <!--Column One-->
                      <td valign="top">
                        <table>
                          <tr>
                            <xsl:choose>
                              <xsl:when test="$OldWebOE = $permissionView">
                                <td><a class="link" href="http://{$sitename}/oe/servlet/DNISServlet?context={$context}&amp;sessionId={$securitytoken}" name="WOE" id="WOE" tabindex="20" onClick="return checkForLogId()">Order Entry</a></td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td><a class="link" href="https://{$sitename}/joe/oe.do?context={$context}&amp;securitytoken={$securitytoken}" name="JOE" id="JOE" tabindex="20" onClick="return checkForLogId()">Order Entry</a></td>
                              </xsl:otherwise>
                            </xsl:choose>
                          </tr>
                          <tr>
                              <td><a class="link" href="http://{$sitename}/customerordermanagement/Dnis.do?context={$context}&amp;securitytoken={$securitytoken}&amp;adminAction={$adminAction}&amp;call_dnis={$call_dnis}&amp;call_brand_name={$call_brand_name}&amp;call_cs_number={$call_cs_number}&amp;call_type_flag={$call_type_flag}&amp;action=customer_service" tabindex="30">Customer Order Maintenance</a></td>
                          </tr>
                        <xsl:if test="$LossPreventionSearch = $permissionView">
                          <tr>
                              <td><a class="link" href="http://{$sitename}/customerordermanagement/Dnis.do?context={$context}&amp;securitytoken={$securitytoken}&amp;adminAction={$adminAction}&amp;call_dnis={$call_dnis}&amp;call_brand_name={$call_brand_name}&amp;call_cs_number={$call_cs_number}&amp;call_type_flag={$call_type_flag}&amp;&amp;action=loss_prevention_search" tabindex="30">Loss Prevention Search</a></td>
                          </tr>
                        </xsl:if>
         									<xsl:if test="$OrderScrub = $permissionView">                            
                            <tr>
                                <td class="label">Order Scrub</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/scrub/servlet/ScrubSearchServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;sc_mode=S" tabindex="40" onClick="return checkForLogId()">Unscrubbed Orders</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/scrub/servlet/ScrubSearchServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;sc_mode=P" tabindex="50" onClick="return checkForLogId()">Pending Orders</a></td>
                            </tr>
           									<xsl:if test="$ReinstateOrder = $permissionView">
                              <tr>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/scrub/servlet/ScrubSearchServlet?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;sc_mode=R" tabindex="60" onClick="return checkForLogId()">Re-Instate Orders</a></td>
                              </tr>
                            </xsl:if>
                          </xsl:if>
                          <tr>
                             <td><a class="link" href="http://{$sitename}/queue/QueueMenu.do?context={$context}&amp;securitytoken={$securitytoken}" tabindex="70" onClick="return checkForLogId()">Queues Main Menu</a></td>
                          </tr>
                          <tr>
                              <td><a tabIndex = "75" class="link" href="http://{$sitename}/loadmemberdata/servlet/FloristSearchServlet?action=loadFloristSearch&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" onClick="return checkForLogId()">Florist Search</a></td>
                          </tr>
                          <xsl:if test="$ServicesInfoLink = $permissionView">
                          <tr>
                              <td><a tabIndex = "76" class="link" href="#" onclick="javascript: menuServicesInfoPopUp()">Services Info</a></td>                                  
                          </tr>
                          </xsl:if>
                        </table>
                      </td>
                      <!-- Column 2-->
                      <td valign="top">
                        <table>
                          <xsl:if test="$CustomerCatalogRequest = $permissionView or $ManageUser = $permissionView or $CSManagement = $permissionView or $ViewQueue = $permissionView or $CustomerService = $permissionView">
                            <tr>
                                <td class="label">Management</td>                            
                            </tr>
                          </xsl:if>
                            <xsl:if test="$CustomerCatalogRequest = $permissionView">
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/catNewsRequest.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;gcc_user=user_cs" tabindex="90" onClick="return checkForLogId()">Customer Catalog Request</a></td>
                            </tr>
                          </xsl:if>
       										<xsl:if test="$ManageUser = $permissionView">
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="javascript: goManageUser()" tabindex="100" onClick="return checkForLogId()">Manage Users</a></td>
                            </tr>
                          </xsl:if>
       										<xsl:if test="$CSManagement = $permissionView">
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadRefunds.do?action_type=RefundReview&amp;page_position=1&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}&amp;start_origin=Refund Review Screen" tabindex="110" onClick="return checkForLogId()">Refund Review</a></td>
                            </tr>
                          </xsl:if>
       										<xsl:if test="$ViewQueue = $permissionView">
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/loadmemberdata/servlet/ViewQueueServlet?action=loadQueue&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="120" onClick="return checkForLogId()">View Queue</a></td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="$QueueManagement = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/queueMessageSearch.do?action_type=load&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="121" onClick="return checkForLogId()">Batch Queue Management - Search</a></td>
                          </tr>
                        </xsl:if>
                          <xsl:if test="$QueueManagement = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/queueMessageDelete.do?action_type=load&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="122" onClick="return checkForLogId()">Batch Queue Management - Delete</a></td>
                          </tr>
                        </xsl:if>
                         <xsl:if test="$QueueManagement = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/queueBatchStatus.do?action=loadData&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="122" onClick="return checkForLogId()">Batch Queue Management - Status</a></td>
                          </tr>
                        </xsl:if>
                          <xsl:if test="$UnlockOrder = $permissionYes">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/unlockRecord.do?action=load&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}" tabindex="130" onClick="return checkForLogId()">Unlock Orders</a></td>
                          </tr>
                          </xsl:if>
       							      <xsl:if test="$CustomerService = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/marketing/updateUnattachedEmail.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;gcc_user=user_cs" tabindex="140" onClick="return checkForLogId()">Update Unattached Email Address</a></td>
                          </tr>
                        </xsl:if>
						<xsl:if test="$MarsBreLink = $permissionView">
						<tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="{$breURL}" target="_blank" tabindex="150" onClick="return checkForLogId()">Message Automation Response System (MARS)</a></td>
                          </tr>
                           </xsl:if>
                           
                        <xsl:if test="$MarsSimulatorLink = $permissionView">
						<tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/mars-simulator/marsSimulator?action=load&amp;adminAction={$adminAction}&amp;context={$context}&amp;securitytoken={$securitytoken}&amp;sitename={$sitename}" tabindex="150" onClick="return checkForLogId()">MARS Simulator</a></td>
                          </tr>
                           </xsl:if>
                   
                        <xsl:if test="$CSManagement = $permissionView or $GiftCert = $permissionView or $ProdFavoritesMaintLink = $permissionView or $CardMsgMaintLink = $permissionView">
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="label">Maintenance</td>
                          </tr>
                        </xsl:if>
     										<xsl:if test="$CSManagement = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadDnisMaintenance.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;page_position=1" tabindex="150" onClick="return checkForLogId()">DNIS Maintenance</a></td>
                          </tr>
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadMsgMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_titles&amp;message_type=Email" tabindex="160" onClick="return checkForLogId()">Stock Email Maintenance</a></td>
                          </tr>
                         <!--  <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/customerordermanagement/loadMsgMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;action_type=load_titles&amp;message_type=Letter" tabindex="170" onClick="return checkForLogId()">Stock Letter Maintenance</a></td>
                          </tr> -->
                        </xsl:if>
                        <xsl:if test="$GiftCert = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/CreateGiftCertAction.do?context={$context}&amp;securitytoken={$securitytoken}&amp;gcc_action=display_create&amp;gcc_type=CS&amp;gcc_user=user_cs" tabindex="180" onClick="return checkForLogId()">Create Coupons</a></td>
                          </tr>
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/SearchGiftCertAction.do?context={$context}&amp;securitytoken={$securitytoken}&amp;gcc_action=display_search&amp;gcc_user=user_cs" tabindex="190" onClick="return checkForLogId()">Gift Certificates/Coupon Maintenance</a></td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="$ProdFavoritesMaintLink = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/joe/favorites.do?context={$context}&amp;securitytoken={$securitytoken}&amp;adminAction={$adminAction}" tabindex="191" onClick="return checkForLogId()">Favorite Products Maintenance</a></td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="$CardMsgMaintLink = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/joe/cardMessageMaint.do?context={$context}&amp;securitytoken={$securitytoken}&amp;adminAction={$adminAction}" tabindex="192" onClick="return checkForLogId()">Card Message Phrase Maintenance</a></td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="$CallDispMaintLink = $permissionView">
                          <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/decisionframework/adminMain.do?decisionType=calldisposition&amp;method=adminMain&amp;context={$context}&amp;securitytoken={$securitytoken}&amp;adminAction={$adminAction}" tabindex="193" onClick="return checkForLogId()">Call Disposition Maintenance</a></td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="$PhoenixAdmin = $permissionView">
                        <tr>
				            <td>
				              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ProductMappingMaintenanceAction.do?context={$context}&amp;action_type=load&amp;selected_context=MERCHANDISING&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="60">Phoenix Product Mapping Maintenance</a>
				            </td>
				        </tr>
				        </xsl:if>
                       </table>
                      </td>
                      <!-- Column 3-->                      
                      <td valign="top">
                        <table>
         									<xsl:if test="$OrderScrub = $permissionView or $ReportLinks = $permissionYes"> 
                            <tr>
                              <td class="label">Reports</td>
                            </tr>
                          </xsl:if>
         									<xsl:if test="$OrderScrub = $permissionView">
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/opsadmin/servlet/ManagerDashboardServlet?context={$context}&amp;securitytoken={$securitytoken}" tabindex="200" onClick="return checkForLogId()">Scrub Dashboard</a></td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="$ReportLinks = $permissionYes">       
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "220" class="link" href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=customerservice&amp;securitytoken={$securitytoken}" onClick="return checkForLogId()">Report Submission</a></td>
                            </tr>  
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "230" class="link" href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=customerservice&amp;securitytoken={$securitytoken}" onClick="return checkForLogId()">Report Viewer</a></td>
                            </tr>
                            <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a tabIndex = "240" class="link" href="#" onclick="javascript: goWorkforceReports()">Workforce Reports</a></td>
                            </tr>
                                 </xsl:if>
                            <xsl:if test="$MarsReportsLink = $permissionView">       
                              <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/MarsReportsAdminAction.do?action=Submit&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;siteName={$sitename}" tabindex="250" onClick="return checkForLogId()">MARS Reports Submission</a></td>
                            </tr>
                             <tr>
                              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/MarsReportsAdminAction.do?action=load&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}&amp;siteName={$sitename}" tabindex="250" onClick="return checkForLogId()">MARS Reports Viewer</a></td>
                             </tr>
                          </xsl:if>
                        </table>
                      </td>
                    </tr>
                </table>
              </td>
            </tr>
          </table>
					<table width="98%" border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td width="55%" align="right">
								<input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="250" onclick="javascript: goMain();"/>
							</td>
							<td width="43%" align="right">
								<input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="270" onClick="javascript: logoff();"/>
							</td>
						</tr>
					</table>
				</form>
				<xsl:call-template name="footer"/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
