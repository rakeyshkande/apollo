<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>

<xsl:param name="searchOption"><xsl:value-of select="key('security','searchOptions')/value" /></xsl:param>
<xsl:param name="searchKey"><xsl:value-of select="key('security','searchKey')/value" /></xsl:param>
<xsl:param name="ERROR"><xsl:value-of select="key('security','ERROR')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Password Reset</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
    <script type="text/javascript" language="javascript" src="/{$applicationcontext}/security/js/ajax.js"></script>

	<script language="javascript">

	var context_js = '<xsl:value-of select="$applicationcontext"/>';
        var site_name = '<xsl:value-of select="$sitename"/>';
	<![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";

    function init() {
    	setNavigationHandlers();
    }
  	function logoff() {
		document.forms[0].adminAction.value = "logoff";
		document.forms[0].securitytoken.value = "";
		submitFormSsl("Login.do");
  	}

    function goExitMenu()
    {
        document.forms[0].adminAction.value = "mainMenu";
        document.forms[0].isExit.value="Y";
        submitForm("Main.do");
    }

  	function goMain() {
  		document.forms[0].adminAction.value = "mainMenu";
      // Revert to unsecured HTTP.
		submitForm("Main.do");
  	}

	function goSystemAdministration() {
		document.forms[0].adminAction.value = "systemAdministration";
		// Revert to unsecured HTTP.
    submitForm("Main.do");
	}

	function submitForm(servlet) {
		document.forms[0].action = "http://" + site_name + servlet_prefix + servlet;
		document.forms[0].submit();
	}
  
  function submitFormSsl(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}
        function goLink(servlet, admin_action) {
             document.forms[0].adminAction.value = admin_action;
             submitFormSsl(servlet);
        }
		
	function go_ajax_search_user() {
		ajax_search_user();
		return false;
  	}
	]]>
	</script>

  </head>
  <body onload="javascript:init();">

    <form name="form" onSubmit="return go_ajax_search_user();">
	<input type="hidden" name="sitename" value="{$sitename}"/>
	<input type="hidden" name="sitenamessl" value="{$sitenamessl}"/>
    <input type="hidden" name="adminAction" value="resetUserPassword"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="contextJs" value="{applicationcontext}"/>
    <input type="hidden" name="isExit" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'User Search'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="4" onclick="javascript: goExitMenu();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs" align="center">User Search</td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Option:<font color="red">&nbsp;***</font></td>
						<td><input type="radio" name="searchOption" value="identityId" checked="checked">UserId</input>
						<input type="radio" name="searchOption" value="lastName">Last Name</input></td>
					</tr>
					<tr>
						<td width="15%" class="label">User ID / Name:<font color="red">&nbsp;***</font></td>
						<td><input type="text" name="searchKey" value="" tabindex="3" size="20" maxlength="20"/>
						</td>
					</tr>									
                </table>
            </td>
        </tr>
    </table>   
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="searchButton" Value="Search" class="BlueButton" tabindex="3" onclick="javascript:ajax_search_user();"/>
        	</td>			
		</tr>
	</table>
	<br/>
	<br/>
	<br/>
	<br/>
	<span id="userSearchResultsHeader" style="display:none">
	    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="60%" align="center" colspan="1" class="header">User Search Results</td>
            <td width="20%" align="left">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><hr/></td>
        </tr>
		</table>
	</span>
	<a name="jumpToPassword"></a>
	<div id="userResultsDiv" style="margin-left: 15px; float:left;">
	</div>
	<div id="userPasswordDiv" style="margin-left: 15px; float:left; width: 600px">
	</div>	
    </form>
    <br/>
	<br/>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>