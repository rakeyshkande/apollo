<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="sitename"><xsl:value-of select="key('security','sitename')/value" /></xsl:param>
<xsl:param name="sitenamessl"><xsl:value-of select="key('security','sitenamessl')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<!-- Security related parameters. -->
<xsl:param name="Merchandising"><xsl:value-of select="key('security','Merchandising')/value" /></xsl:param>
<xsl:param name="ReportLinks"><xsl:value-of select="key('security','ReportLinks')/value" /></xsl:param>
<xsl:param name="CrossSellMaintLink"><xsl:value-of select="key('security','CrossSellMaintLink')/value" /></xsl:param>
<xsl:param name="CarrierZipBlockingLink"><xsl:value-of select="key('security','CarrierZipBlockingLink')/value" /></xsl:param>
<xsl:param name="ReprocessOrdersLink"><xsl:value-of select="key('security','ReprocessOrdersLink')/value" /></xsl:param>
<xsl:param name="PricePayVendorLink"><xsl:value-of select="key('security','PricePayVendorLink')/value" /></xsl:param>
<xsl:param name="PDBMassEdit"><xsl:value-of select="key('security','PDBMassEdit')/value" /></xsl:param>


<xsl:variable name="adminAction" select="'merchandising'"/>
<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Merchandising Menu</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var site_name_ssl = '<xsl:value-of select="$sitenamessl"/>';
    <![CDATA[
    var servlet_prefix = "/" + context_js + "/security/";
    function init() {
      setNavigationHandlers();
    }

    function logoff() {
    document.forms[0].adminAction.value = "logoff";
    submitFormSsl("Login.do");
    }

    function goMain() {
    document.forms[0].adminAction.value = "mainMenu";
    submitForm("Main.do");
    }

  function comingSoon() {
    alert("Coming soon!");
  }


  function submitForm(servlet) {
    document.forms[0].action = servlet_prefix + servlet;
    document.forms[0].submit();
  }

  function submitFormSsl(servlet) {
    document.forms[0].action = "https://" + site_name_ssl + servlet_prefix + servlet;
    document.forms[0].submit();
  }

  ]]>
  </script>
  </head>
  <body onload="javascript: init()">
    <form name="form" method="post">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Merchandising Menu</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>
  <table width="98%" border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td align="right">
      <input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="920" onClick="javascript: logoff();"/>
    </td>
  </tr>
  </table>
<table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
  <td>
  <xsl:variable name="permissionView" select="'View'"/>
  <xsl:variable name="permissionYes" select="'Yes'"/>
  <!-- Check authorizaion -->
  <xsl:if test="$Merchandising = $permissionView">
  <table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
       <!-- Margin -->
      <td width="7%">&nbsp;</td>
      <td valign="top">
        <!-- Column 1 -->
        <table>
           <tr><td class="label">Maintenance</td></tr>
          <!-- *************************************************************************************** -->
          <!-- Add-On Dashboard-->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/AddOnDashboardAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="10">Add-On Dashboard</a>
            </td>
          </tr>
          
          <!-- *************************************************************************************** -->
          <!-- Box Summary -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/BoxAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="10">Box Summary</a>
            </td>
          </tr>
          
          <!--Carrier zip blocking -->
       <xsl:if test="$CarrierZipBlockingLink = $permissionView">
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showDeliveryZipMaintPage.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="20">Carrier Zip Blocking</a>
            </td>
          </tr> 
        </xsl:if >
          
          
          <xsl:if test="$CrossSellMaintLink = $permissionView">
            <tr>
              <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/joe/crossSellMaint.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="30">Cross Sell Maintenance</a>
              </td>
            </tr>
          </xsl:if >
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ComponentSKUMaintAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="40">Component SKU Maintenance</a>
            </td>
          </tr>
          
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/VendorProductSearchAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="50">Dropship Inventory Control</a>
            </td>
          </tr>
          <!-- *************************************************************************************** -->
          <!-- Merchandising Parameters -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ConfigurationAction.do?context={$context}&amp;action_type=load&amp;selected_context=MERCHANDISING&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="60">Merchandising Parameters</a>
            </td>
          </tr>
	          <tr>
	            <td>
	              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ProductMappingMaintenanceAction.do?context={$context}&amp;action_type=load&amp;selected_context=MERCHANDISING&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="60">Phoenix Product Mapping Maintenance</a>
	            </td>
	          </tr>
          <xsl:if test="$PricePayVendorLink = $permissionView">
            <tr>
              <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/PricePayVendorUpdate.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="65">Price/Pay Vendor Update</a>
              </td>
            </tr>
          </xsl:if>
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showProductMaintList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="80">Product Maintenance</a>
            </td>
          </tr>
     <!--  15420 Mass Edit changes        -->  
     <xsl:if test="$PDBMassEdit = $permissionView"> 
       <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showProductMaintMassEdit.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="85">Product Maintenance Mass Edit</a>
            </td>
          </tr>   
     </xsl:if> 
    
     <xsl:if test="$ReprocessOrdersLink = $permissionView">
          <!-- reporcess order -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/VenusOrderReprocessAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="90">Reprocess Orders</a>
            </td>
          </tr>   
      </xsl:if >
          
         <!-- Send Product Updates -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showUpdateNovatorProducts.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="100">Send Product Updates</a>
            </td>
          </tr>
          
          <!-- Send vendor updates -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/NovatorVendorUpdateAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="110">Send Vendor Updates</a>
            </td>
          </tr>
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showVendorProductOverride.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="115">Ship Method Override</a>
            </td>
          </tr>
          
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/pdb/showUpsellMaintList.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="120">Upsell</a>
            </td>
          </tr>
          

          
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/VendorListAction.do?context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="130">Vendor Maintenance</a>
            </td>
          </tr>
          

        </table>
      </td>
      <td valign="top">
        <!-- Column 2 -->
        <table border="0">
          <tr><td class="label">Queues</td></tr>
          <tr>
              <td class="TextLink">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://{$sitename}/loadmemberdata/servlet/ViewQueueServlet?action=loadQueue&amp;context={$context}&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="200">View Queue</a>
              </td>
          </tr>
          <xsl:if test="$ReportLinks = $permissionYes">
              <tr><td class="label">&nbsp;</td></tr>
              <tr><td class="label">Reports</td></tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/GetUserExecutableReportList.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=merchandising&amp;securitytoken={$securitytoken}" tabindex="300">Report Submission</a></td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/accountingreporting/GetReportViewerForm.do?context={$context}&amp;adminAction={$adminAction}&amp;report_main_menu=merchandising&amp;securitytoken={$securitytoken}" tabindex="310">Report Viewer</a></td>
              </tr>
          </xsl:if>
           <tr><td class="label">&nbsp;</td></tr>
<!-- *************************************************************************************** -->
<!-- Zone Jump Section -->
<!-- *************************************************************************************** -->
          <tr><td class="label">Zone Jump</td></tr>
          <!-- *************************************************************************************** -->
          <!-- Zone Jump Dashboard -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ZJTripAction.do?context={$context}&amp;action_type=dashboard&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="400">Dashboard</a>
            </td>
          </tr>
          <!-- *************************************************************************************** -->
          <!-- Zone Jump Injection Hub -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ZJInjectionHubAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="410">Injection Hub Summary</a>
            </td>
          </tr>
          <!-- *************************************************************************************** -->
          <!-- Zone Jump Trip Summary -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/ZJTripAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="420">Trip Summary</a>
            </td>
          </tr>
<!-- *************************************************************************************** -->
<!-- Inventory Tracking -->
<!-- *************************************************************************************** -->
          <tr><td class="label">&nbsp;</td></tr>
          <tr><td class="label">Inventory Tracking</td></tr>
          <!-- *************************************************************************************** -->
          <!-- Inventory Tracking Dashboard -->
          <!-- *************************************************************************************** -->
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/InventoryTrackingDashboardAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="400">Inventory Tracking Dashboard</a>
            </td>
          </tr>
          <tr>
            <td>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="link" href="http://{$sitename}/orderprocessingmaint/InventoryTrackingEmailAlertsAction.do?context={$context}&amp;action_type=load&amp;adminAction={$adminAction}&amp;securitytoken={$securitytoken}" tabindex="400">Inventory Tracking Email Alerts</a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </xsl:if>
  </td>
  </tr>
</table>

<table width="98%" border="0" cellpadding="1" cellspacing="1">
<tr>
    <td width="55%" align="right">
        <input type="button" class="BlueButton" name="MainMenu" Value="Main Menu" tabindex="900" onclick="javascript: goMain();"/>
    </td>
    <td width="43%" align="right">
        <input type="button" class="BlueButton" name="Logoff" Value="Logoff" tabindex="910" onClick="javascript: logoff();"/>
    </td>
</tr>
</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
