<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/USERS/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">

  <html>
 
  <head>

    <title>FTD - Identity Management</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"/>
    <script type="text/javascript" language="JavaScript">

  
	var identityCount = <xsl:value-of select="count(USERS/USER/IDENTITY)"/>;
	
	var fieldNames = new Array("newIdentityId", "newCredentials", "newRecredentials", "newDescription", "newCredentialsExpDate", "newIdentityExpDate");
	var requiredFieldNames = new Array("newIdentityId", "newCredentials", "newRecredentials", "newCredentialsExpDate", "newIdentityExpDate");
	var identityFields = new Array("identityId", "description", "credentials", "reCredentials", "identityExpDate", "credentialsExpDate", "expIdentityButton", "expCredentialsButton");
	var addImages = new Array("identityCalendarNew", "credentialsCalendarNew");
	var identityImages = new Array("identityCalendar", "credentialsCalendar");
	var confirmMsg = "Are you sure you want to remove the selected identity/identities?";
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[

	function processIdentityClick(div_number){
	    var checkbox = document.all("checkboxTD" + div_number);
	    var link = document.all("linkTD" + div_number);
	    var style = getStyleObject("identity" + div_number);
	    var currentDisplay = style.display;
	   
	   if (currentDisplay == "none"){
		style.display = "block";
		checkbox.className = "SelectedIdentity";
		link.className = "SelectedIdentity";
	    } else if (currentDisplay == "block"){
		style.display = "none";
		checkbox.className = "UnselectedIdentity";
		link.className = "UnselectedIdentity";
	    }
	    
	}
	function updateControl(checked, identityNumber){
	    var checkboxText = document.all("checkboxText" + identityNumber);
	    if (checked){   // Lock fields
		checkboxText.innerHTML = "Locked";
	    } else {        // Unlock fields
		checkboxText.innerHTML = "Unlocked";
	    }
	    for (var i = 0; i < identityFields.length; i++){
	    	// Do not allow changing identity id
	    	if (identityFields[i] != "identityId" ) {
				document.all(identityFields[i] + identityNumber).disabled = checked;
			}
	    }
	    setUpdateButton();
	}
	function setUpdateButton(){
	    for (var i = 1; i <= identityCount; i++){
			if (!document.all("updateCheckbox" + i).checked){
		    	document.all("updateButton").disabled = false;
		    	return;
			}
	    }
	    document.all("updateButton").disabled = true;
	}
	function setRemoveButton(){
	
	    for (var i = 1; i <= identityCount; i++){
			if (document.all("removeCheckbox" + i).checked){
		    	document.all("removeButton").disabled = false;
		    	return;
			}
	    }
	    document.all("removeButton").disabled = true;
	}
	function expireNow(fieldName, checkbox, pos){
	    var date = new Date();
	    var dateText = (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear();
	    if (!document.all(checkbox).checked) {
			document.all(fieldName).value = dateText;
		}
		
		document.all("update_id").value = pos;
		submitForm("Identity.do", "update");	
	}

	function submitForm(servlet, action){
		document.forms[0].action = "/" + context_js + "/security/" + servlet;
		document.forms[0].adminAction.value = action;
		document.forms[0].submit();
    }
    
    function callAddIdentity() {
    	//if (checkRequiredFields(requiredFieldNames)
    	//&& checkCredentials("newCredentials", "newRecredentials")) {
		//	if((ValidateDate("newIdentityExpDate"))
		//	&& (ValidateDate("newCredentialsExpDate"))) {
		//		submitForm("Identity","add");
		//	}
		//}

			if(ValidateDate("newIdentityExpDate")
			&& ValidateDate("newCredentialsExpDate")
			&& checkCredentials("newCredentials", "newRecredentials")
			) {
				submitForm("Identity.do","add");
			}

    }

	// use update_id field to hold ids that need to be updated.
	function callUpdateIdentity() {
		// validate date first to guard against checkbox being
		// changed inside the for loop to form the id string.
		for (var i = 1; i <= identityCount; i++){
			var prev_id = document.all("update_id").value;
			if (!document.all("updateCheckbox" + i).checked){
				if(document.all("recredentials" + i).value != ""
				&& !checkCredentials("credentials" + i, "recredentials" + i)) {
					return;
				}
				if((!ValidateDate("identityExpDate" + i))
					|| (!ValidateDate("credentialsExpDate" + i))) {
					return;
				}
			}
		}
		// check which ids are to be updated.
		var prev_id = "";
		for (var i = 1; i <= identityCount; i++){
			if (!document.all("updateCheckbox" + i).checked){
				prev_id = prev_id + "," + i;
			}
		}
		document.all("update_id").value = prev_id;
		submitForm("Identity.do", "update");
	}
	
	function callRemoveIdentity() {
		if (confirm(confirmMsg)) {
			var prev_id = "";
 			for (var i = 1; i <= identityCount; i++){
				if (document.all("removeCheckbox" + i).checked){
					prev_id = prev_id + "," + i;
				}
			}
			document.all("remove_id").value = prev_id;
			submitForm("Identity.do", "remove");
		}
	}
	
	function callManageRole(id) {
		document.forms[0].identity_id.value = id;
		submitForm("Identity.do", "mapRole");
	}
	function checkRequiredFields(fields)
	{
    	for (var i = 0; i < fields.length; i++){
			if(document.all(fields[i]).value == "") {
				alert(document.all(fields[i]).name + " is required");
				return false;
			}
   		}
		return true;
	}
		
	function checkCredentials(cre, recre) {
		if (document.all(cre).value != document.all(recre).value) {
			alert("Password and re-entered password do not match.");
			return false;
		}
		return true;
	}

	function goMain() {
		document.forms[0].action = "/" + context_js + "/security/Main";
		document.forms[0].adminAction.value = "systemAdministration";
		document.forms[0].submit();
	}
    ]]>

       </script>  
  </head>
 
	
  <body>

    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <input type="hidden" name="user_id" value="{USERS/USER/user_id}"/>
     <input type="hidden" name="first_name" value="{USERS/USER/first_name}"/>
     <input type="hidden" name="last_name" value="{USERS/USER/last_name}"/>
	 <input type="hidden" name="identity_id" value=""/>
     <input type="hidden" name="update_id" value=""/>
     <input type="hidden" name="remove_id" value=""/>

     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Manage Identity'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Main Menu</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('SecurityUser.do','view_all')">Users</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('SecurityUser.do','view')">User</a>&nbsp; >
                Identity
            </td>
        </tr>
        <tr>
            <td>   

	<!-- User Information -->
	
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Name:</td>
                        <td width="35%" class="label"><xsl:value-of select="USERS/USER/first_name" disable-output-escaping="yes"/>
                        		&nbsp;<xsl:value-of select="USERS/USER/last_name" disable-output-escaping="yes"/></td>
                    	<td width="15%" class="label" colspan="2" align="left">Email: <xsl:value-of select="USERS/USER/email" disable-output-escaping="yes"/></td>
                        
                    </tr>
                </table>
	<!-- Add Identities -->
	
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">Add Identities</td>
                    </tr>
                    <tr>
                    	<td width="15%" class="label">Identity:</td>
                        <td width="35%"><input type="text" name="newIdentityId" tabindex="1" size="30" maxlength="255"/></td>
                        <td width="15%" class="label">Password:</td>
                        <td width="35%"><input type="password" name="newCredentials" tabindex="4" size="30" maxlength="4000"/></td>
                    </tr>
                    <tr>
                    	<td width="15%" class="label">Description:</td>
                        <td width="35%"><input type="text" name="newDescription" tabindex="2" size="30" maxlength="4000"/></td>
                        <td width="15%" class="label">Re-enter Password:</td>
                        <td width="35%"><input type="password" name="newReCredentials" tabindex="5" size="30"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Identity<br/>Expire Date (mm/dd/yyyy):</td>
                        <td width="35%">
                            <input type="text" name="newIdentityExpDate" tabindex="3" size="10"/>
                            <!--img id="identityCalendarNew" src="../security/images/calendar.gif" width="18" height="18" align="absmiddle"/-->
                        </td>
                        <td width="15%" class="label">Password Expire Date (mm/dd/yyyy):</td>
                        <td width="35%">
                            <input type="text" name="newCredentialsExpDate" tabindex="6" size="10"/>
                            <!--img id="credentialsCalendarNew" src="../security/images/calendar.gif" width="18" height="18" align="absmiddle"/-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                    		<input type="button" name="addIdentity" class="BlueButton" tabindex="7" value="Add Identity" onclick="javascript:callAddIdentity()"/>
                    	</td>
                    </tr>
                </table>

	<!-- Identity Information -->
	
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Current Identities</td>
                    </tr>
                    
                    <xsl:for-each select="USERS/USER/IDENTITY">
                    
                    <tr>
                        <td colspan="3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                            	<xsl:variable name="pos" select="position()"/>
                        		<xsl:variable name="cbName" select="concat('checkboxTD',position())"/>
                                <xsl:variable name="linkId" select="concat('linkTD',position())"/>
                                <xsl:variable name="rmName" select="concat('removeCheckbox',position())"/>
                                <xsl:variable name="divId" select="concat('identity',position())"/>
                                <xsl:variable name="identityId" select="concat('identityId',position())"/>
								<input type="hidden" name="{$identityId}" value="{identity_id}"/>
                                <xsl:variable name="credentials" select="concat('credentials',position())"/>
                                <xsl:variable name="description" select="concat('description',position())"/>
                                <xsl:variable name="recredentials" select="concat('recredentials',position())"/>
                                <xsl:variable name="checkboxText" select="concat('checkboxText', position())"/>
								<xsl:variable name="updateCheckbox" select="concat('updateCheckbox', position())"/>
								<xsl:variable name="identityExpDate" select="concat('identityExpDate',position())"/>
								<xsl:variable name="expIdentityButton" select="concat('expIdentityButton',position())"/>
								<xsl:variable name="identityCalendar" select="concat('identityCalendar',position())"/>
								<xsl:variable name="credentialsExpDate" select="concat('credentialsExpDate',position())"/>
								<xsl:variable name="expCredentialsButton" select="concat('expCredentialsButton',position())"/>
								<xsl:variable name="credentialsCalendar" select="concat('credentialsCalendar',position())"/>
								<xsl:variable name="iExpYear" select="substring(identity_expire_date,1,4)"/>
								<xsl:variable name="iExpMonth" select="substring(identity_expire_date,6,2)"/>
								<xsl:variable name="iExpDay" select="substring(identity_expire_date,9,2)"/>
								<xsl:variable name="cExpYear" select="substring(credentials_expire_date,1,4)"/>
								<xsl:variable name="cExpMonth" select="substring(credentials_expire_date,6,2)"/>
								<xsl:variable name="cExpDay" select="substring(credentials_expire_date,9,2)"/>
        						<tr>
                                    <td id="{$cbName}" width="5%" align="center">
                                        <input type="checkbox" name="{$rmName}" onclick="javascript:setRemoveButton();"/>
                                    </td>
                                    <td id="{$linkId}" width="15%">
                                        <a class="linkBlue" href="javascript:processIdentityClick({$pos});">
                                        	<xsl:value-of select="identity_id" disable-output-escaping="yes"/></a>
                                    </td>
                                    <td></td>
                                </tr>
              
                                <tr>
                                    <td colspan="3">
                                        <div id="{$divId}" style="display:none;">
                                            <table width="100%" bgcolor="#B0C4DE">
                                                <tr>
                                                    <td width="15%" class="label">Identity:</td>
                                                    <td width="35%"><input type="text" name="{$identityId}" size="30" maxlength="255" disabled="true" value="{identity_id}"/></td>
                                                    <td width="15%" class="label">New Password:</td>
                                                    <td width="35%"><input type="password" name="{$credentials}" size="30" maxlength="4000" disabled="true"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="15%" class="label">Description:</td>
                                                    <td width="35%"><input type="text" name="{$description}" size="30" maxlength="4000" disabled="true" value="{description}"/></td>
                                                    <td width="15%" class="label">Re-enter New Password:</td>
                                                    <td width="35%"><input type="password" name="{$recredentials}" size="30" maxlength="4000" disabled="true"/></td>
                                                </tr>
                                                <tr>
                                                    <td width="15%" class="label">Identity<br/>Expire Date (mm/dd/yyyy):</td>
                                                    <td width="35%">
                                                        <input type="text" name="{$identityExpDate}" size="10" disabled="true" value="{$iExpMonth}/{$iExpDay}/{$iExpYear}"/>
                              													<input type="button" name="{$expIdentityButton}" class="BlueButton" value="Expire Now" disabled="true" onclick="javascript:expireNow('{$identityExpDate}', '{$updateCheckbox}', '{$pos}');"/>
                                                    </td>
                                                    <td width="15%" class="label">Password Expire Date (mm/dd/yyyy):</td>
                                                    <td width="35%">
                                                        <input type="text" name="{$credentialsExpDate}" size="10" disabled="true" value="{$cExpMonth}/{$cExpDay}/{$cExpYear}"/>
                                                        <input type="button" name="{$expCredentialsButton}" class="BlueButton" value="Expire Now" disabled="true" onclick="javascript:expireNow('{$credentialsExpDate}', '{$updateCheckbox}', '{$pos}');"/>

                                                        <!-- <img id="{$credentialsCalendar}" src="../images/calendar.gif" width="18" height="18" align="absmiddle"/>
                                                        <a href="javascript:expireNow('{$credentialsExpDate}', '{$updateCheckbox}', '{$pos}');" class="linkblue">Expire Now</a> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" align="right">
                                                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                                                            <tr>
                                                                <td width="85%"><a class="linkBlue" href="javascript:callManageRole('{identity_id}')">Manage Roles</a></td>
                                                                <td width="10%" id="{$checkboxText}" colspan="2" align="right" class="accesslabel">Locked</td>
                                                                <td width="5%" align="center"><input type="checkbox" name="{$updateCheckbox}" checked="true" onclick="javascript:updateControl(this.checked, {$pos});"/></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                      
                            </table>
                        </td>
                    </tr>
           	    	</xsl:for-each>
                    <tr>
                        <td colspan="3" align="center">
                            <input type="button" name="updateButton" class="BlueButton" value="Update Identity" disabled="true" onclick="javascript:callUpdateIdentity()"/>
                            <input type="button" name="removeButton" class="BlueButton" value="Remove Identity" disabled="true" onclick="javascript:callRemoveIdentity()"/>
                        </td>
                    </tr>
                    
                </table>

            </td>
        </tr>
    </table>  

   
    </form>   

    <xsl:call-template name="footer"/>

   </body>   
  </html>
</xsl:template>

</xsl:stylesheet>