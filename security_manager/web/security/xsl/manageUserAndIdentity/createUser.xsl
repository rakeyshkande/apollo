<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:variable name="selectedLocked" select="key('field', 'locked')/@value"/>
<xsl:variable name="selectedCallCenter" select="key('field', 'callCenter')/@value"/>
<xsl:variable name="selectedDepartment" select="key('field', 'department')/@value"/>
<xsl:variable name="selectedCsGroup" select="key('field', 'csGroup')/@value"/>
<xsl:variable name="selectedContext" select="key('field', 'contextId')/@value"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - User Creation</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/calendar.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/calendar.js"/>
    <script language="javascript">
   	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[

	function init(){
		setNavigationHandlers();
		setErrorFields();
        setExpireDate();
	}
	function goSave() {
		submitForm("SecurityUser.do", "add");
	}

	function checkRequiredFields(fields)
	{
    	for (var i = 0; i < fields.length; i++){
			if(document.all(fields[i]).value == "") {
				alert(document.all(fields[i]).name + " is required");
				return false;
			}
   		}
		return true;
	}

	function callSearch() {
		submitForm("SecurityUser.do", "display_search");
	}
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
	// Set pwdExpireDate to 45 days from today.
	function setExpireDate() {
		var date = new Date();
		//var offset = 45 * 24 * 3600 * 1000;
		var offset = 0;
		var expDay = new Date(date.getTime() + offset);
		var dateText = (expDay.getMonth()+1) + "/" + expDay.getDate() + "/" + expDay.getFullYear();
		document.forms[0].pwdExpireDate.value = dateText;
	}

	function callChangeContext() {
		//document.forms[0].context_id.value = document.forms[0].contextId.value;
		submitForm("SecurityUser.do", "refresh_display_add");
	}
	
   ]]>
   </script>

  </head>
  <body onload="javascript:init();">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Create User'"/>
      	<xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="16" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callSearch()">Users</a>&nbsp;>
                Create User
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>

                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">First Name:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="firstName"  value="{key('field', 'firstName')/@value}" tabindex="1" size="20" maxlength="20"/></td>
                        <td width="15%" class="label">User ID:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="identityId"  value="{key('field', 'identityId')/@value}" tabindex="11" size="20" maxlength="20"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'firstName')/@description"/></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'identityId')/@description"/></td>
	                </tr>
                    <tr>
                        <td width="15%" class="label">Last Name:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="lastName" value="{key('field', 'lastName')/@value}" tabindex="2" size="20" maxlength="20"/></td>
                   		<td width="15%" class="label">ID Expiration Date:<font color="red">&nbsp;***</font> (mm/dd/yyyy)</td>
                        <td width="35%">
                            <input type="text" name="expireDate" id="exp_date_id" value="{key('field', 'expireDate')/@value}" tabindex="12" size="10" maxlength="10"/>
                            <!--img id="exp_date_img" name="expireDateCalendar" src="/{$applicationcontext}/security/images/calendar.gif" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='red';" onmouseout="this.style.background=''" /-->
      						<img id="exp_date_img" name="expireDateCalendar" src="/{$applicationcontext}/security/images/calendar.gif"/>
                        </td>
					
					         
					
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'lastName')/@description"/></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'expireDate')/@description"/></td>
	                </tr>
					<tr>
                        <td width="15%" class="label">Phone:</td>
                        <td width="35%">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="label">
                                    <input type="text" name="phone" value="{key('field', 'phone')/@value}" tabindex="3" size="10" maxlength="10"/>
                                    &nbsp;&nbsp;Ext.&nbsp;&nbsp;<input type="text" name="phoneExt"  value="{key('field', 'phoneExt')/@value}" tabindex="4" size="5" maxlength="4"/>
									</td>
                                </tr>
                            </table>
                        </td>
                        <td width="15%" class="label">Password Expire Date: </td>
          				<td width="35%">
                            <input type="text" name="pwdExpireDate" disabled="true" size="10" maxlength="10"/>
                        </td>
                    </tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'phone')/@description"/>
									           &nbsp;&nbsp;<xsl:value-of select="key('error', 'phoneExt')/@description"/>
	                  </td>
	                </tr>
                    <tr>
                    	<td width="15%" class="label">Email:</td>
                    	<td width="35%"><input type="text" name="emailAddress" value="{key('field', 'emailAddress')/@value}" tabindex="5" size="20" maxlength="20"/></td>
                    	<td width="15%" class="label">Locked:<font color="red">&nbsp;***</font></td>
                    	<td width="35%">
			  				<xsl:choose>
								<xsl:when test="'Y'=$selectedLocked">
                    				<input type="radio" name="locked" tabindex="13" value="Y" checked="true"/>Yes&nbsp;&nbsp;
                    				<input type="radio" name="locked" tabindex="14" value="N"/>No
								</xsl:when>
						    	<xsl:otherwise>
						    		<input type="radio" name="locked" tabindex="13" value="Y"/>Yes&nbsp;&nbsp;
						    		<input type="radio" name="locked" tabindex="14" value="N" checked="true"/>No
						    	</xsl:otherwise>
			    			</xsl:choose>
                    	</td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'emailAddress')/@description"/></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'locked')/@description"/></td>
	                </tr>
					
								
					<tr>
						<td width="15%" class="label">Call Center:</td>
						<td width="35%">
							<select name="callCenter" tabindex="6">
								<option value="">- Select One -</option>
								<xsl:for-each select="CALLCENTERS/CALLCENTER">
								<xsl:choose>
									<xsl:when test="call_center_id=$selectedCallCenter">
									<option value="{call_center_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
									<option value="{call_center_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
						<td width="15%" class="label">Role Context:</td>
						<td width="35%">
							<select name="contextId" tabindex="15" onchange="javascript:callChangeContext()">
								<option value="">- Select One -</option>
								<xsl:for-each select="CONTEXTS/CONTEXT">
								<xsl:choose>
									<xsl:when test="context_id=$selectedContext">
										<option value="{context_id}" selected="true"><xsl:value-of select="context_id"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{context_id}"><xsl:value-of select="context_id"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
      				</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'callCenter')/@description"/></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'context')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Department:<font color="red">&nbsp;***</font></td>
						<td width="35%">
							<select name="department" tabindex="7">
								<option value="">- Select One -</option>
								<xsl:for-each select="DEPARTMENTS/DEPARTMENT">
								<xsl:choose>
									<xsl:when test="department_id=$selectedDepartment">
										<option value="{department_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{department_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
						<!-- Do not show this section if role context is not selected -->
						<xsl:if test="$selectedContext != ''">
						<td width="15%" class="label" rowspan="2" valign="top">Select Roles:</td>
						<td width="35%" rowspan="6">
							<select name="roles" multiple="true" size="6" tabindex="16">
								<xsl:for-each select="ROLES/ROLE">
								<xsl:choose>
									<xsl:when test="@selected='Y'">
										<option value="{role_id}" selected="true"><xsl:value-of select="role_name"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{role_id}"><xsl:value-of select="role_name"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
						</xsl:if>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'department')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">CS Group:</td>
						<td width="35%">
							<select name="csGroup" tabindex="8">
								<option value="">- Select One -</option>
								<xsl:for-each select="CSGROUPS/CSGROUP">
								<xsl:choose>
									<xsl:when test="cs_group_id=$selectedCsGroup">
										<option value="{cs_group_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{cs_group_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
					</tr>
					<tr>
                    	<td width="15%" class="label">Employee ID:</td>
                    	<td width="35%"><input type="text" name="employee_id" value="{key('field', 'employee_id')/@value}" tabindex="9" size="20" maxlength="20"/></td>
					</tr>
					<tr>
                    	<td width="15%" class="label">Supervisor:</td>
                    	<td width="35%"><input type="text" name="supervisor_id" value="{key('field', 'supervisor_id')/@value}" tabindex="9" size="20" maxlength="20"/></td>
                    </tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'supervisor_id')/@description"/></td>
	                </tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="createUser" value="Save" class="BlueButton" tabindex="15" onclick="javascript:goSave()"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="16" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
	<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "exp_date_id",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "exp_date_img"  // ID of the button
    }
  );
 
	</script>
</xsl:template>
</xsl:stylesheet>