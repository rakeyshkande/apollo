<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/USERS/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - User Search Results</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
	function init () {
		setNavigationHandlers();
	}
	function submitForm(servlet, act) {
		document.forms[0].action = "/" + context_js + "/security/" + servlet;
		document.forms[0].adminAction.value=act;
		document.forms[0].submit();
	}
	function viewId(userid, id) {
		document.forms[0].user_id.value=userid;
		document.forms[0].identityId.value=id;
		submitForm("Identity.do", "view");
	}
	function viewUser(userid) {
		document.forms[0].user_id.value=userid;
		submitForm("SecurityUser.do", "view");
	}
	/*
	function callViewAll() {
		submitForm("SecurityUser.do", "view_all");
	}*/
	function callSearch() {
		submitForm("SecurityUser.do", "display_search");
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
   ]]>
   </script>

  </head>
	<body onload="javascript:init();">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
    <!--input type="hidden" name="user_id" value="{USERS/USER/user_id}"/-->
    <input type="hidden" name="user_id" value=""/>
	<input type="hidden" name="identityId" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'User Search Results'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callSearch()">Users</a>&nbsp;>
                Update User
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
                    <tr>
                        <td class="colheader">User ID</td>
                        <td class="colheader">User Name</td>
                        <td class="colheader">Locked</td>
                        <td class="colheader">ID Expire Date</td>
                        <td class="colheader">Password Expire Date</td>
                        <td class="colheader">Role</td>
                        <td class="colheader">Call Center</td>
                    </tr>
                    <xsl:for-each select="USERS/USER">
				     <xsl:variable name="idExpDate" select="normalize-space(identity_expire_date)"/>
				     <xsl:variable name="idExpDateYear" select="substring($idExpDate, 1, 4)"/>
				     <xsl:variable name="idExpDateMonth" select="substring($idExpDate, 6, 2)"/>
				     <xsl:variable name="idExpDateDay" select="substring($idExpDate, 9, 2)"/>
				     <xsl:variable name="pwdExpDate" select="normalize-space(credentials_expire_date)"/>
				     <xsl:variable name="pwdExpDateYear" select="substring($pwdExpDate, 1, 4)"/>
				     <xsl:variable name="pwdExpDateMonth" select="substring($pwdExpDate, 6, 2)"/>
				     <xsl:variable name="pwdExpDateDay" select="substring($pwdExpDate, 9, 2)"/>
				     <xsl:variable name="id" select="identity_id"/>
				     <xsl:variable name="userid" select="user_id"/>
                    <tr>
                        <td><a href="javascript: viewId('{$userid}','{$id}')"><xsl:value-of select="identity_id"/></a></td>
                        <td><a href="javascript: viewUser('{$userid}')"><xsl:value-of select="first_name" />&nbsp;<xsl:value-of select="last_name" /></a></td>
                        <td><xsl:value-of select="locked_flag" /></td>
                        <td><xsl:value-of select="concat($idExpDateMonth, '/', $idExpDateDay, '/', $idExpDateYear)"/></td>
                        <td><xsl:value-of select="concat($pwdExpDateMonth, '/', $pwdExpDateDay, '/', $pwdExpDateYear)" /></td>
                        <td><xsl:for-each select="ROLES/ROLE">
                        	<xsl:value-of select="role_name"/>
                        		<xsl:if test="position()!=last()">,</xsl:if>
                        	</xsl:for-each>
                        </td>
                        <td><xsl:value-of select="call_center_desc" /></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>
	<table width="98%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="center">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>