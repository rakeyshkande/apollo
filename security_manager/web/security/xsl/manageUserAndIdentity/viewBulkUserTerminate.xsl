<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Bulk User Terminate</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
	function init(){
		setNavigationHandlers();
	}
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
	function loadFile() {
		submitForm("FileUpload.do", "viewBulkUserTerminate");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript:init();">
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Bulk User Terminate'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     
    <form name="bulkUserTerminateForm" method="post" action="/{$applicationcontext}/security/FileUpload" enctype="multipart/form-data">
    <xsl:call-template name="securityanddata"/>
    <input type="hidden" name="adminAction" value="viewBulkUserTerminate"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="subject" value="user"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="identity" value="{$identity}"/>
     <input type="hidden" name="actionType" value="terminate"/>
    
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
            	<a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Bulk User Terminate
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#006699">
				<tr>
					<td class="ErrorMessage" align="center"><xsl:value-of select="ROOT/@message"/></td>
				</tr>
          		<tr>
            		<td>
						<b>Instructions:</b>
						<ol>
						<li>Update the Security spreadsheet with userIds(Mandatory) and Identity Expire Date(Optional, expiry is treated as sysdate when no data provided).</li>
						<li>Browse and select valid spreadsheet (First occurance of an Empty row is treated as EOF).</li>
						<li>Press Terminate to expire the user identity to the specified date.</li>
						<li>If there are errors in the spreadsheet, correct them and repeat steps 2 and 3.</li>
						<li>If done, press Exit to return to the menu.</li>
						</ol>
      				</td>
          		</tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;<a href="/{$applicationcontext}/security/html/bulk_user_terminate.xls">View Example File</a>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
          		<tr>
            		<td>
						File:<font color="red">&nbsp;***&nbsp;</font><input type="file" name="userExcel"/>
      				</td>
          		</tr>
            </table>
            <xsl:if test="count(/ROOT/FIELDS)>0">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="colheader">The list of User Id's processed successfully in Termination process are: </td>
                  </tr>
					<xsl:for-each select="/ROOT/FIELDS">
                    <tr>
                        <td><xsl:value-of select="FIELD[@fieldname='identity_id']/@value"/></td>                                           </tr>                   
                    </xsl:for-each>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </xsl:if>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="load" class="BlueButton" value="Terminate" onclick="javascript:loadFile()"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>