<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:variable name="selectedCallCenter" select="key('field', 'callCenter')/@value"/>
<xsl:variable name="selectedDepartment" select="key('field', 'department')/@value"/>
<xsl:variable name="selectedCsGroup" select="key('field', 'csGroup')/@value"/>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="credentials"><xsl:value-of select="key('security','credentials')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - User Update</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"></script>
    <script language="javascript">
   	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    var password = '<xsl:value-of select="$credentials"/>';
    <![CDATA[

	function init(){
		setNavigationHandlers();
		setErrorFields();
        //showNewPassword();
	}
    function showNewPassword() {
        if (password != '') {
            alert("New Password is " + password);
        }
    }
	function goSave() {
		submitForm("SecurityUser.do", "update");
	
	}
	function viewId(id) {
		document.forms[0].identityId.value=id;
		submitForm("Identity.do", "view");
	}
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	
	function addIdentity(){
		submitForm("Identity.do", "display_add");
	}

	function callSearch() {
		submitForm("SecurityUser.do", "display_search");
	}

	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}

   ]]>
   </script>

  </head>
  <body onload="javascript:init();">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="user_id" value="{USERS/USER/user_id}"/>
    <input type="hidden" name="identityId" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Update User'"/>
      	<xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="10" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callSearch()">Users</a>&nbsp;>
                Update User
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>

                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">First Name:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="firstName"  value="{key('field', 'firstName')/@value}" tabindex="1" size="20" maxlength="20"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'firstName')/@description"/></td>
	                </tr>
                    <tr>
                        <td width="15%" class="label">Last Name:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="lastName" value="{key('field', 'lastName')/@value}" tabindex="2" size="20" maxlength="20"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'lastName')/@description"/></td>
					</tr>
					<tr>
                        <td width="15%" class="label">Phone:</td>
                        <td width="35%">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="label">
                                    <input type="text" name="phone" value="{key('field', 'phone')/@value}" tabindex="3" size="10" maxlength="10"/>
                                    &nbsp;&nbsp;Ext.&nbsp;&nbsp;<input type="text" name="phoneExt"  value="{key('field', 'phoneExt')/@value}" tabindex="4" size="5" maxlength="4"/>
									</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'phone')/@description"/>
									           &nbsp;&nbsp;<xsl:value-of select="key('error', 'phoneExt')/@description"/>
	                  </td>
	                </tr>
                    <tr>
                    	<td width="15%" class="label">Email:</td>
                    	<td width="35%"><input type="text" name="emailAddress" value="{key('field', 'emailAddress')/@value}" tabindex="5" size="20" maxlength="20"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'emailAddress')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Call Center:</td>
						<td width="35%">
							<select name="callCenter" tabindex="6">
								<option value="">- Select One -</option>
								<xsl:for-each select="CALLCENTERS/CALLCENTER">
								<xsl:choose>
									<xsl:when test="call_center_id=$selectedCallCenter">
									<option value="{call_center_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
									<option value="{call_center_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
      				</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'callCenter')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Department:<font color="red">&nbsp;***</font></td>
						<td width="35%">
							<select name="department" tabindex="7">
								<option value="">- Select One -</option>
								<xsl:for-each select="DEPARTMENTS/DEPARTMENT">
								<xsl:choose>
									<xsl:when test="department_id=$selectedDepartment">
										<option value="{department_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{department_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'department')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">CS Group:</td>
						<td width="35%">
							<select name="csGroup" tabindex="8">
								<option value="">- Select One -</option>
								<xsl:for-each select="CSGROUPS/CSGROUP">
								<xsl:choose>
									<xsl:when test="cs_group_id=$selectedCsGroup">
										<option value="{cs_group_id}" selected="true"><xsl:value-of select="description"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{cs_group_id}"><xsl:value-of select="description"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
						<td align="right" class="Link"><a tabindex="11" href="javascript:addIdentity()">Additional User IDs</a></td>
						
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'csGroup')/@description"/></td>
	                </tr>
	                <tr>
                    	<td width="15%" class="label">Employee ID:</td>
                    	<td width="35%"><input type="text" name="employee_id" value="{key('field', 'employee_id')/@value}" tabindex="9" size="20" maxlength="20"/></td>
						<td width="15%" class="label">Supervisor:</td>
                    	<td width="35%"><input type="text" name="supervisor_id" value="{key('field', 'supervisor_id')/@value}" tabindex="9" size="20" maxlength="20"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'employee_id')/@description"/></td>
					  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'supervisor_id')/@description"/></td>
	                </tr>
            <xsl:if test="$credentials != ''">
            <tr>
                    	<td width="15%" class="label">Temporary Password:</td>
                    	<td width="35%"><xsl:value-of select="$credentials"/></td>
					</tr>
          </xsl:if>
                </table>
           
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="colheader">User ID</td>
                        <td class="colheader">User Name</td>
                        <td class="colheader">ID Expire Date</td>
                        <td class="colheader">Password Expire Date</td>
                        <td class="colheader">Role</td>
                    </tr>
                    <xsl:variable name="fname" select="USERS/USER/first_name"/>
                    <xsl:variable name="lname" select="USERS/USER/last_name"/>

                    <xsl:for-each select="USERS/USER/IDENTITY">
					<xsl:variable name="iExpYear" select="substring(identity_expire_date,1,4)"/>
					<xsl:variable name="iExpMonth" select="substring(identity_expire_date,6,2)"/>
					<xsl:variable name="iExpDay" select="substring(identity_expire_date,9,2)"/>
					<xsl:variable name="cExpYear" select="substring(credentials_expire_date,1,4)"/>
					<xsl:variable name="cExpMonth" select="substring(credentials_expire_date,6,2)"/>
					<xsl:variable name="cExpDay" select="substring(credentials_expire_date,9,2)"/>
                    <tr>
                    	<xsl:variable name="id" select="identity_id"/>
                        <td><a tabindex="12" href="javascript: viewId('{$id}')"><xsl:value-of select="$id"/></a></td>
                        <td><xsl:value-of select="$fname"/>&nbsp;
                  			<xsl:value-of select="$lname"/></td>
                        <td><xsl:value-of select="concat($iExpMonth,'/',$iExpDay,'/',$iExpYear)"/></td>
                        <td><xsl:value-of select="concat($cExpMonth,'/',$cExpDay,'/',$cExpYear)"/></td>

                        <td><xsl:for-each select="ROLE">
                       		<xsl:value-of select="role_name"/>
                        	<xsl:if test="position()!=last()">,</xsl:if>
                        </xsl:for-each></td>
                    </tr>
                    </xsl:for-each>
                </table>
                
            </td>
        </tr>
    </table>
    
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="updateUser" class="BlueButton" value="Save" tabindex="11" onclick="javascript:goSave()"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="12" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>