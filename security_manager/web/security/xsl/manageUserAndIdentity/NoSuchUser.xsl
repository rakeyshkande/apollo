<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="closeWindow"><xsl:value-of select="key('security','closeWindow')/value" /></xsl:param>
<xsl:param name="password"><xsl:value-of select="key('security','password')/value" /></xsl:param>

<!--xsl:param name="securitytoken"/-->

<xsl:template match="/">

  <html>

  <head>
       <title>FTD - Manage Account</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
     <script type="text/javascript" language="JavaScript">
	
	function callCancel() {
		top.close();
	}
       </script>
  </head>


  <body>
  <xsl:call-template name="securityanddata"/>
<br></br>
<br></br>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>


                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">

                    <tr align="center">
<br></br>
<br></br>
<br></br>
<br></br>
<br></br>
					<center>
 					<font face="Verdana" color="#FF0000" size="-1">No such username on the system. Please re-enter your username.</font><br></br>
 					</center>
<br></br>
<br></br>
<br></br>
<br></br>

 		
                     </tr>
                </table>

                
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td colspan="3" align="center">
                           <input type="button" name="doneButton" class="BlueButton" value="Cancel" onclick="javascript:callCancel()"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <xsl:call-template name="footer"/>

   </body>
  </html>
</xsl:template>

</xsl:stylesheet>