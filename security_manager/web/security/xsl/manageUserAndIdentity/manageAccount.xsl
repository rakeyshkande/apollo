<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/IDENTITIES/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">

  <html>

  <head>

    <title>FTD - Manage Account</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"/>
    <script type="text/javascript" language="JavaScript">

	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var requiredFieldNames = new Array("oldCredentials", "newCredentials", "newRecredentials");
    var requiredFieldValues = new Array("Old Password", "New Password", "Re-enter New Password");
	var servletPrefix = "/" + context_js + "/security/";
	
	function submitForm(servlet, action){
		document.forms[0].action = servletPrefix + servlet;
		document.forms[0].adminAction.value = action;
		document.forms[0].submit();
    }

	function callChangeCredentials() {
		if (checkRequiredFields(requiredFieldNames)
			&& checkReCredentials(document.forms[0].newCredentials.value, document.forms[0].newRecredentials.value)
			&& checkNewCredentials(document.forms[0].oldCredentials.value, document.forms[0].newCredentials.value) ){
			submitForm("Identity.do", "change_password");
		}
	}

	function checkRequiredFields(fields)
	{
    	for (var i = 0; i < fields.length; i++){
			if(document.all(fields[i]).value == "") {
				alert(requiredFieldValues[i] + " is required");
				return false;
			}
   		}
		return true;
	}
	
	function checkReCredentials(cre, recre) {
		if (cre != recre) {
			alert("Password and re-entered password do not match.");
			return false;
		}
		return true;
	}
	
	function checkNewCredentials(oldCre, newCre) {
		if (oldCre == newCre) {
			alert("New password cannot be the same as old password.");
			return false;
		}
		return true;
	}
	
	function done() {
		document.forms[0].action = "/" + context_js + "/security/Login";
		document.forms[0].submit();
	}
	
	function goMain() {
		document.forms[0].action = "/" + context_js + "/security/Main";
		document.forms[0].submit();
	}
    ]]>

       </script>
  </head>


  <body>

    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <input type="hidden" name="identity" value="{IDENTITIES/IDENTITY/identity_id}"/>
     <input type="hidden" name="user_id" value="{IDENTITIES/IDENTITY/user_id}"/>
     <input type="hidden" name="description" value="{IDENTITIES/IDENTITY/description}"/>
     <xsl:variable name="idExpDate" select="normalize-space(IDENTITIES/IDENTITY/identity_expire_date)"/>
     <xsl:variable name="pwdExpDate" select="normalize-space(IDENTITIES/IDENTITY/credentials_expire_date)"/>
     <xsl:variable name="idExpDateYear" select="substring($idExpDate, 1, 4)"/>
     <xsl:variable name="idExpDateMonth" select="substring($idExpDate, 6, 2)"/>
     <xsl:variable name="idExpDateDay" select="substring($idExpDate, 9, 2)"/>
     <xsl:variable name="pwdExpDateYear" select="substring($pwdExpDate, 1, 4)"/>
     <xsl:variable name="pwdExpDateMonth" select="substring($pwdExpDate, 6, 2)"/>
     <xsl:variable name="pwdExpDateDay" select="substring($pwdExpDate, 9, 2)"/>
     <input type="hidden" name="identityExpDate" value="{concat($idExpDateMonth, '/', $idExpDateDay, '/', $idExpDateYear)}"/>
     <input type="hidden" name="credentialsExpDate" value="{concat($pwdExpDateMonth, '/', $pwdExpDateDay, '/', $pwdExpDateYear)}"/>

     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Manage Account'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink">&nbsp;</a>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                	<tr>
                		<td class="ErrorMessage" colspan="2" align="center"><xsl:value-of select="IDENTITIES/@message"/></td>
                	</tr>
                    <tr>
                    	<td width="15%" class="label">Identity:</td>
                        <td width="35%"><input type="text" name="newIdentityId" value="{IDENTITIES/IDENTITY/identity_id}" disabled="true" tabindex="1" size="30" maxlength="255"/></td>
					</tr>
					<tr>
                        <td width="15%" class="label">Old Password:</td>
                        <td width="35%"><input type="password" name="oldCredentials" size="30" maxlength="4000"/></td>
                    </tr>
					<tr>
                        <td width="15%" class="label">New Password:</td>
                        <td width="35%"><input type="password" name="newCredentials" size="30" maxlength="4000"/></td>
                    </tr>
					<tr>
                        <td width="15%" class="label">Re-enter New Password:</td>
                        <td width="35%"><input type="password" name="newRecredentials" size="30" maxlength="4000"/></td>
                    </tr>
                </table>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td colspan="3" align="center">
                            <input type="button" name="updateButton" class="BlueButton" value="Change Password" onclick="javascript:callChangeCredentials()"/>
                            <input type="button" name="doneButton" class="BlueButton" value="Done" onclick="javascript:done()"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    </form>

    <xsl:call-template name="footer"/>

   </body>
  </html>
</xsl:template>

</xsl:stylesheet>