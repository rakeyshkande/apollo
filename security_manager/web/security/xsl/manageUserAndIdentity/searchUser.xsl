<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:param name="searchOption"><xsl:value-of select="key('security','searchOptions')/value" /></xsl:param>
<xsl:param name="searchKey"><xsl:value-of select="key('security','searchKey')/value" /></xsl:param>
<xsl:param name="ERROR"><xsl:value-of select="key('security','ERROR')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - User Search</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var error_js = '<xsl:value-of select="$ERROR"/>';
    
    <![CDATA[
	
	function init () {
		setNavigationHandlers();
		if(error_js != "") {
			document.forms[0].searchOption.className = "errorField";
		}
	}
	
    function goSearch() {
        var form = document.forms[0];
    	var selected_item = form.searchOption[form.searchOption.selectedIndex].value;
   		submitForm("SecurityUser.do", "search");
    }

	function submitForm(servlet, act) {
		document.forms[0].action = "/" + context_js + "/security/" + servlet;
		document.forms[0].adminAction.value=act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript:init();">

    <form name="form">
    <input type="hidden" name="adminAction" value="search"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'User Search'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="4" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                User Search
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Option:<font color="red">&nbsp;***</font></td>
						<td><select name="searchOption" tabindex="1">
							<option value="">Select One</option>
							<option value="identityId">User ID</option>
							<option value="lastName">Last Name</option>
							</select>
						</td>
					</tr>
					<tr><td width="15%"></td><td class="ErrorMessage"><xsl:value-of select="$ERROR"/></td></tr>
					<tr>
						<td width="15%" class="label">User ID / Name:<font color="red">&nbsp;***</font></td>
						<td><input type="text" name="searchKey" value="{$searchKey}" tabindex="2" size="20" maxlength="20"/>
						</td>
					</tr>
					
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="searchButton" Value="Search" class="BlueButton" tabindex="3" onclick="javascript: goSearch();"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" tabindex="4" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>