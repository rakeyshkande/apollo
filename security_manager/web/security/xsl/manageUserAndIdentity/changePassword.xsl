<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<!--xsl:import href="../securityanddata.xsl"/-->
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/IDENTITIES/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="closeWindow"><xsl:value-of select="key('security','closeWindow')/value" /></xsl:param>
<xsl:param name="password"><xsl:value-of select="key('security','password')/value" /></xsl:param>

<!--xsl:param name="securitytoken"/-->

<xsl:template match="/">

  <html>

  <head>
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
    <title>FTD - Manage Account</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"/>
    <script type="text/javascript" language="JavaScript">

	var context_js = '<xsl:value-of select="$applicationcontext"/>';
	var closeWindow_js = '<xsl:value-of select="$closeWindow"/>';
	var pwd_js = '<xsl:value-of select="$password"/>';
    <![CDATA[
    var requiredFieldNames = new Array("oldCredentials", "newCredentials", "newRecredentials");
    var requiredFieldValues = new Array("Old Password", "New Password", "Retype New Password");
	var servletPrefix = "/" + context_js + "/security/";

    function checkSuccess(){
      if (closeWindow_js == "true"){
        returnToLogin();
      }
      window.name = "CHANGE_PASSWORD";
      document.forms[0].oldCredentials.focus();
    }
    
    function returnToLogin() {
      var form = document.forms[0];
      var ret = new Array();
	  ret[0] = form.context.value;
	  ret[1] = form.identity.value;
	  ret[2] = pwd_js;
      top.returnValue = ret;
      top.close();
    }

	function submitForm(servlet, action){
		document.forms[0].target = window.name;
		document.forms[0].action = servletPrefix + servlet;
		document.forms[0].adminAction.value = action;
		document.forms[0].submit();
    }

	function callChangeCredentials() {
		if (checkRequiredFields(requiredFieldNames)
			&& checkReCredentials(document.forms[0].newCredentials.value, document.forms[0].newRecredentials.value)
			&& checkNewCredentials(document.forms[0].oldCredentials.value, document.forms[0].newCredentials.value) ){
			submitForm("Login.do", "change_password");
		}
	}

	function checkRequiredFields(fields)
	{
    	for (var i = 0; i < fields.length; i++){
			if(document.all(fields[i]).value == "") {
				alert(requiredFieldValues[i] + " is required");
				return false;
			}
   		}
		return true;
	}
	
	function checkReCredentials(cre, recre) {
		if (cre != recre) {
			alert("Password and re-entered password do not match.");
			return false;
		}
		return true;
	}
	
	function checkNewCredentials(oldCre, newCre) {
		if (oldCre == newCre) {
			alert("New password cannot be the same as old password.");
			return false;
		}
		return true;
	}
	
	function callCancel() {
		top.close();
	}

    ]]>

       </script>
  </head>


  <body onload="javascript:checkSuccess()">

    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="identity" value="{IDENTITIES/IDENTITY/identity_id}"/>
     <input type="hidden" name="user_id" value="{IDENTITIES/IDENTITY/user_id}"/>
     <input type="hidden" name="description" value="{IDENTITIES/IDENTITY/description}"/>
     <xsl:variable name="idExpDate" select="normalize-space(IDENTITIES/IDENTITY/identity_expire_date)"/>
     <xsl:variable name="pwdExpDate" select="normalize-space(IDENTITIES/IDENTITY/credentials_expire_date)"/>
     <xsl:variable name="idExpDateYear" select="substring($idExpDate, 1, 4)"/>
     <xsl:variable name="idExpDateMonth" select="substring($idExpDate, 6, 2)"/>
     <xsl:variable name="idExpDateDay" select="substring($idExpDate, 9, 2)"/>
     <xsl:variable name="pwdExpDateYear" select="substring($pwdExpDate, 1, 4)"/>
     <xsl:variable name="pwdExpDateMonth" select="substring($pwdExpDate, 6, 2)"/>
     <xsl:variable name="pwdExpDateDay" select="substring($pwdExpDate, 9, 2)"/>
     <input type="hidden" name="identityExpDate" value="{concat($idExpDateMonth, '/', $idExpDateDay, '/', $idExpDateYear)}"/>
     <input type="hidden" name="credentialsExpDate" value="{concat($pwdExpDateMonth, '/', $pwdExpDateDay, '/', $pwdExpDateYear)}"/>

     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Change Password'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                	  <tr>
                		    <td class="ErrorMessage" colspan="2" align="center"><xsl:value-of select="IDENTITIES/@message"/></td>
                	  </tr>

                    <tr>
                        <td width="15%" class="label">Old Password:</td>
                        <td width="35%"><input type="password" name="oldCredentials" value="{IDENTITIES/IDENTITY/oldCredentials}" size="20" maxlength="20"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">New Password:</td>
                        <td width="35%"><input type="password" name="newCredentials" value="{IDENTITIES/IDENTITY/newCredentials}" size="20" maxlength="20"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Retype New Password:</td>
                        <td width="35%"><input type="password" name="newRecredentials" value="{IDENTITIES/IDENTITY/newRecredentials}" size="20" maxlength="20"/></td>
                    </tr>                    
                </table>

                
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td colspan="3" align="center">
                            <input type="button" name="updateButton" class="BlueButton" value="Submit" onclick="javascript:callChangeCredentials()"/>
                           <input type="button" name="doneButton" class="BlueButton" value="Cancel" onclick="javascript:callCancel()"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    </form>

    <xsl:call-template name="footer"/>

   </body>
  </html>
</xsl:template>

</xsl:stylesheet>