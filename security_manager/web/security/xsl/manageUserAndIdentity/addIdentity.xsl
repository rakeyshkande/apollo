<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:variable name="selectedLocked" select="key('field', 'locked')/@value"/>
<xsl:variable name="selectedContext" select="key('field', 'contextId')/@value"/>

<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - Additional User ID Creation</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/calendar.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/calendar.js"/>
    <script language="javascript">
   	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[
    function init() {
    	setNavigationHandlers();
    	setErrorFields();
		setExpireDate();
    }
    
	function goSave() {
		submitForm("Identity.do", "add");
	}
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function viewId(id) {
		document.forms[0].identityId.value=id;
		submitForm("Identity.do", "view");
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}
	
	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
	// Set pwdExpireDate to 45 days from today.
	function setExpireDate() {
		var date = new Date();
		//var offset = 45 * 24 * 3600 * 1000;
		var offset = 0;
		var expDay = new Date(date.getTime() + offset);
		var dateText = (expDay.getMonth()+1) + "/" + expDay.getDate() + "/" + expDay.getFullYear();
		document.forms[0].pwdExpireDate.value = dateText;
	}
	function callView() {
		submitForm("SecurityUser.do", "view");
	}

	function callSearch() {
		submitForm("SecurityUser.do", "display_search");
	}
	function callChangeContext() {
		submitForm("Identity.do", "refresh_display_add");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="user_id" value="{USERS/USER/user_id}"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Create Additional User ID'"/>
      	<xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="7" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callSearch()">Users</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callView()">User</a>&nbsp;>
                Add User ID
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>

                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Name:</td>
                        <td width="35%" class="label"><xsl:value-of select="USERS/USER/first_name" disable-output-escaping="yes"/>
                        		&nbsp;<xsl:value-of select="USERS/USER/last_name" disable-output-escaping="yes"/></td>
                    	<td width="15%" class="label" colspan="2" align="left">Email: <xsl:value-of select="USERS/USER/email" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                        <td class="tblHeader" colspan="4">Add User IDs</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">User ID:<font color="red">&nbsp;***</font></td>
                        <td width="35%"><input type="text" name="identityId"  value="{key('field', 'identityId')/@value}" tabindex="1" size="20" maxlength="20"/></td>
      					<td width="15%" class="label">Password Expiration Date: </td>
          				<td width="35%">
                            <input type="text" name="pwdExpireDate" disabled="true" size="10" maxlength="10"/>
                        </td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'identityId')/@description"/></td>
	                </tr>
					<tr>
            		<td width="15%" class="label">ID Expiration Date:<font color="red">&nbsp;***</font> (mm/dd/yyyy)</td>
              		<td width="35%">
                            <input type="text" name="expireDate" id="exp_date_id" value="{key('field', 'expireDate')/@value}" tabindex="2" size="10" maxlength="10"/>
                            <img id="exp_date_img" name="expireDateCalendar" src="/{$applicationcontext}/security/images/calendar.gif"/>
              		</td>
                    	<td width="15%" class="label">Locked:<font color="red">&nbsp;***</font></td>
                    	<td width="35%">
			  				<xsl:choose>
								<xsl:when test="'Y'=$selectedLocked">
                    				<input type="radio" name="locked" value="Y" tabindex="3" checked="true"/>Yes&nbsp;&nbsp;
                    				<input type="radio" name="locked" value="N" tabindex="3"/>No
								</xsl:when>
						    	<xsl:otherwise>
						    		<input type="radio" name="locked" value="Y" tabindex="3"/>Yes&nbsp;&nbsp;
						    		<input type="radio" name="locked" value="N" tabindex="3" checked="true"/>No
						    	</xsl:otherwise>
			    			</xsl:choose>
                    	</td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'expireDate')/@description"/></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'locked')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Role Context:</td>
						<td width="35%">
							<select name="contextId" tabindex="4" onchange="javascript:callChangeContext()">
								<option value="">- Select One -</option>
								<xsl:for-each select="CONTEXTS/CONTEXT">
								<xsl:choose>
									<xsl:when test="context_id=$selectedContext">
										<option value="{context_id}" selected="true"><xsl:value-of select="context_id"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{context_id}"><xsl:value-of select="context_id"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
      				</tr>
					<!-- Do not show this section if role context is not selected -->
					<xsl:if test="$selectedContext != ''">
      				<tr>
						<td width="15%" class="label" rowspan="2" valign="top">Select Roles:</td>
						<td width="35%" rowspan="6">
							<select name="roles" multiple="true" tabindex="5" size="6">
								<xsl:for-each select="ROLES/ROLE">
								<xsl:choose>
									<xsl:when test="@selected='Y'">
										<option value="{role_id}" selected="true"><xsl:value-of select="role_name"/></option>
									</xsl:when>
									<xsl:otherwise>
										<option value="{role_id}"><xsl:value-of select="role_name"/></option>
									</xsl:otherwise>
								</xsl:choose>
								</xsl:for-each>
							</select>
						</td>
					</tr>
					</xsl:if>
                </table>
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="colheader">User ID</td>
                        <td class="colheader">User Name</td>
                        <td class="colheader">ID Expire Date</td>
                        <td class="colheader">Password Expire Date</td>
                        <td class="colheader">Role</td>
                    </tr>
                    <xsl:variable name="fname" select="USERS/USER/first_name"/>
                    <xsl:variable name="lname" select="USERS/USER/last_name"/>
                    
                    <xsl:for-each select="USERS/USER/IDENTITY">
					<xsl:variable name="iExpYear" select="substring(identity_expire_date,1,4)"/>
					<xsl:variable name="iExpMonth" select="substring(identity_expire_date,6,2)"/>
					<xsl:variable name="iExpDay" select="substring(identity_expire_date,9,2)"/>
					<xsl:variable name="cExpYear" select="substring(credentials_expire_date,1,4)"/>
					<xsl:variable name="cExpMonth" select="substring(credentials_expire_date,6,2)"/>
					<xsl:variable name="cExpDay" select="substring(credentials_expire_date,9,2)"/>
                    <tr>
                    	<xsl:variable name="id" select="identity_id"/>
                        <td><a tabindex="8" href="javascript: viewId('{$id}')"><xsl:value-of select="identity_id"/></a></td>
                        <td><xsl:value-of select="$fname"/>&nbsp;
                  						      <xsl:value-of select="$lname"/></td>
                        <td><xsl:value-of select="concat($iExpMonth,'/',$iExpDay,'/',$iExpYear)"/></td>
                        <td><xsl:value-of select="concat($cExpMonth,'/',$cExpDay,'/',$cExpYear)"/></td>
                        <td><xsl:for-each select="ROLE">
                        	<xsl:value-of select="role_name"/>
                        	<xsl:if test="position()!=last()">,</xsl:if>
                        </xsl:for-each></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="createIdentity" class="BlueButton" value="Save"  tabindex="6" onclick="javascript:goSave()"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" tabindex="7" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "exp_date_id",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "exp_date_img"  // ID of the button
    }
  );

</script>
</xsl:template>
</xsl:stylesheet>