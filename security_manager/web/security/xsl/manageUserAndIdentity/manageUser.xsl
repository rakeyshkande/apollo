<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/USERS/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - User Management</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"/>
	<script language="javascript">
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
	<![CDATA[
    
        var fieldNames = new Array("firstName", "phone", "phoneExt", "lastName", "fax", "address1",
            "emailAddress", "address2", "expireDate", "city", "state", "zip", "updateUser", "removeUser");
        var images = new Array("expireCalendar");
        var requiredFields = new Array("firstName", "lastName","expireDate");
        var confirmMsg = "Are you sure you want to remove this user?";
		
        function submitForm(servlet, action){
  
        	if(action == "update") {
        		//if (!checkRequiredFields(requiredFields)) {
        		//	return;
        		//}
        		//check expire date format
 				if(!ValidateDate("expireDate")) {
 					return;
 				}
 			}
			doSubmit(servlet, action);
        }

        function expireNow(fieldName, checkbox){
          var date = new Date();
          var dateText = (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear();
          if (!document.all(checkbox).checked) {
            document.all(fieldName).value = dateText;
			doSubmit("SecurityUser.do", "update");
          }
		  
        }
        
		function callRemove(servlet, action) {
			if (confirm(confirmMsg)) {
				doSubmit(servlet, action);
			}
		}
			
		function doSubmit(servlet, action) {
			document.forms[0].action = "/" + context_js + "/security/" + servlet;
			document.forms[0].adminAction.value = action;
            document.forms[0].submit();
   		}
   		
        function accessControl(checked){
          var checkboxText = document.all("accessTD");
            if (checked){   // Lock fields
               checkboxText.innerHTML = "Locked";
            } else {        // Unlock fields
            checkboxText.innerHTML = "Unlocked";
          }

          for (var i = 0; i < fieldNames.length; i++){
            document.all(fieldNames[i]).disabled = checked;
          }
        }

		function checkRequiredFields(fields)
		{
    		for (var i = 0; i < fields.length; i++){
				if(document.all(fields[i]).value == "") {
					alert(document.all(fields[i]).name + " is required");
					return false;
				}
   			}
			return true;
		}

		function goMain() {
			document.forms[0].action = "/" + context_js + "/security/Main";
			document.forms[0].adminAction.value = "systemAdministration";
			document.forms[0].submit();
		}
  	]]>
    </script>  
  
  </head>
 
  <body>  
 
 
    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <input type="hidden" name="user_id" value="{USERS/USER/user_id}" />
     <input type="hidden" name="first_name" value="{USERS/USER/first_name}" />
     <input type="hidden" name="last_name" value="{USERS/USER/last_name}" />
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Manage User'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

    <table width="98%" border="3" align="center" cellpadding="2" cellspacing="2" bordercolor="#006699">
        <tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Main Menu</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('SecurityUser.do','view_all')">Users</a>&nbsp; >
                User Management
            </td>
        </tr>
               <tr>
            <td>             
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">First Name:</td>
                        <td width="35%"><input type="text" name="firstName" tabindex="1" size="30" maxlength="255" value="{USERS/USER/first_name}" disabled="true"/></td>
                        <td width="15%" class="label">Phone:</td>
                        <td width="35%">
                            <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="label">
                                    <input type="text" name="phone" tabindex="8" size="10" maxlength="10" value="{USERS/USER/phone}" disabled="true"/>
                                    &nbsp;&nbsp;Ext.&nbsp;&nbsp;<input type="text" name="phoneExt" tabindex="9" size="6" maxlength="40"  value="{USERS/USER/extension}" disabled="true"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Last Name:</td>
                        <td width="35%"><input type="text" name="lastName" tabindex="2" size="30" maxlength="255" value="{USERS/USER/last_name}" disabled="true"/></td>
                        <td width="15%" class="label">Fax:</td>
                        <td width="35%"><input type="text" name="fax" tabindex="10" size="20" maxlength="40" value="{USERS/USER/fax}" disabled="true"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Address Line 1:</td>
                        <td width="35%"><input type="text" name="address1" tabindex="3" size="30" maxlength="4000"  value="{USERS/USER/address_1}" disabled="true"/></td>
                        <td width="15%" class="label">Email:</td>
                        <td width="35%"><input type="text" name="emailAddress" tabindex="11" size="20" maxlength="100" value="{USERS/USER/email}" disabled="true"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Address Line 2:</td>
                        <td width="35%"><input type="text" name="address2" tabindex="4" size="30" maxlength="4000" value="{USERS/USER/address_2}" disabled="true"/></td>
                        <td width="15%" class="label">Expire Date:</td>
                        <xsl:variable name="expDate" select="USERS/USER/expire_date"/>
                        <xsl:variable name="expDateYear" select="substring($expDate,1,4)"/>
                        <xsl:variable name="expDateMonth" select="substring($expDate,6,2)"/>
                       	<xsl:variable name="expDateDay" select="substring($expDate,9,2)"/>
                        
                        <td width="35%">
                            <input type="text" name="expireDate" tabindex="12" size="10" maxlength="10"  value="{$expDateMonth}/{$expDateDay}/{$expDateYear}" disabled="true"/>
                            <!--img id="expireCalendar" src="../security/images/calendar.gif" width="18" height="18" align="absmiddle"/-->
                            <a class="linkblue" href="javascript:expireNow('expireDate', 'accessCheckbox');">Expire Now</a>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">City:</td>
                        <td width="35%"><input type="text" name="city" tabindex="5" size="30" maxlength="255"  value="{USERS/USER/city}" disabled="true"/></td>
                        <td width="15%"></td>
                        <td width="35%"></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">State:</td>
                        <td width="35%"><input type="text" name="state" tabindex="6" size="2" maxlength="2"  value="{USERS/USER/state}" disabled="true"/></td>
                        <td width="15%"></td>
                        <td width="35%"></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Zip:</td>
                        <td width="35%"><input type="text" name="zip" tabindex="7" size="6" maxlength="20" value="{USERS/USER/zip}" disabled="true"/></td>
                        <td width="15%"></td>
                        <td width="35%" align="right">
                            <table>
                            	<tr>
                            		<td id="accessTD" class="accesslabel">Locked</td>
                                    <td>
                                    	<input type="checkbox" name="accessCheckbox" checked="true" onclick="javascript:accessControl(this.checked);"/>
                                    </td>
                            	</tr>
                            </table>
                        </td>
                    </tr>             
                </table>

                <!-- Identities -->

                <table width="100%" align="center" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                    	<td width="20%" class="colHeader">Identities</td>
                    	<td width="40%" class="colHeader">Roles</td>
                    	<td width="40%" class="colHeader">Contexts</td>
                    </tr>
                    <tr>
                    	<td><a class="linkblue" href="javascript:submitForm('Identity.do', 'view_all')">Manage Identites</a></td>
                    	<td>&nbsp;</td>
                    	<td>&nbsp;</td>
                    </tr>
                    <xsl:for-each select="USERS/USER/IDENTITY">
                    
                    <tr>
                    	<xsl:variable name="identityId" select="identity_id"/>
                    	<td class="Label"><xsl:value-of select="$identityId"/></td>
                    	<xsl:apply-templates select="ROLE"/>
                    	
                    </tr>
                    
                    </xsl:for-each>
         
                </table>  
            </td>
        </tr>
    </table>  
  
 
    <table width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td align="right">
                <input type="button" name="updateUser" class="BlueButton" tabindex="13" value="Update User" disabled="true" onclick="javascript:submitForm('SecurityUser.do', 'update');"/>
            </td>
            <td align="left">
                <input type="button" name="removeUser" class="BlueButton" tabindex="13" value="Remove User" disabled="true" onclick="javascript:callRemove('SecurityUser.do', 'remove');"/>
            </td>
        </tr>
    </table>
   
    </form>   
    <xsl:call-template name="footer"/>
 

   </body>   
  </html>
 
</xsl:template>

<xsl:template match="ROLE">
<xsl:if test="string-length(identity_id)>0">
<tr>
	<td>&nbsp;</td>
	<td class="tblDataLeft"><xsl:value-of select="role_name"/></td>
 	<td class="tblDataLeft"><xsl:value-of select="context_id"/></td>
</tr>
</xsl:if>
</xsl:template>

</xsl:stylesheet>