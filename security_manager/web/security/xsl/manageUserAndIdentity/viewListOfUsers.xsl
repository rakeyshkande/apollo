<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - User List</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
  	
	function goMain() {
		document.forms[0].action = "/" + context_js + "/security/Main.do";
		document.forms[0].adminAction.value = "systemAdministration";
		document.forms[0].submit();
	}
	
	function submitForm(id){
		document.forms[0].user_id.value = id;
	    document.forms[0].action = "/" + context_js + "/security/SecurityUser.do"; 
	    document.forms[0].submit();
	}
   ]]>
   </script> 
  
  </head>
  <body>
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="user_id" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'User List'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
            	<a class="BreadCrumbsLink" href="javascript:goMain()">Main Menu</a>&nbsp;>
                Users
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
                    <tr>
                        <td>
			<xsl:for-each select="USERS/USER">
				
			    <a class="link" href="javascript:submitForm('{user_id}')">
			    <xsl:value-of select="first_name"/>&nbsp;
			    <xsl:value-of select="last_name"/>
			    </a>
			    <br/>
			</xsl:for-each>                        
                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>  
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>