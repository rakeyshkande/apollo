<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="security" match="/USER/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>

    <title>FTD - Role Management</title>
    <link rel="stylesheet" type="text/css" href="../security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="../security/js/util.js"/>
    <script type="text/javascript" language="JavaScript">
    var removeCount = <xsl:value-of select="count(USER/IDENTITY/ROLE[string-length(identity_id)>0])"/>;
	var addCount = <xsl:value-of select="count(USER/IDENTITY/ROLE[string-length(identity_id)=0])"/>;
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
	<![CDATA[

	function setRemoveButton(){
	
	    for (var i = 1; i <= removeCount; i++){
	    	var index = addCount + i;
			if (document.all("removeCheckbox" + index).checked){
		    	document.all("removeButton").disabled = false;
		    	return;
			}
	    }
	    document.all("removeButton").disabled = true;
	   
	}
	
	function setAddButton(){
	    for (var i = 1; i <= addCount; i++){
			if (document.all("addCheckbox" + i).checked){
		    	document.all("addButton").disabled = false;
		    	return;
			}
	    }
	    document.all("addButton").disabled = true;
	}
	
	function callAddRole() {
		var ids = "";
		for (var i = 1; i <= addCount; i++) {
			if (document.all("addCheckbox" + i).checked) {
				ids = ids + "," + i;
			}
		}
		document.all("role_ids").value = ids;
		submitForm("Identity.do","addRole");
	}
	
	function callRemoveRole() {
		var ids = "";

		for (var i = 1; i <= removeCount; i++) {
			var index = addCount + i;
				if (document.all("removeCheckbox" + index).checked) {
					ids = ids + "," + index;
				}
		}
			
		document.all("role_ids").value = ids;
		submitForm("Identity.do","removeRole");
		
	}
	
	function submitForm(servlet, action){
		document.forms[0].action = "/" + context_js + "/security/" + servlet;
		document.forms[0].adminAction.value = action;
 		document.forms[0].submit();
 	}

	function goMain() {
		document.forms[0].action = "/" + context_js + "/security/Main";
		document.forms[0].adminAction.value = "systemAdministration";
		document.forms[0].submit();
	}
    ]]>

       </script>
  </head>


  <body>

    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <input type="hidden" name="user_id" value="{USER/@user_id}"/>
     <input type="hidden" name="first_name" value="{USER/@first_name}"/>
     <input type="hidden" name="last_name" value="{USER/@last_name}"/>
     <input type="hidden" name="identity_id" value="{USER/IDENTITY/@identity_id}"/>
     <input type="hidden" name="role_ids" value=""/>

     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Manage Role'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>

    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Main Menu</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('SecurityUser.do','view_all')">Users</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('SecurityUser.do','view')">User</a>&nbsp; >
                <a class="BreadCrumbsLink" href="javascript:submitForm('Identity.do','view_all')">Identity</a>&nbsp; >
                Role
            </td>
        </tr>
               <tr>
        	<td>
                <!-- User Information -->
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User And Identity Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Name:</td>
                        <td width="35%" class="label">
                        <xsl:value-of select="USER/@first_name"/>&nbsp;
                        <xsl:value-of select="USER/@last_name"/></td>
                    	<!--td width="15%" class="label">Email:</td>
                        <td width="35%" class="label"><xsl:value-of select="USER/@email"/></td-->
                    </tr>
                    <tr>
                        <td width="15%" class="label">Identity:</td>
                        <td width="35%" class="label"><xsl:value-of select="USER/IDENTITY/@identity_id"/></td>
                        <!--td width="15%" class="label">Description:</td>
                        <td width="35%" class="label"><xsl:value-of select="USER/IDENTITY/@description"/></td-->
                    </tr>
                </table>
                <!-- Role Management -->
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="48%" class="tblHeader">Available Roles</td>
                        <td width="4%">&nbsp;</td>
                        <td width="48%" class="tblHeader">Current Roles</td>
                    </tr>
                    <tr>
                    	<td align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="12%" class="colheader"></td>
                                    <td width="44%" class="colheader">Context</td>
                                    <td width="44%" class="colheader">Role</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div id="avialableRoles" style="overflow:auto; width:100%; height:200; border-left:2px #006699 solid; border-right:2px #006699 solid; border-bottom:2px #006699 solid; padding:0px; margin:0px">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <xsl:for-each select="USER/IDENTITY/ROLE">
                                            <xsl:sort select="identity_id"/>
                                            <!--xsl:sort select="@context_id"/-->
                                            <xsl:if test="string-length(identity_id)=0">
                                            
        										<tr>
        											<td width="12%" align="center"><input type="checkbox" name="{concat('addCheckbox',position())}" onclick="javascript:setAddButton()"/></td>
       												<td width="44%" class="tblDataLeft"><xsl:value-of select="context_id"/></td>
        											<td width="44%" class="tblDataLeft"><xsl:value-of select="role_name"/></td>
        											<input type="hidden" name="{concat('addRoleId', position())}" value="{role_id}"/>
       											</tr>
											</xsl:if>
											</xsl:for-each>
                                            </table>
											
                                            
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="middle">
                            <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <tr>
                                    <td>
                                        <input type="button" name="addButton" class="BlueButton" value="  -&gt;  " disabled="true" onclick="javascript:callAddRole();"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" name="removeButton" class="BlueButton" value="  &lt;-  " disabled="true" onclick="javascript:callRemoveRole();"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="12%" class="colheader"></td>
                                    <td width="44%" class="colheader">Context</td>
                                    <td width="44%" class="colheader">Role</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div id="currentRoles" style="overflow:auto; width:100%; height:200; border-right:2px #006699 solid; border-left:2px #006699 solid; border-bottom:2px #006699 solid; padding:0px; margin:0px">
                                            <table width="100%" cellspacing="0" cellpadding="0">
                                            <xsl:for-each select="USER/IDENTITY/ROLE">
                                            <xsl:sort select="identity_id "/>
                                            <!--xsl:sort select="@context_id"/-->
                                            <xsl:if test="string-length(identity_id)>0">
        										<tr>
        											<td width="12%" align="center"><input type="checkbox" name="{concat('removeCheckbox',position())}" onclick="javascript:setRemoveButton()"/></td>
       												<td width="44%" class="tblDataLeft"><xsl:value-of select="context_id"/></td>
        											<td width="44%" class="tblDataLeft"><xsl:value-of select="role_name"/></td>
        											<input type="hidden" name="{concat('removeRoleId', position())}" value="{role_id}"/>
       											</tr>
											</xsl:if>
											</xsl:for-each>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    </form>

    <xsl:call-template name="footer"/>

   </body>
  </html>
</xsl:template>

</xsl:stylesheet>