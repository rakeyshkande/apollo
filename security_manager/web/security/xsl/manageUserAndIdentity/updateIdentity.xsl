<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>


<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>
<xsl:variable name="selectedLocked" select="key('field', 'locked')/@value"/>
<xsl:variable name="selectedContext" select="key('field', 'contextId')/@value"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="credentials"><xsl:value-of select="key('security','credentials')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - User Id Update</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/calendar.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/date.js"></script>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/calendar.js"/>
    <script language="javascript">
    var removeCount = <xsl:value-of select="count(ROLES/ROLE[string-length(identity_id)>0])"/>;
	var addCount = <xsl:value-of select="count(ROLES/ROLE[string-length(identity_id)=0])"/>;
   	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[
  var password = '<xsl:value-of select="$credentials"/>';
  var unlocked = 'Y';

	function init(){
		setErrorFields();
        // showNewPassword();
	}
  
  function disableAllSubmission() {

   for(var i = 0; i < document.forms[0].elements.length; i++){
        if(document.forms[0].elements[i].type == 'button')
                document.forms[0].elements[i].disabled = true;
   }
   
   document.all("divExpireNow").style.visibility="hidden";
}

    function showNewPassword() {
        if (password != '') {
            alert("New Password is " + password);
        }
    }
    
	function goSave() {

    if(unlocked == 'N')
    {
      confirm('Password for the user will be automatically reset');
    }

    setNavigationHandlers(); 
    disableAllSubmission();
		submitForm("Identity.do", "update");
	}
	
	function goResetPassword() 
  {
		disableAllSubmission();
    submitForm("Identity.do", "reset_password");
	}

  function locked_onclick(clicked)
  {
    unlocked = clicked;
    if(clicked == 'Y')
    {
      document.forms[0].resetpass.value = 'n';
    }
    else
    {
      document.forms[0].resetpass.value = 'y';
    }
  }
	
	function goExpireId() {
		var date = new Date();
		var dateText = (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear();
		document.forms[0].expireDate.value=dateText;
    disableAllSubmission();
		submitForm("Identity.do", "update");
	}
	
	function callAddRole() {
		var ids = "";
		for (var i = 1; i <= addCount; i++) {
			if (document.all("addCheckbox" + i).checked) {
				ids = ids + "," + i;
			}
		}
		document.all("role_ids").value = ids;
		submitForm("Identity.do","addRole");
	}

	function callRemoveRole() {
		var ids = "";

		for (var i = 1; i <= removeCount; i++) {
			var index = addCount + i;
				if (document.all("removeCheckbox" + index).checked) {
					ids = ids + "," + index;
				}
		}

		document.all("role_ids").value = ids;
		submitForm("Identity.do","removeRole");

	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		document.forms[0].isExit.value = "Y";
		submitForm("Main.do", "securityAdministration");
	}
	function callView() {
		submitForm("SecurityUser.do", "view");
	}

	function callSearch() {
		submitForm("SecurityUser.do", "display_search");
	}

	function setRemoveButton(){

	    for (var i = 1; i <= removeCount; i++){
	    	var index = addCount + i;
			if (document.all("removeCheckbox" + index).checked){
		    	document.all("removeButton").disabled = false;
		    	return;
			}
	    }
	    document.all("removeButton").disabled = true;

	}

	function setAddButton(){
	    for (var i = 1; i <= addCount; i++){
			if (document.all("addCheckbox" + i).checked){
		    	document.all("addButton").disabled = false;
		    	return;
			}
	    }
	    document.all("addButton").disabled = true;
	}
   ]]>
   </script>

  </head>
  <body onload="javascript:init();">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="isExit" value=""/>
    <input type="hidden" name="user_id" value="{USERS/USER/user_id}"/>
    <input type="hidden" name="role_ids" value=""/>
    <input type="hidden" name="resetpass" value="n"/>

    <xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Update User Identity'"/>
   		<xsl:with-param name="applicationcontext" select="$applicationcontext"/>
    </xsl:call-template>
    <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" tabindex="7" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callSearch()">Users</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callView()">User</a>&nbsp;>
                Update Identity
            </td>
        </tr>
    </table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">User Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Name:</td>
                        <td width="35%"><xsl:value-of select="USERS/USER/first_name" disable-output-escaping="yes"/>
                        		&nbsp;<xsl:value-of select="USERS/USER/last_name" disable-output-escaping="yes"/></td>
                    	<td width="15%" class="label">Email:</td>
                    	<td width="35%"><xsl:value-of select="USERS/USER/email" disable-output-escaping="yes"/></td>
                    </tr>
                    <tr>
                        <td class="tblHeader" colspan="4">User ID Info</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">User ID:</td>
                        <xsl:variable name="id" select="key('field', 'identityId')/@value"/>
                        <input type="hidden" name="identityId" value="{$id}"/>
                        <td width="35%"><input type="text" name="identityIdDisplay"  value="{$id}" disabled="true" tabindex="1" size="20" maxlength="20"/></td>
      					<td width="15%" class="label">ID Expire Date:<font color="red">&nbsp;***</font> (mm/dd/yyyy)</td>
              			<td width="35%">
                            <input type="text" name="expireDate" id="exp_date_id" value="{key('field', 'expireDate')/@value}" tabindex="2" size="10" maxlength="10"/>
							<img id="exp_date_img" name="expireDateCalendar" src="/{$applicationcontext}/security/images/calendar.gif"/><div id="divExpireNow">&nbsp;<a class="linkblue" href="javascript:goExpireId()">Expire Now</a></div>
                            </td>
              		</tr>
              		<tr>
              		  <td width="15%"></td>
              		  <td width="35%"></td>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'expireDate')/@description"/></td>
	                </tr>
              		<tr>
        				<td width="15%" class="label">Locked:<font color="red">&nbsp;***</font></td>
                    	<td width="35%">
			  				<xsl:choose>
								<xsl:when test="'Y'=$selectedLocked">
                    				<input type="radio" name="locked" value="Y" tabindex="3" checked="true" onClick="locked_onclick('Y')"/>Yes&nbsp;&nbsp;
                    				<input type="radio" name="locked" value="N" tabindex="3" onClick="locked_onclick('N')"/>No
								</xsl:when>
						    	<xsl:otherwise>
                  <input type="radio" name="locked" value="Y" tabindex="3" onClick="locked_onclick('Y')"/>Yes&nbsp;&nbsp;
                  <input type="radio" name="locked" value="N" tabindex="3" checked="true" onClick="locked_onclick('N')"/>No
						    	</xsl:otherwise>
			    			</xsl:choose>
                    	</td>
      					<td width="15%" class="label">Password Expire Date: </td>
          				<td width="35%">
							<xsl:variable name="pwdExpDate" select="key('field', 'pwdExpireDate')/@value"/>
							<input type="hidden" name="pwdExpireDate" value="{$pwdExpDate}"/>
                            <input type="text" name="pwdExpireDateDisplay" value="{$pwdExpDate}" disabled="true" size="10" maxlength="10"/>
                        </td>
					</tr>
              		<tr>
	                  <td width="15%"></td>
	                  <td width="35%" class="ErrorMessage"><xsl:value-of select="key('error', 'locked')/@description"/></td>
              		  <td width="15%"></td>
              		  <td width="35%"></td>
	                </tr>
	                <tr>
              			<td width="15%" class="label">Employee ID:</td>
                        <td width="35%"><xsl:value-of select="USERS/USER/employee_id" disable-output-escaping="yes"/></td>
						<td width="15%" class="label">Supervisor:</td>
                        <td width="35%"><xsl:value-of select="USERS/USER/supervisor_id" disable-output-escaping="yes"/></td>
					</tr>
					
            <xsl:if test="$credentials != ''">
            <tr>
                        <td width="15%" class="label">Temporary Password:</td>
                        <td width="35%"><xsl:value-of select="$credentials"/></td>
                    	<td width="15%"></td>
                    	<td width="35%"></td>
                    </tr>
          </xsl:if>
                </table>
                <!-- Role Management -->
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="48%" class="tblHeader">Available Roles</td>
                        <td width="4%">&nbsp;</td>
                        <td width="48%" class="tblHeader">Current Roles</td>
                    </tr>
                    <tr>
                    	<td align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="12%" class="colheader"></td>
                                    <td width="44%" class="colheader">Role</td>
                                    <td width="44%" class="colheader">Context</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div id="avialableRoles" style="overflow:auto; width:100%; height:200; border-left:2px #006699 solid; border-right:2px #006699 solid; border-bottom:2px #006699 solid; padding:0px; margin:0px">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <xsl:for-each select="ROLES/ROLE">
                                            <xsl:sort select="identity_id"/>
                                            <!--xsl:sort select="@context_id"/-->
                                            <xsl:if test="string-length(identity_id)=0">

        										<tr>
        											<td width="12%" align="center"><input type="checkbox" name="{concat('addCheckbox',position())}" onclick="javascript:setAddButton()"/></td>
       												<td width="44%" class="tblDataLeft"><xsl:value-of select="role_name"/></td>
        											<td width="44%" class="tblDataLeft"><xsl:value-of select="context_id"/></td>
        											<input type="hidden" name="{concat('addRoleId', position())}" value="{role_id}"/>
       											</tr>
											</xsl:if>
											</xsl:for-each>
                                            </table>


                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="middle">
                            <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <tr>
                                    <td>
                                        <input type="button" name="addButton" class="BlueButton" value="  -&gt;  " disabled="true" onclick="javascript:callAddRole();"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="button" name="removeButton" class="BlueButton" value="  &lt;-  " disabled="true" onclick="javascript:callRemoveRole();"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="12%" class="colheader"></td>
                                    <td width="44%" class="colheader">Role</td>
                                    <td width="44%" class="colheader">Context</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div id="currentRoles" style="overflow:auto; width:100%; height:200; border-right:2px #006699 solid; border-left:2px #006699 solid; border-bottom:2px #006699 solid; padding:0px; margin:0px">
                                            <table width="100%" cellspacing="0" cellpadding="0">
                                            <xsl:for-each select="ROLES/ROLE">
                                            <xsl:sort select="identity_id "/>
                                            <!--xsl:sort select="@context_id"/-->
                                            <xsl:if test="string-length(identity_id)>0">
        										<tr>
        											<td width="12%" align="center"><input type="checkbox" name="{concat('removeCheckbox',position())}" onclick="javascript:setRemoveButton()"/></td>
       												<td width="44%" class="tblDataLeft"><xsl:value-of select="role_name"/></td>
        											<td width="44%" class="tblDataLeft"><xsl:value-of select="context_id"/></td>
        											<input type="hidden" name="{concat('removeRoleId', position())}" value="{role_id}"/>
       											</tr>
											</xsl:if>
											</xsl:for-each>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="updateIdentity" class="BlueButton" value="Save"  tabindex="6" onclick="javascript:goSave()"/>
               <!--15689 commenting  <input type="button" name="resetPassword" class="BlueButton" value="Reset Password"  tabindex="7" onclick="javascript:goResetPassword()"/>  -->
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" tabindex="7" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
	<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "exp_date_id",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "exp_date_img"  // ID of the button
    }
  );

	</script>
</xsl:template>
</xsl:stylesheet>