<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/CONTEXTS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Context Detail</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var fieldNames = new Array("description", "updateButton");
    var confirmMsg = "Are you sure you want to remove the Context?";

    function init() {
    	setNavigationHandlers();
    }
	function accessControl(checked){
    	var checkboxText = document.all("accessTD");
    	if (checked){   // Lock fields
        	checkboxText.innerHTML = "Locked";
    	} else {        // Unlock fields
        	checkboxText.innerHTML = "Unlocked";
    	}
    	for (var i = 0; i < fieldNames.length; i++){
        	document.all(fieldNames[i]).disabled = checked;
    	}
	}
	
	function callViewAll() {
		submitForm("Context.do", "view_all");
	}
	
	function callUpdate() {
		submitForm("Context.do", "update");
	}
	
	function callManageConfig() {
		submitForm("Context.do", "manage_config");
	}
	
	function callRemove() {
		if (confirm(confirmMsg)) {
			document.forms[0].remove_ids.value="1";
			document.forms[0].contextId1.value=document.forms[0].context_id.value;
			submitForm("Context.do", "remove");
		}
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context_id" value="{CONTEXTS/CONTEXT/context_id}"/>
    <input type="hidden" name="remove_ids" value=""/>
    <input type="hidden" name="contextId1" value=""/>
    
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Context Detail'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Contexts</a>&nbsp;>
                Context
            </td>
        </tr>
        <tr>
            <td>
        		<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Context Id:</td>
						<td><input type="text" name="contextId" size="10" value="{CONTEXTS/CONTEXT/context_id}" disabled="true"/></td>
					</tr>
					<tr>
						<td width="15%" class="label">Description:</td>
						<td><textarea name="description" cols="30" rows="5" disabled="true">
						<xsl:value-of select="CONTEXTS/CONTEXT/description"/></textarea></td>
						<td width="35%" align="right" valign="bottom">
						    <table>
						        <tr>
						            <td id="accessTD" class="accesslabel">Locked</td>
						            <td><input type="checkbox" name="accessCheckbox" checked="true" onclick="javascript:accessControl(this.checked);"/></td>
								</tr>
						    </table>
         				</td>
					</tr>
     			</table>
				<!-- Config info -->
				<table>
                    <tr>
                        <td width="85%" class="tblHeader" colspan="2">Configration Information</td>
                        <td class="tblHeader" align="right" colspan="2"><a class="linkwhite" href="javascript:callManageConfig()">Manage Config</a></td>
                    </tr>
                    <tr>
                        <td width="50%" class="colheader">Name</td>
                        <td width="50%" colspan="2" class="colheader">Value</td>
                    </tr>
                    <xsl:for-each select="CONTEXTS/CONTEXT">
                    	<xsl:sort select="config_name"/>
                    	<tr>
                        	<td><xsl:value-of select="config_name"/></td>
                        	<td><xsl:value-of select="config_value"/></td>
                    	</tr>
                    </xsl:for-each>
				</table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
        		<input type="button" name="updateButton" class="BlueButton" value="Update Context" disabled="true" onclick="javascript:callUpdate();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>