<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/CONTEXTS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Context List</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
  	
    <script language="javascript">
    var removeCount = <xsl:value-of select="count(CONTEXTS/CONTEXT)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var confirmMsg = "Are you sure you want to remove the context(s)?";
	
    function init() {
    	setNavigationHandlers();
    }
	function updateButton()
	{
		// If any of the check box is checked, enable its submit button.
		var disableRemove = true;
		for (var i=1; i<=removeCount; i++) {
			if (document.all("removeContextCB" + i).checked) {
				disableRemove = false;
				break;
			}
		}
		document.forms[0].removeButton.disabled=disableRemove;
		document.forms[0].addButton.disabled=(!disableRemove);
	}
	
	function callAddContext() {
		//window.location="../security/html/manageContext/CreateContext.html";
		submitForm("Context.do", "display_add");
	}
	
	function callRemoveContext() {
		var ids = "";
		for (var i = 1; i <= removeCount; i++) {
			if(document.all("removeContextCB" + i).checked) {
				ids = ids + "," + i;
			}
		}
	
		document.all("remove_ids").value = ids;

		if (confirm(confirmMsg)) {
			submitForm("Context.do", "remove");
		}
	}

	function callViewContext(contextId) {
		document.forms[0].context_id.value = contextId;
		submitForm("Context.do", "view");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script> 
  
  </head>
  <body onload="init()">
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context_id" value=""/>
    <input type="hidden" name="remove_ids" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Context List'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Contexts
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td class="colheader" width="5%" align="center">&nbsp;</td>
						<td class="colheader" width="95%">Context</td>
					</tr>
					
				<xsl:for-each select="CONTEXTS/CONTEXT">
				<xsl:sort select="context_id"/>
					<xsl:variable name="contextId" select="concat('contextId',position())"/>
					<xsl:variable name="removeContextCB" select="concat('removeContextCB',position())"/>
					<input type="hidden" name="{$contextId}" value="{context_id}"/>
					<tr>
						<xsl:choose>
						<xsl:when test="@selected='Y'">
						<td width="5%" align="center"><input type="checkbox" name="{$removeContextCB}" checked="true" onclick="updateButton()"/></td>
						</xsl:when>
						<xsl:otherwise>
						<td width="5%" align="center"><input type="checkbox" name="{$removeContextCB}" onclick="updateButton()"/></td>
						</xsl:otherwise>
						</xsl:choose>
						<td width="95%"><a class="link" href="javascript:callViewContext('{context_id}')"><xsl:value-of select="context_id"/></a></td>
					</tr>
					<xsl:if test="string-length(@error)>0">
					<tr><td width="5%">&nbsp;</td>
						<td class="errorMessage"><xsl:value-of select="@error"/></td>
					</tr>
					</xsl:if>
				</xsl:for-each>
    				
    				
   			</table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="addButton" value="Add New Context" class="BlueButton" onclick="javascript:callAddContext()"/>
				<input type="button" name="removeButton" value="Remove Checked" class="BlueButton" disabled="true" onclick="javascript:callRemoveContext()"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>