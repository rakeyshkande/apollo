<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/CONFIGS/security/data" use="name"/>

<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Config Detail</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var fieldNames = new Array("value", "updateButton", "removeButton");
   	var confirmMsg = "Are you sure you want to remove the config(s)?";

    function init() {
    	setNavigationHandlers();
    }
	function accessControl(checked){
    	var checkboxText = document.all("accessTD");
    	if (checked){   // Lock fields
        	checkboxText.innerHTML = "Locked";
    	} else {        // Unlock fields
        	checkboxText.innerHTML = "Unlocked";
    	}
    	for (var i = 0; i < fieldNames.length; i++){
        	document.all(fieldNames[i]).disabled = checked;
    	}
	}
	
	function callView() {
		//breadcrum
		submitForm("Context.do", "view");
	
	}
	function callViewAll() {
		//breadcrum
		submitForm("Context.do", "view_all");
	}
	
	function callUpdate() {
		submitForm("Context.do", "update_config");
	}
	
	function callManageConfig() {
		submitForm("Context.do", "manage_config");
	}
	
	function callRemove() {
		if(confirm(confirmMsg)) {
			submitForm("Context.do", "remove_config");
		}
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context_id" value="{CONFIGS/CONFIG/context_id}"/>
    <input type="hidden" name="config_name" value="{CONFIGS/CONFIG/name}"/>
    <input type="hidden" name="remove_ids" value="1"/>
    <input type="hidden" name="removeConfigCB1" value="{CONFIGS/CONFIG/name}"/>
    
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Config Detail'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Contexts</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callView()">Context</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callManageConfig()">Configs</a>&nbsp;>
                Config
            </td>
        </tr>
        <tr>
            <td>
                <!-- Context Information -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="2">Context Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Context Id:</td>
                        <td><xsl:value-of select="CONFIGS/CONFIG/context_id"/></td>
                    </tr>
                </table>
     			
                <!-- Configuration Information -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Context Config Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Config Name:</td>
                        <td><input type="text" name="name" value="{CONFIGS/CONFIG/name}" size="28" maxlength="255" disabled="true"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="15%" class="label" valign="top">Value:</td>
                        <td width="55%"><textarea name="value" cols="30" rows="5" disabled="true">
                        <xsl:value-of select="CONFIGS/CONFIG/value" disable-output-escaping="yes"/></textarea></td>
                        <td width="30%" align="right" valign="bottom">
                            <table>
                                <tr>
                                    <td id="accessTD" class="accesslabel">Locked</td>
                                    <td><input type="checkbox" name="accessCheckbox" checked="true" onclick="accessControl(this.checked);"/></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="updateButton" class="BlueButton" value="Update Config" disabled="true" onclick="javascript:callUpdate();"/>
				<input type="button" name="removeButton" class="BlueButton" value="Remove Config" disabled="true" onclick="javascript:callRemove();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>