<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/CONTEXTS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Manage Configs</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var removeCount = <xsl:value-of select="count(CONTEXTS/CONTEXT)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var confirmMsg = "Are you sure you want to remove the config(s)?";
    
    function init() {
    	setNavigationHandlers();
    }
	function callAdd() {
		submitForm("Context.do", "display_add_config");
	}
	
	function updateBT() {
		for (var i = 1; i <= removeCount; i++) {
			if(document.all("removeConfigCB" + i).checked) {
				document.all("removeButton").disabled = false;
				return;
			}
		}
	}

	function callRemove() {
		//document.forms[0].remove_ids.value="1";
		//document.forms[0].contextId1.value=document.forms[0].context_id.value;
		var ids = "";
		for (var i = 1; i <= removeCount; i++) {
			if(document.all("removeConfigCB" + i).checked) {
				ids = ids + "," + i;
			}
		}

		document.all("remove_ids").value = ids;

		if (confirm(confirmMsg)) {
			submitForm("Context.do", "remove_config");
		}
	}

	function callViewAll() {
		//breadcrum
		submitForm("Context.do", "view_all");
	}

	function callView() {
		//breadcrum
		submitForm("Context.do", "view");
	}
	
	function callViewConfig(pos) {
		document.forms[0].config_name.value = document.all("configName"+pos).value;
		//document.forms[0].config_name.value = cName;
		submitForm("Context.do", "view_config");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="remove_ids" value=""/>
	<input type="hidden" name="context_id" value="{CONTEXTS/CONTEXT/context_id}"/>
	<input type="hidden" name="config_name" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Manage Configs'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="99%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Contexts</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callView()">Context</a>&nbsp;>
                Manage Configs
            </td>
        </tr>
        <tr>
            <td>
                <!-- Context Information -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Context Information</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Context Id:</td>
                        <td><xsl:value-of select="CONTEXTS/CONTEXT/context_id"/></td>
                    </tr>
                </table>
                <!-- Configuration Information -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Context Configuration Information</td>
                    </tr>
                    <tr>
                        <td width="5%" class="colheader"></td>
                        <td class="colheader">Name</td>
                        <td class="colheader">Value</td>
                    </tr>
                    <xsl:if test="string-length(normalize-space(CONTEXTS/CONTEXT/config_name))>0">
                    <xsl:for-each select="CONTEXTS/CONTEXT">
                    <xsl:sort select="config_name"/>
                    <xsl:variable name="removeCB" select="concat('removeConfigCB',position())"/>
                    <xsl:variable name="configName" select="concat('configName', position())"/>
                    <input type="hidden" name="{$configName}" value="{config_name}"/>
                    <tr>
                        <td width="5%" align="center"><input type="checkbox" name="{$removeCB}" value="{config_name}" onclick="javascript:updateBT()"/></td>
                        <td><a href="javascript:callViewConfig('{position()}')"><xsl:value-of select="config_name"/></a></td>
                        <td><xsl:value-of select="config_value"/></td>
                    </tr>
                    </xsl:for-each>
                    </xsl:if>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="addButton" class="BlueButton" value="Add Context Config" onclick="javascript:callAdd();"/>
				<input type="button" name="removeButton" class="BlueButton" value="Remove Checked Config(s)" disabled="true" onclick="javascript:callRemove();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>