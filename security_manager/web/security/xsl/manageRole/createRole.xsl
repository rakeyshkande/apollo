<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:variable name="selectedContextId" select="key('field', 'context_id')/@value"/>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - Role Creation</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[
    function init() {
    	setNavigationHandlers();
    	setErrorFields();
    }
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	
	function callCreate(){
		submitForm("Role.do","add");
	}
	
	function callCancel() {
		submitForm("Role.do","view_all");
	}
	
	function callViewAll() {
		callCancel();
	}
	
	
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Create Role'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
     
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Roles</a>&nbsp;>
                Role
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Role Name:<font color="red">&nbsp;***</font></td>
						<td><input type="text" name="role_name" size="28" maxlength="255" value="{key('field', 'role_name')/@value}"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="85%" class="ErrorMessage"><xsl:value-of select="key('error', 'role_name')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Context Id:<font color="red">&nbsp;***</font></td>
						<td>
                        <select name="context_id">
						<xsl:for-each select="CONTEXTS/CONTEXT">
						  <xsl:choose>
							<xsl:when test="context_id=$selectedContextId">
							  <option value="{context_id}" selected="true"><xsl:value-of select="context_id"/></option>
							</xsl:when>
						    <xsl:otherwise>
							<option value="{context_id}"><xsl:value-of select="context_id"/></option>
							</xsl:otherwise>
						  </xsl:choose>
   						</xsl:for-each>
	 					</select>
						</td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="85%" class="ErrorMessage"><xsl:value-of select="key('error', 'context_id')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Description:</td>
						<td><textarea name="description" cols="30" rows="5"><xsl:value-of select="key('field', 'description')/@value"/></textarea></td>
					</tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" class="BlueButton" value="Save" onClick="javascript:callCreate();"/>
                <input type="button" class="BlueButton" value="Cancel" onClick="javascript:callCancel();"/>
                <input type="reset" class="BlueButton" value="Clear"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>