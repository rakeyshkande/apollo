<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ACLS/security/data" use="name"/>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>


<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Resource Group Detail</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  </head>
<body>
<center>
    <table width="98%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
        	<td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="tblHeader" colspan="2">Resource Group:
                        <xsl:value-of select="ACLS/ACL/@acl_name"/></td>
                        <td class="tblHeader">Context:
                        <xsl:value-of select="ACLS/ACL/@context_id"/></td>
                    </tr>
        			<tr>
						<td class="label">Resource</td>
						<td class="label">Permission</td>
					</tr>
					<xsl:for-each select="ACLS/ACL">
					<xsl:sort select="@resource_id"/>
					<tr>
						<td><xsl:value-of select="@resource_id" disable-output-escaping="yes"/></td>
						<td><xsl:value-of select="@permission_id" disable-output-escaping="yes"/></td>
					</tr>
					</xsl:for-each>
                </table>
        	</td>
        </tr>
    </table>
    <table width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr><td>&nbsp;</td></tr>
        <tr>
        	<td align="center">
				<input type="button" value="Close" class="BlueButton" onClick="javascript:window.close()"/>
        	</td>
        </tr>
    </table>
</center>
</body>
  </html>
</xsl:template>
</xsl:stylesheet>