<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROLES/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Role Detail</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
	
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var fieldNames = new Array("description", "updateButton", "removeButton");
    var confirmMsg = "Are you sure you want to remove this role?";

    function init() {
    	setNavigationHandlers();
    }
	function accessControl(checked){
    	var checkboxText = document.all("accessTD");
    	if (checked){   // Lock fields
        	checkboxText.innerHTML = "Locked";
    	} else {        // Unlock fields
        	checkboxText.innerHTML = "Unlocked";
    	}

    	for (var i = 0; i < fieldNames.length; i++){
        	document.all(fieldNames[i]).disabled = checked;
    	}
	}

	function callMapRole(){
		submitForm("Role.do", "map_ACL");
	}

	function callRemoveRole() {
		if(confirm(confirmMsg)) {
			submitForm("Role.do", "remove");
		}
	}
	
	function callUpdateRole() {
		submitForm("Role.do", "update");
	}
	
	function callViewAll() {
		submitForm("Role.do", "view_all");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="role_id" value="{ROLES/ROLE/role_id}"/>
    <input type="hidden" name="role_name" value="{ROLES/ROLE/role_name}"/>
    <input type="hidden" name="context_id" value="{ROLES/ROLE/context_id}"/>
    <input type="hidden" name="removeRoleId1" value="{ROLES/ROLE/role_id}"/>
    <input type="hidden" name="remove_ids" value="1"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Role Detail'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Roles</a>&nbsp;>
                Role
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Role Name:</td>
						<td><input type="text" name="roleName" size="30" maxlength="255" value="{ROLES/ROLE/role_name}" disabled="true"/></td>
					</tr>
					<tr>
						<td width="15%" class="label">Context Id:</td>
						<td>
                        <select name="contexId">
						<option value="context"><xsl:value-of select="ROLES/ROLE/context_id" disable-output-escaping="yes"/></option>
	 					</select>
						</td>
					</tr>
					<tr>
						<td width="15%" class="label">Description:</td>
						<td><textarea name="description" cols="30" rows="5" disabled="true">
						<xsl:value-of select="ROLES/ROLE/description"/></textarea></td>
						<td width="35%" align="right" valign="bottom">
						    <table>
						        <tr>
						            <td id="accessTD" class="accesslabel">Locked</td>
						            <td><input type="checkbox" name="accessCheckbox" checked="true" onclick="javascript:accessControl(this.checked);"/></td>
							</tr>
						    </table>
         				</td>
					</tr>
                </table>
                <!-- ACL list -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Associated ACLs</td>
                    </tr>
                    <tr>
						<td>
							<table>
			                    <xsl:for-each select="ROLES/ACLS/ACL">
		                    	<xsl:if test="string-length(@role_id)>0">
		        	    		<tr>
									<td colspan="2" class="Wrap">
									<xsl:value-of select="@acl_name"/>
									&nbsp;&nbsp;
									</td>
		   						</tr>
								</xsl:if>
								</xsl:for-each>
							</table>
						</td>
						<td valign="top" align="right"><a class="linkblue" href="javascript:callMapRole()">Assign Resource Group</a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="updateButton" class="BlueButton" value="Update Role" disabled="true" onclick="javascript:callUpdateRole();"/>
				<input type="button" name="removeButton" class="BlueButton" value="Remove Role" disabled="true" onclick="javascript:callRemoveRole();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>