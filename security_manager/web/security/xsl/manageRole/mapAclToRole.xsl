<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROLES/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Assign Resource Group</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var removeCount = <xsl:value-of select="count(ROLES/ACLS/ACL[string-length(@role_id)>0])"/>;
	var addCount = <xsl:value-of select="count(ROLES/ACLS/ACL[string-length(@role_id)=0])"/>;
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
 	var fieldNames = new Array("addButton", "removeButton");

    function init() {
    	setNavigationHandlers();
    }
	function setRemoveButton(){
	    for (var i = 1; i <= removeCount; i++){
	    	var index = addCount + i;
			if (document.all("removeCheckbox" + index).checked){
		    	document.all("removeButton").disabled = false;
		    	return;
			}
	    }
	    document.all("removeButton").disabled = true;
	}
	function setAddButton(){
	    for (var i = 1; i <= addCount; i++){
			if (document.all("addCheckbox" + i).checked){
		    	document.all("addButton").disabled = false;
		    	return;
			}
	    }
	    document.all("addButton").disabled = true;
	}

  	function callViewRoles() {
 		submitForm("Role.do", "view_all");
  	}
    	
  	function callViewRole() {
 		submitForm("Role.do", "view");
  	}
    	
    function callViewAcl(addr) {
    	// Open ACL detail pop up window
    	window.open(addr, 'Detail', 'width=500,height=300,scrollbars=yes,addressbar=yes');
    	return false;
    }

	function callAddACL(){
		var ids = "";
		for (var i = 1; i <= addCount; i++) {
			if (document.all("addCheckbox" + i).checked) {
				ids = ids + "," + i;
			}
		}
		document.all("acl_numbers").value = ids;
		submitForm("Role.do", "add_ACL");
	}

	function callRemoveACL() {
		var ids = "";
		for (var i = 1; i <= removeCount; i++) {
			var index = addCount + i;
			if (document.all("removeCheckbox" + index).checked) {
				ids = ids + "," + index;
			}
		}
		document.all("acl_numbers").value = ids;
		submitForm("Role.do", "remove_ACL");
	}
	
	function updateButton()
	{
    	// If any of the check box is checked, enable its submit button.
    	var disableRemove = true;
    	var disableAdd = true;
    	var removeCB = document.firstForm.removeAcl;
    	var addCB = document.firstForm.addAcl;

    	for (var i=0; i<removeCB.length; i++) {
        	if (removeCB[i].checked) {
            	disableRemove = false;
            	break;
        	}
    	}
    	for (var j=0; j<addCB.length; j++) {
        	if (addCB[j].checked) {
            	disableAdd = false;
            	break;
        	}
    	}
    	document.firstForm.remove.disabled=disableRemove;
    	document.firstForm.add.disabled=disableAdd;
	}	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
     <input type="hidden" name="adminAction" value=""/>
     <input type="hidden" name="context" value="{$context}"/>
     <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <input type="hidden" name="role_id" value="{ROLES/ROLE/role_id}"/>
     <input type="hidden" name="context_id" value="{ROLES/ROLE/context_id}"/>
     <input type="hidden" name="role_name" value="{ROLES/ROLE/role_name}"/>
     <input type="hidden" name="acl_name" value=""/>
     <input type="hidden" name="acl_numbers" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Assign Resource Group'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
     
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewRoles()">Roles</a>&nbsp; >        
                <a class="BreadCrumbsLink" href="javascript:callViewRole()">Role</a>&nbsp; >        
                Assign Resource Group
            </td>
        </tr>
        <tr>
            <td>
        	
                <!-- Role Info -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">Role Info</td>
                    </tr>
                    <tr>
                        <td width="15%" class="label">Role Name:</td>
                        <td width="35%"><xsl:value-of select="ROLES/ROLE/role_name"/></td>
                        <td width="15%" class="label">Context Id:</td>
                        <td width="35%"><xsl:value-of select="ROLES/ROLE/context_id"/></td>
                    </tr>
                </table>
                <!-- List of existing ACLs -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Associated Resource Groups</td>
                    </tr>
                    <xsl:for-each select="ROLES/ACLS/ACL">
                    <xsl:sort select="@role_id"/>
                    <xsl:if test="string-length(@role_id)>0">
                    <tr>
                        <td><input type="checkbox" name="{concat('removeCheckbox',position())}" onclick="javascript:setRemoveButton()"/>
                            <a href="ACL.do?adminAction=view_popup&amp;acl_name={@acl_name}&amp;context={$context}&amp;securitytoken={$securitytoken}" onclick="return callViewAcl(this)">
                            <xsl:value-of select="@acl_name"/></a></td>
                            <input type="hidden" name="{concat('removeAclName', position())}" value="{@acl_name}"/>
                    </tr>
                    </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td align="right">
                            <input type="button" class="BlueButton" name="removeButton" value="Remove Checked" disabled="true" onclick="javascript:callRemoveACL()"/>
                        </td>
                    </tr>
                </table>
                <!-- List of available ACLs -->
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="3">Available Resource Groups</td>
                    </tr>
                    <xsl:for-each select="ROLES/ACLS/ACL">
                    <xsl:sort select="@role_id"/>
                    <xsl:if test="string-length(@role_id)=0">

                    <tr>
                        <td><input type="checkbox" name="{concat('addCheckbox',position())}" onclick="javascript:setAddButton()"/>
                            <a href="ACL.do?adminAction=view_popup&amp;acl_name={@acl_name}&amp;context={$context}&amp;securitytoken={$securitytoken}" onclick="return callViewAcl(this)">
                            <xsl:value-of select="@acl_name"/></a></td>
                            <input type="hidden" name="{concat('addAclName', position())}" value="{@acl_name}"/>
                    </tr>
                    </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td align="right">
                            <input type="button" class="BlueButton" name="addButton" value="Add Checked" disabled="true" onclick="javascript:callAddACL()"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
                <input type="button" name="done" class="BlueButton" value="Done" onclick="javascript:callViewRole()"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>