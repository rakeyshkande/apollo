<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="../header.xsl"/>
  <xsl:import href="../footer.xsl"/>
  <xsl:import href="../securityanddata.xsl"/>
  <xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROLES/security/data" use="name"/>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
  <xsl:template match="/">
    <html>
      <head>
        <title>FTD - Role List</title>
        <link rel="stylesheet" type="text/css"
              href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
        <script type="text/javascript"
                src="/{$applicationcontext}/security/js/util.js">
        </script>
        <script language="javascript">
    var removeCount = <xsl:value-of select="count(ROLES/ROLE)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    
          <![CDATA[
              var confirmMsg = "Are you sure you want to remove the role(s)?";
          
              function init() {
              	setNavigationHandlers();
              }
          	function updateButton()
          	{
          		// If any of the check box is checked, enable its submit button.
          		var disableRemove = true;
          		for (var i=1; i<=removeCount; i++) {
          			if (document.all("removeRoleCB" + i).checked) {
          				disableRemove = false;
          				break;
          			}
          		}
          		document.forms[0].removeButton.disabled=disableRemove;
          		document.forms[0].addButton.disabled=(!disableRemove);
          	}
          	
          	function callAddRole() {
          		submitForm("Role.do", "display_add");
          	}
          	
          	function callRemoveRole() {
          		var ids = "";
          		for (var i = 1; i <= removeCount; i++) {
          			if(document.all("removeRoleCB" + i).checked) {
          				ids = ids + "," + i;
          			}
          		}
          	
          		document.all("remove_ids").value = ids;
          
          		if (confirm(confirmMsg)) {
          			submitForm("Role.do", "remove");
          		}
          	}
          	function callViewRole(pos) {
          		document.forms[0].context_id.value = document.all("removeContextId" + pos).value;
          		document.forms[0].role_id.value = document.all("removeRoleId" + pos).value;
          		submitForm("Role.do", "view");
          	}
          	function submitForm(servlet, act){
          	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
          	    document.forms[0].adminAction.value = act;
          		document.forms[0].submit();
          	}
          	function goMain() {
          		submitForm("Main.do", "securityAdministration");
          	}
          
          	function goSystemAdmin() {
          		submitForm("Main.do", "systemAdministration");
          	}
             ]]>
        </script>
      </head>
      <body onload="javascript: init()">
        <form name="form" method="post">
          <input type="hidden" name="context" value="{$context}"/>
          <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
          <input type="hidden" name="adminAction" value="view"/>
          <input type="hidden" name="role_id" value=""/>
          <input type="hidden" name="context_id" value=""/>
          <input type="hidden" name="remove_ids" value=""/>
          <xsl:call-template name="header">
            <xsl:with-param name="headerName" select="'Role List'"/>
            <xsl:with-param name="applicationcontext"
                            select="$applicationcontext"/>
          </xsl:call-template>
          <xsl:call-template name="securityanddata"/>
          <table width="99%" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td align="right">
                <input type="button" name="MainMenu" class="BlueButton"
                       Value="Exit" onclick="javascript: goMain();"/>
              </td>
            </tr>
          </table>
          <table width="98%" border="3" align="center" cellpadding="1"
                 cellspacing="1" bordercolor="#006699">
            <tr>
              <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>
&nbsp;>
                Roles
            </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellpadding="2" cellspacing="2"
                       bordercolor="#006699">
                  <tr>
                    <td class="colheader">Remove</td>
                    <td class="colheader">Role Name</td>
                    <td class="colheader">Context</td>
                  </tr>
                  <xsl:for-each select="ROLES/ROLE">
                    <xsl:sort select="context_id"/>
                    <xsl:variable name="removeRoleId"
                                  select="concat('removeRoleId',position())"/>
                    <xsl:variable name="removeRoleCB"
                                  select="concat('removeRoleCB',position())"/>
                    <xsl:variable name="removeContextId"
                                  select="concat('removeContextId',position())"/>
                    <input type="hidden" name="{$removeRoleId}"
                           value="{role_id}"/>
                    <input type="hidden" name="{$removeContextId}"
                           value="{context_id}"/>
                    <tr>
                      <td width="10%" align="center">
                        <input type="checkbox" name="{$removeRoleCB}"
                               onclick="updateButton()"/>
                      </td>
                      <td width="45%">
                        <a class="link"
                           href="javascript:callViewRole('{position()}')">
                          <xsl:value-of select="role_name"/>
                        </a>
                      </td>
                      <td width="45%">
                        <xsl:value-of select="context_id"/>
                      </td>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </table>
          <table width="99%" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td width="5%">&nbsp;</td>
              <td align="center">
                <input type="button" name="addButton" class="BlueButton"
                       value="Add New Role" onclick="javascript:callAddRole()"/>
                <input type="button" name="removeButton" class="BlueButton"
                       value="Remove Checked" disabled="true"
                       onclick="javascript:callRemoveRole()"/>
              </td>
              <td width="5%" align="right">
                <input type="button" name="MainMenu" class="BlueButton"
                       Value="Exit" onclick="javascript: goMain();"/>
              </td>
            </tr>
          </table>
        </form>
        <xsl:call-template name="footer"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
