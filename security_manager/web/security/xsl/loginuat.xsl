<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ERROR/security/data" use="name"/>

<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<!--xsl:param name="securitytoken"/-->
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="credentials"><xsl:value-of select="key('security','credentials')/value" /></xsl:param>
<xsl:param name="passwordExpired"><xsl:value-of select="key('security','passwordExpired')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
  <title>FTD - Single Sign-On</title>
  <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
  <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
  <script type="text/javascript" src="/{$applicationcontext}/security/js/clock.js"></script>
  <script type="text/javascript" src="/{$applicationcontext}/security/js/copyright.js"></script>

  <script language="javascript">
  var context_js = '<xsl:value-of select="$applicationcontext"/>';
  var c_js = '<xsl:value-of select="$context"/>';
  <!--var st_js = '<xsl:value-of select="$securitytoken"/>'; -->
  var id_js = '<xsl:value-of select="$identity"/>';
  var expired_js = '<xsl:value-of select="$passwordExpired"/>';

  <![CDATA[

    var fieldNames = new Array("identity", "credentials");
    function init(){
      setNavigationHandlers();
      addDefaultListenersArray(fieldNames);
      document.forms[0].identity.focus();
    }
    function squelchSpaces(){
      var input = event.keyCode;
      if ( input == 32 )
          event.returnValue = false;
    }

    function doLogin(){
      if ( !validate() ){
//        alert("Please correct indicated fields.");
        return false;
      }
      else {
        //submitForm();
        document.forms[0].action = "/" + context_js + "/security/Login.do";
        return true;
      }
    }

	function changePassword() {
		if (document.LoginForm.identity.value == "") {
			alert("Please enter your userid.");
		}
		else {
			openChangePasswordWindow(document.LoginForm.identity.value);
		}
	}

    function validate(){
      var form = document.forms[0];

      form.identity.className = "default";
      form.credentials.className = "default";

      var login = form.identity.value;
      var pass = form.credentials.value;
      var isValid = true;

      if(login.length <= 0 && pass.length <= 0)
      {
        alert("You must enter User ID and Password to login.");
        return false;
      }

      if (login.length <= 0)
      {
        alert("You must enter User ID to login.");
        return false;
      }
      if (pass.length <= 0){
        alert("You must enter Password to login.");
        return false;
      }

      return isValid;
    }

	function checkExpire() {
		if(expired_js == "true") {
			openChangePasswordWindow(id_js);
		}
	}

	function openChangePasswordWindow(userid) {
		var form = document.forms[0];
		var addr = "/" + context_js + "/security/Login.do"
						+ "?adminAction=displayChangePassword"+ "&context=" + c_js
						+ "&identity=" + userid + "&saaaaaaaaaaaaaaaaaaaaa";

		var modal_dim = "dialogWidth:600px; dialogHeight:350px; center:yes; status=0";
		var ret = window.showModalDialog(addr, "", modal_dim);

		if (ret && ret != null) {
  			form.context.value = ret[0];
		    form.identity.value = ret[1];
		    form.credentials.value = ret[2];
		    submitForm();
		}
	}

    function submitForm(){
      document.forms[0].action = "/" + context_js + "/security/Login.do";
      document.forms[0].submit();
    }
	]]>
  </script>
</head>
<body onload="javascript:init();checkExpire()">
  <form name="LoginForm" method="post">
  	<!-- Testing - take out later -->
  	<input type="hidden" name="context" value="{$context}"/>
    <!-- Header -->
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" src="/{$applicationcontext}/security/images/wwwftdcom_131x32.gif" width="131" height="32"/></td>
        <td width="60%" align="center" colspan="1" class="header">Order Processing System</td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="time" align="right" class="label"></td>
              <script type="text/javascript">startClock();</script>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>

    <!-- Content -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
         <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
          	<tr>
          		<td colspan="2" align="center" class="ErrorMessage"><xsl:value-of select="ERROR/@message"/></td>
          	</tr>
            <tr>
            	<td width="40%" align="right">User:</td>
              	<td><input type="text" name="identity" value="{$identity}" tabindex="1" size="20" maxlength="20" onkeypress="javascript:squelchSpaces();"/></td>
            </tr>
            <tr>
            	<td width="40%" align="right">Password:</td>
              	<td><input type="password" name="credentials" value="{$credentials}" tabindex="2" size="20" maxlength="20" onkeypress="javascript:squelchSpaces();"/></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
 	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  		<tr>
    		<!--td width="42%" align="right"><input type="button" name="loginButton" value="Login" tabindex="3" onclick="javascript:doLogin();"/></td-->
    		<td width="42%" align="right"><input type="submit" name="loginButton" value="Login" tabindex="3" onclick="return doLogin();"/></td>
      		<td align="left"><input type="button" name="reset" value="Change Password" tabindex="4" onclick="javascript:changePassword();"/></td>
		</tr>
    </table>
    <!-- Footer -->
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td>&nbsp;</td></tr>
      <tr>
       	<td class="disclaimer"></td><script>showCopyright();</script>
      </tr>
    </table>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td class="ErrorField" align="center">RELEASE_INFO</td>
      </tr>
	  <tr>
		<td bgcolor="#FF0000"><div align="center"><strong><font color="#FFFFFF">THIS IS A TRAINING AREA ONLY.  DO NOT TAKE REAL CUSTOMER ORDERS ON THIS SYSTEM</font></strong></div></td>
	  </tr>
    </table>
  </form>

</body>
  </html>
</xsl:template>
</xsl:stylesheet>