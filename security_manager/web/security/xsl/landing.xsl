<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/CONTEXTS/security/data" use="name"/>

<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Main</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
 <script language="javascript">
  var context_js = '<xsl:value-of select="$applicationcontext"/>';
  <![CDATA[
  	var servlet_prefix = "/" + context_js + "/security/";
  	
	function goAdmin() {       
		submitForm("Main.do");
	}
	
	function goAccount() {
		document.forms[0].adminAction.value = "account";
		submitForm("Identity.do");
	}
	
	function submitForm(servlet) {
		document.forms[0].action = servlet_prefix + servlet;
		document.forms[0].submit();
	}

	]]>
  </script>
  </head>
  <body>

    <form name="form" method="post">
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="identity" value="{$identity}"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Main'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     
    <table width="100%">
		<tr>
			<td width="50%" align="right"><a href="javascript:goAccount()"><img src="/{$applicationcontext}/security/images/account.gif" border="0"/></a></td>
			<td align="left"><a href="javascript:goAdmin()"><img src="/{$applicationcontext}/security/images/admin.gif" border="0"/></a></td>
		</tr>
    </table>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#006699">
               <tr>
            <td colspan="2">
        	<table width="100%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
					<tr>
						<td class="label">Application</td>
						<td class="label">URL</td>
					</tr>
					<xsl:for-each select="CONTEXTS/CONTEXT[@application]">
                    <tr>
                    	<td><a href="{@url}?securitytoken={$securitytoken}"><xsl:value-of select="@application"/></a></td>
 						<td><xsl:value-of select="@url"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>