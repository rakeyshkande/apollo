<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/ERRORS/security/data" use="name"/>

<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="identity"><xsl:value-of select="key('security','identity')/value" /></xsl:param>

<xsl:template match="/">
<xsl:variable name="error_id" select="ERRORS/ERROR/@code"/>
  <html>
  <head>
    <title>FTD - Error</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
 	<script language="javascript">
  		var context_js = '<xsl:value-of select="$applicationcontext"/>';
  	</script>
  </head>
  <body>
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <!-- need this field for logout from error page -->
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="identity" value="{$identity}"/>
    <input type="hidden" name="user_id" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Error'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="/{$applicationcontext}/security/Main.do?adminAction=view_all&amp;context={$context}&amp;securitytoken={$securitytoken}">Main Menu</a>&nbsp;>
                Error
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="3" cellpadding="1" cellspacing="1" align="center" bordercolor="#006699">
                    <tr>
                        <td>
                        	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        	<tr><td>Your request cannot be processed.</td></tr>
                        	
                            <xsl:call-template name="errorOutput">
                                <xsl:with-param name="errorCode" select="$error_id"/>
                            </xsl:call-template>
              	         	<tr><td><xsl:value-of select="ERRORS/ERROR/@info"/></td></tr>
              	         	<tr><td><xsl:value-of select="ERRORS/ERROR/@other"/></td></tr>
							<tr><td>You can click on "Main Menu" to go to the main menu or click on the "Back" button to resume.</td></tr>
							<tr><td>&nbsp;</td></tr>
							</table>
						</td>
					</tr>

                </table>
            </td>
        </tr>
		<tr>
			<td colspan="3" align="center"><input type="button" class="BlueButton" value="Back" onclick="javascript:history.back()"/></td>
  		</tr>
    </table>  
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>

<xsl:template name="errorOutput">
  <xsl:param name="errorCode"/>
  <xsl:choose>
    <xsl:when test="$errorCode='1'">
      <tr><td>INTERNAL ERROR</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='2'">
      <tr><td>CANNOT PARSE DATE STRING</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='3'">
      <tr><td>CANNOT CREATE DATE OBJECT</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='4'">
      <tr><td>CANNOT OBTAIN DATA SOURCE</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='5'">
      <tr><td>REQUEST UNKNOWN</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='6'">
      <tr><td>DATABASE ERROR</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='7'">
      <tr><td>REQUIRED FIELD(S) MISSING - </td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='8'">
      <tr><td>INVALID INPUT</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='9'">
      <tr><td>IDENTITY EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='10'">
      <tr><td>RESOURCE EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='11'">
      <tr><td>PERMISSION EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='12'">
      <tr><td>DATABASE CONSTRAINT - </td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='13'">
      <tr><td>CONTEXT EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='14'">
      <tr><td>CONTEXT CONFIG EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='15'">
      <tr><td>NOT AUTHORIZED TO PERFORM ACTION</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='16'">
      <tr><td>INVALID OLD PASSWORD</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='17'">
      <tr><td>CREDENTIALS AND RE-RECEDNETIALS DON'T MATCH</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='18'">
      <tr><td>CANNOT LOAD FILE</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='19'">
      <tr><td>ACL NAME EXISTS</td></tr>
    </xsl:when>
    <xsl:when test="$errorCode='20'">
      <tr><td>CONTEXT IS NULL</td></tr>
    </xsl:when>
    <xsl:otherwise>
      <tr><td>INTERNAL ERROR</td></tr>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
</xsl:stylesheet>