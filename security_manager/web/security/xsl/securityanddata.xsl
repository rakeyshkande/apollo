<!DOCTYPE ACDemo[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- The security/data has different parent element. The following xpath makes the selection generic. -->
<xsl:key name="filterdata" match="//security/data" use="name"/>

<xsl:param name="sourceMenu"><xsl:value-of select="key('filterdata','sourceMenu')/value" /></xsl:param>
<xsl:param name="contexttest"><xsl:value-of select="key('filterdata','context')/value" /></xsl:param>
<xsl:output method="html" indent="yes"/>
<xsl:template name="securityanddata">
    <input type="hidden" name="sourceMenu"    id="sourceMenu"  value="{$sourceMenu}"/>
</xsl:template>	
</xsl:stylesheet>
