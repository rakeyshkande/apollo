<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/PERMISSIONS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Permission List</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
    <script language="javascript">
    var removeCount = <xsl:value-of select="count(PERMISSIONS/PERMISSION)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var confirmMsg = "Are you sure you want to remove the permission(s)?";
	
    function init() {
    	setNavigationHandlers();
    }
	function updateButton()
	{
		// If any of the check box is checked, enable its submit button.
		var disableRemove = true;
		for (var i=1; i<=removeCount; i++) {
			if (document.all("removePermissionCB" + i).checked) {
				disableRemove = false;
				break;
			}
		}
		document.forms[0].removeButton.disabled=disableRemove;
		document.forms[0].addButton.disabled=(!disableRemove);
	}
	
	function callAddPermission() {
		//window.location="../security/html/manageACL/CreatePermission.html";
		submitForm("Permission.do", "display_add");
	}
	
	function callRemovePermission() {
		var ids = "";
		for (var i = 1; i <= removeCount; i++) {
			if(document.all("removePermissionCB" + i).checked) {
				ids = ids + "," + i;
			}
		}
	
		document.all("remove_ids").value = ids;

		if (confirm(confirmMsg)) {
			submitForm("Permission.do", "remove");
		}
	}

	function callViewPermission(pos) {
		//document.forms[0].permission_id.value = permissionId;
		document.forms[0].permission_id.value = document.all("permissionId"+pos).value;
		submitForm("Permission.do", "view");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script> 
  
  </head>
  <body onload="javascript: init()">
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="permission_id" value=""/>
    <input type="hidden" name="remove_ids" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Permission List'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Permissions
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td class="colheader" width="5%" align="center">&nbsp;</td>
						<td class="colheader" width="95%">Permission</td>
					</tr>
					
				<xsl:for-each select="PERMISSIONS/PERMISSION">
				<xsl:sort select="permission_id"/>
					<xsl:variable name="permissionId" select="concat('permissionId',position())"/>
					<xsl:variable name="removePermissionCB" select="concat('removePermissionCB',position())"/>
					<input type="hidden" name="{$permissionId}" value="{permission_id}"/>
					<tr>
						<xsl:choose>
						<xsl:when test="@selected='Y'">
						<td width="5%" align="center"><input type="checkbox" name="{$removePermissionCB}" checked="true" onclick="updateButton()"/></td>
						</xsl:when>
						<xsl:otherwise>
						<td width="5%" align="center"><input type="checkbox" name="{$removePermissionCB}" onclick="updateButton()"/></td>
						</xsl:otherwise>
						</xsl:choose>
						<td width="95%"><a class="link" href="javascript:callViewPermission('{position()}')"><xsl:value-of select="permission_id"/></a></td>
					</tr>
					<xsl:if test="string-length(@error)>0">
					<tr><td width="5%">&nbsp;</td>
						<td class="errorMessage"><xsl:value-of select="@error"/></td>
					</tr>
					</xsl:if>
				</xsl:for-each>
    				
    				
   			</table>
            </td>
        </tr>
    </table>  
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" class="BlueButton" name="addButton" value="Add New Permission" onclick="javascript:callAddPermission()"/>
				<input type="button" class="BlueButton" name="removeButton" value="Remove Checked" disabled="true" onclick="javascript:callRemovePermission()"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>