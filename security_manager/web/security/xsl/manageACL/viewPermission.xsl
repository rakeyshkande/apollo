<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/PERMISSIONS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Permission Detail</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
    
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var fieldNames = new Array("description", "updateButton");
    var confirmMsg = "Are you sure you want to remove this permission?";

    function init() {
    	setNavigationHandlers();
    }
    function accessControl(checked){
    	var checkboxText = document.all("accessTD");
    	if (checked){   // Lock fields
        	checkboxText.innerHTML = "Locked";
    	} else {        // Unlock fields
        	checkboxText.innerHTML = "Unlocked";
    	}

    	for (var i = 0; i < fieldNames.length; i++){
        	document.all(fieldNames[i]).disabled = checked;
    	}
	}

	function callRemove() {
		if (confirm(confirmMsg)) {
			submitForm("Permission.do", "remove");
		}
	}
	
	function callUpdate() {
		submitForm("Permission.do", "update");
	}
	
	function callViewAll() {
		submitForm("Permission.do", "view_all");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="remove_ids" value="1"/>
    <input type="hidden" name="permission_id" value="{PERMISSIONS/PERMISSION/permission_id}"/>
    <input type="hidden" name="permissionId1" value="{PERMISSIONS/PERMISSION/permission_id}"/>
    <xsl:call-template name="securityanddata"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Permission Detail'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Permissions</a>&nbsp;>
                Permission
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Permission Id:</td>
						<td><input type="text" name="permissionId" size="10" maxlength="255" value="{PERMISSIONS/PERMISSION/permission_id}" disabled="true"/></td>
					</tr>
					<tr>
						<td width="15%" class="label">Description:</td>
						<td><textarea name="description" cols="30" rows="5" disabled="true">
						<xsl:value-of select="PERMISSIONS/PERMISSION/description"/></textarea></td>
						<td width="35%" align="right" valign="bottom">
						    <table>
						        <tr>
						            <td id="accessTD" class="accesslabel">Locked</td>
						            <td><input type="checkbox" name="accessCheckbox" checked="true" onclick="javascript:accessControl(this.checked);"/></td>
							</tr>
						    </table>
         				</td>
					</tr>
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="updateButton" class="BlueButton" value="Update Permission" disabled="true" onclick="javascript:callUpdate();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>