<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">
  <html>
  <head>
    <title>FTD - Permission Creation</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
	<script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
    <script language="javascript">
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[
    function init() {
    	setNavigationHandlers();
    	setErrorFields();
    }
	function callSave()
	{
		submitForm("Permission.do", "add");
	}

	function callViewAll()
	{
		submitForm("Permission.do", "view_all");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Create Permission'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="99%" border="3" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewAll()">Permissions</a>&nbsp;>
                Create Permission
            </td>
        </tr>
        <tr>
        	<td>
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td width="15%" class="label">Permission Id:<font color="red">&nbsp;***</font></td>
						<td><input type="text" name="permission_id" size="28" maxlength="255" value="{key('field', 'permission_id')/@value}"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="85%" class="ErrorMessage"><xsl:value-of select="key('error', 'permission_id')/@description"/></td>
	                </tr>
					<tr>
						<td width="15%" class="label">Description:</td>
						<td><textarea name="description" cols="30" rows="5"><xsl:value-of select="key('field', 'description')/@value"/></textarea>
						</td>
					</tr>
                </table>
        	</td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
				<input type="submit" value="Save" onclick="callSave()"/>
				<input type="button" value="Cancel" class="BlueButton" onClick="callViewAll()"/>
				<input type="reset" value="Clear"/>
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>