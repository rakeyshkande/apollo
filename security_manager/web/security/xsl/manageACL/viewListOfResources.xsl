<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/RESOURCES/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Resource List</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
  
    <script language="javascript">
    var removeCount = <xsl:value-of select="count(RESOURCES/RESOURCE)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var confirmMsg = "Are you sure you want to remove the resource(s)?";
    
    function init() {
    	setNavigationHandlers();
    }
	function updateButton()
	{
		// If any of the check box is checked, enable its submit button.
		var disableRemove = true;
		for (var i=1; i<=removeCount; i++) {
			if (document.all("removeResourceCB" + i).checked) {
				disableRemove = false;
				break;
			}
		}
		document.forms[0].removeButton.disabled=disableRemove;
		document.forms[0].addButton.disabled=(!disableRemove);
	}
	
	function callAddResource() {
		submitForm("Resource.do", "display_add");
	}
	
	function callRemoveResource() {
		var ids = "";
		for (var i = 1; i <= removeCount; i++) {
			if(document.all("removeResourceCB" + i).checked) {
				ids = ids + "," + i;
			}
		}
	
		document.all("remove_ids").value = ids;

		if (confirm(confirmMsg)) {
			submitForm("Resource.do", "remove");
		}
	}

	function callViewResource(pos) {
		//document.forms[0].context_id.value = contextId;
		//document.forms[0].resource_id.value = resourceId;
		document.forms[0].context_id.value = document.all("contextId"+pos).value;
		document.forms[0].resource_id.value = document.all("resourceId"+pos).value;
		submitForm("Resource.do", "view");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script> 
  
  </head>
  <body onload="javascript: init()">
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <!--input type="hidden" name="isExit" value=""/-->
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="resource_id" value=""/>
    <input type="hidden" name="context_id" value=""/>
    <input type="hidden" name="remove_ids" value=""/>
    <xsl:call-template name="securityanddata"/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Resource List'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Resources
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td class="colheader">Remove</td>
						<td class="colheader">Resource</td>
						<td class="colheader">Context</td>
					</tr>
				<xsl:for-each select="RESOURCES/RESOURCE">
				<xsl:sort select="resource_id"/>
					<xsl:variable name="resourceId" select="concat('resourceId',position())"/>
					<xsl:variable name="contextId" select="concat('contextId',position())"/>
					<xsl:variable name="removeResourceCB" select="concat('removeResourceCB',position())"/>
					<input type="hidden" name="{$resourceId}" value="{resource_id}"/>
					<input type="hidden" name="{$contextId}" value="{context_id}"/>
					<tr>
						<xsl:choose>
						<xsl:when test="@selected='Y'">
						<td width="10%" align="center"><input type="checkbox" name="{$removeResourceCB}" checked="true" onclick="updateButton()"/></td>
						</xsl:when>
						<xsl:otherwise>
						<td width="10%" align="center"><input type="checkbox" name="{$removeResourceCB}" onclick="updateButton()"/></td>
						</xsl:otherwise>
						</xsl:choose>
						<td width="45%"><a class="link" href="javascript:callViewResource('{position()}')"><xsl:value-of select="resource_id"/></a></td>
						<td width="45%">
						<xsl:value-of select="context_id"/>
						</td>
					</tr>
					<xsl:if test="string-length(@error)>0">
					<tr><td width="5%">&nbsp;</td>
						<td class="errorMessage"><xsl:value-of select="@error"/></td>
					</tr>
					</xsl:if>
				</xsl:for-each>
                       
   			</table>
            </td>
        </tr>
    </table>  
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" name="addButton" class="BlueButton" value="Add New Resource" onclick="javascript:callAddResource()"/>
				<input type="button" name="removeButton" class="BlueButton" value="Remove Checked" disabled="true" onclick="javascript:callRemoveResource()"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>