<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/ROOT/security/data" use="name"/>


<xsl:key name="field" match="ROOT/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="ROOT/ERRORS/ERROR" use="@fieldname"/>
<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/ROOT">

  <html>
  <head>
    <title>FTD - Resource Group Creation</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var num_resource = <xsl:value-of select="count(CONTEXTS/CONTEXT/RESOURCES/RESOURCE)"/>;
    var perms = <xsl:value-of select="count(CONTEXTS/CONTEXT/RESOURCES/RESOURCE/PERMISSIONS/PERMISSION)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);
    <![CDATA[
    var num_perm = perms/num_resource;
    
    function init() {
    	setNavigationHandlers();
    	setErrorFields();
    	initAddButton();
    }
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
	function callCreate(){
		if(isValidName()) {
			// determine which resource checkboxes are checked.
			var rc = document.forms[0].rCount.value;
			var pc = document.forms[0].pCount.value;
			var rids = "";
			
			for (var i=1; i<=rc; i++) {
			
				if(document.all("resourceCB"+i).checked) {
				
					rids = rids + "," + i;
					var pids = "";
					
					// determine which permission checkboxes are checked.
					for( var j=1; j<=pc; j++) {
						if (document.all("resourceCB"+i+"permissionCB"+j).checked) {
							pids = pids + "," + j;
						}
					}
					document.all("resourceId"+i+"perm_ids").value = pids;
					
				}
				
			}
			document.forms[0].resource_ids.value = rids;
		
			submitForm("ACL.do", "add");
   		} else {
	    	alert("ACL Name cannot contain special characters.");
	    }
	}
	
	function isValidName() {
		var apos = "'";
		var quote = "\"";
		var lt = "<";
		var gt = ">";
		var comma = ",";
		var amp = "&";

		var name = document.all("acl_name").value;

		return (name.indexOf(apos)== -1
				&& name.indexOf(quote) == -1
				&& name.indexOf(lt) == -1
				&& name.indexOf(gt) == -1
				&& name.indexOf(comma) == -1
				&& name.indexOf(amp) == -1);
	}
	
	function callCancel() {
		submitForm("ACL.do", "view_all");
	}
	
	function callChangeContext() {
		//document.forms[0].context_id.value = document.forms[0].contextId.value;
		submitForm("ACL.do", "display_add");

	}
	
	function releasePerm(cbName, numResource, numPerm) {
		//document.forms[0].rCount.value = numResource;
		//document.forms[0].pCount.value = numPerm;
		
		for(var i=1; i<=numPerm; i++) {
			document.all(cbName.name+"permissionCB"+i).disabled = !cbName.checked;
		}
	    // Disable create button once a resource box is checked
    	// The create button can only be enabled by the permission box.
    	if (cbName.checked) {
        	document.all("addButton").disabled=cbName.checked;
    	}
    	else {
    		// uncheck all related permission boxes.
    		for (var i=1; i<=numPerm; i++) {
    			document.all(cbName.name+"permissionCB"+i).checked = false;
    		}
    	
    		// When a resource box is unchecked, check if all rows are valid.
        	document.all("addButton").disabled=(!areRowsValid(numResource,numPerm));
    	}
	}
	
	// If any permission checkbox is checked, check if all rows are valid.
	function updateCreateBT(numResource, numPerm) {
    	document.all("addButton").disabled=(!areRowsValid(numResource, numPerm));
	}
	
	function areRowsValid(numResource, numPerm) {
	// A row is valid if either it's not check at all
	// or the resource and at least one permission is checked.
    	var valid=true;
    	var hasAtLeastOneRow=false;

    	for(var i=1; i<=numResource; i++) {
        	var permBase="resourceCB" + i +"permissionCB"; //base name of permission checkbox
        	var rowValid=false;

        	if(document.all("resourceCB"+i).checked) {
            	hasAtLeastOneRow=true;
            	for(var j=1; j<=numPerm; j++) {
                	if(document.all(permBase+j).checked) {
                    	rowValid=true;
                    	break;
                	}
            	}
        	}
        	else {
            	rowValid=true;
        	}
        	if(!rowValid) {
            	valid=false;
            	break;
        	}
    	}
    	return (valid&&hasAtLeastOneRow);
	}
	
	function initAddButton() {
		if(!areRowsValid(num_resource, num_perm)) {
			document.forms[0].addButton.disabled = true;
		}
	}
	
   ]]>
   </script>

  </head>
  <body onload="init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="rCount" value="{count(CONTEXTS/CONTEXT/RESOURCES/RESOURCE)}"/>
    <input type="hidden" name="pCount" value="{count(CONTEXTS/CONTEXT/RESOURCES/RESOURCE/PERMISSIONS/PERMISSION) div count(CONTEXTS/CONTEXT/RESOURCES/RESOURCE)}"/>
    <input type="hidden" name="resource_ids" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Create Resource Group'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Create Resource Group
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="25%" class="label">Context Id:<font color="red">&nbsp;***</font></td>
						<td>
                        <select name="context_id" onchange="javascript:callChangeContext()">
                        
						<xsl:for-each select="CONTEXTS/CONTEXT">
						<xsl:sort select="context_id"/>
						<xsl:choose>
							<xsl:when test="count(RESOURCES)>0">
								<option value="{context_id}" selected="true"><xsl:value-of select="context_id"/></option>
   							</xsl:when>
   							<xsl:otherwise>
   								<option value="{context_id}"><xsl:value-of select="context_id"/></option>
   							</xsl:otherwise>
   						</xsl:choose>
   						</xsl:for-each>
	 					</select>
						</td>
					</tr>
					<tr>
						<td width="25%" class="label">Resource Group Name:<font color="red">&nbsp;***</font></td>
						<td><input type="text" name="acl_name" size="30" maxlength="255" value="{key('field', 'acl_name')/@value}"/></td>
					</tr>
	                <tr>
	                  <td width="15%"></td>
	                  <td width="85%" class="ErrorMessage"><xsl:value-of select="key('error', 'acl_name')/@description"/></td>
	                </tr>
                    <tr>
                        <td class="colheader">Resource<font color="red">&nbsp;***</font></td>
                        <td class="colheader">Permission<font color="red">&nbsp;***</font></td>
                    </tr>
                    
                    
                    
                    <xsl:for-each select="CONTEXTS/CONTEXT/RESOURCES[count(RESOURCE)>0]">
                    <xsl:variable name="resourceCount" select="count(RESOURCE)"/>
                    <xsl:for-each select="RESOURCE">
                    <xsl:sort select="resource_id"/>
                    <xsl:variable name="permissionCount" select="count(PERMISSIONS/PERMISSION)"/>
                    <xsl:variable name="resourceCB" select="concat('resourceCB',position())"/>
                    <xsl:variable name="resourceId" select="concat('resourceId',position())"/>
                    <xsl:variable name="permission" select="concat($resourceCB, 'permissionCB')"/>
                    <xsl:variable name="permissionIds" select="concat($resourceId, 'perm_ids')"/>
                    <input type="hidden" name="{$permissionIds}" value=""/>
                    <input type="hidden" name="{$resourceId}" value="{resource_id}"/>
                    <tr>
                    	<xsl:choose>
                    	<xsl:when test="@selected='Y'">
                        <td class="Wrap"><input type="checkbox" name="{$resourceCB}" value="{$resourceCB}" checked="true" onclick="releasePerm(this, {$resourceCount}, {$permissionCount})"/>
							<xsl:value-of select="resource_id"/></td>
                        <td class="Wrap">
                        	<xsl:for-each select="PERMISSIONS/PERMISSION">
                        	<xsl:sort select="permission_id"/>
                        	<xsl:variable name="permissionCB" select="concat($permission,position())"/>
                        	<xsl:choose>
                    		<xsl:when test="@selected='Y'">
                            <input type="checkbox" name="{$permissionCB}" value="{permission_id}" checked="true" onclick="updateCreateBT({$resourceCount}, {$permissionCount})"/>
                            <xsl:value-of select="permission_id"/>&nbsp; &nbsp;
							</xsl:when>
							<xsl:otherwise>
							<input type="checkbox" name="{$permissionCB}" value="{permission_id}" onclick="updateCreateBT({$resourceCount}, {$permissionCount})"/>
                            <xsl:value-of select="permission_id"/>&nbsp; &nbsp;
							</xsl:otherwise>
							</xsl:choose>
							</xsl:for-each>
                        </td>
						</xsl:when>
						
						<xsl:otherwise>
						<td class="Wrap"><input type="checkbox" name="{$resourceCB}" value="{$resourceCB}" onclick="releasePerm(this, {$resourceCount}, {$permissionCount})"/>
							<xsl:value-of select="resource_id"/></td>
                        <td class="Wrap">
                        	<xsl:for-each select="PERMISSIONS/PERMISSION">
                        	<xsl:sort select="permission_id"/>
                        	<xsl:variable name="permissionCB" select="concat($permission,position())"/>
                        	<xsl:choose>
                    		<xsl:when test="@selected='Y'">
                            <input type="checkbox" name="{$permissionCB}" value="{permission_id}" disabled="true" checked="true" onclick="updateCreateBT({$resourceCount}, {$permissionCount})"/>
                            <xsl:value-of select="permission_id"/>&nbsp; &nbsp;
							</xsl:when>
							<xsl:otherwise>
							<input type="checkbox" name="{$permissionCB}" value="{permission_id}" disabled="true" onclick="updateCreateBT({$resourceCount}, {$permissionCount})"/>
                            <xsl:value-of select="permission_id"/>&nbsp; &nbsp;
							</xsl:otherwise>
							</xsl:choose>
							</xsl:for-each>
                        </td>
						</xsl:otherwise>
						</xsl:choose>
						

                    </tr>
                    </xsl:for-each>
                    </xsl:for-each>
                    
                    
                </table>
            </td>
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="15%" class="Required">&nbsp;&nbsp;***&nbsp;Required Field</td>
        	<td align="center">
                <input type="button" name="addButton" class="BlueButton" value="Save" onClick="javascript:callCreate();"/>
                <input type="button" name="cancelButton" class="BlueButton" value="Cancel" onClick="javascript:callCancel();"/>
                <!--input type="reset" value="Clear"/-->
        	</td>
			<td width="15%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>