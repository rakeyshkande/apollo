<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/CONTEXTS/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
  <html>
  <head>
    <title>FTD - Access Control List</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>
  
    <script language="javascript">
    var removeCount = <xsl:value-of select="count(CONTEXTS/CONTEXT/ACLS/ACL)"/>;
    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[
    var confirmMsg = "Are you sure you want to remove the ACL(s)?";
    var array = new Array();
    
    function init() {
    	setNavigationHandlers();
    }
 	function addElement(addvalue) {
		var size = array.length;
		array[size] = addvalue;
		array.length = size + 1;
	}
	
	function deleteElement(value) {
		var size = array.length;
		var delindex = 0;
		var found = false;
		
		// look for value
		for(var i=0; i<size; i++) {
			var val = array[i];
			if (val == value) {
				delindex = i;
				found = true;
			}
		}
		
		// move value up
		if(found) {
			for (var j=delindex; j<size-1; j++) {
				if (j != size) {
					array[j] = array[j+1];
				}
			}
		}
		array.length = size-1;
	}

	function updateButton(cb, id)
	{
		var add = true;
		if (cb.checked) {
			addElement(id);
		}
		else {
			deleteElement(id);
		}
		if (array.length > 0 ) {
			add = false;
		}
		else {
			add = true;
		}
		document.forms[0].removeButton.disabled = add;
		document.forms[0].addButton.disabled = !add;
	}
	
	function callAddAcl() {
		submitForm("ACL.do", "display_add");
	}
	
	function callRemoveAcl() {
		var ids = "";
	
		for(var i=0; i<array.length; i++) {
			ids = ids + "," + array[i];
		}
	
		document.all("remove_ids").value = ids;

		if (confirm(confirmMsg)) {
			submitForm("ACL.do", "remove");
		}
		
	}

	function callViewAcl(aclName) {
		document.forms[0].acl_name.value = aclName;
		//document.forms[0].context_id.value = contextId;
		submitForm("ACL.do", "view");
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script> 
  
  </head>
  <body onload="javascript: init()">
  
    <form name="form" method="post">
    <input type="hidden" name="adminAction" value="view"/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="acl_name" value=""/>
    <input type="hidden" name="context_id" value=""/>
    <input type="hidden" name="remove_ids" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Resource Groups'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                Resource Groups
            </td>
        </tr>
        <tr>
            <td>
        	<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td class="colheader">Remove</td>
						<td class="colheader">Resource Group Name</td>
						<td class="colheader">Context</td>
					</tr>
				<xsl:for-each select="CONTEXTS/CONTEXT[count(ACLS/ACL)>0]">
				<xsl:sort select="context_id"/>
				<xsl:variable name="thisContextId" select="context_id"/>
					<xsl:for-each select="ACLS/ACL">
					<xsl:variable name="aclName" select="concat('aclName', position())"/>
					<xsl:variable name="removeAclCB" select="concat('removeAclCB', position())"/>
					<tr>
						<td width="10%" align="center"><input type="checkbox" name="{$removeAclCB}" onclick="updateButton(this, '{@acl_name}')"/></td>
						<td width="45%"><a class="link" href="javascript:document.all('context_id').value='{$thisContextId}';callViewAcl('{@acl_name}')"><xsl:value-of select="@acl_name"/></a></td>
						<td width="45%">
						<xsl:value-of select="$thisContextId" />
						</td>
					</tr>
					</xsl:for-each>
				</xsl:for-each>
                       
   			</table>
            </td>
        </tr>
    </table>  
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
				<input type="button" class="BlueButton" name="addButton" value="Add New Resource Group" onclick="javascript:callAddAcl()"/>
				<input type="button" class="BlueButton" name="removeButton" value="Remove Checked" disabled="true" onclick="javascript:callRemoveAcl()"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" class="BlueButton" name="MainMenu" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>