<!DOCTYPE xsl:stylesheet[
    <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../header.xsl"/>
<xsl:import href="../footer.xsl"/>
<xsl:import href="../securityanddata.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:key name="security" match="/CONTEXT/security/data" use="name"/>


<xsl:param name="context"><xsl:value-of select="key('security','context')/value" /></xsl:param>
<xsl:param name="applicationcontext"><xsl:value-of select="key('security','applicationcontext')/value" /></xsl:param>
<xsl:param name="securitytoken"><xsl:value-of select="key('security','securitytoken')/value" /></xsl:param>

<xsl:template match="/">
<xsl:variable name="nowNumRes" select="count(CONTEXT/RESOURCES/RESOURCE[string-length(acl_name)>0])"/>
  <html>
  <head>
    <title>FTD - Resource Group View</title>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/security/css/ftdAdmin.css"/>
    <script type="text/javascript" src="/{$applicationcontext}/security/js/util.js"></script>

    <script language="javascript">
    var numResource = <xsl:value-of select="count(CONTEXT/RESOURCES/RESOURCE)"/>;
    var perms = <xsl:value-of select="count(CONTEXT/RESOURCES/RESOURCE/PERMISSIONS/PERMISSION)"/>;
	var context_js = '<xsl:value-of select="$applicationcontext"/>';
    <![CDATA[

    var numPerm = perms/numResource;
    function init() {
    	setNavigationHandlers();
    }
	function callViewList() {
		submitForm("ACL.do", "view_all");
	}
	
	function callUpdate(){
		// determine which resource checkboxes are checked.
		//var rids = document.forms[0].resource_ids.value;
		var rids = "";
		
		for (var i=1; i<=numResource; i++) {
		
			if(document.all("resourceCB"+i).checked) {
			
				rids = rids + "," + i;
				var pids = "";
				
				// determine which permission checkboxes are checked.
				for( var j=1; j<=numPerm; j++) {
					if (document.all("resourceCB"+i+"permissionCB"+j).checked) {
						pids = pids + "," + j;
					}
				}
				
				document.all("resourceId"+i+"perm_ids").value = pids;
			}
			
		}
		document.forms[0].resource_ids.value = rids;
		submitForm("ACL.do", "update");
		
	}
	
	function callCancel() {
		submitForm("ACL.do", "view_all");
	}
	
	function releasePerm(cbName) {
	
		for(var i=1; i<=numPerm; i++) {
			document.all(cbName.name+"permissionCB"+i).disabled = !cbName.checked;
		}
	    // Disable create button once a resource box is checked
    	// The create button can only be enabled by the permission box.
    	if (cbName.checked) {
        	document.all("updateButton").disabled=cbName.checked;
    	}
    	else {
    		// uncheck all related permission boxes.
    		for (var i=1; i<=numPerm; i++) {
    			document.all(cbName.name+"permissionCB"+i).checked = false;
    		}
    	
    		// When a resource box is unchecked, check if all rows are valid.
        	document.all("updateButton").disabled=(!areRowsValid());
    	}
    	
	}
	
	// If any permission checkbox is checked, check if all rows are valid.
	function updateBT() {
    	document.all("updateButton").disabled=(!areRowsValid());
	}
	
	function areRowsValid() {
	// A row is valid if either it's not check at all
	// or the resource and at least one permission is checked.
    	var valid=true;
    	var hasAtLeastOneRow=false;

    	for(var i=1; i<=numResource; i++) {
        	var permBase="resourceCB" + i +"permissionCB"; //base name of permission checkbox
        	var rowValid=false;

        	if(document.all("resourceCB"+i).checked) {

            	hasAtLeastOneRow=true;
            	for(var j=1; j<=numPerm; j++) {

                	if(document.all(permBase+j).checked) {
                    	rowValid=true;
                    	break;
                	}
            	}
        	}
        	else {
            	rowValid=true;
        	}
        	if(!rowValid) {
            	valid=false;
            	break;
        	}
    	}

    	return (valid&&hasAtLeastOneRow);
    	
	}

	function accessControl(checked, rNum){
		// rNum is the number of existing resources
    	var checkboxText = document.all("accessTD");
    	if (checked){   // Lock fields
        	checkboxText.innerHTML = "Locked";
    	} else {        // Unlock fields
        	checkboxText.innerHTML = "Unlocked";
    	}
	
		for (var i=1; i<=numResource; i++) {
			
			document.all("resourceCB" + i).disabled = checked;
			for (var j=1; j<=numPerm; j++) {
				document.all("resourceCB" + i + "permissionCB" + j).disabled = checked;
				// always disable new resource permissions
				if (i > rNum) {
					document.all("resourceCB" + i + "permissionCB" + j).disabled = true;
				}
			}
			
		}
    	document.all("updateButton").disabled=checked;
	}
	
	function submitForm(servlet, act){
	    document.forms[0].action = "/" + context_js + "/security/" + servlet;
	    document.forms[0].adminAction.value = act;
		document.forms[0].submit();
	}
	function goMain() {
		submitForm("Main.do", "securityAdministration");
	}

	function goSystemAdmin() {
		submitForm("Main.do", "systemAdministration");
	}
   ]]>
   </script>

  </head>
  <body onload="javascript: init()">

    <form name="form" method="post">
    <input type="hidden" name="adminAction" value=""/>
    <input type="hidden" name="context" value="{$context}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="context_id" value="{CONTEXT/@context_id}"/>
    <input type="hidden" name="acl_name" value="{CONTEXT/@acl_name}"/>
    <input type="hidden" name="rCount" value=""/>
    <input type="hidden" name="resource_ids" value=""/>
     <xsl:call-template name="header">
       <xsl:with-param name="headerName" select="'Resource Group Detail'"/>
       <xsl:with-param name="applicationcontext" select="$applicationcontext"/>
     </xsl:call-template>
     <xsl:call-template name="securityanddata"/>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="right">
				<input type="button" name="MainMenu" Value="Exit" class="BlueButton" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    	<tr>
            <td class="BreadCrumbs">
                <a class="BreadCrumbsLink" href="javascript:goMain()">Security Administration</a>&nbsp;>
                <a class="BreadCrumbsLink" href="javascript:callViewList()">Resource Groups</a>&nbsp;>
                Resource Group
            </td>
        </tr>
        <tr>
            <td>
        		<table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
					<tr>
						<td width="15%" class="label">Context Id:</td>
						<td>
                        <select name="contextId" onchange="javascript:callChangeContext()">
                        <option value="{CONTEXT/@context_id}" selected="true"><xsl:value-of select="CONTEXT/@context_id"/></option>
	 					</select>
						</td>
					</tr>
					<tr>
						<td width="15%" class="label">Resource Group Name:</td>
						<td><input type="text" name="acl_name" size="30" maxlength="255" value="{CONTEXT/@acl_name}" disabled="true"/></td>
						<td id="accessTD" width="15%" align="right" class="accesslabel">Locked</td>
                        <td width="5%"><input type="checkbox" name="accessCheckbox" checked="true" onclick="accessControl(this.checked, '{$nowNumRes}');"/></td>
					</tr>
     			</table>

                <!-- Resource and Permission Information -->

                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">Existing Resource and Permission</td>
                    </tr>
                    <tr>
                        <td class="colheader">Resource</td>
                        <td class="colheader">Permission</td>
                    </tr>
  
                    <xsl:variable name="resourceCount" select="count(CONTEXT/RESOURCES/RESOURCE)"/>
					
                    <xsl:for-each select="CONTEXT/RESOURCES/RESOURCE[string-length(acl_name)>0]">
					<!--xsl:for-each select="CONTEXT/RESOURCES/RESOURCE"-->
					<xsl:sort select="acl_name"/>
                    <xsl:variable name="permissionCount" select="count(PERMISSIONS/PERMISSION)"/>
                    <xsl:variable name="resourceCB" select="concat('resourceCB',position())"/>
                    <xsl:variable name="resourceId" select="concat('resourceId',position())"/>
                    <xsl:variable name="permission" select="concat($resourceCB, 'permissionCB')"/>
                    <xsl:variable name="permissionIds" select="concat($resourceId, 'perm_ids')"/>
                    <input type="hidden" name="{$permissionIds}" value=""/>
                    <input type="hidden" name="{$resourceId}" value="{resource_id}"/>

                    <tr>
                        <td class="Wrap"><input type="checkbox" name="{$resourceCB}" value="{resource_id}" checked="true" disabled="true" onclick="releasePerm(this)"/>
							<xsl:value-of select="resource_id"/></td>
                        <td class="Wrap">
                        	<xsl:for-each select="PERMISSIONS/PERMISSION">
                        	<xsl:sort select="permission_id"/>
                        	<xsl:variable name="permissionCB" select="concat($permission,position())"/>
                        	<!--xsl:variable name="aclID" select="concat($permissionCB,'acl_id')"/-->
                        	<xsl:choose>
                        		<xsl:when test="string-length(@acl_name)>0">
                            		<input type="checkbox" name="{$permissionCB}" value="{permission_id}" checked="true" disabled="true" onclick="updateBT()"/>
                            		<!--input type="hidden" name="{$aclID}" value="{@acl_id}"/-->
                            		<xsl:value-of select="permission_id"/>&nbsp; &nbsp;
								</xsl:when>
								<xsl:otherwise>
			                  		<input type="checkbox" name="{$permissionCB}" value="{permission_id}" disabled="true" onclick="updateBT()"/>
                            		<xsl:value-of select="permission_id"/>&nbsp; &nbsp;
								</xsl:otherwise>
							</xsl:choose>
							</xsl:for-each>
                        </td>
                    </tr>
      
                    </xsl:for-each>

                </table>

                <!-- Available Resource and Permission -->

                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="tblHeader" colspan="4">Available Resource and Permission</td>
                    </tr>
                    <tr>
                        <td class="colheader">Resource</td>
                        <td class="colheader" colspan="3">Permission</td>
       				</tr>
                    <xsl:for-each select="CONTEXT/RESOURCES/RESOURCE[string-length(acl_name)=0]">
					<xsl:sort select="acl_name"/>
                    <xsl:variable name="permissionCount" select="count(PERMISSIONS/PERMISSION)"/>
                    <xsl:variable name="pos" select="position() + $nowNumRes"/>
                    <xsl:variable name="resourceCB" select="concat('resourceCB',$pos)"/>
                    <xsl:variable name="resourceId" select="concat('resourceId',$pos)"/>
                    <xsl:variable name="permission" select="concat($resourceCB, 'permissionCB')"/>
                    <xsl:variable name="permissionIds" select="concat($resourceId, 'perm_ids')"/>
                    <input type="hidden" name="{$permissionIds}" value=""/>
                    <input type="hidden" name="{$resourceId}" value="{resource_id}"/>
                    <tr>
                        <td class="Wrap"><input type="checkbox" name="{$resourceCB}" value="{resource_id}" disabled="true" onclick="releasePerm(this)"/>
							<xsl:value-of select="resource_id"/></td>
                        <td class="Wrap">
                        	<xsl:for-each select="PERMISSIONS/PERMISSION">
							<xsl:sort select="permission_id"/>
                        	<xsl:variable name="permissionCB" select="concat($permission,position())"/>
                            <input type="checkbox" name="{$permissionCB}" value="{permission_id}" disabled="true" onclick="updateBT()"/>
                            <xsl:value-of select="permission_id"/>&nbsp; &nbsp;
							</xsl:for-each>
                        </td>
                    </tr>
                    </xsl:for-each>
                </table>
  
            </td>
            
        </tr>
    </table>
	<table width="99%" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="5%">&nbsp;</td>
        	<td align="center">
                <input type="button" class="BlueButton" name="updateButton" value="Update"  disabled="true" onClick="javascript:callUpdate();"/>
                <input type="button" class="BlueButton" name="cancelButton" value="Cancel" onClick="javascript:callCancel();"/>
        	</td>
			<td width="5%" align="right">
				<input type="button" name="MainMenu" class="BlueButton" Value="Exit" onclick="javascript: goMain();"/>
			</td>
		</tr>
	</table>
    </form>
    <xsl:call-template name="footer"/>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>