#!/bin/bash
export JAVA_HOME=/u01/app/oracle/product/10gR3/jdk
export ACTIVEMQ_HOME=/u02/activemq/apache-activemq-5.1.0

SUNJMX_BASE="-Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"
SUNJMX_NODE1="-Dcom.sun.management.jmxremote.port=61688 $SUNJMX_BASE"
SUNJMX_NODE2="-Dcom.sun.management.jmxremote.port=61689 $SUNJMX_BASE"

export SUNJMX=$SUNJMX_NODE1
./bin/activemq xbean:file:./conf/node1.xml > node1.out &

sleep 10
tail node1.out

export SUNJMX=$SUNJMX_NODE2
./bin/activemq xbean:file:./conf/node2.xml > node2.out &

sleep 10
tail node2.out
