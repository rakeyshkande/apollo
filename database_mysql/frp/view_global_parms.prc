drop procedure if exists frp.view_global_parms;

delimiter $$

create procedure frp.view_global_parms ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning global parm data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
SQL SECURITY INVOKER
  BEGIN
    SELECT context,
    	   name,
    	   description,
    	   value
      FROM frp.global_parms
     ORDER BY context, name;
  END;
$$

delimiter ;

