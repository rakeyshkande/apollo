DROP FUNCTION IF EXISTS frp.get_db_name;

delimiter $$

CREATE DEFINER='frp'@'%' FUNCTION frp.`get_db_name`(
) RETURNS varchar(1000)
BEGIN

     RETURN 'MYSQL';
END;
$$

delimiter ;
