DROP FUNCTION IF EXISTS frp.get_global_parm_value;

delimiter $$

CREATE DEFINER='frp'@'%' FUNCTION frp.`get_global_parm_value`(
IN_CONTEXT      varchar(100),
IN_PARAM        varchar(100)
) RETURNS varchar(1000)
BEGIN
DECLARE v_value varchar(1000);
     SELECT value
       INTO v_value
       FROM frp.global_parms
      WHERE context = IN_CONTEXT
        AND name = IN_PARAM;

     RETURN v_value;
END;
$$

delimiter ;
