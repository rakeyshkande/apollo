DROP FUNCTION IF EXISTS frp.is_db_up;

delimiter $$

CREATE DEFINER='frp'@'%' FUNCTION frp.`is_db_up`(
) RETURNS varchar(1000)
BEGIN
DECLARE v_value varchar(1);
     SELECT 'Y'
       INTO v_value
       ;

     RETURN v_value;
END;
$$

delimiter ;
