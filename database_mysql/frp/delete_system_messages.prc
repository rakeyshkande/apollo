DROP PROCEDURE IF EXISTS frp.delete_system_messages;

delimiter $$

CREATE DEFINER='frp'@'%' PROCEDURE frp.`delete_system_messages`(
IN  IN_SYSTEM_MESSAGE_ID       INT,
OUT OUT_STATUS                 varchar(100),
OUT OUT_MESSAGE                varchar(100)
)
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
      SET OUT_STATUS ='N';
      SET OUT_MESSAGE = 'SQL Exception occured in frp.delete_system_messages'; 
    END;

  SET OUT_STATUS = 'Y';
  SET OUT_MESSAGE = '';
  
  DELETE 
    FROM SYSTEM_MESSAGES
   WHERE SYSTEM_MESSAGE_ID = IN_SYSTEM_MESSAGE_ID;

END;
$$

delimiter ;

