DROP PROCEDURE IF EXISTS frp.save_cache_stats;

delimiter $$

CREATE DEFINER='frp'@'%' PROCEDURE frp.save_cache_stats
(
  IN    IN_NODE_NAME         varchar(100),
  IN    IN_APP_NAME          varchar(200),         
  IN    IN_CACHE_NAME        varchar(50),   
  IN    IN_CACHE_TYPE        varchar(20),
  IN    IN_PRELOAD_FLAG      varchar(1),  
  IN    IN_ACCESS_COUNT      integer, 
  IN    IN_REFRESH_COUNT     integer, 
  IN    IN_REFRESH_RATE      integer, 
  IN    IN_EXPIRE_RATE       integer,  
  IN    IN_EXPIRE_CHECK_RATE integer, 
  IN    IN_FIRST_LOAD_TIME   datetime,
  IN    IN_LAST_REFRESH_TIME datetime, 
  OUT  OUT_STATUS           varchar(100),
  OUT  OUT_MESSAGE          varchar(100)
)
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting application cache use statistics
        into cache_statistics table

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      SET OUT_STATUS ='N';
      SET OUT_MESSAGE = 'SQL Exception occured in frp.save_cache_stats' ; 
    END;
  
    UPDATE  frp.cache_statistics
    SET     node_name = IN_NODE_NAME,
            app_name = IN_APP_NAME,
            cache_name = IN_CACHE_NAME, 
            cache_type = IN_CACHE_TYPE,
            preload_flag = IN_PRELOAD_FLAG,
            access_count = IN_ACCESS_COUNT,
            refresh_count = IN_REFRESH_COUNT,
            refresh_rate = IN_REFRESH_RATE,
            expire_rate = IN_EXPIRE_RATE,
            expire_check_rate = IN_EXPIRE_CHECK_RATE,
            first_load_time = IN_FIRST_LOAD_TIME,
            last_refresh_time = IN_LAST_REFRESH_TIME,
            updated_on = now()
     WHERE  node_name = IN_NODE_NAME 
     AND    app_name = IN_APP_NAME
     AND    cache_name = IN_CACHE_NAME;
        
     IF (ROW_COUNT() = 0) THEN
            INSERT INTO frp.cache_statistics
            (
                    node_name,
                    app_name,
                    cache_name, 
                    cache_type,
                    preload_flag,
                    access_count,
                    refresh_count,
                    refresh_rate,
                    expire_rate,
                    expire_check_rate,
                    first_load_time,
                    last_refresh_time,
                    updated_on
            )
            VALUES
            (
                    IN_NODE_NAME,
                    IN_APP_NAME,
                    IN_CACHE_NAME, 
                    IN_CACHE_TYPE,
                    IN_PRELOAD_FLAG,
                    IN_ACCESS_COUNT,
                    IN_REFRESH_COUNT,
                    IN_REFRESH_RATE,
                    IN_EXPIRE_RATE,
                    IN_EXPIRE_CHECK_RATE,
                    IN_FIRST_LOAD_TIME,
                    IN_LAST_REFRESH_TIME,
                    now()
            );
      END IF;
        
      SET OUT_STATUS = 'Y';
      SET OUT_MESSAGE = '';
END;    
$$

delimiter ;

