DROP PROCEDURE IF EXISTS frp.insert_system_messages;

delimiter $$

CREATE DEFINER='frp'@'%' PROCEDURE frp.`insert_system_messages`(
IN  IN_SOURCE                         varchar(100),
IN  IN_TYPE                           varchar(100),
IN  IN_MESSAGE                        varchar(2000),
IN  IN_COMPUTER                       varchar(256),
IN  IN_EMAIL_SUBJECT                  varchar(100),
OUT RegisterOutParameterMessageId     varchar(100),
OUT RegisterOutParameterStatus        varchar(100),
OUT RegisterOutParameterMessage       varchar(100)
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
      SET RegisterOutParameterStatus ='N';
      SET RegisterOutParameterMessage = 'SQL Exception occured in frp.insert_system_messages'; 
    END;

  SET RegisterOutParameterStatus = 'Y';
  SET RegisterOutParameterMessage = '';
  
  INSERT INTO system_messages (source,                      type,    message,    timestamp, computer,    email_subject)
                       VALUES (IFNULL(IN_SOURCE,'unknown'), IN_TYPE, IN_MESSAGE, now(),     IN_COMPUTER, IN_EMAIL_SUBJECT);
  Set RegisterOutParameterMessageId = LAST_INSERT_ID();

END;
$$

delimiter ;

