DROP PROCEDURE IF EXISTS pas.is_any_florist_available;

delimiter $$

CREATE PROCEDURE pas.`is_any_florist_available`
(
   in_zip_code             varchar(100),
   in_delivery_date        DateTime
)
SQL SECURITY INVOKER
BEGIN

   DECLARE v_result varchar(1);
   DECLARE v_florist_date    datetime;

   select distinct pz.delivery_date
   from (((pas.pas_florist_zipcode_dt pz 
   join ftd_apps.zip_code zc) 
   join ftd_apps.state_master sm) 
   join pas.pas_timezone_dt pt) 
   where (1 = 1)
   and pz.zip_code_id = IN_ZIP_CODE
   and pz.delivery_date = IN_DELIVERY_DATE
   and (
       case
       when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
           DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > 0 or
           (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = 0 and
               DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (pz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
       else
           DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > 0 or
           (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = 0 and
               (pz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
       end
	  )
   and (pt.time_zone_code = sm.time_zone) 
   and (pt.delivery_date = pz.delivery_date) 
   and (sm.state_master_id = zc.state_id) 
   and (zc.zip_code_id = pz.zip_code_id) 
   and (pz.status_code = 'Y') 
   into v_florist_date;
   
   if v_florist_date is null then
       set v_result = 'N';
   else
       set v_result = 'Y';
   end if;
   
   select v_result from dual;

END;
$$

delimiter ;
