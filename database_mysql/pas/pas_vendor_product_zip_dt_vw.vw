drop view if exists pas.pas_vendor_product_zip_dt_vw;
CREATE ALGORITHM=UNDEFINED SQL SECURITY INVOKER VIEW pas.pas_vendor_product_zip_dt_vw AS 
select pvpsd.vendor_id AS vendor_id,
pvpsd.product_id AS product_id,
zip.zip_code_id AS zip_code_id,
pvpsd.delivery_date AS delivery_date,
pvpsd.ship_nd_date AS ship_nd_date,
pvpsd.ship_2d_date AS ship_2d_date,
pvpsd.ship_gr_date AS ship_gr_date,
pvpsd.ship_sat_date AS ship_sat_date,
pvpsd.ship_nd_cutoff_time AS ship_nd_cutoff_time,
pvpsd.ship_2d_cutoff_time AS ship_2d_cutoff_time,
pvpsd.ship_gr_cutoff_time AS ship_gr_cutoff_time,
pvpsd.ship_sat_cutoff_time AS ship_sat_cutoff_time 
from (pas.pas_vendor_product_state_dt pvpsd 
join ftd_apps.zip_code zip) 
where (pvpsd.state_code = zip.state_id);