DROP FUNCTION IF EXISTS pas.get_state_by_zip;

delimiter $$

CREATE FUNCTION pas.`get_state_by_zip`
(
 in_zip_code varchar(100)
) RETURNS varchar(2)
SQL SECURITY INVOKER
BEGIN
DECLARE out_state_code varchar(2);
   select distinct zc.state_id
   into   out_state_code
   from   ftd_apps.zip_code zc
   where  zc.zip_code_id = in_zip_code;
   
   return out_state_code;

END;
$$

delimiter ;
