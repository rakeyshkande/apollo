DROP PROCEDURE IF EXISTS pas.is_product_codified;

delimiter $$

CREATE PROCEDURE pas.`is_product_codified`
(
   in_product_id varchar(100)
)
SQL SECURITY INVOKER
BEGIN
   DECLARE v_codification_id varchar(100);
   DECLARE v_result varchar(1);
   
   select cp.codification_id
   into   v_codification_id
   from   (ftd_apps.codified_products cp
   join   ftd_apps.product_master pm)
   where  pm.novator_id = in_product_id
   and    pm.product_id = cp.product_id;
   
   if v_codification_id is null then
       set v_result = 'N';
   else
       set v_result = 'Y';
   end if;
   
   select v_result from dual;

END;
$$

delimiter ;
