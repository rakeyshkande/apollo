drop view if exists pas.pas_florist_product_zip_dt_vw;
CREATE ALGORITHM=MERGE SQL SECURITY INVOKER VIEW pas.pas_florist_product_zip_dt_vw AS 
SELECT pp.product_id, pz.delivery_date, pz.zip_code_id, pp.codification_id,
          (pz.cutoff_time + pt.delta_to_cst_hhmm) AS cutoff_time, pp.addon_days_qty
    from ((((pas.pas_florist_zipcode_dt pz 
    join pas.pas_product_dt pp) 
    join pas.pas_timezone_dt pt) 
    join pas.pas_country_dt pc) 
    join ftd_apps.state_master sm)
    WHERE 1 = 1
          AND pt.time_zone_code = sm.TIME_ZONE
          AND pt.delivery_date = pz.delivery_date
          AND sm.state_master_id = (select state_id
              from ftd_apps.zip_code zc
              where zc.zip_code_id = pz.zip_code_id
              limit 1)
          AND pt.delivery_date = pp.delivery_date
          AND pc.delivery_date = pz.delivery_date
          AND pc.country_id = 
	      CASE COALESCE(sm.country_code, 0)
	          WHEN 0 then 'US'
		  WHEN 'CAN' then 'CA'
		  ELSE sm.country_code
	      END
          AND pc.delivery_available_flag = 'Y'
          AND pp.delivery_available_flag = 'Y'
          AND pp.ship_method_florist = 'Y'
          AND pz.status_code = 'Y'
          AND pp.product_type IN ('FLORAL', 'SDFC', 'SDG')
          AND (isnull(pp.codification_id) or pp.codification_id = '' or
              ((select 'Y' 
              from ((ftd_apps.florist_codifications fc    
              join ftd_apps.florist_zips fz)    
              join ftd_apps.florist_master fm)    
              where (fc.codification_id = pp.codification_id 
              and fz.florist_id = fc.florist_id
              and fz.zip_code = pz.zip_code_id
              and fm.florist_id = fc.florist_id
              and fm.status <> 'Inactive' 
              and pas.is_florist_open_in_eros(fm.florist_id) = 'Y'
              and pas.is_florist_open_for_delivery(fm.florist_id, pz.delivery_date) = 'Y'
              and pas.is_florist_suspended(fm.florist_id, pz.delivery_date) = 'N'
              and (isnull(fc.block_start_date)
                  or (case 
                  when isnull(fc.block_end_date) then
                      (date_format(pz.delivery_date, '%Y%m%d') - date_format(fc.block_start_date, '%Y%m%d')) < 0
                  else 
                      pz.delivery_date not between fc.block_start_date and fc.block_end_date
                  end))
              and ((pz.delivery_date not between date_format(fz.block_start_date,'%Y%m%d') and fz.block_end_date) or isnull(fz.block_start_date)) 
              and not exists (select 'Y' from ftd_apps.florist_blocks fb
                  where fb.florist_id = fz.florist_id
                  and pz.delivery_date between fb.block_start_date and (case 
                      when isnull(fb.block_end_date) then pz.delivery_date
                      else fb.block_end_date
                  end))
              and (((date_format(pz.delivery_date,'%Y%m%d') - date_format(convert_tz(now(), 'SYSTEM', 'US/Central'),'%Y%m%d')) > pp.addon_days_qty)
                  or (((date_format(pz.delivery_date,'%Y%m%d') - date_format(convert_tz(now(), 'SYSTEM', 'US/Central'),'%Y%m%d')) = pp.addon_days_qty)
                      and ((fz.cutoff_time + pt.delta_to_cst_hhmm) > date_format(convert_tz(now(), 'SYSTEM', 'US/Central'),'%H%i'))))
              ) 
              limit 1) = 'Y'));