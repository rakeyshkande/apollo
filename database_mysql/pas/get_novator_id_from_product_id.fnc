DROP FUNCTION IF EXISTS pas.get_novator_id_from_product_id;

DELIMITER $$

CREATE FUNCTION pas.get_novator_id_from_product_id (
   in_product_id varchar(10)) RETURNS varchar(10)
   SQL SECURITY INVOKER
BEGIN
   DECLARE v_novator_id varchar(10);
   SELECT novator_id INTO v_novator_id FROM ftd_apps.product_master WHERE product_id = in_product_id;
   RETURN v_novator_id;
END;
$$

DELIMITER ;
