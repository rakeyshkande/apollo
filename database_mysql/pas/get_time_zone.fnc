DROP FUNCTION IF EXISTS pas.get_time_zone;

delimiter $$

CREATE FUNCTION pas.`get_time_zone`
(
 in_zip_code varchar(100)
) RETURNS varchar(2)
SQL SECURITY INVOKER
BEGIN
DECLARE out_time_zone varchar(1);
   select distinct sm.time_zone
   into   out_time_zone
   from   ftd_apps.state_master sm
   join   ftd_apps.zip_code zc on sm.state_master_id = zc.state_id
   where  zc.zip_code_id = in_zip_code;
   return out_time_zone;

END;
$$

delimiter ;
