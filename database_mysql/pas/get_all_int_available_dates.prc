DROP PROCEDURE IF EXISTS pas.get_all_int_available_dates;

delimiter $$

CREATE PROCEDURE pas.`get_all_int_available_dates`(
   in_product_id           varchar(100),
   in_country_code         varchar(100),
   in_max_delivery_date    datetime)
   SQL SECURITY INVOKER
BEGIN
      SELECT delivery_date, cutoff_date florist_cutoff_date
        FROM pas_country_product_dt
       WHERE     product_id = IN_PRODUCT_ID
             AND country_id = IN_COUNTRY_CODE
             AND delivery_available_flag = 'Y'
             AND DATE_FORMAT(delivery_date, '%Y%m%d') <= DATE_FORMAT(IN_MAX_DELIVERY_DATE, '%Y%m%d')
             AND (
                 ((DATE_FORMAT(cutoff_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) > 0) OR
                 (((DATE_FORMAT(cutoff_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) = 0) AND
                     (cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
                 )
             )
      ORDER BY delivery_date;
   END;
$$

delimiter ;

