DROP FUNCTION IF EXISTS pas.is_florist_open_in_eros;

delimiter $$

CREATE FUNCTION pas.`is_florist_open_in_eros`
(
 IN_FLORIST_ID      VARCHAR(100)
) RETURNS varchar(100) CHARSET latin1
SQL SECURITY INVOKER
BEGIN

DECLARE v_flag varchar(100);
DECLARE v_open_close_code varchar(10);
DECLARE v_override_date datetime;
DECLARE v_pm_cutoff_time varchar(100);
DECLARE v_eod_cutoff_time varchar(100);
DECLARE v_midnight_cutoff_time varchar(100) default '2400';
DECLARE v_noon_cutoff_time varchar(100) default '1200';
DECLARE v_local_time varchar(100);
DECLARE v_time_zone varchar(100);
DECLARE v_open_close_code_temp varchar(10);
DECLARE v_override_date_temp datetime;
DECLARE v_date datetime;

/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a flag that tells if a florist is open
        right now is EROS based on data in ftd_apps.florist_hours and
        ftd_apps.florist_hours_override

Input:
        florist_id

Output:
        v_flag - VARCHAR2

-----------------------------------------------------------------------------*/

    select sm.time_zone
    into v_time_zone
    from ftd_apps.florist_master fm
    join ftd_apps.state_master sm
    on sm.state_master_id = fm.state
    where fm.florist_id = in_florist_id;

    select value
    into v_pm_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'PM_CLOSED_ORDER_CUTOFF';

    select value
    into v_eod_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'EOD_CLOSED_ORDER_CUTOFF';
    
    set v_date = convert_tz(now(), 'SYSTEM', 'US/Central');
    set v_local_time = DATE_FORMAT(convert_tz(now(), 'SYSTEM', 'US/Central'), '%H%i');
    if cast(v_local_time as unsigned) < 600 then
        set v_local_time = '2400' + v_local_time;
        set v_date = date_sub(v_date, interval 1 day);
    end if;
    set v_local_time = CHANGE_TIMES_TO_REC_TIMEZONE(v_local_time, v_time_zone);
    if cast(v_local_time as unsigned) >= cast(v_midnight_cutoff_time as unsigned) then
        set v_date = date_add(v_date, interval 1 day);
    end if;
    
    select fh.open_close_code,
        (select fho.override_date
        from ftd_apps.florist_hours_override fho
        where fh.florist_id = fho.florist_id
        and fh.day_of_week = fho.day_of_week
        and DATE_FORMAT(fho.override_date, '%Y%m%d') = DATE_FORMAT(v_date, '%Y%m%d'))
    into v_open_close_code, v_override_date
    from ftd_apps.florist_hours fh
    where fh.florist_id = in_florist_id
    and fh.day_of_week = trim(upper(dayname(v_date)));

    if cast(v_local_time as unsigned) >= cast(v_eod_cutoff_time as unsigned) and cast(v_local_time as unsigned) < cast(v_midnight_cutoff_time as unsigned)
            and (ifnull(v_open_close_code, 'X') not in ('F', 'P') or (ifnull(v_open_close_code, 'X') in ('F', 'P') and trim(upper(dayname(v_date))) = 'SUNDAY')) then

        set v_date = date_add(v_date, interval 1 day);
        select fh.open_close_code,
            (select fho.override_date
            from ftd_apps.florist_hours_override fho
            where fh.florist_id = fho.florist_id
            and fh.day_of_week = fho.day_of_week
            and DATE_FORMAT(fho.override_date, '%Y%m%d') = DATE_FORMAT(v_date, '%Y%m%d'))
        into v_open_close_code_temp, v_override_date_temp
        from ftd_apps.florist_hours fh
        where fh.florist_id = in_florist_id
        and fh.day_of_week = trim(upper(dayname(v_date)));
        
        if trim(upper(dayname(v_date))) <> 'MONDAY' or ifnull(v_open_close_code_temp, 'Open') in ('F', 'A') then
            set v_open_close_code = v_open_close_code_temp;
            set v_override_date = v_override_date_temp;
            set v_local_time = '0000';
         else
            set v_date = date_sub(v_date, interval 1 day);
        end if;

    end if;

    if cast(v_local_time as unsigned) >= cast(v_midnight_cutoff_time as unsigned) then
        set v_local_time = v_local_time - '2400';
    end if;

    if trim(upper(dayname(v_date))) = 'SUNDAY' then
        -- everyone is open on Sunday
        set v_flag = 'Y';
    else
    
        set v_flag = 'N';

        if v_open_close_code is null then
            set v_flag = 'Y';
        else
            if v_override_date is not null then
                -- any open/close code with an override is open
                set v_flag = 'Y';
            else
                if v_open_close_code = 'F' then
                    -- Full day closed are always closed
                    set v_flag = 'N';
                else
                    if v_open_close_code = 'A' then
                        if ((DATE_FORMAT(v_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(), 'SYSTEM', 'US/Central'), '%Y%m%d')) > 0) then
                            set v_flag = 'N';
                        else
                            -- AM closed is open after 12:00
                            if cast(v_local_time as unsigned) < cast(v_noon_cutoff_time as unsigned) then
                                set v_flag = 'N';
                            else
                                set v_flag = 'Y';
                            end if;
                        end if;
                    else
                        -- PM closed, check cutoff time
                        if cast(v_local_time as unsigned) < cast(v_pm_cutoff_time as unsigned) then
                            -- before cutoff
                            set v_flag = 'Y';
                        else
                            -- after cutoff
                            set v_flag = 'N';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end if;

    RETURN v_flag;

END;
$$

delimiter ;
