DROP PROCEDURE IF EXISTS pas.get_all_avail_vendor_dates;

delimiter $$

CREATE PROCEDURE pas.`get_all_avail_vendor_dates`(
   in_product_id           varchar(100),
   in_zip_code             varchar(100),
   in_max_delivery_date    DateTime)
   SQL SECURITY INVOKER
BEGIN
	DECLARE v_product_id varchar(100);
	-- get product id from novator id
	set v_product_id = pas.get_product_id_from_novator_id(in_product_id);
	-- in_product_id is specified as the novator id
	select distinct delivery_date,
	
            case when trim(DAYNAME(delivery_date)) = 'Saturday'
            then 
                (select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_BY_NOON_DEL_FLAG='Y'
                 and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
                 and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
                     where vp.product_subcode_id = v_product_id
                     and vp.vendor_id = pas.vendor_id and vp.available = 'Y'
                     and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
                     and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
                         where vcr.vendor_id = vp.vendor_id
                         and vcr.carrier_id = czc.carrier
                         and vcr.usage_percent = -1))))
            when trim(DAYNAME(delivery_date)) = 'Sunday'
            then 'N'
            when trim(DAYNAME(delivery_date)) = 'Monday'
            then 'N' 
            else 
                (select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and WEEKDAY_BY_NOON_DEL_FLAG='Y'
                 and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
                 and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
                     where vp.product_subcode_id = v_product_id
                     and vp.vendor_id = pas.vendor_id and vp.available = 'Y'
                     and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
                     and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
                         where vcr.vendor_id = vp.vendor_id
                         and vcr.carrier_id = czc.carrier
                         and vcr.usage_percent = -1))))
            end as morning_del,
			
            STR_TO_DATE(null, '%Y%m%d') florist_date,
			
            case when ((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
                (((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
                (ship_nd_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
                )
                then ship_nd_date
                else STR_TO_DATE(null, '%Y%m%d')
            end as ship_nd_date,
			
            case when ((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
                (((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
                (ship_2d_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
                )
                then ship_2d_date
                else STR_TO_DATE(null, '%Y%m%d')
            end as ship_2d_date,
			
            case when ((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
                (((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
                (ship_gr_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
                )
                then ship_gr_date
                else STR_TO_DATE(null, '%Y%m%d')
            end as ship_gr_date,
			
            case when ((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
                (((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
                (ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
                )
                then ship_sat_date
                else STR_TO_DATE(null, '%Y%m%d')
            end as ship_sa_date
			
        from pas.pas_vendor_product_zip_dt_vw pas
        where product_id = (select product_id from ftd_apps.product_master where novator_id = in_product_id)
        and zip_code_id = in_zip_code
		
        and DATE_FORMAT(delivery_date, '%Y%m%d') <= DATE_FORMAT(in_max_delivery_date, '%Y%m%d')
        and delivery_date > curdate()
        and (
				(
					((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
					(((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
					(ship_nd_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
				)
				)
				OR
				(
					((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
					(((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
					(ship_2d_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
				)
				)
				OR
				(
					((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
					(((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
					(ship_gr_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
				)
				)
				OR
				(
					((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0 
						and exists (select 1 from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_DEL_AVAILABLE='Y'
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
						where vp.product_subcode_id = v_product_id 
						and vp.vendor_id = pas.vendor_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
						where vcr.vendor_id = vp.vendor_id
						and vcr.carrier_id = czc.carrier
						and vcr.usage_percent = -1))))
					) or
					(((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0 
						and exists (select 1 from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_DEL_AVAILABLE='Y'
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
						where vp.product_subcode_id = v_product_id 
						and vp.vendor_id = pas.vendor_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
						where vcr.vendor_id = vp.vendor_id
						and vcr.carrier_id = czc.carrier
						and vcr.usage_percent = -1))))
					) and
					(ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
				)
				)
        )
        order by delivery_date;
   END;
$$

delimiter ;
