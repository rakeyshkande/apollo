DROP FUNCTION IF EXISTS pas.is_florist_suspended;

delimiter $$

CREATE FUNCTION pas.`is_florist_suspended`
(
 IN_FLORIST_ID      VARCHAR(100),
 IN_DATE_TIME      DATETIME
) RETURNS varchar(1) CHARSET latin1
SQL SECURITY INVOKER
BEGIN

DECLARE v_flag                   varchar(1);
DECLARE v_suspend_start_date     datetime;
DECLARE v_suspend_end_date       datetime;
DECLARE v_suspend_type           varchar(10);
DECLARE v_goto_flag              varchar(10);
DECLARE v_buffer                 integer;

/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a flag that tells if a florist is
        suspended during the input date/time.

Input:
        florist_id, date_time

Output:
        v_flag - VARCHAR2

-----------------------------------------------------------------------------*/

    select fs.suspend_start_date,
        fs.suspend_end_date,
        fs.suspend_type,
        fm.super_florist_flag
    into v_suspend_start_date,
        v_suspend_end_date,
        v_suspend_type,
        v_goto_flag
    from ftd_apps.florist_suspends fs
    join ftd_apps.florist_master fm
    on fm.florist_id = fs.florist_id
    where fs.florist_id = in_florist_id;
    
    set v_flag = 'N';

    if v_suspend_start_date is null then
        set v_flag = 'N';
    elseif (v_suspend_type <> 'G' and v_goto_flag = 'Y') then
        set v_flag = 'N';
    else
 
        select value
        into v_buffer
        from frp.global_parms
        where context = 'UPDATE_FLORIST_SUSPEND'
        and name = 'BUFFER_MINUTES';
        
        if (convert_tz(now(), 'SYSTEM', 'US/Central') between (v_suspend_start_date - interval v_buffer minute) and v_suspend_end_date) then
            set v_flag = 'Y';
        elseif (in_date_time between (v_suspend_start_date - interval v_buffer minute) and v_suspend_end_date) then
            set v_flag = 'Y';
        end if;

    end if;
    
    RETURN v_flag;

END;
$$

delimiter ;
