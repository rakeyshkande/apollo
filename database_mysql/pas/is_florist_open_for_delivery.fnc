DROP FUNCTION IF EXISTS pas.is_florist_open_for_delivery;

delimiter $$

CREATE FUNCTION pas.`is_florist_open_for_delivery`
(
 IN_FLORIST_ID      VARCHAR(100),
 IN_DELIVERY_DATE   DATETIME
) RETURNS varchar(1) CHARSET latin1
SQL SECURITY INVOKER
BEGIN

DECLARE v_flag varchar(1);
DECLARE v_open_close_code varchar(10);
DECLARE v_override_date datetime;
DECLARE v_pm_cutoff_time varchar(100);
DECLARE v_time_zone varchar(100);

/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a flag that tells if a florist is open
        on the input delivery date based on data in ftd_apps.florist_hours and
        ftd_apps.florist_hours_override

Input:
        florist_id, delivery_date

Output:
        v_flag - VARCHAR2

-----------------------------------------------------------------------------*/

    select fh.open_close_code,
        (select fho.override_date
        from ftd_apps.florist_hours_override fho
        where fh.florist_id = fho.florist_id
        and fh.day_of_week = fho.day_of_week
        and DATE_FORMAT(fho.override_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d'))
    into v_open_close_code, v_override_date
    from ftd_apps.florist_hours fh
    where fh.florist_id = in_florist_id
    and fh.day_of_week = trim(upper(dayname(in_delivery_date)));

    select sm.time_zone
    into v_time_zone
    from ftd_apps.florist_master fm
    join ftd_apps.state_master sm
    on sm.state_master_id = fm.state
    where fm.florist_id = in_florist_id;

    select value
    into v_pm_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'PM_CLOSED_DELIVERY_CUTOFF';

    set v_pm_cutoff_time = change_times_timezone(v_pm_cutoff_time, v_time_zone);

    set v_flag = 'N';

    if v_open_close_code is null then
        set v_flag = 'Y';
    else
        if v_override_date is not null then
            -- any open/close code with an override is open
            set v_flag = 'Y';
        else
            if v_open_close_code in ('F', 'A') then
                -- Full day and AM closed are always closed
                set v_flag = 'N';
            else
                -- PM closed, check cutoff time
                if ((DATE_FORMAT(in_delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(), 'SYSTEM', 'US/Central'), '%Y%m%d')) > 0) then
                    set v_flag = 'Y';
                else
                    if cast(DATE_FORMAT(convert_tz(now(), 'SYSTEM', 'US/Central'), '%H%i') as unsigned) < cast(v_pm_cutoff_time as unsigned) then
                        -- before cutoff
                        set v_flag = 'Y';
                    else
                        -- after cutoff
                        set v_flag = 'N';
                    end if;
                end if;
            end if;
        end if;
    end if;

    RETURN v_flag;

END;
$$

delimiter ;
