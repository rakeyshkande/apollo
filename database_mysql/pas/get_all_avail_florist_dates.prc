DROP PROCEDURE IF EXISTS pas.get_all_avail_florist_dates;

delimiter $$

CREATE PROCEDURE pas.`get_all_avail_florist_dates`(
   in_product_id           varchar(100),
   in_zip_code             varchar(100),
   in_max_delivery_date    DateTime)
   SQL SECURITY INVOKER
BEGIN

-- The deal here is, the join on zip_code returns multiple rows but they all have the same zip code. So we don't want to join on zip_code, we want to retrieve the zip_code info we need separately and just use that.
-- Separately retrieve the state_id from zip_code. 
-- Separately retrieve the zime_zone from state_master.
-- Separately retrieve the country_code from state_master, and transform it (null=US,CAN=CA,else don't transform).

    DECLARE v_state_code varchar(2);
    DECLARE v_time_zone varchar(2);
    DECLARE v_country_code varchar(2);

    set v_state_code = pas.get_state_by_zip(in_zip_code);
    set v_time_zone = pas.get_time_zone(in_zip_code);

    select case when isnull(country_code) then 'US' when (country_code='CAN') then 'CA' else country_code end
    into v_country_code 
    from ftd_apps.state_master 
    where state_master_id = v_state_code;

    select pz.delivery_date AS delivery_date,
    DATE_ADD(pz.delivery_date, INTERVAL pp.addon_days_qty DAY) AS florist_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_nd_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_2d_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_gr_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_sa_date
    from ((((pas.pas_florist_zipcode_dt pz 
    join pas.pas_product_dt pp) 
    join pas.pas_timezone_dt pt) 
    join pas.pas_country_dt pc) 
    join ftd_apps.product_master pm)
    where ((1 = 1)
    and pm.novator_id = in_product_id
    and pp.product_id = pm.product_id
    and pz.zip_code_id = in_zip_code
    and DATE_FORMAT(pz.delivery_date, '%Y%m%d') <= DATE_FORMAT(IN_MAX_DELIVERY_DATE, '%Y%m%d')
    and (
        case
        when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
            DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
            (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (pz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
        else
            DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
            (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                (pz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
        end
    )
    and (pt.time_zone_code = v_time_zone)
    and (pt.delivery_date = pz.delivery_date)
    and (pz.zip_code_id = in_zip_code)
    and (pt.delivery_date = pp.delivery_date)
    and (pc.delivery_date = pz.delivery_date)
    and (pc.country_id = v_country_code)
    and (pc.delivery_available_flag = 'Y') 
    and (pp.delivery_available_flag = 'Y') 
    and (pp.ship_method_florist = 'Y') 
    and (pz.status_code = 'Y') 
    and (pp.product_type in ('FLORAL','SDFC','SDG')) 
    and (isnull(pp.codification_id) or (pp.codification_id = '')))
    union 
    select pz.delivery_date AS delivery_date,
    DATE_ADD(pz.delivery_date, INTERVAL pp.addon_days_qty DAY) AS florist_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_nd_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_2d_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_gr_date,
    STR_TO_DATE(null, '%Y%m%d') AS ship_sa_date
    from ((((pas.pas_florist_zipcode_dt pz 
    join pas.pas_product_dt pp) 
    join pas.pas_timezone_dt pt) 
    join pas.pas_country_dt pc) 
    join ftd_apps.product_master pm)
    where ((1 = 1)
    and pm.novator_id = in_product_id
    and pp.product_id = pm.product_id
    and pz.zip_code_id = in_zip_code
    and DATE_FORMAT(pz.delivery_date, '%Y%m%d') <= DATE_FORMAT(IN_MAX_DELIVERY_DATE, '%Y%m%d')
    and (
        case
        when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
            DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
            (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (pz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
        else
            DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
            (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                (pz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
        end
    )
    and (pt.time_zone_code = v_time_zone) 
    and (pt.delivery_date = pz.delivery_date) 
    and (pz.zip_code_id = in_zip_code) 
    and (pt.delivery_date = pp.delivery_date) 
    and (pc.delivery_date = pz.delivery_date) 
    and (pc.country_id = v_country_code)
    and (pc.delivery_available_flag = 'Y') 
    and (pp.delivery_available_flag = 'Y') 
    and (pp.ship_method_florist = 'Y') 
    and (pz.status_code = 'Y') 
    and (pp.codification_id is not null)
    and ((select 'Y' AS Y 
        from ((ftd_apps.florist_codifications fc    
        join ftd_apps.florist_zips fz)    
        join ftd_apps.florist_master fm)    
        where ((fc.codification_id = pp.codification_id) 
        and fz.florist_id = fc.florist_id
        and fz.zip_code = pz.zip_code_id
        and fm.florist_id = fc.florist_id
        and (fm.status <> 'Inactive') 
        and pas.is_florist_open_in_eros(fm.florist_id) = 'Y'
        and pas.is_florist_open_for_delivery(fm.florist_id, pz.delivery_date) = 'Y'
        and (isnull(fc.block_start_date)
            or (case 
            when isnull(fc.block_end_date) then
                (date_format(pz.delivery_date, '%Y%m%d') - date_format(fc.block_start_date, '%Y%m%d')) < 0
            else 
                pz.delivery_date not between fc.block_start_date and fc.block_end_date
            end))
        and ((pz.delivery_date not between date_format(fz.block_start_date,'%Y%m%d') and fz.block_end_date) or isnull(fz.block_start_date)) 
        and not exists (select 'Y' from ftd_apps.florist_blocks fb
            where fb.florist_id = fz.florist_id
            and pz.delivery_date between fb.block_start_date and (case 
                when isnull(fb.block_end_date) then pz.delivery_date
                else fb.block_end_date
            end))
        and pas.is_florist_suspended(fm.florist_id, pz.delivery_date) = 'N'
        and (
            case
            when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
                DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
                (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                    DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (fz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
            else
                DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
                (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                    (fz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
            end
            )
        ) 
        limit 1) = 'Y')
    )
        order by delivery_date, florist_date desc;
   END;
$$

delimiter ;

