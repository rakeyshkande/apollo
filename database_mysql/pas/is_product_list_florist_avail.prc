DROP PROCEDURE IF EXISTS pas.is_product_list_florist_avail;

delimiter $$

CREATE PROCEDURE pas.`is_product_list_florist_avail`(
   in_zip_code             varchar(100),
   in_delivery_date_str        varchar(100),
   in_product_list         varchar(4000))
   SQL SECURITY INVOKER
BEGIN

   DECLARE v_sc varchar(2);
   DECLARE in_delivery_date datetime;  
   DECLARE v_product_list varchar(10);
   DECLARE v_novator_list varchar(10);
   
   set in_delivery_date = str_to_date(in_delivery_date_str, '%m/%d/%Y');
   set v_sc = pas.get_state_by_zip(IN_ZIP_CODE);
   set v_product_list = pas.get_product_id_from_novator_id(in_product_list);
   
   select STRAIGHT_JOIN distinct pp.product_id INTO v_novator_list
      from (((((pas.pas_florist_zipcode_dt pz
      join pas.pas_product_dt pp)
      join ftd_apps.zip_code zc)
      join ftd_apps.state_master sm)
      join pas.pas_timezone_dt pt)
      join pas.pas_country_dt pc)
      where ((1 = 1) and pz.zip_code_id = in_zip_code
      and pp.product_id = v_product_list
      and pz.delivery_date = in_delivery_date
      and (
          case
          when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
              DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
              (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                  DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (pz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
          else
              DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
              (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                  (pz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
          end
      )
      and (pt.time_zone_code = sm.time_zone)
      and (pt.delivery_date = pz.delivery_date)
      and (sm.state_master_id = zc.state_id)
      and (zc.zip_code_id = pz.zip_code_id)
      and (pt.delivery_date = pp.delivery_date)
      and (pc.delivery_date = pz.delivery_date)
      and (pc.country_id = (case
          when isnull(sm.country_code) then 'US' 
          when (sm.country_code = 'CAN') then 'CA' 
          else sm.country_code
       end))
      and (pc.delivery_available_flag = 'Y')
      and (pp.delivery_available_flag = 'Y')
      and (pp.ship_method_florist = 'Y')
      and (pz.status_code = 'Y')
      and (
      (     
      (pp.product_type in ('FLORAL','SDFC','SDG'))
      and (isnull(pp.codification_id) or (pp.codification_id = ''))
      ) 
      or (
      ((select 'Y' 
         from ((ftd_apps.florist_codifications fc
         join ftd_apps.florist_zips fz)
         join ftd_apps.florist_master fm)
         where ((fc.codification_id = pp.codification_id)
         and (fz.florist_id = fc.florist_id)
         and (fz.zip_code = pz.zip_code_id)
         and (fm.florist_id = fc.florist_id)
         and (fm.status <> 'Inactive')
         and pas.is_florist_open_in_eros(fm.florist_id) = 'Y'
         and pas.is_florist_open_for_delivery(fm.florist_id, pz.delivery_date) = 'Y'
         and (isnull(fc.block_start_date)
             or (case 
             when isnull(fc.block_end_date) then
                 (date_format(pz.delivery_date, '%Y%m%d') - date_format(fc.block_start_date, '%Y%m%d')) < 0
             else 
                 pz.delivery_date not between fc.block_start_date and fc.block_end_date
             end))
         and ((pz.delivery_date not between fz.block_start_date and fz.block_end_date) or isnull(fz.block_start_date))
         and not exists (select 'Y' from ftd_apps.florist_blocks fb
             where fb.florist_id = fz.florist_id
             and pz.delivery_date between fb.block_start_date and (case 
                 when isnull(fb.block_end_date) then pz.delivery_date
                 else fb.block_end_date
             end))
         and pas.is_florist_suspended(fm.florist_id, pz.delivery_date) = 'N'
         and (
             case
             when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
                 DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
                 (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                     DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (fz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
             else
                 DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > pp.addon_days_qty or
                 (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = pp.addon_days_qty and
                     (fz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
             end
             )
         )
         limit 1) = 'Y')
	  )
      ))
      ;

   if (v_novator_list is null or v_novator_list = '') then
      select 1 from dual where false;
   else
      select pas.get_novator_id_from_product_id(v_novator_list) as novator_id;
   end if;

END;
$$

delimiter ;

