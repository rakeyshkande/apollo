DROP PROCEDURE IF EXISTS pas.get_vendor_availability_product;

delimiter $$

CREATE PROCEDURE pas.`get_vendor_availability_product`(
   in_product_id           varchar(100),
   in_zip_code             varchar(100),
   in_delivery_date    	   DateTime,
   in_shipping_method      varchar(100))
   SQL SECURITY INVOKER
BEGIN

		DECLARE v_product_id varchar(100);
		DECLARE v_state_code varchar(2);
		
		-- get product id from novator id
		set v_product_id = pas.get_product_id_from_novator_id(in_product_id);
		
		-- get state from zip code
		set v_state_code = pas.get_state_by_zip(in_zip_code);
		
		IF in_shipping_method = 'ND' THEN
			select(SELECT novator_id FROM ftd_apps.product_master WHERE product_id = (select distinct product_id
				from pas.pas_vendor_product_state_dt
				where product_id = v_product_id
				and state_code = v_state_code		
				and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
				and (
    					((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
    					(((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    					(ship_nd_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    				))) as product_id,
				case when trim(DAYNAME(in_delivery_date)) = 'Saturday'
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Sunday' 
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Monday'
						then 'N' 
					else 
						(select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and WEEKDAY_BY_NOON_DEL_FLAG='Y'
						and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_nd_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
							where vcr.vendor_id = vp.vendor_id
							and vcr.carrier_id = czc.carrier
							and vcr.usage_percent = -1))))
				end as morning_del
			from dual;
		END IF;
		
		IF in_shipping_method = '2D' THEN
			select(SELECT novator_id FROM ftd_apps.product_master WHERE product_id = (select distinct product_id
				from pas.pas_vendor_product_state_dt
				where product_id = v_product_id
				and state_code = v_state_code		
				and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
				and (
    					((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
    					(((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    					(ship_2d_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    				))) as product_id,
				case when trim(DAYNAME(in_delivery_date)) = 'Saturday'
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Sunday' 
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Monday'
						then 'N' 
					else 
						(select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and WEEKDAY_BY_NOON_DEL_FLAG='Y'
						and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_2d_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
							where vcr.vendor_id = vp.vendor_id
							and vcr.carrier_id = czc.carrier
							and vcr.usage_percent = -1))))
				end as morning_del
			from dual;
		END IF;
		
		IF in_shipping_method = 'GR' THEN
			select(SELECT novator_id FROM ftd_apps.product_master WHERE product_id = (select distinct product_id
				from pas.pas_vendor_product_state_dt
				where product_id = v_product_id
				and state_code = v_state_code		
				and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
				and (
    					((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
    					(((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    					(ship_gr_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    				))) as product_id,
				case when trim(DAYNAME(in_delivery_date)) = 'Saturday'
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Sunday' 
						then 'N'
					when trim(DAYNAME(in_delivery_date)) = 'Monday'
						then 'N' 
					else 
						(select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and WEEKDAY_BY_NOON_DEL_FLAG='Y'
						and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_gr_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
							where vcr.vendor_id = vp.vendor_id
							and vcr.carrier_id = czc.carrier
							and vcr.usage_percent = -1))))
				end as morning_del
			from dual;
		END IF;
		
		IF in_shipping_method = 'SA' THEN
			select(SELECT novator_id FROM ftd_apps.product_master WHERE product_id = (select distinct product_id
				from pas.pas_vendor_product_state_dt
				where product_id = v_product_id
				and state_code = v_state_code		
				and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
				and (
					((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0 
						and exists (select 1 from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_DEL_AVAILABLE='Y'
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
						where vcr.vendor_id = vp.vendor_id
						and vcr.carrier_id = czc.carrier
						and vcr.usage_percent = -1))))
					) or
					(((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0 
						and exists (select 1 from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_DEL_AVAILABLE='Y'
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
						where vcr.vendor_id = vp.vendor_id
						and vcr.carrier_id = czc.carrier
						and vcr.usage_percent = -1))))
					) and
    					(ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    				))) as product_id,
				case when trim(DAYNAME(in_delivery_date)) = 'Saturday'
					then 
						(select if(count(*)=0,'N','Y') from ftd_apps.carrier_zip_code czc where zip_code= in_zip_code and SATURDAY_BY_NOON_DEL_FLAG='Y'
						and exists(select 1 from ftd_apps.product_master where novator_id = in_product_id and morning_delivery_flag = 'Y')
						and exists(select 1 from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm, pas.pas_vendor_product_state_dt pas
						where vp.product_subcode_id = v_product_id and vp.available = 'Y'
						and vp.vendor_id = vm.vendor_id and vm.active = 'Y'
						and pas.product_id = vp.product_subcode_id and pas.state_code = v_state_code and pas.vendor_id = vm.vendor_id
						and DATE_FORMAT(delivery_date, '%Y%m%d') = DATE_FORMAT(in_delivery_date, '%Y%m%d')
						and (
    							((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) > 0) or
		    					(((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d')) = 0) and
    							(ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i')))
    						)
						and (vm.vendor_type = 'FTP' or exists (select 1 from ftd_apps.vendor_carrier_ref vcr
							where vcr.vendor_id = vp.vendor_id
							and vcr.carrier_id = czc.carrier
							and vcr.usage_percent = -1))))
					else 
						'N' 
				end as morning_del
			from dual;
		END IF;
END;
$$

delimiter ;
