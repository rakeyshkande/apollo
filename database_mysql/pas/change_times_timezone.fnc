DROP FUNCTION IF EXISTS pas.change_times_timezone;

delimiter $$

CREATE FUNCTION pas.`change_times_timezone`
(
 IN_TIME      VARCHAR(100),
 IN_TIME_ZONE VARCHAR(100)
) RETURNS varchar(100) CHARSET latin1
SQL SECURITY INVOKER
BEGIN
DECLARE v_adjusted_time varchar(100);
/*-----------------------------------------------------------------------------
Description:
        This stored procedure converts a time to the central timezone

Input:
        time, time zone

Output:
        adjusted time - VARCHAR2

-----------------------------------------------------------------------------*/

  IF in_time IS NULL THEN

     set v_adjusted_time = NULL;

  ELSE

     CASE TRIM(in_time_zone)
       WHEN '1' THEN
            set v_adjusted_time = right(concat('0000', cast((cast(in_time as unsigned) - 100) as char(10))), 4);
       WHEN '2' THEN
            set v_adjusted_time = in_time;
       WHEN '3' THEN
            set v_adjusted_time = right(concat('0000', cast((cast(in_time as unsigned) + 100) as char(10))), 4);
       WHEN '4' THEN
            set v_adjusted_time = right(concat('0000', cast((cast(in_time as unsigned) + 200) as char(10))), 4);
       WHEN '5' THEN
            set v_adjusted_time = right(concat('0000', cast((cast(in_time as unsigned) + 300) as char(10))), 4);
       WHEN '6' THEN
            set v_adjusted_time = right(concat('0000', cast((cast(in_time as unsigned) + 400) as char(10))), 4);
       ELSE
            set v_adjusted_time = NULL;
     END CASE;

  END IF;

  RETURN v_adjusted_time;

END;
$$

delimiter ;
