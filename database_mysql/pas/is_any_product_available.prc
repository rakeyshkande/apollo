drop procedure if exists pas.is_any_product_available;

delimiter $$

create procedure pas.is_any_product_available 
(
   IN IN_ZIP_CODE            varchar(100),
   IN IN_DELIVERY_DATE       datetime,
   OUT OUT_AVAILABLE         varchar(1)
)
SQL SECURITY INVOKER
/*-----------------------------------------------------------------------------
Description:
        Determines if a product is available in a zip for the delivery date and
        the zip code. The query for florist only checks for non-codified products.

Input:
        in_zip_code      varchar2
        in_delivery_date DateTime

Output:
        out_available    varchar2 Y or N

-----------------------------------------------------------------------------*/
BEGIN

  	    DECLARE v_florist_date    datetime;
  	    DECLARE v_vendor_date     datetime;


            select distinct pz.delivery_date
            from (((((pas.pas_florist_zipcode_dt pz 
	    join pas.pas_product_dt pp) 
	    join ftd_apps.zip_code zc) 
	    join ftd_apps.state_master sm) 
	    join pas.pas_timezone_dt pt) 
    	    join pas.pas_country_dt pc) 
	    where ((1 = 1)
	    and pz.zip_code_id = IN_ZIP_CODE
	    and (
            case
            when (pz.cutoff_time + pt.delta_to_cst_hhmm) < 0 then
                DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') > 0 or
                (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now() + interval 1 day,'+01:00','+00:00'), '%Y%m%d') = 0 and
                    DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i') - (pz.cutoff_time + pt.delta_to_cst_hhmm) < 2400)
            else
                DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') > 0 or
                (DATE_FORMAT(pz.delivery_date, '%Y%m%d') - DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%Y%m%d') = 0 and
                    (pz.cutoff_time + pt.delta_to_cst_hhmm) > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
            end
	    )
	    and (pt.time_zone_code = sm.time_zone) 
	    and (pt.delivery_date = pz.delivery_date) 
	    and (sm.state_master_id = zc.state_id) 
	    and (zc.zip_code_id = pz.zip_code_id) 
	    and (pt.delivery_date = pp.delivery_date) 
	    and (pc.delivery_date = pz.delivery_date) 
	    and (pc.country_id = (case 
	        when isnull(sm.country_code) then 'US' 
	        when (sm.country_code = 'CAN') then 'CA' 
	        else sm.country_code 
	    end)) 
	    and (pc.delivery_available_flag = 'Y') 
	    and (pp.delivery_available_flag = 'Y')
	    and (pp.ship_method_florist = 'Y') 
	    and (pz.status_code = 'Y') 
	    and (pp.product_type in ('FLORAL','SDFC','SDG')) 
	    and (isnull(pp.codification_id) or (pp.codification_id = ''))) 
	    and pz.delivery_date = IN_DELIVERY_DATE
	    into v_florist_date;
	    
	    
	    select distinct delivery_date
	    from pas.pas_vendor_product_zip_dt_vw
	    where zip_code_id = IN_ZIP_CODE
	    and delivery_date = IN_DELIVERY_DATE
	    and (
	            (
	                ((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) > 0) or
	                (((DATE_FORMAT(ship_nd_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) = 0) and
	                (ship_nd_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
	            )
	            )
	            OR
	            (
	                ((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) > 0) or
	                (((DATE_FORMAT(ship_2d_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) = 0) and
	                (ship_2d_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
	            )
	            )
	            OR
	            (
	                ((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) > 0) or
	                (((DATE_FORMAT(ship_gr_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) = 0) and
	                (ship_gr_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
	            )
	            )
	            OR
	            (
	                ((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) > 0) or
	                (((DATE_FORMAT(ship_sat_date, '%Y%m%d') - DATE_FORMAT(curdate(), '%Y%m%d')) = 0) and
	                (ship_sat_cutoff_time > DATE_FORMAT(convert_tz(now(),'+01:00','+00:00'), '%H%i'))
	            )
	            )
	   )
	   into v_vendor_date;
        
       set OUT_AVAILABLE = 'N';
	   IF v_vendor_date is not null THEN
		   set OUT_AVAILABLE = 'Y';
	   END IF;
	   IF v_florist_date is not null THEN
		   set OUT_AVAILABLE := 'Y';
  	   END IF;
  	   
END;
$$

delimiter ;

