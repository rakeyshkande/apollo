DROP FUNCTION IF EXISTS pas.is_country_delivery_available;

delimiter $$

CREATE DEFINER='pas'@'%' FUNCTION pas.`is_country_delivery_available`
(
 in_delivery_date datetime, 
 in_zip_code varchar(100)
) RETURNS varchar(1)
BEGIN
DECLARE out_flag varchar(1);
   select distinct pc.delivery_available_flag
   into   out_flag
   from   pas.pas_country_dt pc
   join   ftd_apps.state_master sm on pc.country_id = 
      (case
          when isnull(sm.country_code) then 'US' 
          when (sm.country_code = 'CAN') then 'CA'
          else sm.country_code
       end)
   join   ftd_apps.zip_code zc on zc.state_id = sm.state_master_id
   where  pc.delivery_date = in_delivery_date
   and    zc.zip_code_id = in_zip_code
   and    pc.delivery_available_flag = 'Y';
   
   return out_flag;

END;
$$

delimiter ;
