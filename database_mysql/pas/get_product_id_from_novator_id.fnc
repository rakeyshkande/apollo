DROP FUNCTION IF EXISTS pas.get_product_id_from_novator_id;

DELIMITER $$

CREATE FUNCTION pas.get_product_id_from_novator_id (
   in_novator_id varchar(10)) RETURNS varchar(10)
   SQL SECURITY INVOKER
BEGIN
   DECLARE v_product_id varchar(10);
   SELECT product_id INTO v_product_id FROM ftd_apps.product_master WHERE novator_id = in_novator_id;
   RETURN v_product_id;
END;
$$

DELIMITER ;
