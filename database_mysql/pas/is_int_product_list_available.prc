DROP PROCEDURE IF EXISTS pas.is_int_product_list_available;

delimiter $$

CREATE PROCEDURE pas.`is_int_product_list_available`(
   in_country_code         varchar(100),
   in_delivery_date        varchar(100),
   in_product_list         varchar(4000))
   SQL SECURITY INVOKER
BEGIN

   SET @sql = concat_ws('',
      'select product_id from pas.pas_country_product_dt where product_id in (', 
      in_product_list,
      ') and country_id = ''',
      in_country_code,
      ''' and DATE_FORMAT(delivery_date, ''%m/%d/%Y'') = ''',
      in_delivery_date,
      ''' and (',
      '((DATE_FORMAT(cutoff_date, ''%Y%m%d'') - DATE_FORMAT(curdate(), ''%Y%m%d'')) > 0) or ',
      '(((DATE_FORMAT(cutoff_date, ''%Y%m%d'') - DATE_FORMAT(curdate(), ''%Y%m%d'')) = 0) and ',
      '(cutoff_time > DATE_FORMAT(convert_tz(now(),''+01:00'',''+00:00''), ''%H%i'') ',
     ')))ORDER BY product_id');
   
   PREPARE s1 FROM @sql;
   EXECUTE s1;
   DEALLOCATE PREPARE s1;
   
END;
$$

delimiter ;

