USE `avs`;
DROP procedure IF EXISTS `insert_avs_request`;
DELIMITER $$
USE `avs`$$

CREATE PROCEDURE `insert_avs_request`(
IN  IN_CLIENT_IDENTIFIER           varchar(45),
IN  IN_RESPONSE_TIME               BIGINT,
IN  IN_VENDOR_RESPONSE_ID          varchar(512),
IN  IN_VENDOR                      varchar(45),
IN  IN_ADDRESS                     varchar(200),
IN  IN_CITY                        varchar(100),
IN  IN_STATE_PROVINCE              varchar(100),
IN  IN_POSTAL_CODE                 varchar(12),
OUT AVS_REQUEST_ID                 BIGINT
)
BEGIN

    INSERT INTO avs_request (CLIENT_IDENTIFIER, RESPONSE_TIME, VENDOR_RESPONSE_ID, VENDOR, ADDRESS, CITY, STATE_PROVINCE, POSTAL_CODE, CREATED_ON)
                     VALUES (IN_CLIENT_IDENTIFIER, IN_RESPONSE_TIME, IN_VENDOR_RESPONSE_ID, IN_VENDOR, IN_ADDRESS, IN_CITY, IN_STATE_PROVINCE, IN_POSTAL_CODE, now());

    SET AVS_REQUEST_ID = LAST_INSERT_ID();
    COMMIT;
    
END


$$

