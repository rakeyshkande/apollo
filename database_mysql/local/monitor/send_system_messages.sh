#!/bin/ksh
#
# This is part of the MySQL implementation of the Apollo system_messages trigger. It should run on the Apollo MySQL replication 
# slaves, not on the master; as of 2012/12/7, that means it should run on radon and xenon, not on opal.
#
. /dbspace/mysql/local/mysqlprofile.sh
SCRIPT_BASE=/home/mysql/msg
MYSQL="mysql -D msg -s -N -e"
sysmsg=`$MYSQL "select get_next_system_message();"`
while [[ $sysmsg != "NULL" ]]; do
   email_msg=`$MYSQL "call system_message_handler($sysmsg);"`
   if [[ -n $email_msg ]]; then
      email_subj=`echo "$email_msg"|grep "##SUBJEC##"|awk -F \#\# '{print $3}'`
      email_dist=`echo "$email_msg"|grep "##DISTRO##"|awk -F \#\# '{print $3}'`
      echo "$email_msg"|grep -v "##SUBJEC##"|grep -v "##DISTRO##"|mailx -s "$email_subj" `cat ${SCRIPT_BASE}/utl/${email_dist}_LIST`
   fi
   sysmsg=`$MYSQL "select get_next_system_message();"`
done
