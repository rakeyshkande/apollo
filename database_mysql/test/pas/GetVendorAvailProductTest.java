package com.mys.db.pro;

import static org.junit.Assert.*;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GetVendorAvailProductTest {
	
	static Connection connection = null;
	static Statement stmt = null;
	static CallableStatement cStmt = null;
	
	public ResultSet getVendorResultSet(String date_, String shipMethod_){
			ResultSet rs = null;
			try{
		 	cStmt.setString(1, "BB02");
	        cStmt.setString(2, "60515");
	        cStmt.setString(3, date_);
	        cStmt.setString(4, shipMethod_);
	        
	        cStmt.execute();
	        rs = cStmt.getResultSet();
			}catch(Exception e){
				e.printStackTrace();
			}
			return rs;
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String driver_ = "com.mysql.jdbc.Driver";
        String database_ = "jdbc:mysql://biapp1.ftdi.com:3306/";
        String user_ = "jbaba";
        String password_ = "jbaba1";
                
        Class.forName(driver_);               
        
        connection =  DriverManager.getConnection(database_, user_, password_);
	    // Disable auto-commit
	    connection.setAutoCommit(false);
	    cStmt = connection.prepareCall("{CALL pas.get_vendor_availability_product(?,?,?,?)}");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		 if(stmt!=null)
        	 connection.close();
		 
		 if(connection!=null)
        	 connection.close();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNDProduct() {
		
		try {
		String date_ = "2012-07-16 00:00:00";
		String shipMethod_ = "ND";
        ResultSet rs1 = getVendorResultSet(date_,shipMethod_);
        int cnt = 0;
        while (rs1.next()) {
             System.out.println(
            		 rs1.getString("product_id")+" | "+
                		 	 rs1.getString("morning_del")
                       );
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs1.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}			
	}
	
	@Test
	public void test2DProduct() {
		
		try {
		String date_ = "2012-07-17 00:00:00";
		String shipMethod_ = "2D";
        ResultSet rs1 = getVendorResultSet(date_,shipMethod_);
        int cnt = 0;
        while (rs1.next()) {
             System.out.println(
            		 rs1.getString("product_id")+" | "+
                		 	 rs1.getString("morning_del")
                       );
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs1.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}			
	}
	
	@Test
	public void testGRProduct() {
		
		try {
		String date_ = "2012-07-18 00:00:00";
		String shipMethod_ = "GR";
        ResultSet rs1 = getVendorResultSet(date_,shipMethod_);
        int cnt = 0;
        while (rs1.next()) {
             System.out.println(
            		 rs1.getString("product_id")+" | "+
                		 	 rs1.getString("morning_del")
                       );
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs1.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}			
	}
	
	@Test
	public void testSAProduct() {
		
		try {
		String date_ = "2012-07-21 00:00:00";
		String shipMethod_ = "SA";
        ResultSet rs1 = getVendorResultSet(date_,shipMethod_);
        int cnt = 0;
        while (rs1.next()) {
             System.out.println(
            		 	 rs1.getString("product_id")+" | "+
            		 	 rs1.getString("morning_del")
                       );
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs1.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}			
	}
	
}
