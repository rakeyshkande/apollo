package com.mys.db.pro;

import static org.junit.Assert.*;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * GetAllVendorDatesTest - for testing all available vendor dates
 * @author jbaba
 *
 */
public class GetAllVendorDatesTest {
	
	static Connection connection = null;
	static Statement stmt = null;
	static CallableStatement cStmt = null;
	
	/**
	 * Returns result-set for the given 
	 * product_id, zip_code, max_delivery_date
	 * @param date_
	 * @return ResultSet
	 */
	public ResultSet getVendorResultSet(String date_){
			ResultSet rs = null;
			try{
		 	cStmt.setString(1, "BB32");
	        cStmt.setString(2, "60515");
	        cStmt.setString(3, date_);        
	        
	        cStmt.execute();
	        rs = cStmt.getResultSet();
			}catch(Exception e){
				e.printStackTrace();
			}
			return rs;
	}
	
	/**
	 * Initializes database connection and prepares call to procedure
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String driver_ = "com.mysql.jdbc.Driver";
        String database_ = "jdbc:mysql://biapp1.ftdi.com:3306/";
        String user_ = "jbaba";
        String password_ = "jbaba1";
                
        Class.forName(driver_);               
        
        connection =  DriverManager.getConnection(database_, user_, password_);
	    // Disable auto-commit
	    connection.setAutoCommit(false);
	    cStmt = connection.prepareCall("{CALL pas.get_all_avail_vendor_dates(?,?,?)}");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		 if(stmt!=null)
        	 connection.close();
		 
		 if(connection!=null)
        	 connection.close();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test10Dates() {		
		try {
		String date_ = "2012-07-21 00:00:00";

        ResultSet rs1 = getVendorResultSet(date_);
        int cnt = 0;
        while (rs1.next()) {
             /*System.out.println(
            		 	 rs1.getString("delivery_date")+ " || "
                       + rs1.getString("morning_del")  + " || "
                       + rs1.getString("florist_date") + " || "
                       + rs1.getString("ship_nd_date") + " || "
                       + rs1.getString("ship_2d_date") + " || "
                       + rs1.getString("ship_gr_date") + " || "
                       + rs1.getString("ship_sa_date") + " || "
                       );*/
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs1.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
				
	}

	@Test
	public void test20Dates() {
	
		try {
		String date_ = "2012-08-09 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test30Dates() {
	
		try {
		String date_ = "2012-08-19 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test40Dates() {
	
		try {
		String date_ = "2012-08-29 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test50Dates() {
	
		try {
		String date_ = "2012-09-10 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test60Dates() {
	
		try {
		String date_ = "2012-09-20 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test70Dates() {
	
		try {
		String date_ = "2012-09-30 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test80Dates() {
	
		try {
		String date_ = "2012-10-10 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test90Dates() {
	
		try {
		String date_ = "2012-10-20 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test100Dates() {
	
		try {
		String date_ = "2012-10-30 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test110Dates() {
	
		try {
		String date_ = "2012-11-10 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
	
	@Test
	public void test120Dates() {
	
		try {
		String date_ = "2012-11-20 00:00:00";
		ResultSet rs = getVendorResultSet(date_);
        int cnt = 0;
        while (rs.next()) {             
             cnt++;
        }
        assertTrue(cnt>0);
        System.out.println("No of records : "+cnt);
        rs.close();
        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//assertEquals("Result - ", 50, 50);
	}
}
