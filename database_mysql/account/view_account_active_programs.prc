DROP PROCEDURE IF EXISTS account.view_account_active_programs;

delimiter $$

CREATE DEFINER='account'@'%' PROCEDURE account.view_account_active_programs
(
   in_email_address	varchar(200),
   in_active_date	datetime
)
BEGIN
    	SELECT ap.account_master_id,        
               ap.account_program_id,
               ap.program_master_id,
               pm.program_name,
               pm.program_description
        FROM   account.account_program ap
        INNER JOIN account.program_master pm
        ON ap.program_master_id = pm.program_master_id
        INNER JOIN account.account_master am
        ON ap.account_master_id = am.account_master_id
        INNER JOIN account.account_email em
        ON am.account_master_id = em.account_master_id
        WHERE ap.account_program_status = 'A'
            and (ap.start_date is null or DATE_FORMAT(ap.start_date, '%Y%m%d') <= DATE_FORMAT(in_active_date, '%Y%m%d'))
            and (ap.expiration_date is null or DATE_FORMAT(in_active_date, '%Y%m%d') <= DATE_FORMAT(ap.expiration_date, '%Y%m%d'))
            and (em.email_address = lower(in_email_address) and em.active_flag='Y')
            and pm.active_flag = 'Y';    
END;
$$

delimiter ;