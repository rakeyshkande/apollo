DROP PROCEDURE IF EXISTS ftd_apps.get_source_list;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.`get_source_list`()
BEGIN


    DECLARE v_service_fee_flag,v_ship_fee_flag       varchar(1);
    SELECT frp.get_global_parm_value ('ORDER_SERVICE_CONFIG','DISPLAY_SHIP_FEE') INTO v_ship_fee_flag;
    SELECT frp.get_global_parm_value ('ORDER_SERVICE_CONFIG','DISPLAY_SERVICE_FEE') INTO v_service_fee_flag;
    
    SELECT upper(sm.source_code) source_code,
       	   sm.snh_id,
           sm.price_header_id,
           sm.order_source,
           sm.related_source_code,
           sm.company_id,
           sm.discount_allowed_flag,
           sm.iotw_flag,
           CASE sm.display_service_fee_code WHEN 'D' THEN v_service_fee_flag ELSE sm.display_service_fee_code END as client_disp_svc_fee_flag,
           CASE sm.display_shipping_fee_code WHEN 'D' THEN v_ship_fee_flag ELSE sm.display_shipping_fee_code END as client_disp_shp_fee_flag,
           sm.allow_free_shipping_flag,
           sm.mp_redemption_rate_id,
           sm.same_day_upcharge,
           sm.display_same_day_upcharge
     FROM ftd_apps.source_master sm

 ;
  END;
$$

delimiter ;

