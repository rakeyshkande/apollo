DROP PROCEDURE IF EXISTS ftd_apps.get_delivery_date_range;

delimiter $$

CREATE PROCEDURE ftd_apps.get_delivery_date_range()
BEGIN

    select start_date, end_date, instruction_text
    from delivery_date_range
    where end_date >= curdate();
	
END;
$$

delimiter ;

