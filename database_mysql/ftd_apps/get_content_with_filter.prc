drop procedure if exists ftd_apps.get_content_with_filter;


delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_content_with_filter ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning content with filter data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT cm.content_context,
    	   cm.content_name,
    	   cd.filter_1_value,
    	   cd.filter_2_value,
    	   cd.content_txt
      FROM ftd_apps.content_detail cd
      JOIN ftd_apps.content_master cm
        ON cd.content_master_id = cm.content_master_id;
 
  END;
$$

delimiter ;

