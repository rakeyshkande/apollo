drop procedure if exists ftd_apps.get_source_template_ref;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_source_template_ref 
(
)
BEGIN
	select sir.source_code, sir.si_template_id
	from si_source_template_ref sir
  	join si_template st
	on st.si_template_id = sir.si_template_id
	join si_template_type stt
	on stt.si_template_type_code = st.si_template_type_code
	order by sir.source_code, stt.sort_num
	;
END;
$$

delimiter ;




