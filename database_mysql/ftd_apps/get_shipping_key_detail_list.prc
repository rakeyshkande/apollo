drop procedure if exists ftd_apps.get_shipping_key_detail_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_shipping_key_detail_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning shipping key detail data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT skd.shipping_detail_id,
    	   skd.shipping_key_id,
    	   skd.min_price,
    	   skd.max_price
      FROM ftd_apps.shipping_key_details skd
      ;
  END;
$$

delimiter ;

