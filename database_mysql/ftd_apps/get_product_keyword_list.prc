drop procedure if exists ftd_apps.get_product_keyword_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_keyword_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product company data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(pk.product_id) as product_id,
    	   pk.keyword
      FROM ftd_apps.product_keywords pk
      ORDER BY pk.product_id
      ;
  END;
$$

delimiter ;

