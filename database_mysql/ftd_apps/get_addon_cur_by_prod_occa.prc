DROP PROCEDURE IF EXISTS ftd_apps.get_addon_cur_by_prod_occa;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.get_addon_cur_by_prod_occa
(
in_product_id                     varchar(10),
in_occasion_id                    integer,
in_source_code                    varchar(10)
)
/***********************************************************
* This procedure differs from the oracle version in the following:
* 1. Returns all occasions for cards.
* 2. Join to ftd_apps.occasion
* 3. Returns occasion id and occasion description.
* 4. Not joining to occasion_addon for addons and funeral banners.
************************************************************/
begin
    
    declare v_partner_name            varchar(50);
    declare v_global_chocolate_flag           varchar(1000);
    declare v_add_on_free_id                  varchar(100);
    declare v_ship_method_florist             varchar(1);
    
    select pp.partner_name 
    into v_partner_name
    from partner_program pp
    left outer join source_program_ref spr
    on spr.program_name = pp.program_name
    left outer join source_master sm
    on spr.program_name = pp.program_name
    and sm.source_code = spr.source_code
    where sm.source_code = in_source_code;

     
    select frp.get_global_parm_value('FTDAPPS_PARMS', 'MOD_ORDER_CHOCOLATES_AVAILABLE') into v_global_chocolate_flag;

    if in_source_code is null then
       select 'N/A'
       into v_add_on_free_id;
    else
       select sm.add_on_free_id
       into v_add_on_free_id
       from source_master sm
       where sm.source_code = in_source_code;
    end if;
    
    select pm.ship_method_florist
    into v_ship_method_florist
    from product_master pm
    where pm.product_id = in_product_id;
   
   
        select faa.addon_id, faa.addon_type, faa.description as add_on_description, faa.price, faa.addon_weight, faa.addon_text, faa.unspsc,faa.product_id, faa.active_flag as add_on_active_flag, faa.default_per_type_flag, faa.display_price_flag, faat.addon_type_id, faat.description as add_on_type_description, faat.product_feed_flag, fapa.display_seq_num, fapa.max_qty, null as occasion_id, null as occasion_description
        from addon faa, addon_type faat, product_addon fapa
        where faa.addon_type = faat.addon_type_id
        and fapa.product_id = in_product_id
        and fapa.addon_id = faa.addon_id
        and faa.active_flag = 'Y'
        and fapa.active_flag = 'Y'
	and (v_ship_method_florist = 'Y' or exists ( 
            select 1 
            from ftd_apps.vendor_addon  fava
            where fava.addon_id = faa.addon_id
            and fava.active_flag = 'Y'
            )
        )
        and faa.addon_type not in ('4', '3', (case v_global_chocolate_flag when 'Y' then (case v_partner_name when 'USAA' then (case v_add_on_free_id when 'FC' then '5' else 'N/A' end) else 'N/A' end) else '5' end),(case v_add_on_free_id when faa.addon_id then 'N/A' else '6' end))			   
      union all
        select faa.addon_id, faa.addon_type, faa.description as add_on_description, faa.price, faa.addon_weight, faa.addon_text, faa.unspsc,faa.product_id, faa.active_flag as add_on_active_flag, faa.default_per_type_flag, faa.display_price_flag, faat.addon_type_id, faat.description as add_on_type_description, faat.product_feed_flag, 1 as display_seq_num, 1 as max_qty, fo.occasion_id, fo.description as occasion_description
        from addon faa, addon_type faat, product_master fapm, occasion_addon faoa, occasion fo
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = in_product_id
        and faa.addon_id = faoa.addon_id
        and fo.occasion_id = faoa.occasion_id
        /* need all occasions for cards */
        /*and (in_occasion_id is null or faoa.occasion_id = in_occasion_id)*/
        and faa.active_flag = 'Y'
        and fapm.add_on_cards_flag = 'Y' 
        and faa.addon_type = '4'
      union all
        select faa.addon_id, faa.addon_type, faa.description as add_on_description, faa.price, faa.addon_weight, faa.addon_text, faa.unspsc,faa.product_id, faa.active_flag as add_on_active_flag, faa.default_per_type_flag, faa.display_price_flag, faat.addon_type_id, faat.description as add_on_type_description, faat.product_feed_flag, 1 as display_seq_num, 1 as max_qty, null as occasion_id, null as occasion_description
        from addon faa, addon_type faat, product_master fapm
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = in_product_id
        and faa.active_flag = 'Y'
        and fapm.add_on_funeral_flag = 'Y' 
        and faa.addon_type = '3'
        order by display_seq_num, add_on_description, addon_id;


end;
$$

delimiter ;

