drop procedure if exists ftd_apps.get_filter_index;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_filter_index 
(
)
BEGIN
	select si.si_template_id, si.index_name, sip.product_id
	from si_template_index si
	join si_template_index_prod sip
	on si.si_template_id = sip.si_template_id
	and si.index_name = sip.index_name
	where si.index_name in ('limit_products','exclude_products','nonsearchitems')
	order by si.si_template_id,si.index_name
	;
END;
$$

delimiter ;




