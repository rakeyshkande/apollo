drop procedure if exists ftd_apps.get_ord_svc_xsl_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_ord_svc_xsl_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order service client data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT osx.xsl_code,
    	     osx.xsl_txt
      FROM ftd_apps.order_service_xsl osx
      ;
  END;
$$

delimiter ;

