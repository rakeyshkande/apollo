drop procedure if exists ftd_apps.get_price_header_detail_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_price_header_detail_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning price header detail data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT pd.price_header_id,
           pd.min_dollar_amt,
           pd.max_dollar_amt,
           pd.discount_type,
           pd.discount_amt
      FROM ftd_apps.price_header_details pd
      ORDER BY pd.price_header_id, pd.min_dollar_amt, pd.max_dollar_amt
      ;
  END;
$$

delimiter ;

