drop procedure if exists ftd_apps.get_product_attr_restr_source;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_attr_restr_source ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product attribute restriction by source code.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(pars.source_code) as source_code,
    	   pars.product_attr_restr_id
    FROM ftd_apps.product_attr_restr_source_excl pars
    ORDER BY pars.source_code;
    	   
  END;
$$

delimiter ;

