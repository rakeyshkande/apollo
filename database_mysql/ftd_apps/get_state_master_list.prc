drop procedure if exists ftd_apps.get_state_master_list;

delimiter $$

create procedure ftd_apps.get_state_master_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning state master data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
BEGIN

    select state_master_id,
        state_name,
        country_code,
        time_zone,
        delivery_exclusion,
        drop_ship_available_flag
    from ftd_apps.state_master;

END;
$$

delimiter ;

