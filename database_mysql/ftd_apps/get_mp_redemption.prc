DROP PROCEDURE IF EXISTS ftd_apps.get_mp_redemption;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.`get_mp_redemption`()
BEGIN

     select mrr.mp_redemption_rate_id,
        pmmp.payment_method_id,
        pmmp.dollar_to_mp_operator,
        pmmp.rounding_method_id,
        pmmp.rounding_scale_qty,
        mrr.mp_redemption_rate_amt
     from ftd_apps.miles_points_redemption_rate mrr
     JOIN  ftd_apps.payment_method_miles_points pmmp ON pmmp.payment_method_id = mrr.payment_method_id
 ;
  END;
$$

delimiter ;

