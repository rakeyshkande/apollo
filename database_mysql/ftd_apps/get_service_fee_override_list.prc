drop procedure if exists ftd_apps.get_service_fee_override_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_service_fee_override_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning service fee override data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT so.snh_id,
    	   so.override_date,
    	   so.domestic_charge,
    	   so.international_charge,
    	   so.vendor_charge,
    	   so.vendor_sat_upcharge,
    	   so.same_day_upcharge
      FROM ftd_apps.service_fee_override so
      ;
  END;
$$

delimiter ;

