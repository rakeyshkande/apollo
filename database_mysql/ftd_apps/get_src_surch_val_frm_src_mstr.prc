DROP PROCEDURE IF EXISTS ftd_apps.get_src_surch_val_frm_src_mstr;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.get_src_surch_val_frm_src_mstr
(
in_source_code	varchar(10)
)
BEGIN
    SELECT 
        surcharge_amount, 
        apply_surcharge_code,
        surcharge_description,
        display_surcharge_flag 
   FROM   FTD_APPS.SOURCE_MASTER
   WHERE  source_code = in_source_code ;
END;
$$

delimiter ;