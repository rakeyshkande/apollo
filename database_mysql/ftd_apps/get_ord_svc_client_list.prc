drop procedure if exists ftd_apps.get_ord_svc_client_list;


delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_ord_svc_client_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning service fee override data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT osc.client_id,
    	     osc.client_name
      FROM ftd_apps.order_service_client osc
     WHERE active_flag = 'Y'
      ;
  END;
$$

delimiter ;

