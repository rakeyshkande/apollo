drop procedure if exists ftd_apps.get_index_sub_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_index_sub_list 
(
   in_source_code       varchar(100),
   in_index_name        varchar(200),
   in_default_domain    varchar(100)
)
BEGIN

    	select si.sub_index_name,
               s.index_desc
    	from ftd_apps.si_template_index_sub si
        LEFT OUTER JOIN ftd_apps.si_template_index s
        ON si.sub_index_name = s.index_name
        AND si.si_template_id = s.si_template_id
    	where si.index_name = in_index_name
    	and si.si_template_id = 
    	(
    		select si_template_id from
    		(
          	    select a.si_template_id
                    from (
    			select st.si_template_id, stt.sort_num
    			from si_template_index sii
    			join si_template st
    			on st.si_template_id = sii.si_template_id
    			join si_template_type stt
    			on stt.si_template_type_code = st.si_template_type_code
    			join si_source_template_ref sir
    			on sir.source_code = in_source_code
    			and sir.si_template_id = sii.si_template_id
    			where sii.index_name = in_index_name
    			union 
    			select st.si_template_id, stt.sort_num
    			from si_template st
    			join si_template_type stt
    			on stt.si_template_type_code = st.si_template_type_code
    			where st.si_template_type_code = 'DN'
          		and st.si_template_key = in_default_domain
    		    ) a
    		    order by a.sort_num
    		) b
    		limit 1
    	) 
    	order by si.display_seq_num
	;     
     
    
END;
$$

delimiter ;




