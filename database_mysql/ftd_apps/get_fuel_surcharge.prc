drop procedure if exists ftd_apps.get_fuel_surcharge;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_fuel_surcharge ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning fuel surcharge fee.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT fuel_surcharge_amt
      FROM fuel_surcharge
     WHERE end_date is null;
      
  END;
$$

delimiter ;

