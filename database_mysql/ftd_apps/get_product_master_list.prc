drop procedure if exists ftd_apps.get_product_master_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_master_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product master data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT   upper(pm.product_id) product_id,
             upper(pm.novator_id) novator_id,
             (select pc.description from product_category pc where pc.product_category_id = pm.category) as category_desc,
             pm.product_type,
             (select pt.type_description from product_types pt where pt.type_id = pm.product_type) as type_desc,
             pm.product_sub_type,
             (select pst.sub_type_description from product_sub_types pst where pst.sub_type_id = pm.product_sub_type and pm.product_type=pst.type_id) as sub_type_desc,
             pm.long_description,
             pm.product_name,
             pm.novator_name,
             pm.dominant_flowers,
             pm.over_21,
             pm.exception_start_date,
             pm.exception_end_date,
             pm.personalization_template_id,
             pm.shipping_key,
             pm.status,
             pm.delivery_type,
             pm.discount_allowed_flag,
             pm.standard_price,
	     pm.deluxe_price,
             pm.premium_price,
             pm.gbb_popover_flag,
             pm.gbb_title_txt,
             pm.gbb_name_override_flag_1,
             pm.gbb_name_override_flag_2,
             pm.gbb_name_override_flag_3,
             pm.gbb_name_override_txt_1,
             pm.gbb_name_override_txt_2,
             pm.gbb_name_override_txt_3,
             pm.gbb_price_override_flag_1,
             pm.gbb_price_override_flag_2,
             pm.gbb_price_override_flag_3,
             pm.gbb_price_override_txt_1,
             pm.gbb_price_override_txt_2,
             pm.gbb_price_override_txt_3,   
             pm.preferred_price_point,
             pm.no_tax_flag,
             pm.ship_method_florist,
             pm.ship_method_carrier,
             pm.allow_free_shipping_flag
    FROM product_master pm
    WHERE novator_id is not null;
  END;
$$

delimiter ;

