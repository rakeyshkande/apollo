DROP PROCEDURE IF EXISTS ftd_apps.get_generic_surcharge_values;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.get_generic_surcharge_values
(
in_start_date	datetime
)

BEGIN
  IF IN_START_DATE is null THEN
    SELECT 
      apply_surcharge_code, 
      fuel_surcharge_amt, 
      surcharge_description,
      display_surcharge_flag, 
      send_surcharge_to_florist_flag 
    FROM   ftd_apps.fuel_surcharge
    WHERE  end_date is null;
  ELSE
    SELECT 
      apply_surcharge_code, 
      fuel_surcharge_amt, 
      surcharge_description,
      display_surcharge_flag, 
      send_surcharge_to_florist_flag 
    FROM   ftd_apps.fuel_surcharge
    WHERE  IN_START_DATE >= start_date
    AND    (end_date is null or IN_START_DATE <= end_date);
  END IF; 
end;
$$

delimiter ;