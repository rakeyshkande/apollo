drop procedure if exists ftd_apps.get_product_attr_restr;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_attr_restr ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product attribute restrictions.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT par.product_attr_restr_id,
    	   par.product_attr_restr_name,
    	   par.product_attr_restr_oper,
    	   par.product_attr_restr_value,
    	   par.product_attr_restr_desc,
    	   par.java_method_name
    FROM ftd_apps.product_attr_restr par;
    	   
  END;
$$

delimiter ;

