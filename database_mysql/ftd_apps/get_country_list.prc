drop procedure if exists ftd_apps.get_country_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_country_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning country data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
  
    SELECT cm.country_id,
    	   cm.name,
           cm.oe_country_type
        FROM    ftd_apps.country_master cm
        WHERE   cm.status = 'Active'
        ORDER BY cm.oe_display_order, cm.name;  
     
  END;
$$

delimiter ;

