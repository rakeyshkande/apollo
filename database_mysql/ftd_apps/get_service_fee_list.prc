drop procedure if exists ftd_apps.get_service_fee_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_service_fee_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning service fee data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT s.snh_id,
    	   s.first_order_domestic,
    	   s.first_order_international,
    	   s.vendor_charge,
    	   s.vendor_sat_upcharge,
    	   s.same_day_upcharge
      FROM ftd_apps.snh s
      ;
  END;
$$

delimiter ;

