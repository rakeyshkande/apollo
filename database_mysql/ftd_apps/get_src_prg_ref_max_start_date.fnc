drop function if exists ftd_apps.get_src_prg_ref_max_start_date;

delimiter $$

create DEFINER='ftd_apps'@'%' function ftd_apps.get_src_prg_ref_max_start_date
(
 in_source_code  varchar(100),
 in_date         date
)
RETURNS DATE
/*------------------------------------------------------------------------------
Description:
   Returns the max start date less than the date passed in for the
   source code passed in.

Input:
        source_code  VARCHAR2
        in_date      DATE

Output:
        start_date      DATE
------------------------------------------------------------------------------*/
BEGIN
DECLARE v_start_date DATE;
     SELECT max(start_date)
       INTO v_start_date
       FROM source_program_ref
      WHERE source_code = in_source_code
        AND start_date <= in_date;

     RETURN v_start_date;
END;
$$

delimiter ;

