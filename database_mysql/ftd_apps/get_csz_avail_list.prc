drop procedure if exists ftd_apps.get_csz_avail_list;

delimiter $$

create procedure ftd_apps.get_csz_avail_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning csz_avail records.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT csz_zip_code,
    	   gnadd_flag
      FROM ftd_apps.csz_avail;
  END;
$$

delimiter ;

