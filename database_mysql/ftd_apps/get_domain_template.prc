drop procedure if exists ftd_apps.get_domain_template;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_domain_template 
(
)
BEGIN
	select st.si_template_key, st.si_template_id
	from si_template st
	where st.si_template_type_code = 'DN'
	;
END;
$$

delimiter ;




