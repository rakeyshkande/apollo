drop procedure if exists ftd_apps.get_shipping_key_cost_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_shipping_key_cost_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning shipping key cost data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT skc.shipping_key_detail_id,
    	   skc.shipping_method_id,
    	   skc.shipping_cost
      FROM ftd_apps.shipping_key_costs skc
     ORDER BY skc.shipping_key_detail_id, skc.shipping_cost
      ;
  END;
$$

delimiter ;

