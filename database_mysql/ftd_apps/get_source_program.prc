DROP PROCEDURE IF EXISTS ftd_apps.get_source_program;

delimiter $$

CREATE DEFINER='ftd_apps'@'%' PROCEDURE ftd_apps.`get_source_program`()
BEGIN

    select  
        spr.source_code,
        pp.program_type,
        pr.calculation_basis,
        pr.points,
        pr.bonus_calculation_basis,
        pr.bonus_points,
        pr.maximum_points,
        pr.reward_type,
        spr.start_date,
        pp.partner_name,
        spr.program_name partner_id
    from ftd_apps.source_program_ref spr
    join ftd_apps.partner_program pp ON spr.program_name = pp.program_name
    JOIN ftd_apps.program_reward pr ON pr.program_name  = pp.program_name
    where spr.start_date=(select max(start_date) 
                          from ftd_apps.source_program_ref r
                          where r.source_code = spr.source_code
                          and r.start_date < now()
                         )
 ;
  END;
$$

delimiter ;

