DROP PROCEDURE IF EXISTS ftd_apps.get_source_code_mp_payment_method;
delimiter $$

CREATE PROCEDURE ftd_apps.`get_source_code_mp_payment_method`(
   in_source_code	varchar(10)
)
BEGIN
    SELECT mprr.payment_method_id
    FROM ftd_apps.miles_points_redemption_rate mprr, ftd_apps.source_master sm
    WHERE sm.source_code = in_source_code
    AND sm.mp_redemption_rate_id = mprr.mp_redemption_rate_id;
 END;
$$
delimiter ;
