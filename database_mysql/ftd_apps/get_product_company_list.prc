drop procedure if exists ftd_apps.get_product_company_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_company_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product company data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(pc.product_id) as product_id,
    	   pc.company_id,
    	   cm.company_name
      FROM ftd_apps.product_company_xref pc
      JOIN ftd_apps.company_master cm ON cm.company_id = pc.company_id
      ORDER BY pc.product_id
      ;
  END;
$$

delimiter ;

