drop procedure if exists ftd_apps.get_state_company_tax;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_state_company_tax()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning state company tax data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT    sct.state_master_id
              , sct.company_id
              , sct.tax1_name
              , sct.tax1_rate
              , CASE sct.tax1_name WHEN null then null ELSE (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = sct.tax1_name) END AS tax1_display_order
              , sct.tax2_name
              , sct.tax2_rate
              , CASE sct.tax2_name WHEN null then null ELSE (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = sct.tax2_name) END AS tax2_display_order
              , sct.tax3_name
              , sct.tax3_rate
              , CASE sct.tax3_name WHEN null then null ELSE (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = sct.tax3_name) END AS tax3_display_order
              , sct.tax4_name
              , sct.tax4_rate
              , CASE sct.tax4_name WHEN null then null ELSE (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = sct.tax4_name) END AS tax4_display_order
              , sct.tax5_name
              , sct.tax5_rate
              , CASE sct.tax5_name WHEN null then null ELSE (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = sct.tax5_name) END AS tax5_display_order
    FROM    ftd_apps.state_company_tax sct
    ORDER BY sct.state_master_id, sct.company_id
    ;
  END;
$$

delimiter ;

