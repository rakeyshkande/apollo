drop procedure if exists ftd_apps.get_ord_svc_client_xsl_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_ord_svc_client_xsl_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order service client data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT oscx.client_id,
    	   oscx.action_type,
    	   oscx.xsl_code
      FROM ftd_apps.order_service_client_xsl oscx
      JOIN ftd_apps.order_service_client osc
        ON osc.client_id = oscx.client_id
     WHERE osc.active_flag = 'Y'
      ;
  END;
$$

delimiter ;

