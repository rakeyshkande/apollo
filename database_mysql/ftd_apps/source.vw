--- Staging view to replace subquery from original view
--- MySQL does not support view creation with subquery showing 
--- ERROR 1349 (HY000): View's SELECT contains a subquery in the FROM clause

CREATE OR REPLACE VIEW ftd_apps.source_program_ref_vw 
AS SELECT
   sp.source_code,
   sp.program_name
FROM ftd_apps.source_program_ref sp
WHERE sp.start_date =
   (
      select max(b.start_date)
      from ftd_apps.source_program_ref b
      where b.source_code = sp.source_code
      and b.start_date <= current_date
   );



--- MySQL main view

CREATE VIEW FTD_APPS.SOURCE AS SELECT
        sm.source_code,
        sm.marketing_group source_type,
        sm.department_code,
        sm.description,
        sm.start_date,
        sm.end_date,
        sm.price_header_id pricing_code,
        sm.snh_id shipping_code,
        spr.program_name partner_id,
        sm.payment_method_id valid_pay_method,
        sm.list_code_flag,
        sm.default_source_code_flag,
        sm.billing_info_prompt,
        sm.billing_info_logic,
        sm.source_type marketing_group,
        sm.external_call_center_flag,
        sm.highlight_description_flag,
        sm.discount_allowed_flag,
        sm.bin_number_check_flag,
        sm.order_source,
        null mileage_bonus,
        sm.promotion_code,
        pr.calculation_basis mileage_calculation_source,
        pr.bonus_points,
        pr.bonus_calculation_basis bonus_type,
        pr.bonus_separate_data separate_data,
        sm.yellow_pages_code,
        pr.points points_miles_per_dollar,
        pr.maximum_points maximum_points_miles,
        sm.jcpenney_flag,
        sm.default_source_code_flag oe_default_flag,
        sm.company_id,
        sm.send_to_scrub,
        sm.fraud_flag,
        sm.enable_lp_processing,
        sm.emergency_text_flag,
        sm.requires_delivery_confirmation,
        sm.webloyalty_flag,
        cm.company_name,
        cm.internet_origin,
        sm.primary_backup_rwd_flag,
        sm.related_source_code,
        sm.apply_surcharge_code,
        sm.surcharge_description,
        sm.display_surcharge_flag,
        sm.surcharge_amount,
        sm.allow_free_shipping_flag,
        pp.partner_name,
        pp.program_type,
        pr.reward_type,
        sm.same_day_upcharge,
    sm.display_same_day_upcharge,
    sm.morning_delivery_flag,
    sm.morning_delivery_free_shipping
FROM    ftd_apps.source_master sm
LEFT OUTER JOIN ftd_apps.source_program_ref_vw spr
ON      sm.source_code = spr.source_code
LEFT OUTER JOIN ftd_apps.program_reward pr
ON      spr.program_name = pr.program_name
LEFT OUTER JOIN ftd_apps.partner_program pp
ON      pp.program_name = spr.program_name
JOIN ftd_apps.company_master cm
ON      cm.company_id = sm.company_id;

