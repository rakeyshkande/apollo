drop procedure if exists ftd_apps.get_zip_code_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_zip_code_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning zip codes.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT distinct zip_code_id,
    	   state_id
      FROM ftd_apps.zip_code
      ; 
  END;
$$

delimiter ;

