drop procedure if exists ftd_apps.get_payment_method_mp;
delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.`get_payment_method_mp`()
BEGIN
    SELECT payment_method_id,
       dollar_to_mp_operator,
       rounding_method_id,
       rounding_scale_qty,
       commission_pct
    FROM ftd_apps.payment_method_miles_points;
 END;
 
 $$
 
delimiter ;