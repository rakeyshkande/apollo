drop procedure if exists ftd_apps.get_product_source_pd;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_source_pd ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source codes for product.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(ps.product_id) as product_id,
    	   upper(ps.source_code) as source_code
      FROM ftd_apps.product_source ps
    UNION
    SELECT upper(us.upsell_master_id) as product_id,
    	   upper(us.source_code) as source_code
      FROM ftd_apps.upsell_source us
    UNION
    SELECT upper(ud.upsell_detail_id) as product_id,
    	   upper(us.source_code) as source_code
    FROM ftd_apps.upsell_detail ud
    JOIN ftd_apps.upsell_master um
    ON ud.upsell_master_id = um.upsell_master_id
    JOIN ftd_apps.upsell_source us
    ON us.upsell_master_id = um.upsell_master_id
    ORDER BY 1;
  END;
$$

delimiter ;

