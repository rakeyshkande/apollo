drop procedure if exists ftd_apps.get_product_color_list;


delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_color_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product color data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(pc.product_id) as product_id,
    	   cm.color_master_id,
    	   cm.description
      FROM ftd_apps.product_colors pc
      JOIN ftd_apps.color_master cm
        ON pc.product_color = cm.color_master_id
      ORDER BY pc.product_id
      ;
  END;
$$

delimiter ;

