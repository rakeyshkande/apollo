drop procedure if exists ftd_apps.get_addon_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_addon_list()

BEGIN
    SELECT faa.addon_ID,
           faa.addon_type,
           faa.description as add_on_description,
           faa.price,
           faa.addon_weight,
           faa.addon_text,
           faa.unspsc,
           faa.product_id,
           faa.active_flag as add_on_active_flag,
           faa.default_per_type_flag,
           faa.display_price_flag,
           faat.addon_type_id,
           faat.description as add_on_type_description,
           faat.product_feed_flag
        FROM ftd_apps.addon faa, ftd_apps.addon_type faat
        WHERE faat.addon_type_id = faa.addon_type
        ORDER BY faat.description, faa.active_flag desc, faa.description;
END;
$$

delimiter ;