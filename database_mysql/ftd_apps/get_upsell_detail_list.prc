drop procedure if exists ftd_apps.get_upsell_detail_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_upsell_detail_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning upsell detail data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT upper(ud.upsell_master_id) upsell_master_id,
    	   um.upsell_name,
    	   um.upsell_status,
    	   um.gbb_popover_flag,
    	   um.gbb_title_txt,
    	   upper(ud.upsell_detail_id) upsell_detail_id,
    	   ud.upsell_detail_name,
    	   ud.upsell_detail_sequence,
    	   pm.novator_id,
    	   pm.novator_name,
    	   pm.standard_price,
    	   ud.gbb_upsell_detail_sequence_num,
    	   ud.gbb_name_override_flag,
    	   ud.gbb_name_override_txt,
    	   ud.gbb_price_override_flag,
    	   ud.gbb_price_override_txt,
    	   ud.default_sku_flag
      FROM ftd_apps.upsell_detail ud
      JOIN ftd_apps.upsell_master um
        ON um.upsell_master_id = ud.upsell_master_id
      JOIN ftd_apps.product_master pm
        ON pm.product_id = ud.upsell_detail_id
      ORDER BY ud.upsell_master_id, ud.upsell_detail_sequence
      ;
  END;
$$

delimiter ;

