drop procedure if exists ftd_apps.get_company_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_company_list()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning company data.
Input:
	n/a
Output:
	cursor
-----------------------------------------------------------------------------*/
  BEGIN
    SELECT company_id,
    	   company_name,
    	   default_domain
      FROM ftd_apps.company_master;
  END;
$$

delimiter ;

