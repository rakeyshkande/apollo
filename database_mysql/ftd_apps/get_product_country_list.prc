drop procedure if exists ftd_apps.get_product_country_list;

delimiter $$

create DEFINER='ftd_apps'@'%' procedure ftd_apps.get_product_country_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product country data.
        Country is only sent in the default source code. When retrieve
        product country, ignore source code.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT DISTINCT UPPER(sip.product_id) as product_id,
      	   si.country_id,
    	   cm.name
      FROM si_template_index_prod sip
      JOIN si_template_index si
        ON sip.index_name = si.index_name
       AND si.si_template_id = sip.si_template_id
      JOIN ftd_apps.country_master cm
        ON cm.country_id = si.country_id
      JOIN ftd_apps.product_master pm
        ON pm.product_id = sip.product_id
     WHERE si.country_id IS NOT NULL
       and si.country_id <> 'US'
       and pm.delivery_type='I'
       AND cm.status = 'Active'
       and cm.oe_country_type = 'I'
       ;
  END;
$$

delimiter ;

