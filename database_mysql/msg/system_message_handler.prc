use msg;
drop procedure if exists msg.system_message_handler;
delimiter $$
create definer='mwoytus'@'%' procedure msg.system_message_handler(p_msg_id bigint(20))
modifies sql data sql security invoker
begin

 declare v_source varchar(100);
 declare v_timestamp timestamp;
 declare v_type varchar(100);
 declare v_computer varchar(256);
 declare v_message varchar(2000);
 declare v_email_subject varchar(100);

 declare v_location varchar(100);

 declare f_system_msg_filter_id bigint(20);
 declare f_filter_name varchar(1);
 declare f_active_flag varchar(1);
 declare f_msg_source varchar(100);
 declare f_msg_subject varchar(100);
 declare f_msg_body varchar(500);
 declare f_action_type varchar(50);
 declare f_nopage_start_hour varchar(2);
 declare f_nopage_end_hour varchar(2);
 declare f_subject_prefix_txt varchar(50);
 declare f_overwrite_distro_txt varchar(100);

 declare v_page_distro varchar(100) default 'SCRUB_ALERTS';
 declare v_nopage_distro varchar(100) default 'SCRUB_NOPAGE_ALERTS';
 declare v_action_suppress varchar(50) default 'SUPPRESS';
 declare v_action_nopage   varchar(50) default 'NOPAGE';
 declare v_action_overwrite_distro varchar(50) default 'OVERWRITE_DISTRO';
 declare v_action_mod_subj         varchar(50) default 'MODIFY_SUBJECT';
 declare v_suppress_flag           varchar(1) default 'N';
 declare v_nopage_flag             varchar(1) default 'N';
 declare v_nopage_start_hour       varchar(2);
 declare v_nopage_end_hour         varchar(2);
 declare v_send_distro             varchar(100);
 declare v_send_msg                varchar(3000);

 declare no_more_rows boolean;

 declare filter_cur cursor for
 SELECT f.FILTER_NAME,
        f.ACTIVE_FLAG,
        f.MSG_SOURCE,
        f.MSG_SUBJECT,
        f.MSG_BODY,
        f.ACTION_TYPE,
        f.NOPAGE_START_HOUR,
        f.NOPAGE_END_HOUR,
        f.SUBJECT_PREFIX_TXT,
        f.OVERWRITE_DISTRO_TXT
   FROM    frp.system_msg_filter f
   JOIN    frp.system_msg_action a
     ON   f.action_type = a.action_type
    AND   f.ACTION_TYPE in ('SUPPRESS','NOPAGE','OVERWRITE_DISTRO','MODIFY_SUBJECT')
       WHERE    f.active_flag = 'Y'
         AND    (
              ((msg_source is not null and msg_subject is not null and msg_body is not null)
              AND upper(v_source) like upper(concat('%',msg_source,'%'))
              AND upper(v_email_subject) like upper(concat('%',msg_subject,'%'))
              AND upper(v_message) like upper(concat('%',msg_body,'%'))
              )
              OR
              ((msg_source is not null and msg_subject is not null and msg_body is null)
              AND upper(v_source) like upper(concat('%',msg_source,'%'))
              AND upper(v_email_subject) like upper(concat('%',msg_subject,'%'))
              )   
              OR
              ((msg_source is not null and msg_subject is null and msg_body is not null)
              AND upper(v_source) like upper(concat('%',msg_source,'%'))
              AND upper(v_message) like upper(concat('%',msg_body,'%'))
              )     
              OR
              ((msg_source is null and msg_subject is not null and msg_body is not null)
              AND upper(v_email_subject) like upper(concat('%',msg_subject,'%'))
              AND upper(v_message) like upper(concat('%',msg_body,'%'))
              )   
              OR
              ((msg_source is not null and msg_subject is null and msg_body is null)
              AND upper(v_source) like upper(concat('%',msg_source,'%'))
              )
              OR
              ((msg_source is null and msg_subject is not null and msg_body is null)
              AND upper(v_email_subject) like upper(concat('%',msg_subject,'%'))
              )
              OR
              ((msg_source is null and msg_subject is null and msg_body is not null)
              AND upper(v_message) like upper(concat('%',msg_body,'%'))
              )              
            )
    ORDER BY    a.priority;

 declare continue handler for not found
  set no_more_rows = TRUE;

 select timestamp,   source,   type,   computer,   message, ifnull(email_subject,'FloristService System Message')
 into v_timestamp, v_source, v_type, v_computer, v_message, v_email_subject
 from   msg.system_messages
 where  system_message_id = p_msg_id;

 set v_send_distro = v_page_distro;
 set v_send_msg = v_message;

 select concat(a.variable_value,':',b.variable_value)
 into   v_location
 from   information_schema.global_variables a, information_schema.global_variables b
 where  a.variable_name = 'hostname' and b.variable_name = 'port';

 open filter_cur;
 the_loop: loop
  fetch filter_cur into f_filter_name, f_active_flag, f_msg_source, f_msg_subject, f_msg_body, f_action_type, f_nopage_start_hour,
  f_nopage_end_hour, f_subject_prefix_txt, f_overwrite_distro_txt;
  if no_more_rows then
   leave the_loop;
  end if;
  if f_action_type = v_action_suppress then
   set v_suppress_flag = 'Y';
   leave the_loop;
  elseif f_action_type = v_action_nopage then
   set v_nopage_flag = 'Y';
  elseif f_action_type = v_action_overwrite_distro then
   if f_overwrite_distro_txt = '##TOKEN_SOURCE##' then
    set v_send_distro = v_source;
   else
    set v_send_distro = f_overwrite_distro_txt;
   end if;
  elseif f_action_type = v_action_mod_subj then
   set v_email_subject = concat(f_subject_prefix_txt,' ',v_email_subject);
  end if;
 end loop the_loop;

 if v_nopage_flag = 'N' then
  set v_send_msg = concat('Database: ',v_location,unhex('A'),v_message);
 else
  set v_send_distro = v_nopage_distro;
  set v_email_subject = concat('NOPAGE ',v_email_subject);
 end if;

 if v_suppress_flag != 'Y' then
  select concat('##DISTRO##',v_send_distro);
  select concat('##SUBJEC##',v_email_subject);
  select v_timestamp;
  select concat('Source: ',v_source);
  select concat('Type: ',v_type);
  select concat('Host: ',v_computer);
  select v_send_msg;
 end if;

 update system_messages set sent='Y' where system_message_id = p_msg_id;
 commit;
end$$
delimiter ;
grant execute on procedure msg.system_message_handler to 'script_user'@'localhost';
flush privileges;

