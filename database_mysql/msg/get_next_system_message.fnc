use msg;
drop function  if exists msg.get_next_system_message;
delimiter $$
create definer='script_user'@'localhost' function get_next_system_message()
returns bigint(20) reads sql data sql security invoker
begin
 declare no_more_rows boolean;
 declare v_system_message_id bigint(20);
 declare c1 cursor for
  select system_message_id
  from   system_messages
  where  timestamp > subtime(sysdate(),'1 00:00:00.0') and sent = 'N'
  limit 1;
 declare continue handler for not found
  set no_more_rows = TRUE;
 open c1;
 fetch c1 into v_system_message_id;
 if no_more_rows then
  select null into v_system_message_id;
 end if;
 close c1;
 return v_system_message_id;
end$$
delimiter ;
grant execute on function msg.get_next_system_message to 'script_user'@'localhost';
flush privileges;

