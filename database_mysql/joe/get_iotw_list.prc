drop procedure if exists joe.get_iotw_list;


delimiter $$

create DEFINER='joe'@'%' procedure joe.get_iotw_list ()
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning IOTW data.
Input:
	n/a
Output:
	cursor

-----------------------------------------------------------------------------*/
  BEGIN
    SELECT ji.iotw_id,
    	   ji.product_id,
    	   ji.source_code,
    	   ji.start_date,
    	   ji.end_date,
    	   ji.iotw_source_code,
    	   ji.product_page_messaging_txt
      FROM joe.iotw ji
      WHERE ji.start_date <= curdate()
      AND (ji.end_date is null or ji.end_date >= curdate())
      AND ji.product_id is not null
      UNION 
    SELECT ji.iotw_id,
    	   ji.upsell_master_id product_id,
    	   ji.source_code,
    	   ji.start_date,
    	   ji.end_date,
    	   ji.iotw_source_code,
    	   ji.product_page_messaging_txt
      FROM joe.iotw ji
      WHERE ji.start_date <= curdate()
      AND (ji.end_date is null or ji.end_date >= curdate())      
      AND ji.upsell_master_id is not null
      ORDER BY 3, 2, 4;
     
  END;
$$

delimiter ;

