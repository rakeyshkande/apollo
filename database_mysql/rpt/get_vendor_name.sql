DELIMITER $$

USE `rpt`$$

DROP FUNCTION IF EXISTS `get_vendor_name`$$

CREATE FUNCTION `get_vendor_name`(p_vendor_id VARCHAR(50)) RETURNS VARCHAR(50) CHARSET utf8 SQL SECURITY INVOKER
BEGIN
    
	DECLARE p_vendor_name  VARCHAR(50);
	DECLARE lchr_filling_vendor VARCHAR(50);
	SET p_vendor_name = ' ';
	SET lchr_filling_vendor = ' ';
	
	IF UPPER(p_vendor_id) = 'ALL' THEN
	SET lchr_filling_vendor = 'ALL';
	ELSEIF p_vendor_id IS NOT NULL THEN
	SET  lchr_filling_vendor = CONCAT(SUBSTRING(UPPER(p_vendor_id), 1, 2) , '-' , SUBSTRING(UPPER(p_vendor_id), 3));
	ELSE
	SET lchr_filling_vendor = p_vendor_id ;
	END IF;
	
	IF UPPER(p_vendor_id) = 'ALL' THEN
	SET p_vendor_name = 'ALL';
	ELSE
	
	SELECT CONCAT(vendor_name,' ( ', lchr_filling_vendor,' )') INTO p_vendor_name FROM ftd_apps.vendor_master WHERE member_number = lchr_filling_vendor;
	END IF;
	
	RETURN p_vendor_name;
    END$$

DELIMITER ;
