delete from ftd_apps.order_service_xsl;

insert into ftd_apps.order_service_xsl values (
'Generic GetOccasion',
'XSL to retrieve an Occasion for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
<xsl:element name="occasions">
<xsl:for-each select="com.ftd.orderservice.common.vo.response.GetOccasionResponseVO/occasionList/com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO">
<xsl:element name="Occasion">
<xsl:element name="value">
<xsl:value-of select="indexId"/>
</xsl:element>
<xsl:element name="description">
<xsl:value-of select="indexName"/>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Generic GetProductByCategory', 'XSL to retrieve products based on a category for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/com.ftd.orderservice.common.vo.response.GetProductsByCategoryResponseVO">
<xsl:element name="getCategories">
<xsl:element name="Category">
<xsl:element name="uri">
<xsl:text>arc://category/</xsl:text>
<xsl:value-of select="requestVO/categoryId"/>
</xsl:element>
<xsl:element name="title">
<xsl:value-of select="indexName"/>
</xsl:element>
<xsl:element name="childProducts">
<xsl:for-each select="productMasterList/com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO">
<xsl:element name="Product">
<xsl:element name="uri">
<xsl:text>arc://product/</xsl:text>
<xsl:choose>
<xsl:when test = "upsellMasterVO">
<xsl:value-of select="upsellMasterVO/upsellMasterId"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="webProductId"/>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/09 08:21:06','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Generic GetError','XSL to format error message for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no" cdata-section-elements="message"/>
<xsl:template match="*">
<xsl:element name="error">
<xsl:element name="code">
<xsl:value-of select="errorCode"/>
</xsl:element>
<xsl:element name="message">
<xsl:value-of select="errorMessage" disable-output-escaping="yes"/>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08', '%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/08 01:41:08', '%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Generic GetProducts','XSL to retrieve products for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no" cdata-section-elements="title description"/>
<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:template match="/">
<xsl:element name="getProducts">
<xsl:for-each select="com.ftd.orderservice.common.vo.response.GetProductResponseVO/productMasterList/com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO">
<xsl:element name="Product">
<xsl:if test="(hasGBB != ''true'' or ../../requestVO/loadLevel = ''display'')">
<xsl:element name="productId">
<xsl:value-of select="webProductId"/>
</xsl:element>
</xsl:if>
<xsl:element name="uri">
<xsl:text>arc://product/</xsl:text>
<xsl:value-of select="webProductId"/>
</xsl:element>
<xsl:element name="title">
<xsl:value-of select="webProductName" disable-output-escaping="yes"/>
</xsl:element>
<xsl:element name="description">
<xsl:value-of select="longDesc" disable-output-escaping="yes"/>
</xsl:element>
<xsl:element name="prices">
<xsl:for-each select="pricingList/com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO">
<xsl:element name="Price">
<xsl:element name="amount">
<xsl:value-of select="priceAmount"/>
</xsl:element>
<xsl:element name="priceType">
<xsl:value-of select="translate(priceType,$ucletters,$lcletters)"/>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
<xsl:if test="not(pricingList/com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO[priceType=''SALE''])">
<xsl:for-each select="pricingList/com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO[priceType=''BASE'']">
<xsl:if test="rewardAmount">
<xsl:element name="promo">
<xsl:element name="Promo">
<xsl:element name="body">
<xsl:value-of select="rewardAmount"/><xsl:text> </xsl:text><xsl:value-of select="rewardType"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:if>
</xsl:for-each>
</xsl:if>
<xsl:element name="customProperties">
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>ageVerificationRequired</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:choose>
<xsl:when test="over21Flag = ''Y''">
<xsl:text>true</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>false</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>personalizationRequired</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:choose>
<xsl:when test="personalizationFlag = ''Y''">
<xsl:text>true</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>false</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>deliveryMethod</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:value-of select="translate(deliveryMethod,$ucletters,$lcletters)"/>
</xsl:element>
</xsl:element>
<xsl:if test = "IOTWMessaging">
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>saleText</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:value-of select="IOTWMessaging"/>
</xsl:element>
</xsl:element>
</xsl:if>
</xsl:element>
<xsl:element name="structure">
<xsl:element name="ProductStructure">
<xsl:element name="type">
<xsl:text>alt</xsl:text>
</xsl:element>
<xsl:element name="dimensions">
<xsl:element name="Dimension">
<xsl:element name="property">
<xsl:text>http://www.allurent.com/allurent/2.0/size</xsl:text>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="assetViews">
<xsl:element name="AssetView">
<xsl:element name="name">
<xsl:text>main</xsl:text>
</xsl:element>
<xsl:element name="assets">
<xsl:element name="Asset">
<xsl:element name="role">
<xsl:text>standard</xsl:text>
</xsl:element>
<xsl:element name="identifier">
<xsl:for-each select="imageList/com.ftd.osp.utilities.cacheMgr.handlers.vo.ImageVO">
<xsl:choose>
<xsl:when test="imageType = ''CATEGORY'' and ../../../../requestVO/loadLevel = ''display''">
<xsl:value-of select="imageURL"/>
</xsl:when>
<xsl:when test="imageType = ''PRODUCT'' and ../../../../requestVO/loadLevel = ''details''">
<xsl:value-of select="imageURL"/>
</xsl:when>
</xsl:choose>
</xsl:for-each>
</xsl:element>
<xsl:element name="format">
<xsl:value-of select="imageFormat"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:if test="../../requestVO/loadLevel != ''display''">
<xsl:element name="childProducts">
<xsl:for-each select="gbbList/com.ftd.osp.utilities.cacheMgr.handlers.vo.GBBVO">
<xsl:element name="Sku">
<xsl:element name="skuId">
<xsl:value-of select="webProductId"/>
</xsl:element>
<xsl:if test="../../hasUpsell = ''true''">
<xsl:element name="uri">
<xsl:text>arc://sku/</xsl:text>
<xsl:value-of select="webProductId"/>
</xsl:element>
</xsl:if>
<xsl:element name="title">
<xsl:value-of select="webProductName"/>
</xsl:element>
<xsl:element name="size">
<xsl:value-of select="gbbNameOverrideTxt"/>
</xsl:element>
<xsl:element name="sizeDescription">
<xsl:value-of select="gbbNameOverrideTxt"/>
</xsl:element>
<xsl:element name="sizeDisplaySequence">
<xsl:value-of select="displaySequence"/>
</xsl:element>
<xsl:element name="assetViews">
<xsl:element name="AssetView">
<xsl:element name="name">
<xsl:text>main</xsl:text>
</xsl:element>
<xsl:element name="assets">
<xsl:for-each select="imageList/com.ftd.osp.utilities.cacheMgr.handlers.vo.ImageVO">
<xsl:if test="imageType !=''CATEGORY''">
<xsl:element name="Asset">
<xsl:element name="role">
<xsl:choose>
<xsl:when test="imageType = ''PRODUCT''">
<xsl:text>standard</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="translate(imageType,$ucletters,$lcletters)"/>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
<xsl:element name="identifier">
<xsl:value-of select="imageURL"/>
</xsl:element>
<xsl:element name="format">
<xsl:value-of select="imageFormat"/>
</xsl:element>
</xsl:element>
</xsl:if>
</xsl:for-each>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="prices">
<xsl:for-each select="pricingList/com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO">
<xsl:element name="Price">
<xsl:element name="amount">
<xsl:value-of select="priceAmount"/>
</xsl:element>
<xsl:element name="priceType">
<xsl:value-of select="translate(priceType,$ucletters,$lcletters)"/>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
<xsl:for-each select="pricingList/com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO[priceType=''BASE'']">
<xsl:if test="rewardAmount">
<xsl:element name="promo">
<xsl:element name="Promo">
<xsl:element name="body">
<xsl:value-of select="rewardAmount"/><xsl:text> </xsl:text><xsl:value-of select="rewardType"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:if>
</xsl:for-each>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:if>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/25 10:11:07','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Generic GetAddons','XSL to retrieve add-ons for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no" cdata-section-elements="title description longDescription"/>
<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:template match="/">
<xsl:element name="getAddOns">
<xsl:for-each select="com.ftd.orderservice.common.vo.response.GetAddonResponseVO/addonList/com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO[addonType != ''card'']">
<xsl:element name="Product">
<xsl:element name="productId">
<xsl:value-of select="addonId"/>
</xsl:element>
<xsl:element name="title">
<xsl:value-of select="addonTitle" disable-output-escaping="yes"/>
</xsl:element>
<xsl:element name="description">
<xsl:value-of select="addonDesc" disable-output-escaping="yes"/>
</xsl:element>
<xsl:element name="longDescription">
<xsl:value-of select="addonText" disable-output-escaping="yes"/>
</xsl:element>
<xsl:element name="maxPurchaseQuantity">
<xsl:value-of select="maxPurchaseQty"/>
</xsl:element>
<xsl:element name="prices">
<xsl:element name="Price">
<xsl:element name="amount">
<xsl:value-of select="addonPrice"/>
</xsl:element>
<xsl:element name="priceType">
<xsl:value-of select="translate(priceType,$ucletters,$lcletters)"/>
</xsl:element>
<xsl:element name="pricingUnit">
<xsl:value-of select="priceUnit"/>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="customProperties">
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>displayPrice</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:choose>
<xsl:when test="displayPriceFlag = ''Y''">
<xsl:text>true</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>false</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>type</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:choose>
<xsl:when test="addonType !=''vase''">
<xsl:text>gift</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="addonType"/>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="structure">
<xsl:element name="ProductStructure">
<xsl:element name="type">
<xsl:text>alt</xsl:text>
</xsl:element>
<xsl:element name="dimensions">
<xsl:element name="Dimension">
<xsl:element name="property">
<xsl:text>http://www.allurent.com/allurent/2.0/size</xsl:text>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="assetViews">
<xsl:element name="AssetView">
<xsl:element name="name">
<xsl:text>main</xsl:text>
</xsl:element>
<xsl:element name="assets">
<xsl:element name="Asset">
<xsl:element name="role">
<xsl:value-of select="translate(image/imageType,$ucletters,$lcletters)"/>
</xsl:element>
<xsl:element name="identifier">
<xsl:value-of select="image/imageURL"/>
</xsl:element>
<xsl:element name="format">
<xsl:value-of select="image/imageFormat"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:for-each>
<!-- cards -->
<xsl:for-each select="com.ftd.orderservice.common.vo.response.GetAddonResponseVO/addonList/com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO[addonType = ''card''][position()=1]">
<xsl:element name="Product">
<xsl:element name="title">
<xsl:text><![CDATA[Greeting Cards]]></xsl:text>
</xsl:element>
<xsl:element name="description">
<xsl:text><![CDATA[Add a greeting card]]></xsl:text>
</xsl:element>
<xsl:element name="maxPurchaseQuantity">
<xsl:value-of select="maxPurchaseQty"/>
</xsl:element>
<xsl:element name="prices">
<xsl:element name="Price">
<xsl:element name="amount">
<xsl:value-of select="addonPrice"/>
</xsl:element>
<xsl:element name="priceType">
<xsl:value-of select="translate(priceType,$ucletters,$lcletters)"/>
</xsl:element>
<xsl:element name="pricingUnit">
<xsl:value-of select="priceUnit"/>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="customProperties">
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>displayPrice</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:choose>
<xsl:when test="displayPriceFlag = ''Y''">
<xsl:text>true</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>false</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
<xsl:element name="Property">
<xsl:element name="name">
<xsl:text>type</xsl:text>
</xsl:element>
<xsl:element name="value">
<xsl:text>gift</xsl:text>
</xsl:element>
</xsl:element>
</xsl:element>
<xsl:element name="childProducts">
<xsl:for-each select="../com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO[addonType = ''card'']">
<xsl:element name="Sku">
<xsl:element name="skuId">
<xsl:value-of select="addonId"/>
</xsl:element>
<xsl:element name="title">
<xsl:value-of select="occasionDesc"/>
</xsl:element>
<xsl:element name="assetViews">
<xsl:element name="AssetView">
<xsl:element name="name">
<xsl:text>main</xsl:text>
</xsl:element>
<xsl:element name="assets">
<xsl:element name="Asset">
<xsl:element name="role">
<xsl:value-of select="translate(image/imageType,$ucletters,$lcletters)"/>
</xsl:element>
<xsl:element name="identifier">
<xsl:value-of select="image/imageURL"/>
</xsl:element>
<xsl:element name="format">
<xsl:value-of select="image/imageFormat"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
<xsl:element name="assetViews">
<xsl:element name="AssetView">
<xsl:element name="name">
<xsl:text>main</xsl:text>
</xsl:element>
<xsl:element name="assets">
<xsl:element name="Asset">
<xsl:element name="role">
<xsl:value-of select="translate(image/imageType,$ucletters,$lcletters)"/>
</xsl:element>
<xsl:element name="identifier">
<xsl:value-of select="image/imageURL"/>
</xsl:element>
<xsl:element name="format">
<xsl:value-of select="image/imageFormat"/>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Generic GetDeliveryDate','XSL to retrieve delivery dates for Allurent',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/">
<xsl:element name="deliveryDates">
<xsl:for-each select="com.ftd.orderservice.common.vo.response.GetAvailableDaysResponseVO/availDateList/com.ftd.osp.utilities.cacheMgr.handlers.vo.AvailableDateVO">
<xsl:element name="DeliveryDate">
<xsl:choose>
<xsl:when test="endDate">
<xsl:element name="startDate">
<xsl:value-of select="translate(substring(deliveryDate,1,10),''-'',''/'')"/>
</xsl:element>
<xsl:element name="endDate">
<xsl:value-of select="translate(substring(endDate,1,10),''-'',''/'')"/>
</xsl:element>
</xsl:when>
<xsl:otherwise>
<xsl:element name="date">
<xsl:value-of select="translate(substring(deliveryDate,1,10),''-'',''/'')"/>
</xsl:element>
</xsl:otherwise>
</xsl:choose>
<xsl:element name="description">
<xsl:value-of select="description"/>
</xsl:element>
<xsl:element name="message">
<xsl:value-of select="message"/>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

insert into ftd_apps.order_service_xsl values (
'Identity Transformation','XSL that will display the data that is passed to it',
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no" />
<xsl:template match="@*|node()">
   <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
   </xsl:copy>
</xsl:template>
</xsl:stylesheet>',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0',
str_to_date('2010/10/08 01:41:08','%Y/%m/%d %H:%i:%s'), 'Defect_6414_Release_4_6_0');

