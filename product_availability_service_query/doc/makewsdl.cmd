ECHO setting axis classpath 
CALL setaxiscp.bat

ECHO Creating WSDL from interface
CALL java2wsdl.bat -o pas.wsdl -l "http://zinc3:8080/product-availability/ProductAvailability" -n "urn:productAvailability" -p"com.ftd.pas.webservices"="urn:productAvailability" -i "com.ftd.pas.server.service.impl.PASRequestServiceImpl" com.ftd.pas.common.service.PASRequestService
