@ECHO OFF  
ECHO setting axis classpath 
CALL setaxiscp.bat
ECHO Creating java classes from WSDL 
CALL wsdl2java.bat -o ..\src -d Session -s -S true -N"urn:productAvailability" "com.ftd.pas.webservices"  -c "com.ftd.pas.server.service.impl.PASRequestServiceImpl" -t pas.wsdl
