package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

public class PASCheckForm extends ActionForm
{
    private String productId;
    private String deliveryDate;
    private String zipCode;
    private String stateCode;
    private String countryCode;
    private String addOns;
        
    public PASCheckForm()
    {
        
    }


    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setDeliveryDate(String param)
    {
        this.deliveryDate = param;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setStateCode(String param)
    {
        this.stateCode = param;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }


	public String getAddOns() {
		return addOns;
	}


	public void setAddOns(String addOns) {
		this.addOns = addOns;
	}
    
    
}
