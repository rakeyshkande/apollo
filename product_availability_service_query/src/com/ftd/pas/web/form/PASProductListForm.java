package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

public class PASProductListForm extends ActionForm
{
    private String deliveryDate;
    private String zipCode;
    private String countryCode;
    private String productList;
        
    public PASProductListForm()
    {
        
    }


    public void setDeliveryDate(String param)
    {
        this.deliveryDate = param;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setProductList(String productList) {
        this.productList = productList;
    }

    public String getProductList() {
        return productList;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
