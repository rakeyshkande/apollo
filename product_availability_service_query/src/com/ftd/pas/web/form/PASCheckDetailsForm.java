package com.ftd.pas.web.form;

import com.ftd.pas.common.vo.PASCountryVO;
import com.ftd.pas.common.vo.PASFloristProductZipVO;
import com.ftd.pas.common.vo.PASProductStateVO;
import com.ftd.pas.common.vo.PASProductVO;

import com.ftd.pas.common.vo.PASVendorProductStateVO;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class PASCheckDetailsForm extends ActionForm
{
    
    // Form Stuff
    private String productId;
    private String deliveryDate;
    private String zipCode;
    private String stateCode;
    private int daysBack;

    // Vendor Stuff
    private List pasProductDateList;
    private List pasProductStateDateList;
    private List pasVendorProductStateDateList;

    // Florist
    private List pasFloristProductZipDateList;

    private List pasCountryDateList;

    public PASCheckDetailsForm()
    {
        pasProductDateList = new ArrayList();
        pasProductStateDateList = new ArrayList();
        pasVendorProductStateDateList = new ArrayList();
        pasCountryDateList = new ArrayList();
        pasFloristProductZipDateList = new ArrayList();
    }

    public void setPasProductDateList(List param)
    {
        this.pasProductDateList = param;
    }

    public List getPasProductDateList()
    {
        return pasProductDateList;
    }

    public void setPasProductStateDateList(List param)
    {
        this.pasProductStateDateList = param;
    }

    public List getPasProductStateDateList()
    {
        return pasProductStateDateList;
    }

    public void setPasVendorProductStateDateList(List param)
    {
        this.pasVendorProductStateDateList = param;
    }

    public List getPasVendorProductStateDateList()
    {
        return pasVendorProductStateDateList;
    }

    public void setPasFloristProductZipDateList(List param)
    {
        this.pasFloristProductZipDateList = param;
    }

    public List getPasFloristProductZipDateList()
    {
        return pasFloristProductZipDateList;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setDeliveryDate(String param)
    {
        this.deliveryDate = param;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void addProduct(PASProductVO vo)
    {
        // These can be null, but we don't want them, so filter them out
        if (vo != null)
        {
            pasProductDateList.add(vo);
        }
    }
    public void addVendorProductState(PASVendorProductStateVO vo)
    {
        // These can be null, but we don't want them, so filter them out
        if (vo != null)
        {
            pasVendorProductStateDateList.add(vo);
        }
    }
    public void addVendorProductState(List<PASVendorProductStateVO> voList)
    {
        // These can be null, but we don't want them, so filter them out
        if (voList != null && voList.size() > 0)
        {
            pasVendorProductStateDateList.addAll(voList);
        }
    }
    public void addFloristProductZip(List<PASFloristProductZipVO> voList)
    {
        // These can be null, but we don't want them, so filter them out
        if (voList != null && voList.size() > 0)
        {
            pasFloristProductZipDateList.addAll(voList);
        }
    }
    public void addProductState(PASProductStateVO vo)
    {
        // These can be null, but we don't want them, so filter them out
        if (vo != null)
        {
            pasProductStateDateList.add(vo);
        }
    }

    public void addCountry(PASCountryVO vo)
    {
        // These can be null, but we don't want them, so filter them out
        if (vo != null)
        {
            pasCountryDateList.add(vo);
        }
    }

    public void setStateCode(String param)
    {
        this.stateCode = param;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public void setPasCountryDateList(List param)
    {
        this.pasCountryDateList = param;
    }

    public List getPasCountryDateList()
    {
        return pasCountryDateList;
    }

    public void setDaysBack(int param)
    {
        this.daysBack = param;
    }

    public int getDaysBack()
    {
        return daysBack;
    }
}
