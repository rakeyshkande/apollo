package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RoutableFloristCheckForm extends ActionForm
{
    private String zipCode;
    private String productId;
    private String deliveryDate;
    private String deliveryDateEnd;
    private String sourceCode;
    private int    orderDetailId;
    private String deliveryCity;
    private String deliveryState;
    private String orderValue;
    private String returnAll;

    SimpleDateFormat sdfmmddyyyy = new SimpleDateFormat("MM/dd/yyyy");

    public RoutableFloristCheckForm()
    {
    }

    public void setDefaults()
    {
        // Some Default Values
        zipCode = "60148";
        productId = "8212";
        Date today = new Date();
        String todayString = sdfmmddyyyy.format(today);
        deliveryDate = todayString;
        deliveryDateEnd = todayString;
        sourceCode = "9999";
        orderDetailId = 1651303;
        deliveryCity = "Lombard";
        deliveryState = "IL";
        orderValue = "40";
        returnAll = "Y";

    }


    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDateEnd() {
        return deliveryDateEnd;
    }

    public void setDeliveryDateEnd(String deliveryDateEnd) {
        this.deliveryDateEnd = deliveryDateEnd;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getDeliveryState() {
        return deliveryState;
    }

    public void setDeliveryState(String deliveryState) {
        this.deliveryState = deliveryState;
    }

    public String getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(String orderValue) {
        this.orderValue = orderValue;
    }

    public String getReturnAll() {
        return returnAll;
    }

    public void setReturnAll(String returnAll) {
        this.returnAll = returnAll;
    }
}