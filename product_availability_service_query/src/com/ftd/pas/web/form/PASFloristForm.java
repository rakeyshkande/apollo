package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

public class PASFloristForm extends ActionForm
{
    private String deliveryDate;
    private String zipCode;

    public PASFloristForm()
    {
    }

    public void setDeliveryDate(String param)
    {
        this.deliveryDate = param;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

}
