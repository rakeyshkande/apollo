package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

public class PASCheckPopularForm extends ActionForm
{
    private String deliveryDate;
    private String zipCode;
    private String companyId;
    private int numberResults = 25;

    public PASCheckPopularForm()
    {

    }

    public void setDeliveryDate(String param)
    {
        this.deliveryDate = param;
    }

    public String getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setNumberResults(int param)
    {
        this.numberResults = param;
    }

    public int getNumberResults()
    {
        return numberResults;
    }

    public void setCompanyId(String param)
    {
        this.companyId = param;
    }

    public String getCompanyId()
    {
        return companyId;
    }
}
