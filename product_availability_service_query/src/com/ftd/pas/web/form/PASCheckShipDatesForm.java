package com.ftd.pas.web.form;

import org.apache.struts.action.ActionForm;

public class PASCheckShipDatesForm extends ActionForm
{
    private String zipCode;
    private String productId;
    private int numberDays = 25;

    public PASCheckShipDatesForm()
    {

    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setNumberDays(int param)
    {
        this.numberDays = param;
    }

    public int getNumberDays()
    {
        return numberDays;
    }
}
