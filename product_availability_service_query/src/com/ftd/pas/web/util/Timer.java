package com.ftd.pas.web.util;

import java.util.Date;

public class Timer
{
    Date timerStart;
    Date timerStop;

    public Timer()
    {
        timerStart = new Date();
    }

    public void stop()
    {
        timerStop = new Date();
    }

    public long getMilliseconds()
    {
        return timerStop.getTime() - timerStart.getTime();
    }
}
