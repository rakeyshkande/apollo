package com.ftd.pas.web.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.form.PASMySQLForm;
import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASMySQLAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.pas.web.action.PASMySQLAction");

    /**
     * Perform a product availability check and return the results.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) throws IOException, ServletException, Exception {

        PASMySQLForm pasMySQLForm = (PASMySQLForm) form;
        logger.debug("Start ");
        
        try {
            String delDate = pasMySQLForm.getDeliveryDate();
            String productId = pasMySQLForm.getProductId();
            String zipCode = pasMySQLForm.getZipCode();
            String countryCode = pasMySQLForm.getCountryCode();
            String productList = pasMySQLForm.getProductList();
            String[] newList = productList.split("\\s");
            logger.debug("FORM " + delDate + " " + productId + " " + zipCode + " " + countryCode);

            PASRequestServiceImpl pas = new PASRequestServiceImpl();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            
            Date deliveryDate = sdf.parse(pasMySQLForm.getDeliveryDate());
            Calendar deliveryDateCal = Calendar.getInstance();
            deliveryDateCal.setTime(deliveryDate);

            String[] returnList = null;
            
            if (productList != null && !productList.equals("")) {
                Timer productListTimer = new Timer();
                if (countryCode == null || countryCode.equals("")) {
                    logger.info("product list");
                    returnList = pas.isProductListAvailable(zipCode, deliveryDateCal, newList);
                } else {
                    logger.info("international product list");
                    returnList = pas.isInternationalProductListAvailable(countryCode, deliveryDateCal, newList);
                }
                productListTimer.stop();
                request.setAttribute("pas_getProductListTimer", productListTimer.getMilliseconds());
                request.setAttribute("availableProductsList", returnList);
            }
            if (productId != null && !productId.equals("")) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String maxDaysString = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                int maxDays = 0;
                if (maxDaysString != null) {
                    try {
                        maxDays = Integer.valueOf(maxDaysString);
                    } catch (NumberFormatException e) {
                        maxDays = 0;
                    }
                }
                ProductAvailVO[] availableDates = null;
                Timer productDatesTimer = new Timer();
                if (countryCode == null || countryCode.equals("")) {
                    logger.info("product availability");
                    availableDates = pas.getProductAvailableDates(productId, zipCode, maxDays);
                } else {
                    logger.info("international product availability");
                    availableDates = pas.getInternationalProductAvailableDates(productId, countryCode, maxDays);
                }
                productDatesTimer.stop();
                request.setAttribute("pas_getProductDatesTimer", productDatesTimer.getMilliseconds());
                request.setAttribute("availableDates", availableDates);
            }
        } catch (Throwable t) {
            logger.error("Error in PAS Processing",t);
        }

        request.setAttribute("pasMySQLForm",pasMySQLForm);
        return mapping.findForward("success");

    }
    
}
