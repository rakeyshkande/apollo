package com.ftd.pas.web.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.RoutableFloristVO;
import com.ftd.pas.server.dao.PASDAO;
import com.ftd.pas.web.form.RoutableFloristCheckForm;
import com.ftd.pas.web.util.Timer;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class RoutableFloristCheckAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.pas.web.action.RoutableFloristCheckAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {
        RoutableFloristCheckForm routableFloristCheckForm = (RoutableFloristCheckForm) form;
        logger.debug("Start ");

        if (routableFloristCheckForm != null && routableFloristCheckForm.getProductId() != null && !routableFloristCheckForm.getProductId().equals(""))
        {
            logger.debug("FORM "+ routableFloristCheckForm.getProductId() + " " + routableFloristCheckForm.getDeliveryDate() + " " + routableFloristCheckForm.getZipCode());
            performRWDStuff(request, routableFloristCheckForm);
        }
        else
        {
            routableFloristCheckForm.setDefaults();
            logger.debug("FORM "+ routableFloristCheckForm.getProductId() + " " + routableFloristCheckForm.getDeliveryDate() + " " + routableFloristCheckForm.getZipCode());
        }
        request.setAttribute("routableFloristCheckForm", routableFloristCheckForm);

        return mapping.findForward("success");
    }

    private void performRWDStuff(HttpServletRequest request, RoutableFloristCheckForm routableFloristCheckForm)
    {
        try
        {
            // Cheating, going right to the DAO
            PASDAO pasDao = new PASDAO();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            Date deliveryDate = sdf.parse(routableFloristCheckForm.getDeliveryDate());
            Date deliveryDateEnd = null;
            if (routableFloristCheckForm.getDeliveryDateEnd() != null && !routableFloristCheckForm.getDeliveryDateEnd().equals("") &&
            		!routableFloristCheckForm.getDeliveryDate().equals(routableFloristCheckForm.getDeliveryDateEnd())) {
                logger.debug("Date range: " + routableFloristCheckForm.getDeliveryDateEnd());
            	deliveryDateEnd = sdf.parse(routableFloristCheckForm.getDeliveryDateEnd());
            }

            // Do the PAS check
            Timer productAvailTimer = new Timer();
            List<RoutableFloristVO> routableFloristList = pasDao.getRoutableFlorists(
                    getNewConnection(),
                    routableFloristCheckForm.getZipCode(),
                    routableFloristCheckForm.getProductId(),
                    deliveryDate,
                    deliveryDateEnd,
                    routableFloristCheckForm.getSourceCode(),
                    routableFloristCheckForm.getOrderDetailId(),
                    routableFloristCheckForm.getDeliveryCity(),
                    routableFloristCheckForm.getDeliveryState(),
                    routableFloristCheckForm.getOrderValue(),
                    routableFloristCheckForm.getReturnAll()
                    );
            productAvailTimer.stop();
            request.setAttribute("routableFloristTime",productAvailTimer.getMilliseconds());

            request.setAttribute("routableFloristList",routableFloristList);
        }
        catch (Throwable t)
        {
            logger.error("Error in RWD Processing",t);
        }

    }

    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection

        String datasource = ConfigurationUtil.getInstance().getProperty(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }


}