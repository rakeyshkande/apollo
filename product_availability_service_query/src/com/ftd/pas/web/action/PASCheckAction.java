package com.ftd.pas.web.action;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.ProductAvailabilityBO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.form.PASCheckForm;

import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASCheckAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.pas.web.action.PASCheckAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
    {
        PASCheckForm pasCheckForm = (PASCheckForm) form;
        logger.debug("Start ");
        
        if (pasCheckForm != null && pasCheckForm.getProductId() != null && !pasCheckForm.getProductId().equals(""))
        {
            logger.debug("FORM "+ pasCheckForm.getProductId() + " " + pasCheckForm.getDeliveryDate() + " " + pasCheckForm.getZipCode());
            performPASStuff(request,pasCheckForm);
        }
        request.setAttribute("pasCheckForm",pasCheckForm);
        
        return mapping.findForward("success");
    }
       
    private void performPASStuff(HttpServletRequest request, PASCheckForm pasCheckForm)
    {
    	
    	Connection con = null;
        try
        {
            PASRequestServiceImpl pas = new PASRequestServiceImpl();
            ProductAvailabilityBO paBO = new ProductAvailabilityBO();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            
            String[] addOnsSplit = null;
            
            Date deliveryDate = sdf.parse(pasCheckForm.getDeliveryDate());
            Calendar deliveryDateCal = Calendar.getInstance();
            deliveryDateCal.setTime(deliveryDate);
            
            if (pasCheckForm.getCountryCode() == null || pasCheckForm.getCountryCode().equals("")) {
                
            	
            	con = getNewConnection();
            	
            	String addOns = pasCheckForm.getAddOns();
            	if(addOns!=null && !addOns.isEmpty()){
            		 addOnsSplit = addOns.split(",");
            	}
            	
            	logger.info("In PAS CHEck FROM getProductAvailability");
            	// Do the PAS check
                logger.info("getProductAvailability");
                Timer productAvailTimer = new Timer();
                //ProductAvailVO productVO = pas.getProductAvailability(pasCheckForm.getProductId(),deliveryDateCal,pasCheckForm.getZipCode());
                ProductAvailVO productVO = paBO.getProductAvailability(con,pasCheckForm.getProductId(),deliveryDate,pasCheckForm.getZipCode(), ((addOnsSplit !=null)? Arrays.asList(addOnsSplit) : null),null);
                productAvailTimer.stop();
                request.setAttribute("pas_getProductAvailability",productAvailTimer.getMilliseconds());
    
                request.setAttribute("productVO",productVO);
            
                // get
                logger.info("getNextAvailableDeliveryDate");
                Timer nextAvailDDTimer = new Timer();
                logger.error("Sending null as source code from here*****");
                Date nextAvailDate = paBO.getNextAvailableDeliveryDate(con,pasCheckForm.getProductId(),pasCheckForm.getZipCode(),((addOnsSplit !=null)? Arrays.asList(addOnsSplit) : null),null);
                nextAvailDDTimer.stop();
                request.setAttribute("pas_nextAvailDDTimer",nextAvailDDTimer.getMilliseconds());
                if (nextAvailDate != null) {
                    request.setAttribute("nextAvailDate",nextAvailDate);
                }
            
                logger.info("getProductAvailableDates");
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String maxDaysString = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                int maxDays = 0;
                if (maxDaysString != null) {
                    try {
                        maxDays = Integer.valueOf(maxDaysString);
                    } catch (NumberFormatException e) {
                        maxDays = 0;
                    }
                }
                Timer productDatesTimer = new Timer();
                ProductAvailVO[] availableDates = paBO.getProductAvailableDates(con,pasCheckForm.getProductId(), pasCheckForm.getZipCode(),null, maxDays, ((addOnsSplit !=null)? Arrays.asList(addOnsSplit) : null));

                productDatesTimer.stop();
                request.setAttribute("pas_getProductDatesTimer", productDatesTimer.getMilliseconds());
                request.setAttribute("availableDates", (availableDates !=null ? availableDates : new ProductAvailVO[]{new ProductAvailVO()}) );

            } else {
                logger.info("getInternationalProductAvailability");
                Timer productAvailTimer = new Timer();
                //ProductAvailVO productVO = pas.getInternationalProductAvailability(pasCheckForm.getProductId(),deliveryDateCal,pasCheckForm.getZipCode());
                boolean isAvailable = pas.isInternationalProductAvailable(pasCheckForm.getProductId(), deliveryDateCal, pasCheckForm.getCountryCode());
                ProductAvailVO productVO = new ProductAvailVO();
                productVO.setProductId(pasCheckForm.getProductId());
                productVO.setIsAvailable(isAvailable);
                productAvailTimer.stop();
                request.setAttribute("pas_getProductAvailability",productAvailTimer.getMilliseconds());
                
                request.setAttribute("productVO",productVO);
                
                logger.info("getInternationalProductAvailableDates");
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String maxDaysString = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                int maxDays = 0;
                if (maxDaysString != null) {
                    try {
                        maxDays = Integer.valueOf(maxDaysString);
                    } catch (NumberFormatException e) {
                        maxDays = 0;
                    }
                }
                Timer productDatesTimer = new Timer();
                ProductAvailVO[] availableDates = pas.getInternationalProductAvailableDates(pasCheckForm.getProductId(), pasCheckForm.getCountryCode(), maxDays);
                productDatesTimer.stop();
                request.setAttribute("pas_getProductDatesTimer", productDatesTimer.getMilliseconds());
                request.setAttribute("availableDates", availableDates);
            }
        }
        catch (Throwable t)
        {
            logger.error("Error in PAS Processing",t);
        }
        finally {
            if( con!=null ) {
                try {
                    if( !con.isClosed() ) {
                        con.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

        

    }
    
    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection      
    
        String datasource = ConfigurationUtil.getInstance().getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }


}
