package com.ftd.pas.web.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.server.dao.PASDAO;
import com.ftd.pas.web.form.PASCheckDetailsForm;

import java.io.IOException;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASCheckDetailsAction extends Action
{
    private static Logger logger = new Logger("com.ftd.pas.web.action.PASCheckDetailsAction");
    private final static int DAYS_TO_SHOW = 5;

    /**
     * Get the product availability details for a product.  
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, Exception
    {
        PASCheckDetailsForm pasCheckDetailsForm = (PASCheckDetailsForm)form;
        logger.debug("Start ");

        if (pasCheckDetailsForm != null && pasCheckDetailsForm.getProductId() != null && !pasCheckDetailsForm.getProductId().equals(""))
        {
            logger.debug("FORM " + pasCheckDetailsForm.getProductId() + " " + pasCheckDetailsForm.getDeliveryDate() + " " + pasCheckDetailsForm.getZipCode());
            performPASStuff(request, pasCheckDetailsForm);

        }

        request.setAttribute("pasCheckDetailsForm", pasCheckDetailsForm);

        return mapping.findForward("success");
    }

    /**
     * Get the PAS data details.  This should really be in a BO, not here.
     * @param request
     * @param pasCheckDetailsForm
     */
    private void performPASStuff(HttpServletRequest request, PASCheckDetailsForm pasCheckDetailsForm)
    {
        Connection conn = null;
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            // Get a connection
            String datasource = ConfigurationUtil.getInstance().getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
            conn = DataSourceUtil.getInstance().getConnection(datasource);

            PASDAO dao = new PASDAO();

            Date deliveryDate = sdf.parse(pasCheckDetailsForm.getDeliveryDate());
            Calendar deliveryDateCal = Calendar.getInstance();

            int daysToShow = pasCheckDetailsForm.getDaysBack();
            if (daysToShow == 0)
            {
                daysToShow = DAYS_TO_SHOW;
            }
            deliveryDateCal.setTime(deliveryDate);
            for (int i=0; i < daysToShow; i++)
            {
                Date oneDayBack = deliveryDateCal.getTime();
                addDataForDate(pasCheckDetailsForm, oneDayBack, conn, dao);
                deliveryDateCal.add(Calendar.DAY_OF_YEAR, -1);
            }
            
        }
        catch (Throwable t)
        {
            logger.error("Error in PAS Processing", t);
        }
        finally 
        {
            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (Exception e)
            {
                logger.error("Error in PAS Processing", e);
            }
        }

    }
    
    private void addDataForDate(PASCheckDetailsForm form, Date deliveryDate, Connection conn, PASDAO dao) 
            throws Exception
    {
        form.addProduct(dao.getPASProduct(conn, form.getProductId(), deliveryDate));
        form.addProductState(dao.getPASProductState(conn, form.getProductId(), form.getStateCode(), deliveryDate));
        form.addCountry(dao.getPASCountry(conn,"US",deliveryDate));
        form.addVendorProductState(dao.getPASVendorProductState(conn, form.getProductId(), form.getStateCode(), deliveryDate));
        form.addFloristProductZip(dao.getPASFloristProductZip(conn, form.getProductId(), form.getZipCode(), deliveryDate));
    }
}
