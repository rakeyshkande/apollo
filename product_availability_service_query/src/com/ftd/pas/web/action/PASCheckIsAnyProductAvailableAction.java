package com.ftd.pas.web.action;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.form.PASCheckForm;

import com.ftd.pas.web.form.PASCheckPopularForm;

import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.io.PrintWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASCheckIsAnyProductAvailableAction extends Action
{
    private static Logger logger = new Logger("com.ftd.pas.web.action.PASCheckIsAnyProductAvailableAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, Exception
    {
        PASCheckForm pasCheckForm = (PASCheckForm)form;
        logger.debug("Start ");

        if (pasCheckForm != null)
        {
            logger.debug("FORM "  + pasCheckForm.getDeliveryDate() + " " + pasCheckForm.getZipCode());
            performPASStuff(request, pasCheckForm);

        }

        PrintWriter out = response.getWriter();

        out.print("IsAnyProductAvailable returning " + request.getAttribute("isAvailable") + " in " + request.getAttribute("pas_isAnyProductAvailable"));
        return null;
    }

    private void performPASStuff(HttpServletRequest request, PASCheckForm pasCheckForm)
    {
        try
        {
            PASRequestServiceImpl pas = new PASRequestServiceImpl();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            Date deliveryDate = sdf.parse(pasCheckForm.getDeliveryDate());
            Calendar deliveryDateCal = Calendar.getInstance();
            deliveryDateCal.setTime(deliveryDate);

            // Do the PAS check
            Timer productAvailTimer = new Timer();
            Boolean a = pas.isAnyProductAvailable(deliveryDateCal, pasCheckForm.getZipCode());
            productAvailTimer.stop();
            request.setAttribute("pas_isAnyProductAvailable", productAvailTimer.getMilliseconds());

            request.setAttribute("isAvailable", a.toString());

        }
        catch (Throwable t)
        {
            logger.error("Error in PAS Processing", t);
        }

    }

}
