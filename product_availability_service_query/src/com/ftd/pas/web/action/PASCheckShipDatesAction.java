package com.ftd.pas.web.action;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.form.PASCheckForm;
import com.ftd.pas.web.form.PASCheckPopularForm;
import com.ftd.pas.web.form.PASCheckShipDatesForm;
import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASCheckShipDatesAction extends Action
{
    private static Logger logger = new Logger("com.ftd.pas.web.action.PASCheckShipDatesAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, Exception
    {
        PASCheckShipDatesForm pasCheckShipDatesForm = (PASCheckShipDatesForm)form;
        logger.debug("Start ");

        if (pasCheckShipDatesForm != null && pasCheckShipDatesForm.getProductId() != null && !pasCheckShipDatesForm.getProductId().equals(""))
        {
            logger.debug("FORM " + pasCheckShipDatesForm.getProductId() + " " + pasCheckShipDatesForm.getZipCode());
            performPASStuff(request, pasCheckShipDatesForm);

        }

        request.setAttribute("pasCheckShipDatesForm", pasCheckShipDatesForm);

        return mapping.findForward("success");
    }

    private void performPASStuff(HttpServletRequest request, PASCheckShipDatesForm pasCheckShipDatesForm)
    {
        try
        {
            PASRequestServiceImpl pas = new PASRequestServiceImpl();

            // Do the PAS check
            Timer productAvailTimer = new Timer();
            PASShipDateAvailVO pasShipDateAvailVO = pas.getAvailableShipDates(pasCheckShipDatesForm.getProductId(), pasCheckShipDatesForm.getNumberDays(), pasCheckShipDatesForm.getZipCode());
            productAvailTimer.stop();
            request.setAttribute("pas_timer", productAvailTimer.getMilliseconds());

            request.setAttribute("pasShipDateAvailVO", pasShipDateAvailVO);

        }
        catch (Throwable t)
        {
            logger.error("Error in PAS Processing", t);
        }

    }



}
