package com.ftd.pas.web.action;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.server.ProductAvailabilityBO;

import com.ftd.pas.web.form.PASFloristForm;

import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product availability check from the web.
 */
public class PASFloristAction extends Action
{
    private static Logger logger = new Logger("com.ftd.pas.web.action.PASFloristAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, Exception
    {
        PASFloristForm pasFloristForm = (PASFloristForm)form;
        logger.debug("Start ");

        if (pasFloristForm != null && pasFloristForm.getZipCode() != null && !pasFloristForm.getZipCode().equals(""))
        {
            logger.debug("FORM " + pasFloristForm.getDeliveryDate() + " " + pasFloristForm.getZipCode());
            performPASStuff(request, pasFloristForm);

        }

        request.setAttribute("pasFloristForm", pasFloristForm);

        return mapping.findForward("success");
    }

    private void performPASStuff(HttpServletRequest request, PASFloristForm pasFloristForm)
    {
    	Connection con = null;
        try
        {
            ProductAvailabilityBO paBO = new ProductAvailabilityBO();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            Date deliveryDate = sdf.parse(pasFloristForm.getDeliveryDate());

            // Do the PAS check
            Timer productAvailTimer = new Timer();
        	con = getNewConnection();
            String[] florists = paBO.getAvailableFlorists(con, deliveryDate, pasFloristForm.getZipCode());
            productAvailTimer.stop();
            request.setAttribute("pas_getFlorist", productAvailTimer.getMilliseconds());

            request.setAttribute("florists", florists);

        }
        catch (Throwable t)
        {
            logger.error("Error in PAS Processing", t);
        } finally {
            if( con!=null ) {
                try {
                    if( !con.isClosed() ) {
                        con.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing database connection",sqle);
                }
            }
        }

    }

    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection      
    
        String datasource = ConfigurationUtil.getInstance().getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }

}
