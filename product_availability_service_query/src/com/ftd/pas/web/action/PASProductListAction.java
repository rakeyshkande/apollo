package com.ftd.pas.web.action;

import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.form.PASProductListForm;
import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASProductListAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.pas.web.action.PASProductListAction");

    /**
     * Perform a product availability check and return the results.  The check will be
     * performed using the new product availability service and the old DeliveryDateUtil.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception {
        PASProductListForm pasProductListForm = (PASProductListForm) form;
        logger.debug("Start ");
        
        if (pasProductListForm != null && pasProductListForm.getProductList() != null && !pasProductListForm.getProductList().equals("")) {

        try {
            PASRequestServiceImpl pas = new PASRequestServiceImpl();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            
            Date deliveryDate = sdf.parse(pasProductListForm.getDeliveryDate());
            Calendar deliveryDateCal = Calendar.getInstance();
            deliveryDateCal.setTime(deliveryDate);

            String[] returnList = null;
            String productList = pasProductListForm.getProductList();
            String[] newList = productList.split("\\s");
            String countryCode = pasProductListForm.getCountryCode();
            String zipCode = pasProductListForm.getZipCode();
            logger.debug("FORM " + sdf.format(deliveryDate) + " " + zipCode + " " + countryCode);
            Timer productListTimer = new Timer();
            
            if (productList != null && !productList.equals("")) {
                if (countryCode == null || countryCode.equals("")) {
                    logger.info("product list");
                    returnList = pas.isProductListAvailable(zipCode, deliveryDateCal, newList);
                } else {
                    logger.info("international product list");
                    returnList = pas.isInternationalProductListAvailable(countryCode, deliveryDateCal, newList);
                }
            }
            productListTimer.stop();
            request.setAttribute("pas_getProductListTimer", productListTimer.getMilliseconds());
            request.setAttribute("availableProducts", returnList);
        } catch (Throwable t) {
            logger.error("Error in PAS Processing",t);
        }

        }
        request.setAttribute("pasProductListForm",pasProductListForm);
        return mapping.findForward("success");

    }
    
}
