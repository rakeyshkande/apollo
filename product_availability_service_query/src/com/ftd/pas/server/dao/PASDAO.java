package com.ftd.pas.server.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.vo.PASCountryVO;
import com.ftd.pas.common.vo.PASFloristProductZipVO;
import com.ftd.pas.common.vo.PASProductStateVO;
import com.ftd.pas.common.vo.PASProductVO;
import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.PASVendorProductStateVO;
import com.ftd.pas.common.vo.PASVendorVO;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.common.vo.ProductDetailVO;
import com.ftd.pas.common.vo.RoutableFloristVO;

/**
 * Data Access Object for the Product Availability Query.  These methods are used
 * for checking the product availability.  The methods just format the calls
 * to the stored procedures and format the results.
 */
public class PASDAO
{
    protected Logger logger = new Logger(this.getClass().getName());

    public PASDAO()
    {
    }

    /**
     * Call the stored procedure to get the next available delivery date for teh product in the zip.
     * @param con
     * @param productId
     * @param zip
     * @return
     * @throws Exception
     */
    public Date getNextAvailableDeliveryDate(Connection con, String productId, String zip) throws Exception
    {
        Date deliveryDate = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_NEXT_AVAIL_DELIVERY_DATE");
        dataRequest.setInputParams(inputParms);

        deliveryDate = (java.sql.Date) DataAccessUtil.getInstance().execute(dataRequest);

        return deliveryDate;
    }

    /**
     * Check to see if a product is available for delivery in the zip.
     * @param con
     * @param productId
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public Boolean isProductAvailable(Connection con, String productId, Date deliveryDate, String zip) throws Exception
    {
        Boolean productAvailable = Boolean.FALSE;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("IS_PRODUCT_AVAILABLE");
        dataRequest.setInputParams(inputParms);

        String productAvailableString = (String)DataAccessUtil.getInstance().execute(dataRequest);

        if ( productAvailableString != null && productAvailableString.equals("Y"))
        {
            productAvailable = Boolean.TRUE;
        }

        return productAvailable;
    }

    /**
     * Get the details of product availability for a product delivered on the delivery date in the zip.
     * @param con
     * @param productId
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public ProductAvailVO getProductAvailability(Connection con, String productId, Date deliveryDate, String zip) throws Exception
    {
        ProductAvailVO productAvailVO = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_PRODUCT_AVAILABILITY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() )
        {
            productAvailVO = new ProductAvailVO();

            productAvailVO.setDeliveryDate(deliveryDate);
            productAvailVO.setProductId(productId);
            productAvailVO.setZipCode(zip);

            String shipMethodFlorist = results.getString("SHIP_METHOD_FLORIST");
            if (shipMethodFlorist != null && shipMethodFlorist.equalsIgnoreCase("Y")) {
                productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_CUTOFF_DATE"));
                productAvailVO.setFloristCutoffTime(results.getString("FLORIST_CUTOFF_TIME"));
            }

            String shipMethodCarrier = results.getString("SHIP_METHOD_CARRIER");
            if (shipMethodCarrier != null && shipMethodCarrier.equalsIgnoreCase("Y")) {
                productAvailVO.setShipDateND(results.getDate("SHIP_ND_DATE"));
                productAvailVO.setShipDate2D(results.getDate("SHIP_2D_DATE"));
                productAvailVO.setShipDateGR(results.getDate("SHIP_GR_DATE"));
                productAvailVO.setShipDateSA(results.getDate("SHIP_SA_DATE"));

                productAvailVO.setShipDateNDCutoff(results.getString("SHIP_ND_DATE_CUTOFF"));
                productAvailVO.setShipDate2DCutoff(results.getString("SHIP_2D_DATE_CUTOFF"));
                productAvailVO.setShipDateGRCutoff(results.getString("SHIP_GR_DATE_CUTOFF"));
                productAvailVO.setShipDateSACutoff(results.getString("SHIP_SA_DATE_CUTOFF"));
            }

            if (productAvailVO.getFloristCutoffDate() != null ||
                productAvailVO.getShipDateND() != null ||
                productAvailVO.getShipDate2D() != null ||
                productAvailVO.getShipDateGR() != null ||
                productAvailVO.getShipDateSA() != null
                )
            {
                productAvailVO.setIsAvailable(Boolean.TRUE);
            }
            else
            {
                productAvailVO.setIsAvailable(Boolean.FALSE);
            }
            // addons are always available in Apollo PAS
            productAvailVO.setIsAddOnAvailable(true);
            productAvailVO.setIsMorningDeliveryAvailable(true);
        }

        return productAvailVO;
    }

    /**
     * Call the stored procedure to see if any product is available for the zip on the date.
     * @param con
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public Boolean isAnyProductAvailable(Connection con, Date deliveryDate, String zip) throws Exception
    {
        Boolean productAvailable = Boolean.FALSE;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("IS_ANY_PRODUCT_AVAILABLE");
        dataRequest.setInputParams(inputParms);

        String productAvailableString = (String)DataAccessUtil.getInstance().execute(dataRequest);

        if ( productAvailableString != null && productAvailableString.equals("Y"))
        {
            productAvailable = Boolean.TRUE;
        }

        return productAvailable;
    }

    /**
     * Call the stored procedure to check international product availability.
     * @param con
     * @param productId
     * @param deliveryDate
     * @param countryId
     * @return
     * @throws Exception
     */
    public Boolean isInternationalProductAvailable(Connection con, String productId, Date deliveryDate, String countryId) throws Exception
    {
        Boolean productAvailable = Boolean.FALSE;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_COUNTRY_ID", countryId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("IS_INTL_PRODUCT_AVAILABLE");
        dataRequest.setInputParams(inputParms);

        String productAvailableString = (String)DataAccessUtil.getInstance().execute(dataRequest);

        if ( productAvailableString != null && productAvailableString.equals("Y"))
        {
            productAvailable = Boolean.TRUE;
        }

        return productAvailable;
    }

    /**
     * Call the stored procedure to get the list of available florists in the zip.
     * @param con
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public String[] getAvailableFlorists(Connection con, Date deliveryDate, String zip) throws Exception
    {
        List<String> florists = new ArrayList<String>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_AVAILABLE_FLORISTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() )
        {
            String floristId = results.getString("FLORIST_ID");
            florists.add(floristId);
        }

        return florists.toArray(new String[florists.size()]);
    }

    /**
     * Call the stored procedure to get the list of the most popular products.  The list is
     * ordered by popularity and limited by the numberOfResults.  Only available products
     * will be returned.
     * @param con
     * @param deliveryDate
     * @param zip
     * @param numberOfResults
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getMostPopularProducts(Connection con,
                                                       Date deliveryDate,
                                                       String zip,
                                                       int numberOfResults,
                                                       String companyId) throws Exception
    {
        List<ProductAvailVO> products = new ArrayList<ProductAvailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zip);
        inputParms.put("IN_COMPANY_ID", companyId);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);
        inputParms.put("IN_MAX_ROWS", new BigDecimal(numberOfResults));

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_MOST_POPULAR_PRODUCTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        int resultsProcessed = 0;
        while ( results != null && results.next() && resultsProcessed++ < numberOfResults)
        {
            ProductAvailVO productAvailVO = new ProductAvailVO();


            productAvailVO.setDeliveryDate(deliveryDate);
            productAvailVO.setProductId(results.getString("PRODUCT_ID"));
            productAvailVO.setZipCode(zip);

            productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_CUTOFF_DATE"));
            productAvailVO.setFloristCutoffTime(results.getString("FLORIST_CUTOFF_TIME"));

            productAvailVO.setShipDateND(results.getDate("SHIP_ND_DATE"));
            productAvailVO.setShipDate2D(results.getDate("SHIP_2D_DATE"));
            productAvailVO.setShipDateGR(results.getDate("SHIP_GR_DATE"));
            productAvailVO.setShipDateSA(results.getDate("SHIP_SAT_DATE"));

            productAvailVO.setShipDateNDCutoff(results.getString("SHIP_ND_CUTOFF_TIME"));
            productAvailVO.setShipDate2DCutoff(results.getString("SHIP_2D_CUTOFF_TIME"));
            productAvailVO.setShipDateGRCutoff(results.getString("SHIP_GR_CUTOFF_TIME"));
            productAvailVO.setShipDateSACutoff(results.getString("SHIP_SAT_CUTOFF_TIME"));

            BigDecimal popBigDecimal = results.getBigDecimal("POPULARITY");
            if (popBigDecimal != null)
            {
                productAvailVO.setPopularityOrderCount(popBigDecimal.intValue());
            }
            else
            {
                productAvailVO.setPopularityOrderCount(0);
            }

            productAvailVO.setIsAvailable(Boolean.TRUE);

            products.add(productAvailVO);
        }


        return products.toArray(new ProductAvailVO[products.size()]);
    }

    /**
     * Call the stored procedure to get the list of the most popular products.  The list is
     * ordered by popularity and limited by the numberOfResults.  Availability is not checked.
     * @param con
     * @param numberOfResults
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getMostPopularProducts(Connection con,
                                                   int numberOfResults,
                                                   String companyId) throws Exception
    {
        List<ProductAvailVO> products = new ArrayList<ProductAvailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_MOST_POPULAR_PRODUCTS_NOPARM");
        inputParms.put("IN_COMPANY_ID", companyId);
        inputParms.put("IN_MAX_ROWS", new BigDecimal(numberOfResults));
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        int resultsProcessed = 0;
        while ( results != null && results.next() && resultsProcessed++ < numberOfResults)
        {
            ProductAvailVO productAvailVO = new ProductAvailVO();

            productAvailVO.setProductId(results.getString("PRODUCT_ID"));

            productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_CUTOFF_DATE"));
            productAvailVO.setFloristCutoffTime(results.getString("FLORIST_CUTOFF_TIME"));

            productAvailVO.setShipDateND(results.getDate("SHIP_ND_DATE"));
            productAvailVO.setShipDate2D(results.getDate("SHIP_2D_DATE"));
            productAvailVO.setShipDateGR(results.getDate("SHIP_GR_DATE"));
            productAvailVO.setShipDateSA(results.getDate("SHIP_SA_DATE"));

            productAvailVO.setShipDateNDCutoff(results.getString("SHIP_ND_CUTOFF_TIME"));
            productAvailVO.setShipDate2DCutoff(results.getString("SHIP_2D_CUTOFF_TIME"));
            productAvailVO.setShipDateGRCutoff(results.getString("SHIP_GR_CUTOFF_TIME"));
            productAvailVO.setShipDateSACutoff(results.getString("SHIP_SA_CUTOFF_TIME"));

            BigDecimal popBigDecimal = results.getBigDecimal("POPULARITY");
            if (popBigDecimal != null)
            {
                productAvailVO.setPopularityOrderCount(popBigDecimal.intValue());
            }
            else
            {
                productAvailVO.setPopularityOrderCount(0);
            }

            productAvailVO.setIsAvailable(Boolean.TRUE);

            products.add(productAvailVO);
        }


        return products.toArray(new ProductAvailVO[products.size()]);
    }

    public PASShipDateAvailVO getAvailableShipDates(Connection con, String productId, int numberOfDays, String zipCode) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        PASShipDateAvailVO vo = new PASShipDateAvailVO();

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_VENDOR_DELIVERY_DATES");
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zipCode);
        inputParms.put("IN_NUMBER_DAYS", new BigDecimal(numberOfDays));
        dataRequest.setInputParams(inputParms);

        Map resultsMap = (Map)DataAccessUtil.getInstance().execute(dataRequest);
        CachedResultSet nextDayResultSet = (CachedResultSet) resultsMap.get("OUT_ND_CUR");
        CachedResultSet twoDayResultSet = (CachedResultSet) resultsMap.get("OUT_2D_CUR");
        CachedResultSet groundResultSet = (CachedResultSet) resultsMap.get("OUT_GR_CUR");
        CachedResultSet saturdayResultSet = (CachedResultSet) resultsMap.get("OUT_SAT_CUR");

        vo.setNextDayDeliveryDates(getDeliveryDates(nextDayResultSet));
        vo.setTwoDayDeliveryDates(getDeliveryDates(twoDayResultSet));
        vo.setGroundDeliveryDates(getDeliveryDates(groundResultSet));
        vo.setSaturdayDeliveryDates(getDeliveryDates(saturdayResultSet));
        // Sunday dates to empty array to prevent caller from throwing NPE.
        // This method should not be called for West orders so shouldn't matter.
        vo.setSundayDeliveryDates(getDeliveryDates(null));

        return vo;
    }

    private Calendar[] getDeliveryDates(CachedResultSet resultSet)
    {
        List deliveryDates = new ArrayList();
        while (resultSet != null && resultSet.next())
        {
            Date deliveryDate = resultSet.getDate("DELIVERY_DATE");
            Calendar delCal = Calendar.getInstance();
            delCal.setTime(deliveryDate);
            deliveryDates.add(delCal);
        }

        return (Calendar[])deliveryDates.toArray(new Calendar[deliveryDates.size()]);

    }

    /**
     * Get the PAS product data from PAS_PRODUCT_DT.
     * @param conn
     * @param productId
     * @return
     * @throws Exception
     */
    public PASProductVO getPASProduct(Connection conn,
                                      String productId,
                                      Date   deliveryDate) throws Exception
    {
        PASProductVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_PRODUCT");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() )
        {
            vo = new PASProductVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setAddonDays(results.getBigDecimal("ADDON_DAYS_QTY").longValue());
            vo.setTransitDays(results.getBigDecimal("TRANSIT_DAYS_QTY").longValue());
            vo.setShipAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setShipRestricted(results.getString("SHIP_RESTRICTED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setCodificationId(results.getString("CODIFICATION_ID"));
            vo.setProductType(results.getString("PRODUCT_TYPE"));
            vo.setShipMethodFlorist(results.getString("SHIP_METHOD_FLORIST"));
            vo.setShipMethodCarrier(results.getString("SHIP_METHOD_CARRIER"));

        }

        return vo;
    }

    /**
     * Get the PAS product state data from PAS_PRODUCT_STATE_DT.
     * @param conn
     * @param productId
     * @param stateCode
     * @return
     * @throws Exception
     */
    public PASProductStateVO getPASProductState(Connection conn,
                                                String productId,
                                                String stateCode,
                                                Date   deliveryDate) throws Exception
    {
        PASProductStateVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_STATE_CODE", stateCode);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_PRODUCT_STATE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() )
        {
            vo = new PASProductStateVO();

            vo.setProductId(results.getString("PRODUCT_ID"));
            vo.setStateCode(results.getString("STATE_CODE"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));

        }

        return vo;
    }

    /**
     * Get the PAS vendor product state data from PAS_VENDOR_PRODUCT_STATE_DT.
     * @param conn
     * @param productId
     * @param stateCode
     * @return
     * @throws Exception
     */
    public List<PASVendorProductStateVO> getPASVendorProductState(Connection conn,
                                                String productId,
                                                String stateCode,
                                                Date   deliveryDate) throws Exception
    {
        List<PASVendorProductStateVO> voList = new ArrayList<PASVendorProductStateVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_STATE_CODE", stateCode);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_VENDOR_PRODUCT_STATE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null)
        {
            PASVendorProductStateVO vo = new PASVendorProductStateVO();
            while (results.next())
            {
                vo = new PASVendorProductStateVO();

                vo.setVendorId(results.getString("VENDOR_ID"));
                vo.setProductId(results.getString("PRODUCT_ID"));
                vo.setStateCode(results.getString("STATE_CODE"));
                vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
                vo.setShipNDDate(results.getDate("SHIP_ND_DATE"));
                vo.setShipNDCutoffTime(results.getString("SHIP_ND_CUTOFF_TIME"));
                vo.setShip2DDate(results.getDate("SHIP_2D_DATE"));
                vo.setShip2DCutoffTime(results.getString("SHIP_2D_CUTOFF_TIME"));
                vo.setShipGRDate(results.getDate("SHIP_GR_DATE"));
                vo.setShipGRCutoffTime(results.getString("SHIP_GR_CUTOFF_TIME"));
                vo.setShipSatDate(results.getDate("SHIP_SAT_DATE"));
                vo.setShipSatCutoffTime(results.getString("SHIP_SAT_CUTOFF_TIME"));

                voList.add(vo);
            }

        }

        return voList;
    }

    /**
     * Get the PAS country data from PAS_COUNTRY_DT.
     * @param conn
     * @param countryId
     * @return
     * @throws Exception
     */
    public PASCountryVO getPASCountry(Connection conn,
                                      String countryId,
                                      Date   deliveryDate) throws Exception
    {
        PASCountryVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_COUNTRY_ID", countryId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_COUNTRY");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() )
        {
            vo = new PASCountryVO();

            vo.setCountryId(results.getString("COUNTRY_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setAddonDays(results.getBigDecimal("ADDON_DAYS_QTY").longValue());
            vo.setShippingAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));
            vo.setTransitAllowed(results.getString("TRANSIT_ALLOWED_FLAG"));

        }

        return vo;
    }

    /**
     * Get the PAS vendor data from PAS_VENDOR_DT.
     * @param conn
     * @param vendorId
     * @return
     * @throws Exception
     */
    public List<PASVendorVO> getPASVendor(Connection conn,
                                     String vendorId,
                                     Date   deliveryDate) throws Exception
    {
        List<PASVendorVO> voList = new ArrayList<PASVendorVO>();
        PASVendorVO vo = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_VENDOR_ID", vendorId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_VENDOR");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() )
        {
            vo = new PASVendorVO();

            vo.setVendorId(results.getString("VENDOR_ID"));
            vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            vo.setCutoffTime(results.getString("CUTOFF_TIME"));
            vo.setShipAllowed(results.getString("SHIP_ALLOWED_FLAG"));
            vo.setDeliveryAvailable(results.getString("DELIVERY_AVAILABLE_FLAG"));

            voList.add(vo);
        }

        return voList;
    }


    public List<PASFloristProductZipVO> getPASFloristProductZip(Connection conn,
                                                                String productId,
                                                                String zipCode,
                                                                Date   deliveryDate) throws Exception
    {
        List<PASFloristProductZipVO> voList = new ArrayList<PASFloristProductZipVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zipCode);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", sqlDeliveryDate);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PAS_FLORIST_PRODUCT_ZIP");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null)
        {
            PASFloristProductZipVO vo = new PASFloristProductZipVO();
            while (results.next())
            {
                vo = new PASFloristProductZipVO();

                vo.setProductId(results.getString("PRODUCT_ID"));
                vo.setZipCode(results.getString("ZIP_CODE"));
                vo.setDeliveryDate(results.getDate("DELIVERY_DATE"));
                vo.setCodificationId(results.getString("CODIFICATION_ID"));
                vo.setCutoffTime(results.getString("CUTOFF_TIME"));
                vo.setAddonDays(results.getString("ADDON_DAYS_QTY"));

                voList.add(vo);
            }

        }

        return voList;
    }

    /**
     * Get the details of product availability for an international product on the delivery date in the country.
     * @param con
     * @param productId
     * @param countryCode
     * @param maxDeliveryDate
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getAllInternationalProductAvailability(Connection con, String productId, String countryCode, Date maxDeliveryDate) throws Exception
    {
        List<ProductAvailVO> dates = new ArrayList<ProductAvailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_COUNTRY_CODE", countryCode);
        java.sql.Date deliveryDateSql = new java.sql.Date(maxDeliveryDate.getTime());
        inputParms.put("IN_MAX_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_ALL_INT_AVAILABLE_DATES");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() )
        {
            ProductAvailVO productAvailVO = new ProductAvailVO();

            productAvailVO.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            productAvailVO.setProductId(productId);
            productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_CUTOFF_DATE"));

            if (productAvailVO.getFloristCutoffDate() != null) {
                productAvailVO.setIsAvailable(Boolean.TRUE);
            } else {
                productAvailVO.setIsAvailable(Boolean.FALSE);
            }
            dates.add(productAvailVO);
        }

        return dates.toArray(new ProductAvailVO[dates.size()]);
    }

    public String[] isProductListAvailable(Connection conn, String zipCode, Date deliveryDate, String[] productList) throws Exception {

        List<String> returnList = new ArrayList<String>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_ZIP_CODE", zipCode);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String stringDeliveryDate = sdf.format(deliveryDate);
        inputParms.put("IN_DELIVERY_DATE", stringDeliveryDate);

        String paramProductList = null;
        for (int i=0; i< productList.length; i++) {
            if (paramProductList == null) {
                paramProductList = "'" + productList[i] + "'";
            } else {
                paramProductList = paramProductList + ", '" + productList[i] + "'";
            }
        }
        inputParms.put("IN_PRODUCT_LIST", paramProductList);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("IS_PRODUCT_LIST_AVAILABLE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        while (results.next()) {
            String productId = results.getString("PRODUCT_ID");
            returnList.add(productId);
        }

        return returnList.toArray(new String[returnList.size()]);
    }

    public String[] isInternationalProductListAvailable(Connection conn, String countryCode, Date deliveryDate, String[] productList) throws Exception {

        List<String> returnList = new ArrayList<String>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_COUNTRY_CODE", countryCode);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String stringDeliveryDate = sdf.format(deliveryDate);
        inputParms.put("IN_DELIVERY_DATE", stringDeliveryDate);

        String paramProductList = null;
        for (int i=0; i< productList.length; i++) {
            if (paramProductList == null) {
                paramProductList = "'" + productList[i] + "'";
            } else {
                paramProductList = paramProductList + ", '" + productList[i] + "'";
            }
        }
        inputParms.put("IN_PRODUCT_LIST", paramProductList);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("IS_INT_PRODUCT_LIST_AVAILABLE");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
        while (results.next()) {
            String productId = results.getString("PRODUCT_ID");
            returnList.add(productId);
        }

        return returnList.toArray(new String[returnList.size()]);
    }

    /**
     * Get the details of product availability for a product delivered on the delivery date in the zip.
     * @param con
     * @param productId
     * @param zip
     * @param maxDeliveryDate
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getAllProductAvailability(Connection con, String productId, String zip, Date maxDeliveryDate) throws Exception
    {
        List<ProductAvailVO> dates = new ArrayList<ProductAvailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(maxDeliveryDate.getTime());
        inputParms.put("IN_MAX_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_ALL_AVAILABLE_DATES");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        //Date lastDeliveryDate = null;
        while ( results != null && results.next() )
        {
           // Date deliveryDate = results.getDate("DELIVERY_DATE");
//            if (lastDeliveryDate == null || lastDeliveryDate.before(deliveryDate)) {
            ProductAvailVO productAvailVO = new ProductAvailVO();

            productAvailVO.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            productAvailVO.setProductId(productId);
            productAvailVO.setZipCode(zip);

            productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_DATE"));

            productAvailVO.setShipDateND(results.getDate("SHIP_ND_DATE"));
            productAvailVO.setShipDate2D(results.getDate("SHIP_2D_DATE"));
            productAvailVO.setShipDateGR(results.getDate("SHIP_GR_DATE"));
            productAvailVO.setShipDateSA(results.getDate("SHIP_SA_DATE"));
            productAvailVO.setIsAddOnAvailable(true); // default it to true

            if (productAvailVO.getFloristCutoffDate() != null ||
                productAvailVO.getShipDateND() != null ||
                productAvailVO.getShipDate2D() != null ||
                productAvailVO.getShipDateGR() != null ||
                productAvailVO.getShipDateSA() != null
                )
            {
                productAvailVO.setIsAvailable(Boolean.TRUE);
            }
            else
            {
                productAvailVO.setIsAvailable(Boolean.FALSE);
            }
            productAvailVO.setHasCharges(false);
            dates.add(productAvailVO);
//            }
           // lastDeliveryDate = deliveryDate;
        }

        return dates.toArray(new ProductAvailVO[dates.size()]);
    }

    /**
     * PROCEDURE GET_ROUTABLE_FLORISTS
     * (
     * IN_ZIP_CODE          IN FLORIST_ZIPS.ZIP_CODE%TYPE,
     * IN_PRODUCT_ID        IN PRODUCT_MASTER.PRODUCT_ID%TYPE,
     * IN_DELIVERY_DATE     IN DATE,
     * IN_DELIVERY_DATE_END IN DATE,
     * IN_SOURCE_CODE       IN SOURCE.SOURCE_CODE%TYPE,
     * IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_FLORIST_USED.ORDER_DETAIL_ID%TYPE,
     * IN_DELIVERY_CITY     IN EFOS_CITY_STATE.CITY_NAME%TYPE,
     * IN_DELIVERY_STATE    IN EFOS_STATE_CODES.STATE_ID%TYPE,
     * IN_ORDER_VALUE       IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
     * IN_RETURN_ALL        IN CHAR,
     * OUT_CURSOR          OUT TYPES.REF_CURSOR
     * )
     * @param conn
     * @return
     * @throws Exception
     */
    public List<RoutableFloristVO> getRoutableFlorists(Connection conn,
                                                            String zipCode,
                                                            String productId,
                                                            Date deliveryDate,
                                                            Date deliveryDateEnd,
                                                            String sourceCode,
                                                            int orderDetailId,
                                                            String deliveryCity,
                                                            String deliveryState,
                                                            String orderValue,
                                                            String returnAll
                                                            ) throws Exception
    {
        List<RoutableFloristVO> voList = new ArrayList<RoutableFloristVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("ZIP_CODE", zipCode);
        inputParms.put("PRODUCT_ID", productId);
        java.sql.Date sqlDeliveryDate = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("DELIVERY_DATE", sqlDeliveryDate);
        java.sql.Date sqlDeliveryDateEnd = null;
        if (deliveryDateEnd != null)
        {
            sqlDeliveryDateEnd = new java.sql.Date(deliveryDateEnd.getTime());
        }
        inputParms.put("DELIVERY_DATE_END", sqlDeliveryDateEnd);
        inputParms.put("SOURCE_CODE", sourceCode);
        inputParms.put("ORDER_DETAIL_ID", Integer.toString(orderDetailId));
        inputParms.put("DELIVERY_CITY", deliveryCity);
        inputParms.put("DELIVERY_STATE", deliveryState);
        inputParms.put("ORDER_VALUE", orderValue);
        inputParms.put("RETURN_ALL", returnAll);

        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_ROUTABLE_FLORISTS");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null)
        {
            logger.debug("Florists returned from RWD: " + results.getRowCount());
            RoutableFloristVO vo;
            while (results.next())
            {
                vo = new RoutableFloristVO();

                vo.setFloristId(results.getString("FLORIST_ID"));
                vo.setScore(results.getInt("SCORE"));
                vo.setWeight(results.getInt("WEIGHT"));
                vo.setFloristName(results.getString("FLORIST_NAME"));
                vo.setPhoneNumber(results.getString("PHONE_NUMBER"));
                vo.setMercuryFlag(results.getString("MERCURY_FLAG"));
                vo.setSuperFloristFlag(results.getString("SUPER_FLORIST_FLAG"));
                vo.setSundayDeliveryFlag(results.getString("SUNDAY_DELIVERY_FLAG"));
                vo.setCutoffTime(results.getString("CUTOFF_TIME"));
                vo.setStatus(results.getString("STATUS"));
                vo.setCodificationId(results.getString("CODIFICATION_ID"));
                vo.setFloristBlocked(results.getString("FLORIST_BLOCKED"));
                vo.setProductCodificationId(results.getString("PRODUCT_CODIFICATION_ID"));
                vo.setSequences(results.getString("SEQUENCES"));
                vo.setZipCityFlag(results.getString("zip_city_flag"));
                vo.setFloristSuspended(results.getString("FLORIST_SUSPENDED"));
                vo.setMinOrderAmt(results.getString("MINIMUM_ORDER_AMOUNT"));

                voList.add(vo);
                logger.debug(vo.getFloristId() + " " + vo.getZipCityFlag());
            }

        }

        return voList;
    }

    public 	ProductDetailVO getProductDetails(Connection conn, String productId) throws Exception{

    	DataRequest dataRequest = new DataRequest();
    	HashMap inputParams = new HashMap();
    	inputParams.put("IN_PRODUCT_ID", productId);

    	dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_PRODUCT_BY_ID");
        dataRequest.setInputParams(inputParams);

        ProductDetailVO productDetailVO = null;
        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null)
        {
            while (results.next())
            {
            	productDetailVO = new ProductDetailVO();
                productDetailVO.setNovatorId(results.getString("novatorId"));
                productDetailVO.setPersonalizationTemplateId(results.getString("personalizationTemplate"));
                productDetailVO.setProductId(results.getString("productId"));
                productDetailVO.setShippingSystem(results.getString("shippingSystem"));
                productDetailVO.setStatus(results.getString("status"));
                productDetailVO.setShipMethodCarrier(results.getString("shipMethodCarrier"));
                productDetailVO.setShipMethodFlorist(results.getString("shipMethodFlorist"));

            }
        }


    	return productDetailVO;
    }

    public ProductAvailVO[] getAllProductAvailabilityFlorist(Connection con, String productId, String zip, Date maxDeliveryDate) throws Exception {
        List<ProductAvailVO> dates = new ArrayList<ProductAvailVO>();

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(maxDeliveryDate.getTime());
        inputParms.put("IN_MAX_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_ALL_FLORIST_DATES");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        while ( results != null && results.next() )
        {
            Date deliveryDate = results.getDate("DELIVERY_DATE");
            ProductAvailVO productAvailVO = new ProductAvailVO();

            productAvailVO.setDeliveryDate(results.getDate("DELIVERY_DATE"));
            productAvailVO.setProductId(productId);
            productAvailVO.setZipCode(zip);

            productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_DATE"));

            productAvailVO.setShipDateND(results.getDate("SHIP_ND_DATE"));
            productAvailVO.setShipDate2D(results.getDate("SHIP_2D_DATE"));
            productAvailVO.setShipDateGR(results.getDate("SHIP_GR_DATE"));
            productAvailVO.setShipDateSA(results.getDate("SHIP_SA_DATE"));

            if (productAvailVO.getFloristCutoffDate() != null ||
                productAvailVO.getShipDateND() != null ||
                productAvailVO.getShipDate2D() != null ||
                productAvailVO.getShipDateGR() != null ||
                productAvailVO.getShipDateSA() != null
                )
            {
                productAvailVO.setIsAvailable(Boolean.TRUE);
            }
            else
            {
                productAvailVO.setIsAvailable(Boolean.FALSE);
            }
            productAvailVO.setHasCharges(false);
            // addons are always available in Apollo PAS
            productAvailVO.setIsAddOnAvailable(true);
            productAvailVO.setIsMorningDeliveryAvailable(true);
            dates.add(productAvailVO);
        }

        return dates.toArray(new ProductAvailVO[dates.size()]);
    }

    public ProductAvailVO getProductAvailabilityFlorist(Connection con, String productId, Date deliveryDate, String zip) throws Exception
    {
        ProductAvailVO productAvailVO = null;

        DataRequest dataRequest = new DataRequest();
        HashMap inputParms = new HashMap();
        inputParms.put("IN_PRODUCT_ID", productId);
        inputParms.put("IN_ZIP_CODE", zip);
        java.sql.Date deliveryDateSql = new java.sql.Date(deliveryDate.getTime());
        inputParms.put("IN_DELIVERY_DATE", deliveryDateSql);

        dataRequest.setConnection(con);
        dataRequest.setStatementID("GET_PRODUCT_AVAIL_FLORIST");
        dataRequest.setInputParams(inputParms);

        CachedResultSet results = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);

        if ( results != null && results.next() )
        {
            productAvailVO = new ProductAvailVO();

            productAvailVO.setDeliveryDate(deliveryDate);
            productAvailVO.setProductId(productId);
            productAvailVO.setZipCode(zip);

            String shipMethodFlorist = results.getString("SHIP_METHOD_FLORIST");
            if (shipMethodFlorist != null && shipMethodFlorist.equalsIgnoreCase("Y")) {
                productAvailVO.setFloristCutoffDate(results.getDate("FLORIST_CUTOFF_DATE"));
                productAvailVO.setFloristCutoffTime(results.getString("FLORIST_CUTOFF_TIME"));
            }

            if (productAvailVO.getFloristCutoffDate() != null) {
                productAvailVO.setIsAvailable(Boolean.TRUE);
            } else {
                productAvailVO.setIsAvailable(Boolean.FALSE);
            }
            // addons are always available in Apollo PAS
            productAvailVO.setIsAddOnAvailable(true);
            productAvailVO.setIsMorningDeliveryAvailable(true);
        }

        return productAvailVO;
    }
    
    public int getMaxDeliveryDate(Connection con) throws Exception {
		String maxDays = null;
		
		int maxDeliveryDate = 0;
		DataRequest dataRequest = new DataRequest();
		dataRequest.setConnection(con);
		dataRequest.setStatementID("GET_MAX_DELIVERY_DATE");
		CachedResultSet rs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
		rs.reset();

		while (rs.next()) {
			maxDays = rs.getObject(15).toString();
		}

		if (maxDays != null) {
			maxDeliveryDate = Integer.parseInt(maxDays);
		}
		return maxDeliveryDate;
	}

}
