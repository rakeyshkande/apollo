package com.ftd.pas.server;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.ordervalidator.util.ValidationDataAccessUtil;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.FTDAppsGlobalParmsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.pac.util.client.PACSClient;
import com.ftd.pac.util.vo.ChargesType;
import com.ftd.pac.util.vo.PACRequest;
import com.ftd.pac.util.vo.PACResponse;
import com.ftd.pac.util.vo.PACResponse.AvailableDates;
import com.ftd.pac.util.vo.PACResponse.AvailableDates.Charges;
import com.ftd.pac.util.vo.PACResponse.AvailableDates.Charges.Charge;
import com.ftd.pac.util.vo.ShipMethodType;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.common.vo.ProductDetailVO;
import com.ftd.pas.server.dao.PASDAO;
import com.ftd.pas.web.util.Timer;


/**
 * Business Object for Product Availability.  All of the product availability
 * requests will pass through this business object.  Most of the method calls
 * just delegate all of the work to the DAO and stored procedures.
 */
public class ProductAvailabilityBO
{

	protected Logger logger = new Logger(this.getClass().getName());
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");


    public ProductAvailabilityBO()
    {
    }

    /**
     * Get the next available delivery date.
     * @param con
     * @param productId
     * @param zip
     * @return
     * @throws Exception
     */
    public Date getNextAvailableDeliveryDate(Connection con, String productId, String zip, List<String> addOns,String sourceCode) throws Exception
    {

    	logger.info("getNextAvailableDeliveryDate START");
		Timer productAvailTimer = new Timer();
        PASDAO dao = new PASDAO();
        Date deliveryDate = null;

		/*
		 * Check for Shipping System first for the given productId
		 */
		ProductDetailVO productDetailVO = getProductDetails(con, productId);

		if (productDetailVO != null && productDetailVO.getShippingSystem().equals(PASConstants.SHIPPING_SYSTEM_WEST)) {
                ConfigurationUtil cu = ConfigurationUtil.getInstance();
                String maxDaysString = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                int maxDays = 0;
                if (maxDaysString != null) {
                    try {
                        maxDays = Integer.valueOf(maxDaysString);
                    } catch (NumberFormatException e) {
                        maxDays = 0;
                    }
                }
                ProductAvailVO[] paVOs = this.getProductAvailableDates(con, productId, zip,sourceCode, maxDays, addOns);
                if (paVOs != null && paVOs.length > 0) {
                	ProductAvailVO paVO = paVOs[0];
                	if (paVO.getDeliveryDate() != null) {
                	    deliveryDate = paVO.getDeliveryDate().getTime();
                	}
                }

		} else {
			 deliveryDate = dao.getNextAvailableDeliveryDate(con, productId, zip);

		}

		productAvailTimer.stop();
		logger.info("getNextAvailableDeliveryDate FINISH took "
				+ productAvailTimer.getMilliseconds() + " ms");

		return deliveryDate;
    }


    /**
     * Check to see if a product is available.
     * @param con
     * @param productId
     * @param zip
     * @param deliveryDate
     * @return
     * @throws Exception
     */
    public Boolean isProductAvailable(Connection con, String productId, String zip, Date deliveryDate) throws Exception
    {
        PASDAO dao = new PASDAO();

        Boolean available = dao.isProductAvailable(con, productId, deliveryDate, zip);

        return available;
    }

    /**
     * Check to see if any product is available in the zip for the delivery date.
     * @param con
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public Boolean isAnyProductAvailable(Connection con, Date deliveryDate, String zip) throws Exception
    {
        PASDAO dao = new PASDAO();

        Boolean available = dao.isAnyProductAvailable(con, deliveryDate, zip);

        return available;

    }

    /**
     * Check for the availability of an international product.
     * @param con
     * @param productId
     * @param deliveryDate
     * @param country
     * @return
     * @throws Exception
     */
    public Boolean isInternationalProductAvailable(Connection con, String productId, Date deliveryDate, String country) throws Exception
    {
        PASDAO dao = new PASDAO();

        Boolean available = dao.isInternationalProductAvailable(con, productId, deliveryDate, country);

        return available;

    }


/*    *//**
     * Get the product availability for a product.  This returns details of the availability.
     * @param productId
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     *//*
    public ProductAvailVO getProductAvailability(Connection con, String productId, Date deliveryDate, String zip) throws Exception
    {
        PASDAO dao = new PASDAO();

        ProductAvailVO available = dao.getProductAvailability(con, productId, deliveryDate, zip);

        return available;

    }*/

    /**
     * Get the product availability for a product.  This returns details of the availability.
     * @param con
     * @param productId
     * @param deliveryDate
     * @param zip
     * @param addOns
     * @return
     * @throws Exception
     */
    public ProductAvailVO getProductAvailability(Connection con, String productId, Date deliveryDate, String zip, List<String> addOns,String sourceCode) throws Exception
    {

		logger.info("getProductAvailability START");
		logger.info(productId + " " + sdf.format(deliveryDate) + " " + zip);
		Timer productAvailTimer = new Timer();
		PASDAO dao = new PASDAO();
		ProductAvailVO available = new ProductAvailVO();
		boolean hasPACDates = false;
		String affiliateCode = "";
		
		/*
		 * Check for Shipping System first for the given productId
		 */
		ProductDetailVO productDetailVO = getProductDetails(con, productId);

		if (productDetailVO != null && productDetailVO.getPersonalizationTemplateId() != null) {
			logger.info("Personal Creations product, returning available");
			ProductAvailVO paVO = new ProductAvailVO();
			paVO.setIsAvailable(true);
			paVO.setIsAddOnAvailable(true);

			if(isSaturdayDelivery(deliveryDate)){
				paVO.setShipDateSA(deliveryDate);
				paVO.setShipDateSACutoff(PASConstants.PAC_CUT_OFF);
			}else{
				paVO.setShipDateGR(deliveryDate);
				paVO.setShipDateGRCutoff(PASConstants.PAC_CUT_OFF);
			}

			return paVO;
		}

		if (productDetailVO != null && productDetailVO.getShippingSystem().equals(PASConstants.SHIPPING_SYSTEM_WEST)) {
			if(productDetailVO.getStatus().equals("A")){
				
				logger.debug("getProductAvailability | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
				logger.debug("getProductAvailability - Calling PAC Serivice");
				affiliateCode = FTDCommonUtils.getAffiliateCode(con, sourceCode);
				logger.debug("affiliateCode---->"+affiliateCode);
				try {
				PACRequest pacRequest = new PACRequest(productDetailVO.getNovatorId(), addOns,
						sourceCode, null, new Date(), zip,
						deliveryDate, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE,affiliateCode);
				PACSClient client = new PACSClient(PASConstants.PAC_WS_GET_AVAILABLE_PRODUCTS);
				PACResponse response = client.sendRequest(pacRequest);

				logger.debug("getProductAvailability - PAC Serivice call finished");
				List<AvailableDates> avlDates = response.getAvailableDates();

				if (!avlDates.isEmpty()) {
					AvailableDates avlDate = avlDates.get(0);

					if(avlDate!=null){

						logger.info("delivery date: " + avlDate.getDeliveryDate());
						logger.info("ship method: " + avlDate.getShippingMethod().value());
						ProductAvailVO productAvailVO = new ProductAvailVO();
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.NextDay)) {
							productAvailVO.setShipDateND(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateNDCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Saturday)) {
							productAvailVO.setShipDateSA(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateSACutoff(PASConstants.PAC_CUT_OFF);
						}

						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Ground)) {
							productAvailVO.setShipDateGR(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateGRCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Sunday)) {
							productAvailVO.setShipDateSU(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateSUCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.TwoDay)) {
							productAvailVO.setShipDate2D(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDate2DCutoff(PASConstants.PAC_CUT_OFF);
						}

						productAvailVO.setDeliveryDate(getDateFromString(avlDate.getDeliveryDate()));
						productAvailVO.setIsAvailable(Boolean.TRUE);
						productAvailVO.setIsAddOnAvailable(isAddOnAvailable(response.getAddonIds(), addOns));
						if (avlDate.isMorningDeliveryAllowed() != null && avlDate.isMorningDeliveryAllowed().equalsIgnoreCase("Y")) {
						    productAvailVO.setIsMorningDeliveryAvailable(Boolean.TRUE);
						} else {
							productAvailVO.setIsMorningDeliveryAvailable(Boolean.FALSE);
						}
						available = productAvailVO;

					}
				}else{
					available.setIsAvailable(false);
					available.setIsAddOnAvailable(false);
					available.setIsMorningDeliveryAvailable(false);
	    			if(!response.getError().isEmpty()){
	    				for(com.ftd.pac.util.vo.Error error : response.getError()){
	    					if(PASConstants.ERROR_CODE_320 == error.getCode() && error.getMessage().equals("No Product available")){
	    		    			logger.error("getProductAvailability - No Product Found, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
	    		    			available.setErrorMessage(error.getMessage());
	    		    			break;
	    					}else{
	    						logger.error("Error in Calling PAC Service-getProductAvailableDatesAndDates, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
	    						available.setErrorMessage(error.getMessage());
	    		    			break;
	    					}
	    				}
	    			}
	    		}

				} catch (Exception e) {
					logger.error(e);
					available.setErrorMessage(e.toString());
					available.setIsAvailable(false);
					available.setIsAddOnAvailable(false);
					available.setIsMorningDeliveryAvailable(false);
				}
			}else{
				logger.debug("getProductAvailability | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
    			logger.info("getProductAvailability - No Product Found");
    			available.setErrorMessage("No Product Found");
    			available.setIsAvailable(false);
				available.setIsAddOnAvailable(false);
				available.setIsMorningDeliveryAvailable(false);
			}
			hasPACDates = true;

		} else {
			available = dao.getProductAvailability(con, productId, deliveryDate, zip);
			available.setIsAddOnAvailable(true); // For NON FTD WEST Addon Availability is true
		}

    	logger.info("hasPACDates: " + hasPACDates);
        if (hasPACDates && (productDetailVO != null && productDetailVO.getShipMethodFlorist() != null &&
        		productDetailVO.getShipMethodFlorist().equalsIgnoreCase("Y"))) {

        	logger.info("FTD West SDFC product: " + available.getIsAvailable());
        	ProductAvailVO paVO = dao.getProductAvailabilityFlorist(con, productId, deliveryDate, zip);
        	if (paVO != null && paVO.getIsAvailable()) {
        		available.setFloristCutoffDate(paVO.getFloristCutoffDate());
        		available.setFloristCutoffTime(paVO.getFloristCutoffTime());
        		if (paVO.getIsAvailable()) {
        			boolean usePACSAddonAvail = false;
        			if (!available.getIsAvailable()) {
        				usePACSAddonAvail = false;
        			} else {
            			if (addOns != null && addOns.size() > 0) {
            				String tempAddonId = addOns.get(0);
            				AddOnUtility addOnUtility = new AddOnUtility();
        	    			AddOnVO addOnVO = addOnUtility.getAddOnById(tempAddonId, false, false, con);
        		    		logger.info("addon type: " + addOnVO.getDeliveryType());
        			    	// use PACS addon availability if the addon has a delivery type of dropship or both
        				    if (addOnVO.getDeliveryType() != null &&
        					    	(addOnVO.getDeliveryType().equalsIgnoreCase("dropship") ||
        						     addOnVO.getDeliveryType().equalsIgnoreCase("both"))) {
            					usePACSAddonAvail = true;
            				}
            			}
        			}
        			logger.info("PACS addonAvail: " + available.getIsAddOnAvailable() +
        					" PAS addonAvail: " + paVO.getIsAddOnAvailable());
        			if (usePACSAddonAvail) {
        				logger.info("Using PACS addonAvail");
        			} else {
            			available.setIsAddOnAvailable(paVO.getIsAddOnAvailable());
            			logger.info("Using PAS addonAvail");
        			}
        			available.setIsAvailable(paVO.getIsAvailable());
        		}
        	}
        }

		productAvailTimer.stop();
		logger.info("getProductAvailability FINISH took "
				+ productAvailTimer.getMilliseconds() + " ms");

		return available;

    }
    
    
    
    /**
     * Get the product availability for a product.  This returns details of the availability.
     * We can send customised flag values in this call like include charge, morningDeliveryCheck 
     * @param con
     * @param productId
     * @param deliveryDate
     * @param zip
     * @param addOns
     * @param sourceCode
     * @param includeCharge
     * @param checkMDAvailable
     * @return
     * @throws Exception
     */
    public ProductAvailVO getProductAvailability(Connection con, String productId, Date deliveryDate, String zip, List<String> addOns,String sourceCode, boolean includeCharge, boolean checkMDAvailable) throws Exception
    {

		logger.info("getProductAvailability START");
		logger.info(productId + " " + sdf.format(deliveryDate) + " " + zip);
		Timer productAvailTimer = new Timer();
		PASDAO dao = new PASDAO();
		ProductAvailVO available = new ProductAvailVO();
		boolean hasPACDates = false;
		String affiliateCode = "";
		
		/*
		 * Check for Shipping System first for the given productId
		 */
		ProductDetailVO productDetailVO = getProductDetails(con, productId);

		if (productDetailVO != null && productDetailVO.getPersonalizationTemplateId() != null) {
			logger.info("Personal Creations product, returning available");
			ProductAvailVO paVO = new ProductAvailVO();
			paVO.setIsAvailable(true);
			paVO.setIsAddOnAvailable(true);

			if(isSaturdayDelivery(deliveryDate)){
				paVO.setShipDateSA(deliveryDate);
				paVO.setShipDateSACutoff(PASConstants.PAC_CUT_OFF);
			}else{
				paVO.setShipDateGR(deliveryDate);
				paVO.setShipDateGRCutoff(PASConstants.PAC_CUT_OFF);
			}

			return paVO;
		}

		if (productDetailVO != null && productDetailVO.getShippingSystem().equals(PASConstants.SHIPPING_SYSTEM_WEST)) {
			if(productDetailVO.getStatus().equals("A")){
				
				logger.debug("getProductAvailability | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
				logger.debug("getProductAvailability - Calling PAC Serivice");
				affiliateCode = FTDCommonUtils.getAffiliateCode(con, sourceCode);
				logger.debug("affiliateCode---->"+affiliateCode);
				try {
				PACRequest pacRequest = new PACRequest(productDetailVO.getNovatorId(), addOns,
						sourceCode, null, new Date(), zip,
						deliveryDate, includeCharge, checkMDAvailable, Boolean.FALSE, Boolean.FALSE,affiliateCode);
				PACSClient client = new PACSClient(PASConstants.PAC_WS_GET_AVAILABLE_PRODUCTS);
				PACResponse response = client.sendRequest(pacRequest);

				logger.debug("getProductAvailability - PAC Serivice call finished");
				List<AvailableDates> avlDates = response.getAvailableDates();

				if (!avlDates.isEmpty()) {
					AvailableDates avlDate = avlDates.get(0);

					if(avlDate!=null){

						logger.info("delivery date: " + avlDate.getDeliveryDate());
						logger.info("ship method: " + avlDate.getShippingMethod().value());
						ProductAvailVO productAvailVO = new ProductAvailVO();
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.NextDay)) {
							productAvailVO.setShipDateND(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateNDCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Saturday)) {
							productAvailVO.setShipDateSA(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateSACutoff(PASConstants.PAC_CUT_OFF);
						}

						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Ground)) {
							productAvailVO.setShipDateGR(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateGRCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.Sunday)) {
							productAvailVO.setShipDateSU(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDateSUCutoff(PASConstants.PAC_CUT_OFF);
						}
						if (avlDate.getShippingMethod()
								.equals(ShipMethodType.TwoDay)) {
							productAvailVO.setShipDate2D(getDateFromString(avlDate
									.getDeliveryDate()));
							productAvailVO
									.setShipDate2DCutoff(PASConstants.PAC_CUT_OFF);
						}

						productAvailVO.setDeliveryDate(getDateFromString(avlDate.getDeliveryDate()));
						productAvailVO.setIsAvailable(Boolean.TRUE);
						productAvailVO.setIsAddOnAvailable(isAddOnAvailable(response.getAddonIds(), addOns));
						if (avlDate.isMorningDeliveryAllowed() != null && avlDate.isMorningDeliveryAllowed().equalsIgnoreCase("Y")) {
						    productAvailVO.setIsMorningDeliveryAvailable(Boolean.TRUE);
						} else {
							productAvailVO.setIsMorningDeliveryAvailable(Boolean.FALSE);
						}
						available = productAvailVO;

					}
				}else{
					available.setIsAvailable(false);
					available.setIsAddOnAvailable(false);
					available.setIsMorningDeliveryAvailable(false);
	    			if(!response.getError().isEmpty()){
	    				for(com.ftd.pac.util.vo.Error error : response.getError()){
	    					if(PASConstants.ERROR_CODE_320 == error.getCode() && error.getMessage().equals("No Product available")){
	    		    			logger.error("getProductAvailability - No Product Found, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
	    		    			break;
	    					}else{
	    						logger.error("Error in Calling PAC Service-getProductAvailableDatesAndDates, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
	    		    			break;
	    					}
	    				}
	    			}
	    		}

				} catch (Exception e) {
					logger.error(e);
					available.setIsAvailable(false);
					available.setIsAddOnAvailable(false);
					available.setIsMorningDeliveryAvailable(false);
				}
			}else{
				logger.debug("getProductAvailability | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
    			logger.info("getProductAvailability - No Product Found");
				available.setIsAvailable(false);
				available.setIsAddOnAvailable(false);
				available.setIsMorningDeliveryAvailable(false);
			}
			hasPACDates = true;

		} else {
			available = dao.getProductAvailability(con, productId,
					deliveryDate, zip);
		}

    	logger.info("hasPACDates: " + hasPACDates);
        if (hasPACDates && (productDetailVO != null && productDetailVO.getShipMethodFlorist() != null &&
        		productDetailVO.getShipMethodFlorist().equalsIgnoreCase("Y"))) {

        	logger.info("FTD West SDFC product: " + available.getIsAvailable());
        	ProductAvailVO paVO = dao.getProductAvailabilityFlorist(con, productId, deliveryDate, zip);
        	if (paVO != null && paVO.getIsAvailable()) {
        		available.setFloristCutoffDate(paVO.getFloristCutoffDate());
        		available.setFloristCutoffTime(paVO.getFloristCutoffTime());
        		if (paVO.getIsAvailable()) {
        			boolean usePACSAddonAvail = false;
        			if (!available.getIsAvailable()) {
        				usePACSAddonAvail = false;
        			} else {
            			if (addOns != null && addOns.size() > 0) {
            				String tempAddonId = addOns.get(0);
            				AddOnUtility addOnUtility = new AddOnUtility();
        	    			AddOnVO addOnVO = addOnUtility.getAddOnById(tempAddonId, false, false, con);
        		    		logger.info("addon type: " + addOnVO.getDeliveryType());
        			    	// use PACS addon availability if the addon has a delivery type of dropship or both
        				    if (addOnVO.getDeliveryType() != null &&
        					    	(addOnVO.getDeliveryType().equalsIgnoreCase("dropship") ||
        						     addOnVO.getDeliveryType().equalsIgnoreCase("both"))) {
            					usePACSAddonAvail = true;
            				}
            			}
        			}
        			logger.info("PACS addonAvail: " + available.getIsAddOnAvailable() +
        					" PAS addonAvail: " + paVO.getIsAddOnAvailable());
        			if (usePACSAddonAvail) {
        				logger.info("Using PACS addonAvail");
        			} else {
            			available.setIsAddOnAvailable(paVO.getIsAddOnAvailable());
            			logger.info("Using PAS addonAvail");
        			}
        			available.setIsAvailable(paVO.getIsAvailable());
        		}
        	}
        }

		productAvailTimer.stop();
		logger.info("getProductAvailability FINISH took "
				+ productAvailTimer.getMilliseconds() + " ms");

		return available;

    }

    /**
     * Get the list of available florists in the zip code for teh delivery date.
     * @param con
     * @param deliveryDate
     * @param zip
     * @return
     * @throws Exception
     */
    public String[] getAvailableFlorists(Connection con, Date deliveryDate, String zip) throws Exception
    {
        PASDAO dao = new PASDAO();

        String[] florists = dao.getAvailableFlorists(con, deliveryDate, zip);

        return florists;

    }

    /**
     * Get a list of the most popular products in the zip code for the delivery date.  All of the
     * returned products will be available.
     * @param con
     * @param deliveryDate
     * @param zip
     * @param numberOfResults max number of results.
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getMostPopularProducts(Connection con, Date deliveryDate, String zip, int numberOfResults, String companyId) throws Exception
    {
        PASDAO dao = new PASDAO();

        ProductAvailVO[] products = dao.getMostPopularProducts(con, deliveryDate, zip, numberOfResults, companyId);

        return products;

    }

    /**
     * Get a list of the most popular products.  Do not perform an availability check.
     * @param con
     * @param numberOfResults max number of results.
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getMostPopularProducts(Connection con, int numberOfResults, String companyId) throws Exception
    {
        PASDAO dao = new PASDAO();

        ProductAvailVO[] products = dao.getMostPopularProducts(con, numberOfResults, companyId);

        return products;

    }

   /* *//**
     * Get the list of available ship dates for each of the ship methods for the specified product
     * and zip code.  The zip code field is optional.
     * @param con Database Connection
     * @param productId
     * @param numberOfDays Number of days to go out into the future for delivery date
     * @param zipCode zip Code to process for, optional
     * @return
     * @throws RemoteException
     *//*
     public PASShipDateAvailVO getAvailableShipDates(Connection con, String productId, int numberOfDays, String zipCode) throws Exception
    {
        PASDAO dao = new PASDAO();
        PASShipDateAvailVO vo = new PASShipDateAvailVO();

        vo = dao.getAvailableShipDates(con, productId, numberOfDays, zipCode);

        return vo;

    }*/


     /**
      * Get the list of available ship dates for each of the ship methods for the specified product
      * and zip code.  The zip code field is optional.
      * @param con Database Connection
      * @param productId
      * @param numberOfDays Number of days to go out into the future for delivery date
      * @param zipCode zip Code to process for, optional
      * @param addOns Add Ons for the Product, optional
      * @return
      * @throws RemoteException
      */
      public PASShipDateAvailVO getAvailableShipDates(Connection con, String productId, int numberOfDays, String zipCode,String sourceCode, List<String> addOns) throws Exception
     {


    	 logger.info("getAvailableShipDates START");
  		 logger.info(productId + " " + numberOfDays+ " " + zipCode);
  		 Timer productAvailTimer = new Timer();
    	 PASDAO dao = new PASDAO();
         PASShipDateAvailVO vo = new PASShipDateAvailVO();

         /*
 		 * Check for Shipping System first for the given productId
 		 */
 		ProductDetailVO productDetailVO = getProductDetails(con, productId);
 		List<Calendar> shipDateND = new ArrayList<Calendar>();
 		List<Calendar> shipDateGR = new ArrayList<Calendar>();
 		List<Calendar> shipDateSA = new ArrayList<Calendar>();
 		List<Calendar> shipDate2D = new ArrayList<Calendar>();
 		List<Calendar> shipDateSU = new ArrayList<Calendar>();
 		List<BigDecimal> shipChargesND = new ArrayList<BigDecimal>();
 		List<BigDecimal> shipCharges2D = new ArrayList<BigDecimal>();
 		List<BigDecimal> shipChargesGR = new ArrayList<BigDecimal>();
 		List<BigDecimal> shipChargesSA = new ArrayList<BigDecimal>();
 		List<BigDecimal> shipChargesSU = new ArrayList<BigDecimal>();
 		List<BigDecimal> sundayUpChgs = new ArrayList<BigDecimal>();


 		if (productDetailVO != null && productDetailVO.getShippingSystem().equals(PASConstants.SHIPPING_SYSTEM_WEST)) {

                 ProductAvailVO[] paVOs = this.getProductAvailableDates(con, productId, zipCode,sourceCode, numberOfDays, addOns);
                 if (paVOs != null && paVOs.length > 0) {

                	for(ProductAvailVO paVO : paVOs){
                		if(paVO.getShipDateND()!=null){
                			shipDateND.add(paVO.getShipDateND());
                			if (paVO.isHasCharges()) {
                			    shipChargesND.add(paVO.getCharges());
                			}
                		}
                		if(paVO.getShipDate2D()!=null){
                			shipDate2D.add(paVO.getShipDate2D());
                			if (paVO.isHasCharges()) {
                			    shipCharges2D.add(paVO.getCharges());
                			}
                		}
                		if(paVO.getShipDateGR()!=null){
                			shipDateGR.add(paVO.getShipDateGR());
                			if (paVO.isHasCharges()) {
                			    shipChargesGR.add(paVO.getCharges());
                			}
                		}
                		if(paVO.getShipDateSA()!=null){
                			shipDateSA.add(paVO.getShipDateSA());
                			if (paVO.isHasCharges()) {
                			    shipChargesSA.add(paVO.getCharges());
                			}
                		}

                		if(paVO.getShipDateSU()!=null){
                			shipDateSU.add(paVO.getShipDateSU());
                			if (paVO.isHasCharges()) {
                			    shipChargesSU.add(paVO.getCharges());
                			    sundayUpChgs.add(paVO.getSundayUpCharges());
                			}
                		}
                	}

                	vo.setGroundDeliveryDates(shipDateGR.toArray(new Calendar[shipDateGR.size()]));
                	vo.setNextDayDeliveryDates(shipDateND.toArray(new Calendar[shipDateND.size()]));
                	vo.setSaturdayDeliveryDates(shipDateSA.toArray(new Calendar[shipDateSA.size()]));
                	vo.setTwoDayDeliveryDates(shipDate2D.toArray(new Calendar[shipDate2D.size()]));
                	vo.setSundayDeliveryDates(shipDateSU.toArray(new Calendar[shipDateSU.size()]));
                	vo.setNextDayCharges(shipChargesND.toArray(new BigDecimal[shipChargesND.size()]));
                	vo.setTwoDayCharges(shipCharges2D.toArray(new BigDecimal[shipCharges2D.size()]));
                	vo.setGroundCharges(shipChargesGR.toArray(new BigDecimal[shipChargesGR.size()]));
                	vo.setSaturdayCharges(shipChargesSA.toArray(new BigDecimal[shipChargesSA.size()]));
                	vo.setSundayCharges(shipChargesSU.toArray(new BigDecimal[shipChargesSU.size()]));
                	vo.setSundayUpCharges(sundayUpChgs.toArray(new BigDecimal[sundayUpChgs.size()]));
                 }

 		} else {
 			 vo = dao.getAvailableShipDates(con, productId, numberOfDays, zipCode);

 		}



 		productAvailTimer.stop();
		logger.info("getAvailableShipDates FINISH took "
				+ productAvailTimer.getMilliseconds() + " ms");
         return vo;

     }

    /**
     * Get the product availability for a product.  This returns details of the availability.
     * @param productId
     * @param deliveryDate
     * @param countryCode
     * @return
     * @throws Exception
     */

    /**
     * Get the all available delivery dates for a product.  This returns details of the availability.
     * @param productId
     * @param zip
     * @param days
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getProductAvailableDates(Connection con, String productId, String zip,String sourceCode, int days, List<String> addOns) throws Exception
    {

    	logger.info("getProductAvailableDates START");
    	Timer productAvailTimer = new Timer();
    	PASDAO dao = new PASDAO();
        ProductAvailVO[] dates = null;
        List<ProductAvailVO> productAvailVOList = new ArrayList<ProductAvailVO>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, days);
        boolean hasPACDates = false;
        String affiliateCode = "";
        if(sourceCode == null){
        	sourceCode = PASConstants.PAC_WS_SOURCE_CODE;
        }

        /*
         * Check for Shipping System first for the given productId
         */

        logger.debug("PAC:Calling for Shipping system");
    	ProductDetailVO productDetailVO = getProductDetails(con, productId);

    	if(productDetailVO != null && productDetailVO.getShippingSystem().equals(PASConstants.SHIPPING_SYSTEM_WEST)){
    		
    		if(productDetailVO.getStatus().equals("A")){

    			try {
    				
    		    affiliateCode = FTDCommonUtils.getAffiliateCode(con, sourceCode) ;
    		    logger.info("affiliateCode---->"+affiliateCode);
    			logger.debug("getProductAvailableDates | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
    			logger.debug("getProductAvailableDates - Calling PAC Service");
        		PACRequest pacRequest = new PACRequest(productDetailVO.getNovatorId(), addOns, sourceCode, Integer.valueOf(days),
        				new Date(), zip, null, Boolean.TRUE, null, Boolean.FALSE, Boolean.FALSE,affiliateCode);
        		PACSClient client = new PACSClient(PASConstants.PAC_WS_GET_PRODUCT_AVAILABLE_DATES_AND_CHARGES);
        		PACResponse response = client.sendRequest(pacRequest);
        		logger.debug("getProductAvailableDates - PAC Serivice call finished");
        		List<AvailableDates> avlDates = response.getAvailableDates();

        		if(!avlDates.isEmpty()){
        			for(AvailableDates avlDate: avlDates){

        				ProductAvailVO productAvailVO = new ProductAvailVO();

        				if(avlDate.getShippingMethod().equals(ShipMethodType.NextDay)){
        					productAvailVO.setShipDateND(getDateFromString(avlDate.getDeliveryDate()));
        					productAvailVO.setShipDateNDCutoff(PASConstants.PAC_CUT_OFF);
        				}
        				if(avlDate.getShippingMethod().equals(ShipMethodType.Saturday)){
        					productAvailVO.setShipDateSA(getDateFromString(avlDate.getDeliveryDate()));
        					productAvailVO.setShipDateSACutoff(PASConstants.PAC_CUT_OFF);
        				}

        				if(avlDate.getShippingMethod().equals(ShipMethodType.Ground)){
        					productAvailVO.setShipDateGR(getDateFromString(avlDate.getDeliveryDate()));
        					productAvailVO.setShipDateGRCutoff(PASConstants.PAC_CUT_OFF);
        				}
        				if(avlDate.getShippingMethod().equals(ShipMethodType.Sunday)){
        					productAvailVO.setShipDateSU(getDateFromString(avlDate.getDeliveryDate()));
        					productAvailVO.setShipDateSUCutoff(PASConstants.PAC_CUT_OFF);
        				}

        				if(avlDate.getShippingMethod().equals(ShipMethodType.TwoDay)){
        					productAvailVO.setShipDate2D(getDateFromString(avlDate.getDeliveryDate()));
        					productAvailVO.setShipDate2DCutoff(PASConstants.PAC_CUT_OFF);
        				}

        				productAvailVO.setDeliveryDate(getDateFromString(avlDate.getDeliveryDate()));
        				productAvailVO.setIsAvailable(Boolean.TRUE);
        				productAvailVO.setIsAddOnAvailable(isAddOnAvailable(response.getAddonIds(), addOns));

        				BigDecimal shipCharges = new BigDecimal("0");
        				boolean hasCharges = false;
        				if (avlDate.getCharges() != null) {
        					Charges charges = avlDate.getCharges();
        					for (Charge charge: charges.getCharge())
        					{
        						if(ChargesType.SundayUpcharge.equals(charge.getType()) || ChargesType.SundayCost.equals(charge.getType()))
        							productAvailVO.setSundayUpCharges(charge.getValue());

        						shipCharges = shipCharges.add(charge.getValue());
        						hasCharges = true;
        					}
        				}
        				productAvailVO.setCharges(shipCharges);
        				productAvailVO.setHasCharges(hasCharges);

        				productAvailVOList.add(productAvailVO);
        			}

        		}else{
        			if(!response.getError().isEmpty()){
        				for(com.ftd.pac.util.vo.Error error : response.getError()){
        					if(PASConstants.ERROR_CODE_320 == error.getCode()){
        		    			logger.debug("getProductAvailableDates - No Product Found, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
        		    			break;
        					}else{
        						logger.error("Error in Calling PAC Service-getProductAvailableDatesAndDates, Error Code : "
        								+error.getCode()+" Message : " +error.getMessage());
        					}
        				}
        			}
        		}

    		} catch (Exception e) {
    			logger.error(e);
    		}

    		}else{
    			logger.debug("getProductAvailableDates | Shipping System : "+productDetailVO.getShippingSystem()+" | Status : "+productDetailVO.getStatus());
    			logger.debug("getProductAvailableDates - No Product Found");
    		}
    		hasPACDates = true;
			dates = productAvailVOList.toArray(new ProductAvailVO[productAvailVOList.size()]);
			logger.info("getProductAvailableDates - Dates(size)returned from PAC : "+dates.length);

    	}else{
    		dates = dao.getAllProductAvailability(con, productId, zip, cal.getTime());
    	}

    	logger.info("hasPACDates: " + hasPACDates);
        if (hasPACDates && (productDetailVO != null && productDetailVO.getShipMethodFlorist() != null &&
        		productDetailVO.getShipMethodFlorist().equalsIgnoreCase("Y"))) {

        	logger.info("FTD West SDFC product");
        	ProductAvailVO[] pasDates = dao.getAllProductAvailabilityFlorist(con, productId, zip, cal.getTime());

        	// If Apollo PAS returns dates, combine them with PACS dates
        	if (pasDates != null && pasDates.length > 0) {
                List<ProductAvailVO> newDates = new ArrayList<ProductAvailVO>();
        	    int pasCnt=0;
        	    int pacsCnt=0;
            	ProductAvailVO paVOpas = null;
            	ProductAvailVO paVOpacs = null;
            	boolean finished = false;

            	while (!finished) {
                	if (pasCnt < pasDates.length) {
                		paVOpas = pasDates[pasCnt];
                	} else {
                		paVOpas = null;
                	}
                	if (pacsCnt < dates.length) {
                		paVOpacs = dates[pacsCnt];
                	} else {
                		paVOpacs = null;
                	}
                	if (paVOpas == null && paVOpacs == null) {
                		finished = true;
                	} else {
                	    if (paVOpas != null && paVOpacs == null) {
                	    	// only PAS dates are left
                	    	newDates.add(paVOpas);
                		    pasCnt = pasCnt + 1;
                	    } else if (paVOpas == null && paVOpacs != null) {
                	    	// only PACS dates are left
                	    	newDates.add(paVOpacs);
                		    pacsCnt = pacsCnt + 1;
                	    } else if (paVOpas.getDeliveryDate().compareTo(paVOpacs.getDeliveryDate()) < 0) {
                	    	// next PAS date is before next PACS date
                	    	newDates.add(paVOpas);
                		    pasCnt = pasCnt + 1;
                	    } else if (paVOpas.getDeliveryDate().compareTo(paVOpacs.getDeliveryDate()) == 0 &&
                	    		paVOpas.getFloristCutoffDate() != null) {
                	    	// next PAS date is the same as the next PACS date and has florist delivery
                	    	newDates.add(paVOpas);
                		    pasCnt = pasCnt + 1;
                		    //pacsCnt = pacsCnt + 1;
                	    } else {
                	    	// by default, next PACS date is before next PAS date
                	    	newDates.add(paVOpacs);
                		    pacsCnt = pacsCnt + 1;
                	    }
                	}
            	}

    			dates = newDates.toArray(new ProductAvailVO[newDates.size()]);
    			logger.info("new dates size: " + dates.length);

        	}

        }

    	productAvailTimer.stop();
        logger.info("getProductAvailableDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");

        return dates;
    }

    /**
     * Get the all available delivery dates for an international product.  This returns details of the availability.
     * @param productId
     * @param countryCode
     * @param days
     * @return
     * @throws Exception
     */
    public ProductAvailVO[] getInternationalProductAvailableDates(Connection con, String productId, String countryCode, int days) throws Exception
    {
        PASDAO dao = new PASDAO();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        if(days <= 0) {
        	days = (int)(getMaxDeliveryDate(con));
        }
        cal.add(Calendar.DATE, days);
        ProductAvailVO[] dates = dao.getAllInternationalProductAvailability(con, productId, countryCode, cal.getTime());

        return dates;
    }

    public String[] isProductListAvailable(Connection con, String zipCode, Date deliveryDate, String[] productList) throws Exception {

        PASDAO dao = new PASDAO();
        String[] returnList = dao.isProductListAvailable(con, zipCode, deliveryDate, productList);

        return returnList;
    }

    public String[] isInternationalProductListAvailable(Connection con, String countryCode, Date deliveryDate, String[] productList) throws Exception {

        PASDAO dao = new PASDAO();
        String[] returnList = dao.isInternationalProductListAvailable(con, countryCode, deliveryDate, productList);

        return returnList;
    }

    public ProductDetailVO getProductDetails(Connection con, String productId) throws Exception {

        PASDAO dao = new PASDAO();
        ProductDetailVO productDetailVO = dao.getProductDetails(con, productId);

        return productDetailVO;
    }


    private Date getDateFromString(String dateStr) throws Exception{

    	SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
    	return sdf.parse(dateStr);
    }

    private static boolean isSaturdayDelivery(Date deliveryDate) {
		boolean isSaturday=false;

			Calendar cal=Calendar.getInstance();
			cal.setTime(deliveryDate);
			isSaturday= Calendar.SATURDAY== cal.get(Calendar.DAY_OF_WEEK);

		return isSaturday;
	}

    private Boolean isAddOnAvailable(String pacAddOns, List<String> addOns){


    	Boolean isAddOnAvailable;

    	if(StringUtils.isEmpty(pacAddOns)){
    		if (addOns == null || addOns.size() == 0) {
    			return Boolean.TRUE;
    		} else {
    		    return Boolean.FALSE;
    		}
    	}
		String[] addOnsSplit = pacAddOns.split(",");
		if(addOnsSplit.length == addOns.toArray().length){
			isAddOnAvailable = Boolean.TRUE;
		}else
			isAddOnAvailable = Boolean.FALSE;

		return isAddOnAvailable;
    }
    
	private static long getMaxDeliveryDate(Connection con) throws Exception {
		String maxDays = null;
		long maxDeliveryDate = 0;

		// Try cache first
		GlobalParmHandler parmHandler = (GlobalParmHandler) CacheManager.getInstance().getHandler("CACHE_NAME_GLOBAL_PARM");
		if (parmHandler != null) {
			FTDAppsGlobalParmsVO ftdAppsParms = parmHandler.getFTDAppsGlobalParms();
			maxDeliveryDate = ftdAppsParms.getDeliveryDaysOut();
		} else {
			PASDAO dao = new PASDAO();
			maxDeliveryDate = dao.getMaxDeliveryDate(con);
		}

		return maxDeliveryDate;
	}
}
