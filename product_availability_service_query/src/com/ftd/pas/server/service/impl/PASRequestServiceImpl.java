package com.ftd.pas.server.service.impl;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;

import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;

import com.ftd.pas.server.ProductAvailabilityBO;

import com.ftd.pas.web.util.Timer;

import java.rmi.RemoteException;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * This class handles the implementation of the Product Availability Service requests.
 * 
 */
public class PASRequestServiceImpl implements com.ftd.pas.webservices.PASRequestService
{
    protected ConfigurationUtil cu;
    protected Logger logger = new Logger(this.getClass().getName());

    /**
     * Get the next available delivery date for a product and zip.
     * @param productId
     * @param zip
     * @return
     * @throws RemoteException
     */
    public Calendar getNextAvailableDeliveryDate(String productId, String zip) throws RemoteException 
    {
        logger.debug("getNextAvailableDeliveryDate START");
        Calendar deliveryDateCal = null;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            Date deliveryDate = bo.getNextAvailableDeliveryDate(con, productId, zip,null,null);
            
            if (deliveryDate != null)
            {
                deliveryDateCal = Calendar.getInstance();
                deliveryDateCal.setTime(deliveryDate);
                logger.debug(productId + " " + zip + " available on " + deliveryDateCal.getTime());
            }
        }
        catch (Exception e)
        {
            logger.error("Error in getNextAvailableDeliveryDate", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getNextAvailableDeliveryDate FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return deliveryDateCal;
    }

    /**
     * Check to see if a product is available for a delivery date in a zip.
     * @param productId
     * @param deliveryDate
     * @param zip
     * @return
     * @throws RemoteException
     */
    public Boolean isProductAvailable(String productId, Calendar deliveryDate, String zip) throws RemoteException 
    {
        logger.debug("isProductAvailable START");
        Boolean productAvailable = Boolean.FALSE;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            productAvailable = bo.isProductAvailable(con, productId, zip, deliveryDate.getTime());
            
            logger.debug(productId + " " + zip + " " + deliveryDate.getTime() + " available = " + productAvailable);
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("isProductAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return productAvailable;
    }

    /**
     * Get the details of product availability for a product, delivery date and zip code.
     * @param productId
     * @param deliveryDate
     * @param zip
     * @return
     * @throws RemoteException
     */
    public ProductAvailVO getProductAvailability(String productId, Calendar deliveryDate, String zip) throws RemoteException 
    {
        logger.debug("getProductAvailability START");
        ProductAvailVO productAvailable = null;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            productAvailable = bo.getProductAvailability(con, productId, deliveryDate.getTime(),zip,null,null);
            
            logger.debug(productId + " " + zip + " " + deliveryDate.getTime() + " available = " + productAvailable);
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getProductAvailability FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return productAvailable;
    }

    /**
     * Check to see if any product is available for delivery in the zip code.
     * @param deliveryDate
     * @param zip
     * @return
     * @throws RemoteException
     */
    public Boolean isAnyProductAvailable(Calendar deliveryDate, String zip) throws java.rmi.RemoteException 
    {
        logger.debug("isAnyProductAvailable START");
        Boolean productAvailable = Boolean.FALSE;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            productAvailable = bo.isAnyProductAvailable(con, deliveryDate.getTime(), zip);
            
            logger.debug(zip + " " + deliveryDate.getTime() + " available = " + productAvailable);
        }
        catch (Exception e)
        {
            logger.error("Error in isAnyProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("isAnyProductAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return productAvailable;
    }

    /**
     * Get the list of the most popular products that are available in the zip for the delivery date.
     * @param deliveryDate
     * @param zip
     * @param numberOfResults
     * @return
     * @throws RemoteException
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getMostPopularProducts(Calendar deliveryDate, String zip, int numberOfResults, String companyId) throws RemoteException 
    {
        logger.debug("getMostPopularProducts START");
        ProductAvailVO[] products = new ProductAvailVO[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            if (zip != null && deliveryDate != null && !zip.equals(""))
            {
                products = bo.getMostPopularProducts(con, deliveryDate.getTime(), zip, numberOfResults, companyId);
                logger.debug("mpp(" + numberOfResults + ") = " + zip  + " " + companyId + " " + deliveryDate.getTime() + " products = " + products.length);
            }
            else
            {
                products = bo.getMostPopularProducts(con, numberOfResults, companyId);
                logger.debug("mpp = products = " + products.length);
            }
            
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getMostPopularProducts FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return products;
    }

    /**
     * Get the list of florists taht will deliver int he zip code.
     * @param deliveryDate
     * @param zip
     * @return
     * @throws RemoteException
     */
    public java.lang.String[] getFlorists(Calendar deliveryDate, String zip) throws RemoteException 
    {
        logger.debug("getFlorists START");
        String[] florists = new String[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            florists = bo.getAvailableFlorists(con, deliveryDate.getTime(), zip);
            
            logger.debug(zip  + " " + deliveryDate.getTime() + " florists = " + florists.length);
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getFlorists FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return florists;
    }

    /**
     * Check for product availability for an international product in the specified country on the delivery date.
     * @param productId
     * @param deliveryDate
     * @param country
     * @return
     * @throws RemoteException
     */
    public Boolean isInternationalProductAvailable(String productId, Calendar deliveryDate, String country) throws RemoteException 
    {
        logger.debug("isInternationalProductAvailable START");
        Boolean productAvailable = Boolean.FALSE;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            productAvailable = bo.isInternationalProductAvailable(con, productId, deliveryDate.getTime(), country);
            
            logger.debug(productId + " " + country + " " + deliveryDate.getTime() + " available = " + productAvailable);
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("isInternationalProductAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return productAvailable;
    }

    /**
     * Get the list of available ship dates for each of the ship methods for the specified product
     * and zip code.  The zip code field is optional.
     * @param productId
     * @param numberOfDays Number of days to go out into the future for delivery date
     * @param zipCode zip Code to process for, optional
     * @return
     * @throws RemoteException
     */
     public PASShipDateAvailVO getAvailableShipDates(String productId, int numberOfDays, String zipCode) throws RemoteException
    {
        logger.debug("getAvailableShipDates START");
        PASShipDateAvailVO shipDates = null;
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try
        {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            shipDates = bo.getAvailableShipDates(con, productId, numberOfDays, zipCode,null, null);
            
            logger.debug(productId + " " + numberOfDays + " " + zipCode + " shipDates.XX.size = " + shipDates.getNextDayDeliveryDates().length + "/" + shipDates.getTwoDayDeliveryDates().length + "/" +shipDates.getGroundDeliveryDates().length + "/" +shipDates.getSaturdayDeliveryDates().length)  ;
        }
        catch (Exception e)
        {
            logger.error("Error in isProductAvailable", e);
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (Exception e)
            {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getAvailableShipDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return shipDates;
    }


    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    protected Connection getNewConnection() throws Exception
    {
        // get database connection      
   
        String datasource = getConfigurationUtil().getPropertyNew(PASConstants.PROPERTY_FILE, PASConstants.DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }
    
    /**
     * Lazy initialization of the ConfigurationUtil object.
     * @return
     * @throws Exception
     */
    protected ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }

    /**
     * Get the list of available delivery dates in the zip for the product id.
     * @param productId
     * @param zipCode
     * @param days
     * @return
     * @throws RemoteException
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(String productId, String zipCode, int days) throws RemoteException {
        logger.debug("getProductAvailableDates START");
        ProductAvailVO[] dates = new ProductAvailVO[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            dates = bo.getProductAvailableDates(con, productId, zipCode,null, days, new ArrayList<String>());
        } catch (Exception e) {
            logger.error("Error in isProductAvailable", e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getProductAvailableDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return dates;
    }

    /**
     * Get the list of available delivery dates in the zip for the product id.
     * @param con
     * @param productId
     * @param zipCode
     * @param days
     * @return
     * @throws RemoteException
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(Connection con, String productId, String zipCode, int days) throws RemoteException {
        logger.debug("getProductAvailableDates START");
        ProductAvailVO[] dates = new ProductAvailVO[0];
        Timer productAvailTimer = new Timer();
        
        try {
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            dates = bo.getProductAvailableDates(con, productId, zipCode,null, days, new ArrayList<String>());
        } catch (Exception e) {
            logger.error("Error in isProductAvailable", e);
        } 
        productAvailTimer.stop();
        logger.debug("getProductAvailableDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return dates;
    }

    /**
     * Get the list of available delivery dates in the country for the product id.
     * @param productId
     * @param countryCode
     * @param days
     * @return
     * @throws RemoteException
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(String productId, String countryCode, int days) throws RemoteException {
        logger.debug("getInternationalProductAvailableDates START");
        ProductAvailVO[] dates = new ProductAvailVO[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            dates = bo.getInternationalProductAvailableDates(con, productId, countryCode, days);
        } catch (Exception e) {
            logger.error("Error in getInternationalProductAvailableDates", e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("getInternationalProductAvailableDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return dates;
    }

    /**
     * Get the list of available delivery dates in the country for the product id.
     * @param con
     * @param productId
     * @param countryCode
     * @param days
     * @return
     * @throws RemoteException
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(Connection con, String productId, String countryCode, int days) throws RemoteException {
        logger.debug("getInternationalProductAvailableDates START");
        ProductAvailVO[] dates = new ProductAvailVO[0];
        Timer productAvailTimer = new Timer();
        
        try {
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            dates = bo.getInternationalProductAvailableDates(con, productId, countryCode, days);
        } catch (Exception e) {
            logger.error("Error in getInternationalProductAvailableDates", e);
        } 
        productAvailTimer.stop();
        logger.debug("getInternationalProductAvailableDates FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return dates;
    }

    /**
     * Get the list of products available in the zip code.
     * @param deliveryDate
     * @param zipCode
     * @param productList
     * @return
     * @throws RemoteException
     */
    public java.lang.String[] isProductListAvailable(String zipCode, Calendar deliveryDate, String[] productList) throws RemoteException 
    {
        logger.debug("isProductListAvailable START");
        String[] products = new String[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            products = bo.isProductListAvailable(con, zipCode, deliveryDate.getTime(), productList);
        } catch (Exception e) {
            logger.error("Error in isProductListAvailable", e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("isProductListAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return products;
    }

    /**
     * Get the list of products available in the zip code.
     * @param con
     * @param deliveryDate
     * @param zipCode
     * @param productList
     * @return
     * @throws RemoteException
     */
    public java.lang.String[] isProductListAvailable(Connection con, String zipCode, Calendar deliveryDate, String[] productList) throws RemoteException 
    {
        logger.debug("isProductListAvailable START");
        String[] products = new String[0];
        Timer productAvailTimer = new Timer();
        
        try {
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            products = bo.isProductListAvailable(con, zipCode, deliveryDate.getTime(), productList);
        } catch (Exception e) {
            logger.error("Error in isProductListAvailable", e);
        } 
        productAvailTimer.stop();
        logger.debug("isProductListAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return products;
    }

    /**
     * Get the list of products available in the country.
     * @param deliveryDate
     * @param countryCode
     * @param productList
     * @return
     * @throws RemoteException
     */
    public java.lang.String[] isInternationalProductListAvailable(String countryCode, Calendar deliveryDate, String[] productList) throws RemoteException 
    {
        logger.debug("isInternationalProductListAvailable START");
        String[] products = new String[0];
        Timer productAvailTimer = new Timer();
        
        Connection con = null;
        try {
            con = getNewConnection();
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            products = bo.isInternationalProductListAvailable(con, countryCode, deliveryDate.getTime(), productList);
        } catch (Exception e) {
            logger.error("Error in isInternationalProductListAvailable", e);
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                logger.error("Error closing connection",e);
            }
        }
        productAvailTimer.stop();
        logger.debug("isInternationalProductListAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return products;
    }

    /**
     * Get the list of products available in the country.
     * @param con
     * @param deliveryDate
     * @param countryCode
     * @param productList
     * @return
     * @throws RemoteException
     */
    public java.lang.String[] isInternationalProductListAvailable(Connection con, String countryCode, Calendar deliveryDate, String[] productList) throws RemoteException 
    {
        logger.debug("isInternationalProductListAvailable START");
        String[] products = new String[0];
        Timer productAvailTimer = new Timer();
        
        try {
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            products = bo.isInternationalProductListAvailable(con, countryCode, deliveryDate.getTime(), productList);
        } catch (Exception e) {
            logger.error("Error in isInternationalProductListAvailable", e);
        } 
        productAvailTimer.stop();
        logger.debug("isInternationalProductListAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return products;
    }
    
    /**
     * Check to see if any product is available for delivery in the zip code.
     * @param deliveryDate
     * @param zip
     * @return
     * @throws RemoteException
     */
    public Boolean isAnyProductAvailable(Connection con, Calendar deliveryDate, String zip) throws java.rmi.RemoteException 
    {
        logger.debug("isAnyProductAvailable START");
        Boolean productAvailable = Boolean.FALSE;
        Timer productAvailTimer = new Timer();
        
        try
        {
            ProductAvailabilityBO bo = new ProductAvailabilityBO();
            
            productAvailable = bo.isAnyProductAvailable(con, deliveryDate.getTime(), zip);
            
            logger.debug(zip + " " + deliveryDate.getTime() + " available = " + productAvailable);
        }
        catch (Exception e)
        {
            logger.error("Error in isAnyProductAvailable", e);
        }
        
        productAvailTimer.stop();
        logger.debug("isAnyProductAvailable FINISH took " + productAvailTimer.getMilliseconds() + " ms");
        
        return productAvailable;
    }    
}
