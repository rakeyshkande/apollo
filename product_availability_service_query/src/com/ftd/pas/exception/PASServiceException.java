package com.ftd.pas.exception;

/**
 * Exception representing an error with the Product Availability Service
 */
public class PASServiceException extends Exception
{
    public PASServiceException(Throwable cause)
    {
        super(cause);
    }

    public PASServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PASServiceException(String message)
    {
        super(message);
    }

    public PASServiceException()
    {
    }
}
