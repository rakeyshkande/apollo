package com.ftd.pas.client.service.impl;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.service.PASRequestService;
import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;

import com.ftd.pas.exception.PASServiceException;
import com.ftd.pas.webservices.PASRequestServiceServiceLocator;
import com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub;

import java.util.Calendar;
import java.util.Date;

/**
 * Remote version of the PASRequestService implementation.  This implementation
 * of the service just passes the request on to a server service through a servlet 
 * connection.
 */
public class PASRequestServiceRemoteImpl implements PASRequestService
{
    protected Logger logger = new Logger(this.getClass().getName());
    
    protected String serverAddress = null;
    
    protected ProductAvailabilitySoapBindingStub cachedBinding = null;
    
    public PASRequestServiceRemoteImpl()
    {
    }

    /**
     * Get the next available delivery date.  The request is proxied to the web service.
     * @param productId
     * @param zip
     * @return
     * @throws PASServiceException
     */
    public Date getNextAvailableDeliveryDate(String productId, String zip) throws PASServiceException
    {
        Date deliveryDate = null;
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCal = binding.getNextAvailableDeliveryDate(productId, zip);
            if (deliveryDateCal != null)
            {
                deliveryDate = deliveryDateCal.getTime();
            }
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return deliveryDate;
     }

    public Boolean isProductAvailable(String productId, Date deliveryDate, String zip) throws PASServiceException
    {
        Boolean available = false;
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            available = binding.isProductAvailable(productId, deliveryDateCalendar, zip);
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return available;
    }

    public ProductAvailVO getProductAvailability(String productId, Date deliveryDate, String zip) throws PASServiceException
    {
        ProductAvailVO productAvailVO = null;
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            productAvailVO = binding.getProductAvailability(productId, deliveryDateCalendar, zip);
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return productAvailVO;
    }

    public Boolean isAnyProductAvailable(Date deliveryDate, String zip) throws PASServiceException
    {
        Boolean available = false;
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            available = binding.isAnyProductAvailable(deliveryDateCalendar, zip);
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return available;
    }

    public ProductAvailVO[] getMostPopularProducts(Date deliveryDate, String zip, int numberOfResults, String companyId) 
            throws PASServiceException
    {
        ProductAvailVO[] available = new ProductAvailVO[0];
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = null;
            if (deliveryDate != null)
            {
                deliveryDateCalendar = Calendar.getInstance();
                deliveryDateCalendar.setTime(deliveryDate);
            }
            Object[] objects = binding.getMostPopularProducts(deliveryDateCalendar, zip, numberOfResults, companyId);
            available = new ProductAvailVO[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                available[i] = (ProductAvailVO) objects[i];
            }
            
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return available;
    }

    public String[] getFlorists(Date deliveryDate, String zip) throws PASServiceException
    {
        String[] florists = new String[0];
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            Object[] objects = binding.getFlorists(deliveryDateCalendar, zip);
            florists = new String[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                florists[i] = (String) objects[i];
            }
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return florists;
    }

    public Boolean isInternationalProductAvailable(String product, Date deliveryDate, String country) throws PASServiceException
    {
        Boolean available = false;
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            available = binding.isInternationalProductAvailable(product, deliveryDateCalendar, country);
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return available;
    }
    
    public PASShipDateAvailVO getAvailableShipDates(String productId, int numberOfDays, String zipCode) throws PASServiceException
    {
        PASShipDateAvailVO shipDates = new PASShipDateAvailVO();
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            shipDates = binding.getAvailableShipDates(productId, numberOfDays, zipCode);
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return shipDates;
    }

    /**
     * Get the soap binding.  Setup the remote connection to be the proper PAS server.
     * 
     * @return
     * @throws Exception
     */
    protected ProductAvailabilitySoapBindingStub getSoapBinding() throws Exception
    {
        if(cachedBinding == null)
        {
          ProductAvailabilitySoapBindingStub binding;
          PASRequestServiceServiceLocator locator;
          String serverAddress = getServerAddress();
          
          locator = new PASRequestServiceServiceLocator();
          locator.setProductAvailabilityEndpointAddress(serverAddress);
  
          binding = (ProductAvailabilitySoapBindingStub)locator.getProductAvailability();
          cachedBinding = binding;
        
        }
        
        return cachedBinding;        
    }
    
    /**
     * Lazy initialization of the server address.  The server address is retrieved
     * from global_parms.
     * @return
     */
    protected String getServerAddress() throws Exception
    {
        if (serverAddress == null)
        {
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            serverAddress = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT, PASConstants.PAS_URL);
        }
        
        return serverAddress;
    }
    
    /**
     * Back door method for performing unit tests to a specific address.
     * @param serverAddress
     */
    public void setServerAddress(String serverAddress)
    {
        this.serverAddress = serverAddress;
    }

    /**
     * Get the all available delivery dates for a product.  This returns details of the availability.
     * @param productId
     * @param zip
     * @param days
     * @return
     * @throws Exception
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(String productId, String zip, int days) throws PASServiceException 
    {
        logger.debug("getProductAvailableDates START");
        ProductAvailVO[] availableList = new ProductAvailVO[0];

        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Object[] objects = binding.getProductAvailableDates(productId, zip, days);
            availableList = new ProductAvailVO[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                availableList[i] = (ProductAvailVO) objects[i];
            }
            
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        
        return availableList;
    }

    /**
     * Get the all available delivery dates for an international product.  This returns details of the availability.
     * @param productId
     * @param countryCode
     * @param days
     * @return
     * @throws Exception
     */
    public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(String productId, String countryCode, int days) throws PASServiceException 
    {
        logger.debug("getProductAvailableDates START");
        ProductAvailVO[] availableList = new ProductAvailVO[0];

        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Object[] objects = binding.getInternationalProductAvailableDates(productId, countryCode, days);
            availableList = new ProductAvailVO[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                availableList[i] = (ProductAvailVO) objects[i];
            }
            
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        
        return availableList;
    }
    
    public String[] isProductListAvailable(String zipCode, Date deliveryDate, String[] productList) throws PASServiceException {
        logger.debug("isProductListAvailable START");

        String[] products = new String[0];
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            Object[] objects = binding.isProductListAvailable(zipCode, deliveryDateCalendar, productList);
            products = new String[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                products[i] = (String) objects[i];
            }
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return products;
    }

    public String[] isInternationalProductListAvailable(String countryCode, Date deliveryDate, String[] productList) throws PASServiceException {
        logger.debug("isProductListAvailable START");

        String[] products = new String[0];
        ProductAvailabilitySoapBindingStub binding;
        try
        {
            binding = getSoapBinding();
            Calendar deliveryDateCalendar = Calendar.getInstance();
            deliveryDateCalendar.setTime(deliveryDate);
            Object[] objects = binding.isInternationalProductListAvailable(countryCode, deliveryDateCalendar, productList);
            products = new String[objects.length];
            for (int i=0 ;i < objects.length; i++)
            {
                products[i] = (String) objects[i];
            }
        } 
        catch (Exception e)
        {
            logger.error("Error processing PAS Request",e);
            throw new PASServiceException("Error Processing PAS Request",e);
        }
        return products;
    }
}
