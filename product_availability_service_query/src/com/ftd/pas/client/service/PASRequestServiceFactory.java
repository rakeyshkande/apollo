package com.ftd.pas.client.service;

import com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.common.service.PASRequestService;

public class PASRequestServiceFactory
{
    public static PASRequestService createPASRequestService()
    {
        PASRequestService service = new PASRequestServiceRemoteImpl();
        
        return service;
    }
}
