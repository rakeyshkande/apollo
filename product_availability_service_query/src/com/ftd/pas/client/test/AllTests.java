package com.ftd.pas.client.test;


import junit.framework.Test;
import junit.framework.TestSuite;
import com.clarkware.junitperf.*;

public class AllTests
{
    private static    int maxElapsedMilliseconds = 1000000;
    private static    int maxUsers = 1;
    
    public static Test suite()
    {
        TestSuite suite;
        suite = new TestSuite("AllTests");
        
        buildServiceMethodTestCase("testGetNextAvailableDeliveryDate", suite);
        buildServiceMethodTestCase("testIsProductAvailable", suite);
        buildServiceMethodTestCase("testGetProductAvailability", suite);
        buildServiceMethodTestCase("testIsAnyProductAvailable", suite);
        buildServiceMethodTestCase("testGetMostPopularProducts", suite);
        buildServiceMethodTestCase("testGetFlorists", suite);
        buildServiceMethodTestCase("testIsInternationalProductAvailable", suite);
        buildServiceMethodTestCase("testGetAvailableShipDates", suite);

        return suite;
    }
    
    private static void buildServiceMethodTestCase(String methodName, TestSuite suite)
    {
        Test testCase = new PASRequestServiceRemoteImplTest(methodName);
        Test loadTest = new LoadTest(testCase, maxUsers);
        Test timedTest = new TimedTest(loadTest, maxElapsedMilliseconds);
        suite.addTest(timedTest);
        
    }
}
