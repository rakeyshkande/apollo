package com.ftd.pas.client.test;

import com.ftd.pas.client.service.PASRequestServiceFactory;
import com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl;
import com.ftd.pas.common.service.PASRequestService;

import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;

import java.util.Calendar;
import java.util.Date;

import junit.framework.TestCase;

public class PASRequestServiceRemoteImplTest extends TestCase
{
    private String floristProductId = "8212";
    private String vendorProductId = "BB02";
    private String zipCode = "60515";
    private String intProductId = "7818";
    private String country = "AR";
    private String companyId = "FTD";

    private Date today;
    private Date tomorrow;
    private Date dayAfterTomorrow;

    //private String serverAddress = "http://zinc2:7778/pasquery/services/ProductAvailability";
    private String serverAddress = "http://consumer-test.ftdi.com/pasquery/services/ProductAvailability";
    //private String serverAddress = "http://iron4:7778/pasquery/services/ProductAvailability";


    public PASRequestServiceRemoteImplTest(String sTestName)
    {
        super(sTestName);
    }


    protected void setUp() throws Exception 
    {
        Calendar calendar = Calendar.getInstance();
        today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_WEEK,1);
        tomorrow = calendar.getTime();
        
        calendar.add(Calendar.DAY_OF_WEEK,1);
        dayAfterTomorrow = calendar.getTime();
    }

    /**
     * Method for getting a PASRequestService and settign the address so the unit test will work.
     * @return
     */
    protected PASRequestService getPASRequestService()
    {
        PASRequestService pas = PASRequestServiceFactory.createPASRequestService();
        ((PASRequestServiceRemoteImpl) pas).setServerAddress(serverAddress);
        return pas;
    }
    
    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#getNextAvailableDeliveryDate(String,String)
     */
    public void testGetNextAvailableDeliveryDate()
    {
    
        PASRequestService pas = getPASRequestService();
        try
        {
            Date floristDeliveryDate = pas.getNextAvailableDeliveryDate(floristProductId, zipCode);
            assertNotNull(floristDeliveryDate);
            Date vendorDeliveryDate = pas.getNextAvailableDeliveryDate(vendorProductId, zipCode);
            assertNotNull(vendorDeliveryDate);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#isProductAvailable(String,java.util.Date,String)
     */
    public void testIsProductAvailable()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            Boolean available = pas.isProductAvailable(floristProductId, tomorrow, zipCode);
            assertNotNull(available);
            available = pas.isProductAvailable("X050", today, zipCode);
            assertNotNull(available);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#getProductAvailability(String,java.util.Date,String)
     */
    public void testGetProductAvailability()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            ProductAvailVO productAvailableFlorist = pas.getProductAvailability(floristProductId, tomorrow, zipCode);
            assertNotNull(productAvailableFlorist);

            ProductAvailVO productAvailableVendor = pas.getProductAvailability(vendorProductId, tomorrow, zipCode);
            assertNotNull(productAvailableVendor);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#isAnyProductAvailable(java.util.Date,String)
     */
    public void testIsAnyProductAvailable()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            Boolean available = pas.isAnyProductAvailable(tomorrow, zipCode);
            assertNotNull(available);
            assertEquals(available,Boolean.TRUE);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#getMostPopularProducts(java.util.Date,String,int,String)
     */
    public void testGetMostPopularProducts()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            ProductAvailVO[] products_avail = pas.getMostPopularProducts(dayAfterTomorrow, zipCode, 25, companyId);
            assertNotNull(products_avail);
            assertEquals(true,products_avail.length > 0);
            //printProductSummary(products_avail);

            ProductAvailVO[] products_noavail = pas.getMostPopularProducts(null, null, 25, companyId);
            assertNotNull(products_noavail);
            //printProductSummary(products_noavail);

        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }
    
    private void printProductSummary(ProductAvailVO[] prods)
    {
        for (int i=0; i < prods.length; i++)
        {
            System.out.println("Product " + prods[i].getIsAvailable() + " " + prods[i].getProductId() + " "  + prods[i].getPopularityOrderCount() + " " + prods[i].getFloristCutoffTime());    
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#getFlorists(java.util.Date,String)
     */
    public void testGetFlorists()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            String[] florists = pas.getFlorists(tomorrow, zipCode);
            assertNotNull(florists);
            assertEquals(true,florists.length > 0);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#isInternationalProductAvailable(String,java.util.Date,String)
     */
    public void testIsInternationalProductAvailable()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            Boolean available = pas.isInternationalProductAvailable(vendorProductId, dayAfterTomorrow, country);
            assertEquals(false,available.booleanValue());

            available = pas.isInternationalProductAvailable(intProductId, dayAfterTomorrow, country);
            assertEquals(true,available.booleanValue());
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }

    /**
     * @see com.ftd.pas.client.service.impl.PASRequestServiceRemoteImpl#getFlorists(java.util.Date,String)
     */
    public void testGetAvailableShipDates()
    {
        PASRequestService pas = getPASRequestService();
        try
        {
            PASShipDateAvailVO vo = pas.getAvailableShipDates(vendorProductId, 30, zipCode);
            assertNotNull(vo);
            assertNotNull(vo.getNextDayDeliveryDates());
            assertTrue(vo.getNextDayDeliveryDates().length > 0);
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            fail("Exception");
        }
    }
    
}
