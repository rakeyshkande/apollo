package com.ftd.pas.util;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.ProductAvailabilityBO;

/**
 * @author rksingh
 *
 */
public class PASUtil {

	private static Logger logger = new Logger("com.ftd.pas.util.PASUtil");
	private static int DEFAULT_NO_DAYS = 1;
	protected static ConfigurationUtil cu;

	/**This Method return ship date from delivery date and Ship method by calling PAS
	 * @param productID
	 * @param deliveryDate
	 * @param zipCode
	 * @param shipMethod
	 * @param country
	 * @return ProductAvailVO
	 * @throws Exception
	 */
	public static ProductAvailVO getPASShipDate(Connection con, String productID, Date deliveryDate,
			String zipCode, String shipMethod, String country, List<String> addons,String sourceCode) throws Exception {
		logger.info("getPASShipDate START");
		logger.info("productID: " + productID + " deliveryDate: "
				+ deliveryDate + " zipCode: " + zipCode + " shipMethod: "
				+ shipMethod + " country : " + country);

		String shipDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat sdf3 = new SimpleDateFormat("yyyyMMddHHmm");
	    ProductAvailVO paVO = null;
	    
		if (zipCode == null) {
			logger.error("zipCode is null in getPASShipDate method ");
			paVO.setShipDate(null);
			paVO.setErrorMessage("zipCode is null in getPASShipDate method");
			return paVO;
		}

		zipCode = getDomesticZipCode(zipCode, country);

		try {
			ProductAvailabilityBO paBO = new ProductAvailabilityBO();
			paVO = paBO.getProductAvailability(con,productID,
					deliveryDate, zipCode, addons,sourceCode);

		} catch (Exception e) {
			logger.error("Error in getProductAvailability", e);
		}

	    if (paVO.getIsAvailable()) {
			
			Date today = new Date();
	        String todayString = sdf3.format(today);
	        String cutoffString = null;

			if (paVO.getShipDateGR() != null && shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_STANDARD_CODE)) 
			{
				 cutoffString = sdf.format(paVO.getShipDateGR().getTime()) + paVO.getShipDateGRCutoff();
			
				if (todayString.compareTo(cutoffString) <= 0)
				{
				shipDate = getFormattedDate(paVO.getShipDateGR().getTime());
				}
			}

			if (paVO.getShipDate2D() != null && shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_TWO_DAY_CODE)) 
			{
				cutoffString = sdf.format(paVO.getShipDate2D().getTime()) + paVO.getShipDate2DCutoff();
				
				if (todayString.compareTo(cutoffString) <= 0)
				{
				shipDate = getFormattedDate(paVO.getShipDate2D().getTime());
				}
			}

			if (paVO.getShipDateND() != null && shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_NEXT_DAY_CODE)) 
			{
				cutoffString = sdf.format(paVO.getShipDateND().getTime()) + paVO.getShipDateNDCutoff();
				
				if (todayString.compareTo(cutoffString) <= 0)
				{
				shipDate = getFormattedDate(paVO.getShipDateND().getTime());
				}
			}

			if (paVO.getShipDateSA() != null && shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_SATURDAY_CODE)) 
			{
				cutoffString = sdf.format(paVO.getShipDateSA().getTime()) + paVO.getShipDateSACutoff();
				
				if (todayString.compareTo(cutoffString) <= 0)
				{
				shipDate = getFormattedDate(paVO.getShipDateSA().getTime());
				}
			}			
			if (paVO.getShipDateSU() != null && shipMethod.equalsIgnoreCase(GeneralConstants.DELIVERY_SUNDAY_CODE)) 
			{
				cutoffString = sdf.format(paVO.getShipDateSU().getTime()) + paVO.getShipDateSUCutoff();
				
				if (todayString.compareTo(cutoffString) <= 0)
				{
				shipDate = getFormattedDate(paVO.getShipDateSU().getTime());
				}
			}
			paVO.setShipDate(shipDate);
		}

		logger.info("shipDate: " + shipDate);
		return paVO;

	}

	/**This method return the 5 digit Zip Code
	 * @param zipCode
	 * @param country
	 * @return
	 */
	private static String getDomesticZipCode(String zipCode, String country) {
		if (country != null
				&& country.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA)
				&& zipCode.length() > 3) {
			zipCode = zipCode.substring(0, 3);
		} else if (country != null
				&& country.equals(GeneralConstants.COUNTRY_CODE_US)
				&& zipCode.length() > 5) {
			zipCode = zipCode.substring(0, 5);
		}
		return zipCode;
	}

	/**This method List of ProductAvailVO for given
	 * @param productId
	 * @param zipCode
	 * @param days
	 * @param countryId
	 * @return
	 * @throws Exception
	 */
	public List<ProductAvailVO> getProductAvailableDates(Connection con, String productId,
			String zipCode, int days, String countryId, List<String> addons,String sourceCode) throws Exception {

		logger.info("productID: " + productId + " zipCode: " + zipCode
				+ " days: " + days + " country : " + countryId);
		List<ProductAvailVO> ProductAvailVOs = new ArrayList<ProductAvailVO>();

		ProductAvailabilityBO paBO = new ProductAvailabilityBO();
		ProductAvailVO[] paVOs = null;
		
		// Check if CA is local.
		if (countryId == null || countryId.equals("") || countryId.equals("US")
				|| countryId.equals("CA")) {
			zipCode = getDomesticZipCode(zipCode, countryId);
			if (logger.isDebugEnabled())
				logger.debug("product availability");
			//paVOs = pas.getProductAvailableDates(productId, zipCode, days);
			
			paVOs = paBO.getProductAvailableDates(con, productId, zipCode,sourceCode, days, addons);

		} else {
			if (logger.isDebugEnabled())
				logger.debug("international product availability");
			paVOs = paBO.getInternationalProductAvailableDates(con, productId,
					countryId, days);
		}

		for (ProductAvailVO paVO : paVOs) {
			if (paVO.getIsAvailable()) {
				ProductAvailVOs.add(paVO);
			}

		}

		return ProductAvailVOs;
	}

	/** This method return list of shipping method from paVO
	 * @param paVO
	 * @return
	 */
	public static List<String> getShippingMethods(ProductAvailVO paVO) {

		List<String> shippingMethods = new ArrayList<String>();

		if (paVO == null) {
			return shippingMethods;
		}

		Calendar floristDate = paVO.getFloristCutoffDate();
		if (floristDate != null) {
			shippingMethods.add(GeneralConstants.DELIVERY_SAME_DAY_CODE);
		}

		Calendar shipDateND = paVO.getShipDateND();
		if (shipDateND != null) {
			shippingMethods.add(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
		}

		Calendar shipDate2D = paVO.getShipDate2D();
		if (shipDate2D != null) {
			shippingMethods.add(GeneralConstants.DELIVERY_TWO_DAY_CODE);
		}

		Calendar shipDateGR = paVO.getShipDateGR();
		if (shipDateGR != null) {
			shippingMethods.add(GeneralConstants.DELIVERY_STANDARD_CODE);
		}

		Calendar shipDateSA = paVO.getShipDateSA();
		if (shipDateSA != null) {
			shippingMethods.add(GeneralConstants.DELIVERY_SATURDAY_CODE);
		}
		return shippingMethods;
	}

	/** This Method return the cheapest shipping method based on input list of shipping method
	 * @param shippingMethods
	 *  Note: DELIVERY_SAME_DAY_CODE is added for SDFC Product.This method is replacement of getShipMethods() in Datamasager.java.
	 *   The sequence GR - 2D - SD - ND - SA is there in that method.
	 * @return
	 */
	public static String getCheapestShipMethod(List<String> shippingMethods) {
		String shipMethod = "";
		if (shippingMethods != null) {
			if (shippingMethods.contains(GeneralConstants.DELIVERY_STANDARD_CODE)) {
				shipMethod = GeneralConstants.DELIVERY_STANDARD_CODE;
			} else if (shippingMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE)) {
				shipMethod = GeneralConstants.DELIVERY_TWO_DAY_CODE;				
			} else if (shippingMethods.contains(GeneralConstants.DELIVERY_SAME_DAY_CODE)) {
				shipMethod = GeneralConstants.DELIVERY_SAME_DAY_CODE;
			} else if (shippingMethods.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE)) {
				shipMethod = GeneralConstants.DELIVERY_NEXT_DAY_CODE;
			} else if (shippingMethods.contains(GeneralConstants.DELIVERY_SATURDAY_CODE)) {
				shipMethod = GeneralConstants.DELIVERY_SATURDAY_CODE;
			}

		}

		return shipMethod;
	}

	/** This method return the first available ProductAvailVO for given 
	 * @param productID
	 * @param zipCode
	 * @param countryId
	 * @return
	 * @throws Exception
	 */
	public ProductAvailVO getPASFirstAvailableDate(Connection con, String productID,
			String zipCode, String countryId, List<String> addons) throws Exception {

		ProductAvailVO firstAvailableDate = null;
		List<ProductAvailVO> deliveryDates = getProductAvailableDates(
				con, productID, zipCode, DEFAULT_NO_DAYS, countryId, addons,null);
		if (deliveryDates != null && deliveryDates.size() > 0) {
			firstAvailableDate = deliveryDates.get(0);
		}

		return firstAvailableDate;
	}

	/**This method take the ProductAvailVO date object and get the delivery date
	 * and fetch the day of the week 
	 * @param date
	 * @return
	 */
	public static String getDayOfTheWeek(ProductAvailVO date) {
		Calendar c = date.getDeliveryDate();
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		return getDayOfWeek(dayOfWeek);
	}

	private static String getDayOfWeek(int day) {
		String dayOfWeek = null;

		switch (day) {
		case 1:
			dayOfWeek = "Sun";
			break;
		case 2:
			dayOfWeek = "Mon";
			break;
		case 3:
			dayOfWeek = "Tue";
			break;
		case 4:
			dayOfWeek = "Wed";
			break;
		case 5:
			dayOfWeek = "Thu";
			break;
		case 6:
			dayOfWeek = "Fri";
			break;
		case 7:
			dayOfWeek = "Sat";
			break;
		}

		return dayOfWeek;
	}

	/**This Method Return formated Date.
	 * @param date
	 * @return String date 
	 */
	public static String getFormattedDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		return sdf.format(date);
	}

    /**
     * Lazy initialization of the ConfigurationUtil object.
     * @return
     * @throws Exception
     */
    protected static ConfigurationUtil getConfigurationUtil() throws Exception
    {
        if (cu == null)
        {
            cu = ConfigurationUtil.getInstance();
        }
        return cu;
    }
    
	public ProductAvailVO getPASFirstAvailableWeekDayDate(Connection con,
			String productID, String zipCode, String countryId, List<String> addons,int maxDeliveryDates) throws Exception {
		  logger.info("PASUtil: getPASFirstAvailableWeekDayDate()");
		List<ProductAvailVO> availableProdDates = getProductAvailableDates(con, 
				productID, zipCode, maxDeliveryDates, countryId, addons,null);
		if(availableProdDates != null && availableProdDates.size() >0){
			for (ProductAvailVO productAvailVO : availableProdDates) {
				Calendar c = productAvailVO.getDeliveryDate();
				if (c != null && !(c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7)) {
					return productAvailVO;
				}
			}
		}
		return null;
	}
	
   public ProductAvailVO getPASNotSameDayFirstAvailableWeekDate(Connection con,
			String productID, String zipCode, String countryId, List<String> addons,int maxDeliveryDates,String sourceCode,Calendar deliveryDate) throws Exception {
	   logger.info("PASUtil: getPASNotSameDayFirstAvailableWeekDate()");
		List<ProductAvailVO> availableProdDates = getProductAvailableDates(con, 
				productID, zipCode, maxDeliveryDates, countryId, addons,sourceCode);
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		if(availableProdDates != null && availableProdDates.size() > 0){
			for (ProductAvailVO productAvailVO : availableProdDates) {
				Calendar c = productAvailVO.getDeliveryDate();
				if (c != null && !(c.get(Calendar.DAY_OF_WEEK) == 1 || c.get(Calendar.DAY_OF_WEEK) == 7)) {
					if(!sdf.format(c.getTime()).equals(sdf.format(new Date())) && !c.before(deliveryDate)) {
						return productAvailVO;
					}
				}
			}
		}
		return null;
	}
	
	public ProductAvailVO getPASNotSameDayFirstAvailableDate(Connection con,
			String productID, String zipCode, String countryId, List<String> addons,int maxDeliveryDates,String sourceCode,Calendar deliveryDate) throws Exception {
		logger.info("PASUtil: getPASNotSameDayFirstAvailableDate()");
		List<ProductAvailVO> availableProdDates = getProductAvailableDates(con, 
				productID, zipCode, maxDeliveryDates, countryId, addons,sourceCode);
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		if(availableProdDates != null && availableProdDates.size() >0){
			for (ProductAvailVO productAvailVO : availableProdDates) { 
				Calendar c = productAvailVO.getDeliveryDate(); 
				if(!sdf.format(c.getTime()).equals(sdf.format(new Date())) && !c.before(deliveryDate)) {
					return productAvailVO;
				}
			}
		} 
		return null;
	}
	
	public ProductAvailVO getPASNoSUNFirstAvailableDate(Connection con,
			String productID, String zipCode, String countryId, List<String> addons,int maxDeliveryDates,String sourceCode,Calendar deliveryDate) throws Exception {
		logger.info("PASUtil: getPASNoSUNFirstAvailableDate()");
		List<ProductAvailVO> availableProdDates = getProductAvailableDates(con, 
				productID, zipCode, maxDeliveryDates, countryId, addons,sourceCode);
		if(availableProdDates != null && availableProdDates.size() >0){
			for (ProductAvailVO productAvailVO : availableProdDates) {
				Calendar c = productAvailVO.getDeliveryDate();
				if (c != null && c.get(Calendar.DAY_OF_WEEK) != 1 && !c.before(deliveryDate)) {
					return productAvailVO;
				}
			}
		}
		return null;
	}
	
	public ProductAvailVO getPASNoSATFirstAvailableDate(Connection con,
			String productID, String zipCode, String countryId, List<String> addons,int maxDeliveryDates,String sourceCode,Calendar deliveryDate) throws Exception {
		logger.info("PASUtil: getPASNoSATFirstAvailableDate()");
		List<ProductAvailVO> availableProdDates = getProductAvailableDates(con, 
				productID, zipCode, maxDeliveryDates, countryId, addons,sourceCode);
		if(availableProdDates != null && availableProdDates.size() >0)	{
			for (ProductAvailVO productAvailVO : availableProdDates) {
				Calendar c = productAvailVO.getDeliveryDate();
				if (c != null && c.get(Calendar.DAY_OF_WEEK) != 7 && !c.before(deliveryDate)) {
					return productAvailVO;
				}
			}
		}
		return null;
	}
	/** The method checks for valid Ship methods available, checks for cutoff as well.
	 * @param paVO
	 * @return
	 */
	public static List<String> getValidShippingMethods(ProductAvailVO paVO) {

		List<String> shippingMethods = new ArrayList<String>();

		if (paVO == null) {
			return shippingMethods;
		}
		
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfDateTime = new SimpleDateFormat("yyyyMMddHHmm");
		Date today = new Date();
        String currTime = sdfDateTime.format(today);
        
        String cutoffTime = null;
        Date pasTempDate = null;
        String cutoffString = null;
        
        Calendar floristDate = paVO.getFloristCutoffDate();        
        if (floristDate != null) {
        	cutoffTime = paVO.getFloristCutoffTime();
        	pasTempDate = floristDate.getTime();
            cutoffString = sdfDate.format(pasTempDate) + cutoffTime;
            logger.debug("florist cutoff: " + cutoffString);
            if (currTime.compareTo(cutoffString) <= 0) {
            	shippingMethods.add(GeneralConstants.DELIVERY_SAME_DAY_CODE);
            }
        }

		Calendar shipDateND = paVO.getShipDateND();		
		if (shipDateND != null) {
			cutoffTime = paVO.getShipDateNDCutoff();
			pasTempDate = shipDateND.getTime();
            cutoffString = sdfDate.format(pasTempDate) + cutoffTime;
            logger.debug("NextDay cutoff: " + cutoffString);
            if (currTime.compareTo(cutoffString) <= 0) {
            	shippingMethods.add(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            }			
		}

		Calendar shipDate2D = paVO.getShipDate2D();		
		if (shipDate2D != null) {
			cutoffTime = paVO.getShipDate2DCutoff();
			pasTempDate = shipDate2D.getTime();
            cutoffString = sdfDate.format(pasTempDate) + cutoffTime;
            logger.debug("TwoDay cutoff: " + cutoffString);
            if (currTime.compareTo(cutoffString) <= 0) {
            	shippingMethods.add(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            }
		}

		Calendar shipDateGR = paVO.getShipDateGR();
		if (shipDateGR != null) {
			cutoffTime = paVO.getShipDateGRCutoff();
			pasTempDate = shipDateGR.getTime();
            cutoffString = sdfDate.format(pasTempDate) + cutoffTime;
            logger.debug("Standard cutoff: " + cutoffString);
            if (currTime.compareTo(cutoffString) <= 0) {
            	shippingMethods.add(GeneralConstants.DELIVERY_STANDARD_CODE);
            }
		}

		Calendar shipDateSA = paVO.getShipDateSA();
		if (shipDateSA != null) {
			cutoffTime = paVO.getShipDateSACutoff();
			pasTempDate = shipDateSA.getTime();
            cutoffString = sdfDate.format(pasTempDate) + cutoffTime;
            logger.debug("Saturday cutoff: " + cutoffString);
            if (currTime.compareTo(cutoffString) <= 0) {
            	shippingMethods.add(GeneralConstants.DELIVERY_SATURDAY_CODE);
            }
		}
		return shippingMethods;
	}	
	
}
