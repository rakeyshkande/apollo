/**
 * PASRequestServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.pas.webservices;

public interface PASRequestServiceService extends javax.xml.rpc.Service {
    public java.lang.String getProductAvailabilityAddress();

    public com.ftd.pas.webservices.PASRequestService getProductAvailability() throws javax.xml.rpc.ServiceException;

    public com.ftd.pas.webservices.PASRequestService getProductAvailability(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
