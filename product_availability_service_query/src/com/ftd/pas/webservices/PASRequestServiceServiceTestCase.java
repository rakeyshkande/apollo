/**
 * PASRequestServiceServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.pas.webservices;

public class PASRequestServiceServiceTestCase extends junit.framework.TestCase {
    public PASRequestServiceServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testProductAvailabilityWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailabilityAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1ProductAvailabilityGetNextAvailableDeliveryDate() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.util.Calendar value = null;
        value = binding.getNextAvailableDeliveryDate(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test2ProductAvailabilityIsProductAvailable() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.Boolean value = null;
            value = binding.isProductAvailable(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test3ProductAvailabilityGetProductAvailability() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.ftd.pas.common.vo.ProductAvailVO value = null;
            value = binding.getProductAvailability(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test4ProductAvailabilityIsAnyProductAvailable() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.Boolean value = null;
            value = binding.isAnyProductAvailable(java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test5ProductAvailabilityGetMostPopularProducts() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.ftd.pas.common.vo.ProductAvailVO[] value = null;
            value = binding.getMostPopularProducts(java.util.Calendar.getInstance(), new java.lang.String(), 0, new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test6ProductAvailabilityGetFlorists() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String[] value = null;
            value = binding.getFlorists(java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test7ProductAvailabilityIsInternationalProductAvailable() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.Boolean value = null;
            value = binding.isInternationalProductAvailable(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String());
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test8ProductAvailabilityGetAvailableShipDates() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.ftd.pas.common.vo.PASShipDateAvailVO value = null;
        value = binding.getAvailableShipDates(new java.lang.String(), 0, new java.lang.String());
        // TBD - validate results
    }

    public void test9ProductAvailabilityGetProductAvailableDates() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.ftd.pas.common.vo.ProductAvailVO[] value = null;
            value = binding.getProductAvailableDates(new java.lang.String(), new java.lang.String(), 0);
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test10ProductAvailabilityGetInternationalProductAvailableDates() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.ftd.pas.common.vo.ProductAvailVO[] value = null;
            value = binding.getInternationalProductAvailableDates(new java.lang.String(), new java.lang.String(), 0);
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test11ProductAvailabilityIsProductListAvailable() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String[] value = null;
            value = binding.isProductListAvailable(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String[0]);
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test12ProductAvailabilityIsInternationalProductListAvailable() throws Exception {
        com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub binding;
        try {
            binding = (com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub)
                          new com.ftd.pas.webservices.PASRequestServiceServiceLocator().getProductAvailability();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String[] value = null;
            value = binding.isInternationalProductListAvailable(new java.lang.String(), java.util.Calendar.getInstance(), new java.lang.String[0]);
        }
        catch (com.ftd.pas.exception.PASServiceException e1) {
            throw new junit.framework.AssertionFailedError("PASServiceException Exception caught: " + e1);
        }
            // TBD - validate results
    }

}
