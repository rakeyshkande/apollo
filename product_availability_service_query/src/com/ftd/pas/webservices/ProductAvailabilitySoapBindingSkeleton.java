 /**
  * ProductAvailabilitySoapBindingSkeleton.java
  *
  * This file was auto-generated from WSDL
  * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
  */

 package com.ftd.pas.webservices;

 public class ProductAvailabilitySoapBindingSkeleton implements com.ftd.pas.webservices.PASRequestService, org.apache.axis.wsdl.Skeleton {
     private com.ftd.pas.webservices.PASRequestService impl;
     private static java.util.Map _myOperations = new java.util.Hashtable();
     private static java.util.Collection _myOperationsList = new java.util.ArrayList();

     /**
     * Returns List of OperationDesc objects with this name
     */
     public static java.util.List getOperationDescByName(java.lang.String methodName) {
         return (java.util.List)_myOperations.get(methodName);
     }

     /**
     * Returns Collection of OperationDescs
     */
     public static java.util.Collection getOperationDescs() {
         return _myOperationsList;
     }

     static {
         org.apache.axis.description.OperationDesc _oper;
         org.apache.axis.description.FaultDesc _fault;
         org.apache.axis.description.ParameterDesc [] _params;
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getNextAvailableDeliveryDate", _params, new javax.xml.namespace.QName("", "getNextAvailableDeliveryDateReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getNextAvailableDeliveryDate"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getNextAvailableDeliveryDate") == null) {
             _myOperations.put("getNextAvailableDeliveryDate", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getNextAvailableDeliveryDate")).add(_oper);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("isProductAvailable", _params, new javax.xml.namespace.QName("", "isProductAvailableReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "isProductAvailable"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("isProductAvailable") == null) {
             _myOperations.put("isProductAvailable", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("isProductAvailable")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getProductAvailability", _params, new javax.xml.namespace.QName("", "getProductAvailabilityReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "ProductAvailVO"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getProductAvailability"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getProductAvailability") == null) {
             _myOperations.put("getProductAvailability", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getProductAvailability")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("isAnyProductAvailable", _params, new javax.xml.namespace.QName("", "isAnyProductAvailableReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "isAnyProductAvailable"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("isAnyProductAvailable") == null) {
             _myOperations.put("isAnyProductAvailable", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("isAnyProductAvailable")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in3"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getMostPopularProducts", _params, new javax.xml.namespace.QName("", "getMostPopularProductsReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getMostPopularProducts"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getMostPopularProducts") == null) {
             _myOperations.put("getMostPopularProducts", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getMostPopularProducts")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getProductAvailableDates", _params, new javax.xml.namespace.QName("", "getProductAvailableDatesReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getProductAvailableDates"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getProductAvailableDates") == null) {
             _myOperations.put("getProductAvailableDates", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getProductAvailableDates")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getInternationalProductAvailableDates", _params, new javax.xml.namespace.QName("", "getInternationalProductAvailableDatesReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getInternationalProductAvailableDates"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getInternationalProductAvailableDates") == null) {
             _myOperations.put("getInternationalProductAvailableDates", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getInternationalProductAvailableDates")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zipCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "deliveryDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"), java.lang.String[].class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("isProductListAvailable", _params, new javax.xml.namespace.QName("", "isProductListAvailableReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "isProductListAvailable"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("isProductListAvailable") == null) {
             _myOperations.put("isProductListAvailable", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("isProductListAvailable")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "countryCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "deliveryDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"), java.lang.String[].class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("isInternationalProductListAvailable", _params, new javax.xml.namespace.QName("", "isInternationalProductListAvailableReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "isInternationalProductListAvailable"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("isInternationalProductListAvailable") == null) {
             _myOperations.put("isInternationalProductListAvailable", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("isInternationalProductListAvailable")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getFlorists", _params, new javax.xml.namespace.QName("", "getFloristsReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getFlorists"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getFlorists") == null) {
             _myOperations.put("getFlorists", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getFlorists")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("isInternationalProductAvailable", _params, new javax.xml.namespace.QName("", "isInternationalProductAvailableReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "isInternationalProductAvailable"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("isInternationalProductAvailable") == null) {
             _myOperations.put("isInternationalProductAvailable", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("isInternationalProductAvailable")).add(_oper);
         _fault = new org.apache.axis.description.FaultDesc();
         _fault.setName("PASServiceException");
         _fault.setQName(new javax.xml.namespace.QName("urn:productAvailability", "fault"));
         _fault.setClassName("com.ftd.pas.exception.PASServiceException");
         _fault.setXmlType(new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"));
         _oper.addFault(_fault);
         _params = new org.apache.axis.description.ParameterDesc [] {
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "numberOfDays"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
             new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zipCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false), 
         };
         _oper = new org.apache.axis.description.OperationDesc("getAvailableShipDates", _params, new javax.xml.namespace.QName("", "getAvailableShipDatesReturn"));
         _oper.setReturnType(new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "PASShipDateAvailVO"));
         _oper.setElementQName(new javax.xml.namespace.QName("urn:productAvailability", "getAvailableShipDates"));
         _oper.setSoapAction("");
         _myOperationsList.add(_oper);
         if (_myOperations.get("getAvailableShipDates") == null) {
             _myOperations.put("getAvailableShipDates", new java.util.ArrayList());
         }
         ((java.util.List)_myOperations.get("getAvailableShipDates")).add(_oper);
     }

     public ProductAvailabilitySoapBindingSkeleton() {
         this.impl = new com.ftd.pas.server.service.impl.PASRequestServiceImpl();
     }

     public ProductAvailabilitySoapBindingSkeleton(com.ftd.pas.webservices.PASRequestService impl) {
         this.impl = impl;
     }
     public java.util.Calendar getNextAvailableDeliveryDate(java.lang.String productId, java.lang.String zip) throws java.rmi.RemoteException
     {
         java.util.Calendar ret = impl.getNextAvailableDeliveryDate(productId, zip);
         return ret;
     }

     public java.lang.Boolean isProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.Boolean ret = impl.isProductAvailable(in0, in1, in2);
         return ret;
     }

     public com.ftd.pas.common.vo.ProductAvailVO getProductAvailability(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         com.ftd.pas.common.vo.ProductAvailVO ret = impl.getProductAvailability(in0, in1, in2);
         return ret;
     }

     public java.lang.Boolean isAnyProductAvailable(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.Boolean ret = impl.isAnyProductAvailable(in0, in1);
         return ret;
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getMostPopularProducts(java.util.Calendar in0, java.lang.String in1, int in2, java.lang.String in3) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         com.ftd.pas.common.vo.ProductAvailVO[] ret = impl.getMostPopularProducts(in0, in1, in2, in3);
         return ret;
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         com.ftd.pas.common.vo.ProductAvailVO[] ret = impl.getProductAvailableDates(in0, in1, in2);
         return ret;
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         com.ftd.pas.common.vo.ProductAvailVO[] ret = impl.getInternationalProductAvailableDates(in0, in1, in2);
         return ret;
     }

     public java.lang.String[] isProductListAvailable(java.lang.String zipCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.String[] ret = impl.isProductListAvailable(zipCode, deliveryDate, productList);
         return ret;
     }

     public java.lang.String[] isInternationalProductListAvailable(java.lang.String countryCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.String[] ret = impl.isInternationalProductListAvailable(countryCode, deliveryDate, productList);
         return ret;
     }

     public java.lang.String[] getFlorists(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.String[] ret = impl.getFlorists(in0, in1);
         return ret;
     }

     public java.lang.Boolean isInternationalProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException
     {
         java.lang.Boolean ret = impl.isInternationalProductAvailable(in0, in1, in2);
         return ret;
     }

     public com.ftd.pas.common.vo.PASShipDateAvailVO getAvailableShipDates(java.lang.String productId, int numberOfDays, java.lang.String zipCode) throws java.rmi.RemoteException
     {
         com.ftd.pas.common.vo.PASShipDateAvailVO ret = impl.getAvailableShipDates(productId, numberOfDays, zipCode);
         return ret;
     }

 }
