/**
 * PASRequestServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.pas.webservices;

public class PASRequestServiceServiceLocator extends org.apache.axis.client.Service implements com.ftd.pas.webservices.PASRequestServiceService {

    public PASRequestServiceServiceLocator() {
    }


    public PASRequestServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PASRequestServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProductAvailability
    private java.lang.String ProductAvailability_address = "http://zinc3:8080/product-availability/ProductAvailability";

    public java.lang.String getProductAvailabilityAddress() {
        return ProductAvailability_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProductAvailabilityWSDDServiceName = "ProductAvailability";

    public java.lang.String getProductAvailabilityWSDDServiceName() {
        return ProductAvailabilityWSDDServiceName;
    }

    public void setProductAvailabilityWSDDServiceName(java.lang.String name) {
        ProductAvailabilityWSDDServiceName = name;
    }

    public com.ftd.pas.webservices.PASRequestService getProductAvailability() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProductAvailability_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProductAvailability(endpoint);
    }

    public com.ftd.pas.webservices.PASRequestService getProductAvailability(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub _stub = new com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub(portAddress, this);
            _stub.setPortName(getProductAvailabilityWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProductAvailabilityEndpointAddress(java.lang.String address) {
        ProductAvailability_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.ftd.pas.webservices.PASRequestService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub _stub = new com.ftd.pas.webservices.ProductAvailabilitySoapBindingStub(new java.net.URL(ProductAvailability_address), this);
                _stub.setPortName(getProductAvailabilityWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProductAvailability".equals(inputPortName)) {
            return getProductAvailability();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:productAvailability", "PASRequestServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:productAvailability", "ProductAvailability"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProductAvailability".equals(portName)) {
            setProductAvailabilityEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
