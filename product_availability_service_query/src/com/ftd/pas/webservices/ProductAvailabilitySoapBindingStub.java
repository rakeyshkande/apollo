 /**
  * ProductAvailabilitySoapBindingStub.java
  *
  * This file was auto-generated from WSDL
  * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
  */

 package com.ftd.pas.webservices;

 public class ProductAvailabilitySoapBindingStub extends org.apache.axis.client.Stub implements com.ftd.pas.webservices.PASRequestService {
     private java.util.Vector cachedSerClasses = new java.util.Vector();
     private java.util.Vector cachedSerQNames = new java.util.Vector();
     private java.util.Vector cachedSerFactories = new java.util.Vector();
     private java.util.Vector cachedDeserFactories = new java.util.Vector();

     static org.apache.axis.description.OperationDesc [] _operations;

     static {
         _operations = new org.apache.axis.description.OperationDesc[12];
         _initOperationDesc1();
         _initOperationDesc2();
     }

     private static void _initOperationDesc1(){
         org.apache.axis.description.OperationDesc oper;
         org.apache.axis.description.ParameterDesc param;
         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getNextAvailableDeliveryDate");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zip"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
         oper.setReturnClass(java.util.Calendar.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getNextAvailableDeliveryDateReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         _operations[0] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("isProductAvailable");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         oper.setReturnClass(java.lang.Boolean.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "isProductAvailableReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[1] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getProductAvailability");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "ProductAvailVO"));
         oper.setReturnClass(com.ftd.pas.common.vo.ProductAvailVO.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getProductAvailabilityReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[2] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("isAnyProductAvailable");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         oper.setReturnClass(java.lang.Boolean.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "isAnyProductAvailableReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[3] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getMostPopularProducts");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in3"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         oper.setReturnClass(com.ftd.pas.common.vo.ProductAvailVO[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getMostPopularProductsReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[4] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getProductAvailableDates");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         oper.setReturnClass(com.ftd.pas.common.vo.ProductAvailVO[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getProductAvailableDatesReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[5] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getInternationalProductAvailableDates");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO"));
         oper.setReturnClass(com.ftd.pas.common.vo.ProductAvailVO[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getInternationalProductAvailableDatesReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[6] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("isProductListAvailable");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zipCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "deliveryDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"), java.lang.String[].class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         oper.setReturnClass(java.lang.String[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "isProductListAvailableReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[7] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("isInternationalProductListAvailable");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "countryCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "deliveryDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productList"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"), java.lang.String[].class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         oper.setReturnClass(java.lang.String[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "isInternationalProductListAvailableReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[8] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getFlorists");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string"));
         oper.setReturnClass(java.lang.String[].class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getFloristsReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[9] = oper;

     }

     private static void _initOperationDesc2(){
         org.apache.axis.description.OperationDesc oper;
         org.apache.axis.description.ParameterDesc param;
         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("isInternationalProductAvailable");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in0"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "in2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
         oper.setReturnClass(java.lang.Boolean.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "isInternationalProductAvailableReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         oper.addFault(new org.apache.axis.description.FaultDesc(
                       new javax.xml.namespace.QName("urn:productAvailability", "fault"),
                       "com.ftd.pas.exception.PASServiceException",
                       new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException"), 
                       true
                      ));
         _operations[10] = oper;

         oper = new org.apache.axis.description.OperationDesc();
         oper.setName("getAvailableShipDates");
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "numberOfDays"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
         oper.addParameter(param);
         param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "zipCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"), java.lang.String.class, false, false);
         oper.addParameter(param);
         oper.setReturnType(new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "PASShipDateAvailVO"));
         oper.setReturnClass(com.ftd.pas.common.vo.PASShipDateAvailVO.class);
         oper.setReturnQName(new javax.xml.namespace.QName("", "getAvailableShipDatesReturn"));
         oper.setStyle(org.apache.axis.constants.Style.RPC);
         oper.setUse(org.apache.axis.constants.Use.ENCODED);
         _operations[11] = oper;

     }

     public ProductAvailabilitySoapBindingStub() throws org.apache.axis.AxisFault {
          this(null);
     }

     public ProductAvailabilitySoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
          this(service);
          super.cachedEndpoint = endpointURL;
     }

     public ProductAvailabilitySoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         if (service == null) {
             super.service = new org.apache.axis.client.Service();
         } else {
             super.service = service;
         }
         ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
             java.lang.Class cls;
             javax.xml.namespace.QName qName;
             javax.xml.namespace.QName qName2;
             java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
             java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
             java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
             java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
             java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
             java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
             java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
             java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
             java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
             java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
             qName = new javax.xml.namespace.QName("http://exception.pas.ftd.com", "PASServiceException");
             cachedSerQNames.add(qName);
             cls = com.ftd.pas.exception.PASServiceException.class;
             cachedSerClasses.add(cls);
             cachedSerFactories.add(beansf);
             cachedDeserFactories.add(beandf);

             qName = new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "PASShipDateAvailVO");
             cachedSerQNames.add(qName);
             cls = com.ftd.pas.common.vo.PASShipDateAvailVO.class;
             cachedSerClasses.add(cls);
             cachedSerFactories.add(beansf);
             cachedDeserFactories.add(beandf);

             qName = new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "ProductAvailVO");
             cachedSerQNames.add(qName);
             cls = com.ftd.pas.common.vo.ProductAvailVO.class;
             cachedSerClasses.add(cls);
             cachedSerFactories.add(beansf);
             cachedDeserFactories.add(beandf);

             qName = new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_soapenc_string");
             cachedSerQNames.add(qName);
             cls = java.lang.String[].class;
             cachedSerClasses.add(cls);
             qName = new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string");
             qName2 = null;
             cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
             cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

             qName = new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_tns3_ProductAvailVO");
             cachedSerQNames.add(qName);
             cls = com.ftd.pas.common.vo.ProductAvailVO[].class;
             cachedSerClasses.add(cls);
             qName = new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "ProductAvailVO");
             qName2 = null;
             cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
             cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

             qName = new javax.xml.namespace.QName("urn:productAvailability", "ArrayOf_xsd_dateTime");
             cachedSerQNames.add(qName);
             cls = java.util.Calendar[].class;
             cachedSerClasses.add(cls);
             qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime");
             qName2 = null;
             cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
             cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

     }

     protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
         try {
             org.apache.axis.client.Call _call = super._createCall();
             if (super.maintainSessionSet) {
                 _call.setMaintainSession(super.maintainSession);
             }
             if (super.cachedUsername != null) {
                 _call.setUsername(super.cachedUsername);
             }
             if (super.cachedPassword != null) {
                 _call.setPassword(super.cachedPassword);
             }
             if (super.cachedEndpoint != null) {
                 _call.setTargetEndpointAddress(super.cachedEndpoint);
             }
             if (super.cachedTimeout != null) {
                 _call.setTimeout(super.cachedTimeout);
             }
             if (super.cachedPortName != null) {
                 _call.setPortName(super.cachedPortName);
             }
             java.util.Enumeration keys = super.cachedProperties.keys();
             while (keys.hasMoreElements()) {
                 java.lang.String key = (java.lang.String) keys.nextElement();
                 _call.setProperty(key, super.cachedProperties.get(key));
             }
             // All the type mapping information is registered
             // when the first call is made.
             // The type mapping information is actually registered in
             // the TypeMappingRegistry of the service, which
             // is the reason why registration is only needed for the first call.
             synchronized (this) {
                 if (firstCall()) {
                     // must set encoding style before registering serializers
                     _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
                     _call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
                     for (int i = 0; i < cachedSerFactories.size(); ++i) {
                         java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                         javax.xml.namespace.QName qName =
                                 (javax.xml.namespace.QName) cachedSerQNames.get(i);
                         java.lang.Object x = cachedSerFactories.get(i);
                         if (x instanceof Class) {
                             java.lang.Class sf = (java.lang.Class)
                                  cachedSerFactories.get(i);
                             java.lang.Class df = (java.lang.Class)
                                  cachedDeserFactories.get(i);
                             _call.registerTypeMapping(cls, qName, sf, df, false);
                         }
                         else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                             org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                  cachedSerFactories.get(i);
                             org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                  cachedDeserFactories.get(i);
                             _call.registerTypeMapping(cls, qName, sf, df, false);
                         }
                     }
                 }
             }
             return _call;
         }
         catch (java.lang.Throwable _t) {
             throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
         }
     }

     public java.util.Calendar getNextAvailableDeliveryDate(java.lang.String productId, java.lang.String zip) throws java.rmi.RemoteException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[0]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getNextAvailableDeliveryDate"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {productId, zip});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.util.Calendar) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.util.Calendar) org.apache.axis.utils.JavaUtils.convert(_resp, java.util.Calendar.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
   throw axisFaultException;
 }
     }

     public java.lang.Boolean isProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[1]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "isProductAvailable"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, in2});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.Boolean) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Boolean.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public com.ftd.pas.common.vo.ProductAvailVO getProductAvailability(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[2]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getProductAvailability"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, in2});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (com.ftd.pas.common.vo.ProductAvailVO) _resp;
             } catch (java.lang.Exception _exception) {
                 return (com.ftd.pas.common.vo.ProductAvailVO) org.apache.axis.utils.JavaUtils.convert(_resp, com.ftd.pas.common.vo.ProductAvailVO.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public java.lang.Boolean isAnyProductAvailable(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[3]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "isAnyProductAvailable"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.Boolean) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Boolean.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getMostPopularProducts(java.util.Calendar in0, java.lang.String in1, int in2, java.lang.String in3) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[4]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getMostPopularProducts"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, new java.lang.Integer(in2), in3});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.ftd.pas.common.vo.ProductAvailVO[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[5]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getProductAvailableDates"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, new java.lang.Integer(in2)});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.ftd.pas.common.vo.ProductAvailVO[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[6]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getInternationalProductAvailableDates"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, new java.lang.Integer(in2)});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (com.ftd.pas.common.vo.ProductAvailVO[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.ftd.pas.common.vo.ProductAvailVO[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public java.lang.String[] isProductListAvailable(java.lang.String zipCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[7]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "isProductListAvailable"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {zipCode, deliveryDate, productList});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.String[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public java.lang.String[] isInternationalProductListAvailable(java.lang.String countryCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[8]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "isInternationalProductListAvailable"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {countryCode, deliveryDate, productList});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.String[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public java.lang.String[] getFlorists(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[9]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getFlorists"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.String[]) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public java.lang.Boolean isInternationalProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[10]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "isInternationalProductAvailable"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {in0, in1, in2});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (java.lang.Boolean) _resp;
             } catch (java.lang.Exception _exception) {
                 return (java.lang.Boolean) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Boolean.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
     if (axisFaultException.detail != null) {
         if (axisFaultException.detail instanceof java.rmi.RemoteException) {
               throw (java.rmi.RemoteException) axisFaultException.detail;
          }
         if (axisFaultException.detail instanceof com.ftd.pas.exception.PASServiceException) {
               throw (com.ftd.pas.exception.PASServiceException) axisFaultException.detail;
          }
    }
   throw axisFaultException;
 }
     }

     public com.ftd.pas.common.vo.PASShipDateAvailVO getAvailableShipDates(java.lang.String productId, int numberOfDays, java.lang.String zipCode) throws java.rmi.RemoteException {
         if (super.cachedEndpoint == null) {
             throw new org.apache.axis.NoEndPointException();
         }
         org.apache.axis.client.Call _call = createCall();
         _call.setOperation(_operations[11]);
         _call.setUseSOAPAction(true);
         _call.setSOAPActionURI("");
         _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
         _call.setOperationName(new javax.xml.namespace.QName("urn:productAvailability", "getAvailableShipDates"));

         setRequestHeaders(_call);
         setAttachments(_call);
  try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {productId, new java.lang.Integer(numberOfDays), zipCode});

         if (_resp instanceof java.rmi.RemoteException) {
             throw (java.rmi.RemoteException)_resp;
         }
         else {
             extractAttachments(_call);
             try {
                 return (com.ftd.pas.common.vo.PASShipDateAvailVO) _resp;
             } catch (java.lang.Exception _exception) {
                 return (com.ftd.pas.common.vo.PASShipDateAvailVO) org.apache.axis.utils.JavaUtils.convert(_resp, com.ftd.pas.common.vo.PASShipDateAvailVO.class);
             }
         }
   } catch (org.apache.axis.AxisFault axisFaultException) {
   throw axisFaultException;
 }
     }

 }
