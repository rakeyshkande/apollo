/**
 * PASRequestService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.pas.webservices;

public interface PASRequestService extends java.rmi.Remote {
    public java.util.Calendar getNextAvailableDeliveryDate(java.lang.String productId, java.lang.String zip) throws java.rmi.RemoteException;
    public java.lang.Boolean isProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public com.ftd.pas.common.vo.ProductAvailVO getProductAvailability(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public java.lang.Boolean isAnyProductAvailable(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public com.ftd.pas.common.vo.ProductAvailVO[] getMostPopularProducts(java.util.Calendar in0, java.lang.String in1, int in2, java.lang.String in3) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public java.lang.String[] getFlorists(java.util.Calendar in0, java.lang.String in1) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public java.lang.Boolean isInternationalProductAvailable(java.lang.String in0, java.util.Calendar in1, java.lang.String in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public com.ftd.pas.common.vo.PASShipDateAvailVO getAvailableShipDates(java.lang.String productId, int numberOfDays, java.lang.String zipCode) throws java.rmi.RemoteException;
    public com.ftd.pas.common.vo.ProductAvailVO[] getProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public com.ftd.pas.common.vo.ProductAvailVO[] getInternationalProductAvailableDates(java.lang.String in0, java.lang.String in1, int in2) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public java.lang.String[] isProductListAvailable(java.lang.String zipCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
    public java.lang.String[] isInternationalProductListAvailable(java.lang.String countryCode, java.util.Calendar deliveryDate, java.lang.String[] productList) throws java.rmi.RemoteException, com.ftd.pas.exception.PASServiceException;
}
