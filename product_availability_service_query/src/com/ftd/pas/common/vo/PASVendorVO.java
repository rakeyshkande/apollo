package com.ftd.pas.common.vo;

import java.util.Date;

public class PASVendorVO
{
    private String vendorId;
    private Date   deliveryDate;
    private String cutoffTime;
    private String shipAllowed;
    private String deliveryAvailable;
    
    public PASVendorVO()
    {
    }

    public void setVendorId(String param)
    {
        this.vendorId = param;
    }

    public String getVendorId()
    {
        return vendorId;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setShipAllowed(String param)
    {
        this.shipAllowed = param;
    }

    public String getShipAllowed()
    {
        return shipAllowed;
    }

    public void setDeliveryAvailable(String param)
    {
        this.deliveryAvailable = param;
    }

    public String getDeliveryAvailable()
    {
        return deliveryAvailable;
    }
}
