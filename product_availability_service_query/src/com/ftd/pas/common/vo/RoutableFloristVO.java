package com.ftd.pas.common.vo;

import java.util.Date;

public class RoutableFloristVO
{
    private String floristId;
    private int    score;
    private int    weight;
    private String floristName;
    private String phoneNumber;
    private String mercuryFlag;
    private String superFloristFlag;
    private String sundayDeliveryFlag;
    private String cutoffTime;
    private String status;
    private String productCodificationId;
    private String floristBlocked;
    private String codificationId;
    private String sequences;
    private String zipCityFlag;
    private String floristSuspended;
    private String minOrderAmt;

    public RoutableFloristVO()
    {
    }

    public String getFloristId() {
        return floristId;
    }

    public void setFloristId(String floristId) {
        this.floristId = floristId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getFloristName() {
        return floristName;
    }

    public void setFloristName(String floristName) {
        this.floristName = floristName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMercuryFlag() {
        return mercuryFlag;
    }

    public void setMercuryFlag(String mercuryFlag) {
        this.mercuryFlag = mercuryFlag;
    }

    public String getSuperFloristFlag() {
        return superFloristFlag;
    }

    public void setSuperFloristFlag(String superFloristFlag) {
        this.superFloristFlag = superFloristFlag;
    }

    public String getSundayDeliveryFlag() {
        return sundayDeliveryFlag;
    }

    public void setSundayDeliveryFlag(String sundayDeliveryFlag) {
        this.sundayDeliveryFlag = sundayDeliveryFlag;
    }

    public String getCutoffTime() {
        return cutoffTime;
    }

    public void setCutoffTime(String cutoffTime) {
        this.cutoffTime = cutoffTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductCodificationId() {
        return productCodificationId;
    }

    public void setProductCodificationId(String productCodificationId) {
        this.productCodificationId = productCodificationId;
    }

    public String getFloristBlocked() {
        return floristBlocked;
    }

    public void setFloristBlocked(String floristBlocked) {
        this.floristBlocked = floristBlocked;
    }

    public String getCodificationId() {
        return codificationId;
    }

    public void setCodificationId(String codificationId) {
        this.codificationId = codificationId;
    }

    public String getSequences() {
        return sequences;
    }

    public void setSequences(String sequences) {
        this.sequences = sequences;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

	public String getZipCityFlag() {
		return zipCityFlag;
	}

	public void setZipCityFlag(String zipCityFlag) {
		this.zipCityFlag = zipCityFlag;
	}

	public String getFloristSuspended() {
		return floristSuspended;
	}

	public void setFloristSuspended(String floristSuspended) {
		this.floristSuspended = floristSuspended;
	}

	public String getMinOrderAmt() {
		return minOrderAmt;
	}

	public void setMinOrderAmt(String minOrderAmt) {
		this.minOrderAmt = minOrderAmt;
	}
}