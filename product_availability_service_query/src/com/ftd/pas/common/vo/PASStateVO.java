package com.ftd.pas.common.vo;

import java.util.Date;

public class PASStateVO
{
    private String stateCode;
    private Date   deliveryDate;
    private String cutoffTime;

    public PASStateVO()
    {
    }

    public void setStateCode(String param)
    {
        this.stateCode = param;
    }

    public String getStateCode()
    {
        return stateCode;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }
}
