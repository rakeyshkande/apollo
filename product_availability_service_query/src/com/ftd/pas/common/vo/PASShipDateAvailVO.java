/**
 * PASShipDateAvailVO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ftd.pas.common.vo;

import java.math.BigDecimal;

public class PASShipDateAvailVO  implements java.io.Serializable {
    private java.util.Calendar[] nextDayDeliveryDates;

    private java.util.Calendar[] twoDayDeliveryDates;

    private java.util.Calendar[] groundDeliveryDates;

    private java.util.Calendar[] saturdayDeliveryDates;
    
    private java.util.Calendar[] sundayDeliveryDates;

    private BigDecimal[] nextDayCharges;
    private BigDecimal[] twoDayCharges;
    private BigDecimal[] groundCharges;
    private BigDecimal[] saturdayCharges;
    private BigDecimal[] sundayCharges;
    private BigDecimal[] sundayUpCharges;
    
    public PASShipDateAvailVO() {
    }

    public PASShipDateAvailVO(
           java.util.Calendar[] nextDayDeliveryDates,
           java.util.Calendar[] twoDayDeliveryDates,
           java.util.Calendar[] groundDeliveryDates,
           java.util.Calendar[] saturdayDeliveryDates,
           java.util.Calendar[] sundayDeliveryDates) {
           this.nextDayDeliveryDates = nextDayDeliveryDates;
           this.twoDayDeliveryDates = twoDayDeliveryDates;
           this.groundDeliveryDates = groundDeliveryDates;
           this.saturdayDeliveryDates = saturdayDeliveryDates;
           this.sundayDeliveryDates = sundayDeliveryDates;
    }


    
    /**
     * Gets the sundayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @return sundayDeliveryDates
     */
    public java.util.Calendar[] getSundayDeliveryDates() {
		return sundayDeliveryDates;
	}
    
    
    /**
     * Sets the sundayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @param sundayDeliveryDates
     */
	public void setSundayDeliveryDates(java.util.Calendar[] sundayDeliveryDates) {
		this.sundayDeliveryDates = sundayDeliveryDates;
	}

	
    /**
     * Gets the nextDayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @return nextDayDeliveryDates
     */
    public java.util.Calendar[] getNextDayDeliveryDates() {
        return nextDayDeliveryDates;
    }


    /**
     * Sets the nextDayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @param nextDayDeliveryDates
     */
    public void setNextDayDeliveryDates(java.util.Calendar[] nextDayDeliveryDates) {
        this.nextDayDeliveryDates = nextDayDeliveryDates;
    }


    /**
     * Gets the twoDayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @return twoDayDeliveryDates
     */
    public java.util.Calendar[] getTwoDayDeliveryDates() {
        return twoDayDeliveryDates;
    }


    /**
     * Sets the twoDayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @param twoDayDeliveryDates
     */
    public void setTwoDayDeliveryDates(java.util.Calendar[] twoDayDeliveryDates) {
        this.twoDayDeliveryDates = twoDayDeliveryDates;
    }


    /**
     * Gets the groundDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @return groundDeliveryDates
     */
    public java.util.Calendar[] getGroundDeliveryDates() {
        return groundDeliveryDates;
    }


    /**
     * Sets the groundDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @param groundDeliveryDates
     */
    public void setGroundDeliveryDates(java.util.Calendar[] groundDeliveryDates) {
        this.groundDeliveryDates = groundDeliveryDates;
    }


    /**
     * Gets the saturdayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @return saturdayDeliveryDates
     */
    public java.util.Calendar[] getSaturdayDeliveryDates() {
        return saturdayDeliveryDates;
    }


    /**
     * Sets the saturdayDeliveryDates value for this PASShipDateAvailVO.
     * 
     * @param saturdayDeliveryDates
     */
    public void setSaturdayDeliveryDates(java.util.Calendar[] saturdayDeliveryDates) {
        this.saturdayDeliveryDates = saturdayDeliveryDates;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PASShipDateAvailVO)) return false;
        PASShipDateAvailVO other = (PASShipDateAvailVO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nextDayDeliveryDates==null && other.getNextDayDeliveryDates()==null) || 
             (this.nextDayDeliveryDates!=null &&
              java.util.Arrays.equals(this.nextDayDeliveryDates, other.getNextDayDeliveryDates()))) &&
            ((this.sundayDeliveryDates==null && other.getSundayDeliveryDates()==null) || 
              (this.sundayDeliveryDates!=null &&
               java.util.Arrays.equals(this.nextDayDeliveryDates, other.getNextDayDeliveryDates()))) &&  
            ((this.twoDayDeliveryDates==null && other.getTwoDayDeliveryDates()==null) || 
             (this.twoDayDeliveryDates!=null &&
              java.util.Arrays.equals(this.twoDayDeliveryDates, other.getTwoDayDeliveryDates()))) &&
            ((this.groundDeliveryDates==null && other.getGroundDeliveryDates()==null) || 
             (this.groundDeliveryDates!=null &&
              java.util.Arrays.equals(this.groundDeliveryDates, other.getGroundDeliveryDates()))) &&
            ((this.saturdayDeliveryDates==null && other.getSaturdayDeliveryDates()==null) || 
             (this.saturdayDeliveryDates!=null &&
              java.util.Arrays.equals(this.saturdayDeliveryDates, other.getSaturdayDeliveryDates())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNextDayDeliveryDates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNextDayDeliveryDates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNextDayDeliveryDates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTwoDayDeliveryDates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTwoDayDeliveryDates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTwoDayDeliveryDates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGroundDeliveryDates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGroundDeliveryDates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGroundDeliveryDates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSaturdayDeliveryDates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSaturdayDeliveryDates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSaturdayDeliveryDates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        
        if (getSundayDeliveryDates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSundayDeliveryDates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSundayDeliveryDates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PASShipDateAvailVO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://vo.common.pas.ftd.com", "PASShipDateAvailVO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextDayDeliveryDates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nextDayDeliveryDates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("twoDayDeliveryDates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "twoDayDeliveryDates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groundDeliveryDates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "groundDeliveryDates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("saturdayDeliveryDates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "saturdayDeliveryDates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sundayDeliveryDates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sundayDeliveryDates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public void setNextDayCharges(BigDecimal[] nextDayCharges) {
		this.nextDayCharges = nextDayCharges;
	}

	public BigDecimal[] getNextDayCharges() {
		return nextDayCharges;
	}

	public void setTwoDayCharges(BigDecimal[] twoDayCharges) {
		this.twoDayCharges = twoDayCharges;
	}

	public BigDecimal[] getTwoDayCharges() {
		return twoDayCharges;
	}

	public void setGroundCharges(BigDecimal[] groundCharges) {
		this.groundCharges = groundCharges;
	}

	public BigDecimal[] getGroundCharges() {
		return groundCharges;
	}

	public void setSaturdayCharges(BigDecimal[] saturdayCharges) {
		this.saturdayCharges = saturdayCharges;
	}

	public BigDecimal[] getSaturdayCharges() {
		return saturdayCharges;
	}

	public BigDecimal[] getSundayCharges() {
		return sundayCharges;
	}

	public void setSundayCharges(BigDecimal[] sundayCharges) {
		this.sundayCharges = sundayCharges;
	}

	public BigDecimal[] getSundayUpCharges() {
		return sundayUpCharges;
	}

	public void setSundayUpCharges(BigDecimal[] sundayUpCharges) {
		this.sundayUpCharges = sundayUpCharges;
	}

	
}
