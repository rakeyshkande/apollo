package com.ftd.pas.common.vo;

import java.math.BigDecimal;
import java.util.Calendar;

public class ProductAvailVO implements java.io.Serializable {
	private java.util.Calendar deliveryDate;

	private java.util.Calendar floristCutoffDate;

	private java.lang.String floristCutoffTime;

	private java.lang.Boolean isAvailable;

	private java.lang.Boolean isAddOnAvailable;

	private java.lang.Boolean isMorningDeliveryAvailable;

	private java.lang.String productId;

	private java.util.Calendar shipDate2D;

	private java.util.Calendar shipDateGR;

	private java.util.Calendar shipDateND;

	private java.util.Calendar shipDateSA;

	private java.util.Calendar shipDateSU;

	private java.lang.String zipCode;

	private java.lang.String shipDate2DCutoff;

	private java.lang.String shipDateGRCutoff;

	private java.lang.String shipDateNDCutoff;

	private java.lang.String shipDateSACutoff;

	private java.lang.String shipDateSUCutoff;

	private java.lang.Integer popularityOrderCount;

	private BigDecimal charges;

	private boolean hasCharges;

	private BigDecimal sundayUpCharges;
	
	private String errorMessage;
	
	private String shipDate;

	public ProductAvailVO() {
	}

	public ProductAvailVO(java.util.Calendar deliveryDate,
			java.util.Calendar floristCutoffDate,
			java.lang.String floristCutoffTime, java.lang.Boolean isAvailable,
			java.lang.String productId, java.util.Calendar shipDate2D,
			java.util.Calendar shipDateGR, java.util.Calendar shipDateND,
			java.util.Calendar shipDateSA, java.lang.String zipCode,
			java.lang.String shipDateNDCutoff,
			java.lang.String shipDate2DCutoff,
			java.lang.String shipDateGRCutoff,
			java.lang.String shipDateSACutoff,
			java.lang.Integer popularityOrderCount, Boolean isAddOnAvailable,
			java.util.Calendar shipDateSU, java.lang.String shipDateSUCutoff) {
		this.deliveryDate = deliveryDate;
		this.floristCutoffTime = floristCutoffTime;
		this.floristCutoffDate = floristCutoffDate;
		this.isAvailable = isAvailable;
		this.isAddOnAvailable = isAddOnAvailable;
		this.productId = productId;
		this.shipDate2D = shipDate2D;
		this.shipDateGR = shipDateGR;
		this.shipDateND = shipDateND;
		this.shipDateSA = shipDateSA;
		this.shipDateSU = shipDateSU;
		this.zipCode = zipCode;
		this.shipDate2DCutoff = shipDate2DCutoff;
		this.shipDateGRCutoff = shipDateGRCutoff;
		this.shipDateNDCutoff = shipDateNDCutoff;
		this.shipDateSACutoff = shipDateSACutoff;
		this.shipDateSUCutoff = shipDateSUCutoff;
		this.popularityOrderCount = popularityOrderCount;
	}

	/**
	 * Gets the deliveryDate value for this ProductAvailVO.
	 * 
	 * @return deliveryDate
	 */
	public java.util.Calendar getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Sets the deliveryDate value for this ProductAvailVO.
	 * 
	 * @param deliveryDate
	 */
	public void setDeliveryDate(java.util.Calendar deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public void setDeliveryDate(java.util.Date deliveryDate) {
		this.deliveryDate = Calendar.getInstance();
		this.deliveryDate.setTime(deliveryDate);
	}

	/**
	 * Gets the floristCutoff value for this ProductAvailVO.
	 * 
	 * @return floristCutoffTime
	 */
	public java.lang.String getFloristCutoffTime() {
		return floristCutoffTime;
	}

	/**
	 * Sets the floristCutoff value for this ProductAvailVO.
	 * 
	 * @param floristCutoffTime
	 */
	public void setFloristCutoffTime(java.lang.String floristCutoffTime) {
		this.floristCutoffTime = floristCutoffTime;
	}

	public java.lang.Boolean getIsAddOnAvailable() {
		return isAddOnAvailable;
	}

	public void setIsAddOnAvailable(java.lang.Boolean isAddOnAvailable) {
		this.isAddOnAvailable = isAddOnAvailable;
	}

	/**
	 * Gets the isAvailable value for this ProductAvailVO.
	 * 
	 * @return isAvailable
	 */
	public java.lang.Boolean getIsAvailable() {
		return isAvailable;
	}

	/**
	 * Sets the isAvailable value for this ProductAvailVO.
	 * 
	 * @param isAvailable
	 */
	public void setIsAvailable(java.lang.Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	/**
	 * Gets the productId value for this ProductAvailVO.
	 * 
	 * @return productId
	 */
	public java.lang.String getProductId() {
		return productId;
	}

	/**
	 * Sets the productId value for this ProductAvailVO.
	 * 
	 * @param productId
	 */
	public void setProductId(java.lang.String productId) {
		this.productId = productId;
	}

	/**
	 * Gets the shipDate2D value for this ProductAvailVO.
	 * 
	 * @return shipDate2D
	 */
	public java.util.Calendar getShipDate2D() {
		return shipDate2D;
	}

	/**
	 * Sets the shipDate2D value for this ProductAvailVO.
	 * 
	 * @param shipDate2D
	 */
	public void setShipDate2D(java.util.Calendar shipDate2D) {
		this.shipDate2D = shipDate2D;
	}

	public void setShipDate2D(java.util.Date shipDate) {
		if (shipDate != null) {
			this.shipDate2D = Calendar.getInstance();
			this.shipDate2D.setTime(shipDate);
		}
	}

	/**
	 * Gets the shipDateGR value for this ProductAvailVO.
	 * 
	 * @return shipDateGR
	 */
	public java.util.Calendar getShipDateGR() {
		return shipDateGR;
	}

	/**
	 * Sets the shipDateGR value for this ProductAvailVO.
	 * 
	 * @param shipDateGR
	 */
	public void setShipDateGR(java.util.Calendar shipDateGR) {
		this.shipDateGR = shipDateGR;
	}

	public void setShipDateGR(java.util.Date shipDate) {
		if (shipDate != null) {
			this.shipDateGR = Calendar.getInstance();
			this.shipDateGR.setTime(shipDate);
		}
	}

	/**
	 * Gets the shipDateND value for this ProductAvailVO.
	 * 
	 * @return shipDateND
	 */
	public java.util.Calendar getShipDateND() {
		return shipDateND;
	}

	/**
	 * Sets the shipDateND value for this ProductAvailVO.
	 * 
	 * @param shipDateND
	 */
	public void setShipDateND(java.util.Calendar shipDateND) {
		this.shipDateND = shipDateND;
	}

	public void setShipDateND(java.util.Date shipDateND) {
		if (shipDateND != null) {
			this.shipDateND = Calendar.getInstance();
			this.shipDateND.setTime(shipDateND);
		}
	}

	/**
	 * Gets the shipDateSA value for this ProductAvailVO.
	 * 
	 * @return shipDateSA
	 */
	public java.util.Calendar getShipDateSA() {
		return shipDateSA;
	}

	/**
	 * Sets the shipDateSA value for this ProductAvailVO.
	 * 
	 * @param shipDateSA
	 */
	public void setShipDateSA(java.util.Calendar shipDateSA) {
		this.shipDateSA = shipDateSA;
	}

	public void setShipDateSA(java.util.Date shipDate) {
		if (shipDate != null) {
			this.shipDateSA = Calendar.getInstance();
			this.shipDateSA.setTime(shipDate);
		}
	}

	/**
	 * Gets the zipCode value for this ProductAvailVO.
	 * 
	 * @return zipCode
	 */
	public java.lang.String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the zipCode value for this ProductAvailVO.
	 * 
	 * @param zipCode
	 */
	public void setZipCode(java.lang.String zipCode) {
		this.zipCode = zipCode;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ProductAvailVO))
			return false;
		ProductAvailVO other = (ProductAvailVO) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.deliveryDate == null && other.getDeliveryDate() == null) || (this.deliveryDate != null && this.deliveryDate
						.equals(other.getDeliveryDate())))
				&& ((this.floristCutoffDate == null && other
						.getFloristCutoffDate() == null) || (this.floristCutoffDate != null && this.floristCutoffDate
						.equals(other.getFloristCutoffDate())))
				&& ((this.floristCutoffTime == null && other
						.getFloristCutoffTime() == null) || (this.floristCutoffTime != null && this.floristCutoffTime
						.equals(other.getFloristCutoffTime())))
				&& ((this.isAvailable == null && other.getIsAvailable() == null) || (this.isAvailable != null && this.isAvailable
						.equals(other.getIsAvailable())))
				&& ((this.isAddOnAvailable == null && other
						.getIsAddOnAvailable() == null) || (this.isAddOnAvailable != null && this.isAddOnAvailable
						.equals(other.getIsAddOnAvailable())))
				&& ((this.productId == null && other.getProductId() == null) || (this.productId != null && this.productId
						.equals(other.getProductId())))
				&& ((this.shipDate2D == null && other.getShipDate2D() == null) || (this.shipDate2D != null && this.shipDate2D
						.equals(other.getShipDate2D())))
				&& ((this.shipDateGR == null && other.getShipDateGR() == null) || (this.shipDateGR != null && this.shipDateGR
						.equals(other.getShipDateGR())))
				&& ((this.shipDateND == null && other.getShipDateND() == null) || (this.shipDateND != null && this.shipDateND
						.equals(other.getShipDateND())))
				&& ((this.shipDateSA == null && other.getShipDateSA() == null) || (this.shipDateSA != null && this.shipDateSA
						.equals(other.getShipDateSA())))
				&& ((this.popularityOrderCount == null && other
						.getPopularityOrderCount() == null) || (this.popularityOrderCount != null && this.shipDateSA
						.equals(other.getPopularityOrderCount())))
				&& ((this.zipCode == null && other.getZipCode() == null) || (this.zipCode != null && this.zipCode
						.equals(other.getZipCode())))
				&& ((this.shipDateSU == null && other.getShipDateSU() == null) || (this.shipDateSU != null && this.shipDateSU
						.equals(other.getShipDateSU())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getDeliveryDate() != null) {
			_hashCode += getDeliveryDate().hashCode();
		}
		if (getFloristCutoffTime() != null) {
			_hashCode += getFloristCutoffTime().hashCode();
		}
		if (getIsAvailable() != null) {
			_hashCode += getIsAvailable().hashCode();
		}
		if (getIsAddOnAvailable() != null) {
			_hashCode += getIsAddOnAvailable().hashCode();
		}

		if (getProductId() != null) {
			_hashCode += getProductId().hashCode();
		}
		if (getShipDate2D() != null) {
			_hashCode += getShipDate2D().hashCode();
		}
		if (getShipDateGR() != null) {
			_hashCode += getShipDateGR().hashCode();
		}
		if (getShipDateND() != null) {
			_hashCode += getShipDateND().hashCode();
		}
		if (getShipDateSA() != null) {
			_hashCode += getShipDateSA().hashCode();
		}
		if (getShipDateSU() != null) {
			_hashCode += getShipDateSU().hashCode();
		}
		if (getZipCode() != null) {
			_hashCode += getZipCode().hashCode();
		}
		if (getShipDate2DCutoff() != null) {
			_hashCode += getShipDate2DCutoff().hashCode();
		}
		if (getShipDateGRCutoff() != null) {
			_hashCode += getShipDateGRCutoff().hashCode();
		}
		if (getShipDateNDCutoff() != null) {
			_hashCode += getShipDateNDCutoff().hashCode();
		}
		if (getShipDateSACutoff() != null) {
			_hashCode += getShipDateSACutoff().hashCode();
		}
		if (getPopularityOrderCount() != null) {
			_hashCode += getPopularityOrderCount().hashCode();
		}
		if (getShipDateSUCutoff() != null) {
			_hashCode += getShipDateSUCutoff().hashCode();
		}

		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			ProductAvailVO.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://vo.common.pas.ftd.com", "ProductAvailVO"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("deliveryDate");
		elemField.setXmlName(new javax.xml.namespace.QName("", "deliveryDate"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("floristCutoffTime");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"floristCutoffTime"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("floristCutoffDate");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"floristCutoffDate"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("isAvailable");
		elemField.setXmlName(new javax.xml.namespace.QName("", "isAvailable"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("productId");
		elemField.setXmlName(new javax.xml.namespace.QName("", "productId"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDate2D");
		elemField.setXmlName(new javax.xml.namespace.QName("", "shipDate2D"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateGR");
		elemField.setXmlName(new javax.xml.namespace.QName("", "shipDateGR"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateND");
		elemField.setXmlName(new javax.xml.namespace.QName("", "shipDateND"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateSA");
		elemField.setXmlName(new javax.xml.namespace.QName("", "shipDateSA"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateSU");
		elemField.setXmlName(new javax.xml.namespace.QName("", "shipDateSU"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "dateTime"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("zipCode");
		elemField.setXmlName(new javax.xml.namespace.QName("", "zipCode"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateNDCutoff");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"shipDateNDCutoff"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDate2DCutoff");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"shipDate2DCutoff"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateGRCutoff");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"shipDateGRCutoff"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateSACutoff");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"shipDateSACutoff"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("shipDateSUCutoff");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"shipDateSUCutoff"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "string"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("popularityOrderCount");
		elemField.setXmlName(new javax.xml.namespace.QName("",
				"popularityOrderCount"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://schemas.xmlsoap.org/soap/encoding/", "int"));
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object.
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer.
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType,
				_xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer.
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType,
				_xmlType, typeDesc);
	}

	public void setShipDate2DCutoff(String param) {
		this.shipDate2DCutoff = param;
	}

	public String getShipDate2DCutoff() {
		return shipDate2DCutoff;
	}

	public void setShipDateGRCutoff(String param) {
		this.shipDateGRCutoff = param;
	}

	public String getShipDateGRCutoff() {
		return shipDateGRCutoff;
	}

	public void setShipDateNDCutoff(String param) {
		this.shipDateNDCutoff = param;
	}

	public String getShipDateNDCutoff() {
		return shipDateNDCutoff;
	}

	public void setShipDateSACutoff(String param) {
		this.shipDateSACutoff = param;
	}

	public String getShipDateSACutoff() {
		return shipDateSACutoff;
	}

	public void setFloristCutoffDate(Calendar param) {
		this.floristCutoffDate = param;
	}

	public Calendar getFloristCutoffDate() {
		return floristCutoffDate;
	}

	public void setFloristCutoffDate(java.util.Date cutoffDate) {
		if (cutoffDate != null) {
			this.floristCutoffDate = Calendar.getInstance();
			this.floristCutoffDate.setTime(cutoffDate);
		}
	}

	public void setPopularityOrderCount(Integer param) {
		this.popularityOrderCount = param;
	}

	public Integer getPopularityOrderCount() {
		return popularityOrderCount;
	}

	public java.util.Calendar getShipDateSU() {
		return shipDateSU;
	}

	public void setShipDateSU(java.util.Calendar shipDateSU) {
		this.shipDateSU = shipDateSU;
	}

	public void setShipDateSU(java.util.Date shipDate) {
		if (shipDate != null) {
			this.shipDateSU = Calendar.getInstance();
			this.shipDateSU.setTime(shipDate);
		}
	}

	public void setShipDateSUCutoff(String param) {
		this.shipDateSUCutoff = param;
	}

	public String getShipDateSUCutoff() {
		return shipDateSUCutoff;
	}

	public void setCharges(BigDecimal charges) {
		this.charges = charges;
	}

	public BigDecimal getCharges() {
		return charges;
	}

	public void setHasCharges(boolean hasCharges) {
		this.hasCharges = hasCharges;
	}

	public boolean isHasCharges() {
		return hasCharges;
	}

	public void setIsMorningDeliveryAvailable(
			java.lang.Boolean isMorningDeliveryAvailable) {
		this.isMorningDeliveryAvailable = isMorningDeliveryAvailable;
	}

	public java.lang.Boolean getIsMorningDeliveryAvailable() {
		return isMorningDeliveryAvailable;
	}

	public BigDecimal getSundayUpCharges() {
		return sundayUpCharges;
	}

	public void setSundayUpCharges(BigDecimal sundayUpCharges) {
		this.sundayUpCharges = sundayUpCharges;
	}

	@Override
	public String toString() {
		return "ProductAvailVO [deliveryDate=" + deliveryDate
				+ ", floristCutoffDate=" + floristCutoffDate
				+ ", floristCutoffTime=" + floristCutoffTime + ", isAvailable="
				+ isAvailable + ", isAddOnAvailable=" + isAddOnAvailable
				+ ", isMorningDeliveryAvailable=" + isMorningDeliveryAvailable
				+ ", productId=" + productId + ", shipDate2D=" + shipDate2D
				+ ", shipDateGR=" + shipDateGR + ", shipDateND=" + shipDateND
				+ ", shipDateSA=" + shipDateSA + ", shipDateSU=" + shipDateSU
				+ ", zipCode=" + zipCode + ", shipDate2DCutoff="
				+ shipDate2DCutoff + ", shipDateGRCutoff=" + shipDateGRCutoff
				+ ", shipDateNDCutoff=" + shipDateNDCutoff
				+ ", shipDateSACutoff=" + shipDateSACutoff
				+ ", shipDateSUCutoff=" + shipDateSUCutoff
				+ ", popularityOrderCount=" + popularityOrderCount
				+ ", charges=" + charges + ", hasCharges=" + hasCharges
				+ ", sundayUpCharges=" + sundayUpCharges + ", __equalsCalc="
				+ __equalsCalc + ", __hashCodeCalc=" + __hashCodeCalc + "]";
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getShipDate() {
		return shipDate;
	}

	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}

}
