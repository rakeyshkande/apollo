package com.ftd.pas.common.vo;

import java.util.Date;

public class PASTimeZoneVO
{
    private int timeZoneCode;
    private String daylightSavingsFlag;
    private Date   deliveryDate;
    private int cstDelta;
    
    public PASTimeZoneVO()
    {
    }

    public void setTimeZoneCode(int param)
    {
        this.timeZoneCode = param;
    }

    public int getTimeZoneCode()
    {
        return timeZoneCode;
    }

    public void setCstDelta(int param)
    {
        this.cstDelta = param;
    }

    public int getCstDelta()
    {
        return cstDelta;
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setDaylightSavingsFlag(String param)
    {
        this.daylightSavingsFlag = param;
    }

    public String getDaylightSavingsFlag()
    {
        return daylightSavingsFlag;
    }
}
