package com.ftd.pas.common.vo;

import java.util.Date;

public class PASFloristProductZipVO
{
    private Date   deliveryDate;
    private String zipCode;
    private String codificationId;
    private String productId;
    private String cutoffTime;
    private String addonDays;
        
    public PASFloristProductZipVO()
    {
    }

    public void setDeliveryDate(Date param)
    {
        this.deliveryDate = param;
    }

    public Date getDeliveryDate()
    {
        return deliveryDate;
    }

    public void setZipCode(String param)
    {
        this.zipCode = param;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setCodificationId(String param)
    {
        this.codificationId = param;
    }

    public String getCodificationId()
    {
        return codificationId;
    }

    public void setProductId(String param)
    {
        this.productId = param;
    }

    public String getProductId()
    {
        return productId;
    }

    public void setCutoffTime(String param)
    {
        this.cutoffTime = param;
    }

    public String getCutoffTime()
    {
        return cutoffTime;
    }

    public void setAddonDays(String param)
    {
        this.addonDays = param;
    }

    public String getAddonDays()
    {
        return addonDays;
    }

}
