package com.ftd.pas.common.vo;

public class ProductDetailVO {
	
	private String productId;
	private String novatorId;
	private String shippingSystem;
	private String personalizationTemplateId;
	private String status;
	private String shipMethodCarrier;
	private String shipMethodFlorist;
	
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getNovatorId() {
		return novatorId;
	}
	public void setNovatorId(String novatorId) {
		this.novatorId = novatorId;
	}
	public String getShippingSystem() {
		return shippingSystem;
	}
	public void setShippingSystem(String shippingSystem) {
		this.shippingSystem = shippingSystem;
	}
	public String getPersonalizationTemplateId() {
		return personalizationTemplateId;
	}
	public void setPersonalizationTemplateId(String personalizationTemplateId) {
		this.personalizationTemplateId = personalizationTemplateId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShipMethodCarrier() {
		return shipMethodCarrier;
	}
	public void setShipMethodCarrier(String shipMethodCarrier) {
		this.shipMethodCarrier = shipMethodCarrier;
	}
	public String getShipMethodFlorist() {
		return shipMethodFlorist;
	}
	public void setShipMethodFlorist(String shipMethodFlorist) {
		this.shipMethodFlorist = shipMethodFlorist;
	}
	
	
	
	
	
	


}
