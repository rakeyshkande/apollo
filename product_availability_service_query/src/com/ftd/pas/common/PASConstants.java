package com.ftd.pas.common;

public interface PASConstants
{
    public final static String PROPERTY_FILE   = "pasq-config.xml";
    public final static String DATASOURCE_NAME = "DATASOURCE";
    public final static String COMMAND_KEYWORD       = "command";
    public final static String PRODUCT_KEYWORD       = "productId";
    public final static String DELIVERY_DATE_KEYWORD = "deliveryDate";
    public final static String ZIP_KEYWORD           = "zipCode";
    public final static String COUNTRY_KEYWORD       = "countryId";
    public final static String COMMAND_GNADD = "getNextAvailableDeliveryDate";
    public final static String PAS_CONFIG_CONTEXT = "PAS_CONFIG";
    public final static String PAS_URL            = "PAS_URL";
    public final static String MAX_DAYS_PARM      = "MAX_DAYS"; 
    public final static String PAC_WS_URI = "http://metisdapp01v1.ftdi.com:38080/pacs/";//"http://10.103.14.43:8080";
    public final static String PAC_WS_GET_PRODUCT_AVAILABLE_DATES_AND_CHARGES = "getProductAvailableDatesAndCharges";
    public final static String PAC_WS_GET_AVAILABLE_PRODUCTS = "getAvailableProductBundle";
    public final static String PAC_WS_SOURCE_CODE = "350";
    public final static String PAC_CUT_OFF = "2359";
    public final static String SHIPPING_SYSTEM_WEST = "FTD WEST";
    public final static short ERROR_CODE_320 = 320;
    
    
}
