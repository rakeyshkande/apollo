package com.ftd.pas.common.service;

import com.ftd.pas.common.vo.PASShipDateAvailVO;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.exception.PASServiceException;

import java.util.Date;

/**
 * Product Availability Request Service interface.
 */
public interface PASRequestService
{
    public Date getNextAvailableDeliveryDate(String productId, String zip) throws PASServiceException;

    public Boolean isProductAvailable(String productId, Date deliveryDate, String zip) throws PASServiceException;

    public ProductAvailVO getProductAvailability(String productId, Date deliveryDate, String zip) throws PASServiceException;

    public Boolean isAnyProductAvailable(Date deliveryDate, String zip) throws PASServiceException;

    public ProductAvailVO[] getMostPopularProducts(Date deliveryDate, String zip, int numberOfResults, String companyId) throws PASServiceException;

    public String[] getFlorists(Date deliveryDate, String zip) throws PASServiceException;

    public Boolean isInternationalProductAvailable(String product, Date deliveryDate, String country) throws PASServiceException;
    
    public PASShipDateAvailVO getAvailableShipDates(String productId, int numberOfDays, String zipCode) throws PASServiceException;
    
    public ProductAvailVO[] getProductAvailableDates(String productId, String zip, int days) throws PASServiceException;

    public ProductAvailVO[] getInternationalProductAvailableDates(String productId, String countryCode, int days) throws PASServiceException;
    
    public String[] isProductListAvailable(String zipCode, Date deliveryDate, String[] productList) throws PASServiceException;

    public String[] isInternationalProductListAvailable(String countryCode, Date deliveryDate, String[] productList) throws PASServiceException;
}
