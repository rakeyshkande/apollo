<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
  <head>  
    <title>PAS Availability</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function productChange()
      {
          var productSelect = document.getElementById("productIdSelect");
          var productInput = document.getElementById("productId");
          
          var productId = productSelect.options[productSelect.selectedIndex].value;
        
          productInput.value = productId;
      }

      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var stateSelect = document.getElementById("stateSelect");
          var zipInput = document.getElementById("zipcode");
          var stateInput = document.getElementById("stateCode");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
          var stateCode = stateSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
          stateInput.value = stateCode;
      }
      
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>
    <div align="center">
      <form action="pascheck.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
          	<div>
            <label for="productid">Product Id:</label>
            <div>
            <input id="productId" name="productId" value="${pasCheckForm.productId}" size="8"/>
            <select id="productIdSelect" name="productIdSelect" onchange="productChange()" >
              <option value="8212">8212 - Florist</option>
              <option value="F088">F088 - Vendor</option>
              <option value="F103">F103 - Both</option>
              <option value="MAC1">MAC1 - Both</option>
              <option value="DEAD1">DEAD1 - Subcode</option>
            </select>
            </div>
            </div>
            
            <div>
            <label for="addons">AddOns :</label>
            <div>
            <input id="addons" name="addOns" value="${pasCheckForm.addOns}" size="25"/>
            </div>
            </div>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label>
            <input id="deliverydate" name="deliveryDate" value="${pasCheckForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <div>
            <input id="zipcode" name="zipCode" value="${pasCheckForm.zipCode}" size="8"/>
            <select id="zipSelect" name="zipSelect" onchange="zipChange()">
              <option value="60515">60515 - IL</option>
              <option value="96766">96766 - HI</option>
              <option value="99705">99705 - AK</option>
              <option value="99701">99701 - AK</option>
              <option value="85005">85005 - AZ</option>
            </select>
            <select id="stateSelect" name="stateSelect" class="hidden"> <!-- Must match zip order -->
              <option value="IL">60515 - IL</option>
              <option value="HI">96766 - HI</option>
              <option value="AK">99705 - AK</option>
              <option value="AK">99701 - AK</option>
              <option value="AZ">85005 - AZ</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="stateCode">State Code:</label>
            <input id="stateCode" name="stateCode" value="${pasCheckForm.stateCode}" size="4"/>
            <br/>
            <label for="countryCode">Country Code:</label>
            <input id="countryCode" name="countryCode" value="${pasCheckForm.countryCode}" size="4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <div class="leftSide">
        <h2 align="center">PAS</h2>
        <div class="leftSide">
          <p class="oneValue">isAvailable:</p>
          <p class="oneValue">isAddOnAvailable:</p>
          <p class="oneValue">isMorningDeliveryAvailable:</p>
          <p class="oneValue">ProductId:</p>
          <p class="oneValue">ZipCode:</p>
          <p class="oneValue">Next Avail Date:</p>
          <p class="oneValue">Delivery Date:</p>
          <p class="oneValue">Florist Cutoff:</p> 
          <p class="oneValue">Next Day Ship:</p>
          <p class="oneValue">Two Day Ship:</p>
          <p class="oneValue">Ground Ship:</p>
          <p class="oneValue">Saturday Ship:</p>
          <p class="oneValue">pas_getProductAvailability:</p>
          <p class="oneValue">pas_nextAvailDDTimer:</p>
          <p class="oneValue">pas_getProductAvailableDates:</p>
        </div>
        <div class="leftSide">
          <p class="oneValue">${productVO.isAvailable}&nbsp;</p>
          <p class="oneValue">${productVO.isAddOnAvailable}&nbsp;</p>
          <p class="oneValue">${productVO.isMorningDeliveryAvailable}&nbsp;</p>
          <p class="oneValue">${productVO.productId}&nbsp;</p>
          <p class="oneValue">${productVO.zipCode}&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${nextAvailDate}" pattern="MM/dd/yyyy" />&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${productVO.deliveryDate.time}" pattern="MM/dd/yyyy" />&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${productVO.floristCutoffDate.time}" pattern="MM/dd/yyyy" /> ${productVO.floristCutoffTime}&nbsp;</p> 
          <p class="oneValue"><fmt:formatDate value="${productVO.shipDateND.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateNDCutoff}&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${productVO.shipDate2D.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDate2DCutoff}&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${productVO.shipDateGR.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateGRCutoff}&nbsp;</p>
          <p class="oneValue"><fmt:formatDate value="${productVO.shipDateSA.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateSACutoff}&nbsp;</p>
          <p class="oneValue">${pas_getProductAvailability}&nbsp;</p>
          <p class="oneValue">${pas_nextAvailDDTimer}&nbsp;</p>
          <p class="oneValue">${pas_getProductDatesTimer}&nbsp;</p>
        </div>
      </div>
      <div class="rightSide">
        <div class="results1">
          <display:table name="availableDates" id="availableDates" class="altstripe">
            <display:caption>Available Dates</display:caption>
            <display:column property="deliveryDate.time" format="{0,date,MM/dd/yyyy}" title="Delivery Date"/>
            <display:column property="floristCutoffDate.time" format="{0,date,MM/dd/yyyy}" title="Florist"/>
            <display:column property="shipDateND.time" format="{0,date,MM/dd/yyyy}" title="Next Day"/>
            <display:column property="shipDate2D.time" format="{0,date,MM/dd/yyyy}" title="Two Day"/>
            <display:column property="shipDateGR.time" format="{0,date,MM/dd/yyyy}" title="Ground"/>
            <display:column property="shipDateSA.time" format="{0,date,MM/dd/yyyy}" title="Saturday"/>
            <display:column property="shipDateSU.time" format="{0,date,MM/dd/yyyy}" title="Sunday"/>
          </display:table>
        </div>
      </div>
    </div>
    
  </body>
</html>