<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
  <head>  
    <title>Get Routable Florist</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function productChange()
      {
          var productSelect = document.getElementById("productIdSelect");
          var productInput = document.getElementById("productId");
          
          var productId = productSelect.options[productSelect.selectedIndex].value;
        
          productInput.value = productId;
      }

      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var stateSelect = document.getElementById("stateSelect");
          var zipInput = document.getElementById("zipcode");
          var stateInput = document.getElementById("deliveryState");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
          var stateCode = stateSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
          stateInput.value = stateCode;
      }
      
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>
    <div align="center">
      <form action="routablefloristcheck.do">
        <div class="top">
          <h2 align="center">Routable Florist Query</h2>
          <div class="full" align="center">
          <table width="100%">
            <tr>
              <td>
                <label for="productid">Product Id:</label>
              </td>
              <td>
                <input id="productId" name="productId" value="${routableFloristCheckForm.productId}" size="8"/>
              </td>
              <td>
                <label for="sourceCode">Source Code:</label>
              </td>
              <td>
                <input id="sourceCode" name="sourceCode" value="${routableFloristCheckForm.sourceCode}" size="12"/>
              </td>
              <td>
                <label for="deliverydate">Delivery Date:</label>
              </td>
              <td>
                <input id="deliverydate" name="deliveryDate" value="${routableFloristCheckForm.deliveryDate}" size="12"/>
              </td>
              <td>
                <label for="zipcode">Zip Code:</label>
              </td>
              <td>
                <input id="zipcode" name="zipCode" value="${routableFloristCheckForm.zipCode}" size="8"/>
              </td>

            </tr>

            <tr>
              <td>&nbsp;</td>
              <td>
                <select id="productIdSelect" name="productIdSelect" onchange="productChange()" >
                  <option value="8212">8212 - Florist</option>
                  <option value="F088">F088 - Vendor</option>
                  <option value="F103">F103 - Both</option>
                  <option value="MAC1">MAC1 - Both</option>
                </select>
              </td>
              <td>
                <label for="orderDetailId">Order Detail ID:</label>
              </td>
              <td>
                <input id="orderDetailId" name="orderDetailId" value="${routableFloristCheckForm.orderDetailId}" size="12"/>
              </td>
              <td>
                <label for="deliverydateEnd">Delivery Date End:</label>
              </td>
              <td>
                <input id="deliverydateEnd" name="deliveryDateEnd" value="${routableFloristCheckForm.deliveryDateEnd}" size="12"/>
              </td>
              <td>&nbsp;</td>
              <td>
                <select id="zipSelect" name="zipSelect" onchange="zipChange()">
                  <option value="60515">60515 - IL</option>
                  <option value="96766">96766 - HI</option>
                  <option value="99705">99705 - AK</option>
                  <option value="99701">99701 - AK</option>
                  <option value="85005">85005 - AZ</option>
                </select>
              </td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <label for="orderValue">Order Value:</label>
              </td>
              <td>
                <input id="orderValue" name="orderValue" value="${routableFloristCheckForm.orderValue}" size="12"/>
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <label for="deliveryState">State Code:</label>
              </td>
              <td>
                <input id="deliveryState" name="deliveryState" value="${routableFloristCheckForm.deliveryState}" size="4"/>
              </td>
            </tr>

            <tr>
              <td>
                <label for="returnAll">Return All:</label>
              </td>
              <td>
                <input id="returnAll" name="returnAll" value="${routableFloristCheckForm.returnAll}" size="4"/>
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select id="stateSelect" name="stateSelect" class="hidden"> <!-- Must match zip order -->
                  <option value="IL">60515 - IL</option>
                  <option value="HI">96766 - HI</option>
                  <option value="AK">99705 - AK</option>
                  <option value="AK">99701 - AK</option>
                  <option value="AZ">85005 - AZ</option>
                </select>
              </td>
              <td>&nbsp;</td>
              <td>
                <label for="deliveryCity">City:</label>
              </td>
              <td>
                <input id="deliveryCity" name="deliveryCity" value="${routableFloristCheckForm.deliveryCity}" size="12"/>
              </td>
            </tr>
          </table>
          </div>

          <div class="full" align="center">
            <input type="submit" name="action" value="Perform RWD" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
        <div class="results">
          <display:table name="routableFloristList" id="routableFloristVO" class="full">
              <display:caption>Florist List</display:caption>
              <display:column property="floristId" title="Florist Id"/>
              <display:column property="status" title="Status"/>
              <display:column property="score" title="score"/>
              <display:column property="weight" title="weight"/>
              <display:column property="floristName" title="Name"/>
              <display:column property="mercuryFlag" title="Mercury"/>
              <display:column property="superFloristFlag" title="GoTo"/>
              <display:column property="sundayDeliveryFlag" title="Sunday"/>
              <display:column property="cutoffTime" title="Cutoff"/>
              <display:column property="floristSuspended" title="Suspended"/>
              <display:column property="minOrderAmt" title="Minimum"/>
              <display:column property="codificationId" title="Codification Id"/>
              <display:column property="floristBlocked" title="Blocked"/>
              <display:column property="productCodificationId" title="Product Codification Id"/>
              <display:column property="sequences" title="Sequences"/>
              <display:column property="zipCityFlag" title="Zip City Flag"/>
            </display:table>
        </div>
      </div>
    </div>
    
  </body>
</html>