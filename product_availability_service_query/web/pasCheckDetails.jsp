<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
  <head>  
    <title>PAS Details</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function productChange()
      {
          var productSelect = document.getElementById("productIdSelect");
          var productInput = document.getElementById("productId");
          
          var productId = productSelect.options[productSelect.selectedIndex].value;
        
          productInput.value = productId;
      }
      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var stateSelect = document.getElementById("stateSelect");
          var zipInput = document.getElementById("zipcode");
          var stateInput = document.getElementById("stateCode");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
          var stateCode = stateSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
          stateInput.value = stateCode;
      }
      
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>    
    <div align="center">
      <form action="pascheckdetails.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productid">Product Id:</label>
            <div>
            <input id="productId" name="productId" value="${pasCheckDetailsForm.productId}" size="8"/>
            <select id="productIdSelect" name="productIdSelect" onchange="productChange()" >
              <option value="8212">8212 - Florist</option>
              <option value="F088">F088 - Vendor</option>
              <option value="F103">F103 - Both</option>
              <option value="MAC1">MAC1 - Both</option>
              <option value="DEAD1">DEAD1 - Subcode</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label>
            <input id="deliverydate" name="deliveryDate" value="${pasCheckDetailsForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <div>
            <input id="zipcode" name="zipCode" value="${pasCheckDetailsForm.zipCode}" size="8"/>
            <select id="zipSelect" name="zipSelect" onchange="zipChange()">
              <option value="60148">60148 - IL</option>
              <option value="60515">60515 - IL</option>
              <option value="96766">96766 - HI</option>
              <option value="99705">99705 - AK</option>
              <option value="99701">99701 - AK</option>
              <option value="85005">85005 - AZ</option>
            </select>
            <select id="stateSelect" name="stateSelect" class="hidden"> <!-- Must match zip order -->
              <option value="IL">60148 - IL</option>
              <option value="IL">60515 - IL</option>
              <option value="HI">96766 - HI</option>
              <option value="AK">99705 - AK</option>
              <option value="AK">99701 - AK</option>
              <option value="AZ">85005 - AZ</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="stateCode">State Code:</label>
            <input id="stateCode" name="stateCode" value="${pasCheckDetailsForm.stateCode}" size="4"/>
          </div>
          <div class="quarter" >
            <label for="daysBack">Days:</label>
            <input id="daysBack" name="daysBack" value="${pasCheckDetailsForm.daysBack}" size="2"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS Details" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <h2 align="center">Get PAS Details</h2>
        <div class="pasProduct">
            <display:table name="pasCheckDetailsForm.pasProductDateList" id="pasProductDate" class="altstripe">
              <display:caption>PAS_PRODUCT_DT</display:caption>
              <display:column property="productId" title="Product Id"/>
              <display:column property="deliveryDate" title="Delivery Date"/>
              <display:column property="cutoffTime" title="Cutoff Time"/>
              <display:column property="addonDays" title="Addon Days"/>
              <display:column property="transitDays" title="Transit Days"/>
              <display:column property="shipAllowed" title="Ship Allowed"/>
              <display:column property="shipRestricted" title="Ship Restricted"/>
              <display:column property="deliveryAvailable" title="Delivery Available"/>
              <display:column property="codificationId" title="Codification Id"/>
              <display:column property="productType" title="Product Type"/>
            </display:table>
        </div>
        <div class="pasVendorProductZip">
            <display:table name="pasCheckDetailsForm.pasVendorProductStateDateList" id="pasVendorProductStateDate" class="altstripe">
              <display:caption>PAS_VENDOR_PRODUCT_STATE_DT</display:caption>
              <display:column property="vendorId" title="Vendor Id"/>
              <display:column property="productId" title="Product Id"/>
              <display:column property="stateCode" title="State Code"/>
              <display:column property="deliveryDate" title="Delivery Date"/>
              <display:column property="shipNDDate" title="ND Date" />
              <display:column property="shipNDCutoffTime" title="ND Cutoff" />
              <display:column property="ship2DDate" title="2D Date" />
              <display:column property="ship2DCutoffTime" title="2D Cutoff" />
              <display:column property="shipGRDate" title="GR Date" />
              <display:column property="shipGRCutoffTime" title="GR Cutoff" />
              <display:column property="shipSatDate" title="Sat Date" />
              <display:column property="shipSatCutoffTime" title="Sat Cutoff" />
            </display:table>
        </div>
        <div class="pasProductState">
            <display:table name="pasCheckDetailsForm.pasProductStateDateList" id="pasProductStateDate" class="altstripe">
              <display:caption>PAS_PRODUCT_STATE_DT</display:caption>
              <display:column property="productId" title="Product Id"/>
              <display:column property="stateCode" title="State Code"/>
              <display:column property="deliveryDate" title="Delivery Date"/>
              <display:column property="deliveryAvailable" title="Delivery Available"/>
            </display:table>
        </div>
        <div class="pasCountry">
            <display:table name="pasCheckDetailsForm.pasCountryDateList" id="pasCountryDate" class="altstripe">
              <display:caption>PAS_COUNTRY_DT</display:caption>
              <display:column property="countryId" title="Country Id"/>
              <display:column property="deliveryDate" title="Delivery Date"/>
              <display:column property="cutoffTime" title="Cutoff"/>
              <display:column property="addonDays" title="Addon Days"/>
              <display:column property="shippingAllowed" title="Ship"/>
              <display:column property="deliveryAvailable" title="Delivery"/>
              <display:column property="transitAllowed" title="Transit"/>
            </display:table>
        </div>
        <div class="pasFlorist">
            <display:table name="pasCheckDetailsForm.pasFloristProductZipDateList" id="pasFloristProductZipDate" class="altstripe">
              <display:caption>PAS_FLORIST_PRODUCT_ZIP_DT_VW</display:caption>
              <display:column property="productId" title="Product Id"/>
              <display:column property="zipCode" title="Zip"/>
              <display:column property="deliveryDate" title="Delivery Date"/>
              <display:column property="cutoffTime" title="Cutoff Time"/>
              <display:column property="addonDays" title="Addon Days"/>
              <display:column property="codificationId" title="Codification Id"/>
            </display:table>
        </div>
    </div>
    
  </body>
</html>
