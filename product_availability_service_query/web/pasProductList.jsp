<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
  <head>  
    <title>PAS Availability</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>
    <div align="center">
      <form action="pasproductlist.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productList">Product List:</label>
            <textarea id="productList" name="productList" cols="20" rows="2">${pasProductListForm.productList}</textarea>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label>
            <input id="deliverydate" name="deliveryDate" value="${pasProductListForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <input id="zipcode" name="zipCode" value="${pasProductListForm.zipCode}" size="8"/>
          </div>
          <div class="quarter" >
            <label for="countryCode">Country Code:</label>
            <input id="countryCode" name="countryCode" value="${pasProductListForm.countryCode}" size="4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <h2 align="center">Available Products</h2>
      <p>Execution Time:  ${pas_getProductListTimer}</p>
      <table>
        <thead>
          <tr>
            <th>Product Id</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="productId" items="${availableProducts}">
            <tr>
              <td>${productId}</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
    
  </body>
</html>