<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
  <head>  
    <title>PAS Availability</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
  </head>

  <body bgcolor="#ffffff"  >
    <div align="center">
      <form action="pasmysql.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productId">Product Id:</label><br>
            <input id="productId" name="productId" value="${pasMySQLForm.productId}" size="8"/>
          </div>
          <div class="quarter" >
            <label for="productList">Product List:</label>
            <textarea id="productList" name="productList" cols="20" rows="2">${pasMySQLForm.productList}</textarea>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label><br>
            <input id="deliverydate" name="deliveryDate" value="${pasMySQLForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <input id="zipcode" name="zipCode" value="${pasMySQLForm.zipCode}" size="8"/>
            <br>-- OR --<br>
            <label for="countryCode">Country Code:</label>
            <input id="countryCode" name="countryCode" value="${pasMySQLForm.countryCode}" size="4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <div class="leftSide">
        <h2 align="center">Product Availability Dates</h2>
        <p>Execution Time:  ${pas_getProductDatesTimer}</p>
            <display:table name="availableDates" id="availableDates" class="altstripe">
              <display:caption>Available Dates</display:caption>
              <display:column property="deliveryDate.time" format="{0,date,MM/dd/yyyy}" title="Delivery Date"/>
              <display:column property="floristCutoffDate.time" format="{0,date,MM/dd/yyyy}" title="Florist"/>
              <display:column property="shipDateND.time" format="{0,date,MM/dd/yyyy}" title="Next Day"/>
              <display:column property="shipDate2D.time" format="{0,date,MM/dd/yyyy}" title="Two Day"/>
              <display:column property="shipDateGR.time" format="{0,date,MM/dd/yyyy}" title="Ground"/>
              <display:column property="shipDateSA.time" format="{0,date,MM/dd/yyyy}" title="Saturday"/>
            </display:table>
      </div>
      <div class="rightSide">
        <h2 align="center">Product List Availability</h2>
        <p>Execution Time:  ${pas_getProductListTimer}</p>
        <table>
          <thead>
            <tr>
              <th>Product Id</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach var="productId" items="${availableProductsList}">
              <tr>
                <td>${productId}</td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
    
  </body>
</html>