<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
  <head>  
    <title>PAS Florist</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var zipInput = document.getElementById("zipcode");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
      }
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>
    <div align="center">
      <form action="pasflorist.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label>
            <input id="deliverydate" name="deliveryDate" value="${pasFloristForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <div>
            <input id="zipcode" name="zipCode" value="${pasFloristForm.zipCode}" size="8"/>
            <select id="zipSelect" name="zipSelect" onchange="zipChange()">
              <option value="60515">60515 - IL</option>
              <option value="96766">96766 - HI</option>
              <option value="99705">99705 - AK</option>
              <option value="99701">99701 - AK</option>
              <option value="85005">85005 - AZ</option>
            </select>
            </div>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS Florist" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <h2 align="center">Available Florists</h2>
      <p>Execution Time:  ${pas_getFlorist}</p>
      <table>
        <thead>
          <tr>
            <th>floristId</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="floristId" items="${florists}">
            <tr>
              <td>${floristId}</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>

    
  </body>
</html>