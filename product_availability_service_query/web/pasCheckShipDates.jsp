<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
  <head>  
    <title>PAS Ship Dates</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function productChange()
      {
          var productSelect = document.getElementById("productIdSelect");
          var productInput = document.getElementById("productId");
          
          var productId = productSelect.options[productSelect.selectedIndex].value;
        
          productInput.value = productId;
      }
      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var zipInput = document.getElementById("zipcode");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
      }
      
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>    
    <div align="center">
      <form action="pascheckshipdates.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productid">Product Id:</label>
            <div>
            <input id="productId" name="productId" value="${pasCheckShipDatesForm.productId}" size="8"/>
            <select id="productIdSelect" name="productIdSelect" onchange="productChange()" >
              <option value="8212">8212 - Florist</option>
              <option value="F088">F088 - Vendor</option>
              <option value="F103">F103 - Both</option>
              <option value="MAC1">MAC1 - Both</option>
              <option value="DEAD1">DEAD1 - Subcode</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <div>
            <input id="zipcode" name="zipCode" value="${pasCheckShipDatesForm.zipCode}" size="8"/>
            <select id="zipSelect" name="zipSelect" onchange="zipChange()">
              <option value="60148"></option>
              <option value="60515">60515 - IL</option>
              <option value="96766">96766 - HI</option>
              <option value="99705">99705 - AK</option>
              <option value="99701">99701 - AK</option>
              <option value="85005">85005 - AZ</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="numberDays">Number of Days:</label>
            <input id="numberDays" name="numberDays" value="${pasCheckShipDatesForm.numberDays}" size="4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS getShipDates" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <h2 align="center">Ship Methods</h2>
      <p>Execution Time:  ${pas_timer}</p>
      <table>
        <thead>
          <tr>
            <th>Next Day</th>
            <th>Two Day</th>
            <th>Ground</th>
            <th>Saturday</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <ul>
                <c:forEach var="deliveryDate" items="${pasShipDateAvailVO.nextDayDeliveryDates}">
                  <li><fmt:formatDate value="${deliveryDate.time}" pattern="MM/dd/yyyy" /></li>
                </c:forEach>
              </ul>
            </td>
            <td>
              <ul>
                <c:forEach var="deliveryDate" items="${pasShipDateAvailVO.twoDayDeliveryDates}">
                  <li><fmt:formatDate value="${deliveryDate.time}" pattern="MM/dd/yyyy" /></li>
                </c:forEach>
              </ul>
            </td>
            <td>
              <ul>
                <c:forEach var="deliveryDate" items="${pasShipDateAvailVO.groundDeliveryDates}">
                  <li><fmt:formatDate value="${deliveryDate.time}" pattern="MM/dd/yyyy" /></li>
                </c:forEach>
              </ul>
            </td>
            <td>
              <ul>
                <c:forEach var="deliveryDate" items="${pasShipDateAvailVO.saturdayDeliveryDates}">
                  <li><fmt:formatDate value="${deliveryDate.time}" pattern="MM/dd/yyyy" /></li>
                </c:forEach>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    
  </body>
</html>
