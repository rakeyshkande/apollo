<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
  <head>  
    <title>PAS Most Popular</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    
    <script type="text/javascript" language="javascript">
      function zipChange()
      {
          var zipSelect = document.getElementById("zipSelect");
          var zipInput = document.getElementById("zipcode");
          
          var zipCode = zipSelect.options[zipSelect.selectedIndex].value;
        
          zipInput.value = zipCode;
      }
      
    </script>

  </head>

  <body bgcolor="#ffffff"  >
    <%@ include file="menu.jsp" %>    
    <div align="center">
      <form action="pascheckpopular.do">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productid">Company Id:</label>
            <div>
            <select id="companyId" name="companyId" >
              <option value="FTD">FTD</option>
              <option value="FTDCA">FTD.CA</option>
              <option value="FLORIST">FLORIST</option>
              <option value="ROSES">ROSES</option>
              <option value="FUSA">FUSA</option>
              <option value="HIGH">HIGH</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label>
            <input id="deliverydate" name="deliveryDate" value="${pasCheckPopularForm.deliveryDate}" size="12"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <div>
            <input id="zipcode" name="zipCode" value="${pasCheckPopularForm.zipCode}" size="8"/>
            <select id="zipSelect" name="zipSelect" onchange="zipChange()">
              <option value="60148"></option>
              <option value="60515">60515 - IL</option>
              <option value="96766">96766 - HI</option>
              <option value="99705">99705 - AK</option>
              <option value="99701">99701 - AK</option>
              <option value="85005">85005 - AZ</option>
            </select>
            </div>
          </div>
          <div class="quarter" >
            <label for="numberResults">Number of Results:</label>
            <input id="numberResults" name="numberResults" value="${pasCheckPopularForm.numberResults}" size="4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS getMostPopularProducts" />
          </div>
        </div>
      </form>
    </div>

    <div class="results">
      <h2 align="center">Most Popular Products</h2>
      <p>Execution Time:  ${pas_getMostPopular}</p>
      <table>
        <thead>
          <tr>
            <th>productId</th>
            <th>popularity</th>
            <th>floristCutoff</th>
            <th>shipDate2D</th>
            <th>shipDateGR</th>
            <th>shipDateND</th>
            <th>shipDateSA</th>
            <th>deliveryDate</th>
            <th>zipCode</th>
            <th>isAvailable</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach var="productVO" items="${productVOArray}">
            <tr>
              <td>${productVO.productId}</td>
              <td>${productVO.popularityOrderCount}</td>
              <td><fmt:formatDate value="${productVO.floristCutoffDate.time}" pattern="MM/dd/yyyy" /> ${productVO.floristCutoffTime}</td>
              <td><fmt:formatDate value="${productVO.shipDate2D.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDate2DCutoff}</td>
              <td><fmt:formatDate value="${productVO.shipDateGR.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateGRCutoff}</td>
              <td><fmt:formatDate value="${productVO.shipDateND.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateNDCutoff}</td>
              <td><fmt:formatDate value="${productVO.shipDateSA.time}" pattern="MM/dd/yyyy" /> ${productVO.shipDateSACutoff}</td>
              <td><fmt:formatDate value="${productVO.deliveryDate.time}" pattern="MM/dd/yyyy" /></td>
              <td>${productVO.zipCode}</td>
              <td>${productVO.isAvailable}</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
    
  </body>
</html>
