//Set_Cookie( 'test', 'it works', '', '/', '', '' );
function setCookie( name, value, expires, path, domain, secure )
{
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime( today.getTime() );
    
    /*
    if the expires variable is set, make the correct
    expires time, the current script below will set
    it for x number of days, to make it for hours,
    delete * 24, for minutes, delete * 60 * 24
    */
    
    if ( expires )
    {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date( today.getTime() + (expires) );
    
    document.cookie = name + "=" +escape( value ) +
    ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
    ( ( path ) ? ";path=" + path : "" ) +
    ( ( domain ) ? ";domain=" + domain : "" ) +
    ( ( secure ) ? ";secure" : "" );
}

function processMenu() {
    var form = document.forms[0];
    form.action = "menu.do";
    form.submit();
}

function processSelectService() {
    var form = document.forms[0];
    form.action = "loadClientTestPage.do";
    form.submit();
}


function showInputDiv(obj) {
    var chosenValue = obj.options[obj.selectedIndex].value;

    // Hide all divs;
    var div_getOccasion = document.getElementById("input_getOccasion");
    var div_getProductByCategory = document.getElementById("input_getProductByCategory");
    var div_getProduct = document.getElementById("input_getProduct");
    var div_getAddon = document.getElementById("input_getAddon");
    var div_getDeliveryDate = document.getElementById("input_getDeliveryDate");
    
    div_getOccasion.style.display = "none";
    div_getProductByCategory.style.display = "none";
    div_getProduct.style.display = "none";
    div_getAddon.style.display = "none";
    div_getDeliveryDate.style.display = "none";  
    
    // Show the selected div
    var showDivElem = "input_" + chosenValue;
    var div_show = document.getElementById(showDivElem);
    div_show.style.display = "block";
    
} 