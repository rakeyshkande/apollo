<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
        <td class = "Label" align = "left">Request Id:</td>
        <td class = "Instruction" align = "left">(An identifier for your request. Optional.)</td>        
        <td><html:text property = "requestId_getAddon" size = "19"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Source Code:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "sourceCode_getAddon" size = "19"/></td>
     </tr>     
     <tr>
        <td class = "Label" align = "left">Web Product Id:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "webProductIds_getAddon" size = "19"/></td>
     </tr>
     </tr>
</table>
