<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<html>
    <head>
        <title>PAS Availability</title>
        <link rel="stylesheet" type="text/css" href="css/ftd.css">
    </head>
    <body>
        <html:form action="/pasmysql" method="post">
        <table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
        <div class="top">
          <h2 align="center">QUERY</h2>
          <div class="quarter" >
            <label for="productId">Product Id:</label><br>
            <html:text property = "productId" size = "19"/>
          </div>
          <div class="quarter" >
            <label for="productList">Product List:</label>
            <html:textarea property="productList" cols="20" rows="2"/>
          </div>
          <div class="quarter" >
            <label for="deliverydate">Delivery Date:</label><br>
            <html:text property = "deliveryDate" size = "10"/>
          </div>
          <div class="quarter" >
            <label for="zipcode">Zip Code:</label>
            <html:text property = "zipCode" size = "10"/>
            <br>-- OR --<br>
            <label for="countryCode">Country Code:</label>
            <html:text property = "countryCode" size = "4"/>
          </div>
          <div class="full" align="center">
            <input type="submit" name="action" value="Query PAS" />
          </div>
        </div>
        </table>
        </html:form>

    <div class="results">
      <div class="leftSide">
        <h2 align="center">Product Availability Dates</h2>
        <p>Execution Time:  <%= request.getAttribute("pas_getProductDatesTimer") %></p>
            <display:table name="availableDates" id="availableDates" class="altstripe">
              <display:caption>Available Dates</display:caption>
              <display:column property="deliveryDate.time" format="{0,date,MM/dd/yyyy}" title="Delivery Date"/>
              <display:column property="floristCutoffDate.time" format="{0,date,MM/dd/yyyy}" title="Florist"/>
              <display:column property="shipDateND.time" format="{0,date,MM/dd/yyyy}" title="Next Day"/>
              <display:column property="shipDate2D.time" format="{0,date,MM/dd/yyyy}" title="Two Day"/>
              <display:column property="shipDateGR.time" format="{0,date,MM/dd/yyyy}" title="Ground"/>
              <display:column property="shipDateSA.time" format="{0,date,MM/dd/yyyy}" title="Saturday"/>
            </display:table>
      </div>
      <div class="rightSide">
        <h2 align="center">Product List Availability</h2>
        <p>Execution Time:  <%= request.getAttribute("pas_getProductListTimer") %></p>
        <display:table name="availableProductsList" id="availableProductsList" class="altstripe">
            <display:caption>Available Products</display:caption>
            <!--display:column property="productId" title="Product Id"/-->
        </display:table>
      </div>
    </div>
<jsp:include page="footer.jsp"/>
  </body>
</html>