<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
        <td class = "Label" align = "left">Request Id:</td>
        <td class = "Instruction" align = "left">(An identifier for your request. Optional.)</td>        
        <td><html:text property = "requestId_getDeliveryDate" size = "19"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Source Code:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "sourceCode_getDeliveryDate" size = "19"/></td>
     </tr>     
     <tr>
        <td class = "Label" align = "left">Web Product Id:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "webProductIds_getDeliveryDate" size = "19"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Zip Code:</td>
        <td class = "Instruction" align = "left">(Optional. Do not supply if country is international.)</td>        
        <td><html:text property = "zipCode_getDeliveryDate" size = "19"/></td>
     </tr>     
     <tr>
      <td class = "Label">Choose a Country: </td>
      <td class = "Instruction" align = "left">(Optional. Not required if domestic.)</td>
      <td valign = "top">        
        <html:select property = "countryId_getDeliveryDate">
          <html:option value = ""></html:option>
          <html:options collection = "countryList" property = "countryId" labelProperty = "countryName"/>
        </html:select>
      </td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Price:</td>
        <td class = "Instruction" align = "left">(Required. Numeric.)</td>        
        <td><html:text property = "priceAmt" size = "19"/></td>
     </tr> 
     <tr>
        <td class = "Label" align = "left">Customer Email:</td>
        <td class = "Instruction" align = "left">(Optional. Used to check Free Shipping eligibility)</td>        
        <td><html:text property = "customerEmail" size = "20" maxlength = "200"/></td>
     </tr>      
     <tr>
        <td class = "Label" align = "left">Customer Signed In Flag:</td>
        <td class = "Instruction" align = "left">(Optional. Used to check Free Shipping eligibility. Y or N)</td>        
        <td><html:text property = "customerSignedInFlag" size = "1"/></td>
     </tr>  
              <tr>
        <td class = "Label" align = "left">Free Shipping Membership in Cart Flag:</td>
        <td class = "Instruction" align = "left">(Optional. Used to check Free Shipping eligibility. Y or N)</td>        
        <td><html:text property = "freeShipMemberInCartFlag" size = "1"/></td>
     </tr>  
</table>
