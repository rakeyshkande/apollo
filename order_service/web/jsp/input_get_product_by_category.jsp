<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
        <td class = "Label" align = "left">Request Id:</td>
        <td class = "Instruction" align = "left">(An identifier for your request. Optional.)</td>        
        <td><html:text property = "requestId_getProductByCategory" size = "10"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Source Code:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "sourceCode_getProductByCategory" size = "10"/></td>
     </tr>     
     <tr>
        <td class = "Label" align = "left">Category Id / Product Index Name:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "categoryId" size = "10"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Delivery Date:</td>
        <td class = "Instruction" align = "left">(Required. Date format mm/dd/yyyy.)</td>        
        <td><html:text property = "deliveryDate" size = "10"/></td>
     </tr>     
     <tr>
        <td class = "Label" align = "left">Zip Code:</td>
        <td class = "Instruction" align = "left">(Optional. Do not supply if country is international.)</td>        
        <td><html:text property = "zipCode_getProductByCategory" size = "10"/></td>
     </tr>     
     <tr>
      <td class = "Label">Choose a Country: </td>
      <td class = "Instruction" align = "left">(Optional. Not required if domestic.)</td>
      <td valign = "top">        
        <html:select property = "countryId_getProductByCategory">
           <html:option value = ""></html:option>
           <html:options collection = "countryList" property = "countryId" labelProperty = "countryName"/>
        </html:select>
      </td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Order By:</td>
        <td class = "Instruction" align = "left">(Optional. Default is "popularity".)</td>        
        <td><html:select property = "productOrderBy">
                <option value="popularity">Popularity</option>
                <option value="priceAscending">Price Ascending</option>
                <option value="priceDescending">Price Descending</option>
            </html:select>
        </td>
     </tr>     
     
</table>
