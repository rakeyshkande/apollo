<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<html:html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/simulator.js"></script>
<script language="javascript"> 

function init() 
{
  showInputDiv(document.getElementById("serviceType"));
}

function processSubmit() {

    var serviceType = document.getElementById('serviceType'); 
    var serviceTypeValue = serviceType.options[serviceType.selectedIndex].value;

    var overrideRequestId = document.getElementById('requestId' + '_' + serviceTypeValue); 
    if (overrideRequestId != null && overrideRequestId.value != null && overrideRequestId.value != "")
      document.getElementById('requestId').value = overrideRequestId.value; 

    var overrideSourceCode = document.getElementById('sourceCode' + '_' + serviceTypeValue); 
    if (overrideSourceCode != null && overrideSourceCode.value != null && overrideSourceCode.value != "")
      document.getElementById('sourceCode').value = overrideSourceCode.value; 

    var overrideZipCode = document.getElementById('zipCode' + '_' + serviceTypeValue); 
    if (overrideZipCode != null && overrideZipCode.value != null && overrideZipCode.value != "")
      document.getElementById('zipCode').value = overrideZipCode.value; 
      
    var overrideProductId = document.getElementById('webProductIds' + '_' + serviceTypeValue); 
    if (overrideProductId != null && overrideProductId.value != null && overrideProductId.value != "")
      document.getElementById('webProductId').value = overrideProductId.value;       

    //Add sourceCode to cookie as markcode
    var cookieName = 'markcode';
    var cookieValue = document.getElementById('sourceCode').value;
    setCookie( cookieName, cookieValue, '', '/', '', '' );
    
    // Pass webProductId as multi value parm "id"
    var form = document.forms[0];
    var webProductIds = form.webProductId.value;
    if(webProductIds) {
    
        var idArray = new Array();
        idArray = webProductIds.split(/[\s,]+/);
        
        var el; 
        // Generates a hidden form field for each id.
        for (var i = 0; i < idArray.length; i++) { 
            el = document.createElement("input"); 
            el.type = "hidden"; 
            el.name = "id"; 
            el.value = idArray[i]; 
         
            // Optional: give each input an id so they can easily be accessed individually: 
            el.id = "hidden-idArray-" + i; 
         
            // Optional: give all inputs a class so they can easily be accessed as a group: 
            el.className = "hidden-idArray"; 
         
            form.appendChild(el); 
            
        } 
    } 
    
    // Pass categoryId as id
    var categoryId = document.getElementById('categoryId').value;
    if(categoryId) {
        var el; 
        el = document.createElement("input"); 
            el.type = "hidden"; 
            el.name = "id"; 
            el.value = categoryId; 
        form.appendChild(el);
    }
    
    // Pass priceAmt as price
    var priceAmt = document.getElementById('priceAmt').value;
    if(priceAmt) {
        var el; 
        el = document.createElement("input"); 
            el.type = "hidden"; 
            el.name = "price"; 
            el.value = priceAmt; 
        form.appendChild(el);
    }
    
    // Pass productOrderBy as sort
    var orderBy = document.getElementById('productOrderBy').value;
    if(orderBy) {
        var el; 
        el = document.createElement("input"); 
            el.type = "hidden"; 
            el.name = "sort"; 
            el.value = orderBy; 
        form.appendChild(el);
    }
    form.submit();
}
</script>

</head>
<body onload="javascript:init();" bgcolor="#006699">
<html:form action="/inputAllurent" method="post">
<html:hidden property = "clientId"/>
<input type="hidden" name="requestId" id="requestId" value=""/>
<input type="hidden" name="sourceCode" id="sourceCode" value=""/>
<input type="hidden" name="zipCode" id="zipCode" value=""/>
<input type="hidden" name="webProductId" id="webProductId" value=""/>

<table width = "98%" border = "1" cellpadding = "0" cellspacing = "2" bgcolor="#cccccc">
     <tr>
      <td class = "Label" align="right">Client: </td>
      <td valign = "top" align="left">        
        <html:text disabled="true" property = "clientId"/>
      </td>
     </tr>
     <tr>
      <td class = "Label" align="right">Choose a service: </td>
      <td valign = "top" align="left">        
        <html:select property = "serviceType" onchange="javascript:showInputDiv(this);">
          <html:option value="getOccasion">getOccasion</html:option>
          <html:option value="getProductByCategory">getProductByCategory</html:option>
          <html:option value="getProduct">getProductById</html:option>
          <html:option value="getAddon">getAddon</html:option>
          <html:option value="getDeliveryDate">getDeliveryDate</html:option>    
        </html:select>
      </td>
     </tr> 
     <tr>
        <td>&nbsp;</td>
        <td>
            <div id="input_getOccasion" style="display: block">
               <jsp:include page="input_get_occasion.jsp" />
            </div>
        </td>
     </tr>
     <tr>
        <td>&nbsp;</td>
        <td>
            <div id="input_getProductByCategory" style="display: none">
                <jsp:include page="input_get_product_by_category.jsp" />
            </div>
        </td>
     </tr>
     <tr>
        <td>&nbsp;</td>
        <td>
            <div id="input_getProduct" style="display: none">
                <jsp:include page="input_get_product.jsp" />
            </div>
        </td>
     </tr>
     <tr>
        <td>&nbsp;</td>
        <td>
            <div id="input_getAddon" style="display: none">
                <jsp:include page="input_get_addon.jsp" />
            </div>
        </td>
     </tr>
     <tr>
        <td>&nbsp;</td>
        <td>
            <div id="input_getDeliveryDate" style="display: none">
                <jsp:include page="input_get_delivery_date.jsp" />
            </div>
        </td>
     </tr>     
     <tr>
        <td align="right">
            <html:button property="btnSubmit" value="Submit" onclick="javascript:processSubmit()"/>
            <html:button property="btnMenu" value="Back" onclick="javascript:processMenu()"/>
        </td>
     </tr>

</table>
</html:form>
<jsp:include page="footer.jsp"/>
</body>
</html:html>
