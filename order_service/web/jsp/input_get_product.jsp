<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<table width = "98%" border = "0" cellpadding = "0" cellspacing = "2">
     <tr>
        <td class = "Label" align = "left">Request Id:</td>
        <td class = "Instruction" align = "left">(An identifier for your request. Optional.)</td>        
        <td><html:text property = "requestId_getProduct" size = "19"/></td>
     </tr>
     <tr>
        <td class = "Label" align = "left">Source Code:</td>
        <td class = "Instruction" align = "left">(Required.)</td>        
        <td><html:text property = "sourceCode_getProduct" size = "19"/></td>
     </tr>     
     <tr>
        <td class = "Label" align = "left">Web Product Ids:</td>
        <td class = "Instruction" align = "left">(Required. Space delimited.)</td>        
        <td><html:textarea property = "webProductIds_getProduct" rows = "6" cols = "19"/></td>
     </tr>

     <tr>
        <td class = "Label" align = "left">Load Level:</td>
        <td class = "Instruction" align = "left">(Optional. Default is "details".)</td>        
        <td><html:select property = "loadLevel">
                <option></option>
                <option value="details">Details</option>
                <option value="display">Display</option>
            </html:select>
        </td>
     </tr>         
</table>
