<%@ page import = "java.util.List"%>
<%@ page import = "java.util.ArrayList"%>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>

<html:html>
<head><title></title>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
</head>
<body bgcolor="#006699">
<html:form action="/loadClientTestPage" method="post">
<table width = "98%" border = "0" cellpadding = "0" cellspacing = "2" bgcolor="#cccccc">
     <tr>
      <td width="45%" class = "Label" align="right">Choose a Client: </td>
      <td valign = "top" align="left">        
        <html:select property = "clientId">
          <html:options collection = "clientList" property = "clientId" labelProperty = "displayName"/>
        </html:select>
      </td>
     </tr>
    
     
     <tr>
        <td align="right"><html:submit property="submit" value="Submit"/></td>
        <td>&nbsp;</td>
     </tr>
</table>
</html:form>
<jsp:include page="footer.jsp"/>
</body>
</html:html>
