<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix = "html"%>
<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix = "bean"%>
<!--bean:define id = "productForm" name = "productForm" type = "com.ftd.orderservice.web.form.ProductForm" scope = "request"/-->

<html:html>
<head><title></title></head>
<link rel="stylesheet" type="text/css" href="css/ftd.css"/>
<script type="text/javascript" src="js/simulator.js"></script>
<body>
    <table>
      <tr>
        <td>
        <bean:write name = "productForm" property = "result"/>
        </td>
      </tr>
      <tr>
        <td><html:button property="btnMenu" value="Back" onclick="javascript:processSelectService()"/></td>
        <td><html:button property="btnMenu" value="Menu" onclick="javascript:processMenu()"/></td>
      </tr>
    </table>
<jsp:include page="footer.jsp"/>
</body>
</html:html>
