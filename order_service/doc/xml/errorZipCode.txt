<!--
Finder widget - user changes zip code - AoD makes a "getCategory" request - zip code does not have delivery available
-->
<error>
    <code>noDeliveryDates</code>
    <message><![CDATA[The zip code you entered is not serviced by an FTD Florist. Please <a href="http://www.ftd.com">click here</a> for products available in this zip code.]]></message>
</error>
