<!-- Call to occasions? -->
<occasions>
    <Occasion>
        <value>ABC</value>
        <description>Birthday</description>
    </Occasion>
    <Occasion>
        <value>BDC</value>
        <description>Get Well</description>
    </Occasion>
    <!-- <Occasion> repeated as needed -->
</occasions>