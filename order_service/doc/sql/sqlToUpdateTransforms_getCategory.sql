
update ftd_apps.order_service_xsl
set XSL_TXT = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" omit-xml-declaration="yes" indent="no"/>
<xsl:template match="/com.ftd.orderservice.common.vo.response.GetProductsByCategoryResponseVO">
<xsl:element name="getCategories">
<xsl:element name="Category">
<xsl:element name="uri">
<xsl:text>arc://category/</xsl:text>
<xsl:value-of select="requestVO/categoryId"/>
</xsl:element>
<xsl:element name="title">
<xsl:value-of select="indexName"/>
</xsl:element>
<xsl:element name="childProducts">
<xsl:for-each select="productMasterList/com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO">
<xsl:element name="Product">
<xsl:element name="uri">
<xsl:text>arc://product/</xsl:text>
<xsl:choose>
<xsl:when test = "upsellMasterVO">
<xsl:value-of select="upsellMasterVO/upsellMasterId"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="webProductId"/>
</xsl:otherwise>
</xsl:choose>
</xsl:element>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>',
updated_on=sysdate,
updated_by='Defect 6414'
where XSL_CODE = 'Generic GetProductByCategory';

