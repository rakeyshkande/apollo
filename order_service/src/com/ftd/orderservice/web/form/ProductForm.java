package com.ftd.orderservice.web.form;


public class ProductForm extends BaseServiceForm
{

    private String webProductId;
    private String categoryId;
    private String deliveryDate;
    private String availDays;
    private String productOrderBy;
    private String loadLevel;
    private String priceAmt;
    private String customerEmail;
    private String customerSignedInFlag;
    private String freeShipMemberInCartFlag;

    private String requestId_getAddon;
    private String requestId_getProductByCategory;
    private String requestId_getDeliveryDate;
    private String requestId_getOccasion;
    private String requestId_getProduct;

    private String sourceCode_getAddon;
    private String sourceCode_getProductByCategory;
    private String sourceCode_getDeliveryDate;
    private String sourceCode_getOccasion;
    private String sourceCode_getProduct;

    private String zipCode_getProductByCategory;
    private String zipCode_getDeliveryDate;

    private String webProductIds_getAddon;
    private String webProductIds_getProduct;
    private String webProductIds_getDeliveryDate;
    
    private String countryId_getProductByCategory;
    private String countryId_getDeliveryDate;

    public void setWebProductId(String webProductId) {
        this.webProductId = webProductId;
    }

    public String getWebProductId() {
        return webProductId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setAvailDays(String availDays) {
        this.availDays = availDays;
    }

    public String getAvailDays() {
        return availDays;
    }

    public void setProductOrderBy(String productOrderBy) {
        this.productOrderBy = productOrderBy;
    }

    public String getProductOrderBy() {
        return productOrderBy;
    }

    public void setLoadLevel(String loadLevel) {
        this.loadLevel = loadLevel;
    }

    public String getLoadLevel() {
        return loadLevel;
    }


    public void setRequestId_getAddon(String requestId_getAddon) {
        this.requestId_getAddon = requestId_getAddon;
    }

    public String getRequestId_getAddon() {
        return requestId_getAddon;
    }

    public void setRequestId_(String requestId_getDeliveryDate) {
        this.requestId_getDeliveryDate = requestId_getDeliveryDate;
    }

    public String getRequestId_getDeliveryDate() {
        return requestId_getDeliveryDate;
    }

    public void setRequestId_getOccasion(String requestId_getOccasion) {
        this.requestId_getOccasion = requestId_getOccasion;
    }

    public String getRequestId_getOccasion() {
        return requestId_getOccasion;
    }

    public void setRequestId_getProduct(String requestId_getProduct) {
        this.requestId_getProduct = requestId_getProduct;
    }

    public String getRequestId_getProduct() {
        return requestId_getProduct;
    }

    public void setSourceCode_getAddon(String sourceCode_getAddon) {
        this.sourceCode_getAddon = sourceCode_getAddon;
    }

    public String getSourceCode_getAddon() {
        return sourceCode_getAddon;
    }

    public void setSourceCode_(String sourceCode_getDeliveryDate) {
        this.sourceCode_getDeliveryDate = sourceCode_getDeliveryDate;
    }

    public String getSourceCode_getDeliveryDate() {
        return sourceCode_getDeliveryDate;
    }

    public void setSourceCode_getOccasion(String sourceCode_getOccasion) {
        this.sourceCode_getOccasion = sourceCode_getOccasion;
    }

    public String getSourceCode_getOccasion() {
        return sourceCode_getOccasion;
    }

    public void setSourceCode_getProduct(String sourceCode_getProduct) {
        this.sourceCode_getProduct = sourceCode_getProduct;
    }

    public String getSourceCode_getProduct() {
        return sourceCode_getProduct;
    }

    public void setZipCode_getDeliveryDate(String zipCode_getDeliveryDate) {
        this.zipCode_getDeliveryDate = zipCode_getDeliveryDate;
    }

    public String getZipCode_getDeliveryDate() {
        return zipCode_getDeliveryDate;
    }


    public void setRequestId_getProductByCategory(String requestId_getProductByCategory) {
        this.requestId_getProductByCategory = requestId_getProductByCategory;
    }

    public String getRequestId_getProductByCategory() {
        return requestId_getProductByCategory;
    }

    public void setRequestId_getDeliveryDate(String requestId_getDeliveryDate) {
        this.requestId_getDeliveryDate = requestId_getDeliveryDate;
    }

    public void setSourceCode_getProductByCategory(String sourceCode_getProductByCategory) {
        this.sourceCode_getProductByCategory = sourceCode_getProductByCategory;
    }

    public String getSourceCode_getProductByCategory() {
        return sourceCode_getProductByCategory;
    }

    public void setSourceCode_getDeliveryDate(String sourceCode_getDeliveryDate) {
        this.sourceCode_getDeliveryDate = sourceCode_getDeliveryDate;
    }

    public void setZipCode_getProductByCategory(String zipCode_getProductByCategory) {
        this.zipCode_getProductByCategory = zipCode_getProductByCategory;
    }

    public String getZipCode_getProductByCategory() {
        return zipCode_getProductByCategory;
    }

    public void setPriceAmt(String priceAmt) {
        this.priceAmt = priceAmt;
    }

    public String getPriceAmt() {
        return priceAmt;
    }


    public void setWebProductIds_getAddon(String webProductIds_getAddon) {
        this.webProductIds_getAddon = webProductIds_getAddon;
    }

    public String getWebProductIds_getAddon() {
        return webProductIds_getAddon;
    }

    public void setWebProductIds_getProduct(String webProductIds_getProduct) {
        this.webProductIds_getProduct = webProductIds_getProduct;
    }

    public String getWebProductIds_getProduct() {
        return webProductIds_getProduct;
    }

    public void setWebProductIds_getDeliveryDate(String webProductIds_getDeliveryDate) {
        this.webProductIds_getDeliveryDate = webProductIds_getDeliveryDate;
    }

    public String getWebProductIds_getDeliveryDate() {
        return webProductIds_getDeliveryDate;
    }

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerSignedInFlag() {
		return customerSignedInFlag;
	}

	public void setCustomerSignedInFlag(String customerSignedInFlag) {
		this.customerSignedInFlag = customerSignedInFlag;
	}

	public String getFreeShipMemberInCartFlag() {
		return freeShipMemberInCartFlag;
	}

	public void setFreeShipMemberInCartFlag(String freeShipMemberInCartFlag) {
		this.freeShipMemberInCartFlag = freeShipMemberInCartFlag;
	}

	public void setCountryId_getProductByCategory(
			String countryId_getProductByCategory) {
		this.countryId_getProductByCategory = countryId_getProductByCategory;
	}

	public String getCountryId_getProductByCategory() {
		return countryId_getProductByCategory;
	}

	public void setCountryId_getDeliveryDate(String countryId_getDeliveryDate) {
		this.countryId_getDeliveryDate = countryId_getDeliveryDate;
	}

	public String getCountryId_getDeliveryDate() {
		return countryId_getDeliveryDate;
	}
}
