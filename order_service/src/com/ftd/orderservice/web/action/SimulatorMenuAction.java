package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * The action class is responsible for displaying a list of clients that send
 * requests to Order Service. Upon selection of the client, the input page
 * for the selected client will be displayed.
 */
public class SimulatorMenuAction extends Action
{

    private Logger logger = null;
    public SimulatorMenuAction () {
        logger = new Logger(this.getClass().getName());
    }

    /**
     * Perform a product query based on action.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {

        try {

            if(logger.isDebugEnabled())
              logger.debug("entering SimulatorMenuAction.");
   
            //OrderServiceBO orderServiceBO = new OrderServiceBO();
            CacheUtil cacheUtil = CacheUtil.getInstance();
            List clientList = cacheUtil.getClientList();

            request.setAttribute("clientList", clientList);


        } catch (Exception ce) {
            logger.error(ce);
            CommonUtil commonUtil = new CommonUtil();
            commonUtil.sendPageSystemMessage("Encountered error:" + ce.getMessage());
            return mapping.findForward("error");
        } 
        return mapping.findForward("success");
    }


}
