package com.ftd.orderservice.web.action;

import com.ftd.orderservice.web.form.ProductForm;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



/**
 * The action class is responsible for displaying the client input page.
 */
public class OrderServiceQueryAction extends Action
{

    private static Logger logger = null;
    public OrderServiceQueryAction () {
        logger = new Logger(this.getClass().getName());
    }

    /**
     * Perform a product query based on action.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {

        ProductForm productForm = (ProductForm)form;

        String clientId = productForm.getClientId();
        String serviceType = productForm.getServiceType();
        String forwardTo = serviceType + "_" + clientId;

        if(logger.isDebugEnabled())
          logger.debug("client id:" + clientId);
        return mapping.findForward(forwardTo);
        
    }


}
