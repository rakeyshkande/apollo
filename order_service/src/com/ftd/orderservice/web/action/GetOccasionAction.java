package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.bo.OrderServiceBO;
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.vo.request.GetOccasionRequestVO;

import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;

import com.ftd.orderservice.common.vo.response.GetOccasionResponseVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductIndexVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;



/**
 * The action class is responsible for serving occasion data.
 */
public class GetOccasionAction extends AbstractServiceAction
{

    private static Logger logger = null;
       
    public GetOccasionAction () {
        if(logger == null) {
            logger = new Logger(this.getClass().getName());
        }
    }

    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = super.getServiceRequest(req);
        GetOccasionRequestVO gorVO = new GetOccasionRequestVO();
        super.copyTo(osrVO, gorVO);
        
        if(gorVO.getActionType() == null) {
            gorVO.setActionType("getOccasion");
        }
        return gorVO;
    }
    
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
        super.validateRequest(requestVO);
    }
    
    protected String processRequest(OrderServiceRequestVO requestVO) throws Exception {
        OrderServiceBO serviceBO = new OrderServiceBO();
        GetOccasionRequestVO gorVO = (GetOccasionRequestVO) requestVO;
        if(this.sourceCodeValid(requestVO)){
            return serviceBO.getOccasion(gorVO);
        }
        else{
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            return this.getDefaultOccasions(gorVO);
        }
        
    }
    
    /**
     * Builds a default list of occasions.
     * @param reqVO
     * @return String
     * @throws Exception
     */
    private String getDefaultOccasions(GetOccasionRequestVO reqVO) throws Exception {

        GetOccasionResponseVO respVO = new GetOccasionResponseVO();
        respVO.setRequestVO(reqVO);
        String responseString = null;

        List<OccasionVO> ocList = new ArrayList<OccasionVO>();
        String[] indexArray = new String[]{"occasion_anniversary", "occasion_birthday", "occasion_congratulations", "occasion_getwell", "occasion_imsorry", "occasion_friendshipjustbecause", "occasion_loveromance", "occasion_newbaby", "occasion_sympathy", "occasion_thankyou"};
        String[] ocArray    = new String[]{"Anniversary","Birthday","Congratulations","Get Well","I'm Sorry", "Just Because", "Love & Romance", "New Baby", "Sympathy / Funeral", "Thank You"};
        
        for(int i = 0; i < indexArray.length; i++) {
            OccasionVO ocVO = new OccasionVO();
            ocVO.setIndexId(indexArray[i]);
            ocVO.setIndexName(ocArray[i]);
            ocList.add(ocVO);
        }
        respVO.setOccasionList(ocList);

        if(logger.isDebugEnabled())
          logger.debug("occasionList size:" + ocList.size());

        // Transform results.
        responseString = commonUtil.transform(respVO);
      
        return responseString;
    }
   
    protected boolean sourceCodeValid (OrderServiceRequestVO requestVO) throws Exception {
        String sourceCode = requestVO.getSourceCode();
        SourceMasterVO sourceObj = null;
        // If missing sourceCode, return false
        if (sourceCode == null || sourceCode.length() == 0) {
            return false;
        }
        // If sourceCode is invalid, return false
        sourceObj = cacheUtil.getSourceCodeById(sourceCode.toUpperCase());
        if(sourceObj == null) {
            return false;
        }
        return true;
    }

}
