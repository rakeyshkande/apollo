package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.dao.OrderServiceDAO;
import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * The action class is accessed by the client to determine liveness of the database
 * and the application in general. The action will return 404 if DB is down. If
 * all is well, it returns OK status 200.
 */
public class CheckPulseAction extends Action
{

    private static Logger logger = null;
    public CheckPulseAction () {
        logger = new Logger(this.getClass().getName());
    }

    /**
     * Perform a product query based on action.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {
        CommonUtil commonUtil = new CommonUtil();
        Connection conn = null; 
        PrintWriter out = response.getWriter();

        try {
             OrderServiceDAO dao = new OrderServiceDAO();
             conn = commonUtil.getNewConnection();
             String isDBUp = (String)dao.executeQueryReturnObject(conn, "IS_DB_UP", null);
             
             if(!"Y".equals(isDBUp)) {
                 commonUtil.sendPageSystemMessage("Order Service Database is down!");
                 out.print("FAILURE");
             } else {
                out.print("SUCCESS");;
             }
             
        } catch(Exception ee) {
            logger.error(ee);
            out.print("FAILURE");
        } finally {
            out.flush();
            out.close();
            if(conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    logger.error(e);
                }
            }
        }
        return null;
    }


}
