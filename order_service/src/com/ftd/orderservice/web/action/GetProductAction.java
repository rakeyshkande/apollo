package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.bo.OrderServiceBO;
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.vo.request.GetProductRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductLoadLevelEnum;
import com.ftd.osp.utilities.plugins.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 * The action class is responsible for serving product data.
 */
public class GetProductAction extends AbstractServiceAction
{

    private static Logger logger = null;
    public GetProductAction () {
        if(logger == null) {
            logger = new Logger(this.getClass().getName());
        }
    }

    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = super.getServiceRequest(req);
        GetProductRequestVO gorVO = new GetProductRequestVO();
        super.copyTo(osrVO, gorVO);
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String webProductIdParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_WEB_PRODUCT_ID + "_" + gorVO.getClientId());
        String loadLevelParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_LOAD_LEVEL + "_" + gorVO.getClientId());
        
        
        String[] webProductIds = req.getParameterValues(webProductIdParmName);
        gorVO.setWebProductIds(webProductIds);
        
        String loadLevel = req.getParameter(loadLevelParmName);
        if(logger.isDebugEnabled())
          logger.debug("getServiceRequest:loadLevel:" + loadLevel);
        if(loadLevel == null || loadLevel.length() == 0) {
            loadLevel = ProductLoadLevelEnum.toProductLoadLevelString(ProductLoadLevelEnum.DETAILS);
        }
        if(logger.isDebugEnabled())
        {
          logger.debug("getServiceRequest:loadLevel:" + loadLevel);
          logger.debug("ProductLoadLevelEnum.DETAILS.toString():" + ProductLoadLevelEnum.DETAILS.toString());
        }
        gorVO.setLoadLevel(loadLevel);
        
        if(gorVO.getActionType() == null) {
            gorVO.setActionType("getProduct");
        }
        return gorVO;
    }
    
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
        super.validateRequest(requestVO);
        super.validateGetProductRequest((GetProductRequestVO)requestVO);
        super.validateSourceCode(requestVO);
    }
    
    protected String processRequest(OrderServiceRequestVO requestVO) throws Exception {
        OrderServiceBO serviceBO = new OrderServiceBO();
        GetProductRequestVO gorVO = (GetProductRequestVO) requestVO;
        return serviceBO.getProduct(gorVO);

    }


}
