package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.bo.OrderServiceBO;
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.vo.request.GetAddonRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 * The action class is responsible for serving product data.
 */
public class GetAddonAction extends AbstractServiceAction
{

    private static Logger logger = null;
    public GetAddonAction () {
        if(logger == null) {
            logger = new Logger(this.getClass().getName());
        }
    }

    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = super.getServiceRequest(req);
        GetAddonRequestVO gorVO = new GetAddonRequestVO();
        super.copyTo(osrVO, gorVO);
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String webProductIdParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_WEB_PRODUCT_ID + "_" + gorVO.getClientId());
        
        
        String webProductId = req.getParameter(webProductIdParmName);
        gorVO.setWebProductId(webProductId);
        
        if(gorVO.getActionType() == null) {
            gorVO.setActionType("getAddon");
        }
        return gorVO;
    }
    
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
        super.validateRequest(requestVO);
        super.validateGetAddonRequest((GetAddonRequestVO)requestVO);
        super.validateSourceCode(requestVO);
    }
    
    protected String processRequest(OrderServiceRequestVO requestVO) throws Exception {
        OrderServiceBO serviceBO = new OrderServiceBO();
        GetAddonRequestVO gorVO = (GetAddonRequestVO) requestVO;
        return serviceBO.getProductAddon(gorVO);

    }


}
