package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.osp.utilities.plugins.Logger;

import com.ftd.pas.common.PASConstants;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.orderservice.web.form.PASMySQLForm;
import com.ftd.pas.web.util.Timer;

import java.io.IOException;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * Action for doing a product avaialbility check from the web.
 */
public class PASMySQLAction extends Action
{
    private static Logger logger  = new Logger("com.ftd.orderservice.web.action.PASMySQLAction");

    /**
     * Perform a product availability check and return the results.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response) throws IOException, ServletException, Exception {

        PASMySQLForm pasMySQLForm = (PASMySQLForm) form;
        if(logger.isDebugEnabled())
          logger.debug("Start ");
        
        Connection conn = null;
        try {
            CommonUtil commonUtil = new CommonUtil();
            conn = commonUtil.getNewConnection();
            String delDate = pasMySQLForm.getDeliveryDate();
            String productId = pasMySQLForm.getProductId();
            String zipCode = pasMySQLForm.getZipCode();
            String countryCode = pasMySQLForm.getCountryCode();
            String productList = pasMySQLForm.getProductList();
            if(logger.isDebugEnabled())
              logger.debug("FORM " + delDate + " " + productId + " " + zipCode + " " + countryCode);

            if (delDate != null && !delDate.equals("") &&
                ((zipCode != null && !zipCode.equals("")) || (countryCode != null && !countryCode.equals("")))) {

                PASRequestServiceImpl pas = new PASRequestServiceImpl();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

                Date deliveryDate = sdf.parse(pasMySQLForm.getDeliveryDate());
                Calendar deliveryDateCal = Calendar.getInstance();
                deliveryDateCal.setTime(deliveryDate);

                String[] returnList = null;
                

                if (productList != null && !productList.equals("")) {
                    Timer productListTimer = new Timer();
                    String[] newList = productList.split("\\s");
                    
                    if (countryCode == null || countryCode.equals("")) {
                        if(logger.isDebugEnabled())
                          logger.debug("product list");
                        returnList = pas.isProductListAvailable(conn, zipCode, deliveryDateCal, newList);
                    } else {
                        if(logger.isDebugEnabled())
                          logger.debug("international product list");
                        returnList = pas.isInternationalProductListAvailable(conn, countryCode, deliveryDateCal, newList);
                    }
                    productListTimer.stop();
                    request.setAttribute("pas_getProductListTimer", productListTimer.getMilliseconds());
                    if(logger.isDebugEnabled())
                      logger.debug("timer: " + productListTimer.getMilliseconds());
                    request.setAttribute("availableProductsList", returnList);
                    for (int i=0; i<returnList.length; i++) {
                        if(logger.isDebugEnabled())
                          logger.debug(returnList[i]);
                    }
                }
                if (productId != null && !productId.equals("")) {
                    Timer productDatesTimer = new Timer();
                    //ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    //String maxDaysString = cu.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                    String maxDaysString = commonUtil.getFrpGlobalParm(PASConstants.PAS_CONFIG_CONTEXT,PASConstants.MAX_DAYS_PARM);
                    int maxDays = 0;
                    if (maxDaysString != null) {
                        try {
                            maxDays = Integer.valueOf(maxDaysString);
                        } catch (NumberFormatException e) {
                            maxDays = 0;
                        }
                    }
                    if(logger.isDebugEnabled())
                      logger.debug("maxDays: " + maxDays);
                    
                    ProductAvailVO[] availableDates = null;
                    if (countryCode == null || countryCode.equals("")) {
                        if(logger.isDebugEnabled())
                          logger.debug("product availability");
                        availableDates = pas.getProductAvailableDates(conn, productId, zipCode, maxDays);
                    } else {
                        if(logger.isDebugEnabled())
                          logger.debug("international product availability");
                        availableDates = pas.getInternationalProductAvailableDates(conn, productId, countryCode, maxDays);
                    }
                    productDatesTimer.stop();
                    request.setAttribute("pas_getProductDatesTimer", productDatesTimer.getMilliseconds());
                    request.setAttribute("availableDates", availableDates);
                    if(logger.isDebugEnabled())
                      logger.debug("timer: " + productDatesTimer.getMilliseconds() + " " + availableDates.length);
                }
            }
        } catch (Throwable t) {
            logger.error("Error in PAS Processing",t);
        } finally {
            if(conn != null) {
                conn.close();
            }
        }

        request.setAttribute("pasMySQLForm",pasMySQLForm);
        return mapping.findForward("success");

    }
    
}
