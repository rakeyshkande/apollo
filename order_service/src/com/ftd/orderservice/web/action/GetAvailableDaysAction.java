package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.bo.OrderServiceBO;
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.vo.request.GetAvailableDaysRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.plugins.Logger;
  
import javax.servlet.http.HttpServletRequest;


/**
 * The action class is responsible for displaying the client input page.
 */
public class GetAvailableDaysAction extends AbstractServiceAction
{

    private static Logger logger = null;
    public GetAvailableDaysAction () {
        if(logger == null) {
            logger = new Logger(this.getClass().getName());
        }
    }

    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = super.getServiceRequest(req);
        GetAvailableDaysRequestVO gadVO = new GetAvailableDaysRequestVO();
        super.copyTo(osrVO, gadVO);
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String webProductIdParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_WEB_PRODUCT_ID + "_" + gadVO.getClientId());
        String zipCodeParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_ZIP_CODE + "_" + gadVO.getClientId());
        String countryParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_COUNTRY + "_" + gadVO.getClientId());
        String priceAmtParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_PRODUCT_PRICE + "_" + gadVO.getClientId());
        String customerEmailParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                OrderServiceConstants.REQUEST_PARM_CUSTOMER_EMAIL  + "_" + gadVO.getClientId());
        String customerSignedInFlagParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                OrderServiceConstants.REQUEST_PARM_CUSTOMER_SIGNED_IN_FLAG  + "_" + gadVO.getClientId());
        String freeShipMemberInCartFlagParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                OrderServiceConstants.REQUEST_PARM_FREE_SHIP_MEMBER_IN_CART_FLAG  + "_" + gadVO.getClientId());
 
        String webProductId = req.getParameter(webProductIdParmName);
        String zipCode = req.getParameter(zipCodeParmName);
        String country = req.getParameter(countryParmName);        
        String priceAmt = req.getParameter(priceAmtParmName);
        String customerEmail = req.getParameter(customerEmailParmName);
        String customerSignedInFlag = req.getParameter(customerSignedInFlagParmName);
        String freeShipMemberInCartFlag = req.getParameter(freeShipMemberInCartFlagParmName);

        gadVO.setWebProductId(webProductId);
        gadVO.setZipCode(zipCode);
        gadVO.setCountryId(country);
        gadVO.setPriceSelectedStr(priceAmt);
        gadVO.setCustomerEmail(customerEmail);
        gadVO.setCustomerSignedInFlag(customerSignedInFlag);
        gadVO.setFreeShipMemberInCartFlag(freeShipMemberInCartFlag);
        if(gadVO.getActionType() == null) {
            gadVO.setActionType("getDeliveryDate");
        }
        return gadVO;
    }
    
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
        super.validateRequest(requestVO);
        super.validateGetAvailableDaysRequest((GetAvailableDaysRequestVO)requestVO);
        super.validateSourceCode(requestVO);
    }
    
    protected String processRequest(OrderServiceRequestVO requestVO) throws Exception {
        OrderServiceBO serviceBO = new OrderServiceBO();
        GetAvailableDaysRequestVO gadVO = (GetAvailableDaysRequestVO) requestVO;
        return serviceBO.getAvailableDays(gadVO);

    }
    
    protected OrderServiceRequestVO validateZipCountry(OrderServiceRequestVO reqVO) throws Exception {
        // Zip code max length CA=6, US=9, INT=12
        String isValid = "Y";
        String errorCode = null;
        String errorFieldValue = null;
        boolean isValidZip = false;
        
        GetAvailableDaysRequestVO getAvailReqVO = (GetAvailableDaysRequestVO)reqVO;
            
        String countryId = getAvailReqVO.getCountryId();
        String zipCode = getAvailReqVO.getZipCode();
        
        
        //zip code is required if country is US or CA or blank;
        if( ((zipCode == null || zipCode.length() == 0) && (countryId == null || countryId.length() == 0 || countryId.equals("US") || countryId.equals("CA")))) {
            isValid = "N";
            errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
        }
        // If zip code is longer than 12 digits it's too long.
        else if (zipCode != null && zipCode.length() > 12) {
            isValid = "N";
            errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
            errorFieldValue = zipCode;
        } 
        // If country is null, treat zipCode as that of US or CA
        else if ((countryId == null || countryId.length() == 0 || 
            countryId.equals("US") || countryId.equals("CA")) && zipCode != null) {
            // If zipcode first 5 is valid, country is US
            String z = null;
            String c = null;
            if (zipCode.length() >= 5) {
                z = zipCode.substring(0, 5);
                
                if (cacheUtil.zipCodeExists(z)) {
                    isValidZip = true;
                    c = "US";
                }
            }
            // If zipcode first 3 is valid, country is CA
            if (!isValidZip) {
                z = zipCode.substring(0, 3);
                
                if (cacheUtil.zipCodeExists(z)) {
                    isValidZip = true;
                    c = "CA";
                }
            }

            if (isValidZip) {
                zipCode = z;
                countryId = c;
                getAvailReqVO.setZipCode(z);
                getAvailReqVO.setCountryId(countryId);
            }
            // Otherwise, zip code is invalid.
            if (!isValidZip) {
                isValid = "N";
                errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
                errorFieldValue = zipCode;
            }
        } else {
            // validate country
            CountryMasterVO country = countryId==null || "".equals(countryId) ? null : cacheUtil.getCountryById(countryId);
            if (country == null) {
                isValid = "N";
                errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_COUNTRY_ID;
                errorFieldValue = countryId;
            } 
        }
        
        if("N".equals(isValid)) {
            getAvailReqVO.setIsValid("N");
            getAvailReqVO.setErrorCode(errorCode);
            getAvailReqVO.setErrorFieldValue(errorFieldValue);
            getAvailReqVO.setZipCodeFormatted(zipCode);
            getAvailReqVO.setCountryIdFormatted(countryId);
        }
        
        return getAvailReqVO;
    }
    


}
