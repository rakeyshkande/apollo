package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.bo.OrderServiceBO;
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.vo.request.GetAvailableDaysRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductsByCategoryRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;


/**
 * The action class is responsible for serving request product by category data.
 */
public class GetProductsByCategoryAction extends AbstractServiceAction
{

    private static Logger logger = null;
    public GetProductsByCategoryAction () {
        if(logger == null) {
            logger = new Logger(this.getClass().getName());
        }
    }

    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = super.getServiceRequest(req);
        GetProductsByCategoryRequestVO gpcVO = new GetProductsByCategoryRequestVO();
        super.copyTo(osrVO, gpcVO);
        
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String categoryIdParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_CATEGORY_ID + "_" + gpcVO.getClientId());
        String deliveryDateStrParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_DELIVERY_DATE + "_" + gpcVO.getClientId());
        String zipCodeParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_ZIP_CODE + "_" + gpcVO.getClientId());
        String countryParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_COUNTRY + "_" + gpcVO.getClientId());
        String orderByParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_PRODUCT_ORDER_BY + "_" + gpcVO.getClientId());
        
        String categoryId = req.getParameter(categoryIdParmName);
        String deliveryDateStr = req.getParameter(deliveryDateStrParmName);
        String zipCode = req.getParameter(zipCodeParmName);
        String country = req.getParameter(countryParmName);
        String orderByStr = req.getParameter(orderByParmName);
        gpcVO.setCategoryId(categoryId);
        gpcVO.setDeliveryDateStr(deliveryDateStr);
        gpcVO.setZipCode(zipCode);
        gpcVO.setCountryId(country);
        gpcVO.setProductOrderByStr(orderByStr);
        
        if(gpcVO.getActionType() == null) {
            gpcVO.setActionType("getProductByCategory");
        }
        
        return gpcVO;
    }
    
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
        super.validateRequest(requestVO);
        super.validateGetProductsByCategoryRequest((GetProductsByCategoryRequestVO)requestVO);
        super.validateSourceCode(requestVO);
    }
    
    protected String processRequest(OrderServiceRequestVO requestVO) throws Exception {
        OrderServiceBO serviceBO = new OrderServiceBO();
        GetProductsByCategoryRequestVO gorVO = (GetProductsByCategoryRequestVO) requestVO;
        return serviceBO.getProductsByCategory(gorVO);

    }

    protected OrderServiceRequestVO validateZipCountry(OrderServiceRequestVO reqVO) throws Exception {
        // Zip code max length CA=6, US=9, INT=12
        String isValid = "Y";
        String errorCode = null;
        String errorFieldValue = null;
        boolean isValidZip = false;
       
        GetProductsByCategoryRequestVO getCategoryReqVO = (GetProductsByCategoryRequestVO)reqVO;
            
        String countryId = getCategoryReqVO.getCountryId();
        String zipCode = getCategoryReqVO.getZipCode();
        String deliveryDate = getCategoryReqVO.getDeliveryDateStr();

        //zip code is required if country is US or CA or blank; zipCode or country required if  delivery date is not null
        if( ((zipCode == null || zipCode.length() == 0) && ("US".equals(countryId) || "CA".equals(countryId))) ||
            ((zipCode == null || zipCode.length() == 0) && (countryId == null || countryId.length() == 0) && deliveryDate!=null)) {
            isValid = "N";
            errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
            errorFieldValue = zipCode;
        }
        // If zip code is longer than 12 digits it's too long.
        else if (zipCode != null && zipCode.length() > 12) {
            isValid = "N";
            errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
            errorFieldValue = zipCode;
        } 
        // If country is null, treat zipCode as that of US or CA
        else if ((countryId == null || countryId.length() == 0 || 
            countryId.equals("US") || countryId.equals("CA")) && (zipCode != null && zipCode.length()!=0)) {
            // If zipcode first 5 is valid, country is US
            String z = null;
            String c = null;
            if (zipCode.length() >= 5) {
                z = zipCode.substring(0, 5);
                
                if (cacheUtil.zipCodeExists(z)) {
                    isValidZip = true;
                    c = "US";
                }
            }
            // If zipcode first 3 is valid, country is CA
            if (!isValidZip) {
                z = zipCode.substring(0, 3);
                
                if (cacheUtil.zipCodeExists(z)) {
                    isValidZip = true;
                    c = "CA";
                }
            }

            if (isValidZip) {
                zipCode = z;
                countryId = c;
            }
            // Otherwise, zip code is invalid.
            if (!isValidZip) {
                isValid = "N";
                errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_ZIP_CODE;
                errorFieldValue = zipCode;
            }
        } else {
            // validate country
            if (countryId!=null) {
                CountryMasterVO country = cacheUtil.getCountryById(countryId);
                if (country == null) {
                    isValid = "N";
                    errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_COUNTRY_ID;
                    errorFieldValue = countryId;
                } 
            } else if (deliveryDate!=null) {
                isValid = "N";
                errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_COUNTRY_ID;
                errorFieldValue = countryId;
            }
        }
        
        if("N".equals(isValid)) {
            getCategoryReqVO.setIsValid("N");
            getCategoryReqVO.setErrorCode(errorCode);
            getCategoryReqVO.setErrorFieldValue(errorFieldValue);
            getCategoryReqVO.setZipCodeFormatted(zipCode);
            getCategoryReqVO.setCountryIdFormatted(countryId);
        }
        return getCategoryReqVO;
    }

}
