package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.dao.OrderServiceDAO;
import com.ftd.orderservice.common.exception.InvalidDateException;
import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.orderservice.common.util.Timer;
import com.ftd.orderservice.common.vo.request.GetAddonRequestVO;
import com.ftd.orderservice.common.vo.request.GetAvailableDaysRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductsByCategoryRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductLoadLevelEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductOrderByEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ClientVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductIndexVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellMasterVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;
import java.io.PrintWriter;

import java.math.BigDecimal;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public abstract class AbstractServiceAction extends Action
{
    protected static Logger logger = new Logger("com.ftd.orderservice.web.action.AbstractServiceAction");
    protected CommonUtil commonUtil = new CommonUtil();
    protected OrderServiceDAO osDAO = new OrderServiceDAO();
    protected CacheUtil cacheUtil = CacheUtil.getInstance();
    protected static String underscore = "_";
    protected String DEFAULT_INDEX_NAME = "product_flowers_bestsellers";
    
    public AbstractServiceAction()
    {        
    }

    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {
        Timer requestTimer = new Timer();
        
        String responseString = null;
        
        PrintWriter out = response.getWriter();
        
        
        OrderServiceRequestVO requestVO = getServiceRequest(request);
        requestVO.setRequestStatus(OrderServiceConstants.REQUEST_SUCCESS_STATUS);

        try {
            validateRequest(requestVO);
      
            if(logger.isDebugEnabled())
              logger.debug("isValid?" + requestVO.getIsValid());
            if(requestVO.getIsValid().equals("N")) {
                //if client id is missing or invalid, no response is given, Allurent will time out - no page
                if ( OrderServiceConstants.VALIDATION_ERROR_MISSING_CLIENT_ID.equalsIgnoreCase(requestVO.getErrorCode()) ||
                    OrderServiceConstants.VALIDATION_ERROR_INVALID_CLIENT_ID.equalsIgnoreCase(requestVO.getErrorCode()) ){
                        //do nothing
                         requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
                }
                else{
                    // Tranform error to client format.
                    responseString = commonUtil.transformError(requestVO);   
                }
            } else {
                // No error. Process request.
                responseString = processRequest(requestVO);

            }
            
            if(!responseString.equalsIgnoreCase("null")){
                out.println(responseString);
            }
                
        }catch (Exception e) {
            String msgString = "Failed to fulfill Order Service request for input: \n" + requestVO.toLogString() + " \n Exception: " + e.getMessage();
            logger.error(msgString);
            logger.error(e);
            
            // Send system message.
            CommonUtil commonUtil = new CommonUtil();
            commonUtil.sendNoPageSystemMessage(msgString);
            
            // Transform error and send generic error.
            requestVO.setErrorMessage(e.getMessage());
            requestVO.setErrorFieldValue(requestVO.getSourceCode());
            if (OrderServiceConstants.VALIDATION_ERROR_MISSING_CLIENT_ID.equalsIgnoreCase(requestVO.getErrorCode()) ||
                OrderServiceConstants.VALIDATION_ERROR_INVALID_CLIENT_ID.equalsIgnoreCase(requestVO.getErrorCode()) ){
                     requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            }
            else{
                requestVO.setRequestStatus(OrderServiceConstants.REQUEST_FAILURE_STATUS);
            }
            requestVO.setErrorCode(OrderServiceConstants.GENERIC_ERROR_CANNOT_FULFILL_REQUEST);
            responseString = commonUtil.transformError(requestVO);
            
            if(out != null) {
                out.println(responseString);
            }
        } catch (Throwable t) {
            String msgString = "Failed to fulfill Order Service request for input: \n" + requestVO.toLogString() + " \n Exception: " + t.getMessage();
            logger.error(msgString);
            logger.error(t);
            
            // Send system message.
            CommonUtil commonUtil = new CommonUtil();
            commonUtil.sendNoPageSystemMessage(msgString);
            
            // Transform error and send generic error.
            requestVO.setErrorCode(OrderServiceConstants.GENERIC_ERROR_CANNOT_FULFILL_REQUEST);
            requestVO.setErrorMessage(t.getMessage());
            requestVO.setErrorFieldValue(requestVO.getSourceCode());
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_FAILURE_STATUS);
            responseString = commonUtil.transformError(requestVO);
            
            if(out != null) {
                out.println(responseString);
            }
        } finally {
            if(logger.isDebugEnabled())
              logger.debug(responseString);
            requestTimer.stop();
            if(logger.isInfoEnabled())
              logger.info("Time taken to process request " + requestVO.getActionType() + " with requestId " + requestVO.getRequestId() + " is=" + requestTimer.getMilliseconds());
            
            out.flush();
            out.close();
        }

        return null;
    }
    
    /**
     * Retrieves data fields common to all order service requests.
     * @param req
     * @return
     * @throws Exception
     */
    protected OrderServiceRequestVO getServiceRequest(HttpServletRequest req) throws Exception {
        OrderServiceRequestVO osrVO = new OrderServiceRequestVO();
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        
        // Parameter name common to all partners
        String clientId = req.getParameter(OrderServiceConstants.REQUEST_PARM_CLIENT_ID);
        String actionType = req.getParameter(OrderServiceConstants.REQUEST_PARM_ACTION_TYPE);
        
        // Parameter name by partner
        String sourceCodeParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_SOURCE_CODE + "_" + clientId);
        String requestIdParmName = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, 
                            OrderServiceConstants.REQUEST_PARM_REQUEST_ID + "_" + clientId);
        
        String sourceCode = req.getParameter(sourceCodeParmName);
        if(sourceCode == null) {
            // get source code from cookie
            sourceCode = this.getCookieValue(req, sourceCodeParmName);
        }
        
        osrVO.setClientId(clientId);
        osrVO.setSourceCode(sourceCode);
        osrVO.setActionType(actionType);
        String requestId = req.getParameter(requestIdParmName);
        
       
        // Generate a random requestId if an request id is not passed.
        if(requestId == null || requestId.length() == 0) {
            requestId = commonUtil.generateRequestId();
            osrVO.setCustomRequestId(true);
        }
        osrVO.setRequestId(requestId);
        
        return osrVO;
    }
    
    protected void copyTo(OrderServiceRequestVO from, OrderServiceRequestVO to) throws Exception {
        to.setClientId(from.getClientId());
        to.setRequestId(from.getRequestId());
        to.setSourceCode(from.getSourceCode());
        to.setCustomRequestId(from.isCustomRequestId());
        to.setActionType(from.getActionType());
    }
    
    /**
     * Validates fields common to all requests.
     * @param requestVO
     * @return
     * @throws Exception
     */
    protected void validateRequest(OrderServiceRequestVO requestVO) throws Exception {
    
        String clientId = requestVO.getClientId();
        // If missing clientId, set isValid to false, set errorMessage to VALIDATION_ERROR_MISSING_CLIENT_ID
        if(clientId == null || clientId.length() == 0) {
             requestVO.setIsValid("N");
             requestVO.setErrorCode(OrderServiceConstants.VALIDATION_ERROR_MISSING_CLIENT_ID);
             requestVO.setErrorFieldValue("");
             return;
        }
        // If clientId is invalid, set isValid to false, set errorMessage to VALIDATION_ERROR_INVALID_CLIENT_ID
        ClientVO clientObj = cacheUtil.getClientById(clientId);
        if(clientObj == null) {
             requestVO.setIsValid("N");
             requestVO.setErrorCode(OrderServiceConstants.VALIDATION_ERROR_INVALID_CLIENT_ID);
             requestVO.setErrorFieldValue(clientId);
             return;
        }
  }
    
    /**
     * @param reqVO
     * @return
     * @throws Exception
     */
    protected OrderServiceRequestVO validateZipCountry(OrderServiceRequestVO reqVO) throws Exception {
      return reqVO;
    }

    
    protected abstract String processRequest(OrderServiceRequestVO requestVO) throws Exception;

    protected void logStartTime() throws Exception
    {
        if(logger.isDebugEnabled())
          logger.debug("Start request: " + commonUtil.now());
    }
    
    protected void logEndTime() throws Exception
    {
        if(logger.isDebugEnabled())
          logger.debug("End request: " + commonUtil.now());
    }    

    public static String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();
        if(cookies != null) {
            for(int i=0; i<cookies.length; i++) {
                Cookie cookie = cookies[i];
                
                if(logger.isDebugEnabled())
                {
                  logger.debug("cookie name:" + cookie.getName());
                  logger.debug("cookie value:" + cookie.getValue());
                }
                if (cookieName.equals(cookie.getName()))
                    return(cookie.getValue());
            }
        }
        return null;
    }

    /**
     * Web product ids are required. The number passed cannot exceed the max configured value.
     * @param requestVO
     * @throws Exception
     */
    protected void validateGetProductRequest(GetProductRequestVO requestVO) throws Exception {
        String[] webProductIds = requestVO.getWebProductIds();
        if(webProductIds == null || webProductIds.length == 0) {
            webProductIds = new String[1];
            webProductIds[0] = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
            requestVO.setWebProductIds(webProductIds);
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        if(webProductIds != null && webProductIds.length == 1) {
            if (webProductIds[0].equalsIgnoreCase("")){
             webProductIds[0] = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
             requestVO.setWebProductIds(webProductIds);
             requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            }
            else if(!validateProductId(webProductIds[0])) {
                webProductIds[0] = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
                requestVO.setWebProductIds(webProductIds);
                requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            }
        }
        int maxProductsInt = 0;
        String maxProducts = commonUtil.getFrpGlobalParm(OrderServiceConstants.ORDER_SERVICE_CONFIG, OrderServiceConstants.GET_PRODUCT_MAX_COUNT);
        if(maxProducts == null) {
            maxProductsInt = 10;
            commonUtil.sendPageSystemMessage("Global parm GET_PRODUCT_MAX_COUNT::GET_PRODUCT_MAX_COUNT is not set up. Default of 10 is in effect.");
        } else {
            try {
                maxProductsInt = Integer.parseInt(maxProducts);
            } catch (Exception x) {
                logger.error(x);
                commonUtil.sendPageSystemMessage("Global parm GET_PRODUCT_MAX_COUNT::GET_PRODUCT_MAX_COUNT is not set up correctly. Default of 10 is in effect. \n" + x.getMessage());
            }
        }
        
        if(webProductIds.length > maxProductsInt) {
            //remove product ids > than product max count
             String[] maxWebProductIds = new String[maxProductsInt];
             for (int i = 0; i < maxProductsInt; i++) {
                 maxWebProductIds[i] = webProductIds[i];
             }
             requestVO.setWebProductIds(maxWebProductIds);
             requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        
        // Validate product load level
        String loadLevel = requestVO.getLoadLevel();
        if(logger.isDebugEnabled())
          logger.debug("validateGetProductRequest loadLevel is:" + loadLevel);
        if(!ProductLoadLevelEnum.isValidLoadLevel(loadLevel)) {
            //set details as default
            requestVO.setLoadLevel(ProductLoadLevelEnum.toProductLoadLevelString(ProductLoadLevelEnum.DETAILS));
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
    }
    
    /**
     * Validate input for get products by category id.
     * @param requestVO
     * @throws Exception
     */
    protected void validateGetProductsByCategoryRequest(GetProductsByCategoryRequestVO requestVO) throws Exception {
        // validate zip code and country - zip code must exist if country is null or US or Canada
        requestVO = (GetProductsByCategoryRequestVO)this.validateZipCountry(requestVO);
        if(requestVO.getIsValid().equals("N")) {
            return;
        }

        try{
            if (requestVO.getDeliveryDateStr()!=null && requestVO.getDeliveryDateStr().length()>0) {
              logger.debug("delivery date string is: " + requestVO.getDeliveryDateStr());
              String error = validateDeliveryDate(requestVO.getDeliveryDateStr());
              logger.debug("delivery date error is: " + error);
              if(error != null) {
                  throw new InvalidDateException(error);
              } else {
                  SimpleDateFormat sdf = new SimpleDateFormat(OrderServiceConstants.DATE_FORMAT_DD);
                  Date dd = sdf.parse(requestVO.getDeliveryDateStr());
                  requestVO.setDeliveryDate(dd);
              }    
            } else if ((requestVO.getZipCode()!=null && requestVO.getZipCode().length()>0) || (requestVO.getCountryId()!=null && requestVO.getCountryId().length()>0)) {
            //delivery date is required if zip code country populated
                throw new InvalidDateException(null);
            }
        } catch(InvalidDateException ide) {
        	logger.debug("Caught InvalidDateException. Changing date to first available date.");
            // date is not in the expected format.  Default to today; default to tomorrow if after 2pm
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            if(cal.get(Calendar.HOUR_OF_DAY) >= 14)
            {
             cal.add(Calendar.DATE, 1);
            }  
            SimpleDateFormat sdf = new SimpleDateFormat(OrderServiceConstants.DATE_FORMAT_DD);
            String dateString = sdf.format(cal.getTime());
            Date defaultDate = sdf.parse(dateString);
            requestVO.setDeliveryDate(defaultDate);
            requestVO.setDeliveryDateStr(dateString);
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        // validate product order by - valid values: priority, priceAscending, priceDescending
        String orderByStr = requestVO.getProductOrderByStr();
        boolean isValid = validateProductOrderBy(orderByStr);
        
        if(!isValid) {
            requestVO.setProductOrderBy(ProductOrderByEnum.POPULARITY);
            logger.warn("Product order by is not valid:" + orderByStr);
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        } else {
            requestVO.setProductOrderBy(ProductOrderByEnum.toProductOrderBy(orderByStr));
        }
        
    }
    
    protected boolean validateCategoryId (String sourceCode, String categoryId) throws Exception {
        Connection conn = null;
        boolean valid = true;
        
        try {
            conn = commonUtil.getNewConnection();
            String defaultDomain = commonUtil.getCompanyDefaultDomain(sourceCode);
            ProductIndexVO pivo = osDAO.getIndexByIdAndSource(conn, sourceCode, categoryId, defaultDomain);
            if(pivo == null) {
                valid = false;
            }
        } finally {
            if(conn != null) {
                conn.close();
            }
        }
        return valid;
    }
    
    protected String validateDeliveryDate(String deliveryDateStr) throws Exception {
        Date dd = null;
        String errorCode = null;
        SimpleDateFormat sdf = new SimpleDateFormat(OrderServiceConstants.DATE_FORMAT_DD);
        String datePattern = "[0|1]\\d/[0-3]\\d/\\d{4}";
        
        try {
        	if(!deliveryDateStr.matches(datePattern)){
        		throw new Exception("Invalidate date:" + deliveryDateStr);
        	}
            dd = sdf.parse(deliveryDateStr);

        } catch (Exception e) {
            // date is not in the expected format.
        	logger.error(e);
            errorCode = OrderServiceConstants.VALIDATION_ERROR_INVALID_DELIVERY_DATE_FORMAT;
            
        }
        return errorCode;
    }
    
    protected boolean validateProductOrderBy(String productOrderBy) throws Exception {
        String orderByStr = productOrderBy;
        if(orderByStr == null) {
            orderByStr = ProductOrderByEnum.toProductOrderByStr(ProductOrderByEnum.POPULARITY);
        } 
        if(ProductOrderByEnum.isValidProductOrderBy(productOrderBy)) {
            return true;
        }
        return false;
    }
    
    protected void validateGetAddonRequest(GetAddonRequestVO requestVO) throws Exception {
        String webProductId = requestVO.getWebProductId();
        String errorCode = null;
        if(webProductId == null || webProductId.length() == 0) {
            webProductId = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
            requestVO.setWebProductId(webProductId);
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        else if(webProductId != null && webProductId.equalsIgnoreCase("")){
             webProductId  = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
             requestVO.setWebProductId(webProductId );
             requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        else if(!validateProductId(webProductId)) {
            webProductId  = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
            requestVO.setWebProductId(webProductId );
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        
        if(errorCode != null) {
            requestVO.setIsValid("N");
            requestVO.setErrorCode(errorCode);
        } 

    }
    
    protected boolean validateProductId (String webProductId) throws Exception {
        CacheUtil cacheUtil = CacheUtil.getInstance();
        ProductMasterVO objProd = cacheUtil.getProductByWebProductId(webProductId);
        UpsellMasterVO objUpsell = null;
        if(objProd == null) {
            objUpsell = cacheUtil.getUpsellMasterByMasterId(webProductId);
        }
        if(objProd != null || objUpsell != null) {
            return true;
        }
        return false;
    }

    protected void validateGetAvailableDaysRequest (GetAvailableDaysRequestVO requestVO) throws Exception {
        String webProductId = requestVO.getWebProductId();
        if(webProductId == null || webProductId.length() == 0) {
            webProductId = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
            requestVO.setWebProductId(webProductId);
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        else if(webProductId != null && webProductId.equalsIgnoreCase("")){
             webProductId  = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
             requestVO.setWebProductId(webProductId );
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        if(!validateProductId(webProductId)) {
             webProductId  = this.retrieveDefaultProductId(requestVO.getSourceCode(), DEFAULT_INDEX_NAME);
             requestVO.setWebProductId(webProductId );
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }     
        
        boolean valid = validatePrice(requestVO);

        if(valid) {
            validateZipCountry(requestVO);
        }        
        
        if(!validateEmail(requestVO.getCustomerEmail())) {
        	requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        	requestVO.setErrorCode(OrderServiceConstants.VALIDATION_ERROR_INVALID_CUSTOMER_EMAIL);
        	requestVO.setCustomerEmail(null);
        } else if(!validateFlag(requestVO.getCustomerSignedInFlag())) {
        	requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        	requestVO.setErrorCode(OrderServiceConstants.VALIDATION_ERROR_INVALID_CUSTOMER_SIGNED_IN_FLAG);
        	requestVO.setCustomerSignedInFlag("N");
        } else if(!validateFlag(requestVO.getFreeShipMemberInCartFlag())) {
        	requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        	requestVO.setErrorCode(OrderServiceConstants.VALIDATION_ERROR_INVALID_FREE_SHIP_MEMBER_IN_CART_FLAG);
        	requestVO.setFreeShipMemberInCartFlag("N");
        }
    }

    protected boolean validatePrice (GetAvailableDaysRequestVO requestVO) throws Exception {
        String priceAmtStr = requestVO.getPriceSelectedStr();
        BigDecimal priceAmt = null;
        String errorCode = null;
        if(priceAmtStr == null || priceAmtStr.length() == 0) {
            ProductMasterVO pmvo = cacheUtil.getProductOrBaseProductByWebProductId(requestVO.getWebProductId());
            if(pmvo == null) {
                priceAmt = new BigDecimal(0);
            } else {
                priceAmt = pmvo.getStandardPrice();
            }
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
        }
        else {
            try {
                priceAmt = new BigDecimal(priceAmtStr);
            } catch (Exception e) {
                logger.error(e);
                ProductMasterVO pmvo = null;
                pmvo = cacheUtil.getProductOrBaseProductByWebProductId(requestVO.getWebProductId());
                if(pmvo == null) {
                    priceAmt = new BigDecimal(0);
                } else {
                    priceAmt = pmvo.getStandardPrice();
                    if(priceAmt == null) {
                        priceAmt = new BigDecimal(0);
                    }
                }
                requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            }
            
        }
        
        if(errorCode != null) {
            requestVO.setIsValid("N");
            requestVO.setErrorCode(errorCode);
        } else {
            requestVO.setPriceSelected(priceAmt);
        }
        if(logger.isDebugEnabled())
          logger.debug("errorcode:" + errorCode);
        return (errorCode == null);
    }
    
    protected void validateSourceCode (OrderServiceRequestVO requestVO) throws Exception {
        String sourceCode = requestVO.getSourceCode();
        SourceMasterVO sourceObj = null;
        // If missing sourceCode, return results back using 350 as the source code
        if (sourceCode == null || sourceCode.length() == 0) {
            requestVO.setSourceCode("350");
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            return;
        }
        // If sourceCode is invalid, return results back using 350 as the source code
        sourceObj = cacheUtil.getSourceCodeById(sourceCode.toUpperCase());
        if(sourceObj == null) {
            //return results back using 350 as the source code
            requestVO.setSourceCode("350");
            requestVO.setRequestStatus(OrderServiceConstants.REQUEST_PARTIAL_SUCCESS_STATUS);
            return;
        }
    }
    
    /**
     * Return the first product defined in the product_flowers_bestsellers index.  If nothing returned for that source code
     * look to domain
     * @param sourceCode
     * @param categoryId
     * @return String
     * @throws Exception
     */
    protected String retrieveDefaultProductId (String sourceCode, String categoryId) throws Exception {
        //return the first product defined in the product_flowers_bestsellers index.  If nothing returned for that source code
        //look to domain
        String defaultProductId = null;
        Connection conn = null;
        
        try {
            conn = commonUtil.getNewConnection();
            String defaultDomain = commonUtil.getCompanyDefaultDomain(sourceCode);
            SourceProductUtility spUtil = new SourceProductUtility();
            List<String> childProducts = spUtil.getIndexProductList(conn, sourceCode, categoryId, defaultDomain);
            if(logger.isDebugEnabled())
              logger.debug("getDistinctProductIdForIndex:" + sourceCode + "; " + categoryId); 
            if(childProducts != null) {
                if(logger.isDebugEnabled())
                  logger.debug("child product:" + childProducts.get(0));
                defaultProductId = childProducts.get(0);
                
                //Use product id to find web product id. If not found return as is.
                String webProductId = cacheUtil.getWebProductIdByProductId(defaultProductId);
                if(webProductId != null && webProductId.length() > 0) {
                    defaultProductId = webProductId;
                }
            }
            
        } finally {
            if(conn != null) {
                conn.close();
            }
        }
        return defaultProductId;
    }
    
    protected boolean validateEmail(String emailAddress) throws Exception {
        if(emailAddress != null && emailAddress.length() > 0 && !emailAddress.contains("@")) {
        	return false;
        }
        return true;
    }
    
    protected boolean validateFlag(String flag) throws Exception {
        if(flag != null && flag.length() > 0) {
        	if(!flag.equals("Y") && !flag.equals("N"))
        	{
        		return false;
        	}
        }
        return true;
    } 

}
