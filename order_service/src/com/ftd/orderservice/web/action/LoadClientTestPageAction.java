package com.ftd.orderservice.web.action;

import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.orderservice.web.form.ProductForm;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * The action class is responsible for displaying the client input page.
 */
public class LoadClientTestPageAction extends Action
{

    private static Logger logger = null;
    public LoadClientTestPageAction () {
        logger = new Logger(this.getClass().getName());
    }

    /**
     * Perform a product query based on action.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws Exception
     */
    public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                HttpServletResponse response)
          throws IOException, ServletException, Exception
    {

        ProductForm productForm = (ProductForm)form;
        //logger.info("entering LoadClientTestPageAction.");
        String clientId = productForm.getClientId();
        String forwardTo = "ftd"; //default
        Connection conn = null;

        forwardTo = "input_" + clientId;
        if(logger.isInfoEnabled())
          logger.info("client id:" + clientId);

        try {
            // get country list
            CommonUtil commonUtil = new CommonUtil();
            conn = commonUtil.getNewConnection();
            
            CacheUtil cacheUtil = CacheUtil.getInstance();
            List countryList = cacheUtil.getCountryList();

            if(logger.isDebugEnabled())
              logger.debug("Countries loaded from cache:" + (countryList == null? "0" : String.valueOf(countryList.size())));
            
            request.setAttribute("clientId", clientId);
            productForm.setClientId(clientId);
            request.setAttribute("countryList", countryList);

        } catch(Exception ee) {
            
            ee.printStackTrace();
            logger.error(ee);
        } finally {
            if(!conn.isClosed()) {
                conn.close();
            }
        }

        return mapping.findForward(forwardTo);
    }


}
