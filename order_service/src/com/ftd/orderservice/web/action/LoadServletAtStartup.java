package com.ftd.orderservice.web.action;
/*
import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.loader.CompanyMasterLoader;
import com.ftd.orderservice.common.loader.ContentFilterLoader;
import com.ftd.orderservice.common.loader.GlobalParmLoader;
import com.ftd.orderservice.common.loader.IOTWLoader;
import com.ftd.orderservice.common.loader.PriceHeaderLoader;
import com.ftd.orderservice.common.loader.ProductColorLoader;
import com.ftd.orderservice.common.loader.ProductCompanyLoader;
import com.ftd.orderservice.common.loader.ProductCountryLoader;
import com.ftd.orderservice.common.loader.ProductInfoLoader;
import com.ftd.orderservice.common.loader.ProductKeywordLoader;
import com.ftd.orderservice.common.loader.ProductSourceLoader;
import com.ftd.orderservice.common.loader.ServiceClientLoader;
import com.ftd.orderservice.common.loader.ServiceClientXSLLoader;
import com.ftd.orderservice.common.loader.ServiceXSLLoader;
import com.ftd.orderservice.common.loader.SourceIndexChildLoader;
import com.ftd.orderservice.common.loader.SourceIndexProductLoader;
import com.ftd.orderservice.common.loader.SourceMasterLoader;
*/
import com.ftd.orderservice.common.util.CommonUtil;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import javax.servlet.http.HttpServlet;

/**
 * This class preloads cache objects as a temporary solution before cache implementation is in place.
 */
public class LoadServletAtStartup extends HttpServlet {
    private static Logger logger;
    public void init() {
    //System.out.println( getServletName() + ": initialised" );
      CommonUtil commonUtil = new CommonUtil();
      Connection conn = null;
      try {
          logger = new Logger(this.getClass().getName());
          //String loadCacheOnStartup = commonUtil.getConfigValue(OrderServiceConstants.LOAD_TEST_CACHE);
          //logger.info("loadCacheOnStartup: " + loadCacheOnStartup);
          //if("Y".equals(loadCacheOnStartup)) {
          /*
              conn = commonUtil.getNewConnection();
              GlobalParmLoader gpLoader = new GlobalParmLoader();
              gpLoader.load(conn);
              
              ServiceClientLoader cLoader = new ServiceClientLoader();
              cLoader.load(conn);
              
              CompanyMasterLoader cmLoader = new CompanyMasterLoader();
              cmLoader.load(conn);
              
              ContentFilterLoader cfLoader = new ContentFilterLoader();
              cfLoader.load(conn);
              
              SourceIndexChildLoader sicLoader = new SourceIndexChildLoader();
              sicLoader.load(conn);
              
              SourceIndexProductLoader sipLoader = new SourceIndexProductLoader();
              sipLoader.load(conn);
              
              SourceMasterLoader smLoader = new SourceMasterLoader();
              smLoader.load(conn);
              
              ProductInfoLoader pmLoader = new ProductInfoLoader();
              pmLoader.load(conn);
              
              ProductColorLoader pcLoader = new ProductColorLoader();
              pcLoader.load(conn);
              
              IOTWLoader iotwLoader = new IOTWLoader();
              iotwLoader.load(conn);
              
              ProductCountryLoader pctLoader = new ProductCountryLoader();
              pctLoader.load(conn);
              
              ProductKeywordLoader pkLoader = new ProductKeywordLoader();
              pkLoader.load(conn);
              
              ProductCompanyLoader cmpLoader = new ProductCompanyLoader();
              cmpLoader.load(conn);
              
              PriceHeaderLoader phLoader = new PriceHeaderLoader();
              phLoader.load(conn);
              
              ProductSourceLoader psLoader = new ProductSourceLoader();
              psLoader.load(conn);
              
              ServiceClientXSLLoader xslLoader = new ServiceClientXSLLoader();
              xslLoader.load(conn);
          
              ServiceXSLLoader serviceXslLoader = new ServiceXSLLoader();
              serviceXslLoader.load(conn);
*/
          //}
          
      } catch (Exception e) {
          logger.error(e);
          e.printStackTrace();
      } finally {
          try {
            if(conn != null) {
              conn.close(); 
            }
          } catch (Exception ee) {
              logger.error(ee);
          }
      }
  }

}
