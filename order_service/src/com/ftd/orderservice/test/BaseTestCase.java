package com.ftd.orderservice.test;

import com.ftd.orderservice.common.util.CommonUtil;

import junit.framework.TestCase;

public class BaseTestCase extends TestCase {
    protected CommonUtil commonUtil = new CommonUtil();
    
    public BaseTestCase(String sTestName) {
        super(sTestName);
    }
}
