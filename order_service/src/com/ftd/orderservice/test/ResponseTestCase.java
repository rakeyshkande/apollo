package com.ftd.orderservice.test;

import com.ftd.osp.utilities.cacheMgr.handlers.enums.DeliveryMethodEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ImageTypeEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.PriceTypeEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductLoadLevelEnum;
import com.ftd.orderservice.common.transformer.ServiceResultTransformer;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AvailableDateVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.GBBVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ImageVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.orderservice.common.vo.request.GetProductRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductsByCategoryRequestVO;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.orderservice.common.vo.response.GetAddonResponseVO;
import com.ftd.orderservice.common.vo.response.GetAvailableDaysResponseVO;
import com.ftd.orderservice.common.vo.response.GetOccasionResponseVO;
import com.ftd.orderservice.common.vo.response.GetProductResponseVO;
import com.ftd.orderservice.common.vo.response.GetProductsByCategoryResponseVO;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import junit.framework.Test;
import junit.framework.TestSuite;


public class ResponseTestCase extends BaseTestCase {

    private static int maxElapsedMilliseconds = 75;
    private static int maxUsers = 40;
    
    private String transformData = "<?xml version=\"1.0\" encoding=\"ISO8859-1\" ?><?xml-stylesheet type=\"text/xsl\"?>\n" + 
    "<root>\n" + 
    "   <add_ons>\n" + 
    "      <add_on>\n" + 
    "         <AddOnVO num=\"1\">\n" + 
    "            <addOnId>A</addOnId>\n" + 
    "            <addOnTypeId>1</addOnTypeId>\n" + 
    "            <addOnDescription>Mylar Balloon</addOnDescription>\n" + 
    "            <addOnTypeDescription>Balloon</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Balloon - A mylar balloon creates a beautiful accompaniment to our bouquets and gifts.  We will select a balloon appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>4923150300</addOnUNSPSC>\n" + 
    "            <addOnPrice>5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"2\">\n" + 
    "            <addOnId>RC95</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Anniversary</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"3\">\n" + 
    "            <addOnId>RC84</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Birthday</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"4\">\n" + 
    "            <addOnId>RC99</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Congratulations</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"5\">\n" + 
    "            <addOnId>RC108</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>For you, Mom on Mothers Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>For a wonderful Mother who's loved as much as you, only the sweetest, most beautiful day could ever be good enough. Happy Mother's Day</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"6\">\n" + 
    "            <addOnId>RC89</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Get Well</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"7\">\n" + 
    "            <addOnId>RC109</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Happy Mothers Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>A day of love and thanks especially for you.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"8\">\n" + 
    "            <addOnId>RC107</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Happy Mothers Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Loving wishes for a day bursting with happiness</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"9\">\n" + 
    "            <addOnId>RC103</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Holiday - No Front Text</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Greetings of the season and best wished for the new year</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"10\">\n" + 
    "            <addOnId>RC101</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Holiday - No Front Text</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Wishing you a very Merry Christmas</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"11\">\n" + 
    "            <addOnId>RC105</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>LOVE</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Happy Valentine's Day</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"12\">\n" + 
    "            <addOnId>RC97</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Love and Romance</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"13\">\n" + 
    "            <addOnId>RC116</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Mother&amp;#39;s Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card. We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1234569032</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"14\">\n" + 
    "            <addOnId>RC100</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>New Baby</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"15\">\n" + 
    "            <addOnId>RC999</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Occasion</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"16\">\n" + 
    "            <addOnId>RC86</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Sympathy</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Card.  We will select a card appropriate for the occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"17\">\n" + 
    "            <addOnId>RC93</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Thank You</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"18\">\n" + 
    "            <addOnId>RC90</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Thinking of You</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Full-Size Greeting Card and we will upgrade your gift with a full sized greeting card.  We will select a card appropriate for your special occasion.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"19\">\n" + 
    "            <addOnId>RC106</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Valentines Day - No Front Text</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>You are a constant reminder of how beautiful love can really be. Happy Valentine's Day I love you</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"20\">\n" + 
    "            <addOnId>RC102</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Wishing you a warm and wonderful season...</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Happy Holidays</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"21\">\n" + 
    "            <addOnId>RC36</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>With Sympathy</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText/>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"22\">\n" + 
    "            <addOnId>RC85</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>A warm wish from the heart for a day of beauty...</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Happy Birthday.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"23\">\n" + 
    "            <addOnId>RC83</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Birthday Wishes</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>May you enjoy the simple pleasures in life and let the littlest thing carry you away. Happy Birthday</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"24\">\n" + 
    "            <addOnId>RC104</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>For My Love On Valentine Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Every day I'm thankful that we share a life so rich in everything that matters and that we've found true love together. Happy Valentine's Day</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"25\">\n" + 
    "            <addOnId>RC96</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Happy Anniversary The best view of life is...</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Wishing you every happiness as you celebrate your anniversary</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"26\">\n" + 
    "            <addOnId>RC118</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Happy Mothers Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>All the flowers of Mother's Day don't brighten the world as beautifully as you do.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"27\">\n" + 
    "            <addOnId>RC88</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Just for you</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Just because. Enjoy! &amp;#153;&amp;reg;</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"28\">\n" + 
    "            <addOnId>RC98</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Love - No Front Text</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>My heart belongs to you</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"29\">\n" + 
    "            <addOnId>RC117</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Loving Wishes</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Hoping your day is bursting with happiness. Happy Mother's Day</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"30\">\n" + 
    "            <addOnId>RC110</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>N/A</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>May the holiday season bring you warm memories, good friends, great peace.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"31\">\n" + 
    "            <addOnId>RC113</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>N/A</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>I'm hoping this valentine brings you a smile and that it makes you happy just knowing someone thinks you're special - because you are.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"32\">\n" + 
    "            <addOnId>RC114</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>On Valentines Day</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Sending you a little love</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"33\">\n" + 
    "            <addOnId>RC91</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Thinking of You</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Hope it helps to know how much I care.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"34\">\n" + 
    "            <addOnId>RC92</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Thinking of You - No Front Text</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Thinking of You</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"35\">\n" + 
    "            <addOnId>RC115</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>We are Perfect Together</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>I can't help feeling we're meant to be together. Every time I look into your eyes, every time we kiss, every time you hold me - it just feels right.  Happy Valentine's Day</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"36\">\n" + 
    "            <addOnId>RC111</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Wishing you the gifts of peace, hope, and light.</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Merry Christmas</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"37\">\n" + 
    "            <addOnId>RC87</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>With Deepest Sympathy</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Please know our thoughts are with you at this difficult time</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"38\">\n" + 
    "            <addOnId>RC94</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>Your Thoughtfulness</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>Was very thoughtful.</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"39\">\n" + 
    "            <addOnId>RC112</addOnId>\n" + 
    "            <addOnTypeId>4</addOnTypeId>\n" + 
    "            <addOnDescription>joy to the world</addOnDescription>\n" + 
    "            <addOnTypeDescription>Card</addOnTypeDescription>\n" + 
    "            <addOnText>...and especially to YOU! Happy Holidays</addOnText>\n" + 
    "            <addOnUNSPSC>1411160500</addOnUNSPSC>\n" + 
    "            <addOnPrice>3.5</addOnPrice>\n" + 
    "            <addOnWeight>0</addOnWeight>\n" + 
    "            <productId/>\n" + 
    "            <addOnAvailableFlag>false</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>false</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "         <AddOnVO num=\"40\">\n" + 
    "            <addOnId>C</addOnId>\n" + 
    "            <addOnTypeId>5</addOnTypeId>\n" + 
    "            <addOnDescription>Chocolate</addOnDescription>\n" + 
    "            <addOnTypeDescription>Chocolate</addOnTypeDescription>\n" + 
    "            <addOnText>Add a Box of Chocolates to your bouquet and we will include a box of delicious chocolates to send sweet wishes their way. Size and flavor may vary. (May include nuts).</addOnText>\n" + 
    "            <addOnUNSPSC>5016181300</addOnUNSPSC>\n" + 
    "            <addOnPrice>17.99</addOnPrice>\n" + 
    "            <addOnWeight>0.2</addOnWeight>\n" + 
    "            <productId>CKJ</productId>\n" + 
    "            <addOnAvailableFlag>true</addOnAvailableFlag>\n" + 
    "            <addOnTypeIncludedInProductFeedFlag>true</addOnTypeIncludedInProductFeedFlag>\n" + 
    "            <defaultPerTypeFlag>false</defaultPerTypeFlag>\n" + 
    "            <displayPriceFlag>true</displayPriceFlag>\n" + 
    "            <maxQuantity/>\n" + 
    "            <displaySequenceNumber/>\n" + 
    "            <orderQuantity/>\n" + 
    "         </AddOnVO>\n" + 
    "      </add_on>\n" + 
    "   </add_ons>\n" + 
    "   <pageData>\n" + 
    "      <data>\n" + 
    "         <name>updateAllowed</name>\n" + 
    "         <value>Y</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>adminAction</name>\n" + 
    "         <value>merchandising</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>context</name>\n" + 
    "         <value>Order Proc</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>original_action_type</name>\n" + 
    "         <value>load</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>securitytoken</name>\n" + 
    "         <value>FTD_GUID_12865505810-201970487106093502610176258456901544808327089949831901867051834011643464641-14094960060549765400011948906641-24645541440-52842289639201966858447-211989895128-402568525227</value>\n" + 
    "      </data>\n" + 
    "   </pageData>\n" + 
    "   <security>\n" + 
    "      <data>\n" + 
    "         <name>sc_cust_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>call_brand_name</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>action</name>\n" + 
    "         <value>load</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_cc_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>t_comment_origin_type</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>t_entity_history_id</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_recip_ind</name>\n" + 
    "         <value>N</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_order_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_email_address</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>start_origin</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_zip_code</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>call_dnis</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_last_name</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>securitytoken</name>\n" + 
    "         <value>FTD_GUID_12865505810-201970487106093502610176258456901544808327089949831901867051834011643464641-14094960060549765400011948906641-24645541440-52842289639201966858447-211989895128-402568525227</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>t_call_log_id</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_cust_ind</name>\n" + 
    "         <value>N</value>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_tracking_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>call_type_flag</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>call_cs_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_phone_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>sc_rewards_number</name>\n" + 
    "         <value/>\n" + 
    "      </data>\n" + 
    "      <data>\n" + 
    "         <name>context</name>\n" + 
    "         <value>Order Proc</value>\n" + 
    "      </data>\n" + 
    "   </security>\n" + 
    "</root>\n";
    
    private String transformXML = "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" >\n" + 
    "\n" + 
    "<xsl:output method=\"html\" indent=\"yes\"/>\n" + 
    "\n" + 
    "<!-- Keys -->\n" + 
    "<xsl:key name=\"pageData\" match=\"/root/pageData/data\" use=\"name\"/>\n" + 
    "\n" + 
    "\n" + 
    "<xsl:template match=\"/root\">\n" + 
    "\n" + 
    "\n" + 
    "\n" + 
    "<html>\n" + 
    "  <head>\n" + 
    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></meta>\n" + 
    "    <title>Add On Dashboard</title>\n" + 
    "    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/ftd.css\"></link>\n" + 
    "\n" + 
    "    <script type=\"text/javascript\" src=\"js/AddOnDashboard.js\"></script>\n" + 
    "    <script type=\"text/javascript\" src=\"js/FormChek.js\"></script>\n" + 
    "    <script type=\"text/javascript\" src=\"js/util.js\"></script>\n" + 
    "    <script type=\"text/javascript\" src=\"js/commonUtil.js\"></script>\n" + 
    "  </head>\n" + 
    "  <body onload=\"javascript: init()\">\n" + 
    "    <form name=\"fAddOnDashboard\" method=\"post\">\n" + 
    "\n" + 
    "      <input type=\"hidden\" name=\"securitytoken\"           id=\"securitytoken\"     value=\"{key('pageData', 'securitytoken')/value}\"/>\n" + 
    "      <input type=\"hidden\" name=\"context\"                 id=\"context\"           value=\"{key('pageData', 'context')/value}\"/>\n" + 
    "      <input type=\"hidden\" name=\"adminAction\"             id=\"adminAction\"       value=\"{key('pageData', 'adminAction')/value}\"/>\n" + 
    "      <input type=\"hidden\" name=\"action_type\"             id=\"action_type\"/>\n" + 
    "      \n" + 
    "      <table width=\"955\" border=\"0\" >\n" + 
    "        <tr>\n" + 
    "          <td width=\"20%\" align=\"left\" valign=\"center\">\n" + 
    "            <div class=\"floatleft\">\n" + 
    "              <img border=\"0\" src=\"images/wwwftdcom_131x32.gif\" width=\"131\" height=\"32\"></img>\n" + 
    "            </div>\n" + 
    "          </td>\n" + 
    "          <td width=\"60%\" align=\"center\" valign=\"center\" class=\"Header\" id=\"pageHeader\">Add On Dashboard</td>\n" + 
    "          <td width=\"20%\"></td>\n" + 
    "        </tr>\n" + 
    "      </table>\n" + 
    "      \n" + 
    "\n" + 
    "      <!-- Main Div -->\n" + 
    "      <div id=\"content\" style=\"display:block\">\n" + 
    "        <tr><td></td></tr>\n" + 
    "        <tr><td></td></tr>\n" + 
    "        <tr>\n" + 
    "          <td align=\"left\" valign=\"top\">\n" + 
    "            <table>\n" + 
    "              <tr>\n" + 
    "                <td>\n" + 
    "                  <table width=\"955\" align=\"center\" cellspacing=\"1\" border=\"0\">\n" + 
    "                    <tr>\n" + 
    "                      <td align=\"left\" valign=\"top\"></td>\n" + 
    "                      <td align=\"right\" valign=\"top\">\n" + 
    "                        <button name=\"bMainMenu\" id=\"bMainMenu\" class=\"BlueButton\" style=\"width:80;\" accesskey=\"M\" onclick=\"performMainMenu();\">(M)ain Menu</button>\n" + 
    "                      </td>\n" + 
    "                    </tr>\n" + 
    "                  </table>\n" + 
    "                </td>\n" + 
    "              </tr>\n" + 
    "            </table>\n" + 
    "          </td>\n" + 
    "        </tr>\n" + 
    "        <tr><td></td></tr>\n" + 
    "\n" + 
    "        <!-- Header -->\n" + 
    "        <!-- Note that the colors are defined at the TD level instead of Table level because we didnt want to display a different color table border -->\n" + 
    "        <tr>\n" + 
    "          <td align=\"left\" valign=\"top\">\n" + 
    "            <table>\n" + 
    "              <tr>\n" + 
    "                <td>\n" + 
    "                  <table width=\"955\" align=\"left\" border=\"0\" style=\"background-color:rgb(209,238,252);\">\n" + 
    "                    <tr>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:040;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:040;\">          </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:040;\">Select                   </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:065;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:065;\">Add-on    </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:065;\">ID                       </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:120;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:120;\">Add-on    </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:120;\">Type                     </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:165;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:165;\">Add-on    </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:165;\">Description              </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:315;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:315;\">          </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:315;\">Add-on Text              </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:070;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;\">          </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;\">Price $                  </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:070;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;\">Product   </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:070;\">Id                       </td>    </tr>    </table>   </td>\n" + 
    "                      <td align=\"left\" style=\"background-color:rgb(209,238,252); width:060;\">    <table>   <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;\">          </td>    </tr>    <tr>   <td align=\"left\" style=\"background-color:rgb(209,238,252); font-size:11; font-weight:bold; width:060;\">Available                </td>    </tr>    </table>   </td>\n" + 
    "                    </tr>\n" + 
    "                  </table>\n" + 
    "                </td>\n" + 
    "              </tr>\n" + 
    "            </table>\n" + 
    "          </td>\n" + 
    "        </tr>\n" + 
    "\n" + 
    "\n" + 
    "        <!-- Details -->\n" + 
    "        <tr>\n" + 
    "          <td align=\"left\" valign=\"top\">\n" + 
    "            <div id=\"detailDiv\" style=\"width:980; height:15em; overflow:auto;\">\n" + 
    "              <table>\n" + 
    "                <tr>\n" + 
    "                  <td>\n" + 
    "                    <table width=\"955\" align=\"center\" cellspacing=\"1\" class=\"mainTable\" border=\"1\">\n" + 
    "                      <!-- Detail Lines -->\n" + 
    "                      <xsl:for-each select=\"add_ons/add_on/AddOnVO\">\n" + 
    "                        <tr>\n" + 
    "                          <xsl:if test=\"position() mod 2 = 0\">\n" + 
    "                            <xsl:attribute name=\"style\">background-color:#D3D3D3;</xsl:attribute>\n" + 
    "                          </xsl:if>\n" + 
    "                          <td align=\"left\" style=\"width:040;\"><input type=\"radio\" id=\"addOnIdRadio\" name=\"addOnIdRadio\" value=\"{addOnId}\"/></td>\n" + 
                                  "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
                                  
                                  
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +
    "   <td><xsl:value-of select=\"format-number(-123.45, '###,###.00;(###,###.00)')\"/></td>" +

                                  
    "                          <td align=\"left\" style=\"width:065;\">\n" + 
    "                            <xsl:value-of select=\"addOnId\"/>\n" + 
    "                            <xsl:if test=\"defaultPerTypeFlag = 'true'\"><span style=\"color:red\">*</span></xsl:if>\n" + 
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:120;word-break:break-all;\">\n" + 
    "                            <xsl:value-of select=\"addOnTypeDescription\"/>\n" + 
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:165;word-break:break-all;\">\n" + 
    "                            <xsl:if test=\"string-length(addOnDescription/text()) = 0\"></xsl:if>\n" + 
    "                            <xsl:value-of select=\"addOnDescription\" disable-output-escaping=\"yes\"/>\n" + 
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:315;word-break:break-all;\">\n" + 
    "                            <xsl:if test=\"string-length(addOnText/text()) = 0\">;</xsl:if>\n" + 
    "                            <xsl:value-of select=\"addOnText\" disable-output-escaping=\"yes\" />\n" + 
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:070;\">\n" + 
    "                            <xsl:value-of select='format-number(addOnPrice, \"0.00\")' />\n" + 
                                  "      <xsl:if test=\"addOnPrice &gt; 1\">\n" + 
                                  "        <tr>\n" + 
                                  "          <td><xsl:value-of select=\"addOnDescription\"/></td>\n" + 
                                  "          <td><xsl:value-of select=\"addOnDescription\"/></td>\n" + 
                                  "        </tr>\n" + 
                                  "      </xsl:if>"+
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:070;\">\n" + 
    "                            <xsl:if test=\"string-length(productId/text()) = 0\"></xsl:if>\n" + 
    "                            <xsl:value-of select=\"productId\"/>\n" + 
    "                          </td>\n" + 
    "                          <td align=\"left\" style=\"width:060;\"><xsl:choose><xsl:when test=\"addOnAvailableFlag = 'true'\">Y</xsl:when><xsl:otherwise>N</xsl:otherwise></xsl:choose></td>\n" + 
    "                        </tr>\n" + 
    "                      </xsl:for-each>\n" + 
    "                    </table>                                                                                                                          \n" + 
    "                  </td>                                                                                                                                \n" + 
    "                </tr>                                                                                                                                  \n" + 
    "              </table>                                                                                                                                \n" + 
    "            </div>                                                                                                                                    \n" + 
    "          </td>                                                                                                                                        \n" + 
    "        </tr>                                                                                                                                          \n" + 
    "                                                                                                                                                      \n" + 
    "\n" + 
    "        <tr>\n" + 
    "          <td align=\"left\" valign=\"top\">\n" + 
    "            <table>\n" + 
    "              <tr>\n" + 
    "                <td>\n" + 
    "                  <table width=\"955\" align=\"center\" cellspacing=\"1\" border=\"0\">\n" + 
    "\n" + 
    "                    <tr><td colspan=\"2\"></td></tr>                                                                                                                      \n" + 
    "                    <tr><td><span style=\"color:red\">*</span>DefaultCard</td></tr>\n" + 
    "                    <tr><td colspan=\"2\"></td></tr>                                                                                                                      \n" + 
    "\n" + 
    "                    <tr>\n" + 
    "                      <td align=\"left\" valign=\"top\">\n" + 
    "                        <xsl:choose>\n" + 
    "                          <xsl:when test=\"key('pageData', 'updateAllowed')/value = 'Y'\">\n" + 
    "                            <button name=\"bAdd\"  id=\"bAdd\"  class=\"BlueButton\" style=\"width:80;\" accesskey=\"A\" onclick=\"performAdd();\">(A)dd</button>\n" + 
    "                            <button name=\"bEdit\" id=\"bEdit\" class=\"BlueButton\" style=\"width:80;\" accesskey=\"E\" onclick=\"performEdit();\">(E)dit</button>\n" + 
    "                          </xsl:when>\n" + 
    "                          <xsl:otherwise>                           \n" + 
    "                            <button name=\"bAdd\"  id=\"bAdd\"  class=\"BlueButton\" style=\"width:80;\" disabled=\"true\">(A)dd</button>\n" + 
    "                            <button name=\"bEdit\" id=\"bEdit\" class=\"BlueButton\" style=\"width:80;\" disabled=\"true\">(E)dit</button>\n" + 
    "                          </xsl:otherwise>                            \n" + 
    "                        </xsl:choose>\n" + 
    "                      </td>\n" + 
    "                      <td align=\"right\" valign=\"top\">\n" + 
    "                        <button name=\"bMainMenu\" id=\"bMainMenu\" class=\"BlueButton\" style=\"width:80;\" accesskey=\"M\" onclick=\"performMainMenu();\">(M)ain Menu</button>\n" + 
    "                      </td>\n" + 
    "                    </tr>\n" + 
    "                  </table>\n" + 
    "                </td>\n" + 
    "              </tr>\n" + 
    "            </table>\n" + 
    "          </td>\n" + 
    "        </tr>\n" + 
    "                                                                                                                                                      \n" + 
    "      </div>\n" + 
    "\n" + 
    "      <!-- Used to dislay Processing... message to user -->\n" + 
    "      <div id=\"waitDiv\" style=\"display:none\">\n" + 
    "         <table id=\"waitTable\" class=\"mainTable\" width=\"98%\" height=\"100%\" align=\"center\" cellpadding=\"2\" cellspacing=\"1\" >\n" + 
    "            <tr>\n" + 
    "              <td width=\"100%\">\n" + 
    "                <table class=\"innerTable\" width=\"100%\" height=\"100%\" cellpadding=\"30\" cellspacing=\"1\">\n" + 
    "                 <tr>\n" + 
    "                    <td>\n" + 
    "                       <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n" + 
    "                           <tr>\n" + 
    "                             <td id=\"waitMessage\" align=\"right\" width=\"50%\" class=\"waitMessage\"></td>\n" + 
    "                             <td id=\"waitTD\"  width=\"50%\" class=\"waitMessage\"></td>\n" + 
    "                           </tr>\n" + 
    "                       </table>\n" + 
    "                     </td>\n" + 
    "                  </tr>\n" + 
    "                </table>\n" + 
    "              </td>\n" + 
    "            </tr>\n" + 
    "          </table>\n" + 
    "      </div>\n" + 
    "\n" + 
    "\n" + 
    "\n" + 
    "      <table width=\"98%\" border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\">\n" + 
    "        <tr>\n" + 
    "          <td></td>\n" + 
    "        </tr>\n" + 
    "        <tr>\n" + 
    "          <td class=\"Disclaimer\" align=\"center\">\n" + 
    "            COPYRIGHT\n" + 
    "            <script>document.write(\"®\");</script>\n" + 
    "            2010. FTD INC. ALL RIGHTS RESERVED.\n" + 
    "          </td>\n" + 
    "        </tr>\n" + 
    "      </table>\n" + 
    "\n" + 
    "    </form>\n" + 
    "    <script type=\"text/javascript\" language=\"javascript\">\n" + 
    "      <!--************* USER COMMUNICATION AREA - SUCCESS & ERROR MESSAGES WILL BE DISPLAYED HERE *************-->\n" + 
    "      <xsl:if test = \"key('pageData', 'errorMessage')/value != ''\">\n" + 
    "         displayOkError('<xsl:value-of select = \"key('pageData', 'errorMessage')/value\"/>');\n" + 
    "      </xsl:if>\n" + 
    "    </script>\n" + 
    "  </body>\n" + 
    "</html>\n" + 
    "</xsl:template>\n" + 
    "</xsl:stylesheet>";

    public ResponseTestCase(String testCase) {
        super(testCase);
    }
    
    public void setUp() throws Exception
    {
        
    }
    
    public void tearDown() throws Exception
    {

    }
    
    public static TestSuite suite()
    { 
        TestSuite suite = null;
        
        try 
        {
            suite = new TestSuite(ResponseTestCase.class);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return suite;
    }

    public static void main(String[] args) {
        try {
            

            TestSuite suite;
            suite = new TestSuite("ResponseTestCase");
            Test loadTest = null;
            Test timedTest = null;

            ResponseTestCase responseTestCase = null;
            //responseTestCase = new ResponseTestCase("testGetOccasion");
            //suite.addTest(responseTestCase);
            
            //timedTest = new TimedTest(responseTestCase, maxElapsedMilliseconds);
            //suite.addTest(timedTest);
            
            //loadTest = new LoadTest(responseTestCase, maxUsers);
            //suite.addTest(loadTest);

            
            //responseTestCase = new ResponseTestCase("testGetCategory");
            //loadTest = new LoadTest(responseTestCase, 40, 50);
            //suite.addTest(responseTestCase);
            
            //loadTest = new LoadTest(responseTestCase, maxUsers);
            //suite.addTest(loadTest);
            
            responseTestCase = new ResponseTestCase("testGetProduct");
            suite.addTest(responseTestCase);
            
            //loadTest = new LoadTest(responseTestCase, maxUsers);
            //suite.addTest(loadTest);
            
            
            //responseTestCase = new ResponseTestCase("testGetAvailableDays");
            //suite.addTest(responseTestCase);
            
            
            //responseTestCase = new ResponseTestCase("testGetAddon");
            //suite.addTest(responseTestCase);

            //responseTestCase.runBare();
             
            
            //responseTestCase = new ResponseTestCase("testTransformXMLJava");
            //responseTestCase.runBare();
            
            //loadTest = new LoadTest(responseTestCase, 40, 50);
            //suite.addTest(loadTest);

                     
            junit.textui.TestRunner.run(suite);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test the getOccasion response object.
     */
    public void testGetOccasion() {
        try {
            GetOccasionResponseVO respVO = new GetOccasionResponseVO();
            OrderServiceRequestVO reqVO = new OrderServiceRequestVO();
            reqVO.setClientId("allurent");
            reqVO.setSourceCode("8888");
            
            respVO.setRequestVO(reqVO);
            
            List<OccasionVO> olist = new ArrayList<OccasionVO>();
            OccasionVO ovo = new OccasionVO();
            ovo.setIndexId("birthday");
            ovo.setIndexName("blushing beauty");
            olist.add(ovo);
            respVO.setOccasionList(olist);
            ServiceResultTransformer serviceResultTransformer = new ServiceResultTransformer();
            System.out.println(serviceResultTransformer.transform(respVO));
              //System.out.println(commonUtil.toXMLString(respVO.toXMLDoc()));
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    /**
     * Test the getOccasion response object.
     */
    public void testGetCategory() {
        try {
            GetProductsByCategoryResponseVO respVO = new GetProductsByCategoryResponseVO();
            GetProductsByCategoryRequestVO reqVO = new GetProductsByCategoryRequestVO();
            reqVO.setClientId("allurent");
            reqVO.setSourceCode("8888");
            reqVO.setCategoryId("555");
                        
            respVO.setRequestVO(reqVO);
            
            List<ProductMasterVO> plist = new ArrayList<ProductMasterVO>();
            ProductMasterVO pmvo = new ProductMasterVO();
            pmvo.setWebProductId("F807");
            plist.add(pmvo);
            
            pmvo = new ProductMasterVO();
            pmvo.setWebProductId("C-4302");
            plist.add(pmvo);
            
            respVO.setProductMasterList(plist);
            respVO.setIndexId("1111");
            respVO.setIndexName("IndexName");
            // System.out.println(respVO.toXMLDoc());
            
            ServiceResultTransformer serviceResultTransformer = new ServiceResultTransformer();
            
            System.out.println(serviceResultTransformer.transform(respVO));

            //System.out.println(new CommonUtil().toXMLString(respVO.toXMLDoc()));
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    /**
     * Test the getOccasion response object.
     */
    public void testGetProduct() {
        try {
            GetProductResponseVO respVO = new GetProductResponseVO();
            GetProductRequestVO reqVO = new GetProductRequestVO();
            reqVO.setClientId("allurent");
            reqVO.setSourceCode("8888");
            String [] productIds = new String [2];
            productIds [0] = "8082";
            productIds [1] = "4321";
            reqVO.setWebProductIds(productIds);
            reqVO.setLoadLevel(ProductLoadLevelEnum.toProductLoadLevelString(ProductLoadLevelEnum.DETAILS));
            
            respVO.setRequestVO(reqVO);
            List<ProductMasterVO> plist = new ArrayList();
            ProductMasterVO pm = new ProductMasterVO();
            
            
            pm.setProductId("abcd");
            pm.setWebProductId("abcd-c");
            pm.setCategoryDesc("baby category");
            pm.setProductType("FRECUT");
            pm.setTypeDesc("FRESH CUT");
            pm.setSubTypeDesc("home gift");
            pm.setLongDesc("This is the long description.");
            pm.setWebProductName("Novator name");
            pm.setDominantFlowers("dominant flowers");
            pm.setOver21Flag("Y");
            pm.setStatus("A");
            pm.setPersonalizationFlag("personalization description");
            //pm.setPersonalizationRegExp("personalizationRegexp");
            pm.setDeliveryType("D");
            pm.setDiscountAllowedFlag("Y");
            pm.setNoTaxFlag("N");
            pm.setPreferredPricePoint("1");
            pm.setDeliveryMethod(DeliveryMethodEnum.BOTH);
            pm.setIOTWMessaging("default message");
            pm.setExceptionStartDate(commonUtil.getDate(2));
            pm.setExceptionEndDate(commonUtil.getDate(20));
            
            List<ImageVO> imageList = new ArrayList();
            ImageVO ivo = new ImageVO();
            ivo.setImageURL("http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/products/F806_330x370.jpg");
            ivo.setImageType(ImageTypeEnum.PRODUCT);
            ivo.setImageFormat("jpg");
            imageList.add(ivo);
            pm.setImageList(imageList);
            
            List<ColorMasterVO> colorList = new ArrayList();
            ColorMasterVO cvo = new ColorMasterVO();
            cvo.setColorId("R");
            cvo.setColorDesc("Red");
            colorList.add(cvo);
            pm.setColorList(colorList);
            
            List<CompanyMasterVO> companyList = new ArrayList();
            CompanyMasterVO cmvo = new CompanyMasterVO();
            cmvo.setCompanyId("FTD");
            cmvo.setCompanyName("FTD.com");
            companyList.add(cmvo);
            pm.setCompanyList(companyList);
            
            List<String> keywordList = new ArrayList();
            keywordList.add("lily");
            keywordList.add("red");
            pm.setKeywordList(keywordList);
            
            List<PricingVO> priceList = new ArrayList();
            PricingVO pvo = new PricingVO();
            pvo.setPriceType(PriceTypeEnum.BASE);
            pvo.setPriceAmount(new BigDecimal("45.99"));
            pvo.setRewardAmount(new BigDecimal("300"));
            pvo.setRewardType("miles");
            pvo.setRedemptionAmount(new BigDecimal("6000"));
            priceList.add(pvo);
            
            
            pvo = new PricingVO();
            pvo.setPriceType(PriceTypeEnum.SALE);
            pvo.setPriceAmount(new BigDecimal("35.99"));
            pvo.setRewardAmount(new BigDecimal("200"));
            pvo.setRewardType("miles");
            pvo.setRedemptionAmount(new BigDecimal("5000"));
            priceList.add(pvo);
            pm.setPricingList(priceList);
            
            List<GBBVO> gbbList = new ArrayList();
            GBBVO gbbvo = new GBBVO();
            gbbvo.setProductId("F806");
            gbbvo.setWebProductId("C-F806");
            gbbvo.setGbbNameOverrideTxt("beautiful");
            gbbvo.setDefaultFlag("Y");
            gbbvo.setDisplaySequence("1");
            gbbvo.setGbbNameOverrideTxt("good");
            
            imageList = new ArrayList();
            ivo = new ImageVO();
            ivo.setImageURL("http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/products/F806_330x370.jpg");
            ivo.setImageType(ImageTypeEnum.PRODUCT);
            ivo.setImageFormat("jpg");
            imageList.add(ivo);
            gbbvo.setImageList(imageList);
            
            pvo = new PricingVO();
            pvo.setPriceType(PriceTypeEnum.SALE);
            pvo.setPriceAmount(new BigDecimal("35.99"));
            pvo.setRewardAmount(new BigDecimal("200"));
            pvo.setRewardType("miles");
            pvo.setRedemptionAmount(new BigDecimal("5000"));
            priceList.add(pvo);
            gbbvo.setPricingList(priceList);
            
            gbbList.add(gbbvo);
            pm.setGbbList(gbbList);
            
            plist.add(pm);
            respVO.setProductMasterList(plist);
            
            //respVO.toXMLDoc();
            ServiceResultTransformer serviceResultTransformer = new ServiceResultTransformer();
            System.out.println(serviceResultTransformer.transform(respVO));
            //System.out.println(new CommonUtil().toXMLString(respVO.toXMLDoc()));
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
        
    }
    
    
    /**
     * Test the getOccasion response object.
     */
    public void testGetAvailableDays()
    {
            try {
                GetAvailableDaysResponseVO respVO = new GetAvailableDaysResponseVO();
                OrderServiceRequestVO reqVO = new OrderServiceRequestVO();
                reqVO.setClientId("allurent");
                reqVO.setSourceCode("888\"8");
                
                respVO.setRequestVO(reqVO);
                
                List<AvailableDateVO> alist = new ArrayList<AvailableDateVO>();
                AvailableDateVO avo = new AvailableDateVO();
                avo.setDeliveryDate(commonUtil.getDate(3));
                avo.setDescription("MAY 3 Monday");
                avo.setMessage("Shipping Cost: $15.50");
                alist.add(avo);
                
                avo = new AvailableDateVO();
                avo.setDeliveryDate(commonUtil.getDate(4));
                avo.setEndDate(commonUtil.getDate(5));
                avo.setDescription("MAY 4 Tuesday - MAY 5 Wednesday");
                avo.setMessage("Service Fee: $12");
                alist.add(avo);
                
                respVO.setAvailDateList(alist);


                ServiceResultTransformer serviceResultTransformer = new ServiceResultTransformer();
                System.out.println(serviceResultTransformer.transform(respVO));
                
            } catch (Throwable t) {
                t.printStackTrace();
            }
    }
    
    
    /**
     * Test the getOccasion response object.
     */
    public void testGetAddon() {
        try {
            GetAddonResponseVO respVO = new GetAddonResponseVO();
            OrderServiceRequestVO reqVO = new OrderServiceRequestVO();
            reqVO.setClientId("allurent");
            reqVO.setSourceCode("888'8");
            
            respVO.setRequestVO(reqVO);
            
            List<AddonVO> alist = new ArrayList<AddonVO>();
            AddonVO avo = new AddonVO();
            avo.setAddonId("V8556");
            avo.setAddonDesc("Sweetheart Glass Rounded Vase");
            avo.setAddonTitle("Sweetheart Glass Rounded Vase");
            avo.setAddonTypeId("7");
            avo.setAddonType("Vase");
            avo.setDefaultPerTypeFlag("Y");
            avo.setMaxPurchaseQty(1);
            avo.setDisplayPriceFlag("N");
            avo.setAddonPrice(new BigDecimal("5.3"));
            avo.setPriceType("base");
            avo.setPriceUnit("each");
            ImageVO ivo = new ImageVO();
            ivo.setImageURL("http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/addons/vase2.jpg");
            ivo.setImageFormat("jpg");
            ivo.setImageType(ImageTypeEnum.THUMBNAIL);
            avo.setImage(ivo);
            alist.add(avo);
            
            avo = new AddonVO();
            avo.setAddonId("RC111");
            avo.setAddonDesc("Wishing you the gifts of peace, hope, and light.");
            avo.setAddonTitle("Wishing you the gifts of peace, hope, and light.");
            avo.setAddonTypeId("4");
            avo.setAddonType("Card");
            avo.setDefaultPerTypeFlag("N");
            avo.setMaxPurchaseQty(1);
            avo.setDisplayPriceFlag("Y");
            avo.setAddonPrice(new BigDecimal("3.5"));
            avo.setPriceType("base");
            avo.setPriceUnit("each");
            ivo = new ImageVO();
            ivo.setImageURL("http://a248.e.akamai.net/f/80/71/6h/www.ftd.com/pics/addons/card1.jpg");
            ivo.setImageFormat("jpg");
            ivo.setImageType(ImageTypeEnum.THUMBNAIL);
            avo.setImage(ivo);
            alist.add(avo);

            respVO.setAddonList(alist);
           // System.out.println(new CommonUtil().toXMLString(respVO.toXMLDoc()));
           ServiceResultTransformer serviceResultTransformer = new ServiceResultTransformer();
           System.out.println(serviceResultTransformer.transform(respVO));

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    public void testTransformXMLJava() {
    try {
        InputStream transformationDataStream = new ByteArrayInputStream( transformData.getBytes("UTF-8"));
        InputStream transformationXMLStream = new ByteArrayInputStream( transformXML.getBytes("UTF-8"));

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();

        TransformerFactory tFactory = TransformerFactory.newInstance();
        
        Transformer transformer =
          tFactory.newTransformer
             (new javax.xml.transform.stream.StreamSource
                (transformationXMLStream));

        transformer.transform
          (new javax.xml.transform.stream.StreamSource
              (transformationDataStream), new javax.xml.transform.stream.StreamResult (outStream));
        
        outStream.flush();
        outStream.close();
        
        
        
        } catch (Throwable t) {
            t.printStackTrace();
            
        }
    
    }
    
}
