package com.ftd.orderservice.common.transformer.xstream;

import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

import java.io.Writer;

/**
 * Provides the ability to wrap the Text in a CDATA section.
 * Custom Converters can selectively enable/disable the CDATA mode
 * prior to the invocation of writeText.
 */
public class XMLPrintWriter extends PrettyPrintWriter
{
  /**
   * Flag indicating whether text should be wrapped in a CDATA section. <br>
   * Value of <code>true</code> will wrap the text in a CDATA section <br>
   * Value of <code>false</code> will utilize the superclass method, which results in Text escaping of special characters
   */
  private boolean wrapTextInCData = false;

  /**
   * Initializes the Print Writer with default value, to write to the passed in Writer.
   * @param writer
   */
  public XMLPrintWriter(Writer writer)   
  {
    super(writer);
  }

  /**
   * Overrides the parent writeText method to determing if the text should be 
   * wrapped in a CDATA section. If the flag is false, the parent class's writeText is invoked
   * which performs XML escaping.
   * 
   * @param writer The writer to write the Text to.
   * @param text The text to write.
   */
  protected void writeText(QuickWriter writer, String text) 
  {
    if(wrapTextInCData) 
    {
      // Write the Text in a CDATA section.
      writer.write("<![CDATA["); 
      writer.write(text); 
      writer.write("]]>"); 
    } else
    {
      // Invoke parent class, allow default escaping behavior
      super.writeText(writer, text);
    }
  }

  /**
   * @see #wrapTextInCData
   */
  public void setWrapTextInCData(boolean wrapTextInCData)
  {
    this.wrapTextInCData = wrapTextInCData;
  }
}
