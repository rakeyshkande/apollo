package com.ftd.orderservice.common.transformer.xstream;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * This class implements an XStream String Converter that wraps the String in a CDATA section
 * without XML escaping.
 */
public class CDataStringConverter implements Converter 
{
  /**
   * Returns true for {@link java.lang.String} 
   */
  public boolean canConvert(Class clazz)
  {
    return String.class.equals(clazz);
  }
  
  /**
   * Returns the String/Text value for the current node.
   */
  public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader, UnmarshallingContext unmarshallingContext)
  {
    String value = hierarchicalStreamReader.getValue();
    
    // The parser's/reader's generally strip the CDATA declarations, so, just return the value.
    // If that is not the case, we need to strip the CDATA portion from the value before returning it.
    
    return value;
  }

  /**
   * Configures the Print Writer to embed the passed in String Object within a CDATA text node, then writes the Node Value.
   * 
   * @param object String value to output. Requires the object to be of type String
   * @param hierarchicalStreamWriter The underlying writer must be of type {@link com.ftd.orderservice.common.transformer.xstream.XMLPrintWriter}
   * @param marshallingContext
   */
  public void marshal(Object object, HierarchicalStreamWriter hierarchicalStreamWriter, MarshallingContext marshallingContext)
  {
    // Write an empty node if the object is null
    if(object == null)
    {
      object = "";
    }
    
    // This will only work if Xstream was created with this Print Writer
    XMLPrintWriter xmlPrintWriter = (XMLPrintWriter) hierarchicalStreamWriter.underlyingWriter();
    
    try 
    {
      xmlPrintWriter.setWrapTextInCData(true);  
      xmlPrintWriter.setValue((String) object);
    } finally 
    {
      xmlPrintWriter.setWrapTextInCData(false);  
    }
  }



}
