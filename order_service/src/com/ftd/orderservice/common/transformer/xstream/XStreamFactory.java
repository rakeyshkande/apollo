package com.ftd.orderservice.common.transformer.xstream;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

import java.io.Writer;

/**
 * Factory class for instantiating XStream and returning a configured instance.
 * 
 * Supports embedding Text nodes in CDATA blocks.
 * 
 * 
 * 
 */
public class XStreamFactory
{
  /**
   * @return An instance of XStream that is configured
   */
  static public XStream getXStream() 
  {
    XStream xStream = new XStream(new XppDriver() { 
          public HierarchicalStreamWriter createWriter(Writer out) { 
            return new XMLPrintWriter(out);
          }
    });
    
    xStream.setMode(XStream.NO_REFERENCES);

    // use this if we want all String Classes to be processed with this Converter
    //xStream.registerConverter(new CDataStringConverter());
    
    // Setup specific nodes to output as CDATA
    setupCDATANodes(xStream);
    
    return xStream;    
  }
  
  /**
   * Registers specific nodes to be converted with CDATA Fields
   * @param xStream
   */
  static void setupCDATANodes(XStream xStream)
  {
    CDataStringConverter cDataStringConverter = new CDataStringConverter();
    xStream.registerLocalConverter(AddonVO.class, "addonTitle", cDataStringConverter);
    xStream.registerLocalConverter(AddonVO.class, "addonDesc", cDataStringConverter);
    xStream.registerLocalConverter(ProductMasterVO.class, "webProductName", cDataStringConverter);
    xStream.registerLocalConverter(ProductMasterVO.class, "longDesc", cDataStringConverter);   
    xStream.registerLocalConverter(ProductMasterVO.class, "productName", cDataStringConverter); 
    xStream.registerLocalConverter(ProductMasterVO.class, "subTypeDesc", cDataStringConverter); 
    xStream.registerLocalConverter(ProductMasterVO.class, "gbbTitle", cDataStringConverter); 
  }
  
}
