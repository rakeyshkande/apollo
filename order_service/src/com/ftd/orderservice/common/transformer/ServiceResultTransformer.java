package com.ftd.orderservice.common.transformer;

import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.transformer.xstream.XStreamFactory;
import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.orderservice.common.vo.response.OrderServiceResponseVO;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import com.thoughtworks.xstream.XStream;

import java.io.StringWriter;

import javax.xml.transform.Templates;

import org.apache.log4j.Logger;


public class ServiceResultTransformer
{
    protected static final Logger logger  = Logger.getLogger("com.ftd.orderservice.common.transformer.ServiceResultTransformer");
    protected static final XStream xstream = XStreamFactory.getXStream();
    protected static final TraxUtil traxUtil = TraxUtil.getInstance();
    protected CommonUtil commonUtil = null;
    
    private static final int XML_SIZE_HINT = 16384; // 16K
    
    public ServiceResultTransformer () {
        commonUtil = new CommonUtil();
    }
    public String transform(OrderServiceResponseVO respVO) throws Exception
    {
        // Pass in a Writer to XStream so we can optimize buffer size
        StringWriter xmlDataWriter = new StringWriter(XML_SIZE_HINT);
        xstream.toXML(respVO, xmlDataWriter);
        String xmlData = xmlDataWriter.toString();
        
        OrderServiceRequestVO reqVO = respVO.getRequestVO();
        String clientId = reqVO.getClientId();
        String actionType = reqVO.getActionType();
        
        Templates xslContent = getXSLByClientAction(clientId, actionType);
        
        String transformationResult = null;
        if(xslContent != null) {
            transformationResult = traxUtil.transform(xmlData, xslContent, XML_SIZE_HINT);
        } else {
            transformationResult = xmlData;
        }
        
        return transformationResult;
    }


    
    private Templates getXSLByClientAction(String clientId, String actionType)throws Exception
    {
        // Retrieve from cache the XSL based on client and action type.
        Templates xslContent = null;
        CacheUtil cacheUtil = CacheUtil.getInstance();
        
        
        // If this fails, send system message and return generic service unavailable error.
        xslContent = cacheUtil.getXSLTemplateByClientAction(clientId, actionType);

        return xslContent;
    }
    
    public String transformError(OrderServiceRequestVO reqVO)throws Exception
    {
        
        
        // Use clientId to look up error.
        String responseString = "";
        String errorCode = reqVO.getErrorCode();
        StringWriter xmlDataWriter = new StringWriter(XML_SIZE_HINT);
       
        String clientId = reqVO.getClientId();
        String actionType = OrderServiceConstants.ACTION_TYPE_ERROR;
        String clientError = "";
        String xmlData = "";
        
        try {
            Templates xslContent = getXSLByClientAction(clientId, actionType);
    
            
            //Look for client specific error message.. 
            clientError = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_CLIENT_ERROR, 
                                                           reqVO.getErrorCode(), reqVO.getClientId(), null);

            // If content message contains a replaceable token, replace it.
            if(clientError.indexOf(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1) > 0) {
                clientError = clientError.replaceAll(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, reqVO.getErrorFieldValue());
            }
            else if(clientError.indexOf(OrderServiceConstants.CONTENT_REPLACE_SOURCE_CODE) > 0) {
                clientError = clientError.replaceAll(OrderServiceConstants.CONTENT_REPLACE_SOURCE_CODE, reqVO.getSourceCode());
            }
            else if(clientError.indexOf(OrderServiceConstants.CONTENT_REPLACE_DELIVERY_DATE) > 0) {
                clientError = clientError.replaceAll(OrderServiceConstants.CONTENT_REPLACE_DELIVERY_DATE, reqVO.getErrorFieldValue());
            }
            reqVO.setErrorMessage(clientError);
           
            xstream.toXML(reqVO, xmlDataWriter);
            xmlData = xmlDataWriter.toString();
            
            if(xslContent != null) {
                responseString = traxUtil.transform(xmlData, xslContent, XML_SIZE_HINT);
            } else {
                responseString = xmlData;
            }
        } catch (Exception e) {
            String errorStr = "Encountered error while transform error." + reqVO.toLogString() + e.getMessage();
            logger.error(errorStr);
            logger.error(e);
            commonUtil.sendNoPageSystemMessage(errorStr);
            responseString = "";
        }
        return responseString;
    }    

}
