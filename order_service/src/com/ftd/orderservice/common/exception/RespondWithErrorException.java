package com.ftd.orderservice.common.exception;

public class RespondWithErrorException extends Exception
{
     Throwable exceptionCause = null;

    public RespondWithErrorException(String message)
    {
        super(message);
    }

    public RespondWithErrorException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }

    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}