package com.ftd.orderservice.common.exception;

public class RequestProcessingException extends Exception
{
     Throwable exceptionCause = null;

    public RequestProcessingException(String message)
    {
        super(message);
    }

    public RequestProcessingException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }

    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}