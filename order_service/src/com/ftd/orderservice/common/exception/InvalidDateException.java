package com.ftd.orderservice.common.exception;

public class InvalidDateException extends Exception
{
     Throwable exceptionCause = null;

    public InvalidDateException(String message)
    {
        super(message);
    }

    public InvalidDateException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }

    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}