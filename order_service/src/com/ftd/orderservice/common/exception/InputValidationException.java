package com.ftd.orderservice.common.exception;

public class InputValidationException extends Exception
{
     Throwable exceptionCause = null;

    public InputValidationException(String message)
    {
        super(message);
    }

    public InputValidationException(String msg, Throwable exception){
        super(msg, exception);
        exceptionCause = exception;
    }

    /**Overriding the printStackTraceMethod*/
    public void printStackTrace(){
        if (exceptionCause!=null){
            System.err.println("An exception has been caused by: " + exceptionCause.toString());
            exceptionCause.printStackTrace();
        }
    }
}