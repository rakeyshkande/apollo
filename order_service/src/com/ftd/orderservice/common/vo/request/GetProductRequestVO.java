package com.ftd.orderservice.common.vo.request;


public class GetProductRequestVO extends OrderServiceRequestVO
{
    private String[] webProductIds;
    private String loadLevel;


    public void setLoadLevel(String loadLevel) {
        this.loadLevel = loadLevel;
    }

    public String getLoadLevel() {
        return loadLevel;
    }

    public void setWebProductIds(String[] webProductIds) {
        this.webProductIds = webProductIds;
    }

    public String[] getWebProductIds() {
        return webProductIds;
    }
    
    public String toWebProductIdStr() {
        StringBuffer ids = new StringBuffer(50);
        if(webProductIds != null && webProductIds.length > 0) {
            ids.append(webProductIds[0]);
            for(int i = 1; i < webProductIds.length; i++) {
                ids.append(",").append(webProductIds[i]);
            }
        }
        return ids.toString();
    }
    
    public StringBuffer getLogString(StringBuffer sb) {
        return super.getLogString(sb)
               .append(";webProductId=").append(toWebProductIdStr())
               .append(";loadLevel=").append(loadLevel);
    }
}
