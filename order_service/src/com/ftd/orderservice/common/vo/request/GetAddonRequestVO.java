package com.ftd.orderservice.common.vo.request;


public class GetAddonRequestVO extends OrderServiceRequestVO
{
    private String webProductId;

    public void setWebProductId(String webProductId) {
        this.webProductId = webProductId;
    }

    public String getWebProductId() {
        return webProductId;
    }
    
    public StringBuffer getLogString(StringBuffer sb) {
        return super.getLogString(sb)
              .append(";webProductId=").append(webProductId);
    }
}
