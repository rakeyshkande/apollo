package com.ftd.orderservice.common.vo.request;


public class OrderServiceRequestVO
{
    private String clientId;
    private String sourceCode;
    private String requestId;
    private boolean customRequestId;
    private String effectiveSourceCode;
    private String isValid;
    private String errorCode;
    private String errorFieldValue;
    private String errorMessage;
    private String actionType;
    private String requestStatus;

    public OrderServiceRequestVO () {
        customRequestId = false;
        isValid = "Y";
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setEffectiveSourceCode(String effectiveSourceCode) {
        this.effectiveSourceCode = effectiveSourceCode;
    }

    public String getEffectiveSourceCode() {
        return effectiveSourceCode;
    }

    public void setCustomRequestId(boolean customRequestId) {
        this.customRequestId = customRequestId;
    }

    public boolean isCustomRequestId() {
        return customRequestId;
    }
    
   /**
   * Invokes {@link #getLogString(StringBuffer)} and returns the resulting string.
   * Subclasses should generally override {@link #getLogString(StringBuffer)} instead of this method.
   * 
   * @return
   */
    public String toLogString() {
      // Buffer Size based on Sampling of lines in log file during Perf Testing found Log String Max=524, Min=327, Avg=360
      StringBuffer sb = new StringBuffer(600);
      return getLogString(sb).toString();
    }
    
   /**
   * Appends the Log information into the passed in string buffer. Sub-classes should override this method
   * invoke the superclass method, then append any sub-class information to the StringBuffer.
   * 
   * @param sb The string buffer to append information to.
   * @return Returns the passed in StringBuffer
   */
    public StringBuffer getLogString(StringBuffer sb) {
      return sb.append("Request Parameters: clientId=").append(clientId)
                      .append(";sourceCode=").append(sourceCode)
                      .append(";requestId=").append(requestId)
                      .append(";effectiveSourceCode=").append(effectiveSourceCode)
                      .append(";isValid=").append(isValid)
                      .append(";errorCode=").append(errorCode)
                      .append(";errorFieldValue=").append(errorFieldValue)
                      .append(";errorMessage=").append(errorMessage)
                      .append(";actionType=").append(actionType);
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }


    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorFieldValue(String errorFieldValue) {
        this.errorFieldValue = errorFieldValue;
    }

    public String getErrorFieldValue() {
        return errorFieldValue;
    }
    
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionType() {
        return actionType;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestStatus() {
        return requestStatus;
    }
}
