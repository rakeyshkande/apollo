package com.ftd.orderservice.common.vo.request;

import java.math.BigDecimal;


public class GetAvailableDaysRequestVO extends OrderServiceRequestVO
{
    private String webProductId;
    private String zipCode;
    private String countryId;
    private BigDecimal priceSelected;
    private String priceSelectedStr;
    private String zipCodeFormatted;
    private String countryIdFormatted;
    private String customerEmail;
    private String customerSignedInFlag;
    private String freeShipMemberInCartFlag;

	public void setWebProductId(String webProductId) {
        this.webProductId = webProductId;
    }

    public String getWebProductId() {
        return webProductId;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setPriceSelected(BigDecimal priceSelected) {
        this.priceSelected = priceSelected;
    }

    public BigDecimal getPriceSelected() {
        return priceSelected;
    }

    public void setZipCodeFormatted(String zipCodeFormatted) {
        this.zipCodeFormatted = zipCodeFormatted;
    }

    public String getZipCodeFormatted() {
        return zipCodeFormatted;
    }

    public void setCountryIdFormatted(String countryIdFormatted) {
        this.countryIdFormatted = countryIdFormatted;
    }

    public String getCountryIdFormatted() {
        return countryIdFormatted;
    }

    public void setPriceSelectedStr(String priceSelectedStr) {
        this.priceSelectedStr = priceSelectedStr;
    }

    public String getPriceSelectedStr() {
        return priceSelectedStr;
    }
    
    public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerSignedInFlag() {
		return customerSignedInFlag;
	}

	public void setCustomerSignedInFlag(String customerSignedInFlag) {
		this.customerSignedInFlag = customerSignedInFlag;
	}

	public String getFreeShipMemberInCartFlag() {
		return freeShipMemberInCartFlag;
	}

	public void setFreeShipMemberInCartFlag(String freeShipMemberInCartFlag) {
		this.freeShipMemberInCartFlag = freeShipMemberInCartFlag;
	}    
    
    public StringBuffer getLogString(StringBuffer sb) {
        return super.getLogString(sb)
               .append(";webProductId=").append(webProductId)
               .append(";zipCode=").append(zipCode)
               .append(";countryId=").append(countryId)
               .append(";priceSelectedStr=").append(priceSelectedStr)
               .append(";customerEmail=").append(customerEmail)
               .append(";customerSignedInFlag=").append(customerSignedInFlag)
               .append(";freeShipMemberInCartFlag").append(freeShipMemberInCartFlag);
    }
}
