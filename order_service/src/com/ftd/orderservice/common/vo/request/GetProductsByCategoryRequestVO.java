package com.ftd.orderservice.common.vo.request;

import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductOrderByEnum;

import java.util.Date;


public class GetProductsByCategoryRequestVO extends OrderServiceRequestVO
{
    private String categoryId;
    private String zipCode;
    private String zipCodeFormatted;
    private String countryId;
    private String countryIdFormatted;
    private Date deliveryDate;
    private String deliveryDateStr;
    private String productOrderByStr;
    private ProductOrderByEnum productOrderBy;

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDateStr(String deliveryDateStr) {
        this.deliveryDateStr = deliveryDateStr;
    }

    public String getDeliveryDateStr() {
        return deliveryDateStr;
    }

    public void setProductOrderBy(ProductOrderByEnum productOrderBy) {
        this.productOrderBy = productOrderBy;
    }

    public ProductOrderByEnum getProductOrderBy() {
        return productOrderBy;
    }

    public void setProductOrderByStr(String productOrderByStr) {
        this.productOrderByStr = productOrderByStr;
    }

    public String getProductOrderByStr() {
        return productOrderByStr;
    }

    public void setZipCodeFormatted(String zipCodeFormatted) {
        this.zipCodeFormatted = zipCodeFormatted;
    }

    public String getZipCodeFormatted() {
        return zipCodeFormatted;
    }

    public void setCountryIdFormatted(String countryIdFormatted) {
        this.countryIdFormatted = countryIdFormatted;
    }

    public String getCountryIdFormatted() {
        return countryIdFormatted;
    }
    
    public StringBuffer getLogString(StringBuffer sb) {
        return super.getLogString(sb)
               .append(";categoryId=").append(categoryId)
               .append(";zipCode=").append(zipCode)
               .append(";countryId=").append(countryId)
               .append(";deliveryDateStr=").append(deliveryDateStr)
               .append(";productOrderByStr=").append(productOrderByStr);
    }
}
