package com.ftd.orderservice.common.vo.response;

import java.util.List;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;


public class GetProductsByCategoryResponseVO extends OrderServiceResponseVO
{
    private List<ProductMasterVO> productMasterList;
    private String indexId;
    private String indexName;

    public void setProductMasterList(List<ProductMasterVO> productMasterList) {
        this.productMasterList = productMasterList;
    }

    public List<ProductMasterVO> getProductMasterList() {
        return productMasterList;
    }
   
    public void setIndexId(String indexId) {
        this.indexId = indexId;
    }

    public String getIndexId() {
        return indexId;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }
}
