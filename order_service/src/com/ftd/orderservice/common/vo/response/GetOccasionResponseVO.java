package com.ftd.orderservice.common.vo.response;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;

import java.util.List;


public class GetOccasionResponseVO extends OrderServiceResponseVO
{
    private List<OccasionVO> occasionList;


    public void setOccasionList(List<OccasionVO> occasionList) {
        this.occasionList = occasionList;
    }

    public List<OccasionVO> getOccasionList() {
        return occasionList;
    }
}
