package com.ftd.orderservice.common.vo.response;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.AvailableDateVO;

import java.util.List;


public class GetAvailableDaysResponseVO extends OrderServiceResponseVO
{
    private List<AvailableDateVO> availDateList;
    private String displayServiceFeeFlag;
    private String displayShippingFeeFlag;

    public String getDisplayServiceFeeFlag() {
		return displayServiceFeeFlag;
	}

	public void setDisplayServiceFeeFlag(String displayServiceFeeFlag) {
		this.displayServiceFeeFlag = displayServiceFeeFlag;
	}

	public String getDisplayShippingFeeFlag() {
		return displayShippingFeeFlag;
	}

	public void setDisplayShippingFeeFlag(String displayShippingFeeFlag) {
		this.displayShippingFeeFlag = displayShippingFeeFlag;
	}

	public void setAvailDateList(List<AvailableDateVO> availDateList) {
        this.availDateList = availDateList;
    }

    public List<AvailableDateVO> getAvailDateList() {
        return availDateList;
    }
    
    
}
