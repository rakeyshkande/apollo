package com.ftd.orderservice.common.vo.response;


import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;



import java.util.List;


public class GetAddonResponseVO extends OrderServiceResponseVO
{
    private List<AddonVO> addonList;

    public void setAddonList(List<AddonVO> addonList) {
        this.addonList = addonList;
    }

    public List<AddonVO> getAddonList() {
        return addonList;
    }
    
    public void addCDATATags(){
        for(AddonVO adonVO : addonList)
        {
            adonVO.setAddonTitle("<![CDATA[" + adonVO.getAddonTitle() + "]]>");
            adonVO.setAddonDesc("<![CDATA[" + adonVO.getAddonDesc() + "]]>");
        }    
    };
}
