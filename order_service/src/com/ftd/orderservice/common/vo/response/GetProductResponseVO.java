package com.ftd.orderservice.common.vo.response;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;

import java.util.List;


public class GetProductResponseVO extends OrderServiceResponseVO
{
    private List<ProductMasterVO> productMasterList;

    public void setProductMasterList(List<ProductMasterVO> productMasterList) {
        this.productMasterList = productMasterList;
    }

    public List<ProductMasterVO> getProductMasterList() {
        return productMasterList;
    }
    
    public void addCDATATags(){
        for(ProductMasterVO productMasterVO : productMasterList)
        {
            productMasterVO.setWebProductName("<![CDATA[" + productMasterVO.getWebProductName() + "]]>");
            productMasterVO.setLongDesc("<![CDATA[" + productMasterVO.getLongDesc() + "]]>");
        }    
    };
}
