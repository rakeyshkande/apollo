package com.ftd.orderservice.common.vo.response;

import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;

import org.w3c.dom.Document;


public abstract class OrderServiceResponseVO
{
    private OrderServiceRequestVO requestVO;

    public void setRequestVO(OrderServiceRequestVO requestVO) {
        this.requestVO = requestVO;
    }

    public OrderServiceRequestVO getRequestVO() {
        return requestVO;
    }
    
    /*
    public Document getRequestDoc() throws Exception {
        return requestVO.toXMLDoc();
    }
    */
    public void addCDATATags(){};
}
