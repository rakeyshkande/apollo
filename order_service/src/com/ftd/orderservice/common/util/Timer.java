package com.ftd.orderservice.common.util;

import java.util.Date;

public class Timer
{
    Date timerStart;
    Date timerStop;

    public Timer()
    {
        timerStart = new Date();
    }

    public void stop()
    {
        timerStop = new Date();
    }

    public long getMilliseconds()
    {
        return timerStop.getTime() - timerStart.getTime();
    }
}
