package com.ftd.orderservice.common.util;

import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.exception.RequestProcessingException;
import com.ftd.orderservice.common.transformer.ServiceResultTransformer;
import com.ftd.orderservice.common.vo.request.OrderServiceRequestVO;
import com.ftd.orderservice.common.vo.response.OrderServiceResponseVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.StringWriter;

import java.net.InetAddress;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;


public class CommonUtil {
    protected static Logger logger  = new Logger("com.ftd.orderservice.common.util.CommonUtil");
    private static final int PAD_RIGHT = 0; 
    private static final int PAD_LEFT = 1; 
    private static final int lowerBound = 1000;
    private static final int upperBound = 10000;
    
    public void sendPageSystemMessage(String logMessage) {
        Connection con = null;
        try {
             con = this.getNewConnection();
             String appSource = OrderServiceConstants.SM_PAGE_SOURCE;
             String errorType = OrderServiceConstants.SM_TYPE;
             String subject = OrderServiceConstants.SM_PAGE_SUBJECT;
             int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

             SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
             systemMessengerVO.setLevel(pageLevel);
             systemMessengerVO.setSource(appSource);
             systemMessengerVO.setType(errorType);
             systemMessengerVO.setSubject(subject);
             systemMessengerVO.setMessage(logMessage);
             String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);
         
         } catch (Exception ex) {
            // Do not attempt to send system message it requires obtaining a connection
            // and may end up in an infinite loop.
             logger.error(ex);
         } finally {
             if(con != null) {
                try {
                    con.close();
                } catch (SQLException sx) {
                    logger.error(sx);
                }
             }
         }
    
    }
    
    public void sendNoPageSystemMessage(String logMessage) {
        Connection con = null;
        try {
             con = this.getNewConnection();
             String appSource = OrderServiceConstants.SM_PAGE_SOURCE;
             String errorType = OrderServiceConstants.SM_TYPE;
             String subject = OrderServiceConstants.SM_NOPAGE_SUBJECT;
             int pageLevel = SystemMessengerVO.LEVEL_PRODUCTION;

             SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
             systemMessengerVO.setLevel(pageLevel);
             systemMessengerVO.setSource(appSource);
             systemMessengerVO.setType(errorType);
             systemMessengerVO.setSubject(subject);
             systemMessengerVO.setMessage(logMessage);
             String result = SystemMessenger.getInstance().send(systemMessengerVO, con, false);
         
         } catch (Exception ex) {
            // Do not attempt to send system message it requires obtaining a connection
            // and may end up in an infinite loop.
             logger.error(ex);
         } finally {
             if(con != null) {
                try {
                    con.close();
                } catch (SQLException sx) {
                    logger.error(sx);
                }
             }
         }
    
    }
    
    /**
     * Get a new database connection.
     * @return
     * @throws Exception
     */
    public Connection getNewConnection() throws Exception
    {
        // get database connection
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        Connection conn = null;

        String datasource = configUtil.getPropertyNew(OrderServiceConstants.PROPERTY_FILE, OrderServiceConstants.DATASOURCE_NAME);
        conn = DataSourceUtil.getInstance().getConnection(datasource);

        return conn;
    }    
    
    /**
     * Get today's date formated
     * @return
     */
    public static String now() throws Exception {
      return getDateFormated(OrderServiceConstants.DATE_FORMAT_NOW, 0);
    }    

    /**
     * Get a formated date relative to today.
     * @return
     */
    public static String getDateFormated(String format, int offset) throws Exception {
      Date dt = getDate(offset);
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      return sdf.format(dt);
    }  
    
    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Date getDate(int offset) {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, offset);
      return cal.getTime();
    }  

    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Date getDateTruncated(int offset) throws Exception {
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DATE, offset);
      
      SimpleDateFormat sdf = new SimpleDateFormat(OrderServiceConstants.DATE_FORMAT_DD);
      String dateString = sdf.format(cal.getTime());
      Date truncatedDate = sdf.parse(dateString);
      return truncatedDate;
    }  
    
    public static String dateToString(Date dt) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(OrderServiceConstants.DATE_FORMAT_DD);
        return sdf.format(dt);
    }
    
    /**
     * Returns a date relative to today.
     * @param offset
     * @return
     */
    public static Calendar getCalendarTruncated(int offset) throws Exception {
      Calendar cal = Calendar.getInstance();
      Date dt = getDateTruncated(offset);
      cal.setTime(dt);
      return cal;
    }      
    /*
    public BigDecimal getGBBPriceDiff(String token, BigDecimal standard, BigDecimal deluxe, BigDecimal premium) throws Exception {
        BigDecimal priceDiff = null;
        if(OrderServiceConstants.GBB_TOKEN_1.equals(token)) {
            // deluxe price - standard price
            priceDiff = deluxe.subtract(standard);
        } else if(OrderServiceConstants.GBB_TOKEN_2.equals(token)) {
            // premium price - standard price
            priceDiff = premium.subtract(standard);
        } else if(OrderServiceConstants.GBB_TOKEN_3.equals(token)) {
            // premium price - deluxe price
            priceDiff = premium.subtract(deluxe);
        } else {
            priceDiff = new BigDecimal("0");
        }
        return priceDiff;
    }  
    */
    public String getFrpGlobalParm(String context, String name) throws CacheException, Exception {
        CacheUtil cacheUtil = CacheUtil.getInstance();
        return cacheUtil.getGlobalParm(context, name);
    }
    
    public String getContentWithFilter(String context, String name, String filter1, String filter2) throws Exception {
        return getContentWithFilter(context, name, filter1, filter2, false);
    }    
    
    public String getContentWithFilter(String context, String name, String filter1, String filter2, boolean missingContentOk) throws Exception {
        
        CacheUtil cacheUtil = CacheUtil.getInstance();
        return cacheUtil.getContentWithFilter(context, name, filter1, filter2, missingContentOk);
        
    }   
    
    public String transform(OrderServiceResponseVO respVO) throws Exception {
        ServiceResultTransformer transformer = new ServiceResultTransformer();
                
        if(logger.isInfoEnabled()) {
          String logString = getLogString(respVO.getRequestVO(), OrderServiceConstants.STATS_CAT_REQUEST_PROCESSED, respVO.getRequestVO().getRequestStatus(), null);
          logger.info(logString);
        }
        
        return transformer.transform(respVO);
    }    
    
    public String transformError(OrderServiceRequestVO reqVO) throws Exception {
        ServiceResultTransformer transformer = new ServiceResultTransformer();

        String errorMsg = reqVO.getErrorMessage();
        if(errorMsg == null) {
            errorMsg = reqVO.getErrorCode();
        }
        if(errorMsg == null) {
            errorMsg = "";
        }
                
        if(logger.isInfoEnabled()) {
          String logString = getLogString(reqVO, OrderServiceConstants.STATS_CAT_REQUEST_PROCESSED, reqVO.getRequestStatus(), errorMsg);
          logger.info(logString);
        }
        
        return transformer.transformError(reqVO);
    }    
    
    public void logCacheRequest(OrderServiceRequestVO reqVO) throws Exception {

        if(logger.isInfoEnabled()) {
          String logString = getLogString(reqVO, OrderServiceConstants.STATS_CAT_TRANS_CACHE_HIT, reqVO.getRequestStatus(), null);
          logger.info(logString);
        }
    }
    
    /**
     * Create a request id with a 4 digit random number concatenated with a 6 digit timestamp of HHMMSS.
     * @return
     * @throws Exception
     */
    public String generateRequestId() throws Exception {
        // Generate a random number >= lowerBound and < upperBound.
        int randomNumber = lowerBound + (int)(Math.random()*(upperBound - lowerBound));
        
        // Generate a random number based on timestamp.
        Calendar cal = Calendar.getInstance();
        String hour = padTime(cal.get(Calendar.HOUR_OF_DAY));
        String minute = padTime(cal.get(Calendar.MINUTE));
        String second = padTime(cal.get(Calendar.SECOND));

        return randomNumber + hour + minute + second;
    }
    
    private String padTime(int time) throws Exception {
        return pad(String.valueOf(time), PAD_LEFT, "0", 2);
    }
    
    public static String pad(String value,int padSide, String padChar, int size) throws Exception 
    {
     //null check
     if(value == null)
        value = "";
       
     String padded = value;       
    
     if(value.length() > size) {
        if(padSide == PAD_RIGHT) {
          padded = value.substring(0,size);
        }
        else {
          padded = value.substring(padded.length()-size);
        }
     }
     else
     {
       for(int i=value.length();i<size;i++)
       {
         if(padSide == PAD_LEFT)
            padded = padChar + padded;
         else
            padded = padded + padChar;
       }//end for loop
     }//end else less then max size
    
     return padded;
    }    
    
    /**
     * Create a document for given root name.
     * @param rootName
     * @return
     */
    public Document createDocument(String rootName) throws Exception {
        Document doc = DOMUtil.getDefaultDocument();
        Element root = doc.createElement(rootName);
        doc.appendChild(root);
        
        return doc;
    }    
    
    
    public Element getFirstElementByTagName(Document doc, String tagName) throws Exception {
        return (Element)(doc.getElementsByTagName(tagName).item(0));
    }  
    
    public Element createElement(Document doc, String elemName) {
        Element elem = doc.createElement(elemName);
        return elem;
    }    
    
    /**
     * Create an element as child of parent for input document.
     * @param doc
     * @param elemName
     * @param elemValue
     * @return
     */
    public Element createElement(Document doc, String elemName, String elemValue) {
    
        Element elem = doc.createElement(elemName);
        if(elemValue == null) {
            elemValue = "";
        }
        elemValue = DOMUtil.encodeChars(elemValue);
        Text indexIdText = doc.createTextNode(elemValue);
        elem.appendChild(indexIdText);
        
        return elem;
    }
    
    /**
     * Convert a document to string.
     * @param doc
     * @return
     * @throws Exception
     */
    public String toXMLString (Document doc) throws Exception {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //initialize StreamResult with File object to save to file
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);

            String xmlString = result.getWriter().toString();
            return xmlString;

        } catch (Exception e) {
            throw new RequestProcessingException("Exception caught in CommonUtil.toXMLString.", e);
        }
        
    }
    
    public String getImageFormat(String url) throws Exception {
        if(url != null) {
            return "image/" + url.substring(url.lastIndexOf(".") + 1);
        }
        return null;
    }
    
    public String listToDelimitedString(List<String> list, String delimiter) throws Exception {
        String str = "";
        if(list != null) {
            for(int i = 0; i < list.size(); i++) {
                str += list.get(i);
                if(i < (list.size() - 1)) {
                    str += delimiter;
                }
            }
        }
        return str;
    }
    
    public String getLogString (OrderServiceRequestVO req, String statsCategory, String status, String errorMessage) throws Exception {
        // Buffer Size based on Sampling of lines in log file during Perf Testing found Log String Max=524, Min=327, Avg=360
        StringBuffer sb = new StringBuffer(600); 
        sb.append("APPLICATION=" + OrderServiceConstants.APPLICATION_NAME + ";");
        sb.append("NODE=" + InetAddress.getLocalHost().getHostName() + ";");
        sb.append("CLIENT_ID=" + req.getClientId() + ";");
        sb.append("ACTION_TYPE=" + req.getActionType() + ";");
        sb.append("STATS_CATEGORY=" + statsCategory);
        
        // Append the Request Details
        sb.append(";");
        req.getLogString(sb); 
        
        // Append the Status
        sb.append(";").append(status);
        
        // Append the error message if there is one
        if(errorMessage != null) {
          sb.append(";").append(errorMessage);
        }
        
        return sb.toString();
    }
    
    /**
     * Find the default source code for company of the given source code.
     * @param sourceCode
     * @return
     * @throws Exception
     */
    public String getCompanyDefaultDomain(String sourceCode) throws Exception {
        CacheUtil cacheUtil = CacheUtil.getInstance();
        SourceMasterVO smVO = cacheUtil.getSourceCodeById(sourceCode);
        
        if(smVO == null) {
            String errorMsg = "Error in getCompanyDefaultSourceCode - source code not found in cache: " + sourceCode;
            logger.error(errorMsg);
            this.sendNoPageSystemMessage(errorMsg);
            return "ftd";
        }
        String defaultCompany = smVO.getCompanyId();


        CompanyMasterVO cmVO = cacheUtil.getCompanyById(defaultCompany);
        
        if(cmVO == null) {
            String errorMsg = "Error in getCompanyDefaultSourceCode - company not found in cache: " + defaultCompany;
            logger.error(errorMsg);
            this.sendNoPageSystemMessage(errorMsg);
            return "ftd";
        }
        
        return cmVO.getDefaultDomain();
    }
}    