package com.ftd.orderservice.common.dao;

import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.sql.Connection;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class ProductDAO
{
    private static Logger logger =
        new Logger("com.ftd.orderservice.dao.ProductDAO");

    public Document getProductByWebId(Connection con, String productId) throws Exception
    {
    // call FTD_APPS.ORDER_SERVICE_QUERY_PKG.GET_PRODUCT_INTO_BY_WEB_ID
        if(logger.isDebugEnabled())
          logger.debug("Entering getProductInfo");
        Document doc = null;

        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID(OrderServiceConstants.DB_GET_PRODUCT_INFO);
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            HashMap inputParams = new HashMap();

            inputParams.put("IN_PRODUCT_ID", productId);
            if(logger.isDebugEnabled())
              logger.debug("productId=" + productId);
            dataRequest.setInputParams(inputParams);
            HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);

            doc = DOMUtil.getDefaultDocument();
            Element root = doc.createElement("root");
            doc.appendChild(root);


            Element sub = doc.createElement("OUT_PRODUCT_CUR");
            root.appendChild(sub);
            NodeList nl = ((Document)outMap.get("OUT_PRODUCT_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_KEYWORD_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_KEYWORD_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_COLOR_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_COLOR_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_RECIPIENT_SEARCH_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_RECIPIENT_SEARCH_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_COMPANY_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_COMPANY_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_EXCL_STATES_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_EXCL_STATES_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_SHIP_METHODS_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_SHIP_METHODS_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_SUB_CODES_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_SUB_CODES_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_SHIP_DATES_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_SHIP_DATES_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_VENDORS_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_VENDORS_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_PARTNER_PRODUCT_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_PARTNER_PRODUCT_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

            sub = doc.createElement("OUT_COMPONENT_CUR");
            root.appendChild(sub);
            nl = ((Document)outMap.get("OUT_COMPONENT_CUR")).getElementsByTagName("PRODUCT");
            if(nl != null && nl.item(0) != null) {
                sub.appendChild(doc.importNode(nl.item(0), true));
            }

      //((XMLDocument)doc).print(System.out);

            dataRequest.reset();

            if(logger.isDebugEnabled())
              logger.debug("Exiting getProductInfo");
        } finally {

        }
        return  doc;
    }

    public CachedResultSet getProductInfoRS(Connection con, String productId) throws Exception
    {
        if(logger.isDebugEnabled())
          logger.debug("Entering getProductInfo");
        CachedResultSet rs = null;

        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(con);
            dataRequest.setStatementID("PDB_GET_PRODUCT_DETAILS");
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            HashMap inputParams = new HashMap();

            inputParams.put("PRODUCT_ID", productId);
            if(logger.isDebugEnabled())
              logger.debug("productId=" + productId);
            dataRequest.setInputParams(inputParams);
            HashMap outMap = (HashMap) dataAccessUtil.execute(dataRequest);



            rs = (CachedResultSet)outMap.get("OUT_PRODUCT_CUR");


            dataRequest.reset();

            if(logger.isDebugEnabled())
              logger.debug("Exiting getProductInfo");
        } finally {

        }
        return  rs;
    }

}