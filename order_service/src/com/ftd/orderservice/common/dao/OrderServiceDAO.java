package com.ftd.orderservice.common.dao;

import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductIndexVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OrderServiceDAO
{
    protected static Logger logger  = 
        new Logger("com.ftd.orderservice.common.dao.OrderServiceDAO");
        

    public Object executeQueryReturnObject(Connection conn, String statement, Map inputs) throws Exception {

        Object output = null;
        try {
            DataRequest dataRequest = new DataRequest();
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(statement);
            dataRequest.setInputParams(inputs);

            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

            if(logger.isDebugEnabled())
              logger.debug("Calling statement: " + statement);
            output = dataAccessUtil.execute(dataRequest);
            dataRequest.reset();
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting executeQueryReturnObject:" + statement);
            }
        }
        return output;

    }
    
    public List<OccasionVO> getOccasionList(Connection conn, String sourceCode, String indexName, String defaultDomain) throws Exception
    {
        if(logger.isDebugEnabled()) {
            logger.debug("getOccasionList(" + sourceCode + "," + indexName + "," + defaultDomain + ")");
        }
        CachedResultSet output = null;
        List<OccasionVO> subIndexList = null;
        try {
            
            Map inputs = new HashMap();
            inputs.put("IN_SOURCE_CODE", sourceCode);
            inputs.put("IN_INDEX_NAME", indexName);
            inputs.put("IN_DEFAULT_DOMAIN", defaultDomain);
            
            if(logger.isDebugEnabled())
              logger.debug("Calling statement: GET_INDEX_SUB_LIST");
              
            output = (CachedResultSet)executeQueryReturnObject(conn, "GET_INDEX_SUB_LIST", inputs);

            if(output != null) {
           
                subIndexList = new ArrayList<OccasionVO>();
                while(output != null && output.next()) {
                    OccasionVO ovo = new OccasionVO();
                    ovo.setIndexName(output.getString("index_desc"));
                    ovo.setIndexId(output.getString("sub_index_name"));

                    subIndexList.add(ovo);
                }
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting getOccasionList");
            }
        }
        return subIndexList;        
    
    }
    
    
    public ProductIndexVO getIndexByIdAndSource(Connection conn, String sourceCode, String indexName, String defaultDomain) throws Exception
    {
        CachedResultSet output = null;
        ProductIndexVO index = null;
        try {
            
            Map inputs = new HashMap();
            inputs.put("IN_SOURCE_CODE", sourceCode);
            inputs.put("IN_INDEX_NAME", indexName);
            inputs.put("IN_DEFAULT_DOMAIN", defaultDomain);
            
            if(logger.isDebugEnabled()) {
              logger.debug("Calling statement: GET_INDEX_BY_ID_AND_SOURCE");
              logger.debug("getIndexByIdAndSource(" + sourceCode + "," + indexName + "," + defaultDomain + ")");
            }
              
            output = (CachedResultSet)executeQueryReturnObject(conn, "GET_INDEX_BY_ID_AND_SOURCE", inputs);

            if(output != null && output.next()) {
                index = new ProductIndexVO();
                index.setIndexName(output.getString("index_name"));
                index.setIndexDesc(output.getString("index_desc"));
                index.setCountryId(output.getString("country_id"));
                
            }
        } finally {
            if (logger.isDebugEnabled()) {
                logger.debug("Exiting getIndexByIdAndSource(" + sourceCode + "," + indexName + "," + defaultDomain);
            }
        }
        return index;        
    
    }  

}
