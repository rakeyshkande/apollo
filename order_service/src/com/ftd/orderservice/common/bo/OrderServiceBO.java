package com.ftd.orderservice.common.bo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ftd.orderservice.common.constant.OrderServiceConstants;
import com.ftd.orderservice.common.dao.OrderServiceDAO;
import com.ftd.orderservice.common.exception.RespondWithErrorException;
import com.ftd.orderservice.common.util.CommonUtil;
import com.ftd.orderservice.common.util.IndexOrderComparator;
import com.ftd.orderservice.common.vo.request.GetAddonRequestVO;
import com.ftd.orderservice.common.vo.request.GetAvailableDaysRequestVO;
import com.ftd.orderservice.common.vo.request.GetOccasionRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductRequestVO;
import com.ftd.orderservice.common.vo.request.GetProductsByCategoryRequestVO;
import com.ftd.orderservice.common.vo.response.GetAddonResponseVO;
import com.ftd.orderservice.common.vo.response.GetAvailableDaysResponseVO;
import com.ftd.orderservice.common.vo.response.GetOccasionResponseVO;
import com.ftd.orderservice.common.vo.response.GetProductResponseVO;
import com.ftd.orderservice.common.vo.response.GetProductsByCategoryResponseVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.dao.RecalculateOrderCacheDAO;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.DeliveryMethodEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ImageTypeEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.PriceTypeEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.enums.ProductOrderByEnum;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AddonVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AvailableDateVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ColorMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CompanyMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.CountryMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.GBBVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.IOTWVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ImageVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.OccasionVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PricingVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductIndexVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.ProductMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.SourceMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.StateMasterVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellDetailVO;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.UpsellMasterVO;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.pas.common.vo.ProductAvailVO;
import com.ftd.pas.server.service.impl.PASRequestServiceImpl;
import com.ftd.pas.web.util.Timer;


public class OrderServiceBO
{
    private static Logger logger =
        new Logger("com.ftd.orderservice.bo.OrderServiceBO");
    private CommonUtil commonUtil = new CommonUtil();
    private CacheUtil cacheUtil = CacheUtil.getInstance();
    private OrderServiceDAO osDAO = new OrderServiceDAO();
    
    private String FTDAPPS_PARMS_CONTEXT = "FTDAPPS_PARMS";
    private String GNADD_LEVEL = "GNADD_LEVEL";
    private String GNADD_DATE = "GNADD_DATE";
    private String DATE_FORMAT_NO_DATE_RANGE = "MMM dd - E";
    private String DATE_FORMAT_DATE_RANGE = "MMM dd EEE.";
    private String DATE_FORMAT_TODAY = "MMM dd - ";
    
    private String DEFAULT_COUNTRY = "US";
    private String DEFAULT_STATE = "IL";

    /**
     * Return occasion list for given source code. Format based on client.
     * If results in cache, return it. Otherwise, build it.
     * @param reqVO
     * @return
     * @throws Exception
     */
    public String getOccasion(GetOccasionRequestVO reqVO) throws Exception {
        String clientId = reqVO.getClientId();
        String sourceCode = reqVO.getSourceCode();
        sourceCode = sourceCode.toUpperCase();
        String responseString = cacheUtil.getOccasionTransient(clientId, sourceCode);

        if(logger.isDebugEnabled())
          logger.debug("found level 1 cache?" + (responseString != null));

        if(responseString == null) {
            // Not found in level 1 cache. Build it.
            GetOccasionResponseVO respVO = new GetOccasionResponseVO();
            respVO.setRequestVO(reqVO);
            

            List<OccasionVO> occasionList = this.buildGetOccasion(sourceCode);
            respVO.setOccasionList(occasionList);

            if(logger.isDebugEnabled())
              logger.debug("occasionList size:" + occasionList.size());

            // Transform results.
            responseString = commonUtil.transform(respVO);

            // Store results back in level 1 cache.
            cacheUtil.setOccasionTransient(clientId, sourceCode, responseString);
        } else {
            commonUtil.logCacheRequest(reqVO);
        }
        return responseString;

    }

    /**
     * Retrieve a list of occasions for given source code.
     * @param sourceCode
     * @return
     * @throws Exception
     */
    private List<OccasionVO> buildGetOccasion(String sourceCode) throws Exception {
        // Retrieve the global parm "occasion" index name.
        String occasionIndexName = commonUtil.getFrpGlobalParm(OrderServiceConstants.ORDER_SERVICE_CONFIG, OrderServiceConstants.GET_OCCASION_INDEX_NAME);
        if(occasionIndexName == null) {
            occasionIndexName = "occasion";
            commonUtil.sendPageSystemMessage("Global parm not set:" + OrderServiceConstants.ORDER_SERVICE_CONFIG + ":" 
            + OrderServiceConstants.GET_OCCASION_INDEX_NAME + ". Defaulting to: occasion");
        }
        if(logger.isDebugEnabled())
          logger.debug("occasionIndexName is :" + occasionIndexName);
        // Check if there are occasions for this source code.
        String defaultCompanyDomain = commonUtil.getCompanyDefaultDomain(sourceCode);
        Connection conn = null;
        List<OccasionVO> ocList = null;
        
        try {
            conn = commonUtil.getNewConnection();
            ocList = osDAO.getOccasionList(conn, sourceCode, occasionIndexName, defaultCompanyDomain);
        } finally {
            if(conn != null) {
                conn.close();
            }
        }
        

        return ocList;
    }

    /**
     * Return product information. Format based on client.
     * If results in cache, return it. Otherwise, build it.
     * @param reqVO
     * @return
     * @throws Exception
     */
    public String getProduct(GetProductRequestVO reqVO) throws Exception {
        String clientId = reqVO.getClientId();
        String sourceCode = reqVO.getSourceCode();
        String loadLevel = reqVO.getLoadLevel();
        String[] webProductIds = reqVO.getWebProductIds();
        
        // Look up results in level 1 cache.
        String responseString = cacheUtil.getProductTransient(clientId, sourceCode, webProductIds, loadLevel);
        
        if(logger.isDebugEnabled())
          logger.debug("getProduct found transient cache?" + (responseString == null? "N" : "Y"));
        if(responseString == null) {
            // Not found in level 1 cache. Build it.
            GetProductResponseVO respVO = new GetProductResponseVO();
            respVO.setRequestVO(reqVO);
            List<ProductMasterVO> productList = new ArrayList();
            if(logger.isDebugEnabled())
              logger.debug("getProduct webProductIds size:" + webProductIds.length);
            for(int i=0; i<webProductIds.length; i++) {
                String webProductId = webProductIds[i];

                // retrieve product information from cache for product id.
                //
                if(logger.isDebugEnabled())
                {
                  logger.debug("getProduct sourceCode:" + reqVO.getSourceCode());
                  logger.debug("getProduct webProductId:" + webProductId);
                }
                try {
                    ProductMasterVO pmvo = buildGetProductById(reqVO.getSourceCode(), webProductId, reqVO.getLoadLevel());
                    if(pmvo != null) {
                        productList.add(pmvo);
                    }
                } catch (Exception e) {
                    // Bypass that product if it fails.
                    String msg = "Encountered error while trying to retrieve product: " + webProductId + e.getMessage();
                    logger.error(e);
                    commonUtil.sendNoPageSystemMessage(msg);
                }

            }

            respVO.setProductMasterList(productList);

            // Transform results.
            responseString = commonUtil.transform(respVO);

            // Store results back in level 1 cache.
            cacheUtil.setProductTransient(clientId, sourceCode, webProductIds, loadLevel, responseString);
        } else {
            commonUtil.logCacheRequest(reqVO);
        }
        return responseString;

    }

    public ProductMasterVO buildGetProductById(String sourceCode, String webProductId, String loadLevel) throws Exception {
        // First check if webProductId passed in is an upsell master id.
        String productId = null;
        UpsellMasterVO upsellMasterVO = cacheUtil.getUpsellMasterByMasterId(webProductId);
        ProductMasterVO pmvo = null;
        String iotwProductId = null;
        String iotwUpsellMasterId = null;
        
        if(upsellMasterVO != null) {
            productId = upsellMasterVO.getDefaultUpsellDetailId();
            if(logger.isDebugEnabled())
              logger.debug("default upsell detail id:" + productId);
            if(productId == null) {
                commonUtil.sendNoPageSystemMessage("Error in buildGetProductById - no default product id for upsell master id: " + webProductId);
                return null;
            }
            pmvo = cacheUtil.getProductByWebProductId(cacheUtil.getWebProductIdByProductId(productId));
            
            if(pmvo == null) {
                commonUtil.sendNoPageSystemMessage("Error in buildGetProductById - default product for upsell master id not found in cache: " + productId);
                return null;
            }
            
            pmvo.setWebProductId(webProductId);
            iotwUpsellMasterId = webProductId; //webProductId is an upsell master id
        } else {

            productId = cacheUtil.getProductIdByWebProductId(webProductId);

            
            if(productId == null) {
                commonUtil.sendNoPageSystemMessage("Error in buildGetProductById - no product id for given webProductId: " + webProductId);
                return null;
            }

            pmvo = cacheUtil.getProductByWebProductId(webProductId);
            if(pmvo == null) {
                commonUtil.sendNoPageSystemMessage("Error in buildGetProductById - product not found in cache: " + productId);
                return null;
            }
            iotwProductId = productId;
        }

        // Attach product color
        List<ColorMasterVO> colorList = cacheUtil.getProductColor(productId);
        if (colorList != null) {
            pmvo.setColorList(colorList);
            
            for(int i = 0; i < colorList.size(); i++) {
                ColorMasterVO cm = colorList.get(i);
            }
        }


        // Attach product company
        List<CompanyMasterVO> companyList = cacheUtil.getProductCompany(productId);
        pmvo.setCompanyList(companyList);


        // Attach product country if product is international
        List<CountryMasterVO> countryList = cacheUtil.getProductCountry(productId);
        pmvo.setCountryList(countryList);

        // Attach product keyword
        List<String> keywordList = null;
        if(upsellMasterVO != null) {
            //Upsell keywords are from upsell master id.
            keywordList = cacheUtil.getProductKeyword(webProductId);
        } else {
            //Non upsell keywords are from apollo product id.
            keywordList = cacheUtil.getProductKeyword(productId);
        }
        pmvo.setKeywordList(keywordList);

        // Attach IOTW info
        IOTWVO iotw = cacheUtil.getIOTW(sourceCode, iotwProductId, iotwUpsellMasterId);
        String effectiveSourceCode = sourceCode;
        String productPageMessaging = null;

        if(iotw != null) {
            effectiveSourceCode = iotw.getIOTWSourceCode();
            productPageMessaging = iotw.getProductPageMessaging();
            String contentProductPageMessaging = null;

            try {
                contentProductPageMessaging = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_CONTEXT, OrderServiceConstants.IOTW_MESSAGING_API, null, null);
            } catch (Exception e) {
                //page
                commonUtil.sendPageSystemMessage("Global parm not set:" + OrderServiceConstants.ORDER_SERVICE_CONFIG + ":" 
                 + OrderServiceConstants.IOTW_MESSAGING_API + ". Defaulting to: " + OrderServiceConstants.IOTW_MESSAGING_API);
                contentProductPageMessaging = OrderServiceConstants.IOTW_MESSAGING_API_DEFAULT;
            }
            // If IOTW source code does not specify product page messaging, use global value.
            if(productPageMessaging == null) {
                productPageMessaging = contentProductPageMessaging;
            }
            if(logger.isDebugEnabled())
              logger.debug("productPageMessaging = " + productPageMessaging);
            pmvo.setIOTWMessaging(productPageMessaging);
        }

        // Attach floral price points
         List<GBBVO> gbbList = null;
         
         //Image URLs
         String imageBaseCategoryIntl = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                               OrderServiceConstants.NAME_IMAGE_CATEGORY_INTL,
                                                               null, null);
         String imageBaseCategory = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                               OrderServiceConstants.NAME_IMAGE_CATEGORY,
                                                               null, null);
                                                               
         //logger.info("imageBaseCategory:" + imageBaseCategory);
         String imageGoodCategory = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_GOOD, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_CATEGORY);                                                      
         String imageGoodProduct = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_GOOD, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_PRODUCT);                                                                   
         String imageGoodThumbnail = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_GOOD, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_THUMBNAIL);    
         String imageBetterCategory = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BETTER, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_CATEGORY);                                                      
         String imageBetterProduct = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BETTER, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_PRODUCT);                                                                   
         String imageBetterThumbnail = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BETTER, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_THUMBNAIL);   
         String imageBestCategory = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BEST, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_CATEGORY);                                                      
         String imageBestProduct = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BEST, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_PRODUCT);                                                                   
         String imageBestThumbnail = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                            OrderServiceConstants.NAME_IMAGE_GBB,
                                                            OrderServiceConstants.FILTER_GBB_BEST, 
                                                            OrderServiceConstants.FILTER_GBB_TYPE_THUMBNAIL);                                                              
         
         // Global parm gbb price labels
         String sizeLabelsGlobalStandard = commonUtil.getFrpGlobalParm(OrderServiceConstants.CONTEXT_FTDAPPS_PARMS, OrderServiceConstants.FLORAL_LABEL_STANDARD);
         String sizeLabelsGlobalDeluxe = commonUtil.getFrpGlobalParm(OrderServiceConstants.CONTEXT_FTDAPPS_PARMS, OrderServiceConstants.FLORAL_LABEL_DELUXE);
         String sizeLabelsGlobalPremium = commonUtil.getFrpGlobalParm(OrderServiceConstants.CONTEXT_FTDAPPS_PARMS, OrderServiceConstants.FLORAL_LABEL_PREMIUM);
        if(sizeLabelsGlobalStandard == null) {
            sizeLabelsGlobalStandard = "Standard";
        }
        if(sizeLabelsGlobalDeluxe == null) {
            sizeLabelsGlobalDeluxe = "Deluxe";
        }
        if(sizeLabelsGlobalPremium == null) {
            sizeLabelsGlobalPremium = "Premium";
        }
        String labelSuffix = " Price";
        sizeLabelsGlobalStandard += labelSuffix;
        sizeLabelsGlobalDeluxe += labelSuffix;
        sizeLabelsGlobalPremium += labelSuffix;

        //Image URL for product itself
        List<ImageVO> images = new ArrayList();
        //Category Image
        ImageVO image = new ImageVO();
        String imageUrl = null;
        
        if("I".equals(pmvo.getDeliveryType())) {
            // international
            imageUrl = imageBaseCategoryIntl.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
        } else {
            // domestic
            //logger.info("imageBaseCategory:" + imageBaseCategory);
            //logger.info("productId:" + pmvo.getProductId());
            //logger.info("webProductId:" + webProductId);
            imageUrl = imageBaseCategory.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());          
        }
        
        image.setImageURL(imageUrl);
        image.setImageFormat(commonUtil.getImageFormat(imageUrl));
        image.setImageType(ImageTypeEnum.CATEGORY);
        images.add(image);
        
        //Product image
        ImageVO productImage = new ImageVO();
        imageUrl = imageGoodProduct.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
        productImage.setImageURL(imageUrl);
        productImage.setImageFormat(commonUtil.getImageFormat(imageUrl));
        productImage.setImageType(ImageTypeEnum.PRODUCT);
        images.add(productImage);
        
        pmvo.setImageList(images);
        
        // GBB section. Not an upsell product.
        if(upsellMasterVO == null) {
            pmvo.setProductName(pmvo.getWebProductName());
            gbbList = new ArrayList<GBBVO>();
            List<PricingVO> priceList = null;
            BigDecimal standardPrice = pmvo.getStandardPrice();
            BigDecimal deluxePrice = pmvo.getDeluxePrice();
            BigDecimal premiumPrice = pmvo.getPremiumPrice();            
            BigDecimal zero = BigDecimal.ZERO;

            String preferredPricePoint = pmvo.getPreferredPricePoint();
            if(preferredPricePoint == null) {
                preferredPricePoint = "1";
            }
    
            // STANDARD
            if(standardPrice != null && standardPrice.compareTo(zero) > 0) {
                GBBVO gbb = new GBBVO();
                gbb.setProductId(pmvo.getProductId());
                gbb.setWebProductId(pmvo.getWebProductId());
                gbb.setDisplaySequence("1");

                // Add image url here.
                //Image URL for price point
                List<ImageVO> imagesGood = new ArrayList();
                //CATEGORY
                ImageVO imageGoodCat = new ImageVO();
                imageUrl = imageGoodCategory.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                imageGoodCat.setImageURL(imageUrl);
                imageGoodCat.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageGoodCat.setImageType(ImageTypeEnum.CATEGORY);
                imagesGood.add(imageGoodCat);
                
                //PRODUCT
                ImageVO imageGoodProd = new ImageVO();
                imageUrl = imageGoodProduct.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                imageGoodProd.setImageURL(imageUrl);
                imageGoodProd.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageGoodProd.setImageType(ImageTypeEnum.PRODUCT);
                imagesGood.add(imageGoodProd);
                
                //THUMBNAIL
                ImageVO imageGoodThumb = new ImageVO();
                imageUrl = imageGoodThumbnail.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                imageGoodThumb.setImageURL(imageUrl);
                imageGoodThumb.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageGoodThumb.setImageType(ImageTypeEnum.THUMBNAIL);
                imagesGood.add(imageGoodThumb);
                gbb.setImageList(imagesGood);
                
                // Add original price to price list
                priceList = new ArrayList();
                PricingVO price = new PricingVO();
                price.setOrigPriceAmount(standardPrice);
                price.setPriceType(PriceTypeEnum.BASE);
                
                if("1".equals(preferredPricePoint)) {
                    gbb.setDefaultFlag("Y");
                } else {
                    gbb.setDefaultFlag("N");
                }
                //price.setIsDefaultPrice("Y".equals(gbb.getDefaultFlag()));
                priceList.add(price); 
                
                gbb.setPricingList(priceList);
                gbb.setGbbNameOverrideTxt(sizeLabelsGlobalStandard);
                gbb.setGbbPriceOverrideTxt(standardPrice.toString()); 
                gbbList.add(gbb);
            } else {
                pmvo.setStandardPrice(null);
            }
            
            // DELUXE
            if(deluxePrice != null && deluxePrice.compareTo(zero) > 0 ) {
                pmvo.setHasGBB(true);
                GBBVO gbb = new GBBVO();
                gbb.setProductId(pmvo.getProductId());
                gbb.setWebProductId(pmvo.getWebProductId());
                gbb.setDisplaySequence("2");

                // Add image url here.
                List<ImageVO> imagesBetter = new ArrayList();
                //CATEGORY
                ImageVO imageBetterCat = new ImageVO();
                imageUrl = imageBetterCategory.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
                imageBetterCat.setImageURL(imageUrl);
                imageBetterCat.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageBetterCat.setImageType(ImageTypeEnum.CATEGORY);
                imagesBetter.add(imageBetterCat);
                 
                //PRODUCT
                ImageVO imageBetterProd = new ImageVO();
                imageUrl = imageBetterProduct.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
                imageBetterProd.setImageURL(imageUrl);
                imageBetterProd.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageBetterProd.setImageType(ImageTypeEnum.PRODUCT);
                imagesBetter.add(imageBetterProd);
                 
                //THUMBNAIL
                ImageVO imageBetterThumb = new ImageVO();
                imageUrl = imageBetterThumbnail.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
                imageBetterThumb.setImageURL(imageUrl);
                imageBetterThumb.setImageFormat(commonUtil.getImageFormat(imageUrl));
                imageBetterThumb.setImageType(ImageTypeEnum.THUMBNAIL);
                imagesBetter.add(imageBetterThumb);
                gbb.setImageList(imagesBetter);
                 
                // Add original price to price list
                priceList = new ArrayList();
                PricingVO price = new PricingVO();
                price.setOrigPriceAmount(deluxePrice);
                price.setPriceType(PriceTypeEnum.BASE);
     
                if("2".equals(preferredPricePoint)) {
                    gbb.setDefaultFlag("Y");
                } else {
                    gbb.setDefaultFlag("N");
                }
                //price.setIsDefaultPrice("Y".equals(gbb.getDefaultFlag()));
                priceList.add(price);    
                gbb.setPricingList(priceList);
                gbb.setGbbNameOverrideTxt(sizeLabelsGlobalDeluxe);
                gbb.setGbbPriceOverrideTxt(deluxePrice.toString());
                gbbList.add(gbb);
            } else {
                pmvo.setDeluxePrice(null);
            }
            
            // PREMIUM
            if(premiumPrice != null && premiumPrice.compareTo(zero) > 0 ) {
                pmvo.setHasGBB(true);
                GBBVO gbb = new GBBVO();
                gbb.setProductId(pmvo.getProductId());
                gbb.setWebProductId(pmvo.getWebProductId());
                gbb.setDisplaySequence("3");

                // Add image url here.
                 List<ImageVO> imagesBest = new ArrayList();
                 //CATEGORY
                 ImageVO imageBestCat = new ImageVO();
                 imageUrl = imageBestCategory.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                 imageBestCat.setImageURL(imageUrl);
                 imageBestCat.setImageFormat(commonUtil.getImageFormat(imageUrl));
                 imageBestCat.setImageType(ImageTypeEnum.CATEGORY);
                 imagesBest.add(imageBestCat);
                 
                 //PRODUCT
                 ImageVO imageBestProd = new ImageVO();
                 imageUrl = imageBestProduct.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                 imageBestProd.setImageURL(imageUrl);
                 imageBestProd.setImageFormat(commonUtil.getImageFormat(imageUrl));
                 imageBestProd.setImageType(ImageTypeEnum.PRODUCT);
                 imagesBest.add(imageBestProd);
                 
                 //THUMBNAIL
                 ImageVO imageBestThumb = new ImageVO();
                 imageUrl = imageBestThumbnail.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, webProductId.toUpperCase());
    
                 imageBestThumb.setImageURL(imageUrl);
                 imageBestThumb.setImageFormat(commonUtil.getImageFormat(imageUrl));
                 imageBestThumb.setImageType(ImageTypeEnum.THUMBNAIL);
                 imagesBest.add(imageBestThumb);
                 gbb.setImageList(imagesBest);
                
                // Add original price to price list
                priceList = new ArrayList();
                PricingVO price = new PricingVO();
                price.setOrigPriceAmount(premiumPrice);
                price.setPriceType(PriceTypeEnum.BASE);
                
                
                if("3".equals(preferredPricePoint)) {
                    gbb.setDefaultFlag("Y");
                } else {
                    gbb.setDefaultFlag("N");
                }
                //price.setIsDefaultPrice("Y".equals(gbb.getDefaultFlag()));
                priceList.add(price);   
                gbb.setPricingList(priceList);
                gbb.setGbbNameOverrideTxt(sizeLabelsGlobalPremium);
                gbb.setGbbPriceOverrideTxt(premiumPrice.toString());
                gbbList.add(gbb);
            } else {
                pmvo.setPremiumPrice(null);
            }
            
            pmvo.setGbbList(gbbList);
            pmvo.setGbbTitle(pmvo.getWebProductName());
        }
        // Attach upsell info
        else {
            // Product is upsell detail. Populate upsell details to gbblist.
            pmvo.setProductName(upsellMasterVO.getUpsellName());
            pmvo.setGbbList(null);
            List<UpsellDetailVO> upsellDetailList = upsellMasterVO.getUpsellDetailList();

            if(upsellDetailList != null) {
                pmvo.setGbbTitle(upsellMasterVO.getUpsellName());
                pmvo.setHasUpsell(true);
                pmvo.setHasGBB(false);
                gbbList = new ArrayList<GBBVO>();
                for(int i = 0;  i< upsellDetailList.size(); i++) {
                    UpsellDetailVO upsellDetail = upsellDetailList.get(i);
                    String upsellDetailId = upsellDetail.getUpsellDetailId();
                    GBBVO gbb = new GBBVO();
                    gbb.setProductId(upsellDetail.getUpsellDetailId());
                    gbb.setWebProductId(upsellDetail.getNovatorId());
                    gbb.setDefaultFlag(upsellDetail.getDefaultSkuFlag());
                    gbb.setDisplaySequence(String.valueOf(i+1));
                    gbb.setGbbNameOverrideTxt(upsellDetail.getUpsellDetailName());

                    List<PricingVO> priceList = new ArrayList<PricingVO>();
                    PricingVO price = new PricingVO();
                    price.setOrigPriceAmount(upsellDetail.getPriceAmt());
                    price.setPriceType(PriceTypeEnum.BASE);
                    priceList.add(price);
                    gbb.setPricingList(priceList);
                    
                    List<ImageVO> upsellImages = new ArrayList();
                    //Category
                    ImageVO upsellImage = new ImageVO();
                    imageUrl = imageBaseCategory.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, upsellDetailId);          
                    upsellImage.setImageURL(imageUrl);
                    upsellImage.setImageFormat(commonUtil.getImageFormat(imageUrl));
                    upsellImage.setImageType(ImageTypeEnum.CATEGORY);
                    upsellImages.add(upsellImage);
                    
                    //PRODUCT
                    ImageVO upsellImageProduct = new ImageVO();
                    imageUrl = imageGoodProduct.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, upsellDetailId);
                    
                    upsellImageProduct.setImageURL(imageUrl);
                    upsellImageProduct.setImageFormat(commonUtil.getImageFormat(imageUrl));
                    upsellImageProduct.setImageType(ImageTypeEnum.PRODUCT);
                    upsellImages.add(upsellImageProduct);
                    
                    //Thumbnail
                    ImageVO upsellImageThumbnail = new ImageVO();
                    imageUrl = imageGoodThumbnail.replace(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, upsellDetailId);
                    
                    upsellImageThumbnail.setImageURL(imageUrl);
                    upsellImageThumbnail.setImageFormat(commonUtil.getImageFormat(imageUrl));
                    upsellImageThumbnail.setImageType(ImageTypeEnum.THUMBNAIL);
                    upsellImages.add(upsellImageThumbnail);
                    
                    gbb.setImageList(upsellImages);
                    gbbList.add(gbb);
                }
                pmvo.setGbbList(gbbList);
            } else {
                // The upsell has no upsell details
                String errorMsg = "Error in buildGetProductById - upsell has no details: " + webProductId;
                logger.error(errorMsg);
                commonUtil.sendNoPageSystemMessage(errorMsg);
            }
        }
        

        gbbList = pmvo.getGbbList();

        // calculate discounts, rewards, and redemption
        calcPricingForProduct(gbbList, effectiveSourceCode);
        
        // Populate main product information.
        // Populate pricing info
        if(gbbList != null && gbbList.size() > 0) {
            GBBVO gbb = gbbList.get(0);
            pmvo.setPricingList(gbb.getPricingList());
            
            // If there's one price point then no gbb list is necessary.
            if(gbbList.size() == 1) {
                pmvo.setGbbList(null);
            } 
        }

        return pmvo;
    }

    // Calculate discounts, rewards, redemption for each price point.
    public void calcPricingForProduct(List<GBBVO> gbbList, String sourceCode) throws Exception {
        SourceMasterVO smvo = cacheUtil.getSourceCodeById(sourceCode);
        String companyId = smvo.getCompanyId();
        Connection conn = null;
        
        try {
            conn = commonUtil.getNewConnection();
	        if(gbbList != null) {
	            for (int i = 0; i < gbbList.size(); i++ ) {
	                GBBVO gbb = gbbList.get(i);
	                String webProductId = gbb.getWebProductId();
	                List<PricingVO> priceList = gbb.getPricingList();
	                //logger.info("priceList size:" + priceList.size());
	                PricingVO origPrice = priceList.get(0);
	                origPrice.setPriceAmount(origPrice.getOrigPriceAmount());
	                origPrice.setDiscountAmount(new BigDecimal("0.00"));
	                
	                BigDecimal price = origPrice.getOrigPriceAmount();
	                
	                //Load product
	                ProductMasterVO pmvo = cacheUtil.getProductByWebProductId(webProductId);
	                
	                // calculate rewards and redemption for original price point
	                OrderVO orderVO = new OrderVO();
	                orderVO.setOrderDate(new Date());
	                orderVO.setCompanyId(companyId);
	              
	                if(smvo.getMpRedemptionRateAmt() != null) {
	                    if(smvo.getMpRedemptionRateAmt().compareTo(new BigDecimal(0)) == 0) {
	                        commonUtil.sendPageSystemMessage("Redemption rate should not be 0. Source code:" + smvo.getSourceCode());
	                        return;
	                    }
	                    String paymentMethodId = smvo.getPaymentMethodId();
	                   
	                    if(logger.isDebugEnabled()) {
	                    	logger.debug("paymentMethodId:" + paymentMethodId);
	                    }
	                    
		            	if(paymentMethodId != null && !paymentMethodId.equals("")){
		                    PaymentsVO paymentsVO = new PaymentsVO();
		                    paymentsVO.setPaymentType(paymentMethodId);
		                    paymentsVO.setAmount(origPrice.getPriceAmount().toString());
		                    paymentsVO.setMilesPointsAmt("1"); //setting to a non-zero value to enable recalc.
		                    List<PaymentsVO> paymentsList = new ArrayList();
			                paymentsList.add(paymentsVO);
			                orderVO.setPayments(paymentsList);
			                orderVO.setMpRedemptionRateAmt(smvo.getMpRedemptionRateAmt().toString());
		            	}
	            	}
	                                
	                OrderDetailsVO orderDetailsVO = new OrderDetailsVO();
	                orderDetailsVO.setSourceCode(sourceCode);
	                orderDetailsVO.setDeliveryDate(new Date());
	                orderDetailsVO.setProductId(gbb.getProductId());
	                                
	                BigDecimal standardPrice = pmvo.getStandardPrice();
	                BigDecimal deluxePrice = pmvo.getDeluxePrice();
	                BigDecimal premiumPrice = pmvo.getPremiumPrice();
	                String variablePriceFlag = pmvo.getVariablePriceFlag();
	                BigDecimal zero = new BigDecimal("0");
	                
	                if(standardPrice != null && standardPrice.compareTo(price) == 0 && !price.equals(zero))
	                {
	                    orderDetailsVO.setSizeChoice("A");
	                }
	                else if(deluxePrice != null && deluxePrice.compareTo(price) == 0 && !price.equals(zero))
	                {
	                	orderDetailsVO.setSizeChoice("B");
	                }
	                else if(premiumPrice != null && premiumPrice.compareTo(price) == 0 && !price.equals(zero))
	                {
	                	orderDetailsVO.setSizeChoice("C");
	                }
	                else if(variablePriceFlag != null && variablePriceFlag.equalsIgnoreCase("Y"))
	                {
	                	orderDetailsVO.setSizeChoice("A");
	                }
	                if(orderDetailsVO.getSizeChoice() == null){
		            	//set size choice based off of gbb default flag and display sequence
		            	if(gbb.getDefaultFlag() != null && !gbb.getDefaultFlag().equals("")
		            	   && gbb.getDisplaySequence() != null && !gbb.getDisplaySequence().equals("")){
		            		if(gbb.getDefaultFlag().equalsIgnoreCase("Y")){
		            			if(gbb.getDisplaySequence().equalsIgnoreCase("1")){
		            				orderDetailsVO.setSizeChoice("A");
		            			}
		            			else if(gbb.getDisplaySequence().equalsIgnoreCase("2")){
		            				orderDetailsVO.setSizeChoice("B");
		            			} 
		            			else if(gbb.getDisplaySequence().equalsIgnoreCase("3")){
		            				orderDetailsVO.setSizeChoice("C");
		            			}
		            		}
		            		
		            	}
	                }    
	                
                    RecipientAddressesVO recipientAddresses = new RecipientAddressesVO();
                    recipientAddresses.setCountry(DEFAULT_COUNTRY);
                    recipientAddresses.setStateProvince(DEFAULT_STATE);
                    List<RecipientAddressesVO> recipientAddressesList = new ArrayList();
                    recipientAddressesList.add(recipientAddresses);
                    
                    RecipientsVO recipientsVO = new RecipientsVO();
                    recipientsVO.setRecipientAddresses(recipientAddressesList);
                    List<RecipientsVO> recipientsList = new ArrayList();
                    recipientsList.add(recipientsVO);
                    
                    orderDetailsVO.setRecipients(recipientsList);
	                
	                List<OrderDetailsVO> orderDetailsList = new ArrayList();
	                orderDetailsList.add(orderDetailsVO);
	                
	                orderVO.setOrderDetail(orderDetailsList);
	                
	                RecalculateOrderCacheDAO rocDAO = new RecalculateOrderCacheDAO();
	                RecalculateOrderBO recalcOrderBO = new RecalculateOrderBO(rocDAO, true);
	               
	                recalcOrderBO.recalculate(conn, orderVO);
	                              
	                Iterator it = null;
	                
	                it = orderVO.getOrderDetail().iterator();
	                while(it.hasNext())
	                {
	                	orderDetailsVO = (OrderDetailsVO)it.next();
	                	
	                	//Redemption
	                	if(orderDetailsVO.getMilesPointsAmt() != null) {
	                		origPrice.setRedemptionAmount(new BigDecimal(orderDetailsVO.getMilesPointsAmtProduct()));
	                	}

	                    //Reward
	                    if(orderDetailsVO.getRewardType() != null){	                    	
	                    	origPrice.setRewardType(orderDetailsVO.getRewardType());
	                    	origPrice.setRewardAmount(new BigDecimal(orderDetailsVO.getMilesPoints()));
	                    }
	                    if(logger.isDebugEnabled()){
	                        logger.debug("rewardType: " + orderDetailsVO.getRewardType() + " - value: " + orderDetailsVO.getMilesPoints());
	                    }
	                    
	                    //Discount
	                    PricingVO discPrice = new PricingVO();
	                    discPrice.setOrigPriceAmount(origPrice.getOrigPriceAmount());
	                    discPrice.setDiscountAmount(new BigDecimal(orderDetailsVO.getDiscountAmount()));
	                    discPrice.setDiscountType(orderDetailsVO.getDiscountType());
	                    if(orderDetailsVO.getPercentOff() != null){
	                    	discPrice.setPercentOff(new BigDecimal(orderDetailsVO.getPercentOff()));
	                    }
	                    discPrice.setPriceAmount(new BigDecimal(orderDetailsVO.getDiscountedProductPrice()));
	                    discPrice.setPriceType(PriceTypeEnum.SALE);
	
	                    // adds sale price point to the pricing list.
	                    // If there is discount, then add the discounted price to list.
	                    if(origPrice.getPriceAmount().compareTo(discPrice.getPriceAmount()) != 0) {
	                        priceList.add(discPrice);
	                    }

	                    // Not showing dollar price if there are redemption value
	                    if(origPrice.getRedemptionAmount() != null) {
	                         origPrice.setPriceAmount(null);
	                         origPrice.setOrigPriceAmount(null);
	                         origPrice.setDiscountAmount(null);
	                    }
	                }
	            }
	        }
        } finally {
            if(conn != null) {
                conn.close();
            }
        }
    }


    /**
     * Return product id for given category. Format based on client.
     * If results in cache, return it. Otherwise, build it.
     * @param reqVO
     * @return
     * @throws Exception
     */
    public String getProductsByCategory(GetProductsByCategoryRequestVO reqVO) throws Exception {
        if(logger.isDebugEnabled())
          logger.debug("getProductsByCategory:" + reqVO.toLogString());
        String sourceCode = reqVO.getSourceCode();
        String categoryId = reqVO.getCategoryId();
        List<ProductMasterVO> productList = null;
        String responseString = null;
        
        // Get product list
        try {
            productList = this.buildGetProductsByCategory(sourceCode,
                                                          categoryId,
                                                          reqVO.getProductOrderBy(),
                                                          reqVO.getZipCode(),
                                                          reqVO.getCountryId(),
                                                          reqVO.getDeliveryDate());
        } catch (RespondWithErrorException e) {
            String deliveryDateStr = reqVO.getDeliveryDateStr();
            if(deliveryDateStr == null) {
                deliveryDateStr = "";
            }
            reqVO.setErrorCode(e.getMessage());
            reqVO.setErrorFieldValue(deliveryDateStr);
            responseString = commonUtil.transformError(reqVO);
        }
        
        if(responseString == null) {
            GetProductsByCategoryResponseVO respVO = new GetProductsByCategoryResponseVO();
            respVO.setRequestVO(reqVO);
            respVO.setProductMasterList(productList);
            // set index id and index name on response object.
            Connection conn = null;
            ProductIndexVO pivo  = null;
            
            try {
                conn = commonUtil.getNewConnection();
                String defaultDomain = commonUtil.getCompanyDefaultDomain(sourceCode);
                pivo = osDAO.getIndexByIdAndSource(conn, sourceCode, categoryId, defaultDomain);
            } finally {
                if(conn != null) {
                    conn.close();
                }
            }
            
            
            respVO.setIndexId(categoryId);
            // The client Allurent currently does not need this field
            if(pivo != null) {
                respVO.setIndexName(pivo.getIndexDesc());
            }
            
            // Transform.
            responseString = commonUtil.transform(respVO);
        }

        return responseString;

    }

     /**
      * 1.       If source code has a mapping to this category_id/index id in FTD_APPS.SHOPPING_INDEX_SRC, use that mapping;
      *          otherwise, use the source code company's default source code mapping. Only return products for that index and its sub indexes.
      * 2.       Order by index display order, price ascending, or price descending based on input "order by" parameter.
      * 3.       Exclude products in the nonsearchitems index or exclude_products index.
      * 4.       If product id is in FTD_APPS.PRODUCT_SOURCE and product source mapping does not exist, exclude flag is true;
      * 5.       If product id is in FTD_APPS.UPSELL_SOURCE.UPSELL_MASTER_ID and product source mapping does not exist, exlude flag is true;
      * 6.       If source code is in the source product type exclusion table FTD_APPS.PRODUCT_ATTR_RESTR_SOURCE_EXCL,
      *          only return products that do not have the excluded values.
      * 7.       Only return products that are available (ftd_apps.product_master.status=A)
      * 8.       No duplicates.
      * 9.       Index feed may contain upsell master ids as product id. If so, the call needs to return upsell master id with availability
      *          of the base upsell detail.
      * 10.      Check availablity of products for delivery date and zip/country.
      * @param sourceCode
      * @param categoryId
      * @return
      * @throws Exception
      */
     public List<ProductMasterVO> buildGetProductsByCategory(String sourceCode,
                                                             String categoryId,
                                                             ProductOrderByEnum orderBy,
                                                             String zipCode,
                                                             String countryId,
                                                             Date deliveryDate) throws RespondWithErrorException, Exception {
          // Get product id list by index
          Connection conn = null; 
          String defaultCompanyDomain = commonUtil.getCompanyDefaultDomain(sourceCode);
          List<ProductMasterVO> productList = new ArrayList<ProductMasterVO>();
          PASRequestServiceImpl pas = new PASRequestServiceImpl();
          Calendar deliveryDateCal = Calendar.getInstance();
          SourceProductUtility spUtil = new SourceProductUtility();
          String[] availIdArray = null;
          Map<String,ProductMasterVO> productMasterMap = new HashMap();
         List<String> productIdList = null;

         try {
             conn = commonUtil.getNewConnection();
            
             ProductIndexVO productIndexVO = osDAO.getIndexByIdAndSource(conn, sourceCode, categoryId, defaultCompanyDomain);
             if(productIndexVO == null) {
            	 logger.error("index not found:" + categoryId);
            	 throw new RespondWithErrorException(OrderServiceConstants.GENERIC_ERROR_CANNOT_FULFILL_REQUEST);
             }
             String indexCountryId = productIndexVO.getCountryId();
             //Determine if index is domestic or international as international will not go through 
             // special index filtering.             
             String countryType = this.getCountryType(indexCountryId);
             
             // Get all products for index.
             // Upsell master ids are in the list.
             productIdList = spUtil.getIndexProductList(conn, sourceCode, categoryId, defaultCompanyDomain);          
                           
             if(logger.isDebugEnabled()) {
                 logger.debug("returned shopping index product id size:" + productIdList.size());
                 logger.debug("indexCountryId:" + indexCountryId);
                 logger.debug("countryType:" + countryType);
                 if(productIdList == null) {
                    logger.debug("productIdList before status filtering is empty");
                 } else {
                    logger.debug("productIdList size before status filtering:" + productIdList.size());
                 }
             }

             // Loop through the products. Replace upsell master id with base sku.
             for(int i = 0; i < productIdList.size(); i++) {
                 String productIdInList = productIdList.get(i);
                 boolean isUpsell = false;
                 
                 if(logger.isDebugEnabled())
                   logger.debug("product id is: " + productIdInList);

                 // Load product info
                 try {
                     ProductMasterVO pmvo = cacheUtil.getProductById(productIdInList);
                     if(pmvo == null) {
                         // Retrieve as upsell. Base sku.
                         pmvo = cacheUtil.getBaseProductByUpsellMasterId(productIdInList);
                         isUpsell = true;
                     }
                     if(pmvo != null) {
                         //Establish index order.
                         pmvo.setIndexOrder(i + 1);

                         if(isUpsell) {
                             // Replace the product id in productIdList from upsellMasterId to productId.
                             productIdList.remove(i);
                             productIdList.add(i, pmvo.getProductId());
                             // Flag that the product in index list is an upsell master id so it will need to be switched back after PAS check.
                             pmvo.setIsUpsellMasterId(true);
                         }
                         
                     } else {
                         logger.error("Product in index but not in product master or upsell table:" + productIdInList);
                         // Remove from list.
                         productIdList.remove(i);
                         i--;
                         continue;
                     }               
                     
                     //productIndexOrdered.put(productIdList.get(i), Integer.valueOf(i));
                     
                      String status = pmvo.getStatus();
                      
                      // Remove products that are not available
                      if(logger.isDebugEnabled()) {
                        logger.debug("product master status: " + status);
                        logger.debug("isUpsell: " + isUpsell);
                      }
                      if(!"A".equals(status)) {
                           //Product not available. Continue to the next product.
                            productIdList.remove(i);
                            i--;
                            if(logger.isDebugEnabled()) {
                                 logger.debug("product excluded by status: " + productIdInList);
                            }
                           continue;
                      }

                      productMasterMap.put(pmvo.getProductId(), pmvo);
                      if(logger.isDebugEnabled()) {
                         logger.debug("added to map:" + pmvo.getProductId());
                      }
                 } catch (Exception e) {
                     // Skip product in case of an exception.
                      String errorMsg = "Error in buildGetProductsByCategory : " + productIdInList;
                      logger.error(e);
                      commonUtil.sendNoPageSystemMessage(errorMsg);
                      
                      // Remove the product from the list to skip it
                      productIdList.remove(i);
                      i--;
                      continue;
                 }
             }
             
             //Partition the id list into chunks. For each chuck, go through PAS and SourceProductUtility to filter products until the count is met.
             String recordsMaxCnt = commonUtil.getFrpGlobalParm("ORDER_SERVICE_CONFIG", "GET_PRODUCT_BY_CATEGORY_MAX_COUNT");
             String pasCallSizeStr = commonUtil.getFrpGlobalParm("ORDER_SERVICE_CONFIG", "PAS_CALL_SIZE");
             int maxCnt = 0;
             int pasCallSize = 0;
             try {
                  maxCnt = Integer.parseInt(recordsMaxCnt);
             } catch (Exception e) {
                  maxCnt = 100;
                  if(logger.isDebugEnabled())
                    logger.debug("Can not parse GET_PRODUCT_BY_CATEGORY_MAX_COUNT global parm", e);
             }
             
             try {
                  pasCallSize = Integer.parseInt(pasCallSizeStr);
             } catch (Exception e) {
                  pasCallSize = 2 * maxCnt;
                  if(logger.isDebugEnabled())
                    logger.debug("Can not parse PAS_CALL_SIZE global parm", e);
             }             
             
             if(logger.isDebugEnabled()) {
                logger.debug("recordsMaxCnt:" + recordsMaxCnt);
                logger.debug("pasCallSizeStr:" + pasCallSizeStr);
                 if(productIdList == null) {
                    logger.debug("productIdList before pas call is empty");
                 } else {
                    logger.debug("productIdList size before pas call:" + productIdList.size());
                 }
             }
             
             String[] inProductIdArray = productIdList.toArray(new String[0]);
             int pos = 0;
             while (productList.size() < maxCnt && pos < inProductIdArray.length) {
                 // Get the products ids that are available.                 
                 int copyCount = pasCallSize;
                 int remainderCount = inProductIdArray.length - pos;
                 if(pasCallSize > remainderCount) {
                     copyCount = remainderCount;
                 } 
                 String[] arrayCopy = new String[copyCount];
                 System.arraycopy(inProductIdArray, pos, arrayCopy, 0, copyCount);
                 
                 if(logger.isDebugEnabled()) {
                    logger.debug("productSize:" + productList.size());
                    logger.debug("pos: " + pos);
                    logger.debug("inProductIdArray.length:" + inProductIdArray.length);
                    logger.debug("copyCount" + arrayCopy.length);
                 }
                 
                 //if delivery date is null consider all products as available
                 if (deliveryDate!=null) {
                   if(inProductIdArray.length > 0) {
                       deliveryDateCal.setTime(deliveryDate);
                       if(logger.isDebugEnabled())
                         logger.debug("number of product requested for availability:" + arrayCopy.length);
                       	 String productStr = "";
                         for(int ac = 0; ac < arrayCopy.length; ac++) {
                            productStr += "''" ;
                            productStr += arrayCopy[ac];
                            productStr += "'',";
                         }
                         logger.debug("Product requested:" + productStr);
                         
                       if(countryId == null || countryId.equals("US") || countryId.equals("CA")) {
                           availIdArray = pas.isProductListAvailable(conn, zipCode, deliveryDateCal, arrayCopy);
                       } else {
                           availIdArray = pas.isInternationalProductListAvailable(conn, countryId, deliveryDateCal, arrayCopy);
                       }
                   } else {
                       availIdArray = null;
                   }
                   pos = pos + copyCount;
                 } else {
                   availIdArray = inProductIdArray;
                   pos = inProductIdArray.length;
                 }
                 
                 // Create list to be sent back.
                 if(availIdArray != null) {
                     if(logger.isDebugEnabled()) {
                        logger.debug("availIdArray size:" + availIdArray.length);
                     }
                     for (int i = 0; i < availIdArray.length; i++) {
                         String productId = availIdArray[i];    
                         // Load product info
                         ProductMasterVO pmvo = productMasterMap.get(productId);
                         
                         if(logger.isDebugEnabled()) {
                            logger.debug("getting from map:" + productId);
                            logger.debug("is null? " + (pmvo == null?"Y" : "N"));
                         }
                         
                         // Switch product id to upsell master id if it's an upsell.
                         if(pmvo.isIsUpsellMasterId()) {
                            productId = pmvo.getUpsellMasterVO().getUpsellMasterId();
                         }
                         // Filter by product source code exclusion rules. Pass upsell master id if it's an upsell.
                         String excludeMessage = spUtil.getProductRestrictionMsg(conn, sourceCode, productId, false, countryType);

                         if(excludeMessage != null && excludeMessage.length() > 0) {
                             if(logger.isDebugEnabled()) {
                               logger.debug("product excluded by source product utility:" + productId);
                               logger.debug("exclusion message:" + excludeMessage);
                             }
                         } else {
                             productList.add(pmvo);
                             if(productList.size() >= maxCnt) {
                                break;
                             }
                         }
                     }
                 }
                 
                 if(logger.isDebugEnabled()) {
                    logger.debug("Number of products added:" + productList.size());
                 }
                 
             }
             
             // Sort if necessary
             if(productList.size() > 0) {
                 if(logger.isDebugEnabled())
                 {
                   logger.debug("productOrderBy:" + orderBy);
                   logger.debug("list size after all filtering:" + productList.size());
                 }
                 if(orderBy.equals(ProductOrderByEnum.PRICE_ASC)) {
                     sortProductByPriceAsc(productList);
                 } else if(orderBy.equals(ProductOrderByEnum.PRICE_DESC)) {
                     sortProductByPriceDsc(productList);
                 } else {
                     // Sort by index display order
                     for(int j = 0; j<productList.size(); j++) {
                         ProductMasterVO pmvo = productList.get(j);
                         if(pmvo == null) {
                             logger.debug("this is null:" + j);
                         } else {
                             logger.debug(pmvo.getProductId());
                         }
                     }
                     Collections.sort(productList, new IndexOrderComparator());
                 } 
                 
             } else {
                 // No products are returned. Check if any products are available if domestic.
                 boolean anyProductAvail = true;
                 if((countryId == null || countryId.equals("US") || countryId.equals("CA")) && deliveryDate!=null) {
                     deliveryDateCal.setTime(deliveryDate);
                     anyProductAvail = pas.isAnyProductAvailable(conn, deliveryDateCal, zipCode);
                 } else {
                     // If international, we know no products are available if the category call returned nothing.
                     anyProductAvail = false;
                 }
                 
                 if(logger.isDebugEnabled()) {
                     logger.debug("anyProductAvail returns: " + anyProductAvail);
                 }
                 if(!anyProductAvail) {
                     //If no products are available for delivery date, add 1 to delivery date
                     //and check to see if any products are available for that day.  
                     //If there are products available then return DATE_UNAVAILABLE error, else return 
                     //ZIP_CODE_AND_DATE_UNAVAILABLE error
                     boolean anyProductAvailTomorrow = false;

                     if (deliveryDate!=null) {
                        deliveryDateCal.add(Calendar.DATE,1);
                        anyProductAvailTomorrow = pas.isAnyProductAvailable(conn, deliveryDateCal, zipCode);
                     }
                     if(anyProductAvailTomorrow) {
                         throw new RespondWithErrorException(OrderServiceConstants.DATE_UNAVAILABLE);
                     }
                     else
                     {
                         throw new RespondWithErrorException(OrderServiceConstants.ZIP_CODE_AND_DATE_UNAVAILABLE);
                     }
                     
                 } else {
                     // Products in other index are available
                     throw new RespondWithErrorException(OrderServiceConstants.ZIP_CODE_AND_DATE_UNAVAILABLE);
                 }
             }
         } catch (Exception e) {
           if(logger.isDebugEnabled())
             logger.debug(e);
           throw e;
         } finally {
             if(conn != null) {
                 conn.close();
             }
         }

         return productList;
     }



    /**
     * Sort product by price ascending
     * @param productList
     */
    public void sortProductByPriceAsc(List<ProductMasterVO> productList) {
        // Backed by implementing compareTo on ProductMasterVO.
        Collections.sort(productList);
    }

    /**
     * Sort product by price descending
     * @param productList
     */
    public void sortProductByPriceDsc(List<ProductMasterVO> productList) {
        Collections.sort(productList);
        Collections.reverse(productList);
    }


    public String getProductAddon(GetAddonRequestVO reqVO) throws Exception {
        String responseString = null;
        GetAddonResponseVO respVO = new GetAddonResponseVO();
        respVO.setRequestVO(reqVO);

        String webProductId = reqVO.getWebProductId();
        String productId = cacheUtil.getProductIdByWebProductId(webProductId);
        Connection conn = null;
        AddOnUtility addonUtil = new AddOnUtility();
        HashMap<String, ArrayList <AddOnVO>> addonMap = null;

        try {
            conn = commonUtil.getNewConnection();

            addonMap =
            addonUtil.getActiveAddonListByProductIdAndOccasionAndSourceCode(productId,
                                                                            null, // Occasion id. Do not restrict by occasion.
                                                                            reqVO.getSourceCode(),
                                                                            true, //returnAllAddOnFlag
                                                                            conn);
            // The existing addon utilities trims the number of addons by retrieving from global parms values via cache.
            // Oracle cache is not supported JBoss. Here we let the utilities return all addons (so the global parm
            // code will not be invoked), then trim the count by retriving the global parm from cache supported on JBoss.
            // Do not trim counts for cards.
            String returnAllAddOnString = commonUtil.getFrpGlobalParm(OrderServiceConstants.ORDER_SERVICE_CONFIG, OrderServiceConstants.RETURN_ALL_ADDON_FLAG);
            boolean returnAllAddOnFlag = false;
            if(returnAllAddOnString != null && returnAllAddOnString.equals("Y"))
            {
                returnAllAddOnFlag = true;
            }
            if (!returnAllAddOnFlag)
            {
                ArrayList <AddOnVO> addOnVOList = addonMap.get(AddOnVO.ADD_ON_VO_ADD_ON_KEY);
                ArrayList <AddOnVO> vaseAddOnVOList = addonMap.get(AddOnVO.ADD_ON_VO_VASE_KEY);
                ArrayList <AddOnVO> bannerAddOnVOList = addonMap.get(AddOnVO.ADD_ON_VO_BANNER_KEY);
                ArrayList <AddOnVO> cardAddOnVOList = addonMap.get(AddOnVO.ADD_ON_VO_CARD_KEY);
                
                String maxDisplayCountString = commonUtil.getFrpGlobalParm(OrderServiceConstants.ADDON_VALUES_CONTEXT,OrderServiceConstants.MAX_DISPLAY_COUNT_NAME);
                if (maxDisplayCountString != null)
                {
                     Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                     if (cardAddOnVOList != null && cardAddOnVOList.size() > 0)
                     {
                         maxDisplayCount--;
                     }
             
                     if (addOnVOList != null && addOnVOList.size() > maxDisplayCount)
                     {
                         ArrayList <AddOnVO> trimmedAddOnVOList = new ArrayList <AddOnVO> ();
                         for (int i = 0; i < maxDisplayCount; i++)
                         {
                             trimmedAddOnVOList.add(addOnVOList.get(i));
                         }
                         addOnVOList = trimmedAddOnVOList;
                     }
                }
                else
                {
                     logger.error("Missing max display count global parm for add-ons.  Context: " + OrderServiceConstants.ADDON_VALUES_CONTEXT +
                                  " Value: " + OrderServiceConstants.MAX_DISPLAY_COUNT_NAME);
                }
                 
                maxDisplayCountString = commonUtil.getFrpGlobalParm(OrderServiceConstants.ADDON_VALUES_CONTEXT,OrderServiceConstants.MAX_VASE_DISPLAY_COUNT_NAME);
                if (maxDisplayCountString != null)
                {
                     Integer maxDisplayCount = Integer.parseInt(maxDisplayCountString);
                     if (vaseAddOnVOList.size() > maxDisplayCount)
                     {
                         ArrayList <AddOnVO> trimmedVaseAddOnVOList = new ArrayList <AddOnVO> ();
                         for (int i = 0; i < maxDisplayCount; i++)
                         {
                             trimmedVaseAddOnVOList.add(vaseAddOnVOList.get(i));
                         }
                         vaseAddOnVOList = trimmedVaseAddOnVOList;
                     }
                }
                else
                {
                     logger.error("Missing max display count global parm for add-ons.  Context: " + OrderServiceConstants.ADDON_VALUES_CONTEXT +
                                  " Value: " + OrderServiceConstants.MAX_VASE_DISPLAY_COUNT_NAME);
                }                
            }     
            
            //Remove funeral banners  
            String includeFuneralBannerString = commonUtil.getFrpGlobalParm(OrderServiceConstants.ORDER_SERVICE_CONFIG, OrderServiceConstants.INCLUDE_FUNERAL_BANNER_FLAG);
            boolean includeFuneralBannerFlag = false;
            if(includeFuneralBannerString != null && includeFuneralBannerString.equals("Y"))
            {
                includeFuneralBannerFlag = true;
            }
            if (!includeFuneralBannerFlag)
            {
                addonMap.remove(AddOnVO.ADD_ON_VO_BANNER_KEY);
            }
            
        } finally {
            if(conn != null && !conn.isClosed()) {
                conn.close();
            }
        }
        List<AddonVO> addonList = new ArrayList();
        List<String> keyList = new ArrayList<String>();
        // Define order: vase, addon, cards
        keyList.add(AddOnVO.ADD_ON_VO_VASE_KEY);
        keyList.add(AddOnVO.ADD_ON_VO_ADD_ON_KEY);
        keyList.add(AddOnVO.ADD_ON_VO_BANNER_KEY);
        keyList.add(AddOnVO.ADD_ON_VO_CARD_KEY);
        for(int i = 0; i < keyList.size(); i++ ) {
            String key = keyList.get(i);
            List<AddOnVO> addonByTypeList = addonMap.get(key);
            
            //If type is card, reorder cards such that the default card is first.
            if(key.equals(AddOnVO.ADD_ON_VO_CARD_KEY)) {
                List<AddOnVO> cardList = new ArrayList<AddOnVO>();
                
                for(int k = 0; k < addonByTypeList.size(); k++) {
                    AddOnVO addon = addonByTypeList.get(k);
                    if("RC999".equals(addon.getAddOnId())) {
                        cardList.add(addon);
                        addonByTypeList.remove(k);
                        k--;
                        break;
                    }
                }
                //Add the rest of the cards
                 for(int k = 0; k < addonByTypeList.size(); k++) {
                     AddOnVO addon = addonByTypeList.get(k);
                     cardList.add(addon);
                 }   
                 addonByTypeList = cardList;
            }         

            if(addonByTypeList != null) {
                for(int k = 0; k < addonByTypeList.size(); k++) {
                    AddOnVO addon = addonByTypeList.get(k);
                    
                    logger.debug("addon id:" + addon.getAddOnId());

                    // Convert to order service addon obj.
                    AddonVO addonOS = new AddonVO();
                    addonOS.setAddonDesc(addon.getAddOnDescription());
                    addonOS.setAddonId(addon.getAddOnId());
                    addonOS.setAddonPrice(new BigDecimal(addon.getAddOnPrice()));
                    addonOS.setAddonTitle(addon.getAddOnDescription());
                    addonOS.setAddonText(addon.getAddOnText());
                    addonOS.setAddonType(key);


                    addonOS.setAddonTypeId(addon.getAddOnTypeId());
                    addonOS.setDefaultPerTypeFlag(addon.getDefaultPerTypeFlag() ? "Y" : "N");
                    addonOS.setDisplayPriceFlag(addon.getDisplayPriceFlag()? "Y" : "N");

                    // image
                    ImageVO image = new ImageVO();
                    String imageURL = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_IMAGE_URL,
                                                           OrderServiceConstants.NAME_IMAGE_ADDON,
                                                           null, null);
                    imageURL = imageURL.replaceFirst(OrderServiceConstants.CONTENT_REPLACE_TOKEN_1, addon.getAddOnId());
                    image.setImageURL(imageURL);
                    image.setImageFormat(commonUtil.getImageFormat(imageURL));
                    image.setImageType(ImageTypeEnum.THUMBNAIL);
                    addonOS.setImage(image);
                    addonOS.setMaxPurchaseQty(addon.getMaxQuantity() == null? 0 : Integer.parseInt(addon.getMaxQuantity()));
                    addonOS.setOccasionId(addon.getOccasionId());
                    
                    // Retrieve addon card occasion display name from content.
                    String occasionDisplayName = commonUtil.getContentWithFilter(OrderServiceConstants.ORDER_SERVICE_CONTEXT,
                                                            OrderServiceConstants.NAME_ADDON_OCCA_DISP_NAME, 
                                                            addon.getAddOnId(), null, true);
                    if(occasionDisplayName != null && occasionDisplayName.length() > 0) {
                        addonOS.setOccasionDesc(occasionDisplayName);
                    } else {
                        addonOS.setOccasionDesc(addon.getOccasionDescription());
                    }
                    
                    addonOS.setPriceType(OrderServiceConstants.ADDON_PRICE_TYPE);
                    addonOS.setPriceUnit(OrderServiceConstants.ADDON_PRICE_UNIT);
                    addonList.add(addonOS);
                }
            }

        }


        respVO.setAddonList(addonList);;


        // Transform results.
        responseString = commonUtil.transform(respVO);

        return responseString;
    }



        public String getAvailableDays(GetAvailableDaysRequestVO requestVO) throws Exception {

        	String responseString = null;

            GetAvailableDaysResponseVO respVO = new GetAvailableDaysResponseVO();
            respVO.setRequestVO(requestVO);

            String webProductId = requestVO.getWebProductId();
            String zipCode = requestVO.getZipCode();
            String countryId = requestVO.getCountryId();
            String sourceCode = requestVO.getSourceCode();
            SourceMasterVO smvo = cacheUtil.getSourceCodeById(sourceCode);
            if (smvo == null) {
                throw new Exception("Source Master value object is null:" + sourceCode);
            }
            ProductMasterVO pmVO = cacheUtil.getProductOrBaseProductByWebProductId(webProductId);
            if (pmVO == null) {
                throw new Exception("Product Master value object is null:" + webProductId);
            }
            String productId = pmVO.getProductId();
            BigDecimal price = requestVO.getPriceSelected();
            String companyId = smvo.getCompanyId();
            if(logger.isDebugEnabled())
            {
              logger.debug("webProductId: " + webProductId + " (" + productId + ")");
              logger.debug("zipCode: " + zipCode + " countryId: " + countryId);
              logger.debug("sourceCode: " + sourceCode + " " + smvo.getSnhId() + " " + smvo.getDisplayServiceFeeFlag() + " " + smvo.getDisplayShippingFeeFlag());
            }

            Connection conn = null;
            try {
                conn = commonUtil.getNewConnection();
                Timer productDatesTimer = new Timer();
                String maxDaysString = commonUtil.getFrpGlobalParm(OrderServiceConstants.ORDER_SERVICE_CONFIG, OrderServiceConstants.GET_DELIVERY_DAYS_MAX_COUNT);
                int maxDays = 0;
                if (maxDaysString != null) {
                    try {
                        maxDays = Integer.valueOf(maxDaysString);
                    } catch (NumberFormatException e) {
                        maxDays = 120;
                    }
                }
                if(logger.isDebugEnabled())
                  logger.debug("maxDays: " + maxDays);

                PASRequestServiceImpl pas = new PASRequestServiceImpl();
                List<AvailableDateVO> dateList = new ArrayList();
                ProductAvailVO[] availableDates = null;
                if (countryId == null || countryId.equals("") || countryId.equals("US") || countryId.equals("CA")) {
                    if(logger.isDebugEnabled())
                      logger.debug("product availability");
                    availableDates = pas.getProductAvailableDates(conn, productId, zipCode, maxDays);
                } else {
                    if(logger.isDebugEnabled())
                      logger.debug("international product availability");
                    availableDates = pas.getInternationalProductAvailableDates(conn, productId, countryId, maxDays);
                }
                
                if (availableDates == null || availableDates.length == 0) {
                    if (pmVO.getStatus() == null || !pmVO.getStatus().equals("A")) {
                        throw new RespondWithErrorException(OrderServiceConstants.VALIDATION_ERROR_PRODUCT_UNAVAILABLE);
                    } else {
                        if (countryId != null && !countryId.equals("") && !countryId.equals("US") && !countryId.equals("CA")) {
                            throw new RespondWithErrorException(OrderServiceConstants.VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_INT);
                        } else {
                        DeliveryMethodEnum deliveryMethod = pmVO.getDeliveryMethod();
                        if (DeliveryMethodEnum.FLORIST.equals(deliveryMethod)) {
                            String recipientState = cacheUtil.getZipCodeState(zipCode);
                            String dropShipAvailableFlag = null;
                            if(logger.isDebugEnabled())
                              logger.debug("recipientState: " + recipientState);
                            StateMasterVO stateVO = cacheUtil.getStateById(recipientState);
                            dropShipAvailableFlag = stateVO.getDropShipAvailableFlag();
                            if (dropShipAvailableFlag != null && dropShipAvailableFlag.equals("Y")) {
                                  throw new RespondWithErrorException(OrderServiceConstants.VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_US);
                            } else {
                                throw new RespondWithErrorException(OrderServiceConstants.VALIDATION_ERROR_FLORIST_PRODUCT_NO_DELIVERY_DATES_CA);
                            }
                        } else {
                              throw new RespondWithErrorException(OrderServiceConstants.VALIDATION_ERROR_VENDOR_PRODUCT_NO_DELIVERY_DATES);
                        }
                        }
                    }
                    //logger.debug("responseString: " + responseString);
                } else {

                    int gnaddLevel = new Integer(cacheUtil.getGlobalParm(FTDAPPS_PARMS_CONTEXT, GNADD_LEVEL)).intValue();
                    Date gnaddDate = null;
                    String cszAvailGnaddFlag = null;
                    if (gnaddLevel >= 1) {
                        String gnaddDateString = cacheUtil.getGlobalParm(FTDAPPS_PARMS_CONTEXT, GNADD_DATE);
                        SimpleDateFormat format = new SimpleDateFormat("MMMMM dd, yyyy");
                        gnaddDate = format.parse(gnaddDateString);
                        cszAvailGnaddFlag = cacheUtil.getCSZAvailGnaddFlag(zipCode);
                    }
                
                    Date dateRangeEnd = null;
                    for (int i=0; i<availableDates.length; i++) {
                        ProductAvailVO paVO = availableDates[i];
                        AvailableDateVO dateVO = new AvailableDateVO();
                        //FeesVO feeVO = new FeesVO();
                        dateVO.setDeliveryDate(paVO.getDeliveryDate().getTime());
                        if (dateRangeEnd != null) {
                            if (dateVO.getDeliveryDate().compareTo(dateRangeEnd) <= 0) {
                                //still in date range, skip this date
                                continue;
                            } else {
                                dateRangeEnd = null;
                            }
                        }
                        if (paVO.getFloristCutoffDate() != null) {
                            //check GNADD
                            if (gnaddLevel >= 1 && dateVO.getDeliveryDate().compareTo(gnaddDate) < 0 && 
                                (cszAvailGnaddFlag != null && cszAvailGnaddFlag.equals("Y"))) {
                                //Zip is shut off
                                if(logger.isDebugEnabled())
                                  logger.debug("Zip is on GNADD hold");
                                continue;
                            }
                            dateVO.setShipMethod("FL");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("FL"));
                            Date endDate = cacheUtil.isDateInDeliveryDateRange(dateVO.getDeliveryDate());
                            if (endDate != null) {
                                dateVO.setEndDate(endDate);
                                dateRangeEnd = endDate;
                                //logger.debug("found date range: " + commonUtil.dateToString(dateVO.getDeliveryDate()) + " " + commonUtil.dateToString(endDate));
                            }
                        } else if (paVO.getShipDateGR() != null) {
                            dateVO.setShipMethod("GR");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("GR"));
                        } else if (paVO.getShipDate2D() != null) {
                            dateVO.setShipMethod("2F");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("2D"));
                        } else if (paVO.getShipDateND() != null) {
                            dateVO.setShipMethod("ND");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("ND"));
                        } else if (paVO.getShipDateSA() != null) {
                            dateVO.setShipMethod("SA");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("SA"));
                        } else if (paVO.getShipDateSU() != null) {
                            dateVO.setShipMethod("SU");
                        } else if (countryId != null && !countryId.equals("")) {
                            dateVO.setShipMethod("FL");
                            //feeVO.setFeeType(FeeTypeEnum.toFeeType("FL"));
                        } else {
                            dateVO.setShipMethod("??");
                        }

                        OrderVO orderVO = new OrderVO();
                        orderVO.setOrderDate(new Date());
                        orderVO.setBuyerEmailAddress(requestVO.getCustomerEmail());
                        orderVO.setBuyerSignedIn(requestVO.getCustomerSignedInFlag());
                        orderVO.setFreeShippingMembershipInCartFlag(requestVO.getFreeShipMemberInCartFlag());
                        orderVO.setCompanyId(companyId);
                        
                        OrderDetailsVO orderDetailsVO = new OrderDetailsVO();
                        orderDetailsVO.setSourceCode(sourceCode);
                        orderDetailsVO.setDeliveryDate(dateVO.getDeliveryDate());
                        orderDetailsVO.setProductId(productId);
                        orderDetailsVO.setProductType(pmVO.getProductType());
                        orderDetailsVO.setShipMethod(dateVO.getShipMethod());
                        orderDetailsVO.setProductsAmount(price.toString());
                        
                        BigDecimal standardPrice = pmVO.getStandardPrice();
                        BigDecimal deluxePrice = pmVO.getDeluxePrice();
                        BigDecimal premiumPrice = pmVO.getPremiumPrice();
                        String variablePriceFlag = pmVO.getVariablePriceFlag();
                        BigDecimal zero = new BigDecimal("0");
                        
                        if(standardPrice != null && standardPrice.compareTo(price) == 0 && !price.equals(zero))
                        {
                            orderDetailsVO.setSizeChoice("A");
                        }
                        else if(deluxePrice != null && deluxePrice.compareTo(price) == 0 && !price.equals(zero))
                        {
                        	orderDetailsVO.setSizeChoice("B");
                        }
                        else if(premiumPrice != null && premiumPrice.compareTo(price) == 0 && !price.equals(zero))
                        {
                        	orderDetailsVO.setSizeChoice("C");
                        }
                        else if(variablePriceFlag != null && variablePriceFlag.equalsIgnoreCase("Y"))
                        {
                        	orderDetailsVO.setSizeChoice("A");
                        }
                        
                        if(orderDetailsVO.getSizeChoice() == null){
                        	//retrieve good, better, best 
                        	//set size choice based off of default flag and display sequence
                        	List<GBBVO> goodBetterBestList = pmVO.getGbbList();
                        	if (goodBetterBestList != null) {
                            	for(int j = 0; j < goodBetterBestList.size(); j++) {
                                	GBBVO gbb = goodBetterBestList.get(j);
                                	if(gbb.getDefaultFlag() != null && !gbb.getDefaultFlag().equals("")
                                	   && gbb.getDisplaySequence() != null && !gbb.getDisplaySequence().equals("")){
                                		if(gbb.getDefaultFlag().equalsIgnoreCase("Y")){
                                			if(gbb.getDisplaySequence().equalsIgnoreCase("1")){
                                				orderDetailsVO.setSizeChoice("A");
                                			}
                                			else if(gbb.getDisplaySequence().equalsIgnoreCase("2")){
                                				orderDetailsVO.setSizeChoice("B");
                                			} 
                                			else if(gbb.getDisplaySequence().equalsIgnoreCase("3")){
                                				orderDetailsVO.setSizeChoice("C");
                                			}
                                		}
                                	}
                                }
                            } else {
                            	//GBB list is null. Use standard price as default.
                            	orderDetailsVO.setSizeChoice("A");
                            }
                        }

                        RecipientAddressesVO recipientAddresses = new RecipientAddressesVO();
                        recipientAddresses.setPostalCode(zipCode);
                        recipientAddresses.setCountry(countryId);
                        recipientAddresses.setStateProvince(cacheUtil.getZipCodeState(zipCode));
                        List<RecipientAddressesVO> recipientAddressesList = new ArrayList();
                        recipientAddressesList.add(recipientAddresses);
                        
                        RecipientsVO recipientsVO = new RecipientsVO();
                        recipientsVO.setRecipientAddresses(recipientAddressesList);
                        List<RecipientsVO> recipientsList = new ArrayList();
                        recipientsList.add(recipientsVO);
                        
                        orderDetailsVO.setRecipients(recipientsList);
                        List<OrderDetailsVO> orderDetailsList = new ArrayList();
                        orderDetailsList.add(orderDetailsVO);
                        
                        orderVO.setOrderDetail(orderDetailsList);
                        
                        RecalculateOrderCacheDAO rocDAO = new RecalculateOrderCacheDAO();
                        RecalculateOrderBO recalcOrderBO = new RecalculateOrderBO(rocDAO, false);
                        
                        recalcOrderBO.recalculate(conn, orderVO);
                        
                        Iterator it = null;
                        BigDecimal serviceFeeAmount = new BigDecimal("0");
                        BigDecimal shippingFeeAmount = new BigDecimal("0");
                        
                        it = orderVO.getOrderDetail().iterator();
                        while(it.hasNext())
                        {
                        	orderDetailsVO = (OrderDetailsVO)it.next();
                        	serviceFeeAmount = new BigDecimal(orderDetailsVO.getServiceFeeAmount());
                            shippingFeeAmount = new BigDecimal(orderDetailsVO.getShippingFeeAmount());
                        	dateVO.setServiceFeeAmt(serviceFeeAmount);
                            dateVO.setShippingFeeAmt(shippingFeeAmount);
                            if(logger.isDebugEnabled()) {
                                logger.debug("service fee: " + dateVO.getServiceFeeAmt().toString());
                                logger.debug("shipping fee: " + dateVO.getShippingFeeAmt().toString());
                            }

                        }
                                
                        if(logger.isDebugEnabled())
                          logger.debug("date: " + commonUtil.dateToString(dateVO.getDeliveryDate()) + " " + dateVO.getShipMethod() + " " + orderDetailsVO.getServiceFeeAmount() + " / " + orderDetailsVO.getShippingFeeAmount());
                                            
                        // set description and message
                        String description = "";
                        String message = null;
                        SimpleDateFormat sdf = null;
                        Date startDate = dateVO.getDeliveryDate();
                        Date endDate = dateVO.getEndDate();
                        //String feeType = FeeTypeEnum.toFeeTypeString(feeVO.getFeeType());
                        String feeType = "";
                        BigDecimal feeAmt = BigDecimal.ZERO;
                                                      
                        if(serviceFeeAmount.compareTo(BigDecimal.ZERO) > 0){
                            feeAmt = feeAmt.add(serviceFeeAmount);
                            feeType = "Service Fee";
                        }
                        if(shippingFeeAmount.compareTo(BigDecimal.ZERO) > 0){
                            feeAmt = feeAmt.add(shippingFeeAmount);
                            if(feeType.length() > 0) {
                                feeType = feeType + " and ";
                            }
                            feeType = feeType + "Shipping Cost";
                        }
                        if(endDate == null) {
                            // no date range
                            
                        	//Defect 10500 Same Day Upcharge
                        	//If delivery date = today send date in following format
                        	//Mon DD - Today
                        	Calendar cal = Calendar.getInstance();
                            
                            String DATE_FORMAT = "MM/dd/yyyy";
                            sdf = new SimpleDateFormat(DATE_FORMAT); 
                            String dateString = sdf.format(cal.getTime());
                            Date currentDate = sdf.parse(dateString);
                            
                            if (currentDate.compareTo(startDate) == 0){
                            	sdf = new SimpleDateFormat(DATE_FORMAT_TODAY);
                                description = sdf.format(startDate) + "Today";
                            }
                            else{
                            	sdf = new SimpleDateFormat(DATE_FORMAT_NO_DATE_RANGE);
                                description = sdf.format(startDate);
                            }
                        	
                        } else {
                            sdf = new SimpleDateFormat(DATE_FORMAT_DATE_RANGE);
                            description = sdf.format(startDate) + " to " + sdf.format(endDate);
                        }
                        
                        if(feeAmt.compareTo(BigDecimal.ZERO) > 0) {
                            description = description + " - " + "$" + feeAmt.toString();
                            message = feeType + ": $" + feeAmt.toString();
                        }
                        
                        dateVO.setDescription(description);
                        dateVO.setMessage(message);
                        dateList.add(dateVO);
                    }

                    respVO.setAvailDateList(dateList);
                    respVO.setDisplayServiceFeeFlag(smvo.getDisplayServiceFeeFlag());
                    respVO.setDisplayShippingFeeFlag(smvo.getDisplayShippingFeeFlag());
                    
                    // Transform results.
                    responseString = commonUtil.transform(respVO);
                }
                productDatesTimer.stop();
                if(logger.isDebugEnabled())
                  logger.debug("timer: " + productDatesTimer.getMilliseconds() + " " + availableDates.length);
            } catch (RespondWithErrorException e) {
                requestVO.setErrorCode(e.getMessage());
                requestVO.setErrorFieldValue(requestVO.getSourceCode());
                responseString = commonUtil.transformError(requestVO);
            }  catch (Exception e) {
                logger.error(e);
                throw e;
            } finally {
                if (conn != null) {
                    conn.close();
                }
            }

            return responseString;
        }

    
    private String getCountryType(String countryId) throws Exception {
        String countryType = null;
        if (countryId == null || countryId.equals("")) {
            countryId = "US";
        }
        CountryMasterVO country = cacheUtil.getCountryById(countryId);
        if (country != null) {
              countryType = country.getCountryType();
        }
        if(logger.isDebugEnabled()) {
            logger.debug("countryType: " + countryType);
        }

        // If the country is not in the country master table, return null.
        return countryType;
    }    
        
}
