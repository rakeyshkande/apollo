@echo off

if "%1" == "" (
    @echo .
    @echo .
    @echo . cnf [configuration name]
    @echo .
) ELSE (
    ant -file cnf.xml -Dconfiguration.name=%1 
)
