<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
<xsl:decimal-format name="money_format" decimal-separator="." grouping-separator=","/>

  <xsl:param name="orderDetailId"/>
  <xsl:param name="itemCount"/>

  <xsl:variable name="transmission_type" select="'order'"/>
  <xsl:variable name="fol_indicator" select="'FTD'"/>
  <xsl:variable name="delimiter" select="'~~'"/>
  <xsl:variable name="space" select="' '"/>
  <xsl:variable name="empty" select="''"/>
  <xsl:variable name="amazon_order" select="'AMZNI'"/>
  <xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
  <xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>
 <!-- <xsl:variable name="isAmazonOrder" select="boolean(translate(/order/header/origin, $lower_case, $upper_case) = translate(key('originationCodes', '$amazon_order')/value, $lower_case, $upper_case))"/>-->

 <xsl:variable name="isAmazonOrder" select="boolean(/order/header[origin=$amazon_order])"/>


  <xsl:template match="/">

    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_number"/>
    <!-- Order Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$transmission_type"/>
    <!-- Tranmisssion Type -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/item_source_code"/>
    <!-- 1 Source Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_first_name"/>
    <!-- 2 Bill To First Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_last_name"/>
    <!-- 3 Bill To Last Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_address1"/>
    <!-- 4 Bill To Address Line One -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_address2"/>
    <!-- 5 Bill To Address Line Two -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_city"/>
    <!-- 6 Bill To City -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_state"/>
    <!-- 7 Bill To State -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_postal_code"/>
    <!-- 8 Bill To Zip -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_evening_phone"/>
    <!-- 9 Bill To Home Phone -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_daytime_phone"/>
    <!-- 10 Bill To Work Phone -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_fax"/>
    <!-- 11 Bill To Fax Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_email_address"/>
    <!-- 12 Bill to Email -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_country"/>
    <!-- 13 Bill to Country -->
    <xsl:value-of select="$delimiter"/>
    <xsl:call-template name="transform_credit_card_type">
      <xsl:with-param name="credit_card_type" select="/order/header/payment_method_id"/>
    </xsl:call-template>
    <!-- 14 CC Type -->
    <xsl:value-of select="/order/header/cc_number"/>
    <!-- 15 CC Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="substring(/order/header/cc_exp_date, 1, 2)"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="substring(/order/header/cc_exp_date, 4, 2)"/>
    <!-- 16 CC Expiration Date -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_approval_code"/>
    <!-- 17 CC Approval Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_avs_result"/>
    <!-- 18 CC AVS Result -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_approval_amt"/>
    <!-- 19 CC Approval Amount -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_acq_data"/>
    <!-- 20 ACQ Reference Data -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_approval_verbage"/>
    <!-- 21 Approval Verbiage -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_approval_action_code"/>
    <!-- 21 Approval Action Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:choose>
      <xsl:when test="$isAmazonOrder" >
        <xsl:call-template name="calculate_tax" >
           <xsl:with-param name="amazon_tax" select="/order/items/item[order_detail_id = $orderDetailId]/tax_amount"/>
           <xsl:with-param name="amazon_ship_tax" select="/order/items/item[order_detail_id=$orderDetailId]/shipping_tax"/>
        </xsl:call-template> 
      </xsl:when>
      <xsl:otherwise>
         <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/tax_amount"/>
      </xsl:otherwise>
    </xsl:choose>
    <!--
       <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/tax_amount"/>
       <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/shipping_tax"/>
    -->
   
    <!-- 23 Tax Amount -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_total"/>
    <!-- 24 Order Total -->
    <xsl:value-of select="$delimiter"/>
    <xsl:choose>
      <!--xsl:when test="boolean(/order/header/membership_type)"-->
      <!--xsl:when test="boolean(/order/header/membership_type) and /order/header/membership_type != ''"-->
      <!-- Not Null && Not Empty String && Not TARGET -->
      <xsl:when test="string-length(/order/header/membership_type) > 0 and /order/header/membership_type != '' and /order/header/membership_type != 'TARGET'">
      	<xsl:value-of select="/order/header/membership_type"/>
	    <xsl:text>:</xsl:text>
	    <xsl:value-of select="/order/header/membership_id"/>
	    <xsl:text>,</xsl:text>
	    <xsl:text>FNAME:</xsl:text>
	    <xsl:value-of select="/order/header/membership_first_name"/>
	    <xsl:text>,</xsl:text>
	    <xsl:text>LNAME:</xsl:text>
	    <xsl:value-of select="/order/header/membership_last_name"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="/order/header/co_brands/co_brand">
	      <xsl:value-of select="cb_name"/>
	      <xsl:text>:</xsl:text>
	      <xsl:value-of select="cb_data"/>
	      <xsl:text>,</xsl:text>
	    </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
    <!-- 25 Co-Branded Info -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/order_date"/>
    <!-- 26 Transaction Date -->
    <xsl:choose>
      <xsl:when test="count(/order/header/gift_certificates/gift_certificate) > 0">
		<xsl:value-of select="$delimiter"/>
        <xsl:value-of select="count(/order/header/gift_certificates/gift_certificate)"/>
        <xsl:value-of select="$delimiter"/>
   	    <xsl:value-of select="/order/header/gift_certificates/gift_certificate/gc_number"/>
        <xsl:text>:</xsl:text>
        <xsl:value-of select="/order/header/gift_certificates/gift_certificate/gc_amount"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$delimiter"/>
        <xsl:value-of select="$delimiter"/>
      </xsl:otherwise>
    </xsl:choose>
    <!-- 27 and 28 Gift Certificate Detail -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/news_letter_flag"/>
    <!-- 29 Newsletter Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_first_name"/>
    <!-- 30 Ship To First Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_last_name"/>
    <!-- 31 Ship To Last Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address1"/>
    <xsl:text>$</xsl:text>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address2"/>
    <!-- 32 Ship To Address One -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$space"/>
    <!-- 33 Ship To Address Two -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_city"/>
    <!-- 34 Ship To City -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_state"/>
    <!-- 35 Ship To State -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_postal_code"/>
    <!-- 36 Zip Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_country"/>
    <!-- 37 Country -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_international"/>
    <!-- 38 International Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address1"/>
    <xsl:text>$</xsl:text>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_address2"/>
    <!-- 39 QMS Address One -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$space"/>
    <!-- 40 QMS Address Two -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_city"/>
    <!-- 41 QMS City -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_state"/>
    <!-- 42 QMS State -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_postal_code"/>
    <!-- 43 QMS Zip -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_result_code"/>
    <!-- 44 QMS Result Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_override_flag"/>
    <!-- 45 QMS Override Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_firm_name"/>
    <!-- 46 QMS Firm Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_latitude"/>
    <!-- 47 QMS Latitude -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_longitude"/>
    <!-- 48 QMS Longitude -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/qms_usps_range_record_type"/>
    <!-- 49 QMS USPS Range Record Type -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_phone"/>
    <!-- 50 Ship To Phone -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_phone_ext"/>
    <!-- 51 Ship To Extension -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ship_to_type_code"/>
    <!-- 52 Ship To Type -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ship_to_type_name"/>
    <!-- 53 Ship To Type Name -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ship_to_type_info"/>
    <!-- 54 Ship To Type Info -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/occasion_description"/>
    <!-- 55 Occasion -->
    <xsl:value-of select="$delimiter"/>
    <xsl:choose>
      <xsl:when test="/order/items/item[order_detail_id = $orderDetailId]/subcodeid and normalize-space(/order/items/item[order_detail_id = $orderDetailId]/subcodeid) != ''">
        <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/subcodeid"/>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/productid"/>
      </xsl:otherwise>
    </xsl:choose>
    <!-- 56 Item Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/product_price"/>
    <!-- 57 Item Price -->
    <xsl:value-of select="$delimiter"/>
    <xsl:if test="/order/items/item[order_detail_id = $orderDetailId]/first_color_choice/text() or /order/items/item[order_detail_id = $orderDetailId]/second_color_choice/text()">
      <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/first_color_choice"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/second_color_choice"/>
	</xsl:if>
    <!-- 58 Color/Size -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="count(/order/items/item[order_detail_id = $orderDetailId]/add_ons/add_on)"/>
    <!-- 59 Add-On Count -->
    <xsl:value-of select="$delimiter"/>
    <xsl:if test="count(/order/items/item[order_detail_id = $orderDetailId]/add_ons/add_on) > 0">
      <xsl:for-each select="/order/items/item[order_detail_id = $orderDetailId]/add_ons/add_on">
        <xsl:value-of select="code"/>
        <xsl:text>:</xsl:text>
        <xsl:value-of select="quantity"/>
        <xsl:text>,</xsl:text>
      </xsl:for-each>
	</xsl:if>
    <!-- 60 Add-On Detail -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/service_fee"/>
    <!-- 61 Service Fee -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/extra_shipping_fee"/>
    <!-- 62 Extra Shipping Fee -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="substring(/order/items/item[order_detail_id = $orderDetailId]/delivery_date, 1, 10)"/>
    <!-- 63 Delivery Date -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/card_message"/>
    <!-- 64 Card Message -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/card_signature"/>
    <!-- 65 Card Signature -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/special_instructions"/>
    <!-- 66 Special Instructions -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/shipping_method"/>
    <!-- 67 Shipping method -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/drop_ship_charges"/>
    <!-- 68 Drop Ship Charges -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_daytime_ext"/>
    <!-- 69 Bill To Work Extension -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/second_delivery_date"/>
    <!-- 70 Second Delivery Date -->
    <xsl:value-of select="$delimiter"/>
    <xsl:if test="/order/items/item[order_detail_id = $orderDetailId]/lmg_email_address and /order/items/item[order_detail_id = $orderDetailId]/lmg_email_address != ''">
      <xsl:text>Y</xsl:text>
	</xsl:if>
    <!-- 71 Last Minute Gift Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$fol_indicator"/>
    <!-- 72 FOL Indicator -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$space"/>
    <!-- 73 Socket Date/Time Stamp -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/lmg_email_address"/>
    <!-- 74 Last Minute Gift Email Address -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/lmg_email_signature"/>
    <!-- 75 Last Minute Gift Email Signature -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/master_order_number"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="$itemCount"/>
    <!-- 76 Master Order Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$space"/>
    <!-- 77 Not Used -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/sender_release_flag"/>
    <!-- 78 Sender Release Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ariba_unspsc_code"/>
    <!-- 79 Ariba UNSPSC Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$empty"/>
    <!-- 80 Item Description -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/ariba_payload"/>
    <!-- 81 Ariba Payload -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/ariba_buyer_cookie"/>
    <!-- 82 Ariba Buyer Cookie -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/ariba_asn_buyer_number"/>
    <!-- 83 Ariba ASN Buyer Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$empty"/>
    <!-- 84 Order Message URL -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ariba_po_number"/>
    <!-- 85 Ariba PO Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ariba_cost_center"/>
    <!-- 86 Ariba Cost Center -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="substring(/order/header/csr_id, 1, 5)"/>
    <!-- 87 User ID -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$empty"/>
    <!-- 88 Call Time -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/dnis_code"/>
    <!-- 89 DNIS -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/yellow_pages_code"/>
    <!-- 90 Yellow Pages Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_comments"/>
    <xsl:value-of select="$space"/>
    <xsl:value-of select="/order/header/contact_first_name"/>
    <xsl:value-of select="$space"/>
    <xsl:value-of select="/order/header/contact_last_name"/>
    <xsl:value-of select="$space"/>
    <xsl:value-of select="/order/header/contact_phone"/>
    <xsl:value-of select="$space"/>
    <xsl:value-of select="/order/header/contact_phone_ext"/>
    <xsl:value-of select="$space"/>
    <xsl:value-of select="/order/header/contact_email"/>
    <!-- 91 Order Comments -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$empty"/>
    <!-- 92 Contact Information -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/origin"/>
    <!-- 93 Origin -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/florist_number"/>
    <!-- 94 Florist Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/ariba_ams_project_code"/>
    <!-- 95 AMS Project code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cc_aafes_ticket"/>
    <!-- 96 Military Star Ticket Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/novator_sender_release_flag"/>
    <!-- 97 Novator Sender Release Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/product_substitution_acknowledgement"/>
    <!-- 98 Product substitution acknowledgement -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/discounted_product_price"/>
    <!-- 99 Retail Variable Price -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/cobrand_cc_code"/>
    <!-- 100 CoBrand Credit Card Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/hp_sequence"/>
    <!-- 101 Sequential Order Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/buyer_id"/>
    <!-- 102 Buyer Customer Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/recip_id"/>
    <!-- 103 Recipient Customer Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/fraud_flag"/>
    <xsl:for-each select="/order/header/fraud_comments/fraud_comment">
      <xsl:text>,</xsl:text>
      <xsl:value-of select="fraud_id"/>
    </xsl:for-each>
    <xsl:for-each select="/order/header/fraud_codes/fraud_code">
      <xsl:text>,</xsl:text>
      <xsl:value-of select="fraud_code_id"/>
    </xsl:for-each>
    <!-- 104 Fraud Key Data -->
    <xsl:value-of select="$delimiter"/>
    <xsl:for-each select="/order/header/fraud_comments/fraud_comment">
      <xsl:value-of select="fraud_comments"/>
      <xsl:value-of select="$space"/>
    </xsl:for-each>
    <!-- 105 Fraud Comments -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/source_code"/>
    <!-- 106 Order Source Code -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/iow_flag"/>
    <!-- 107 Item of the week Flag -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/pdb_price"/>
    <!-- 108 Product Database Price Field -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/transaction"/>
    <!-- 109 Transaction -->
    <xsl:value-of select="$delimiter"/>
    <xsl:choose>
  		<xsl:when test="string(number(/order/items/item[order_detail_id=$orderDetailId]/wholesale)) = 'NaN'">
		    <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/wholesale"/>
  		</xsl:when>
  		<xsl:otherwise>
    		<xsl:value-of select="round(/order/items/item[order_detail_id=$orderDetailId]/wholesale * 100)"/>
  		</xsl:otherwise>
	</xsl:choose>
    <!-- 110 Wholesale -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/commission"/>
    <!-- 111 Commission -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/loss_prevention_indicator"/>
    <!-- 112 Loss Prevention Indicator -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/items/item[order_detail_id=$orderDetailId]/shipping_tax"/>
    <!-- 113 Shipping Tax -->
  </xsl:template>
  <xsl:template name="transform_credit_card_type">
    <xsl:param name="credit_card_type"/>
    <xsl:choose>
      <!-- CC Type -->
      <xsl:when test="contains($credit_card_type, 'VI')">
        <xsl:value-of select="'Visa'" />
      </xsl:when>
      <xsl:when test="contains($credit_card_type, 'MC')">
        <xsl:value-of select="'Master'"/>
      </xsl:when>
      <xsl:when test="contains($credit_card_type, 'AX')">
        <xsl:value-of select="'Amex'" />
      </xsl:when>
      <xsl:when test="contains($credit_card_type, 'DI')">
        <xsl:value-of select="'Discover'"/>
      </xsl:when>
      <xsl:when test="contains($credit_card_type, 'DC')">
        <xsl:value-of select="'Diners'"/>
      </xsl:when>
      <xsl:when test="contains($credit_card_type, 'CB')">
        <xsl:value-of select="'Carte'"/>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="$credit_card_type"/>
      </xsl:otherwise>
    </xsl:choose>
    <!-- Add Delimiter -->
    <xsl:value-of select="$delimiter"/>
  </xsl:template>
  
<!--Calculate taxes-->
    <xsl:template name="calculate_tax">
    <xsl:param name="amazon_ship_tax"/>
    <xsl:param name="amazon_tax"/>
    <xsl:variable name="zero_value" select="'0'"/>
    <xsl:choose>
      <xsl:when test="string-length($amazon_ship_tax) &lt;= '0' and string-length($amazon_tax) &lt;= '0'" >
            <xsl:value-of select="$zero_value"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &gt; '0' and string-length($amazon_tax) &lt;= '0'">
            <xsl:value-of select="$amazon_ship_tax"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &lt;= '0' and string-length($amazon_tax) &gt; '0'">
            <xsl:value-of select="$amazon_tax"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &gt; '0' and string-length($amazon_tax) &gt; '0'">
            <xsl:value-of select="$amazon_tax + $amazon_ship_tax"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$zero_value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>