<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  
  <xsl:param name="orderDetailId"/>
  <xsl:param name="itemCount"/>
  
  <xsl:variable name="transmission_type" select="'pending'"/>
  <xsl:variable name="fol_indicator" select="'FTD'"/>
  <xsl:variable name="delimiter" select="'~~'"/>
  <xsl:variable name="space" select="' '"/>
  <xsl:variable name="empty" select="''"/>
  
  <xsl:template match="/">
    <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_number"/>
    <!-- Order Number -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="$transmission_type"/>
    <!-- Tranmisssion Type -->
    <xsl:value-of select="$delimiter"/>
    <xsl:value-of select="/order/header/master_order_number"/>
    <xsl:text>/</xsl:text>
    <xsl:value-of select="$itemCount"/>
    <!-- Master Order Number -->
    <xsl:choose>
      <xsl:when test="count(/order/items/item[order_detail_id = $orderDetailId]/order_dispositions/disposition) > 0">
      	<xsl:value-of select="$delimiter"/>
        <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_dispositions/disposition/description"/>
        <xsl:value-of select="$delimiter"/>
        <xsl:value-of select="/order/items/item[order_detail_id = $orderDetailId]/order_dispositions/disposition/comments"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$delimiter"/>
        <xsl:value-of select="$delimiter"/>
      </xsl:otherwise>
    </xsl:choose>
    <!-- Dispositions -->
  </xsl:template>
</xsl:stylesheet>