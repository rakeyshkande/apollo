package com.ftd.osp.sequencecheck.dao;

import com.ftd.osp.sequencecheck.vo.PrefixVO;
import com.ftd.osp.sequencecheck.vo.MissingVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains data access methods used within the Sequence Checker project.
 */
public class SequenceDAO 
{
  private Connection conn;
  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.osp.sequencecheck.dao.SequenceDAO";
  private static final String LOCK_PREFIX = "LOCKED";

  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
  
  /**
   * Constructor
   * @param conn Connection
   */
  public SequenceDAO(Connection conn)
  {
    this.conn = conn;
    logger =  new Logger(LOGGER_CATEGORY);
  }
  
  /**
   * Returns the External_Order_Prefix rows
   * 
   * @param prefix String
   * @return HashMap
   */
  public Map getPrefixData(String prefix) throws Exception
  {
      logger.debug("getPrefixData(" + prefix + ")");
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
      
      dataRequest.setStatementID("VIEW_EXTERNAL_ORDER_PREFIX");
      Map paramMap = new HashMap();
      paramMap.put("IN_PREFIX",prefix); 
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      
      Document xmlDoc = (Document)dataAccessUtil.execute(dataRequest);

      StringWriter sw = new StringWriter();
      DOMUtil.print(xmlDoc, new PrintWriter(sw));
      logger.debug("xmlDoc:\n" + sw.toString());
      
      Map prefixMap = new HashMap();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      String xpath = "prefixList/prefix";
      NodeList nl = DOMUtil.selectNodes(xmlDoc, xpath);
      if(nl.getLength() > 0){
          for (int i = 0; i < nl.getLength(); i++)
          {
              Element node = (Element)nl.item(i);

              PrefixVO vo = new PrefixVO();
              vo.setPrefix(node.getAttribute("order_prefix"));
              vo.setEndOrderNumber(Long.valueOf(node.getAttribute("last_order_number")));
              vo.setStartOrderNumber(vo.getEndOrderNumber() + 1);
              vo.setCreatedOn(sdf.parse(node.getAttribute("created_on")));
              prefixMap.put(vo.getPrefix(), vo);
          }
      }

      return prefixMap;
  }

    /**
     * Returns the Missing_Order_Sequence row
     * 
     * @return HashMap
     */
    public Map getMissingSequence() throws Exception
    {
        logger.debug("getMissingSequence()");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        dataRequest.setStatementID("GET_MISSING_ORDER_SEQUENCE");
        Map paramMap = new HashMap();
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        Map missingMap = new HashMap();

        //if something was found
        while (rs.next())
        {
            MissingVO vo = new MissingVO();
            vo.setOrderNumber(rs.getString("EXTERNAL_ORDER_NUMBER"));
            vo.setMessageID(rs.getString("SYSTEM_MESSAGE_ID"));
            vo.setCreated(rs.getDate("CREATED_ON"));
            missingMap.put(vo.getOrderNumber(), vo);
        }
        
        return missingMap;
    }

  /**
   * Updates the External_Order_Preifx row for the given prefix.
   * 
   * @param vo PrefixVO
   */
  public void updateExternalOrderPrefix(PrefixVO vo) throws Exception
  {

      logger.debug("updateExternalOrderPrefix(" + vo.getPrefix() + "): " + vo.getEndOrderNumber());
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(conn);
    
      dataRequest.setStatementID("UPDATE_EXTERNAL_ORDER_PREFIX");
      Map paramMap = new HashMap();
      paramMap.put("IN_PREFIX",vo.getPrefix()); 
      paramMap.put("IN_LAST_ORDER_NUMBER",vo.getEndOrderNumber()); 
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      Map outputs = (Map) dataAccessUtil.execute(dataRequest);
    
      String status = (String) outputs.get(STATUS_PARAM);
      if(status.equals("N"))
      {
          String message = (String) outputs.get(MESSAGE_PARAM);
          throw new Exception(message);
      }
      
  }   

    /**
     * Deletes the External_Order_Preifx row for the given prefix.
     * 
     * @param vo PrefixVO
     */
    public void deleteExternalOrderPrefix(PrefixVO vo) throws Exception
    {

        logger.debug("deleteExternalOrderPrefix(" + vo.getPrefix() + ")");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);

        dataRequest.setStatementID("DELETE_EXTERNAL_ORDER_PREFIX");
        Map paramMap = new HashMap();
        paramMap.put("IN_PREFIX",vo.getPrefix()); 
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        
    }   

    /**
     * Inserts a Missing_Order_Sequence row
     * 
     * @param externalOrderNumber String
     * @param systemMessageId String
     * @return HashMap
     */
    public void insertMissingSequence(String externalOrderNumber, String systemMessageId) throws Exception
    {
        logger.debug("insertMissingSequence(" + externalOrderNumber + " , " + systemMessageId + ")");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        dataRequest.setStatementID("INSERT_MISSING_ORDER_SEQUENCE");
        Map paramMap = new HashMap();
        paramMap.put("IN_ORDER_NUMBER", externalOrderNumber);
        paramMap.put("IN_SYSTEM_MESSAGE_ID", systemMessageId);
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
    }

    /**
     * Deletes a Missing_Order_Sequence row
     * 
     * @param externalOrderNumber String
     * @return HashMap
     */
    public void deleteMissingSequence(String externalOrderNumber) throws Exception
    {
        logger.debug("deleteMissingSequence(" + externalOrderNumber + ")");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        dataRequest.setStatementID("DELETE_MISSING_ORDER_SEQUENCE");
        Map paramMap = new HashMap();
        paramMap.put("IN_ORDER_NUMBER", externalOrderNumber); 
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
    }


    public boolean isLocked() throws Exception {
        boolean result = false;
        Map prefixMap = getPrefixData(LOCK_PREFIX);
        if (prefixMap.size() <= 0) {
            result = false;
        } else {
            PrefixVO prefix = (PrefixVO)prefixMap.get(LOCK_PREFIX);
            Date createdOn = prefix.getCreatedOn();
            Date today = new Date();
            Long dateDiff = today.getTime() - createdOn.getTime();
            System.out.println(today.getTime() + " " + createdOn.getTime() + " " + dateDiff);
            Long secs = dateDiff / 1000;
            Long mins = secs / 60;
            System.out.println(secs + " " + mins);
            logger.debug("dateDiff: " + dateDiff + " --> " + mins + " minutes");
            Long threshhold = Long.parseLong("999999");
            String temp = getGlobalParameter("SEQUENCE_CHECKER", "LOCK_TIMEOUT_THRESHHOLD");
            if (temp != null) {
                threshhold = Long.parseLong(temp);
            }
            if (mins > threshhold) {
                logger.debug("Releasing lock");
                result = false;
                deleteLock();
            } else {
                result = true;
            }
        }
        return result;
    }


    public void writeLock() throws Exception {
        PrefixVO vo = new PrefixVO();
        vo.setPrefix(LOCK_PREFIX);
        vo.setEndOrderNumber(0);
        updateExternalOrderPrefix(vo);
    }


    public void deleteLock() throws Exception {
        PrefixVO vo = new PrefixVO();
        vo.setPrefix(LOCK_PREFIX);
        deleteExternalOrderPrefix(vo);
    }


    /**
    * This method is a wrapper for the SP_GET_GLOBAL_PARAMETER SP.
    * It populates a global parameter VO based on the returned record set.
    *
    * @param context - String
    * @param name - String
    * @return String
    */
    public String getGlobalParameter(String context, String name) throws Exception {
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        String rs = null;

        logger.info("getGlobalParameter (String context (" + context + "), String name (" + name + ")) :: GlobalParameterVO");
        /* setup store procedure input parameters */
        dataRequest.setStatementID("GET_GLOBAL_PARAM");
        Map paramMap = new HashMap();
        paramMap.put("IN_CONTEXT", context);
        paramMap.put("IN_PARAM", name);
        //dataRequest.setInputParams(paramMap);
        dataRequest.addInputParam("IN_CONTEXT", context);
        dataRequest.addInputParam("IN_PARAM", name);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        rs = (String)dataAccessUtil.execute(dataRequest);
        logger.info("retrieved: " + rs);
        dataRequest.reset();
        return rs;
    }

    /**
     * Returns the partner_id values from ptn_pi.partner_mapping
     * 
     * @return HashMap
     */
    public Map getPartnerPrefixes() throws Exception
    {
        logger.debug("getPartnerPrefixes()");
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(conn);
        
        dataRequest.setStatementID("GET_PARTNER_PREFIXES");
        Map paramMap = new HashMap();
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        CachedResultSet rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);
        Map partnerMap = new HashMap();

        //if something was found
        while (rs.next()) {
        	String partnerId = rs.getString("partner_id");
        	partnerMap.put(partnerId, partnerId);
        }
        
        return partnerMap;
    }

}
