package com.ftd.osp.sequencecheck.constants;

public interface ConfigurationConstants 
{
    public static final String SEQUENCE_CONFIG_FILE = "sequence_config.xml";
    public static final String APPLICATION_TIMEOUT = "APPLICATION_TIMEOUT";
}