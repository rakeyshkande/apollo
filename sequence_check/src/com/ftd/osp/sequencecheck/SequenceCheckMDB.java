package com.ftd.osp.sequencecheck;

import com.ftd.osp.sequencecheck.business.SequenceCheckBO;
import com.ftd.osp.utilities.plugins.Logger;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;


/**
 * This a message driven bean.
 *
 * When the container is start the setMessageDrivenContext is executed.
 *
 */
public class SequenceCheckMDB implements MessageDrivenBean, MessageListener 
{
  private MessageDrivenContext context;
  private static String LOGGER_CATEGORY = "com.ftd.osp.sequencecheck.SequenceCheckMDB";

  /**
   * ejbCreate
   */
  public void ejbCreate()
  {
  }

  /**
   * onMesage...a JMS message was received
   * @param msg
   */
  public void onMessage(Message msg)
  {
  
    Logger logger = new Logger(LOGGER_CATEGORY);
    SequenceCheckBO sequenceCheckBO = new SequenceCheckBO();
    try
    {
        logger.debug("New message");
        // Execute business logic
        
        sequenceCheckBO.execute();
    }
    catch(Exception e)
    {
        logger.error(e);
        try {
        	sequenceCheckBO.sendMessage("SequenceCheckMDB failed: " + e.getMessage());
        } catch (Exception se) {
        	logger.error(se);
        }
    }
    
  }

  public void ejbRemove()
  {
  }

  /**
   * This message is invoked when the container is started
   * @param ctx
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {

    this.context = ctx;
    
  }
  
}