package com.ftd.osp.sequencecheck.business;

import com.ftd.osp.sequencecheck.dao.SequenceDAO;
import com.ftd.osp.sequencecheck.vo.MissingVO;
import com.ftd.osp.sequencecheck.vo.PrefixVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This object contains all the logic to preform the sequence checking.
 * 
 * @author Ed Mueller
 */
public class SequenceCheckBO 
{

    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
    private static final String LOGGER_CATEGORY = "com.ftd.osp.sequencecheck.business.SequenceCheckBO";
    private static final String PROPERTY_FILE = "sequence_config.xml";

    Connection connection = null;
    String systemTempDir = null;
    Logger logger = new Logger(LOGGER_CATEGORY);

    long duplicateOrderMaxMessage = 0;
    long missingOrderMaxMessage = 0;
    long numberToDisplayWithThreshold = 0;
    int missingPurgeDays = 0;
    private String messageSource = "";
    private String SEQUENCE_DATASOURCE_NAME = "";
    ConfigurationUtil configUtil;

    /** 
     * Constructor
     * Reads the configuration file and sets some class variables.
     */
    public SequenceCheckBO() 
    {

    }

    /**
     * This method controls the process of the of Sequence check.  This method
     * checks for missing and duplicate sequences.  If either of these are found
     * system messages are generated.  This process also checks if any messages
     * that were previously flagged as missing have been received.  If this happens
     * the previosly generated system message is removed.
     * 
     * This method keeps tracks of missing order numbers and that last recieved
     * sequence numbers.  This is done using the local file system (temp directory).
     * The order sequences are tracked for each order prefix.  The order prefix 
     * consists of the alpha chars in the beginning of the order number.  The code
     * was written so that it will dynamically track order prefixes as they arrive.
     */
    public void execute()
    {

        String threshold = "";

        try {
            configUtil = ConfigurationUtil.getInstance();            

            //message source as inserted into systems message table
            messageSource = configUtil.getProperty(PROPERTY_FILE,"MESSAGE_SOURCE");
            SEQUENCE_DATASOURCE_NAME = configUtil.getProperty(PROPERTY_FILE, "DATASOURCE_NAME");
        }
        catch(Exception e)
        {
            logger.error("Error getting configuration values.");
            logger.error(e);
        }

        DataAccessUtil dau = null;
        DataRequest dataRequest = null;
        try {
            dau = DataAccessUtil.getInstance();
            dataRequest = this.retrieveDataRequest();
        }
        catch(Exception e)
        {
            logger.error(e);
        }

        //Max number of duplicate messages that should be sent.
        try
        {
            dataRequest.reset();
            dataRequest.addInputParam("IN_CONTEXT", "SEQUENCE_CHECKER");
            dataRequest.addInputParam("IN_PARAM", "DUPLICATE_ORDER_THRESHOLD");
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            threshold = (String)dau.execute(dataRequest);
            duplicateOrderMaxMessage = Long.parseLong(threshold);
        }
        catch(Throwable e)
        {
            logger.error("Invalid DUPLICATE_ORDER_THRESHOLD in database..defaulting to 100");
            logger.error(e);
            duplicateOrderMaxMessage = 100;
        }

        //get max number of missing order messages to send
        try
        {
            dataRequest.reset();
            dataRequest.addInputParam("IN_CONTEXT", "SEQUENCE_CHECKER");
            dataRequest.addInputParam("IN_PARAM", "MISSING_ORDER_THRESHOLD");
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            threshold = (String)dau.execute(dataRequest);
            missingOrderMaxMessage = Long.parseLong(threshold);
        }
        catch(Throwable e)
        {
            logger.error("Invalid MISSING_ORDER_THRESHOLD in database..defaulting to 100");
            logger.error(e);
            missingOrderMaxMessage = 100;
        }

        //get max number of order numbers to display on an 'over threshold message'
        try
        {
            dataRequest.reset();
            dataRequest.addInputParam("IN_CONTEXT", "SEQUENCE_CHECKER");
            dataRequest.addInputParam("IN_PARAM", "NUMBER_TO_DISPLAY_ON_THRESHOLD");
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            threshold = (String)dau.execute(dataRequest);      
            numberToDisplayWithThreshold = Long.parseLong(threshold);
        }
        catch(Throwable e)
        {
            logger.error("Invalid NUMBER_TO_DISPLAY_ON_THRESHOLD in database..defaulting to 1");
            logger.error(e);
            numberToDisplayWithThreshold = 1;
        }

        //number of days to purge missing messages
        try
        {
            dataRequest.reset();
            dataRequest.addInputParam("IN_CONTEXT", "SEQUENCE_CHECKER");
            dataRequest.addInputParam("IN_PARAM", "MISSING_MESSAGE_PURGE_DAYS");
            dataRequest.setStatementID("GET_GLOBAL_PARAM");
            threshold = (String)dau.execute(dataRequest);
            missingPurgeDays = Integer.parseInt(threshold);
        }
        catch(Throwable e)
        {
            logger.error("Invalid MISSING_MESSAGE_PURGE_DAYS in database..defaulting to 5");
            logger.error(e);
            missingPurgeDays = 5;
        }

        logger.debug("duplicateOrderMaxMessage: " + duplicateOrderMaxMessage);
        logger.debug("missingOrderMaxMessage: " + missingOrderMaxMessage);
        logger.debug("numberToDisplayWithThreshold: " + numberToDisplayWithThreshold);
        logger.debug("missingPurgeDays: " + missingPurgeDays);

        Map orderMap = new HashMap();
        Map missingMap = new HashMap();
        Map prefixMap = new HashMap();        
        Map receivedOrderPrefixs = new HashMap();
        Map partnerMap = new HashMap();

        List duplicateOrders = new ArrayList();

        try
        {
            dataRequest = this.retrieveDataRequest();

            SequenceDAO sdao = new SequenceDAO(connection);
            if (!sdao.isLocked()) {

                sdao.writeLock();

                prefixMap = sdao.getPrefixData(null);
                missingMap = sdao.getMissingSequence();
                partnerMap = sdao.getPartnerPrefixes();

                //get order numbers from database
                CachedResultSet sequenceResultSet = this.loadSequences(dataRequest);

                //if data was found in the DB
                if(sequenceResultSet != null && sequenceResultSet.getRowCount() > 0)
                {

                    //loop throuch each record returned by DB
                    while(sequenceResultSet.next())
                    {
                        //get order number
                        String orderNumber = (String)sequenceResultSet.getObject(1);
                        logger.debug("orderNumber: " + orderNumber);

                        //parse out prefix and number
                        String orderPrefix = getOrderPrefix(orderNumber);
                        
                        // skip if all numeric (new website orders)
                        if (orderPrefix == null || orderPrefix.equals("")) {
                            logger.debug("Skipping new website order");
                            continue;
                        }
                        
                        String orderDigits = getOrderDigits(orderNumber,orderPrefix);
                        long orderDigitValue = Long.parseLong(orderDigits);
                        
                        String tempPrefix = (String) partnerMap.get(orderPrefix);
                        if (tempPrefix != null) {
                        	orderNumber = orderNumber.replace(orderPrefix, "PTN_PI");
                        	orderPrefix = "PTN_PI";
                        }

                        //get last order number for this prefix
                        PrefixVO prefix = (PrefixVO)prefixMap.get(orderPrefix);
                        long lastOrderNumberValue = -1;
                        if(prefix != null)
                        {
                            lastOrderNumberValue = prefix.getEndOrderNumber();
                        }

                        //Check if this order number precedes the last order number for this prefix
                        if(prefix != null && orderDigitValue <= lastOrderNumberValue)
                        {
                            //If order is not in missing map then we assume it is a duplicate order number
                            MissingVO value = (MissingVO)missingMap.get(orderNumber);
                            if(value == null)
                            {
                                duplicateOrders.add(orderNumber);
                            }
                            //Order was previously marked as missing, delete the system message
                            else
                            {
                                processDeleteMessage(orderNumber,missingMap);
                            }

                        }
                        //Else, insert order into Map to be traversed later                      
                        else
                        {

                            //make sure this order wasn't received twice in the same load
                            if(orderMap.get(orderNumber) != null){
                                duplicateOrders.add(orderNumber);
                            }
                            else
                            {
                                //store all the prefixes in a map so we know who to check later
                                receivedOrderPrefixs.put(orderPrefix,orderPrefix);

                                //new order is put into map
                                orderMap.put(orderNumber,orderNumber);
                            }
                        }

                    }//end while more in DB result set

                    //send duplicate error messages
                    processDuplicateOrders(duplicateOrders);
        
                    //set prefix counters..this sets the beginning and end order num for each prefix
                    setPrefixCounters(orderMap,prefixMap);

                    //search for missing orders & then send missing order messages
                    List missingList = searchForMissing(orderMap, missingMap,prefixMap,receivedOrderPrefixs,sdao);
                    processMissingOrders(missingList,missingMap);

                    //update the prefix map with the last order numbers
                    updateLastSequence(prefixMap, receivedOrderPrefixs);

                }//end if sequence resultset obtained

                //remove old missing orders from the map
                cleanUpMissingMap(missingMap);

                sdao.deleteLock();

            }
            else
            {
                String message = "Sequence Check application is locked. Check frp.external_order_prefix.";
                sendMessage(message);
            }

        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
          if(dataRequest.getConnection() != null)
          {
            try
            {
              dataRequest.getConnection().close();
            }
            catch(Exception e){
              logger.error(e);
            }
          }
        }
    }

    /*
     * Remove old orders from the missing map.  This prevents the 
     * map from constantly increasing in size.
     * 
     * @param Map missing Map     * 
     */
    private void cleanUpMissingMap(Map missingMap)
    {
        logger.debug("cleanUpMissingMap()");
        Set missingSet = missingMap.keySet();
        Iterator iter = missingSet.iterator();        

        SequenceDAO sdao = new SequenceDAO(connection);

        //determine purge date
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, - missingPurgeDays);
        Date purgeDate = cal.getTime();

        //check each order in the missing map
        while(iter.hasNext())
        {
            //get order prefix
            String key = (String) iter.next();        

            //get starting and end point for this prefix
            MissingVO missingVO = (MissingVO)missingMap.get(key);
            logger.debug(key + " " + missingVO.getCreated());

            //if this is an old order remove it          
            if(purgeDate.after(missingVO.getCreated()))
            {
                try {
                    logger.debug("Deleting " + key);
                    sdao.deleteMissingSequence(key);
                }
                catch(Exception e) {
                    logger.error(e);
                }
            }
          
        }//end while has next

    }

    /*
     * This method examines all the received orders and determines if any orders
     * are missing.  All missing order and inserted into the missingMap.
     * 
     * @param Map orderMap Map of received orders
     * @param Map missingMap Map of missing orders (this contains data from past runs & is updated in this run)
     * @param Map prefixMap Map of order prefix data (contains data from past runs and this run)
     * @param Map receivedPrefixMap Map of all the order prefixs that were received in this run
     * @returns List List of ordernumbers which are missing
     */
    private List searchForMissing(Map orderMap, Map missingMap, Map prefixMap,
    		Map receivedOrderprefixs, SequenceDAO sdao)
      throws Exception
    {

        //list of new Missing orders
        List missingList = new ArrayList();
    
        //loop through all order prefixes that were returned
        Set prefixSet = receivedOrderprefixs.keySet();
        Iterator iter = prefixSet.iterator();        
        while(iter.hasNext())
        {
          //get order prefix
          String receivedOrderPrefix = (String) iter.next();        

          //get starting and end point for this prefix
          PrefixVO prefix = (PrefixVO)prefixMap.get(receivedOrderPrefix);
          long prefixStart = prefix.getStartOrderNumber();
          long prefixEnd = prefix.getEndOrderNumber();
          long stringLength = prefix.getMinStringLength();
          logger.debug("searchForMissing: " + prefix.getPrefix() + " start: " + prefixStart + " end: " + prefixEnd + " max: " + missingOrderMaxMessage);

          //make sure all the sequences are in the order map
          for(long i = prefixStart; i<= prefixEnd; i++)
          {
            //this order number should exist in the map
            String searchOrder = formatMissingOrder(receivedOrderPrefix, i, stringLength);
            String orderNumber = (String)orderMap.get(searchOrder);

            //if null this order is missing
            if(orderNumber == null)
            {
                 //create missing VO
                 MissingVO missingVO = new MissingVO();
                 missingVO.setOrderNumber(searchOrder);
                 missingVO.setCreated(new Date());
                 missingMap.put(searchOrder,missingVO);
   
                 //also add order number to list
                 missingList.add(searchOrder);
           
                 //stop if max missing is reached
                 if (missingList.size() > missingOrderMaxMessage) {
                     logger.debug("Missing Max threshhold reached");
                     break;
                 }              
             }//end order number null
          }//end for look each order for prefix          
        }//while more prefixes

        return missingList;
    }

  /*
   * This method formats and order number for display purposes.  Formatting includes
   * padding the order number with zeroes.  The prefix map contains the length of
   * past orders numbers that were received for this prefix.  Using this information
   * it determines how many zeroes to pad the order number with.
   * 
   * @param String prefix The order prefix
   * @param long orderNumber
   * @param String stringLength The length the order number should be..obtained from prefixMap
   * @returns String The formatted string
   */
  private String formatMissingOrder(String prefix, long orderNumber, long stringLength)
  {
    StringBuffer sb = new StringBuffer(prefix);
    String orderNumString = Long.toString(orderNumber);
    long neededZeros = stringLength - orderNumString.length() - prefix.length();

    for(int i = 0; i < neededZeros ; i++)
    {
      sb.append("0");
    }

    sb.append(orderNumString);

    return sb.toString();
  }

  /*
   * Send missing message 
   * 
   * @param String order Number
   * @returns String message id
   */
  private String sendMissingAlert(String orderNumber) throws Exception
  {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            

      String messageID = "";

      String missingMessage = configUtil.getProperty(PROPERTY_FILE,"ORDER_MISSING_MESSAGE");
      missingMessage = missingMessage + " [OrderNumber="+ orderNumber +"]";

      messageID = sendMessage(missingMessage);

      return messageID;
      
  }

    /*
     * Sets the counters in the prefix map.  This includes the start and
     * end order number counters.
     * 
     * @param Map orderMap Map or received orders
     * @param Map of order prefix data\counters
     */
    private void setPrefixCounters(Map orderMap, Map prefixMap)
    {

      logger.debug("setPrefixCounters");
      Set seqSet = orderMap.keySet();
      Iterator iter = seqSet.iterator();

      //iterate through order map
      while ( iter.hasNext() )
      {
          String orderNumber = (String) iter.next();
          String longOrderNumber = (String)orderMap.get(orderNumber);
          String orderPrefix = getOrderPrefix(orderNumber);
          String orderDigits = getOrderDigits(orderNumber,orderPrefix);
          long orderDigitValue = Long.parseLong(orderDigits);
                  
          //look for existing element in prefix map
          PrefixVO prefix = (PrefixVO)prefixMap.get(orderPrefix);

          //if element was not found and it to the prefix map
          if (prefix == null)
          {
            //add it to the new map
            PrefixVO newPrefix = new PrefixVO();
            newPrefix.setPrefix(orderPrefix);
            newPrefix.setStartOrderNumber(orderDigitValue);
            newPrefix.setEndOrderNumber(orderDigitValue);
            newPrefix.setMinStringLength(longOrderNumber.length());
            prefixMap.put(orderPrefix, newPrefix);          
          }
          else
          //else data about this prefix already exists...see if we need to update that info
          {
            //update the end point if this number is larger then what is in the prefix
            if(orderDigitValue > prefix.getEndOrderNumber())
            {
              prefix.setEndOrderNumber(orderDigitValue);
            }         

            //Since past data does not exist for this prefix, we need to make sure
            //we set the start counter to the lowest order number
            if(orderDigitValue < prefix.getStartOrderNumber())
            {
              prefix.setStartOrderNumber(orderDigitValue);
            }

            //Set min order number string length.  This is used to format missing orders
            //for display purposes.
            if((orderNumber.length() < prefix.getMinStringLength()) || prefix.getMinStringLength() == 0)
            {
              prefix.setMinStringLength(longOrderNumber.length());
            }
            
          }//end else element found

      }//end while iterator has more

    }


    /*
     * Obtains a data request object
     * @returns DataRequest
     */
    private DataRequest retrieveDataRequest() throws Exception
    {
        connection =  getConnection();

        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        return dataRequest;
    }

    /* 
     * Get database connection
     */
    private Connection getConnection() throws Exception
    {
        logger.debug("Connecting to data source " + SEQUENCE_DATASOURCE_NAME);
        Connection conn = DataSourceUtil.getInstance().getConnection(SEQUENCE_DATASOURCE_NAME);
        return conn;
    }


    /*
     * Loads the sequences from the database
     * @param DataRequest dataRequest
     * @returns CachedResultSet
     */
    private CachedResultSet loadSequences(DataRequest dataRequest) throws Exception
    {

        CachedResultSet returnSet = null;
        
        dataRequest.reset();
        dataRequest.setStatementID("load_order_sequences");

        Map outputs = (Map) DataAccessUtil.getInstance().execute(dataRequest);
      
        String status = (String) outputs.get(STATUS_PARAM);
        if(status.equals("N"))
        {
            String message = (String) outputs.get(MESSAGE_PARAM);
            throw new Exception(message);
        }
        else
        {
            returnSet = (CachedResultSet) outputs.get("OUT_SEQ_CUR");
        }

        return returnSet;
    }

  /*
   * This method takes in a order number (e.g. FTDA00123) and parses out the
   * prefix.  The prefix is determined by parsing the order number from left
   * to right.  All the alpha chars that read in (before a a numeric) are 
   * considered to be the order prefix.
   * 
   * @param String orderNumber
   * @returns String the order prefix
   */
  private String getOrderPrefix(String orderNumber)
  {
    StringBuffer buf = new StringBuffer(); 

    int i = 0;
    boolean digitFound = false;
    while(i < orderNumber.length() && !digitFound) 
    {
        //c = in.charAt(i); 
        char c = orderNumber.charAt(i);
        if(Character.isDigit(c))
        {
          digitFound = true;
        }
        else
        {
          buf.append(c);
        }

      //increment position
      i++;        
    }

    return buf.toString();
  }

  /*
   * This method parses out the numeric part of the the order number.
   * (e.g. FTDA00123).
   * @param String orderNumber
   * @param String prefix
   * @returns String Order Number (e.g. 123)
   */
  private String getOrderDigits(String orderNumber, String prefix)
  {
    String digits = null;

    digits = orderNumber.substring(prefix.length());

    //if not null convert remove preceding zeroes
    if(digits != null)
    {
      digits = new Long(digits).toString();
    }
    
    return digits;
  }

  /*
   *  This method deletes a mesage from System Messages and also
   *  deletes the order from the missing order map.
   *  
   *  @param String orderNumber
   *  @param Map missingMap
   */
  private void processDeleteMessage(String orderNumber, Map missingMap)
    throws Exception
  {

      logger.debug("Missing order was received. (" + orderNumber + ")");
  
      //get missing order message id
      MissingVO missingVO = (MissingVO)missingMap.remove(orderNumber);
   
      String messageID = missingVO.getMessageID();

      try
      {
          SystemMessenger.getInstance().deleteMessage(messageID,connection);
          SequenceDAO sdao = new SequenceDAO(connection);
          sdao.deleteMissingSequence(orderNumber);
      }
      catch(Exception e) {
          logger.error(e);
      }

  }

  /*
   * This method processes a list of duplicate order numbers.  Depending on the
   * number of duplicate orders the method will either send one message saying 
   * something like 'There were a lot of duplicate orders..' or it will send one message
   * for each duplicate order.
   * 
   * @param List List of duplicate orders.
   */
  private void processDuplicateOrders(List duplicateOrders) throws Exception
  {

      if(duplicateOrders.size() > duplicateOrderMaxMessage)
      {
        //send threshold message
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        String message = configUtil.getProperty(PROPERTY_FILE,"DUPLICATE_ORDER_THRESHOLD_MESSAGE");        

        //An abbreviated list order numbers is displayed with this message.  The
        //number of orders to display is based on the configuration file.
        String orderList = "";
        for(int i=0; i<numberToDisplayWithThreshold;i++)
        {
          if(orderList.length() > 0)
          {
            orderList = orderList + ",";
          }
          orderList = orderList + (String)duplicateOrders.get(i);          
        }
        orderList = orderList + "...";

        sendMessage(duplicateOrders.size() + " " + message + "[" + orderList + "]");
        logger.debug("processDuplicateOrders: " + orderList);

      }
      else
      //else, send 1 mesasge for each duplicate order
      {
        for(int i = 0; i < duplicateOrders.size(); i++)
        {
          String order = (String)duplicateOrders.get(i);
          String messageID = sendDuplicateAlert(order);
          logger.debug("processDuplicateOrders: " + messageID);
        }
      }
     }

  /*
   * This method processes a list of missing order numbers.  Depending on the
   * number of missing orders the method will either send one message saying 
   * something like 'There were a lot of missing orders..' or it will send one message
   * for each missing order.
   * 
   * @param List List of duplicate orders.
   */
  private void processMissingOrders(List missingOrders, Map missingMap) throws Exception
  {

      if(missingOrders.size() > missingOrderMaxMessage)
      {
        //send threshold message
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        String message = configUtil.getProperty(PROPERTY_FILE,"MISSING_ORDER_THRESHOLD_MESSAGE");        

        //An abbreviated list order numbers is displayed with this message.  The
        //number of orders to display is based on the configuration file.
        String orderList = "";
        for(int i=0; i<numberToDisplayWithThreshold;i++)
        {
          if(orderList.length() > 0)
          {
            orderList = orderList + ",";
          }
          orderList = orderList + (String)missingOrders.get(i);          
        }
        orderList = orderList + "...";

        sendMessage(missingOrders.size() + " " + message + "[" + orderList + "]");
        logger.debug("processMissingOrders: " + orderList);

      }
      else
      //else send 1 message for each missing order
      {
        for(int i = 0; i < missingOrders.size(); i++)
        {

            String orderNumber = (String)missingOrders.get(i);

            //grab order format from the map
            MissingVO missingVO = (MissingVO)missingMap.get(orderNumber);
            String formattedOrderNumber = missingVO.getOrderNumber();

            String messageID = sendMissingAlert(formattedOrderNumber);   

            //update missing Map with mesage ID
            missingVO.setMessageID(messageID);
            missingMap.put(orderNumber,missingVO);
            logger.debug("processMissingOrders: " + orderNumber + " " + messageID);
  
            SequenceDAO sdao = new SequenceDAO(connection);
            sdao.insertMissingSequence(formattedOrderNumber, messageID);
        }
      }
    
  }

  /*
   * This method sends the duplicate system message.
   * 
   * @param String orderNumber
   * @returns String mesage id
   */
  private String sendDuplicateAlert(String orderNumber) throws Exception
  {
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            

      String missingMessage = configUtil.getProperty(PROPERTY_FILE,"ORDER_DUPLICATE_MESSAGE");
      missingMessage = missingMessage + " [OrderNumber="+ orderNumber +"]";

      return sendMessage(missingMessage);
      
  }

  /*
   * This method sends a message to the System Messenger.
   * 
   * @param String message
   * @returns String message id
   */
  public String sendMessage(String message) throws Exception
  {
      String messageID = "";

      //build system vo
      SystemMessengerVO sysMessage = new SystemMessengerVO();
      sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
      sysMessage.setSource(messageSource);
      sysMessage.setType("ERROR");
      sysMessage.setMessage(message);
      Connection conn = null;
      try
      {
          logger.debug("Sending Message:" + message);
          conn = getConnection();
          messageID = SystemMessenger.getInstance().send(sysMessage,conn);
      }
      catch(Throwable t)
      {
          //LOG Sending system message failed
          logger.error("Sending system message failed. Message=" + message);
          logger.error(t);
      }
      finally {
          conn.close();
      }

      return messageID;  
  }

  /*
   * This method updates the last sequence number values in the prefixmap.
   * It goes through the whole maps and sets the last sequence number
   * equal to the last order number that was sent.
   * 
   * @param Map prefixMap   * 
   */
  private void updateLastSequence(Map prefixMap, Map receivedOrderMap)
  {
      try {
          Set receivedOrderSet = receivedOrderMap.keySet();
          Iterator iter = receivedOrderSet.iterator();            
          while(iter.hasNext())
          {
              PrefixVO prefix = (PrefixVO)prefixMap.get((String)iter.next());
              logger.debug("prefix: " + prefix.getPrefix());
              logger.debug("start: " + prefix.getStartOrderNumber());
              logger.debug("end: " + prefix.getEndOrderNumber());

              SequenceDAO sdao = new SequenceDAO(connection);
              sdao.updateExternalOrderPrefix(prefix);
          }
      }
      catch(Exception e)
      {
          logger.error(e);
      }
  }


} 