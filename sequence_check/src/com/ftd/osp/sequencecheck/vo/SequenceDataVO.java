package com.ftd.osp.sequencecheck.vo;

import java.util.Map;

/**
 * This VO contains data which needs to be saved to the file system each time
 * this application is ran.  It contains information about the missing orders and
 * about the order number counters for each order prefix.
 * 
 * @author Ed Mueller
 */
public class SequenceDataVO
{
  //Map of MissingVOs
  private Map MissingOrders;

  //Map of PrefixVOs
  private Map Prefixs;

  public SequenceDataVO()
  {

  }

  public Map getMissingOrders()
  {
    return MissingOrders;
  }

  public void setMissingOrders(Map newMissingOrders)
  {
    MissingOrders = newMissingOrders;
  }

  public Map getPrefixs()
  {
    return Prefixs;
  }

  public void setPrefixs(Map newPrefixs)
  {
    Prefixs = newPrefixs;
  }

}