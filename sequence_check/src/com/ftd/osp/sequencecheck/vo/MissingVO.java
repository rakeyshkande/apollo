package com.ftd.osp.sequencecheck.vo;

import java.util.Date;

/**
 * This VO represents a missing order.  Thjis object is persisted to the file
 * system.
 * 
 * @author Ed Mueller
 */
public class MissingVO
{
  //order number of missing order
  private String OrderNumber;

  //system message id or missing message that was inserted into System Message table
  private String MessageID;

  //Date this objectg was created
  private Date Created;

  public MissingVO()
  {
  }

  public String getOrderNumber()
  {
    return OrderNumber;
  }

  public void setOrderNumber(String newOrderNumber)
  {
    OrderNumber = newOrderNumber;
  }

  public String getMessageID()
  {
    return MessageID;
  }

  public void setMessageID(String newMessageID)
  {
    MessageID = newMessageID;
  }

  public Date getCreated()
  {
    return Created;
  }

  public void setCreated(Date newCreated)
  {
    Created = newCreated;
  }
}