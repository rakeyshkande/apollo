package com.ftd.osp.sequencecheck.vo;

import java.util.Date;

/**
 * This VO contains information about an order prefix.  This object
 * is persisted to the file system.
 *
 * @author Ed Mueller
 */
public class PrefixVO
{
  private long EndOrderNumber;
  private long StartOrderNumber;
  private long MinStringLength;
  private String Prefix;
  private Date createdOn;

  public long getEndOrderNumber()
  {
    return EndOrderNumber;
  }

  public void setEndOrderNumber(long newEndOrderNumber)
  {
    EndOrderNumber = newEndOrderNumber;
  }

  public long getStartOrderNumber()
  {
    return StartOrderNumber;
  }

  public void setStartOrderNumber(long newStartOrderNumber)
  {
    StartOrderNumber = newStartOrderNumber;
  }

  public long getMinStringLength()
  {
    return MinStringLength;
  }

  public void setMinStringLength(long newMinStringLength)
  {
    MinStringLength = newMinStringLength;
  }

  public String getPrefix()
  {
    return Prefix;
  }

  public void setPrefix(String newPrefix)
  {
    Prefix = newPrefix;
  }

  public Date getCreatedOn() {
      return createdOn;
  }
  
  public void setCreatedOn(Date newCreatedOn) {
      createdOn = newCreatedOn;
  }

}