function doMainMenuAction(){
  var url = '';
  url = "MainMenuAction.do" + getSecurityParams(true);
  performAction(url);
}

function performAction(url){
  document.forms[0].action = url;
  document.forms[0].submit();
}

function getSecurityParams(areOnlyParams){
   return   ((areOnlyParams) ? "?" : "&") +
            "securitytoken=" + document.getElementById("securitytoken").value +
            "&context=" + document.getElementById("context").value +
            "&adminAction=" + document.getElementById("adminAction").value;
}