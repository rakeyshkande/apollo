<%@ page contentType="text/html;charset=windows-1252" errorPage="/error.jsp"%>
<%@ page import="com.ftd.scheduler.common.SchedulerConstants" %>
<%
  String securitytoken = request.getParameter("securitytoken");
  if( securitytoken==null ) {
    securitytoken="null";
  }
  String applicationcontext = request.getParameter("applicationcontext");
  if( applicationcontext==null ) {
    applicationcontext="null";
  }
  String context = request.getParameter("context");
  if( context==null ) {
    context="null";
  }
%> 

<script type="text/javascript" src="js/clock.js"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/rico.js"></script>
<script type="text/javascript" src="js/ftddom.js"></script>
<script type="text/javascript" src="js/ftdajax.js"></script>
<script type="text/javascript" src="js/ftdxml.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/selectbox.js"></script>
<script type="text/javascript" src="js/OptionTransfer.js"></script>
<script type="text/javascript" src="js/table.js"></script>

<script type="text/javascript" language="javascript">
    var accordion;
    var setupAccordion;
    var onloads = new Array();
    onloads.push( accord ); function accord() {accordion = new Rico.Accordion( 'accordionDiv', {panelHeight:550, onHideTab:changeAccordionTab} ); }
    onloads.push( accord2 ); function accord2() {setupAccordion = new Rico.Accordion( 'setupDiv', {panelHeight:425} ); }
    var securitytoken = "<%=securitytoken%>";
    var applicationcontext = "<%=applicationcontext%>";
    var context = "<%=context%>";
    var payloadChanged = false;
    var scheduleChanged = false;
    var selectedJobSuffix = "<%=SchedulerConstants.NO_SELECTED_JOB%>";
    var selectedTriggerSuffix = "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>";
    var xmlSqlStatements;
    var xmlEmailAddresses;
    var borderFocusColor = "GoldenRod";
    var labelFocusColor = "DarkGoldenRod";
    var selectedCaptionColor = 'PaleGoldenRod';
    var selectedModuleValue = '';
    var selectedSetupFunctionModuleValue = '';
    var selectedPipelineValue = '';
    var editMode = false;
    var changingToEditMode = false;
    var editJobName = "";
    var editGroup = "";
    var editTabIdx = 2;
    var definedTabIdx = 1;
    var xmlModules;
    var newFunctionMode = true;

    var newEmailListOpt = new OptionTransfer("newAvailableRecipients","newEmailRecipients");
    newEmailListOpt.setAutoSort(true);
    newEmailListOpt.saveRemovedLeftOptions("newEmailRemovedLeft");
    newEmailListOpt.saveRemovedRightOptions("newEmailRemovedRight");
    newEmailListOpt.saveAddedLeftOptions("newEmailAddedLeft");
    newEmailListOpt.saveAddedRightOptions("newEmailAddedRight");
    newEmailListOpt.saveNewLeftOptions("newEmailNewLeft");
    newEmailListOpt.saveNewRightOptions("newEmailNewRight");

    function init()
    {
        newEmailListOpt.init(document.forms[0]);
        document.getElementById("refreshScheduleButton").disabled = true;
        document.getElementById("runButton").disabled = true;
        document.getElementById("scheduleButton").disabled = true;
        document.getElementById("removeButton").disabled = true;

        for ( var i = 0 ; i < onloads.length ; i++ ) {
          onloads[i]();
        }

        document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>').checked = true;
        newFunctionMode = true;
        document.getElementById('setupFunctionNewButton').disabled = true;

        getModules();
        getScheduledJobs();
        getDefinedJobs();
        getEmailAddresses(getEmailAddressesCallback);
        getGetSchedulerStatus();
        getRunningJobs();
        onSelectSchedule();
        onEmailCheckbox();
        getSQLStatements();
        getAllPipelineDetails();
        getApolloJobs();
        getJMSQueues();
        getJMSAMQQueues();
    }

    function unload() {
    }

    function getRunningJobs() {
        document.getElementById("runRefreshButton").disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getRunningJobsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_RUNNING_JOBS%>");
        newRequest.send();
    }

    function getRunningJobsCallback(data) {
        //alert(data);
        var xmlDoc = parseXmlString(data);
        var jobs = xmlDoc.getElementsByTagName("<%=SchedulerConstants.DEFINED_JOB%>");

        var tbody = document.getElementById('runningJobsTbody');
        FTD_DOM.removeAllTableRows(tbody);

        for( var i = 0; i < jobs.length; i++)
        {
            var idSuffix = i;
            var jobName   = jobs[i].getAttribute('<%=SchedulerConstants.RUNNING_JOB_NAME%>');
            var module    = jobs[i].getAttribute('<%=SchedulerConstants.RUNNING_MODULE%>');
            var jobParms  = jobs[i].getAttribute('<%=SchedulerConstants.RUNNING_JOB_PARMS%>');
            var startTime = jobs[i].getAttribute('<%=SchedulerConstants.RUNNING_START_TIME%>');
            var duration  = jobs[i].getAttribute('<%=SchedulerConstants.RUNNING_DURATION%>');

            var row = tbody.insertRow(tbody.rows.length);

            if( i % 2 != 0 ) {
                row.className = "alternate";
            }

            var cell = row.insertCell(0);
            cell.innerHTML = jobName;

            cell = row.insertCell(1);
            cell.innerHTML = module;

            cell = row.insertCell(2);
            cell.innerHTML = jobParms;

            cell = row.insertCell(3);
            cell.innerHTML = startTime;

            cell = row.insertCell(4);
            cell.innerHTML = duration;
        }

        document.getElementById("runRefreshButton").disabled = false;
    }

    function getGetSchedulerStatus() {
        document.getElementById("runRefreshButton").disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getGetSchedulerStatusCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_SCHEDULER_STATUS%>");
        newRequest.send();
    }

    function getGetSchedulerStatusCallback(data) {
        document.getElementById("schedulerStatsId").value=data;
        document.getElementById("runRefreshButton").disabled = false;
    }

    function getScheduledJobs() {
        disableScheduledPanel(true);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getScheduledJobsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_SCHEDULED_JOBS%>");
        newRequest.send();
    }

    function getScheduledJobsCallback(data) {
        var xmlDoc = parseXmlString(data);
        var jobs = xmlDoc.getElementsByTagName("<%=SchedulerConstants.SCHEDULED_JOB%>");

        var tbody = document.getElementById('scheduledJobsTbody');
        FTD_DOM.removeAllTableRows(tbody);

        for( var i = 0; i < jobs.length; i++)
        {
            var idSuffix = +i;
            var jobName      = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_JOB_NAME%>');
            var groupName    = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_GROUP%>');
            var module       = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_MODULE%>');
            var payload      = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_PAYLOAD%>');
            var pipeline     = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_PIPELINE%>');
            var triggerName  = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_TRIGGER%>');
            var schedule     = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_SCHEDULE%>');
            var jobParms     = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_JOB_PARMS%>');
            var previousRun  = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_PREVIOUS_RUN%>');
            var nextRun      = jobs[i].getAttribute('<%=SchedulerConstants.SCHEDULED_NEXT_RUN%>');

            var row = tbody.insertRow(tbody.rows.length);

            if( i % 2 != 0 ) {
                row.className = "alternate";
            }

            var cell = row.insertCell(0);
            cell.align = "center";
            var input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SCHEDULED_RADIO%>'+idSuffix,
                type:'radio',
                name:'<%=SchedulerConstants.SCHEDULED_RADIO%>',
                onClick:'scheduledRadioSelected('+i+');'} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SCHEDULED_GROUP%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.SCHEDULED_GROUP%>',
                value:groupName} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SCHEDULED_MODULE%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.SCHEDULED_MODULE%>',
                value:module} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SCHEDULED_TRIGGER%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.SCHEDULED_TRIGGER%>',
                value:triggerName} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(1);
            cell.innerHTML = jobName;

            cell = row.insertCell(2);
            cell.innerHTML = module;

            cell = row.insertCell(3);
            cell.innerHTML = jobParms;

            cell = row.insertCell(4);
            cell.innerHTML = schedule;

            cell = row.insertCell(5);
            cell.innerHTML = previousRun;

            cell = row.insertCell(6);
            cell.innerHTML = nextRun;

            if( nextRun=="<%=SchedulerConstants.DISPLAY_JOB_PAUSED%>" ) {
                cell.style.color = "Red";
            }
        }

        selectedTriggerSuffix = "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>";
        disableScheduledPanel(false);
    }

    function getDefinedJobs() {
        document.getElementById("refreshScheduleButton").disabled = true;
        document.getElementById("runButton").disabled = true;
        document.getElementById("scheduleButton").disabled = true;
        document.getElementById("removeButton").disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getDefinedJobsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_DEFINED_JOBS%>");
        newRequest.send();
    }

    function getDefinedJobsCallback(data) {
        //alert(data);
        var xmlDoc = parseXmlString(data);
        var jobs = xmlDoc.getElementsByTagName("<%=SchedulerConstants.DEFINED_JOB%>");

        var tbody = document.getElementById('definedJobsTbody');
        FTD_DOM.removeAllTableRows(tbody);

        for( var i = 0; i < jobs.length; i++)
        {
            var idSuffix = i;
            var jobName   = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_JOB_NAME%>');
            var groupName = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_GROUP%>');
            var module    = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_MODULE%>');
            var payload   = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_PAYLOAD%>');
            var pipeline  = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_PIPELINE%>');
            var jobClass  = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_JOB_CLASS%>');
            var schedule  = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_SCHEDULE%>');
            var queueName = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_JMS_QUEUE_NAME%>');
            var jobParms  = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_JOB_PARMS%>');
            var classCode = jobs[i].getAttribute('<%=SchedulerConstants.DEFINED_CLASS_CODE%>');

            var row = tbody.insertRow(tbody.rows.length);

            if( i % 2 != 0 ) {
                row.className = "alternate";
            }

            var cell = row.insertCell(0);
            cell.align = "center";
            var input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_RADIO%>'+idSuffix,
                type:'radio',
                name:'<%=SchedulerConstants.DEFINED_RADIO%>',
                onClick:'definedRadioSelected('+i+');'} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_JOB_NAME%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_JOB_NAME%>',
                value:jobName} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_GROUP%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_GROUP%>',
                value:groupName} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_MODULE%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_MODULE%>',
                value:module} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_PAYLOAD%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_PAYLOAD%>',
                value:payload} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_SCHEDULE%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_SCHEDULE%>',
                value:schedule} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.DEFINED_JMS_QUEUE_NAME%>'+idSuffix,
                type:'hidden',
                name:'<%=SchedulerConstants.DEFINED_JMS_QUEUE_NAME%>',
                value:queueName} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(1);
            var span = new FTD_DOM.SpanElement( {
                title:'Click here to edit',
                onclick:'getJobDetailForEdit('+i+');',
                onmouseover:"this.style.cursor = 'pointer';this.style.color = 'darkblue';",
                onmouseout:"this.style.color = 'blue';",
                idx:'<%=SchedulerConstants.PARAMETER_INDEX%>'
                } ).createNode();
            span.className = 'linktext';
            span.innerHTML = jobName;
            cell.appendChild(span);

            cell = row.insertCell(2);
            cell.innerHTML = module;

            cell = row.insertCell(3);
            cell.innerHTML = jobParms;

            cell = row.insertCell(4);
            cell.innerHTML = classCode;
        }

        payloadChanged = false;
        scheduleChanged = false;
        selectedJobSuffix = "<%=SchedulerConstants.NO_SELECTED_JOB%>";
        document.getElementById("refreshScheduleButton").disabled = false;
        document.getElementById("runButton").disabled = false;
        document.getElementById("scheduleButton").disabled = false;
        document.getElementById("removeButton").disabled = false;
    }

    function removeTrigger() {
        if( selectedTriggerSuffix == "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>" ) {
            alert("You must select a scheduled job before you can remove it.");
            return;
        }

        var group = document.getElementById("<%=SchedulerConstants.SCHEDULED_GROUP%>"+selectedTriggerSuffix).value;
        var trigger = document.getElementById("<%=SchedulerConstants.SCHEDULED_TRIGGER%>"+selectedTriggerSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,removeTriggerCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_REMOVE_TRIGGER%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",group);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_TRIGGER_NAME%>",trigger);
        newRequest.send();
        disableScheduledPanel(true);

      return false;
    }

    function removeTriggerCallback(data) {
        if( data == "<%=SchedulerConstants.RESULTS_TRIGGER_REMOVAL_SUCCESS%>" ) {
            getScheduledJobs();
        } else {
            disableScheduledPanel(false);
        }
        alert(data);
    }

    function removeJob() {
        if( selectedJobSuffix == "<%=SchedulerConstants.NO_SELECTED_JOB%>" ) {
            alert("You must select a defined job before you can remove it.");
            return;
        }

        var jobName = document.getElementById("<%=SchedulerConstants.DEFINED_JOB_NAME%>"+selectedJobSuffix).value;
      var answer = confirm("Are you sure you want to delete "+jobName+"?");

  if (answer){
            var group = document.getElementById("<%=SchedulerConstants.DEFINED_GROUP%>"+selectedJobSuffix).value;

            //XMLHttpRequest
            var newRequest =
                new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,removeJobCallback,false);
            newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_REMOVE_JOB%>");
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",group);
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_JOB_NAME%>",jobName);
            newRequest.send();

            document.getElementById("refreshScheduleButton").disabled = false;
            document.getElementById("runButton").disabled = true;
            document.getElementById("scheduleButton").disabled = true;
            document.getElementById("removeButton").disabled = true;
  }

      return false;
    }

    function removeJobCallback(data) {
        if( data == "<%=SchedulerConstants.RESULTS_JOB_REMOVAL_SUCCESS%>" ) {
            getDefinedJobs();
            getScheduledJobs();
        } else {
            document.getElementById("refreshScheduleButton").disabled = false;
            document.getElementById("runButton").disabled = false;
            document.getElementById("scheduleButton").disabled = false;
            document.getElementById("removeButton").disabled = false;
        }
        alert(data);
    }

    function definedRadioSelected(idx) {
        selectedJobSuffix = idx;

        document.getElementById('newSchedule').value =
            document.getElementById("<%=SchedulerConstants.DEFINED_SCHEDULE%>"+selectedJobSuffix).value;
    }

    function scheduledRadioSelected(idx) {
        selectedTriggerSuffix = idx;
    }

    function scheduleJob() {
        if( selectedJobSuffix == "<%=SchedulerConstants.NO_SELECTED_JOB%>" ) {
            alert("You must select a defined job before you can schedule it.");
            return;
        }
        document.getElementById("refreshScheduleButton").disabled = true;
        document.getElementById("runButton").disabled = true;
        document.getElementById("scheduleButton").disabled = true;
        document.getElementById("removeButton").disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,scheduleJobCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_SCHEDULE_JOB%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",document.getElementById("<%=SchedulerConstants.DEFINED_GROUP%>"+selectedJobSuffix).value);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_JOB_NAME%>",document.getElementById("<%=SchedulerConstants.DEFINED_JOB_NAME%>"+selectedJobSuffix).value);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_SCHEDULE%>",document.getElementById("newSchedule").value);
        newRequest.send();
    }

    function scheduleJobCallback(data) {
        alert(data);
        document.getElementById("refreshScheduleButton").disabled = false;
        document.getElementById("runButton").disabled = false;
        document.getElementById("scheduleButton").disabled = false;
        document.getElementById("removeButton").disabled = false;

        if( data == "<%=SchedulerConstants.RESULTS_JOB_SCHEDULE_SUCCESS%>") {
            getScheduledJobs();
        }
    }

    function runOnce() {
        if( selectedJobSuffix == "<%=SchedulerConstants.NO_SELECTED_JOB%>" ) {
            alert("You must select a defined job before you can run it.");
            return;
        }
        document.getElementById("refreshScheduleButton").disabled = true;
        document.getElementById("runButton").disabled = true;
        document.getElementById("scheduleButton").disabled = true;
        document.getElementById("removeButton").disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,runOnceCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_RUN_ONCE%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_JOB_NAME%>",document.getElementById("<%=SchedulerConstants.DEFINED_JOB_NAME%>"+selectedJobSuffix).value);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",document.getElementById("<%=SchedulerConstants.DEFINED_GROUP%>"+selectedJobSuffix).value);
        newRequest.send();
    }

    function runOnceCallback(data) {
        alert(data);
        document.getElementById("refreshScheduleButton").disabled = false;
        document.getElementById("runButton").disabled = false;
        document.getElementById("scheduleButton").disabled = false;
        document.getElementById("removeButton").disabled = false;
    }

    function getModules() {
        if( editMode == true ) {
            document.getElementById("submitJobButton").disabled = false;
            return;
        }

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getModulesCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_MODULES%>");
        newRequest.send();
    }

    function getModulesCallback(data) {
        if( editMode == true ) {
            document.getElementById("submitJobButton").disabled = false;
            return;
        }

        var xmlDoc = parseXmlString(data);
        xmlModules = data;

        var moduleCombo = document.getElementById("module_combo");
        var modules = xmlDoc.getElementsByTagName("<%=SchedulerConstants.PARAMETER_MODULE%>");

        //clear out the module combo
        for( var i = moduleCombo.length-1; i>=0; i-- ) {
            moduleCombo.remove(i);
        }

        //clear out the module setup table (tbody only)
        var setupTable = document.getElementById("setupModuleTbody");
        var rows = setupTable.rows;
        for( var i = rows.length-1; i>=0; i-- ) {
            setupTable.deleteRow(i);
        }

        //iterate through vendor nodes to populate combo
        var gotFirst = false;
        for( var i = 0; i < modules.length; i++)
        {
            var moduleId = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_MODULE_ID%>')[i].firstChild.nodeValue;
            var desc = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_DESCRIPTION%>')[i].firstChild.nodeValue;
            var activeFlag = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_ACTIVE_FLAG%>')[i].firstChild.nodeValue;

            //Add to the module setup table
            var row = setupTable.insertRow(setupTable.rows.length);

            if( i % 2 != 0 ) {
                row.className = "alternate";
            }

            var cell = row.insertCell(0);
            var input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>'+i,
                type:'checkbox',
                name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>',
                <%=SchedulerConstants.PARAMETER_INDEX%>:i} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(1);
            cell.innerHTML = moduleId;
            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ID%>'+i,
                type:'hidden',
                name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ID%>',
                value:moduleId} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(2);
            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_DESCRIPTION%>'+i,
                type:'text',
                name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_DESCRIPTION%>',
                size:'40',
                maxsize:'256',
                value:desc,
                onchange:'onSetupModuleChange('+i+');'} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(3);
            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX%>'+i,
                type:'checkbox',
                name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX%>',
                onclick:'onSetupModuleChange('+i+');'} ).createNode();
            cell.appendChild(input);

            //Add to the module combo
            if( activeFlag=="Y" ) {
                input.checked = true;
                //Add to the module combo
                moduleCombo.options[i] = new Option(desc, moduleId);

                if( gotFirst==false ) {
                    moduleCombo.options[i].selected=true;
                    selectedModuleValue = moduleId;
                    gotFirst = true;
                }
            }
        }

        populateSetupFunctionCombo(true);
        getPipelinesForModule();
    }

    function populateSetupFunctionCombo(populateAll) {
        var setupFunctionModuleCombo = document.getElementById("setupFunctionModuleCombo");
        var xmlDoc = parseXmlString(xmlModules);
        var modules = xmlDoc.getElementsByTagName("<%=SchedulerConstants.PARAMETER_MODULE%>");

        //clear out the setup function module combo
        for( var i = setupFunctionModuleCombo.length-1; i>=0; i-- ) {
            setupFunctionModuleCombo.remove(i);
        }

        //iterate through vendor nodes to populate combo
        var gotFirst = false;
        for( var i = 0; i < modules.length; i++)
        {
            var moduleId = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_MODULE_ID%>')[i].firstChild.nodeValue;
            var desc = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_DESCRIPTION%>')[i].firstChild.nodeValue;
            var activeFlag = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_ACTIVE_FLAG%>')[i].firstChild.nodeValue;

            //Add to the module combo
            if( activeFlag=="Y" || populateAll==true ) {
                //Add to the module combo
                setupFunctionModuleCombo.options[i] = new Option(desc, moduleId);

                if( gotFirst==false ) {
                    setupFunctionModuleCombo.options[i].selected=true;
                    selectedSetupFunctionModuleValue = moduleId;
                    gotFirst = true;
                }
            }
        }
    }

    function onSetupNewModule() {
        var setupTable = document.getElementById("setupModuleTbody");
        var i = setupTable.rows.length;
        var row = setupTable.insertRow(i);

        var cell = row.insertCell(0);
        var input = new FTD_DOM.InputElement( {
            id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>'+i,
            type:'checkbox',
            checked:'true',
            name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>',
            <%=SchedulerConstants.PARAMETER_INDEX%>:i} ).createNode();
        cell.appendChild(input);

        cell = row.insertCell(1);
        input = new FTD_DOM.InputElement( {
            id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ID%>'+i,
            type:'text',
            name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ID%>',
            size:'20',
            maxsize:'32'} ).createNode();
        cell.appendChild(input);

        cell = row.insertCell(2);
        input = new FTD_DOM.InputElement( {
            id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_DESCRIPTION%>'+i,
            type:'text',
            name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_DESCRIPTION%>',
            size:'40',
            maxsize:'256'} ).createNode();
        cell.appendChild(input);

        cell = row.insertCell(3);
        input = new FTD_DOM.InputElement( {
            id:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX%>'+i,
            type:'checkbox',
            name:'<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX%>',
            onclick:'onSetupModuleChange('+i+');',
            checked:'true'} ).createNode();
        cell.appendChild(input);
    }

    function onSetupSaveModule() {
        var saveBoxes = document.getElementsByName('<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>');

        var xmlDoc = createXmlDocument("<%=SchedulerConstants.PARAMETER_MODULES%>");
        var xmlRoot = xmlDoc.documentElement;

        for( var i = 0; i<saveBoxes.length; i++) {
            if( saveBoxes[i].checked == true ) {
                var record = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_MODULE%>");
                var idx = record.<%=SchedulerConstants.PARAMETER_INDEX%>;
                record.setAttribute("<%=SchedulerConstants.PARAMETER_MODULE_ID%>",document.getElementById("<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ID%>"+i).value);
                record.setAttribute("<%=SchedulerConstants.PARAMETER_DESCRIPTION%>",document.getElementById("<%=SchedulerConstants.PARAMETER_SETUP_MODULE_DESCRIPTION%>"+i).value);
                record.setAttribute("<%=SchedulerConstants.PARAMETER_ACTIVE_FLAG%>",document.getElementById("<%=SchedulerConstants.PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX%>"+i).checked==true?'Y':'N');
                xmlRoot.appendChild(record);
            }
        }

        disableSetupControls(true);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,onSetupSaveModuleCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_UPDATE_MODULES%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_MODULES%>",getXMLNodeSerialization(xmlDoc));
        newRequest.send();
    }

    function onSetupSaveModuleCallback(data) {
        alert(data);
        disableSetupControls(false);
    }

    function disableSetupControls(disableSwitch) {
        document.getElementById('setupModuleSaveButton').disabled=disableSwitch;
        document.getElementById('setupModuleNewButton').disabled=disableSwitch;
    }

    function getPipelinesForModule() {
        //Don't get the pipelines if your in edit mode
        //It causes the combos to activate their "onchange" methods
        if( editMode == true ) {
            document.getElementById("submitJobButton").disabled = false;
            return;
        }

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getPipelinesCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_GROUP_PIPELINES%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",selectedModuleValue);
        newRequest.send();
    }

    function getPipelinesCallback(data) {
         if( editMode==true ) {
            document.getElementById("submitJobButton").disabled = false;
            return;
        }

        var xmlDoc = parseXmlString(data);

        var pipelineCombo = document.getElementById("pipelineCombo");
        var pipelines = xmlDoc.getElementsByTagName("<%=SchedulerConstants.PARAMETER_PIPELINE%>");

        //iterate through vendor nodes and set to null
        for( var i = pipelineCombo.length-1; i>=0; i-- ) {
            pipelineCombo.options[i] = null;
        }

        //iterate through vendor nodes to populate group select
        for( var i = 0; i < pipelines.length; i++)
        {
            var pipelineId = pipelines.item(i).getElementsByTagName('<%=SchedulerConstants.PARAMETER_PIPELINE_ID%>')[0].childNodes[0].nodeValue;
            var desc = pipelines.item(i).getElementsByTagName('<%=SchedulerConstants.PARAMETER_DESCRIPTION%>')[0].childNodes[0].nodeValue;

            pipelineCombo.options[i] = new Option(desc, pipelineId);

            if( i == 0 ) {
                pipelineCombo.options[0].selected=true;
                selectedPipelineValue = pipelineId;
            }
        }

        getPipelineDetail();
    }

    function getPipelineDetail() {
         if( editMode==true ) {
            document.getElementById("submitJobButton").disabled = false;
            return;
        }

        //XMLHttpRequest
        var newRequest =
          new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getPipelineDetailCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_PIPELINE_DETAIL%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",selectedModuleValue);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_PIPELINE%>",selectedPipelineValue);
        newRequest.send();
        document.getElementById("submitJobButton").disabled = true;
    }

    function getPipelineDetailCallback(data) {
        document.getElementById("submitJobButton").disabled = false;
         if( editMode==true ) {
            return;
        }

        var xmlDoc = parseXmlString(data);

        try {
            var tmpStr = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE%>')[0].childNodes[0].nodeValue;
            if( tmpStr == undefined || tmpStr.length==0 ) {
                document.getElementById('newCronSchedule').value = '';
                document.getElementById('newIntervalSchedule').value = '';
                document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>').checked = false;
                document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>').checked = false;
                document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>').checked = true;
                onSelectScheduleOnce();
            } else {
                var spaceIndex = tmpStr.indexOf(' ');
                if( spaceIndex > 0) {
                    document.getElementById('newCronSchedule').value = tmpStr;
                    document.getElementById('newIntervalSchedule').value = '';
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>').checked = false;
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>').checked = false;
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>').checked = true;
                } else {
                    document.getElementById('newCronSchedule').value = '';
                    document.getElementById('newIntervalSchedule').value = tmpStr;
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>').checked = false;
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>').checked = false;
                    document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>').checked = true;
                }
                onSelectSchedule();
            }
        } catch (err) {
            document.getElementById('newCronSchedule').value = '';
            document.getElementById('newIntervalSchedule').value = '';
            document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>').checked = false;
            document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>').checked = false;
            document.getElementById('<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>').checked = true;
            onSelectScheduleOnce();
        }

        try {
            document.getElementById('jobNotes').innerHTML = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_TOOL_TIP%>')[0].childNodes[0].nodeValue;
        } catch (err) {
            document.getElementById('jobNotes').innerHTML = '';
        }

        try {
            document.getElementById('queueName').value = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_JMS_QUEUE_NAME%>')[0].childNodes[0].nodeValue;
        } catch(err) {
            document.getElementById('queueName').value = '';
        }

        var forceDefaultPayload;
        try {
            forceDefaultPayload = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD%>')[0].childNodes[0].nodeValue;
        } catch (err) {
            forceDefaultPayload = 'N';
        }

        var newPayloadValue;
        try {
            newPayloadValue = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD%>')[0].childNodes[0].nodeValue;
        } catch(err) {
            newPayloadValue = '';
        }
        if( 'Y' == forceDefaultPayload ) {
            document.getElementById('newPayloadId').value = newPayloadValue;
            document.getElementById('newPayloadId').disabled = true;
            document.getElementById('payloadSingleId').disabled=true;
        } else {
            document.getElementById('newPayloadId').value = newPayloadValue;
            document.getElementById('newPayloadId').disabled = false;
            document.getElementById('payloadSingleId').disabled=false;
        }

        try {
            var classCode = xmlDoc.getElementsByTagName('<%=SchedulerConstants.PARAMETER_CLASS_CODE%>')[0].childNodes[0].nodeValue;
            setClassCode(classCode);
        } catch (err) {
            alert(err+'/r/n'+data);
        }
    }

    function getAllPipelineDetails() {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getAllPipelineDetailsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_ALL_PIPELINE_DETAILS%>");
        newRequest.send();
    }

    function getAllPipelineDetailsCallback(data) {
//        alert(data);

        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        var tBody = document.getElementById("setupFunctionTbody");

        FTD_DOM.removeAllTableRows(tBody);

        var pipelines = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_PIPELINE%>");

        for( var i = 0; i < pipelines.length; i++) {
            var pipelineId = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_PIPELINE_ID%>').nodeValue;
            var moduleId = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_MODULE_ID%>').nodeValue;
            var desc = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_DESCRIPTION%>').nodeValue;
            var activeFlag = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_ACTIVE_FLAG%>').nodeValue;

            var toolTip = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_TOOL_TIP%>').nodeValue;
            var defaultSchedule = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE%>').nodeValue;
            var defaultPayload = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD%>').nodeValue;
            var queueName = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_JMS_QUEUE_NAME%>').nodeValue;
            var classCode = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_CLASS_CODE%>').nodeValue;
            var forceDefaultPayload = pipelines[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD%>').nodeValue;

            var row = tBody.insertRow(tBody.rows.length);
            var colCount = 0;

            if( i % 2 != 0 ) {
                row.className = "alternate";
            }

            var cell = row.insertCell(colCount++);
            cell.align = "center";
            var input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SETUP_FUNCT_RADIO%>'+i,
                type:'radio',
                name:'<%=SchedulerConstants.SETUP_FUNCT_RADIO%>',
                onClick:'onSetupFunctionRadioSelected('+i+');'} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_TOOL_TIP%>'+i,
                value:toolTip} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_DEFAULT_SCHEDULE%>'+i,
                value:defaultSchedule} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_DEFAULT_PAYLOAD%>'+i,
                value:defaultPayload} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_QUEUE_NAME%>'+i,
                value:queueName} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_CLASS_CODE%>'+i,
                value:classCode} ).createNode();
            cell.appendChild(input);

            input = new FTD_DOM.InputElement( {
                type:'hidden',
                id:'<%=SchedulerConstants.SETUP_FUNCT_FORCE_DEFAULT_PAYLOAD%>'+i,
                value:forceDefaultPayload} ).createNode();
            cell.appendChild(input);

            cell = row.insertCell(colCount++);
            cell.id = "<%=SchedulerConstants.SETUP_FUNCT_PIPELINE_ID%>"+i;
            cell.innerHTML = pipelineId;

            cell = row.insertCell(colCount++);
            cell.id = "<%=SchedulerConstants.SETUP_FUNCT_MODULE_ID%>"+i;
            cell.innerHTML = moduleId;

            cell = row.insertCell(colCount++);
            cell.id = "<%=SchedulerConstants.SETUP_FUNCT_DESCRIPTION%>"+i;
            cell.innerHTML = desc;

            cell = row.insertCell(colCount++);
            input = new FTD_DOM.InputElement( {
                id:'<%=SchedulerConstants.SETUP_FUNCT_ACTIVE%>'+i,
                type:'checkbox',
                name:'<%=SchedulerConstants.SETUP_FUNCT_ACTIVE%>',
                disabled:'true',
                <%=SchedulerConstants.PARAMETER_INDEX%>:i} ).createNode();
            cell.appendChild(input);

            if( activeFlag=="Y" ) {
                input.checked = true;
            }
        }
    }

    function onSetupFunctionRadioSelected(idx) {
        FTD_DOM.selectOptionByValue(document.getElementById('setupFunctionModuleCombo'),document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_MODULE_ID%>'+idx).innerHTML);
        FTD_DOM.selectOptionByText(document.getElementById('setupFunctionJobCombo'),document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_CLASS_CODE%>'+idx).value);
        FTD_DOM.selectOptionByValue(document.getElementById('setupFunctionJMS'),document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_QUEUE_NAME%>'+idx).value);
        FTD_DOM.selectOptionByValue(document.getElementById('setupFunctionJMSAMQ'),document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_QUEUE_NAME%>'+idx).value);
        document.getElementById('setupFunctionFunction').value = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_PIPELINE_ID%>'+idx).innerHTML;
        document.getElementById('setupFunctionDescription').value = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_DESCRIPTION%>'+idx).innerHTML;
        document.getElementById('setupFunctionToolTip').value = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_TOOL_TIP%>'+idx).value;
        document.getElementById('setupFunctionSchedule').value = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_DEFAULT_SCHEDULE%>'+idx).value;
        document.getElementById('setupFunctionPayload').value = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_DEFAULT_PAYLOAD%>'+idx).value;
        document.getElementById('setupFunctionActive').checked = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_ACTIVE%>'+idx).checked;
        document.getElementById('setupFunctionForceDefault').checked = document.getElementById('<%=SchedulerConstants.SETUP_FUNCT_FORCE_DEFAULT_PAYLOAD%>'+idx).value=='Y'?true:false;

        document.getElementById('setupFunctionModuleCombo').disabled = true;
        document.getElementById('setupFunctionFunction').disabled = true;
        document.getElementById('setupFunctionNewButton').disabled = false;
        newFunctionMode = false;
        onSetupFunctionJobChange();
    }

    function onSetupFunctionJobChange() {
        var jobCombo = document.getElementById('setupFunctionJobCombo');
        var selectedJob = jobCombo[jobCombo.selectedIndex].text;

        if( selectedJob=='<%=SchedulerConstants.JMS_JOB_CLASS_CODE%>' ) {
            document.getElementById('setupFunctionJMSLabel').style.display='inline';
            document.getElementById('setupFunctionJMS').style.display='inline';
            document.getElementById('setupFunctionJMSAMQLabel').style.display='none';
            document.getElementById('setupFunctionJMSAMQ').style.display='none';
        } else if( selectedJob=='<%=SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE%>' ) {
            document.getElementById('setupFunctionJMSAMQLabel').style.display='inline';
            document.getElementById('setupFunctionJMSAMQ').style.display='inline';
            document.getElementById('setupFunctionJMSLabel').style.display='none';
            document.getElementById('setupFunctionJMS').style.display='none';
        } else {
            document.getElementById('setupFunctionJMSLabel').style.display='none';
            document.getElementById('setupFunctionJMS').style.display='none';
            document.getElementById('setupFunctionJMSAMQLabel').style.display='none';
            document.getElementById('setupFunctionJMSAMQ').style.display='none';
        }
    }

    function onSetupNewFunction() {
        var selectedButton = FTD_DOM.getSelectedRadioButton("<%=SchedulerConstants.SETUP_FUNCT_RADIO%>");

        if( selectedButton!="" ) {
            selectedButton.checked=false;
        }

        var newFunctionMode = true;
        clearSetupFunctionControls();
        document.getElementById('setupFunctionNewButton').disabled = true;
        document.getElementById('setupFunctionModuleCombo').disabled = false;
        document.getElementById('setupFunctionFunction').disabled = false;
    }

    function clearSetupFunctionControls() {
        document.getElementById('setupFunctionFunction').value = "";
        document.getElementById('setupFunctionDescription').value = "";
        document.getElementById('setupFunctionToolTip').value = "";
        document.getElementById('setupFunctionSchedule').value = "";
        document.getElementById('setupFunctionPayload').value = "";
        document.getElementById('setupFunctionActive').checked = true;
        document.getElementById('setupFunctionForceDefault').checked = false;
    }

    function onSetupSaveFunction() {
        var moduleId = FTD_DOM.getSelectedValue(document.getElementById('setupFunctionModuleCombo'));
        var functionId = document.getElementById('setupFunctionFunction').value;
        var desc = document.getElementById('setupFunctionDescription').value;
        var toolTip = document.getElementById('setupFunctionToolTip').value;
        var defaultSchedule = document.getElementById('setupFunctionSchedule').value;
        var defaultPayload = document.getElementById('setupFunctionPayload').value;
        var active = document.getElementById('setupFunctionActive').checked;
        var forceDefaultPayload = document.getElementById('setupFunctionForceDefault').checked;
        var classCode = FTD_DOM.getSelectedText(document.getElementById('setupFunctionJobCombo'));

        if (classCode == 'SimpleJmsJob')
            var queueName = FTD_DOM.getSelectedValue(document.getElementById('setupFunctionJMS'));
        else
            var queueName = FTD_DOM.getSelectedValue(document.getElementById('setupFunctionJMSAMQ'));
        document.getElementById('setupFunctionSaveButton').disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,onSetupSaveFunctionCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_UPDATE_PIPELINE%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_MODULE_ID%>",moduleId);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_PIPELINE_ID%>",functionId);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_DESCRIPTION%>",desc);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE%>",defaultSchedule);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD%>",defaultPayload);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_ACTIVE_FLAG%>",active);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD%>",forceDefaultPayload);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_CLASS_CODE%>",classCode);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_QUEUE_NAME%>",queueName);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_TOOL_TIP%>",toolTip);

        newRequest.send();
    }

    function onSetupSaveFunctionCallback(data) {
        alert(data);

        if( data=='<%=SchedulerConstants.RESULTS_UPDATE_PIPELINE_SUCCESS%>' ) {
            clearSetupFunctionControls();
            getAllPipelineDetails();
            document.getElementById('setupFunctionModuleCombo').disabled = false;
            document.getElementById('setupFunctionFunction').disabled = false;
            document.getElementById('setupFunctionNewButton').disabled = false;

        }
        document.getElementById('setupFunctionSaveButton').disabled = false;
    }

    function getApolloJobs() {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getApolloJobsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_APOLLO_JOBS%>");
        newRequest.send();
    }

    function getApolloJobsCallback(data) {
        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        var jobCombo = document.getElementById("setupFunctionJobCombo");

        //iterate through the list and remove all options
        for( var i = jobCombo.length-1; i>=0; i-- ) {
            jobCombo.remove(i);
        }

        var jobTypes = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_JOB_TYPE%>");

        for( var i = 0; i < jobTypes.length; i++) {
            var classCode = jobTypes[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_CLASS_CODE%>').nodeValue;
            var classPackage = jobTypes[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_CLASS_PACKAGE%>').nodeValue;
            jobCombo.options[i] = new Option(classCode, classPackage);
        }

        onSetupFunctionJobChange();
    }

    function getJMSQueues() {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getJMSQueuesCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_JMS_QUEUES%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_SCHEMA%>","<%=SchedulerConstants.PARAMETER_SCHEMA_OJMS%>");
        newRequest.send();
    }

    function getJMSQueuesCallback(data) {
//        alert(data);
        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        var jmsCombo = document.getElementById("setupFunctionJMS");

        //iterate through the list and remove all options
        for( var i = jmsCombo.length-1; i>=0; i-- ) {
            jmsCombo.remove(i);
        }

        var queues = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_JMS_QUEUE%>");

        for( var i = 0; i < queues.length; i++) {
            var queueName = queues[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_NAME%>').nodeValue;
            var queueValue = queues[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_VALUE%>').nodeValue;
            jmsCombo.options[i] = new Option(queueName, queueValue);
        }
    }
    
    function getJMSAMQQueues() {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getJMSAMQQueuesCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_JMS_AMQ_QUEUES%>");
        newRequest.send();
    }

    function getJMSAMQQueuesCallback(data) {
//        alert(data);
        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        var jmsCombo = document.getElementById("setupFunctionJMSAMQ");

        //iterate through the list and remove all options
        for( var i = jmsCombo.length-1; i>=0; i-- ) {
            jmsCombo.remove(i);
        }

        var queues = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_JMS_QUEUE%>");

        for( var i = 0; i < queues.length; i++) {
            var queueName = queues[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_NAME%>').nodeValue;
            var queueValue = queues[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_VALUE%>').nodeValue;
            jmsCombo.options[i] = new Option(queueName, queueValue);
        }
    }

    function onSelectSchedule() {
        if( document.getElementById('payloadMultipleId').checked==true ) {
            document.getElementById('payloadSingleId').checked=true;
        }
        document.getElementById('payloadMultipleId').disabled=true;
        document.getElementById("payloadMultipleIdLabel").disabled=true;
        document.getElementById('newPayloadsId').disabled=true;
        if( editMode == false ) {
            document.getElementById('submitJobButton').innerHTML = 'Add Job';
        }
    }

    function onSelectScheduleOnce() {
        document.getElementById('payloadMultipleId').disabled=false;
        document.getElementById("payloadMultipleIdLabel").disabled=false;
        document.getElementById('newPayloadsId').disabled=document.getElementById('newPayloadId').disabled;
        if( editMode == false ) {
            document.getElementById('submitJobButton').innerHTML = 'Run Job';
        }
    }

    function addJob() {
        var scheduleType='';
        var buttons = document.getElementsByName('scheduleRadio');

        for( var i=0; i < buttons.length; i++ ) {
            if( buttons[i].checked==true ) {
                scheduleType = buttons[i].id;
                break;
            }
        }

        if( scheduleType=='' ) {
            alert('A schedule type must be selected');
            return;
        }

        var scheduleValue="";
        var requestType="";
        if( scheduleType == "<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>" ) {
            scheduleValue = document.getElementById('newCronSchedule').value;
            requestType = "<%=SchedulerConstants.REQUEST_TYPE_ADD_JOB%>";
        } else if( scheduleType == "<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>" ) {
            scheduleValue = document.getElementById('newIntervalSchedule').value;
            requestType = "<%=SchedulerConstants.REQUEST_TYPE_ADD_JOB%>";
        } else {    //run once (don't schedule)
            requestType = "<%=SchedulerConstants.REQUEST_TYPE_RUN_JOBS%>";
            scheduleValue = "";
        }

        //Override the request type for all save/edit actions
        if( editMode == true ) {
            requestType = "<%=SchedulerConstants.REQUEST_TYPE_ADD_JOB%>";
        }


        var classCode = document.getElementById('classCode').value;

        //XMLHttpRequest
        var newRequest =
          new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,addJobCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>",requestType);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_SCHEDULE_TYPE%>",scheduleType);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_MODULE%>",selectedModuleValue);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",new Date().getTime());
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE%>",scheduleValue);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_CLASS_CODE%>",classCode);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_PIPELINE%>",selectedPipelineValue);

        if( editMode == true ) {
            newRequest.addParam("<%=SchedulerConstants.DEFINED_GROUP%>",editGroup);
            newRequest.addParam("<%=SchedulerConstants.DEFINED_JOB_NAME%>",editJobName);
        }

        if( classCode == 'SimpleJmsJob' || classCode == 'ActiveMQJmsJob' ) {
            var newPayloadValue;
            var sendMultiple;
            if( document.getElementById('payloadSingleId').checked==true ) {
                newPayloadValue=document.getElementById('newPayloadId').value;
                sendMultiple = false;
            } else {
                newPayloadValue=document.getElementById('newPayloadsId').value;
                sendMultiple = true;
            }

            var queueName = document.getElementById('queueName').value;

            newRequest.addParam("<%=SchedulerConstants.PARAMETER_PAYLOAD%>",newPayloadValue);
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_JMS_QUEUE_NAME%>",queueName);
        }  else if( classCode == 'SQLJob' ) {
            //SQL statement id
            var sqlCombo = document.getElementById("sqlCombo");
            var selectedSQLId = sqlCombo[sqlCombo.selectedIndex].value;
            var selectedSQLName = sqlCombo[sqlCombo.selectedIndex].text;
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_STATEMENT_ID%>",selectedSQLId);
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_STATEMENT_NAME%>",selectedSQLName);

            //Error checkbox
            if( document.getElementById('emailErrorCheckbox').checked == true ) {
                newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_ERROR%>","Y");
            } else {
                newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_ERROR%>","N");
            }

            //Success checkbox
            if( document.getElementById('emailSuccessCheckbox').checked == true ) {
                newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_SUCCESS%>","Y");
            } else {
                newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_SUCCESS%>","N");
            }

            //Default email subject
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_DEFAULT_SUBJECT%>",document.getElementById('defaultEmailSubject').value);

            //Default email body
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_DEFAULT_BODY%>",document.getElementById('defaultEmailBody').value);

            //Email addresses
            var emailCombo = document.getElementById('newEmailRecipients');
            var xmlDoc = createXmlDocument("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES%>","");
            var xmlRoot = xmlDoc.documentElement;
            for( var i = 0; i<emailCombo.length; i++ ) {
                var addr = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_ADDR%>");
                addr.appendChild(xmlDoc.createTextNode(emailCombo.options[i].value));
                xmlRoot.appendChild(addr);
            }
            var xmlString = getXMLNodeSerialization(xmlDoc);
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES%>",xmlString);

            //SQL Parameters
            var inParms = document.getElementsByName("<%=SchedulerConstants.PARAMETER_IN_PARAMETER%>");
            xmlDoc = createXmlDocument("<%=SchedulerConstants.PARAMETER_SQL_PARAMETERS%>","");
            xmlRoot = xmlDoc.documentElement;
            var xmlInParms = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_IN_PARAMETERS%>");
            xmlRoot.appendChild(xmlInParms);
            for( var i = 0; i < inParms.length; i++) {
                var xmlInParm = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_IN_PARAMETER%>");
                xmlInParm.setAttribute("name",inParms[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_ID%>').nodeValue);
                xmlInParm.setAttribute("value",inParms[i].value);
                xmlInParm.setAttribute("<%=SchedulerConstants.PARAMETER_PARM_TYPE%>",inParms[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_PARM_TYPE%>').nodeValue);
                xmlInParms.appendChild(xmlInParm);
            }

            var outParms = document.getElementsByName("<%=SchedulerConstants.PARAMETER_OUT_PARAMETER%>");
            var xmlOutParms = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_OUT_PARAMETERS%>");
            xmlRoot.appendChild(xmlOutParms);
            for( var i = 0; i < outParms.length; i++) {
                var xmlOutParm = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_OUT_PARAMETER%>");
                var idx = outParms[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_INDEX%>').nodeValue;
                var parmName = document.getElementById('<%=SchedulerConstants.PARAMETER_OUT_LABEL%>'+idx).innerHTML;
                var parmType = document.getElementById('<%=SchedulerConstants.PARAMETER_OUT_LABEL%>'+idx).getAttributeNode('<%=SchedulerConstants.PARAMETER_PARM_TYPE%>').nodeValue;
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_NAME%>",parmName);
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_VALUE%>",outParms[i].value);
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_PARM_TYPE%>",parmType);
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>",document.getElementById('<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>'+idx).checked?'Y':'N');
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>",document.getElementById('<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>'+idx).checked?'Y':'N');
                xmlOutParm.setAttribute("<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>",document.getElementById('<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>'+idx).checked?'Y':'N');

                xmlOutParms.appendChild(xmlOutParm);
            }

            xmlString = getXMLNodeSerialization(xmlDoc);
            newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_PARAMETERS%>",xmlString);
        }
        newRequest.send();
        document.getElementById("submitJobButton").disabled = true;
    }

    function addJobCallback(data) {
        document.getElementById("submitJobButton").disabled = false;

        alert(data);
        if( data!="<%=SchedulerConstants.RESULTS_SUCCESS%>" ) {
            return;
        }

        getScheduledJobs();
        getDefinedJobs();
        if( editMode == true ) {
            accordion.showTabByIndex(definedTabIdx,true);
        }
    }

    function pauseJob() {
        if( selectedTriggerSuffix == "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>" ) {
            alert("You must select a scheduled job before you can pause it's trigger group.");
            return;
        }

        var group = document.getElementById("<%=SchedulerConstants.SCHEDULED_GROUP%>"+selectedTriggerSuffix).value;
        var trigger = document.getElementById("<%=SchedulerConstants.SCHEDULED_TRIGGER%>"+selectedTriggerSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,pauseJobCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_PAUSE_JOB%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",group);
        newRequest.send();

        disableScheduledPanel(true);
    }

    function pauseJobCallback(data) {
        if( data== "<%=SchedulerConstants.RESULTS_PAUSE_JOB_SUCCESS%>" ) {
            getScheduledJobs();
        } else {
            alert(data);
            disableScheduledPanel(false);
        }
    }

    function resumeJob() {
        if( selectedTriggerSuffix == "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>" ) {
            alert("You must select a scheduled job before you can resume it.");
            return;
        }

        var group = document.getElementById("<%=SchedulerConstants.SCHEDULED_GROUP%>"+selectedTriggerSuffix).value;
        var trigger = document.getElementById("<%=SchedulerConstants.SCHEDULED_TRIGGER%>"+selectedTriggerSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,resumeJobCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_RESUME_JOB%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",group);
        newRequest.send();

        disableScheduledPanel(true);
    }

    function resumeJobCallback(data) {
        if( data== "<%=SchedulerConstants.RESULTS_RESUME_JOB_SUCCESS%>" ) {
            getScheduledJobs();
        } else {
            alert(data);
            disableScheduledPanel(false);
        }
    }

    function pauseModule() {
        if( selectedTriggerSuffix == "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>" ) {
            alert("You must select a scheduled job before you can pause it's trigger group.");
            return;
        }

        var module = document.getElementById("<%=SchedulerConstants.SCHEDULED_MODULE%>"+selectedTriggerSuffix).value;
        var trigger = document.getElementById("<%=SchedulerConstants.SCHEDULED_TRIGGER%>"+selectedTriggerSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,pauseModuleCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_PAUSE_MODULE%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_MODULE%>",module);
        newRequest.send();

        disableScheduledPanel(true);
    }

    function pauseModuleCallback(data) {
        if( data== "<%=SchedulerConstants.RESULTS_PAUSE_MODULE_SUCCESS%>" ) {
            getScheduledJobs();
        } else {
            alert(data);
            disableScheduledPanel(false);
        }
    }

    function resumeModule() {
        if( selectedTriggerSuffix == "<%=SchedulerConstants.NO_SELECTED_TRIGGER%>" ) {
            alert("You must select a scheduled job before you can resume the module jobs.");
            return;
        }

        var module = document.getElementById("<%=SchedulerConstants.SCHEDULED_MODULE%>"+selectedTriggerSuffix).value;
        var trigger = document.getElementById("<%=SchedulerConstants.SCHEDULED_TRIGGER%>"+selectedTriggerSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,resumeModuleCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_RESUME_MODULE%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_MODULE%>",module);
        newRequest.send();

        disableScheduledPanel(true);
        document.getElementById("scheduledRefreshButton").disabled = true;
        document.getElementById("scheduledRemoveButton").disabled = true;
        document.getElementById("scheduledPauseJobButton").disabled = true;
        document.getElementById("scheduledResumeJobButton").disabled = true;
        document.getElementById("scheduledPauseModuleButton").disabled = true;
        document.getElementById("scheduledResumeModuleButton").disabled = true;
    }

    function resumeModuleCallback(data) {
        if( data== "<%=SchedulerConstants.RESULTS_RESUME_MODULE_SUCCESS%>" ) {
            getScheduledJobs();
        } else {
            alert(data);
            disableScheduledPanel(false);
        }
    }

    function getSQLStatements() {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getSQLStatementsCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_SQL_STATEMENTS%>");
        newRequest.send();
    }

    function getSQLStatementsCallback(data) {
        xmlSqlStatements = parseXmlString(data);
        var xmlRoot = xmlSqlStatements.documentElement;

        var sqlCombo = document.getElementById("sqlCombo");
        var statements = xmlRoot.getElementsByTagName("statement");

        //iterate through sql nodes and set to null
        for( var i = sqlCombo.length-1; i>=0; i-- ) {
            sqlCombo.options[i] = null;
        }

        //iterate through sql nodes to populate group select
        for( var i = 0; i < statements.length; i++)
        {
            var statementId = statements[i].getAttributeNode('<%=SchedulerConstants.PARAMETER_ID%>').nodeValue;
            var desc = statements[i].getAttributeNode('name').nodeValue;

            sqlCombo.options[i] = new Option(desc, statementId);

            if( i == 0 ) {
                sqlCombo.options[0].selected=true;
                onSqlStatementChange();
            }
        }
    }

    function getEmailAddresses(callbackFunction) {
        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,callbackFunction,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_EMAIL_ADDRESSES%>");
        newRequest.send();
    }

    function getEmailAddressesCallback(data) {
        xmlEmailAddresses = parseXmlString(data);
        populateEmailAddresses(document.getElementById('newAvailableRecipients'));
        populateEmailAddresses(document.getElementById('setupEmailList'));
    }

    function getEmailAddressesCallback2(data) {
        xmlEmailAddresses = parseXmlString(data);
    }

    function getEmailAddressesCallback3(data) {
        xmlEmailAddresses = parseXmlString(data);
        populateEmailAddresses(document.getElementById('setupEmailList'));
    }

    function populateEmailAddresses(listBox) {
        var xmlRoot = xmlEmailAddresses.documentElement;
        var addresses = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_ADDR%>");

        //iterate through the list and remove all options
        for( var i = listBox.length-1; i>=0; i-- ) {
            listBox.remove(i);
        }

        //iterate through the email addresses and populate the list
        for( var i = 0; i < addresses.length; i++)
        {
            var address = addresses[i].firstChild.nodeValue;
            listBox.options[i] = new Option(address, address);
        }
    }

    function addEmailAddress(newEmailAddress,callbackFunction) {

      if( !isValidEmail(newEmailAddress) ) {
        alert(newEmailAddress+' does not appear to be a valid email address!');
        return;
      }

      document.getElementById('addEmailRecipientButton').disabled = true;
      document.getElementById('addEmailRecipientField').disabled = true;

      document.getElementById('setupEmailAddress').disabled = true;
      document.getElementById('setupEmailAddButton').disabled = true;
      document.getElementById('setupEmailDeleteButton').disabled = true;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,callbackFunction,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_ADD_EMAIL_ADDRESS%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_ADDR%>",newEmailAddress);
        newRequest.send();

    }

    function addEmailAddressCallback(data) {
      var expectedResults = document.getElementById('addEmailRecipientField').value;

      if( data == expectedResults ) {
          var listBox = document.getElementById('newEmailRecipients');
          listBox.options[listBox.length] = new Option(data, data);

          listBox = document.getElementById('setupEmailList');
          listBox.options[listBox.length] = new Option(data, data);

          document.getElementById('addEmailRecipientField').value = "";
      } else {
        alert('Problem with adding the email address:\r\n'+data);
      }

      document.getElementById('addEmailRecipientButton').disabled = false;
      document.getElementById('addEmailRecipientField').disabled = false;

      document.getElementById('setupEmailAddress').disabled = false;
      document.getElementById('setupEmailAddButton').disabled = false;
      document.getElementById('setupEmailDeleteButton').disabled = false;

      getEmailAddresses(getEmailAddressesCallback2);
    }

    function addSetupEmailAddressCallback(data) {
      var expectedResults = document.getElementById('setupEmailAddress').value;

      if( data == expectedResults ) {
          var listBox = document.getElementById('newAvailableRecipients');
          listBox.options[listBox.length] = new Option(data, data);

          listBox = document.getElementById('setupEmailList');
          listBox.options[listBox.length] = new Option(data, data);

          document.getElementById('setupEmailAddress').value = "";
      } else {
        alert('Problem with adding the email address:\r\n'+data);
      }

      document.getElementById('addEmailRecipientButton').disabled = false;
      document.getElementById('addEmailRecipientField').disabled = false;

      document.getElementById('setupEmailAddress').disabled = false;
      document.getElementById('setupEmailAddButton').disabled = false;
      document.getElementById('setupEmailDeleteButton').disabled = false;

      getEmailAddresses(getEmailAddressesCallback2);
    }

    function isValidEmail(str) {
      return (str.indexOf(".") > 2) && (str.indexOf("@") > 0);
    }

    function onSetupEmailDelete() {
        var listBox = document.getElementById('setupEmailList');

        var xmlDoc = createXmlDocument("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES%>","");
        var xmlRoot = xmlDoc.documentElement;
        var selectCount = 0;

        for( var i = 0; i<listBox.length; i++ ) {
            if (listBox.options[i].selected) {
                var addr = xmlDoc.createElement("<%=SchedulerConstants.PARAMETER_ADDR%>");
                addr.appendChild(xmlDoc.createTextNode(listBox.options[i].value));
                xmlRoot.appendChild(addr);
                selectCount++;
            }
        }

        if( selectCount==0 ) {
            alert("No email addresses were selected.  Nothing to do.");
            return;
        }

        document.getElementById('addEmailRecipientButton').disabled = true;
        document.getElementById('addEmailRecipientField').disabled = true;

        document.getElementById('setupEmailAddress').disabled = true;
        document.getElementById('setupEmailAddButton').disabled = true;
        document.getElementById('setupEmailDeleteButton').disabled = true;

        var xmlString = getXMLNodeSerialization(xmlDoc);
        alert(xmlString);

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,onSetupEmailDeleteCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_DELETE_EMAIL_ADDRESSES%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES%>",xmlString);
        newRequest.send();
    }

    function onSetupEmailDeleteCallback(data) {
        getEmailAddresses(getEmailAddressesCallback3);
        alert(data);

        document.getElementById('addEmailRecipientButton').disabled = false;
        document.getElementById('addEmailRecipientField').disabled = false;

        document.getElementById('setupEmailAddress').disabled = false;
        document.getElementById('setupEmailAddButton').disabled = false;
        document.getElementById('setupEmailDeleteButton').disabled = false;

        getScheduledJobs();
        getDefinedJobs();
    }

    function onSqlStatementChange() {
        var sqlCombo = document.getElementById("sqlCombo");
        var inParmsTable = document.getElementById("sqlInputParms");
        var outParmsTable = document.getElementById("sqlOutputParms");
        var selectedId = sqlCombo[sqlCombo.selectedIndex].value;

        //Clear out the input parms table (tbody)
        FTD_DOM.removeAllTableRows(inParmsTable);

        //Clear out the output parms table (tbody)
        FTD_DOM.removeAllTableRows(outParmsTable);

        var xmlRoot = xmlSqlStatements.documentElement;
        var statements = xmlRoot.getElementsByTagName("statement");

        //iterate through sql nodes to populate group select
        for( var i = 0; i < statements.length; i++)
        {
            var statementId = statements[i].getAttributeNode('id').nodeValue;
            if( statementId == selectedId ) {
                var xmlParms = statements[i].getElementsByTagName("<%=SchedulerConstants.PARAMETER_PARAMETERS%>")[0];
                var xmlInParms = xmlParms.getElementsByTagName("<%=SchedulerConstants.PARAMETER_IN_PARAMETERS%>")[0].getElementsByTagName("<%=SchedulerConstants.PARAMETER_IN_PARAMETER%>");

                for( var j = 0; j < xmlInParms.length; j++)
                {
                  var parmName = xmlInParms[j].getAttributeNode('name').nodeValue;
                  var parmType = xmlInParms[j].getAttributeNode('type').nodeValue;
                  var row = inParmsTable.insertRow(inParmsTable.rows.length);

                  var cell = row.insertCell(0);
                  cell.className='Label';
                  var label = document.createElement('label');
                  label.setAttribute('<%=SchedulerConstants.PARAMETER_ID%>',parmName+'Label');
                  label.setAttribute('for',parmName);
                  label.setAttribute('title',parmType);
                  label.innerHTML = parmName;
                  cell.appendChild(label);

                  var input = new FTD_DOM.InputElement( {
                        type:'text',
                        id:parmName,
                        name:'<%=SchedulerConstants.PARAMETER_IN_PARAMETER%>',
                        <%=SchedulerConstants.PARAMETER_INDEX%>:j,
                        size:40,
                        title:parmType,
                        <%=SchedulerConstants.PARAMETER_PARM_TYPE%>:parmType} ).createNode();

                  cell = row.insertCell(1);
                  cell.appendChild(input);

                  new FTD_DOM.FocusElement(parmName, borderFocusColor, parmName+'Label', labelFocusColor);
                }

                var xmlOutParms = xmlParms.getElementsByTagName("<%=SchedulerConstants.PARAMETER_OUT_PARAMETERS%>")[0].getElementsByTagName("<%=SchedulerConstants.PARAMETER_OUT_PARAMETER%>");
                for( var j = 0; j < xmlOutParms.length; j++)
                {
                  var parmName = xmlOutParms[j].getAttributeNode('name').nodeValue;
                  var parmType = xmlOutParms[j].getAttributeNode('type').nodeValue;
                  var row = outParmsTable.insertRow(outParmsTable.rows.length);

                  var cell = row.insertCell(0);
                  cell.className='Label';
                  var label = document.createElement('label');
                  label.setAttribute('<%=SchedulerConstants.PARAMETER_ID%>','<%=SchedulerConstants.PARAMETER_OUT_LABEL%>'+j);
                  label.setAttribute('title',parmType);
                  label.setAttribute('<%=SchedulerConstants.PARAMETER_PARM_TYPE%>',parmType);
                  label.setAttribute('<%=SchedulerConstants.PARAMETER_INDEX%>',j);
                  label.innerHTML = parmName;
                  cell.appendChild(label);

                  var input = new FTD_DOM.InputElement( {
                        type:'hidden',
                        id:parmName,
                        <%=SchedulerConstants.PARAMETER_INDEX%>:j} ).createNode();
                  cell.appendChild(input);

                  input = new FTD_DOM.InputElement( {
                        id:'<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>'+j,
                        type:'checkbox',
                        name:'<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>',
                        onclick:'singleCheck(this);'} ).createNode();
                  cell.appendChild(input);
                  cell = row.insertCell(1);
                  cell.appendChild(input);

                  input = new FTD_DOM.InputElement( {
                        id:'<%=SchedulerConstants.PARAMETER_ERROR_FIELD%>'+j,
                        type:'text',
                        name:'<%=SchedulerConstants.PARAMETER_OUT_PARAMETER%>',
                        <%=SchedulerConstants.PARAMETER_INDEX%>:j} ).createNode();
                  cell = row.insertCell(2);
                  cell.appendChild(input);

                  input = new FTD_DOM.InputElement( {
                        id:'<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>'+j,
                        type:'checkbox',
                        name:'<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>',
                        onclick:'singleCheck(this);'} ).createNode();
                  cell = row.insertCell(3);
                  cell.appendChild(input);

                  input = new FTD_DOM.InputElement( {
                        id:'<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>'+j,
                        type:'checkbox',
                        name:'<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>',
                        onclick:'singleCheck(this);'} ).createNode();
                  cell = row.insertCell(4);
                  cell.appendChild(input);

                  new FTD_DOM.FocusElement('<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>'+j, borderFocusColor, 'resultFieldLabel', selectedCaptionColor);
                  new FTD_DOM.FocusElement('<%=SchedulerConstants.PARAMETER_ERROR_FIELD%>'+j, borderFocusColor, 'errorValueLabel', selectedCaptionColor);
                  new FTD_DOM.FocusElement('<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>'+j, borderFocusColor, 'emailSubjectLabel', selectedCaptionColor);
                  new FTD_DOM.FocusElement('<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>'+j, borderFocusColor, 'emailBodyLabel', selectedCaptionColor);
                }
            }
        }
    }

    function getJobDetailForEdit(selectedDefinedIdx) {
        selectedJobSuffix = +selectedDefinedIdx;
        document.getElementById("<%=SchedulerConstants.DEFINED_RADIO%>"+selectedJobSuffix).checked = true;

        document.getElementById("refreshScheduleButton").disabled = true;
        document.getElementById("runButton").disabled = true;
        document.getElementById("scheduleButton").disabled = true;
        document.getElementById("removeButton").disabled = true;

        var jobName = document.getElementById("<%=SchedulerConstants.DEFINED_JOB_NAME%>"+selectedJobSuffix).value;
        var group = document.getElementById("<%=SchedulerConstants.DEFINED_GROUP%>"+selectedJobSuffix).value;

        //XMLHttpRequest
        var newRequest =
            new FTD_AJAX.Request('submitSchedule.do',FTD_AJAX_REQUEST_TYPE_POST,getJobDetailForEditCallback,false);
        newRequest.addParam("<%=SchedulerConstants.REQUEST_TYPE%>","<%=SchedulerConstants.REQUEST_TYPE_GET_JOB_DETAIL%>");
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_GROUP%>",group);
        newRequest.addParam("<%=SchedulerConstants.PARAMETER_JOB_NAME%>",jobName);
        newRequest.send();
    }

    function getJobDetailForEditCallback(data) {
        var xmlDoc = parseXmlString(data);
        var xmlRoot = xmlDoc.documentElement;
        editJobName = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_JOB_NAME%>")[0].firstChild.nodeValue;
        editGroup = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_GROUP%>")[0].firstChild.nodeValue;

        var parms = xmlRoot.getElementsByTagName("<%=SchedulerConstants.PARAMETER_PARAMETERS%>")[0];
        selectedModuleValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_MODULE%>");
        selectedPipelineValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_PIPELINE%>");

        document.getElementById('jobNotes').innerHTML = "Module "+selectedModuleValue+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Function "+selectedPipelineValue;
        
        var defaultSchedule = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE%>");
        var scheduleType = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SCHEDULE_TYPE%>");

        if( scheduleType == "<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>" ) {
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>").checked = true;
            document.getElementById("newCronSchedule").value = defaultSchedule;
            document.getElementById("newIntervalSchedule").value = "";
        } else if( scheduleType == "<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>" ) {
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>").checked = true;
            document.getElementById("newCronSchedule").value = "";
            document.getElementById("newIntervalSchedule").value = defaultSchedule;
        } else if( scheduleType == "<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>" ) {
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>").checked = false;
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>").checked = false;
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>").checked = false;
            document.getElementById("newCronSchedule").value = "";
            document.getElementById("newIntervalSchedule").value = "";
        } else {
          alert("Unable to determine default schedule type.");
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>").checked = false;
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>").checked = false;
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>").checked = false;
            document.getElementById("newCronSchedule").value = "";
            document.getElementById("newIntervalScheduleLabel").value = "";
        }

        var classCode = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_CLASS_CODE%>");
        setClassCode(classCode);

        if( classCode == "<%=SchedulerConstants.JMS_JOB_CLASS_CODE%>" ) {
            var selectedJmsQueueNameValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_JMS_QUEUE_NAME%>");
            var payload = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_PAYLOAD%>");
            document.getElementById("queueName").value = selectedJmsQueueNameValue;
            document.getElementById("newPayloadId").value = payload;
            document.getElementById("newPayloadsId").value = "";
            document.getElementById("payloadSingleId").checked = true;
            document.getElementById("payloadMultipleId").disabled = true;
            document.getElementById("payloadMultipleIdLabel").disabled=true;
            document.getElementById("newPayloadsId").disabled = true;
        } else if( classCode == "<%=SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE%>" ) {
            var selectedJmsQueueNameValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_JMS_QUEUE_NAME%>");
            var payload = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_PAYLOAD%>");
            document.getElementById("queueName").value = selectedJmsQueueNameValue;
            document.getElementById("newPayloadId").value = payload;
            document.getElementById("newPayloadsId").value = "";
            document.getElementById("payloadSingleId").checked = true;
            document.getElementById("payloadMultipleId").disabled = true;
            document.getElementById("payloadMultipleIdLabel").disabled=true;
            document.getElementById("newPayloadsId").disabled = true;
        } else if( classCode == "<%=SchedulerConstants.SQL_JOB_CLASS_CODE%>" ) {
            //SQL combo
            var selectedSQLId = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_STATEMENT_ID%>");
            var sqlCombo = document.getElementById("sqlCombo");
            for( var i = 0; i < sqlCombo.length; i++ ) {
                if( sqlCombo.options[i].value == selectedSQLId ) {
                    sqlCombo.options[i].selected=true;
                    onSqlStatementChange();
                    break;
                }
            }

            var checkValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_ERROR%>");
            var getEmailDetails = false;
            if( checkValue == 'Y' ) {
                document.getElementById("emailErrorCheckbox").checked = true;
                getEmailDetails = true;
            } else {
                document.getElementById("emailErrorCheckbox").checked = false;
            }

            checkValue = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ON_SUCCESS%>");
            if( checkValue == 'Y' ) {
                document.getElementById("emailSuccessCheckbox").checked = true;
                getEmailDetails = true;
            } else {
                document.getElementById("emailSuccessCheckbox").checked = false;
            }

            if( getEmailDetails == true ) {
                var defaultEmailSubject;
                try {
                    defaultEmailSubject = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_DEFAULT_SUBJECT%>");
                } catch (err) {
                    defaultEmailSubject = "";
                }
                document.getElementById("defaultEmailSubject").value = defaultEmailSubject;

                var defaultEmailBody;
                try {
                    defaultEmailBody = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_DEFAULT_BODY%>");
                } catch (err) {
                    defaultEmailBody = "";
                }
                document.getElementById("defaultEmailBody").value = defaultEmailBody;

                //Populate the available address list
                populateEmailAddresses(document.getElementById('newAvailableRecipients'));

                //Empty out the recipient list
                var emailList = document.getElementById('newEmailRecipients');
                for( var j = emailList.length-1; j>=0; j-- ) {
                    emailList.remove(j);
                }

                //Get the email addresses that pertain to this job
                var strXml = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES%>");
                var xmlDoc = parseXmlString(strXml);
                var addresses = xmlDoc.getElementsByTagName("<%=SchedulerConstants.PARAMETER_ADDR%>");
                var availList = document.getElementById('newAvailableRecipients');
                //iterate through the email addresses and populate the list
                for( var i = 0; i < addresses.length; i++)
                {
                    var address = addresses[i].firstChild.nodeValue;
                    emailList.options[i] = new Option(address, address);

                    //Now loop through and remove it from the available email list
                    var bFound = false;
                    for( var j = availList.length-1; j>=0; j-- ) {
                        if( availList.options[j].value == address ) {
                            availList.remove(j);
                            bFound = true;
                            break;
                        }
                    }
                }


                onEmailCheckbox();
            }

            //SQL input parameters
            var strXml = selectNodeText(parms,"<%=SchedulerConstants.PARAMETER_SQL_PARAMETERS%>");
            var sqlParms = parseXmlString(strXml);

            var inParms = sqlParms.getElementsByTagName("<%=SchedulerConstants.PARAMETER_IN_PARAMETER%>");
            for( var i = 0; i<inParms.length; i++ ) {
                var parmName = inParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_NAME%>").nodeValue;
                var parmValue = inParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_VALUE%>").nodeValue;
                document.getElementById(parmName).value = parmValue;
            }

            //SQL output parameters
            var outParms = sqlParms.getElementsByTagName("<%=SchedulerConstants.PARAMETER_OUT_PARAMETER%>");
            for( var i = 0; i<outParms.length; i++ ) {
                var parmName = outParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_NAME%>").nodeValue;
                var parmValue = outParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_VALUE%>").nodeValue;
                var resultField = outParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>").nodeValue;
                var emailSubject = outParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>").nodeValue;
                var emailBody = outParms[i].getAttributeNode("<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>").nodeValue;
                var rowIdx = document.getElementById(parmName).getAttribute("<%=SchedulerConstants.PARAMETER_INDEX%>");

                if( resultField == "Y" ) {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>"+rowIdx).checked = true;
                } else {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_RESULT_FIELD%>"+rowIdx).checked = false;
                }

                if( emailSubject == "Y" ) {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>"+rowIdx).checked = true;
                } else {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_EMAIL_SUBJECT%>"+rowIdx).checked = false;
                }

                if( emailBody == "Y" ) {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>"+rowIdx).checked = true;
                } else {
                    document.getElementById("<%=SchedulerConstants.PARAMETER_EMAIL_BODY%>"+rowIdx).checked = false;
                }

                document.getElementById("<%=SchedulerConstants.PARAMETER_ERROR_FIELD%>"+rowIdx).value = parmValue;
            }
        }

        payloadChanged = false;
        scheduleChanged = false;
        document.getElementById("refreshScheduleButton").disabled = false;
        document.getElementById("runButton").disabled = false;
        document.getElementById("scheduleButton").disabled = false;
        document.getElementById("removeButton").disabled = false;

        editMode = true;
        changingToEditMode = true;
        changeEditModeTitle('<%=SchedulerConstants.EDIT_MODE_TITLE_EDIT%> '+editJobName);
        document.getElementById('div_new_panal_parms').style.display = "none";
        document.getElementById('submitJobButton').innerHTML = 'Save Job';
        document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>").disabled = true;
        document.getElementById('runOnceSpan').disabled = true;
        accordion.showTabByIndex(editTabIdx,true);
    }

    function changeAccordionTab(currentTab) {
        var exitedEditMode = false;
        if( changingToEditMode == false ) {
            editMode = false;
            changeEditModeTitle('<%=SchedulerConstants.EDIT_MODE_TITLE_CREATE%>');
            document.getElementById("<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>").disabled = false;
            document.getElementById('runOnceSpan').disabled = false;
            document.getElementById("payloadMultipleId").disabled = false;
            document.getElementById("payloadMultipleIdLabel").disabled = false;
            document.getElementById("newPayloadsId").disabled = false;
            document.getElementById('div_new_panal_parms').style.display = "inline";
            var moduleCombo = document.getElementById('module_combo');
            selectedModuleValue=moduleCombo[moduleCombo.selectedIndex].value;
            exitedEditMode = true;
        }
        changingToEditMode = false;

        if( exitedEditMode == true ) {
            getPipelinesForModule();
        }
    }

    function changeEditModeTitle(newTitle) {
        accordion.accordionTabs[editTabIdx].titleBar.innerHTML=newTitle;
    }

    function onEmailCheckbox() {
      var errorCheckbox = document.getElementById("emailErrorCheckbox");
      var successCheckbox = document.getElementById("emailSuccessCheckbox");
      var emailArea = document.getElementById("emailNotify");

      if( errorCheckbox.checked || successCheckbox.checked ) {
        emailArea.style.display = "inline";
      } else {
        emailArea.style.display = "none";
      }
    }

    function setClassCode(newClassCode) {
        document.getElementById('classCode').value = newClassCode;
        if( newClassCode == '<%=SchedulerConstants.JMS_JOB_CLASS_CODE%>' ) {
            document.getElementById('payloadParmsId').style.display = "inline";
        } else if( newClassCode == '<%=SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE%>' ) {
            document.getElementById('payloadParmsId').style.display = "inline";
        } else {
            document.getElementById('payloadParmsId').style.display = "none";
        }
        if( newClassCode == '<%=SchedulerConstants.SQL_JOB_CLASS_CODE%>' ) {
            document.getElementById('sqlParmsId').style.display = "inline";
        } else {
            document.getElementById('sqlParmsId').style.display = "none";
        }
    }

    function singleCheck(checkbox) {
      var boxes = document.getElementsByName(checkbox.name);
      for( var i = 0; i < boxes.length; i++ ) {
        if( boxes[i].id != checkbox.id ) {
          boxes[i].checked = false;
        }
      }
    }

    function disableScheduledPanel(trueOrFalse) {
        document.getElementById("scheduledRefreshButton").disabled = trueOrFalse;
        document.getElementById("scheduledRemoveButton").disabled = trueOrFalse;
        document.getElementById("scheduledPauseJobButton").disabled = trueOrFalse;
        document.getElementById("scheduledResumeJobButton").disabled = trueOrFalse;
        document.getElementById("scheduledPauseModuleButton").disabled = trueOrFalse;
        document.getElementById("scheduledResumeModuleButton").disabled = trueOrFalse;
    }

    function onSetupModuleChange(idx) {
        document.getElementById('<%=SchedulerConstants.PARAMETER_SETUP_MODULE_SAVE_CHECKBOX%>'+idx).checked=true;
    }

    onloads.push(addFTDDOMEvents);

    function addFTDDOMEvents()
    {
        new FTD_DOM.FocusElement('newSchedule', borderFocusColor, 'newScheduleLabel', labelFocusColor);
        new FTD_DOM.FocusElement('module_combo', borderFocusColor, 'module_comboLabel', labelFocusColor);
        new FTD_DOM.FocusElement('pipelineCombo', borderFocusColor, 'pipelineComboLabel', labelFocusColor);
        new FTD_DOM.FocusElement('sqlCombo', borderFocusColor, 'sqlComboLabel', labelFocusColor);

        new FTD_DOM.FocusElement('newCronSchedule', borderFocusColor, 'newCronScheduleLabel', labelFocusColor);
        new FTD_DOM.FocusElement('<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>', borderFocusColor, 'newCronScheduleLabel', labelFocusColor);

        new FTD_DOM.FocusElement('newIntervalSchedule', borderFocusColor, 'newIntervalScheduleLabel', labelFocusColor);
        new FTD_DOM.FocusElement('<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>', borderFocusColor, 'newIntervalScheduleLabel', labelFocusColor);

        new FTD_DOM.FocusElement('<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>', borderFocusColor, 'runOnceSpan', labelFocusColor);

        new FTD_DOM.FocusElement('newPayloadId', borderFocusColor, 'newPayloadIdLabel', labelFocusColor);
        new FTD_DOM.FocusElement('payloadSingleId', borderFocusColor, 'newPayloadIdLabel', labelFocusColor);

        new FTD_DOM.FocusElement('newPayloadsId', borderFocusColor, 'payloadMultipleIdLabel', labelFocusColor);
        new FTD_DOM.FocusElement('payloadMultipleId', borderFocusColor, 'payloadMultipleIdLabel', labelFocusColor);

        new FTD_DOM.FocusElement('emailErrorCheckbox', borderFocusColor, 'emailErrorCheckboxLabel', labelFocusColor);
        new FTD_DOM.FocusElement('emailSuccessCheckbox', borderFocusColor, 'emailSuccessCheckboxLabel', labelFocusColor);
        new FTD_DOM.FocusElement('defaultEmailSubject', borderFocusColor, 'defaultEmailSubjectLabel', labelFocusColor);
        new FTD_DOM.FocusElement('defaultEmailBody', borderFocusColor, 'defaultEmailBodyLabel', labelFocusColor);

        new FTD_DOM.FocusElement('setupFunctionModuleCombo', borderFocusColor, 'setupFunctionModuleComboLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionFunction', borderFocusColor, 'setupFunctionFunctionLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionActive', borderFocusColor, 'setupFunctionActiveLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionDescription', borderFocusColor, 'setupFunctionDescriptionLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionToolTip', borderFocusColor, 'setupFunctionToolTipLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionSchedule', borderFocusColor, 'setupFunctionScheduleLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionPayload', borderFocusColor, 'setupFunctionPayloadLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionJobCombo', borderFocusColor, 'setupFunctionJobComboLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionJMS', borderFocusColor, 'setupFunctionJMSLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionJMSAMQ', borderFocusColor, 'setupFunctionJMSAMQLabel', labelFocusColor);
        new FTD_DOM.FocusElement('setupFunctionForceDefault', borderFocusColor, 'setupFunctionForceDefaultLabel', labelFocusColor);

        new FTD_DOM.FocusElement('setupEmailAddress', borderFocusColor, 'setupEmailAddressLabel', labelFocusColor);

    }
</script>

<form action="/submitSchedule" method="post">
  <jsp:include page="/includes/security.jsp"/>
  <a name="top"></a>
  <table width="850px" align="center">
      <tbody>
      <tr>
          <td>
              <div id="accordionDiv" style="border-top: 1px solid; margin-top: 6px" align="center">
                <div id="div_scheduled_panel">
                  <div id="div_scheduled_header" class="accordionTabTitleBar" align="left">Scheduled Jobs</div>
                  <div id="div_scheduled_context">
                    <br/>
                    <table border="1" cellpadding="0" cellspacing="0">
                      <tbody>
                      <tr>
                          <td valign="top">
                            <div id="scheduledJobsDiv" style="overflow: auto; height: 500px">
                                <table class="dztable sort01 table-autostripe table-autosort:2 table-stripeclass:alternate table-rowshade-alternate"
                                       border="0"
                                       cellpadding="2"
                                       cellspacing="0">
                                  <caption>Scheduled Jobs</caption>
                                  <thead>
                                      <tr>
                                          <th>&nbsp;</th>
                                          <th class="table-sortable:default">Job</th>
                                          <th class="table-sortable:default" width="125px">Module</th>
                                          <th class="table-sortable:default" width="350px">Parameters</th>
                                          <th class="table-sortable:default" width="125px">Schedule</th>
                                          <th class="table-sortable:default" width="125px">Previous Run</th>
                                          <th class="table-sortable:default" width="125px">Next Run</th>
                                     </tr>
                                  </thead>
                                  <tbody id="scheduledJobsTbody">
                                    <tr><td colspan="5">&nbsp;</td></tr>
                                 </tbody>
                                 </table>
                             </div>
                          </td>
                      </tr>
                      </tbody>
                    </table>
                    <table border="0" cellspacing="3">
                      <tbody>
                      <tr>
                          <td><button id="scheduledRefreshButton" type="button" onclick="getScheduledJobs()">Refresh</button></td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td><button id="scheduledPauseJobButton" type="button" onclick="pauseJob()">Pause Job</button></td>
                          <td><button id="scheduledResumeJobButton" type="button" onclick="resumeJob()">Resume Job</button></td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td><button id="scheduledPauseModuleButton" type="button" onclick="pauseModule()">Pause Module</button></td>
                          <td><button id="scheduledResumeModuleButton" type="button" onclick="resumeModule()">Resume Module</button></td>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td><button id="scheduledRemoveButton" type="button" onclick="removeTrigger()">Remove</button></td>
                      </tr>
                    </tbody>
                    </table>
                  </div>
                </div>

                <div id="div_defined_panel">
                  <div id="div_defined_header" class="accordionTabTitleBar" align="left">Defined Jobs</div>
                  <div id="div_defined_context">
                    <br/>
                    <table border="1" cellpadding="0" cellspacing="0">
                      <tbody>
                      <tr>
                          <td valign="top">
                            <div id="definedJobsDiv" style="overflow: auto; height: 500px">
                                <table class="dztable sort01 table-autostripe table-autosort:2 table-stripeclass:alternate table-rowshade-alternate"
                                       border="0"
                                       cellpadding="2"
                                       cellspacing="0">
                                  <caption>Defined Jobs</caption>
                                  <thead>
                                      <tr>
                                          <th>&nbsp;</th>
                                          <th class="table-sortable:default" width="125px">Job</th>
                                          <th class="table-sortable:default" width="125px">Module</th>
                                          <th class="table-sortable:default" width="350px">Parameters</th>
                                          <th class="table-sortable:default" width="125px">Class</th>
                                      </tr>
                                  </thead>
                                  <tbody id="definedJobsTbody">
                                    <tr><td colspan="5">&nbsp;</td></tr>
                                  </tbody>
                                 </table>
                             </div>
                          </td>
                      </tr>
                    </tbody>
                    </table>
                    <table border="0" cellspacing="3">
                      <tbody>
                        <tr>
                            <td>
                                <button id="refreshScheduleButton" type="button" onClick="getDefinedJobs();">Refresh</button>
                            </td>
                            <td>
                                    &nbsp;
                            </td>
                            <td class="Label">
                                <label id="newScheduleLabel" for="newSchedule">Schedule</label></td>
                            <td>
                                <input type="text" id="newSchedule" size="30" maxlength="80" onchange="scheduleChanged=true;" title="Enter a cron expression or interval seconds" />
                            </td>
                            <td>
                                <button id="scheduleButton" type="button" onClick="scheduleJob();">Schedule Job</button>
                            </td>
                            <td>
                                    &nbsp;
                            </td>
                            <td>
                                <button id="runButton" type="button" value="Run" onClick="runOnce();">Run Now</button>
                            </td>
                            <td>
                                <button id="removeButton" type="button" value="Remove" onClick="removeJob();">Remove</button>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div id="div_new_panel">
                  <div id="div_new_header" class="accordionTabTitleBar" align="left"><%=SchedulerConstants.EDIT_MODE_TITLE_CREATE%></div>
                  <div id="div_new_context">
                    <br/>
                    <table border="1" cellpadding="0" cellspacing="0" width="700px">
                      <tbody>
                      <tr>
                        <td colspan="2" id="jobDetailsCell">
                        <div id="div_new_panal_parms">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                              <td width="50%">
                                <div align="center">
                                  <table border="0" cellpadding="2" cellspacing="0">
                                    <tbody>
                                    <tr>
                                      <td class="LabelRight" valign="top"><label id="module_comboLabel" for="module_combo">Module&nbsp;</label></td>
                                        <td>
                                          <div id="div_new_module_combo">
                                            <select id="module_combo" onchange="selectedModuleValue=this[this.selectedIndex].value; getPipelinesForModule();">
                                              <option value="LOADING">Loading...</option>
                                            </select>
                                          </div>
                                        </td>
                                      </tr>
                                  </tbody>
                                  </table>
                                </div>
                              </td>
                              <td width="50%">
                                <div align="center">
                                  <table border="0" cellpadding="2" cellspacing="0">
                                    <tbody>
                                    <tr>
                                      <td class="LabelRight" valign="top"><label id="pipelineComboLabel" for="pipelineCombo">Function&nbsp;</label></td>
                                      <td>
                                        <div id="div_new_pipeline_combo">
                                          <select id="pipelineCombo" onchange="selectedPipelineValue=this[this.selectedIndex].value; getPipelineDetail();">
                                            <option value="LOADING">Loading...</option>
                                          </select>
                                        </div>
                                      </td>
                                    </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                          </table>
                          </div>

                          <div id="div_new_job_notes">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <span id="jobNotes">&nbsp;</span>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>

                          <div id="jobDetailsDiv">
                            <div id="defaultScheduleDiv">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <caption>Default Schedule</caption>
                                <tbody>
                                  <tr>
                                    <td align="center">
                                      <input id="queueName" type="hidden" name="queueName"/>
                                      <input id="classCode" type="hidden" name="classCode"/>
                                      <table border="0" cellpadding="2" cellspacing="2" align="center" >
                                        <tbody>
                                            <tr>
                                                <td><input type="radio" align="left" id="<%=SchedulerConstants.SELECTED_SCHEDULE_CRON%>" name="scheduleRadio" checked="checked" onfocus="onSelectSchedule();" /></td>
                                                <td class="LabelLeft"><label id="newCronScheduleLabel" for="newCronSchedule">Cron Schedule</label></td>
                                                <td><input type="text" size="30" maxlength="80" id="newCronSchedule"/></td>
                                            </tr>
                                            <tr>
                                                <td><input type="radio" id="<%=SchedulerConstants.SELECTED_SCHEDULE_INTERVAL%>" name="scheduleRadio" onfocus="onSelectSchedule();" /></td>
                                                <td class="LabelLeft"><label id="newIntervalScheduleLabel" for="newIntervalSchedule">Interval (seconds)</label></td>
                                                <td><input type="text" size="30" maxlength="80" id="newIntervalSchedule"/></td>
                                            </tr>
                                            <tr>
                                                <td><input type="radio" id="<%=SchedulerConstants.SELECTED_SCHEDULE_RUN_ONCE%>" name="scheduleRadio" onfocus="onSelectScheduleOnce();" /> </td>
                                                <td class="LabelLeft" colspan="2"><span id="runOnceSpan">Run Once (no schedule)</span></td>
                                            </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <div id="payloadParmsId"
                               style="visibility: visible;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td valign="top" width="100%" align="center">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <caption>Payload</caption>
                                                  <tbody>
                                                    <tr>
                                                      <td align="center">
                                                        <table border="0" cellpadding="2" cellspacing="2" align="center">
                                                          <tbody>
                                                            <tr>
                                                                <td><input type="radio" id="payloadSingleId" name="payloadRadio" checked="checked" /> </td>
                                                                <td class="LabelLeft"><label id="newPayloadIdLabel" for="newPayloadId">Single</label></td>
                                                                <td style="TEXT-ALIGN: left;"><input type="text" size="30" maxlength="500" id="newPayloadId"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top"><input type="radio" id="payloadMultipleId" name="payloadRadio"/> </td>
                                                                <td valign="top" class="LabelLeft"><label id="payloadMultipleIdLabel" for="payloadMultipleId">Multiple</label></td>
                                                                <td><textarea cols="40" rows="3" id="newPayloadsId" title="Multiple payloads are only valid with the &#34;Run Once&#34; schedule."></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">(one payload per line)</td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="sqlParmsId"
                               style="visibility: visible;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="100%">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <caption>SQL Parameters</caption>
                                                  <tbody>
                                                  <tr>
                                                    <td align="center">
                                                      <table border="0" align="center">
                                                        <tbody>
                                                        <tr>
                                                          <td>&nbsp;</td>
                                                          <td class="LabelRight">
                                                            <label id="sqlComboLabel" for="sqlCombo">SQL Statement</label>
                                                          </td>
                                                          <td>
                                                            <select id="sqlCombo" onchange="onSqlStatementChange();">
                                                              <option value="LOADING">Loading...</option>
                                                            </select>
                                                          </td>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>
                                                            <input id="emailErrorCheckbox" type="checkbox" onclick="onEmailCheckbox();" />
                                                          </td>
                                                          <td class="Label">
                                                            <label id="emailErrorCheckboxLabel" for="emailErrorCheckbox">Send email on error</label>
                                                          </td>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>&nbsp;</td>
                                                          <td>
                                                            <input id="emailSuccessCheckbox" type="checkbox" onclick="onEmailCheckbox();" />
                                                          </td>
                                                          <td class="Label">
                                                            <label id="emailSuccessCheckboxLabel" for="emailSuccessCheckbox">Send email on success</label>
                                                          </td>
                                                          <td>&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td valign="top"  width="100%" align="center">
                                                      <div id="emailNotify">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                                        <caption>Email Notifications</caption>
                                                        <!-- <thead/> -->
                                                        <tbody>
                                                        <tr>
                                                          <td width="50%" valign="top">
                                                            <table border="1" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                              <tbody>
                                                                <tr>
                                                                  <td width="50%" valign="top" height="100%">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                                      <caption>Defaults</caption>
                                                                      <!-- <thead/> -->
                                                                      <tbody>
                                                                        <tr>
                                                                          <td>
                                                                            <table border="0" align="center" width="100%">
                                                                              <tbody>
                                                                              <tr>
                                                                                <td class="LabelRight">
                                                                                  <label id="defaultEmailSubjectLabel" for="defaultEmailSubject">Subject</label>
                                                                                </td>
                                                                                <td style="text-align:left;">
                                                                                  <input id="defaultEmailSubject" type="text" size="40"/>
                                                                                </td>
                                                                              </tr>
                                                                              <tr>
                                                                                <td class="LabelRight" valign="top">
                                                                                  <label id="defaultEmailBodyLabel" for="defaultEmailBody">Body</label>
                                                                                </td>
                                                                                <td align="left"
                                                                                    style="text-align:left;">
                                                                                  <textarea id="defaultEmailBody" cols="40" rows="5"></textarea>
                                                                                </td>
                                                                              </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </td>
                                                                  <td width="50%" valign="top" height="100%">
                                                                    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                                                                      <caption>Recipients</caption>
                                                                      <thead>
                                                                        <tr>
                                                                          <th>Available</th>
                                                                          <th>&nbsp;</th>
                                                                          <th>Send To</th>
                                                                        </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                        <tr>
                                                                          <td>
                                                                              <select size="5"
                                                                                      id="newAvailableRecipients"
                                                                                      name="newAvailableRecipients"
                                                                                      multiple="multiple">
                                                                                <option value="LOADING">Loading...</option>
                                                                              </select>
                                                                          </td>
                                                                          <td>
                                                                            <table>
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td>
                                                                                              <img alt="select"
                                                                                                     src="images/arrowSingleRight.gif"
                                                                                                     name="newEmailRecipAddButton"
                                                                                                     id="newEmailRecipAddButton"
                                                                                                     title="Press to include an email recipient"
                                                                                                     onclick="newEmailListOpt.transferRight();"
                                                                                                     onkeyup="newEmailListOpt.transferRight();"/>
                                                                                            </td>
                                                                                </tr>
                                                                                <tr>
                                                                                  <td>
                                                                                              <img alt="deselect"
                                                                                                     src="images/arrowSingleLeft.gif"
                                                                                                     name="newEmailRecipDeleteButton"
                                                                                                     id="newEmailRecipDeleteButton"
                                                                                                     title="Press to exclude an email recipient"
                                                                                                     onclick="newEmailListOpt.transferLeft();"
                                                                                                     onkeyup="newEmailListOpt.transferLeft();"/>
                                                                                            </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                          <td>
                                                                              <select size="5"
                                                                                      id="newEmailRecipients"
                                                                                      name="newEmailRecipients"
                                                                                      multiple="multiple">
                                                                              </select>
                                                                              <input type="hidden" name="newEmailRemovedLeft" id="newEmailRemovedLeft"/>
                                                                              <input type="hidden" name="newEmailRemovedRight" id="newEmailRemovedRight"/>
                                                                              <input type="hidden" name="newEmailAddedLeft" id="newEmailAddedLeft"/>
                                                                              <input type="hidden" name="newEmailAddedRight" id="newEmailAddedRight"/>
                                                                              <input type="hidden" name="newEmailNewLeft" id="newEmailNewLeft"/>
                                                                              <input type="hidden" name="newEmailNewRight" id="newEmailNewRight"/>
                                                                          </td>
                                                                        </tr>
                                                                        <tr>
                                                                          <td colspan="3" align="center">
                                                                            <table border="0" cellspacing="3" align="center">
                                                                              <tbody>
                                                                              <tr>
                                                                                <td>
                                                                                  <input type="text"
                                                                                             name="addEmailRecipientField"
                                                                                             size="15"
                                                                                             id="addEmailRecipientField"
                                                                                             title="Add an email recipient that is not listed above"/>
                                                                                </td>
                                                                                <td>
                                                                                  <button
                                                                                             id="addEmailRecipientButton"
                                                                                            name="addEmailRecipientField"
                                                                                         onclick="addEmailAddress(document.getElementById('addEmailRecipientField').value,addEmailAddressCallback);"
                                                                                          type="button">Add Recipient</button>
                                                                                </td>
                                                                              </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                        </tbody>
                                                      </table>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td valign="top" width="100%" align="center">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                                        <caption>Input Parameters</caption>
                                                        <tbody>
                                                        <tr>
                                                          <td>
                                                            <table border="0" align="center">
                                                              <thead>
                                                                <tr>
                                                                  <th>Name</th>
                                                                  <th>Value</th>
                                                                </tr>
                                                              </thead>
                                                              <tbody id="sqlInputParms">
                                                              <tr>
                                                                <td>
                                                                  &nbsp;
                                                                <td>
                                                              </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td valign="top" width="100%" align="center">
                                                      <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                                        <caption>Output Parameters</caption>
                                                        <tbody>
                                                        <tr>
                                                          <td>
                                                            <table border="0" align="center">
                                                              <thead>
                                                                <tr>
                                                                  <th>Name</th>
                                                                  <th><span id="resultFieldLabel">Result Field</span></th>
                                                                  <th><span id="errorValueLabel">Error Value</span></th>
                                                                  <th><span id="emailSubjectLabel">Email Subject</span></th>
                                                                  <th><span id="emailBodyLabel">Email Body</span></th>
                                                                </tr>
                                                              </thead>
                                                              <tbody id="sqlOutputParms">
                                                                <tr>
                                                                  <td>
                                                                    &nbsp;
                                                                  <td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                        </tbody>
                                                      </table>
                                                    </td>
                                                  </tr>
                                                  </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                      </td>
                      </tr>
                    </tbody>
                    </table>
                    <table border="0">
                      <tbody>
                      <tr>
                          <td><button id="submitJobButton" type="button" onclick="addJob()">Add Job</button></td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div id="div_status_panel">
                  <div id="div_status_header" class="accordionTabTitleBar" align="left">Scheduler Status</div>
                  <div id="div_status_context">
                    <br/>
                    <table border="1" cellpadding="0" cellspacing="0">
                      <tbody>
                      <tr>
                          <td valign="top">
                            <div id="runningJobsDiv" style="overflow: auto; height: 375px">
                                <table class="dztable sort01 table-autostripe table-autosort:3 table-stripeclass:alternate table-rowshade-alternate"
                                       border="0"
                                       cellpadding="2"
                                       cellspacing="0">
                                  <caption>Running Jobs</caption>
                                  <thead>
                                    <tr>
                                      <th class="table-sortable:default" width="125px">Name</th>
                                      <th class="table-sortable:default" width="125px">Module</th>
                                      <th class="table-sortable:default" width="350px">Parameters</th>
                                      <th class="table-sortable:default" width="125px">Start</th>
                                      <th class="table-sortable:default" width="125px">Duration</th>
                                    </tr>
                                  </thead>
                                  <tbody id="runningJobsTbody">
                                    <tr><td colspan="5"></td></tr>
                                  </tbody>
                                 </table>
                             </div>
                          </td>
                      </tr>
                      </tbody>
                    </table>
                    <table border="0">
                      <tbody>
                      <tr>
                          <td><button id="runRefreshButton" type="button" onclick="getRunningJobs()">Refresh</button></td>
                      </tr>
                      </tbody>
                    </table>
                    <br/>
                      <div id="schedulerStatusDiv">
                        <table border="0" cellpadding="2" cellspacing="0">
                          <tbody>
                            <tr>
                              <td class="LabelRight">Summary:</td>
                              <td style="text-align: left;" valign="top">
                                <textarea id="schedulerStatsId"
                                          cols="80"
                                          rows="5"
                                          readonly="readonly">Gathering statistics...please stand by!</textarea>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>

                <div id="div_config_panel">
                  <div id="div_config_header" class="accordionTabTitleBar" align="left">Configuration</div>
                  <div id="div_config_context">
                    <br/>
                    <table width="750px" align="center">
                      <tbody>
                        <tr>
                            <td>
                                <div id="setupDiv" style="border-top: 1px solid; margin-top: 6px" align="center">
                                    <div id="div_setup_module_panel">
                                        <div id="div_setup_module_header" class="accordionTabTitleBar" align="left">Modules</div>
                                        <div id="div_setup_module_context">
                                            <br/>
                                            <table border="1" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td valign="top">
                                                    <div style="overflow: auto; height: 200px">
                                                      <table class="dztable sort01 table-autostripe table-autosort:1 table-stripeclass:alternate table-rowshade-alternate" border="0" cellpadding="2" cellspacing="0">
                                                        <caption>Module Configuration</caption>
                                                        <thead>
                                                          <tr>
                                                            <th width="75px">Save</th>
                                                            <th class="table-sortable:default" width="125px">Module Id</th>
                                                            <th class="table-sortable:default" width="350px">Description</th>
                                                            <th class="table-sortable:default" width="75px">Active</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody id="setupModuleTbody">
                                                          <tr>
                                                            <td>
                                                              &nbsp;
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table border="0" cellpadding="2" cellspacing="0" align="center">
                                              <tbody>
                                                <tr>
                                                  <td>
                                                    <button id="setupModuleSaveButton" onclick="onSetupSaveModule();" type="button">Save</button>
                                                  </td>
                                                  <td>
                                                    <button id="setupModuleNewButton" onclick="onSetupNewModule();" type="button">New</button>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="div_setup_function_panel">
                                        <div id="div_setup_function_header" class="accordionTabTitleBar" align="left">Functions</div>
                                        <div id="div_setup_function_context">
                                            <br/>
                                            <table border="1" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td valign="top">
                                                    <div style="overflow: auto; height: 200px">
                                                      <table class="dztable sort01 table-autostripe table-autosort:1 table-stripeclass:alternate table-rowshade-alternate"
                                                            border="0"
                                                            cellpadding="2"
                                                            cellspacing="0">
                                                        <caption>Function Configuration</caption>
                                                        <thead>
                                                          <tr>
                                                            <th width="75px">Select</th>
                                                            <th class="table-sortable:default" width="125px">Function Id</th>
                                                            <th class="table-sortable:default" width="125px">Module Id</th>
                                                            <th class="table-sortable:default" width="350px">Description</th>
                                                            <th class="table-sortable:default" width="75px">Active</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody id="setupFunctionTbody">
                                                          <tr>
                                                            <td colspan="5">
                                                              &nbsp;
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <br/>
                                            <table border="0" cellpadding="2" cellspacing="0" align="center">
                                              <tbody>
                                                <tr>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionModuleComboLabel" for="setupFunctionModuleCombo">Module</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <select id="setupFunctionModuleCombo"
                                                              onchange="javascript: selectedSetupFunctionModuleValue=this[this.selectedIndex].value;">
                                                        <option value="LOADING">Loading...</option>
                                                      </select>
                                                    </div>
                                                  </td>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionFunctionLabel" for="setupFunctionFunction">Function</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="text" id="setupFunctionFunction" size="32" maxlength="32"/>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionDescriptionLabel" for="setupFunctionDescription">Description</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="text" id="setupFunctionDescription" size="40" maxlength="256"/>
                                                    </div>
                                                  </td>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionToolTipLabel" for="setupFunctionToolTip">Tool Tip</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="text" id="setupFunctionToolTip" size="40" maxlength="256"/>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionScheduleLabel" for="setupFunctionSchedule">Default Schedule</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="text" id="setupFunctionSchedule" size="40" maxlength="80"/>
                                                    </div>
                                                  </td>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionActiveLabel" for="setupFunctionActive">Active</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="checkbox" id="setupFunctionActive"/>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionPayloadLabel" for="setupFunctionPayload">Default Payload</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="text" id="setupFunctionPayload" size="40" maxlength="4000"/>
                                                    </div>
                                                  </td>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionForceDefaultLabel" for="setupFunctionForceDefault">Force Default Payload</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <input type="checkbox" id="setupFunctionForceDefault"/>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionJobComboLabel" for="setupFunctionJobCombo">Job Type</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <select id="setupFunctionJobCombo" onchange="onSetupFunctionJobChange();">
                                                        <option value="LOADING">Loading...</option>
                                                      </select>
                                                    </div>
                                                  </td>
                                                  <td class="LabelRight">
                                                    <label id="setupFunctionJMSLabel" for="setupFunctionJMS">JMS Queue</label>
                                                    <label id="setupFunctionJMSAMQLabel" for="setupFunctionJMSAMQ">JMS Queue</label>
                                                  </td>
                                                  <td>
                                                    <div align="left">
                                                      <select id="setupFunctionJMS">
                                                        <option value="LOADING">Loading...</option>
                                                      </select>
                                                      <select id="setupFunctionJMSAMQ">
                                                        <option value="LOADING">Loading...</option>
                                                      </select>
                                                    </div>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <br/>
                                            <table border="0" cellpadding="2" cellspacing="0" align="center">
                                              <tbody>
                                                <tr>
                                                  <td>
                                                    <button id="setupFunctionSaveButton" onclick="onSetupSaveFunction();" type="button">Save</button>
                                                  </td>
                                                  <td>
                                                    <button id="setupFunctionNewButton" onclick="onSetupNewFunction();" type="button">New</button>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="div_setup_email_panel">
                                        <div id="div_setup_email_header" class="accordionTabTitleBar" align="left">Email Recipients</div>
                                        <div id="div_setup_email_context">
                                            <br/>
                                            <table border="1" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td valign="top">
                                                      <table border="0" cellpadding="2" cellspacing="0">
                                                        <caption>Email Configuration</caption>
                                                        <thead>
                                                            <tr>
                                                              <th>Existing</th>
                                                              <th>New</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                          <tr>
                                                            <td>
                                                              <select id="setupEmailList"
                                                                      multiple="multiple"
                                                                      title="Add an email recipient"
                                                                      size="20">
                                                                <option value="LOADING">Loading...</option>
                                                              </select>
                                                            </td>
                                                            <td valign="bottom">
                                                              <table border="0" cellpadding="2" cellspacing="0" >
                                                                <tbody>
                                                                  <tr>
                                                                    <td>
                                                                      <label id="setupEmailAddressLabel" for="setupEmailAddress">New Email address</label>
                                                                    </td>
                                                                  </tr>
                                                                  <tr>
                                                                    <td>
                                                                      <input type="text" id="setupEmailAddress" size="40" maxlength="64"/>
                                                                    </td>
                                                                  </tr>
                                                                </tbody>
                                                              </table>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <button id="setupEmailDeleteButton" onclick="onSetupEmailDelete();" type="button">Delete Selected</button>
                                                            </td>
                                                            <td>
                                                              <button id="setupEmailAddButton"
                                                                      onclick="addEmailAddress(document.getElementById('setupEmailAddress').value,addSetupEmailAddressCallback);"
                                                                      type="button">Add Address</button>
                                                            </td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> <!-- accordion division -->
          </td>
      </tr>
      </tbody>
  </table>

  <table align="center" >
    <tbody>
      <tr><td><button type="button" value="Exit" onclick="doMainMenuAction();">Exit</button></td></tr>
    </tbody>
  </table>
</form>
