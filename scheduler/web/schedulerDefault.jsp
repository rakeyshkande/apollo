<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252" errorPage="/error.do"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>

<html>
  <head>  
    <html:base />
    <title><tiles:getAsString name="title"/></title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/ftd.css">
    <link rel="stylesheet" type="text/css" href="css/calendar.css">
    <link rel="stylesheet" type="text/css" href="css/rico.css">
    <link rel="stylesheet" type="text/css" href="css/dztable.css">
  </head>

  <body bgcolor="#ffffff" 
        onload="init();" 
        onunload="unload();">
    <tiles:importAttribute name="title"/>
    <tiles:insert attribute="header">
      <tiles:put beanName="title" name="title" direct="true"></tiles:put>
    </tiles:insert>
    
    <div align="center">
        <html:errors/>
    </div>
    
    <tiles:insert attribute="pagecontent" />
    
    <tiles:insert attribute="footer" />
  </body>
</html>