package com.ftd.scheduler.common;

import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.j2ee.NotificationUtil;
import com.ftd.osp.utilities.vo.NotificationVO;
import com.ftd.scheduler.common.resources.IResourceProvider;
import com.ftd.scheduler.common.resources.J2EEResourceProvider;
import com.ftd.scheduler.common.resources.TestResourceProviderImpl;

import java.io.IOException;

import javax.mail.Session;


public class SchedulerUtils {
    private static SchedulerUtils INSTANCE = null;
    private static final Object LOCK = new Object();
    
    private SchedulerUtils() {
    }
    
    public static SchedulerUtils getInstance() {
        if(INSTANCE==null) {
            synchronized(LOCK) {
                if( INSTANCE==null ) {
                    INSTANCE = new SchedulerUtils();
                }
            }
        }
        
        return INSTANCE;
    }
    
    public static void sendPlainTextEmail(Session session, EmailVO emailVO) throws Exception
    {
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setMessageFromAddress(emailVO.getSender());
        notificationVO.setMessageTOAddress(emailVO.getRecipient());
        notificationVO.setMessageSubject(emailVO.getSubject());
        notificationVO.setMessageContent(emailVO.getContent());
        notificationVO.setMessageMimeType("text/plain; charset=us-ascii");
        NotificationUtil.getInstance().notify(session, notificationVO);
    }
    
    public static String getHostName() {
        String retval;
        
        try
        {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();    
            retval = localMachine.getHostName();
        }
        catch(java.net.UnknownHostException uhe)
        {
            retval = "unknown host";
        }
        
        return retval;
    }
    
    public IResourceProvider getResourceProvider() {
        IResourceProvider retval;
        if( this.getClass().getClassLoader().getResource(SchedulerConstants.TEST_CONFIG_FILE)!=null ) {
            retval = new TestResourceProviderImpl();
        } else {
            retval = new J2EEResourceProvider();
        }
        
        return retval;
    }
}
