package com.ftd.scheduler.common;

public class SchedulerConstants {
    public static final String REQUEST_TYPE = "AJAX_REQUEST_TYPE";
    public static final String REQUEST_TYPE_GET_RUNNING_JOBS = "GET_RUNNING_JOBS";
    public static final String REQUEST_TYPE_GET_SCHEDULED_JOBS = "GET_SCHEDULED_JOBS";
    public static final String REQUEST_TYPE_GET_DEFINED_JOBS = "GET_DEFINED_JOBS";
    public static final String REQUEST_TYPE_SCHEDULER_STATUS = "SCHEDULER_STATUS";
    public static final String REQUEST_TYPE_PIPELINE_DETAIL = "PIPELINE_DETAIL";
    public static final String REQUEST_TYPE_RUN_ONCE = "RUN_ONCE";
    public static final String REQUEST_TYPE_SAVE_PAYLOAD = "SAVE_PAYLOAD";
    public static final String REQUEST_TYPE_UPDATE_PIPELINE = "UPDATE_PIPELINE";
    public static final String REQUEST_TYPE_REMOVE_TRIGGER = "REMOVE_TRIGGER";
    public static final String REQUEST_TYPE_REMOVE_JOB = "REMOVE_JOB";
    public static final String REQUEST_TYPE_GET_GROUP_PIPELINES = "GET_GROUP_PIPELINES";
    public static final String REQUEST_TYPE_GET_ALL_PIPELINE_DETAILS = "GET_ALL_PIPELINE_DETAILS";
    public static final String REQUEST_TYPE_GET_MODULES = "GET_MODULES";
    public static final String REQUEST_TYPE_ADD_JOB = "ADD_JOB";
    public static final String REQUEST_TYPE_RUN_JOBS = "RUN_JOBS";
    public static final String REQUEST_TYPE_SCHEDULE_JOB = "SCHEDULE_JOB";
    public static final String REQUEST_TYPE_PAUSE_JOB = "PAUSE_JOB";
    public static final String REQUEST_TYPE_RESUME_JOB = "RESUME_JOB";
    public static final String REQUEST_TYPE_PAUSE_MODULE = "PAUSE_MODULE";
    public static final String REQUEST_TYPE_RESUME_MODULE = "RESUME_MODULE";
    public static final String REQUEST_TYPE_GET_SQL_STATEMENTS = "GET_SQL_STATEMENTS";
    public static final String REQUEST_TYPE_GET_EMAIL_ADDRESSES = "GET_EMAIL_ADDRESSES";
    public static final String REQUEST_TYPE_ADD_EMAIL_ADDRESS = "ADD_EMAIL_ADDRESS";
    public static final String REQUEST_TYPE_DELETE_EMAIL_ADDRESSES = "DELETE_EMAIL_ADDRESSES";
    public static final String REQUEST_TYPE_GET_JOB_DETAIL = "GET_JOB_DETAIL";
    public static final String REQUEST_TYPE_UPDATE_MODULES = "UPDATE_MODULES";
    public static final String REQUEST_TYPE_GET_APOLLO_JOBS = "GET_APOLLO_JOBS";
    public static final String REQUEST_TYPE_GET_JMS_QUEUES = "GET_JMS_QUEUES";
    public static final String REQUEST_TYPE_GET_JMS_AMQ_QUEUES = "GET_JMS_AMQ_QUEUES";
    
    public static final String PARAMETER_GROUP = "GROUP";    
    public static final String PARAMETER_GROUPS = "GROUPS";
    public static final String PARAMETER_PIPELINE = "PIPELINE";
    public static final String PARAMETER_PIPELINES = "PIPELINES";
    public static final String PARAMETER_PAYLOAD = "PAYLOAD";
    public static final String PARAMETER_JOB_CLASS = "JOB_CLASS";
    public static final String PARAMETER_JOB_NAME = "JOB_NAME";
    public static final String PARAMETER_TRIGGER_NAME = "TRIGGER_NAME";    
//    public static final String PARAMETER_GROUP_ID = "GROUP_ID";
    public static final String PARAMETER_DESCRIPTION = "DESCRIPTION";
    public static final String PARAMETER_ACTIVE_FLAG = "ACTIVE_FLAG";
    public static final String PARAMETER_PIPELINE_ID = "PIPELINE_ID";
    public static final String PARAMETER_SCHEDULE = "SCHEDULE";
    public static final String PARAMETER_SCHEDULE_JOB = "SCHEDULE_JOB";
    public static final String PARAMETER_SCHEDULE_TYPE = "SCHEDULE_TYPE";
    public static final String PARAMETER_DEFAULT_SCHEDULE = "DEFAULT_SCHEDULE";
    public static final String PARAMETER_DEFAULT_PAYLOAD = "DEFAULT_PAYLOAD";
    public static final String PARAMETER_FORCE_DEFAULT_PAYLOAD = "FORCE_DEFAULT_PAYLOAD";
    public static final String PARAMETER_TOOL_TIP = "TOOL_TIP";
    public static final String PARAMETER_MODULE = "MODULE";
    public static final String PARAMETER_MODULES = "MODULES";
    public static final String PARAMETER_MODULE_ID = "MODULE_ID";
    public static final String PARAMETER_JMS_QUEUE_NAME = "JMS_QUEUE_NAME";
    public static final String PARAMETER_CLASS_CODE = "CLASS_CODE";
    public static final String PARAMETER_CLASS_PACKAGE = "CLASS_PACKAGE";
    public static final String PARAMETER_EMAIL_ADDRESSES = "EMAIL_ADDRESSES";
    public static final String PARAMETER_ADDR = "ADDR";
    public static final String PARAMETER_PARAMETERS = "parameters";
    public static final String PARAMETER_IN_PARAMETERS = "in-parameters";
    public static final String PARAMETER_IN_PARAMETER = "in-parameter";
    public static final String PARAMETER_OUT_PARAMETERS = "out-parameters";
    public static final String PARAMETER_OUT_PARAMETER = "out-parameter";
    public static final String PARAMETER_OUT_LABEL = "outParmLabel";
    public static final String PARAMETER_NAME = "name";
    public static final String PARAMETER_VALUE = "value";
    public static final String PARAMETER_ID = "id";
    public static final String PARAMETER_INDEX = "idx";
    public static final String PARAMETER_PARM_TYPE = "parmType";
    public static final String PARAMETER_RESULT_FIELD = "resultField";
    public static final String PARAMETER_ERROR_FIELD = "errorText";
    public static final String PARAMETER_EMAIL_SUBJECT = "emailSubject";
    public static final String PARAMETER_EMAIL_BODY = "emailBody";
    public static final String PARAMETER_SETUP_MODULE_SAVE_CHECKBOX = "setup_module_save_checkbox";
    public static final String PARAMETER_SETUP_MODULE_ID = "setup_module_id";
    public static final String PARAMETER_SETUP_MODULE_DESCRIPTION = "setup_module_desc";
    public static final String PARAMETER_SETUP_MODULE_ACTIVE_CHECKBOX = "setup_module_active_checkbox";
    public static final String PARAMETER_JOB_TYPES = "jobTypes";
    public static final String PARAMETER_JOB_TYPE = "jobType";
    public static final String PARAMETER_JMS_QUEUES = "jmsQueues";
    public static final String PARAMETER_JMS_QUEUE = "jmsQueue";
    public static final String PARAMETER_SCHEMA = "schema";
    public static final String PARAMETER_SCHEMA_OJMS = "OJMS";
    public static final String PARAMETER_QUEUE_NAME = "queueName";
    
    
    /** PARAMETERS specific to the SQLJob class **/
    public static final String PARAMETER_SQL_STATEMENT_ID = "SQL_STATEMENT_ID";
    public static final String PARAMETER_SQL_STATEMENT_NAME = "SQL_STATEMENT_NAME";
    public static final String PARAMETER_SQL_EMAIL_ON_ERROR = "SQL_EMAIL_ON_ERROR";
    public static final String PARAMETER_SQL_EMAIL_ON_SUCCESS = "SQL_EMAIL_ON_SUCCESS";
    public static final String PARAMETER_SQL_DEFAULT_SUBJECT = "SQL_DEFAULT_EMAIL_SUBJECT";
    public static final String PARAMETER_SQL_DEFAULT_BODY = "SQL_DEFAULT_EMAIL_BODY";
    public static final String PARAMETER_SQL_EMAIL_ADDRESSES = "SQL_EMAIL_ADDRESSES";
    public static final String PARAMETER_SQL_PARAMETERS = "SQL_PARAMETERS";
    
    public static final String DEFINED_JOB_NAME = "definedJobName";    
    public static final String DEFINED_GROUP = "definedGroup";  
    public static final String DEFINED_PAYLOAD = "definedPayload";
    public static final String DEFINED_PIPELINE = "definedPipeline";    
    public static final String DEFINED_JOB_CLASS = "definedJobClass";    
    public static final String DEFINED_TRIGGER = "definedTrigger";   
    public static final String DEFINED_SCHEDULE = "definedSchedule";  
    public static final String DEFINED_MODULE = "definedModule";   
    public static final String DEFINED_JMS_QUEUE_NAME = "definedJMSQueueName";  
    public static final String DEFINED_RADIO = "definedRadio";
    public static final String DEFINED_JOB_PARMS = "definedJobParms";
    public static final String DEFINED_CLASS_CODE = "definedClassCode";
    public static final String DEFINED_JOBS = "definedJobs";
    public static final String DEFINED_JOB = "definedJob";
    
    public static final String SCHEDULED_JOBS = "jobs";    
    public static final String SCHEDULED_JOB = "job";
    public static final String SCHEDULED_JOB_NAME = "name";
    public static final String SCHEDULED_GROUP = "group";
    public static final String SCHEDULED_MODULE = "module";
    public static final String SCHEDULED_TRIGGER = "trigger";
    public static final String SCHEDULED_PAYLOAD = "payload";
    public static final String SCHEDULED_PIPELINE = "pipeline";
    public static final String SCHEDULED_SCHEDULE = "schedule";
    public static final String SCHEDULED_JOB_PARMS = "parms";
    public static final String SCHEDULED_CLASS_CODE = "class_code";
    public static final String SCHEDULED_PREVIOUS_RUN = "previous";
    public static final String SCHEDULED_NEXT_RUN = "next";
    public static final String SCHEDULED_RADIO = "scheduled_radio";
    
    public static final String RUNNING_JOBS = "jobs";
    public static final String RUNNING_JOB = "job";
    public static final String RUNNING_JOB_NAME = "name";
    public static final String RUNNING_MODULE = "module";
    public static final String RUNNING_JOB_PARMS = "parms";
    public static final String RUNNING_START_TIME = "start";
    public static final String RUNNING_DURATION = "duration";
    
    public static final String RESULTS_JOB_STARTED = "Job has been started.";
    public static final String RESULTS_JOB_START_FAILED = "Job failed to start.";
    public static final String RESULTS_TRIGGER_REMOVAL_SUCCESS = "Trigger successfully removed.";
    public static final String RESULTS_TRIGGER_REMOVAL_FAILED = "Trigger removal failed.";
    public static final String RESULTS_JOB_REMOVAL_SUCCESS = "Job and any associatied triggers have been successfully removed.";
    public static final String RESULTS_JOB_REMOVAL_FAILED = "Job removal failed.";
    public static final String RESULTS_SUCCESS = "Request was successful.";
    public static final String RESULTS_FAILURE = "Request failed!";
    public static final String RESULTS_JOB_SCHEDULE_SUCCESS = "Job has been scheduled.";
    public static final String RESULTS_JOB_SCHEDULE_FAILED = "Job failed to schedule.";
    public static final String RESULTS_PAUSE_JOB_SUCCESS = "Job have been paused.";
    public static final String RESULTS_PAUSE_JOB_FAILED = "Job pause failed.";
    public static final String RESULTS_RESUME_JOB_SUCCESS = "Job schedule have been resumed.";
    public static final String RESULTS_RESUME_JOB_FAILED = "Job resume failed.";
    public static final String RESULTS_PAUSE_MODULE_SUCCESS = "Module jobs have been paused.";
    public static final String RESULTS_PAUSE_MODULE_FAILED = "Module pause failed.";
    public static final String RESULTS_RESUME_MODULE_SUCCESS = "Module schedules have been resumed.";
    public static final String RESULTS_RESUME_MODULE_FAILED = "Module resume failed.";
    public static final String RESULTS_UPDATE_PIPELINE_SUCCESS = "Your changes have been saved.";
    
    public static final String NO_SELECTED_JOB = "no_selected_job";    
    public static final String NO_SELECTED_TRIGGER = "no_selected_trigger";
    
    public static final String NEW_PAYLOAD_ID = "newPayload";
    
    public static final String SELECTED_SCHEDULE_CRON = "scheduleCron";    
    public static final String SELECTED_SCHEDULE_INTERVAL = "scheduleInterval";
    public static final String SELECTED_SCHEDULE_RUN_ONCE = "scheduleOnce";
    public static final String SQL_JOB_CLASS_CODE = "SQLJob";
    public static final String JMS_JOB_CLASS_CODE = "SimpleJmsJob";
    public static final String ACTIVEMQ_JMS_JOB_CLASS_CODE = "ActiveMQJmsJob";
    public static final String SELECT_A_RECIPIENT = "--select a recipient--";
    public static final String SELECT_FROM_LIST = "SELECT_FROM_LIST";
    public static final String SQL_RETURN_DUPLICATE = "DUPLICATE";
    public static final String SQL_STATEMENTS_FILE = "sql-statements.xml";
    public static final String EDIT_MODE_TITLE_CREATE = "Create Job";
    public static final String EDIT_MODE_TITLE_EDIT = "Edit Job";
    public static final String SCHEDULED_FIRST_HEADER_ID = "firstScheduledHeaderId";
    public static final String DEFINED_FIRST_HEADER_ID = "firstDefinedHeaderId";
    public static final String RUNNING_FIRST_HEADER_ID = "firstRunningHeaderId";
    
    public static final String SETUP_FUNCT_RADIO = "setupFunctionRadio";    
    public static final String SETUP_FUNCT_PIPELINE_ID = "setupFunctionPipelineId";    
    public static final String SETUP_FUNCT_MODULE_ID = "setupFunctionModuleId";     
    public static final String SETUP_FUNCT_DESCRIPTION = "setupFunctionDesc";  
    public static final String SETUP_FUNCT_ACTIVE = "setupFunctionActiveFlag";
    public static final String SETUP_FUNCT_TOOL_TIP = "setupFunctionToolTip";
    public static final String SETUP_FUNCT_DEFAULT_SCHEDULE = "setupFunctionDefaultSchedule";
    public static final String SETUP_FUNCT_DEFAULT_PAYLOAD = "setupFunctionDefaultPayload";
    public static final String SETUP_FUNCT_QUEUE_NAME = "setupFunctionQueueName";
    public static final String SETUP_FUNCT_CLASS_CODE = "setupFunctionClassCode";
    public static final String SETUP_FUNCT_FORCE_DEFAULT_PAYLOAD = "setupFunctionForceDefaultPayload";
    
    /*********************************************************************************************
    // Security parameters
    *********************************************************************************************/
    public static final String CONTEXT    = "context";
    public static final String SEC_TOKEN  = "securitytoken";
    public static final String CONS_MAIN_MENU_URL   = "security.main.menu";
    public static final String ADMIN_ACTION = "adminAction";
    public static final String SECURITY_IS_ON  = "SECURITY_IS_ON";
    public static final String SECURE_CONFIG_CONTEXT = "SCHEDULER";
    

    public static final String TEST_CONFIG_FILE = "scheduler_test_config.xml";
    public static final String DISPLAY_JOB_PAUSED = "Paused";
    
    public SchedulerConstants() {
    }
}
