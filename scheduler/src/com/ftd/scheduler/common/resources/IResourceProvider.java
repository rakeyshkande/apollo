package com.ftd.scheduler.common.resources;

import java.sql.Connection;

import javax.mail.Session;


public interface IResourceProvider {
    
    public Connection getDatabaseConnection(String datasource) 
        throws Exception;
        
    public Session getEmailSession() 
        throws Exception;
}
