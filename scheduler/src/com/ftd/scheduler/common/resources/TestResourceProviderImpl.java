package com.ftd.scheduler.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;

import com.ftd.scheduler.common.SchedulerConstants;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

import java.util.Properties;

import javax.mail.Session;


public class TestResourceProviderImpl implements IResourceProvider {
    
    public TestResourceProviderImpl() {
    }
    
    public Connection getDatabaseConnection(String datasource) throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String DB_DRIVER_CLASS = config.getProperty(SchedulerConstants.TEST_CONFIG_FILE,datasource+".db.driver.class");
        String DB_USER_NAME = config.getProperty(SchedulerConstants.TEST_CONFIG_FILE,datasource+".db.user.name");
        String DB_USER_CREDENTIALS = config.getProperty(SchedulerConstants.TEST_CONFIG_FILE,datasource+".db.user.credentials");
        String DB_JDBC_URL = config.getProperty(SchedulerConstants.TEST_CONFIG_FILE,datasource+".db.jdbc.url");            
        
        DriverManager.registerDriver((Driver)(Class.forName(DB_DRIVER_CLASS).newInstance()));
            
        return DriverManager.getConnection(DB_JDBC_URL, DB_USER_NAME, DB_USER_CREDENTIALS);
    }


    public Session getEmailSession() throws Exception {
        ConfigurationUtil config = ConfigurationUtil.getInstance();
        String mailHost;
        try {
            mailHost = config.getProperty(SchedulerConstants.TEST_CONFIG_FILE,"email.server");    
        } catch (Exception e) {
            mailHost = "mail4.ftdi.com";
        }
        Properties properties = new Properties();
        properties.setProperty("mail.transport.protocol","smtp");
        properties.setProperty("mail.host",mailHost);
        Session session = Session.getInstance(properties);
        return session;
    }
}
