package com.ftd.scheduler.common.resources;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import javax.mail.Session;

import javax.naming.InitialContext;


public class J2EEResourceProvider implements IResourceProvider {
    private static final Logger logger = new Logger("com.ftd.scheduler.common.resources.J2EEResourceProvider");
    private static final String SESSION_JNDI_NAME = "mail/scheduler_session";
    
    public J2EEResourceProvider() {
    }
    
  /**
   * Obtain connectivity with the database
   * @return Connection - db connection
   * @throws Exception
   */

    public Connection getDatabaseConnection(String datasource)
            throws Exception
    {
        logger.debug("Entering 'getDatabaseConnection' with datasource "+datasource);
        
        String mappedDatasource = ConfigurationUtil.getInstance().getProperty("scheduler_config.xml",datasource);
        logger.debug("Datasource "+datasource+" maps to "+mappedDatasource);
        
        return DataSourceUtil.getInstance().getConnection(mappedDatasource);
    }

    public Session getEmailSession() throws Exception {
        InitialContext initialContext = InitialContextUtil.getInstance().getInitialContext("LOCALHOST");
        Session session = (Session) initialContext.lookup(SESSION_JNDI_NAME);
        
        return session;
    }
}
