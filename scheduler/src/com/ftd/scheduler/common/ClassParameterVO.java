package com.ftd.scheduler.common;

public class ClassParameterVO extends ParametersVO {
    private String classCode;
    private boolean required;
    
    public ClassParameterVO() {
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isRequired() {
        return required;
    }
}
