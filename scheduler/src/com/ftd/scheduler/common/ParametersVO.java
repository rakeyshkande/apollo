package com.ftd.scheduler.common;

public class ParametersVO {
    private String parameterCode;
    private boolean manditory;
    
    public ParametersVO() {
    }

    public void setParameterCode(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    public String getParameterCode() {
        return parameterCode;
    }

    public void setManditory(boolean manditory) {
        this.manditory = manditory;
    }

    public boolean isManditory() {
        return manditory;
    }
}
