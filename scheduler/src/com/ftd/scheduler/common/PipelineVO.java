package com.ftd.scheduler.common;

import org.apache.commons.lang.BooleanUtils;

public class PipelineVO {
    private String moduleId;
    private String pipelineId;
    private String description;
    private String defaultSchedule;
    private String defaultPayload;
    private boolean activeFlag;
    private boolean forceDefaultPayloadFlag;
    private String classCode;
    private String queueName;
    private String toolTip;
    
    public PipelineVO() {
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setPipelineId(String pipelineId) {
        this.pipelineId = pipelineId;
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDefaultSchedule(String defaultSchedule) {
        this.defaultSchedule = defaultSchedule;
    }

    public String getDefaultSchedule() {
        return defaultSchedule;
    }

    public void setDefaultPayload(String defaultPayload) {
        this.defaultPayload = defaultPayload;
    }

    public String getDefaultPayload() {
        return defaultPayload;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setForceDefaultPayloadFlag(boolean forceDefaultPayloadFlag) {
        this.forceDefaultPayloadFlag = forceDefaultPayloadFlag;
    }

    public boolean isForceDefaultPayloadFlag() {
        return forceDefaultPayloadFlag;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public String getToolTip() {
        return toolTip;
    }
}
