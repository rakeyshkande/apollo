package com.ftd.scheduler.common;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

public class ClassVO {
    private String classCode;
    private String className;
    private HashMap<String,ClassParameterVO> 
            parameters = new HashMap<String,ClassParameterVO>();
    
    public ClassVO() {
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    /**
     * Retrieve a value from the property array
     * @param key to value
     * @return found value
     */
    public ClassParameterVO getParameter(String key) {
        ClassParameterVO retVal = null;
        
        if( StringUtils.isNotBlank(key) ) {
            retVal = this.parameters.get(key);
        }
        
        return retVal;
    }

    /**
     * Set a property value.  If a property value is null, the value will be 
     * removed from the map.
     * @param key to new value
     * @param value to set
     */
    public void setParameter(String key, ClassParameterVO value) {
        if( StringUtils.isNotBlank(key) ) {
            if( value!=null ) {
                parameters.put(key,value);
            } else if( parameters.containsKey(key) ) {
                parameters.remove(key);
            }
        }
    }

    public HashMap<String, ClassParameterVO> getParameters() {
        return parameters;
    }
}
