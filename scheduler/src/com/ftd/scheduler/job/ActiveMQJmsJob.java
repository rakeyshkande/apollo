package com.ftd.scheduler.job;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.scheduler.common.SchedulerConstants;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;

/**
 * Execute a job for posting a message to the active MQ JMS queue.
 */
public class ActiveMQJmsJob implements Job
{
    private static final Logger LOGGER = new Logger("com.ftd.scheduler.job.ActiveMQJmsJob");

    public ActiveMQJmsJob()
    {
    }

    public void execute(JobExecutionContext jobExecutionContext)
    {
        try
        {
            JobDetail detail = jobExecutionContext.getJobDetail();
            JobDataMap map = detail.getJobDataMap();

            String payload = map.getString(SchedulerConstants.PARAMETER_PAYLOAD);

            String correlationId = payload;
            if (correlationId != null && correlationId.length() > 127)
            {
                correlationId = payload.substring(0, 127);
            }
            String pipeline = map.getString(SchedulerConstants.PARAMETER_PIPELINE);
            String queueName = map.getString(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME);

            LOGGER.debug("********** Firing job " + detail.getFullName() + " to pipeline " + pipeline + " with payload " + payload + " to queue " + 
                         queueName + ". **********");

            sendJMSMessage(payload,correlationId,queueName);
            
           LOGGER.debug("Successfully posted the message to queue " + queueName);

        } catch (Exception e)
        {
            LOGGER.error(e);
        }
    }
    
    protected void sendJMSMessage(String payload, String correlationId, String jmsQueueName) throws Exception
    {
        MessageToken token = new MessageToken();
        Context context = new InitialContext();
        token.setMessage(payload);
        token.setStatus(jmsQueueName);
        token.setJMSCorrelationID(correlationId);
        Dispatcher dispatcher = Dispatcher.getInstance();
        dispatcher.dispatchTextMessage(context, token);
    }

}
