package com.ftd.scheduler.job;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.scheduler.common.SchedulerConstants;
import com.ftd.scheduler.common.SchedulerUtils;
import com.ftd.scheduler.common.resources.IResourceProvider;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;

import javax.mail.Session;

import org.apache.commons.lang.StringUtils;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class SQLJob  implements Job {
    private static final Logger LOGGER = new Logger("com.ftd.scheduler.job.SQLJob");
    private static final IResourceProvider resourceProvider = SchedulerUtils.getInstance().getResourceProvider();
    
    public SQLJob() {
    }
    
    public void execute(JobExecutionContext jobExecutionContext) {
        Connection conn = null;
        try{
            JobDetail detail = jobExecutionContext.getJobDetail();
            JobDataMap map = detail.getJobDataMap();
            
            if( LOGGER.isDebugEnabled() ) {
                String[] keys = map.getKeys();
                
                for(int idx=0; idx<keys.length; idx++) {
                    LOGGER.debug("Parameter name: "+keys[idx]+" value: "+map.getString(keys[idx]));
                    //System.out.println("Parameter name: "+keys[idx]+" value: "+map.getString(keys[idx]));
                }
            }
            
            String jobName = detail.getName();
            String sqlId = map.getString(SchedulerConstants.PARAMETER_SQL_STATEMENT_ID);
            String strValue = map.getString(SchedulerConstants.PARAMETER_SQL_PARAMETERS);
            Document sqlParmsDoc = JAXPUtil.parseDocument(strValue);
            String emailSubject = null;
            String emailBody = null;
            boolean bEmailOnSuccess = StringUtils.equals("Y",map.getString(SchedulerConstants.PARAMETER_SQL_EMAIL_ON_SUCCESS));
            boolean bEmailOnError = StringUtils.equals("Y",map.getString(SchedulerConstants.PARAMETER_SQL_EMAIL_ON_ERROR));
            strValue = map.getString(SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES);
            Document emailAddrsDoc = JAXPUtil.parseDocument(strValue);
            emailSubject = map.getString(SchedulerConstants.PARAMETER_SQL_DEFAULT_SUBJECT);
            emailBody = map.getString(SchedulerConstants.PARAMETER_SQL_DEFAULT_BODY);
            
            DataRequest dataRequest = new DataRequest();
            HashMap inputParams = new HashMap();
            
            //Get the input and output parms
            NodeList inputParms = JAXPUtil.selectNodes(sqlParmsDoc,"//"+SchedulerConstants.PARAMETER_IN_PARAMETER);
            
            for( int idx = 0; idx < inputParms.getLength(); idx++ ) {
                Element parm = (Element)inputParms.item(idx);
                String parmType = parm.getAttribute(SchedulerConstants.PARAMETER_PARM_TYPE);
                String parmValue = parm.getAttribute(SchedulerConstants.PARAMETER_VALUE);
                String parmName = parm.getAttribute(SchedulerConstants.PARAMETER_NAME);
                Object objParm = null;
                
                if( StringUtils.equals("java.lang.String",parmType) ) {
                    objParm = parmValue;
                } else if( StringUtils.equals(parmType,"java.math.BigDecimal") ) {
                    objParm = new BigDecimal(parmValue);
                } else if( StringUtils.equals(parmType,"long") ) {
                    objParm = new Long(parmValue);
                } else if(StringUtils.equals(parmType,"int") ) {
                    objParm = new Integer(parmValue);
                } else if( StringUtils.equals(parmType,"short") ) {
                    objParm = new Short(parmValue);
                } else if( StringUtils.equals(parmType,"double") ) {
                    objParm = new Double(parmValue);
                } else if( StringUtils.equals(parmType,"float") ) {
                    objParm = new Float(parmValue);
                } else if( StringUtils.equals(parmType,"boolean") ) {
                    objParm = new Boolean(parmValue);
                } else {
                    throw new Exception("Unsupport input parameter of type "+parmType+" has been configured in job "+jobName); 
                }
                
                inputParams.put(parmName,objParm);
            }
            
            dataRequest.setInputParams(inputParams);
            conn = resourceProvider.getDatabaseConnection("CLEANDS");
            dataRequest.setConnection(conn);
            dataRequest.setStatementID(sqlId);
            
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
            Map outputs = null;
            boolean bSQLException = false;
            boolean bReportedFailure = false;
            
            try {
                outputs = (Map) dataAccessUtil.execute(dataRequest);
            } catch (SQLException sqle) {
                bSQLException = true;    
                emailBody = "An unexpected error has occurred while scheduler was running "+sqlId+"\r\n"+sqle.getMessage();
                LOGGER.error("An unexpected error has occurred while scheduler was running "+sqlId,sqle);
            }
            
            if( !bSQLException ) {
                //Check for a results field
                Element element = JAXPUtil.selectSingleNode(sqlParmsDoc,"//out-parameter[@resultField = \"Y\"]");
                if( element!=null ) {
                    String failTestValue = element.getAttribute(SchedulerConstants.PARAMETER_VALUE);
                    if( StringUtils.isNotBlank(failTestValue) ) {
                        String result = String.valueOf(outputs.get(element.getAttribute(SchedulerConstants.PARAMETER_NAME)));
                        
                        if( StringUtils.equals(failTestValue,result) ) {
                            bReportedFailure = true;
                        }
                    }
                }
            }
            
            if( bEmailOnSuccess || ((bSQLException || bReportedFailure) && bEmailOnError) ) {
                Element element = JAXPUtil.selectSingleNode(sqlParmsDoc,"//out-parameter[@emailSubject = \"Y\"]");
                if( element!=null ) {
                    strValue = (String) outputs.get(element.getAttribute(SchedulerConstants.PARAMETER_NAME));
                    if( StringUtils.isNotBlank(strValue)) {
                        emailSubject = strValue;
                    }
                }
                    
                if( !bSQLException ) {
                    element = JAXPUtil.selectSingleNode(sqlParmsDoc,"//out-parameter[@emailBody = \"Y\"]");
                    if( element!=null ) {
                        strValue = (String) outputs.get(element.getAttribute(SchedulerConstants.PARAMETER_NAME));
                        if( StringUtils.isNotBlank(strValue)) {
                            emailBody = strValue;
                        }
                    }
                }
                
                if( StringUtils.isBlank(emailSubject) ) {
                    if( bEmailOnSuccess ) {
                        emailSubject = "A message from the Apollo scheduler";
                    } else {
                        emailSubject = "An error message from the scheduled on "+SchedulerUtils.getHostName();
                    }
                }
                
                if( StringUtils.isBlank(emailBody) ) {
                    if( bEmailOnSuccess ) {
                        emailBody = "SQL statement "+sqlId+" has successfully completed";
                    } else {
                        emailBody = "An unexpected error has occurred while scheduler was running "+sqlId;
                    }
                }
                
                NodeList emailRecips = JAXPUtil.selectNodes(emailAddrsDoc,"//"+SchedulerConstants.PARAMETER_ADDR);
                Session emailSession = resourceProvider.getEmailSession();
                String emailFrom = SchedulerUtils.getHostName()+"@ftdi.com";
                String recip = null;
                
                for( int idx = 0; idx<emailRecips.getLength(); idx++ ) {
                    try {
                    recip = JAXPUtil.getTextValue(emailRecips.item(idx));
                    EmailVO emailVO = new EmailVO();
                    emailVO.setRecipient(recip);
                    emailVO.setSender(emailFrom);
                    emailVO.setSubject(emailSubject);
                    emailVO.setContent(emailBody);
                    SchedulerUtils.sendPlainTextEmail(emailSession,emailVO);  
                    } catch (Exception e) {
                        LOGGER.error("Failed to send email to "+recip+".  Will continue...");
                    }
                }
            }
            
        } catch (Throwable t) {
            LOGGER.error(t);
        } finally {
            if( conn!=null ) {
                try {
                    if( !conn.isClosed() ) {
                        conn.close();
                    }
                } catch (SQLException sqle2) {
                    LOGGER.error("Error closing database conncetion.",sqle2);
                }
            }
        }
    }
}
