package com.ftd.scheduler.job;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.scheduler.common.SchedulerConstants;
import com.ftd.scheduler.common.SchedulerUtils;
import com.ftd.scheduler.common.resources.IResourceProvider;
import com.ftd.scheduler.dao.SchedulerDAO;

import java.sql.Connection;
import java.sql.SQLException;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;


public class SimpleJmsJob implements Job {
    private static final Logger LOGGER = new Logger("com.ftd.scheduler.job.SimpleJmsJob");
    private SchedulerDAO schedulerDAO = new SchedulerDAO();
    private static final IResourceProvider resourceProvider = SchedulerUtils.getInstance().getResourceProvider();
    
    public SimpleJmsJob() {
    }
        
    public void execute(JobExecutionContext jobExecutionContext) {
        Connection conn = null;
        try {
            JobDetail detail = jobExecutionContext.getJobDetail();
            JobDataMap map = detail.getJobDataMap();
            
            String payload = map.getString(SchedulerConstants.PARAMETER_PAYLOAD);
            
            String correlationId = payload;
            if (correlationId != null && correlationId.length() > 127) {
                correlationId = payload.substring(0,127);
            }
            String pipeline = map.getString(SchedulerConstants.PARAMETER_PIPELINE);
            String queueName = map.getString(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME);
            
            LOGGER.debug("********** Firing job "+detail.getFullName()+" to pipeline "+pipeline+" with payload "+payload+" to queue "+queueName+". **********");
            
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");

            schedulerDAO.postAMessage(conn,queueName,correlationId,payload,0);
            
            LOGGER.debug("Successfully posted the message to queue "+queueName);
            
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    LOGGER.error(sqle);
                }
            }
        }
    }
}
