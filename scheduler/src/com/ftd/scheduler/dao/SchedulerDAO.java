package com.ftd.scheduler.dao;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;

import com.ftd.scheduler.common.ClassParameterVO;
import com.ftd.scheduler.common.ClassVO;
import com.ftd.scheduler.common.PipelineVO;
import com.ftd.scheduler.common.SchedulerConstants;

import java.sql.Connection;

import java.sql.DatabaseMetaData;

import java.sql.ResultSet;

import java.util.HashMap;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SchedulerDAO {
    public static final Logger logger = new Logger("com.ftd.scheduler.dao.SchedulerDAO");
    public SchedulerDAO() {
    }
    
    public Document getPipelineDetailXML(Connection conn, String pipelineId, String groupId) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement("pipeline");
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PIPELINE_ID",pipelineId);
        inputParams.put("IN_GROUP_ID",groupId);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_PIPELINE_DETAIL");
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        if(rs.next()) {
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_PIPELINE_ID,rs.getString("PIPELINE_ID")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_MODULE_ID,rs.getString("GROUP_ID")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_DESCRIPTION,rs.getString("DESCRIPTION")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_TOOL_TIP,rs.getString("TOOL_TIP")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE,rs.getString("DEFAULT_SCHEDULE")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD,rs.getString("DEFAULT_PAYLOAD")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_JMS_QUEUE_NAME,rs.getString("QUEUE_NAME")));
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_CLASS_CODE,rs.getString("CLASS_CODE")));
            String strTmp = rs.getString("FORCE_DEFAULT_PAYLOAD");
            if( StringUtils.isBlank(strTmp) ) {
                strTmp = "N";
            }
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD,strTmp));
        }
        
        return doc;
    }
    
    public Document getAllPipelineDetailsXML(Connection conn) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_PIPELINES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_PIPELINE_ID",null);
        inputParams.put("IN_GROUP_ID",null);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_PIPELINE_DETAIL");
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            Element pipeline = doc.createElement(SchedulerConstants.PARAMETER_PIPELINE);
            root.appendChild(pipeline);
            pipeline.setAttribute(SchedulerConstants.PARAMETER_PIPELINE_ID,rs.getString("PIPELINE_ID"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_MODULE_ID,rs.getString("GROUP_ID"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_DESCRIPTION,rs.getString("DESCRIPTION"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_TOOL_TIP,rs.getString("TOOL_TIP"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE,rs.getString("DEFAULT_SCHEDULE"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD,rs.getString("DEFAULT_PAYLOAD"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME,rs.getString("QUEUE_NAME"));
            pipeline.setAttribute(SchedulerConstants.PARAMETER_CLASS_CODE,rs.getString("CLASS_CODE"));
            String strTmp = rs.getString("FORCE_DEFAULT_PAYLOAD");
            if( StringUtils.isBlank(strTmp) ) {
                strTmp = "N";
            }
            pipeline.setAttribute(SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD,strTmp);
            strTmp = rs.getString("ACTIVE_FLAG");
            if( StringUtils.isBlank(strTmp) ) {
                strTmp = "N";
            }
            pipeline.setAttribute(SchedulerConstants.PARAMETER_ACTIVE_FLAG,strTmp);
        }
        
        return doc;
    }
    
    public String getNextJobId(Connection conn) throws Exception {
        DataRequest dataRequest = new DataRequest();
        logger.debug("getNextJobId() :: String ");
        /* setup stored procedure input parameters */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_NEXT_JOB_ID");
  
        /* execute the stored prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
        return ((String) dataAccessUtil.execute(dataRequest));
    }
    
    public String getNextTriggerId(Connection conn) throws Exception {
        DataRequest dataRequest = new DataRequest();
        logger.debug("getNextTriggerId() :: String ");
        /* setup stored procedure input parameters */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_NEXT_TRIGGER_ID");
  
        /* execute the stored prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();          
        return ((String) dataAccessUtil.execute(dataRequest));
    }
    
    public Document getGroupPipelinesXML(Connection conn, String groupId) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_PIPELINES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_GROUP_ID",groupId);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_GROUP_PIPELINES");
        
        /**
         SELECT PIPELINE_ID, DESCRIPTION
                   FROM pipeline 
                   WHERE GROUP_ID=IN_GROUP_ID 
                   ORDER BY PIPELINE_ID; 
         **/
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            Element pipeline = doc.createElement(SchedulerConstants.PARAMETER_PIPELINE);
            pipeline.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_PIPELINE_ID,rs.getString("PIPELINE_ID")));
            pipeline.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_DESCRIPTION,rs.getString("DESCRIPTION")));
            root.appendChild(pipeline);
        }
        
        return doc;
    }
    
    public Document getModulesXML(Connection conn) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_MODULES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_MODULES");
        
        /**
         SELECT group_id,description 
                   FROM groups 
                   ORDER BY description; 
         **/
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            Element group = doc.createElement(SchedulerConstants.PARAMETER_MODULE);
            group.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_MODULE_ID,rs.getString("group_id")));
            group.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_DESCRIPTION,rs.getString("description")));
            group.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_ACTIVE_FLAG,rs.getString("active_flag")));
            root.appendChild(group);
        }
        
        return doc;
    }
    
    public void postAMessage(Connection conn, String queueName, String correlationId, String payload, long delaySeconds ) throws Exception {
        
    /*
     IN_QUEUE_NAME     IN  VARCHAR2,
       IN_CORRELATION_ID IN  VARCHAR2,
       IN_PAYLOAD        IN  VARCHAR2,
       IN_DELAY_SECONDS  IN  NUMBER,
       OUT_STATUS        OUT VARCHAR2,
       OUT_MESSAGE       OUT VARCHAR2
     */
     
        StringBuilder sb = new StringBuilder();
        sb.append("Calling postamessage");
        sb.append("\r\nqueueName: ");
        sb.append(queueName);
        sb.append("\r\ncorrelationId: ");
        sb.append(correlationId);
        sb.append("\r\npayload: ");
        sb.append(payload);
        sb.append("\r\ndelaySeconds: ");
        sb.append(delaySeconds);
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_QUEUE_NAME",queueName);
        inputParams.put("IN_CORRELATION_ID",correlationId);
        inputParams.put("IN_PAYLOAD",payload);
        inputParams.put("IN_DELAY_SECONDS",new Long(delaySeconds));
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_POST_A_MESSAGE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public Document getEmailAddressesXML(Connection conn) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_EMAIL_ADDRESSES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_EMAIL_ADDRESSES");
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_ADDR,rs.getString(1)));
        }
        
        return doc;
    }
    
    public String addEmailAddress(Connection conn, String emailAddress) throws Exception {
        String retval;
        DataRequest dataRequest = new DataRequest();
        
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_EMAIL_ADDRESS",emailAddress);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_INSERT_EMAIL_ADDRESS");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            if( StringUtils.equals(message,SchedulerConstants.SQL_RETURN_DUPLICATE) ) {
                retval = message;
            } else {
                throw new Exception(message);
            }
        } else {
            retval = status;
        }
        
        return retval;
    }
    
    public ClassVO getClassPathForCode(Connection conn, String classCode) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_CLASS_CODE",classCode);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_CLASS_FOR_CODE");
        
        /* execute the stored procedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
        ClassVO vo = new ClassVO();
        vo.setClassCode(classCode);
        String className = (String) outputs.get("OUT_CLASS_NAME");
        vo.setClassName(className);
        
        CachedResultSet results = (CachedResultSet)outputs.get("OUT_PARAMETERS");

        if ( results != null ) {
            while(results.next()) {
                ClassParameterVO cpVO = new ClassParameterVO();
                cpVO.setClassCode(classCode);
                cpVO.setParameterCode(results.getString("PARAMETER_CODE"));
                cpVO.setRequired(StringUtils.equals(results.getString("REQUIRED_FLAG"),"Y"));
                cpVO.setManditory(StringUtils.equals(results.getString("ALWAYS_REQUIRED_FLAG"),"Y"));
                vo.setParameter(cpVO.getParameterCode(),cpVO);
            }
        }
        
        return vo;
    }
    
    public void updateModule(Connection conn, String moduleId, String description, String activeFlag) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_GROUP_ID",moduleId);
        inputParams.put("IN_DESC",description);
        inputParams.put("IN_ACTIVE_FLAG",activeFlag);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_UPDATE_MODULE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
    
    public Document getApolloJobs(Connection conn) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_JOB_TYPES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_GET_APOLLO_JOBS");
  
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            Element code = doc.createElement(SchedulerConstants.PARAMETER_JOB_TYPE);
            code.setAttribute(SchedulerConstants.PARAMETER_CLASS_CODE, rs.getString("CLASS_CODE"));
            code.setAttribute(SchedulerConstants.PARAMETER_CLASS_PACKAGE, rs.getString("CLASS_PACKAGE"));
            root.appendChild(code);
        }
        
        return doc;
    }
    
    public Document getJMSQueues(Connection conn, String schema) throws Exception {
        Document doc = JAXPUtil.createDocument();
        DatabaseMetaData md = conn.getMetaData();
        String[] types = {"TABLE"}; 
        
        ResultSet rs = md.getTables(null,schema,"%",types);
        
        Element root = doc.createElement(SchedulerConstants.PARAMETER_JMS_QUEUES);
        doc.appendChild(root);
        
        while( rs.next() ) {
            String tableName = rs.getString("TABLE_NAME");
            
            //Only include tables which hava a column named "Q_NAME"
            ResultSet column = md.getColumns(null,schema,tableName,"Q_NAME");
            if( column.next() ) {
                Element queue = doc.createElement(SchedulerConstants.PARAMETER_JMS_QUEUE);
                queue.setAttribute(SchedulerConstants.PARAMETER_NAME,tableName);
                queue.setAttribute(SchedulerConstants.PARAMETER_VALUE,schema+"."+tableName);
                root.appendChild(queue);
            }
            
            column.close();
        }
        
        return doc;
    }
    
    public Document getActiveMQJMSQueues(Connection conn) throws Exception {
        Document doc;
        DataRequest dataRequest = new DataRequest();
        CachedResultSet rs = null;
        
        doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.PARAMETER_JMS_QUEUES);
        doc.appendChild(root);
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("GET_JMS_QUEUE_LIST");
        
        /* execute the store prodcedure */
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();    
        rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
        while(rs.next()) {
            Element code = doc.createElement(SchedulerConstants.PARAMETER_JMS_QUEUE);
            code.setAttribute(SchedulerConstants.PARAMETER_NAME, rs.getString("JMS_QUEUE_ID"));
            code.setAttribute(SchedulerConstants.PARAMETER_VALUE, rs.getString("JMS_QUEUE_ID"));
            
            logger.debug("JMS_QUEUE_ID = " + rs.getString("JMS_QUEUE_ID"));
            root.appendChild(code);
        }
        
        return doc;
    }
    
    public String updatePipeline(Connection conn, PipelineVO vo) throws Exception {
        DataRequest dataRequest = new DataRequest();
        String retval = "";
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_MODULE_ID",vo.getModuleId());
        inputParams.put("IN_PIPELINE_ID",vo.getPipelineId());
        inputParams.put("IN_DESC",vo.getDescription());
        inputParams.put("IN_ACTIVE_FLAG",vo.isActiveFlag()?"Y":"N");
        inputParams.put("IN_FORCE_DEFAULT_PAYLOAD",vo.isForceDefaultPayloadFlag()?"Y":"N");
        inputParams.put("IN_DEFAULT_SCHEDULE",vo.getDefaultSchedule());
        inputParams.put("IN_DEFAULT_PAYLOAD",vo.getDefaultPayload());
        inputParams.put("IN_CLASS_CODE",vo.getClassCode());
        inputParams.put("IN_QUEUE_NAME",vo.getQueueName());
        inputParams.put("IN_TOOL_TIP",vo.getToolTip());
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_INSERT_PIPELINE");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            retval = (String) outputs.get("OUT_MESSAGE");
        } else {
            retval = SchedulerConstants.RESULTS_UPDATE_PIPELINE_SUCCESS;
        }
        
        return retval;
    }
    
    public void deleteEmailAddress(Connection conn, String emailAddress) throws Exception {
        DataRequest dataRequest = new DataRequest();
        
        /* build DataRequest object */
        HashMap inputParams = new HashMap();
        inputParams.put("IN_EMAIL_ADDRESS",emailAddress);
        dataRequest.setInputParams(inputParams);
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCHEDULER_DELETE_EMAIL_ADDRESS");
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputs.get("OUT_STATUS");
        if(status != null && status.equalsIgnoreCase("N"))
        {
            String message = (String) outputs.get("OUT_MESSAGE");
            throw new Exception(message);
        }
    }
}
