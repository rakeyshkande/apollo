package com.ftd.scheduler.web;

import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.scheduler.common.ClassParameterVO;
import com.ftd.scheduler.common.ClassVO;
import com.ftd.scheduler.common.PipelineVO;
import com.ftd.scheduler.common.SchedulerConstants;
import com.ftd.scheduler.common.SchedulerUtils;
import com.ftd.scheduler.common.resources.IResourceProvider;
import com.ftd.scheduler.dao.SchedulerDAO;
import com.ftd.scheduler.servlet.QuartzServletContextListener;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class SchedulerSubmitAction extends Action {

    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm");
    private static final Logger logger = new Logger("com.ftd.scheduler.web.SchedulerSubmitAction");
    private SchedulerDAO schedulerDAO;
    private IResourceProvider resourceProvider;

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                 HttpServletRequest request, 
                                 HttpServletResponse response) throws Exception {        
        try {
            String requestType = request.getParameter(SchedulerConstants.REQUEST_TYPE);
            Scheduler scheduler = getScheduler(request);
            
            if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_RUNNING_JOBS) ) {
                response.setContentType("text/xml");
                response.getWriter().write(this.getRunningJobs(scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_SCHEDULER_STATUS) ) {
                response.setContentType("text/html");
                response.getWriter().write(this.getSchedulerStats(scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_SCHEDULED_JOBS)) {
                response.setContentType("text/xml");
                response.getWriter().write(this.getScheduledJobs(scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_DEFINED_JOBS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getDefinedJobs(scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_PIPELINE_DETAIL)) {
                response.setContentType("text/xml");
                response.getWriter().write(getPipelineDetail(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_RUN_ONCE)) {
                response.setContentType("text/html");
                response.getWriter().write(runOnce(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_REMOVE_JOB)) {
                response.setContentType("text/html");
                response.getWriter().write(removeJob(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_REMOVE_TRIGGER)) {
                response.setContentType("text/html");
                response.getWriter().write(removeTrigger(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_GROUP_PIPELINES)) {
                response.setContentType("text/xml");
                response.getWriter().write(getGroupPipelines(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_MODULES)) {
                response.setContentType("text/xml");
                response.getWriter().write(getModules());
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_ADD_JOB)) {
                response.setContentType("text/html");
                response.getWriter().write(addJob(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_SCHEDULE_JOB)) {
                response.setContentType("text/html");
                response.getWriter().write(scheduleJob(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_PAUSE_JOB)) {
                response.setContentType("text/html");
                response.getWriter().write(pauseJob(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_RESUME_JOB)) {
                response.setContentType("text/html");
                response.getWriter().write(resumeJob(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_PAUSE_MODULE)) {
                response.setContentType("text/html");
                response.getWriter().write(pauseModule(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_RESUME_MODULE)) {
                response.setContentType("text/html");
                response.getWriter().write(resumeModule(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_RUN_JOBS)) {
                response.setContentType("text/html");
                response.getWriter().write(runJobs(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_SQL_STATEMENTS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getSQLStatements());
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_EMAIL_ADDRESSES)) {
                response.setContentType("text/xml");
                response.getWriter().write(getEmailAddresses());
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_ADD_EMAIL_ADDRESS)) {
                response.setContentType("text/html");
                response.getWriter().write(addEmailAdderss(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_JOB_DETAIL)) {
                response.setContentType("text/xml");
                response.getWriter().write(getJobDetail(request,scheduler));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_UPDATE_MODULES)) {
                response.setContentType("text/html");
                response.getWriter().write(updateModules(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_ALL_PIPELINE_DETAILS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getAllPipelineDetails());
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_APOLLO_JOBS)) {
                response.setContentType("text/xml");
                response.getWriter().write(getApolloJobs());
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_JMS_QUEUES)) {
                response.setContentType("text/xml");
                response.getWriter().write(getJMSQueues(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_GET_JMS_AMQ_QUEUES)) {
                response.setContentType("text/xml");
                response.getWriter().write(getJMSAMQQueues(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_UPDATE_PIPELINE)) {
                response.setContentType("text/html");
                response.getWriter().write(updatePipeline(request));
            } else if( StringUtils.equals(requestType,SchedulerConstants.REQUEST_TYPE_DELETE_EMAIL_ADDRESSES)) {
                response.setContentType("text/html");
                response.getWriter().write(deleteEmailAddresses(request,scheduler));
            } else {
                throw new Exception("Unknown AJAX request type of "+requestType);
            }
        } catch (Throwable t) {
            logger.error(t);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            request.getSession().setAttribute("errorStr",sw.toString());
            response.setContentType("text/html");
            response.getWriter().write("REDIRECT="+mapping.findForward("error").getPath()+".do");
        }
        
        return null;
    }
    
    private Scheduler getScheduler(HttpServletRequest request) throws SchedulerException {
        ServletContext ctx = request.getSession().getServletContext();
        StdSchedulerFactory factory = (StdSchedulerFactory)ctx.getAttribute(QuartzServletContextListener.QUARTZ_FACTORY_KEY);
        return factory.getScheduler();
    }
    
    private String deleteEmailAddresses(HttpServletRequest request, Scheduler scheduler) throws Exception {
        String retval = "";
        Connection qConn = null;
        
        try {
            String strXML = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES));
            Document doc = null;
            NodeList addresses;
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            
            try {
                doc = JAXPUtil.parseDocument(strXML);
                addresses = JAXPUtil.selectNodes(doc.getDocumentElement(),SchedulerConstants.PARAMETER_ADDR);
            } catch (Exception e) {
                logger.error("Passed in email addresses: "+strXML,e);
                return "Error while parsing passed in email addresses.\r\nSee the \"scheduler.log\" on "+SchedulerUtils.getHostName()+" for details.";
            }
            
            if( addresses.getLength()==0 ) {
                String strMsg = "No email addresses were received.  Nothing to do.";
                logger.info(strMsg);
                return strMsg;
            }
            
            StringBuilder sb = new StringBuilder();
            int errCnt = 0;
            
            for( int idx=0; idx<addresses.getLength(); idx++ ) {
                String address = JAXPUtil.getTextValue(addresses.item(idx));
                try {
                    //Loop through all the jobs and remove the email address
                    StringBuilder warnSB = new StringBuilder();

                    String[] jobGroups = scheduler.getJobGroupNames();
                    
                    for( int gIdx=0; gIdx<jobGroups.length; gIdx++ ) {
                        String group = jobGroups[gIdx];
                        
                        String[] jobNames = scheduler.getJobNames(group);
                        for( int jIdx=0; jIdx<jobNames.length; jIdx++ ) {
                            JobDetail jobDetail = scheduler.getJobDetail(jobNames[jIdx],group);
                            if( jobDetail==null ) {
                                continue;
                            }
                            
                            JobDataMap map = jobDetail.getJobDataMap();
                            String strXml = StringUtils.trimToEmpty((String)map.get(SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES));
                            if( StringUtils.isBlank(strXml) ) {
                                continue;
                            }
                            
                            Document emailDoc = null;
                            
                            try {
                                emailDoc = JAXPUtil.parseDocument(strXml);
                            } catch (Exception je) {
                                logger.error("Unparseable email address xml found in job "+jobNames[jIdx],je);
                                continue;
                            }
                            
                            Element root = emailDoc.getDocumentElement();
                            
                            //Get the specific node
                            NodeList matches = JAXPUtil.selectNodes(root,"//ADDR[. = \""+address+"\"]");
                            
                            int removeCtr = 0;
                            for( int mIdx=0; mIdx<matches.getLength(); mIdx++ ) {
                                Node node = matches.item(idx);
                                root.removeChild(node);
                                removeCtr++;
                            }
                            
                            if( removeCtr>0 ) {
                                map.put(SchedulerConstants.PARAMETER_SQL_EMAIL_ADDRESSES,JAXPUtil.toString(emailDoc));
                                JobDetail newJob = (JobDetail)jobDetail.clone();  
                                String newJobName = schedulerDAO.getNextJobId(qConn);
                                newJob.setName(newJobName);
                                scheduler.addJob(newJob,false);
                                
                                //Check for triggers
                                Trigger[] oldTriggers = scheduler.getTriggersOfJob(jobDetail.getName(),group);
                                for( int tIdx=0; tIdx<oldTriggers.length; tIdx++ ) {
                                    Trigger ot = oldTriggers[tIdx];
                                    String oldSchedule = "";
                                    
                                    if( ot instanceof CronTrigger ) {
                                        oldSchedule = ((CronTrigger)oldTriggers[tIdx]).getCronExpression();
                                    } else if( ot instanceof SimpleTrigger ) {
                                        oldSchedule = String.valueOf(((SimpleTrigger)oldTriggers[tIdx]).getRepeatInterval()/1000);
                                    } 
                                    
                                    //Recreate the trigger for the new job
                                    scheduleJob(scheduler, newJobName, group, oldSchedule);
                                    
                                    //Remove the old trigger
                                    scheduler.unscheduleJob(jobDetail.getName(),group);
                                }
                                
                                //Delete the old job
                                scheduler.deleteJob(jobDetail.getName(),group);

                                //Have all the email reciepient been removed from the job?
                                 matches = JAXPUtil.selectNodes(root,"//ADDR");
                                if( matches.getLength()==0 )  {
                                    warnSB.append("There are no email addresses associated with job ");
                                    warnSB.append(newJobName);
                                }
                            }
                        }
                    }
                    
                    //Remove from the database
                    schedulerDAO.deleteEmailAddress(qConn,address);
                    sb.append("\r\n");
                    sb.append(address);
                    sb.append(" deleted");
                    
                    String warnStr = StringUtils.trimToEmpty(warnSB.toString());
                    if( StringUtils.isNotBlank(warnStr) ) {
                        sb.append("\r\n");
                        sb.append(warnStr);
                    }
                } catch( Exception e) {
                    logger.error("Error while trying to remove email address "+address,e);
                    sb.append("\r\n");
                    sb.append(address);
                    sb.append(" FAILED");
                    errCnt++;
                }
            }
            
            if( errCnt>0 ) {
                sb.append("\r\n\r\nSee the \"scheduler.log\" file on "+SchedulerUtils.getHostName()+" for details.");
            }
            
            retval = StringUtils.trimToEmpty(sb.toString());
        
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
        
        return retval;
    }
    
    private String updatePipeline(HttpServletRequest request) throws Exception {
        String retval;
        String moduleId = request.getParameter(SchedulerConstants.PARAMETER_MODULE_ID);
        String pipelineId = request.getParameter(SchedulerConstants.PARAMETER_PIPELINE_ID);
        String desc = request.getParameter(SchedulerConstants.PARAMETER_DESCRIPTION);
        String defaultSchedule = request.getParameter(SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE);
        String defaultPayload = request.getParameter(SchedulerConstants.PARAMETER_DEFAULT_PAYLOAD);
        boolean active = BooleanUtils.toBoolean(request.getParameter(SchedulerConstants.PARAMETER_ACTIVE_FLAG));
        boolean forceDefaultPayload = BooleanUtils.toBoolean(request.getParameter(SchedulerConstants.PARAMETER_FORCE_DEFAULT_PAYLOAD));
        String classCode = request.getParameter(SchedulerConstants.PARAMETER_CLASS_CODE);
        String queueName = request.getParameter(SchedulerConstants.PARAMETER_QUEUE_NAME);
        String toolTip = request.getParameter(SchedulerConstants.PARAMETER_TOOL_TIP);
        
        if( !StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE) &&
            !StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE)
        ) 
        {
            queueName = "";
        }
        
        PipelineVO vo = new PipelineVO();
        vo.setModuleId(moduleId);
        vo.setPipelineId(pipelineId);
        vo.setDescription(desc);
        vo.setDefaultSchedule(defaultSchedule);
        vo.setDefaultPayload(defaultPayload);
        vo.setActiveFlag(active);
        vo.setForceDefaultPayloadFlag(forceDefaultPayload);
        vo.setClassCode(classCode);
        vo.setQueueName(queueName);
        vo.setToolTip(toolTip);

        Connection qConn = null;
        
        try {
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            retval = schedulerDAO.updatePipeline(qConn,vo);
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
        
        return retval;
    }
    
    private String updateModules(HttpServletRequest request) throws Exception {
        String retval = "Module update status:";
        Connection qConn = null;
        
        try {
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            String strXml = request.getParameter(SchedulerConstants.PARAMETER_MODULES);
            Document doc = JAXPUtil.parseDocument(strXml);
            
            NodeList nodes = JAXPUtil.selectNodes(doc,"//"+SchedulerConstants.PARAMETER_MODULE);
            for( int idx=0; idx<nodes.getLength(); idx++) {
                Element module = (Element)nodes.item(idx);
                String moduleId = module.getAttribute(SchedulerConstants.PARAMETER_MODULE_ID);
                String desc = module.getAttribute(SchedulerConstants.PARAMETER_DESCRIPTION);
                String active = module.getAttribute(SchedulerConstants.PARAMETER_ACTIVE_FLAG);
    
                try {
                    schedulerDAO.updateModule(qConn,moduleId,desc,active);
                    retval += "\r\nModule "+moduleId+" successfully updated.";
                } catch (Exception e) {
                    logger.error(e);
                    retval += "\r\nModule "+moduleId+" failed to updated.\r\n"+e.getMessage();
                }
            }
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
        
        return retval;
    }
    
    private String getJobDetail(HttpServletRequest request, Scheduler scheduler) throws Exception {
        String jobName = request.getParameter(SchedulerConstants.PARAMETER_JOB_NAME);
        String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
        
        JobDetail jobDetail = scheduler.getJobDetail(jobName,group);
        if( jobDetail==null ) {
            throw new Exception("The job "+jobName+" does not exist.\r\n\r\nRefresh the details tab and try again.");
        }
        JobDataMap map = jobDetail.getJobDataMap();
        
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement("jobdetail");
        doc.appendChild(root);
        
        root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_JOB_NAME,jobDetail.getName()));        
        root.appendChild(JAXPUtil.buildSimpleXmlNode(doc,SchedulerConstants.PARAMETER_GROUP,jobDetail.getGroup()));
        Element parms = doc.createElement(SchedulerConstants.PARAMETER_PARAMETERS);
        root.appendChild(parms);
        
        String[] keys = map.getKeys();
        
        for( int idx=0; idx<keys.length; idx++) {
            String value = map.getString(keys[idx]);
            parms.appendChild(JAXPUtil.buildSimpleXmlNode(doc,keys[idx],value));
        }
        
        return JAXPUtil.toString(doc);
        
    }
    
    private String addEmailAdderss(HttpServletRequest request) throws Exception {
        Connection qConn = null;
        String retval;
        
        try {
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            
            String emailAddress = request.getParameter(SchedulerConstants.PARAMETER_ADDR);
            
            String result = schedulerDAO.addEmailAddress(qConn,emailAddress);
            
            if( StringUtils.equals(SchedulerConstants.SQL_RETURN_DUPLICATE,result) ) {
                retval = "Email address "+emailAddress+" is already in the database";
            } else {
                retval = emailAddress;
            }
        } catch (Throwable t) {
            logger.error("Error in addEmailAdderss.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String getEmailAddresses() throws Exception {
        Connection qConn = null;
        String retval;
        
        try {
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            Document doc = schedulerDAO.getEmailAddressesXML(qConn);
            retval = JAXPUtil.toString(doc);
        } catch (Throwable t) {
            logger.error("Error in getEmailAddresses.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String getJMSQueues(HttpServletRequest request) throws Exception {
        String schema = request.getParameter(SchedulerConstants.PARAMETER_SCHEMA);

        Connection qConn = null;
        String retval;
        
        try {
            qConn = resourceProvider.getDatabaseConnection("OJMSDS");
            Document doc = schedulerDAO.getJMSQueues(qConn,schema);
            retval = JAXPUtil.toString(doc);
        } catch (Throwable t) {
            logger.error("Error in getJMSQueues.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
        
    }
    private String getJMSAMQQueues(HttpServletRequest request) throws Exception {

        Connection conn = null;
        String retval;
        
        try {
            conn = resourceProvider.getDatabaseConnection("CLEANDS");
            Document doc = schedulerDAO.getActiveMQJMSQueues(conn);
            retval = JAXPUtil.toString(doc);
        } catch (Throwable t) {
            logger.error("Error in getJMSAMQQueues.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( conn!=null ) {
                try {
                    if( !conn.isClosed() ) {
                        conn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
        
    }
    
    private String getSQLStatements() throws Exception {
        Document doc = JAXPUtil.parseDocument(this.getClass().getClassLoader().getResourceAsStream(SchedulerConstants.SQL_STATEMENTS_FILE));
//        Element root = doc.getDocumentElement();
//        NodeList statements = root.getElementsByTagName("statement");
        return JAXPUtil.toString(doc);
    }
    
    private String runJobs(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        Connection qConn = null;
        String retval;
        
        try {
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
            String classCode = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_CLASS_CODE));
            
            if( StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE) ||
                StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE)
            ) 
            {
                String pipeline = request.getParameter(SchedulerConstants.PARAMETER_PIPELINE);
                String payloads = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_PAYLOAD));
                String queueName = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME));
                
                if( StringUtils.isBlank(payloads) ) {
                    return "No payload was specified";
                }
                
                String[] payloadArray = payloads.split("\\n");
                 
                StringBuilder sb = new StringBuilder();
                qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
                
                for( int idx = 0; idx < payloadArray.length; idx++) {
                    String payload = StringUtils.trimToEmpty(payloadArray[idx]);
                    
                    try {
                        if( StringUtils.isNotBlank(payload) ) {
                            String jobName = schedulerDAO.getNextJobId(qConn);
                            //JobDetail jobDetail = new JobDetail(jobName,group,Class.forName(jobClass));
                            Class jmsJobClass = null;
                            if( StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE) )
                            {
                                jmsJobClass= com.ftd.scheduler.job.SimpleJmsJob.class;
                            }
                            else if (StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE))
                            {
                                jmsJobClass= com.ftd.scheduler.job.ActiveMQJmsJob.class;
                                
                            }
                            JobDetail jobDetail = new JobDetail(jobName,group,jmsJobClass);
                            JobDataMap map = new JobDataMap();
                            map.put(SchedulerConstants.PARAMETER_PAYLOAD,payload);
                            map.put(SchedulerConstants.PARAMETER_PIPELINE,pipeline);
                            map.put(SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE,"");
                            map.put(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME,queueName);
                            jobDetail.setJobDataMap(map);
                            jobDetail.setDurability(false); 
                            
                            Trigger trigger = TriggerUtils.makeImmediateTrigger(0, 0);
                            trigger.setName(schedulerDAO.getNextTriggerId(qConn));
                            scheduler.scheduleJob(jobDetail,trigger);
                        }
                    } catch (Throwable t) {
                        sb.append(payload);
                        sb.append(": ");
                        sb.append(t.getMessage());
                        sb.append("\r\n");
                        logger.error("Error while scheduling payload "+payload,t);
                    }
                }
                
                if( sb.length()>0 ) {
                    retval = "One or more jobs failed to start.  See log for details.";
                } else {
                    retval = SchedulerConstants.RESULTS_SUCCESS;
                }
                
            } else if( StringUtils.equals(classCode,SchedulerConstants.SQL_JOB_CLASS_CODE) ) {
                qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
                ClassVO classVO = schedulerDAO.getClassPathForCode(qConn,classCode);
                String className = classVO.getClassName();
                if( StringUtils.isBlank(className) ) {
                    throw new Exception("Unable to locate class for code "+classCode+" in the database.");
                }
                
                String jobName = schedulerDAO.getNextJobId(qConn);
                JobDetail jobDetail = new JobDetail(jobName,group,Class.forName(className));
                JobDataMap map = new JobDataMap();
                
                Iterator<ClassParameterVO> parms = classVO.getParameters().values().iterator();
                while( parms.hasNext() ) {
                    ClassParameterVO cpVO = parms.next();
                    String parmCode = cpVO.getParameterCode();
                    String parmValue = request.getParameter(parmCode);
                    if( logger.isDebugEnabled() ) {
                        logger.debug("Code: "+parmCode+" Value: "+parmValue);
                    }
                    
                    if( parmValue == null ) {
                        if( cpVO.isRequired() || cpVO.isManditory() ) {
                            throw new Exception("Required parameter of "+parmCode+" was not found.");
                        }
                    } else {
                        map.put(parmCode,parmValue);
                    }
                }
                
                jobDetail.setJobDataMap(map);
                jobDetail.setDurability(false); 
                
                Trigger trigger = TriggerUtils.makeImmediateTrigger(0, 0);
                trigger.setName(schedulerDAO.getNextTriggerId(qConn));
                scheduler.scheduleJob(jobDetail,trigger);
                
                retval = SchedulerConstants.RESULTS_SUCCESS;
            } else {
                throw new Exception("Unsupported class "+classCode+" cannot be processed");
            }
            
        } catch (Throwable t) {
            logger.error("Error in runJobs.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String pauseJob(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        String retval;
        
        try {
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
             
            scheduler.pauseTriggerGroup(group);
            retval = SchedulerConstants.RESULTS_PAUSE_JOB_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in pauseJob.",t);
            retval = SchedulerConstants.RESULTS_PAUSE_JOB_FAILED+"\r\n"+t.getMessage();
        }
    
        return retval;
    }
    
    private String resumeJob(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        String retval;
        
        try {
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
             
            scheduler.resumeTriggerGroup(group);
            retval = SchedulerConstants.RESULTS_RESUME_JOB_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in resumeJob.",t);
            retval = SchedulerConstants.RESULTS_RESUME_JOB_FAILED+"\r\n"+t.getMessage();
        }
    
        return retval;
    }
    
    private String pauseModule(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        String retval;
        String module = request.getParameter(SchedulerConstants.PARAMETER_MODULE);
        
        try {
            String[] triggerGroups = scheduler.getTriggerGroupNames();
            Set pausedTriggerGroups = scheduler.getPausedTriggerGroups();
            
            for( int idx=0; idx<triggerGroups.length; idx++ ) {
        
                String group = triggerGroups[idx];
                
                //If the job is already paused, the just continue
                if( pausedTriggerGroups.contains(group) ) {
                    continue;
                }
                        
                String[] triggers = scheduler.getTriggerNames(group);
            
                for( int idx2=0; idx2<triggers.length; idx2++ ) {
                    Trigger trigger = scheduler.getTrigger(triggers[idx2],group);
                    if( trigger!=null ) {   
                        JobDetail jobDetail = scheduler.getJobDetail(trigger.getJobName(),trigger.getJobGroup());

                        if( jobDetail==null ) {
                            throw new Exception("The job "+trigger.getJobName()+" does not exist.\r\n\r\nRefresh the scheduled tab and try again.");
                        }
                        
                        String jobModule = "unknown";
                        JobDataMap map = jobDetail.getJobDataMap();
                        
                        if( map==null ) {
                            continue;
                        }
                        
                        jobModule = (String)map.get(SchedulerConstants.PARAMETER_MODULE);
                        
                        if( StringUtils.equals(module,jobModule) ) {
                            scheduler.pauseTriggerGroup(group);
                        }
                    }
                }
            }
            
            retval = SchedulerConstants.RESULTS_PAUSE_MODULE_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in pauseModule.",t);
            retval = SchedulerConstants.RESULTS_PAUSE_MODULE_FAILED+"\r\n"+t.getMessage();
        }
    
        return retval;
    }
    
    private String resumeModule(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        String retval;
        String module = request.getParameter(SchedulerConstants.PARAMETER_MODULE);
        
        try {
            String[] triggerGroups = scheduler.getTriggerGroupNames();
            Set pausedTriggerGroups = scheduler.getPausedTriggerGroups();
            
            for( int idx=0; idx<triggerGroups.length; idx++ ) {
        
                String group = triggerGroups[idx];
                
                //If the job is not paused, the just continue
                if( !pausedTriggerGroups.contains(group) ) {
                    continue;
                }
                        
                String[] triggers = scheduler.getTriggerNames(group);
            
                for( int idx2=0; idx2<triggers.length; idx2++ ) {
                    Trigger trigger = scheduler.getTrigger(triggers[idx2],group);
                    if( trigger!=null ) {   
                        JobDetail jobDetail = scheduler.getJobDetail(trigger.getJobName(),trigger.getJobGroup());

                        if( jobDetail==null ) {
                            throw new Exception("The job "+trigger.getJobName()+" does not exist.\r\n\r\nRefresh the scheduled tab and try again.");
                        }
                        
                        String jobModule = "unknown";
                        JobDataMap map = jobDetail.getJobDataMap();
                        
                        if( map==null ) {
                            continue;
                        }
                        
                        jobModule = (String)map.get(SchedulerConstants.PARAMETER_MODULE);
                        
                        if( StringUtils.equals(module,jobModule) ) {
                            scheduler.resumeTriggerGroup(group);
                        }
                    }
                }
            }
            
            retval = SchedulerConstants.RESULTS_RESUME_MODULE_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in resumeModule.",t);
            retval = SchedulerConstants.RESULTS_RESUME_MODULE_FAILED+"\r\n"+t.getMessage();
        }
    
        return retval;
    }
    
    private String scheduleJob(HttpServletRequest request, Scheduler scheduler) throws Exception {
        String schedule = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_SCHEDULE));
        String jobName = request.getParameter(SchedulerConstants.PARAMETER_JOB_NAME);
        String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
        
        return scheduleJob(scheduler, jobName, group, schedule);
    }
    
    private String scheduleJob(Scheduler scheduler, String jobName, String group, String schedule) throws Exception {
    
        Connection qConn = null;
        String retval;
        
        try {
             
            long repeatInterval=0L;
            
            if( StringUtils.isBlank(schedule) ) {
                return "Schedule must be provided";
            }
            
            Trigger trigger = null;
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            try {
                //Is this a simple job?
                repeatInterval = Long.parseLong(schedule)*1000;
                Date daDate = new Date();
                trigger = new SimpleTrigger(schedulerDAO.getNextTriggerId(qConn), group, jobName, group, daDate, null, SimpleTrigger.REPEAT_INDEFINITELY, repeatInterval);
            } catch (NumberFormatException  nfe) {
                //Is it a cron job
                if( !CronExpression.isValidExpression(schedule) ) {
                    return "Invalid cron expression.";
                }
                trigger = new CronTrigger(schedulerDAO.getNextTriggerId(qConn), group, jobName, group, schedule);
            }
            
            scheduler.scheduleJob(trigger);
            retval = SchedulerConstants.RESULTS_JOB_SCHEDULE_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in scheduleJob.",t);
            retval = SchedulerConstants.RESULTS_JOB_SCHEDULE_FAILED+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String addJob(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        Connection qConn = null;
        String retval;
        
        try {;
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
            String schedule = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE));
            String scheduleType = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_SCHEDULE_TYPE));
            String classCode = StringUtils.trimToEmpty(request.getParameter(SchedulerConstants.PARAMETER_CLASS_CODE));
            
            //Look to see if an existing job name was passed in
            boolean bEditMode;
            String definedJobName = request.getParameter(SchedulerConstants.DEFINED_JOB_NAME);
            String definedGroup = request.getParameter(SchedulerConstants.DEFINED_GROUP);
            if( StringUtils.isNotBlank(definedJobName) ) {
                bEditMode = true;
                
                JobDetail definedJobDetail = scheduler.getJobDetail(definedJobName,definedGroup);
                if( definedJobDetail==null ) {
                    throw new Exception("The job "+definedJobName+" does not exist.\r\n\r\nRefresh the details tab and try again.");
                }
            } else {
                bEditMode = false;
            }
             
            long repeatInterval=0L;
            if( StringUtils.equals(scheduleType,SchedulerConstants.SELECTED_SCHEDULE_CRON) ) {
                if( !CronExpression.isValidExpression(schedule) ) {
                    return "Invalid cron expression.";
                }
            } else if( StringUtils.equals(scheduleType,SchedulerConstants.SELECTED_SCHEDULE_INTERVAL) ) {
                try {
                    repeatInterval = Long.parseLong(schedule)*1000;
                } catch (NumberFormatException  nfe) {
                    return "Invalid number ("+schedule+") entered for an interval seconds.";
                }
            } else {
                return "Unsupported schedule of "+scheduleType+" found.";
            }
            
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");
            ClassVO classVO = schedulerDAO.getClassPathForCode(qConn,classCode);
            String className = classVO.getClassName();
            if( StringUtils.isBlank(className) ) {
                throw new Exception("Unable to locate class for code "+classCode+" in the database.");
            }
            
            String jobName = schedulerDAO.getNextJobId(qConn);
            JobDetail jobDetail = new JobDetail(jobName,group,Class.forName(className));
            JobDataMap map = new JobDataMap();
            
            Iterator<ClassParameterVO> parms = classVO.getParameters().values().iterator();
            while( parms.hasNext() ) {
                ClassParameterVO cpVO = parms.next();
                String parmCode = cpVO.getParameterCode();
                String parmValue = request.getParameter(parmCode);
                if( logger.isDebugEnabled() ) {
                    logger.debug("Code: "+parmCode+" Value: "+parmValue);
                }
                
                if( parmValue == null ) {
                    if( cpVO.isRequired() || cpVO.isManditory() ) {
                        throw new Exception("Required parameter of "+parmCode+" was not found.");
                    }
                } else {
                    map.put(parmCode,parmValue);
                }
            }
            
            jobDetail.setJobDataMap(map);
            jobDetail.setDurability(true); 
            scheduler.addJob(jobDetail,false);
            
            //Now remove the old job is this was a save
            if( bEditMode ) {
                //Check for triggers
                Trigger[] oldTriggers = scheduler.getTriggersOfJob(definedJobName,definedGroup);
                for( int idx=0; idx<oldTriggers.length; idx++ ) {
                    Trigger ot = oldTriggers[idx];
                    String oldSchedule = "";
                    
                    if( ot instanceof CronTrigger ) {
                        oldSchedule = ((CronTrigger)oldTriggers[idx]).getCronExpression();
                    } else if( ot instanceof SimpleTrigger ) {
                        oldSchedule = String.valueOf(((SimpleTrigger)oldTriggers[idx]).getRepeatInterval()/1000);
                    } else {
                        oldSchedule = schedule;
                    }
                    
                    //Recreate the trigger for the new job
                    scheduleJob(scheduler, jobName, group, oldSchedule);
                    
                    //Remove the old trigger
                    scheduler.unscheduleJob(definedJobName,definedGroup);
                }
                
                //Delete the job
                scheduler.deleteJob(definedJobName,definedGroup);
                
            }
            
            retval = SchedulerConstants.RESULTS_SUCCESS;
            
        } catch (Throwable t) {
            logger.error("Error in addJob.",t);
            retval = SchedulerConstants.RESULTS_FAILURE+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String getModules() throws Exception {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");
            doc = schedulerDAO.getModulesXML(conn);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    logger.error(sqle);
                }
            }
        }
        
        
        return JAXPUtil.toString(doc);
    }
    
    private String getGroupPipelines(HttpServletRequest request) throws Exception {
        String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
        
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");
            doc = schedulerDAO.getGroupPipelinesXML(conn,group);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    logger.error(sqle);
                }
            }
        }
        
        
        return JAXPUtil.toString(doc);
    }
    
    private String removeTrigger(HttpServletRequest request, Scheduler scheduler) throws Exception {
         String retval;
         
         try {
             String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
             String trigger = request.getParameter(SchedulerConstants.PARAMETER_TRIGGER_NAME);
             
             //Schedule the job
             scheduler.unscheduleJob(trigger,group);
             
             retval = SchedulerConstants.RESULTS_TRIGGER_REMOVAL_SUCCESS;
         } catch (Throwable t) {
             logger.error("Error in removeTrigger.",t);
             retval = SchedulerConstants.RESULTS_TRIGGER_REMOVAL_FAILED+"\r\n"+t.getMessage();
         }
         
         return retval;
    }
    
    private String removeJob(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        String retval;
        
        try {
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
            String jobName = request.getParameter(SchedulerConstants.PARAMETER_JOB_NAME);
              
              //Delete the job
              scheduler.deleteJob(jobName,group);
              
              retval = SchedulerConstants.RESULTS_JOB_REMOVAL_SUCCESS;
          } catch (Throwable t) {
              logger.error("Error in removeTrigger.",t);
              retval = SchedulerConstants.RESULTS_JOB_REMOVAL_FAILED+"\r\n"+t.getMessage();
          }
          
          return retval;
    }
    
    private String runOnce(HttpServletRequest request, Scheduler scheduler) throws Exception {
    
        Connection qConn = null;
        String retval;
        
        try {
            String jobName = request.getParameter(SchedulerConstants.PARAMETER_JOB_NAME);
            String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
            qConn = resourceProvider.getDatabaseConnection("QUARTZDS");

            JobDetail jobDetail = scheduler.getJobDetail(jobName,group);

            if( jobDetail==null ) {
                throw new Exception("The job "+jobName+" does not exist.\r\n\r\nRefresh the details tab and try again.");
            }
                    
            jobDetail = (JobDetail)jobDetail.clone();
            jobDetail.setName(schedulerDAO.getNextJobId(qConn));
            jobDetail.setDurability(false);
            
            //Create the "run once" trigger
            Trigger trigger = TriggerUtils.makeImmediateTrigger(0, 0);
            trigger.setName(schedulerDAO.getNextTriggerId(qConn));
            
            //Schedule the job
            scheduler.scheduleJob(jobDetail,trigger);
            
            retval = SchedulerConstants.RESULTS_JOB_STARTED;
        } catch (Throwable t) {
            logger.error("Error in runOnce.",t);
            retval = SchedulerConstants.RESULTS_JOB_START_FAILED+"\r\n"+t.getMessage();
        } finally {
            if( qConn!=null ) {
                try {
                    if( !qConn.isClosed() ) {
                        qConn.close();
                    }
                } catch (SQLException sqle) {
                    logger.warn("Error closing quartz database connection",sqle);
                }
            }
        }
    
        return retval;
    }
    
    private String getPipelineDetail(HttpServletRequest request) throws Exception {
        String group = request.getParameter(SchedulerConstants.PARAMETER_GROUP);
        String pipeline = request.getParameter(SchedulerConstants.PARAMETER_PIPELINE);
        
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");
            doc = schedulerDAO.getPipelineDetailXML(conn,pipeline,group);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    logger.error(sqle);
                }
            }
        }
        
        
        return JAXPUtil.toString(doc);
    }
    
    private String getAllPipelineDetails() throws Exception {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");
            doc = schedulerDAO.getAllPipelineDetailsXML(conn);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    logger.error(sqle);
                }
            }
        }
        
        
        return JAXPUtil.toString(doc);
    }
    
    private String getApolloJobs() throws Exception {
        Connection conn = null;
        Document doc = null;
        
        try {
            conn = resourceProvider.getDatabaseConnection("QUARTZDS");
            doc = schedulerDAO.getApolloJobs(conn);
        } finally {
            if( conn!=null ) {
                try {
                    conn.close();
                } catch (SQLException sqle) {
                    logger.error(sqle);
                }
            }
        }
        
        
        return JAXPUtil.toString(doc);
    }
    
    private String getRunningJobs(Scheduler scheduler) throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.RUNNING_JOBS);
        doc.appendChild(root);

        List list = scheduler.getCurrentlyExecutingJobs();
 
        Element tbody = doc.createElement("tbody");
        root.appendChild(tbody);
        
        for(int idx=0; idx<list.size(); idx++) {
            JobExecutionContext ctx = (JobExecutionContext)list.get(idx);
            JobDetail detail = ctx.getJobDetail();
            Date startTime = ctx.getFireTime();
            String jobName = detail.getName();
            long duration = ctx.getJobRunTime();
            
            String payload = "none";
            String pipeline = "unknown";
            String module = "unknown";
            String classCode = "unknown";
            String jobParms = "n/a";
            String sqlName = "unknown";
            
            JobDataMap map = detail.getJobDataMap();
            if( map!=null ) {
                String tmp = (String)map.get(SchedulerConstants.PARAMETER_PAYLOAD);
                if( StringUtils.isNotBlank(tmp) ) {
                    payload = tmp;
                }
                tmp = (String)map.get(SchedulerConstants.PARAMETER_PIPELINE);
                if( StringUtils.isNotBlank(tmp) ) {
                    pipeline = tmp;
                }
                tmp = (String)map.get(SchedulerConstants.PARAMETER_MODULE);
                if( StringUtils.isNotBlank(tmp) ) {
                    module = tmp;
                }
                tmp = (String)map.get(SchedulerConstants.PARAMETER_CLASS_CODE);
                if( StringUtils.isNotBlank(tmp) ) {
                    classCode = tmp;
                }
                tmp = (String)map.get(SchedulerConstants.PARAMETER_SQL_STATEMENT_NAME);
                if( StringUtils.isNotBlank(tmp) ) {
                    sqlName = tmp;
                }   
            }
                            
            if(StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE)) 
            {
                jobParms = "Function: " + pipeline+"  Payload: "+payload;
            } 
            else if(StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE)) 
            {
                jobParms = "Function: " + pipeline+"  Payload: "+payload;
            } 
            else if(StringUtils.equals(classCode,SchedulerConstants.SQL_JOB_CLASS_CODE)) 
            {
                jobParms = "SQL: "+sqlName;
            }
            
            Element row = doc.createElement(SchedulerConstants.RUNNING_JOB);
            row.setAttribute(SchedulerConstants.RUNNING_JOB_NAME,jobName);
            row.setAttribute(SchedulerConstants.RUNNING_MODULE,module);
            row.setAttribute(SchedulerConstants.RUNNING_JOB_PARMS,jobParms);
            row.setAttribute(SchedulerConstants.RUNNING_START_TIME,DATE_TIME_FORMAT.format(startTime));
            row.setAttribute(SchedulerConstants.RUNNING_DURATION,String.valueOf(duration/60000)+" seconds");
            tbody.appendChild(row);
        }
                
        return JAXPUtil.toString(doc);
    }
    
    private String getSchedulerStats(Scheduler scheduler) throws Exception {
        SchedulerMetaData metaData = scheduler.getMetaData();
        return metaData.getSummary();
    }
    
    private String getScheduledJobs(Scheduler scheduler) throws Exception {
        Document doc = JAXPUtil.createDocument();
        Element root = doc.createElement(SchedulerConstants.SCHEDULED_JOBS);
        doc.appendChild(root);
    
        String[] triggerGroups = scheduler.getTriggerGroupNames();
        Set pausedTriggerGroups = scheduler.getPausedTriggerGroups();      
    
        for( int idx=0; idx<triggerGroups.length; idx++ ) {
            boolean groupPaused;
            String group = triggerGroups[idx];
            String[] triggers = scheduler.getTriggerNames(group);
                    
            if( pausedTriggerGroups.contains(group) ) {
                groupPaused = true;
            } else {
                groupPaused = false;
            }
            
            for( int idx2=0; idx2<triggers.length; idx2++ ) {
                Trigger trigger = scheduler.getTrigger(triggers[idx2],group);
                if( trigger!=null ) {            
                    String triggerName = trigger.getName();
                    Date previous = trigger.getPreviousFireTime();
                    Date next = trigger.getNextFireTime();
                    String nextStr;
                    
                    if( groupPaused ) {
                        nextStr = SchedulerConstants.DISPLAY_JOB_PAUSED;
                    } else {
                        if( next==null ) {
                            nextStr = "n/a";
                        } else {
                            nextStr = DATE_TIME_FORMAT.format(next);
                        }
                    }
                                        
                    String schedule = "none";
                    if( trigger instanceof CronTrigger ) {
                        CronTrigger cron = (CronTrigger)trigger;
                        String tmpExpression = cron.getCronExpression();
                        if( StringUtils.isNotBlank(tmpExpression) ) {
                            schedule = tmpExpression;
                        }
                    } else if( trigger instanceof SimpleTrigger ) {
                        SimpleTrigger st = (SimpleTrigger)trigger;
                        schedule = String.valueOf(st.getRepeatInterval()/1000);
                    }
                    
                    JobDetail jobDetail = scheduler.getJobDetail(trigger.getJobName(),trigger.getJobGroup());
                    if( jobDetail==null ) {
                        continue;
                    }
                    String jobName = jobDetail.getName();
                    String groupName = jobDetail.getGroup();
                    String classCode = "unknown";
                    String payload = "none";
                    String pipeline = "unknown";
                    String module = "unknown";
                    String jobParms = "n/a";
                    String sqlName = "unknown";
                    
                    JobDataMap map = jobDetail.getJobDataMap();
                    
                    if( map!=null ) {
                        String tmp = (String)map.get(SchedulerConstants.PARAMETER_PAYLOAD);
                        if( StringUtils.isNotBlank(tmp) ) {
                            payload = tmp;
                        }
                        tmp = (String)map.get(SchedulerConstants.PARAMETER_PIPELINE);
                        if( StringUtils.isNotBlank(tmp) ) {
                            pipeline = tmp;
                        }
                        tmp = (String)map.get(SchedulerConstants.PARAMETER_MODULE);
                        if( StringUtils.isNotBlank(tmp) ) {
                            module = tmp;
                        }
                        tmp = (String)map.get(SchedulerConstants.PARAMETER_CLASS_CODE);
                        if( StringUtils.isNotBlank(tmp) ) {
                            classCode = tmp;
                        }
                        tmp = (String)map.get(SchedulerConstants.PARAMETER_SQL_STATEMENT_NAME);
                        if( StringUtils.isNotBlank(tmp) ) {
                            sqlName = tmp;
                        }   
                    }
                
                    if(StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE)) {
                        jobParms = "Function: " + pipeline+"  Payload: "+payload;
                    } else if(StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE)) {
                            jobParms = "Function: " + pipeline+"  Payload: "+payload;
                    } else if(StringUtils.equals(classCode,SchedulerConstants.SQL_JOB_CLASS_CODE)) {
                        jobParms = "SQL: "+sqlName;
                    }
                
                    Element row = doc.createElement(SchedulerConstants.SCHEDULED_JOB);
                    row.setAttribute(SchedulerConstants.SCHEDULED_JOB_NAME,jobName);
                    row.setAttribute(SchedulerConstants.SCHEDULED_GROUP,groupName);
                    row.setAttribute(SchedulerConstants.SCHEDULED_MODULE,module);
                    row.setAttribute(SchedulerConstants.SCHEDULED_PAYLOAD,payload);
                    row.setAttribute(SchedulerConstants.SCHEDULED_PIPELINE,pipeline);
                    row.setAttribute(SchedulerConstants.SCHEDULED_TRIGGER,triggerName);
                    row.setAttribute(SchedulerConstants.SCHEDULED_SCHEDULE,schedule);
                    row.setAttribute(SchedulerConstants.SCHEDULED_JOB_PARMS,jobParms);
                    row.setAttribute(SchedulerConstants.SCHEDULED_PREVIOUS_RUN,previous==null?"n/a":DATE_TIME_FORMAT.format(previous));
                    row.setAttribute(SchedulerConstants.SCHEDULED_NEXT_RUN,nextStr);
                    root.appendChild(row);
                }
            }
        }
                
         return JAXPUtil.toString(doc);
    }
    
    private String getDefinedJobs(Scheduler scheduler) throws Exception {
        Document doc = JAXPUtil.createDocument();        
        Element root = doc.createElement(SchedulerConstants.DEFINED_JOBS);
        doc.appendChild(root);
        String[] jobGroups = scheduler.getJobGroupNames();
        
        for( int idx=0; idx<jobGroups.length; idx++ ) {
            String group = jobGroups[idx];
            
            String[] jobNames = scheduler.getJobNames(group);
            for( int idx2=0; idx2<jobNames.length; idx2++ ) {
                JobDetail jobDetail = scheduler.getJobDetail(jobNames[idx2],group);
                if( jobDetail==null ) {
                    continue;
                }
                
                String jobName = jobDetail.getName();
                String groupName = jobDetail.getGroup();
                String jobClass = jobDetail.getJobClass().getName();
                String classCode = "unknown";
                String payload = "none";
                String pipeline = "unknown";
                String schedule = "";
                String module = "";
                String queueName = "";
                String jobParms = "n/a";
                String sqlName = "unknown";
                
                JobDataMap map = jobDetail.getJobDataMap();
                if( map!=null ) {
                    String tmp = (String)map.get(SchedulerConstants.PARAMETER_PAYLOAD);
                    if( StringUtils.isNotBlank(tmp) ) {
                        payload = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_PIPELINE);
                    if( StringUtils.isNotBlank(tmp) ) {
                        pipeline = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_DEFAULT_SCHEDULE);
                    if( StringUtils.isNotBlank(tmp) ) {
                        schedule = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_MODULE);
                    if( StringUtils.isNotBlank(tmp) ) {
                        module = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_JMS_QUEUE_NAME);
                    if( StringUtils.isNotBlank(tmp) ) {
                        queueName = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_CLASS_CODE);
                    if( StringUtils.isNotBlank(tmp) ) {
                        classCode = tmp;
                    }
                    tmp = (String)map.get(SchedulerConstants.PARAMETER_SQL_STATEMENT_NAME);
                    if( StringUtils.isNotBlank(tmp) ) {
                        sqlName = tmp;
                    }
                }
                
                if(StringUtils.equals(classCode,SchedulerConstants.JMS_JOB_CLASS_CODE)) {
                    jobParms = "Function: " + pipeline+"  Payload: "+payload;
                } else if(StringUtils.equals(classCode,SchedulerConstants.ACTIVEMQ_JMS_JOB_CLASS_CODE)) {
                    jobParms = "Function: " + pipeline+"  Payload: "+payload;
                } else if(StringUtils.equals(classCode,SchedulerConstants.SQL_JOB_CLASS_CODE)) {
                    jobParms = "SQL: "+sqlName;
                }
                
                Element row = doc.createElement(SchedulerConstants.DEFINED_JOB);
                row.setAttribute(SchedulerConstants.DEFINED_JOB_NAME,jobName);
                row.setAttribute(SchedulerConstants.DEFINED_GROUP,groupName);
                row.setAttribute(SchedulerConstants.DEFINED_MODULE,module);
                row.setAttribute(SchedulerConstants.DEFINED_PAYLOAD,payload);
                row.setAttribute(SchedulerConstants.DEFINED_PIPELINE,pipeline);
                row.setAttribute(SchedulerConstants.DEFINED_JOB_CLASS,jobClass);
                row.setAttribute(SchedulerConstants.DEFINED_SCHEDULE,schedule);
                row.setAttribute(SchedulerConstants.DEFINED_JMS_QUEUE_NAME,queueName);
                row.setAttribute(SchedulerConstants.DEFINED_JOB_PARMS,jobParms);
                row.setAttribute(SchedulerConstants.DEFINED_CLASS_CODE,classCode);
                root.appendChild(row);
            }
        }
                        
        return JAXPUtil.toString(doc);
    }

    public void setSchedulerDAO(SchedulerDAO schedulerDAO) {
        this.schedulerDAO = schedulerDAO;
    }

    public SchedulerDAO getSchedulerDAO() {
        return schedulerDAO;
    }

    public void setResourceProvider(IResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    public IResourceProvider getResourceProvider() {
        return resourceProvider;
    }
}
