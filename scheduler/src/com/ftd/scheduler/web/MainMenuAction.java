package com.ftd.scheduler.web;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.scheduler.common.SchedulerConstants;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class MainMenuAction extends Action {
    private static Logger logger  = new Logger("com.ftd.scheduler.web.MainMenusAction");

  /******************************************************************************
  * execute(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
  *******************************************************************************
  *
  * This is the main action called from the Struts framework.
  * @param mapping The ActionMapping used to select this instance.
  * @param form The optional ActionForm bean for this request.
  * @param request The HTTP Request we are processing.
  * @param response The HTTP Response we are processing.
  */
  public  ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
                                HttpServletResponse response) 
          throws IOException, ServletException, Exception
  {

    logger.debug("MainMenuAction.execute");
    String  mainMenuUrl     = "";
    HashMap requestParms    = new HashMap();

    //Get info passed in the request object
    requestParms = getRequestInfo(request);


      if(ConfigurationUtil.getInstance().getFrpGlobalParm("SCHEDULER_CONFIG", "MAIN_MENU_URL") != null)
      {
        //main menu url
        mainMenuUrl = ConfigurationUtil.getInstance().getFrpGlobalParm("SCHEDULER_CONFIG", "MAIN_MENU_URL");
        
        //append adminAction
        mainMenuUrl += "?adminAction=";
        mainMenuUrl += (String)requestParms.get("adminAction");

        //append security to the main menu url
        mainMenuUrl += "&context=";
        mainMenuUrl += (String)requestParms.get("context");
        mainMenuUrl += "&securitytoken=";
        mainMenuUrl += (String)requestParms.get("securityToken");               

        response.sendRedirect(mainMenuUrl);
      }

      return null;


  }



  /******************************************************************************
  * getRequestInfo(HttpServletRequest request)
  *******************************************************************************
  * Retrieve the info from the request object, and set class level variables
  * @param  HttpServletRequest
  * @return none 
  * @throws none
  */
  private HashMap getRequestInfo(HttpServletRequest request)
  {
    String  adminAction                   = "";
    String  context                       = "";
    String  securityToken                 = ""; 
    HashMap requestParms                  = new HashMap();    
  

    /**********************retreive the security info**********************/
    //retrieve the context
    if(request.getParameter(SchedulerConstants.ADMIN_ACTION)!=null)
      adminAction = request.getParameter(SchedulerConstants.ADMIN_ACTION);

    //retrieve the context
    if(request.getParameter(SchedulerConstants.CONTEXT)!=null)
      context = request.getParameter(SchedulerConstants.CONTEXT);

    //retrieve the security token
    if(request.getParameter(SchedulerConstants.SEC_TOKEN)!=null)
      securityToken = request.getParameter(SchedulerConstants.SEC_TOKEN);

 
    requestParms.put("adminAction", adminAction);
    requestParms.put("context", context);
    requestParms.put("securityToken", securityToken);
  
  
    return requestParms;  
  }
}
