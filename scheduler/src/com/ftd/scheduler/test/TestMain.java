package com.ftd.scheduler.test;

import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SchedulerMetaData;
import org.quartz.impl.StdSchedulerFactory;


public class TestMain {
    public TestMain() {
    }

    public static void main(String[] args) {
        TestMain testMain = new TestMain();
        testMain.testIt();
    }
    
    public void testIt() {
        try {
            System.out.println("------- Initializing -------------------");
    
            // First we must get a reference to a scheduler
            SchedulerFactory sf = new StdSchedulerFactory();
            Scheduler sched = sf.getScheduler();
    
            System.out.println("------- Initialization Complete --------");
    
            System.out.println("------- Scheduling Jobs ----------------");

            // jobs can be scheduled before sched.start() has been called

            // job 1 will run every 20 seconds
            JobDetail job = new JobDetail("job1", "SHIPPROCESSING", SimpleTestJob.class);
            JobDataMap map = new JobDataMap();
            map.put("PAYLOAD","job 1 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            CronTrigger trigger = new CronTrigger("trigger1", "SHIPPROCESSING", "job1",
                            "SHIPPROCESSING", "0/20 * * * * ?");
            sched.deleteJob("job1","SHIPPROCESSING");
            sched.addJob(job, true);
            Date ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 2 will run every other minute (at 15 seconds past the minute)
            job = new JobDetail("job2", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            map.put("PAYLOAD","job 2 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger2", "SHIPPROCESSING", "job2", "SHIPPROCESSING",
                            "15 0/2 * * * ?");
            sched.deleteJob("job2","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 3 will run every other minute but only between 8am and 5pm
            job = new JobDetail("job3", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            map.put("PAYLOAD","job 3 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger3", "SHIPPROCESSING", "job3", "SHIPPROCESSING",
                            "0 0/2 8-17 * * ?");
            sched.deleteJob("job3","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 4 will run every three minutes but only between 5pm and 11pm
            job = new JobDetail("job4", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            job.setDurability(true);
            map.put("PAYLOAD","job 4 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger4", "SHIPPROCESSING", "job4", "SHIPPROCESSING",
                            "0 0/3 17-23 * * ?");
            sched.deleteJob("job4","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 5 will run at 10am on the 1st and 15th days of the month
            job = new JobDetail("job5", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            map.put("PAYLOAD","job 5 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger5", "SHIPPROCESSING", "job5", "SHIPPROCESSING",
                            "0 0 10am 1,15 * ?");
            sched.deleteJob("job5","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 6 will run every 30 seconds but only on Weekdays (Monday through
            // Friday)
            job = new JobDetail("job6", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            map.put("PAYLOAD","job 6 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger6", "SHIPPROCESSING", "job6", "SHIPPROCESSING",
                            "0,30 * * ? * MON-FRI");
            sched.deleteJob("job6","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            // job 7 will run every 30 seconds but only on Weekends (Saturday and
            // Sunday)
            job = new JobDetail("job7", "SHIPPROCESSING", SimpleTestJob.class);
            map = new JobDataMap();
            map.put("PAYLOAD","job 7 payload");
            map.put("PIPELINE","GETSCANS");
            job.setJobDataMap(map);
            job.setDurability(true);
            trigger = new CronTrigger("trigger7", "SHIPPROCESSING", "job7", "SHIPPROCESSING",
                            "0,30 * * ? * SAT,SUN");
            sched.deleteJob("job7","SHIPPROCESSING");
            sched.addJob(job, true);
            ft = sched.scheduleJob(trigger);
            System.out.println(job.getFullName() + " has been scheduled to run at: " + ft
                            + " and repeat based on expression: "
                            + trigger.getCronExpression());

            System.out.println("------- Starting Scheduler ----------------");

            // All of the jobs have been added to the scheduler, but none of the
            // jobs
            // will run until the scheduler has been started
            sched.start();

            System.out.println("------- Started Scheduler -----------------");

            System.out.println("------- Waiting five minutes... ------------");
            try {
                    // wait five minutes to show jobs
                    Thread.sleep(300L * 1000L);
                    // executing...
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("------- Shutting Down ---------------------");

            sched.shutdown(true);

            System.out.println("------- Shutdown Complete -----------------");

            SchedulerMetaData metaData = sched.getMetaData();
            System.out.println("Executed " + metaData.numJobsExecuted() + " jobs.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
