package com.ftd.scheduler.test;

import com.ftd.osp.utilities.plugins.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class SimpleTestJob implements Job {
    private static final Logger LOGGER = new Logger("com.ftd.scheduler.test.SimpleTestJob");
    
    public SimpleTestJob() {
    }

    public void execute(JobExecutionContext jobExecutionContext) {
    }
}
