package com.ftd.scheduler.test;

import com.ftd.scheduler.common.resources.TestResourceProviderImpl;

import javax.mail.Session;

import junit.framework.TestCase;

public class SLQJobTestCase extends TestCase {
    public SLQJobTestCase(String testCase) {
        super(testCase);
    }

    public static void main(String[] args) {
        SLQJobTestCase testCase = new SLQJobTestCase("testGetEmailSession");
        
        try {
            testCase.runBare();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void testGetEmailSession() throws Exception {
        TestResourceProviderImpl rp = new TestResourceProviderImpl();
        
        Session session = rp.getEmailSession();
    }
}
