package com.ftd.scheduler.servlet;

import com.ftd.osp.utilities.plugins.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzServletContextListener implements ServletContextListener {
    private static final Logger LOGGER = new Logger("com.ftd.scheduler.servlet.QuartzServletContextListener");
    public static final String QUARTZ_FACTORY_KEY = "QUARTZ_FACTORY_KEY";
    
    private ServletContext ctx = null;
    private StdSchedulerFactory factory = null;
    
    public QuartzServletContextListener() {
    }
    
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        
        ctx = servletContextEvent.getServletContext();
        
        try {
            factory = new StdSchedulerFactory();
            ctx.setAttribute(QUARTZ_FACTORY_KEY,factory);
            
            //Start the scheduler now
            factory.getScheduler().start();
        } catch (Exception ex) {
            LOGGER.error("Quartz scheduler failed to initialize", ex);
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            /**
             * Shut down, but, let running processes complete.
             * Since the only jobs are just firing JMS events,
             * then this should only take a brief time to complete
             */
            factory.getDefaultScheduler().shutdown(true);
        } catch (SchedulerException ex) {
            LOGGER.error("Error stopping Quartz",ex);
        }
    }
}
