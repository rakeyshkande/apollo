<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:template name="footer">

	<CENTER>
		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR> <TD>&nbsp; </TD> </TR>
			<TR>
				<TD bgcolor="#FF0000"><div align="center"><strong><font color="#FFFFFF">THIS IS A TRAINING AREA ONLY.  DO NOT TAKE REAL CUSTOMER ORDERS ON THIS SYSTEM</font></strong></div></TD>
			</TR>
			<TR>
				<TD class="disclaimer">
					<DIV align="center">
					COPYRIGHT
					<script>
					<![CDATA[
					document.write("&#174;")
					]]>
					</script>
					2005. FTD INC. ALL RIGHTS RESERVED.  @cvs_build_tag@</DIV>
				</TD>
			</TR>
		</TABLE>
	</CENTER>
</xsl:template>
</xsl:stylesheet>
