  var genericBag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";
	var enterKeyCode = 13;
	var disableFlag =false;

  document.onDblClick=stopIt;
  document.oncontextmenu=stopIt;
  document.onkeydown = backKeyHandler;

  function init()
  {
    if (document.forms[0].adjustedPrice.value <= 0)
    {
      document.getElementById("balanceDueDIV").style.display = 'block';
      document.getElementById("creditCardDIV").style.display = 'none';
    }
    else
    {
      if (document.forms[0].displayCreditCard.value == 'Y')
      {
        // set default values for credit cards if there are any //
        var year = document.forms[0].creditCardExpirationYear;
        var month = document.forms[0].creditCardExpirationMonth;
        year.selectedIndex = year.initialValue - year.currentYear + 1;
        month.selectedIndex = month.initialValue;
      }
      creditCardType_onChange();
    }
  }

	function disableContinue()
	{
    disableFlag = true;
    document.all.placeOrderButton.src  = '../images/button_placeorder_disabled.gif';
    document.all.backButton.src  = '../images/button_placeorder_disabled.gif';
	}

	function enableContinue()
	{
		disableFlag = false;
		document.all.placeOrderButton.src  = '../images/button_placeorder.gif';
		document.all.backButton.src  = '../images/button_back.gif';
	}

	function creditCardType_onChange()
	{
    if (!document.forms[0].promptValue0)
    {
      if ( document.forms[0].creditCardType.value == "NC" )
      {
        document.all.noChargeDIV.style.display = "block";
        document.all.creditCardNumberDiv.style.display = "none";
        document.all.creditCardExpDateDiv.style.display = "none";
      }
      else
      {
        document.all.authDIV.style.display = "none";
        document.forms[0].verbalAuthorization.value = "";
        document.all.noChargeDIV.style.display = "none";
        document.all.creditCardNumberDiv.style.display = "block";
        document.all.creditCardExpDateDiv.style.display = "block";
      }
    }
	}

  function clearOutValues()
  {
    document.forms[0].noChargeMgrId.value = "";
	  document.forms[0].noChargePassword.value = "";
		document.forms[0].noChargeMgrId.className= "";
		document.forms[0].noChargePassword.className= "";
    document.forms[0].creditCardNumber.value = "";
    document.forms[0].creditCardExpirationMonth.value = "";
    document.forms[0].creditCardExpirationYear.value = "";
    document.forms[0].creditCardNumber.className= "";
    document.forms[0].creditCardExpirationMonth.className= "";
    document.forms[0].creditCardExpirationYear.className= "";
   
  }

	function openUpdateWin()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=document.body.clientHeight/2-100

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";
	}

	function onValidationNotAvailable()
	{
    // Do nothing.  We can't submit billing information without validation. //
	}

	function cancelPayment(nextServlet)
	{
    document.forms[0].forwardToAction.value = "BILLING_SCREEN";
    disableContinue();
    creditCardPaymentForm.action="http://" + siteName + nextServlet;
    creditCardPaymentForm.submit();
	}

	function EnterToCancelPayment()
	{
		if (window.event.keyCode == 13)
		{
		    cancelPayment();
		}
	}


	function onValidationResponse(validation)
	{
            document.getElementById("placeOrderButton").disabled = false;
    if (validation.VALIDATION && (validation.VALIDATION.value == 'OK'))
    {
      var ret_BILLING_INFO_ID = "OK";
      if(validation.BILLING_INFO_ID != null)
      {
        ret_BILLING_INFO_ID = validation.BILLING_INFO_ID.value;
        
        if ( ret_BILLING_INFO_ID != "OK" )
        {
          serverCheck = false;
          alert(ret_BILLING_INFO_ID + ".");
          enableContinue();
        }
      }
  
      var ret_CREDIT_CARD_NUMBER = "OK";
      var ret_CREDIT_CARD_EXP = "OK";
      var ret_NO_CHARGE_APPROVAL = "OK";
      var ret_DYNAMIC_PROMPT_GRP = "OK";
      if ((document.forms[0].displayCreditCard.value == 'Y') &&  (document.forms[0].adjustedPrice.value > 0))
      {
        // only authorize if credit card type is not 'No Charge' or have verbal authorization
        if ( document.forms[0].creditCardType.value != "NC" && document.all.authDIV.style.display == "none" )
        {
          ret_CREDIT_CARD_NUMBER = validation.CREDIT_CARD_NUMBER.value;
          ret_CREDIT_CARD_EXP = validation.CREDIT_CARD_EXP.value;
  
          if ( ret_CREDIT_CARD_NUMBER != "OK" )
          {
            serverCheck = false;
            document.forms[0].creditCardNumber.className= "Error";
          }
          else if(ret_CREDIT_CARD_EXP != "OK")
          {
            serverCheck = false;
            document.forms[0].creditCardExpirationMonth.className= "Error";
            document.forms[0].creditCardExpirationYear.className= "Error";
          }
          else
          {
            document.forms[0].ccApprovalCode.value = validation.CREDIT_CARD_APPROVAL_CODE.value;
            document.forms[0].ccAVSResult.value = validation.CREDIT_CARD_AVS_RESULT.value;
            document.forms[0].ccApprovalAmount.value = validation.CREDIT_CARD_APPROVAL_AMOUNT.value;
            document.forms[0].ccACQReferenceData.value = validation.CREDIT_CARD_ACQ_REFERENCE_DATA.value;
            document.forms[0].ccApprovalVerbiage.value = validation.CREDIT_CARD_APPROVAL_VERBIAGE.value;
            document.forms[0].ccActionCode.value = validation.CREDIT_CARD_ACTION_CODE.value;
          }
        }
        else if ( document.forms[0].creditCardType.value == "NC" &&
              document.all.noChargeDIV.style.display == "block" )
        {
          ret_NO_CHARGE_APPROVAL = validation.USER_FUNCTION.value;
          if ( ret_NO_CHARGE_APPROVAL != "OK" )
          {
            serverCheck = false;
            document.forms[0].noChargeMgrId.style.className= "Error";
            document.forms[0].noChargePassword.style.className= "Error";
          }
        }
      }
            // Dynamic Fields Validation //
        if (document.getElementById("promptListRow")) 
        {
          var promptName;
          var promptArr = document.getElementsByName("promptListRow");
          for (var i=0; i< promptArr.length; i++)
          {
            var row = promptArr[i];
            var promptName = "DYNAMIC_PROMPT" + i;
            var element = document.getElementById("validation." + promptName);
            if (element)
            {
              ret_DYNAMIC_PROMPT = element.value;
              if(element.value != "OK")
              {
                ret_DYNAMIC_PROMPT_GRP = ret_DYNAMIC_PROMPT;
                serverCheck = false;
                var Field = document.forms[0].getElementById("promptValue" + i);
                errorField.className= "Error";
                alert(element.value + ". Please correct!");
                enableContinue();
              }
            }
          }
        }
        closeUpdateWin();
  
  
        if ( (ret_CREDIT_CARD_NUMBER == "OK") &&
             (ret_CREDIT_CARD_EXP == "OK") &&
           (ret_NO_CHARGE_APPROVAL == "OK") &&
           (ret_DYNAMIC_PROMPT_GRP == "OK") &&
           (ret_BILLING_INFO_ID == "OK"))
        {
          disableContinue();
          creditCardPaymentForm.submit();
        }
        else if ( ret_CREDIT_CARD_NUMBER == "Could not authorize credit card" )
        {
          document.forms[0].creditCardNumber.focus();
          alert(ret_CREDIT_CARD_NUMBER + ". Please correct!");
          enableContinue();
        }
        else if ( ret_CREDIT_CARD_NUMBER == "Credit card validation system error" ||
              ret_CREDIT_CARD_NUMBER == "Error occurred during credit card authorization" )
        {
  
          var x = confirm(ret_CREDIT_CARD_NUMBER + ". Would you like to continue?");
          if ( x == true )
          {
            disableContinue();
            creditCardPaymentForm.submit();
          }
          else
          {
            enableContinue();
          }
  
        }
        else if ( ret_CREDIT_CARD_NUMBER == "Conditional approval granted" )
        {
          document.forms[0].ccApprovalCode.value = "";
          document.forms[0].ccAVSResult.value = "Y";
          document.forms[0].ccApprovalAmount.value = document.forms[0].totalAmount.value;
          document.forms[0].ccACQReferenceData.value = "";
          document.forms[0].ccApprovalVerbiage.value = "AP";
          document.forms[0].ccActionCode.value = "000";
  
          document.all.authDIV.style.display = "block";
          document.forms[0].verbalAuthorization.value = "";
          document.forms[0].verbalAuthorization.focus();
          alert(ret_CREDIT_CARD_NUMBER + ". Please obtain verbal authorization");
          enableContinue();
  
        }
        else if ( ret_CREDIT_CARD_NUMBER == "Credit card not active or please call card issuer" )
        {
          document.forms[0].creditCardNumber.focus();
          alert("Credit card not active. Please correct!");
          enableContinue();
        }
        else if ( ret_CREDIT_CARD_NUMBER != "OK" )
        {
          document.forms[0].creditCardNumber.focus();
          alert(ret_CREDIT_CARD_NUMBER + ". Please correct!");
          enableContinue();
        }
  
        else if ( ret_CREDIT_CARD_EXP != "OK" )
        {
          document.forms[0].creditCardExpirationMonth.focus();
          alert(ret_CREDIT_CARD_EXP + ". Please correct!");
          enableContinue();
        }
        else if ( ret_NO_CHARGE_APPROVAL != "OK" )
        {
          document.forms[0].noChargeMgrId.focus();
          alert("Approval Manager information is not valid. Please re-enter");
          enableContinue();
        }
      }
      else
      {
        alert ("Validation error.  Please try again.");
      }
 		}

	function fieldExceedsMaxLength(fieldValue, fieldLength)
	{
    if (fieldLength == -1)
    {
      return false;
    }
    else if (fieldValue.length > fieldLength) 
    {
      return true;	
    } 
    else
    {
      return false;
    }
	}

	function applyValidation()
	{
    if(disableFlag == false)
		{
     	var ccCheck = true;
      // Credit Card Validation //
      if ((document.forms[0].displayCreditCard.value == 'Y') && (document.forms[0].adjustedPrice.value > 0))
      {
        if (document.forms[0].ccApprovalVerbiage.value != 'AP')
        { 
          if ( document.forms[0].creditCardType.value == "NC" )
          {
            ccCheck =  fieldValidationText ('noChargeMgrId', -1, ccCheck, true, 'A', genericBag);
            ccCheck =  fieldValidationText ('noChargePassword', -1, ccCheck, true, 'A', "");
          }
          else
          {
            ccCheck =  fieldValidationText ('creditCardNumber', -1, ccCheck, true, 'A', "");
            ccCheck =  fieldValidationSelect ('creditCardType', ccCheck);
            ccCheck =  fieldValidationSelect ('creditCardExpirationMonth', ccCheck);
            ccCheck =  fieldValidationSelect ('creditCardExpirationYear', ccCheck);
            if ( document.all.authDIV.style.display == "block" )
            {
              ccCheck =  fieldValidationText ('verbalAuthorization', -1, ccCheck, true, 'A', genericBag);
            }
          }
        }
        if ( ccCheck == true )
        {
          if (document.forms[0].creditCardType.value == "NC" )
          {
            addValidationEntry("USER_FUNCTION", "NOCHARGEAPPRV");
            addValidationEntry("USER_FUNCTION_USER_NAME", document.forms[0].noChargeMgrId.value);
            addValidationEntry("USER_FUNCTION_USER_PASSWORD", document.forms[0].noChargePassword.value);
          }
          else
          {
            addValidationEntry("CREDIT_CARD_NUMBER", document.forms[0].creditCardNumber.value)
            addValidationEntry("CREDIT_CARD_TYPE", document.forms[0].creditCardType.value)
            addValidationEntry("CREDIT_CARD_ADDRESS", document.forms[0].billingAddress1.value)
            addValidationEntry("CREDIT_CARD_DNIS_TYPE",document.forms[0].dnisType.value);
            var expiration = (document.forms[0].creditCardExpirationYear.value).substring(2,4) + document.forms[0].creditCardExpirationMonth.value
            addValidationEntry("CREDIT_CARD_EXPIRATION", expiration)
            addValidationEntry("CREDIT_CARD_AMOUNT", document.forms[0].totalAmount.value)
            addValidationEntry("CREDIT_CARD_ZIP", document.forms[0].billingZipCode.value)
          }
        }
        else
        {
          closeUpdateWin();
          alert("Please correct the marked fields.");
          enableContinue();
          return;
        }
      }
      // Dynamic Fields Validation //
      if (document.getElementById("promptListRow")) 
      {
        var promptArr = document.getElementsByName("promptListRow");
        for (var i=0; i< promptArr.length; i++)
        {
          var row = promptArr[i];
          var inputArr = row.getElementsByTagName("INPUT");
          for (var j=0; j < inputArr.length; j++)
          {
            if (inputArr[j].disabled == false &&  inputArr[j].type.toUpperCase() != "HIDDEN")
            {
              inputArr[j].className="TblText";
              if (!matchDynamicField(inputArr[j], i+1))
              {
                if (ccCheck == true)
                {
                  inputArr[j].focus();
                  ccCheck = false;
                }
                inputArr[j].className="Error";
               }
             }
          }
        }
        if(ccCheck == true)
        {
          addValidationEntry("BILLING_INFO", "OK");
          addValidationEntry("PARTNER_ID", document.forms[0].programId.value);
          addValidationEntry("SOURCE_CODE_ID", document.forms[0].sourceCodeId.value);
          var promptArr = document.getElementsByName("promptListRow");
          for (var i=0; i< promptArr.length; i++)
          {
            var name = "promptValue" + i;
            var value = document.getElementById(name).value;
            addValidationEntry(name, value);
          }
        }
        else
        {
          closeUpdateWin();
          alert("Please correct the marked fields.");
          enableContinue();
          return;
        }
      }
      if ( ccCheck == true )
      {
          document.getElementById("placeOrderButton").disabled = true;
          callToServer(siteNameSsl);
      }
    }
  }
  
	function EnterToApplyValidation()
	{
  	if (window.event.keyCode == 13)
		{
		    applyValidation();
		}
	}

	function doShoppingCartLocal(nextServlet)
	{
    document.forms[0].forwardToAction.value = "SHOPPING_CART";
    disableContinue();
    creditCardPaymentForm.action="http://" + siteName + nextServlet;
    creditCardPaymentForm.submit();
  }

  function doCancelOrderLocal()
  {
    if(confirm("Cancel your entire order?"))
    {
    	document.location =  "http://" + siteName + applicationContext + "/servlet/CancelOrderServlet?sessionId=" + document.forms[0].sessionId.value + "&persistentObjId=" + document.forms[0].persistentObjId.value;
    }
  }
  
	function JCPPopChoice(inChoice)
	{
		if(inChoice == 'Y')
		{
			var url= "DNISChangeServlet?";
			url = url.concat(buildURL());
			url = url.concat("&source=billingJCP");

			document.location = url;

		} else {
			document.all.billingCountry.value = 'US';
			closeJCPPopup();
		}
	}

	function openJCPPopup()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		JCPPopV.top=scroll_top+document.body.clientHeight/2-100

		JCPPopV.width=290
		JCPPopV.height=100
		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
	}