  var genericBag = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

	var enterKeyCode = 13;
	var disableFlag =false;
  
  document.onDblClick=stopIt;
  document.oncontextmenu=stopIt;
  document.onkeydown = backKeyHandler;


  function init()
  {
    var customerState = document.getElementById("billingStateSelect").customerState;
    var countryElement = document.forms[0].billingCountry;
    if (countryElement.value == 'CA')
    {
			  document.getElementById("stateSelect").style.display="block";
	  	  document.getElementById("stateText").style.display="none";
        buildStateDropdown("billingStateSelect", "CAN", customerState);
        buildStateDropdown("stateInput", "CAN", "");
	  }
    else if (countryElement.options[countryElement.selectedIndex].countryType == "I")
    {
			  document.getElementById("stateSelect").style.display="none";
			  document.getElementById("stateSelect").value = "";
        document.getElementById("stateText").style.display="block";
		}
    else
		{
			  document.getElementById("stateSelect").style.display= "block";
			  document.getElementById("stateText").style.display="none";
        buildStateDropdown("billingStateSelect", "", customerState);
        buildStateDropdown("stateInput", "", "");
		} 
  }

  function buildStateDropdown(selectName, countryCode, selected)
  {
    var element = document.getElementById(selectName);
    element.options.length = 0;
    var hiddenStateSelect = document.getElementById("hiddenStateSelect");
    element.appendChild(new Option());
    for (var i = 0; i< hiddenStateSelect.length; i++)
    {
      if (hiddenStateSelect.options[i].countryCode == countryCode)
      {
        var option = hiddenStateSelect.options[i].cloneNode(true);
        if (hiddenStateSelect.options[i].value == selected)
          option.selected = true;
        element.appendChild(option);
      }
    }
    return;
  }
  
	function applyServerValidation()
	{
    if (document.getElementById("displayMembershipId").value == "Y")
    {
      if (document.getElementById("membershipIdNOTRequired").value != "Y")
      {
        if (billingPaymentForm.programId.value == 'AAA' || !document.all.skipMemberInfo.checked )
        {
          addValidationEntry("MEMBERSHIP_ID", billingPaymentForm.memberId.value);
          addValidationEntry("PARTNER_ID", billingPaymentForm.programId.value);
        }
      }
    }
    if (billingPaymentForm.giftCertId)
    {
      if (document.getElementById("giftCertId").value != "")
      {
        addValidationEntry("GIFT_CERTIFICATE_ID", billingPaymentForm.giftCertId.value)
      	addValidationEntry("GIFT_ORDER_TOTAL_AMOUNT", billingPaymentForm.orderTotalPrice.value);
				addValidationEntry("GIFT_MAX_AMOUNT", billingPaymentForm.orderTotalPrice.value);
				addValidationEntry("COMPANY_ID", billingPaymentForm.companyId.value);
      }
    }
		callToServer();
	}

	function openUpdateWin()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=document.body.clientHeight/2-100

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";
	}

	function checkForPOAPO(inString)
	{
    var POAPO = new Array("PO BOX", "P.O.", "POSTOFFICE", "APO", "A.P.O.", "FPO", "F.P.O");
    if (document.getElementById("dnisType").value == 'JP')
    {
    	for (var i = (POAPO.length - 1); i >= 0; i--)
      {
        if ((inString.toUpperCase()).indexOf(POAPO[i]) >= 0)
        {
            return true;
        }
      }
    }
    return false;
  }

	function onValidationNotAvailable()
	{
    // Do nothing.  We can't submit billing information without validation. //
	}

	function onValidationResponse(validation)
	{
    if (validation.VALIDATION && (validation.VALIDATION.value == 'OK'))
    {
      var serverCheck = true;
      
      if (billingPaymentForm.displayMembershipId.value == 'Y')
      {
        if (billingPaymentForm.membershipIdNOTRequired.value == 'Y')
        {
          document.forms[0].memberId.style.backgroundColor='white';
        }
        else
        {
          if (billingPaymentForm.programId.value == 'AAA' || !document.forms[0].skipMemberInfo.checked)
          {
            if ( validation.MEMBERSHIP_ID.value != "OK" )
            {
                serverCheck = false;
                document.forms[0].memberId.style.backgroundColor='pink';
                alert(validation.MEMBERSHIP_ID.value + ". Please correct!");
            }
            else
            {
                document.forms[0].memberId.style.backgroundColor='white';
            }
          }
        }    
      }
      if (billingPaymentForm.giftCertId)
      {
        if (billingPaymentForm.giftCertId.value != "")
        {
          if ( validation.GIFT_CERTIFICATE_ID.value != "OK" )
          {
            serverCheck = false;
            document.forms[0].giftCertId.style.backgroundColor='pink';
            alert(validation.GIFT_CERTIFICATE_ID.value + ". Please correct!");
          }
          else
          {
            document.forms[0].giftCertificateAmount.value = validation.GIFT_CERTIFICATE_AMOUNT.value;
            document.forms[0].giftCertId.style.backgroundColor='white';
            document.forms[0].orderTotalPrice.value = validation.GIFT_ADJUSTED_TOTAL.value;
          }
        }
      }
      closeUpdateWin();
      if (serverCheck == true)
      {
        billingPaymentForm.submit();
      }
    }
    else
    {
      alert ("Validation error.  Please try again.");
    }
    return;
	}

	function fieldExceedsMaxLength(fieldValue, fieldLength)
	{
    if (fieldLength == -1)
    {
      return false;
    }
    else if (fieldValue.length > fieldLength) 
    {
      return true;	
    } 
    else
    {
      return false;
    }
	}

	function applyValidation()
	{
    openUpdateWin();
    var hasValidFields = true;
    hasValidFields = fieldValidationText ("billingFirstName", 20, hasValidFields, true, "A", genericBag);
    hasValidFields = fieldValidationText ("billingLastName", 40, hasValidFields, true, "A", genericBag);
    hasValidFields = fieldValidationText ("businessName", 20, hasValidFields, false, "A", genericBag);
    hasValidFields = fieldValidationText ("billingAddress1", 80, hasValidFields, true, "A", genericBag);
    // need to check for po box  on billing address 1 and 2//
    hasValidFields = fieldValidationText ("billingAddress2", 40, hasValidFields, false, "A", genericBag);
    hasValidFields = fieldValidationText ("contactInformation", 200, hasValidFields, false, "A", genericBag);
    var countryElement = document.forms[0].billingCountry;
    if (countryElement.options[countryElement.selectedIndex].countryType == "I")
		{
      hasValidFields = fieldValidationText ("billingStateText", -1, hasValidFields, true, "A", genericBag);
    }
    else
		{
      hasValidFields = fieldValidationText ("billingZipCode", -1, hasValidFields, true, "A", genericBag);
      hasValidFields = fieldValidationSelect ("billingStateSelect", hasValidFields);
    }

    hasValidFields = fieldValidationText ("billingCity", 40, hasValidFields, true, "A", genericBag);
    hasValidFields = fieldValidationSelect ("billingCountry", hasValidFields, genericBag);
    hasValidFields = fieldValidationText ("eMail", 40, hasValidFields, false, "A", "@._-'");
    hasValidFields = fieldValidationText ("homePhone", 20, hasValidFields, true, "I", "-()");
    hasValidFields = fieldValidationText ("altPhone", 20, hasValidFields, false, "I", "-()");
    hasValidFields = fieldValidationText ("extension", 10, hasValidFields, false, "A", "-()");
    if (document.getElementById("giftCertId") != null)
    {
      var certRequired = document.getElementById("giftCertificateRequired").value;
      if (certRequired == "true")
        hasValidFields = fieldValidationText ("giftCertId", -1, hasValidFields, true, "A", genericBag);
      else
        hasValidFields = fieldValidationText ("giftCertId", -1, hasValidFields, false, "A", genericBag);
    }
    if (document.getElementById("displayMembershipId") == "Y")
    {
      hasValidFields = fieldValidationText ("memberId", 120, hasValidFields, false, "", "");
     // first name required if AADV membership id is entered
      programId = document.forms[0].programId.value.toUpperCase();
			// first name required if AADV membership id is entered
      if ((programId.indexOf('AADV') != -1) && document.getElementById("memberId").value.length >0)
      {
        hasValidFields = fieldValidationText ("memberFirstName", 20, hasValidFields, true, "A", "");
      }
      else     
      {
        hasValidFields = fieldValidationText ("memberFirstName", 20, hasValidFields, false , "A", "");
      }
			// last name required if AADV membership id is entered
      if ((programId.indexOf('AADV') != -1) && document.getElementById("memberId").value.length >0)
      {
        hasValidFields = fieldValidationText ("memberLastName", 40, hasValidFields, true, "A", "");
      }
      else     
      { 
        hasValidFields = fieldValidationText ("memberLastName", 40, hasValidFields, false , "A", "");
      }
    }
		if(hasValidFields == true)
		{
   		applyServerValidation();
		}
		else
		{
			closeUpdateWin();
			alert("Please correct the marked fields.");
		}
		return hasValidFields;
	}

	function checkSpecialPromotion()
	{
		var email = document.forms[0].eMail.value;
		if(email.length == '0')
		{
			document.all.specialPromotions.checked = false;
		}
	}

	function doShoppingCartLocal(nextServlet)
	{
    document.forms[0].forwardToAction.value = "SHOPPING_CART";
    billingPaymentForm.action="http://" + siteName + nextServlet;
    billingPaymentForm.submit();
  }

  function doCancelOrderLocal()
  {
    if(confirm("Cancel your entire order?"))
    {
    	document.location = "http://" + siteName + applicationContext + "/servlet/CancelOrderServlet?sessionId=" + document.forms[0].sessionId.value + "&persistentObjId=" + document.forms[0].persistentObjId.value;
    }
  }


	function processCountryChange()
	{
    if (document.getElementById("dnisType").value == 'JP')
    {
      var countryElement = document.forms[0].billingCountry;
      if (countryElement.options[countryElement.selectedIndex].countryType == "I")
     	{
        openJCPPopup();
			} 
      else 
      {
        changeCountry();
			}
    }
    else
    {
      changeCountry();
    }
	}


	function changeCountry(){
    var countryElement = document.forms[0].billingCountry;
    if (countryElement.value == 'CA')
    {
			  document.getElementById("stateSelect").style.display="block";
	  	  document.getElementById("stateText").style.display="none";
        buildStateDropdown("billingStateSelect", "CAN");
        buildStateDropdown("stateInput", "CAN");
	  }
    else if (countryElement.options[countryElement.selectedIndex].countryType == "I")
    {
			  document.getElementById("stateSelect").style.display="none";
        document.getElementById("stateText").style.display="block";
		}
    else
		{
			  document.getElementById("stateSelect").style.display= "block";
			  document.getElementById("stateText").style.display="none";
        buildStateDropdown("billingStateSelect", "");
        buildStateDropdown("stateInput", "");
		} 
  }
  
	function customerChange()
	{
		document.all.specialPromotionsDIV.style.display = "block";
	}

	function EnterToZip()
	{
    if (window.event.keyCode == enterKeyCode)
    {
      openCityPopup();
	  }
  }

  function EnterToCustomer()
  {
    if (window.event.keyCode == enterKeyCode)
		{
			goSearchCustomer();
		}
  }

	function EnterToCustomerCancel()
	{
	  if (window.event.keyCode == enterKeyCode)
		{
			goCancelSearch();
		}
  }

  function goApplyValidation()
	{
		if (window.event.keyCode == enterKeyCode)
		{
			applyValidation();
		}
	}
	
  // Popup lookup methods //
  function openCustomerLookup()
	{
		//In case the DIV is opened a second time
		document.all.searchphone.style.backgroundColor='white'

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
    
		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		customersearch = (dom)?document.getElementById("customerLookup").style : ie? document.all.customerLookup : document.customerLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset

		customersearch.width = 290
		customersearch.height = 100

		customersearch.top=scroll_top+document.body.clientHeight/2 - 100
		customersearch.left=document.body.clientWidth/2

		customersearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the lookup and place the cursor there
		document.all.searchphone.value = document.all.homePhone.value;
		document.all.searchphone.focus();
	}

	function goSearchCustomer()
	{
		//First validate the phone number input
		var check = true;
		var phone = document.all.searchphone.value;
		var phone = stripWhitespace(phone);

	   //Check to make sure the Phone exists
		if(phone == "")
		{
			if (check == true)
			{
			  check = false;
			}
		}

		if (check == false)
		{
 			document.all.searchphone.style.backgroundColor='pink'
 			document.all.searchphone.focus();
			alert("Please correct the marked fields");
			return false;
		}

		//Every is valid, so open the popup
		var val = document.all.searchphone.value;
		document.all.homePhone.value = val;
		document.all.customerLookupSearching.style.visibility = "visible";

	  var url_source="PopupServlet?POPUP_ID=LOOKUP_CUSTOMER&phoneInput=" + val;
	  var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
	  //Open the window
 	  var ret = window.showModalDialog(url_source,"", modal_dim);

		//in case the X icon is clicked
 		if ( !ret )
 		{
			var ret = new Array();
 			ret[0] = '';
 		}

 		//Populate the text boxes
 		if (ret[0] != '')
 		{
			document.all.billingFirstName.value = ret[1];
			document.all.billingLastName.value = ret[2];
			document.all.billingAddress1.value = ret[3];
			document.all.billingAddress2.value = ret[4];
			document.all.billingCity.value = ret[5];

			//Populate either the dropdown or the text box
			if ( ret[8] == 'US')
			{
				document.all.billingStateSelect.value = ret[6];
			}
			else
			{
				document.all.billingStateText.value = ret[6];
			}

			document.all.billingZipCode.value = ret[7];
			document.all.homePhone.value = ret[9];
			document.all.altPhone.value = ret[10];
			document.all.extension.value = ret[11];
			document.all.eMail.value = ret[12];
			document.all.businessName.value = ret[13];
			document.all.specialPromotions.value = ret[14];

			if(ret[14] == 'Y' || ret[14] == 'N') {
				document.all.specialPromotionsDIV.style.display = "none";
				if(ret[14] == 'Y')
				{
					document.all.specialPromotions.checked = true;
				}
				else
				{
					document.all.specialPromotions.checked = false;
				}
			}
			else 
      {
				document.all.specialPromotionsDIV.style.display = "";
			}
		}

		//set focus on city field
		document.all.billingLastName.focus();

		customersearch.visibility="hidden";
		document.all.customerLookupSearching.style.visibility = "hidden";
	}

	function goCancelSearch()
	{
		document.all.billingLastName.focus();
		customersearch.visibility="hidden"
	}

	function openCityLookup()
	{
		//In case its opened a second time
		document.all.stateInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
		HEIGHT = 100;
		WIDTH = 290;

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		citysearch = (dom)?document.getElementById("cityLookup").style : ie? document.all.cityLookup : document.cityLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		citysearch.top=scroll_top+document.body.clientHeight/2-100

		citysearch.width = WIDTH
		citysearch.height = HEIGHT

		citysearch.left=document.body.clientWidth/2 - WIDTH/2 + 150;
		citysearch.top=scroll_top + document.body.clientHeight/2 - HEIGHT/2;

		citysearch.visibility=(dom||ie)? "visible" : "show"

		document.all.cityInput.value = document.all.billingCity.value;
		document.all.zipCodeInput.value = document.all.billingZipCode.value;

		document.all.stateInput.value = document.all.billingStateSelect.value;

		//set the focus on the city input
		document.all.cityInput.focus();
	}

	function goCancelCity()
	{
		document.all.billingCity.focus();
		citysearch.visibility= "hidden";
	}

	function goSearchCity()
	{
		openCityPopup();
	}

	function openCityPopup()
	{
		//initialize the background colors
		document.all.cityInput.style.backgroundColor='white';
		document.all.stateInput.style.backgroundColor='white';
		document.all.zipCodeInput.style.backgroundColor='white';

		//First validate the inputs
		check = true;
    state = stripWhitespace(document.all.stateInput.value);
    city = stripWhitespace(document.all.cityInput.value);
    zip = stripWhitespace(document.all.zipCodeInput.value);

    //State is required if zip is empty
    if((state.length == 0)&&(zip.length == 0))
    {
      if (check == true)
      {
        document.all.stateInput.focus();
		 	  check = false;
			}
			document.all.stateInput.style.backgroundColor='pink';
		} // close state if

		//City is required if zip is empty
    if((city.length == 0)&&(zip.length == 0))
    {
      if (check == true)
      {
			  document.all.cityInput.focus();
		 	  check = false;
			}
			document.all.cityInput.style.backgroundColor='pink';
		} // close state if


		if (!check)
		{
			alert("Please correct the marked fields")
		 	return false;
		}

    //Now that everything is valid, open the popup
		document.all.cityLookupSearching.style.visibility = "visible";

		var form = document.forms[0];
	  var url_source="PopupServlet?POPUP_ID=LOOKUP_ZIP_CODE" +
        "&cityInput=" + document.all.cityInput.value +
        "&stateInput=" + document.all.stateInput.value +
        "&zipCodeInput=" + document.all.zipCodeInput.value +
        "&countryVal=" + document.all.billingCountry.value;

	  var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	  //Open the window
 	  var ret = window.showModalDialog(url_source,"", modal_dim);

		//in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 			ret[0] = '';
 			ret[1] = '';
 			ret[2] = '';
 		}
 		if (!ret[1])
 			ret[1] ='';

		if (!ret[2])
 			ret[2] ='';

		if(ret[0] != null)
		{
			document.all.billingCity.value = ret[0];
			document.all.billingZipCode.value = ret[1];
			document.all.billingStateSelect.value = ret[2];
		}

		//Set the focus on the next field
		document.all.billingCity.focus();
		citysearch.visibility= "hidden";
		document.all.cityLookupSearching.style.visibility = "hidden";
	}
  
  // JCPenny specific methods //
	function JCPPopChoice(inChoice)
	{
	/***  JC penney popup for international change
	*/
		if(inChoice == 'Y')
		{
			var url= "DNISChangeServlet?";
			url = url.concat(buildURL());
			url = url.concat("&source=billingJCP");

			document.location = url;

		} else {
			document.all.billingCountry.value = 'US';
			closeJCPPopup();
		}
	}

	function openJCPPopup()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		JCPPopV.top=scroll_top+document.body.clientHeight/2-100

		JCPPopV.width=290
		JCPPopV.height=100
		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
	}


