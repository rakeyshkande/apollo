function stopIt()
{
	window.event.returnValue = false;
	return false;
}

function backKeyHandler()
{
	// backspace
	if (window.event && window.event.keyCode == 8)
	{
		var formElement = false;
		
		//alert(document.activeElement.name);
		for(i = 0; i < document.forms[0].elements.length; i++)
		{
			//alert(document.forms[0].elements[i].name + " : " + document.activeElement.name);
			if(document.forms[0].elements[i].name == document.activeElement.name)
			{
				formElement = true;
				break;
			}
		}
		
		if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" ))
		{
			window.event.cancelBubble = true;
			window.event.returnValue = false;
			return false;
		}
	}

	// F5 and F11
	if (window.event.keyCode == 122 || window.event.keyCode == 116) 
	{
		window.event.keyCode = 0;
		event.returnValue = false;
		return false;

	}

	// Atl + Left Arrow
	if(window.event.altLeft)
	{
		window.event.returnValue = false;
		return false;
	}

	// Atl + Right Arrow
	if(window.event.altKey)
	{
		window.event.returnValue = false;
		return false;
	}
	
	// Only allow ctrl-c and ctrl-v
	if(window.event.ctrlKey )
	{
           var pressedKey = String.fromCharCode(event.keyCode).toLowerCase();
           if(pressedKey == "c" || pressedKey == "v")
           {
		window.event.returnValue = true;
		return true;
           }
           else
           {	
		window.event.returnValue = false;
		return false;
           }
	}	
	
	// F12 - Copy GUID to clipboard
	if (window.event.keyCode == 123) 
	{
		document.execCommand("Copy")
		copiedtext=window.clipboardData.setData("Text", persistentObjId);
		
		alert("Copied GUID to Clipboard");
		
		event.returnValue = false;
		return false;
	}		
}


function deCode(strSelection)
{
	//alert(strSelection);
	strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
	strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

	return strSelection;
}


function closeWindow()
{  
    window.close();
}
function doShoppingCart()
{
	if(confirm("Abandon ordering the current item and proceed to the Shopping Cart page?"))
	{	
		document.location = "ShoppingCartServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId;
	}
}
	
function doCancelOrder()
    /***
    *   This function redirects to the Shopping Cart page
    */
{
	if(confirm("Abandon ordering the current item and cancel your entire Order?"))
	{	
		document.location = "CancelOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId;
	}
}
	

function checkOut()
    /***
    *   This function redirects to the Cancel Order page
    */
{
    //alert("You will now proceed to the CHECKOUT Page");
    var url = "BillingServlet?sessionId=" + sessionId_ + "&persistentObjId=" + persistentObjId_;
    //	alert(url);
    if(checkOutEnabled == true)
    {
    	document.location = url;
    }
    else
    {
    	alert("You must have items in your cart in order to check out.");
    }
}

function logoff()
    /***
    *   This function redirects to the Cancel Order page
    */
{
    var url = "LogoutServlet?sessionId=" + document.all.sessionId.value + "&persistentObjId=" + document.all.persistentObjId.value;
    document.location = url;
    //alert("You will now proceed to the Logoff Page, Please Drive Safe");
    
}

function doFAQ()
    /**
      *	This function redirects to the FAQ page
      */
{
	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
	window.showModalDialog("../faq.htm","", modal_dim);

}

function alertHIorAK()
    /**
      *	This function displays is for displaying an alert if the recipient is in Hawaii or Alaska
      */
{
   alert("A $12.99 surcharge will be added to deliver this item to Alaska or Hawaii.");
}

function fieldFocus(fieldName)
{
    if(document.all(fieldName))
    {
	document.all(fieldName).style.borderWidth=3;
        document.all(fieldName).style.borderColor='red';
    }
}

function fieldBlur(fieldName)
{
    if(document.all(fieldName))
    {
        document.all(fieldName).style.borderWidth=2;
        document.all(fieldName).style.borderColor=document.body.style.backgroundColor;
    }
}
