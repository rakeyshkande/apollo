<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="maintServlet" select="root/parameters/maintServlet"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE>Order Entry User Aministration</TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">
	<![CDATA[
	
	var result = "";
	
	function getUser()
	{
		document.all.functionName.value = "getUser";
		submitForm();
	}

	function updateUser()
	{
		document.all.functionName.value = "updateUser";
		if(document.all.activeBox.checked == true)
		{
			document.all.activeFlag.value = "A";			
		}
		else
		{
			document.all.activeFlag.value = "I";	
		}
		
		submitForm();
	}

	function submitForm()	
	{
		document.forms[0].submit();
	}
		
	function init()
	{	
  ]]> 		
		document.forms[0].username.value = "<xsl:value-of select="root/user/userInfo/data/@userId"/>";
		document.forms[0].password.value = "<xsl:value-of select="root/user/userInfo/data/@currentPassword"/>";
		document.forms[0].firstName.value = "<xsl:value-of select="root/user/userInfo/data/@firstName"/>";
		document.forms[0].lastName.value = "<xsl:value-of select="root/user/userInfo/data/@lastName"/>";
		document.forms[0].callCenterId.value = "<xsl:value-of select="root/user/userInfo/data/@callCenterId"/>";
		document.forms[0].roleId.value = "<xsl:value-of select="root/user/userInfo/data/@roleID"/>";
		
		result = "<xsl:value-of select="root/result/@data"/>";
		
		if(result.length > 0)
		{
			document.all.resultDIV.innerText = result;
			document.all.resultDIV.style.color = "red";
			document.all.resultDIV.style.display = "";
		}
		else
		{
			document.all.resultDIV.style.display = "none";
		}
		
		var active = "<xsl:value-of select="root/user/userInfo/data/@activeFlag"/>";
		if(active == "A")
		{
			document.all.activeBox.checked = true;			
		}
		
		document.all.sessionId.value = '<xsl:value-of select="$sessionId"/>';
		document.all.updatePassword.value = "false";
				
	<![CDATA[
	//set the focus on the first field
	document.forms[0].getUserButton.focus();
  	}
	]]>
	
	function backToAdmin()
	{
		var url = '<xsl:value-of select="$maintServlet"/>';
		url = url + "?sessionId=" + document.all.sessionId.value;
		
		//alert(url);
    		document.location = url;    	
	}	
	
	
</SCRIPT>
<link REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></link>
</HEAD>

<BODY onLoad="init();">
<FORM name="form" method="post" action="{$actionServlet}">
	<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
	<INPUT type="hidden" name="functionName" value=""/>
	<INPUT type="hidden" name="updatePassword" value="false" />
	<INPUT type="hidden" name="activeFlag" value="I" />

	<CENTER>
	<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
		<TR>
			<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
			<TD width="20%" align="right"><IMG src="../images/orderEntry.gif" onclick="javascript:backToAdmin();" /></TD>
		</TR>
	
		<TR> <TD colspan="4"> <HR/> </TD> </TR>
	</TABLE>
	
	<TABLE width="98%">
	<TR>
		<TD width="60%">
			<CENTER>
			<TABLE width="100%" border="0">
			<TR>
				<TD colspan="2">
					<DIV id="resultDIV"></DIV>
				</TD>
			</TR>
			<TR>
				<TD align="right">Username: </TD>
				<TD align="left"><INPUT type="text" name="username" value="" /><INPUT type="button" name="getUserButton" value="Get User" onclick="getUser();" /></TD>
			</TR>
			<TR>
				<TD align="right">Password: </TD>
				<TD align="left"><INPUT type="password" name="password" value="" onchange="document.all.updatePassword.value=true;" /></TD>
				<!--<TD align="left"><INPUT type="password" name="password" value="" /></TD>-->
			</TR>
			<TR>
				<TD align="right">First Name: </TD>
				<TD align="left"><INPUT type="text" name="firstName" value="" /></TD>
			</TR>
			<TR>
				<TD align="right">Last Name: </TD>
				<TD align="left"><INPUT type="text" name="lastName" value="" /></TD>
			</TR>
			<TR>
				<TD align="right">Call Center ID: </TD>
				<TD align="left">
					<SELECT name="callCenterId">
						<xsl:for-each select="/root/user/callCenterInfo/data">
							<OPTION value="{@callCenterId}"> <xsl:value-of select="@description"/>&nbsp;-&nbsp;<xsl:value-of select="@callCenterId"/></OPTION>
						</xsl:for-each>
					</SELECT>
				</TD>
			</TR>
			<TR>
				<TD align="right">Role ID: </TD>
				<TD align="left">
					<SELECT name="roleId">
						<xsl:for-each select="/root/user/roleIdInfo/data">
							<OPTION value="{@roleId}"> <xsl:value-of select="@description"/>&nbsp;-&nbsp;<xsl:value-of select="@roleId"/> </OPTION>
						</xsl:for-each>
					</SELECT>
				</TD>
			</TR>
			<TR>
				<TD align="right">Active Flag: </TD>
				<TD align="left">
					<!--<INPUT type="text" name="activeFlag" value="" />-->
					<INPUT type="checkbox" name="activeBox" />
					
				</TD>
			</TR>
			<!--
			<TR>
				<TD align="right">Reset Password?: </TD>
				<TD align="left"><INPUT type="checkbox" name="updatePassword" disabled="true" /></TD>
			</TR>			
			-->
			<TR>
				<TD colspan="2" align="center">					
					<INPUT type="button" name="updateUserButton" value="Update User" onclick="updateUser();" />
				</TD>
			</TR>
			<TR>
				<TD colspan="2" align="left">					
					<BR/>
					Batch update format: <BR/>
					USERNAME,PASSWORD,FIRST_NAME,LAST_NAME,CALL_CENTER_ID,ROLE_ID,ACTIVE_FLAG<BR/>
					<BR/>
					A = Active User<BR/>
					I = Inactive User<BR/>					
					<BR/>
					For Role ID and Call Center ID use the correct code<BR/>
					<BR/>
				</TD>
			</TR>			
			<TR>
				<TD align="right">Batch Update: </TD>
				<TD align="left"><TEXTAREA name="batchUpdate" rows="10" cols="75"/></TD>
			</TR>			
			
			</TABLE>
			</CENTER>
		</TD>
		<TD>
			&nbsp;
		</TD>
	</TR>
	</TABLE>
	
	<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	<TR>
		<TD>
			<xsl:call-template name="footer"/>
		</TD>
	</TR>
	</TABLE>	
	
	</CENTER>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>