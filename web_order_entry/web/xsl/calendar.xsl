<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY spaceCalendar "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="openerWindow" select="root/parameters/openerWindow"/>

<xsl:template match="*|/"><xsl:apply-templates/></xsl:template>
<xsl:template match="text()|@*"><xsl:value-of select="."/></xsl:template>

<xsl:template match="/">

<HTML>
<HEAD>
	<TITLE> FTD - Delivery Date Lookup </TITLE>

	<LINK rel="STYLESHEET" type="text/css" href="../css/ftd.css"/>

	<STYLE type="text/css">
		TD {Font-family:tahoma, Arial, Verdana; font-weight:400; font-size: 8pt; color:#000000;}

		A:link		{Fontfamily:Tahoma, Arial, Verdana;  Font-size: 8pt;  text-decoration: none;}
		A:active	{Fontfamily:Tahoma, Arial, Verdana;  Font-size: 8pt; color: #000000; text-decoration: none;}
		A:visited	{Fontfamily:Tahoma, Arial, Verdana;  Font-size: 8pt; color: #000000; text-decoration: none;}
		A:hover		{Fontfamily:Tahoma, Arial, Verdana;  Font-size: 8pt; color: #00cc00; text-decoration: none;}

	</STYLE>
	
	<script language="javascript">

		var sessionId = "<xsl:value-of select="$sessionId"/>";
		var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
		
<![CDATA[	
		function selectYear()
		{
			productId = document.forms[0].productId.value;
			month = document.forms[0].cMonth.value;
			year = document.forms[0].cYear.value;
			
			document.forms[0].year.value = year;
			document.forms[0].month.value = month;
			
			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId + "&year=" + year + "&month=" + month;
			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CALENDAR";
			document.forms[0].submit();

		}
		
		function selectMonth()
		{
			productId = document.forms[0].productId.value;
			month = document.forms[0].cMonth.value;
			year = document.forms[0].cYear.value;
			
			document.forms[0].year.value = year;
			document.forms[0].month.value = month;

			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId + "&year=" + year + "&month=" + month;

			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CALENDAR";
			document.forms[0].submit();

		}
		
		function previousMonth()
		{
			productId = document.forms[0].productId.value;
			month = document.forms[0].cMonth.value;
			year = document.forms[0].cYear.value;
			var previousDate = new Date(year, month-2, 1);
						
			previousYear = previousDate.getFullYear();
			var previousMonth = previousDate.getMonth() + 1;
			
			document.forms[0].year.value = previousYear;
			document.forms[0].month.value = previousMonth;

			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId + "&year=" + previousYear + "&month=" + previousMonth;

			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CALENDAR";
			document.forms[0].submit();
			
		}
		
		function nextMonth()
		{
			productId = document.forms[0].productId.value;
			month = document.forms[0].cMonth.value;
			year = document.forms[0].cYear.value;
			var nextDate = new Date(year, month, 1);

			nextYear = nextDate.getFullYear();
			nextMonth = nextDate.getMonth() + 1;
			
			document.forms[0].year.value = nextYear;
			document.forms[0].month.value = nextMonth;
			
			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId + "&year=" + nextYear + "&month=" + nextMonth;

			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CALENDAR";
			document.forms[0].submit();
		}
		
		function replaceString(start,end,origStr,subStr)
		{
			var startStr = "";
			var endStr = "";
			var newStr = "";

			startStr = origStr.substring(0, start);
			endStr = origStr.substring(end+1, origStr.length);
			
			if(subStr < 10)
			{
				subStr = "0" + subStr;
			}

			newStr = startStr + subStr + endStr;

			//alert("startStr = " + startStr);
			//alert("endStr = " + endStr);
			//alert("newStr = " + newStr);

			return newStr;
		}
]]>
	</script>
	
	<script language="javascript">
	<![CDATA[
	
		function calYears( value, selected )
		{
			year = value.split(/\s* \s*/);
			year = year.sort();
			for (var i=0; i<year.length; i++)
			{
				document.write("<OPTION value=\"");
				document.write(year[i]);
				document.write("\"");
				if (year[i] == selected) {
					document.write( "selected=\"\"");
				}
				document.write(">");
				document.write(year[i]);
				document.write("</OPTION>");
			}
		}

	]]>
	</script>
	
	<script language="javascript">
		<![CDATA[
		
		function fGetDaysInMonth(iYear, iMonth) {
			var dPrevDate = new Date(iYear, iMonth, 0);
			return(dPrevDate.getDate());
		}
		
		function fBuildCal(iYear, iMonth) {
			var aMonth = new Array();
			aMonth[0] = new Array(7);
			aMonth[1] = new Array(7);
			aMonth[2] = new Array(7);
			aMonth[3] = new Array(7);
			aMonth[4] = new Array(7);
			aMonth[5] = new Array(7);
			var dCalDate = new Date(iYear, iMonth-1, 1);
			var iDayOfFirst = dCalDate.getDay();
			var iDaysInMonth = fGetDaysInMonth(iYear, iMonth);
			var iVarDate = 1;
			var i, d, w;
			for (d = iDayOfFirst; d < 7; d++) {
				aMonth[0][d] = iVarDate;
				iVarDate++;
			}
			for (w = 1; w < 6; w++) {
				for (d = 0; d < 7; d++) {
					if (iVarDate <= iDaysInMonth)
					{
						aMonth[w][d] = iVarDate;
						iVarDate++;
			    	}
		   		}
			}
			
			return aMonth;
		}
	]]>
	</script>
	
	<script language="javascript">
	<![CDATA[
	
		function drawCal(iYear, iMonth)
		{
			var desc;
			var priceDesc;
			var clickDate;
			
			myMonth = fBuildCal( iYear, iMonth );	
			var indexValue = 1;
			var boolInRange = false;
			var rangeStart = 0;
			for (w = 0; w < 6; w++) {
				document.write("<tr>")
				for (d = 0; d < 7; d++) {
					document.write("<TD width=\"50\" align=\"center\">");
					if (!isNaN(myMonth[w][d])) {
						
						desc = "desc" + indexValue;
						priceDesc = "pricedesc" + indexValue;
						
						if ( indexValue < 10 && iMonth < 10 ) {
							clickDate = "0" + iMonth + "/0" + indexValue + "/" + iYear;
						} else if ( indexValue < 10 && iMonth >= 10 ) {
							clickDate = iMonth + "/0" + indexValue + "/" + iYear;
						} else if ( indexValue >=10 && iMonth < 10 ) {
							clickDate = "0" + iMonth + "/" + indexValue + "/" + iYear;
						} else {
							clickDate = iMonth + "/" + indexValue + "/" + iYear;
						}
						
						document.write(" <Input type=\"Button\" id=\"Button4\" name=\"Button4\" ");
												
						]]>							
							<xsl:for-each select="/root/calendarDates/calendar/day">
							 	var v = <xsl:value-of select="@value"/>;
							 	
							 	if ( v == indexValue ) {
							 		var holiday = "<xsl:value-of select="@holiday"/>";
							 		var currDay = "<xsl:value-of select="@currentDay"/>";
							 		var range = "<xsl:value-of select="@range"/>";
							 		
								<![CDATA[							 		
							 		// set range day
							 		if(range == "Y" && boolInRange == false)
							 		{
							 			rangeStart = indexValue;
							 			boolInRange = true;
							 		}
							 		else if(range != "Y")
							 		{
							 			boolInRange = false;
							 			rangeStart = 0;
							 		}
							 	]]>
							 		if ( holiday != '' ) {
						 				document.write(" value=\"" + indexValue + "**\" ");
							 		} else {
						 				document.write(" value=\"" + indexValue + "\" ");
							 		}
							 		
	   								var isDelivable = "<xsl:value-of select="@isDeliverable"/>";

							 		if (isDelivable == "Y") {
							 			document.write(" class=\"DeliverableWithCharge\" ");
							 			//if ((d + 1) % 7 != 0)
							 			//{
								 		if ( currDay != "" ) {
		 									document.write(" style=\"background-color: #FFF1B8\" ");
								 		}
								 		else if(range == "Y")
								 		{
								 			document.write(" style=\"background-color: #EEEEEE\" ");
								 		}
								 		else {
		 									document.write(" style=\"background-color: #E0E0F0\" ");
								 		}
		 								//}
		 								//else
		 								//{
		 								//	document.write(" style=\"background-color: #FFF1B8\" ");
		 								//}	
		 								
		 								 								
		 								if(boolInRange == true)
		 								{
		 									clickDate = replaceString(3, 4, clickDate, rangeStart);
		 								}
		 								
			 							document.write(" onClick=\"javascript:SetDate('" + clickDate + "')\" ");

							 		}
							 		else {
							 			document.write(" class=\"NonDeliverable\" ");
							 		}

									document.write(" onMouseOver=\"showObject(this,''," + desc + "," + priceDesc + ")\" ");
									document.write(" onMouseOut=\"hideObject(this,''," + desc + "," + priceDesc + ")\" ");
							 	}
							
							</xsl:for-each>							
							
						<![CDATA[

						document.write(" />");
						
  						indexValue++;
					} else {
						if ( w != 5 )
						{
							document.write("<Input type=\"Button\" class=\"BlankDay\" value=\"\" id=\"Button4\" name=\"Button4\"/>");
						} else {
							if (!isNaN(myMonth[5][0]))
								document.write("<Input type=\"Button\" class=\"BlankDay\" value=\"\" id=\"Button4\" name=\"Button4\"/>");
						}
					}
					document.write("</td>")
				}
				document.write("</tr>");
			}
		}
	]]>
</script>

	<SCRIPT language="javascript">
	<![CDATA[
		function SetDate(aDate)
		{
	//		alert( "setDate: " + aDate );
		
			window.returnValue = aDate;
	//		opener.document.forms[0].deliveryDates.value = aDate;
			window.close();			

	   }
	   
	    /* Show an object */
	    function showObject(o,col,object,object1)
	    {
	        object.visibility = VISIBLE;
	        object1.visibility = VISIBLE;
	    }

	   /* Hide an object */
	    function hideObject(o,col,object,object1)
	    {
	        object.visibility = HIDDEN;
	        object1.visibility = HIDDEN;
	    }		
	    
	    function viewCalendar(month, year)
		{
			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR&month=" + month + "&year=" + year;
	  		var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
	 	    var ret = window.showModalDialog(url_source,"", modal_dim);
		}
		function init()
		{
			window.name = "VIEW_CALENDAR";
		}
	]]>
	</SCRIPT>
</HEAD>

<BODY onLoad="javascript:window.focus(); init();" bgcolor="#FFFFFF" leftmargin="5" topmargin="2" rightmargin="5" bottommargin="5">
	<FORM method="get" action="{$actionServlet}" name="form">
	
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>		
		
		<input type="hidden" name="productId">
			<xsl:attribute name="value"><xsl:value-of select="/root/calendarDates/product/data/@productId"/></xsl:attribute>
		</input>
		<input type="hidden" name="year" value=""/>
		<input type="hidden" name="month" value=""/>
		<input type="hidden" name="POPUP_ID" value="DISPLAY_CALENDAR"/>
		
		<CENTER>
		<TABLE border="0" width="292" height="25" cellpadding="0" cellspacing="0">
			<TR> <TD> &nbsp; </TD> </TR>

			<TR>
				<TD>
					<TABLE border="0" width="292" height="25" cellpadding="0" cellspacing="0">
						<TR>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
							<TD valign="bottom" height="1" bgcolor="#FFCC00"><spacer type="block" height="1"></spacer></TD>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
						</TR>
						<TR>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
							<TD align="center" class="body12" bgcolor="#FFF1B8"><b>Click the day you would like your order delivered:</b></TD>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
						</TR>
						<TR>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
							<TD valign="bottom" height="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
							<TD valign="bottom" width="1" bgcolor="#FFCC00"><spacer type="block" width="1"></spacer></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>

			<TR> <TD height="5"> &nbsp; </TD> </TR>

			<TR>
				<TD>
					<TABLE border="0" width="292" height="40" cellpadding="0" cellspacing="0">
						<TR>
							<TD width="37">							
								<img src="../images/cal_del_prev.gif" width="37" height="40" border="0" onclick="javascript:previousMonth();" alt=""/>							
							</TD>
							<TD width="218" background="../images/cal_del_back.gif" align="center">
								<SELECT class="INPUTComb" onChange="javascript:selectMonth();" id="cMonth" name="cMonth">
									<OPTION value="1" ><xsl:if test="/root/calendarDates/calendar/@month = 1"><xsl:attribute name="selected"></xsl:attribute></xsl:if>January</OPTION>
									<OPTION value="2" ><xsl:if test="/root/calendarDates/calendar/@month = 2"><xsl:attribute name="selected"></xsl:attribute></xsl:if>February</OPTION>
									<OPTION value="3" ><xsl:if test="/root/calendarDates/calendar/@month = 3"><xsl:attribute name="selected"></xsl:attribute></xsl:if>March</OPTION>
									<OPTION value="4" ><xsl:if test="/root/calendarDates/calendar/@month = 4"><xsl:attribute name="selected"></xsl:attribute></xsl:if>April</OPTION>
									<OPTION value="5" ><xsl:if test="/root/calendarDates/calendar/@month = 5"><xsl:attribute name="selected"></xsl:attribute></xsl:if>May</OPTION>
									<OPTION value="6" ><xsl:if test="/root/calendarDates/calendar/@month = 6"><xsl:attribute name="selected"></xsl:attribute></xsl:if>June</OPTION>
									<OPTION value="7" ><xsl:if test="/root/calendarDates/calendar/@month = 7"><xsl:attribute name="selected"></xsl:attribute></xsl:if>July</OPTION>
									<OPTION value="8" ><xsl:if test="/root/calendarDates/calendar/@month = 8"><xsl:attribute name="selected"></xsl:attribute></xsl:if>August</OPTION>
									<OPTION value="9" ><xsl:if test="/root/calendarDates/calendar/@month = 9"><xsl:attribute name="selected"></xsl:attribute></xsl:if>September</OPTION>
									<OPTION value="10" ><xsl:if test="/root/calendarDates/calendar/@month = 10"><xsl:attribute name="selected"></xsl:attribute></xsl:if>October</OPTION>
									<OPTION value="11" ><xsl:if test="/root/calendarDates/calendar/@month = 11"><xsl:attribute name="selected"></xsl:attribute></xsl:if>November</OPTION>
									<OPTION value="12" ><xsl:if test="/root/calendarDates/calendar/@month = 12"><xsl:attribute name="selected"></xsl:attribute></xsl:if>December</OPTION>
								</SELECT>
								&nbsp;
								<SELECT class="INPUTComb" onChange="javascript:selectYear();" id="cYear" name="cYear">
									<script language="javascript">
										calYears("<xsl:value-of select="/root/calendarDates/years/@value"/>", "<xsl:value-of select="/root/calendarDates/@year"/>");
									</script>									
								</SELECT>
							</TD>
						
							<TD width="37">							
								<img src="../images/cal_del_next.gif" width="37" height="40" border="0" onclick="javascript:nextMonth();" alt=""/>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE border="0" height="20" width="290" cellpadding="0" cellspacing="0">
			<TR>
				<TD width="50" align="center" class="label">Sun</TD>
				<TD width="50" align="center" class="label">Mon</TD>
				<TD width="50" align="center" class="label">Tue</TD>
				<TD width="50" align="center" class="label">Wed</TD>
				<TD width="50" align="center" class="label">Thu</TD>
				<TD width="50" align="center" class="label">Fri</TD>
				<TD width="50" align="center" class="label">Sat</TD>
			</TR>
		</TABLE>
		
		<DIV ID="displayText" CLASS="displayText">
			<TABLE  border="0" width="290" cellpadding="0" cellspacing="0">		
				<script language="javascript">
					drawCal("<xsl:value-of select="/root/calendarDates/calendar/@year"/>", "<xsl:value-of select="/root/calendarDates/calendar/@month"/>");
				</script>
			</TABLE>
		</DIV>
		
		<TABLE border="0" width="290" cellpadding="0" cellspacing="0">
			<tr >
				<td>
				<small>&nbsp;</small>
				</td>
			</tr>
			<TR>
				<TD>
					<DIV ID="Instruction" class="InstructionMsg_ie_5">					
						<li> ** - denotes special day.<br></br></li>
						<li> Dates in gray are not available for delivery.</li><br></br>
						<li> Shipping charges vary by color.</li><br></br>
						<li> Move your cursor over the highlighted dates to see the <br></br></li>
						&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;shipping charges.
						<li> Click the desired delivery date to select.<BR></BR></li>					
					</DIV>
				</TD>
			</TR>

		
			<TR>
				<TD>
					<DIV ID="desc0" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc1" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc2" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc3" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc4" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc5" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc6" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc7" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc8" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc9" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc10" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc11" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc12" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc13" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc14" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc15" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc16" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc17" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc18" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc19" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc20" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc21" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc22" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc23" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc24" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc25" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc26" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc27" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc28" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc29" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc30" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc31" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc32" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc33" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc34" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc35" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc36" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc37" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc38" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc39" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc40" CLASS="desc_ie_5"></DIV>
					<DIV ID="desc41" CLASS="desc_ie_5"></DIV>
					
					<DIV ID="pricedesc0" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='0']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='0']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='0']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='0']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='0']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc1" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='1']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='1']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='1']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='1']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='1']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc2" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='2']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='2']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='2']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='2']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='2']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc3" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='3']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='3']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='3']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='3']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='3']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc4" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='4']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='4']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='4']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='4']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='4']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc5" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='5']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='5']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='5']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='5']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='5']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc6" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='6']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='6']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='6']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='6']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='6']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc7" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='7']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='7']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='7']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='7']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='7']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc8" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='8']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='8']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='8']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='8']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='8']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc9" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='9']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='9']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='9']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='9']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='9']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc10" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='10']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='10']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='10']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='10']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='10']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc11" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='11']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='11']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='11']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='11']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='11']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc12" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='12']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='12']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='12']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='12']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='12']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc13" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='13']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='13']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='13']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='13']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='13']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc14" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='14']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='14']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='14']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='14']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='14']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc15" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='15']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='15']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='15']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='15']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='15']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc16" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='16']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='16']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='16']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='16']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='16']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc17" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='17']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='17']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='17']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='17']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='17']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc18" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='18']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='18']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='18']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='18']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='18']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc19" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='19']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='19']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='19']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='19']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='19']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc20" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='20']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='20']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='20']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='20']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='20']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc21" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='21']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='21']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='21']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='21']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='21']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc22" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='22']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='22']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='22']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='22']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='22']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc23" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='23']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='23']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='23']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='23']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='23']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc24" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='24']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='24']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='24']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='24']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='24']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc25" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='25']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='25']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='25']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='25']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='25']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc26" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='26']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='26']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='26']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='26']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='26']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc27" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='27']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='27']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='27']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='27']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='27']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc28" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='28']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='28']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='28']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='28']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='28']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc29" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='29']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='29']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='29']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='29']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='29']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc30" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='30']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='30']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='30']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='30']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='30']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc31" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='31']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='31']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='31']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='31']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='31']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc32" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='32']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='32']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='32']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='32']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='32']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc33" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='33']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='33']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='33']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='33']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='33']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc34" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='34']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='34']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='34']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='34']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='34']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc35" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='35']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='35']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='35']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='35']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='35']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc36" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='36']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='36']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='36']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='36']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='36']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc37" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='37']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='37']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='37']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='37']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='37']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc38" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='38']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='38']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='38']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='38']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='38']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc39" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='39']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='39']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='39']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='39']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='39']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc40" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='40']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='40']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='40']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='40']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='40']/@charge"/></xsl:if> </DIV>
					<DIV ID="pricedesc41" CLASS="pricedesc_ie_5"> <BR></BR> &spaceCalendar; <xsl:if test="/root/calendarDates/calendar/day[@value='41']/@currentDay ='Y'"><SPAN class="label">Today</SPAN> <BR></BR> &spaceCalendar; </xsl:if> <xsl:if test="/root/calendarDates/calendar/day[@value='41']/@holiday !=''"><SPAN class="label"><xsl:value-of select="/root/calendarDates/calendar/day[@value='41']/@holiday"/> </SPAN> <BR></BR>&spaceCalendar; </xsl:if><xsl:if test="/root/calendarDates/calendar/day[@value='41']/@isDeliverable ='Y'">Estimated service charge for this item is $<xsl:value-of select="/root/calendarDates/calendar/day[@value='41']/@charge"/></xsl:if> </DIV>
				</TD>
			</TR>
		</TABLE>
	</CENTER>
	</FORM>
</BODY>
</HTML>

<SCRIPT LANGUAGE = "JavaScript">
	<![CDATA[
    /* Finding the Browser Version detection */
    var isNS = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 4);

    /* These constants were not discussed in the article. They can be used in place
       of hidden and visible because on occasion Navigator has problems with the two */
    var HIDDEN = (isNS) ? 'hide' : 'hidden';
    var VISIBLE = (isNS) ? 'show' : 'visible';

   /* Create shortcut variables for different absolutely positioned elements */
    var displayText = (isNS) ? document.displayText : document.all.displayText.style;
    var desc0 = (isNS) ? document.desc0 : document.all.desc0.style;
    var desc1 = (isNS) ? document.desc1 : document.all.desc1.style;
    var desc2 = (isNS) ? document.desc2 : document.all.desc2.style;
    var desc3 = (isNS) ? document.desc3 : document.all.desc3.style;
    var desc4 = (isNS) ? document.desc4 : document.all.desc4.style;
    var desc5 = (isNS) ? document.desc5 : document.all.desc5.style;
    var desc6 = (isNS) ? document.desc6 : document.all.desc6.style;
    var desc7 = (isNS) ? document.desc7 : document.all.desc7.style;
    var desc8 = (isNS) ? document.desc8 : document.all.desc8.style;
    var desc9 = (isNS) ? document.desc9 : document.all.desc9.style;
    var desc10 = (isNS) ? document.desc10 : document.all.desc10.style;
    var desc11 = (isNS) ? document.desc11 : document.all.desc11.style;
    var desc12 = (isNS) ? document.desc12 : document.all.desc12.style;
    var desc13 = (isNS) ? document.desc13 : document.all.desc13.style;
    var desc14 = (isNS) ? document.desc14 : document.all.desc14.style;
    var desc15 = (isNS) ? document.desc15 : document.all.desc15.style;
    var desc16 = (isNS) ? document.desc16 : document.all.desc16.style
    var desc17 = (isNS) ? document.desc17 : document.all.desc17.style;
    var desc18 = (isNS) ? document.desc18 : document.all.desc18.style;
    var desc19 = (isNS) ? document.desc19 : document.all.desc19.style;
    var desc20 = (isNS) ? document.desc20 : document.all.desc20.style;
    var desc21 = (isNS) ? document.desc21 : document.all.desc21.style;
    var desc22 = (isNS) ? document.desc22 : document.all.desc22.style;
    var desc23 = (isNS) ? document.desc23 : document.all.desc23.style;
    var desc24 = (isNS) ? document.desc24 : document.all.desc24.style;
    var desc25 = (isNS) ? document.desc25 : document.all.desc25.style;
    var desc26 = (isNS) ? document.desc26 : document.all.desc26.style;
    var desc27 = (isNS) ? document.desc27 : document.all.desc27.style;
    var desc28 = (isNS) ? document.desc28 : document.all.desc28.style;
    var desc29 = (isNS) ? document.desc29 : document.all.desc29.style;
    var desc30 = (isNS) ? document.desc30 : document.all.desc30.style;
    var desc31 = (isNS) ? document.desc31 : document.all.desc31.style;
    var desc32 = (isNS) ? document.desc32 : document.all.desc32.style;
    var desc33 = (isNS) ? document.desc33 : document.all.desc33.style;
    var desc34 = (isNS) ? document.desc34 : document.all.desc34.style;
    var desc35 = (isNS) ? document.desc35 : document.all.desc35.style;
    var desc36 = (isNS) ? document.desc36 : document.all.desc36.style;
    var desc37 = (isNS) ? document.desc37 : document.all.desc37.style;
    var desc38 = (isNS) ? document.desc38 : document.all.desc38.style;
    var desc39 = (isNS) ? document.desc39 : document.all.desc39.style;
    var desc40 = (isNS) ? document.desc40 : document.all.desc40.style;
    var desc41 = (isNS) ? document.desc41 : document.all.desc41.style;

    var pricedesc0 = (isNS) ? document.pricedesc0 : document.all.pricedesc0.style;
    var pricedesc1 = (isNS) ? document.pricedesc1 : document.all.pricedesc1.style;
    var pricedesc2 = (isNS) ? document.pricedesc2 : document.all.pricedesc2.style;
    var pricedesc3 = (isNS) ? document.pricedesc3 : document.all.pricedesc3.style;
    var pricedesc4 = (isNS) ? document.pricedesc4 : document.all.pricedesc4.style;
    var pricedesc5 = (isNS) ? document.pricedesc5 : document.all.pricedesc5.style;
    var pricedesc6 = (isNS) ? document.pricedesc6 : document.all.pricedesc6.style;
    var pricedesc7 = (isNS) ? document.pricedesc7 : document.all.pricedesc7.style;
    var pricedesc8 = (isNS) ? document.pricedesc8 : document.all.pricedesc8.style;
    var pricedesc9 = (isNS) ? document.pricedesc9 : document.all.pricedesc9.style;
    var pricedesc10 = (isNS) ? document.pricedesc10 : document.all.pricedesc10.style;
    var pricedesc11 = (isNS) ? document.pricedesc11 : document.all.pricedesc11.style;
    var pricedesc12 = (isNS) ? document.pricedesc12 : document.all.pricedesc12.style;
    var pricedesc13 = (isNS) ? document.pricedesc13 : document.all.pricedesc13.style;
    var pricedesc14 = (isNS) ? document.pricedesc14 : document.all.pricedesc14.style;
    var pricedesc15 = (isNS) ? document.pricedesc15 : document.all.pricedesc15.style;
    var pricedesc16 = (isNS) ? document.pricedesc16 : document.all.pricedesc16.style;
    var pricedesc17 = (isNS) ? document.pricedesc17 : document.all.pricedesc17.style;
    var pricedesc18 = (isNS) ? document.pricedesc18 : document.all.pricedesc18.style;
    var pricedesc19 = (isNS) ? document.pricedesc19 : document.all.pricedesc19.style;
    var pricedesc20 = (isNS) ? document.pricedesc20 : document.all.pricedesc20.style;
    var pricedesc21 = (isNS) ? document.pricedesc21 : document.all.pricedesc21.style;
    var pricedesc22 = (isNS) ? document.pricedesc22 : document.all.pricedesc22.style;
    var pricedesc23 = (isNS) ? document.pricedesc23 : document.all.pricedesc23.style;
    var pricedesc24 = (isNS) ? document.pricedesc24 : document.all.pricedesc24.style;
    var pricedesc25 = (isNS) ? document.pricedesc25 : document.all.pricedesc25.style;
    var pricedesc26 = (isNS) ? document.pricedesc26 : document.all.pricedesc26.style;
    var pricedesc27 = (isNS) ? document.pricedesc27 : document.all.pricedesc27.style;
    var pricedesc28 = (isNS) ? document.pricedesc28 : document.all.pricedesc28.style;
    var pricedesc29 = (isNS) ? document.pricedesc29 : document.all.pricedesc29.style;
    var pricedesc30 = (isNS) ? document.pricedesc30 : document.all.pricedesc30.style;
    var pricedesc31 = (isNS) ? document.pricedesc31 : document.all.pricedesc31.style;
    var pricedesc32 = (isNS) ? document.pricedesc32 : document.all.pricedesc32.style;
    var pricedesc33 = (isNS) ? document.pricedesc33 : document.all.pricedesc33.style;
    var pricedesc34 = (isNS) ? document.pricedesc34 : document.all.pricedesc34.style;
    var pricedesc35 = (isNS) ? document.pricedesc35 : document.all.pricedesc35.style;
    var pricedesc36 = (isNS) ? document.pricedesc36 : document.all.pricedesc36.style;
    var pricedesc37 = (isNS) ? document.pricedesc37 : document.all.pricedesc37.style;
    var pricedesc38 = (isNS) ? document.pricedesc38 : document.all.pricedesc38.style;
    var pricedesc39 = (isNS) ? document.pricedesc39 : document.all.pricedesc39.style;
    var pricedesc40 = (isNS) ? document.pricedesc40 : document.all.pricedesc40.style;
   ]]>
</SCRIPT>

</xsl:template>

</xsl:stylesheet>