<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY sepd "&#168;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="pdbServlet" select="root/parameters/pdbServlet"/>
<xsl:variable name="userAdminServlet" select="root/parameters/userAdminServlet"/>

<xsl:template match="/">

<script language="javascript" src="../js/FormChek.js"> </script>
<script language="javascript" src="../js/util.js"> </script>

<HTML>
<HEAD>
	<TITLE> FTD - Site Administration </TITLE>
	
	<SCRIPT language="JavaScript">
		var sessionId = '<xsl:value-of select="$sessionId"/>';
		var actionServlet = '<xsl:value-of select="$actionServlet"/>';
		var pdbServlet = '<xsl:value-of select="$pdbServlet"/>';
		var userAdminServlet = '<xsl:value-of select="$userAdminServlet"/>';
		
		var showPDBMaint = "<xsl:value-of select="/root/functionData/data[@name='pdbMaintFlag']/@value"/>";
		var showEditUser = "<xsl:value-of select="/root/functionData/data[@name='editUserMaintFlag']/@value"/>";
		var showUserMaint = "<xsl:value-of select="/root/functionData/data[@name='userMaintFlag']/@value"/>";
		var showListUsers = "<xsl:value-of select="/root/functionData/data[@name='listUserMaintFlag']/@value"/>";
		
		
		function init()
		{
			document.all.sessionId.value = sessionId;
			
			if(showPDBMaint != "Y")
			{
				document.all.pdbMaintDiv.style.display="none";
			}
			
			if(showUserMaint != "Y")
			{
				document.all.userMaintDiv.style.display="none";
			}
			
			if(showEditUser != "Y")
			{
				document.all.editUserMaintDiv.style.display="none";
			}
			
			if(showListUsers != "Y")
			{
				document.all.listUserMaintDiv.style.display="none";
			}			
		}
		
		function submitPage(theFunction)
		{
			var url = "";
			
			init();
			
			if(theFunction == "product")
			{
				url = pdbServlet + "/showProductMaintList.do";
			}
			else if(theFunction == "imageFTP")
			{
				url = pdbServlet + "/showImageFTP.do";
			}
			else if(theFunction == "sendMercuryRecipe")
			{
				url = pdbServlet + "/showSendMercuryRecipe.do";
			}
			else if(theFunction == "occasionMaint")
			{
				url = pdbServlet + "/showOccasionMaintenance.do";
			}			
			else if(theFunction == "upsellMaint")
			{
				url = pdbServlet + "/showUpsellMaintList.do";
			}			

			else if(theFunction == "holidayPricingActivate")
			{
				url = pdbServlet + "/showHolidayPricingActivate.do";
			}
			else if(theFunction == "productHolidayPricing")
			{
				url = pdbServlet + "/showProductHolidayList.do";
			}			
			else if(theFunction == "shippingKey")
			{
				url = pdbServlet + "/showShippingKeyControl.do";
			}
			else if(theFunction == "aribaCIFCatelog")
			{
				url = pdbServlet + "/createCIFcatalog.do";
			}
			else if(theFunction == "addCompany")
			{
				url = pdbServlet + "/companyInsert.do";
			}
			else if(theFunction == "updateCompany")
			{
				url = pdbServlet + "/showCompanyUpdateDelete.do";
			}
			else if(theFunction == "orderEntry")
			{
				url = actionServlet;
			}
			else if(theFunction == "EditUser")
			{
				url = userAdminServlet;
			}
			else if(theFunction == "sourceCode")
			{
				url = pdbServlet + "/showCPNList.do";
			}
			
			url = url + "?sessionId=" + document.all.sessionId.value;
    			document.location = url;
		}
	
	</SCRIPT>
	
	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY onload="init();" marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
	<FORM name="adminform" method="post" action="/oedev/servlet/LoginServlet" target="_top">
		<input type="hidden" name="sessionId"/>
		<CENTER>
		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
				<TD width="20%" align="right"><IMG src="../images/orderEntry.gif" onclick="javascript:submitPage('orderEntry');" /></TD>
			</TR>
	
			<TR> <TD colspan="4"> <HR/> </TD> </TR>
		</TABLE>
	    <!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
		<TR>
			<TD>	
				<DIV id="pdbMaintDiv">
				<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD width="15%" class="Header3"> Maintenance </TD>
					<TD width="85%"> &nbsp; </TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('product');">
						    Product Maintenance
						</a>                     
					</TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('imageFTP');">
						    FTP Image Upload
						</a>
					</TD>
				</TR>                
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('sendMercuryRecipe');">
						    Mercury Recipe Maintenance 
						</a>
					</TD>  
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('occasionMaint');">
							Occasion Maintenance 
						</a>
					</TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('upsellMaint');">
							Upsell Maintenance 
						</a>
					</TD>
				</TR>
				<TR>
					<TD> <hr/> </TD>
					<TD> &nbsp; </TD>
				</TR>
				<TR>
					<TD class="Header3"> Holiday Pricing </TD>
					<TD> &nbsp; </TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
					<TD>
						<a href="javascript:submitPage('holidayPricingActivate');">
						Holiday Pricing Activate
						</a>
					</TD>
				</TR>                   
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('productHolidayPricing');">
					    	Display Products with Holiday Pricing
						</a>
				    	</TD>
				</TR>
				<TR>
					<TD> <hr/> </TD>
					<TD> &nbsp; </TD>
				</TR>
				<TR>
					<TD class="Header3"> Shipping Key </TD>
					<TD> &nbsp; </TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('shippingKey');">
					    	Shipping Key Control Page
						</a>
				    	</TD>
				</TR>
				<TR>
					<TD> <hr/> </TD>
					<TD> &nbsp; </TD>
				</TR>
				<TR>
				    	<TD class="Header3"> Ariba </TD>
					<TD> &nbsp; </TD>
				</TR>        
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('aribaCIFCatelog');">
					    	Create Ariba CIF Catalog
						</a>
				    	</TD>
				</TR>
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('addCompany');">
					    	Add Company to Database
						</a>
				    	</TD>
				</TR>                
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('updateCompany');">
					    	Update\Delete Existing Company
						</a>
				    	</TD>
				</TR>			
				<TR>
					<TD> &nbsp; </TD>
				    	<TD>
						<a href="javascript:submitPage('sourceCode');">
					    	Source Code / Product Cross Reference Maintenance
						</a>
				    	</TD>
				</TR>				
				<TR>
					<TD> <hr/> </TD>
					<TD> &nbsp; </TD>
				</TR>
			</TABLE>
			</DIV>
		</TD>
		</TR>
		<TR>
		<TD>
			<DIV id="userMaintDiv">
				<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD>
						<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
						<TR>
						<TD width="15%" class="Header3">User Maintenance</TD>
						<TD width="85%"> &nbsp; </TD>
						</TR>						
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<DIV id="editUserMaintDiv">
						<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
						<TR>
						<TD width="15%"> &nbsp; </TD>
						<TD width="85%">
						<a href="javascript:submitPage('EditUser');">
						Add / Edit User
						</a>
				    		</TD>
						</TR>					
						</TABLE>
						</DIV>
					</TD>
				</TR>
				<TR>
					<TD>
						<DIV id="listUserMaintDiv">
						<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
						<TR>
						<TD width="15%"> &nbsp; </TD>
						<TD width="85%">
						<a href="javascript:submitPage('ListUsers');">
						List Users
						</a>
				    		</TD>
						</TR>					
						</TABLE>
						</DIV>
					</TD>
				</TR>				
				<TR>
					<TD>
						<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
						<TR>
						<TD width="15%"><hr/></TD>
						<TD width="85%">&nbsp;</TD>
						</TR>					
						</TABLE>
					</TD>
				</TR>				
				</TABLE>
			</DIV>
		</TD>
		</TR>
		</TABLE>
		
		<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
		<TR>
			<TD>
				<xsl:call-template name="footer"/>
			</TD>
		</TR>
		</TABLE>
	</CENTER>
	</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>