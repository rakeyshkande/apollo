<!DOCTYPE ACDemo [
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:template match="/">


<HTML>
<BODY>
<xsl:for-each select="/root/validation">
<INPUT TYPE="HIDDEN">
	<xsl:attribute name="NAME"><xsl:value-of select="@object"/></xsl:attribute>
	<xsl:attribute name="VALUE"><xsl:value-of select="@result"/></xsl:attribute>
</INPUT> 
	
</xsl:for-each>

</BODY>
<script type="text/javascript">
<![CDATA[
	window.parent.onValidationResponse(document.all);
	window.parent.window._notifyValidatonSuccess_();
]]>
</script>
</HTML>

</xsl:template>
</xsl:stylesheet>