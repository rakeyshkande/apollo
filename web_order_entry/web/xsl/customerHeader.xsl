<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:template name="customerHeader">

Current customer is: <font color="#0000FF"><xsl:value-of select="/root/pageHeader/headerDetail[@name='customerFirstName']/@value"/> &nbsp; <xsl:value-of select="/root/pageHeader/headerDetail[@name='customerLastName']/@value"/></font>
<br></br>
Source Code is:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#0000FF"><xsl:value-of select="/root/pageHeader/headerDetail[@name='sourceCodeSelect']/@value"/> - <xsl:value-of select="/root/pageHeader/headerDetail[@name='sourceCodeText']/@value"/></font>

</xsl:template>
</xsl:stylesheet>