<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="dnisType" select="root/parameters/dnisType"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> Source Code Lookup </TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">

<![CDATA[
	function onKeyDown()
	{
		if ( window.event.keyCode == 13 )
		{
			reopenPopup();
			return false;
		}
	}

	function closeSourceLookup(sourceCode)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(sourceCode);
		}
	}

	//These variables are used for writing the anchor tags
	//var destination = "";
	//var previous = "";
	var sourceCodeDesc = "";

	//This function populates the introduction page with the selected source code
	function populatePage(sourceCode)
	{
		var retArray = new Array();
		retArray[0] = sourceCode;
		retArray[1] = sourceCodeDesc;
		window.returnValue = retArray;
		window.close();
	}

	//This function submits the page and populates it with new search results
	function reopenPopup(letter)
	{
		//If the user keys in a parameter, rather then clicking on a letter
		if(!letter)
			letter = document.forms[0].sourceCodeInput.value;

		//Populate the text box with the selected letter
		if(letter != '')
		{
			document.forms[0].sourceCodeInput.value = letter;
		}

		//First validate the Source Code input
		var check = true;
		var sourceCode = document.forms[0].sourceCodeInput.value;
		var sourceCode = stripWhitespace(sourceCode);

		if(sourceCode == "" || sourceCode.length < 2)
		{
		   document.forms[0].sourceCodeInput.focus();
           document.forms[0].sourceCodeInput.style.backgroundColor='pink'
           check = false;
           alert("Please correct the marked fields");
		}

		//Post to the servlet if no errors
		if (check)
		{
			document.forms[0].sourceCodeInput.value = letter;
			var val = document.forms[0].sourceCodeInput.value;
			var url_source="";
			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_SOURCE_CODE_LOOKUP";
			document.forms[0].submit();
		}
	}


	function init()
	{
		window.name = "VIEW_SOURCE_CODE_LOOKUP";

		//set the focus on the first field
		document.forms[0].sourceCodeInput.focus();
		]]>
		document.forms[0].dnisType.value = "<xsl:value-of select="$dnisType"/>";
		<![CDATA[

  	}

	]]>
</SCRIPT>

<LINK REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></LINK>

</HEAD>

<BODY onLoad="javascript:window.focus(); init();">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_SOURCE_CODE"/>
<CENTER>
    <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
            <INPUT name="dnisType" type="hidden"/>
    <TR>
        <TD align="center">
            <H1>Source Code Lookup </H1>
        </TD>
    </TR>
    <TR>
        <TD nowrap="true" colspan="3" align="left">
           	<SPAN class="instruction"> Enter Source Code or Description value </SPAN> &nbsp;
            <INPUT tabindex="1" name="sourceCodeInput" type="text" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('sourceCodeInput')" onblur="javascript:fieldBlur('sourceCodeInput')"/>
            <SPAN class="instruction"> and press </SPAN>
          	<IMG tabindex="2" onkeydown="javascript:onKeyDown();" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()"/>
   	    </TD>
    </TR>
    	<!--
	<TR>
		<TD colspan="3" class="instruction"> Select first character of the <b>Name/Description</b> &nbsp;to locate: </TD>
	</TR>
	
	<TR>
		<TD>
			<SPAN>
			<FONT color="blue">
			<A onclick="javascript: reopenPopup('A')" style="cursor:hand;"><U>A</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('B')" style="cursor:hand;"><U>B</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('C')" style="cursor:hand;"><U>C</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('D')" style="cursor:hand;"><U>D</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('E')" style="cursor:hand;"><U>E</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('F')" style="cursor:hand;"><U>F</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('G')" style="cursor:hand;"><U>G</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('H')" style="cursor:hand;"><U>H</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('I')" style="cursor:hand;"><U>I</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('J')" style="cursor:hand;"><U>J</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('K')" style="cursor:hand;"><U>K</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('L')" style="cursor:hand;"><U>L</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('M')" style="cursor:hand;"><U>M</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('N')" style="cursor:hand;"><U>N</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('O')" style="cursor:hand;"><U>O</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('P')" style="cursor:hand;"><U>P</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('Q')" style="cursor:hand;"><U>Q</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('R')" style="cursor:hand;"><U>R</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('S')" style="cursor:hand;"><U>S</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('T')" style="cursor:hand;"><U>T</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('U')" style="cursor:hand;"><U>U</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('V')" style="cursor:hand;"><U>V</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('W')" style="cursor:hand;"><U>W</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('X')" style="cursor:hand;"><U>X</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('Y')" style="cursor:hand;"><U>Y</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('Z')" style="cursor:hand;"><U>Z</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('0')" style="cursor:hand;"><U>0</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('1')" style="cursor:hand;"><U>1</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('2')" style="cursor:hand;"><U>2</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('3')" style="cursor:hand;"><U>3</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('4')" style="cursor:hand;"><U>4</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('5')" style="cursor:hand;"><U>5</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('6')" style="cursor:hand;"><U>6</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('7')" style="cursor:hand;"><U>7</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('8')" style="cursor:hand;"><U>8</U></A> &nbsp;&nbsp;
			<A onclick="javascript: reopenPopup('9')" style="cursor:hand;"><U>9</U></A> &nbsp;&nbsp;
			</FONT>
			</SPAN>
		</TD>
	</TR>
	-->
	<TR>
		<TD>
			<HR></HR>
		</TD>
	</TR>
	<TR>
		<TD colspan="3">
			<SCRIPT>
			<![CDATA[
			/*
	 		var anchors = "";
	 		var allAnchors = "$0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	 		var i = 0;
			var j = 0;
			var current="";
			]]>
				<xsl:for-each select="/root/anchors/anchor">
					anchors = anchors + "<xsl:value-of select="@value"/>";
				</xsl:for-each>

			<![CDATA[
			//loop through the Allanchor string,
			//write out a link if the character exists
			//or display text if it doesn't
			for (i; i < allAnchors.length; i++)
			{
				current = allAnchors.substr(i,1);
				if (anchors.substr(j,1) == allAnchors.substr(i,1))
				{
					document.write("<A href = \"#" + current + "\">" + current + "\</A>" + "&nbsp;&nbsp;&nbsp;");
					j++;
				}
				else
				{
					document.write(current + "&nbsp;&nbsp;&nbsp;");
				}
			}
			*/
			]]>

			</SCRIPT>
		</TD>
	</TR>
    </TABLE>

    <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
    	<TR>
    		<TD align="right">
                <IMG tabindex="3" onkeydown="javascript:closeSourceLookup('')" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')"/>
    		</TD>
    	</TR>

    	<TR>
    		<TD>
		    <TABLE width="100%" class="LookupTable" cellpadding="1" cellspacing="2">
				<TR>
					<TD>
						<TABLE width="100%" border="1" cellpadding="2" cellspacing="2">
							<TR>
								<TD width="5%" class="label"> &nbsp; </TD>
								<TD class="label" valign="bottom"> Name/Description </TD>
								<TD class="label" valign="bottom"> Offer </TD>
								<TD class="label" align="center" valign="bottom"> Service <BR></BR>Charge </TD>
								<TD class="label" align="center" valign="bottom"> Expiration <BR></BR>Date</TD>
								<TD class="label" align="center" valign="bottom"> Order <BR></BR>Source</TD>
								<TD class="label" align="center" valign="bottom"> Source <BR></BR>Code</TD>
								<TD>
									<xsl:for-each select="/root/sourceCodeLookup/searchResults/searchResult">
									<TR>
										<TD>
											<SCRIPT language="javascript">
											<![CDATA[
											/*
												//write out the anchor tag when the anchor attribute changes
												previous = destination
												destination = "<xsl:value-of select="@anchor"/>"

												if (previous != destination)
													document.write("<A name=\'" + destination + "\'></A>\n")
											*/
												//write out the function and image to populate the parent page
												//but if the expiredFlag is Y, then write out the disabled image
											]]>
												<xsl:choose>
													<xsl:when test = "@expiredFlag = 'Y'">
														<![CDATA[
														document.write("<IMG alt=\"disabled\" src=\"../images/selectButtonRight_disabled.gif\">")
														]]>
													</xsl:when>
													<xsl:when test = "@expiredFlag = 'N'">
														var sourceCode = "<xsl:value-of select="@sourceCode"/>";
														sourceCodeDesc = "<xsl:value-of select="@sourceDescription"/>";
														<![CDATA[
														document.write("<IMG onclick=\"javascript: populatePage(\'" + sourceCode + "\')\"")
														document.write("onkeydown=\"javascript:closeSourceLookup(\'" + sourceCode + "\')\" tabindex=\"4\" src=\"../images/selectButtonRight.gif\">")
														document.write("</IMG>")
														]]>
													</xsl:when>
												</xsl:choose>
											</SCRIPT>
										</TD>
										<TD align="left">
											<xsl:value-of select="@sourceDescription"/>
										</TD>
										<TD align="left">
											<xsl:value-of select="@offerDescription"/>
										</TD>
										<TD  align="center">
											<xsl:value-of select="@serviceCharge"/>
										</TD>
										<TD  align="right">
											<xsl:value-of select="@expirationDate"/>&nbsp;
										</TD>
										<TD  align="center">
											<xsl:value-of select="@orderSource"/>
										</TD>
										<TD  align="center">
											<xsl:value-of select="@sourceCode"/>
										</TD>
									</TR>
									</xsl:for-each>
								</TD>
							</TR>

							<TR>
					            <TD width="5%" valign="center">
					                <IMG tabindex="4" onkeydown="javascript:closeSourceLookup('')" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');"/>
					            </TD>
					            <TD colspan="7" valign="top"> None of the above </TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
		    </TABLE>
		</TD>
	</TR>
   	<TR>
   		<TD align="right">
   			<IMG tabindex="5" onkeydown="javascript:closeSourceLookup('')" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')"/>
		</TD>
   	</TR>
   </TABLE>

</CENTER>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>