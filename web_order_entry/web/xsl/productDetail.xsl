<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="countryid" select="root/parameters/countryid"/>
<xsl:variable name="country" select="root/parameters/country"/>
<xsl:variable name="cartItemNumber" select="root/parameters/cartItemNumber"/>
<xsl:variable name="custom" select="root/parameters/custom"/>
<xsl:variable name="refreshServlet" select="root/parameters/refreshServlet"/>
<xsl:variable name="cancelItemShoppingServlet" select="root/parameters/cancelItemShoppingServlet"/>
<xsl:variable name="searchType" select="root/parameters/searchType"/>
<xsl:variable name="focusElement" select="root/parameters/focusElement"/>
<xsl:variable name="upsellFlag" select="root/parameters/upsellFlag"/>
<xsl:variable name="companyId" select="root/parameters/companyId"/>
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>
<xsl:variable name="standardLabel" select="root/parameters/standardLabel"/>
<xsl:variable name="deluxeLabel" select="root/parameters/deluxeLabel"/>
<xsl:variable name="premiumLabel" select="root/parameters/premiumLabel"/>

<xsl:template match="/root">

<HTML>
<HEAD>
<TITLE> FTD - Product Detail </TITLE>
	<script language="javascript" src="../js/FormChek.js"/>
	<script language="javascript" src="../js/util.js"/>

	<SCRIPT language="javascript">
	var disableFlag = false;
	var disableRefreshFlag = true;
	var submitInd = "submit";
	var zipCode = "<xsl:value-of select="/root/orderData/data[@name = 'zipCode']/@value"/>";
	var backupFlag = false;	
	var companyId = "<xsl:value-of select="$companyId"/>";

	document.oncontextmenu=stopIt;	


	<![CDATA[

		document.onkeydown = backKeyHandler;

		function hideDIVs()
		{
		   if(document.all.deliveryDateDIV)
		   	document.all.deliveryDateDIV.style.visibility = "hidden";

		   if(document.all.cardDIV)
		   	document.all.cardDIV.style.visibility = "hidden";

		   if(document.all.methodsDIV)
		     document.getElementById("methodsDIV").style.display = "none";
		}

		function showDIVs()
		{
		   if(document.all.deliveryDateDIV)
		   	document.all.deliveryDateDIV.style.visibility = "visible";

		   if(document.all.cardDIV)
		   	document.all.cardDIV.style.visibility = "visible";

		   if(document.all.methodsDIV)
		   	document.getElementById("methodsDIV").style.display = "";
		}

		function onKeyDown()
		{
		   if (window.event.keyCode == 13)
			{
			    submitForm();
			}
		}

		function onKeyDownBack()
		{
		   if (window.event.keyCode == 13)
			{
			    backUp();
			}
		}

		function backToShopping()
		{
		]]>
			
			<xsl:choose>
			<xsl:when test= "$upsellFlag = 'Y'">	
			
			var url = "<xsl:value-of select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>";
			<![CDATA[
			document.location = url;
			]]>
						
			</xsl:when>
			<xsl:otherwise>
			var country = "<xsl:value-of select="/root/orderData/data[@name = 'country']/@value"/>";
			var deliveryDate = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDate']/@value"/>";
			var deliveryDateDisplay = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDateDisplay']/@value"/>";
			var occasionDescription = "<xsl:value-of select="/root/orderData/data[@name = 'occasionDescription']/@value"/>";
			var city = "<xsl:value-of select="/root/orderData/data[@name = 'city']/@value"/>";
			var state = "<xsl:value-of select="/root/orderData/data[@name = 'state']/@value"/>";
			var occasion = "<xsl:value-of select="/root/orderData/data[@name = 'occasion']/@value"/>";
		<![CDATA[

			var url = "CategoryServlet" + "?sessionId=" + sessionId +
						       "&persistentObjId=" + persistentObjId +
						       "&countries=" + country +
						       "&deliveryDates=" + deliveryDate +
						       "&deliveryDateDisplay=" + deliveryDateDisplay +
						       "&occasionDescription=" + occasionDescription +
						       "&city=" + city +
						       "&state=" + state +
						       "&occasions=" + occasion +
						       "&itemCartNumber=" + itemNumber +
						       "&companyId=" + companyId +
						       "&zipCode=" + zipCode;
			document.location = url;
				]]>
			</xsl:otherwise>
			</xsl:choose>
			
		<![CDATA[	
		}
		
		function acceptGnaddUpsell()
		{
		]]>
		
		var upsellBaseId = "<xsl:value-of select="/root/pageData/data[@name='upsellBaseId']/@value"/>";
		var upsellCrumbFlag = "<xsl:value-of select="$upsellFlag"/>";
		var searchType = "<xsl:value-of select="$searchType"/>";
		
		<![CDATA[
			var servletCall = "";
			if(searchType == 'simpleproductsearch') {
				var url = "SearchServlet" + "?sessionId=" + sessionId +
					      "&persistentObjId=" + persistentObjId +
					      "&upsellFlag=" + upsellCrumbFlag +
					      "&search=" + searchType +
					      "&productId=" + upsellBaseId +
					      "&soonerOverride=Y"  + "&companyId=" + companyId;
			}
			else {
				var url = "ProductDetailServlet" + "?sessionId=" + sessionId +
					  	  "&persistentObjId=" + persistentObjId +
					      "&upsellFlag=" + upsellCrumbFlag +
					      "&search=" + searchType +
					      "&productId=" + upsellBaseId +
					      "&soonerOverride=Y"  + "&companyId=" + companyId;
			}
					  
			document.location = url;
		}

		function backUp()
		{
			window.history.back();
		}

		function openCalendarPopup()
		{
			openCalendarLoadingDiv();
			productId = document.forms[0].productId.value;
			var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId + "&companyId=" + companyId;

		    var modal_dim="dialogWidth:400px; dialogHeight:500px; center:yes; status=0";
	 	    var ret = window.showModalDialog(url_source,"VIEW_CALENDAR", modal_dim);

  			closeCalendarLoadingDiv();

	 	    if ( ret!= null && document.forms[0].floristDate != null )
	 	    {
	 	    	document.forms[0].floristDate.value = ret;
	 	    	var selectBox = document.all.floristDate;
 	    		setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].text);
	 	    }

	 	    //set the focus on the next field
	 	    document.forms[0].floristDate.focus();
	 	}

	    /* Show an object */
	    function showObject(o,col,object,object1)
	    {
	        object.visibility = VISIBLE;
	        object1.visibility = VISIBLE;
	    }

	   /* Hide an object */
	    function hideObject(o,col,object,object1)
	    {
	        object.visibility = HIDDEN;
	        object1.visibility = HIDDEN;
	    }
	]]>
	</SCRIPT>


<!-- for addOn selected checkbox and quantity textfield -->
	<script language="javascript">
	<![CDATA[
		function addOnSelected( checkbox )
		{
			if ( checkbox == null || checkbox == "" )
				return;
			if (checkbox == "funeralBannerCheckBox")
			{
				if ( document.all.funeralBannerCheckBox.checked ) {
					 if ( document.all.funeralBannerQty.value == "" )
						document.all.funeralBannerQty.value = 1;
				} else {
					document.all.funeralBannerQty.value = "";
				}


			} else if ( checkbox == "bearCheckBox" ) {
				if ( document.all.bearCheckBox.checked ) {
					if ( document.all.bearQty.value == "" )
						document.all.bearQty.value = 1;
				} else {
					document.all.bearQty.value = "";
				}

			} else if ( checkbox == "baloonCheckBox" ) {
				if ( document.all.baloonCheckBox.checked ) {
					if ( document.all.baloonQty.value == "" )
						document.all.baloonQty.value = 1;
				} else {
					document.all.baloonQty.value = "";
				}

			} else if ( checkbox == "chocolateCheckBox" ) {
				if ( document.all.chocolateCheckBox.checked ) {
					if ( document.all.chocolateQty.value == "" )
						document.all.chocolateQty.value = 1;
				} else {
					document.all.chocolateQty.value = "";
				}

			} else {
				// do nothing
			}

			return;
		}

	]]>
	</script>

	<script language="javascript">
<![CDATA[


	function makeChecked(checkbox)
	{
		checkbox.checked='true';
	}

	function openGreetingCardPopup()
	{
		var occasionId = document.forms[0].occasionId.value;
		var form = document.forms[0];
		var url_source="PopupServlet?POPUP_ID=LOOKUP_GREETING_CARDS&occasionInput=" + occasionId  + "&companyId=" + companyId;
	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	    //open the window
 	    var ret =  window.showModalDialog(url_source,"", modal_dim);

		//in case the X icon is clicked
 		if (!ret)
 			ret = '';

 		//populate the dropdown
 		if (ret != '')
			document.all.cards.value = ret;

		//set the focus on the next field
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.all.zip.focus();
	}
]]>
	</script>


	<script language="javascript">

		var sessionId = "<xsl:value-of select="$sessionId"/>";
		var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
		var focusElement = "<xsl:value-of select="$focusElement"/>";
		var searchType = "<xsl:value-of select="$searchType"/>";
		var upsellFlag = "<xsl:value-of select="$upsellFlag"/>";

		var itemNumber = "<xsl:value-of select="$cartItemNumber"/>";

		var shipMethodFlorist = "<xsl:value-of select="/root/productList/products/product/@shipMethodFlorist"/>";
		var shipMethodCarrier = "<xsl:value-of select="/root/productList/products/product/@shipMethodCarrier"/>";
		var carrier = "<xsl:value-of select="/root/productList/products/product/@carrier"/>";

		var variablePriceMax = "<xsl:value-of select="/root/productList/products/product/@variablePriceMax"/>";
		var variablePriceMin = "<xsl:value-of select="/root/productList/products/product/@standardPrice"/>";


	</script>
	<script language="javascript">
	<![CDATA[

		function openUpdateWin()
		{
			var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers

			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
			updateWinV.top=scroll_top+document.body.clientHeight/2-100

			updateWinV.width=290
			updateWinV.height=100
			updateWinV.left=document.body.clientWidth/2 - 100
			updateWinV.visibility=(dom||ie)? "visible" : "show"
		}
	]]>
	</script>
	<SCRIPT language="javascript" >
	<![CDATA[

		function init()
  		{
  			disableFlag = false;
  			disableRefreshFlag = true;
  			]]>

			<![CDATA[
			
 			// Set focus to the passed in element name
 			if(focusElement.length > 0)
			{				
					document.forms[0].elements[focusElement].focus();
			}

  			//Populate the hidden occasion and countryType variables
  			]]>
  			document.forms[0].occasionId.value ="<xsl:value-of select="/root/orderData/data[@name = 'occasion']/@value"/>";
  			document.forms[0].countryType.value ="<xsl:value-of select="/root/orderData/data[@name = 'countryType']/@value"/>";
  			document.forms[0].searchType.value ="<xsl:value-of select="$searchType"/>";
  			document.forms[0].upsellFlag.value ="<xsl:value-of select="$upsellFlag"/>";

			<xsl:if test="/root/productList/products/product/@shipMethodFlorist = 'Y'">
				document.all.DELIVERY_DATE.value = "<xsl:value-of select="/root/orderData/data[@name ='requestedDeliveryDate']/@value"/>";
				
				<!-- Second choice (substitution) flag -->
				<xsl:if test="/root/productList/products/product/@secondChoiceCode != '0' and /root/productList/products/product/@shipMethodFlorist = 'Y'">
				<xsl:choose>
				<xsl:when test="/root/pageData/data[@name='secondChoiceAuth']/@value = 'N'">
					document.all.secondChoiceAuth.checked = false;
				</xsl:when>
				<xsl:otherwise>
					document.all.secondChoiceAuth.checked = true;
				</xsl:otherwise>
				</xsl:choose>
				</xsl:if>
				
				document.all.DELIVERY_SHIPPING_METHOD.value = 'florist';
				
			</xsl:if>
						
			
			<!--End of if ShipMethodFlorist = Y -->
<!-- ***** -->
<!-- ***** -->
			<!--Begin ShipMethodCarrier = Y -->
			

			<!--End of if ShipMethodCarrier = Y -->

			// populate all other form fields
 			<xsl:for-each select="/root/item/ADD_ONS/ADD_ON">
				<xsl:choose>
					<xsl:when test="@type = '1'">
						document.all.baloonCheckBox.checked = 'true';
						document.all.baloonQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@type = '2'">
						document.all.bearCheckBox.checked = 'true';
						document.all.bearQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@type = '3'">
						document.all.funeralBannerCheckBox.checked = 'true';
						document.all.funeralBannerQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@type = '4'">
						document.all.cards.value = "<xsl:value-of select="/root/item/ADD_ONS/ADD_ON[@type = '4']/@id"/>";
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@type = '5'">
						document.all.chocolateCheckBox.checked = 'true';
						document.all.chocolateQty.value = "<xsl:value-of select="@quantity"/>";
					</xsl:when>
				</xsl:choose>

			</xsl:for-each>


			if(document.all.color1 != null) document.all.color1.value = "<xsl:value-of select="/root/item/DETAIL/@color1"/>";
			if(document.all.color2 != null) document.all.color2.value = "<xsl:value-of select="/root/item/DETAIL/@color2"/>";

			var priceValue = '';
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'standard']">
					priceValue = 'standard';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'deluxe']">
					priceValue = 'deluxe';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'premium']">
					priceValue = 'premium';
				</xsl:when>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="/root/item/DETAIL[@price = 'variable']">
					priceValue = 'variable';
				</xsl:when>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/orderData/data[@name='countryType']/@value = 'I'">
						<![CDATA[
							openJCPPopup();
						]]>
				</xsl:when>

				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/productList/products/product/@jcpCategory != 'A'">
							<![CDATA[
								openJcpFoodItem();
							]]>
						</xsl:when>

						<xsl:otherwise>
							<xsl:if test="/root/pageData/data[@name='displaySpecGiftPopup']/@value = 'Y'">
								<![CDATA[
									//display the Yes/No Floral DIV Tag
									openNoFloral();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayNoProductPopup']/@value = 'Y'">
								<![CDATA[
									//display the No Products DIV Tag
									openNoProduct();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayNoProductPopup']/@value = 'Y'">
								<![CDATA[
									//display the No Products DIV Tag
									openNoProduct();
								]]>
							</xsl:if>							
							<xsl:if test="/root/pageData/data[@name='displayNoCodifiedFloristHasCommonCarrier']/@value = 'Y'">
								<![CDATA[
									//display the No Codified Florist Has Common Carrier DIV Tag
									//openNoCodifiedFloristHasCommonCarrier();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayNoFloristHasCommonCarrier']/@value = 'Y'">
								<![CDATA[
									//display the No Florist Has Common Carrier DIV Tag
									//openNoFloristHasCommonCarrier();
								]]>
							</xsl:if>							
							<xsl:if test="/root/pageData/data[@name='displayCodifiedSpecial']/@value = 'Y'">
								<![CDATA[
									// display the Codified Special DIV Tag
									openCodifiedSpecial();
								]]>
							</xsl:if>
							<xsl:if test="/root/pageData/data[@name='displayProductUnavailable']/@value = 'Y'">
								<![CDATA[
									// display the Product Unavailable DIV Tag
									openProductUnavailable();
								]]>
							</xsl:if>
							<xsl:if test="/root/productList/products/product/@weboeBlocked = 'Y'">
								<![CDATA[
									// Product is blocked from being sold on WEBOE 
                                    // display the Product Unavailable DIV Tag
									openProductUnavailable();
								]]>
							</xsl:if>
              <xsl:if test="/root/pageData/data[@name='displayUpsellGnaddSpecial']/@value = 'Y'">
								<![CDATA[
									// display the GNADD upsell DIV tag
									openGnaddUpsell();
								]]>
							</xsl:if>
						</xsl:otherwise>

					</xsl:choose>

				</xsl:otherwise>
			</xsl:choose>


			<xsl:if test="/root/extraInfo/subtypes/product">
				document.all.subCodeId.value = "<xsl:value-of select="/root/orderData/data[@name = 'subCodeSelected']/@value"/>";
			</xsl:if>
			if (document.all.variablePriceAmount != null) document.all.variablePriceAmount.value = "<xsl:value-of select="/root/item/DETAIL/@variablePrice"/>";

 			<![CDATA[

 			// populate all other form fields
 			if(document.all.price != null)
 			{
				for (i=0; i<document.all.price.length; i++)
				{
					if ( document.all.price[i].value == priceValue )
						document.all.price[i].checked = true;
				}
			}

 			document.all.cartItemNumber.value = itemNumber;

			//Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
			]]>
		   	<xsl:if test="/root/orderData/data[@name='displaySpecialFee']/@value = 'Y'">
		   	<![CDATA[
		   		alertHIorAK();
		   	]]>
		   	</xsl:if>
		   	<![CDATA[
   	}

	function JCPPopChoice(inChoice)
	{
	/***  JC penney popup for international change
	*/
		if(inChoice == 'Y')
		{

		var url = "DNISChangeServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&source=productDetailJCP" + "&companyId=" + companyId;

			document.forms[0].source.value = 'productDetailJCP';
			document.forms[0].action = url;
			document.forms[0].method = "get";
			document.forms[0].submit();

		} else {
			//document.forms[0].zip.value = '';
			refreshPage();
			closeJCPPopup();
		}



	}
	function openJCPPopup()
	{

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		JCPPopV.top=scroll_top+document.body.clientHeight/2-100

		JCPPopV.width=290
		JCPPopV.height=100
		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
	}

	function deliveryPolicy()
	{
		var url_source = "../deliveryPolicy_floral.htm";

		if ( carrier == "Fed Ex" )
		{
			url_source = "../deliveryPolicy_dropship.htm";
		}
		else
		{
			url_source = "../deliveryPolicy_floral.htm";
		}
		
		var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
		window.showModalDialog(url_source,"delivery_policy", modal_dim);
	}

	function resetRadio( value )
	{
		var size = document.all.price.length;
		for (i=0; i<size; i++) {
			if ( document.all.price[i].value == "variable" )
				document.all.price[i].checked = true;
		}
	}

		function isValidUSDollar( dollar )
		{
			if ( dollar == null || dollar == "" )
				return false;

			first = dollar.indexOf(".");

			if ( first != -1 )
			{
				dollar_length = dollar.length;

				firstSub = dollar.substr( 0, first );
				secondSub = dollar.substr( first + 1, dollar_length - first );

				if ( ( dollar_length - first ) == 1 )
					return isInteger( firstSub );
				else
					return ( isInteger( firstSub ) && isInteger( secondSub ) );

			} else {
				return isInteger( dollar );
			}

			return false;
		}


		function validateVariablePrice()
		{
			var field = document.all.variablePriceAmount;
			if ( field == null )
				return true;

			checked = isValidUSDollar ( field.value );
		  	//Check to make sure the variablePriceAmount is valid US dollar

		  	// Check the variable price range
		  	if ( checked )
		  	{
		  		if ( parseFloat(field.value) < parseFloat(variablePriceMin) || parseFloat(field.value) > parseFloat(variablePriceMax ) )
		  		{
		  			invalidVariablePrice.style.visibility = "visible";
		  			message = "Varible price range: " + variablePriceMin + " - " + variablePriceMax;
		  			checked = false;
		  		} else {
]]>
			<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
<![CDATA[

		  			var decpos = (field.value).indexOf(".")
		  			if(decpos == "-1" || (decpos >= ((field.value).length - 2)))
		  			{
		  				invalidVariablePrice.style.visibility = "visible";
		  				checked = false;
		  				message = "Price must end with .99";
		  			} else {

		  				if((field.value).substr(decpos, 3) != ".99")
		  				{
		  					invalidVariablePrice.style.visibility = "visible";
		  					checked = false;
		  					message = "Price must end with .99";
		  				}
		  			}
]]>
			</xsl:if>
<![CDATA[


		  			invalidVariablePrice.style.visibility = "hidden";
		  		}

		  	}

		  	if( !checked )
		  	{
	   			field.focus();
		   		field.style.backgroundColor='pink'
		  	}
		    return checked;
		 }

		 function checkVariablePrice( value )
		 {

		 	 if (value != "" && document.all.variablePriceAmount != null ) {
		 	 	var v = document.all.variablePriceAmount.value= "";
		 	 	document.all.variablePriceAmount.style.backgroundColor = "white";
			  }
		 }


 		function validateForm()
		{
			checked = true;
  	     		if(document.all.price != null)
  	     		{
  	      	  		size = document.all.price.length;
  	      	  		
			  	for (i=0; i<size; i++)
			  	{
			  		if (document.forms[0].price[i].checked && document.forms[0].price[i].value == "variable" )
			  		{
			  		checked = validateVariablePrice();
		  			}
			  	}
			}

]]>
			<xsl:if test="/root/extraInfo/subtypes/product">
<![CDATA[
				document.forms[0].subCodeId.className="TblText";
				if(document.forms[0].subCodeId.value == "")
				{
					document.forms[0].subCodeId.className="Error";
					document.forms[0].subCodeId.focus();
					checked = false;
				}

]]>
			</xsl:if>
			<xsl:if test="/root/productList/products/product/@color1 !=''">
<![CDATA[
				document.forms[0].color1.className="TblText";
				if(document.forms[0].color1.value == "")
				{
					document.forms[0].color1.className="Error";
					if(checked != false)
					{
						document.forms[0].color1.focus();
						checked = false;
					}
				}

]]>
			</xsl:if>
			<xsl:if test="/root/productList/products/product/@color2 !=''">
<![CDATA[
				document.forms[0].color2.className="TblText";
				if(document.forms[0].color2.value == "")
				{
					document.forms[0].color2.className="Error";
					if(checked != false)
					{
						document.forms[0].color2.focus();
						checked = false;
					}
				}



]]>
			</xsl:if>
<![CDATA[

 			  return checked;
		 }

	function submitForm()
	{
		if(backupFlag == true)
		{
			backToShopping();
			return;
		}


		if(disableFlag == false)
		{

     	 		if(validateForm())
     	 		{
		  		openUpdateWin();
		  		disableContinue();
		  		form.submit();
		  	}
			else
			{
				enableContinue();
		  		alert("Please correct the marked fields.");
		 	}
		}
 	}


	 function viewLargeImage()
	 {

		//Hide the dropdowns
		hideDIVs();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		largeImage = (dom)?document.getElementById("showLargeImage").style : ie? document.all.showLargeImage : document.showLargeImage
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		largeImage.top=scroll_top+document.body.clientHeight/2 -200

		largeImage.width=340;
		largeImage.height=400;
		largeImage.left= 200;
		largeImage.visibility=(dom||ie)? "visible" : "show"

	 }

	function closeLargeImage()
	{
		largeImage.visibility= "hidden";
		showDIVs();
	}

	function disableContinue()
	{
	/*** Change image of continue button to disabled and prevent submitting the page
	*/
	//but only if the country is domestic and the key stroke isn't a tab
	        if (window.event.keyCode != 9)
		{
	]]>
		<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
			<xsl:if test="/root/productList/products/product/@status = 'A' and /root/pageData/data[@name='displayProductUnavailable']/@value != 'Y'"> 
				<![CDATA[
					disableFlag = true;
					document.images['continueButton'].src  = '../images/button_continue_disabled.gif';
				]]>
			</xsl:if>
		</xsl:if>
	<![CDATA[
		}
	}

	function enableContinue()
	{
	/*** Change image of continue button to enabled and allow submitting the page
	*/
	//but only if domestic
	]]>
		<xsl:if test="/root/orderData/data[@name='countryType']/@value='D'">
			<xsl:if test="/root/productList/products/product/@status = 'A'">
				<![CDATA[
						disableFlag = false;
						document.images['continueButton'].src  = '../images/button_continue.gif';
				]]>
			</xsl:if>
		</xsl:if>
	<![CDATA[
	}


	function deCode(strSelection)
	{
			strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
			strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

			return strSelection;
	}

	function onValidationNotAvailable()
	{
	closeUpdateWin();
	var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");
		if(x == true)
		{
			if(submitInd == "submit")
			{
				occasionform.submit();
			} else
			{
				//do nothing
				return false;
			}
		}
	}


	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";
	}


	/*	**************************************************************************
			This group of functions is used to display/hide the Specialty Gift
			only available for zip code message box.
		**************************************************************************	*/
	function openNoFloral()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloralV = (dom)?document.getElementById("noFloral").style : ie? document.all.noFloral : document.noFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloralV.top=scroll_top+document.body.clientHeight/2-100

		noFloralV.width=290
		noFloralV.height=100
		noFloralV.left=document.body.clientWidth/2 - 100
		noFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoFloral()
	{
		//enableContinue();
		disableContinue();
		noFloral.style.visibility = "hidden";
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the No Product
			available for zip code message box
		**************************************************************************	*/
	function openNoProduct()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noProductV = (dom)?document.getElementById("noProduct").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noProductV.top=scroll_top+document.body.clientHeight/2-100

		noProductV.width=290
		noProductV.height=100
		noProductV.left=document.body.clientWidth/2 - 50
		noProductV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoProduct()
	{
		noProductV.visibility = "hidden";
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.forms[0].zip.focus();
	}

	function noProduct()
	{
		closeNoProduct();
	}


// Called when a same day gift has no codified florists for this zip code
	function openNoCodifiedFloristHasCommonCarrier()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noCodifiedFloristHasCommonCarrierV = (dom)?document.getElementById("noCodifiedFloristHasCommonCarrier").style : ie? document.all.noCodifiedFloristHasCommonCarrier : document.noCodifiedFloristHasCommonCarrier
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noCodifiedFloristHasCommonCarrierV.top=scroll_top+document.body.clientHeight/2-100

		noCodifiedFloristHasCommonCarrierV.width=290
		noCodifiedFloristHasCommonCarrierV.height=100
		noCodifiedFloristHasCommonCarrierV.left=document.body.clientWidth/2 - 50
		noCodifiedFloristHasCommonCarrierV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoCodifiedFloristHasCommonCarrier()
	{
		noCodifiedFloristHasCommonCarrierV.visibility = "hidden";
		enableContinue();
	}

	function noCodifiedFloristHasCommonCarrier()
	{
		closeNoCodifiedFloristHasCommonCarrier();
	}

// Called when a same day gift has no florists for this zip code
	function openNoFloristHasCommonCarrier()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloristHasCommonCarrierV = (dom)?document.getElementById("noFloristHasCommonCarrier").style : ie? document.all.noFloristHasCommonCarrier : document.noFloristHasCommonCarrier
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloristHasCommonCarrierV.top=scroll_top+document.body.clientHeight/2-100

		noFloristHasCommonCarrierV.width=290
		noFloristHasCommonCarrierV.height=100
		noFloristHasCommonCarrierV.left=document.body.clientWidth/2 - 50
		noFloristHasCommonCarrierV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoFloristHasCommonCarrier()
	{
		noFloristHasCommonCarrierV.visibility = "hidden";
		enableContinue();
	}

	function noFloristHasCommonCarrier()
	{
		closeNoFloristHasCommonCarrier();
	}
	
	function closeGnaddUpsell()
	{
		showDIVs();
		gnaddUpsellUV.visibility = "hidden";
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.forms[0].zip.focus();
		
		enableContinue();
	}
	
	function gnaddUpsell()
	{
		closeGnaddUpsell();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Product Unavailable
			for zip code message box
		**************************************************************************	*/
	function openProductUnavailable()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		productUV = (dom)?document.getElementById("productUnavailable").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		productUV.top=scroll_top+document.body.clientHeight/2-100

		productUV.width=290
		productUV.height=100
		productUV.left=document.body.clientWidth/2 - 50
		productUV.visibility=(dom||ie)? "visible" : "show"
	}
	
	function openGnaddUpsell()
	{
		hideDIVs();
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		gnaddUpsellUV = (dom)?document.getElementById("gnaddUpsell").style : ie? document.all.gnaddUpsell : document.gnaddUpsell
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		gnaddUpsellUV.top=scroll_top+document.body.clientHeight/2-100

		gnaddUpsellUV.width=290
		gnaddUpsellUV.height=100
		gnaddUpsellUV.left=document.body.clientWidth/2 - 50
		gnaddUpsellUV.visibility=(dom||ie)? "visible" : "show"
	}

	function openJcpFoodItem()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		productUV = (dom)?document.getElementById("jcpFood").style : ie? document.all.jcpFood : document.jcpFood
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		productUV.top=scroll_top+document.body.clientHeight/2-100

		productUV.width=290
		productUV.height=100
		productUV.left=document.body.clientWidth/2 - 50
		productUV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJcpFoodItem()
	{
		productUV.visibility = "hidden";
		document.forms[0].backButton.focus();
	}

	function closeProductUnavailable()
	{
		productUV.visibility = "hidden";
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.forms[0].zip.focus();
	}

	function productUnavailable()
	{
		closeProductUnavailable();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Floral Only
			for zip code message box
		**************************************************************************	*/
	function openOnlyFloral()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		onlyFloralV = (dom)?document.getElementById("onlyFloral").style : ie? document.all.onlyFloral : document.onlyFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		onlyFloralV.top=scroll_top+document.body.clientHeight/2-100

		onlyFloralV.width=290
		onlyFloralV.height=100
		onlyFloralV.left=document.body.clientWidth/2 - 50
		onlyFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeOnlyFloral()
	{
		onlyFloralV.visibility = "hidden";
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.forms[0].zip.focus();
	}

	function onlyFloral()
	{
		closeOnlyFloral();
	}


	/*	**************************************************************************
			This group of functions is used to display/hide the Codified Special
			for zip code message box
		**************************************************************************	*/
	function openCodifiedSpecial()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		codifiedSpecialV = (dom)?document.getElementById("codifiedSpecial").style : ie? document.all.codifiedSpecial : document.codifiedSpecial
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		codifiedSpecialV.top=scroll_top+document.body.clientHeight/2-100

		codifiedSpecialV.width=290
		codifiedSpecialV.height=100
		codifiedSpecialV.left=document.body.clientWidth/2 - 50
		codifiedSpecialV.visibility=(dom||ie)? "visible" : "show"

		document.all.continueButton.src = "../images/button_back.gif";
		backupFlag = true;

	}

	function closeCodifiedSpecial()
	{
		codifiedSpecialV.visibility = "hidden";
		//if (document.all.zip && document.all.zip.enabled == true)
		//	document.forms[0].zip.focus();
	}

	function codifiedSpecial()
	{
		closeCodifiedSpecial();
	}


	function cancelItem()
    	/***
    	*   This function will call the cancel Item servlet
    	*/
	{
	]]>
		target = "<xsl:value-of select="$cancelItemShoppingServlet"/>";

<![CDATA[
var url = target + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&fromPageName=ProductDetail&zipCode=" + zipCode + "&companyId=" + companyId;
]]>
		<xsl:if test="/root/pageData/data[@name='cartItemNumber']/@value">
			var cartitem = "<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
<![CDATA[
			url = url.concat("&itemCartNumber=" + cartitem);

]]>
		</xsl:if>
<![CDATA[

		document.location = url;

	}


]]>
	</SCRIPT>

	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
	<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
		<TR>
			<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
			<TD align="right"> <IMG src="../images/headerFAQ.gif" onclick="javascript:doFAQ();"/> </TD>
		</TR>
		<TR> <TD colspan="6" valign="center"> <HR> </HR> </TD> </TR>
	</TABLE>

	<FORM name="form" method="get" action="{$actionServlet}" >
		<input type="hidden" name="deliveryDateDisplay" value=""/>
		<INPUT type="hidden" name="command" value="updateItem"/>
		<INPUT type="hidden" name="cartItemNumber" value=""/>
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
		<INPUT type="hidden" name="searchType" value="{$searchType}"/>
		<INPUT type="hidden" name="upsellFlag" value="{$upsellFlag}"/>
		<INPUT type="hidden" name="productId">
			<xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@productID"/></xsl:attribute>
		</INPUT>
		<INPUT type="hidden" name="custom" value="{$custom}"/>
		<INPUT type="hidden" name="DELIVERY_DATE" value=""/>
		<INPUT type="hidden" name="DELIVERY_SHIPPING_METHOD" value=""/>
		<INPUT type="hidden" name="SELECTED_YEAR" value=""/>
		<INPUT type="hidden" name="SELECTED_MONTH" value=""/>
		<INPUT type="hidden" name="CHOSEN_DATE" value=""/>
		<INPUT type="hidden" name="occasionId" value=""/>
		<INPUT type="hidden" name="countryType" value=""/>
		<INPUT type="hidden" name="source" value=""/>
		<INPUT type="hidden" name="focusElement" value=""/>
		<input type="hidden" name="companyId" value="{$companyId}"/>
		<INPUT type="hidden" name="test">
			<xsl:attribute name="value"><xsl:value-of select="/root/deliveryDates/deviveryDate[@type='nextday']/@date"/></xsl:attribute>
		</INPUT>

		<CENTER>
		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
	    	<TR>
			    <TD width="70%"> &nbsp; </TD>
				<TD valign="center" align="right" nowrap="">
					<xsl:choose>
						<xsl:when test="$cartItemNumber=''">
            				<A tabindex="-1" href="javascript:doShoppingCart();" STYLE="text-decoration:none">
						<IMG tabindex="-1" id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A>
			    		</xsl:when>
			    		<xsl:otherwise>
							<IMG tabindex="-1" id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/><A tabindex="-1"><font color="#C0C0C0">Shopping Cart</font></A>
						</xsl:otherwise>
					</xsl:choose>
        		</TD>
				<TD valign="center" align="right" nowrap="">
					<A href="javascript:doCancelOrder();" STYLE="text-decoration:none">Cancel Order</A>
				</TD>
			</TR>
	    	<TR>
			    <TD width="70%"><xsl:call-template name="customerHeader"/> &nbsp; </TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD colspan="4" class="tblheader">
					<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					<xsl:variable name="categories" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
					<xsl:variable name="productlist" select="/root/previousPages/previousPage[@name = 'Product List']/@value"/>
					<xsl:variable name="upselldetail" select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>

					<!--<xsl:if test="$cartItemNumber=''">-->
					<xsl:if test="$occasionlbl != ''">
						&nbsp;
						<a tabindex="-1" href="{$occasionlbl}" class="tblheader">Occasion</a>&nbsp;>
						<xsl:if test= "/root/orderData/data[@name='countryType']/@value = 'D'">
							<a tabindex="-1" href="{$categories}" class="tblheader">Categories</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$searchType != 'simpleproductsearch'">
							<a tabindex="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$upsellFlag = 'Y'">
							<a tabindex="-1" href="{$upselldetail}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
						</xsl:if>
						Product Detail
					</xsl:if>
				</TD>
			</TR>
		</TABLE>

		<!-- Main Table -->
		<xsl:choose>
		<xsl:when test="/root/productList/products/product[@productID != '']">

		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD colspan="3">
								<SPAN class="header">
									<script><![CDATA[
										document.write(deCode("]]> <xsl:value-of select="/root/productList/products/product/@novatorName"/> <![CDATA["));
									]]></script>
								</SPAN> # <SPAN class="header2"><xsl:value-of select="/root/productList/products/product/@productID"/> </SPAN>
							</TD>
						</TR>

						<TR valign="top">
							<TD>
								<CENTER>
									<SCRIPT language="JavaScript">

											// Exception display trigger variables
											var exceptionMessageTrigger = false;
											var selectedDate = new Date("<xsl:value-of select="/root/orderData/data[@name='requestedDeliveryDate']/@value"/>");
											var exceptionStart = new Date("<xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/>");
											var exceptionEnd = new Date("<xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/>");
 											var pid = "<xsl:value-of select="/root/productList/products/product/@novatorID"/>";
											var imageSuffixSmall = '<xsl:value-of select="$imageSuffixSmall"/>';
											<![CDATA[
 											document.write("<img  border=\"0\" height=\"120\" width=\"120\" ")
 											document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
											document.write("if(imag != \'npi" + imageSuffixSmall + "\'){ ")

											document.write("this.src = \'/product_images/npi_1.gif\'}\" ")
											document.write("src=\"/product_images/" + pid + imageSuffixSmall + "\"> ")
											document.write("</img>")
											]]>
									</SCRIPT>
									 <br></br>
									<A tabindex ="1">
										<xsl:attribute name="href">javascript:viewLargeImage();</xsl:attribute>
										View Larger Image
									</A>
								</CENTER>
							</TD>

							<TD colspan="2" align="left">
								<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
									<TR>
										<TD class="description" colspan="3">
											<xsl:value-of select="/root/productList/products/product/@longDescription"  disable-output-escaping="yes"/>
										</TD>
									</TR>

									<TR>
										<xsl:if test="/root/productList/products/product/@productType != 'SPEGFT' and /root/productList/products/product/@serviceCharge != ''">
											<TD width="20%" class="label"> Service Charge: </TD>
											<TD width="10%" class="description" align="right"> $<xsl:value-of select="/root/productList/products/product/@serviceCharge"/></TD>
											<TD> &nbsp; </TD>
										</xsl:if>
									</TR>
									
									<TR>
										<xsl:if test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
											<xsl:if test="/root/productList/products/product/@extraShippingFee != ''">
												<TD width="20%" class="label"> Saturday Delivery Charge: </TD>
												<TD width="10%" class="description" align="right">$<xsl:value-of select="format-number(number(/root/productList/products/product/@extraShippingFee),'###,##0.00')"/></TD>
												<TD> &nbsp; </TD>
											</xsl:if>
										</xsl:if>
									</TR>

									<TR>
										<TD> &nbsp; </TD>
										<TD class="description" colspan="2">
											<A tabindex ="2" href="javascript:deliveryPolicy();">Delivery Policies</A>
										</TD>
									</TR>







									<xsl:choose>
										<xsl:when test="/root/orderData/data[@name='requestedDeliveryDate']/@value">

											<xsl:if test="/root/productList/products/product/@exceptionCode = 'U'">
												<SCRIPT language="JavaScript">
												<![CDATA[
													if(selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
														exceptionMessageTrigger = true;
		 												document.write("<TR><TD colspan=\"3\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/><![CDATA[</font></TD></TR>");
													}
												]]>
												</SCRIPT>
											</xsl:if>

											<xsl:if test="/root/productList/products/product/@exceptionCode = 'A'">
												<SCRIPT language="JavaScript">
												<![CDATA[
													if(selectedDate < exceptionStart || selectedDate > exceptionEnd) {
														exceptionMessageTrigger = true;
		 												document.write("<TR><TD colspan=\"3\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/><![CDATA[</font></TD></TR>");
													}
												]]>
												</SCRIPT>
											</xsl:if>

											<SCRIPT language="JavaScript">
											<![CDATA[
												if(exceptionMessageTrigger == true) {
													document.write("<TR><TD colspan=\"3\" class=\"labelleft\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="/root/productList/products/product/@exceptionMessage"/><![CDATA[</font></TD></TR>");
												}
												else {
													document.write("<TR><TD colspan=\"3\" class=\"labelleft\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="/root/productList/products/product/@exceptionMessage"/><![CDATA[</font></TD></TR>");
												}
											]]>
											</SCRIPT>

										</xsl:when>
										<xsl:otherwise>

											<xsl:if test="/root/productList/products/product/@exceptionCode = 'A'">
			   									<TR>
			   										<TD  colspan="3" class="labelleft"> <font color="red"> This product is only available for delivery from&nbsp;<xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/></font> </TD>
												</TR>
											</xsl:if>

											<xsl:if test="/root/productList/products/product/@exceptionCode = 'U'">
			   									<TR>
			   										<TD  colspan="3" class="labelleft"> <font color="red"> This product is not available for delivery from&nbsp;<xsl:value-of select="/root/productList/products/product/@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="/root/productList/products/product/@exceptionEndDate"/></font> </TD>
												</TR>
											</xsl:if>

											<TR>
			   									<TD  colspan="3" class="labelleft"> <font color="red"> <xsl:value-of select="/root/productList/products/product/@exceptionMessage"/></font> </TD>
											</TR>

										</xsl:otherwise>
									</xsl:choose>



									<xsl:if test="/root/extraInfo/subtypes/product">
										<TR>
											<TD width="20%" class="label"> <xsl:value-of select="/root/productList/products/product/@productName"/>: </TD>
											<TD align="left" colspan="2">
												<SELECT name="subCodeId">
												<OPTION value=""> -- Please Select --</OPTION>
												<xsl:for-each select="/root/extraInfo/subtypes/product">
													<OPTION value="{@productSubcodeId}"> <xsl:value-of select="@subcodeDescription"/> - $<xsl:value-of select="@subcodePrice"/></OPTION>
												</xsl:for-each>
												</SELECT>
											</TD>
										</TR>
									</xsl:if>
								</TABLE>
							</TD>
						</TR>

						<xsl:if test="/root/productList/products/product/@shipMethodFlorist = 'Y'">
							<TR>
								<TD align="center" bgcolor="#C1DEF3">
									FTD
									<script>
										<![CDATA[
											document.write("&#174;")
										]]>
									</script>
									Florist
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/productList/products/product/@shipMethodCarrier = 'Y' and /root/orderData/data[@name = 'country']/@value != 'CA' ">
							<TR>
								<TD align="center" bgcolor="#FF9933"> <xsl:value-of select="/root/productList/products/product/@carrier"/> </TD>
							</TR>
						</xsl:if>

						<TR> <TD colspan="3"> <HR size="5" color="#006699"/> </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<script>
									<![CDATA[
										document.write(deCode("]]> <xsl:value-of select="extraInfo/scripting/script[@fieldName='PRICE']/@scriptText"/>	 <![CDATA["));
									]]>
								</script>
							</TD>
						</TR>

						<!-- for the price selection -->
						<xsl:if test="/root/productList/products/product/@standardPrice > 0 and not(/root/extraInfo/subtypes/product/@productSubcodeId)">
							<TR>
								<TD class="labelright"> Price: &nbsp; </TD>
								<TD align="left">
									<INPUT tabindex="3" type="radio" checked="true" onclick="checkVariablePrice(value);" name="price" value="standard"/>

									<xsl:choose>
										<xsl:when test="/root/productList/products/product/@standardDiscountPrice > 0 and /root/productList/products/product/@standardPrice != /root/productList/products/product/@standardDiscountPrice">
											<SPAN style="color='red'"><strike>$<xsl:value-of select="/root/productList/products/product/@standardPrice"/> </strike></SPAN>
											$<xsl:value-of select="/root/productList/products/product/@standardDiscountPrice"/>
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="/root/productList/products/product/@standardPrice"/>
										</xsl:otherwise>
									</xsl:choose>

                                                                        <xsl:choose>
                                                                            <xsl:when test="$standardLabel != ''">
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$standardLabel"/>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;Shown
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
									<xsl:if test="/root/productList/products/product/@standardRewardValue > 0">
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Percent'">
											(<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>%)
										</xsl:if>
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Dollars'">
											($<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>&nbsp;Off)
										</xsl:if>
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Miles'">
											(<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>&nbsp;Miles)
										</xsl:if>
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Charity'">
											($<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>&nbsp;Charity)
										</xsl:if>
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Points'">
											(<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>&nbsp;Points)
										</xsl:if>
										<xsl:if test="/root/orderData/data[@name='rewardType']/@value ='Cash Back'">
											($<xsl:value-of select="/root/productList/products/product/@standardRewardValue"/>&nbsp;Cash Back)
										</xsl:if>
									</xsl:if>
								</TD>

								<TD align="left" class="Instruction" valign="top">
									<xsl:value-of select="/root/extraInfo/scripting/script[@fieldName='PRICE']/@instructionText"/>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test= "$upsellFlag != 'Y'">
						
						<xsl:if test="/root/productList/products/product/@deluxePrice > 0">
							<TR>
								<TD class="labelright"> &nbsp; </TD>
								<TD align="left">
									<INPUT tabindex="4" type="radio" onclick="checkVariablePrice(value);" name="price" value="deluxe"/>
									<xsl:choose>
										<xsl:when test="/root/productList/products/product/@deluxeDiscountPrice > 0 and /root/productList/products/product/@deluxePrice != /root/productList/products/product/@deluxeDiscountPrice">
											<SPAN style="color='red'"><strike>$<xsl:value-of select="/root/productList/products/product/@deluxePrice"/> </strike></SPAN>
											$<xsl:value-of select="/root/productList/products/product/@deluxeDiscountPrice"/>
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="/root/productList/products/product/@deluxePrice"/>
										</xsl:otherwise>
									</xsl:choose>
                                                                        <xsl:choose>
                                                                            <xsl:when test="$deluxeLabel != ''">
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$deluxeLabel"/>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;Deluxe
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
									<xsl:if test="/root/productList/products/product/@deluxeRewardValue > 0">
										(<xsl:value-of select="/root/productList/products/product/@deluxeRewardValue"/>)
									</xsl:if>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/productList/products/product/@premiumPrice > 0">
							<TR>
								<TD class="labelright"> &nbsp; </TD>
								<TD align="left">
									<INPUT tabindex ="5" type="radio" onclick="checkVariablePrice(value);" name="price" value="premium"/>
									<xsl:choose>
										<xsl:when test="/root/productList/products/product/@premiumDiscountPrice > 0 and /root/productList/products/product/@premiumPrice != /root/productList/products/product/@premiumDiscountPrice">
											<SPAN style="color='red'"><strike>$<xsl:value-of select="/root/productList/products/product/@premiumPrice"/> </strike></SPAN>
											$<xsl:value-of select="/root/productList/products/product/@premiumDiscountPrice"/>
										</xsl:when>
										<xsl:otherwise>
											$<xsl:value-of select="/root/productList/products/product/@premiumPrice"/>
										</xsl:otherwise>
									</xsl:choose>
                                                                        <xsl:choose>
                                                                            <xsl:when test="$premiumLabel != ''">
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$premiumLabel"/>
                                                                            </xsl:when>
                                                                            <xsl:otherwise>
                                                                                &nbsp;&nbsp; - &nbsp;&nbsp;Premium
                                                                            </xsl:otherwise>
                                                                        </xsl:choose>
									<xsl:if test="/root/productList/products/product/@premiumRewardValue > 0">
										(<xsl:value-of select="/root/productList/products/product/@premiumRewardValue"/>)
									</xsl:if>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/productList/products/product/@variablePriceFlag = 'Y'">
							<TR>
								<TD class="labelright"> &nbsp; </TD>
								<TD align="left">
									<INPUT tabindex ="6" type="radio" onclick="checkVariablePrice(value);" name="price" value="variable"/>
									Variable Price
									&nbsp;&nbsp; $<input tabindex ="7" type="text" onselect="resetRadio(value);" onkeypress="resetRadio(value);" onclick="resetRadio(value);" onFocus="javascript:fieldFocus('variablePriceAmount')" onblur="javascript:fieldBlur('variablePriceAmount')" name="variablePriceAmount" value="" size="8" maxlength="8" class="tblText"></input>
									<DIV id="invalidVariablePrice" style="visibility: hidden">
										<FONT color="red"> Variable Price Range should be: <xsl:value-of select="/root/productList/products/product/@standardPrice"/>&nbsp;-&nbsp;<xsl:value-of select="/root/productList/products/product/@variablePriceMax"/> </FONT>
									</DIV>
								</TD>
								<TD class="instruction"> &nbsp; </TD>
							</TR>
						</xsl:if>

						</xsl:if>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<xsl:if test="/root/productList/products/product/@color1 !=''">
							<TR>
								<TD class="ScreenPrompt" colspan="3"> </TD>
							</TR>

							<TR>
								<TD class="labelright"> 1st Choice Color: &nbsp; </TD>
								<TD align="left">
									<SELECT tabindex ="8" id="itemPrice" name="color1">
										<OPTION value=""> -- Please Select --</OPTION>
										<xsl:if test="/root/productList/products/product/@color1 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color1"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color1"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color2 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color2"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color2"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color3 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color3"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color3"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color4 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color4"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color4"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color5 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color5"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color5"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color6 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color6"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color6"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color7 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color7"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color7"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color8 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color8"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color8"/></option>
										</xsl:if>
									</SELECT>
									&nbsp; <SPAN style="color:red"> *** </SPAN>
								</TD>
								<TD class="instruction">&nbsp;  </TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/productList/products/product/@color2 !=''">
							<TR>
								<TD class="labelright"> 2nd Choice Color: &nbsp; </TD>
								<TD align="left">
									<SELECT tabindex ="9" id="itemPrice" name="color2">
										<OPTION value=""> -- Please Select --</OPTION>
										<xsl:if test="/root/productList/products/product/@color1 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color1"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color1"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color2 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color2"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color2"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color3 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color3"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color3"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color4 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color4"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color4"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color5 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color5"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color5"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color6 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color6"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color6"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color7 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color7"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color7"/></option>
										</xsl:if>
										<xsl:if test="/root/productList/products/product/@color8 != ''">
											<option><xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@color8"/></xsl:attribute><xsl:value-of select="/root/productList/products/product/@color8"/></option>
										</xsl:if>
									</SELECT>
									&nbsp; <SPAN style="color:red"> *** </SPAN>
								</TD>
							</TR>
							<TR>
								<TD class="labelright">&nbsp; </TD>
								<TD align="left" class="Instruction" valign="top">
									<xsl:value-of select="/root/extraInfo/scripting/script[@fieldName='COLOR']/@instructionText"/>
								</TD>
							</TR>

							<TR> <TD colspan="3"> &nbsp; </TD> </TR>
						</xsl:if>

						<!-- <xsl:if test="/root/productList/products/product/@color1 !=''">  -->
						<xsl:if test="/root/productList/products/product/@secondChoiceCode != '0' and /root/productList/products/product/@shipMethodFlorist = 'Y'">
							<TR>
								<TD class="ScreenPrompt" colspan="3"> <xsl:value-of select="extraInfo/scripting/script[@fieldName='COLOR']/@scriptText"/>
								 &nbsp;<xsl:value-of select="productList/products/product/@secondChoice"/>.
								 </TD>
							</TR>
								<xsl:if test="/root/pageData/data[@name='allowSubstitution']/@value = 'N'">
							<TR><TD><BR/></TD></TR>

							<TR>
								<TD class="labelright">Allow substitution:</TD>
								<TD align="left"><input tabindex ="10" type="checkbox" name="secondChoiceAuth"/></TD>
							</TR>
							<TR><TD><BR/></TD></TR>
						</xsl:if>							
						<xsl:if test="/root/pageData/data[@name='allowSubstitution']/@value = 'Y'">
							<TR>
								<TD align="left"><input tabindex ="10" type="checkbox" name="secondChoiceAuth" style="visibility: hidden"/></TD>
							</TR>
						</xsl:if>							
						</xsl:if>

						<xsl:if test="/root/productList/products/product[@addonBearsFlag='Y'] or /root/productList/products/product[@addonBalloonsFlag='Y'] or /root/productList/products/product[@addonCardsFlag='Y'] or /root/productList/products/product[@addonFuneralFlag='Y'] or /root/productList/products/product[@addonChocolateFlag='Y'] ">
							<TR>
								<TD class="ScreenPrompt" colspan="3"> <xsl:value-of select="extraInfo/scripting/script[@fieldName='ADDONS']/@scriptText"/></TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/extraInfo/addOns/addOn[@addonType='1'] and /root/productList/products/product[@addonBalloonsFlag='Y']">
							<TR>
								<TD class="labelright"> Mylar Balloon: &nbsp; </TD>
								<TD colspan="2">
									<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
										<TR>
											<TD width="50%">
												<input tabindex ="10" type="checkbox" name="baloonCheckBox" onclick="addOnSelected('baloonCheckBox');">
													<xsl:attribute name="value">
														<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='1']/@addonId"/>
													</xsl:attribute>
												</input>
												&nbsp;&nbsp;
												$<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='1']/@price"/>
											</TD>
											<TD width="20%" class="Labelright">
												Quantity: &nbsp;
											</TD>
											<TD width="30%">
												<INPUT tabindex ="11" type="text" class="tblText" name="baloonQty" size="1" maxlength="2" onchange="makeChecked(document.all.baloonCheckBox);" onFocus="javascript:fieldFocus('baloonQty')" onblur="javascript:fieldBlur('baloonQty')">
													<xsl:attribute name="value"><xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='1']/@quantity"/></xsl:attribute>
												</INPUT>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/extraInfo/addOns/addOn[@addonType='2'] and /root/productList/products/product[@addonBearsFlag='Y']">
							<TR>
								<TD class="labelright"> Small Stuffed Bear: &nbsp; </TD>
								<TD colspan="2">
									<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
										<TR>
											<TD width="50%">
												<input tabindex ="12" type="checkbox" name="bearCheckBox" onclick="addOnSelected('bearCheckBox');">
												<xsl:attribute name="value">
													<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='2']/@addonId"/>
												</xsl:attribute>
												</input>

											 	&nbsp;&nbsp;
											 	$<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='2']/@price"/>
											</TD>
											<TD width="20%" class="Labelright">
												Quantity: &nbsp;
											</TD>
											<TD width="30%">
												<INPUT tabindex ="13" type="text" class="tblText" name="bearQty" size="1" maxlength="2" onchange="makeChecked(document.all.bearCheckBox);" onFocus="javascript:fieldFocus('bearQty')" onblur="javascript:fieldBlur('bearQty')">
													<xsl:attribute name="value"><xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='2']/@quantity"/></xsl:attribute>
												</INPUT>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/extraInfo/addOns/addOn[@addonType='3'] and /root/productList/products/product[@addonFuneralFlag='Y']">
							<TR>
								<TD class="labelright"> Funeral Banner: &nbsp; </TD>
								<TD colspan="2">
									<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
										<TR>
											<TD width="50%">
												<input tabindex ="14" type="checkbox" name="funeralBannerCheckBox" onclick="addOnSelected('funeralBannerCheckBox');">
												<xsl:attribute name="value">
													<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='3']/@addonId"/>
												</xsl:attribute>

												</input>
											 	&nbsp;&nbsp;
											 	$<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='3']/@price"/>
											</TD>
											<TD width="20%" class="Labelright">
												Quantity: &nbsp;
											</TD>
											<TD width="30%">
												<INPUT tabindex ="15" type="text" class="tblText" name="funeralBannerQty" size="1" maxlength="2" onchange="makeChecked(document.all.funeralBannerCheckBox);" onFocus="javascript:fieldFocus('funeralBannerQty')" onblur="javascript:fieldBlur('funeralBannerQty')">
													<xsl:attribute name="value"><xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='3']/@quantity"/></xsl:attribute>
												</INPUT>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/extraInfo/addOns/addOn[@addonType='5'] and /root/productList/products/product[@addonChocolateFlag='Y']">
							<TR>
								<TD class="labelright"> Boxed Chocolate &nbsp; </TD>
								<TD colspan="2">
									<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
										<TR>
											<TD width="50%">
												<input tabindex ="16" type="checkbox" name="chocolateCheckBox" onclick="addOnSelected('chocolateCheckBox');">
													<xsl:attribute name="value">
														<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='5']/@addonId"/>
													</xsl:attribute>
												</input>
											 	&nbsp;&nbsp;
											 	$<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='5']/@price"/>
											</TD>
											<TD width="20%" class="Labelright">
												Quantity: &nbsp;
											</TD>
											<TD width="30%">
												<INPUT tabindex ="17" type="text" class="tblText" name="chocolateQty" size="1" maxlength="2" onchange="makeChecked(document.all.chocolateCheckBox);" onFocus="javascript:fieldFocus('chocolateQty')" onblur="javascript:fieldBlur('chocolateQty')">
													<xsl:attribute name="value"><xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='3']/@quantity"/></xsl:attribute>
												</INPUT>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</xsl:if>

						<xsl:if test="/root/extraInfo/addOns/addOn[@addonType='4'] and /root/productList/products/product[@addonCardsFlag ='Y']">

							<TR>
								<TD class="labelright"> Full Sized Greeting Card: &nbsp; </TD>
								<TD colspan="2">
									<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
										<TR>
											<TD width="50%">
												<SPAN id="cardDIV">
													<SELECT tabindex ="18" id="card" name="cards">														
														<OPTION value=""> No Card </OPTION>
														<xsl:for-each select="/root/extraInfo/addOns/addOn[@addonType='4']">
															<option><xsl:attribute name="value"><xsl:value-of select="@addonId"/></xsl:attribute><xsl:value-of select="@description"/></option>
														</xsl:for-each>
													</SELECT>
												</SPAN>
												&nbsp;&nbsp;

												<SCRIPT language="javascript">
													<![CDATA[
														//format the price
													]]>
													var price = "<xsl:value-of select="/root/extraInfo/addOns/addOn[@addonType='4']/@price"/>";
													<![CDATA[
													 var place = price.indexOf('.');
													 var rightOf = price.substring(place, 10);
													 if ((rightOf.length) == 2)
													 	price +="0";
													 document.write("&nbsp;$"+price+"")
													]]>
												</SCRIPT>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>

							<TR>
								<TD>&nbsp;  </TD>
								<TD>
									<A tabindex="19" href="javascript: openGreetingCardPopup()">View and Select cards</A>
								</TD>
								<TD class="instruction">&nbsp;  </TD>
							</TR>
						</xsl:if>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						
						<TR>
							<TD class="ScreenPrompt" colspan="3"> <xsl:value-of select="extraInfo/scripting/script[@fieldName='SUBSTITUTION']/@scriptText"/> </TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD colspan="3" align="right">
								<xsl:choose>
									<xsl:when test="/root/productList/products/product/@status = 'A'">
										<xsl:choose>
											<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/productList/products/product/@jcpCategory != 'A'">
												<IMG tabindex ="37" onkeydown="javascript:onKeyDownBack()" name="backButton" src="../images/button_back.gif" onclick="backUp()" border="0"/> &nbsp;&nbsp;
											</xsl:when>
											<!--
											<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value != ''">
											-->
											<xsl:when test="/root/pageData/data[@name='displayProductUnavailable']/@value = 'Y'">
												<IMG tabindex ="37" onkeydown="javascript:onKeyDownBack()" name="backButton" src="../images/button_back.gif" onclick="backUp()" border="0"/> &nbsp;&nbsp;
				   						</xsl:when>
				   						<xsl:otherwise>
				   								<IMG tabindex ="37" onkeydown="javascript:onKeyDown()" name="continueButton" src="../images/button_continue.gif" onclick="submitForm()" border="0"/> &nbsp;&nbsp;
				   						</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
		   							<xsl:otherwise>
		   								<IMG tabindex ="37" onkeydown="javascript:onKeyDownBack()" name="backButton" src="../images/button_back.gif" onclick="backUp()" border="0"/> &nbsp;&nbsp;
		   							</xsl:otherwise>
								</xsl:choose>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<TD class="tblheader">&nbsp;
					<xsl:variable name="occasionlbl2" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					<xsl:variable name="categories2" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
					<xsl:variable name="productlist2" select="/root/previousPages/previousPage[@name = 'Product List']/@value"/>
					<xsl:variable name="upselldetail2" select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>

					<!--<xsl:if test="$cartItemNumber=''">-->
					<xsl:if test="$occasionlbl2 != ''">
						&nbsp;
						<a tabindex="-1" href="{$occasionlbl2}" class="tblheader">Occasion</a>&nbsp;>
						<xsl:if test= "/root/orderData/data[@name='countryType']/@value = 'D'">
							<a tabindex="-1" href="{$categories2}" class="tblheader">Categories</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$searchType != 'simpleproductsearch'">
							<a tabindex="-1" href="{$productlist2}" class="tblheader">Product List</a>&nbsp;>
						</xsl:if>
						<xsl:if test= "$upsellFlag = 'Y'">
							<a tabindex="-1" href="{$upselldetail2}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
						</xsl:if>
						Product Detail
					</xsl:if>
				</TD>
			</TR>

		</TABLE>
		</xsl:when>
		<xsl:otherwise>

		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
						<TR><td>&nbsp;</td></TR>
						<TR><td><center>No products were found matching your criteria.</center></td></TR>
						<TR><td>&nbsp;</td></TR>
						<TR>
							<td align="right">
								<IMG tabindex ="37" onkeydown="javascript:onKeyDownBack()" name="backButton" src="../images/button_back.gif" onclick="backUp()" border="0"/> &nbsp;&nbsp;
							</td>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		</xsl:otherwise>
		</xsl:choose>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>
		</CENTER>


	<DIV id="showLargeImage" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<center>
			<br></br>
			<br></br>
			<SCRIPT language="JavaScript">
			 var lpid = "<xsl:value-of select="/root/productList/products/product/@novatorID"/>";
			 var imageSuffixMedium = '<xsl:value-of select="$imageSuffixMedium"/>';
			 <![CDATA[
		 	document.write("<img ")
 			document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
			document.write("if(imag != \'npi" + imageSuffixMedium + "\'){ ")
			document.write("this.src = \'/product_images/npi.jpg\'}\" ")
			document.write("src=\"/product_images/" + lpid + imageSuffixMedium + "\"> ")
			document.write("</img>")
			]]>
			</SCRIPT>
			<br></br>
			<br></br>
			<a href="javascript:closeLargeImage();">close</a>
		</center>
	</DIV>

<!-- beginning of zipcode lookup DIV -->

	<DIV id="zipCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
	<center>
		<DIV id="zipCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
	</DIV>
	</center>

	<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD colspan="3" align="left">

					<font face="Verdana"><strong>Zip/Postal Code Lookup </strong></font>
	            </TD>

	        </TR>

	        <TR>
	            <TD nowrap="true" width="50%" class="label" align="right"> City: </TD>
	            <TD width="50%">
	                <INPUT tabindex="22" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="cityInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"/>
	            </TD>
	        </TR>

	        <TR>
	            <TD nowrap="true" width="50%" class="label" align="right"> State: </TD>
	            <TD width="50%">
		  			<xsl:choose>
		  			<xsl:when test="/root/orderData/data[@name = 'country']/@value = 'US'">
		  				<SELECT tabindex ="23" onkeydown="javascript:EnterToZipPopup()" class="TblText" name="stateInput">
		  					<OPTION value="" ></OPTION>
							<xsl:for-each select="/root/extraInfo/states/state[@countryCode='']">
					  	      <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
							</xsl:for-each>
						</SELECT>
		  			</xsl:when>
		  			<xsl:when test="/root/orderData/data[@name = 'country']/@value = 'CA'">
		  				<SELECT tabindex ="24" onkeydown="javascript:EnterToZipPopup()" class="TblText" name="stateInput">
		  					<OPTION value="" ></OPTION>
							<xsl:for-each select="/root/extraInfo/states/state[@countryCode='CAN']">
					  	      <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
							</xsl:for-each>
						</SELECT>
		  			</xsl:when>
		  			<xsl:otherwise>
		                <INPUT tabindex ="25" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="stateInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"/>
					</xsl:otherwise>
		  			</xsl:choose>
	            </TD>
	        </TR>

			<TR>
				<TD nowrap="true" class="labelright"> Zip/Postal Code:</TD>
				<TD>
	                <INPUT tabindex ="26" onkeydown="javascript:EnterToZipPopup()" class="TblText" type="text" name="zipCodeInput" size="5" maxlength="10" onFocus="javascript:fieldFocus('zipCodeInput')" onblur="javascript:fieldBlur('zipCodeInput')"/>
				</TD>
			</TR>
		    <TR>
				<TD colspan="3" align="right">
				<IMG tabindex ="27" onkeydown="javascript:EnterToZipPopup()" src="../images/button_search.gif" alt="Search" onclick="javascript:goSearchZipCode()"  />
		           	<IMG tabindex ="28" onkeydown="javascript:EnterToCancelZip()" src="../images/button_close.gif" alt="Close screen" onclick="javascript:goCancelZipCode()" />
	         	</TD>
	        </TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>
	</DIV>
<!-- end of zipcode lookup  DIV -->

<!--Begin No Floral DIV -->
<DIV id="noFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE>
				<TR>
					<TD align="center">Floral Items can no longer be delivered to this zip/postal code area.
						<BR></BR>
						<BR></BR>
					    Would you be interested in our specialty items?
			   		</TD>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:closeNoFloral()"></IMG>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Floral DIV -->

<!--Begin No Product DIV -->
<DIV id="noProduct" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						I'm sorry but the zip code you have provided is not currently serviced by an FTD florist or FedEx.
						<BR></BR>
						<BR></BR>
					    	Would you like to shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noProduct()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Product DIV -->

<!--Begin no codified florist but has common carrier DIV -->
<DIV id="noCodifiedFloristHasCommonCarrier" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
<![CDATA[					
						I'm sorry but the product you have selected is not currently available in your delivery area.
]]>
						<BR></BR>
						<BR></BR>
					    	This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noCodifiedFloristHasCommonCarrier()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End no codified florist but has common carrier DIV -->


<!--Begin no florist but has common carrier DIV -->
<DIV id="noFloristHasCommonCarrier" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
<![CDATA[					
						I'm sorry but the zip code you have provided is not currently serviced by an FTD florist for floral deliveries.
]]>						
						<BR></BR>
						<BR></BR>
					    	This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noFloristHasCommonCarrier()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End no florist but has common carrier DIV -->

<!--Begin GNADD Upsell DIV -->
<DIV id="gnaddUpsell" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						NEED IT THERE SOONER?  
						<BR></BR>
			   		</TD>
				</TR>
				<TR> <TD>
						<xsl:value-of select="/root/pageData/data[@name='upsellBaseName']/@value"/>&nbsp;is available for delivery as soon as&nbsp;<xsl:value-of select="/root/pageData/data[@name='upsellBaseDeliveryDate']/@value"/>.
				</TD> </TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_yes.gif" onclick="javascript:acceptGnaddUpsell()"></IMG> &nbsp; &nbsp;
						<IMG  src="../images/button_no.gif" onclick="javascript:gnaddUpsell()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End GNADD Upsell DIV -->

<!--Begin Product Unavailable DIV -->
<DIV id="productUnavailable" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				
				<xsl:choose>
				<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'noMonday'">
				<TR>
					<TD align="center">
						This product is not available for delivery on Monday in your delivery area.
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
				<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exAlways'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
				<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exMonday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Monday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Monday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
				<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exTuesday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Tuesday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Tuesday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exWednesday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Wednesday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Wednesday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exThursday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Thursday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Thursday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exFriday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Friday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Friday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exSaturday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Saturday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Saturday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/pageData/data[@name='displayProductUnavailableStatus']/@value = 'exSunday'">
				<TR>
					<TD align="center">
						<xsl:choose>
							<xsl:when test="/root/productList/products/product/@productType = 'FRECUT' or /root/productList/products/product/@productType = 'SDFC'">
								This product is not available for delivery on Sunday due to Department of Agriculture restrictions in your delivery area.
							</xsl:when>
							<xsl:otherwise>
							  This product is not available for delivery on Sunday in your delivery area.
							</xsl:otherwise>
						</xsl:choose>
						<BR></BR>
						<BR></BR>
					    	Please select another product.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
          		<xsl:when test="/root/productList/products/product/@weboeBlocked = 'Y'">
				<TR>
					<TD align="center">
						I'm sorry but this product is unavailable for phone orders.
						<BR></BR>
						<BR></BR>
					    	Would you like to shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
		  		<xsl:when test="/root/productList/products/product/@status = 'A'">
				<TR>
					<TD align="center">
						I'm sorry but the product you have selected is not currently available in your delivery area.
						<BR></BR>
						<BR></BR>
					    	Would you like to shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:when>
	  			<xsl:otherwise>
	            <TR>
					<TD align="center">
						Product is currently unavailable.  Please select another product.
						<BR></BR>
						
					    
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
				</xsl:otherwise>
	  			</xsl:choose>				
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Product Unavailable DIV -->

<!--Begin JCPenney Food DIV -->
<DIV id="jcpFood" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						We're sorry, food products are unavailable for purchase using a JCPenney credit card.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:closeJcpFoodItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPenney Food DIV -->

<!--Begin Floral only DIV -->
<DIV id="onlyFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						Only Floral items can be delivered to this Zip/Postal Code.
						<BR></BR>
						<BR></BR>
					    Would you be interested in selecting a floral item?
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:onlyFloral()"></IMG>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItem()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Only Floral DIV -->

<!--Begin Codified Special DIV -->
<DIV id="codifiedSpecial" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						We're sorry the product you've selected is currently not available in the
						zip code you entered.  However, the majority of our flowers can be delivered
						to this zip code.
						<BR/><BR/>

						If this is an incorrect zip code, please re-enter the zip code on the Occasion page.  
						Otherwise, please click the "Back" button to return to shopping.
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Codified Special DIV -->


<!--Begin JCP Popup DIV -->
<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						At this time, JC Penney accepts orders going to or coming from the 50 United States
						only.  We can process your Order using any major credit card through FTD.com.
						<BR></BR>
						<BR></BR>
					    Would you like to proceed?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPPop DIV -->

</FORM>

<!-- insert footer
	<xsl:call-template name="footer"/>	-->

<!--SERVER SIDE VALIDATION SECTION-->
<xsl:call-template name="validation"/>
	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
	</DIV>

	<DIV id="calendarLoading" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... loading!</font> </p>
	</DIV>

</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
