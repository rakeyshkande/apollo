<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY original "&#174;">
	<!ENTITY quot "&#54;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="footer.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="occasion" select="root/parameters/occasion"/>
<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="indexid" select="root/parameters/indexid"/>
<xsl:variable name="productListServlet" select="root/parameters/productListServlet"/>
<xsl:variable name="color" select="root/parameters/color"/>
<xsl:variable name="name" select="root/parameters/name"/>
<xsl:variable name="recipient" select="root/parameters/recipient"/>
<xsl:variable name="search" select="root/parameters/search"/>
<xsl:variable name="intlflag" select="root/parameters/intlflag"/>
<xsl:variable name="sortType" select="root/parameters/sortType"/>
<xsl:variable name="keywords" select="root/parameters/keywords"/>
<xsl:variable name="itemCartNumber" select="root/parameters/itemCartNumber"/>
<xsl:variable name="category" select="root/parameters/category"/>
<xsl:variable name="advSearchType" select="root/parameters/advSearchType"/>
<xsl:variable name="companyId" select="root/parameters/companyId"/>
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>
<xsl:variable name="standardLabel" select="root/parameters/standardLabel"/>
<xsl:variable name="deluxeLabel" select="root/parameters/deluxeLabel"/>
<xsl:variable name="premiumLabel" select="root/parameters/premiumLabel"/>
<xsl:template match="/">

<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>
<SCRIPT language="JavaScript">

	document.oncontextmenu=stopIt;
	
	var categoryIndex = "";
	
	<!-- For regular index searches -->
	<xsl:if test="$indexid != ''">
			categoryIndex = "<xsl:value-of select="$indexid"/>";
	</xsl:if>
	
	<!-- For International index searches -->
	<xsl:if test="/root/pageData/data[@name = 'indexId']/@value != ''">
			categoryIndex = "<xsl:value-of select="/root/pageData/data[@name='indexId']/@value"/>";
	</xsl:if>
	
	<!-- For advanced searches searches -->
	<xsl:if test="$category != ''">
			categoryIndex = "<xsl:value-of select="$category"/>";
	</xsl:if>
	
	//alert("categoryIndex = " + categoryIndex);
	
 	var totpages = <xsl:value-of select="/root/pageData/data[@name='totalPages']/@value"/>;
 	var currPage = <xsl:value-of select="/root/pageData/data[@name='currentPage']/@value"/>;
 	var pid;
	var sessionId = '<xsl:value-of select="$sessionId"/>';
	var persistentObjId = '<xsl:value-of select="$persistentObjId"/>';
	var itemCartNumber = '<xsl:value-of select="$itemCartNumber"/>';
	var rewardType = "<xsl:value-of select="/root/pageData/data[@name='rewardType']/@value"/>";
	var productListServlet = "<xsl:value-of select="$productListServlet"/>";
	var companyId = "<xsl:value-of select="$companyId"/>";
	var imageSuffixMedium = '<xsl:value-of select="$imageSuffixMedium"/>';

	var color = "<xsl:value-of select="$color"/>";
	var recipient = "<xsl:value-of select="$recipient"/>";
	var name = "<xsl:value-of select="$name"/>";
	var search = "<xsl:value-of select="$search"/>";
	var occasionDescription = "<xsl:value-of select="$occasion"/>";
	var keywords = "<xsl:value-of select="$keywords"/>";
	var sortType = "<xsl:value-of select="$sortType"/>";
	var advSearchType = "<xsl:value-of select="$advSearchType"/>";
	
	
<![CDATA[

	document.onkeydown = backKeyHandler;

	function deCode(strSelection)
	{
		//strSelection = strSelection.replace(new RegExp("\';","g"), "\\\'");
		strSelection = strSelection.replace(new RegExp("&quot;","g"), "\"");
		strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
		strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

		return strSelection;
	}

	function openUpdateWin()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-100

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

    /***
    * This function will determine if the "NEXT" and "Previous" should appear
    * in the page scrolling
    */
	function init()
	{
		if(currPage == "1")
		{
			if(document.all.PrevPageSection != null)
			{
				document.all.PrevPageSection.style.display = "none";
				document.all.PrevPageSectionB.style.display = "none";
			}
		} else {
			if(document.all.PrevPageSection != null)
			{
				document.all.PrevPageSection.style.display = "";
				document.all.PrevPageSectionB.style.display = "";
			}
		}
		if(currPage ==	totpages)
		{
			if(document.all.NextPageSection != null)
			{
				document.all.NextPageSection.style.display = "none";
				document.all.NextPageSectionB.style.display = "none";
			}
		} else {
			if(document.all.NextPageSection != null)
			{
				document.all.NextPageSection.style.display = "";
				document.all.NextPageSectionB.style.display = "";
			}
		}
	}

    /***
    * This function initiates the viewing of a new page of products
    */
 	function viewPage(pageNum){
		//alert("execute view page");
		openUpdateWin();
		var url = productListServlet + "?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&pagenumber=" + pageNum + 
		"&categoryindex=" + categoryIndex + "&occasionDescription=" + occasionDescription + "&sortType=" + sortType + 
		"&keywords=" + keywords + "&itemCartNumber=" + itemCartNumber + "&color=" + color + "&name=" + name +
		"&recipient=" + recipient + "&search=" + search + "&advSearchType=" + advSearchType + "&companyId=" + companyId;
		document.location = url;
 	}

    /***
    * This function initiates the viewing of a products filtered v=by selected price
    */
 	function priceFilter(inPrice){
    	//alert("price filter " + inPrice + " " + categoryIndex + " here");
		openUpdateWin();
    	if (inPrice > 0)
    	{
			var url = productListServlet + "?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + 
			"&categoryindex=" + categoryIndex + "&pricepointid=" + inPrice + "&color=" + color + "&name=" + name +
			"&recipient=" + recipient + "&search=" + search + "&occasionDescription=" + occasionDescription +
			"&keywords=" + keywords + "&itemCartNumber=" + itemCartNumber + "&advSearchType=" + advSearchType + "&companyId=" + companyId;
			//alert(url);
			document.location = url;
		}
 	}
 
    /***
    * This function initiates the viewing of products sorted by price
    */
	 function sortByPrice(pageNum)
	 {
	 	openUpdateWin();
		var url = productListServlet + "?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId  + "&search=" + search + "&categoryindex=" + categoryIndex + "&occasionDescription=" + occasionDescription + "&pagenumber=" + pageNum + "&sortType=priceSortAsc" + "&keywords=" + keywords + "&itemCartNumber=" + itemCartNumber + "&advSearchType=" + advSearchType + "&companyId=" + companyId;
		document.location = url;
	 }
	
    /***
    * This function opens a window and displays a large image of product
    */
	 function viewLargeImage(prodId){
	
			 	var closeTag= "<A href=\"javascript:self.close();\" tabindex='-1'>close the window</A>";
				imageWindow = window.open("","enlarged_view","toolbar=no,resizable=no,scrollbars=no,width=340,height=470");
				imageWindow.document.write("<HTML>");
			 	imageWindow.document.write("<HEAD>");
			 	imageWindow.document.write("<TITLE>Large Image</TITLE></HEAD><BODY><FORM><CENTER>");
			 	imageWindow.document.write("<BR></BR>");
			 	imageWindow.document.write(closeTag);
			 	imageWindow.document.write("<BR></BR>");
			 	imageWindow.document.write("<img ")
	 			imageWindow.document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
				imageWindow.document.write("if(imag != \'npi" + imageSuffixMedium + "\'){ ")
				imageWindow.document.write("this.src = \'/product_images/npi.jpg\'}\" ")
				imageWindow.document.write("src=\"/product_images/" + prodId + imageSuffixMedium + "\"> ")
				imageWindow.document.write("</img>")
			 	imageWindow.document.write("<BR></BR>");
			 	imageWindow.document.write(closeTag);
			 	imageWindow.document.write("</CENTER></FORM></BODY></HTML>");
	
	 }
	
    /***
    * This function submits to the product detail page
    */
 	function orderNow(inProdId)
 	{
 		//document.location
 		var actiontarget = "ProductDetailServlet";
		document.location = actiontarget + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&productid=" + inProdId + "&cartItemNumber=" + itemCartNumber + "&addWithSameRecpFlag=Y" + "&companyId=" + companyId;
 		
 	}

	function doShoppingCartLocal()
	{
		if(confirm("Abandon ordering the current item and proceed to the Shopping Cart page?"))
		{
		document.location = "ShoppingCartServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&companyId=" + companyId;
		}
	}

    /***
    *   This function redirects to the Shopping Cart page
    */
	function doCancelOrderLocal()
	{
		if(confirm("Abandon ordering the current item and cancel your entire Order?"))
		{
		document.location = "CancelOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&companyId=" + companyId;
		}
	}
 ]]>
</SCRIPT>
<HTML>
<HEAD>
<TITLE> FTD - Product List </TITLE>
<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
	<FORM name="productListform" method="get" action="">
		<CENTER>
		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
				<TD align="right"> <IMG src="../images/headerFAQ.gif" onclick="javascript:doFAQ()" tabindex="-1"/> </TD>
			</TR>

			<TR> <TD colspan="6"> <HR> </HR> </TD> </TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD width="65%"> &nbsp; </TD>
				<TD align="right" ><A href="javascript: doShoppingCartLocal()" STYLE="text-decoration:none" tabindex="-1"><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A> </TD>
				<TD align="right" ><A href="javascript:doCancelOrderLocal()" STYLE="text-decoration:none" tabindex="-1">Cancel Order</A> </TD>
			</TR>
			<TR>
				<TD colspan="4"><xsl:call-template name="customerHeader"/> 	 &nbsp; </TD>
			</TR>
 		</TABLE>

		<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD class="tblheader" colspan="8">
        	 	&nbsp;
				<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
				<xsl:variable name="categories" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>

					<a href="{$occasionlbl}" class="tblheader" tabindex="-1">Occasion</a> &nbsp; >
					<xsl:if test="$intlflag = 'D' or $intlflag = ''">
						<a href="{$categories}" class="tblheader" tabindex="-1">Category</a> &nbsp; >
					</xsl:if>
					Product List
        		</TD>
     		</TR>
		</TABLE>

		<!-- Main Table -->
		<xsl:choose>
			<xsl:when test="count(/root/productList/products/product) != 0">
				<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
					<TR>
						<TD>
							<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
								<TR>
									<TD width="70%" align="right">
										<A href="javascript: sortByPrice(currPage)" tabindex="-1">Sort By Price</A>
									</TD>
									<TD width="30%" align="right">
										<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
											<TR>
												<TD align="right"> Filter By Price: &nbsp; </TD>
												<TD width="10%">
													<SELECT name="pricePointFilter" onchange="javascript: priceFilter(document.forms[0].pricePointFilter.value)" tabindex="-1">
														<OPTION value="0"> &nbsp; </OPTION>
														<xsl:for-each select="/root/pricePointList/pricePoints/pricePoint">
															<OPTION value="{@pricePointsId}"> <xsl:value-of select="@pricePointDescription"/></OPTION>
														</xsl:for-each>
													</SELECT>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>

							<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
								<TR>
									<TD align="right" valign="top"> Page &nbsp; </TD>
									<TD align="right" valign="top" width="1">
                                		<DIV ID="PrevPageSection">
                                       		<A href="javascript: viewPage(currPage - 1)" tabindex="-1">Previous</A>
                                		</DIV>
       								</TD>
                            		<TD align="right" valign="top" width="1">
 										<SCRIPT language="JavaScript">
										<![CDATA[
											for (var i = 0; i < totpages; i++){
												var x = i + 1;
												if(x == currPage)
												{
									 				document.write("<TD align=\"right\" valign=\"top\" width=\"1\">" + x + "</TD>")
												} else {
									 				document.write("<TD align=\"right\" valign=\"top\" width=\"1\"><A href=\"javascript: viewPage(" + x + ")\" tabindex='-1'>" + x + "</A> &nbsp;&nbsp;</TD>")
												}
											}
										]]>
 										</SCRIPT>
                            		</TD>
									<TD align="right" valign="top" width="1">
                                		<DIV ID="NextPageSection">
											<A href="javascript: viewPage(currPage + 1)" tabindex="-1">Next</A>
                                		</DIV>
                                	</TD>
								</TR>
								<TR> &nbsp; </TR>
							</TABLE>
							
							<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
								<xsl:variable name="rewardType" select="/root/pageData/data[@name='rewardType']/@value"/>
								<TR> &nbsp; </TR>
								<TR>
									<TD class="TblTextHeader" width="15%">&nbsp;  </TD>
									<TD class="TblTextHeader" width="20%"> Price (
                                                                <xsl:if test="$rewardType = 'TotSavings'">
                                                    								Dollars
                                                                </xsl:if>
                                                                <xsl:if test="$rewardType != 'TotSavings'">
                                                    								<xsl:value-of select="$rewardType"/>
                                                                </xsl:if>
                                                                ) </TD>
									<TD class="TblTextHeader" width="65%"> Name / Description </TD>
								</TR>
	
								<xsl:for-each select="/root/productList/products/product">
								
									<script language="javascript">
										var number_ = '<xsl:number value="position()"/>';
									</script>
						
									<TR>
										<TD align="center" valign="top">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
												<TR>
													<TD align="center">
													<SCRIPT language="JavaScript">
												
														// Exception display trigger variables
														var exceptionMessageTrigger = false;
														var selectedDate = new Date("<xsl:value-of select="/root/pageData/data[@name='requestedDeliveryDate']/@value"/>");
														var exceptionStart = new Date("<xsl:value-of select="@exceptionStartDate"/>");
														var exceptionEnd = new Date("<xsl:value-of select="@exceptionEndDate"/>");
							
														var pid = "<xsl:value-of select="@novatorID"/>";
														var imageSuffixSmall = '<xsl:value-of select="$imageSuffixSmall"/>';
			 											<![CDATA[
			 											document.write("<img onclick=\"javascript: viewLargeImage(\'" + pid + "\')\" ")
			 											document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
														document.write("if(imag != \'npi" + imageSuffixSmall + "\'){ ")
														document.write("this.src = \'/product_images/npi_1.gif\'}\" ")
														document.write("src=\"/product_images/" + pid + imageSuffixSmall + "\"> ")
														document.write("</img>")
														]]>
													</SCRIPT>
													</TD>
												</TR>
			
												<TR>	
													<TD align="center">		
													<SCRIPT language="JavaScript">
			 											var newpid = "<xsl:value-of select="@productID"/>";
			 											<![CDATA[
			 												document.write("<A href=\"javascript: orderNow(\'" + newpid + "\')\" tabindex=\'" + number_ + "\'> Order Now </A> &nbsp;&nbsp;")
														]]>
													</SCRIPT>
													</TD>
												</TR>
									
												<xsl:if test="@firstDeliveryMethod = 'florist'">
													<TR>
														<TD align="center" bgcolor="#C1DEF3">
															FTD
															<script>
															<![CDATA[
															document.write("&#174;")
															]]>
															</script>
															Florist
														</TD>
													</TR>
			    								</xsl:if>
			    								
												<xsl:if test="@firstDeliveryMethod = 'carrier'">
													<TR>
														<TD align="center" bgcolor="#FF9933"> <xsl:value-of select="@carrier"/> </TD>
													</TR>
			    								</xsl:if>
											</TABLE>
										</TD>
										<TD valign="top">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
											<xsl:if test="@standardPrice > 0">
												<TR>
													<TD class="tblText" valign="top" >
														<xsl:choose>
															<xsl:when test="@standardDiscountPrice > 0 and @standardDiscountPrice != @standardPrice">
																<SPAN style="color='red'"><strike>$<xsl:value-of select="@standardPrice"/> </strike></SPAN>
																$<xsl:value-of select="@standardDiscountPrice"/>
															</xsl:when>
    														<xsl:otherwise>
																$<xsl:value-of select="@standardPrice"/>
															</xsl:otherwise>
														</xsl:choose>
                                                                                                                <xsl:choose>
                                                                                                                    <xsl:when test="$standardLabel != ''">
                                                                                                                        &nbsp; - <xsl:value-of select="$standardLabel"/>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:otherwise>
                                                                                                                        &nbsp; - Shown
                                                                                                                    </xsl:otherwise>
                                                                                                                </xsl:choose>
														<xsl:if test="@standardRewardValue > 0">
															<xsl:if test="$rewardType != 'Percent' and $rewardType != 'Dollars'">
																(<xsl:value-of select="@standardRewardValue"/>)
															</xsl:if>
														</xsl:if>
													</TD>
												</TR>
    										</xsl:if>

											<xsl:if test="@deluxePrice > 0">
												<TR>
													<TD class="tblText" valign="top">
														<xsl:choose>
															<xsl:when test="@deluxeDiscountPrice > 0 and @deluxeDiscountPrice != @deluxePrice">
																<SPAN style="color='red'"><strike>$<xsl:value-of select="@deluxePrice"/> </strike></SPAN>
																$<xsl:value-of select="@deluxeDiscountPrice"/>
															</xsl:when>
    														<xsl:otherwise>
																$<xsl:value-of select="@deluxePrice"/>
															</xsl:otherwise>
														</xsl:choose>
                                                                                                                <xsl:choose>
                                                                                                                    <xsl:when test="$deluxeLabel != ''">
                                                                                                                        &nbsp; - <xsl:value-of select="$deluxeLabel"/>
                                                                                                                    </xsl:when>
                                                                                                                    <xsl:otherwise>
                                                                                                                        &nbsp; - Deluxe
                                                                                                                    </xsl:otherwise>
                                                                                                                </xsl:choose>
														<xsl:if test="@deluxeRewardValue > 0">
															<xsl:if test="$rewardType != 'Percent' and $rewardType != 'Dollars'">
																	(<xsl:value-of select="@deluxeRewardValue"/>)
															</xsl:if>
														</xsl:if>
													</TD>
												</TR>
    										</xsl:if>
											<xsl:if test="@premiumPrice > 0">
												<TR> <TD class="tblText" valign="top">
												<xsl:choose>
													<xsl:when test="@premiumDiscountPrice > 0 and @premiumDiscountPrice != @premiumPrice">
														<SPAN style="color='red'"><strike>$<xsl:value-of select="@premiumPrice"/> </strike></SPAN>
														$<xsl:value-of select="@premiumDiscountPrice"/>
													</xsl:when>
			    									<xsl:otherwise>
														$<xsl:value-of select="@premiumPrice"/>
													</xsl:otherwise>
												</xsl:choose>
                                                                                                <xsl:choose>
                                                                                                    <xsl:when test="$premiumLabel != ''">
                                                                                                        &nbsp; - <xsl:value-of select="$premiumLabel"/>
                                                                                                    </xsl:when>
                                                                                                    <xsl:otherwise>
                                                                                                        &nbsp; - Premium
                                                                                                    </xsl:otherwise>
                                                                                                </xsl:choose>
												<xsl:if test="@premiumRewardValue > 0">
													<xsl:if test="$rewardType != 'Percent' and $rewardType != 'Dollars'">
														(<xsl:value-of select="@premiumRewardValue"/>)
													</xsl:if>
												</xsl:if>
												</TD> </TR>
		    								</xsl:if>
										</TABLE>
									</TD>
									<TD valign="top">
										<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
											<TR>
												<TD colspan="2" class="tblText">
													<SPAN class="label"> <xsl:value-of select="@productName" />&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productID"/> 
													
													<xsl:if test="@upsellFlag = 'Y'">
														&nbsp;&nbsp;&nbsp;&nbsp; <font color="red"> This is an Upsell Product </font> 
													</xsl:if>
													
													</SPAN> <BR> </BR>
													
													<xsl:value-of select="@longDescription"  disable-output-escaping="yes"/>
														<BR> </BR>
													<xsl:value-of select="@arrangementSize"/>
												</TD>
											</TR>

											<xsl:if test="(@productType = 'FRECUT') or (@shipMethodFlorist = 'Y' and @serviceCharge != '')">
												<TR>
													<TD width="15%" class="TblTextBold" align="left">
														Service Charge:	
													</TD>
													<TD width="85%" class="tblText" align="left"> $<xsl:value-of select="@serviceCharge"/> </TD>
												</TR>
    										</xsl:if>

											<xsl:if test="(@productType = 'FRECUT' or @productType = 'SDFC') and (@extraShippingFee != '')">
												<TR>
													<TD width="15%" class="TblTextBold" align="left">
														Saturday Delivery Charge:
													</TD>
													<TD width="85%" class="tblText" align="left"> $<xsl:value-of select="format-number(number(@extraShippingFee),'###,##0.00')"/> </TD>
												</TR>
    										</xsl:if>

											<TR>
												<TD width="20%" class="TblTextBold" align="left" valign="top">
													Delivery Method:	
												</TD>

												<TD width="70%" class="tblText">We can deliver this product as early as
													<xsl:value-of select="@deliveryDate"/> &nbsp;
													<xsl:if test="@firstDeliveryMethod = 'florist'">
												 		using an FTD Florist
													</xsl:if>
													
    												<xsl:if test="@firstDeliveryMethod = 'carrier'">
    													using <xsl:value-of select="@carrier"/>.
    												</xsl:if>
    												
    												<xsl:variable name="devtype1" select="@deliveryType1"/>
    												
    												<xsl:if test="@deliveryType1 !='' and @deliveryType1 = /root/shippingMethods/shippingMethod/method[@description=$devtype1]/@description">
	    												<TR>
	    													<TD width="20%" class="tblText">
	    														<xsl:value-of select="@deliveryType1"/>
	   														</TD>
	    													<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
																	$<xsl:value-of select="@deliveryPrice1"/>
	    														</xsl:if>
	    													</TD>
	    												</TR>
    												</xsl:if>
    												
    												<xsl:variable name="devtype2" select="@deliveryType2"/>
    												
    												<xsl:if test="@deliveryType2 !='' and @deliveryType2 = /root/shippingMethods/shippingMethod/method[@description=$devtype2]/@description">
			    										<TR>
			    											<TD width="20%" class="tblText">
			    												<xsl:value-of select="@deliveryType2"/>
		    												</TD>
			    											<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
				    												$<xsl:value-of select="@deliveryPrice2"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>
    												
    												<xsl:variable name="devtype3" select="@deliveryType3"/>
    												
    												<xsl:if test="@deliveryType3 !='' and @deliveryType3 = /root/shippingMethods/shippingMethod/method[@description=$devtype3]/@description">
    													<TR>
    														<TD width="20%" class="tblText">
    															<xsl:value-of select="@deliveryType3"/>
   															</TD>
    														<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
	    															$<xsl:value-of select="@deliveryPrice3"/>
	    														</xsl:if>
    														</TD>
    													</TR>
    												</xsl:if>
    												
    												<xsl:variable name="devtype4" select="@deliveryType4"/>
   													
   													<xsl:if test="@deliveryType4 !='' and @deliveryType4 = /root/shippingMethods/shippingMethod/method[@description=$devtype4]/@description">
    													<TR>
    														<TD width="20%" class="tblText">
    															<xsl:value-of select="@deliveryType4"/>
   															</TD>
    														<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
	    															$<xsl:value-of select="@deliveryPrice4"/>
	    														</xsl:if>
    														</TD>
    													</TR>
		    										</xsl:if>
		    										
		    										<xsl:variable name="devtype5" select="@deliveryType5"/>
		    										
		    										<xsl:if test="@deliveryType5 !='' and @deliveryType5 = /root/shippingMethods/shippingMethod/method[@description=$devtype5]/@description">
			    										<TR>
			    											<TD width="20%" class="tblText">
			    												<xsl:value-of select="@deliveryType5"/>
			    												</TD>
			    											<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
					    											$<xsl:value-of select="@deliveryPrice5"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>
		    										
		    										<xsl:variable name="devtype6" select="@deliveryType6"/>
		    										
		    										<xsl:if test="@deliveryType6 !='' and @deliveryType6 = /root/shippingMethods/shippingMethod/method[@description=$devtype6]/@description">
			    										<TR>
			    											<TD width="20%" class="tblText">
			    												<xsl:value-of select="@deliveryType6"/>
			    											</TD>
			    											<TD width="50%" class="tblText">
																<xsl:if test="@productType != 'FRECUT'">
				    												$<xsl:value-of select="@deliveryPrice6"/>
	    														</xsl:if>
			    											</TD>
			    										</TR>
		    										</xsl:if>
												</TD>
											</TR>

											<xsl:if test="@discountPercent > 0">
												<TR>
		             								<TD width="15%" class="TblTextBold" align="left">
														Discount:	</TD>
													<TD width="85%" class="tblText" align="left"> <xsl:value-of select="@discountPercent"/>% </TD>
												</TR>
    										</xsl:if>

											<xsl:choose>
												<xsl:when test="/root/pageData/data[@name='requestedDeliveryDate']/@value">
													
													<xsl:if test="@exceptionCode = 'U'">
														<SCRIPT language="JavaScript">
														<![CDATA[
															if(selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
																exceptionMessageTrigger = true;
				 												document.write("<TR><TD colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionEndDate"/><![CDATA[</font></TD></TR>");
															}
														]]>
														</SCRIPT>
													</xsl:if>
													
													<xsl:if test="@exceptionCode = 'A'">
														<SCRIPT language="JavaScript">
														<![CDATA[
															if(selectedDate < exceptionStart || selectedDate > exceptionEnd) {
																exceptionMessageTrigger = true;
				 												document.write("<TR><TD colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionEndDate"/><![CDATA[</font></TD></TR>");
															}
														]]>
														</SCRIPT>
													</xsl:if>
													
													<SCRIPT language="JavaScript">
													<![CDATA[
														if(exceptionMessageTrigger == true) {
															document.write("<TR><TD colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></TD></TR>");
														}
														else {
															document.write("<TR><TD colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></TD></TR>");
														}
													]]>
													</SCRIPT>
													
												</xsl:when>
												<xsl:otherwise>
												
													<xsl:if test="@exceptionCode = 'A'">
		    											<TR>
			   												<TD colspan="2" class="tblText" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionEndDate"/></font> </TD>
														</TR>
													</xsl:if>
													
													<xsl:if test="@exceptionCode = 'U'">
		    											<TR>
			   												<TD colspan="2" class="tblText" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionEndDate"/></font> </TD>
														</TR>
													</xsl:if>
													
													<TR>
			   											<TD colspan="2" class="tblText" align="left"><font color="red"><xsl:value-of select="@exceptionMessage"/></font></TD>
													</TR>
													
												</xsl:otherwise>
											</xsl:choose>
											

											<xsl:if test="@displaySpecialFee = 'Y'">
    											<TR>
	   												<TD colspan="2" class="tblText" align="left"> <font color="red">A $12.99 surcharge will  be added to deliver this item to Alaska or Hawaii</font> </TD>
												</TR>
											</xsl:if>
											
											<xsl:if test="@displayCableFee = 'Y'">
    											<TR>
	   												<TD colspan="2" class="tblText" align="left"> <font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font> </TD>
												</TR>
											</xsl:if>
										</TABLE>
									</TD>
								</TR>

									<TR> <TD colspan="3"> <HR> </HR> </TD> </TR>
								</xsl:for-each>
							</TABLE>

							<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
								<TR>
									<TD width="70%" align="right">
										<A href="javascript: sortByPrice(currPage)" tabindex="-1">Sort By Price</A>
									</TD>
									<TD width="30%" align="right">
										<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
											<TR>
												<TD align="right"> Filter By Price: &nbsp; </TD>
												<TD width="10%">
													<SELECT name="pricePointFilter2" onchange="javascript: priceFilter(document.forms[0].pricePointFilter2.value)" tabindex="-1">
														<OPTION value="0"> &nbsp; </OPTION>
														<xsl:for-each select="/root/pricePointList/pricePoints/pricePoint">
															<OPTION value="{@pricePointsId}"> <xsl:value-of select="@pricePointDescription"/></OPTION>
														</xsl:for-each>
													</SELECT>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
		
								<TR> <TD colspan="4">&nbsp;  </TD> </TR>

								<TR>
									<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
										<TR>
											<TD align="right" valign="top" > Page &nbsp; </TD>
											<TD align="right" valign="top" width="1">
                                				<DIV ID="PrevPageSectionB">
                                       				<A href="javascript: viewPage(currPage - 1)" tabindex="-1">Previous</A>
                                				</DIV>
											</TD>
                            				<TD align="right" valign="top" width="1">
 												<SCRIPT language="JavaScript">
												<![CDATA[
													for (var i = 0; i < totpages; i++){
														var x = i + 1;
														if(x == currPage)
														{
													 		document.write("<TD align=\"right\" valign=\"top\" width=\"1\">" + x + "</TD>")
														} else {
													 		document.write("<TD align=\"right\" valign=\"top\" width=\"1\"><A href=\"javascript: viewPage(" + x + ")\"  tabindex='-1'>" + x + "</A> &nbsp;&nbsp;</TD>")
														}
													}
												]]>
 												</SCRIPT>
											</TD>
                                			<TD align="right" valign="top" width="1">
                                				<DIV ID="NextPageSectionB">
                                                	<A href="javascript: viewPage(currPage + 1)" tabindex="-1">Next</A>
                                				</DIV>
                                			</TD>
										</TR>
										<TR> <TD> &nbsp; </TD> </TR>
									</TABLE>
								</TR>

								<TR> <TD colspan="4">&nbsp;    </TD> </TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				
				<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
		     		<TR>
		        	 	<TD class="tblheader" colspan="8">
		   					<xsl:variable name="occasionlbl1" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
							<xsl:variable name="categories1" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
							<a href="{$occasionlbl1}" class="tblheader" tabindex="-1">Occasion</a> &nbsp; > <a href="{$categories1}" class="tblheader" tabindex="-1">Category</a> &nbsp; > Product List
		        		</TD>
		     		</TR>
				</TABLE>
		
				<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
					<TR> <TD> &nbsp; </TD> </TR>
					<TR>
						<xsl:call-template name="footer"/>
					</TR>
				</TABLE>
			</xsl:when>
			<xsl:otherwise>
				<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
					<TR>
						<TD>
							<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
								<TR><td>&nbsp;</td></TR>
								<TR><td><center>No products were found matching your criteria.</center></td></TR>
								<TR><td>&nbsp;</td></TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</xsl:otherwise>
		</xsl:choose>
		</CENTER>
	</FORM>

	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
	</DIV>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
