<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>

<xsl:template match="/">
<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT>
	<![CDATA[
	function onKeyDown()
	{
		if ( window.event.keyCode == 13 )
		{
			reopenPopup();
			return false;
		}
	}

	function closeCustomerLookup(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, promoFlag)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, promoFlag);
		}
	}

	//This function passes parameters from this page to a function on the calling page
	function populatePage(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, promoFlag)
	{
		var ret = new Array();

		ret[0] = customerid;
		ret[1] = first;
		ret[2] = last;
		ret[3] = address1;
		ret[4] = address2;
		ret[5] = city;
		ret[6] = state;
		ret[7] = zip;
		ret[8] = country;
		ret[9] = phone;
		ret[10] = altPhone;
		ret[11] = altPhoneExt;
		ret[12] = email;
		ret[13] = bfh;
		ret[14] = promoFlag;
		window.returnValue = ret;
		window.close();
	}

	function deCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("\'","g"),"\\'" );

		return strSelection;
	}


//This function submits the page and populates it with new search results.
	function reopenPopup()
	{
		//First validate the phone input
		var check = true;
		var phoneInput = document.forms[0].phoneInput.value;
		var phoneInput = stripWhitespace(phoneInput);

/* REMOVE
		//Check to make sure the phone is a number
		if(!isInteger(phoneInput))
		{
			if (check == true)
			{
			  check = false;
			}
		}
*/
		if(phoneInput == "")
		{
		   document.forms[0].phoneInput.focus();
           document.forms[0].phoneInput.style.backgroundColor='pink'
           check = false;
           alert("Please correct the marked fields");
		}

		//Now that everything is valid, open the popup
		if (check)
		{
			var url_source="";
			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CUSTOMER_LOOKUP";
			document.forms[0].submit();
		}
	}

	function init()
	{
		window.name = "VIEW_CUSTOMER_LOOKUP";

		//set the focus on the first field
		document.forms[0].phoneInput.focus()
  	}

]]>
</SCRIPT>

<HTML>
<HEAD>
<TITLE> Customer Lookup </TITLE>
<LINK REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></LINK>

</HEAD>

<BODY onLoad="javascript:window.focus(); init();">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_CUSTOMER"/>
<TABLE width="100%" border="0" cellspacing="2" cellpadding="2">
    <TR>
        <TD colspan="3" align="center">
            <H1>Customer Lookup</H1>
        </TD>
    </TR>
	<TR>
		<TD WIDTH="100%" VALIGN="top">
			<CENTER>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD>
					<TABLE width="98%" border="0" cellpadding="1" cellspacing="1">
			   	       <TR>
				            <TD nowrap="true" colspan="3" align="left">
				            	<SPAN class="instruction"> Enter Phone Number </SPAN> &nbsp;
				            	<INPUT tabindex="1" type="text" name="phoneInput" size="30" maxlength="50" onFocus="javascript:fieldFocus('phoneInput')" onblur="javascript:fieldBlur('phoneInput')"/> &nbsp;
				            	<SPAN class="instruction"> and press </SPAN>
				            	<IMG tabindex="2" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/>
			            	</TD>
		  	          </TR>
		  	          <TR>
		  	          	<TD colspan="10">
		  	           		<HR></HR>
		  	            </TD>
		  	          </TR>
			            <TR>
			            	<TD class="instruction">
								&nbsp;Please click on an arrow to select a customer.
							</TD>
			    			 <TD colspan="10" align="right">
							 	<IMG tabindex="3" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','','')" onkeydown="javascript:closeCustomerLookup('','','','','','','','','','','','','','','')"/>
						 	</TD>

			    		</TR>
					</TABLE>
					<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD>
								<TABLE class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
									<TR>
										<TD width="5%" class="label"> &nbsp; </TD>
										<TD class="label" valign="bottom"> Name </TD>
										<TD class="label" valign="bottom"> Address </TD>
										<TD class="label" valign="bottom"> City </TD>
										<TD class="label" valign="bottom"> State </TD>
										<TD class="label" valign="bottom"> Zip/Postal Code</TD>
										<TD>
											<xsl:for-each select="/root/customerLookup/searchResults/searchResult">
												<TR>
													<TD>
														<SCRIPT language="javascript">
         													var customerdid = "<xsl:value-of select="@customerId"/>";
															var state = "<xsl:value-of select="@state"/>";
															var zip = "<xsl:value-of select="@zipCode"/>";
															var country = "<xsl:value-of select="@countryId"/>";
															var phone = "<xsl:value-of select="@homePhone"/>";
															var altPhone = "<xsl:value-of select="@workPhone"/>";
															var altPhoneExt = "<xsl:value-of select="@workPhoneExt"/>";
															var email = "<xsl:value-of select="@email"/>";
															var promoFlag = "<xsl:value-of select="@promoFlag"/>";
															<![CDATA[
																var firstName = deCode("]]><xsl:value-of select="@firstName"/><![CDATA[");
																var lastName = deCode("]]><xsl:value-of select="@lastName"/><![CDATA[");
																var address1 = deCode("]]><xsl:value-of select="@address1"/><![CDATA[");
																var address2 = deCode("]]><xsl:value-of select="@address2"/><![CDATA[");
																var city = deCode("]]><xsl:value-of select="@city"/><![CDATA[");
																var bfh = deCode("]]><xsl:value-of select="@bfhName"/><![CDATA[");
																document.write("<IMG onclick=\"javascript: populatePage(\'" + customerdid + "\', \'" + firstName + "\', \'" + lastName + "\', \'" + address1 + "\', \'" + address2 + "\', \'" + city + "\', \'" + state + "\', \'" + zip + "\', \'" + country + "\', \'" + phone + "\', \'" + altPhone + "\', \'" + altPhoneExt + "\', \'" + email + "\', \'" + bfh + "\', \'" + promoFlag + "\')\"")
																document.write("onkeydown=\"javascript:closeCustomerLookup(\'" + customerdid + "\', \'" + firstName + "\', \'" + lastName + "\', \'" + address1 + "\', \'" + address2 + "\', \'" + city + "\', \'" + state + "\', \'" + zip + "\', \'" + country + "\', \'" + phone + "\', \'" + altPhone + "\', \'" + altPhoneExt + "\', \'" + email + "\', \'" + bfh + "\', \'" + promoFlag + "\')\" tabindex=\"4\" src=\"../images/selectButtonRight.gif\">")
																document.write("</IMG>")
															]]>
														</SCRIPT>
													</TD>
													<TD align="left">
														<xsl:value-of select="@firstName"/> &nbsp;<xsl:value-of select="@lastName"/>
													</TD>
													<TD align="left">
														<xsl:value-of select="@address1"/> &nbsp;<xsl:value-of select="@address2"/>
													</TD>
													<TD align="left">
														<xsl:value-of select="@city"/>
													</TD>
													<TD align="left">
														<xsl:value-of select="@state"/>
													</TD>
													<TD align="left">
														<xsl:value-of select="@zipCode"/>
													</TD>
												</TR>
											</xsl:for-each>
										</TD>
									</TR>
									<TR>
										<TD>
											<IMG tabindex="5" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','','')" onkeydown="javascript:closeCustomerLookup('','','','','','','','','','','','','','','')"></IMG>
										</TD>
										<TD colspan="5"> None of the above </TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
			</TABLE>
			<TABLE width="98%">
	            <TR>
	    			 <TD align="right">
					 	<IMG tabindex="6" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','','')" onkeydown="javascript:closeCustomerLookup('','','','','','','','','','','','','','','')"/>
				 	</TD>
	    		</TR>
           </TABLE>
	</TD>
  </TR>
</TABLE>
</CENTER>
</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>