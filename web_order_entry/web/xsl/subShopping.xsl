<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="headerButtons.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="search" select="root/parameters/search"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> FTD - Shopping - Quick Shop </TITLE>
<SCRIPT language="JavaScript">
<![CDATA[
	function init()
    {
		document.forms[0].search.value = "<xsl:value-of select="$search"/>";
	}

	function submitForm()
	{
		if(validateForm())
			form.submit();
		else
			alert("Please correct the marked fields.");
	}
]]>
</SCRIPT>
<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>
<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>

<CENTER>
<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
	<TR>
		<xsl:apply-imports/>
	</TR>
</TABLE>

<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
	<TR>
		<TD width="65%"> &nbsp; </TD>
		<TD align="right" nowrap="true"><A href="shoppingCart.jsp" STYLE="text-decoration:none"><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A> </TD>
		<TD align="right" nowrap="true"><A href="cancelOrder.jsp" STYLE="text-decoration:none">Cancel Order</A> </TD>
		<TD align="right" nowrap="true"><A href="checkout.jsp" STYLE="text-decoration:none">Checkout</A> </TD>
	</TR>
	<TR>
		<TD colspan="4"> &nbsp; </TD>
	</TR>
	<TR>
		<TD class="tblheader" colspan="8">TO BE FROM XML
			<xsl:for-each select="/root/previousPages/previousPage">
	        	<a href="{@href}" class="tblheader">
	        	<xsl:value-of select="@name"/>
	        	</a> >
			</xsl:for-each>
		</TD>
	</TR>
</TABLE>

<!-- Main Table -->
<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD width="100%">
						<TABLE width="100%" border="0" cellpadding="2" cellspacing="2" align="left">

							<TR>
								<TD valign="top" height="126" width="20%">
									<IMG src="../images/titles_products.gif" width="130" height="16"/><BR></BR>
									<xsl:for-each select="/root/subShopping/subIndexes/index">
											<A href=""><xsl:value-of select="@name"/></A><BR></BR>
									</xsl:for-each>
								</TD>

								<TD align="left" class="Instruction" valign="top" width="34%">
									TO BE FROM XML
									If the customer knows the product number enter the number
									in the Product Search field and click the Search button.
									<BR></BR>
									<BR></BR>
									TO BE FROM XML
									If they do not have a product number, use the shopping links.
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>

				<TR> <TD> &nbsp; </TD> </TR>

				<TR>
					<TD class="Instruction">
						TO BE FROM XML
						Enter the Item Number, Catalog Page or Keyword in the search field then select the search type and click Search.
					</TD>
				</TR>

				<TR>
					<TD valign="top">
						<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
							<TR>
								<TD width="80%">
									<SPAN class="label"> Product Search: </SPAN> &nbsp;&nbsp;

									<SELECT name="select">
										<OPTION value="ItemNumber">Item Number</OPTION>
										<OPTION value="CatalogPage">Catalog Page</OPTION>
										<OPTION value="Keyword">Keyword</OPTION>
									</SELECT> &nbsp;&nbsp;

									<INPUT type="text" name="search" onclick=""/> &nbsp;&nbsp;
									<IMG src="../images/button_go.gif" onclick=""/> &nbsp;&nbsp;
									<A href="javascript:something()"> Advanced Search </A>
								</TD>

								<TD width="20%" align="right">
									<IMG onclick="" src="../images/button_customorder.gif" alt="Custom Order"/> &nbsp;&nbsp;
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>

<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
	<TR>
		<TD class="tblheader" colspan="8">TO BE FROM XML
			<xsl:for-each select="/root/previousPages/previousPage">
	        	<a href="{@href}" class="tblheader">
	        	<xsl:value-of select="@name"/>
	        	</a> >
			</xsl:for-each>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp; </TD>
	</TR>
	<TR>
		<TD class="disclaimer">
			<DIV align="center">COPYRIGHT 2005. FTD INC. ALL RIGHTS RESERVED. </DIV>
		</TD>
	</TR>
</TABLE>
</CENTER>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
