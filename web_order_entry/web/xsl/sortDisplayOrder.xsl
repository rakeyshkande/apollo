<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="/">
	
	<productList>
		<products>
	
			<xsl:for-each select="productList/products/product">
				<xsl:sort select="@displayOrder" data-type="number"/>
				<xsl:copy-of select="."/>
			</xsl:for-each>
    
    		</products>
    
    		<xsl:copy-of select="productList/globalParmsData"/>
    		<xsl:copy-of select="productList/shippingMethods"/>
    		<xsl:copy-of select="productList/holidayDates"/>
    		<xsl:copy-of select="productList/deliveryRangeData"/>
	</productList>
    
</xsl:template>

</xsl:stylesheet>