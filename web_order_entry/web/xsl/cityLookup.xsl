<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="cityInput" select="root/parameters/cityInput"/>
<xsl:variable name="stateInput" select="root/parameters/stateInput"/>
<xsl:variable name="zipCodeInput" select="root/parameters/zipCodeInput"/>
<xsl:variable name="countryVal" select="root/parameters/countryVal"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE>City Lookup </TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">
	<![CDATA[

	function onKeyDown()
	{
		if ( window.event.keyCode == 13 )
		{
			reopenPopup();
			return false;
		}
	}

	//This function passes parameters from this page to a function on the calling page
	function populatePage(city, zip, state)
	{
		var ret = new Array();

		ret[0] = city;
		ret[1] = zip;
		ret[2] = state;

		window.returnValue = ret;
		window.close();

	}

	function closeCityLookup(city, zip, state)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(city, zip, state);
		}
	}

	function reopenPopup()
	{
		//initialize the background colors
		document.all.cityInput.style.backgroundColor='white';
		document.all.stateInput.style.backgroundColor='white';
		document.all.zipCodeInput.style.backgroundColor='white';

		//First validate the inputs
		check = true;

        state = stripWhitespace(document.all.stateInput.value);
        city = stripWhitespace(document.all.cityInput.value);
        zip = stripWhitespace(document.forms[0].zipCodeInput.value);

        //State is required if zip is empty
        if((state.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.stateInput.focus();
		 	  check = false;
			}
			  document.all.stateInput.style.backgroundColor='pink';
		} // close state if

		//City is required if zip is empty
        if((city.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.cityInput.focus();
		 	  check = false;
			}
			  document.all.cityInput.style.backgroundColor='pink';
		} // close state if

		if (!check)
		{
			alert("Please correct the marked fields");
		 	return false;
		}

        //Now that everything is valid, open the popup
		if (check)
		{
			var url_source="";
			document.forms[0].action = url_source;
			document.forms[0].method = "get";
			document.forms[0].target = "VIEW_CITY_LOOKUP";
			document.forms[0].submit();
        }
     }

	function init()
	{
		window.name = "VIEW_CITY_LOOKUP";


     //Populate the countryTypeInput and the search criteria inputs
  ]]>
		document.forms[0].countryVal.value = "<xsl:value-of select="$countryVal"/>";
		document.forms[0].cityInput.value = "<xsl:value-of select="$cityInput"/>";
  	    document.forms[0].stateInput.value = "<xsl:value-of select="$stateInput"/>";
  		document.forms[0].zipCodeInput.value = "<xsl:value-of select="$zipCodeInput"/>";

	<![CDATA[
	//set the focus on the first field
	document.forms[0].cityInput.focus();
  	}
	]]>

</SCRIPT>
<link REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></link>
</HEAD>

<BODY onLoad="javascript:window.focus(); init();">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_ZIP_CODE"/>
<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
<INPUT type="hidden" name="countryVal" value=""/>

	<TABLE width="98%" border="0" cellspacing="0" cellpadding="2">
		<TR>
			<TD WIDTH="100%" VALIGN="top">
				<BR></BR>
				<H1 align="CENTER"> City Lookup </H1>
				<CENTER>
				<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
		            <TD width="50%" class="label" align="right"> City: </TD>
		            <TD width="50%">
		                <INPUT class="TblText" tabindex="0" type="text" name="cityInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"/>
		            </TD>
	            </TR>
				<TR>
					<TD class="labelright"> State: </TD>
					<TD>
	            <xsl:choose>
				<xsl:when test="$countryVal ='CA'">
				     <SELECT tabindex="1" class="TblText" name="stateInput">
            			     <OPTION value=""></OPTION>
						<xsl:for-each select="/root/zipCodeLookup/states/state[@countryCode='CAN']">
					        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
						</xsl:for-each>
					 </SELECT>
				</xsl:when>
				<xsl:when test="$countryVal ='US'">
				     <SELECT tabindex="2" class="TblText" name="stateInput">
				     		<OPTION value=""></OPTION>
						<xsl:for-each select="/root/zipCodeLookup/states/state[@countryCode='']">
					        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
						</xsl:for-each>
					 </SELECT>
				</xsl:when>
				<!--if from Product Detail page, then the countryType is being passed into the countryVal -->
         		<!--whereas on every other page the countryId is being passed-->
         		<!--so show US and Canada-->
				<xsl:when test="$countryVal ='D'">
				     <SELECT tabindex="3" class="TblText" name="stateInput">
		            	    <OPTION value=""></OPTION>
						<xsl:for-each select="/root/zipCodeLookup/states/state">
					        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
						</xsl:for-each>
					 </SELECT>
				</xsl:when>
				<xsl:when test="$countryVal !='US' and $countryVal !='CA' and $countryVal !='D'">
		              <INPUT class="TblText" tabindex="3" type="text" name="stateInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"/>
   				</xsl:when>
		       </xsl:choose>
		            </TD>
		        </TR>
				<TR>
					<TD class="labelright"> Zip/Postal Code:</TD>
					<TD>
		                <INPUT class="TblText" tabindex="4" type="text" name="zipCodeInput" size="5" maxlength="10" onFocus="javascript:fieldFocus('zipCodeInput')" onblur="javascript:fieldBlur('zipCodeInput')"/>
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD>
						<IMG tabindex="5" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/>
					</TD>
				</TR>
				<TR>
					<TD colspan="10">
					<HR></HR>
					</TD>
				</TR>
     				<CENTER>
					<TABLE width="100%" id="FormContainer">
		    		<TR>
						<TD class="instruction">
							&nbsp;Please click on an arrow to select a customer.
						</TD>
		    			<TD align="right">
						 	<IMG tabindex="6" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeCityLookup('')"/>
					 	</TD>
		    		</TR>
					<TR>
							<TABLE class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
							<TR>
								<TD width="5%" class="label"> &nbsp; </TD>
								<TD class="label" valign="bottom"> City </TD>
								<TD class="label" valign="bottom"> State </TD>
								<TD width="65%" class="label" valign="bottom"> Zip/Postal Code </TD>
							</TR>
							<TR>
								<TD>
									<xsl:for-each select="/root/zipCodeLookup/searchResults/searchResult">
										<TR>
											<TD width="5%">
												<SCRIPT language="javascript">
													var city = "<xsl:value-of select="@city"/>";
													var zip = "<xsl:value-of select="@zipCode"/>";
													var state = "<xsl:value-of select="@state"/>";
												<![CDATA[
													document.write("<IMG onclick=\"javascript: populatePage(\'" + city + "\', \'" + zip + "\', \'" + state + "\')\"")
													document.write("onkeydown=\"javascript: closeCityLookup(\'" + city + "\', \'" + zip + "\', \'" + state + "\')\" tabindex=\"7\" src=\"../images/selectButtonRight.gif\">")
													document.write("</IMG>")
													]]>
												</SCRIPT>
											</TD>
											<TD align="left">
												<xsl:value-of select="@city"/>
											</TD>
											<TD>
												<xsl:value-of select="@state"/>
											</TD>
											<TD>
     											<xsl:value-of select="@zipCode"/>
											</TD>
										</TR>
									</xsl:for-each>
								</TD>
							</TR>
							<TR>
								<TD>
									<IMG tabindex="8" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');" onkeydown="javascript:closeCityLookup('','','')"></IMG>
								</TD>
								<TD colspan="8"> None of the above </TD>
							</TR>
						</TABLE>
				</TR>
				<TABLE width="98%">
					<TR>
	    				<TD align="right">
	    					<IMG tabindex="9" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeCityLookup('','','')"/>
	   					</TD>
			    	</TR>
		    	</TABLE>
			</TABLE>
		</CENTER>
	</TABLE>
</CENTER>
</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>