<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="occasion" select="root/parameters/occasion"/>
<xsl:variable name="occasionDescription" select="root/parameters/occasionDescription"/>
<xsl:variable name="itemCartNumber" select="root/parameters/itemCartNumber"/>
<xsl:variable name="companyId" select="/root/shoppingData/scripting/script[@fieldName='CATEGORIES']/@companyId"/>";



<xsl:template match="/">

<HTML>
<HEAD>
	<TITLE> FTD - Shopping - Quick Shop </TITLE>
	<script language="javascript" src="../js/util.js"/>
	<SCRIPT language="JavaScript">
	
		document.oncontextmenu=stopIt;
		var disableFlag = false;

	 	var sessionId = '<xsl:value-of select="$sessionId"/>';
		var persistentObjId = '<xsl:value-of select="$persistentObjId"/>';
		var itemCartNumber = '<xsl:value-of select="$itemCartNumber"/>';

		var occasionDescription = "<xsl:value-of select="$occasion"/>";
	 	var companyId = '<xsl:value-of select="$companyId"/>';

		if(occasionDescription == "")
		{
			<xsl:variable name="occasionId" select="/root/orderOccasion/orderOccasion[@name = 'occasionId']/@value"/>
			occasionDescription = "<xsl:value-of select="/root/shoppingData/occasions/occasion[@occasionId = $occasionId]/@description"/>";
		}
	<![CDATA[

		//document.onkeydown = backKeyHandler;
		var subcategoryHTML = new Array();
		var subcategoryHTMLMapping = new Array();
		var c_counter = 0;
		var catwinopen = "no";

		function document.onkeydown()
		{
			if ( window.event.keyCode == 13 )
			{
				return false;
			}
			backKeyHandler();
		}

		function doSearchEnter()
		{
		   if (window.event.keyCode == 13)
			{
			    doSearch();
			}
		}

		function startSubCategoryHTML(_categoryName)
		{
			subcategoryHTML[c_counter] = "";
			subcategoryHTMLMapping[c_counter] = _categoryName;
			subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + "<strong>" + "<font face=Verdana>" + _categoryName + " List</font><br>" + "</strong>";
		}

		function endSubCategoryHTML()
		{
			c_counter = c_counter + 1;
		}

		function addSubCategoryHTML(_name, _index, haschildren)
		{

			if(haschildren == "Y")
			{
			subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + "<A href=ProductListServlet?categoryindex=" + _index + "&amp;sessionId=" + sessionId + "&amp;persistentObjId=" + persistentObjId + "&amp;itemCartNumber=" + itemCartNumber + "&search=category" + "&occasionDescription=" + occasionDescription  + "&companyId=" + companyId + "><font face=Verdana>" + _name + "</font></A><BR>";
			}
			else
			{
			subcategoryHTML[c_counter] = "ProductListServlet?categoryindex=" + _index + "&amp;sessionId=" + sessionId + "&amp;persistentObjId=" + persistentObjId + "&amp;itemCartNumber=" + itemCartNumber + "&occasionDescription=" + occasionDescription + "&search=category" + "&companyId=" + companyId;
			}

		}

		function init()
	    	{
	    		document.all.searchSelect.focus();
	    		document.all.occasionDescriptionDiv.innerHTML = occasionDescription;

	    		document.forms["advancesearch"].itemCartNumber.value = itemCartNumber;

		}

		function initbox(w, h)
		{
			var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers

			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			crossobj=(dom)?document.getElementById("dropin").style : ie? document.all.dropin : document.dropin
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
			crossobj.top=scroll_top+document.body.clientHeight/2-h

			crossobj.width=w
			crossobj.height=h
			crossobj.left=document.body.clientWidth/2 - w/2
			crossobj.visibility=(dom||ie)? "visible" : "show"

			document.all.occasion.focus();
		}


		function dismissbox()
		{
			document.all.main.style.visibility = "visible";
			crossobj.visibility="hidden"
		}

		function dismissboxEnter()
		{
			if (window.event.keyCode == 13)
			{
			    document.all.main.style.visibility = "visible";
			    crossobj.visibility="hidden"
			}
		}

		function doAdvancedSearch()
		{
			document.all.main.style.visibility = "hidden";
			initbox(600, 200);
		}

		function doSearch()
		{
			if(disableFlag == false) 
			{
	]]>
				disableFlag=true;
				var pgName = "<xsl:value-of select="/root/shoppingData/scripting/script[@fieldName='CATEGORIES']/@pageName"/>";
				var cpmyName = "<xsl:value-of select="/root/shoppingData/scripting/script[@fieldName='CATEGORIES']/@companyId"/>";
		<![CDATA[
	
				var searchType = "";
				if(document.ctrl.searchSelect.value == "simpleproductsearch")
				{
					searchType = "&productId=";
				}
				else
				{
					searchType = "&searchKeyword=";
				}
				
				openUpdateWin();
				document.location = "SearchServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&itemCartNumber=" + itemCartNumber +
						searchType + document.ctrl.searchInput.value + "&pagename=" + pgName
						+ "&company=" + cpmyName + "&search=" + document.ctrl.searchSelect.value + "&companyId=" + companyId;
			}
		}

		function doCustumOrder()
		{
		document.location = "CustomOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&itemCartNumber=" + itemCartNumber + "&companyId=" + companyId;
		}


		function doShoppingCart()
		{
		if(confirm("Abandon ordering the current item and proceed to the Shopping Cart page?"))
		{
			document.location = "ShoppingCartServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId  + "&companyId=" + companyId;
		}
		}

		function doCancelOrder()
		{
		if(confirm("Abandon ordering the current item and cancel your entire Order?"))
		{
			document.location = "CancelOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId;
		}
		}


		function doShowCategoryDetail(_sender)
		{
			for (var i = 0; i < c_counter; i++)
			{
				if(_sender.id == subcategoryHTMLMapping[i])
				{
					document.all.subcategorydetail.innerHTML = subcategoryHTML[i];
					break;
				}
			}
		}

		var currentButton;
		var isWinMoving = false;
		var isIE = document.all?true:false;
		var curX = 0;
		var curY = 0;
		if (!isIE) document.captureEvents(Event.MOUSEMOVE);
		document.onmousemove = getMousePosition;


		function doShowCategory(w, h, sender, haschildren)
		{
			if(haschildren == 'no')
			{
				for (var j = 0; j < c_counter; j++)
				{
					if(sender.id == subcategoryHTMLMapping[j])
					{
						document.location = subcategoryHTML[j];
						break;
					}
				}
				return true;
			}

			var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers

			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			subcategorywin = (dom)?document.getElementById("subcategory").style : ie? document.all.subcategory : document.subcategory
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset;
			//subcategorywin.top=scroll_top+document.body.clientHeight/2-h;

			for (var i = 0; i < c_counter; i++)
			{
				if(sender.id == subcategoryHTMLMapping[i])
				{
					subcategorydetail.innerHTML = subcategoryHTML[i];
					break;
				}
			}

			if(curX == 0 && curX ==0)
			{

				subcategorywin.top=scroll_top + document.body.clientHeight/2 - 155;
				subcategorywin.left=document.body.clientWidth/2 - w/2;
			}
			else
			{
				subcategorywin.left=curX;
				subcategorywin.top=scroll_top + curY;
			}
			subcategorywin.height = 200;
			subcategorywin.visibility=(dom||ie)? "visible" : "show"
			catwinopen = "yes";
		}


		function getMousePosition(e)
		{

	   		if(isWinMoving)
	  		{
	    		_x = event.clientX ;
	    		_y = event.clientY ;
				subcategorywin.left = _x - 15;
				subcategorywin.top = _y - 10;
				curY = _y-10;
				curX = _x-15;
	  		}

	  	return true;
		}

		function moveBtn_onmouseclick(button)
		{
			button.style.cursor =  "move";

			if(isWinMoving	== true)
			{
				isWinMoving = false
			}
			else
			isWinMoving = true;
		}


	function dismisssubcatbox()
	{
			catwinopen = "no"
			subcategorywin.visibility="hidden"
	}

	function doHighlightCatBtn(button)
	{
		if(button.ftype == "product")
		{
			doShowByProduct();
		}
		else
		if(button.ftype == "occasion")
		{
			doShowByOccasion();
		}
		if(button.ftype == "price")
		{
			doShowByPrice();
		}
			button.focus();
			if(currentButton)
			{
				currentButton.style.fontWeight = 'normal';
			}

			button.style.fontWeight = 'bold';
			currentButton = button;
	}


	function doShowByPrice()
	{
		currlevel = "price";
		//ctrl.priceBtn.style.backgroundColor = "#F0E68C";
		ctrl.occasionBtn.style.backgroundColor = "#FFF8DC";
		ctrl.productBtn.style.backgroundColor = "#FFF8DC";

		document.all.occasionLayer.style.backgroundColor = "#FFFFFF";
		//document.all.priceLayer.style.backgroundColor = "#FFF8DC";
		document.all.productLayer.style.backgroundColor = "#FFFFFF";
	}

	function doShowByProduct()
	{
		currlevel = "product";
		//ctrl.priceBtn.style.backgroundColor = "#FFF8DC";
		ctrl.occasionBtn.style.backgroundColor = "#FFF8DC";
		ctrl.productBtn.style.backgroundColor = "#F0E68C";
		document.all.occasionLayer.style.backgroundColor = "#FFFFFF";
		//document.all.priceLayer.style.backgroundColor = "#FFFFFF";
		document.all.productLayer.style.backgroundColor = "#FFF8DC";
	}


	function doShowByOccasion()
	{
		currlevel = "occasion";
		//ctrl.priceBtn.style.backgroundColor = "#FFF8DC";
		ctrl.occasionBtn.style.backgroundColor = "#F0E68C";
		ctrl.productBtn.style.backgroundColor = "#FFF8DC";
		document.all.occasionLayer.style.backgroundColor = "#FFF8DC";
		//document.all.priceLayer.style.backgroundColor = "#FFFFFF";
		document.all.productLayer.style.backgroundColor = "#FFFFFF";
	}


	function doHandlePricePoint(sender)
	{
		openUpdateWin()
		if(catwinopen == "yes")
		dismisssubcatbox();

		var indexid = sender.indexid;
		document.location = "ProductListServlet?pricepointid=" + indexid + "&amp;sessionId=" + sessionId + "&amp;persistentObjId=" + persistentObjId + "&itemCartNumber=" + itemCartNumber + "&amp;search=advancedproductsearch" + "&companyId=" + companyId;
		//closeUpdateWin();
	}
	
	function openUpdateWin()
	{
		//hideDIVs();
		
		//alert("TEST");

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 -100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";

		//showDIVs();
	}
	

	]]>
	</SCRIPT>

	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onLoad="init();">
	<FORM name="ctrl" method="get" action="">
		<CENTER>
		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
				<TD align="right"> <IMG src="../images/headerFAQ.gif" onclick="javascript:doFAQ()"/> </TD>
			</TR>

			<TR> <TD colspan="6"> <HR> </HR> </TD> </TR>
		</TABLE>



		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD width="65%"> &nbsp; </TD>
				<TD align="right" nowrap="true"><A tabindex="-1" href="javascript:doShoppingCart()" STYLE="text-decoration:none"><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A> </TD>
				<TD align="right" nowrap="true"><A tabindex="-1" href="javascript:doCancelOrder()" STYLE="text-decoration:none">Cancel Order</A> </TD>
			</TR>
			<TR>
				<TD colspan="4"><xsl:call-template name="customerHeader"/> &nbsp; </TD>
			</TR>
			<TR>
				<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
				<TD class="tblheader" colspan="8">
					&nbsp;<a tabindex="-1" href="{$occasionlbl}" class="tblheader">Occasion</a> &nbsp; >Categories
				</TD>
			</TR>
		</TABLE>

		<!-- Main Table -->
		<TABLE width="98%" style="border: 1px inset Black;">
			<TR>
				<TD><FONT size="+1">Categories for: <FONT color="#0000FF"><SPAN id="occasionDescriptionDiv"  style="visibility:"></SPAN></FONT></FONT></TD>
			</TR>
		</TABLE>
<!--
		<BR></BR><BR></BR>
		<font size="+1">Categories for: <font color="#0000FF">Birthday</font></font>
		<BR></BR><BR></BR>
-->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD width="100%">
								<TABLE width="100%" border="0" cellpadding="2" cellspacing="2" align="left">
									<TR>
										<TD colspan="8" class="ScreenPrompt">
											<xsl:value-of select="/root/shoppingData/scripting/script[@fieldName='CATEGORIES']/@scriptText"/>
										</TD>
									</TR>

						<TR>
							<TD class="Instruction">
								<xsl:value-of select="/root/shoppingData/scripting/script[@fieldName='PRODUCTSEARCH']/@instructionText"/>
							</TD>
						</TR>

						<TR>
							<TD valign="top">
								<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
									<TR>
										<TD width="80%">
										<DIV id="main" style="VISIBILITY: visible;">
											<SPAN class="label"> Product Search: </SPAN> &nbsp;&nbsp;

											<SELECT tabindex="1" name="searchSelect">
												<OPTION value="simpleproductsearch">Item Number</OPTION>
												<OPTION value="keywordsearch">Keyword</OPTION>
											</SELECT> &nbsp;&nbsp;

											<INPUT onkeydown="javascript:doSearchEnter()" tabindex="2" type="text" name="searchInput" onclick="" onFocus="javascript:fieldFocus('searchInput')" onblur="javascript:fieldBlur('searchInput')"/> &nbsp;&nbsp;
											<IMG onkeydown="javascript:doSearchEnter()" tabindex="3" name="searchButton" src="../images/button_go.gif" onclick="javascript:doSearch()"/> &nbsp;&nbsp;
											<!--<A tabindex="4" href="javascript:doAdvancedSearch()"> Advanced Search </A>-->
										</DIV>
										</TD>

										<TD width="20%" align="right">

										</TD>
									</TR>
								</TABLE>
							</TD>
						</TR>





									<TR>
										<TD valign="top" height="126" width="20%">
										<!--	STARTS HERE -->
											<TABLE cellpadding="0" cellspacing="0">
												<TR>
													<TD>
														<button tabindex="-1" id="occasionBtn" ftype="occasion" style="border: thin solid #F0E68C; font-family: Verdana; font-style: normal; width: 242px; height: 23; text-align: left; font-weight: normal; background-color: #F0E68C; text-decoration: none;" onMouseOver="javascript:doHighlightCatBtn(this)"><u>Occasion</u></button>
														<button tabindex="-1" id="productBtn" ftype="product" style="border: thin solid #F0E68C; font-family: Verdana; font-style: normal; width: 242px; height: 23; text-align: left; font-weight: normal; background-color: #FFF8DC; text-decoration: none;" onMouseOver="javascript:doHighlightCatBtn(this)"><u>Product</u></button>
														<!--<button tabindex="-1" id="priceBtn" ftype="price" style="border: thin solid #F0E68C; font-family: Verdana; font-style: normal; width: 244px; height: 23; text-align: left; font-weight: normal; background-color: #FFF8DC; text-decoration: none;" onMouseOver="javascript:doHighlightCatBtn(this)"><u>Price</u></button>-->
													<!--
														<button tabindex="-1" id="NOTHING" style="border: none; font-family: Verdana; font-style: normal; width: 280px; height: 22; text-align: center; font-weight: normal; background-color: #F0E68C; text-decoration: none;" ></button>
													-->
													</TD>
												</TR>
											</TABLE>

											<TABLE cellpadding="0" cellspacing="0" style="border: 1px solid Black;">
												<TR>
													<TD>
														<DIV id="occasionLayer" style="VISIBILITY: visible;  absolute; background-color: #FFF8DC; height: 350px; width: 242;">
														<xsl:for-each select="/root/shoppingData/occasion_indexes/index">
															<xsl:variable name="occasionindexid" select="@indexId"/>
															<xsl:variable name="name" select="@name"/>
															<xsl:variable name="hassubindexes" select="@subIndexesExist"/>

															<xsl:if test="$hassubindexes = 'N'">
																<button id="{$name}" ftype="occasion" style=" border:none;font-family: Verdana; font-style: normal; width: 242px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onclick="javascript:doShowCategory(200, 400, this, 'no')" onMouseOver="javascript:doHighlightCatBtn(this)"><U><xsl:value-of select="@name"/></U></button>
																<script>
																	<![CDATA[startSubCategoryHTML("]]> <xsl:value-of select="@name"/>  <![CDATA[");]]>
																	<![CDATA[addSubCategoryHTML("]]> <xsl:value-of select="@name"/>","<xsl:value-of select="@indexId"/><![CDATA[","N");]]>
																	<![CDATA[endSubCategoryHTML();]]>
																</script>
															</xsl:if>

															<xsl:if test="$hassubindexes = 'Y'">
																<button id="{$name}" ftype="occasion" style=" border:none;font-family: Verdana; font-style: normal; width: 242px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onclick="javascript:doShowCategory(200, 400, this, 'yes')" onMouseOver="javascript:doHighlightCatBtn(this)"><xsl:value-of select="@name"/></button>
																<script>
																	<![CDATA[startSubCategoryHTML("]]> <xsl:value-of select="@name"/>  <![CDATA[");]]>
																		<xsl:for-each select="/root/shoppingData/subIndexes/index[@parentIndexId = $occasionindexid]">
																			<![CDATA[addSubCategoryHTML("]]> <xsl:value-of select="@name"/>","<xsl:value-of select="@indexId"/><![CDATA[","Y");]]>
																		</xsl:for-each>
																	<![CDATA[endSubCategoryHTML();]]>
																</script>
															</xsl:if>
															<script>currentButton = occasionLayer.all[0];</script>
														</xsl:for-each>
														</DIV>
													</TD>

													<TD>
														<DIV id="productLayer" style="VISIBILITY: visible;  absolute; background-color: #FFFFFF; height: 350px; width: 242;">
														<xsl:for-each select="/root/shoppingData/product_indexes/index">
															<xsl:variable name="productindexid" select="@indexId"/>
															<xsl:variable name="name" select="@name"/>
															<xsl:variable name="hassubindexes" select="@subIndexesExist"/>

															<xsl:if test="$hassubindexes = 'N'">
																<button id="{$name}" ftype="product" style=" border:none;font-family: Verdana; font-style: normal; width: 242px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onclick="javascript:doShowCategory(200, 400, this, 'no')" onMouseOver="javascript:doHighlightCatBtn(this)"><U><xsl:value-of select="@name"/></U></button>
																<script>
																	<![CDATA[startSubCategoryHTML("]]> <xsl:value-of select="@name"/>  <![CDATA[");]]>
																	<![CDATA[addSubCategoryHTML("]]> <xsl:value-of select="@name"/>","<xsl:value-of select="@indexId"/><![CDATA[","N");]]>
																	<![CDATA[endSubCategoryHTML();]]>
																</script>
															</xsl:if>

															<xsl:if test="$hassubindexes = 'Y'">
																<button id="{$name}" ftype="product" style=" border:none; font-family: Verdana; font-style: normal; width: 242px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onclick="javascript:doShowCategory(200, 400, this, 'yes')" onMouseOver="javascript:doHighlightCatBtn(this)"><xsl:value-of select="@name"/></button>
																<script>
																	<![CDATA[startSubCategoryHTML("]]> <xsl:value-of select="@name"/>  <![CDATA[");]]>
																	<xsl:for-each select="/root/shoppingData/subIndexes/index[@parentIndexId = $productindexid]">
																			<![CDATA[addSubCategoryHTML("]]> <xsl:value-of select="@name"/>","<xsl:value-of select="@indexId"/><![CDATA[","Y");]]>
																	</xsl:for-each>
																	<![CDATA[endSubCategoryHTML();]]>
																</script>
															</xsl:if>
														</xsl:for-each>
														</DIV>
													</TD>

													<TD>
													<!--
														<DIV id="priceLayer" style="VISIBILITY: visible;  background-color: #FFFFFF; height: 350px; width: 242;">
														<xsl:for-each select="/root/shoppingData/pricePoints/pricePoint">
															<xsl:variable name="pricePointId" select="@pricePointsId"/>
															<xsl:variable name="name" select="@pricePointDescription"/>
															<button id="{$name}" ftype="price" indexid="{$pricePointId}" style=" border:none;font-family: Verdana; font-style: normal; width: 242px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onclick="javascript:doHandlePricePoint(this)" onMouseOver="javascript:doHighlightCatBtn(this)"><xsl:value-of select="@pricePointDescription"/></button>
														</xsl:for-each>
													</DIV>
													-->
												</TD>
												<!--
													<TD>
														<DIV id="helpLayer" style="VISIBILITY: visible;  background-color: #FFF8DC; height: 450px; width: 68;"></DIV>
													</TD>
												-->
												</TR>
											</TABLE>

											<DIV id="subcategory" style="VISIBILITY: hidden; POSITION: absolute; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
												<button id="moveBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;" onClick="javascript:moveBtn_onmouseclick(this)"><font color="#FFFFFF">Move Window</font></button>
												<button id="closeBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;"  onClick="javascript:dismisssubcatbox()"><font color="#FFFFFF">Close Window</font></button>

												<TABLE cellspacing="20">
													<TR>
														<TD>
															<DIV id="subcategorydetail"></DIV>
														</TD>
													</TR>
												</TABLE>
											</DIV>
										<!--	END HERE -->
										</TD>
									</TR>
								</TABLE>
							</TD>
						</TR>

						<TR> <TD> &nbsp; </TD> </TR>


						<TR>
							<TD valign="top">
								<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
									<TR>
										<TD width="80%">

										</TD>

										<TD width="20%" align="right">
                    <xsl:if test="/root/pageData/data[@name='custOrderAllowed']/@value ='Y'">
											<IMG tabindex="5" onclick="javascript:doCustumOrder()" src="../images/button_custom_order.gif" alt="Custom Order"/> &nbsp;&nbsp;
                    </xsl:if>
										</TD>
									</TR>
								</TABLE>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<TD class="tblheader" colspan="8">
					<xsl:variable name="occasionlbl1" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					&nbsp;<a tabindex = "-1" href="{$occasionlbl1}" class="tblheader">Occasion</a> &nbsp; >Categories
				</TD>
			</TR>
			<TR> <TD>&nbsp; </TD> </TR>
			<TR>
				<TD class="disclaimer">
					<DIV align="center">COPYRIGHT 2005. FTD INC. ALL RIGHTS RESERVED. </DIV>
				</TD>
			</TR>
		</TABLE>
		</CENTER>
	</FORM>

	<DIV id="dropin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: thin ridge; border-color: Black; background: #FFF8DC;">
		<br></br>
		&nbsp;&nbsp;<strong><font face="Verdana">Advanced Search Form</font></strong>

		<FORM name="advancesearch" method="get" action="SearchServlet">
			<input type="hidden" name="itemCartNumber" value=""/>
			<input type="hidden" name="companyId" value="{$companyId}"/>
			<P align="right">
				<IMG tabindex="15" onkeydown="javascript:dismissboxEnter()" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:dismissbox()"/>&nbsp;&nbsp;&nbsp;&nbsp;
			</P>

			<CENTER>
			<!-- Main Table -->
			<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
				<TR>
					<TD>
						<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
							<TR>
								<TD>
									<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
										<TR>
											<TD class="instruction" colspan="3">
												Use the Search By, Flower Search or Search by Country to find the correct item.
												Click the search button to perform the product search.
											</TD>
										</TR>

										<TR>
											<TD width="50%" align="center" valign="top">
												<TABLE width="100%" height="175" border="1" cellspacing="0" cellpadding="0" bordercolor="#FFCC00">
													<TR>
														<TD>
															<TABLE width="100%" border="0" cellspacing="2" cellpadding="2" height="100%">
																<TR>
																	<TD colspan="2" class="TblHeader" align="center"> Search By </TD>
																</TR>
																<TR>
																	<TD class="labelright"> Occasions:</TD>
																	<TD>
																		<SELECT tabindex="6" name="occasion">
																			<OPTION value="" selected="true">--Select occasion--</OPTION>
																			<xsl:for-each select="/root/shoppingData/occasionList/occasion">
																			<OPTION value="{@occasionId}"> <xsl:value-of select="@description"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
																	<TD class="labelright"> Products:</TD>
																	<TD>
																		<SELECT tabindex="7" name="category">
																			<OPTION value="" selected="true">--Select product--</OPTION>
																			<xsl:for-each select="/root/shoppingData/categoryList/category">
																			<OPTION value="{@productCategoryId}"> <xsl:value-of select="@description"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
																	<TD class="labelright"> Prices:</TD>
																	<TD>
																		<SELECT tabindex="8" name="pricePointId">
																			<OPTION value="" selected="true">--Select price--</OPTION>
																			<xsl:for-each select="/root/shoppingData/pricePoints/pricePoint">
																			<OPTION value="{@pricePointsId}"> <xsl:value-of select="@pricePointDescription"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
		    														<TD class="labelright"> Recipients:</TD>
																	<TD>
																		<SELECT tabindex="9" name="recipient">
																		    <OPTION value="" selected="true">--Select recipient--</OPTION>
																			<xsl:for-each select="/root/shoppingData/recipientList/recipient">
																			<OPTION value="{@receipientSearchId}"> <xsl:value-of select="@description"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
																	<TD></TD>
																	<TD align="right">
																		<IMG onkeydown="doAdvSearchByEnter()" tabindex="10" name="searchflowers" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:doAdvSearchBy()"/>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
											<TD width="50%" align="center" valign="top">
												<TABLE width="100%" height="175" border="1" cellspacing="0" cellpadding="0" bordercolor="#FFCC00">
													<TR>
														<TD valign="top">
															<TABLE width="100%" border="0" cellspacing="2" cellpadding="2" height="100%">
																<TR>
																	<TD colspan="2" class="TblHeader" align="center"> Flower Search </TD>
																</TR>
																<TR>
																	<TD nowrap="true" class="labelright"> Flower Colors:</TD>
																	<TD>
																		<SELECT tabindex="11" name="color">
																			<OPTION value="" selected="true">--Select flower color--</OPTION>
																			<xsl:for-each select="/root/shoppingData/colorList/color">
																			<!--<OPTION value="{@colorMasterId}"> <xsl:value-of select="@description"/> </OPTION>-->
																			<OPTION value="{@description}"> <xsl:value-of select="@description"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
																	<TD nowrap="true" class="labelright"> Flower Names:</TD>
																	<TD>
																		<SELECT tabindex="12" name="name">
																			<OPTION value="" selected="true">--Select flower name--</OPTION>
																			<xsl:for-each select="/root/shoppingData/flowerList/flower">
																			<OPTION value="{@flowerName}"> <xsl:value-of select="@flowerName"/> </OPTION>
																			<!--<OPTION value="{@flowerId}"> <xsl:value-of select="@flowerName"/> </OPTION>-->
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR>
																	<TD class="labelright"> Flower Prices:</TD>
																	<TD>
																		<SELECT tabindex="13" name="price">
																			<OPTION value="" selected="true">--Select flower price--</OPTION>
																			<xsl:for-each select="/root/shoppingData/pricePoints/pricePoint">
																			<OPTION value="{@pricePointsId}"> <xsl:value-of select="@pricePointDescription"/> </OPTION>
																			</xsl:for-each>
																		</SELECT>
																	</TD>
																</TR>
																<TR> <TD> &nbsp; </TD> </TR>
																<TR>
																	<TD></TD>
																	<TD align="right">
																		<IMG onkeydown="doAdvSearchFlowersEnter()" tabindex="14" name="searchflowers" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:doAdvSearchFlowers()"/>
																	</TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</TD>
						                    <TD valign="top" height="175" align="center" width="33%"> </TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			</CENTER>

			<INPUT type="hidden" name="sessionId" value=""/>
			<INPUT type="hidden" name="persistentObjId" value=""/>
			<INPUT type="hidden" name="advSearchType" value=""/>
			<INPUT type="hidden" name="search" value="advancedproductsearch"/>
		</FORM>
	</DIV>

	<DIV id="subcategory" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<p align="left">
			<button id="moveBtn" style="border: none; #808080; font-family: Verdana; font-style: normal; width: 100px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onClick="onclick:moveBtn_onmouseclick(this)"><font color="#FFFFFF">Move Window</font></button>
			<button id="closeBtn" style="border: none; #808080; font-family: Verdana; font-style: normal; width: 100px; height: 22; text-align: left; font-weight: normal; background-color: transparent; text-decoration: none;" width="160" size="65" onClick="javascript:dismissbox()"><font color="#FFFFFF">Close Window</font></button>
		</p>
		&nbsp;&nbsp;
		<strong>
			<font face="Verdana">Subcategory List</font>
		</strong>
	</DIV>
	<!--Begin update DIV -->
	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... searching!</font> </p>
	</DIV>
	<!--End update DIV -->

</BODY>

	<script>
	<![CDATA[
		function doAdvSearchFlowers()
		{
			document.advancesearch.sessionId.value = sessionId;
			document.advancesearch.persistentObjId.value = persistentObjId;
			document.all.advSearchType.value = 'flower';
			document.all.advancesearch.submit();
		}

		function doAdvSearchBy()
		{
			document.advancesearch.sessionId.value = sessionId;
			document.advancesearch.persistentObjId.value = persistentObjId;
			document.all.advSearchType.value = 'searchby';
			document.all.advancesearch.submit();

		}

		function doAdvSearchFlowersEnter()
		{
   		   if (window.event.keyCode == 13)
		   {
		     doAdvSearchFlowers();
		   }
		}

		function doAdvSearchByEnter()
		{
		   if (window.event.keyCode == 13)
		   {
		     doAdvSearchBy();
		   }
		}
	]]>
	</script>
</HTML>
</xsl:template>
</xsl:stylesheet>
