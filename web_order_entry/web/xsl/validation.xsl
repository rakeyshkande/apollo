<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:template name="validation">

<script>
<![CDATA[

	var validationSuccess = false;
	var validationNames = new Array();
    var validationValues = new Array();
    var c_count = 0;
	var IFrameObj;

	function addValidationEntry(key, value)
  	{
  		validationValues[c_count] = escape(value);
		validationNames[c_count] = key;
		c_count ++;
		
		//for(i = 0; i < validationNames.length; i++)
		//{
		//	alert(validationNames[i] + " = " +  validationValues[i]);
		//}
  	}

	function _clearValidationEntries_()
  	{
  		validationNames = new Array();
		validationValues = new Array();
		c_count = 0;
  	}

	function _getQueryString_()
	{
		var url = '?'
	 	url += (validationNames[0] + "=" + validationValues[0]);

	 	for (var i = 1; i < c_count; i++)
	 	{
	 		url += "&";
			url += (validationNames[i] + "=" + validationValues[i]);
	 	}

		return url;
	}

	function _onValidationNotAvailable_()
	{
		if(!validationSuccess)
			onValidationNotAvailable();
	}

	function _notifyValidatonSuccess_()
	{
		validationSuccess = true;
	}


	function callToServer(siteNameSslInput) {

		setTimeout('_onValidationNotAvailable_()', 40000);
		if (!document.createElement) {return true};
  		var IFrameDoc;
      var URL = "ValidationServlet" + _getQueryString_();
		_clearValidationEntries_();
  		if (!IFrameObj && document.createElement) {
   		try
		{
      		var tempIFrame=document.createElement('iframe');
      		tempIFrame.setAttribute('id','RSIFrame');
      		tempIFrame.style.border='0px';
      		tempIFrame.style.width='0px';
      		tempIFrame.style.height='0px';
          // This "dummy" src is needed to prevent the MS IE browser from
          // treating the iframe as a nonsecured resource.
          if (siteNameSslInput != null) {
          tempIFrame.src='https://' + siteNameSslInput + applicationContext + '/dnistest.htm';
          }
      		IFrameObj = document.body.appendChild(tempIFrame);

      	if (document.frames) {
        	IFrameObj = document.frames['RSIFrame'];
      	}
    	} catch(exception) {
      		iframeHTML='\<iframe id="RSIFrame" style="';
      		iframeHTML+='border:0px;';
      		iframeHTML+='width:0px;';
      		iframeHTML+='height:0px;';
      		iframeHTML+='"><\/iframe>';
      		document.body.innerHTML+=iframeHTML;
      		IFrameObj = new Object();
      		IFrameObj.document = new Object();
      		IFrameObj.document.location = new Object();
      		IFrameObj.document.location.iframe = document.getElementById('RSIFrame');
      		IFrameObj.document.location.replace = function(location) {
        	this.iframe.src = location;
      	}
    	}
  	}
  	IFrameDoc = IFrameObj.document;
  	IFrameDoc.location.replace(URL);
  	
  	//IFrameObj.src = URL;
  	
	}
]]>	
</script>

</xsl:template>
</xsl:stylesheet>