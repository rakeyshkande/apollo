<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="validation.xsl"/>
	<xsl:import href="headerButtons.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="customerHeader.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
	<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
	<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
  <xsl:variable name="siteName" select="root/parameters/siteName"/>
  <xsl:variable name="siteNameSsl" select="root/parameters/siteNameSsl"/>
<xsl:variable name="applicationContext" select="root/parameters/applicationContext"/>
	<xsl:template match="/">
		<xsl:variable name="membershipIdLength" select="/root/pageData/data[@name='membershipIdLength']/@value "/>
		<xsl:variable name="membershipIdNOTRequired" select="/root/pageData/data[@name='membershipIdNOTRequired']/@value "/>
		<xsl:variable name="programId" select="/root/pageData/data[@name='partnerId']/@value "/>
		<SCRIPT language="JavaScript" src="../js/FormChek.js"></SCRIPT>
		<SCRIPT language="JavaScript" src="../js/util.js"></SCRIPT>
  	<SCRIPT language="JavaScript">

      var siteName = '<xsl:value-of select="$siteName"/>';
      var siteNameSsl = '<xsl:value-of select="$siteNameSsl"/>';
      var applicationContext = '<xsl:value-of select="$applicationContext"/>';

  		<![CDATA[
      /* This is a hack.  I can't find a way to turn the string infoFormat
        from a string into a regular expression.  If you can fix it, please do*/
      function matchDynamicField(element, rowNum)
      {
        ]]> 
        <xsl:for-each select="/root/billingData/promptList/data">        
          var  billingSeq = "<xsl:value-of select="@billingInfoSequence"/>"; 
          <![CDATA[
            if (billingSeq == rowNum)
            {
                ]]>
              var formatString = <xsl:value-of select="@infoFormat"/>;
                  <![CDATA[
            }
                ]]>
        </xsl:for-each>
          <![CDATA[
        return ((element.value).match(formatString));
      }
    ]]> 
    </SCRIPT>
		<SCRIPT language="JavaScript" src="../js/creditCardPayment.js"></SCRIPT>
		<HTML>
			<HEAD>
				<TITLE> FTD - Billing and Payment </TITLE>
				<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
			</HEAD>
			<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onLoad="init();">
				<FORM name="creditCardPaymentForm" method="post" action="{$actionServlet}">
					<INPUT type="hidden" name="ccApprovalCode" maxlength="2" />
					<INPUT type="hidden" name="ccAVSResult" maxlength="2" />
					<INPUT type="hidden" name="ccApprovalAmount" />
					<INPUT type="hidden" name="ccACQReferenceData" maxlength="30" />
					<INPUT type="hidden" name="ccApprovalVerbiage" maxlength="10" />
					<INPUT type="hidden" name="ccActionCode" maxlength="2" />
					<INPUT type="hidden" name="giftCertificateAmount"/>
          <input type="hidden" name="dnisType" value="{/root/pageHeader/headerDetail[@name='dnisType']/@value}"/>
          <input type="hidden" name="billingZipCode" value = "{/root/customer/@zip}"/>
          <input type="hidden" name="billingAddress1" value = "{/root/customer/@address1}"/>
					<input type="hidden" name="programId" value="{/root/pageData/data[@name='partnerId']/@value}"/>
					<input type="hidden" name="displayCreditCard" value="{/root/pageData/data[@name='displayCreditCard']/@value}"/>
					<input type="hidden" name="sourceCodeId" value="{/root/pageData/data[@name='sourceCodeId']/@value}"/>
          <input type="hidden" name="adjustedPrice" value="{/root/pageData/data[@name='adjustedPrice']/@value}" />
          <input type="hidden" name="forwardToAction" value=""/>
          <CENTER>
						<xsl:call-template name="header"></xsl:call-template>
						<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
							<TR>
								<TD width="65%"> &nbsp; </TD>
								<TD tabindex ="-1" align="right" valign="top">
									<A href="javascript:clearOutValues();doShoppingCartLocal('{$actionServlet}')" STYLE="text-decoration:none" >
										<IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart
									</A>
								</TD>
								<TD tabindex ="-1" align="right" valign="top">
									<A href="javascript:clearOutValues();doCancelOrderLocal()" STYLE="text-decoration:none" >Cancel Order</A>
								</TD>
							</TR>
							<TR>
								<TD colspan="4">
									<xsl:call-template name="customerHeader"/>&nbsp; 
								</TD>
							</TR>
						</TABLE>
						<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
							<TR>
								<TD class="tblheader">
					&nbsp; Billing
								</TD>
							</TR>
						</TABLE>
						<!-- Main Table -->
						<TABLE width="98%" border="0" bordercolor="#006699" cellspacing="1" cellpadding="1">
							<TR>
                <TD>
                  <DIV id="balanceDueDIV" style="display:none">
                    <TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
                      <TR>
                        <TD width="33%"> &nbsp; </TD>
                        <TD width="33%">
                          <TABLE width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#FFCC00">
                            <TR>
                              <TD>
                                <TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
                                  <TR>
                                    <TD valign="top" width="50%" class="labelright"> Balance Due: </TD>
                                    <TD valign="top" width="50%" align="left" style="COLOR:red;FONT-WEIGHT: bold;">
                                      $<SPAN id="balanceDue">0.00</SPAN>
                                    </TD>
                                  </TR>
                                </TABLE>
                              </TD>
                            </TR>
                          </TABLE>
                        </TD>
                        <TD width="33%"> &nbsp; </TD>
                      </TR>
                    </TABLE>
                  </DIV>
                </TD>
              </TR>
              <TR>
								<TD>
									<DIV id="paymentDIV" style="display:block">
										<DIV id="creditCardDIV">
  										<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
                        <xsl:if test="/root/pageData/data[@name='displayCreditCard']/@value ='Y'">
                          <TR>
                            <TD colspan="3" align="left" class="SectionHeader"> Credit Card </TD>
                          </TR>
                          <TR>
                            <TD colspan="3" class="ScreenPrompt">
                              <xsl:value-of select="/root/billingData/scripting/script[@fieldName='CREDITCARD']/@scriptText"/>
                            </TD>
                          </TR>
                          <TR>
                            <TD width="25%" valign="top" class="labelright"> Credit Card Type </TD>
                            <TD width="35%" valign="top" >
                              <xsl:choose>
                                <xsl:when test="/root/pageData/data[@name='paymentMethod']/@value =''">
                                  <SELECT tabindex="1" name="creditCardType" onchange="javascript:clearOutValues(); creditCardType_onChange();">
                                    <option value=""></option>
                                    <xsl:for-each select="/root/billingData/creditCards/creditCard">
                                      <OPTION value="{@paymentMethodId}">
                                        <xsl:if test="/root/billing/@creditType = @paymentMethodId"><xsl:attribute name="selected">true</xsl:attribute></xsl:if>
                                        <xsl:value-of select="@description"/>
                                      </OPTION>
                                    </xsl:for-each>
                                  </SELECT>
                            &nbsp; 
                                  <SPAN style="color:red"> *** </SPAN>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="/root/billingData/creditCards/creditCard[@paymentMethodId = /root/pageData/data[@name='paymentMethod']/@value]/@description"/>
                                  <input type="hidden" name="creditCardType" />
                                </xsl:otherwise>
                              </xsl:choose>
                            </TD>
                            <TD width="40%" class="Instruction" valign="top">
                              <xsl:value-of select="/root/billingData/scripting/script[@fieldName='CREDITTYPE']/@instructionText"/>
                            </TD>
                          </TR>
                          <TR>
                            <TD colspan="3">
                              <DIV ID="creditCardNumberDiv" STYLE="DISPLAY:BLOCK">
                                <TABLE width="100%" border="0">
                                  <TR>
                                    <TD width="25%" valign="top" class="labelright"> Credit Card Number </TD>
                                    <TD width="35%" valign="top" >
                                      <input tabindex="2" type="text" name="creditCardNumber" size="20" maxlength="16" onFocus="javascript:fieldFocus('creditCardNumber')" onblur="javascript:fieldBlur('creditCardNumber')" value="{/root/billing/@creditNumber}"/>
                        &nbsp;
                                      <SPAN style="color:red"> *** </SPAN>
                                    </TD>
                                    <TD width="40%" class="Instruction" valign="top">
                                      <xsl:value-of select="/root/billingData/scripting/script[@fieldName='CREDITNUMBER']/@instructionText"/>
                                    </TD>
                                  </TR>
                                </TABLE>
                              </DIV>
                            </TD>
                          </TR>
                          <TR>
                            <TD COLSPAN="3">
                              <DIV  ID="creditCardExpDateDiv" STYLE="DISPLAY:BLOCK">
                                <TABLE width="100%" border="0">
                                  <TR>
                                    <TD width="25%" valign="top" class="labelright">  Expiration Date </TD>
                                    <TD width="35%" valign="top" >
                                      <SELECT tabindex="3" name="creditCardExpirationMonth" initialValue = "{/root/billing/@creditExpMonth}">
                                        <option value=""> &nbsp; </option>
                                        <option value="01">01-Jan</option>
                                        <option value="02">02-Feb</option>
                                        <option value="03">03-Mar</option>
                                        <option value="04">04-Apr</option>
                                        <option value="05">05-May</option>
                                        <option value="06">06-Jun</option>
                                        <option value="07">07-Jul</option>
                                        <option value="08">08-Aug</option>
                                        <option value="09">09-Sep</option>
                                        <option value="10">10-Oct</option>
                                        <option value="11">11-Nov</option>
                                        <option value="12">12-Dec</option>
                                      </SELECT>
                        &nbsp;/&nbsp;
                                      <xsl:variable name="currentYear" select="/root/billing/@currentYear"/>
                                      <SELECT tabindex="4" name="creditCardExpirationYear" initialValue = "{/root/billing/@creditExpYear}" currentYear = "{/root/billing/@currentYear}">
                          &nbsp;
                                        <SPAN style="color:red"> *** </SPAN>
                                        <option value=""> &nbsp; </option>
                                        <option value="{number($currentYear)}">
                                          <xsl:value-of select="number($currentYear)"/>
                                        </option>
                                        <option value="{number($currentYear)+1}">
                                          <xsl:value-of select="number($currentYear)+1"/>
                                        </option>
                                        <option value="{number($currentYear)+2}">
                                          <xsl:value-of select="number($currentYear)+2"/>
                                        </option>
                                        <option value="{number($currentYear)+3}">
                                          <xsl:value-of select="number($currentYear)+3"/>
                                        </option>
                                        <option value="{number($currentYear)+4}">
                                          <xsl:value-of select="number($currentYear)+4"/>
                                        </option>
                                        <option value="{number($currentYear)+5}">
                                          <xsl:value-of select="number($currentYear)+5"/>
                                        </option>
                                        <option value="{number($currentYear)+6}">
                                          <xsl:value-of select="number($currentYear)+6"/>
                                        </option>
                                        <option value="{number($currentYear)+7}">
                                          <xsl:value-of select="number($currentYear)+7"/>
                                        </option>
                                        <option value="{number($currentYear)+8}">
                                          <xsl:value-of select="number($currentYear)+8"/>
                                        </option>
                                        <option value="{number($currentYear)+9}">
                                          <xsl:value-of select="number($currentYear)+9"/>
                                        </option>
                                      </SELECT>
                                      <SPAN style="color:red"> *** </SPAN>
                                    </TD>
                                    <TD width="40%" class="Instruction" valign="top">
                                      <xsl:value-of select="/root/billingData/scripting/script[@fieldName='CREDITEXPIRATION']/@instructionText"/>
                                    </TD>
                                  </TR>
                                </TABLE>
                              </DIV>
                            </TD>
                          </TR>
                        </xsl:if>
												<xsl:if test="/root/billingData/promptList/data/@billingInfoPrompt">
													<TR>
														<TD colspan="3" class="SectionHeader">
															<xsl:choose>
																<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'EXTRA_INFO_TITLE']">
																	<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='EXTRA_INFO_TITLE']/@scriptText"/>
																</xsl:when>
																<xsl:otherwise>Payment</xsl:otherwise>
															</xsl:choose>																				
											&nbsp;Information 
														</TD>
													</TR>
													<xsl:choose>
														<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'CORP_DISCOUNT']">
															<tr>
																<TD colspan="3" class="ScreenPrompt">
																	<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='CORP_DISCOUNT']/@scriptText"/>
																</TD>
															</tr>
														</xsl:when>
														<xsl:otherwise>
															<xsl:choose>
																<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'MKT_DAYS']">
																	<tr>
																		<TD colspan="3" class="ScreenPrompt">
																			<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='MKT_DAYS']/@scriptText"/>
																		</TD>
																	</tr>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:choose>
																		<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'IBO_NUMBER']">
																			<tr>
																				<TD colspan="3" class="ScreenPrompt">
																					<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='IBO_NUMBER']/@scriptText"/>
																				</TD>
																			</tr>
																		</xsl:when>
																		<xsl:otherwise>
																			<TR>
																				<TD colspan="3" class="ScreenPrompt">
																					<xsl:value-of select="/root/billingData/scripting/script[@fieldName='INVOICE']/@scriptText"/>
																				</TD>
																			</TR>
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:otherwise>
													</xsl:choose>
													<xsl:for-each select="/root/billingData/promptList/data">

														<TR id="promptListRow" billingInfoPrompt="{@billingInfoPrompt}">
															<TD valign="top" class="labelright">
																<xsl:value-of select="@billingInfoDisplay"/>
															</TD>

															<TD valign="top" >
																<xsl:variable name="billingInfoPromptVar" select="@billingInfoPrompt"/>
																<xsl:variable name="promptNumber" select="position()-1"/>
																<xsl:variable name="promptName" select="concat('promptName',$promptNumber)"/>
																<xsl:variable name="promptValue" select="concat('promptValue',$promptNumber)"/>
                                <xsl:variable name="promptValueView" select="concat('promptValueView',$promptNumber)"/>
																<xsl:choose>
																	<xsl:when	test="@promptType = '' or @promptType = 'TEXTBOX'">
																		<input type="hidden" name="{$promptName}" value="{@billingInfoPrompt}"/>
																		<input tabindex="5" type="text" name="{$promptValue}"  infoFormat = "{@infoFormat}" size="16" maxlength="30">
																			<xsl:attribute name="onfocus">javascript:fieldFocus('<xsl:value-of select="$promptValue"/>')</xsl:attribute>
																			<xsl:attribute name="onblur">javascript:fieldBlur('<xsl:value-of select="$promptValue"/>')</xsl:attribute>
																		</input>
																	</xsl:when>
																	<xsl:when	test="@promptType = 'DROPDOWN'">
																		<input type="hidden" name="{$promptName}" value="{@billingInfoPrompt}"/>
																		<select tabindex="5" name="{$promptValue}"  infoFormat = "{@infoFormat}">
																			<xsl:for-each select="/root/billingData/promptListOptions/data[@billingInfoDesc = $billingInfoPromptVar]">
																				<option value="{@optionValue}">
																					<xsl:value-of select="@optionDisplay"/>
																				</option>
																			</xsl:for-each>
																		</select>
																	</xsl:when>
																	<xsl:when	test="@promptType = 'STATIC'">
																		<xsl:variable name="hiddenValue" select="/root/billingData/promptListOptions/data[@billingInfoDesc = $billingInfoPromptVar]/@optionValue"/>
																		<input type="hidden" name="{$promptName}" value="{@billingInfoPrompt}"/>
																		<input type="hidden" name="{$promptValue}" value="{$hiddenValue}"/>
																		<input tabindex="5" type="text" name="{$promptValueView}" infoFormat = "{@infoFormat}" size="16" maxlength="30" value="{$hiddenValue}" disabled="true" />
																	</xsl:when>
        												</xsl:choose>
															</TD>
															<TD class="Instruction" valign="top">
																<xsl:value-of select="/root/billingData/scripting/script[@fieldName='INVOICENUMBER']/@instructionText"/>
															</TD>
														</TR>
													</xsl:for-each>
												</xsl:if>
												<TR>
													<TD colspan="3" class="ScreenPrompt">
										The total amount charged to your card will be...
													</TD>
												</TR>
												<TR>
													<TD valign="top" class="labelright"> Charge Amount: </TD>
													<TD valign="top" colspan="2">
														<INPUT name="totalAmount" disabled="true" size="8" value="{/root/pageData/data[@name='adjustedPrice']/@value}"></INPUT>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<DIV id="authDIV" style="display:none">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
												<TR>
													<TD colspan="3">
														<HR/>
													</TD>
												</TR>
												<TR>
													<TD valign="top" width="25%" class="labelright"> Verbal Authorization:</TD>
													<TD valign="top">
														<INPUT tabindex="5" name="verbalAuthorization" size="6" maxlength="6" value="" onFocus="javascript:fieldFocus('verbalAuthorization')" onblur="javascript:fieldBlur('verbalAuthorization')"></INPUT>
										&nbsp; 
														<SPAN style="color:red"> *** </SPAN>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='VERBALAUTH']/@instructionText"/>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<DIV id="noChargeDIV" style="display:none">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
												<TR>
													<TD colspan="3">
														<HR/>
													</TD>
												</TR>
												<TR>
													<TD width="25%" valign="top" class="labelright"> Approval ID </TD>
													<TD width="35%" valign="top" >
														<input tabindex="6" type="text" name="noChargeMgrId" size="10" maxlength="30" onFocus="javascript:fieldFocus('noChargeMgrId')" onblur="javascript:fieldBlur('noChargeMgrId')"/>
										&nbsp; 
														<SPAN style="color:red"> *** </SPAN>
													</TD>
													<TD width="40%" class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='NOCHARGEMGR']/@instructionText"/>
													</TD>
												</TR>
												<TR>
													<TD valign="top" class="labelright"> Approval Password </TD>
													<TD valign="top" >
														<input tabindex="7" type="password" name="noChargePassword" size="10" maxlength="10" onFocus="javascript:fieldFocus('noChargePassword')" onblur="javascript:fieldBlur('noChargePassword')"/>
										&nbsp; 
														<SPAN style="color:red"> *** </SPAN>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='NOCHARGEPSWD']/@instructionText"/>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
											<TR>
												<TD colspan="3" align="right">
													<img tabindex="8" onkeydown="javascript:EnterToCancelPayment()" name="backButton" src="../images/button_back.gif" width="74" height="17" onclick="javascript:clearOutValues();cancelPayment('{$actionServlet}')"/>&nbsp;&nbsp;
													<img tabindex="9" onkeydown="javascript:EnterToApplyValidation()" name="placeOrderButton" src="../images/button_placeorder.gif" width="74" height="17" onclick="javascript:applyValidation()" onDblClick="return false;"/>&nbsp;&nbsp;
												</TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD class="tblheader">
          	   		&nbsp; Billing
								</TD>
							</TR>
							<xsl:call-template name="footer"/>
						</TABLE>
						<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
						<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
					</CENTER>
					<!--Begin JCP Popup DIV -->
					<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
						<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
							<TR>
								<TD>
									<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
										<TR>
											<TD align="center">
							At this time, JC Penney accepts orders going to or coming from the 50 United States
							only.  We can process your Order using any major credit card through FTD.com.
												<BR></BR>
												<BR></BR>
							Would you like to proceed?
											</TD>
										</TR>
										<TR>
											<TD> &nbsp; </TD>
										</TR>
										<TR>
											<TD align="center">
												<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/> &nbsp;&nbsp;&nbsp;&nbsp;
												<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</DIV>
					<!--End JCPPop DIV -->
				</FORM>
				<!--
	SERVER SIDE VALIDATION SECTION
-->
				<xsl:call-template name="validation"/>
				<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
					<br/>
					<br/>
					<p align="center">
						<font face="Verdana">Please wait ... validating input!</font>
					</p>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>