

<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="validation.xsl"/>
	<xsl:import href="headerButtons.xsl"/>
	<xsl:import href="footer.xsl"/>
	<xsl:import href="customerHeader.xsl"/>
	<xsl:output method="html" indent="no"/>
	<xsl:output indent="yes"/>
	<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
	<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
	<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
  <xsl:variable name="siteName" select="root/parameters/siteName"/>
  <xsl:variable name="siteNameSsl" select="root/parameters/siteNameSsl"/>
	<xsl:variable name="applicationContext" select="root/parameters/applicationContext"/>
	<xsl:template match="/">
  	<xsl:variable name="membershipIdLength" select="/root/pageData/data[@name='membershipIdLength']/@value "/>
		<xsl:variable name="membershipIdNOTRequired" select="/root/pageData/data[@name='membershipIdNOTRequired']/@value "/>
		<xsl:variable name="programId" select="/root/pageData/data[@name='partnerId']/@value "/>
		<SCRIPT language="JavaScript" src="../js/FormChek.js"></SCRIPT>
		<SCRIPT language="JavaScript" src="../js/util.js"></SCRIPT>
		<SCRIPT language="JavaScript" src="../js/billingPayment.js"></SCRIPT>
    <SCRIPT type="text/javascript" language="JavaScript">
      var siteName = '<xsl:value-of select="$siteName"/>';
      var siteNameSsl = '<xsl:value-of select="$siteNameSsl"/>';
      var applicationContext = '<xsl:value-of select="$applicationContext"/>';
    </SCRIPT>
		<HTML>
			<HEAD>
				<TITLE> FTD - Billing and Payment </TITLE>
				<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
			</HEAD>
			<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
				<FORM name="billingPaymentForm" method="post" action="https://{$siteNameSsl}{applicationContext}{$actionServlet}">
          <input type="hidden" name="giftCertificateRequired" value="{/root/billingData/giftCertificate/data/@promptType = 'REQUIRED'}"/>
          <input type="hidden" name="displayMembershipId" value ="{/root/pageData/data[@name='displayMembershipId']/@value}"/>
 					<input type="hidden" name="displayCreditCard" value="{/root/pageData/data[@name='displayCreditCard']/@value}"/>
          <input type="hidden" name="dnisType" value="{/root/pageHeader/headerDetail[@name='dnisType']/@value}"/>
          <input type="hidden" name="membershipIdNOTRequired" value="{/root/pageData/data[@name='membershipIdNOTRequired']/@value}"/>
          <input type="hidden" name="programId" value="{/root/pageData/data[@name='partnerId']/@value}"/>
          <input type="hidden" name="orderTotalPrice" value="{/root/pageData/data[@name='orderTotalPrice']/@value}"/>
          <input type="hidden" name="companyId" value="{/root/pageData/data[@name='companyId']/@value}"/>
					<INPUT type="hidden" name="ccApprovalCode" maxlength="2" />
					<INPUT type="hidden" name="ccAVSResult" maxlength="2" />
					<INPUT type="hidden" name="ccApprovalAmount" />
					<INPUT type="hidden" name="ccACQReferenceData" maxlength="30" />
					<INPUT type="hidden" name="ccApprovalVerbiage" maxlength="10" />
					<INPUT type="hidden" name="ccActionCode" maxlength="2" />
					<INPUT type="hidden" name="giftCertificateAmount"/>
          <INPUT type="hidden" name="forwardToAction" value=""/>
					<CENTER>
						<xsl:call-template name="header"></xsl:call-template>
						<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
							<TR>
								<TD width="65%"> &nbsp; </TD>
								<TD tabindex ="-1" align="right" valign="top">
									<A href="javascript:doShoppingCartLocal('{$actionServlet}')" STYLE="text-decoration:none" >
										<IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart
									</A>
								</TD>
								<TD tabindex ="-1" align="right" valign="top">
									<A href="javascript:doCancelOrderLocal()" STYLE="text-decoration:none" >Cancel Order</A>
								</TD>
							</TR>
							<TR>
								<TD colspan="4">
									<xsl:call-template name="customerHeader"/>&nbsp; 
								</TD>
							</TR>
						</TABLE>
						<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
							<TR>
								<TD class="tblheader">
					&nbsp; Billing
								</TD>
							</TR>
						</TABLE>
						<!-- Main Table -->
            <xsl:variable name="customerCountry" select="/root/customer/@country"/>
						<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
            	<TR>
								<TD>
									<DIV id="billingInfoDIV" >
										<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:choose>
														<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'BILLINGFIRSTNAME']">
															<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='BILLINGFIRSTNAME']/@scriptText"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGFIRSTNAME']/@scriptText"/>
														</xsl:otherwise>
													</xsl:choose>
												</TD>
											</TR>
											<TR>
												<TD valign="top" width="25%" class="labelright"> Billing first name </TD>
												<TD valign="top" width="35%">
													<INPUT tabindex ="1" type="text" name="billingFirstName" size="20" maxlength="20" onchange="javascript:customerChange()" onFocus="javascript:fieldFocus('billingFirstName')" onblur="javascript:fieldBlur('billingFirstName')" value="{/root/customer/@firstName}"/>&nbsp;&nbsp;&nbsp;
									&nbsp; 
													<SPAN style="color:red"> *** </SPAN>
													<a tabindex ="2" href="javascript: openCustomerLookup()"  >  Lookup customer</a>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGFIRSTNAME']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright"> Billing last name </TD>
												<TD valign="top" >
													<input tabindex ="6" type="text" name="billingLastName" size="30" maxlength="30" onchange="javascript:customerChange()" onFocus="javascript:fieldFocus('billingLastName')" onblur="javascript:fieldBlur('billingLastName')" value="{/root/customer/@lastName}"/>
									&nbsp; 
													<SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD valign="top" class="Instruction" >
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGLASTNAME']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright">Business name</TD>
												<TD valign="top" >
													<input tabindex ="7" type="text" name="businessName" size="20" maxlength="20" onFocus="javascript:fieldFocus('businessName')" onblur="javascript:fieldBlur('businessName')" value="{/root/customer/@business}"/>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BUSINESS']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:choose>
														<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'BILLINGADDRESS1']">
															<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='BILLINGADDRESS1']/@scriptText"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGADDRESS1']/@scriptText"/>
														</xsl:otherwise>
													</xsl:choose>
												</TD>
											</TR>
											<TR>
												<TD class="labelright" valign="top"> Street Address </TD>
												<TD valign="top" >
													<input tabindex ="8" type="text" name="billingAddress1" size="30" maxlength="30" onFocus="javascript:fieldFocus('billingAddress1')" onblur="javascript:fieldBlur('billingAddress1')" value="{/root/customer/@address1}"/>
									&nbsp; 
													<SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGADDRESS1']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD> &nbsp;&nbsp;
												</TD>
												<TD valign="top" >
													<input tabindex ="9" type="text" name="billingAddress2" size="30" maxlength="30" onFocus="javascript:fieldFocus('billingAddress2')" onblur="javascript:fieldBlur('billingAddress2')" value="{/root/customer/@address2}"/>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='BILLINGADDRESS2']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='ZIPCODE']/@scriptText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright"> Country </TD>
												<TD valign="top" >
													<SELECT tabindex ="10" name="billingCountry" onchange="javascript:processCountryChange()" >
														<xsl:for-each select="/root/billingData/countries/country">
															<OPTION value="{@countryId}" countryType ="{@oeCountryType}">
                                <xsl:if test="$customerCountry = @countryId">
																		<xsl:attribute name="selected">true</xsl:attribute>
																</xsl:if>
																<xsl:value-of select="@name"/>
															</OPTION>
														</xsl:for-each>
													</SELECT>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='COUNTRY']/@instructionText"/>
												</TD>
											</TR>
											<xsl:if test="/root/customer/@countryType != 'I'">
												<TR>
													<TD valign="top" class="labelright">Zip/Postal Code </TD>
													<TD valign="top" >
														<input tabindex ="11" type="text" name="billingZipCode" size="10" maxlength="10" onFocus="javascript:fieldFocus('billingZipCode')" onblur="javascript:fieldBlur('billingZipCode')" value="{/root/customer/@zip}"/> &nbsp;&nbsp;&nbsp;
										&nbsp; 
														<SPAN style="color:red"> *** </SPAN>
														<a tabindex ="12" href="javascript: openCityLookup()">  Lookup by City</a>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='ZIPCODE']/@instructionText"/>
													</TD>
												</TR>
											</xsl:if>
											<TR>
												<TD valign="top" class="labelright"> City </TD>
												<TD valign="top" >
													<input tabindex ="18" type="text" name="billingCity" size="30" maxlength="30" onFocus="javascript:fieldFocus('billingCity')" onblur="javascript:fieldBlur('billingCity')" value="{/root/customer/@city}"/>
									&nbsp; 
													<SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='CITY']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright">State/Province </TD>
												<TD valign="top">
                          <div id="hiddenSelectDiv" style="display:none">
                          	<SELECT id="hiddenStateSelect" name="hiddenStateSelect"> 
															<xsl:for-each select="/root/billingData/states/state">
															  <OPTION value="{@stateMasterId}" countryCode="{@countryCode}">
																	<xsl:value-of select="@stateName"/>
																</OPTION>
															</xsl:for-each>
														</SELECT>
                          </div>
                          <div id="stateSelect" style="display:block">
														<SELECT tabindex ="19" name="billingStateSelect" customerState="{/root/customer/@state}">
															<xsl:for-each select="/root/billingData/states/state">
															  <OPTION value="{@stateMasterId}" countryCode="{@countryCode}">
																	<xsl:value-of select="@stateName"/>
																</OPTION>
															</xsl:for-each>
                            </SELECT>
														<SPAN style="color:red"> *** </SPAN>
                          </div>
													<DIV id="stateText">
                            <xsl:if test="/root/customer/@countryType != 'I'"> 
                              <xsl:attribute name="style">display:none</xsl:attribute>
                            </xsl:if>
														<input tabindex ="19" type="text" name="billingStateText" size="30" maxlength="30" onchange="refreshPage('ZipChange')" onFocus="javascript:fieldFocus('billingStateText')" onblur="javascript:fieldBlur('billingStateText')" value="{/root/customer/@state}"/>
													</DIV>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='STATE']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='EMAIL']/@scriptText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright"> E-mail </TD>
												<TD valign="top" >
													<INPUT tabindex ="20" type="text" name="eMail" size="40" maxlength="40" onchange="checkSpecialPromotion()" onFocus="javascript:fieldFocus('eMail')" onblur="javascript:fieldBlur('eMail')" value="{/root/customer/@email}"/> &nbsp;&nbsp;
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='EMAIL']/@instructionText"/>
												</TD>
											</TR>
										</TABLE>
										<DIV id="specialPromotionsDIV">
                      <xsl:if test="/root/pageData/data[@name='displaySpecialPromotionCheckbox']/@value !='Y'">
                        <xsl:attribute name="style">display:none</xsl:attribute>
                      </xsl:if>
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
												<TR>
													<TD colspan="3" class="ScreenPrompt">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='SPECIALPROMOTIONS']/@scriptText"/>
													</TD>
												</TR>
												<TR>
													<TD valign="top" width="25%" class="labelright"> Special promotions</TD>
													<TD valign="top" width="35%" >
														<INPUT tabindex ="21" type="checkbox" name="specialPromotions" onclick="checkSpecialPromotion()" value="Y"/>
													</TD>
													<TD width="40%" class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='SPECIALPROMOTIONS']/@instructionText"/>
													</TD>
												</TR>
											</TABLE>
										</DIV>
										<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='PHONENUMBER']/@scriptText"/>
												</TD>
											</TR>
											<TR>
												<TD width="25%" valign="top" class="labelright"> Home Phone </TD>
												<TD width="35%" valign="top" >
													<input tabindex ="22" type="text" name="homePhone" size="20" maxlength="20" onchange="javascript:customerChange()" onFocus="javascript:fieldFocus('homePhone')" onblur="javascript:fieldBlur('homePhone')" value="{/root/customer/@homePhone}"/>
									&nbsp; 
													<SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='PHONENUMBER']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='ALTPHONE']/@scriptText"/>
												</TD>
											</TR>
											<TR>
												<TD valign="top" class="labelright"> Alternate Phone </TD>
												<TD valign="top" >
													<INPUT tabindex ="23" type="text" name="altPhone" size="20" maxlength="20" onFocus="javascript:fieldFocus('altPhone')" onblur="javascript:fieldBlur('altPhone')" value="{/root/customer/@workPhone}"/> &nbsp;&nbsp;
													<SPAN class="labelright"> Ext. </SPAN>
													<INPUT tabindex ="24" type="text" name="extension" maxlength="10" size="10" onFocus="javascript:fieldFocus('extension')" onblur="javascript:fieldBlur('extension')" value="{/root/customer/@phoneExt}"/>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='ALTPHONE']/@instructionText"/>
												</TD>
											</TR>
											<TR>
												<TD class="labelright" valign="top"> Contact Information </TD>
												<TD valign="top" >
													<input type="text"  maxlength="20" size="21" tabindex ="25" name="contactInformation" onFocus="javascript:fieldFocus('contactInformation')" onblur="javascript:fieldBlur('contactInformation')" value="{/root/customer/@contactInfo}"/>
												</TD>
												<TD class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='CONTACTINFO']/@instructionText"/>
												</TD>
											</TR>
											<xsl:if test="/root/pageData/data[@name='displayMembershipId']/@value ='Y'">
												<TR>
													<TD colspan="3" class="ScreenPrompt">
														<xsl:choose>
															<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'EBAY_USER_ID']">
																<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='EBAY_USER_ID']/@scriptText"/>
															</xsl:when>
															<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'MEMBERSHIPID']">
																<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='MEMBERSHIPID']/@scriptText"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="/root/billingData/scripting/script[@fieldName='MEMBERSHIPID']/@scriptText"/>
															</xsl:otherwise>
														</xsl:choose>
													</TD>
												</TR>
												<xsl:if test="/root/pageData/data[@name='partnerId']/@value !='AAA'">
													<xsl:choose>
														<xsl:when test="$membershipIdNOTRequired = 'Y' "></xsl:when>
														<xsl:otherwise>
															<TR>
																<TD class="labelright"> Skip Member Information </TD>
																<TD valign="top" colspan="2">
																	<TABLE width="100%" border="0">
																		<TR>
																			<TD width="5%" valign="top">
																				<INPUT tabindex="26" type="checkbox" name="skipMemberInfo"/>
																			</TD>
																			<TD width="95%" valign="top">
																				<SPAN class="instruction">
															Select if the customer cannot provide their membership information at this time. 
																					<br/>
															Remind them to contact Customer Service once they have their member information.
																				</SPAN>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD class="Instruction" valign="top"> &nbsp; </TD>
															</TR>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:if>
												<TR>
													<TD valign="top" class="labelright"> Member ID </TD>
													<TD valign="top" >
														<INPUT tabindex ="27" type="text" name="memberId" onkeydown="" value="{/root/customer/@memberId}" onFocus="javascript:fieldFocus('memberId')" onblur="javascript:fieldBlur('memberId')" maxlength="{/root/pageData/data[@name='membershipIdLength']/@value}" />
														<xsl:choose>
															<xsl:when test="$membershipIdNOTRequired = 'Y' "></xsl:when>
															<xsl:otherwise>
                        									  &nbsp; 
																<SPAN style="color:red"> *** </SPAN>
															</xsl:otherwise>
														</xsl:choose>
														<xsl:if test="/root/billingData/scriptOverride2/script2[@fieldName = 'SIX_DIGIT_ONLY']">
											&nbsp; &nbsp; &nbsp; 
															<SPAN class="Instruction">
																<xsl:value-of select="/root/billingData/scriptOverride2/script2[@fieldName='SIX_DIGIT_ONLY']/@instructionText"/>
															</SPAN>
														</xsl:if>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='MEMBERSHIPID']/@instructionText"/>
													</TD>
												</TR>
												<TR>
													<TD valign="top" class="labelright"> First Name </TD>
													<TD valign="top" >
														<INPUT tabindex ="28" type="text" name="memberFirstName" value="{/root/customer/@memberFirstName}" onFocus="javascript:fieldFocus('memberFirstName')" onblur="javascript:fieldBlur('memberFirstName')" maxlength="20"/>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='MEMBERFIRSTNAME']/@instructionText"/>
													</TD>
												</TR>
												<TR>
													<TD valign="top" class="labelright"> Last Name </TD>
													<TD valign="top" >
														<INPUT tabindex ="29" type="text" name="memberLastName" value="{/root/customer/@memberLastName}" onFocus="javascript:fieldFocus('memberLastName')" onblur="javascript:fieldBlur('memberLastName')" maxlength="40"/>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='MEMBERLASTNAME']/@instructionText"/>
													</TD>
												</TR>
											</xsl:if>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='PAYMENT']/@scriptText"/>
												</TD>
											</TR>
											<TR>
												<TD colspan="3" class="Instruction" valign="top">
													<xsl:value-of select="/root/billingData/scripting/script[@fieldName='PAYMENT']/@instructionText"/>
												</TD>
											</TR>
											<xsl:if test="/root/billingData/giftCertificate/data/@billingInfoPrompt">
												<TR>
													<TD colspan="3" align="left" class="SectionHeader" >
														<xsl:choose>
															<xsl:when test="root/billingData/scriptOverride/script[@fieldName='GIFTC']">
																<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='GIFTC']/@scriptText"/> &nbsp;
															</xsl:when>
															<xsl:otherwise>
											Gift Certificate																     </xsl:otherwise>
														</xsl:choose>
													</TD>
												</TR>
												<TR>
													<TD colspan="3" class="ScreenPrompt">
														<xsl:choose>
															<xsl:when test="/root/billingData/scriptOverride/script[@fieldName = 'GIFTCERTIFICATE']">
																<xsl:value-of select="/root/billingData/scriptOverride/script[@fieldName='GIFTCERTIFICATE']/@scriptText"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="/root/billingData/scripting/script[@fieldName='GIFTCERTIFICATE']/@scriptText"/>
															</xsl:otherwise>
														</xsl:choose>
													</TD>
												</TR>
												<TR>
													<TD valign="top" class="labelright"> ID Number </TD>
													<TD valign="top" >
														<input tabindex="30" type="text" name="giftCertId" size="16" maxlength="16" onkeydown="" onFocus="javascript:fieldFocus('giftCertId')" onblur="javascript:fieldBlur('giftCertId')"/>&nbsp; &nbsp;
														<xsl:if test="/root/billingData/giftCertificate/data/@promptType = 'REQUIRED'">
															<SPAN style="color:red"> *** </SPAN>
														</xsl:if>
													</TD>
													<TD class="Instruction" valign="top">
														<xsl:value-of select="/root/billingData/scripting/script[@fieldName='GIFTCERTIFICATE']/@instructionText"/>
													</TD>
												</TR>
											</xsl:if>
											<TR>
												<TD valign="top" class="labelright"> &nbsp; </TD>
												<TD colspan="3" align="right">
													<DIV id="validateButtonDIV">
														<img tabindex="31" name="validateButton" src="../images/button_continue.gif" width="74" height="17" onkeyDown="javascript:goApplyValidation()" onclick="javascript:applyValidation();"/>&nbsp;&nbsp;
													</DIV>
												</TD>
											</TR>
										</TABLE>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD class="tblheader">
          	   		&nbsp; Billing
								</TD>
							</TR>
							<xsl:call-template name="footer"/>
						</TABLE>
						<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
						<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
					</CENTER>
					<!-- Begin Customer Lookup DIV-->
					<DIV id="customerLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
						<CENTER>
							<DIV id="customerLookupSearching" style="VISIBILITY: hidden;">
								<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
							</DIV>
						</CENTER>
						<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
							<TR>
								<TD class="label" colspan="3" align="left">
									<font face="Verdana">
										<strong>Lookup Customer</strong>
									</font>
								</TD>
							</TR>
							<TR>
								<TD nowrap="true" colspan="3" align="left">
									<SPAN class="label">
										<font face="Verdana">Phone Number:</font>
									</SPAN> &nbsp;&nbsp;
									<INPUT onkeydown="javascript:EnterToCustomer()" tabindex ="3" type="text" name="searchphone" size="30" maxlength="50" onFocus="javascript:fieldFocus('searchphone')" onblur="javascript:fieldBlur('searchphone')"/> &nbsp;&nbsp;
								</TD>
							</TR>
							<TR>
								<TD colspan="3" align="right">
									<IMG onkeydown="javascript:EnterToCustomer()" tabindex ="4" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchCustomer(); return false;"/>
									<IMG onkeydown="javascript:EnterToCustomerCancel()" tabindex ="4" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSearch(); return false;"/>
								</TD>
							</TR>
							<TR>
								<TD colspan="3"> &nbsp; </TD>
							</TR>
						</TABLE>
					</DIV>
					<!-- End Customer Lookup DIV-->
					<!-- Begin City Lookup DIV-->
					<DIV id="cityLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
						<CENTER>
							<DIV id="cityLookupSearching" style="VISIBILITY: hidden;">
								<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
							</DIV>
						</CENTER>
						<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
							<TR>
								<TD nowrap="true" colspan="3" align="right">
									<font face="Verdana">
										<strong>City Lookup </strong>
									</font>
								</TD>
								<TD> &nbsp; </TD>
							</TR>
							<TR>
								<TD align="right" nowrap="true" colspan="3" >
									<SPAN class="label">
										<font face="Verdana">City:</font>
									</SPAN>
								</TD>
								<TD>
									<INPUT onkeydown="javascript:EnterToZip()" tabindex ="12" type="text" name="cityInput" size="30" maxlength="50" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"/>
								</TD>
							</TR>
							<TR>
								<TD align="right" nowrap="true" colspan="3" >
									<SPAN class="label">
										<font face="Verdana">State:</font>
									</SPAN>
								</TD>
								<TD>
									<SELECT onkeydown="javascript:EnterToZip()" tabindex ="13" name="stateInput">
										<OPTION value="">55555</OPTION>
										<xsl:for-each select="/root/billingData/states/state">
											<OPTION value="{@stateMasterId}">
												<xsl:value-of select="@stateName"/>
											</OPTION>
										</xsl:for-each>
									</SELECT>
								</TD>
							</TR>
							<TR>
								<TD align="right" nowrap="true" colspan="3" >
									<SPAN class="label">
										<font face="Verdana">Zip/Postal Code:</font>
									</SPAN>
								</TD>
								<TD>
									<INPUT onkeydown="javascript:EnterToZip()" tabindex ="14" type="text" name="zipCodeInput" size="10" maxlength="10" onFocus="javascript:fieldFocus('zipCodeInput')" onblur="javascript:fieldBlur('zipCodeInput')"/>
								</TD>
							</TR>
							<TR>
								<TD> &nbsp; </TD>
								<TD colspan="3" align="right">
									<INPUT onkeydown="javascript:EnterToZip()" tabindex ="15" type="image" src="../images/button_search.gif" alt="Search" onclick="javascript:goSearchCity(); return false;"  />
									<INPUT tabindex ="16" type="image" src="../images/button_close.gif" alt="Close screen" onclick="javascript:goCancelCity(); return false;" />
								</TD>
							</TR>
							<TR>
								<TD colspan="3"> &nbsp; </TD>
							</TR>
						</TABLE>
					</DIV>
					<!-- End City Lookup DIV-->
					<!--Begin JCP Popup DIV -->
					<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
						<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
							<TR>
								<TD>
									<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
										<TR>
											<TD align="center">
							At this time, JC Penney accepts orders going to or coming from the 50 United States
							only.  We can process your Order using any major credit card through FTD.com.
												<BR></BR>
												<BR></BR>
							Would you like to proceed?
											</TD>
										</TR>
										<TR>
											<TD> &nbsp; </TD>
										</TR>
										<TR>
											<TD align="center">
												<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/> &nbsp;&nbsp;&nbsp;&nbsp;
												<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</DIV>
					<!--End JCPPop DIV -->
				</FORM>
				<!--
	SERVER SIDE VALIDATION SECTION
-->
				<xsl:call-template name="validation"/>
				<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
					<br/>
					<br/>
					<p align="center">
						<font face="Verdana">Please wait ... validating input!</font>
					</p>
				</DIV>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>