<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="occasionInput" select="root/parameters/occasionInput"/>
<xsl:variable name="companyId" select="root/parameters/companyId"/>";
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>";
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>";
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>";
<xsl:variable name="addonImageSuffixSmall" select="root/parameters/addonImageSuffixSmall"/>";
<xsl:variable name="addonImageSuffixMedium" select="root/parameters/addonImageSuffixMedium"/>";
<xsl:variable name="addonImageSuffixLarge" select="root/parameters/addonImageSuffixLarge"/>";

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> Lookup Greeting Card</TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">

	var companyId = "<xsl:value-of select="$companyId"/>";

<![CDATA[

	//This function passes parameters from this page to a function on the calling page
	function goBack()
	{
		window.returnValue = "BACK";
		window.close();
	}

	function processGoBack()
	{
		if ( window.event.keyCode == 13 )
		{
			goBack();
		}
	}

	//This function closes both the current window and the lookup window
	function closePage()
	{
		window.returnValue = '';
		window.close();
	}

	function processClosePage()
	{
		if ( window.event.keyCode == 13 )
		{
			closePage();
		}
	}

	//This function passes parameters from this page to a function on the calling page
	function populatePage(cardId)
	{
		window.returnValue = cardId;
		window.close();
	}

	function processPopulatePage(cardId)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(cardId);
		}
	}

	//This function refreshes the current page with the selected card
	//Or reopens the list page with the proper occasion
	function openPopup()
	{
		//Redisplay current window with a different card
	    var url_source="";
		document.forms[0].action = url_source;
		document.forms[0].method = "get";
		document.forms[0].target = "VIEW_GREETING_CARD_DETAIL";
		document.forms[0].submit();
	}

	function init()
	{
		window.name = "VIEW_GREETING_CARD_DETAIL";
		]]>
		document.forms[0].occasionInput.value = "<xsl:value-of select="$occasionInput"/>";

<![CDATA[
  	}
 ]]>
</SCRIPT>

<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY onLoad="javascript:window.focus(); init();" bgcolor="#FFFFFF">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_GREETING_CARD"/>
<CENTER>
<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
<INPUT type="hidden" name="occasionInput" value=""/>
<input type="hidden" name="companyId" value="{$companyId}"/>
<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
		<TR>
            <TD align="center">
               <h1>Greeting Card Detail</h1>
            </TD>
		</TR>
    	<TR>
    		<TD>
		    <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
		        <TR>
		            <TD align="center">
					     <SELECT tabindex="1" name="cardIdInput" onchange="javascript:openPopup()">
					     		<OPTION value=""> ---Select a card---</OPTION>
							<xsl:for-each select="/root/greetingCardLookup/searchResults/searchResult">
						        <OPTION value="{@addonId}"> <xsl:value-of select="@description"/> </OPTION>
							</xsl:for-each>
						 </SELECT>
					</TD>
		        </TR>
		        <TR>
	 	           <TD>
 	 	          	<HR></HR>
    	        	</TD>
     		   </TR>
		        <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
			        <TR>
	      			    <TD align="right">
				          	<IMG tabindex="2" src="../images/button_back_to_cards.gif" alt="Back to Card Selections" border="0" onclick="javascript:goBack()" onkeydown="javascript:processGoBack()"/>&nbsp;
				           	<IMG tabindex="3" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()"/>
				        </TD>
					</TR>
				</TABLE>
		    </TABLE>
		    <TABLE width="98%" class="LookupTable" cellpadding="2" cellspacing="2">
				<TR>
					<TD width="34%">
						<TABLE width="100%" border="1" cellpadding="2" cellspacing="2">
							<TR>
								<TD>
									<IMG width="280" height="260">
										<xsl:attribute name="src">/product_images/<xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@addonId"/><xsl:value-of select="$addonImageSuffixMedium"/></xsl:attribute>
									</IMG>
								</TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="66%" valign="top">
						<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
							<TR>
								<TD class="label"> Greeting Card: </TD>
								<TD> <xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@description"/>  </TD>
							</TR>
							<TR>
								<TD class="label"> Front: </TD>
								<TD> <xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@description"/>  </TD>
							</TR>
							<TR>
								<TD class="label"> Inside: </TD>
								<TD> <xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@inside"/>  </TD>
							</TR>
							<TR>
								<TD class="label"> Price: </TD>
								<TD>
								<SCRIPT language="javascript">
									<![CDATA[
										//format the price
									]]>
									var price = "<xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@price"/>";
									<![CDATA[
									 var place = price.indexOf('.');
									 var rightOf = price.substring(place, 10);
									 if ((rightOf.length) == 2)
									 	price +="0";
									 document.write("&nbsp;$"+price+"")
									]]>
								</SCRIPT>

								</TD>
							</TR>
							<TR>
								<TD colspan="2"> &nbsp; </TD>
							</TR>
							<TR>
								<TD colspan="2" align="center">
									<SCRIPT language="javascript">
										<![CDATA[
											//write out the function to populate the parent page
										]]>
											var cardId = "<xsl:value-of select="/root/greetingCardLookup/searchResults/searchResult[@selectedAddOnId = @addonId]/@addonId"/>";
											<![CDATA[
											document.write("<IMG onclick=\"javascript: populatePage(\'" + cardId + "\')\"")
											document.write("onkeydown=\"javascript: processPopulatePage(\'" + cardId + "\')\" tabindex=\"4\" src=\"../images/button_select_card.gif\"")
											document.write("</IMG>")
											]]>
									</SCRIPT>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
		    </TABLE>
		    <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
			    <TR>
	      			 <TD align="right">
			          	<IMG tabindex="5" src="../images/button_back_to_cards.gif" alt="Back to Card Selections" border="0" onclick="javascript:goBack()" onkeydown="javascript:processGoBack()"/>&nbsp;
			           	<IMG tabindex="6" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()"/>
			        </TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</CENTER>
</FORM>
</BODY>
</HTML>

</xsl:template>
</xsl:stylesheet>
