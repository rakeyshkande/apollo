<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="countryType" select="root/parameters/countryType"/>
<xsl:variable name="phoneInput" select="root/parameters/phoneInput"/>
<xsl:variable name="institutionInput" select="root/parameters/institutionInput"/>
<xsl:variable name="addressInput" select="root/parameters/addressInput"/>
<xsl:variable name="cityInput" select="root/parameters/cityInput"/>
<xsl:variable name="stateInput" select="root/parameters/stateInput"/>
<xsl:variable name="zipCodeInput" select="root/parameters/zipCodeInput"/>
<xsl:variable name="productId" select="root/parameters/productId"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE>Florist Lookup </TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">
	<![CDATA[
	function onKeyDown()
	{
		if ( window.event.keyCode == 13 )
		{
			reopenPopup();
			return false;
		}
	}

	function closeFloristLookup(floristId)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(floristId)
		}
	}

	//This function passes parameters from this page to a function on the calling page
	function populatePage(floristId)
	{
		window.returnValue = floristId;
		window.close();
	}

	//This function submits the page and populates it with new search results.
	function reopenPopup()
	{
		check = true;
		business = stripWhitespace(document.forms[0].institutionInput.value);
        city = stripWhitespace(document.forms[0].cityInput.value);
        state = stripWhitespace(document.forms[0].stateInput.value);
        zip = stripWhitespace(document.forms[0].zipCodeInput.value);
        phone = stripWhitespace(document.forms[0].phoneInput.value);
        countryTypeInput = stripWhitespace(document.forms[0].countryTypeInput.value);
]]>

<![CDATA[
	 //state is required if business is given
	 if ((business.length > 0) ]]> &amp; <![CDATA[(state.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].stateInput.focus();
	 	  check = false;
		}
		document.forms[0].stateInput.style.backgroundColor='pink';
     }  // close state if

     //city is required if no zip and phone are given
	 if ((city.length == 0) ]]>&amp; <![CDATA[(zip.length == 0) ]]>&amp; <![CDATA[(phone.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].cityInput.focus();
	 	  check = false;
		}
		document.forms[0].cityInput.style.backgroundColor='pink';
     }  // close city if

     //zip is required if no city, state and phone are given
	 if ((zip.length == 0) ]]>&amp; <![CDATA[(city.length == 0) ]]>&amp; <![CDATA[(state.length == 0) ]]> &amp; <![CDATA[(phone.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].zipCodeInput.focus();
	 	  check = false;
		}
		  document.forms[0].zipCodeInput.style.backgroundColor='pink';
     }  // close zip if

	 if (!check)
	 {
	 	alert("Please correct the marked fields")
	 	return false;
	 }

     //Now that everything is valid, open the popup
	 if (check)
	 {
		var url_source="";
		document.forms[0].action = url_source;
		document.forms[0].method = "get";
		document.forms[0].target = "VIEW_FLORIST_LOOKUP";
		document.forms[0].submit();
	 }

	}

	function init()
	{
		window.name = "VIEW_FLORIST_LOOKUP";
		//Populate the countryTypeInput
		//and the search criteria inputs
	]]>
		document.forms[0].countryTypeInput.value = "<xsl:value-of select="$countryType"/>";
		document.forms[0].phoneInput.value = "<xsl:value-of select="$phoneInput"/>";
		document.forms[0].institutionInput.value = "<xsl:value-of select="$institutionInput"/>";
        document.forms[0].addressInput.value = "<xsl:value-of select="$addressInput"/>";
	    document.forms[0].cityInput.value = "<xsl:value-of select="$cityInput"/>";
  		document.forms[0].stateInput.value = "<xsl:value-of select="$stateInput"/>";
  		document.forms[0].zipCodeInput.value = "<xsl:value-of select="$zipCodeInput"/>";
  		document.forms[0].productId.value = "<xsl:value-of select="$productId"/>";


<![CDATA[
		//Set the focus on the first field
		document.forms[0].institutionInput.focus();
  	}
	]]>
</SCRIPT>

<link REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></link>

</HEAD>

<BODY onLoad="javascript:window.focus(); init();">
<FORM name="form" method="get" action="{$actionServlet}">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_FLORIST"/>
<INPUT type="hidden" name="countryTypeInput" value=""/>
<INPUT type="hidden" name="productId" value=""/>
	<TABLE  width="98%" border="0" cellspacing="0" cellpadding="2">
		<TR>
			<TD WIDTH="100%" VALIGN="top">
				<BR></BR>
				<H1 align="CENTER"> Florist Lookup </H1>

				<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">
		        	<TR>
			            <TD width="50%" class="labelright" > Name: &nbsp; </TD>
			            <TD>
			                <INPUT tabindex="0" class="TblText" name="institutionInput" type="text" size="30" maxlength="75" onFocus="javascript:fieldFocus('institutionInput')" onblur="javascript:fieldBlur('institutionInput')">
			                </INPUT>
			            </TD>
				    </TR>

					<TR>
						<TD class="labelright"> Phone: &nbsp; </TD>
						<TD>
			                <INPUT tabindex="1" class="TblText" name="phoneInput" type="text" size="20" maxlength="20" onFocus="javascript:fieldFocus('phoneInput')" onblur="javascript:fieldBlur('phoneInput')">
			                </INPUT>
						</TD>
					</TR>

					<TR>
						<TD class="labelright"> Address: &nbsp; </TD>
						<TD>
			                <INPUT tabindex="2" class="TblText" name="addressInput" type="text" size="20" maxlength="75" onFocus="javascript:fieldFocus('addressInput')" onblur="javascript:fieldBlur('addressInput')">
			                </INPUT>
						</TD>
					</TR>

					<TR>
						<TD class="labelright"> City: &nbsp; </TD>
						<TD>
							<INPUT tabindex="3" class="TblText" name="cityInput"  maxlength="50" size="20" type="text" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')">
							</INPUT>
						</TD>
					</TR>

					<TR>
						<TD class="labelright" > State: &nbsp; </TD>
					<xsl:choose>
						<xsl:when test="$countryType = 'US'">
							<TD>
							     <SELECT tabindex="4" class="TblText" name="stateInput">
								      <OPTION value=""> </OPTION>
									<xsl:for-each select="/root/floristLookup/states/state[@countryCode='']">
								        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
									</xsl:for-each>
								 </SELECT>
							</TD>
				   		</xsl:when>
				   		<xsl:when test="$countryType = 'CA'">
							<TD class="TblText">
							     <SELECT tabindex="4" class="TblText" name="stateInput">
							     	<OPTION value=""></OPTION>
									<xsl:for-each select="/root/floristLookup/states/state[@countryCode='CAN']">
								        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
									</xsl:for-each>
								 </SELECT>
							</TD>
					   </xsl:when>
				   		<xsl:otherwise>
							<TD>
								<INPUT tabindex="4" name="stateInput" class="TblText" maxlength="30" size="6" type="text" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"></INPUT>
							</TD>
						</xsl:otherwise>
					</xsl:choose>
					</TR>

					<TR>
						<TD nowrap="true" class="labelright" > Zip/Postal Code: &nbsp;</TD>
						<TD>
							<INPUT tabindex="5" name="zipCodeInput" class="TblText" maxlength="6" size="6" type="text" value="" onFocus="javascript:fieldFocus('zipCodeInput')" onblur="javascript:fieldBlur('zipCodeInput')">
							</INPUT>
						</TD>
					</TR>
					<TR>
						<TD>
						</TD>
						<TD>
							<IMG tabindex="6" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/>
						</TD>
					</TR>
					<TR>
						<TD colspan="10">
							<HR></HR>
						</TD>
					</TR>
						<TABLE width="100%" id="FormContainer">
						<TR>
							<TD class="instruction">
								&nbsp;Please click on an arrow to select a customer.
							</TD>
			    			<TD align="right">
							 	<IMG tabindex="7" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeFloristLookup('')"/>
						 	</TD>
			    		</TR>
						<TR>
							<TABLE class="LookupTable" width="100%">
							<TR>
					            <TD width="5%" valign="bottom"> &nbsp; </TD>
								<TD width="5%" class="label" valign="bottom"> Weight </TD>
								<TD class="label" valign="bottom"> Member <BR></BR> Number </TD>
								<TD class="label" valign="bottom"> Name / Address </TD>
								<TD class="label" valign="bottom"> Phone </TD>
								<TD class="label" valign="bottom" align="center"> Goto <BR></BR> Flag </TD>
								<TD class="label" valign="bottom" align="center"> Sunday <BR></BR> Delivery </TD>
								<TD class="label" valign="bottom" align="center"> Mercury <BR></BR> Flag </TD>
								<TD class="label" valign="bottom"> Hours </TD>
							</TR>
							<TR>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
					            <TD> <HR></HR> </TD>
							</TR>
							<TR>
								<TD>
									<xsl:for-each select="/root/floristLookup/searchResults/searchResult">
										<TR>
											<TD>
												<SCRIPT language="javascript">
													var floristId = "<xsl:value-of select="@floristId"/>";
													<xsl:choose>
													<xsl:when test = "@floristBlockType != ''">
														<![CDATA[
															document.write("<IMG alt=\"disabled\" src=\"../images/selectButtonRight_disabled.gif\">")
														]]>
													</xsl:when>
													<xsl:otherwise>
														<![CDATA[
															document.write("<IMG onclick=\"javascript: populatePage(\'" + floristId + "\')\"")
															document.write("onkeydown=\"javascript:closeFloristLookup(\'" + floristId + "\')\" tabindex=\"8\" src=\"../images/selectButtonRight.gif\">")
															document.write("</IMG>")
														]]>
													</xsl:otherwise>
													</xsl:choose>
												</SCRIPT>
											</TD>
											<xsl:choose>
												<xsl:when test="@floristZipCode = $zipCodeInput">
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@floristWeight"/>
													</TD>
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@floristId"/>
													</TD>
													<TD class="matchedSearchResult">
														<xsl:value-of select="@floristName"/> &nbsp;<xsl:value-of select="@address"/>&nbsp;<xsl:value-of select="@city"/>&nbsp;<xsl:value-of select="@state"/>&nbsp;<xsl:value-of select="@floristZipCode"/>
													</TD>
													<TD class="matchedSearchResult">
														<xsl:value-of select="@phoneNumber"/>
													</TD>
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@goToFlag"/>
													</TD>
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@sundayFlag"/>
													</TD>
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@mercuryFlag"/>
													</TD>
													<TD align="center" class="matchedSearchResult">
														<xsl:value-of select="@hours"/>
													</TD>
												</xsl:when>
												<xsl:otherwise>
													<TD align="center">
														<xsl:value-of select="@floristWeight"/>
													</TD>
													<TD align="center">
														<xsl:value-of select="@floristId"/>
													</TD>
													<TD>
														<xsl:value-of select="@floristName"/> &nbsp;<xsl:value-of select="@address"/>&nbsp;<xsl:value-of select="@city"/>&nbsp;<xsl:value-of select="@state"/>&nbsp;<xsl:value-of select="@floristZipCode"/>
													</TD>
													<TD>
														<xsl:value-of select="@phoneNumber"/>
													</TD>
													<TD align="center">
														<xsl:value-of select="@goToFlag"/>
													</TD>
													<TD align="center">
														<xsl:value-of select="@sundayFlag"/>
													</TD>
													<TD align="center">
														<xsl:value-of select="@mercuryFlag"/>
													</TD>
													<TD align="center">
														<xsl:value-of select="@hours"/>
													</TD>
												</xsl:otherwise>
											</xsl:choose>
										</TR>
										<TR>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
								            <TD> <HR></HR> </TD>
										</TR>
									</xsl:for-each>
								</TD>
							</TR>
							<TR>
								<TD>
									<IMG tabindex="9" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');" onkeydown="javascript:closeFloristLookup('');"></IMG>
								</TD>
								<TD colspan="8"> None of the above </TD>
							</TR>
						</TABLE>
					</TR>
					<TR>
		    			<TD align="right">
						 	<IMG tabindex="10" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeFloristLookup('');"/>
					 	</TD>
		    		</TR>
				</TABLE>

	</TABLE>



	</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>