<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="countryType" select="root/parameters/countryType"/>
<xsl:variable name="institutionType" select="root/parameters/institutionType"/>
<xsl:variable name="phoneInput" select="root/parameters/phoneInput"/>
<xsl:variable name="institutionInput" select="root/parameters/institutionInput"/>
<xsl:variable name="addressInput" select="root/parameters/addressInput"/>
<xsl:variable name="cityInput" select="root/parameters/cityInput"/>
<xsl:variable name="stateInput" select="root/parameters/stateInput"/>
<xsl:variable name="zipCodeInput" select="root/parameters/zipCodeInput"/>

<xsl:template match="/">

<HTML>
<HEAD>
<title> Recipient Lookup </title>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">

	var keyCodeEnter = 13;

	<![CDATA[

	function onKeyDown()
	{
		if ( window.event.keyCode == keyCodeEnter )
		{
			reopenPopup();
			return false;
		}
	}

	//This function passes parameters from this page to a function on the calling page
	function populatePage(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, addressType, bfhInfo)
	{
		var ret = new Array();

		ret[0] = customerid;
		ret[1] = first;
		ret[2] = last;
		ret[3] = address1;
		ret[4] = address2;
		ret[5] = city;
		ret[6] = state;
		ret[7] = zip;
		ret[8] = country;
		ret[9] = phone;
		ret[10] = altPhone;
		ret[11] = altPhoneExt;
		ret[12] = email;
		ret[13] = bfh;
                ret[14] = addressType;
                ret[15] = bfhInfo;
		window.returnValue = ret;
		window.close();
	}

	function closeRecipientLookup(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, addressType, bfhInfo)
	{
		if ( window.event.keyCode == keyCodeEnter )
		{
			populatePage(customerid, first, last, address1, address2, city, state, zip, country, phone, altPhone, altPhoneExt, email, bfh, addressType, bfhInfo);
		}
	}

	//This function submits the page and populates it with new search results.
	function reopenPopup()
	{
		//initialize the background colors to white
		 document.forms[0].phoneInput.style.backgroundColor='white';
		 document.forms[0].addressInput.style.backgroundColor='white';
		 document.forms[0].cityInput.style.backgroundColor='white';
		 document.forms[0].stateInput.style.backgroundColor='white';
		 document.forms[0].zipCodeInput.style.backgroundColor='white';

	 	check = true;
	    institutionTypeInput = stripWhitespace(document.forms[0].institutionTypeInput.value);

		//institutionInput may be hidden
		if ((institutionTypeInput == "F") | (institutionTypeInput == "H") | (institutionTypeInput == "N"))
		{
	       institution = stripWhitespace(document.forms[0].institutionInput.value);
 		}

		phone = stripWhitespace(document.forms[0].phoneInput.value);
        address = stripWhitespace(document.forms[0].addressInput.value);
        city = stripWhitespace(document.forms[0].cityInput.value);
        state = stripWhitespace(document.forms[0].stateInput.value);
        zip = stripWhitespace(document.forms[0].zipCodeInput.value);
   	 	countryType = stripWhitespace(document.forms[0].countryTypeInput.value);

     //For certain institution types, institution name is required
     if ((institutionTypeInput == "F") | (institutionTypeInput == "H") | (institutionTypeInput == "N"))
     {
	     if ((phone.length == 0) ]]> &amp; <![CDATA[(institution.length == 0) ]]> &amp; <![CDATA[(address.length == 0) ]]> &amp; <![CDATA[(city.length == 0)]]> &amp; <![CDATA[(state.length == 0) ]]> &amp;<![CDATA[(zip.length == 0))
		 {
	 	    if (check == true)
			{
			  document.forms[0].institutionInput.focus();
		 	  check = false;
			}
 	        document.forms[0].institutionInput.style.backgroundColor='pink';
	     }  // close institution inner if
   	 } // close institution outer if

     //if phone is not given, city and state are required
	 if (phone.length == 0)
	 {
		 if (city.length == 0)
	 	 {
			if (check == true)
			{
			  document.forms[0].cityInput.focus();
	          document.forms[0].cityInput.style.backgroundColor='pink';
   		 	  check = false;
			}
	     }  // close city if

	     if (state.length == 0)
	 	 {
			if (check == true)
			{
			  document.forms[0].stateInput.focus();
			  document.forms[0].stateInput.style.backgroundColor='pink';
		 	  check = false;
			}
  	     }  // close state if
	  }  //close phone if

	 if (!check)
	 {
		alert("Please correct the marked fields");
		return false;
     }

	//Everything is valid, so the popup can be opened
	if (check)
	 {
		var url_source="";
		document.forms[0].action = url_source;
		document.forms[0].method = "get";
		document.forms[0].target = "VIEW_RECIPIENT_LOOKUP";
		document.forms[0].submit();
	 }

	}

	function init()
	{
		window.name = "VIEW_RECIPIENT_LOOKUP";
		//Populate the countryTypeInput and institutionTypeInput
		//and the search criteria inputs
	]]>
		document.forms[0].countryTypeInput.value = "<xsl:value-of select="$countryType"/>";
		document.forms[0].institutionTypeInput.value = "<xsl:value-of select="$institutionType"/>";
		document.forms[0].phoneInput.value = "<xsl:value-of select="$phoneInput"/>";
	    document.forms[0].cityInput.value = "<xsl:value-of select="$cityInput"/>";
  		document.forms[0].stateInput.value = "<xsl:value-of select="$stateInput"/>";
  		document.forms[0].zipCodeInput.value = "<xsl:value-of select="$zipCodeInput"/>";
<![CDATA[
		document.forms[0].institutionInput.value = deCode("]]><xsl:value-of select="$institutionInput"/><![CDATA[");
        document.forms[0].addressInput.value = deCode("]]><xsl:value-of select="$addressInput"/><![CDATA[");

		institutionType = document.all.institutionTypeInput.value;

		//set the focus on the first field
		if ((institutionType == "F") | (institutionType == "H") | (institutionType == "N"))
		{
			//Take the state from the proper input
			if (document.all.countryTypeInput.value == "D")
				document.all.stateInput.value = document.all.stateInput.value;
			else
				document.all.stateInput.value = document.all.stateInput.value;

			document.forms[0].institutionInput.focus();
		}
		else
		{
			document.forms[0].phoneInput.focus();
		}
  	}

	function deCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("\'","g"),"\\'" );

		return strSelection;
	}


	]]>
</SCRIPT>

<link REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></link>
</HEAD>

<BODY onLoad="javascript:window.focus(); init();">
<FORM name="form" method="get" action="{$actionServlet}" onsubmit="javascript: reopenPopup();">
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_RECIPIENT"/>
<INPUT type="hidden" name="countryTypeInput" value=""/>
<INPUT type="hidden" name="institutionTypeInput" value=""/>
<INPUT type="hidden" name="sessionid" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentobjId" value="{$persistentObjId}"/>
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="2">
		<TR>
			<TD WIDTH="100%" VALIGN="top">
				<BR></BR>
				<H1 align="CENTER"> Recipient Lookup </H1>
				<CENTER>
				<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">
					<TR>
						<TD>
							<TABLE width="95%">


								<xsl:choose>
								   	<xsl:when test="$institutionType = 'F' or $institutionType = 'H' or $institutionType = 'N'">

								         <TR>
									<TD width="50%">
										<INPUT name="phoneInput" class="TblText" maxlength="20" size="10" type="hidden" value=""></INPUT>
									</TD>
								</TR>

								         <TR>
											<TD nowrap="true" class="labelright"> Institution Name:  </TD>
											<TD>
												<INPUT name="institutionInput" class="TblText" maxlength="50" size="20" type="text" onFocus="javascript:fieldFocus('institutionInput')" onblur="javascript:fieldBlur('institutionInput')"></INPUT>
											</TD>
										</TR>


										<TR>
									<TD class="labelright"> City: </TD>
									<TD>
										<INPUT name="cityInput" class="TblText" maxlength="50" size="20" type="text" value="" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"></INPUT>
									</TD>
								</TR>

								<TR>
									<TD class="labelright"> State: </TD>
									<xsl:choose>
									   <xsl:when test="$countryType = 'US'">
											<TD class="TblText">
											     <SELECT class="TblText" name="stateInput">
											     	<OPTION value=""></OPTION>
													<xsl:for-each select="/root/customerLookup/states/state[@countryCode='']">
												        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
													</xsl:for-each>
												 </SELECT>
											</TD>
									   </xsl:when>
									   <xsl:when test="$countryType = 'CA'">
											<TD class="TblText">
											     <SELECT class="TblText" name="stateInput">
											     	<OPTION value=""></OPTION>
													<xsl:for-each select="/root/customerLookup/states/state[@countryCode='CAN']">
												        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
													</xsl:for-each>
												 </SELECT>
											</TD>
									   </xsl:when>
									   <xsl:otherwise>

											<TD>
												<INPUT name="stateInput" class="TblText" maxlength="30" size="6" type="text" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"></INPUT>
											</TD>
									   </xsl:otherwise>
									</xsl:choose>
								</TR>


								   	</xsl:when>
								   	<xsl:otherwise>
								   	<TR>
									<TD nowrap="true" width="50%" class="labelright"> Phone Number:  </TD>
									<TD width="50%">
										<INPUT tabindex="0" name="phoneInput" class="TblText" maxlength="20" size="10" type="text" onFocus="javascript:fieldFocus('phoneInput')" onblur="javascript:fieldBlur('phoneInput')"></INPUT>
									</TD>
								</TR>
								<TR>
									<TD>
										<INPUT name="cityInput" class="TblText" maxlength="50" size="20" type="hidden" value=""></INPUT>
									</TD>
									</TR>
									<TR>

									<TD>
										<INPUT name="stateInput" class="TblText" maxlength="50" size="20" type="hidden" value=""></INPUT>
									</TD>
									</TR>
									   	<TR>
											<TD>
										      <INPUT name="institutionInput" type="hidden" class=""/>
										     </TD>
										</TR>
								   	</xsl:otherwise>
								</xsl:choose>

								<TR>
									<TD>
										<INPUT name="addressInput" class="TblText" maxlength="100" size="20" type="hidden" value=""></INPUT>
									</TD>
								</TR>



								<TR>
									<TD>
										<INPUT name="zipCodeInput" class="TblText" maxlength="6" size="6" type="hidden" value=""></INPUT>
									</TD>
								</TR>




								<TR>
									<TD> &nbsp; </TD>
									<TD >
										<IMG tabindex="1" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/>
									</TD>
								</TR>

								<TR>
									<TD colspan="10">
										<HR></HR>
									</TD>
								</TR>

								<TR>
									<TD class="instruction">
										&nbsp;Please click on an arrow to select a customer.
									</TD>
					    			 <TD align="right">
									 	<IMG tabindex="2" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','')" onkeydown="javascript:closeRecipientLookup('','','','','','','','','','','','','','')"/>
								 	</TD>
					    		</TR>
							</TABLE>

							<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
								<TR>
									<TD>
										<TABLE class="LookupTable" width="100%" border="1" cellpadding="2" cellspacing="2">
											<TR>
												<TD width="5%" class="label"> &nbsp; </TD>
												<TD class="label" valign="bottom"> Name </TD>
												<TD class="label" valign="bottom"> Address </TD>
												<TD class="label" valign="bottom"> City </TD>
												<TD class="label" valign="bottom"> State </TD>
												<TD class="label" valign="bottom"> Zip/Postal Code</TD>
												<TD class="label" valign="bottom"> Phone</TD>
												<TD>
													<xsl:for-each select="/root/customerLookup/searchResults/searchResult">
														<TR>
															<TD>
																<SCRIPT language="javascript">
																<![CDATA[
																	//If the location type is not residental, get the extension
																]]>
		         													var customerId = "<xsl:value-of select="@customerId"/>";
		         													if ( customerId == "" )
		         													{
		         														customerId = "<xsl:value-of select="@businessId"/>";
		         													}
																	var state = "<xsl:value-of select="@state"/>";
																	var zip = "<xsl:value-of select="@zipCode"/>";
																	var country = "<xsl:value-of select="@countryId"/>";
																	var phone = "<xsl:value-of select="@homePhone"/>";
																	var altPhone = "<xsl:value-of select="@workPhone"/>";
																	var altPhoneExt = "<xsl:value-of select="@workPhoneExt"/>";
																	var email = "<xsl:value-of select="@email"/>";

																	<![CDATA[
																		var firstName = deCode("]]><xsl:value-of select="@firstName"/><![CDATA[");
																		var lastName = deCode("]]><xsl:value-of select="@lastName"/><![CDATA[");
																		var address1 = deCode("]]><xsl:value-of select="@address1"/><![CDATA[");
																		var address2 = deCode("]]><xsl:value-of select="@address2"/><![CDATA[");
																		var city = deCode("]]><xsl:value-of select="@city"/><![CDATA[");
																		var bfh = deCode("]]><xsl:value-of select="@bfhName"/><![CDATA[");
                                                                                                                                                var addressType=deCode("]]><xsl:value-of select="@addressType"/><![CDATA[");
                                                                                                                                                var bfhInfo=deCode("]]><xsl:value-of select="@bfhInfo"/><![CDATA[");

																		document.write("<IMG onclick=\"javascript: populatePage(\'" + customerId + "\', \'" + firstName + "\', \'" + lastName + "\', \'" + address1 + "\', \'" + address2 + "\', \'" + city + "\', \'" + state + "\', \'" + zip + "\', \'" + country + "\', \'" + phone + "\', \'" + altPhone + "\', \'" + altPhoneExt + "\', \'" + email + "\', \'" + bfh + "\', \'" + addressType + "\', \'" + bfhInfo + "\')\"")
																		document.write("onkeydown=\"javascript: closeRecipientLookup(\'" + customerId + "\', \'" + firstName + "\', \'" + lastName + "\', \'" + address1 + "\', \'" + address2 + "\', \'" + city + "\', \'" + state + "\', \'" + zip + "\', \'" + country + "\', \'" + phone + "\', \'" + altPhone + "\', \'" + altPhoneExt + "\', \'" + email + "\', \'" + bfh + "\', \'" + addressType + "\', \'" + bfhInfo + "\')\" tabindex=\"3\" src=\"../images/selectButtonRight.gif\">")
																		document.write("</IMG>")
																	]]>

																</SCRIPT>
															</TD>
															<TD align="left">
																<xsl:value-of select="@bfhName"/> &nbsp; <xsl:value-of select="@firstName"/> &nbsp; <xsl:value-of select="@lastName"/>
															</TD>
															<TD align="left">
																<xsl:value-of select="@address1"/> &nbsp; <xsl:value-of select="@address2"/>
															</TD>
															<TD align="left">
																<xsl:value-of select="@city"/>&nbsp;
															</TD>
															<TD align="left">
																<xsl:value-of select="@state"/>&nbsp;
															</TD>
															<TD align="left">
																<xsl:value-of select="@zipCode"/>&nbsp;
															</TD>
															<TD align="left">
																<xsl:value-of select="@homePhone"/>&nbsp;
															</TD>
														</TR>
													</xsl:for-each>
												</TD>
											</TR>
											<TR>
												<TD>
													<IMG tabindex="4" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','')" onkeydown="javascript:closeRecipientLookup('','','','','','','','','','','','','','','','')"></IMG>
												</TD>
												<TD colspan="8"> None of the above </TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>

							<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
					            <TR>
					    			 <TD align="right">
									 	<IMG tabindex="5" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('','','','','','','','','','','','','','')" onkeydown="javascript:closeRecipientLookup('','','','','','','','','','','','','','','','')"/>
								 	</TD>
					    		</TR>
				         	</TABLE>
					 	</TD>
		    		</TR>
				</TABLE>
			</CENTER>
		  </TD>
		</TR>
	</TABLE>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>