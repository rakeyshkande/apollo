<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:variable name="sessionId" select="root/parameters/sessionId"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE>Order Entry Application Statistics</TITLE>

<SCRIPT language="JavaScript">
<![CDATA[
			
	function init()
	{	
]]> 		
				
  	}
		
	
</SCRIPT>
<link REL="STYLESHEET" TYPE="text/css" href="../css/ftd.css"></link>
</HEAD>

<BODY onLoad="init();">
<FORM name="form" method="post" action="">
	
Orders since 7:00 = <xsl:value-of select="/root/pageData/data[@name='ordersToday']/@value"/>
<BR/>
Orders in last 24 hours = <xsl:value-of select="/root/pageData/data[@name='ordersLast24']/@value"/>
<BR/>
Orders in last 48 hours = <xsl:value-of select="/root/pageData/data[@name='ordersLast48']/@value"/>
<BR/>
Open DB cursors = <xsl:value-of select="/root/pageData/data[@name='openDBCursors']/@value"/>
<BR/>
Average call time = <xsl:value-of select="/root/pageData/data[@name='averageCallTime']/@value"/>
<BR/>
Current users = <xsl:value-of select="/root/pageData/data[@name='currentUsers']/@value"/>
<BR/>
Run time = <xsl:value-of select="/root/pageData/data[@name='runDate']/@value"/>
<BR/>

</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>