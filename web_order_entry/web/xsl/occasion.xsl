<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="countryid" select="root/parameters/countryid"/>
<xsl:variable name="country" select="root/parameters/country"/>
<xsl:variable name="zipCode" select="root/parameters/zipCode"/>
<xsl:variable name="city" select="root/parameters/city"/>
<xsl:variable name="state" select="root/parameters/state"/>
<xsl:variable name="occasion" select="root/parameters/occasion"/>
<xsl:variable name="deliveryDate" select="root/parameters/deliveryDate"/>
<xsl:variable name="refreshTarget" select="root/parameters/refreshTarget"/>
<xsl:variable name="focusElement" select="root/parameters/focusElement"/>
<xsl:variable name="recipientId" select="root/parameters/recipientId"/>
<xsl:variable name="recipientPhone" select="root/parameters/recipientPhone"/>
<xsl:variable name="recipientPhoneExt" select="root/parameters/recipientPhoneExt"/>
<xsl:variable name="recipientFirstName" select="root/parameters/recipientFirstName"/>
<xsl:variable name="recipientLastName" select="root/parameters/recipientLastName"/>
<xsl:variable name="recipientAddress1" select="root/parameters/recipientAddress1"/>
<xsl:variable name="recipientAddress2" select="root/parameters/recipientAddress2"/>
<xsl:variable name="recipientAddressType" select="root/parameters/recipientAddressType"/>
<xsl:variable name="recipientBhfInfo" select="root/parameters/recipientBhfInfo"/>
<xsl:variable name="recipientBhfName" select="root/parameters/recipientBhfName"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> FTD - Shopping </TITLE>
<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>


<SCRIPT language="JavaScript">

var sessionId = "<xsl:value-of select="$sessionId"/>";
var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
var focusElement = "<xsl:value-of select="$focusElement"/>";
var submitInd = "submit";
var actionServlet = "<xsl:value-of select="$actionServlet"/>";
// var popupFlag = false;
var disableFlag = false;
var disableRefreshFlag = true;
var JCPSwitch = false;
<![CDATA[

	document.onkeydown = backKeyHandler;
	document.oncontextmenu=stopIt;

	var cntryType;

	function hideDIVs()
	{
	   document.all.occasionsDIV.style.visibility = "hidden";
	   document.all.deliveryDateDIV.style.visibility = "hidden";
	   document.all.countryDIV.style.visibility = "hidden";
	}

	function showDIVs()
	{
	   document.all.occasionsDIV.style.visibility = "visible";
	   document.all.deliveryDateDIV.style.visibility = "visible";
	   document.all.countryDIV.style.visibility = "visible";
	}

	function onKeyDown()
	{
	   if (window.event.keyCode == 13)
	   {
	       submitForm();
	   }
	}

	function EnterToRefresh()
	{
	   if (window.event.keyCode == 13)
		{
		    doRefresh();
		}
	}

	function doMAXDeliveryDaysOut()
	{
	    document.forms[0].MAXDeliveryDaysOut.value = "true";
            submitInd = "refresh";
	    validateForm();
	}

	function EnterToZip()
	{
	   if (window.event.keyCode == 13)
		{
		    openZipCodePopup();
		}
		
		return false;
	}

	function openCalendarPopup()
	{
		openCalendarLoadingDiv();

		var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR";
	    	var modal_dim="dialogWidth:400px; dialogHeight:500px; center:yes; status=0";

	 	var ret = window.showModalDialog(url_source,"", modal_dim);
// 	    	window.open(url_source,"", modal_dim);
		closeCalendarLoadingDiv();
 	    	if ( ret!= null ) 
 	    	{
 	    		document.forms[0].deliveryDates.value = ret;
 	    		var selectBox = document.all.deliveryDates;
 	    		setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].text);
 	    	}
 	}
 	
 	function openCalendarLoadingDiv()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("calendarLoading").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-200

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 40
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeCalendarLoadingDiv()
	{
		calendarLoading.style.visibility = "hidden";
	}



	function init() {
]]>
	disableFlag = false;
	disableRefreshFlag = true;
	document.forms[0].countries.value = "<xsl:value-of select="root/selectedData/data[@name='country']/@value"/>";

	<xsl:if test="root/selectedData/data[@name='country']/@value = ''">
		document.forms[0].countries.value = "US";
   	</xsl:if>

	document.forms[0].zipCode.value = "<xsl:value-of select="root/selectedData/data[@name='zip']/@value"/>";
    	document.forms[0].city.value = "<xsl:value-of select="$city"/>";
    	document.forms[0].state.value = "<xsl:value-of select="$state"/>";
	document.forms[0].deliveryDates.value = "<xsl:value-of select="root/selectedData/data[@name='deliveryDate']/@value"/>";
	
	document.forms[0].recipientId.value = "<xsl:value-of select="$recipientId"/>";
	document.forms[0].recipientPhone.value = "<xsl:value-of select="$recipientPhone"/>";
	document.forms[0].recipientPhoneExt.value = "<xsl:value-of select="$recipientPhoneExt"/>";
	document.forms[0].recipientFirstName.value = "<xsl:value-of select="$recipientFirstName"/>";
	document.forms[0].recipientLastName.value = "<xsl:value-of select="$recipientLastName"/>";
	document.forms[0].recipientAddress1.value = "<xsl:value-of select="$recipientAddress1"/>";
	document.forms[0].recipientAddress2.value = "<xsl:value-of select="$recipientAddress2"/>";
	document.forms[0].recipientAddressType.value = "<xsl:value-of select="$recipientAddressType"/>";
	document.forms[0].recipientBhfInfo.value = "<xsl:value-of select="$recipientBhfInfo"/>";
	document.forms[0].recipientBhfName.value = "<xsl:value-of select="$recipientBhfName"/>";

	<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I'">
			openJCPPopup();
	</xsl:if>

	<xsl:choose>
		<xsl:when test="root/selectedData/data[@name='deliveryDate']/@value = ''">
			<![CDATA[
				invalidDeliveryDate.style.visibility = "hidden";
			]]>
		</xsl:when>
		<xsl:otherwise>

			<![CDATA[
				if ( (document.forms[0].deliveryDates.value != " ") &&
					 (document.forms[0].deliveryDates.value != "") )
				{
					invalidDeliveryDate.style.visibility = "hidden";
				}
				else
				{
					invalidDeliveryDate.style.visibility = "visible";
				}
			]]>

		</xsl:otherwise>
	</xsl:choose>

	<xsl:choose>
		<xsl:when test="root/selectedData/data[@name='occasion']/@value = ''">
			document.forms[0].occasions.value = "default";
		</xsl:when>
		<xsl:otherwise>
			document.forms[0].occasions.value = "<xsl:value-of select="root/selectedData/data[@name='occasion']/@value"/>";
		</xsl:otherwise>
	</xsl:choose>

	<xsl:if test="/root/pageData/data[@name='displaySpecGiftPopup']/@value = 'Y'">
		<![CDATA[

			//display the Yes/No Floral DIV Tag
			openNoFloral();
		]]>
	</xsl:if>
	<xsl:if test="/root/pageData/data[@name='displayNoProductPopup']/@value = 'Y'">
		<![CDATA[

			//display the No Products DIV Tag
			openNoProduct();
		]]>
	</xsl:if>
	<xsl:if test="/root/pageData/data[@name='displayFloristPopup']/@value = 'Y'">
		<![CDATA[

			//display the No Products DIV Tag
			openOnlyFloral();
		]]>
	</xsl:if>


<![CDATA[
		if(focusElement.length > 0)
		{
			document.forms[0].elements[focusElement].focus();
		}
		else
		{
		document.forms[0].occasions.focus();
		}

	}

	function doRefresh()
	{


		//only refresh the page if the disableRefresh flag is false (the button is not dimmed)
		if (disableRefreshFlag == false)
		{
			submitInd = "refresh";
			validateForm();
		}
		else
		{
			//do nothing
			return false;
		}
	}

	function refresh()
	{
]]>
		var target = "<xsl:value-of select="$refreshTarget"/>";
		var sessionId = "<xsl:value-of select="$sessionId"/>";
		var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
<![CDATA[
		var source = "occasion";
		if (JCPSwitch)
		{
			source = "occasionJCP";
			target = "DNISChangeServlet";
		}
		var country = document.forms[0].countries.value;
		var zip = document.forms[0].zipCode.value;
		var occasion = document.forms[0].occasions.value
		var deliverydate = document.forms[0].deliveryDates.value;
		var city = document.forms[0].city.value;
		var state = document.forms[0].state.value;
		var recipientPhone = document.forms[0].recipientPhone.value;
		var recipientPhoneExt = document.forms[0].recipientPhoneExt.value;
		var recipientLastName = document.forms[0].recipientLastName.value;
		var recipientFirstName = document.forms[0].recipientFirstName.value;
		var recipientAddress1 = document.forms[0].recipientAddress1.value;
		var recipientAddress2 = document.forms[0].recipientAddress2.value;
		var recipientAddressType = document.forms[0].recipientAddressType.value;
		var recipientBhfInfo = document.forms[0].recipientBhfInfo.value;
		var recipientBhfName = document.forms[0].recipientBhfName.value;
		var recipientId = document.forms[0].recipientId.value;
		var MAXDeliveryDaysOut = document.forms[0].MAXDeliveryDaysOut.value;
		var focusElement = "";

		// Set the focus for when the page refreshes
		if(document.activeElement.name == document.all.countries.name && checkForValidInter() == false)
		{
			focusElement = "zipCode";
		}
		else if(document.activeElement.name == document.all.countries.name && checkForValidInter() == true)
		{
			focusElement = "deliveryDates";
		}
		else if(document.activeElement.name == "clickToAcceptButton" || document.activeElement.name == "zipCode")
		{
			focusElement = "deliveryDates";
		}
		
		var url = target + "?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&country=" + country + 
		                   "&zip=" + zip + "&deliverydate=" + deliverydate + "&occasion=" + occasion + "&countryType=" + cntryType + 
		                   "&source=" + source + "&city=" + city + "&state=" + state + "&focusElement=" + focusElement + 
		                   "&recipientPhone=" + recipientPhone + "&recipientPhoneExt=" + recipientPhoneExt + "&recipientId=" + recipientId +
		                   "&recipientFirstName=" + recipientFirstName + "&recipientLastName=" + recipientLastName +
				   "&recipientAddress1=" + recipientAddress1 + "&recipientAddress2=" + recipientAddress2 + "&MAXDeliveryDaysOut=" + MAXDeliveryDaysOut +
                                   "&recipientAddressType=" + recipientAddressType + "&recipientBhfInfo=" + recipientBhfInfo + "&recipientBhfName=" + recipientBhfName;
		openUpdateWin();
		document.location = url;

	}

	function changeOfCountry()
	{
		checkForInternational('true');
		var x = checkForValidInter();
		if(x)
		{
 ]]>
 			<xsl:choose>
			<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
					openJCPPopup();
			</xsl:when>
			<xsl:otherwise>
					refresh();
			</xsl:otherwise>
			</xsl:choose>
<![CDATA[

		} else {
			refresh();
		}
	}

	/*** This function will dynamically create a domestic check for each country selected
	*    Is the country is not domestic a N/A will be placed in the zip code field
	*/
	function checkForInternational(countryChanged)
	{
		var cntry2 = document.forms[0].countries.value;
		var cntry3;
		var domestic;
]]>
		<xsl:for-each select="/root/occasionData/countries/country">
			cntry3 = "<xsl:value-of select="@countryId"/>";
			domestic = "<xsl:value-of select="@oeCountryType"/>";
<![CDATA[

        	if( cntry2 == cntry3 )
        	{
        		cntryType = domestic;
       	   		if ( domestic == "I" )
       	   		{
       	   			]]>
       	   			<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value != 'JP'">
						<![CDATA[
							document.forms[0].zipCode.value = "N/A";
   							//document.forms[0].deliveryDates.focus();
						]]>
					</xsl:if>
					<![CDATA[
   				}
    			// if country changed to domestic country, clear zip code
    			else if ( countryChanged == "true" )
   				{
   					document.forms[0].zipCode.value = "";
   					//document.forms[0].deliveryDates.focus();
    			}
    		}
]]>
		</xsl:for-each>
<![CDATA[
	}

	/*** This function will dynamically create a domestic check for each country selected
	*    Is the country is not domestic a N/A will be placed in the zip code field
	*/
	function checkForValidInter()
	{
		var cntry2 = document.forms[0].countries.value;
		var cntry3;
		var domestic;

]]>
		<xsl:for-each select="/root/occasionData/countries/country">
			cntry3 = "<xsl:value-of select="@countryId"/>";
			domestic = "<xsl:value-of select="@oeCountryType"/>";
<![CDATA[

        	if ( cntry2 == cntry3 )
        	{
        		cntryType = domestic;

       	   		if ( domestic == "I" )
       	   		{
   					return true;
    			}
    			else
    			{
    				return false;
    			}
    		}

]]>
		</xsl:for-each>
<![CDATA[
	}
	function JCPPopChoice(inChoice)
	{
	/***  popup JC penney popup for international change
	*/
		if(inChoice == 'Y')
		{
				JCPSwitch = true;
				refresh();
		} else {
				JCPSwitch = false;
				document.forms[0].countries.value = 'US';
				document.forms[0].zipCode.value = '';
				enableContinue();
				closeJCPPopup();
		}



	}
	function openJCPPopup()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		JCPPopV.top=scroll_top+document.body.clientHeight/2-100

		JCPPopV.width=290
		JCPPopV.height=100
		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
	}


	function submitForm()
	{
		if ( disableFlag == false )
		{

			var selectBox = document.all.deliveryDates;
			if(selectBox.selectedIndex != null && selectBox.selectedIndex > -1)
				setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].text);

			var description = "";
	 	 	for (var i=0; i < document.all.occasions.length; i++)
	  		{
				if (document.all.occasions[i].selected) {
	    	     	description = document.all.occasions[i].text;
	    	  	}
	  		}
			document.all.occasionDescription.value = description;

			if(checkForValidInter())
			{
				occasionform.action = "InternationalProductList";
				document.all.search.value = "category";
			}
			else
			{
				occasionform.action = actionServlet;
			}

			submitInd = "submit";
			return validateForm();

		} else {
			return false;
		}
	}

	function openUpdateWin()
	{
		hideDIVs();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-100

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 -100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";

		showDIVs();
	}

	function openNoFloral()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloralV = (dom)?document.getElementById("noFloral").style : ie? document.all.noFloral : document.noFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloralV.top=scroll_top+document.body.clientHeight/2-100

		noFloralV.width=290
		noFloralV.height=100
		noFloralV.left=document.body.clientWidth/2 - 50
		noFloralV.top=document.body.clientHeight/2 - 50;
		noFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoFloral()
	{
		noFloralV.visibility = "hidden";
	}

	function noFloral(inChoice)
	{
		if ( inChoice == "Y" )
		{
			document.forms[0].deliveryDates.focus();
		}
		else
		{
			document.forms[0].zipCode.focus();
		}
		// go ahead and enable continue button as they might change their mind
		// and cannot submit page if disabled
		enableContinue();
		closeNoFloral();
	}

	function openNoProduct()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noProductV = (dom)?document.getElementById("noProduct").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noProductV.top=scroll_top+document.body.clientHeight/2-100

		noProductV.width=290
		noProductV.height=100
		noProductV.left=document.body.clientWidth/2 - 50
		noProductV.top=document.body.clientHeight/2 - 50;
		noProductV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoProduct()
	{
		noProductV.visibility = "hidden";
		document.forms[0].zipCode.focus();
	}

	function noProduct()
	{
		closeNoProduct();
	}

	function openOnlyFloral()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		onlyFloralV = (dom)?document.getElementById("onlyFloral").style : ie? document.all.onlyFloral : document.onlyFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		onlyFloralV.top=scroll_top+document.body.clientHeight/2-100

		onlyFloralV.width=290
		onlyFloralV.height=100
		onlyFloralV.left=document.body.clientWidth/2 - 50
		onlyFloralV.top=document.body.clientHeight/2 - 50;
		onlyFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeOnlyFloral()
	{
		onlyFloralV.visibility = "hidden";
		document.forms[0].zipCode.focus();
	}

	function onlyFloral()
	{
		enableContinue();
		closeOnlyFloral();
	}

	function validateForm()
	{
		var check = true;
		var msg;
		
		openUpdateWin();

	 	document.forms[0].occasions.className="TblText";

		if ( document.all.occasions.value == "default" )
		{
	    		check = false;
	    		msg = "Occasion is required";
		}
		else
		{
			if ( checkForValidInter() )
			{
				if (submitInd == "submit")
				{
					occasionform.submit();
				}
				else
				{
					refresh();
				}
				return;
			}
		}

		if (check == false)
		{
			closeUpdateWin();

			//if (zip != '')
				alert(msg);
			document.forms[0].occasions.focus();
	   		document.forms[0].occasions.className="Error";

			return check;
		}
]]>
		
		<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
<![CDATA[
        ziplen = document.all.zipCode.value.length;
        zip = stripWhitespace(document.all.zipCode.value);
        firstchar = zip.substring(0,1);

		if( (ziplen == 0) ]]> )
		{
	    	check = false;
<![CDATA[
	    	msg = "Zip/Postal Code is required";
		}

		else if( (ziplen != 5) ]]> &amp; <![CDATA[ (ziplen != 6) )
		{
            check = false;
            msg = "Invalid Zip/Postal Code";
        }
            
        //validation error
		if (check == false)
		{
			document.forms[0].zipCode.focus();
			document.forms[0].zipCode.className="Error";
			alert(msg)
			closeUpdateWin();
			return false;
		}
]]>
    	    </xsl:if>
<![CDATA[
///////////////////////////////////////////////////////////////////////////
//INITIATE SERVER SIDE VALIDATIION
///////////////////////////////////////////////////////////////////////////

		if(document.all.zipCode.value.length == 0)
		{
			if(submitInd == "submit")
			{					
				occasionform.submit();
			}
			else
			{
				refresh();
			}
		}
		else
		{
			first5 = document.forms[0].zipCode.value;
			first5 = first5.substr(0,5);
			addValidationEntry("ZIPCODE", first5)
			callToServer();
		}

		if (zip == '')
		    closeUpdateWin();
	}


/////////////////////////////////////////////////////////////////////////////
//SERVER SIDE VALIDATION CALL BACK FUNCTIONS
////////////////////////////////////////////////////////////////////////////
	function onValidationNotAvailable()
	{
		closeUpdateWin();
		var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");

		if ( x == true )
		{
			if ( submitInd == "submit" )
			{				
				occasionform.submit();
			}
			else
			{
				refresh();
			}
		}
	}

	function onValidationResponse(validation)
	{
		var ret_zipCode = validation.ZIPCODE.value;
		closeUpdateWin();

		if (( ret_zipCode == "OK" ) | (zip == ''))
		{

			if ( submitInd == "submit" )
			{				
				occasionform.submit();
			}
			else
			{
				refresh();
			}
		}
		else
		{
		 	if (zip != '')
		 	{
				occasionform.zipCode.style.backgroundColor='pink';
				alert(ret_zipCode);
			}
		}
	}

///////////////////////////////////////////////////////////////////////////////
//	function doCancelOrder()
//	{
//		document.location = "CancelOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId;
//	}


	function deCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
		strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

		return strSelection;
	}

	function disableContinue()
	{
	/*** Change image of continue button to disabled and prevent submitting the page
	*/
	//but only if the country is domestic and the key stroke isn't a tab
	  if (window.event.keyCode != 9)
	  {
]]>
	     <xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
<![CDATA[
		disableFlag = true;
		document.all.continueButton.src  = '../images/button_continue_disabled.gif';
]]>
	     </xsl:if>
<![CDATA[
	  }
	}

	function enableContinue()
	{
	/*** Change image of continue button to enabled and allow submitting the page
	*/
	//but only if its domestic
]]>
	<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
<![CDATA[
		disableFlag = false;
		document.all.continueButton.src  = '../images/button_continue.gif';
]]>
	</xsl:if>
<![CDATA[
	}

	function disableClickToAccept()
	{
	/*** Change image of click to accept button to disabled and prevent page refresh
	*/
	//but only if its domestic
]]>
	<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
<![CDATA[
		disableRefreshFlag = true;
		document.all.clickToAcceptButton.src  = '../images/button_click_to_accept_disabled.gif';
]]>
	</xsl:if>

<![CDATA[
	}

	function enableClickToAccept()
	{
	/*** Change image of clicktoaccept button to enabled and allow page refresh
	*/
	//but only if domestic and only if the key stroke isn't the tab
		if (window.event.keyCode != 9)
		{
]]>
			<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
<![CDATA[
				disableRefreshFlag = false;
				document.all.clickToAcceptButton.src  = '../images/button_click_to_accept.gif';
]]>
			</xsl:if>

<![CDATA[

		}
	}

	function deliveryDateChanged()
	{
		invalidDeliveryDate.style.visibility = "hidden";

		var selectBox = document.all.deliveryDates;
		setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].text);
	}

	function setDeliveryDateDisplay(newValue)
	{
		document.all.deliveryDateDisplay.value = newValue;
	}

]]>
</SCRIPT>


<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>
<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init();">
	<FORM name="occasionform" method="get" action="{$actionServlet}" onsubmit="javascript: return submitForm();">	<CENTER>
		<input type="hidden" name="SELECTED_YEAR" value=""/>
		<input type="hidden" name="SELECTED_MONTH" value=""/>
		<input type="hidden" name="CHOSEN_DATE" value=""/>
		<input type="hidden" name="intlFlag" value=""/>
		<input type="hidden" name="deliveryDateDisplay" value=""/>
        <input type="hidden" name="city" value=""/>
        <input type="hidden" name="state" value=""/>
		<input type="hidden" name="recipientId" value=""/>
		<input type="hidden" name="recipientFirstName" value=""/>
		<input type="hidden" name="recipientLastName" value=""/>
		<input type="hidden" name="recipientAddress1" value=""/>
		<input type="hidden" name="recipientAddress2" value=""/>
		<input type="hidden" name="recipientPhoneExt" value=""/>
                <input type="hidden" name="recipientAddressType" value=""/>
                <input type="hidden" name="recipientBhfInfo" value=""/>
                <input type="hidden" name="recipientBhfName" value=""/>
		<input type="hidden" name="search" value=""/>
		<input type="hidden" name="MAXDeliveryDaysOut" value="false"/>

		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
				<TD align="right"> <IMG src="../images/headerFAQ.gif"  onclick="javascript:doFAQ()"/> </TD>
			</TR>

			<TR> <TD colspan="2"> <HR> </HR> </TD> </TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD width="65%"> &nbsp; </TD>
				<TD align="right" valign="top"><A href="javascript:doShoppingCart()" STYLE="text-decoration:none" tabindex="-1" ><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A> </TD>
				<TD align="right" valign="top"><A href="javascript:doCancelOrder()" STYLE="text-decoration:none" tabindex="-1" >Cancel Order</A> </TD>
			</TR>
			<TR>
				<TD colspan="3"><xsl:call-template name="customerHeader"/> &nbsp; </TD>
			</TR>
		</TABLE>

		<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD class="tblheader" colspan="8">
					&nbsp;Occasion
        		</TD>
     		</TR>
 		</TABLE>

    	<!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='OCCASION']/@scriptText"/>
							</TD>
						</TR>

						<TR>
							<TD width="20%" class="label"> Occasion: &nbsp; </TD>
							<TD width="45%">
								<SPAN id="occasionsDIV">
									<SELECT name="occasions" tabindex="1">
										<OPTION value="default"> Please Select Occasion  </OPTION>
											<xsl:for-each select="/root/occasionData/occasions/occasion">
												<OPTION value="{@occasionId}"> <xsl:value-of select="@description"/>  </OPTION>
											</xsl:for-each>
									</SELECT>
									&nbsp; <SPAN style="color:red"> *** </SPAN>
								</SPAN>
							</TD>
							<TD class="instruction">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='OCCASION']/@instructionText"/>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD colspan="3" class="ScreenPrompt">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='PHONENUMBER']/@scriptText"/>
							</TD>
						</TR>

						<TR>
							<TD class="label"> Recipient Phone Number: &nbsp; </TD>
							<TD >
								<INPUT tabindex ="2" type="text" name="recipientPhone" size="20" maxlength="20" onFocus="javascript:fieldFocus('recipientPhone')" onblur="javascript:fieldBlur('recipientPhone')"/> &nbsp;&nbsp;
								<!--span class="labelright">Ext.</span>
								<INPUT tabindex ="19" type="text" name="recipientPhoneExt" size="8" maxlength="10" value="" onFocus="javascript:fieldFocus('recipientPhoneExt')" onblur="javascript:fieldBlur('recipientPhoneExt')"/--> &nbsp;&nbsp;
								<a tabindex ="3" href="javascript:openRecipientLookup()">Search</a>
							</TD>
							<TD width="40%" class="Instruction" valign="top">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='PHONENUMBER']/@instructionText"/>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='COUNTRY']/@scriptText"/>
							</TD>
						</TR>

						<TR>
							<TD class="label"> Recipient Country: &nbsp; </TD>
							<TD>
							 	<SPAN id="countryDIV">
									<SELECT name="countries" onchange="changeOfCountry()" tabindex="4">
										<xsl:for-each select="/root/occasionData/countries/country">
											<OPTION value="{@countryId}"> <xsl:value-of select="@name"/> </OPTION>
										</xsl:for-each>
									</SELECT>
								</SPAN>
							</TD>

							<TD class="instruction">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='COUNTRY']/@instructionText"/>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='ZIPCODE']/@scriptText"/>
							</TD>
						</TR>
						<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
							<TR>
								<TD> &nbsp; </TD>
								<TD class="Instruction">
									You must click the button below to check for valid delivery dates of the selected zip/postal code.
								</TD>
							</TR>
						</xsl:if>
						<TR>
							<TD nowrap="true" width="20%" class="label"> Recipient Zip/Postal Code: &nbsp; </TD>
							<TD nowrap="true" width="30%" >
								<INPUT onkeydown="javascript:EnterToRefresh()" type="text" name="zipCode" size="9" maxlength="9" tabindex="5"  onkeyup="disableContinue();javascript:enableClickToAccept()" onFocus="javascript:fieldFocus('zipCode')" onblur="javascript:fieldBlur('zipCode')"/> &nbsp;&nbsp;
								<xsl:if test="/root/selectedData/data[@name='countryType']/@value='D' or (/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/selectedData/data[@name='countryType']/@value = 'I')">
									<IMG onkeydown="javascript:EnterToRefresh()" tabindex="6" name="clickToAcceptButton" src="../images/button_click_to_accept_disabled.gif" onclick="checkForInternational('false');javascript:doRefresh()" />
								</xsl:if>
								<SPAN style="color:red"> *** </SPAN>
								<BR></BR>
								<A href="javascript:openZipCodeLookup()" tabindex="7"> Lookup by City </A>
							</TD>
							<TD class="instruction">
									<script>
										<![CDATA[
											document.write(deCode("]]> <xsl:value-of select="/root/occasionData/scripting/script[@fieldName='ZIPCODE']/@instructionText"/> <![CDATA["));
										]]>
									</script>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='DELIVERYDATE']/@scriptText"/>
							</TD>
						</TR>

						<TR>
							<TD> &nbsp; </TD>
							<TD colspan="2">
								<DIV id="invalidDeliveryDate" style="visibility: hidden">
									<FONT color="red"> *** The previously selected delivery date is not valid for the entered zip/postal code *** </FONT>
								</DIV>
							</TD>
						</TR>

						<TR>
							<TD class="label" valign="center"> Delivery Date: &nbsp; </TD>
							<TD valign="center">
								<SPAN id="deliveryDateDIV">
									<SELECT onkeydown="javascript:onKeyDown()" name="deliveryDates" tabindex="8" onchange="javascript: deliveryDateChanged();">
										<option value=""></option>
										<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
											<OPTION value="{@date}"> <xsl:value-of select="@dayOfWeek"/>&nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
										</xsl:for-each>
									</SELECT>
								</SPAN>
								&nbsp;&nbsp;
								<IMG src="../images/show-calendar.gif" tabindex="9" onclick="javascript:openCalendarPopup();"></IMG>&nbsp;&nbsp;
	<xsl:if test="/root/selectedData/data[@name='deliveryDaysOut']/@value = 'MIN'">
		<input type ="button" tabindex="10" name="getMaxDeliveryDate" value="Get Max" onclick="javascript:doMAXDeliveryDaysOut();" />
	</xsl:if>
	<xsl:if test="/root/selectedData/data[@name='deliveryDaysOut']/@value = 'MAX'">
		<input type ="button" tabindex="10" name="getMaxDeliveryDate" value="Get Max" disabled="true"/>
	</xsl:if>
							</TD>

							<TD class="instruction">
								<xsl:value-of select="/root/occasionData/scripting/script[@fieldName='DELIVERYDATE']/@instructionText"/>
							</TD>
						</TR>

						<TR> <TD colspan="3"> &nbsp; </TD> </TR>

						<TR>
							<TD colspan="3" align="right">
								<IMG onkeydown="javascript:onKeyDown()" name="continueButton" tabindex="11" src="../images/button_continue.gif" onclick="submitForm()"/> &nbsp;&nbsp;
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
	 	</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD class="tblheader"> &nbsp;Occasion </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>

		<INPUT type="hidden" name="occasionDescription" value=""/>
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
	</CENTER>


<!--Begin update DIV -->
<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
	<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
</DIV>
<!--End update DIV -->

<!--Begin No Floral DIV -->
<DIV id="noFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						Floral Items can no longer be delivered to this Zip/Postal Code.
						<BR></BR>
						<BR></BR>
					    Would you be interested in our specialty items?
						<BR></BR>
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<INPUT type="image" src="../images/button_no.gif" onclick="javascript:noFloral('N'); return false;"></INPUT>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<INPUT type="image" src="../images/button_yes.gif" onclick="javascript:noFloral('Y'); return false;"></INPUT>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Floral DIV -->

<!--Begin No Product DIV -->
<DIV id="noProduct" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						No Items can be delivered to this Zip/Postal Code at this time.
						<BR></BR>
						<BR></BR>
					    Please enter a new Zip/Postal Code.
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<INPUT type="image" src="../images/button_ok.gif" onclick="javascript:noProduct(); return false;"></INPUT>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Product DIV -->

<!--Begin Floral only DIV -->
<DIV id="onlyFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						Only Floral items can be delivered to this Zip/Postal Code.
						<BR></BR>
						<BR></BR>
						<BR></BR>
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<INPUT type="image" src="../images/button_close.gif" onclick="javascript:onlyFloral(); return false;"></INPUT>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Only Floral DIV -->

<!--Begin JCP Popup DIV -->
<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						At this time, JC Penney accepts orders going to or coming from the 50 United States
						only.  We can process your Order using any major credit card through FTD.com.
						<BR></BR>
						<BR></BR>
					    Would you like to proceed?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N'); return false;"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y'); return false;"/>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPPop DIV -->


<!--Begin zip code DIV -->
<DIV id="zipCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
	<CENTER>
	<DIV id="zipCodeLookupSearching" style="VISIBILITY: hidden;">
		<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
	</DIV>
	</CENTER>

	<TABLE width="98%" cellpadding="2" cellspacing="2">
		<TR>
			<TD nowrap="true" colspan="2" align="left">
				<font face="Verdana"><strong>Zip/Postal Code Lookup </strong></font>
			</TD>
		</TR>

		<TR>
            <TD width="50%" class="label" align="right"> City: </TD>
            <TD width="50%">
                <INPUT tabindex="11" onkeydown="javascript:EnterToZip()" class="tblText" type="text" name="cityInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"/>
            </TD>
        </TR>

		<xsl:choose>
			<xsl:when test="/root/selectedData/data[@name='country']/@value = 'US'">
			<TR>
	        	<TD class="label" align="right"> State: </TD>
            	<TD>
            		<select tabindex="12" onkeydown="javascript:EnterToZip()" class="tblText" name="stateInput">
						<xsl:for-each select="/root/occasionData/states/state[@countryCode='']">
							<OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/></OPTION>
						</xsl:for-each>
					</select> &nbsp;&nbsp;
            	</TD>
   			</TR>
			</xsl:when>
			<xsl:when test="/root/selectedData/data[@name='country']/@value = 'CA'">
			<TR>
		        <TD class="label" align="right"> State: </TD>
            	<TD>
	            	<select tabindex="13" class="tblText" name="stateInput">
						<xsl:for-each select="/root/occasionData/states/state[@countryCode='CAN']">
							<OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/></OPTION>
						</xsl:for-each>
					</select> &nbsp;&nbsp;
            	</TD>
        	</TR>
			</xsl:when>
			<xsl:otherwise>
			<TR>
	        	<TD tabindex="14" class="label" align="right"> State: </TD>
	            <TD>
	                <INPUT onkeydown="javascript:EnterToZip()" class="tblText" type="text" name="stateInput" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"/>
	            </TD>
        	</TR>
			</xsl:otherwise>
		</xsl:choose>
		<TR>
			<TD nowrap="true" class="labelright"> Zip/Postal Code: </TD>
			<TD>
                <INPUT tabindex="15" onkeydown="javascript:EnterToZip()" type="text" name="zipCodeInput" size="5" maxlength="10" onFocus="javascript:fieldFocus('zipCodeInput')" onblur="javascript:fieldBlur('zipCodeInput')"/>
			</TD>
		</TR>
        <TR>
        	<TD> &nbsp; </TD>
			<TD colspan="2" align="right">
				<INPUT tabindex="16" onkeydown="javascript:EnterToZip();" type="image" src="../images/button_search.gif" alt="Search" onclick="javascript:goSearchZipCode(); return false;"  />
 	           	<INPUT tabindex="17" type="image" src="../images/button_close.gif" alt="Close screen" onclick="javascript:goCancelZipCode();return false;" />
         	</TD>
        </TR>
        <TR>
            <TD colspan="2"></TD>
        </TR>
    </TABLE>
</DIV>
<!--End zip code DIV -->

<!--Begin Recipient Lookup DIV-->
<DIV id="recipientLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">

<CENTER>
<DIV id="recipientLookupSearching" style="VISIBILITY: hidden;">
	<FONT face="Verdana" color="#FF0000">Please wait ... searching!</FONT>
</DIV>
</CENTER>
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="2">
		<TD WIDTH="100%" VALIGN="top">
			<H1 align="CENTER"> Recipient Lookup </H1>
				<CENTER>
					<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">
						<TD>
							<TABLE  width="95%" border="0" cellpadding="1" cellspacing="1">
						   		<TR>
									<TD nowrap="true" class="labelright"> Phone Number:  </TD>
									<TD width="50%">
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="21" name="phoneInput" class="TblText" maxlength="20" size="15" type="text" onFocus="javascript:fieldFocus('phoneInput')" onblur="javascript:fieldBlur('phoneInput')"></INPUT>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TABLE>
				</CENTER>
  			</TD>
	        <TR>
				<TD colspan="3" align="right">
					<IMG onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchRecipient()"/>
 	            	<IMG onkeydown="javascript:closeRecipientLookup()" tabindex ="27" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelRecipient()"/>
	         	</TD>
        	</TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	</TABLE>
</DIV>
<!--End Recipient Lookup DIV-->

</FORM>	

	<DIV id="calendarLoading" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... loading!</font> </p>
	</DIV>

<!--
	SERVER SIDE VALIDATION SECTION
-->

	<xsl:call-template name="validation"/>
		
</BODY>

<SCRIPT language="JavaScript">

<![CDATA[

	function EnterToRecipientPopup()
   	{
   		if (window.event.keyCode == 13)
   		{
   		    openRecipientPopup();
   		}
   	}

	function openZipCodeLookup()
	{
		//Hide the dropdows
		hideDIVs();

		//In case its opened a second time
		document.all.stateInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
		HEIGHT = 100;
		WIDTH = 290;

//		popupFlag=true;
		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		zipcodesearch = (dom)?document.getElementById("zipCodeLookup").style : ie? document.all.zipCodeLookup : document.sourceCodeLookup
		//GET THE CURRENT SCROLL
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset

		zipcodesearch.width = WIDTH
		zipcodesearch.height= HEIGHT

		zipcodesearch.top= scroll_top + document.body.clientHeight/2 - HEIGHT/2
		zipcodesearch.left=document.body.clientWidth/2 -  WIDTH/2

		zipcodesearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the input and set the focus
		document.all.zipCodeInput.value = document.all.zipCode.value;
		document.all.cityInput.focus();

		if(document.all.countries.value == 'US' || document.all.countries.value == 'CA')
		{
			changeInputCountry();
		}

	}

	function goCancelZipCode()
	{
//	    popupFlag=true;
	    zipcodesearch.visibility= "hidden";
	    showDIVs();
	    return false;
	}

	function goSearchZipCode()
	{
//		popupFlag=true;
		openZipCodePopup();
	}

	function openZipCodePopup()
	{
		//Show the dropdowns
		showDIVs();

		//initialize the background colors
		document.all.cityInput.style.backgroundColor='white';
		document.all.stateInput.style.backgroundColor='white';
		document.all.zipCodeInput.style.backgroundColor='white';

		//First validate the inputs
		check = true;

        state = stripWhitespace(document.all.stateInput.value);
        city = stripWhitespace(document.all.cityInput.value);
        zip = stripWhitespace(document.all.zipCodeInput.value);

        //State is required if zip is empty
        if((state.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.stateInput.focus();
		 	  check = false;
			}
			  document.all.stateInput.style.backgroundColor='pink';
		} // close state if

		//City is required if zip is empty
        if((city.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.cityInput.focus();
		 	  check = false;
			}
			  document.all.cityInput.style.backgroundColor='pink';
		} // close state if

		if (!check)
		{
			alert("Please correct the marked fields")
		 	return false;
		}

        //Now that everything is valid, open the popup after removing special characters
        bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

    	var cityVal = stripCharsInBag(document.all.cityInput.value, bag2);
    	var stateVal = stripCharsInBag(document.all.stateInput.value, bag2);
    	var zipVal = stripCharsInBag(document.all.zipCodeInput.value, bag2);

		var countryVal = document.all.countries.value;

		document.all.zipCodeLookupSearching.style.visibility = "visible";

		var form = document.forms[0];
	    var url_source="PopupServlet?POPUP_ID=LOOKUP_ZIP_CODE" +
	    "&cityInput=" + cityVal +
	    "&stateInput=" + stateVal +
	    "&zipCodeInput=" + zipVal +
	    "&countryVal=" + countryVal;

	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	    //Open the window
 	    var ret = window.showModalDialog(url_source,"", modal_dim);
 	    //in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 			ret[1] = '';
 		}

		if(ret[1] != null)
		{
			if(document.all.zipCode.value != ret[1])
			{
				//Enable the Click to Select button and disable the continue button
				enableClickToAccept();
				disableContinue();
			}

			//only change the zip if a different zip is selected
			if (document.all.zipCode.value != ret[1])
			{
				document.all.zipCode.value = ret[1];
			}
 			document.all.city.value = ret[0];
			document.all.state.value = ret[2];

		}


		//focus on the click to accept button
		document.all.clickToAcceptButton.focus()

		zipcodesearch.visibility= "hidden";
		document.all.zipCodeLookupSearching.style.visibility = "hidden";
	}
	function changeInputCountry()

	{

		if(document.all.countries.value == 'US')
		{
			  USInputStateDropDown();


		} else {
			if(document.all.countries.value == 'CA')
			{
				CANInputStateDropDown();
			} else {
				USInputStateDropDown();
			}
		}
	}
	function USInputStateDropDown()
	{
		counter = 0;
		document.all.stateInput.options.length = 0;
		document.all.stateInput.options[counter++] = new Option("", "", false, false);

]]>
		<xsl:for-each select="/root/occasionData/states/state">
			<xsl:if test="@countryCode =''">
			var stateID = "<xsl:value-of select="@stateMasterId"/>";
			var stateName = "<xsl:value-of select="@stateName"/>";


<![CDATA[
			document.all.stateInput.options[counter++] = new Option(stateName, stateID, false, false);
]]>
			</xsl:if>
		</xsl:for-each>
<![CDATA[

	}

	function CANInputStateDropDown()
	{

		counter = 0;
		document.all.stateInput.options.length = 0;
		document.all.stateInput.options[counter++] = new Option("", "", false, false);

]]>
		<xsl:for-each select="/root/occasionData/states/state">
			<xsl:if test="@countryCode ='CAN'">
			var stateID = "<xsl:value-of select="@stateMasterId"/>";
			var stateName = "<xsl:value-of select="@stateName"/>";


<![CDATA[
				document.all.stateInput.options[counter++] = new Option(stateName, stateID, false, false);

]]>
			</xsl:if>
		</xsl:for-each>
<![CDATA[


	}

	function openRecipientLookup()
	{

		//hide the dropdowns
		hideDIVs()

		//In case the DIV is opened a second time
		document.forms[0].phoneInput.style.backgroundColor='white';


		var ie=document.all;
		var dom=document.getElementById;
		var ns4=document.layers;
		HEIGHT = 100;
		WIDTH = 290;

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155;
		recipientsearch = (dom)?document.getElementById("recipientLookup").style : ie? document.all.recipientLookup : document.recipientLookup;
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset;

		recipientsearch.width = WIDTH;
		recipientsearch.height = HEIGHT;

		recipientsearch.left=document.body.clientWidth/2 - WIDTH/2;
		recipientsearch.top=scroll_top + document.body.clientHeight/2 - HEIGHT/2;

		recipientsearch.visibility=(dom||ie)? "visible" : "show";

		//Populate inputs in the DIV if they are given on the main page
		document.all.phoneInput.value = document.all.recipientPhone.value;

		document.all.cityInput.value = document.all.city.value;

		document.all.zipCodeInput.value = document.all.zipCode.value;

		document.forms[0].phoneInput.focus();
	}

	function goCancelRecipient()
	{
		recipientsearch.visibility= "hidden";
		showDIVs()
	}

	function goSearchRecipient()
	{
		openRecipientPopup();
	}

	function openRecipientPopup()
	{
		//show the dropdowns
		showDIVs()

		//initialize the background colors
        document.forms[0].phoneInput.style.backgroundColor='white';
        document.forms[0].zipCodeInput.style.backgroundColor='white';

	    //First Validate that the DIV inputs are valid
        phone = stripWhitespace(document.forms[0].phoneInput.value);
		zip = stripWhitespace(document.forms[0].zipCodeInput.value);
   	 	//countryType = stripWhitespace(document.forms[0].countryType.value);

   	 	if (phone.length == 0)
	 	{
	 		alert("Please correct the marked fields");
	 		document.forms[0].phoneInput.style.backgroundColor='pink';
	 		document.forms[0].phoneInput.focus();
			return false;
	 	}

		//Every is valid, so the popup can be opened
		document.all.recipientLookupSearching.style.visibility = "visible";

		var form = document.forms[0];     		
     	var url_source="PopupServlet?POPUP_ID=LOOKUP_RECIPIENT" + "&phoneInput=" + document.all.phoneInput.value;	
	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	    //Open the popup
 	     var ret = window.showModalDialog(url_source,"" , modal_dim);

		//in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 	     	ret[0] = '';
 		}

 		//Populate the text boxes if the ret value has something in it
		if (ret[0] != '')
 		{
 			document.all.recipientId.value = ret[0];
			document.all.recipientFirstName.value = ret[1];
			document.all.recipientLastName.value = ret[2];
			document.all.recipientAddress1.value = ret[3];
			document.all.recipientAddress2.value = ret[4];
			document.all.city.value = ret[5];
			document.all.state.value = ret[6];

			var origZip = document.all.zipCode.value;
			zip = ret[7];
			var cntryAbbr = ret[8];
			if( cntryAbbr=="US" )			//Grab the first 5 digits of the zip code
				zip5Digit = zip.substring(0,5);
			else if( cntryAbbr=="CA" )		//Grab the first 6 digits of the zip code	
				zip5Digit = zip.substring(0,6);
			else						//Use all of the zip code
				zip5Digit = zip; 
				
			document.all.zipCode.value = zip5Digit;
			document.all.countries.value = ret[8];
			
			if ( ret[9] != '' )
			{
				document.all.recipientPhone.value = ret[9];
	        	}
			else
			{
				document.all.recipientPhone.value = ret[10];
				document.all.recipientPhoneExt.value = ret[11];
			}
                        document.all.recipientAddressType.value = ret[14];
                        document.all.recipientBhfInfo.value = ret[15];
                        document.all.recipientBhfName.value = ret[13];
			]]>
			<![CDATA[

			if(origZip != document.all.zipCode.value)
			{
				enableClickToAccept();
				disableContinue();
				//set the focus on the next field
				document.all.clickToAcceptButton.focus();				
			}
			else
			{
				//set the focus on the next field
				document.all.deliveryDates.focus();
			}
		}

		recipientsearch.visibility= "hidden";
		document.all.recipientLookupSearching.style.visibility = "hidden";
}
]]>
</SCRIPT>

</HTML>

</xsl:template>
</xsl:stylesheet>
