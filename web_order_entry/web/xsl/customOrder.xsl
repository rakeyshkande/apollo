<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="headerButtons.xsl"/>
<xsl:import href="footer.xsl"/>

<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="cartItemNumber" select="root/parameters/cartItemNumber"/>

<!-- These variables are defined to remove carriage returns from textareas -->
<xsl:variable name="itemComments" select="normalize-space(/root/item/COMMENT/@item)" />
<xsl:variable name="floristComments" select="normalize-space(/root/item/COMMENT/@florist)" />

<xsl:template match="/root">

<xsl:call-template name="header"></xsl:call-template>

<HTML>
<HEAD>
	<TITLE> FTD - Custom Order </TITLE>

	<script language="javascript">

		var sessionId = "<xsl:value-of select="$sessionId"/>";
		var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
		var itemNumber = "<xsl:value-of select="$cartItemNumber"/>";
		var itemComments = "<xsl:value-of select="$itemComments"/>";
		var floristComments = "<xsl:value-of select="$floristComments"/>";
		var variablePrice = "<xsl:value-of select="/root/item/DETAIL/@variablePrice"/>";
		var variable = "<xsl:value-of select="/root/item/DETAIL/@variable"/>";
		var variablePriceMax = "<xsl:value-of select="/root/productList/products/product/@variablePriceMax"/>";
		var variablePriceMin = "<xsl:value-of select="/root/productList/products/product/@standardPrice"/>";

	</script>

	<script language="javascript" src="../js/FormChek.js"/>
	<script language="javascript" src="../js/util.js"/>

	<SCRIPT language="javascript" >
	<![CDATA[


   	function checkFloristCommentsSize(){
	/***  This function will limited to length of the gift message to 240
	*/
   	var availableFieldLength = 240;
	if(document.forms[0].desc.value.length > availableFieldLength)
	{
		document.forms[0].desc.value = document.forms[0].desc.value.substring(0,(availableFieldLength));
	}

	}
   	function checkCommentsSize(){
	/***  This function will limited to length of the order Comments to 240
	*/
   	var availableFieldLength = 240;
	if(document.forms[0].comments.value.length > availableFieldLength)
	{
		document.forms[0].comments.value = document.forms[0].comments.value.substring(0,(availableFieldLength));
	}

	}


		function onKeyDown()
		{
	    	    if (window.event.keyCode == 13)
			{
			    submitForm();
			}
		}


		function openUpdateWin()
		{
			var ie=document.all
			var dom=document.getElementById
			var ns4=document.layers

			cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
			updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
			scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
			updateWinV.top=scroll_top+document.body.clientHeight/2-100

			updateWinV.width=290
			updateWinV.height=100
			updateWinV.left=document.body.clientWidth/2 - 100
			updateWinV.top=document.body.clientHeight/2 - 50;
			updateWinV.visibility=(dom||ie)? "visible" : "show"
		}

		function deCode(strSelection)
		{
			strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
			strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

			return strSelection;
		}

		function init()
  		{
  			document.all.cartItemNumber.value = itemNumber;
  			document.all.comments.value = itemComments;
  			document.all.desc.value = floristComments;
  			document.all.VARIABLE_PRICE_AMT.value = variablePrice;

  			if ( variablePrice != "" )
  			{
				var size = document.all.PRICE.length;
				for (i=0; i<size; i++)
				{
					if ( document.all.PRICE[i].value == "variable" )
						document.all.PRICE[i].checked = true;
				}
  			}

  			document.all.VARIABLE_PRICE_AMT.focus();

   	    }

		function resetRadio( value )
		{
			var size = document.all.PRICE.length;
			for (i=0; i<size; i++) {
				if ( document.all.PRICE[i].value == "variable" )
					document.all.PRICE[i].checked = true;
			}
		}

		function isValidUSDollar( dollar )
		{
			if ( dollar == null || dollar == "" )
				return false;

			first = dollar.indexOf(".");

			if ( first != -1 )
			{
				dollar_length = dollar.length;

				firstSub = dollar.substr( 0, first );
				secondSub = dollar.substr( first + 1, dollar_length - first );

				if ( ( dollar_length - first ) == 1 )
					return isInteger( firstSub );
				else
					return ( isInteger( firstSub ) && isInteger( secondSub ) );

			} else {
				return isInteger( dollar );
			}

			return false;
		}


		function validateVariablePrice()
		{
			var field = document.all.VARIABLE_PRICE_AMT;

			//Check to make sure the VARIABLE_PRICE_AMT is valid US dollar
			checked = isValidUSDollar ( field.value );


			if ( parseFloat(field.value) < parseFloat(variablePriceMin) || parseFloat(field.value) > parseFloat(variablePriceMax ) )
			{
				alert("Varible price range: " + variablePriceMin + " - " + variablePriceMax);
				checked = false;
		  	}


		  	if( !checked )
		  	{
	   			field.focus();
		   		field.style.backgroundColor='pink';
		   		checked = false;
		  	}


]]>		  	<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
<![CDATA[

		  			var decpos = (field.value).indexOf(".")
		  			if(decpos == "-1" || (decpos >= ((field.value).length - 2)))
		  			{
		  				checked = false;
		  				field.focus();
					   	field.style.backgroundColor='pink';

		  				alert("JC Penny requires price to end with .99");
		  			} else {

		  				if((field.value).substr(decpos, 3) != ".99")
		  				{
		  					checked = false;
		  					field.focus();
					   		field.style.backgroundColor='pink';

		  					alert("JC Penny requires price to end with .99");
		  				}
		  			}
]]>
			</xsl:if>



<![CDATA[



		    return checked;
		 }

		 function checkVariablePrice( value )
		 {
		 	 if ( value != "" )
		 	 {
		 	 	var v = document.forms[0].VARIABLE_PRICE_AMT.value= "";
		 	 	document.forms[0].VARIABLE_PRICE_AMT.style.backgroundColor = "white";
			 }
		 }


 		 function validateForm()
		 {
	       	      	  checked = true;
    /***
    *   Check price
    */

  	     	  size = document.all.PRICE.length;
			  for (i=0; i<size; i++)
			  {
			  	if (document.forms[0].PRICE[i].checked && document.forms[0].PRICE[i].value == "variable" )
			  	{
			  		checked = validateVariablePrice();
		  		}
			  }
	 /***
    *   Check Description if it exist
    */
    document.forms[0].desc.className="TblText";
     	var a1nws = stripWhitespace(document.forms[0].desc.value);

	if ((a1nws.length == 0))
    	{
    		if (checked == true)
    		{
    			document.forms[0].desc.focus();
    			checked = false;
   			}
    		document.forms[0].desc.className="Error";
    	}

			  return checked;
			  
		 }

		 function submitForm()
		 {
		 	 if (validateForm())
		 	 {
		 		openUpdateWin();
		  		form.submit();
		  	 }
			 else
			 {
			 	alert("Please correct the marked fields.");
			 	return false;
  	   	  	 }
		 }
	]]>
	</SCRIPT>

	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onLoad="init()">
	<FORM name="form" method="post" action="{$actionServlet}" >
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
		<input type="hidden" name="productId" >

			<xsl:attribute name="value"><xsl:value-of select="/root/productList/products/product/@productID"/></xsl:attribute>
		</input>
		<input type="hidden" name="custom" value="Y" />
		<input type="hidden" name="command" value="updateItem" />
		<input type="hidden" name="cartItemNumber" />
		<CENTER>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
		    	<TD width="70%"> <!-- <xsl:call-template name="customerHeader"/> --> &nbsp; </TD>
				<TD valign="center" align="right" nowrap="">
					<xsl:choose>
						<xsl:when test="$cartItemNumber=''">
    	        			<A tabindex="-1" href="javascript:doShoppingCart();" STYLE="text-decoration:none">
			    			<IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A>
			    		</xsl:when>
			    		<xsl:otherwise>
			    			<IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/><A tabindex="-1"><font color="#C0C0C0">Shopping Cart</font></A>
						</xsl:otherwise>
					</xsl:choose>
        		</TD>
				<TD valign="center" align="right" nowrap="">
            		<A tabindex="-1" href="javascript:doCancelOrder();" STYLE="text-decoration:none">Cancel Order</A>
        		</TD>
			</TR>
			<TR>
				<TD colspan="4"> &nbsp; </TD>
			</TR>
			<TR>
				<TD colspan="4" class="tblheader">
					<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					<xsl:variable name="customItem" select="/root/previousPages/previousPage[@name = 'Custom Item']/@value"/>
					<xsl:variable name="categories" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
					&nbsp;
					<a href="{$occasionlbl}" class="tblheader"  tabindex="-1">Occasion</a>&nbsp;>
					<a href="{$categories}" class="tblheader" tabindex="-1">Category</a>&nbsp;>
					Custom Item
    			</TD>
			</TR>
		</TABLE>

	    <!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<script><![CDATA[
									document.write(deCode("]]> <xsl:value-of select="extraInfo/scripting/script[@fieldName='PRICE']/@scriptText"/>	 <![CDATA["));
								]]></script>
							</TD>
						</TR>


						<!-- for the price selection -->
							<xsl:if test="/root/productList/products/product/@standardPrice > 0">
							 <tr>
								<TD class="labelright"> Price: &nbsp; </TD>
								<TD align="left">

									<INPUT tabindex="1" type="radio" checked="true" onclick="checkVariablePrice(value);" name="PRICE" value="standard" />

										$<xsl:value-of select="/root/productList/products/product/@standardDiscountPrice"/>

								</TD>
								<TD class="instruction">&nbsp;   </TD>
							</tr>
							</xsl:if>
							<tr>
								<TD class="labelright"> &nbsp; </TD>
								<TD align="left">
									<INPUT type="radio" onclick="checkVariablePrice(value);" name="PRICE" value="variable">
									</INPUT> Variable Price
									&nbsp;&nbsp; $<input tabindex="2" type="text" onselect="resetRadio(value);"  onkeypress="resetRadio(value);" onclick="resetRadio(value);" name="VARIABLE_PRICE_AMT" value="" size="8" maxlength="8" class="tblText" onFocus="javascript:fieldFocus('VARIABLE_PRICE_AMT')" onblur="javascript:fieldBlur('VARIABLE_PRICE_AMT')"></input>
								</TD>
								<TD class="instruction">&nbsp;   </TD>
							</tr>

						<TR> <TD colspan="3">&nbsp;  </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="scripting/script[@FIELD_NAME='Description']/@SCRIPT_TEXT"/>
							</TD>
						</TR>

						<TR>
							<TD width="20%" class="labelright" valign="top"> Custom Order Description: &nbsp; </TD>
							<TD width="30%">
								<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
									<TR>
										<TD width="95%" valign="top">
											<TEXTAREA tabindex="3" name="desc" rows="3" cols="80" onkeyup="javascript:checkFloristCommentsSize();" onFocus="javascript:fieldFocus('desc')" onblur="javascript:fieldBlur('desc')"></TEXTAREA>
										</TD>
										<TD width="5%" valign="top">
											&nbsp; <SPAN style="color:red"> *** </SPAN>
										</TD>
									</TR>
								</TABLE>
							</TD>
							<TD width="50%">&nbsp;  </TD>
						</TR>

						<TR> <TD colspan="3" valign="top"> &nbsp; </TD> </TR>

						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="scripting/script[@FIELD_NAME='Comment']/@SCRIPT_TEXT"/>
							</TD>
						</TR>

						<TR>
							<TD width="20%" class="labelright" valign="top"> Order Comments: &nbsp; </TD>
							<TD width="30%" valign="top">
								<TEXTAREA onkeydown="javascript:onKeyDown()" tabindex="4" name="comments" onkeyup="javascript:checkCommentsSize();" rows="3" cols="80" onFocus="javascript:fieldFocus('comments')" onblur="javascript:fieldBlur('comments')"></TEXTAREA>
							</TD>
							<TD width="50%" valign="top">&nbsp;  </TD>
						</TR>

						<TR> <TD colspan="3">&nbsp;  </TD> </TR>

						<TR>
							<TD colspan="3" align="right">
								<IMG onkeydown="javascript:onKeyDown()" src="../images/button_continue.gif" onclick="javascript:submitForm(); " tabindex="5"/> &nbsp;&nbsp;
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<TD colspan="4" class="tblheader">
					<xsl:variable name="occasionlblbottom" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
					<xsl:variable name="customItembottom" select="/root/previousPages/previousPage[@name = 'Custom Item']/@value"/>
					<xsl:variable name="categoriesbottom" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
					&nbsp;
					<a href="{$occasionlblbottom}" class="tblheader" tabindex="-1">Occasion</a>&nbsp;>
					<a href="{$categoriesbottom}" class="tblheader" tabindex="-1">Category</a>&nbsp;>
					Custom Item
    			</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>
		</CENTER>
	</FORM>

	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... Continue!</font> </p>
	</DIV>
</BODY>
</HTML>

</xsl:template>
</xsl:stylesheet>
