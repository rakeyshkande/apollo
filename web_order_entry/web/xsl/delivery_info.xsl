<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY reg "&#174;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:import href="../xsl/headerButtons.xsl"/>
<xsl:import href="../xsl/footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="breadCrumbType" select="root/parameters/breadCrumbType"/>
<xsl:variable name="cancelItemServlet" select="root/parameters/cancelItemServlet"/>
<xsl:variable name="cancelItemShoppingServlet" select="root/parameters/cancelItemShoppingServlet"/>
<xsl:variable name="searchType" select="root/parameters/searchType"/>
<xsl:variable name="upsellFlag" select="root/parameters/upsellFlag"/>
<xsl:variable name="focusElement" select="root/parameters/focusElement"/>

<xsl:variable name="shippingNextDay" select="/root/shippingData/shippingMethods/shippingMethod/method[@code='ND']/@code"/>
<xsl:variable name="shippingTwoDay" select="/root/shippingData/shippingMethods/shippingMethod/method[@code='2D']/@code"/>
<xsl:variable name="shippingStandard" select="/root/shippingData/shippingMethods/shippingMethod/method[@code='GR']/@code"/>

<!-- These variables are defined to remove carriage returns from textareas -->
<xsl:variable name="giftMessage" select="normalize-space(/root/deliveryDetail/@giftMessage)" />
<xsl:variable name="orderComments" select="normalize-space(/root/deliveryDetail/@orderComments)" />
<xsl:variable name="floristComments" select="normalize-space(/root/deliveryDetail/@floristComments)" />


<xsl:template match="/">
<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>
<SCRIPT language="JavaScript">
	var target = "<xsl:value-of select="$actionServlet"/>";
	var sessionId = '<xsl:value-of select="$sessionId"/>';
	var persistentObjId = '<xsl:value-of select="$persistentObjId"/>';
	var bypassZipChange = false;
	var disableFlag =false;
	var disableRefreshFlag = true;
	var submitInd = "";
	var validZip = false;
	var backupFlag = false;
	var focusElement = '<xsl:value-of select="$focusElement"/>';
	var productType = '<xsl:value-of select="/root/deliveryData/products/product/@productType"/>';
	var stateCode = '<xsl:value-of select="/root/recipient/@state"/>';


<![CDATA[

	document.onkeydown = backKeyHandler;

	document.oncontextmenu=stopIt;

	function checkFieldLengths()
	{
		document.forms[0].QMSAddress1.value = truncateFieldToMaxLength(document.forms[0].QMSAddress1.value, 80);
		document.forms[0].QMSAddress2.value = truncateFieldToMaxLength(document.forms[0].QMSAddress2.value, 40);
		document.forms[0].QMSCity.value = truncateFieldToMaxLength(document.forms[0].QMSCity.value, 40)	;
		document.forms[0].QMSState.value = truncateFieldToMaxLength(document.forms[0].QMSState.value, 20);
		document.forms[0].QMSZipCode.value = truncateFieldToMaxLength(document.forms[0].QMSZipCode.value, 10);
		document.forms[0].QMSMatchCode.value = truncateFieldToMaxLength(document.forms[0].QMSMatchCode.value, 20);
		document.forms[0].QMSFirmName.value = truncateFieldToMaxLength(document.forms[0].QMSFirmName.value, 80);
		document.forms[0].QMSLatitude.value = truncateFieldToMaxLength(document.forms[0].QMSLatitude.value, 80)	;
		document.forms[0].QMSLongitude.value = truncateFieldToMaxLength(document.forms[0].QMSLongitude.value, 80);
		document.forms[0].QMSRangeRecordType.value = truncateFieldToMaxLength(document.forms[0].QMSRangeRecordType.value, 80);
		document.forms[0].QMSOverrideFlag.value = truncateFieldToMaxLength(document.forms[0].QMSOverrideFlag.value, 1);

	}

	function backToShopping()
	{
	]]>

		<xsl:choose>
		<xsl:when test= "$upsellFlag = 'Y'">

		var url = "<xsl:value-of select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>";
		<![CDATA[
		document.location = url;
		]]>

		</xsl:when>
		<xsl:otherwise>

		var country = "<xsl:value-of select="/root/recipient/@country"/>";
		var zip = "<xsl:value-of select="/root/recipient/@zip"/>";
		var deliveryDate = "<xsl:value-of select="/root/deliveryDetail/@requestedDeliveryDate"/>";
		var deliveryDateDisplay = "<xsl:value-of select="/root/orderData/data[@name = 'requestedDeliveryDateDisplay']/@value"/>";
		var occasionDescription = "<xsl:value-of select="/root/orderData/data[@name = 'occasionDescription']/@value"/>";
		var city = "<xsl:value-of select="/root/recipient/@city"/>";
		var state = "<xsl:value-of select="/root/recipient/@state"/>";
		var occasion = "<xsl:value-of select="/root/orderData/data[@name = 'occasion']/@value"/>";
		var searchType = "<xsl:value-of select="$searchType"/>";
		var upsellFlag = "<xsl:value-of select="$upsellFlag"/>";
	<![CDATA[

		var url = "CategoryServlet" + "?sessionId=" + sessionId +
		                               "&persistentObjId=" + persistentObjId +
		                               "&countries=" + country +
		                               "&zipCode=" + zip +
		                               "&deliveryDates=" + deliveryDate +
		                               "&deliveryDateDisplay=" + deliveryDateDisplay +
		                               "&occasionDescription=" + occasionDescription +
		                               "&city=" + city +
		                               "&state=" + state +
		                               "&occasions=" + occasion +
		                               "&upsellFlag=" + upsellFlag +
									   "&searchType=" + searchType;
		document.location = url;
		]]>
		</xsl:otherwise>
		</xsl:choose>

		<![CDATA[
	}

	function acceptGnaddUpsell()
	{
	]]>

	var upsellBaseId = "<xsl:value-of select="/root/pageData/data[@name='upsellBaseId']/@value"/>";
	var upsellCrumbFlag = "<xsl:value-of select="$upsellFlag"/>";
	var searchType = "<xsl:value-of select="$searchType"/>";

	<![CDATA[
		var servletCall = "";
		if(searchType == 'simpleproductsearch') {
			var url = "SearchServlet" + "?sessionId=" + sessionId +
				      "&persistentObjId=" + persistentObjId +
				      "&upsellFlag=" + upsellCrumbFlag +
				      "&search=" + searchType +
				      "&productId=" + upsellBaseId +
				      "&soonerOverride=Y";
		}
		else {
			var url = "ProductDetailServlet" + "?sessionId=" + sessionId +
				  	  "&persistentObjId=" + persistentObjId +
				      "&upsellFlag=" + upsellCrumbFlag +
				      "&search=" + searchType +
				      "&productId=" + upsellBaseId +
				      "&soonerOverride=Y";
		}

		document.location = url;
	}

	function backUp()
	{
		window.history.back();
	}


   function hideDIVs()
   {

   	if (document.all.giftMessageDIV)
   		document.all.giftMessageDIV.style.visibility = "hidden";

   	if (document.all.stateDIV)
   		document.all.stateDIV.style.visibility = "hidden";

   	if (document.all.countryDIV)
   		document.all.countryDIV.style.visibility = "hidden";

]]>
	<xsl:if test="/root/shippingData/shippingMethods/shippingMethod/method/@description != ''">
<![CDATA[

   	if (document.all.deliveryDIV)
	   	document.all.deliveryDIV.style.visibility = "hidden";
]]>
	</xsl:if>
<![CDATA[

	if (document.all.locationDIV)
	   	document.all.locationDIV.style.visibility = "hidden";

	if (document.all.serviceDIV)
   		document.all.serviceDIV.style.visibility = "hidden";

   	if (document.all.methodsDIV)
   		document.all.methodsDIV.style.visibility = "hidden";

   }

   function showDIVs()
   {
   	if (document.all.giftMessageDIV)
   		document.all.giftMessageDIV.style.visibility = "visible";

   	if (document.all.stateDIV)
		document.all.stateDIV.style.visibility = "visible";

	if (document.all.countryDIV)
   		document.all.countryDIV.style.visibility = "visible";
]]>
	<xsl:if test="/root/shippingData/shippingMethods/shippingMethod/method/@description != ''">
<![CDATA[
   	if (document.all.deliveryDIV)
   		document.all.deliveryDIV.style.visibility = "visible";
]]>
	</xsl:if>
<![CDATA[

   	if (document.all.locationDIV)
		document.all.locationDIV.style.visibility = "visible";

	if (document.all.serviceDIV)
		document.all.serviceDIV.style.visibility = "visible";

	if (document.all.methodsDIV)
		document.all.methodsDIV.style.visibility = "visible";
   }



   function onKeyDown()
   {
       if (window.event.keyCode == 13)
	{
	    submitForm();
	}
   }

   function EnterToCalendarPopup()
   {
        if (window.event.keyCode == 13)
	{
	    openCalendarPopup();
	}
   }

   function EnterToZipCodeLookup()
   {
   		if (window.event.keyCode == 13)
   		{
   		    openZipCodeLookup();
   		}
	}

   function EnterToAccept()
   {
	 if (window.event.keyCode == 13)
	{
	    //enableContinue();
	    validateZip('zip')
	}
   }

   function EnterToRecipientPopup()
   {
   	if (window.event.keyCode == 13)
   	{
   	    openRecipientPopup();
   	}
   }

   function closeRecipientLookup()
   {
   	if (window.event.keyCode == 13)
   	{
   	    goCancelRecipient();
   	}
   }

   function EnterToFloristPopup()
      {
      	if (window.event.keyCode == 13)
      	{
      	    openFloristPopup();
      	}
      }

   function closeFloristLookup()
      {
      	if (window.event.keyCode == 13)
      	{
      	    goCancelFlorist();
      	}
   }

   function EnterToZipPopup()
   {
      if (window.event.keyCode == 13)
   	{
   	    openZipCodePopup();
   	}
    }

   function closeZipLookup()
   {
          if (window.event.keyCode == 13)
       	{
       	    goCancelZipCode();
       	}
    }


	/*	**************************************************************************
			This group of functions is used to display/hide the Codified Special
			for zip code message box
		**************************************************************************	*/
	function openCodifiedSpecial()
	{
		disableContinue();
		hideDIVs();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		codifiedSpecialV = (dom)?document.getElementById("codifiedSpecial").style : ie? document.all.codifiedSpecial : document.codifiedSpecial
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		codifiedSpecialV.top=scroll_top+document.body.clientHeight/2-100

		codifiedSpecialV.width=290
		codifiedSpecialV.height=100
		codifiedSpecialV.left=document.body.clientWidth/2 - 50
		codifiedSpecialV.visibility=(dom||ie)? "visible" : "show"

		document.all.continueButton.src = "../images/button_back.gif";
		backupFlag = true;
	}

	function closeCodifiedSpecial()
	{
		showDIVs();
		codifiedSpecialV.visibility = "hidden";
		if (document.all.zipCode && document.all.zipCode.enabled == true)
			document.forms[0].zipCode.focus();
	}

	function codifiedSpecial()
	{
		closeCodifiedSpecial();
	}

   function init()
	/***  This function will prepoluate fields
	*/
   	{
	   	bypassZipChange = false;
	   	disableFlag = false;
		disableRefreshFlag = true;

		// Set focus to an element if we have refreshed the page
   		if(focusElement.length > 0 && document.forms[0].elements[focusElement].style.visibility != "hidden")
   		{
   			if(focusElement == "country")
   			{
   				if(document.all.countryDIV.style.visibility != "hidden")
		   		{
		   			document.forms[0].elements[focusElement].focus();
		   		}
   			}
	   		else
	   		{
   				document.forms[0].elements[focusElement].focus();
   			}
		}

	]]>
		document.all.cartItemNumber.value ="<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
	    document.all.countryType.value ="<xsl:value-of select="/root/recipient/@countryType"/>";
		document.all.deliveryLocation.value ="<xsl:value-of select="/root/deliveryDetail/@deliveryLocation"/>";
		document.all.recipientPhone.value ="<xsl:value-of select="/root/recipient/@phone"/>";
	    document.all.recipientPhoneExt.value ="<xsl:value-of select="/root/recipient/@phoneExt"/>";
	   	document.all.recipientFirstName.value ="<xsl:value-of select="/root/recipient/@firstName"/>";
	   	document.all.recipientLastName.value ="<xsl:value-of select="/root/recipient/@lastName"/>";
	   	document.all.recipientAddress1.value = "<xsl:value-of select="/root/recipient/@address1"/>";
	   	document.all.recipientAddress2.value ="<xsl:value-of select="/root/recipient/@address2"/>";
	   	document.all.city.value ="<xsl:value-of select="/root/recipient/@city"/>";
		document.all.upsellFlag.value ="<xsl:value-of select="$upsellFlag"/>";
	   	document.all.searchType.value ="<xsl:value-of select="$searchType"/>";


	   	if(document.all.lastMinuteEmailAddress != null)
	   	{
	   		document.all.lastMinuteEmailAddress.value = "<xsl:value-of select="/root/recipient/@lastMinuteEmailAddress"/>";
	   	}

	   	<xsl:if test="/root/recipient/@lastMinuteCheckBox = 'Y'">
	   		if(document.all.lastMinuteCheckBox != null)
	   		{
	   			document.all.lastMinuteCheckBox.checked = "true"
	   		}
	   	</xsl:if>

		<xsl:choose>
			<xsl:when test="/root/recipient/@country = ''">
				document.all.country.value = 'US';
			</xsl:when>
			<xsl:otherwise>
		   		document.all.country.value ="<xsl:value-of select="/root/recipient/@country"/>";
			</xsl:otherwise>
		</xsl:choose>

		<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or ((/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC') and /root/recipient/@countryType != 'I')">
	   		document.all.floristCodex.value ="<xsl:value-of select="/root/deliveryDetail/@floristCode"/>";
		</xsl:if>


		document.all.giftMessage.value ="<xsl:value-of select="$giftMessage" />";
	   	document.all.signature.value ="<xsl:value-of select="/root/deliveryDetail/@signature"/>";
	   	document.all.orderComments.value ="<xsl:value-of select="$orderComments" />";

	   	<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
	   	   	document.all.institution.value ="<xsl:value-of select="/root/deliveryDetail/@deliveryLocationName"/>";
	   	</xsl:if>

	   	<xsl:if test="/root/pageData/data[@name='displayRoomNumber']/@value = 'Y'">
	   		document.all.roomNumber.value ="<xsl:value-of select="/root/deliveryDetail/@deliveryLocationExtra"/>";
	   	</xsl:if>

	   	<xsl:if test="/root/pageData/data[@name='displayWorkingHours']/@value = 'Y'">
	   	  	document.all.fromWorkHours.value ="<xsl:value-of select="/root/deliveryDetail/@fromWorkHours"/>";
	   		document.all.toWorkHours.value ="<xsl:value-of select="/root/deliveryDetail/@toWorkHours"/>";
	   	</xsl:if>

		<xsl:choose>
			<xsl:when test="/root/recipient/@countryType = 'I'">
				document.all.stateText.value = "<xsl:value-of select="/root/recipient/@state"/>";
			</xsl:when>
		   	<xsl:otherwise>
				if(document.all.country.value == 'CA')
				{
					CANstateDropDown();
				} else {
					USstateDropDown();
				}
				document.all.stateSelect.value = "<xsl:value-of select="/root/recipient/@state"/>";
			</xsl:otherwise>
		</xsl:choose>

	   	document.all.zipCode.value ="<xsl:value-of select="/root/recipient/@zip"/>";

		<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
			document.all.deliveryDate.value = "<xsl:value-of select="/root/deliveryDetail/@requestedDeliveryDate"/>";

	   		document.all.floristComments.value ="<xsl:value-of select="$floristComments" />";

			<xsl:choose>
				<xsl:when test="/root/deliveryDetail/@requestedDeliveryDate != ''">
					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
						<xsl:if test="/root/deliveryDetail/@requestedDeliveryDate = @date">
							document.all.deliveryDateChangeDIV.style.visibility = "hidden";
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					document.all.deliveryDateChangeDIV.style.visibility = "hidden";
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'N'">
			<xsl:choose>
				<xsl:when test="/root/shippingData/shippingMethods/shippingMethod/method/@description">
					<xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">

						var dateExist = initializeDeliveryDate("<xsl:value-of select="@description"/>");
						<![CDATA[
							if ( !dateExist )
							{
								]]>
									var desc = "<xsl:value-of  select="@description"/>";
								<![CDATA[

								if ( desc == "Next Day Delivery" ) {
									document.all.nextDayDate.value = document.forms[0].nextDayDate.options[0].value;
								}
								else if ( desc == "Standard Delivery" ) {
									document.all.standardDate.value = document.forms[0].standardDate.options[0].value;
								}
								else if ( desc == "Two Day Delivery" ) {
								  	document.all.twoDayDate.value = document.forms[0].twoDayDate.options[0].value;
								}
								else if ( desc == "Saturday Delivery" ) {
								  	document.all.saturdayDate.value = document.forms[0].saturdayDate.options[0].value;
								}
								else if ( desc == "Florist Delivery" ) {
								  	document.all.floristDate.value = document.forms[0].floristDate.options[0].value;
								}
							}
						]]>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="/root/orderData/data[@name ='requestedDeliveryDate']/@value">
							<xsl:for-each select="/root/deliveryDates/deliveryDate">
								<xsl:if test="/root/orderData/data[@name ='requestedDeliveryDate']/@value = @date">
									if(document.all.deliveryDateChangeDIV != null)
									{
										document.all.deliveryDateChangeDIV.style.visibility = "hidden";
									}
								</xsl:if>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							if(document.all.deliveryDateChangeDIV != null)
							{
								document.all.deliveryDateChangeDIV.style.visibility = "hidden";
							}
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
<![CDATA[
			// Find out if this is the only option.
			// If so then select this option.
			var radioGrpx = document.forms[0].deliveryMethodx;
			if(radioGrpx != null && radioGrpx[0] == null)
			{
				radioGrpx.checked = true;
				deliveryMethodXCheck();
			}
]]>
		</xsl:if>

		<xsl:choose>
			<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP' and /root/recipient/@countryType = 'I'">
					openJCPPopup();
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="/root/pageData/data[@name='displayCodifiedSpecial']/@value = 'Y'">
					<![CDATA[
						// display the Codified Special DIV Tag
						openCodifiedSpecial();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayProductUnavailable']/@value = 'Y'">
					<![CDATA[
						// display the Product Unavailable DIV tag
						openProductUnavailable();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displaySpecGiftPopup']/@value = 'Y'">
					<![CDATA[
						//display the Only Specialty Gifts DIV Tag
						openNoFloral();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayNoProductPopup']/@value = 'Y'">
					<![CDATA[
						//display the No Products DIV Tag
						openNoProduct();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayNoCodifiedFloristHasCommonCarrier']/@value = 'Y'">
					<![CDATA[
						//display the No Codified Florist Has Common Carrier DIV Tag
						openNoCodifiedFloristHasCommonCarrier();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayNoFloristHasCommonCarrier']/@value = 'Y'">
					<![CDATA[
						//display the No Florist Has Common Carrier DIV Tag
						openNoFloristHasCommonCarrier();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayFloristSelectedButNotAvailable']/@value = 'Y'">
					<![CDATA[
						//display the FloristSelectedButNotAvailable DIV Tag
						openFloristSelectedButNotAvailable();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayNoCodifiedFloristHasTwoDayDeliveryOnly']/@value = 'Y'">
					<![CDATA[
						//display the No Florist Has Common Carrier DIV Tag
						openNoCodifiedFloristHasTwoDayDeliveryOnly();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayFloristPopup']/@value = 'Y'">
					<![CDATA[
						//display the Only Floral Products DIV Tag
						openOnlyFloral();
					]]>
				</xsl:if>
				<xsl:if test="/root/pageData/data[@name='displayUpsellGnaddSpecial']/@value = 'Y'">
					<![CDATA[
						// display the GNADD upsell DIV tag
						openGnaddUpsell();
					]]>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

   	<xsl:if test="/root/pageData/data[@name='displayReleaseCheckbox']/@value = 'Y'">
   		var releaseName ="<xsl:value-of select="/root/deliveryDetail/@releaseSendersName"/>";
<![CDATA[

		if(releaseName == "Y" || releaseName == "on")
		{
		document.all.releaseSendersNameCheckBox.checked = true;
		} else {
		document.all.releaseSendersNameCheckBox.checked = false;
		}
]]>
   	</xsl:if>

<![CDATA[

		//Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
		]]>

   		<xsl:if test="/root/pageData/data[@name='displaySpecialFee']/@value = 'Y'">
   			<![CDATA[
   				alertHIorAK();
   			]]>
   		</xsl:if>
   		<![CDATA[
	}

	function USstateDropDown()
	{
		counter = 0;

		//Main page
		document.forms[0].stateSelect.options.length = 0;

		if (document.all.country.value == "US")
		{
			//recipeint DIV
			//document.forms[0].stateInput.options.length = 0;

			if(document.forms[0].floristStateInput.style.visibility != "hidden")
			{
				//florist DIV
				document.forms[0].floristStateInput.options.length = 0;
			}

			//zip code DIV
			document.forms[0].stateInputLookup.options.length = 0;
		}

]]>
		<xsl:for-each select="/root/deliveryData/states/state">
			<xsl:if test="@countryCode =''">
			var stateID = "<xsl:value-of select="@stateMasterId"/>";
			var stateName = "<xsl:value-of select="@stateName"/>";

<![CDATA[
//counter only needs incremented once

	//Main page
	document.forms[0].stateSelect.options[counter++] = new Option(stateName, stateID, false, false);

	if (document.all.country.value == "US")
	{
		//recipeint DIV
		//document.forms[0].stateInput.options[counter] = new Option(stateName, stateID, false, false);

		//florist DIV
		document.forms[0].floristStateInput.options[counter] = new Option(stateName, stateID, false, false);

		//zip code DIV
		document.forms[0].stateInputLookup.options[counter] = new Option(stateName, stateID, false, false);
	}
]]>
			</xsl:if>
		</xsl:for-each>
<![CDATA[

	}

	function CANstateDropDown()
	{
		counter = 0;

		//Main page
		document.forms[0].stateSelect.options.length = 0;

	if (document.all.country.value == "CA")
	{
		//recipeint DIV
		//document.forms[0].stateInput.options.length = 0;

		//florist DIV
		document.forms[0].floristStateInput.options.length = 0;

		//zip code DIV
		document.forms[0].stateInputLookup.options.length = 0;
	}
]]>
		<xsl:for-each select="/root/deliveryData/states/state">
			<xsl:if test="@countryCode ='CAN'">
			var stateID = "<xsl:value-of select="@stateMasterId"/>";
			var stateName = "<xsl:value-of select="@stateName"/>";


<![CDATA[
//counter only needs incremented once
		//Main page
		document.forms[0].stateSelect.options[counter++] = new Option(stateName, stateID, false, false);

		if (document.all.country.value == "CA")
		{
		//florist DIV
		document.forms[0].floristStateInput.options[counter] = new Option(stateName, stateID, false, false);

		//recipeint DIV
		//document.forms[0].stateInput.options[counter] = new Option(stateName, stateID, false, false);

		//zip code DIV
		document.forms[0].stateInputLookup.options[counter] = new Option(stateName, stateID, false, false);
		}

]]>
			</xsl:if>
		</xsl:for-each>
<![CDATA[


	}

   	function copyPhrase(){
	/***  This function will copy phrases to the gift message
	*/
   	var counter=0;
]]>
   	<xsl:for-each select="/root/deliveryData/giftMessages/giftMessage">
<![CDATA[
		if (document.forms[0].giftMessageList.options[counter].selected == true)
		{
			document.forms[0].giftMessage.value = document.forms[0].giftMessage.value + document.forms[0].giftMessageList.options[counter].text + ' ';
			checkMessageSize();
		}
		counter = counter + 1;
]]>
	</xsl:for-each>
<![CDATA[
   	}
   	function checkMessageSize(){
	/***  This function will limited to length of the gift message to 180
	*/
   	var availableFieldLength = 180;
	if(document.forms[0].giftMessage.value.length > availableFieldLength)
	{
		document.forms[0].giftMessage.value = document.forms[0].giftMessage.value.substring(0,(availableFieldLength));
	}

	}
   	function checkCommentsSize(){
	/***  This function will limited to length of the order Comments to 180
	*/
   	var availableFieldLength = 180;
	if(document.forms[0].orderComments.value.length > availableFieldLength)
	{
		document.forms[0].orderComments.value = document.forms[0].orderComments.value.substring(0,(availableFieldLength));
	}

	}
   	function checkFloristCommentsSize(){
	/***  This function will limited to length of the order Comments to 180
	*/
   	var availableFieldLength = 180;
	if(document.forms[0].floristComments.value.length > availableFieldLength)
	{
		document.forms[0].floristComments.value = document.forms[0].floristComments.value.substring(0,(availableFieldLength));
	}

	}
	function openCalendarPopup()
	{
		openCalendarLoadingDiv();

		productId = document.forms[0].productId.value;
		var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=DISPLAY_CALENDAR" + "&productId=" + productId;
//		window.open(url_source,"VIEW_CALENDAR","width=471,height=500,resizable=0,status=0,menubar=0,scrollbars=yes")

	    var modal_dim="dialogWidth:400px; dialogHeight:500px; center:yes; status=0";
 	    var ret = window.showModalDialog(url_source,"", modal_dim);
 	    closeCalendarLoadingDiv();

 	    if ( ret!= null )
 	    {
 	    	document.forms[0].deliveryDate.value = ret;
 	    	var selectBox = document.all.deliveryDate;
 	    	setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].displayDate, selectBox.options[selectBox.selectedIndex].day);
 	    }

 	    //Set the focus on the next field
 	    document.forms[0].deliveryLocation.focus();
 	}

	function openCalendarLoadingDiv()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("calendarLoading").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-200

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeCalendarLoadingDiv()
	{
		calendarLoading.style.visibility = "hidden";
	}

	function deliveryMethodXCheck(element)
	{
		var radioRef;

		if(document.all.deliveryMethodx[0] == null)
		{
			// This is the only delivery method
			radioRef = document.all.deliveryMethodx;
		}
		else
		{
			// We have more than one delivery method
			// so use the one that is passed in as a param
			radioRef = element;
		}

		if(radioRef.value != "Florist Delivery" && productType == "SDG" && (stateCode == 'HI' || stateCode == 'AK'))
		{
			alertHIorAK();
		}
	}


	function checkDeliveryMessage(){
   /***
    * This function determines if a alert box should be display for the delivery location
    */
    var intype = document.all.deliveryLocation.value;
    var delivType;
    var messageInd;
]]>
	<xsl:for-each select="/root/delayMessage/location">
		delivType = "<xsl:value-of select="@name"/>";
		messageInd = "<xsl:value-of select="@value"/>";

<![CDATA[
		if (document.all.deliveryLocation.value == delivType)
		{
			if (messageInd == "Y")
			{
				alert("Please inform the customer that delivery to the chosen location type could be delayed by three to five days");
			}
		}
]]>
	</xsl:for-each>
<![CDATA[
	}
	function checkForInternational(inCountry) {
	/***  This function will determine if the country is international or not
	*/

		var international = false;
		var cntry2 = inCountry;
		var cntry3;
		var domestic;
]]>
		<xsl:for-each select="/root/deliveryData/countries/country">
			cntry3 = "<xsl:value-of select="@countryId"/>";
			domestic = "<xsl:value-of select="@oeCountryType"/>";
<![CDATA[
        	if(cntry2 == cntry3){
       	   		if(domestic == "I") {
   					international = true;
    			}
    		}
]]>
		</xsl:for-each>
<![CDATA[
		return international;
	}

	function JCPPopChoice(inChoice)
	{
	/***  popup JC penney popup for international change
	*/
		if(inChoice == 'Y')
		{
			var url= "DNISChangeServlet";
			url = url.concat(buildURL());
			url = url.concat("&source=deliveryJCP");

			document.location = url;
		} else {

			document.forms[0].country.value = 'US';
			closeJCPPopup();
		}



	}
	function openJCPPopup()
	{
		//hide dropdowns
		hideDIVs();

		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		JCPPopV = (dom)?document.getElementById("JCPPop").style : ie? document.all.JCPPop : document.JCPPop
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset

		JCPPopV.width = 290
		JCPPopV.height = 100

		JCPPopV.left=document.body.clientWidth/2 - 50
		JCPPopV.top=scroll_top+document.body.clientHeight/2 - 100

		JCPPopV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeJCPPopup()
	{
		JCPPopV.visibility = "hidden";
		showDIVs();
	}



	function checkForPOAPO(inString)
    /***
    *   This function checks fo Post Office or APO in a string
    */
	{
   /***
    * This function will return true if any of the PO/APO characters found in input string
    */

	var POAPO = new Array("PO BOX", "P.O.", "POSTOFFICE",
						  " APO", "A.P.O.", "FPO", "F.P.O");

	for (var i = (POAPO.length - 1); i >= 0; i--){

		if ((inString.toUpperCase()).indexOf(POAPO[i]) >= 0)
		{
			return true;
		}

	}
	return false;

	}
function openUpdateWin()
{
	var ie=document.all
	var dom=document.getElementById
	var ns4=document.layers

	cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
	updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
	scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
	updateWinV.top=scroll_top+document.body.clientHeight/2-100

	updateWinV.width=290
	updateWinV.height=100
	updateWinV.left=document.body.clientWidth/2 - 100

	updateWinV.visibility=(dom||ie)? "visible" : "show"
}

function closeUpdateWin()
{
	updateWin.style.visibility = "hidden";
}

	/*	**************************************************************************
			This group of functions is used to display/hide the Specialty Gift
			only available for zip code message box.
		**************************************************************************	*/
	function openNoFloral()
	{
		//hide the dropdowns
		hideDIVs();

		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloralV = (dom)?document.getElementById("noFloral").style : ie? document.all.noFloral : document.noFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloralV.top=scroll_top+document.body.clientHeight/2-100

		noFloralV.width=290
		noFloralV.height=100
		noFloralV.left=document.body.clientWidth/2 - 100
		noFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoFloral()
	{
		//enableContinue();
		disableContinue()
		noFloral.style.visibility = "hidden";
		showDIVs();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the No Product
			available for zip code message box
		**************************************************************************	*/
	function openNoProduct()
	{
		//hide the dropdowns
		hideDIVs();

		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noProductV = (dom)?document.getElementById("noProduct").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noProductV.top=scroll_top+document.body.clientHeight/2-100

		noProductV.width=290
		noProductV.height=100
		noProductV.left=document.body.clientWidth/2 - 50
		noProductV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoProduct()
	{
		showDIVs();
		noProductV.visibility = "hidden";
		if (document.all.zipCode && document.all.zipCode.enabled == true)
			document.forms[0].zipCode.focus();
	}

	function noProduct()
	{
		closeNoProduct();
	}

// Called when a same day gift has no codified florists for this zip code
	function openNoCodifiedFloristHasCommonCarrier()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noCodifiedFloristHasCommonCarrierV = (dom)?document.getElementById("noCodifiedFloristHasCommonCarrier").style : ie? document.all.noCodifiedFloristHasCommonCarrier : document.noCodifiedFloristHasCommonCarrier
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noCodifiedFloristHasCommonCarrierV.top=scroll_top+document.body.clientHeight/2-100

		noCodifiedFloristHasCommonCarrierV.width=290
		noCodifiedFloristHasCommonCarrierV.height=100
		noCodifiedFloristHasCommonCarrierV.left=document.body.clientWidth/2 - 50
		noCodifiedFloristHasCommonCarrierV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoCodifiedFloristHasCommonCarrier()
	{
		noCodifiedFloristHasCommonCarrierV.visibility = "hidden";
		enableContinue();
	}

	function noCodifiedFloristHasCommonCarrier()
	{
		closeNoCodifiedFloristHasCommonCarrier();
	}

// Called when a same day gift has no florists for this zip code
	function openNoFloristHasCommonCarrier()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noFloristHasCommonCarrierV = (dom)?document.getElementById("noFloristHasCommonCarrier").style : ie? document.all.noFloristHasCommonCarrier : document.noFloristHasCommonCarrier
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noFloristHasCommonCarrierV.top=scroll_top+document.body.clientHeight/2-100

		noFloristHasCommonCarrierV.width=290
		noFloristHasCommonCarrierV.height=100
		noFloristHasCommonCarrierV.left=document.body.clientWidth/2 - 50
		noFloristHasCommonCarrierV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoFloristHasCommonCarrier()
	{
		noFloristHasCommonCarrierV.visibility = "hidden";
		enableContinue();
	}

	function noFloristHasCommonCarrier()
	{
		closeNoFloristHasCommonCarrier();
	}



	function openFloristSelectedButNotAvailable()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		floristSelectedButNotAvailableV = (dom)?document.getElementById("floristSelectedButNotAvailable").style : ie? document.all.floristSelectedButNotAvailable : document.floristSelectedButNotAvailable
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		floristSelectedButNotAvailableV.top=scroll_top+document.body.clientHeight/2-100

		floristSelectedButNotAvailableV.width=290
		floristSelectedButNotAvailableV.height=100
		floristSelectedButNotAvailableV.left=document.body.clientWidth/2 - 50
		floristSelectedButNotAvailableV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeFloristSelectedButNotAvailable()
	{
		floristSelectedButNotAvailableV.visibility = "hidden";
		enableContinue();
	}

	function floristSelectedButNotAvailable()
	{
		closeFloristSelectedButNotAvailable();
	}


	function openNoCodifiedFloristHasTwoDayDeliveryOnly()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		noCodifiedFloristHasTwoDayDeliveryOnlyV = (dom)?document.getElementById("noCodifiedFloristHasTwoDayDeliveryOnly").style : ie? document.all.noCodifiedFloristHasTwoDayDeliveryOnly : document.noCodifiedFloristHasTwoDayDeliveryOnly
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		noCodifiedFloristHasTwoDayDeliveryOnlyV.top=scroll_top+document.body.clientHeight/2-100

		noCodifiedFloristHasTwoDayDeliveryOnlyV.width=290
		noCodifiedFloristHasTwoDayDeliveryOnlyV.height=100
		noCodifiedFloristHasTwoDayDeliveryOnlyV.left=document.body.clientWidth/2 - 50
		noCodifiedFloristHasTwoDayDeliveryOnlyV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeNoCodifiedFloristHasTwoDayDeliveryOnly()
	{
		noCodifiedFloristHasTwoDayDeliveryOnlyV.visibility = "hidden";
		enableContinue();
	}

	function noCodifiedFloristHasTwoDayDeliveryOnly()
	{
		closeNoCodifiedFloristHasTwoDayDeliveryOnly();
	}

	function closeGnaddUpsell()
	{
		gnaddUpsellUV.visibility = "hidden";
		if (document.all.zip && document.all.zip.enabled == true)
			document.forms[0].zip.focus();

		enableContinue();
	}

	function gnaddUpsell()
	{
		closeGnaddUpsell();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Product Unavailable
			for zip code message box
		**************************************************************************	*/
	function openProductUnavailable()
	{
		//hide the dropdowns
		hideDIVs();

		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		productUV = (dom)?document.getElementById("productUnavailable").style : ie? document.all.noProduct : document.noProduct
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		productUV.top=scroll_top+document.body.clientHeight/2-100

		productUV.width=290
		productUV.height=100
		productUV.left=document.body.clientWidth/2 - 50
		productUV.visibility=(dom||ie)? "visible" : "show"
	}

	function openGnaddUpsell()
	{
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		gnaddUpsellUV = (dom)?document.getElementById("gnaddUpsell").style : ie? document.all.gnaddUpsell : document.gnaddUpsell
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		gnaddUpsellUV.top=scroll_top+document.body.clientHeight/2-25

		gnaddUpsellUV.width=290
		gnaddUpsellUV.height=100
		gnaddUpsellUV.left=document.body.clientWidth/2 - 50
		gnaddUpsellUV.visibility=(dom||ie)? "visible" : "show"

	}

	function closeProductUnavailable()
	{
		showDIVs();
		productUV.visibility = "hidden";
		if (document.all.zipCode && document.all.zipCode.enabled == true)
			document.forms[0].zipCode.focus();
	}

	function productUnavailable()
	{
		closeProductUnavailable();
	}

	/*	**************************************************************************
			This group of functions is used to display/hide the Floral Only
			for zip code message box
		**************************************************************************	*/
	function openOnlyFloral()
	{
		hideDIVs();
		disableContinue();

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		onlyFloralV = (dom)?document.getElementById("onlyFloral").style : ie? document.all.onlyFloral : document.onlyFloral
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		onlyFloralV.top=scroll_top+document.body.clientHeight/2-100

		onlyFloralV.width=290
		onlyFloralV.height=100
		onlyFloralV.left=document.body.clientWidth/2 - 50
		onlyFloralV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeOnlyFloral()
	{
		showDIVs();
		onlyFloralV.visibility = "hidden";
		if (document.all.zipCode && document.all.zipCode.enabled == true)
			document.forms[0].zipCode.focus();
	}

	function onlyFloral()
	{
		closeOnlyFloral();
	}

/////////////////////////////////////////////////////////////////////////////
//SERVER SIDE VALIDATION CALL BACK FUNCTIONS
////////////////////////////////////////////////////////////////////////////

function onValidationNotAvailable()
{
	closeUpdateWin();
	var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");
	if(x == true)
	{
		checkFieldLengths();
		deliveryInfoform.submit();
	}
}

function onValidationResponse(validation)
{
	var serverCheck = true;

	//check if this is the zip response (REFRESH) or the submit response (no value)

	if (submitInd == "REFRESH" || submitInd == "REFRESHMAXDAYS")
	{
		var ret_zipCode = validation.ZIPCODE.value;

		//get the return value from the server

		//if OK then refresh the page
		if(ret_zipCode == "OK")
		{
		validZip = true;
		if (submitInd == "REFRESH") {refreshPage('zipChange');}
		if (submitInd == "REFRESHMAXDAYS") {refreshPage('zipChange','deliveryLocation');}
		}

		//not OK, so set the background color, display alert,
		//disable the button, close the update window, and set a variable
		else
		{
		deliveryInfoform.zipCode.style.backgroundColor='pink';
		closeUpdateWin();
		alert("Could not validate Zip code");
		disableContinue();

		validZip = false;
		}

		//reset the submit indicator
		submitInd = "";

		//exit the function
		return false;
	}

	var ret_QMS_ADDRESS = validation.QMS_ADDRESS.value;

	var origadd1 = deliveryInfoform.recipientAddress1.value;
	var origadd2 = deliveryInfoform.recipientAddress2.value;
	var origcity = deliveryInfoform.city.value;
	var origstate = deliveryInfoform.stateSelect.value;

	var origZip = deliveryInfoform.zipCode.value;


	deliveryInfoform.QMSAddress1.value = validation.QMS_ADDRESS1.value;
	deliveryInfoform.QMSAddress2.value = validation.QMS_ADDRESS2.value;
	deliveryInfoform.QMSCity.value = validation.QMS_CITY.value;
	deliveryInfoform.QMSState.value = validation.QMS_STATE.value;
	bypassZipChange = true;
	deliveryInfoform.QMSZipCode.value = validation.QMS_ZIP.value;
	bypassZipChange = false;

	closeUpdateWin();
	if(ret_QMS_ADDRESS == "OK" | document.all.country.value == "CA" | document.all.country.value == "PR" | document.all.country.value == "VI")
	{
		checkFieldLengths();
		deliveryInfoform.submit();
	} else {
		if((ret_QMS_ADDRESS == "Florist does not exist in the database as a valid florist"))
		{
]]>
			<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or ((/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC') and /root/recipient/@countryType != 'I')">
<![CDATA[
				deliveryInfoform.floristCodex.style.backgroundColor='pink';
				document.forms[0].floristCodex.focus();
				alert(ret_QMS_ADDRESS);
]]>
			</xsl:if>
<![CDATA[

		} else {
			if(ret_QMS_ADDRESS == "Zip code has been changed")
			{
				alert(ret_QMS_ADDRESS + ".  New delivery dates will be calculated." )
				deliveryInfoform.zipCode.value = validation.QMS_ZIP.value;
				bypassZipChange = false;
				validZip = true;
				trimzip = deliveryInfoform.zipCode.value
				trimzip = trimzip.substring(0,5)
				deliveryInfoform.zipCode.value = trimzip;
				alert(deliveryInfoform.zipCode.value);

				refreshPage('zipChange');
			} else {

				if(ret_QMS_ADDRESS == "Address has been standardized")
				{
					closeUpdateWin();

					document.all.oldAddress1.value =
					deliveryInfoform.recipientAddress1.value + "\n"
					+ deliveryInfoform.recipientAddress2.value + "\n"
					+ deliveryInfoform.city.value + "\n"
					+ deliveryInfoform.stateSelect.value + "\n"
					+ deliveryInfoform.zipCode.value;

					document.all.newAddress1.value =
					deliveryInfoform.QMSAddress1.value + "\n"
					+ deliveryInfoform.QMSAddress2.value + "\n"
					+ deliveryInfoform.QMSCity.value + "\n"
					+ deliveryInfoform.QMSState.value + "\n"
					+ deliveryInfoform.QMSZipCode.value;
					openStandardAddress()

				} else {

					closeUpdateWin();

					document.all.invalidAddress.value =
					deliveryInfoform.recipientAddress1.value + "\n"
					+ deliveryInfoform.recipientAddress2.value + "\n"
					+ deliveryInfoform.city.value + "\n"
					+ deliveryInfoform.stateSelect.value + "\n"
					+ deliveryInfoform.zipCode.value;

					openInvalidAddress()
				}
				closeUpdateWin();
			}
		}
	}
}
function addressChoice(inChoice)
{
	if(inChoice == 'Y')
	{
		document.all.QMSOverrideFlag.value = 'Y';
		checkFieldLengths();
		deliveryInfoform.submit();
	} else {
		if(inChoice == 'N')
		{
			document.all.QMSOverrideFlag.value = 'N';
			checkFieldLengths();
			deliveryInfoform.submit();
		} else {

		}
	}
	closeStandardAddress()
}
function openStandardAddress()
{
	hideDIVs();
	var ie=document.all
	var dom=document.getElementById
	var ns4=document.layers

	cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
	standV = (dom)?document.getElementById("standardAddressDIV").style : ie? document.all.standardAddressDIV : document.standardAddressDIV
	scroll_top=(ie)? document.body.scrollTop : window.pageYOffset


	standV.top=scroll_top+document.body.clientHeight/2-100

	standV.width=310
	standV.height=100
	standV.left=document.body.clientWidth/2 - 155;
	standV.top=scroll_top + 100/2;
	standV.visibility=(dom||ie)? "visible" : "show"
}

function closeStandardAddress()
{
	showDIVs();
	standardAddressDIV.style.visibility = "hidden";
}

function openInvalidAddress()
{
	hideDIVs();
	var ie=document.all
	var dom=document.getElementById
	var ns4=document.layers

	cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
	standV = (dom)?document.getElementById("invalidAddressDIV").style : ie? document.all.invalidAddressDIV : document.invalidAddressDIV
	scroll_top=(ie)? document.body.scrollTop : window.pageYOffset


	standV.top=scroll_top+document.body.clientHeight/2-100

	standV.width=310
	standV.height=100
	standV.left=document.body.clientWidth/2 - 155;
	standV.top=scroll_top + 100/2;
	standV.visibility=(dom||ie)? "visible" : "show"
}

function closeInvalidAddress()
{
	showDIVs();
	invalidAddressDIV.style.visibility = "hidden";
}

function invalidAddressChoice(inChoice)
{
	if(inChoice == 'Y')
	{
		document.all.QMSOverrideFlag.value = 'N';
		var oclen = (document.all.orderComments.value).length;
		/**  If Customer insisted comment not already in Order Comment insert it  */
		if((document.all.orderComments.value).substring((oclen - 29), oclen) != "Customer insisted on Address ")
		{
			document.all.orderComments.value = document.all.orderComments.value.concat(" Customer insisted on Address ");
		}
		checkFieldLengths();
		deliveryInfoform.submit();
	}
	closeInvalidAddress()
}

	function validateZip(insource){
	//This function is for validating the zip code
	//when the delivery date is being recalculated

	// reset the backupFlag
	backupFlag = false;

	//reset the background color
	document.forms[0].zipCode.style.backgroundColor='white';

	//basic zip validation
	var check = true
]]>
    	<xsl:if test="/root/recipient/@countryType != 'I'">
<![CDATA[

		ziplen = document.all.zipCode.value.length;
		zip = stripWhitespace(document.all.zipCode.value);
		firstchar = zip.substring(0,1);

		//length of zip must be 5 or 6
		if ((ziplen != 5) ]]> &amp; <![CDATA[ (ziplen != 6))
		    check = false;

		// Canada must start with a letter
		// if ((ziplen == 6) ]]> &amp; <![CDATA[ (!isLetter(firstchar)))
		//    check = false;

		// USA must start with a number
		// if ((ziplen == 5) ]]> &amp; <![CDATA[ (isLetter(firstchar)))
		//    check = false;

		//validation error
		if (check == false)
		{
		   document.forms[0].zipCode.focus();
		   document.forms[0].zipCode.style.backgroundColor='pink';
		   alert("Invalid zip code");
		   disableContinue();
		   return false;
		}
]]>
	</xsl:if>
<![CDATA[

	//Continue only refresh the page if the disableRefresh flag is false (the button is not dimmed)
	if (disableRefreshFlag != false)
		return false;

	//open the DIV tag
	openUpdateWin();

	//set the submit indicator for the validation response
	if(insource == 'zip'){submitInd = "REFRESH";}
	if(insource == 'getMaxDays'){submitInd = "REFRESHMAXDAYS";}

	var check = true;
	/***
     *   Check Recipient Zip Code if it exist
     */
 		document.forms[0].zipCode.className="TblText";
 		var bag = "-";
		var zpnws = stripCharsInBag(document.forms[0].zipCode.value, bag);
     	var zcnws = stripWhitespace(zpnws);

		//If the zip code doesn't exist or its made up of invalid characters then its invalid
		//so set the check variable to false and disable the continue button
		//if (zcnws.length == 0 || (zcnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(zcnws)))

		if ((zcnws.length == 0) || (!isAlphanumeric(zcnws)))
    	{
    		deliveryInfoform.zipCode.style.backgroundColor='pink';
    		closeUpdateWin();
    		alert("Please correct marked fields");
   			document.forms[0].zipCode.focus();
    		check = false;
    		disableContinue()
    	}

///////////////////////////////////////////////////////////////////////////
//INITIATE SERVER SIDE VALIDATIION
///////////////////////////////////////////////////////////////////////////
	if(check == true)
	{
]]>
    	<xsl:if test="/root/recipient/@countryType != 'I'">
<![CDATA[
		addValidationEntry("ZIPCODE", deliveryInfoform.zipCode.value);
		callToServer();
]]>
		</xsl:if>
<![CDATA[
	}

	return check;

	}   //close function

	function truncateFieldToMaxLength(fieldValue, fieldLength) {

		//alert(fieldValue);
		//alert(fieldLength);
		var ret;

		if (fieldValue.length > fieldLength)
		{
			ret = fieldValue.substr(0, fieldLength);
		}
		else
		{
			ret = fieldValue;
		}

		//alert(ret.length);
		return ret;
	}

	function fieldExceedsMaxLength(fieldValue, fieldLength) {
		if (fieldValue.length > fieldLength) {
			return true;
		} else {
			return false;
		}
	}


	function validateForm(){
   /***
    * This function validates all fields
    */

 		openUpdateWin();

  		bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

    	var check = true;

]]>
    	<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'N'">
<![CDATA[
    		var radioGrpx = document.forms[0].deliveryMethodx;

			if(radioGrpx[0] != null)
			{
				for (var h=0; h < radioGrpx.length; h++)
				{
					radioGrpx[h].className="TblText";
				}

				var radioflag = false;
				for (var i=0; i < radioGrpx.length; i++)
				{
					if(radioGrpx[i].checked )
						radioflag = true;
				}
				if(radioflag == false)
				{
					check = false;
					radioGrpx[0].focus();
					for (var h=0; h < radioGrpx.length; h++)
					{
						radioGrpx[h].className="Error";
					}
				}
			} else {

				if(!document.forms[0].deliveryMethodx.checked)
				{
					check = false;
					document.forms[0].deliveryMethodx.className="Error";

				}
			}


 ]]>
    	</xsl:if>
<![CDATA[
]]>
    	<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
<![CDATA[
		document.forms[0].deliveryDate.className="TblText";

		if (document.forms[0].deliveryDate.selectedIndex < 0)
    	{
    		if (check == true)
    		{
    			document.forms[0].deliveryDate.focus();
    			check = false;
   			}
    		document.forms[0].deliveryDate.className="Error";
    	}

 ]]>
    	</xsl:if>
<![CDATA[
    /***
    *   Check Recipient Phone Number if it exist
    */
    document.forms[0].recipientPhone.className="TblText";
 		var bag = "-()";
     	var pvnws = stripCharsInBag(document.forms[0].recipientPhone.value, bag);
     	var pnnws = stripWhitespace(pvnws);
		if (pnnws.length > 0)
    	{
	if ((!isInteger(pnnws)) || fieldExceedsMaxLength(document.forms[0].recipientPhone.value, 20))
    		{
    			if (check == true)
    				{
    					document.forms[0].recipientPhone.focus();
    					check = false;
   					}

    				document.forms[0].recipientPhone.className="Error";
   			}
   			else
   			{
   				if ((checkForInternational(document.forms[0].country.value) ]]> &amp;&amp; <![CDATA[ pnnws.length > 20) || (!checkForInternational(document.forms[0].country.value) ]]> &amp;&amp; <![CDATA[ pnnws.length != 10))
	  		  	{
	    			if (check == true)
    				{
    					document.forms[0].recipientPhone.focus();
    					check = false;
   					}

    				document.forms[0].recipientPhone.className="Error";
    			}
    		}
    	}
    /***
    *   Check Recipient Phone Extension if it exist
    */
    document.forms[0].recipientPhoneExt.className="TblText";
 		var bag = "-()";
     	var penws = stripWhitespace(document.forms[0].recipientPhoneExt.value);
		if (penws.length > 0)
    	{
	if ((!isInteger(penws)) || fieldExceedsMaxLength(document.forms[0].recipientPhoneExt.value, 10))
    		{
    			if (check == true)
    				{
    					document.forms[0].recipientPhoneExt.focus();
    					check = false;
   					}
    				document.forms[0].recipientPhoneExt.className="Error";
   			}
   		}
    /***
    *   Check Institution Name if it exist and is required
    */
]]>
   	<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
<![CDATA[
document.forms[0].institution.className="TblText";
    	var ixnws = stripCharsInBag(document.forms[0].institution.value, bag2);
     	var innws = stripWhitespace(ixnws);

	if ((innws.length == 0 || (innws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(innws))) || fieldExceedsMaxLength(document.forms[0].institution.value, 60))
    	{
    		if (check == true)
    		{
    			document.forms[0].institution.focus();
    			check = false;
   			}
    		document.forms[0].institution.className="Error";
    	}
]]>
    </xsl:if>
<![CDATA[
    /***
    *   Check Room Number if it exist and is required
    */
]]>
   	<xsl:if test="/root/pageData/data[@name='displayRoomNumber']/@value = 'Y'">
<![CDATA[
document.forms[0].roomNumber.className="TblText";
     	var rnnws = stripWhitespace(document.forms[0].roomNumber.value);
	if ((rnnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(rnnws)) || fieldExceedsMaxLength(document.forms[0].roomNumber.value, 120))
    	{
    		if (check == true)
    		{
    			document.forms[0].roomNumber.focus();
    			check = false;
   			}
   			document.forms[0].roomNumber.className="Error";
    	}
]]>
    </xsl:if>
<![CDATA[
    /***
    *   Check Recipient First Name if it exist
    */
    document.forms[0].recipientFirstName.className="TblText";
    	var rxnws = stripCharsInBag(document.forms[0].recipientFirstName.value, bag2);
     	var rfnws = stripWhitespace(rxnws);

	if ((rfnws.length == 0 || (rfnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(rfnws))) || fieldExceedsMaxLength(document.forms[0].recipientFirstName.value, 20))
    	{
    		if (check == true)
    		{
    			document.forms[0].recipientFirstName.focus();
    			check = false;
   			}
    		document.forms[0].recipientFirstName.className="Error";
    	}
    /***
    *   Check Recipient last Name if it exist
    */
    document.forms[0].recipientLastName.className="TblText";
    	var rfxnws = stripCharsInBag(document.forms[0].recipientLastName.value, bag2);
     	var rfnws = stripWhitespace(rfxnws);

	if ((rfnws.length == 0 || (rfnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(rfnws))) || fieldExceedsMaxLength(document.forms[0].recipientLastName.value, 40))
    	{
    		if (check == true)
    		{
    			document.forms[0].recipientLastName.focus();
    			check = false;
   			}
    		document.forms[0].recipientLastName.className="Error";
    	}
    /***
    *   Check Recipient Address 1 if it exist
    */
    document.forms[0].recipientAddress1.className="TblText";
    	var a1xnws = stripCharsInBag(document.forms[0].recipientAddress1.value, bag2);
     	var a1nws = stripWhitespace(a1xnws);
		var xyz = checkForPOAPO(document.forms[0].recipientAddress1.value);

		if (((a1nws.length == 0) || (a1nws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(a1nws))
		||(checkForPOAPO(document.forms[0].recipientAddress1.value))) || fieldExceedsMaxLength(document.forms[0].recipientAddress1.value, 80))

    	{
    		if (check == true)
    		{
    			document.forms[0].recipientAddress1.focus();
    			check = false;
   			}
    		document.forms[0].recipientAddress1.className="Error";
    	}
    /***
    *   Check Recipient Address 2 if it exist
    */
    document.forms[0].recipientAddress2.className="TblText";
    	var a2xnws = stripCharsInBag(document.forms[0].recipientAddress2.value, bag2);
     	var a2nws = stripWhitespace(a2xnws);

	if (((a2nws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(a2nws)) || (checkForPOAPO(document.forms[0].recipientAddress2.value))) || fieldExceedsMaxLength(document.forms[0].recipientAddress2.value, 40))
    	{
    		if (check == true)
    		{
    			document.forms[0].recipientAddress2.focus();
    			check = false;
   			}
    		document.forms[0].recipientAddress2.className="Error";
    	}
]]>
		<xsl:if test="/root/recipient/@countryType != 'I'">
<![CDATA[
		  	/***
		    *   Check Recipient Zip Code if it exist
		    */
		 		document.forms[0].zipCode.className="TblText";
		 		var bag = "-";
     			var zpnws = stripCharsInBag(document.forms[0].zipCode.value, bag);
		     	var zcnws = stripWhitespace(zpnws);

				if (zcnws.length == 0 || (zcnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(zcnws)))
		    	{
		    		if (check == true)
		    		{
		    			document.forms[0].zipCode.focus();
		    			check = false;
		   			}
		    		document.forms[0].zipCode.className="Error";
		    	}
]]>
    </xsl:if>
<![CDATA[
  /***
    *   Check City if it exist
    */
    document.forms[0].city.className="TblText";
    	var ctxnws = stripCharsInBag(document.forms[0].city.value, bag2);
     	var ctnws = stripWhitespace(ctxnws);

	if ((ctnws.length == 0 || (ctnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(ctnws))) || fieldExceedsMaxLength(document.forms[0].city.value, 40))
    	{
    		if (check == true)
    		{
    			document.forms[0].city.focus();
    			check = false;
   			}
    		document.forms[0].city.className="Error";
    	}
    /***
    *   Check state text box if it exist
    */
]]>
    <xsl:if test="/root/recipient/@countryType = 'I'">
<![CDATA[
 		document.forms[0].stateText.className="TblText";
    	var stxnws = stripCharsInBag(document.forms[0].stateText.value, bag2);
     	var stnws = stripWhitespace(stxnws);

		if (stnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(stnws))
    	{
    		if (check == true)
    		{
    			document.forms[0].stateText.focus();
    			check = false;
   			}
    		document.forms[0].stateText.className="Error";
    	}
]]>
	</xsl:if>
<![CDATA[
    /***
    *   Check state Select box if it exist
    */
]]>
    <xsl:if test="/root/recipient/@countryType != 'I'">
<![CDATA[
 		document.forms[0].stateSelect.className="TblText";

		if (document.forms[0].stateSelect.selectedIndex < 0)
    	{
    		if (check == true)
    		{
    			document.forms[0].stateSelect.focus();
    			check = false;
   			}
    		document.forms[0].stateSelect.className="Error";
    	}
]]>
	</xsl:if>
<![CDATA[
    /***
    *   Check country Select box if it exist
    */
 		document.forms[0].country.className="TblText";

		if (document.forms[0].country.selectedIndex < 0)
    	{
    		if (check == true)
    		{
		document.forms[0].country.focus();
    			check = false;
   			}
    		document.forms[0].country.className="Error";
    	}
    /***
    *   Check signature if it exist
    */
    document.forms[0].signature.className="TblText";
    	var sixnws = stripCharsInBag(document.forms[0].signature.value, bag2);
     	var signws = stripWhitespace(sixnws);

	if ((signws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(signws)) || fieldExceedsMaxLength(document.forms[0].signature.value, 100))
    	{
    		if (check == true)
    		{
    			document.forms[0].signature.focus();
    			check = false;
   			}
    		document.forms[0].signature.className="Error";
    	}
]]>
	<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">    /***
<![CDATA[
    /*   Check florist Comments if it exist
    */
    document.forms[0].floristComments.className="TblText";
    	var fcxnws = stripCharsInBag(document.forms[0].floristComments.value, bag2);
     	var fcgnws = stripWhitespace(fcxnws);

	if ((fcgnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(fcgnws)) || fieldExceedsMaxLength(document.forms[0].floristComments.value, 1000))
    	{
    		if (check == true)
    		{
    			document.forms[0].floristComments.focus();
    			check = false;
   			}
    		document.forms[0].floristComments.className="Error";
    	}
]]>
	</xsl:if>
<![CDATA[
    /***
    *   Check Order Comments Comments if it exist
    */
    document.forms[0].orderComments.className="TblText";
    	var ocxnws = stripCharsInBag(document.forms[0].orderComments.value, bag2);
     	var ocgnws = stripWhitespace(ocxnws);

	if ((ocgnws.length > 0 ]]> &amp;&amp; <![CDATA[ !isAlphanumeric(ocgnws)) || fieldExceedsMaxLength(document.forms[0].orderComments.value, 1000))
    	{
    		if (check == true)
    		{
    			document.forms[0].orderComments.focus();
    			check = false;
   			}
    		document.forms[0].orderComments.className="Error";
    	}




	/**
	 *	Check Last Minute email address not to be empty if lastMinuteCheckBox is clicked
	 */

	 	if ( document.all.lastMinuteCheckBox != null && document.all.lastMinuteCheckBox != null )
	 	{
	 		if ( document.all.lastMinuteCheckBox.checked )
	 		{
	 			if ( document.all.lastMinuteEmailAddress.value == "" )
	 			{
	// 				alert( "please provide last minute email address" );
	 				document.all.lastMinuteEmailAddress.style.backgroundColor='pink';
	 				check = false;
	 			}
	 		} else {
	 			document.all.lastMinuteEmailAddress.style.backgroundColor='white';
	 		}
	 	}

///////////////////////////////////////////////////////////////////////////
//INITIATE SERVER SIDE VALIDATIION
///////////////////////////////////////////////////////////////////////////

	if(check == true)
	{
]]>
    	<xsl:choose>
    	<xsl:when test="/root/recipient/@countryType != 'I'">
<![CDATA[
		addValidationEntry("QMS_ADDRESS1", enCode(deliveryInfoform.recipientAddress1.value))
		addValidationEntry("QMS_ADDRESS2", enCode(deliveryInfoform.recipientAddress2.value))
		addValidationEntry("QMS_CITY", enCode(deliveryInfoform.city.value))
		addValidationEntry("QMS_STATE", deliveryInfoform.stateSelect.value)
		addValidationEntry("QMS_ZIP", enCode(deliveryInfoform.zipCode.value))
                addValidationEntry("QMS_DELIVERY_METHOD", enCode(deliveryInfoform.deliveryMethod.value))
]]>
			<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or ((/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC') and /root/recipient/@countryType != 'I')">
<![CDATA[
				if(deliveryInfoform.floristCodex.value != "")
				{
					addValidationEntry("QMS_FLORIST", enCode(deliveryInfoform.floristCodex.value))
					addValidationEntry("QMS_PRODUCT", enCode(deliveryInfoform.productId.value))
				}
]]>
			</xsl:if>

   		<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
<![CDATA[

		addValidationEntry("QMS_FIRM_NAME", deliveryInfoform.institution.value)
]]>
		</xsl:if>
<![CDATA[

		callToServer();
]]>
		</xsl:when>
		<xsl:otherwise>
			<![CDATA[

			checkFieldLengths();
			deliveryInfoform.submit();
]]>
		</xsl:otherwise>
		</xsl:choose>
<![CDATA[
	}

	return check;
	}

	function confirmCountryChange()
    /***
    *   This function will check if changing country will effect domestic/International item orderd
    */
    {
]]>
	var cntry2 = "<xsl:value-of select="/root/recipient/@country"/>";
<![CDATA[
	if (checkForInternational(cntry2))
	{
		if(checkForInternational(document.forms[0].country.value))
		{
			if(checkItemCountry())
			{
				cancelItem();
			}
		} else {
			if(confirm("Changing from an international country to a domestic country will force you to go back and re-select. Make change and Re-Start process?"))
			{
				cancelItem();

			} else {
				document.all.country.value = cntry2;
			}

		}
	} else {
		if(checkForInternational(document.forms[0].country.value))
		{
]]>
		 	<xsl:choose>
			<xsl:when test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
<![CDATA[
				// openJCPPopup();
]]>
			</xsl:when>
			<xsl:otherwise>
<![CDATA[
				if(confirm("Changing from a domestic country to an International country will force you to go back and re-select. Make change and Re-Start process?"))
				{
					cancelItem();
				} else {
					document.all.country.value = cntry2;
				}
]]>
			</xsl:otherwise>
			</xsl:choose>

<![CDATA[
		} else {
			if(checkItemCountry())
			{
				cancelItem();
			}
		}
	}

	//Refresh the page if not going back to occasion page
	//in other words, if cancelItem() is not being executed
	//TWJ next 4 lines
	validZip = true;
	openUpdateWin()
	document.all.zipCode.value = "";
	refreshPage('zipChange')
	}

	function checkItemCountry()
    /***
    *   This function will check if item is only available for pre selected country
    */
	{
]]>
	var selectCntryOnlyItem = "<xsl:value-of select="/root/productData/data[@name = 'selectedCountryOnlyItem']/@value"/>";
	var cntry3 = "<xsl:value-of select="/root/recipient/@country"/>";
<![CDATA[
	if(selectCntryOnlyItem == "Y")
	{
		if(confirm("Selected item not avialable for new country. Changing will force you to go back and re-select. Make change and Re-Start process?"))
		{
			return true;
		} else {
			document.all.country.value = cntry3;
			return false;
		}
	} else {
		if(document.all.country.value == 'CA')
		{
			CANstateDropDown();
		} else {
			USstateDropDown();
		}

		return false;
	}

	}
	function cancelItem()
    /***
    *   This function will call the cancel Item servlet
    */
	{
	]]>
		target = "<xsl:value-of select="$cancelItemServlet"/>";

<![CDATA[

		var url = target + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&country=" + document.forms[0].country.value;
]]>
		<xsl:if test="/root/pageData/data[@name='cartItemNumber']/@value">
			var cartitem = "<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
<![CDATA[
			url = url.concat("&itemCartNumber=" + cartitem);

]]>
		</xsl:if>
<![CDATA[

		document.location = url;

	}

	function cancelItemToShopping()
    /***
    *   This function will call the cancel Item servlet
    */
	{

	]]>
		target = "<xsl:value-of select="$cancelItemShoppingServlet"/>";

<![CDATA[
		var url = target + "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId + "&fromPageName=ProductDetail" + "&zipCode=" + document.all.zipCode.value;
]]>
		<xsl:if test="/root/pageData/data[@name='cartItemNumber']/@value">
			var cartitem = "<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
<![CDATA[
			url = url.concat("&itemCartNumber=" + cartitem);

]]>
		</xsl:if>
<![CDATA[

		document.location = url;

	}

	function cancelItemToUpsell()
	{
		]]>
		var url = "<xsl:value-of select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>";
		<![CDATA[
		document.location = url;
	}

	function refreshPage(insource, focusto){
    /***
    *   This function sets up parameters to refresh page
    */

    //evaluate the response from the server side zip validation

    if (insource == 'zipChange' && validZip == false){
    	return false;
    }
    	if(bypassZipChange == false)
    	{
			var url = target
			url = url.concat(buildURL());
			url = url.concat("&source=" + insource);


			// Send the current element name through so we
			// can set focus to the next element when the page refreshes.
			if(insource == "zipChange")
			{
				if (focusto == 'deliveryLocation') {url = url.concat("&focusElement=" + "deliveryLocation");}
				else {url = url.concat("&focusElement=" + "country");}
			}
			else if(insource == "LocationChange")
			{

				url = url.concat("&focusElement=" + "recipientPhone");
				//alert(url);
			}


			document.location = url;
		}
		bypassZipChange = false;

		//close update window
		//closeUpdateWin();

	}

	function buildURL()
	{
	   	var fComments = "";
   		var flors = "";


]]>
    	<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
   			fComments = enCode(document.all.floristComments.value);
    	</xsl:if>

	<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or (/root/recipient/@countryType != 'I' and (/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC'))">
		flors = enCode(document.all.floristCodex.value);
	</xsl:if>

    	<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'N'">
    		populateDeliveryDate();
    	</xsl:if>


<![CDATA[
		var delMthd = document.all.deliveryMethod.value;
   		var delivDt = document.all.deliveryDate.value;
   		var delivLoc = document.all.deliveryLocation.value;
   		var recPhoneNum = document.all.recipientPhone.value;
   		var recPhoneExt = document.all.recipientPhoneExt.value;
   		var firstName = enCode(document.all.recipientFirstName.value);
   		var lastName = enCode(document.all.recipientLastName.value);
		var instit = "";
		var roomNum = "";
   		var addr1 = enCode(document.all.recipientAddress1.value);
   		var addr2 = enCode(document.all.recipientAddress2.value);
   		var fromWorkHrs = "";
   		var toWorkHrs = "";
   		var city = enCode(document.all.city.value);
   		var st = "";
   		var cntry = document.all.country.value;
   		var cntyType = document.all.countryType.value;
   		var gMessage = enCode(document.all.giftMessage.value);
   		var sign = enCode(document.all.signature.value);
   		var releaseName = "";
   		var oComments = enCode(document.all.orderComments.value);
   		var zipper = document.all.zipCode.value;
   		var lastMinuteCheckBox = "";
   		var cartItemNumber = "";
   		var searchType = document.all.searchType.value;
		var upsellFlag = document.all.upsellFlag.value;
		var MAXDeliveryDaysOut = document.all.MAXDeliveryDaysOut.value;

		var deliveryCost = "";
		if(document.all.deliveryCost != null)
		{
			deliveryCost = document.all.deliveryCost.value;
		}



   		if ( document.all.lastMinuteCheckBox != null && document.all.lastMinuteCheckBox.checked )
   			lastMinuteCheckBox = "Y";
   		else
   			lastMinuteCheckBox = "N";

   		var lastMinuteEmailAddress;
   		if(document.all.lastMinuteEmailAddress != null)
   		{
   			lastMinuteEmailAddress = document.all.lastMinuteEmailAddress.value;
   		}

]]>
		<xsl:if test="/root/pageData/data[@name='cartItemNumber']/@value">
			var cartItemNumber = "<xsl:value-of select="/root/pageData/data[@name='cartItemNumber']/@value"/>";
		</xsl:if>

	   	<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
	   	   	 instit = enCode(document.all.institution.value);
	   	</xsl:if>

	   	<xsl:if test="/root/pageData/data[@name='displayRoomNumber']/@value = 'Y'">
	   		roomNum = document.all.roomNumber.value;
	   	</xsl:if>

	   	<xsl:if test="/root/pageData/data[@name='displayWorkingHours']/@value = 'Y'">
	   	  	fromWorkHrs = document.all.fromWorkHours.value;
	   		toWorkHrs = document.all.toWorkHours.value;
	   	</xsl:if>

		<xsl:choose>
		<xsl:when test="/root/recipient/@countryType != 'I'">
			var st = document.all.stateSelect.value;
		</xsl:when>
	   	<xsl:otherwise>
			var st = document.all.stateText.value;
		</xsl:otherwise>
		</xsl:choose>

		<xsl:if test="/root/pageData/data[@name='displayReleaseCheckbox']/@value = 'Y'">
			if (document.all.releaseSendersNameCheckBox.checked) releaseName = 'Y'
			else releaseName = 'N';
	   	</xsl:if>

<![CDATA[

		var url = "?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId
		+ "&deliveryLocation=" + delivLoc + "&recipientPhone=" + recPhoneNum
		+ "&recipientPhoneExt=" + recPhoneExt + "&recipientFirstName=" + firstName
		+ "&recipientLastName=" + lastName + "&institution=" + instit
		+ "&roomNumber=" + roomNum + "&recipientAddress1=" + addr1
		+ "&recipientAddress2=" + addr2 + "&fromWorkHours=" + fromWorkHrs
		+ "&toWorkHours=" + toWorkHrs + "&city=" + city + "&state=" + st
		+ "&zipCode=" + zipper + "&country=" + cntry
		+ "&floristCode=" + flors + "&giftMessage=" + gMessage
		+ "&signature=" + sign + "&releaseSendersName=" + releaseName
		+ "&orderComments=" + oComments + "&floristComments=" + fComments + "&deliveryDate=" + delivDt
		+ "&deliveryMethod=" + delMthd + "&countryType=" + cntyType
		+ "&lastMinuteCheckBox=" + lastMinuteCheckBox
		+ "&cartItemNumber=" + cartItemNumber
		+ "&lastMinuteEmailAddress=" + lastMinuteEmailAddress
		+ "&upsellFlag=" + upsellFlag
		+ "&searchType=" + searchType
		+ "&deliveryCost=" + deliveryCost
		+ "&MAXDeliveryDaysOut=" + MAXDeliveryDaysOut;

		return url;
	}

        function spellCheck(value, name)
        {
            value = escape(value).replace(/\+/g,"%2B");
            var url = "SpellCheckServlet?content=" + value + "&callerName=" + name;

            window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300");
        }
	function enCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("%","g"),"%25" );
		strSelection = strSelection.replace(new RegExp("<","g"), "&lt;");
		strSelection = strSelection.replace(new RegExp(">","g"),"&gt;" );
		strSelection = strSelection.replace(new RegExp("#","g"),"%23" );
		strSelection = strSelection.replace(new RegExp("&","g"),"%26" );
		strSelection = strSelection.replace(new RegExp("\"","g"),"\\%22" );

		return strSelection;
	}
	function deCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("%23","g"),"#" );

		return strSelection;
	}



	function submitForm()
    /***
    *   This function will validate the form and submit to the servlet if no errors found
    */
    {
    if(document.all.releaseSendersNameCheckBox != null)
    {
      if (document.all.releaseSendersNameCheckBox.checked)
        document.all.releaseSendersName.value = 'Y'
      else
        document.all.releaseSendersName.value = 'N';
    }

		if(backupFlag == true)
		{
			backToShopping();
			return;
		}

    if(disableFlag == false)
    {
]]>
	    <xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'N'">
<![CDATA[
      populateDeliveryDate();
]]>
   		</xsl:if>
   		<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
			var selectBox = document.all.deliveryDate;
			if(selectBox.selectedIndex > -1)
				setDeliveryDateDisplay(selectBox.options[selectBox.selectedIndex].displayDate, selectBox.options[selectBox.selectedIndex].day);
   		</xsl:if>
<![CDATA[

		// Send the state as a consistant "state" field
]]>
		<xsl:choose>
    	<xsl:when test="/root/recipient/@countryType != 'I'">
<![CDATA[
			document.all.state.value = document.all.stateSelect.value;
]]>
      </xsl:when>
      <xsl:otherwise>
<![CDATA[
			document.all.state.value = document.all.stateText.value;
]]>
      </xsl:otherwise>
		</xsl:choose>

    	<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or ((/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC') and /root/recipient/@countryType != 'I')">
			document.all.floristCode.value = document.all.floristCodex.value;
		</xsl:if>
<![CDATA[


      if(validateForm())
      {
      }
      else
      {
    		closeUpdateWin();
      	alert("Please correct the marked fields.");
  		}
    }
	} //end function

	function openRecipientLookup()
	{

		//hide the dropdowns
		hideDIVs()

		//In case the DIV is opened a second time
		document.forms[0].phoneInput.style.backgroundColor='white';


		var ie=document.all;
		var dom=document.getElementById;
		var ns4=document.layers;
		HEIGHT = 100;
		WIDTH = 290;

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155;
		recipientsearch = (dom)?document.getElementById("recipientLookup").style : ie? document.all.recipientLookup : document.recipientLookup;
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset;

		recipientsearch.width = WIDTH;
		recipientsearch.height = HEIGHT;

		recipientsearch.left=document.body.clientWidth/2 - WIDTH/2;
		recipientsearch.top=scroll_top + document.body.clientHeight/2 - HEIGHT/2;

		recipientsearch.visibility=(dom||ie)? "visible" : "show";

]]>
		<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
			document.all.institutionInput.value = document.all.institution.value;
			document.forms[0].cityInput.style.backgroundColor='white';
			document.forms[0].stateInput.style.backgroundColor='white';
			document.forms[0].zipCodeInput.style.backgroundColor='white';
		</xsl:if>
<![CDATA[
		//Populate inputs in the DIV if they are given on the main page
		document.all.phoneInput.value = document.all.recipientPhone.value;
		// document.all.addressInput.value = document.all.recipientAddress1.value;
		document.all.cityInput.value = document.all.city.value;

		//Take the state from the proper input
		//if (document.all.countryType.value == "D")
		//	document.all.stateInput.value = document.all.stateSelect.value;
		//else
		//	document.all.stateInput.value = document.all.stateText.value;

		document.all.zipCodeInput.value = document.all.zipCode.value;

		institutionType = document.all.deliveryLocation.value;

		//set the focus on the first field
		if ((institutionType == "F") | (institutionType == "H") | (institutionType == "N"))
		{
			//Take the state from the proper input
			if (document.all.countryType.value == "D")
				document.all.stateInput.value = document.all.stateSelect.value;
			else
				document.all.stateInput.value = document.all.stateText.value;

			document.forms[0].institutionInput.focus();
		}
		else
		{
			document.forms[0].phoneInput.focus();
		}
	}

	function openFloristLookup()
	{
		//hide the dropdowns
		hideDIVs();

		//In case the DIV is opened a second time
		document.forms[0].floristBusinessInput.style.backgroundColor='white';
		document.forms[0].floristCityInput.style.backgroundColor='white';
		document.forms[0].floristStateInput.style.backgroundColor='white';
		document.forms[0].floristZipInput.style.backgroundColor='white';
		document.forms[0].floristPhoneInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
		HEIGHT = 100;
		WIDTH = 290;

		cen=(ie)? document.body.clientWidth/2-160 : (dom)?window.innerWidth/2-160  : window.innerWidth/2-160;
		floristsearch = (dom)?document.getElementById("floristLookup").style : ie? document.all.floristLookup : document.floristLookup;
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset;

		floristsearch.width = WIDTH;
		floristsearch.height = HEIGHT;

		floristsearch.top=scroll_top+document.body.clientHeight/2 - HEIGHT/2;
		floristsearch.left=document.body.clientWidth/2 - WIDTH/2;

		floristsearch.visibility=(dom||ie)? "visible" : "show";

]]>
		<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
			document.all.institutionInput.value = document.all.institution.value;
		</xsl:if>
<![CDATA[

		//Populate inputs in the DIV if they are given on the main page

		//Take the state from the proper input
		if (document.all.countryType.value == "D")
			document.all.floristStateInput.value = document.all.stateSelect.value;
		else
			document.all.floristStateInput.value = document.all.stateText.value;

		document.all.floristZipInput.value = document.all.zipCode.value;
		document.all.floristCityInput.value = document.all.city.value;

		//set the focus on the first field
		document.forms[0].floristBusinessInput.focus();
	}

	function goCancelRecipient()
	{
		recipientsearch.visibility= "hidden";
		showDIVs()
	}

	function goSearchRecipient()
	{
		openRecipientPopup();
	}

	function goCancelFlorist()
	{
		floristsearch.visibility= "hidden";
		showDIVs();
	}

	function goSearchFlorist()
	{
		openFloristPopup();
	}

	function openRecipientPopup()
	{
	//show the dropdowns
	showDIVs()

	//initialize the background colors
        document.forms[0].institutionInput.style.backgroundColor='white';
        document.forms[0].addressInput.style.backgroundColor='white';
        document.forms[0].phoneInput.style.backgroundColor='white';
        document.forms[0].cityInput.style.backgroundColor='white';
        document.forms[0].stateInput.style.backgroundColor='white';
        document.forms[0].zipCodeInput.style.backgroundColor='white';

	    //First Validate that the DIV inputs are valid
        check = true;
        phone = stripWhitespace(document.forms[0].phoneInput.value);
        institutionType = document.forms[0].deliveryLocation.value;

		//institutionInput may be hidden
		if ((institutionType == "F") | (institutionType == "H") | (institutionType == "N"))
		{
	       institution = stripWhitespace(document.forms[0].institutionInput.value);
 		}

        address = stripWhitespace(document.forms[0].addressInput.value);
        city = stripWhitespace(document.forms[0].cityInput.value);
        state = stripWhitespace(document.forms[0].stateInput.value);
        zip = stripWhitespace(document.forms[0].zipCodeInput.value);
   	 	countryType = stripWhitespace(document.forms[0].countryType.value);

     //For certain institution types, institution name is required
     if ((institutionType == "F") | (institutionType == "H") | (institutionType == "N"))
     {


		 if (city.length == 0)
	 	 {
	 	 	document.forms[0].cityInput.style.backgroundColor='pink';
			if (check == true)
			{
			  document.forms[0].cityInput.focus();
   		 	  check = false;
			}
	     }  // close city if

	     if (state.length == 0)
	 	 {
	 	 	document.forms[0].stateInput.style.backgroundColor='pink';
			if (check == true)
			{
			  document.forms[0].stateInput.focus();
		 	  check = false;
			}
  	     }  // close state if


   	 } // close institution outer if

   	 else {
   	 	if (phone.length == 0)
	 	{
	 		alert("Please correct the marked fields");
	 		document.forms[0].phoneInput.style.backgroundColor='pink';
	 		document.forms[0].phoneInput.focus();
			return false;
	 	}
   	 }







	 if (!check)
	 {
		alert("Please correct the marked fields");
		return false;
	 }

		//Every is valid, so the popup can be opened
		document.all.recipientLookupSearching.style.visibility = "visible";

		var form = document.forms[0];
	    var url_source="PopupServlet?POPUP_ID=LOOKUP_RECIPIENT" +
						"&institutionInput=" + document.all.institutionInput.value +
						"&cityInput=" + document.all.cityInput.value +
						"&stateInput=" + document.all.stateInput.value +
						"&countryTypeInput=" + document.all.country.value +
						"&institutionTypeInput=" + document.all.deliveryLocation.value;

		//institutionType = document.forms[0].deliveryLocation.value;
		if ((institutionType != "F") && (institutionType != "H") && (institutionType != "N"))
     		{
     		 url_source = url_source + "&phoneInput=" + document.all.phoneInput.value
     		}

	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	    //Open the popup
 	     var ret = window.showModalDialog(url_source,"" , modal_dim);

		//in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 	     	ret[0] = '';
 		}

 		//Populate the text boxes if the ret value has something in it
 		if (ret[0] != '')
 		{
			document.all.recipientId.value = ret[0];
			if ((institutionType == "R") | (institutionType == "B") | (institutionType == "O"))
		        {
				document.all.recipientFirstName.value = ret[1];
				document.all.recipientLastName.value = ret[2];
			}
			document.all.recipientAddress1.value = ret[3];
			document.all.recipientAddress2.value = ret[4];
			document.all.city.value = ret[5];

			//Populate the correct state input
			//alert(document.all.countryType.value)
			if ( document.all.countryType.value == "D" )
			{
				document.all.stateSelect.value = ret[6];
			}
			else
			{
				document.all.stateText.value = ret[6];
			}

			//Grab the first 5 digits of the zip code
			zip = ret[7];
			zip5Digit = zip.substring(0,5);

			//Disable the continue button
			//and for domestic enable the click to accept button
			if ( document.all.zipCode.value != zip &&
				 document.all.zipCode.value != zip5Digit )
			{
				disableContinue();
				if ( document.all.countryType.value == "D" )
				{
					enableClickToAccept();
				}

			}
			document.all.zipCode.value = zip5Digit;

			if ((institutionType != "F") && (institutionType != "H") && (institutionType != "N"))
			{
				document.all.country.value = ret[8];
			}

			if ( ret[9] != '' )
			{
				document.all.recipientPhone.value = ret[9];
	        }
			else
			{
				document.all.recipientPhone.value = ret[10];
				document.all.recipientPhoneExt.value = ret[11];
			}

			]]>
			<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
				document.all.institution.value = ret[13];
			</xsl:if>
			<![CDATA[
		}

		recipientsearch.visibility= "hidden";
		document.all.recipientLookupSearching.style.visibility = "hidden";

		//set the focus on the next field
		document.all.recipientFirstName.focus();
	}

function openFloristPopup()
	{
	//show the dropdowns;
	showDIVs()

	//First validate that the inputs are valid
        check = true;
		business = stripWhitespace(document.forms[0].floristBusinessInput.value);
        city = stripWhitespace(document.forms[0].floristCityInput.value);
        state = stripWhitespace(document.forms[0].floristStateInput.value);
        zip = stripWhitespace(document.forms[0].floristZipInput.value);
        phone = stripWhitespace(document.forms[0].floristPhoneInput.value);
        countryType = stripWhitespace(document.forms[0].countryType.value);

	 //state is required if business is given
	 if ((business.length > 0) ]]> &amp; <![CDATA[(state.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].floristStateInput.focus();
	 	  check = false;
		}
		document.forms[0].floristStateInput.style.backgroundColor='pink';
     }  // close state if

    //city is required if no zip and phone are given
	 if ((city.length == 0) ]]>&amp; <![CDATA[(zip.length == 0)]]> &amp; <![CDATA[(phone.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].floristCityInput.focus();
	 	  check = false;
		}
		document.forms[0].floristCityInput.style.backgroundColor='pink';
     }  // close city if

     //zip is required if no city, state, and phone are given
	 if ((zip.length == 0) ]]>&amp; <![CDATA[(city.length == 0) ]]>&amp; <![CDATA[(state.length == 0) ]]>&amp; <![CDATA[(phone.length == 0))
 	 {
		if (check == true)
		{
		  document.forms[0].floristZipInput.focus();
	 	  check = false;
		}
		  document.forms[0].floristZipInput.style.backgroundColor='pink';
     }  // close zip if

	 if (!check)
	 {
	 	alert("Please correct the marked fields")
	 	return false;
	 }
		//Now that everything is valid, open the popup
		document.all.floristLookupSearching.style.visibility = "visible";

		productId = document.forms[0].productId.value;

		var form = document.forms[0];
	    var url_source="PopupServlet?POPUP_ID=LOOKUP_FLORIST" +
	    "&institutionInput=" + document.all.floristBusinessInput.value +
	    "&phoneInput=" + document.all.floristPhoneInput.value +
        "&addressInput=" + document.all.floristAddressInput.value +
        "&cityInput=" + document.all.floristCityInput.value +
        "&stateInput=" + document.all.floristStateInput.value +
        "&zipCodeInput=" + document.all.floristZipInput.value +
        "&countryTypeInput=" + document.all.country.value +
        "&productId=" + productId;

	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";

	    //Open the popup
 	    var ret = window.showModalDialog(url_source,"", modal_dim);

		//in case the X icon is clicked
		if (!ret)
			ret = '';

		if (ret != '')
			document.all.floristCodex.value = ret;

		floristsearch.visibility= "hidden";
		document.all.floristLookupSearching.style.visibility = "hidden";

		//Set the focus on the next field
		document.all.giftMessage.focus();
	}

	function populateDeliveryDate()
	{
		var radioGrp = document.forms[0].deliveryMethodx;

		if(radioGrp != null && radioGrp[0] != null)
		{
			for (var i=0; i < radioGrp.length; i++)
			{
				if(radioGrp[i].checked )
				{
					document.all.deliveryMethod.value = radioGrp[i].value;
					getDeliveryDate(document.all.deliveryMethod.value);
					populateDeliveryCost(document.all.deliveryMethod.value);
				}
			}
		}
		else if(radioGrp != null)
		{
		  	document.all.deliveryMethod.value = document.all.deliveryMethodx.value;
		  	getDeliveryDate(document.all.deliveryMethod.value);
		  	populateDeliveryCost(document.all.deliveryMethod.value);
		}
	}

	function getDeliveryDate(inMethod)
	{
		if ( inMethod == "Next Day Delivery" )
		{
			document.all.deliveryDate.value = document.all.nextDayDate.value;
		}
		else if ( inMethod == "Standard Delivery" )
		{
			document.all.deliveryDate.value = document.all.standardDate.value;
		}
		else if ( inMethod == "Two Day Delivery" )
		{
			document.all.deliveryDate.value = document.all.twoDayDate.value;
		}
		else if ( inMethod == "Saturday Delivery" )
		{
			document.all.deliveryDate.value = document.all.saturdayDate.value;
		}
		else if ( inMethod == "Florist Delivery" )
		{
			document.all.deliveryDate.value = document.all.floristDate.value;
			//alert(document.all.floristDate.options[document.all.floristDate.selectedIndex].text);
      var stringDeliveryDateDisplay;
          stringDeliveryDateDisplay = document.all.floristDate.options[document.all.floristDate.selectedIndex].day +
                                      " " +
                                      document.all.floristDate.options[document.all.floristDate.selectedIndex].displayDate;

			document.all.deliveryDateDisplay.value = stringDeliveryDateDisplay;
		}
	}

	function populateDeliveryCost(inMethod)
	{
]]>
		<xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">
			var tmpMethod = "<xsl:value-of select="@description"/>";
			var tmpCost = "<xsl:value-of select="@cost"/>";
<![CDATA[
			if(tmpMethod == inMethod)
			{
				document.all.deliveryCost.value = tmpCost;
			}
]]>
		</xsl:for-each>

<![CDATA[

	}

	/****  initialize delivery method and date
	*/
	function initializeDeliveryDate(inType)
	{
]]>
		var inDate = "<xsl:value-of select="/root/deliveryDetail/@requestedDeliveryDate"/>";
<![CDATA[
		var dateExist = false;

		if ( inDate == "" )
		{
			dateExist = true;
		}
		else if ( inType == "Next Day Delivery" )
		{
			document.all.nextDayDate.value = inDate;
			if ((document.forms[0].nextDayDate.value != "") &&
				(document.forms[0].nextDayDate.value != " "))
			{
				dateExist = true;
			}
		}
		else if ( inType == "Standard Delivery" )
		{
			document.all.standardDate.value = inDate;
			if ((document.forms[0].standardDate.value != "") &&
				(document.forms[0].standardDate.value != " "))
			{
				dateExist = true;
			}
		}
		else if ( inType == "Two Day Delivery" )
		{
			document.all.twoDayDate.value = inDate;
			if ((document.forms[0].twoDayDate.value != "") &&
				(document.forms[0].twoDayDate.value != " "))
			{
				dateExist = true;
			}
		}
		else if ( inType == "Saturday Delivery" )
		{
			document.all.saturdayDate.value = inDate;
			if ((document.forms[0].saturdayDate.value != "") &&
				(document.forms[0].saturdayDate.value != " "))
			{
				dateExist = true;
			}
		}
		else if ( inType == "Florist Delivery" )
		{
			document.all.floristDate.value = inDate;
			if ((document.forms[0].floristDate.value != "") &&
				(document.forms[0].floristDate.value != " "))
			{
				dateExist = true;
			}
		}

		return dateExist;
	}

	function disableContinue()
	{
	/*** Change image of continue button to disabled and prevent submitting the page
	*/
	//but only if the country is domestic and the key stroke isn't a tab
	 if (window.event.keyCode != 9)
	  {
]]>
		<xsl:if test="/root/recipient/@countryType = 'D'">
<![CDATA[
			disableFlag = true;
			document.images['continueButton'].src  = '../images/button_add_to_cart_disabled.gif';
]]>
		</xsl:if>
<![CDATA[
          }
	}
  	function doMAXDeliveryDaysOut()
 	{
  	    document.forms[0].MAXDeliveryDaysOut.value = "true";
 	    disableRefreshFlag = false;
  	    validateZip('getMaxDays');
  	}

	function clickToAccept()
	{
		if(disableRefreshFlag == false)
		{
			enableContinue();
			validateZip('zip');
		}
	}

	function enableContinue()
	{
	/*** Change image of continue button to enabled and allow submitting the page
	*/
	//but only if domestic
	]]>
	<xsl:if test="/root/recipient[@countryType='D']">
	<![CDATA[
			disableFlag = false;
			document.images['continueButton'].src  = '../images/button_add_to_cart.gif';
	]]>
	</xsl:if>
	<![CDATA[
	}

	function goCancelZipCode()
	{
		zipcodesearch.visibility= "hidden";
		showDIVs();
	}

	function goSearchZipCode()
	{
		openZipCodePopup();
	}

	function openZipCodeLookup()
	{
		//hide the dropdowns
		hideDIVs();

		//In case its opened a second time
		document.all.stateInputLookup.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers
		HEIGHT = 100;
		WIDTH = 290;

		popupFlag=true;
		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		zipcodesearch = (dom)?document.getElementById("zipCodeLookup").style : ie? document.all.zipCodeLookup : document.zipCodeLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset

		zipcodesearch.width = WIDTH
		zipcodesearch.height = HEIGHT

		zipcodesearch.top=scroll_top + document.body.clientHeight/2 - HEIGHT/2;
		zipcodesearch.left=document.body.clientWidth/2 - WIDTH/2;

		zipcodesearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the input and set the focus
		document.all.cityInputLookup.value = document.all.city.value;

		//Take the state from either the dropdown or the text box
		if(document.all.countryType.value == "D")
			document.all.stateInputLookup.value = document.all.stateSelect.value;
		else
			document.all.stateInputLookup.value = document.all.stateText.value;

		document.all.zipCodeInputLookup.value = document.all.zipCode.value;
		document.all.cityInputLookup.focus();
		enableClickToAccept();

	}

	function openZipCodePopup()
	{
	//show the dropdowns;
	showDIVs()

	//initialize the background colors
	document.all.cityInputLookup.style.backgroundColor='white';
	document.all.stateInputLookup.style.backgroundColor='white';
	document.all.zipCodeInputLookup.style.backgroundColor='white';

	//First validate the inputs
	check = true;

        state = stripWhitespace(document.all.stateInputLookup.value);
        city = stripWhitespace(document.all.cityInputLookup.value);
        zip = stripWhitespace(document.forms[0].zipCodeInputLookup.value);

        //State is required if zip is empty
        if((state.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.stateInputLookup.focus();
		 	  check = false;
			}
			  document.all.stateInputLookup.style.backgroundColor='pink';
		} // close state if

		//City is required if zip is empty
        if((city.length == 0) 	]]>&amp; <![CDATA[ (zip.length == 0))
        {
	        if (check == true)
			{
			  document.all.cityInputLookup.focus();
		 	  check = false;
			}
			  document.all.cityInputLookup.style.backgroundColor='pink';
		} // close state if

		if (!check)
		{
			alert("Please correct the marked fields");
		 	return false;
		}

        //Now that everything is valid, open the popup
		var cityVal = document.all.cityInputLookup.value;
		var stateVal = document.all.stateInputLookup.value;
		var zipVal = document.all.zipCodeInputLookup.value;
		var countryVal = document.all.country.value;

		document.all.zipCodeLookupSearching.style.visibility = "visible";

		var form = document.forms[0];
	    var url_source="PopupServlet?POPUP_ID=LOOKUP_ZIP_CODE" +
	    "&cityInput=" + cityVal +
	    "&stateInput=" + stateVal +
	    "&zipCodeInput=" + zipVal +
	    "&countryVal=" + countryVal;

	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
 	    var ret = window.showModalDialog(url_source,"", modal_dim);

 	    //in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 			ret[0] = '';
 			ret[1] = '';
 			ret[2] = '';
 		}

		//Populate the text boxes if the ret value has something in it
 		if (ret[0] != '')
		{
			document.all.city.value = ret[0];

			//only change the zip if a different zip is selected
			if (document.all.zipCode.value != ret[1])
				document.all.zipCode.value = ret[1];

			//Populate the state dropdown for domestic or for international populate the state text box
			if ((document.all.country.value == "US") | (document.all.country.value == "CA") | (document.all.country.value == "PR") | (document.all.country.value == "VI"))
				document.all.stateSelect.value = ret[2];
			else
				document.all.stateText.value = ret[2];
		}

		zipcodesearch.visibility= "hidden";
		document.all.zipCodeLookupSearching.style.visibility = "hidden";

		//Set the focus on the next field
		document.all.country.focus();

		enableClickToAccept();
	}

	/*** Change image of click to accept button to disabled and prevent page refresh
	*/
	function disableClickToAccept()
	{
		//but only if domestic
	]]>
		<xsl:if test="/root/recipient[@countryType='D']">
			<![CDATA[
				disableRefreshFlag = true;
				document.all.clickToAcceptButton.src  = '../images/button_click_to_accept_disabled.gif';
			]]>
		</xsl:if>
	<![CDATA[
}

	function setDeliveryDateDisplay(newDate, newDay)
	{
    var stringDeliveryDateDisplay = newDay + " " + newDate;
		document.all.deliveryDateDisplay.value = stringDeliveryDateDisplay;

	}

function enableClickToAccept()
{
	if (window.event != null && window.event.keyCode != 9)
	{
	]]>
		<xsl:if test="/root/recipient[@countryType='D']">
		<![CDATA[
		disableRefreshFlag = false;
		document.all.clickToAcceptButton.src  = '../images/button_click_to_accept.gif';
		]]>
		</xsl:if>
	<![CDATA[
	}
}


]]>

	var sessionId = "<xsl:value-of select="$sessionId"/>";
 	var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
	var searchType = "<xsl:value-of select="$searchType"/>";
	var upsellFlag = "<xsl:value-of select="$upsellFlag"/>";

</SCRIPT>
<HTML>
<HEAD>
	<TITLE> FTD - Delivery Information </TITLE>

<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
	<FORM name="deliveryInfoform" method="get" action="{$actionServlet}">
		<INPUT name="countryType" type="hidden" value=""/>
		<input type="hidden" name="deliveryDateDisplay" value=""/>
		<INPUT name="recipientId" type="hidden" value=""/>
		<INPUT name="state" type="hidden" value=""/>
		<INPUT name="cartItemNumber" type="hidden" value=""/>
		<INPUT name="QMSAddress1" type="hidden" value="" maxlength="30"/>
  		<INPUT name="QMSAddress2" type="hidden" value="" maxlength="30"/>
  		<INPUT name="QMSCity" type="hidden" value="" maxlength="30"/>
		<INPUT name="QMSState" type="hidden" value="" maxlength="2"/>
  		<INPUT name="QMSZipCode" type="hidden" value="" maxlength="10"/>
  		<INPUT name="QMSMatchCode" type="hidden" value="" maxlength="3"/>
  		<INPUT name="QMSFirmName" type="hidden" value="" maxlength="50"/>
  		<INPUT name="QMSLatitude" type="hidden" value="" maxlength="15"/>
  		<INPUT name="QMSLongitude" type="hidden" value="" maxlength="15"/>
  		<INPUT name="QMSRangeRecordType" type="hidden" value="" maxlength="1"/>
  		<INPUT name="QMSOverrideFlag" type="hidden" value=""/>
		<INPUT type="hidden" name="searchType" value="{$searchType}"/>
		<INPUT type="hidden" name="upsellFlag" value="{$upsellFlag}"/>
 		<INPUT type="hidden" name="MAXDeliveryDaysOut" value="false"/>
		<INPUT type="hidden" name="productId">
			<xsl:attribute name="value"><xsl:value-of select="/root/productData/data[@name = 'productId']/@value"/></xsl:attribute>
		</INPUT>

		<CENTER>

		<xsl:call-template name="header"></xsl:call-template><br></br>

   		<xsl:variable name="occasionlbl" select="/root/previousPages/previousPage[@name = 'Occasion']/@value"/>
    		<xsl:variable name="productlist" select="/root/previousPages/previousPage[@name = 'Product List']/@value"/>
    		<xsl:variable name="productDetail" select="/root/previousPages/previousPage[@name = 'Product Detail']/@value"/>
   		<xsl:variable name="shopping" select="/root/previousPages/previousPage[@name = 'NEW Search']/@value"/>
   		<xsl:variable name="customOrder" select="/root/previousPages/previousPage[@name = 'Custom Item']/@value"/>
   		<xsl:variable name="upselldetail" select="/root/previousPages/previousPage[@name = 'Upsell Detail']/@value"/>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD width="65%"> &nbsp; </TD>
				<xsl:choose>
					<xsl:when test="$occasionlbl != ''">
						<TD align="right" valign="top"><A tabindex ="-1" href="javascript:doShoppingCart()" STYLE="text-decoration:none" ><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/>Shopping Cart</A> </TD>
					</xsl:when>
					<xsl:otherwise>
				    	<TD align="right" valign="top"><A tabindex ="-1" STYLE="text-decoration:none" ><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/><font color="#C0C0C0">Shopping Cart</font></A> </TD>
					</xsl:otherwise>
				</xsl:choose>
				<TD align="right" valign="top"><A tabindex ="-1" href="javascript:doCancelOrder()" STYLE="text-decoration:none" >Cancel Order</A> </TD>
			</TR>
			<TR>
				<TD colspan="3"> <xsl:call-template name="customerHeader"/>&nbsp; </TD>
			</TR>
		</TABLE>

		<TABLE border="0" width="98%" cellspacing="0" cellpadding="0">
     		<TR  class="tblheader">
				<xsl:if test="$occasionlbl != ''">
					&nbsp;
     				<a tabindex ="-1" href="{$occasionlbl}" class="tblheader">Occasion</a>&nbsp;>
					<xsl:choose>
						<xsl:when test="$breadCrumbType = 'I'"> <!-- International -->
			     			<a tabindex ="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
			     			<xsl:if test= "$upsellFlag = 'Y'">
								<a tabindex ="-1" href="{$upselldetail}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
							</xsl:if>
			     			<a tabindex ="-1" href="{$productDetail}" class="tblheader">Product Detail</a>
						</xsl:when>
						<xsl:when test="$breadCrumbType = 'D'"> <!-- Domestic -->
		     				<a tabindex ="-1" href="{$shopping}" class="tblheader">Categories</a>&nbsp;>
		     				<xsl:if test= "$searchType != 'simpleproductsearch'">
								<a tabindex ="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
							</xsl:if>
							<xsl:if test= "$upsellFlag = 'Y'">
								<a tabindex ="-1" href="{$upselldetail}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
							</xsl:if>
		     				<a tabindex ="-1" href="{$productDetail}" class="tblheader">Product Detail</a>
		     			</xsl:when>
						<xsl:otherwise> <!-- Custome Order -->
			     			<a tabindex ="-1" href="{$shopping}" class="tblheader">Categories</a>&nbsp;>
			     			<a tabindex ="-1" href="{$customOrder}" class="tblheader">Custom Order</a>
			     		</xsl:otherwise>
		     		</xsl:choose>
	     			&nbsp;> Delivery Information
	     		</xsl:if>
      		</TR>
 		</TABLE>

		<!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD>
								<TABLE width="100%" border="0" cellpadding="1" cellspacing="10">
									<TR>
										<TD colspan="3">
											<SPAN class="header">
												<script><![CDATA[
													document.write(deCode("]]> <xsl:value-of select="/root/productData/data[@name = 'novatorName']/@value"/> <![CDATA["));]]>
						 						</script>
				 						    </SPAN>
											<BR> </BR>
											<SPAN class="header2"> <xsl:value-of select="/root/productData/data[@name = 'productId']/@value"/> </SPAN>
										</TD>
									</TR>

									<TR>
										<TD colspan="3"> <HR size="5" color="#006699"/> </TD>
									</TR>

									<xsl:choose>
										<xsl:when test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
											<INPUT tabindex ="1" type="hidden" name="deliveryMethod" value="florist"/>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DELIVERYDATE']/@scriptText"/>
 													</TD>
											</TR>

											<TR>
												<TD width="25%" class="labelright"> Delivery Date </TD>
												<TD width="35%" >
													<DIV id="deliveryDIV">
														<SELECT tabindex ="2" name="deliveryDate" onchange="setDeliveryDateDisplay(document.all.deliveryDate.options[document.all.deliveryDate.selectedIndex].displayDate, document.all.deliveryDate.options[document.all.deliveryDate.selectedIndex].day);">
															<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
                                <OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}">  <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/></OPTION>
															</xsl:for-each>
														</SELECT>
														&nbsp;&nbsp;
														<IMG onkeydown="javascript:EnterToCalendarPopup()" tabindex ="3" src="../images/show-calendar.gif" onclick="javascript:openCalendarPopup();"></IMG>
														&nbsp; <SPAN style="color:red"> *** </SPAN>
 	<xsl:if test="/root/selectedData/data[@name='deliveryDaysOut']/@value = 'MIN'">
 		<input type ="button" tabindex="4" name="getMaxDeliveryDate" value="Get Max" onclick="javascript:doMAXDeliveryDaysOut();" />
 	</xsl:if>
 	<xsl:if test="/root/selectedData/data[@name='deliveryDaysOut']/@value = 'MAX'">
 		<input type ="button" tabindex="4" name="getMaxDeliveryDate" value="Get Max" disabled="true"/>
 	</xsl:if>
													</DIV>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DELIVERYDATE']/@instructionText"/>
												</TD>
											</TR>

											<xsl:if test="/root/exceptionData/data[@name='exceptionCode']/@value='Y'">
												<TR>
													<TD> &nbsp; </TD>
													<TD colspan="2" class="labelleft"> <font size="2" color="blue"> This product is only available from&nbsp;<xsl:value-of select="/root/exceptionData/data[@name='exceptionStartDate']/@value"/>&nbsp;to&nbsp;<xsl:value-of select="/root/exceptionData/data[@name='exceptionEndDate']/@value"/></font> </TD>
												</TR>
											</xsl:if>

											<TR>
												<TD> &nbsp; </TD>
												<TD colspan="2" >
													<DIV id="deliveryDateChangeDIV">
														<SPAN style="color='red'; font-size=10pt">
															Requested Delivery date not available please select new date.
														</SPAN>
													</DIV>
												</TD>
											</TR>
										</xsl:when>

										<xsl:when test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'N'">

											<INPUT type="hidden" name="deliveryDate" value=""/>
											<INPUT type="hidden" name="deliveryMethod" value=""/>
											<INPUT type="hidden" name="deliveryCost" value=""/>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DELIVERYDATE']/@scriptText"/>
												</TD>
											</TR>

											<TR>
												<TD width="25%" class="labelright" valign="top"> Requested Delivery Date </TD>
												<TD colspan="2" valign="top">
													<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
														<TR>
															<TD>
																<SPAN style="color='red'">
																	<xsl:value-of select="/root/deliveryDetail/@requestedDeliveryDate"/>
																</SPAN>
																&nbsp; <SPAN style="color:red"> *** </SPAN>
															</TD>
														</TR>

														<SCRIPT language="JavaScript">
															var tabord = 4;
														</SCRIPT>

														<DIV id="methodsDIV">
															<xsl:for-each select="/root/shippingData/shippingMethods/shippingMethod/method">
																<TR>
																	<TD width="25%" class="tblText" valign="top">
																		<xsl:choose>
																			<xsl:when test="@description = /root/deliveryDetail/@deliveryMethod">
																				<SCRIPT language="JavaScript">
		 																			var pid = "<xsl:value-of select="@description"/>";
		 																			<![CDATA[
		 																				document.write("<INPUT onclick=\"deliveryMethodXCheck(this);\" type=\"radio\" name=\"deliveryMethodx\" value=\"" + pid + "\" CHECKED/>")
		 																				tabord++;
																					]]>
																				</SCRIPT>
																			</xsl:when>
																			<xsl:otherwise>
																				<SCRIPT language="JavaScript">
																					var pid = "<xsl:value-of select="@description"/>";
																					<![CDATA[
																						document.write("<INPUT onclick=\"deliveryMethodXCheck(this);\" tabindex=\"1\" type=\"radio\" name=\"deliveryMethodx\" value=\"" + pid + "\"/>")
																					]]>
																				</SCRIPT>
																			</xsl:otherwise>
																		</xsl:choose>
																		<xsl:value-of select="@description"/>
																	</TD>

																	<TD width="10%" align="right">
																		<xsl:if test="@costWithSurcharge != ''">
																			$<xsl:value-of select="@costWithSurcharge"/>
																		</xsl:if>
																	</TD>

																	<xsl:choose>
																		<xsl:when test="@code = 'ND' or @code = 'SA'">
																			<TD width="25%" class="tblTextBold" align="right"> Will be delivered on &nbsp; &nbsp;</TD>
																		</xsl:when>
																		<xsl:otherwise>
																			<TD width="25%" class="tblTextBold" align="right"> Arrives no later than &nbsp; </TD>
																		</xsl:otherwise>
																	</xsl:choose>

																	<TD width="40%">
																		<xsl:choose>
																			<xsl:when test="@code = 'ND'">
																				<SELECT tabindex ="14" name="nextDayDate">
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<xsl:if test="contains(@types, 'ND')">
																							<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"> <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																						</xsl:if>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:when>

																			<xsl:when test="@code = '2D'">
																				<SELECT tabindex ="15" name="twoDayDate" >
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<xsl:if test="contains(@types, '2D')">
																							<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"> <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																						</xsl:if>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:when>

																			<xsl:when test="@code = 'GR'">
																				<SELECT tabindex ="13" name="standardDate" >
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<xsl:if test="contains(@types, 'GR')">
																							<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"><xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																						</xsl:if>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:when>

																			<xsl:when test="@code = 'SA'">
																				<SELECT tabindex ="12" name="saturdayDate" >
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<xsl:if test="contains(@types, 'SA')">
																							<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"> <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																						</xsl:if>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:when>

																			<xsl:when test="@code = 'FL'">
																				<SELECT tabindex ="16" name="floristDate" >
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<xsl:if test="contains(@types, 'FL')">
																							<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"><xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																						</xsl:if>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:when>

																			<xsl:otherwise>
																				<SELECT tabindex ="16" name="otherDate" >
																					<xsl:for-each select="/root/shippingData/deliveryDates/deliveryDate">
																						<OPTION value="{@date}" day="{@dayOfWeek}" displayDate="{@displayDate}"> <xsl:value-of select="@dayOfWeek"/> &nbsp;<xsl:value-of select="@displayDate"/>  </OPTION>
																					</xsl:for-each>
																				</SELECT>
																			</xsl:otherwise>
																		</xsl:choose>
																	</TD>
																</TR>
															</xsl:for-each>
														</DIV>
													</TABLE>
												</TD>
											</TR>

										</xsl:when>
									</xsl:choose>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DELIVERYLOCATIONTYPE']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD width="25%" class="labelright"> Delivery Location Type </TD>
										<TD width="35%" >
											<DIV id="locationDIV">
												<SELECT tabindex ="17" name="deliveryLocation" onchange="checkDeliveryMessage();openUpdateWin();refreshPage('LocationChange')">
													<xsl:for-each select="/root/deliveryData/deliveryLocations/deliveryLocation">
														<OPTION value="{@institutionType}"> <xsl:value-of select="@description"/> </OPTION>
													</xsl:for-each>
												</SELECT>
											</DIV>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DELIVERYLOCATIONTYPE']/@instructionText"/>
										</TD>
									</TR>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='PHONENUMBER']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright"> Phone Number </TD>
										<TD >
											<INPUT tabindex ="18" type="text" name="recipientPhone" size="20" maxlength="20" onFocus="javascript:fieldFocus('recipientPhone')" onblur="javascript:fieldBlur('recipientPhone')"/> &nbsp;&nbsp;
											<span class="labelright">Ext.</span>
											<INPUT tabindex ="19" type="text" name="recipientPhoneExt" size="8" maxlength="10" value="" onFocus="javascript:fieldFocus('recipientPhoneExt')" onblur="javascript:fieldBlur('recipientPhoneExt')"/> &nbsp;&nbsp;
											<a tabindex ="20" href="javascript:openRecipientLookup()">Search</a>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='PHONENUMBER']/@instructionText"/>
										</TD>
									</TR>

									<xsl:choose>
										<xsl:when test="/root/deliveryDetail/@deliveryLocation = 'F'">
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DECEASEDFIRSTNAME']/@scriptText"/>
												</TD>
											</TR>

											<TR>
												<TD class="labelright"> Deceased First Name </TD>
												<TD>
													<INPUT tabindex ="28" type="text" name="recipientFirstName" size="20" maxlength="20" onFocus="javascript:fieldFocus('recipientFirstName')" onblur="javascript:fieldBlur('recipientFirstName')"/>
													&nbsp; <SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DECEASEDFIRSTNAME']/@instructionText"/>
												</TD>
											</TR>

											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DECEASEDLASTNAME']/@scriptText"/>
												</TD>
											</TR>

											<TR>
												<TD class="labelright">	Deceased Last Name	</TD>
												<TD>
													<INPUT tabindex ="29" type="text" name="recipientLastName" size="30" maxlength="30" onFocus="javascript:fieldFocus('recipientLastName')" onblur="javascript:fieldBlur('recipientLastName')"/>
													&nbsp; <SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='DECEASEDLASTNAME']/@instructionText"/>
												</TD>
											</TR>
										</xsl:when>
										<xsl:otherwise>
											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTFIRSTNAME']/@scriptText"/>
												</TD>
											</TR>

											<TR>
												<TD class="labelright"> Recipient First Name </TD>
												<TD>
													<INPUT tabindex ="28" type="text" name="recipientFirstName" size="20" maxlength="20" onFocus="javascript:fieldFocus('recipientFirstName')" onblur="javascript:fieldBlur('recipientFirstName')"/>
													&nbsp; <SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTFIRSTNAME']/@instructionText"/>
												</TD>
											</TR>

											<TR>
												<TD colspan="3" class="ScreenPrompt">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTLASTNAME']/@scriptText"/>
												</TD>
											</TR>

											<TR>
												<TD class="labelright">	Recipient Last Name	</TD>
												<TD>
													<INPUT tabindex ="29" type="text" name="recipientLastName" size="30" maxlength="30" onFocus="javascript:fieldFocus('recipientLastName')" onblur="javascript:fieldBlur('recipientLastName')"/>
													&nbsp; <SPAN style="color:red"> *** </SPAN>
												</TD>
												<TD width="40%" class="Instruction" valign="top">
													<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTLASTNAME']/@instructionText"/>
												</TD>
											</TR>
										</xsl:otherwise>
									</xsl:choose>

									<xsl:if test="/root/pageData/data[@name='displayInstitution']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='BFHNAME']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Business/Institution Name: </TD>
											<TD>
												<input tabindex ="30" type="text" name="institution" maxlength="40" size="30" onFocus="javascript:fieldFocus('institution')" onblur="javascript:fieldBlur('institution')"/> &nbsp;&nbsp;
												<xsl:if test="/root/pageData/data[@name='displayInstitutionLookUp']/@value = 'Y'">
													<a tabindex ="31" href="javascript:openRecipientLookup()">Lookup Institution</a>
												</xsl:if>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='BFHNAME']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<xsl:if test="/root/pageData/data[@name='displayRoomNumber']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='BFHINFO']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Room Number/Ward: </TD>
											<TD >
												<input tabindex ="32" type="text" name="roomNumber" size="15" maxlength="15" onFocus="javascript:fieldFocus('roomNumber')" onblur="javascript:fieldBlur('roomNumber')"/>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='BFHINFO']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<xsl:if test="/root/pageData/data[@name='displayWorkingHours']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='SERVICEWORKINGHOURS']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Time of Service/Working Hours: </TD>
											<TD >
												<DIV id ="serviceDIV">
													<select tabindex ="33" name="fromWorkHours">
														<option value="5:00 AM">5:00 AM</option>
														<option value="6:00 AM">6:00 AM</option>
														<option value="7:00 AM">7:00 AM</option>
														<option value="8:00 AM">8:00 AM</option>
														<option value="9:00 AM">9:00 AM</option>
														<option value="10:00 AM">10:00 AM</option>
														<option value="11:00 AM">11:00 AM</option>
														<option value="12:00 PM">12:00 PM</option>
														<option value="1:00 PM">1:00 PM</option>
														<option value="2:00 PM">2:00 PM</option>
														<option value="3:00 PM">3:00 PM</option>
														<option value="4:00 PM">4:00 PM</option>
														<option value="5:00 PM">5:00 PM</option>
														<option value="6:00 PM">6:00 PM</option>
														<option value="7:00 PM">7:00 PM</option>
														<option value="8:00 PM">8:00 PM</option>
														<option value="9:00 PM">9:00 PM</option>
														<option value="10:00 PM">10:00 PM</option>
														<option value="11:00 PM">11:00 PM</option>
														<option value="12:00 AM">12:00 AM</option>
													</select>
													to
													<select tabindex ="34" name="toWorkHours">
														<option value="5:00 AM">5:00 AM</option>
														<option value="6:00 AM">6:00 AM</option>
														<option value="7:00 AM">7:00 AM</option>
														<option value="8:00 AM">8:00 AM</option>
														<option value="9:00 AM">9:00 AM</option>
														<option value="10:00 AM">10:00 AM</option>
														<option value="11:00 AM">11:00 AM</option>
														<option value="12:00 PM">12:00 PM</option>
														<option value="1:00 PM">1:00 PM</option>
														<option value="2:00 PM">2:00 PM</option>
														<option value="3:00 PM">3:00 PM</option>
														<option value="4:00 PM">4:00 PM</option>
														<option value="5:00 PM">5:00 PM</option>
														<option value="6:00 PM">6:00 PM</option>
														<option value="7:00 PM">7:00 PM</option>
														<option value="8:00 PM">8:00 PM</option>
														<option value="9:00 PM">9:00 PM</option>
														<option value="10:00 PM">10:00 PM</option>
														<option value="11:00 PM">11:00 PM</option>
														<option value="12:00 AM">12:00 AM</option>
													</select>
												</DIV>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='SERVICEWORKINGHOURS']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTADDRESS']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright" valign="top"> Street Address: </TD>
										<TD>
											<input tabindex ="35" type="text" name="recipientAddress1" size="30" maxlength="30" onFocus="javascript:fieldFocus('recipientAddress1')" onblur="javascript:fieldBlur('recipientAddress1')"/>
											&nbsp; <SPAN style="color:red"> *** </SPAN>
											<br></br>
											<input tabindex ="36" type="text" name="recipientAddress2" size="30" maxlength="30" onFocus="javascript:fieldFocus('recipientAddress2')" onblur="javascript:fieldBlur('recipientAddress2')"/>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTADDRESS']/@instructionText"/>
										</TD>
									</TR>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTCITY']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright"> City: </TD>
										<TD>
											<input tabindex ="37" type="text" name="city" size="30" maxlength="30" onFocus="javascript:fieldFocus('city')" onblur="javascript:fieldBlur('city')"/>
											&nbsp; <SPAN style="color:red"> *** </SPAN>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTCITY']/@instructionText"/>
										</TD>
									</TR>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTSTATEZIPCOUNTRY']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD width="25%" class="labelright"> State/Province: </TD>
										<TD width="30%">
											<xsl:choose>
												<xsl:when test="/root/recipient/@countryType = 'I'">
													<input tabindex ="38" type="text" name="stateText" size="30" maxlength="30" onFocus="javascript:fieldFocus('stateText')" onblur="javascript:fieldBlur('stateText')"/>
												</xsl:when>
												<xsl:otherwise>
													<DIV id="stateDIV">
														<select tabindex ="38" name="stateSelect">
															<xsl:for-each select="/root/deliveryData/states/state">
																<OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/></OPTION>
															</xsl:for-each>
														</select>
														&nbsp; <SPAN style="color:red"> *** </SPAN>
													</DIV>
												</xsl:otherwise>
											</xsl:choose>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RECIPIENTSTATEZIPCOUNTRY']/@instructionText"/>
										</TD>
									</TR>

									<xsl:if test="/root/recipient/@countryType = 'D' and $occasionlbl != ''">
										<TR>
											<TD> &nbsp; </TD>
											<TD class="Instruction">
												If zip code has changed or the field is empty, enter the recipient's zip code and click button to show valid delivery dates.
											</TD>
										</TR>
									</xsl:if>

									<TR>
										<TD nowrap="true" class="labelright"> Zip/Postal Code: </TD>
										<TD nowrap="true">
											<xsl:choose>
												<xsl:when test="/root/recipient/@countryType = 'D'">
													<input tabindex ="39" type="text" name="zipCode" size="10" maxlength="10" onkeydown="disableContinue();javascript:enableClickToAccept();javascript:EnterToAccept();" onFocus="javascript:fieldFocus('zipCode')" onblur="javascript:fieldBlur('zipCode')"/>&nbsp; &nbsp;
													<IMG onkeydown="javascript:EnterToAccept()" tabindex ="40" name="clickToAcceptButton" src="../images/button_click_to_accept_disabled.gif"  onclick="clickToAccept()" />
													&nbsp; <SPAN style="color:red"> *** </SPAN>
													<BR></BR>
													<A tabindex ="41" href="javascript:openZipCodeLookup()"> Lookup by City </A>
												</xsl:when>
												<xsl:otherwise>
													<input tabindex ="39" type="text" name="zipCode" size="10" maxlength="10" onFocus="javascript:fieldFocus('zipCode')" onblur="javascript:fieldBlur('zipCode')"/>
													<BR></BR>
													<A tabindex ="41" href="javascript:openZipCodeLookup()"> Lookup by City </A>
												</xsl:otherwise>
											</xsl:choose>
										</TD>
									</TR>

									<TR>
										<TD class="labelright"> Country: </TD>
										<TD>
											<DIV id="countryDIV">
												<select tabindex ="47" name="country" onchange="confirmCountryChange()">
													<xsl:for-each select="/root/deliveryData/countries/country">
														<OPTION value="{@countryId}"> <xsl:value-of select="@name"/> </OPTION>
													</xsl:for-each>
												</select>
											</DIV>
										</TD>
									</TR>

									<input type="hidden"  name="floristCode" size="9" maxlength="9" value=""/>
									<xsl:if test="(/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y' and /root/recipient/@countryType != 'I') or ((/root/deliveryData/products/product/@productType = 'SDG' or /root/deliveryData/products/product/@productType = 'SDFC') and /root/recipient/@countryType != 'I') ">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='FLORISTCODE']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Florist Code: </TD>
											<TD>
												<input type="text" name="floristCodex" size="9" maxlength="9"/>
												<BR></BR>
												<A tabindex ="48" href="javascript:openFloristLookup()"> Select Florist </A>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='FLORISTCODE']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='CARDMESSAGE']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD> &nbsp; </TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='CARDMESSAGE']/@instructionText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright" valign="top"> Card Message: </TD>
										<TD colspan="2" valign="top">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">
												<TR>
													<TD width="50%" valign="top">
														<TEXTAREA tabindex ="57" name="giftMessage" cols="45" rows="4"  onkeyup="checkMessageSize()" onFocus="javascript:fieldFocus('giftMessage')" onblur="javascript:fieldBlur('giftMessage')"></TEXTAREA>
													</TD>
													<TD width="10%" align="center" valign="top">
														<br></br>
														<input tabindex ="58" type="button" name="CopyPhrase" value="&lt;" onclick="copyPhrase()"/>
														<br></br>
													</TD>
													<TD width="40%" valign="top">
														<DIV id="giftMessageDIV">
															<select tabindex ="59" name="giftMessageList" multiple="true" size="3" onDblClick="copyPhrase()">
																<xsl:for-each select="/root/deliveryData/giftMessages/giftMessage">
																	<OPTION value="{@giftMessageId}"> <xsl:value-of select="@messageText"/> </OPTION>
																</xsl:for-each>
															</select>
														</DIV>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>

									<TR>
										<TD colspan="3" align="center">
											<INPUT tabindex ="60" type="button" value="Spell Check Gift Message" onclick="spellCheck(giftMessage.value, 'giftMessage')"/>
										</TD>
									</TR>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='SIGNATURE']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright"> Signature: </TD>
										<TD >
											<input tabindex ="61" type="text" maxlength="60" size="55" name="signature" onFocus="javascript:fieldFocus('signature')" onblur="javascript:fieldBlur('signature')"/>
										</TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='SIGNATURE']/@instructionText"/>
										</TD>
									</TR>

									<!-- JMP 11/27/02 - Added for UAT defect 175 -->
									<xsl:if test="/root/productData/data[@name = 'egift']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='LASTMINUTEEMAIL']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Last Minute: </TD>
											<TD colspan="1" class="labelleft" valign="top" nowrap="true">
												<input tabindex ="62" align="top" type="checkbox" name="lastMinuteCheckBox" value="Y"/> <B>Email Address:</B><input tabindex ="63" align="left" type="text" maxlength="40" size="35" name="lastMinuteEmailAddress" onFocus="javascript:fieldFocus('lastMinuteEmailAddress')" onblur="javascript:fieldBlur('lastMinuteEmailAddress')"></input>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='LASTMINUTEEMAIL']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<xsl:if test="/root/pageData/data[@name='displayReleaseCheckbox']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RELEASESENDERSNAME']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright"> Release Sender's Name:</TD>
											<TD >
												<input tabindex ="64" type="checkbox" name="releaseSendersNameCheckBox"/>
												<input tabindex ="64" type="hidden" name="releaseSendersName"/>
											</TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='RELEASESENDERSNAME']/@instructionText"/>
											</TD>
										</TR>
									</xsl:if>

									<xsl:if test="/root/pageData/data[@name='displayDeliveryDate']/@value = 'Y'">
										<TR>
											<TD colspan="3" class="ScreenPrompt">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='FLORISTCOMMENTS']/@scriptText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright" valign="top">&nbsp; </TD>
											<TD width="40%" class="Instruction" valign="top">
												<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='FLORISTCOMMENTS']/@instructionText"/>
											</TD>
										</TR>

										<TR>
											<TD class="labelright" valign="top"> Florist Comments: <br></br> (optional) </TD>
											<TD colspan="2">
												<textarea tabindex ="65" name="floristComments" cols="60" rows="3" onkeyup="checkFloristCommentsSize()" onFocus="javascript:fieldFocus('floristComments')" onblur="javascript:fieldBlur('floristComments')"> </textarea>
											</TD>
										</TR>
									</xsl:if>

									<TR>
										<TD colspan="3" class="ScreenPrompt">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='ORDERCOMMENTS']/@scriptText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright" valign="top">&nbsp; </TD>
										<TD width="40%" class="Instruction" valign="top">
											<xsl:value-of select="/root/deliveryData/scripting/script[@fieldName='ORDERCOMMENTS']/@instructionText"/>
										</TD>
									</TR>

									<TR>
										<TD class="labelright" valign="top"> Order Comments: <br></br> (optional) </TD>
										<TD colspan="2">
											<textarea tabindex ="66" name="orderComments" cols="60" rows="3" onkeyup="checkCommentsSize()" onFocus="javascript:fieldFocus('orderComments')" onblur="javascript:fieldBlur('orderComments')"></textarea>
										</TD>
									</TR>
								</TABLE>
							</TD>
						</TR>

						<TR>
							<TD colspan="2" onkeydown="javascript:onKeyDown()" align="right">
								<xsl:choose>
									<xsl:when test="$occasionlbl != ''">
										<img tabindex ="67" name="continueButton" src="../images/button_add_to_cart.gif" width="74" height="17" onclick="submitForm()" />&nbsp;&nbsp;
					     			</xsl:when>
									<xsl:otherwise> <!--Edit Mode -->
										<img tabindex ="67" name="continueButton" src="../images/button_continue.gif" width="74" height="17" onclick="submitForm()" />&nbsp;&nbsp;
					     			</xsl:otherwise>
				     			</xsl:choose>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
     		<TR  class="tblheader">
				<xsl:if test="$occasionlbl != ''">
					&nbsp;
     				<a tabindex ="-1" href="{$occasionlbl}" class="tblheader">Occasion</a>&nbsp;>
					<xsl:choose>
						<xsl:when test="$breadCrumbType = 'I'"> <!-- International -->
	     					<a tabindex ="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
	     					<xsl:if test= "$upsellFlag = 'Y'">
								<a tabindex ="-1" href="{$upselldetail}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
							</xsl:if>
	     					<a tabindex ="-1" href="{$productDetail}" class="tblheader">Product Detail</a>
						</xsl:when>
						<xsl:when test="$breadCrumbType = 'D'"> <!-- Domestic -->
		     				<a tabindex ="-1" href="{$shopping}" class="tblheader">Categories</a>&nbsp;>
		     				<xsl:if test= "$searchType != 'simpleproductsearch'">
								<a tabindex ="-1" href="{$productlist}" class="tblheader">Product List</a>&nbsp;>
							</xsl:if>
							<xsl:if test= "$upsellFlag = 'Y'">
								<a tabindex ="-1" href="{$upselldetail}&amp;upsellCrumb=Y" class="tblheader">Upsell Detail</a>&nbsp;>
							</xsl:if>
		     				<a tabindex ="-1" href="{$productDetail}" class="tblheader">Product Detail</a>
		     			</xsl:when>
						<xsl:otherwise> <!-- Custome Order -->
		     				<a tabindex ="-1" href="{$shopping}" class="tblheader">Category</a>&nbsp;>
		     				<a tabindex ="-1" href="{$customOrder}" class="tblheader">Custom Order</a>
		     			</xsl:otherwise>
	     			</xsl:choose>
	     			&nbsp;> Delivery Information
	     		</xsl:if>
      		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
     		<TR>
        	 	<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
     		</TR>
 		</TABLE>

		<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>
		</CENTER>

		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
		<INPUT type="hidden" name="submitt" value="yes"/>

<!--Begin Recipient Lookup DIV-->
<DIV id="recipientLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">

<CENTER>
<DIV id="recipientLookupSearching" style="VISIBILITY: hidden;">
	<FONT face="Verdana" color="#FF0000">Please wait ... searching!</FONT>
</DIV>
</CENTER>

	<TABLE width="100%" border="0" cellspacing="0" cellpadding="2">

			<TD WIDTH="100%" VALIGN="top">
				<H1 align="CENTER"> Recipient Lookup </H1>
				<CENTER>
				<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">

						<TD>
							<TABLE  width="95%" border="0" cellpadding="1" cellspacing="1">





									<xsl:choose>
								   	<xsl:when test="/root/deliveryDetail/@deliveryLocation = 'F' or /root/deliveryDetail/@deliveryLocation = 'H' or /root/deliveryDetail/@deliveryLocation = 'N'">
								         <TR>

									<TD width="50%">
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="21" name="phoneInput" class="TblText" maxlength="20" size="10" type="hidden"></INPUT>
									</TD>
								</TR>

								         <TR>
											<TD nowrap="true" class="labelright"> Institution Name:  </TD>
											<TD>
												<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="22" name="institutionInput" class="TblText" maxlength="50" size="20" type="text" onFocus="javascript:fieldFocus('institutionInput')" onblur="javascript:fieldBlur('institutionInput')"></INPUT>
											</TD>
										</TR>
										<TR>
											<TD class="labelright"> City: </TD>
											<TD>
												<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="24" name="cityInput" class="TblText" maxlength="50" size="20" type="text" value="" onFocus="javascript:fieldFocus('cityInput')" onblur="javascript:fieldBlur('cityInput')"></INPUT>
											</TD>
										</TR>

										<xsl:choose>
								   <xsl:when test="/root/recipient/@country = 'US'">
								      <TR>
										<TD class="labelright"> State: </TD>
										<TD class="TblText">
										     <SELECT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" class="TblText" name="stateInput">
												<xsl:for-each select="/root/deliveryData/states/state[@countryCode='']">
											        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
												</xsl:for-each>
											 </SELECT>
										</TD>
									</TR>
								   </xsl:when>
								   <xsl:when test="/root/recipient/@country = 'CA'">
								      <TR>
										<TD class="labelright"> State: </TD>
										<TD class="TblText">
										     <SELECT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" class="TblText" name="stateInput">
												<xsl:for-each select="/root/deliveryData/states/state[@countryCode='CAN']">
											        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
												</xsl:for-each>
											 </SELECT>
										</TD>
									</TR>



								   </xsl:when>
								   <xsl:otherwise>



								   </xsl:otherwise>
								</xsl:choose>

								<TR>
									<TD>
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" name="zipCodeInput" class="TblText" maxlength="6" size="6" type="hidden" value=""></INPUT>
									</TD>
								</TR>

								   	</xsl:when>
								   	<xsl:otherwise>

								   	<TR>
									<TD nowrap="true" class="labelright"> Phone Number:  </TD>
									<TD width="50%">
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="21" name="phoneInput" class="TblText" maxlength="20" size="15" type="text" onFocus="javascript:fieldFocus('phoneInput')" onblur="javascript:fieldBlur('phoneInput')"></INPUT>
									</TD>
								</TR>

								   	<TR>
										<TD>
											<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="24" name="cityInput" class="TblText" maxlength="50" size="20" type="hidden" value=""></INPUT>
										</TD>
									</TR>

									<TR>
									<TD>
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" name="zipCodeInput" class="TblText" maxlength="6" size="6" type="hidden" value=""></INPUT>
									</TD>
								</TR>
								   	<TR>
									<TD>
										<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" name="stateInput" class="TblText" maxlength="6" size="6" type="hidden" value=""></INPUT>
									</TD>
								</TR>

								   	<TR>
										<TD>
									      <INPUT name="institutionInput" type="hidden" class=""/>
									     </TD>
									</TR>
								   	</xsl:otherwise>
									</xsl:choose>




									<TR>
										<!-- <TD class="labelright"> Address: </TD> -->
										<TD>
											<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="23" name="addressInput" class="TblText" maxlength="100" size="20" type="hidden" value=""></INPUT>
										</TD>
									</TR>


								<!--
								<xsl:choose>
								   <xsl:when test="/root/recipient/@country = 'US'">
								      <TR>
										<TD class="labelright"> State: </TD>
										<TD class="TblText">
										     <SELECT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" class="TblText" name="stateInput">
												<xsl:for-each select="/root/deliveryData/states/state[@countryCode='']">
											        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
												</xsl:for-each>
											 </SELECT>
										</TD>
									</TR>
								   </xsl:when>
								   <xsl:when test="/root/recipient/@country = 'CA'">
								      <TR>
										<TD class="labelright"> State: </TD>
										<TD class="TblText">
										     <SELECT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" class="TblText" name="stateInput">
												<xsl:for-each select="/root/deliveryData/states/state[@countryCode='CAN']">
											        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
												</xsl:for-each>
											 </SELECT>
										</TD>
									</TR>
								   </xsl:when>
								   <xsl:otherwise>
										<TR>
											<TD class="labelright"> State: </TD>
											<TD>
												<INPUT onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" name="stateInput" class="TblText" maxlength="30" size="6" type="text" value="" onFocus="javascript:fieldFocus('stateInput')" onblur="javascript:fieldBlur('stateInput')"></INPUT>
											</TD>
										</TR>
								   </xsl:otherwise>
								</xsl:choose>
								-->

							</TABLE>
						</TD>

				</TABLE>
				</CENTER>
  			</TD>
	        <TR>
				<TD colspan="3" align="right">
					<IMG onkeydown="javascript:EnterToRecipientPopup()" tabindex ="25" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchRecipient()"/>
 	            	<IMG onkeydown="javascript:closeRecipientLookup()" tabindex ="27" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelRecipient()"/>
	         	</TD>
        	</TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	</TABLE>
</DIV>
<!--End Recipient Lookup DIV-->

<!--Begin Florist Lookup DIV-->
<DIV id="floristLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">

<CENTER>
<DIV id="floristLookupSearching" style="VISIBILITY: hidden;">
	<FONT face="Verdana" color="#FF0000">Please wait ... searching!</FONT>
</DIV>
</CENTER>

<TABLE width="98%" border="0" cellspacing="0" cellpadding="2">
	<TD WIDTH="100%" VALIGN="top">

		<H1 align="CENTER"> Florist Lookup	</H1>
		<CENTER>
		<TABLE  width="100%" border="0" cellpadding="2" cellspacing="2">
			<TR>
			<TD>
			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">

		        <TR>
		            <TD width="25%" class="labelright" > Name: &nbsp; </TD>
		            <TD width="75%">
		                <INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="49" class="TblText" name="floristBusinessInput" type="text" size="30" maxlength="75" onFocus="javascript:fieldFocus('floristBusinessInput')" onblur="javascript:fieldBlur('floristBusinessInput')">
		                </INPUT>
		            </TD>
		        </TR>

				<TR>
					<TD class="labelright"> Phone: &nbsp; </TD>
					<TD>
		                <INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="50" class="TblText" name="floristPhoneInput" type="text" size="20" maxlength="20" onFocus="javascript:fieldFocus('floristPhoneInput')" onblur="javascript:fieldBlur('floristPhoneInput')">
		                </INPUT>
					</TD>
				</TR>

				<TR>
					<TD class="labelright"> Address: &nbsp; </TD>
					<TD>
		                <INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="51" class="TblText" name="floristAddressInput" type="text" size="20" maxlength="75" onFocus="javascript:fieldFocus('floristAddressInput')" onblur="javascript:fieldBlur('floristAddressInput')">
		                </INPUT>
					</TD>
				</TR>
				<TR>
					<TD class="labelright"> City: &nbsp; </TD>
					<TD>
						<INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="52" class="TblText" name="floristCityInput"  maxlength="50" size="20" type="text" onFocus="javascript:fieldFocus('floristCityInput')" onblur="javascript:fieldBlur('floristCityInput')">
						</INPUT>
					</TD>
				</TR>
				<xsl:choose>
					<xsl:when test="/root/recipient/@country = 'US'">
					<TR>
						<TD class="labelright"> State: &nbsp; </TD>
						<TD>
						     <SELECT onkeydown="javascript:EnterToFloristPopup()" tabindex ="53" class="TblText" name="floristStateInput">
								<option value=""></option>
								<xsl:for-each select="/root/deliveryData/states/state[@countryCode='']">
							        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
								</xsl:for-each>
							 </SELECT>
						</TD>
					</TR>
			   		</xsl:when>
			   		<xsl:when test="/root/recipient/@country = 'CA'">
					<TR>
						<TD class="labelright"> State: &nbsp; </TD>
						<TD>
						     <SELECT onkeydown="javascript:EnterToFloristPopup()" tabindex ="53" class="TblText" name="floristStateInput">
								<xsl:for-each select="/root/deliveryData/states/state[@countryCode='CAN']">
							        <OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/> </OPTION>
								</xsl:for-each>
							 </SELECT>
						</TD>
					</TR>
			   		</xsl:when>
			   		<xsl:otherwise>
					<TR>
						<TD class="labelright" > State: &nbsp; </TD>
						<TD>
							<INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="53" name="floristStateInput" class="TblText" maxlength="30" size="6" type="text" value="" onFocus="javascript:fieldFocus('floristStateInput')" onblur="javascript:fieldBlur('floristStateInput')"></INPUT>
						</TD>
					</TR>
					</xsl:otherwise>
				</xsl:choose>
				<TR>
					<TD nowrap="true" class="labelright" > Zip/Postal Code: &nbsp;</TD>
					<TD>
						<INPUT onkeydown="javascript:EnterToFloristPopup()" tabindex ="54" name="floristZipInput" class="TblText" maxlength="6" size="6" type="text" value="" onFocus="javascript:fieldFocus('floristZipInput')" onblur="javascript:fieldBlur('floristZipInput')">
						</INPUT>
					</TD>
				</TR>
				<TR>
					<TD colspan="3" align="right">
						<IMG onkeydown="javascript:EnterToFloristPopup()" tabindex ="55" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchFlorist()"/>
	 	            	<IMG onkeydown="javascript:closeFloristLookup()" tabindex ="56" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelFlorist()"/>
		         	</TD>
	        	</TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	</TABLE>
	</TD>
	</TR>
</TABLE>
</CENTER>
</TD>

</TABLE>
</DIV>
<!--End Florist Lookup DIV-->

<!--Begin JCP Popup DIV -->
<DIV id="JCPPop" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						At this time, JC Penney accepts orders going to or coming from the 50 United States
						only.  We can process your Order using any major credit card through FTD.com.
						<BR></BR>
						<BR></BR>
					    Would you like to proceed?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<IMG src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End JCPPop DIV -->


<!--Begin City Lookup DIV -->
<DIV id="zipCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<center>
<DIV id="zipCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
</DIV>
</center>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD nowrap="true" colspan="3" align="left">
					<H1 align="CENTER"> Zip/Postal Code Lookup	</H1>
	            </TD>

	        </TR>
	        <TR>
	            <TD width="50%" class="labelright" align="right"> City: </TD>
	            <TD width="50%">
	                <INPUT onkeydown="javascript:EnterToZipPopup()" tabindex ="42" type="text" class="TblText" name="cityInputLookup" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('cityInputLookup')" onblur="javascript:fieldBlur('cityInputLookup')"/>
	            </TD>
	        </TR>
	        <TR>
		        <TD width="50%" class="labelright" align="right"> State: </TD>
		<xsl:choose>
		<xsl:when test="root/recipient[@country = 'US']">
           	<TD width="50%">
            	<SELECT onkeydown="javascript:EnterToZipPopup()" tabindex ="43" class="TblText" name="stateInputLookup">
					<xsl:for-each select="/root/deliveryData/states/state[@countryCode='']">
						<OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/>
						</OPTION>
					</xsl:for-each>
				</SELECT> &nbsp;&nbsp;
			</TD>
		</xsl:when>
		<xsl:when test="root/recipient[@country = 'CA']">
            <TD width="50%">
            	<SELECT onkeydown="javascript:EnterToZipPopup()" tabindex ="43" class="TblText" name="stateInputLookup">
					<xsl:for-each select="/root/deliveryData/states/state[@countryCode='CAN']">
						<OPTION value="{@stateMasterId}"> <xsl:value-of select="@stateName"/>
						</OPTION>
					</xsl:for-each>
				</SELECT> &nbsp;&nbsp;
			</TD>
		</xsl:when>
		<xsl:otherwise>
	         <TD width="50%">
	             <INPUT onkeydown="javascript:EnterToZipPopup()" tabindex ="43" class="TblText" type="text" name="stateInputLookup" size="20" maxlength="50" value="" onFocus="javascript:fieldFocus('stateInputLookup')" onblur="javascript:fieldBlur('stateInputLookup')"/>
			  </TD>
		</xsl:otherwise>
		</xsl:choose>
		</TR>
		<TR>
			<TD nowrap="true" class="labelright"> Zip/Postal Code: </TD>
			<TD>
                <INPUT onkeydown="javascript:EnterToZipPopup()" tabindex ="44" class="TblText" type="text" name="zipCodeInputLookup" size="5" maxlength="10" onFocus="javascript:fieldFocus('zipCodeInputLookup')" onblur="javascript:fieldBlur('zipCodeInputLookup')"/>
			</TD>
		</TR>
        <TR>
        	<TD></TD>
			<TD colspan="3" align="right">
				<IMG onkeydown="javascript:EnterToZipPopup()" tabindex ="45" src="../images/button_search.gif" alt="Search" onclick="javascript:goSearchZipCode()"  />
 	           	<IMG onkeydown="javascript:closeZipLookup()" tabindex ="46" src="../images/button_close.gif" alt="Close screen" onclick="javascript:goCancelZipCode()" />
         	</TD>
        </TR>
        <TR>
            <TD colspan="3"></TD>
        </TR>
	 </TABLE>
</DIV>
<!--End city Lookup DIV-->

<!--Begin No Floral DIV -->
<DIV id="noFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE>
				<TR>
					<TD align="center">Floral Items can no longer be delivered to this zip/postal code area.
						<BR></BR>
						<BR></BR>
					    Would you be interested in our specialty items?
			   		</TD>
				</TR>
				<TR>
					<TD align="center">
						<!-- <INPUT type="image" src="../images/button_no.gif" onclick="javascript:closeNoFloral()"></INPUT> -->
						<IMG src="../images/button_no.gif" onclick="javascript:closeNoFloral()"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- <INPUT type="image" src="../images/button_yes.gif" onclick="javascript:cancelItemToShopping()"></INPUT> -->
						<xsl:choose>
						<xsl:when test= "$upsellFlag = 'Y'">
							<IMG src="../images/button_yes.gif" onclick="javascript:cancelItemToUpsell()"/>
						</xsl:when>
						<xsl:otherwise>
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItemToShopping()"/>
						</xsl:otherwise>
						</xsl:choose>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Floral DIV -->

<!--Begin No Product DIV -->
<DIV id="noProduct" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						I'm sorry but the zip code you have provided is not currently serviced by an FTD florist or FedEx.
						<BR></BR>
						<BR></BR>
					    	Would you like to shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_ok.gif" onclick="javascript:noProduct();"/>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End No Product DIV -->

<!--Begin GNADD Upsell DIV -->
<DIV id="gnaddUpsell" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						NEED IT THERE SOONER?
						<BR></BR>
			   		</TD>
				</TR>
				<TR> <TD>
						<xsl:value-of select="/root/pageData/data[@name='upsellBaseName']/@value"/>&nbsp;is available for delivery as soon as&nbsp;<xsl:value-of select="/root/pageData/data[@name='upsellBaseDeliveryDate']/@value"/>.
				</TD> </TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_yes.gif" onclick="javascript:acceptGnaddUpsell()"></IMG> &nbsp; &nbsp;
						<IMG  src="../images/button_no.gif" onclick="javascript:gnaddUpsell()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End GNADD Upsell DIV -->

<!--Begin Product Unavailable DIV -->
<DIV id="productUnavailable" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						I'm sorry but the product you've selected is currently not available for delivery to the zip code you've provided.
						<BR></BR>
						<BR></BR>
					    	Would you like to try another zip code or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:productUnavailable()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Product Unavailable DIV -->

<!--Begin no codified florist but has common carrier DIV -->
<DIV id="noCodifiedFloristHasCommonCarrier" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
<![CDATA[
						I'm sorry, the selected item cannot be delivered by an FTD florist to the zip code provided.
]]>
						<!--***<xsl:value-of select="$shippingNextDay"/>*<xsl:value-of select="$shippingTwoDay"/>*<xsl:value-of select="$shippingStandard"/>***-->
						<BR></BR>
						<BR></BR>
					    	This item can be delivered via FedEx
					    	<xsl:choose>
					    		<xsl:when test=" $shippingNextDay = 'ND' and $shippingTwoDay = '2D' and $shippingStandard = 'GR'">
					    			Next Day, Two Day or Standard (within 3 days) delivery
					    		</xsl:when>
					    	<xsl:otherwise>
					    		<xsl:choose>
					    		<xsl:when test=" $shippingNextDay = 'ND' and $shippingTwoDay = '2D' ">
								Next Day or Two Day
						    	</xsl:when>
							 <xsl:otherwise>
							 	<xsl:choose>
							    	<xsl:when test=" $shippingNextDay = 'ND' ">
									Next Day
							    	</xsl:when>
								<xsl:otherwise>

								</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
							</xsl:choose>
					    	</xsl:otherwise>
					    	</xsl:choose>
					    	service as early as <xsl:value-of select="/root/pageData/data[@name='firstAvailableDate']/@value"/>. Would you like to select a shipping option or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noCodifiedFloristHasCommonCarrier()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End no codified florist but has common carrier DIV -->

<!--Begin no codified florist but has two day delivery only DIV -->
<DIV id="noCodifiedFloristHasTwoDayDeliveryOnly" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
<![CDATA[
						I'm unable to complete your order because product is not available for delivery to Alaska or Hawaii
						using Next Day Delivery. The only available shipping option is Two Day Delivery.
]]>
						<BR></BR>
						<BR></BR>
					    	The next available delivery date to this area is
					    	<xsl:value-of select="/root/pageData/data[@name='firstAvailableDate']/@value"/>. Would you like to select this date for delivery or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noCodifiedFloristHasTwoDayDeliveryOnly()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End no codified florist but has two day delivery only DIV -->



<!--Begin no florist but has common carrier DIV -->
<DIV id="noFloristHasCommonCarrier" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
<![CDATA[
						I'm sorry but the zip code you have provided is not currently serviced by an FTD florist for floral deliveries.
]]>
						<BR></BR>
						<BR></BR>
					    	This item can be delivered via FedEx as early as <xsl:value-of select="/root/pageData/data[@name='firstAvailableDate']/@value"/>. Would you like to select a shipping option or shop for another item?
			   		</TD>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:noFloristHasCommonCarrier()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End no florist but has common carrier DIV -->



<!--Begin floristSelectedButNotAvailable DIV -->
<DIV id="floristSelectedButNotAvailable" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						I'm sorry, the selected item cannot be delivered by a florist to the zip code provided.
						This item can be delivered via FexEx Two-Day service.
						<BR></BR>
						<BR></BR>
					    	Would you like to select a shiping option or shop for another item?
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR> <TD> &nbsp; </TD> </TR>
				<TR>
					<TD align="center">
						<IMG  src="../images/button_ok.gif" onclick="javascript:floristSelectedButNotAvailable()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End floristSelectedButNotAvailable DIV -->

<!--Begin Floral only DIV -->
<DIV id="onlyFloral" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						Only Floral items can be delivered to this Zip/Postal Code.
						<BR></BR>
						<BR></BR>
					    Would you be interested in selecting a floral item?
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_no.gif" onclick="javascript:onlyFloral()"/>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<xsl:choose>
						<xsl:when test= "$upsellFlag = 'Y'">
							<IMG src="../images/button_yes.gif" onclick="javascript:cancelItemToUpsell()"/>
						</xsl:when>
						<xsl:otherwise>
						<IMG src="../images/button_yes.gif" onclick="javascript:cancelItemToShopping()"/>
						</xsl:otherwise>
						</xsl:choose>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Only Floral DIV -->

<!--Begin standardAddress DIV -->
<DIV id="standardAddressDIV" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE>
	<TR>
		<TD align="left">
			You have provided the following address: <BR></BR>
		</TD>
	</TR>
	<TR>
		<TD align="left">
			<textarea name="oldAddress1" cols="40" rows="5" style="background: lightgrey;" READONLY="true" ></textarea>
		</TD>
	</TR>
	<TR>
		<TD>
<!--
		<SCRIPT language="JavaScript">
		<![CDATA[
		document.write("xyz");
		]]>
		</SCRIPT>
-->
		</TD>

	</TR>

	<TR>
		<TD align="left">
			<BR></BR>
			We have validated the address to be: <BR></BR>
		</TD>
	</TR>
	<TR>
		<TD align="left">
			<textarea name="newAddress1"  cols="40" rows="5" style="background: lightgrey;" READONLY="true" ></textarea>
		</TD>
	</TR>
	<TR>
		<TD align="left">
			<BR></BR>
			Do you want to:
			<BR></BR>
			Accept validated address (Accept)
			<BR></BR>
			Reject validated address, use entered address (Reject)
			<BR></BR>
			Return to Delivery page (Return)
			<BR></BR>
		</TD>
	</TR>
	<TR>
		<TD align="center">
			<IMG src="../images/button_accept.gif" border="0" onclick="javascript:addressChoice('N')"/> &nbsp;&nbsp;&nbsp;&nbsp;
			<IMG src="../images/button_reject.gif" border="0" onclick="javascript:addressChoice('Y')"/> &nbsp;&nbsp;&nbsp;&nbsp;
			<IMG src="../images/button_return.gif" border="0" onclick="javascript:addressChoice('R')"/> &nbsp;&nbsp;&nbsp;&nbsp;
		</TD>
	</TR>
</TABLE>

</DIV>

<!--Begin Codified Special DIV -->
<DIV id="codifiedSpecial" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
	<TR>
		<TD>
			<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
				<TR>
					<TD align="center">
						We're sorry the product you've selected is currently not available in the
						zip code you entered.  However, the majority of our flowers can be delivered
						to this zip code.
						<BR/><BR/>

						If this is an incorrect zip code, please re-enter the zip code
						below and "Click to Accept".  Otherwise, please click the "Back" button to
						return to shopping.
			   		</TD>
				<TR> <TD> &nbsp; </TD> </TR>
				</TR>
				<TR>
					<TD align="center">
						<IMG src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"></IMG>
					</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
</DIV>
<!--End Codified Special DIV -->


<!--Begin invalidAddress DIV -->
<DIV id="invalidAddressDIV" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<TABLE>
	<TR>
		<TD align="left">
			We could not Validate the following address: <BR></BR>
		</TD>
	</TR>
	<TR>
		<TD align="left">
			<textarea name="invalidAddress" cols="40" rows="5" style="background: lightgrey;" READONLY="true" ></textarea>
		</TD>
	</TR>
	<TR>
		<TD align="left">
			<BR></BR>
			Do you want to:
			<BR></BR>
			Accept the invalid address (Accept)
			<BR></BR>
			Return to Delivery page (Return)
			<BR></BR>
		</TD>
	</TR>
	<TR>
		<TD align="center">
			<IMG src="../images/button_accept.gif" border="0" onclick="javascript:invalidAddressChoice('Y')"/> &nbsp;&nbsp;&nbsp;&nbsp;
			<IMG src="../images/button_return.gif" border="0" onclick="javascript:invalidAddressChoice('R')"/> &nbsp;&nbsp;&nbsp;&nbsp;
		</TD>
	</TR>
</TABLE>

</DIV>

</FORM>

<!--SERVER SIDE VALIDATION SECTION-->
<xsl:call-template name="validation"/>
	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... validating input!</font> </p>
	</DIV>

	<DIV id="calendarLoading" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... loading!</font> </p>
	</DIV>

</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
