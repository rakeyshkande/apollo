﻿<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY reg  "®">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="headerButtons.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="customerHeader.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>";
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>";
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>";
<xsl:variable name="siteNameSsl" select="root/parameters/siteNameSsl"/>
<xsl:variable name="applicationContext" select="root/parameters/applicationContext"/>

<xsl:template match="/root">
<xsl:call-template name="header"/>

<HTML>
  <HEAD>
    <TITLE>
      FTD - Shopping Cart
    </TITLE>
<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>


    <SCRIPT type="text/javascript" language="JavaScript">

	var sessionId_ = '<xsl:value-of select="$sessionId"/>';
	var sessionId = '<xsl:value-of select="$sessionId"/>';
	var persistentObjId_ = '<xsl:value-of select="$persistentObjId"/>';
	var persistentObjId = '<xsl:value-of select="$persistentObjId"/>';
  var siteNameSsl = '<xsl:value-of select="$siteNameSsl"/>';
  var applicationContext = '<xsl:value-of select="$applicationContext"/>';

	var shipFloristArray = new Array();
	var shipCarrierArray = new Array();
	var customArray = new Array();

	<xsl:for-each select="ORDER/ITEM">
		shipFloristArray[<xsl:value-of select="@cart_item_number"/>] = '<xsl:value-of select="@shipMethodFlorist"/>';
		shipCarrierArray[<xsl:value-of select="@cart_item_number"/>] = '<xsl:value-of select="@shipMethodCarrier"/>';
		customArray[<xsl:value-of select="@cart_item_number"/>] = '<xsl:value-of select="@custom"/>';
	</xsl:for-each>

	var sourceCode_ = '<xsl:value-of select="ORDER/@sourceCode"/>';
	var sourceCodeDesc = "<xsl:value-of select='ORDER/@sourceCodeDescription'/>";
	var totalItems = "<xsl:value-of select='ORDER/@totalItems'/>";
	var companyId = '<xsl:value-of select="ORDER/@lastOrderAction"/>';
	var checkOutEnabled = true;

	document.oncontextmenu=stopIt;	



<![CDATA[

	document.onkeydown = backKeyHandler;

	function OnEnterShop()
	{
	    if (window.event.keyCode == 13)
	    {
		continueShopping()
            }
   	}

	function OnEnterCheckOut()
	{
             if (window.event.keyCode == 13)
             {
		javascript:checkOut()
             }
   	}

   	function EnterToSourcePopup()
	{
	   if (window.event.keyCode == 13)
		{
		    openSourceCodePopup();
		}
	}

	function CloseSourceLookup()
        {
           if (window.event.keyCode == 13)
		{
		    goCancelSourceCode();
		}
	}

	function deliveryPolicy(itemNumber)
	{
		var url_source = "../deliveryPolicy_floral.htm";

		if ( shipCarrierArray[itemNumber] == "Y" )
		{
			url_source = "../deliveryPolicy_dropship.htm";
		}

		if ( shipFloristArray[itemNumber] == "Y" )
			url_source = "../deliveryPolicy_floral.htm";

		var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
		window.showModalDialog(url_source,"delivery_policy", modal_dim);
	}


    	function changeSource()
    	{
    		openSourceCodeLookup();
    	}

		function continueShopping()
		{
			var url = "OccasionServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
				"&selection=bysourcecode&source=introduction&sourceText=" + sourceCode_;
			//alert(url);
			document.location = url;

		}

		function removeItem( itemNumber )
		{
			if(confirm("Do you want to remove this item from the shopping cart?"))
			{
				//alert(itemNumber);
				var url = "ShoppingCartServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
				"&command=remove&itemCartNumber=" + itemNumber + "&companyId=" + companyId;
				document.location = url;
			}


		}
		function editItem( itemNumber )
		{
			var url = "EditItemDetailServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
			"&command=getDetail&itemCartNumber=" + itemNumber + "&custom=" + customArray[itemNumber] + "&companyId=" + companyId;
			document.location = url;
			//alert("Edit item is still in progress");
		}

		function removeAllItem()
		{

			if(confirm("Do you want to remove all items the shopping cart?"))
			{
				var url = "ShoppingCartServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
				"&command=remove&itemCartNumber=all" + "&companyId=" + companyId;
				document.location = url;
			}
		}

		function addOneMoreItem( itemNumber, inProdId )
		{
			var url_source="../add1ItemDlg.htm";
	    		var modal_dim="dialogWidth:485px; dialogHeight:150px; center:yes; status=0";
		 	var ret = window.showModalDialog(url_source,"", modal_dim);

 	    		if ( ret!= null )
			{
				if(ret == "same")
				{
					var url = "ShoppingCartServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
					"&command=addSameItem&itemCartNumber=" + itemNumber + "&companyId=" + companyId;
					document.location = url;
				}
				else if(ret == "different")
				{
					var actiontarget = "";
					if(customArray[itemNumber] == "Y")
					{
						actiontarget = "CustomOrderServlet";
					}
					else
					{
						actiontarget = "ProductDetailServlet";
					}
					var url = "AddItemServlet" + "?sessionId=" + sessionId_ + "&persistentObjId=" + persistentObjId_ + "&productid=" + inProdId + "&companyId=" + companyId + "&actionServlet=" + actiontarget;
					//alert(url);
 					document.location = url;
				}
				//else if(ret == "differentProduct")
				//{
				//	var actiontarget = "";
				//	
				//	if(countryType == "I")
				//	{
				//		actiontarget = "InternationalProductList";
				//	}
				//	else
				//	{
				//		actiontarget = "CategoryServlet";
				//	}					
				//	
				//	var url = actiontarget + "?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ + "&itemCartNumber=" + itemNumber + "&command=addDifferentItem" + "&companyId=" + companyId;
				//	
				//	document.location = url;
				//}
			}
		}

		function changeProduct( itemNumber, inProdId, countryType )
		{
			var actiontarget = "";
			
			if(countryType == "I")
			{
				actiontarget = "InternationalProductList";
			}
			else
			{
				actiontarget = "CategoryServlet";
			}					
				
			var url = actiontarget + "?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ + "&itemCartNumber=" + itemNumber + "&command=addDifferentItem" + "&companyId=" + companyId;
				
			document.location = url;			
		}
		
		function editDeliveryInformation( itemNumber )
		{
			var url = "EditDeliveryServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
			"&cartItemNumber=" + itemNumber + "&companyId=" + companyId;
			//alert(url);
			document.location = url;
		}


		function editFloristComment( itemNumber )
		{
			var url = "EditDeliveryServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
			"&cartItemNumber=" + itemNumber + "&companyId=" + companyId + "#floristCommentsAnchor";
			//alert(url);
			document.location = url;
		}

		function editItemComment( itemNumber )
		{
			var url = "EditDeliveryServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
			"&cartItemNumber=" + itemNumber + "&companyId=" + companyId + "#itemCommentsAnchor";
			//alert(url);
			document.location = url;
		}

		function addFloristComment( itemNumber )
		{
			editFloristComment(itemNumber);
		}

		function addItemComment( itemNumber )
		{
			editItemComment(itemNumber);
		}

		function init()
		{
			document.form.txtSourceCode.value = sourceCode_;
			document.all.sourceCodeDiv.innerText = sourceCode_ + " - " + sourceCodeDesc;
			if(totalItems == "0")
			{
				checkOutEnabled = false;
			}
		}


		function deCode(strSelection)
		{
			strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
			strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

			return strSelection;
		}

		function document.onkeydown()
		{
			if ( window.event.keyCode == 13 )
			{
				return false;
			}
		}
]]>
    </SCRIPT>
    <LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  </HEAD>
<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onLoad="init()">
	<FORM name="form" method="get" action="{$actionServlet}">

	<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
	<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>

		<CENTER>
		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD align="right" nowrap="">
					<A tabindex="-1" href="javascript:doCancelOrder()" STYLE="text-decoration:none" >Cancel Order</A>  &nbsp;&nbsp;&nbsp;
				</TD>
			</TR>
			<TR>
				<TD> <xsl:call-template name="customerHeader"/>&nbsp; </TD>
			</TR>
		</TABLE>

        <!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD valign="center">
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
							<TD>
								<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
									<TR>
											<TD width="10%" class="label">&nbsp;</TD>
											<TD width="40%">&nbsp;</TD>
										<xsl:if test="ORDER/@sourceCode">
											<TD width="10%" class="label"> Source Code: </TD>
											<TD width="40%" NOWRAP="true"> &nbsp;
												<DIV class="label" id="sourceCodeDiv"/><input tabindex="-1" type="button" class="label" name="changeSourceCode" value="Change Source Code" onclick="javascript:changeSource();" /><INPUT TYPE="hidden" name="txtSourceCode" value="" />
											</TD>
										</xsl:if>
									</TR>
								</TABLE>
							</TD>
						</TR>

						<TR>
							<TD class="ScreenPrompt">
								Is there anything else you would like to order today?
							</TD>
						</TR>

						<TR>
							<TD>
								<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
                        			<TR>
                          				<TD>
											<TABLE width="100%" cellpadding="1" cellspacing="2">
                              					<TR>
													<TD width="70%" valign="bottom">
													     &nbsp;
                                					</TD>
                                					<TD width="15%" align="right" valign="top">
                                  						<IMG onkeydown="javascript:OnEnterShop()" tabindex="1" src="../images/button_continue_shopping.gif" onclick="javascript:continueShopping();" alt="Continue Shopping" border="0"/>
                                					</TD>
                                					<TD width="2%" valign="top">
                                  						<B>or</B>
                                					</TD>
                                					<TD width="15%" align="left" valign="top">
                                 	 					<IMG onkeydown="javascript:OnEnterCheckOut()" tabindex="2" src="../images/button_checkout_now.gif" onclick="javascript:checkOut()" alt="Checkout Now" border="0"/>
                                					</TD>
												</TR>
											</TABLE>
											<BR></BR>
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="2">
												<TR>
													<TD width="50%" valign="bottom">
														<FONT color="red"><B></B><BR></BR>
															<B>Your cart contains (<xsl:value-of select="count(ORDER/ITEM)"/>) items.<BR></BR></B>
														</FONT> <BR></BR>
													</TD>
													<TD align="right" valign="top">
														<B>Shopping Cart Subtotal in US Dollars</B><BR></BR>
														(including shipping/service fees &amp; &nbsp;taxes)
													</TD>
													<TD width="10%" align="right" valign="top">
														<TABLE class="LookupTable" border="1" cellpadding="1" cellspacing="2">
															<TR> <TD> $ <xsl:value-of select="/root/ORDER/@totalOrderPrice"/> </TD> </TR>
														</TABLE>
													</TD>
												</TR>
												<xsl:if test="/root/pageHeader/headerDetail[@name='dnisType']/@value = 'JP'">
													<TR>
														<TD colspan="4">
															<SPAN style="color:red"> Taxes have not been charged,
															yet there may be additional tax assessed at a later time by JC Penney </SPAN>
														</TD>
													</TR>
												</xsl:if>
											</TABLE>
										</TD>
									</TR>
							<xsl:for-each select="ORDER/ITEM">

								<script language="javascript">
									var itemNumber = <xsl:value-of select="@cart_item_number"/>;
									var countryType = '<xsl:value-of select="DELIVERY/RECIPIENT/@countryType"/>';
									var number_ = '<xsl:value-of select="@number"/>';
								</script>

									<TR>
										<TD>
											<!--start of ftd garden basket -->
          									<TABLE width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFCC00" border="0">
            									<TR>
              										<TD>
                										<TABLE summary="" width="100%" cellpadding="0" cellspacing="1" border="0">
                  											<TR>
                    											<TD width="100%" bgcolor="#FFFFFF"> <!-- column 1 -->
                      												<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
                        												<TR>
                        													<!-- Picture -->

																			<TD width="15%" valign="top">
														  						<TABLE summary="Item" width="100%" cellpadding="3" border="0">
															  						<TR>
																  						<TD align="center">
                                    														<SCRIPT language="JavaScript">

 																							var pid = "<xsl:value-of select="@novatorId"/>";
      			   var imageSuffixSmall = '<xsl:value-of select="$imageSuffixSmall"/>';
																							<![CDATA[
 																							document.write("<img ")
																							document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
																							document.write("if(imag != \'npi" + imageSuffixSmall + "\'){ ")
																							document.write("this.src = \'/product_images/npi_1.gif\'}\" ")
																							document.write("src=\"/product_images/" + pid + imageSuffixSmall + "\"> ")
																							document.write("</img>")
																							]]>
																							</SCRIPT> <!-- IL 60613 -->
																		  					<BR></BR>
																		  					<script language="javascript">
																								href = "javascript: removeItem(" + itemNumber + ");";
																		  						<![CDATA[
																  								document.write("<img onclick=\"" + href + "\"");
																								document.write("src=\"../images/button_txt_remove_2.jpg\">");
																								document.write("</img>");
																								]]>
																		  					</script>
														      							</TD>
																					</TR>
																				</TABLE>
                          													</TD>
                          													<!-- Item -->
                          													<TD width="25%" valign="top">
                            													<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD bgcolor="#C1DEF3">
																							Item
																						</TD>
																					</TR>

																					<TR>
								                                    					<TD>
								                                      						<B><xsl:if test="@sku !=''">Customer Order: SKU <xsl:value-of select="@sku"/><br></br></xsl:if></B>
								                                      					</TD>
								                                  					</TR>

								 													<TR>
								                                    					<TD>

								                                    					<B>
									                                    					<script><![CDATA[
																								document.write(deCode("]]> <xsl:value-of select="@name"/> <![CDATA["));
																							]]></script>
                																		</B>


								                                    					</TD>
								                                  					</TR>
																					<TR>
																						<TD>
																							<xsl:value-of select="@number"/>
																						</TD>
																					</TR>

																					<TR>
																						<TD>
																							<TABLE width="100%" border="0">
																								<xsl:if test="@subCodeDescription !=''">
																							   			<TR>

																							   				<TD><xsl:value-of select="@subCodeDescription"/></TD>
																				   						</TR>
																							   	</xsl:if>
																			 	  				   <xsl:if test="@team !=''">
																							   			<TR>
																							  				<TD>Team:</TD>
																							   				<TD><xsl:value-of select="@team"/></TD>
																				   						</TR>
																							   	  </xsl:if>

																							   	   <xsl:if test="DETAIL/@color1 !=''">
																							   			<TR>
																							  				<TD>1st Choice Color:</TD>
																							   				<TD><xsl:value-of select="DETAIL/@color1"/></TD>
																				   						</TR>
																							   	  </xsl:if>

																								  <xsl:if test="DETAIL/@color2 !=''">
																							   			<TR>
																							  				<TD>2nd Choice Color:</TD>
																							   				<TD><xsl:value-of select="DETAIL/@color2"/></TD>
																				   						</TR>
																							   	  </xsl:if>
																				   	  		</TABLE>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<!-- Prices -->
																			<TD width="30%" valign="top">
																				<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD bgcolor="#C1DEF3"> Price </TD>
																					</TR>

																					<TR>
																						<TD valign="top" nowrap="">
																							<TABLE width="100%" cellpadding="1" cellspacing="1">
																								<TR>
																									<TD> Price </TD>
																									<TD align="right">
																										<xsl:if test="PAYMENT/@discount_description !='Miles' and PAYMENT/@discount_description !='Points' and PAYMENT/@item_regular_price != PAYMENT/@item_price and PAYMENT/@discount_description != ''">
																											<SPAN class="strikethru">$<xsl:value-of select="PAYMENT/@item_regular_price"/></SPAN>&nbsp;&nbsp;
																										</xsl:if>
																										$<xsl:value-of select="PAYMENT/@item_price"/></TD>
																								</TR>

																								<xsl:for-each select="ADD_ONS/ADD_ON">
																									<xsl:variable name="addOnType" select="@type"/>
																									<TR>
																										<TD><xsl:if test="$addOnType = '1'">Balloon</xsl:if>
																											<xsl:if test="$addOnType = '2'">Bear</xsl:if>
																											<xsl:if test="$addOnType = '3'">Funeral Banner</xsl:if>
																											<xsl:if test="$addOnType = '4'">Card</xsl:if>
																											<xsl:if test="$addOnType = '5'">Chocolate</xsl:if>
																											(<xsl:value-of select="@quantity"/>)</TD>
																										<TD align="right">$<xsl:value-of select="@price"/>
																									</TD>
																									</TR>
																								</xsl:for-each>

																								<xsl:if test="PAYMENT/@service_fee != ''">
																									<TR>
																										<TD>Service Charge</TD>
																										<TD align="right">$<xsl:value-of select="format-number(number(PAYMENT/@service_fee),'###,##0.00')"/></TD>
																									</TR>
																								</xsl:if>		
																										
																								<xsl:if test="DELIVERY/@shipping_amount != ''">
																									<TR>
																										<TD><xsl:value-of select="DELIVERY/@shipping_type"/></TD>
																										<TD align="right">$<xsl:value-of select="format-number(number(DELIVERY/@shipping_amount),'###,##0.00')"/></TD>
																									</TR>
																								</xsl:if>


																								<TR>
																								  	<TD>Taxes</TD>
																										<TD align="right">$<xsl:value-of select="format-number(number(PAYMENT/@tax),'###,##0.00')"/></TD>
																								</TR>

																								<TR> <TD colspan="2"> <hr/> </TD> </TR>

																								<TR>
																									<TD><B>Item&nbsp;Total</B></TD>
																									<TD align="right"><B>$<xsl:value-of select="@total_price"/></B></TD>
																								</TR>

																								<TR> <TD colspan="2"> &nbsp; </TD> </TR>

																								<xsl:if test="PAYMENT/@discount_description = 'Miles'">
																									<TR>
																									  	<TD>Miles Earned</TD>
																										<TD align="right"><xsl:value-of select="PAYMENT/@discount_amount"/></TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="PAYMENT/@discount_description = 'Points'">
																									<TR>
																										<TD>Points Earned</TD>
																										<TD align="right"><xsl:value-of select="PAYMENT/@discount_amount"/></TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="PAYMENT/@discount_description = 'Charity'">
																									<TR>
																										<TD>Donation Amount</TD>
																										<TD align="right">$<xsl:value-of select="PAYMENT/@discount_amount"/></TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="PAYMENT/@discount_description = 'Cash Back'">
																									<TR>
																										<TD>Cash Back Bonus</TD>
																										<TD align="right">$<xsl:value-of select="PAYMENT/@discount_amount"/></TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="PAYMENT/@item_regular_price != PAYMENT/@item_price">
																									<xsl:if test="PAYMENT/@discount_description = 'Percent'">
																										<TR>
																											<TD>Percent Off</TD>
																											<TD align="right"><xsl:value-of select="PAYMENT/@discount_amount"/>%</TD>
																										</TR>
																									</xsl:if>

																									<xsl:if test="PAYMENT/@discount_description = 'Dollars'">
																										<TR>
																											<TD>Dollars Off</TD>
																											<TD align="right">$<xsl:value-of select="PAYMENT/@discount_amount"/></TD>
																										</TR>
																									</xsl:if>
																								</xsl:if>
																							</TABLE>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																			<!-- Delivery Information -->
																			<TD width="30%" valign="top">
																				<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD colspan="2" bgcolor="#C1DEF3">
																							Delivery Address
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																							<TABLE width="100%" border="0" cellpadding="1" cellspacing="1">

																								<xsl:if test="DELIVERY/@location_type != ''">
																									<TR>
																										<TD colspan="2" class="label" valign="top">
																											<xsl:value-of select="DELIVERY/@location_type"/>
																										</TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="DELIVERY/@location_name != ''">
																									<TR>
																										<TD colspan="2">
																											<xsl:value-of select="DELIVERY/@location_name"/><BR/>
																										</TD>
																									</TR>
																								</xsl:if>

																								<TR>
																									<TD colspan="2">
																										<xsl:value-of select="DELIVERY/RECIPIENT/@firstName"/>&nbsp;&nbsp;<xsl:value-of select="DELIVERY/RECIPIENT/@lastName"/><BR/>
																									</TD>
																								</TR>


																								<TR>
																									<TD colspan="2">
																										<xsl:value-of select="DELIVERY/RECIPIENT/@address1"/><BR/>
																										<xsl:if test="DELIVERY/RECIPIENT/@address2 != ''">
																											<xsl:value-of select="DELIVERY/RECIPIENT/@address2"/><BR></BR>
																										</xsl:if>
																									</TD>
																								</TR>

																								<TR>
																									<TD colspan="2">
																										<xsl:if test="DELIVERY/RECIPIENT/@city != ''"><xsl:value-of select="DELIVERY/RECIPIENT/@city"/>,</xsl:if>
																										<xsl:value-of select="DELIVERY/RECIPIENT/@state"/>&nbsp;
																										<xsl:value-of select="DELIVERY/RECIPIENT/@zip"/><BR/>
																									</TD>
																								</TR>

																								<TR>
																									<TD colspan="2">
																										<xsl:value-of select="DELIVERY/RECIPIENT/@countryName"/><BR/>
																									</TD>
																								</TR>

																								<TR>
																									<TD colspan="2">
																										<xsl:value-of select="DELIVERY/RECIPIENT/@phone"/><BR/><BR/>
																									</TD>
																								</TR>

																								<TR>
																									<TD class="label"> Delivery Date: </TD>
																									<TD> <xsl:value-of select="DELIVERY/@deliveryDateDisplay"/> </TD>
																								</TR>

																								<xsl:if test="DELIVERY/@hours_from != ''">
																									<TR>
																										<xsl:choose>
																											<xsl:when test="DELIVERY/@location_type = 'Funeral Home'">
																												<TD class="label"> Time of Service: </TD>
																											</xsl:when>
																											<xsl:otherwise>
																												<TD class="label"> Hours: </TD>
																											</xsl:otherwise>
																										</xsl:choose>
																										<TD width="50%"> <xsl:value-of select="DELIVERY/@hours_from"/> - <xsl:value-of select="DELIVERY/@hours_to"/> </TD>
																									</TR>
																								</xsl:if>

																								<xsl:if test="DELIVERY/@deliveryLocationExtra != ''">
																									<TR>
																										<TD class="label"> Room Number/Ward: </TD>
																										<TD width="50%"> <xsl:value-of select="DELIVERY/@deliveryLocationExtra"/></TD>
																									</TR>
																								</xsl:if>

																							</TABLE>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>

																		<xsl:if test="@custom = 'Y'">
																			<xsl:if test="COMMENT/@florist != ''">
																				<TR>
																					<TD> &nbsp; </TD>
																					<TD colspan="3" valign="top">
																						<SPAN class="label"> Florist Comment: </SPAN>&nbsp;&nbsp;
																					    <xsl:value-of select="COMMENT/@florist"/>
																					</TD>
																				</TR>
																			</xsl:if>

																			<xsl:if test="COMMENT/@item != ''">
																				<TR>
																					<TD> &nbsp; </TD>
																					<TD colspan="2" valign="top">
																						<SPAN class="label"> Item Comment: </SPAN> &nbsp;&nbsp;
																						<xsl:value-of select="COMMENT/@item"/>
																					</TD>
																				</TR>
																			</xsl:if>

																		</xsl:if>

																		<TR>
																			<TD>
																				&nbsp;
																			</TD>
																			<TD>
																				<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD>
																							<script language="javascript">
																								href = "javascript: editItem(" + itemNumber + ");";
																								<![CDATA[
																									document.write( "<A href=\"" + href + "\">Edit Item Information</A>" );
																								]]>
																							</script>
																						</TD>
																					</TR>

																					<TR>
																						<TD>
																							<script language="javascript">
																								href = "javascript: addOneMoreItem(" + itemNumber + ", '" + number_ + "');";
																								<![CDATA[
																									document.write( "<A href=\"" + href + "\">Add one (1) more to cart</A>" );
																								]]>
																							</script>
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																							<script language="javascript">
																								href = "javascript: changeProduct(" + itemNumber + ", '" + number_ + "', '" + countryType + "');";
																								<![CDATA[
																									document.write( "<A href=\"" + href + "\">Change Product</A>" );
																								]]>
																							</script>																							
																						</TD>
																					</TR>																					
																					
																				</TABLE>
																			</TD>

																			<TD>
																				<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD>
																							<xsl:if test="@custom = 'Y'">
																								<xsl:choose>
																									<xsl:when test="COMMENT/@florist != ''">
																										<script language="javascript">
																											href = "javascript: editFloristComment(" + itemNumber + ");";
																											<![CDATA[
																												document.write( "<A href=\"" + href + "\">Edit Florist Comment</A>" );
																											]]>
																										</script>
																									</xsl:when>
																									<xsl:otherwise>
																										<script language="javascript">
																											href = "javascript: addFloristComment(" + itemNumber + ");";
																											<![CDATA[
																												document.write( "<A href=\"" + href + "\">Add Florist Comment</A>" );
																											]]>
																										</script>
																									</xsl:otherwise>
																								</xsl:choose>
																							</xsl:if>
																						</TD>
																					</TR>
																					<TR>
																						<TD>
																							<xsl:if test="@custom = 'Y'">
																								<xsl:choose>
																									<xsl:when test="COMMENT/@item != ''">
																										<script language="javascript">
																											href = "javascript: editItemComment(" + itemNumber + ");";
																											<![CDATA[
																												document.write( "<A href=\"" + href + "\">Edit Item Comment</A>" );
																											]]>
																										</script>
																									</xsl:when>
																									<xsl:otherwise>
																										<script language="javascript">
																											href = "javascript: addItemComment(" + itemNumber + ");";
																											<![CDATA[
																												document.write( "<A href=\"" + href + "\">Add Item Comment</A>" );
																											]]>
																										</script>
																									</xsl:otherwise>
																								</xsl:choose>
																							</xsl:if>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>

																			<TD>
																				<TABLE width="100%" cellpadding="3" border="0">
																					<TR>
																						<TD>
																							<script language="javascript">
																								href = "javascript: editDeliveryInformation(" + itemNumber + ");";
																								<![CDATA[
																								document.write( "<A href=\"" + href + "\">Edit Delivery Information</A>" );
																								]]>
																							</script>
																						</TD>
																					</TR>

																					<TR>
																						<TD>
																							<script language="javascript">
																								href = "javascript: deliveryPolicy(" + itemNumber + ");";
																								<![CDATA[
																								document.write( "<A href=\"" + href + "\">Delivery Policies</A>" );
																								]]>
																							</script>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
											<!--end of ftd garden basket -->
										</TD>
									</TR>
							</xsl:for-each>


									<TR>
										<TD>
											<TABLE summary="remove all items" width="98%" border="0">
												<TR>
													<TD colspan="4" align="left">
														<A tabindex ="50" href="javascript:removeAllItem();">Remove all items in cart</A><BR></BR><BR></BR>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>

									<TR>
										<TD align="left">
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="2">
												<TR>
                                					<TD width="70%" valign="bottom">
														&nbsp;
                                					</TD>
                                					<TD width="15%" align="right" valign="top">
                                  						<IMG tabindex ="-1" src="../images/button_continue_shopping.gif" onclick="javascript:continueShopping();" alt="Continue Shopping" border="0"/>
                                					</TD>
                                					<TD width="2%" valign="top">
                                  						<B>or</B>
                                					</TD>
                                					<TD width="15%" align="left" valign="top">
                                 	 					<IMG tabindex ="-1" src="../images/button_checkout_now.gif" alt="Checkout Now" onclick="javascript:checkOut();" border="0"/>
                                					</TD>
												</TR>
											</TABLE>
											<BR></BR>
											<TABLE width="100%" border="0" cellpadding="1" cellspacing="2">
												<TR>
													<TD align="right" valign="top">
														<B>Shopping Cart Subtotal in US Dollars</B><BR></BR>
														(including shipping/service fees &amp; &nbsp;taxes)
													</TD>
													<TD width="10%" align="right" valign="top">
														<TABLE class="LookupTable" border="1" cellpadding="1" cellspacing="2">
															<TR> <TD> $ <xsl:value-of select="/root/ORDER/@totalOrderPrice"/> </TD> </TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
								</TABLE>
								<!-- end main table -->
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		</CENTER>
<DIV id="sourceCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">

<center>
<DIV id="sourceCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
</DIV>


</center>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD class="label" colspan="3" align="left">

					<font face="Verdana"><strong>Source Code Lookup </strong></font>
	            </TD>

	        </TR>
	        <TR>
	            <TD nowrap="true" colspan="3" align="left">
	            	<SPAN class="label"> <font face="Verdana">Source Code or Description:</font> </SPAN> &nbsp;&nbsp;
	            	<INPUT tabindex="3" onkeydown="javascript:EnterToSourcePopup()" type="text" name="sourceCodeInput" size="30" maxlength="50" onFocus="javascript:fieldFocus('sourceCodeInput')" onblur="javascript:fieldBlur('sourceCodeInput')"/> &nbsp;&nbsp;
	            </TD>
	        </TR>
	        <TR>
				<TD colspan="3" align="right">
				<IMG tabindex="4" onkeydown="javascript:EnterToSourcePopup()" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchSourceCode()"/>
             	<IMG tabindex="5" onkeydown="javascript:CloseSourceLookup()" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSourceCode()"/>
	         	</TD>
	        </TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>

</DIV>
	</FORM>
</BODY>


<SCRIPT language="JavaScript">

<![CDATA[
	function openSourceCodeLookup()
	{
		//In case the DIV is opened a second time
		document.forms[0].sourceCodeInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		sourcecodesearch = (dom)?document.getElementById("sourceCodeLookup").style : ie? document.all.sourceCodeLookup : document.sourceCodeLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		sourcecodesearch.top=scroll_top+document.body.clientHeight/2-100

		sourcecodesearch.width=290
		sourcecodesearch.height=100
		sourcecodesearch.left=document.body.clientWidth/2 - 100
		sourcecodesearch.top=document.body.clientHeight/2 - 20;
		sourcecodesearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the source code input with the source code on the page and set the focus
		//document.all.sourceCodeInput.value = document.all.txtSourceCode.value;
		document.all.sourceCodeInput.focus();
	}

	function goCancelSourceCode()
	{
		sourcecodesearch.visibility= "hidden";
	}

	function goSearchSourceCode()
	{
		//First validate the Source Code input
		var check = true;
		var sourceCode = document.forms[0].sourceCodeInput.value;
		sourceCode = stripWhitespace(sourceCode);

		if(sourceCode == "" || sourceCode.length < 2)
		{
		   document.forms[0].sourceCodeInput.focus();
           		document.forms[0].sourceCodeInput.style.backgroundColor='pink'
           		check = false;
           		alert("Please correct the marked fields");
		}

		if (check)
           openSourceCodePopup();
	}

	function openSourceCodePopup()
	{
		var val = document.all.sourceCodeInput.value;
		
		var dnisVal = "]]><xsl:value-of select="/root/pageHeader/headerDetail[@name='dnisType']/@value"/><![CDATA[";
		document.all.sourceCodeLookupSearching.style.visibility = "visible";

		var form = document.forms[0];
	    //var url_source="PopupServlet?sessionId=" + sessionId +"&persistentObjId=" + persistentObjId + "&POPUP_ID=LOOKUP_SOURCE_CODE&sourceCodeInput=" + val;
	    var url_source="PopupServlet?POPUP_ID=LOOKUP_SOURCE_CODE&sourceCodeInput=" + val + "&dnisType=" + dnisVal + "&companyId=" + companyId;
	    var modal_dim="dialogWidth:800px; dialogHeight:650px; center:yes; status=0";
 	    var ret =  window.showModalDialog(url_source,"", modal_dim);
 	    //var ret =  window.open(url_source);

 		//in case the X icon is clicked
 		if (!ret)
 			ret[0] = '';

 		//populate the text box
 		if (ret[0] != '')
 		{
			document.all.txtSourceCode.value = ret[0];
			document.all.sourceCodeDiv.innerText = ret[0] + " - " + ret[1];
		}

		sourcecodesearch.visibility= "hidden";
		document.all.sourceCodeLookupSearching.style.visibility = "hidden";

		var url = "ShoppingCartServlet?sessionId=" + sessionId_ +"&persistentObjId=" + persistentObjId_ +
		"&command=changeSourceCode&sourceCode=" + document.all.txtSourceCode.value + "&companyId=" + companyId;
		document.location = url;
	}

]]>
</SCRIPT>

</HTML>
<xsl:call-template name="footer"/>

</xsl:template>
</xsl:stylesheet>
