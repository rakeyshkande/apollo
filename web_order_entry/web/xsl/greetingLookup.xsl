<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="occasionInput" select="root/parameters/occasionInput"/>
<xsl:variable name="companyId" select="root/parameters/companyId"/>";
<xsl:variable name="imageSuffixSmall" select="root/parameters/imageSuffixSmall"/>";
<xsl:variable name="imageSuffixMedium" select="root/parameters/imageSuffixMedium"/>";
<xsl:variable name="imageSuffixLarge" select="root/parameters/imageSuffixLarge"/>";
<xsl:variable name="addonImageSuffixSmall" select="root/parameters/addonImageSuffixSmall"/>";
<xsl:variable name="addonImageSuffixMedium" select="root/parameters/addonImageSuffixMedium"/>";
<xsl:variable name="addonImageSuffixLarge" select="root/parameters/addonImageSuffixLarge"/>";

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> Lookup Greeting Card</TITLE>

<SCRIPT language="JavaScript" src="../js/FormChek.js"/>
<SCRIPT language="JavaScript" src="../js/util.js"/>
<SCRIPT language="JavaScript">

	var companyId = "<xsl:value-of select="$companyId"/>";

<![CDATA[

	//This function passes parameters from this page to a function on the calling page
	function populatePage(cardId)
	{
		window.returnValue = cardId;
		window.close();
	}

	function processPopup(cardId)
	{
		if ( window.event.keyCode == 13 )
		{
			openPopup(cardId);
		}
	}

	function closeGreetingLookup(cardId)
	{
		if ( window.event.keyCode == 13 )
		{
			populatePage(cardId);
		}
	}

	//This opens the detail page
	function openPopup(cardId)
	{
		var form = document.forms[0];
		var url_source="PopupServlet?POPUP_ID=LOOKUP_GREETING_CARD" +"&cardIdInput=" + cardId + "&occasionInput=" + document.forms[0].occasionInput.value  + "&companyId=" + companyId;
	    var modal_dim="dialogWidth:600px; dialogHeight:600px; center:yes";

	    //open the window
 	    var ret = window.showModalDialog(url_source,"",modal_dim);

		//if the return value is BACK then the back button was clicked, so keep this window open
 	    if(ret == "BACK")
 	    	return false;

 	    //if a return value is received from the detail page, then populate the main page
		if (ret != '')
		{
			populatePage(ret);
		}

 	    //if a blank return value is received from the detail page then also close this window
 	    if(ret == '')
		{
			window.close();
		}

		//the X icon was clicked, so keep this window open
 		if (!ret)
 			return false;
	}

	function init()
	{
		//Populate the occcasionInput
]]>
		document.forms[0].occasionInput.value = "<xsl:value-of select="$occasionInput"/>";
<![CDATA[
  	}
	]]>
</SCRIPT>

<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY onLoad="init();">
<FORM name="form" method="get" action="{$actionServlet}">
<CENTER>
<INPUT type="hidden" name="POPUP_ID" value="LOOKUP_GREETING_CARDS"/>
<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
<INPUT type="hidden" name="occasionInput" value=""/>
<input type="hidden" name="companyId" value="{$companyId}"/>
<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
    <TR>
    	<TD>
	    <TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD align="center">
	                <h1>Lookup Greeting Card</h1>
	            </TD>
	        </TR>
	        <TR>
	            <TD>
	            	<HR></HR>
            	</TD>
	        </TR>
		</TABLE>
		</TD>
	</TR>
</TABLE>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
     <TR>
		<TD class="instruction" colspan="3">
			Click on image or link for larger photo &amp; details or click on the button to select the card.
			<BR></BR>
			The card you select will be filled in automatically on the order checkout form.
		</TD>
	</TR>
	<TR>
        <TD align="right">
           	<IMG tabindex="1" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeGreetingLookup('')"/>
        </TD>
	</TR>
</TABLE>

<TABLE width="98%"  class="LookupTable">
  <TR valign="top">
	<TD>
		<TABLE width="98%" border="1" cellpadding="2" cellspacing="2">
			<TR valign="top">
				<SCRIPT language="JavaScript">
				<![CDATA[
					var counter = 0;
				]]>
				</SCRIPT>

				<xsl:for-each select="/root/greetingCardLookup/searchResults/searchResult">
				<TD align="center" width="33%" valign="top">
				<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
					<TR >
						<TD width="40%" align="center" valign="top">
							<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
								<TR>
									<TD align="center" valign="top">

										<SCRIPT language="JavaScript">
											var cardId = "<xsl:value-of select="@addonId"/>";
											var addonImageSuffixSmall = '<xsl:value-of select="$addonImageSuffixSmall"/>';
								            <![CDATA[
								              document.write("<IMG onclick=\"javascript: openPopup(\'" + cardId + "\')\"")
										      document.write("onkeydown=\"javascript: processPopup(\'" + cardId + "\')\" tabindex=\"2\" src=\"/product_images/" + cardId + addonImageSuffixSmall + "\">")
								              document.write("</IMG>")
								             ]]>
                     		            </SCRIPT>



										<SCRIPT language="JavaScript">
											<![CDATA[
											document.write("</A>")
											 ]]>
										</SCRIPT>
									</TD>
								</TR>
								<TR>
									<TD align="center" valign="top">
										<SCRIPT language="JavaScript">
								            var description = "<xsl:value-of select="@description"/>";
								            <![CDATA[
								              document.write("<A style=\"cursor:hand;\" onclick=\"javascript: openPopup(\'" + cardId + "\')\" ")
								              document.write("onkeydown=\"javascript: processPopup(\'" + cardId + "\')\" tabindex=\"2\"> ")
								              document.write("<SPAN class=\"label\"><FONT color=\"blue\"><U>" + description + "</U></FONT></SPAN></A>\ ")
								            ]]>
			        		            </SCRIPT>
									</TD>
								</TR>
								<TR>
									<TD align="center" valign="top">
									    <SCRIPT language="javascript">
											<![CDATA[
												//write out the function to populate the parent page
											]]>
												var cardId = "<xsl:value-of select="@addonId"/>";
												<![CDATA[
												document.write("<IMG onclick=\"javascript: populatePage(\'" + cardId + "\')\"")
												document.write("onkeydown=\"javascript: closeGreetingLookup(\'" + cardId + "\')\" tabindex=\"2\" ")
												document.write("src=\"../images/button_select.gif\"")
												document.write("width='45' height='15'</IMG>")
												]]>
										</SCRIPT>
									</TD>
								</TR>
							</TABLE>
						</TD>
						<TD rowspan="3" valign="top">
							<TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
								<TR> <TD colspan="2" class="label"> Front: </TD> </TR>
								<TR> <TD colspan="2"> <xsl:value-of select="@description"/> </TD> </TR>
								<TR> <TD colspan="2" class="label"> &nbsp; </TD> </TR>
								<TR> <TD colspan="2" class="label"> Inside: </TD> </TR>
								<TR> <TD colspan="2"> <xsl:value-of select="@inside"/><br></br> </TD> </TR>
								<TR> <TD colspan="2" class="label"> &nbsp; </TD> </TR>
								<TR>
									<TD width="20%" class="label"> Price: </TD>
									<TD width="80%">
									<SCRIPT language="javascript">
											<![CDATA[
												//format the price
											]]>
												var price = "<xsl:value-of select="@price"/>";
												<![CDATA[
												 var place = price.indexOf('.');
												 var rightOf = price.substring(place, 10);
												 if ((rightOf.length) == 2)
												 	price +="0";
												 document.write("&nbsp;$"+price+"")
												]]>
									</SCRIPT>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				</TD>
				<SCRIPT language="JavaScript">
				<![CDATA[
					//start a new row with each 4th card
					counter = counter + 1;
					if ((counter % 3) == 0) {
						document.write("</TR><TR>");
					}
			      ]]>
				</SCRIPT>
				</xsl:for-each>
			</TR>
		</TABLE>
	</TD>
</TR>
</TABLE>
<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
<TR>
    <TD align="right">
       	<IMG tabindex="3" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeGreetingLookup('')"/>
    </TD>
</TR>
</TABLE>
</CENTER>
</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>
