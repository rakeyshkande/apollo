<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="searchServlet" select="root/parameters/searchServlet"/>
<xsl:variable name="yellowPageBookCode" select="root/parameters/yellowPageBookCode"/>
<xsl:variable name="sourceSelect" select="root/parameters/sourceSelect"/>
<xsl:variable name="sourceText" select="root/parameters/sourceText"/>
<xsl:variable name="homePhone" select="root/parameters/homePhone"/>
<xsl:variable name="firstName" select="root/parameters/firstName"/>
<xsl:variable name="lastName" select="root/parameters/lastName"/>
<xsl:variable name="city" select="root/parameters/city"/>
<xsl:variable name="address" select="root/parameters/address"/>
<xsl:variable name="state" select="root/parameters/state"/>
<xsl:variable name="zip" select="root/parameters/zip"/>
<xsl:variable name="userFirstName" select="root/parameters/userFirstName"/>
<xsl:variable name="userLastName" select="root/parameters/userLastName"/>

<xsl:template match="/">

<HTML>
<HEAD>
<TITLE> FTD - Introduction </TITLE>
<SCRIPT language="JavaScript" src="../js/FormChek.js"> </SCRIPT>
<SCRIPT language="JavaScript" src="../js/util.js"> </SCRIPT>
<SCRIPT language="JavaScript">

	var sessionId = "<xsl:value-of select="$sessionId"/>";
	var persistentObjId = "<xsl:value-of select="$persistentObjId"/>";
	var searchServlet = "<xsl:value-of select="$searchServlet"/>";
	var yp = "no";

<![CDATA[
	document.onkeydown = backKeyHandler;
	document.oncontextmenu=stopIt;
	
	function init()
	{
	document.forms[0].sourceSelect.focus();
]]>
	document.forms[0].sourceSelect.value = "<xsl:value-of select="$sourceSelect"/>";
	<xsl:variable name="defSourceCode" select="/root/introductionData/dnis_information/dnis/@defaultSourceCode" />
	<xsl:if test="$sourceSelect =''">
		<xsl:if test="$sourceText =''">
 			document.forms[0].sourceSelect.value = "<xsl:value-of select="$defSourceCode"/>";
 			document.forms[0].selection[0].checked = "true";
   		</xsl:if>
   	</xsl:if>
	<xsl:if test="$sourceText =''">
 		document.forms[0].selection[0].checked = "true";
   	</xsl:if>
	<xsl:if test="$sourceText !=''">
 		document.forms[0].selection[1].checked = "true";
   	</xsl:if>

	document.forms[0].sourceText.value = "<xsl:value-of select="$sourceText"/>";
	document.forms[0].homePhone.value = "<xsl:value-of select="$homePhone"/>";
	document.forms[0].firstName.value = "<xsl:value-of select="$firstName"/>";
	document.forms[0].lastName.value = "<xsl:value-of select="$lastName"/>";


	<xsl:variable name="displayYellowPage" select="/root/introductionData/dnis_information/dnis/@yellowPagesFlag" />
 		<xsl:if test="$displayYellowPage ='Y'">
 			document.forms[0].yellowPageBookCode.value = "<xsl:value-of select="$yellowPageBookCode"/>";
			yp = "yes";
    </xsl:if>

<![CDATA[
	}

	function onKeyDown()
	{
	   if (window.event.keyCode == 13)
		{
		    submitForm();
		}
	}

	function EnterToSourcePopup()
	{
	   if (window.event.keyCode == 13)
		{
		    openSourceCodePopup();
		}
	}

	function CloseSourceLookup()
	{
	   if (window.event.keyCode == 13)
		{
		    goCancelSourceCode();
		}
	}


	function EnterToCustomerPopup()
	{
	   if (window.event.keyCode == 13)
		{
		    goSearchCustomer();
		}
	}

	function CloseCustomerLookup()
	{
	   if (window.event.keyCode == 13)
		{
		    goCancelSearch();
		}
	}

	function openCustomerLookup()
	{
		//In case the DIV is opened a second time
		document.forms[0].searchphone.style.backgroundColor='white'

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		customersearch = (dom)?document.getElementById("customerLookup").style : ie? document.all.customerLookup : document.customerLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		customersearch.top=scroll_top+document.body.clientHeight/2-10

		customersearch.width=290
		customersearch.height=100
		customersearch.left=document.body.clientWidth/2 - 100
		customersearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the lookup and place the cursor there
		document.all.searchphone.value = document.all.homePhone.value;
		document.all.searchphone.focus();
	}

	function goSearchCustomer()
	{
		//First validate the phone number input
		var check = true;
		var phone = document.forms[0].searchphone.value;
		var phone = stripWhitespace(phone);

	   //Check to make sure the Phone exists
		if(phone == "")
		{
			if (check == true)
			{
			  check = false;
			}
		}

		if (check == false)
		{
 			document.forms[0].searchphone.style.backgroundColor='pink'
 			document.forms[0].searchphone.focus();
			alert("Please correct the marked fields");
			return false;
		}

		//Every is valid, so open the popup
		var val = document.all.searchphone.value;
		document.all.homePhone.value = val;
		document.all.customerLookupSearching.style.visibility = "visible";

	    var url_source="PopupServlet?POPUP_ID=LOOKUP_CUSTOMER&phoneInput=" + val;
	    var modal_dim="dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
 	    var ret = window.showModalDialog(url_source,"", modal_dim);

		//in case the X icon is clicked
 		if (!ret)
 		{
			var ret = new Array();
 			ret[0] = '';
 		}

 		//Populate the text boxes
 		if (ret[0] != '')
 		{
		document.all.customerId.value = ret[0];
		document.all.firstName.value = ret[1];
		document.all.lastName.value = ret[2];
		document.all.homePhone.value = ret[9];
		}

		//Set the focus on the next field
		document.all.firstName.focus()

		customersearch.visibility="hidden";
		document.all.customerLookupSearching.style.visibility = "hidden";
	}

	function goCancelSearch()
	{

		//Set the focus on the next field
		document.all.firstName.focus()

		customersearch.visibility="hidden"
	}


    function resetSourceText() {
    /***
    * This function resets the Source Text field to null
    */
    document.forms[0].selection[0].checked = true;
    document.forms[0].sourceText.value = " ";

    }

    function resetSourceSelect() {
    /***
    * This function sets the Source Text Radio Button and sets the "other selection" option
    * for the Source Select DropDown
    */
    document.forms[0].selection[1].checked = 1;
    document.forms[0].sourceSelect.value = "X";
    }


    function validateForm() {
    /***
    * This function validates all fields and submits after all validatiion passes
    */
    openUpdateWin();

  	bag2 = ",/.<>?;:\|[]{}~!@#$%^&*()-_=+`" + "\\\'";

    var check = true;

    /***
    *   Check Source Text if it exist
    */
	document.forms[0].sourceText.className="TblText";
	document.forms[0].sourceText.className="TblText";
    var stnws = stripWhitespace(document.forms[0].sourceText.value);
	if (document.forms[0].selection[1].checked == 1)
   	{
    	if (!isAlphanumeric(stnws))
    	{
    		if (check == true)
    		{
    			document.forms[0].sourceText.focus();
    			check = false;
    		}
    		document.forms[0].sourceText.className="Error";

    	}
	}
	
	
	/***
	*   Check Source Select box if it exist
    	*/
    	if(document.forms[0].sourceSelect.value == "X" && document.forms[0].selection[0].checked == 1)
    	{
    		if (check == true)
    		{
    			document.forms[0].sourceSelect.focus();
    			check = false;
    		}
    		document.forms[0].sourceSelect.className="Error";    	
    	}

    /***
    *   Check Yellow Page Book Code if it exist
    */

]]>

 	<xsl:if test="$displayYellowPage ='Y'">
<![CDATA[

 		document.forms[0].yellowPageBookCode.className="TblText";
     	var ypnws = stripWhitespace(document.forms[0].yellowPageBookCode.value);
		if (ypnws.length > 0)
    	{
    		if (!isAlphanumeric(ypnws))
    		{
    		if (check == true)
    		{
    			document.forms[0].yellowPageBookCode.focus();
    			check = false;
    		}
    			document.forms[0].yellowPageBookCode.className="Error";
    		}
    	}

]]>
    </xsl:if>
<![CDATA[

    /***
    *   Check Home Phone Number if it exist
    */
	document.forms[0].homePhone.className="TblText";
    var bag = "()-";
    var emnws = stripCharsInBag(document.forms[0].homePhone.value, bag);
    var pnnws = stripWhitespace(emnws);
	if (pnnws.length > 0)
   	{
 	   if (!isAlphanumeric(pnnws))
  	  {
    		if (check == true)
    		{
    			document.forms[0].homePhone.focus();
    			check = false;
    		}
    		document.forms[0].homePhone.className="Error";
    	}
	}

    /***
    *   Check First Name if it exist
    */
    document.forms[0].firstName.className="TblText";
    var fxnws = stripCharsInBag(document.forms[0].firstName.value, bag2);
    var fnnws = stripWhitespace(fxnws);
	if (fnnws.length > 0)
   	{
	    if (!isAlphanumeric(fnnws))
 	   {
    		if (check == true)
    		{
    			document.forms[0].firstName.focus();
    			check = false;
    		}
    		document.forms[0].firstName.className="Error";
    	}
	}

    /***
    *   Check Last Name if it exist
    */
    document.forms[0].lastName.className="TblText";
    var lxnws = stripCharsInBag(document.forms[0].lastName.value, bag2);
    var lnnws = stripWhitespace(lxnws);
	if (lnnws.length > 0)
   	{
    	if (!isAlphanumeric(lnnws))
    	{
    		document.forms[0].lastName.className="Error";
    		if (check == true)
    		{

    			document.forms[0].lastName.focus();
    			check = false;
    		}
    	}
    }
 ///////////////////////////////////////////////////////////////////////////
//INITIATE SERVER SIDE VALIDATIION
///////////////////////////////////////////////////////////////////////////

	if(check == true && document.forms[0].selection[1].checked == 1)
	{
		addValidationEntry("SOURCE_CODE", introductionform.sourceText.value)
		addValidationEntry("SOURCE_CODE_DNIS_TYPE", "]]><xsl:value-of select="/root/pageHeader/headerDetail[@name='dnisType']/@value"/><![CDATA[")

		callToServer();
	} else
	{
		if(check == true)
		{
			introductionform.submit();
		}
	}

    return check;
}

    function doCancelOrder()
    {
    	//alert("Handle Cancel Order");
    	document.location = "CancelOrderServlet?sessionId=" + sessionId + "&persistentObjId=" + persistentObjId;
    }


	function submitForm()
    /***
    *   This function will validate the form and submit to the servlet if no errors found
    */
	{
	if(validateForm())
	{

	}
	else
	{
		closeUpdateWin();
		alert("Please correct the marked fields.")
	}
}


/////////////////////////////////////////////////////////////////////////////
//SERVER SIDE VALIDATION CALL BACK FUNCTIONS
////////////////////////////////////////////////////////////////////////////

function onValidationNotAvailable()
{
	closeUpdateWin();
	var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");
	if(x == true)
	{
		introductionform.submit();
	}
}

function onValidationResponse(validation)
{
	var ret_sourceCode = validation.SOURCE_CODE.value;
	closeUpdateWin();

	if(ret_sourceCode == "OK")
	{

		introductionform.submit();
	}
	else
	{
		//introductionform.sourceText.style.backgroundColor='pink';
		document.forms[0].sourceText.className="Error";
		alert(ret_sourceCode);
	}
}

///////////////////////////////////////////////////////////////////////////////

function openUpdateWin()
{
	var ie=document.all
	var dom=document.getElementById
	var ns4=document.layers

	cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
	updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
	scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
	updateWinV.top=scroll_top+document.body.clientHeight/2 - 200

	updateWinV.width=290
	updateWinV.height=100
	updateWinV.left=document.body.clientWidth/2 - 100
	updateWinV.visibility=(dom||ie)? "visible" : "show"
}

function closeUpdateWin()
{
	updateWin.style.visibility = "hidden";
}


]]>
</SCRIPT>
<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF" onload="init()">
	<FORM name="introductionform" method="get" action="{$actionServlet}">
	<CENTER>
		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
				<TD align="right"> <IMG src="../images/headerFAQ.gif"  onclick="javascript:doFAQ()"/> </TD>
			</TR>

			<TR> <TD colspan="6"> <HR> </HR> </TD> </TR>
		</TABLE>

		<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD width="65%"> &nbsp; </TD>
				<TD align="right" valign="top"><IMG id="shoppingCart" src="../images/icon_shopping_cart.gif" vspace="0" hspace="0" border="0"/><A tabindex="-1"><font color="#C0C0C0">Shopping Cart</font></A> </TD>
				<TD align="right" nowrap="true"><A tabindex="-1" href="javascript:doCancelOrder()" STYLE="text-decoration:none">Cancel Order</A> </TD>
			</TR>
			<TR>
				<TD colspan="4"> &nbsp; </TD>
			</TR>
		</TABLE>

    <!-- Main Table -->
	<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
					<TR>
						<TD class="ScreenPrompt" colspan="3">
							<xsl:choose>
								<xsl:when test="root/introductionData/scriptOverride/script[@fieldName='DNIS']">

									<xsl:value-of select="/root/introductionData/scriptOverride/script[@fieldName='DNIS']/@scriptText"/>. &nbsp;
									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='DNIS1']/@scriptText"/>

									<xsl:value-of select="$userFirstName"/>
									&nbsp;
									<xsl:value-of select="/root/introductionData/scriptOverride/script[@fieldName='DNIS2']/@scriptText"/>
								</xsl:when>

								<xsl:otherwise>

									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='DNIS']/@scriptText"/>. &nbsp;
									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='DNIS1']/@scriptText"/>

									<xsl:value-of select="$userFirstName"/>
									&nbsp;
									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='DNIS2']/@scriptText"/>
								</xsl:otherwise>
							</xsl:choose>
						</TD>
					</TR>
					<xsl:variable name="displayYellowPage1" select="/root/introductionData/dnis_information/dnis/@yellowPagesFlag" />
					<xsl:if test="$displayYellowPage1 ='Y'">
						<TR>
							<TD class="ScreenPrompt" colspan="3">
								<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='YELLOWPAGE']/@scriptText"/>
							</TD>
						</TR>
					</xsl:if>
					<TR>
						<TD class="label"> DNIS: </TD>
						<TD colspan="2"> <xsl:value-of select="/root/introductionData/dnis_information/dnis/@dnisId"/> </TD>
					</TR>

					<TR>
						<TD width="20%" class="label" valign="top"> Source Code: </TD>
						<TD valign="top">
							<INPUT tabindex="-1" type="radio" name="selection" value="bydescription" checked="true"></INPUT>
							<SELECT tabindex="1" name="sourceSelect" onchange="resetSourceText()" onFocus="javascript:fieldFocus('sourceSelect')" onblur="javascript:fieldBlur('sourceSelect')">
									<OPTION value="X">---Other Source Code---</OPTION>
							<xsl:for-each select="/root/introductionData/sourceCodes/sourceCode">

									<OPTION value="{@sourceCode}"> <xsl:value-of select="@sourceCode"/>-<xsl:value-of select="@description"/>

									</OPTION>
							</xsl:for-each>



							</SELECT>
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='SOURCESELECT']/@instructionText"/>
						</TD>
					</TR>

					<TR>
						<TD class="label" valign="top"> &nbsp; </TD>
						<TD valign="top">
							<INPUT tabindex="-1" type="radio" name="selection" value="bysourcecode" ></INPUT>
							<INPUT tabindex="2" type="text" name="sourceText" value="" size="10"  maxlength="10" class="TblText" onFocus="javascript:fieldFocus('sourceText')" onblur="javascript:fieldBlur('sourceText')" onKeypress="javascript:resetSourceSelect();" /> <BR/>
							<A tabindex="3" href="javascript: openSourceCodeLookup()">Lookup Source Code</A>
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='SOURCETEXT']/@instructionText"/>
						</TD>
					</TR>

					<TR>
						<TD class="label" valign="top"> &nbsp; </TD>

						<TD class="instruction" valign="top"> &nbsp; </TD>
					</TR>
					<xsl:if test="$displayYellowPage1 ='Y'">
					<TR>
						<TD class="label" valign="top"> Yellow Page Book Code </TD>
						<TD valign="top">
							<INPUT tabindex="6" type="text" name="yellowPageBookCode" value="" size="10" maxlength="10" class="TblText" onFocus="javascript:fieldFocus('yellowPageBookCode')" onblur="javascript:fieldBlur('yellowPageBookCode')"/> <BR/>
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='YELLOWPAGE']/@instructionText"/>
						</TD>
					</TR>
					</xsl:if>

					<TR> <TD colspan="3"> &nbsp; </TD> </TR>

					<TR>
						<TD class="ScreenPrompt" colspan="3">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='PHONE']/@scriptText"/>
						</TD>
					</TR>

					<TR>
						<TD class="label" valign="top"> Customer Phone Number: </TD>
						<TD valign="top">
							<INPUT tabindex="7" type="text" name="homePhone" size="8" maxlength="20" value="" class="TblText" onFocus="javascript:fieldFocus('homePhone')" onblur="javascript:fieldBlur('homePhone')"/> <BR/>
							<A tabindex="8" href="javascript: openCustomerLookup()">Lookup Customer</A>
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='PHONE']/@instructionText"/>
						</TD>
					</TR>

					<TR> <TD colspan="3"> &nbsp; </TD> </TR>

					<TR>
						<TD class="ScreenPrompt" colspan="3">
							<xsl:choose>
								<xsl:when test="root/introductionData/scriptOverride/script[@fieldName='FIRSTNAME']">
									<xsl:value-of select="/root/introductionData/scriptOverride/script[@fieldName='FIRSTNAME']/@scriptText"/> &nbsp;
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='FIRSTNAME']/@scriptText"/> &nbsp;
								</xsl:otherwise>
							</xsl:choose>
						</TD>
					</TR>

					<TR>
						<TD class="label"> Customer First Name: &nbsp; </TD>
						<TD>
							<INPUT tabindex="9" type="text" name="firstName" size="25" maxlength="20" value="" class="TblText" onFocus="javascript:fieldFocus('firstName')" onblur="javascript:fieldBlur('firstName')" />
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='FIRSTNAME']/@instructionText"/>
						</TD>
					</TR>

					<TR> <TD colspan="3"> &nbsp; </TD> </TR>

					<TR>
						<TD class="ScreenPrompt" colspan="3">
							<xsl:choose>
								<xsl:when test="root/introductionData/scriptOverride/script[@fieldName='LASTNAME']">
									<xsl:value-of select="/root/introductionData/scriptOverride/script[@fieldName='LASTNAME']/@scriptText"/> &nbsp;
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='LASTNAME']/@scriptText"/>
								</xsl:otherwise>
							</xsl:choose>
						</TD>
					</TR>

					<TR>
						<TD class="label"> Customer Last Name: </TD>
						<TD>
							<INPUT tabindex="13" onkeydown="javascript:onKeyDown()" type="text" name="lastName" size="25" maxlength="30" value="" class="TblText" onFocus="javascript:fieldFocus('lastName')" onblur="javascript:fieldBlur('lastName')" />
						</TD>
						<TD class="instruction" valign="top">
							<xsl:value-of select="/root/introductionData/scripting/script[@fieldName='LASTNAME']/@instructionText"/>
						</TD>
					</TR>

					<TR> <TD colspan="3"> &nbsp; </TD> </TR>

					<TR>
						<TD id="continutImage" colspan="3" align="right">
							<IMG onkeydown="javascript:onKeyDown()" tabindex="14" src="../images/button_continue.gif" width="74" height="17" onClick="javascript:submitForm()" /> &nbsp;&nbsp;
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>

	<TABLE width="98%" border="0" cellpadding="0" cellspacing="0">
			<TR><td>&nbsp;</td></TR>
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
	</TABLE>
		<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
		<INPUT type="hidden" name="source" value="introduction"/>
		<INPUT type="hidden" name="city" value="{$city}"/>
		<INPUT type="hidden" name="address" value="{$address}"></INPUT>
		<INPUT type="hidden" name="state" value="{$state}"></INPUT>
		<INPUT type="hidden" name="zip" value="{$zip}"></INPUT>
		<INPUT type="hidden" name="search" value=""></INPUT>
		<xsl:variable name="dnisid" select="/root/introductionData/dnis_information/dnis/@dnisId"/>
		<INPUT type="hidden" name="dnis" value="{$dnisid}"></INPUT>
		<INPUT type="hidden" name="customerId" value=""></INPUT>


	</CENTER>


<DIV id="customerLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
<center>
<DIV id="customerLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
</DIV>
</center>


			<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD class="label" colspan="3" align="left">

					<font face="Verdana"><strong>Lookup Customer</strong></font>
	            </TD>

	        </TR>
	        <TR>
	            <TD nowrap="true" colspan="3" align="left">
	            	<SPAN class="label"> <font face="Verdana">Phone Number:</font> </SPAN> &nbsp;&nbsp;
	            	<INPUT tabindex="9" onkeydown="javascript:EnterToCustomerPopup()" type="text" name="searchphone" size="30" maxlength="50" onFocus="javascript:fieldFocus('searchphone')" onblur="javascript:fieldBlur('searchphone')"/> &nbsp;&nbsp;
	            </TD>
	        </TR>
	        <tr>
				<TD colspan="3" align="right">
					<IMG onkeydown="javascript:EnterToCustomerPopup()" tabindex="10" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchCustomer()"/>
 	            	<IMG onkeydown="javascript:CloseCustomerLookup()" tabindex="11" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSearch()"/>
	         	</TD>

	        </tr>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>
</DIV>


<DIV id="sourceCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">

<center>
<DIV id="sourceCodeLookupSearching" style="VISIBILITY: hidden;">
	<font face="Verdana" color="#FF0000">Please wait ... searching!</font>
</DIV>
<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
	<br/><br/><p align="center"> <font face="Verdana">Please wait ... updating!</font> </p>
</DIV>


</center>

<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
	        <TR>
	            <TD class="label" colspan="3" align="left">

					<font face="Verdana"><strong>Source Code Lookup </strong></font>
	            </TD>

	        </TR>
	        <TR>
	            <TD nowrap="true" colspan="3" align="left">
	            	<SPAN class="label"> <font face="Verdana">Source Code or Description:</font> </SPAN> &nbsp;&nbsp;
	            	<INPUT tabindex="4" onkeydown="javascript:EnterToSourcePopup()" type="text" name="sourceCodeInput" size="30" maxlength="50" onFocus="javascript:fieldFocus('sourceCodeInput')" onblur="javascript:fieldBlur('sourceCodeInput')"/> &nbsp;&nbsp;
	            </TD>
	        </TR>
	        <TR>
				<TD colspan="3" align="right">
				<IMG onkeydown="javascript:EnterToSourcePopup()" tabindex="4" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchSourceCode()"/>
             			<IMG onkeydown="javascript:CloseSourceLookup()" tabindex="5" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSourceCode()"/>
	         	</TD>
	        </TR>
	        <TR>
	            <TD colspan="3"></TD>
	        </TR>
	    </TABLE>

</DIV>

</FORM>


<!--
	SERVER SIDE VALIDATION SECTION
-->

					<xsl:call-template name="validation"/>


</BODY>

<SCRIPT language="JavaScript">

<![CDATA[
	function openSourceCodeLookup()
	{
		//In case the DIV is opened a second time
		document.forms[0].sourceCodeInput.style.backgroundColor='white';

		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		sourcecodesearch = (dom)?document.getElementById("sourceCodeLookup").style : ie? document.all.sourceCodeLookup : document.sourceCodeLookup
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		sourcecodesearch.top=scroll_top+document.body.clientHeight/2 -10

		sourcecodesearch.width=290
		sourcecodesearch.height=100
		sourcecodesearch.left=document.body.clientWidth/2 - 100
		sourcecodesearch.visibility=(dom||ie)? "visible" : "show"

		//Populate the source code input with the source code on the page and set the focus
		document.all.sourceCodeInput.value = document.all.sourceText.value;
		document.all.sourceCodeInput.focus();

		//Select the option button
		document.forms[0].selection[1].checked = "true";



	}

	function goCancelSourceCode()
	{
		sourcecodesearch.visibility= "hidden";
		document.forms[0].sourceText.focus();
	}

	function goSearchSourceCode()
	{
		//First validate the Source Code input
		var check = true;
		var sourceCode = document.forms[0].sourceCodeInput.value;
		var sourceCode = stripWhitespace(sourceCode);

		if(sourceCode == "" || sourceCode.length < 2)
		{
		   document.forms[0].sourceCodeInput.focus();
           document.forms[0].sourceCodeInput.style.backgroundColor='pink'
           check = false;
           alert("Please correct the marked fields");
		}

		if (check)
           openSourceCodePopup();
	}

	function openSourceCodePopup()
	{
		var val = document.all.sourceCodeInput.value;
		var dnisVal = "]]><xsl:value-of select="/root/pageHeader/headerDetail[@name='dnisType']/@value"/><![CDATA[";

		//First validate the sourcd code input
		var check = true;
		var val = stripWhitespace(val);

	   	//Check to make sure the code exists
		if(val == "")
		{
			if (check == true)
			{
			  check = false;
			}
		}

		if (check == false)
		{
			document.all.sourceCodeInput.style.backgroundColor='pink'
			document.all.sourceCodeInput.focus();
			alert("Please correct the marked fields");
			return false;
		}

		//Every is valid, so open the popup
		document.all.sourceCodeLookupSearching.style.visibility = "visible";

		var form = document.forms[0];

	    var url_source="PopupServlet?POPUP_ID=LOOKUP_SOURCE_CODE&sourceCodeInput=" + val + "&dnisType=" + dnisVal;
	    var modal_dim="dialogWidth:800px; dialogHeight:650px; center:yes; status=0";
	    var ret =  window.showModalDialog(url_source,"", modal_dim);
	    //var ret =  window.open(url_source,"", modal_dim);

 		//in case the X icon is clicked
 		if (!ret)
 		{
                    var ret = new Array();
                    ret[0] = '';
 		}

 	 	//clear the dropdown
 	    	document.all.sourceSelect.value = "X";

 		//populate the text box
 		if (ret[0] != '')
			document.all.sourceText.value = ret[0];

		//Set the focus on the next field
		document.forms[0].sourceText.focus();

		sourcecodesearch.visibility= "hidden";
		document.all.sourceCodeLookupSearching.style.visibility = "hidden";
	}

]]>
</SCRIPT>

</HTML>
</xsl:template>
</xsl:stylesheet>
