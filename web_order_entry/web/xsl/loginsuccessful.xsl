<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY sepd "&#168;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>

<xsl:template match="/">

<script language="javascript" src="../js/FormChek.js"> </script>
<script language="javascript" src="../js/util.js"> </script>

<HTML>
<HEAD>
	<TITLE> FTD - Login Successful </TITLE>
	
	<SCRIPT language="JavaScript">
		var sessionId = '<xsl:value-of select="$sessionId"/>';
		var actionServlet = '<xsl:value-of select="$actionServlet"/>';
	
<![CDATA[

		function openWin() {
			// open a new window to display work pages disabling most options 
			// except scrollbars. 
			
			var url_source = actionServlet + "?sessionId=" + sessionId;	

			var win = window.open(url_source, 'oeWindow', 'toolbar=no,menubar=no,scrollbars,location=no,directories=no,resizable,status');
			
			win.moveTo(0,0);
			win.resizeTo(screen.availWidth, screen.availHeight);
		}
]]>
	</SCRIPT>
	
	<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>

<BODY onLoad="javascript:openWin();" marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
	<FORM name="loginform" method="post" action="/oedev/servlet/LoginServlet" target="_top">
	<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
		<CENTER>
		<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
			<TR>
				<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
			</TR>
	
			<TR> <TD colspan="4"> <HR/> </TD> </TR>
		</TABLE>
		
	    <!-- Main Table -->
		<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
						<TR>
                    		<TD width="100%" align="center" bgcolor="#ffffff"><FONT size="+1">You are successfully logged into the FTD.COM Order Entry System</FONT> </TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		
		<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
			<TR>
				<xsl:call-template name="footer"/>
			</TR>
		</TABLE>
		</CENTER>
	</FORM>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>