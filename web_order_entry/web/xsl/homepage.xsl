<!DOCTYPE ACDemo [
	<!ENTITY nbsp "&#160;">
	<!ENTITY copy "&#169;">
	<!ENTITY sepd "&#168;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="validation.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:variable name="menupage" select="root/parameters/menupage"/>
<xsl:variable name="context" select="root/parameters/context"/>
<xsl:variable name="sessionId" select="root/parameters/sessionId"/>
<xsl:variable name="persistentObjId" select="root/parameters/persistentObjId"/>
<xsl:variable name="actionServlet" select="root/parameters/actionServlet"/>
<xsl:variable name="GENERALMAINT" select="root/parameters/GENERALMAINT"/>
<xsl:variable name="pdbServlet" select="root/parameters/pdbServlet"/>

<xsl:template match="/">

<script language="javascript" src="../js/FormChek.js">
</script>
<script language="javascript" src="../js/util.js">
</script>



<HTML>
<HEAD>
<TITLE> FTD - Home </TITLE>
<SCRIPT language="JavaScript">

menupage = "<xsl:value-of select="$menupage"/>";
context = "<xsl:value-of select="$context"/>";
securitytoken = "<xsl:value-of select="$sessionId"/>";

<![CDATA[
//<!--
	var pdbServlet = "";

	document.oncontextmenu=stopIt;
	

	document.onkeydown = backKeyHandler;

	function init()
	{
]]>
		pdbServlet = "<xsl:value-of select="$pdbServlet"/>";
<![CDATA[
		document.forms[0].dnis.focus();
	}

	function deCode(strSelection)
	{
		strSelection = strSelection.replace(new RegExp("&lt;","g"), "<");
		strSelection = strSelection.replace(new RegExp("&gt;","g"), ">");

		return strSelection;
	}

	function submitForm()
	{
		validateForm();

		return false;
	}


	function menu()
	    /***
	    *   This function redirects to the Cancel Order page
	    */
	{
	      

		theUrl = menupage + "?context=" + escape(context) + "&securitytoken=" + securitytoken + "&adminAction=customerService";

		
		        	
        	document.location=theUrl;


	    //var url = "LogoutServlet?sessionId=" + document.all.sessionId.value + "&persistentObjId=" + document.all.persistentObjId.value;
	    //document.location = url;
	    //alert("You will now proceed to the Logoff Page, Please Drive Safe");

	}


	function openUpdateWin()
	{
		var ie=document.all
		var dom=document.getElementById
		var ns4=document.layers

		cen=(ie)? document.body.clientWidth/2-155 : (dom)?window.innerWidth/2-155  : window.innerWidth/2-155
		updateWinV = (dom)?document.getElementById("updateWin").style : ie? document.all.updateWin : document.updateWin
		scroll_top=(ie)? document.body.scrollTop : window.pageYOffset
		updateWinV.top=scroll_top+document.body.clientHeight/2-100

		updateWinV.width=290
		updateWinV.height=100
		updateWinV.left=document.body.clientWidth/2 - 100
		updateWinV.top=document.body.clientHeight/2 - 50;
		updateWinV.visibility=(dom||ie)? "visible" : "show"
	}

	function closeUpdateWin()
	{
		updateWin.style.visibility = "hidden";
	}

	function validateForm()
	{
		openUpdateWin();

		var check = true;
		var msg = "";
		if ( document.forms[0].dnis.value.length == 0 )
		{
			msg = "DNIS Number is required and must be 4 digits in length";
			check = false;
		}

		else if ( document.forms[0].dnis.value.length != 4 )
		{
			msg = "DNIS Number must be 4 digits in length";
			check = false;
		}

		else if ( !isInteger( document.forms[0].dnis.value ) )
		{
			msg = "DNIS Number must be numeric";
			check = false;
		}

		if ( check == false )
		{
			closeUpdateWin();
			document.forms[0].dnis.focus();
			document.forms[0].dnis.style.backgroundColor='pink'
			alert(msg);
			return;
		}

	///////////////////////////////////////////////////////////////////////////
	//INITIATE SERVER SIDE VALIDATIION
	///////////////////////////////////////////////////////////////////////////

		if(check == true)
		{
			addValidationEntry("DNIS", dnisform.dnis.value)
			callToServer();
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	//SERVER SIDE VALIDATION CALL BACK FUNCTIONS
	////////////////////////////////////////////////////////////////////////////

	function onValidationNotAvailable()
	{
		closeUpdateWin();
		var x = confirm("Validation for this page was not fully completed. Would you still like to continue?");
		if(x == true)
			dnisform.submit();
	}

	function onValidationResponse(validation)
	{
		var ret_dnis = validation.DNIS.value;
		closeUpdateWin();

		if(ret_dnis == "OK")
		{
			dnisform.submit();
		}
		else
		{
			if(ret_dnis == "DNIS is a customer service DNIS")
			{

				msg2 = "The DNIS code you provided indicates a customer service call.  Please use the customer service features on the HP application and input the following DNIS: \n\n"
				+ dnisform.dnis.value
				+ "\n\nTo use this DNIS number and continue with Order Entry, click \"OK\" "
				+ "To begin a new order, click \"Cancel\"";
				//var x = confirm(msg2)
				//if(x == true)
				//{
					dnisform.submit();
				//} else {
					//dnisform.dnis.style.backgroundColor='white';
				//}
			} else {
				dnisform.dnis.style.backgroundColor='pink';
				document.forms[0].dnis.focus();
				alert(ret_dnis);
			}

		}
	}
	///////////////////////////////////////////////////////////////////////////////
]]>


</SCRIPT>
<script language="javascript">

	function headerNonOrders()
	{
		alert( "headerNonOrders" );
	}

	function headerAdmin()
	{
		var url = pdbServlet + "?sessionId=" + document.all.sessionId.value;
    		document.location = url;
	}

</script>

<LINK rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</HEAD>


        
        



<BODY onload="javascript:init();" marginheight="0" marginwidth="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
	<FORM name="dnisform" method="get" action="{$actionServlet}" onsubmit="javascript: return submitForm();">
	<CENTER>
	<TABLE width="98%" border="0" cellspacing="2" cellpadding="0">
		<TR>
			<TD width="80%" align="left"><IMG src="../images/wwwftdcom.gif"/></TD>
	<!--
			<TD align="right"><IMG src="../images/headerNonOrders.gif" onclick="javascript:headerNonOrders();"/></TD>
	-->
		<xsl:choose>
		<xsl:when test="$GENERALMAINT = 'true'">
			<TD align="right"><IMG src="../images/headerAdmin.gif" onclick="javascript:headerAdmin();"/></TD>
		</xsl:when>
		</xsl:choose>
			<TD align="right"><IMG src="../images/headerFAQ.gif" onclick="javascript:doFAQ();"/></TD>
			<TD align="right"><IMG src="../images/headerExit.gif" onclick="javascript:menu();"/></TD>			
		</TR>

		<TR> <TD colspan="5"> <HR></HR> </TD> </TR>
	</TABLE>

    <!-- Main Table -->
	<TABLE width="98%" border="3" bordercolor="#006699" cellspacing="1" cellpadding="1">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
					<TR>
						<TD colspan="3" class="instruction">
							&nbsp;
						</TD>
					</TR>
					<TR>
						<TD colspan="3" class="instruction">
							<xsl:value-of select="/root/DNIS_SCRIPT/SCRIPTING/SCRIPT[@fieldName='DAILYINSTRUCTIONS']/@scriptText"/>
						</TD>
					</TR>
					<TR>
						<TD colspan="3" class="instruction">
							&nbsp;
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellpadding="2" cellspacing="2">
					<TR>
						<TD class="ScreenPrompt" colspan="3">
						<xsl:value-of select="/root/DNIS_SCRIPT/SCRIPTING/SCRIPT[@fieldName='DNIS']/@scriptText"/>
						</TD>
					</TR>

					<TR>
						<TD width="20%" class="label"> DNIS Number: &nbsp; </TD>
						<TD width="30%">
							<INPUT type="text" name="dnis" size="2" maxlength="4" value="" onFocus="javascript:fieldFocus('dnis')" onblur="javascript:fieldBlur('dnis')"/>
							&nbsp; <SPAN style="color:red"> *** </SPAN>
							<INPUT type="hidden" name="sessionId" value="{$sessionId}"/>
							<INPUT type="hidden" name="persistentObjId" value="{$persistentObjId}"/>
							<INPUT type="hidden" name="reqsource" value="homepage"/>
						</TD>
						<TD width="50%" class="instruction">
							Enter 4 Digit DNIS #
						</TD>
					</TR>

					<TR> <TD colspan="3"> <HR></HR> </TD> </TR>

					<TR>
						<TD colspan="3" align="right">
							<IMG src="../images/button_continue.gif" width="74" height="17" onclick="javascript:submitForm()"/> &nbsp;&nbsp;
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>

	<TABLE width="98%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD> &nbsp; <SPAN style="color:red"> *** - required field</SPAN> </TD>
		</TR>
	</TABLE>

	<TABLE width="98%" border="0" cellpadding="2" cellspacing="2">
		<TR>
			<xsl:call-template name="footer"/>
		</TR>
	</TABLE>
	</CENTER>

	<DIV id="updateWin" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
		<br/><br/><p align="center"> <font face="Verdana">Please wait ... validating input!</font> </p>
	</DIV>

	</FORM>


<!--
	SERVER SIDE VALIDATION SECTION
-->

					<xsl:call-template name="validation"/>

</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>